<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026879777</ID>
<ANCIEN_ID>JG_L_2012_12_000000364645</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/87/97/CETATEXT000026879777.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 27/12/2012, 364645, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-12-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364645</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2012:364645.20121227</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée le 19 décembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentée pour la Fédération CFTC de l'agriculture (CFTC-AGRI), dont le siège est 2, rue Albert Camus à Paris (75010), et la Fédération générale des travailleurs de l'agriculture, de l'alimentation, des tabacs et des services annexes Force ouvrière (FGTA-FO), dont le siège est 7, passage Tenaille à Paris (75014), représentées par leurs représentants légaux ; les fédérations requérantes demandent au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution du I du 8° de l'article 1er du décret n° 2012-838 du 29 juin 2012 relatif aux élections aux chambres d'agriculture ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative, ensemble les entiers dépens de l'instance, y compris le coût du timbre fiscal en application de l'article R. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              elles soutiennent que :<br/>
              - la condition d'urgence est remplie dès lors que la date limite de dépôt des candidatures aux élections des chambres départementales d'agriculture est fixée au 2 janvier 2013, l'impossibilité de constituer des listes dans 47 départements emporterait des conséquences importantes quant à leur représentativité tant au niveau de la branche qu'au niveau nationale ;<br/>
              - en imposant la présence d'au moins un tiers de candidats de sexe féminin au sein des listes électorales, le décret contesté a méconnu le domaine réservé à la loi tant par l'article 34 que par l'article 1er de la Constitution ;<br/>
              - les dispositions litigieuses méconnaissent la liberté syndicale, garantie par la Constitution et par le droit de l'Union européenne ;<br/>
              - le décret contesté est entaché d'une erreur manifeste d'appréciation dès lors que l'obligation qu'il introduit ne peut être respectée par les organisations syndicales dans un grand nombre de départements faute qu'un nombre suffisant de femmes soient employées comme salariés agricoles ;<br/>
<br/>
<br/>
              Vu le décret dont la suspension de l'exécution est demandée ;<br/>
<br/>
              Vu la copie de la requête à fin d'annulation de ce décret ; <br/>
<br/>
              Vu le mémoire en défense, enregistré le 26 décembre 2012, présenté par le ministre de l'agriculture, de l'agroalimentaire et de la forêt, qui conclut au rejet de la requête ; il soutient que : <br/>
              - la requête est irrecevable dès lors que les fédérations requérantes n'ont ni intérêt ni qualité pour agir ;<br/>
              - la condition d'urgence n'est pas remplie, l'impossibilité de constituer des listes dans 47 départements n'étant pas imputable au décret contesté, mais à la représentativité des fédérations requérantes ;<br/>
              - les fédérations requérantes, qui ont saisi le juge des référés du Conseil d'Etat plus de cinq mois après la publication du décret contesté, ont contribué à créer la situation d'urgence qu'elles invoquent ;<br/>
              - il existe un intérêt public justifiant le maintien du décret litigieux ;<br/>
              - le pouvoir réglementaire était compétent pour édicter le décret contesté, qui régit l'organisation des élections au conseil d'administration d'un établissement public ;<br/>
              - les dispositions en litige ne méconnaissent pas l'article 1er de la Constitution ;<br/>
              - les dispositions en litige ne méconnaissent aucun principe fondamental du droit du travail, et ne constituent notamment pas une atteinte à la liberté syndicale ;<br/>
              - le décret contesté n'est entaché d'aucune erreur manifeste d'appréciation dès lors que les dispositions tendant à l'inclusion dans chaque liste de candidats des collèges de salariés de la production agricole aux élections des chambres d'au moins un candidat de sexe féminin par tranche de trois candidats sont directement applicables compte tenu de la composition démographique du salariat agricole ; <br/>
<br/>
              Vu les pièces du dossier desquelles il résulte que la requête a été communiquée au Premier ministre ;<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu la Constitution ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la Fédération CFTC de l'agriculture (CFTC-AGRI), la Fédération générale des travailleurs de l'agriculture, de l'alimentation, des tabacs et des services annexes Force ouvrière (FGTA-FO) et, d'autre part, le ministre de l'agriculture, de l'agroalimentaire et de la forêt ainsi que le Premier ministre ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 27 décembre 2012 à 10 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Luc-Thaler, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la Fédération CFTC de l'agriculture (CFTC-AGRI) et de la Fédération générale des travailleurs de l'agriculture, de l'alimentation, des tabacs et des services annexes Force ouvrière (FGTA-FO) ;<br/>
<br/>
              - les représentants de la Fédération CFTC de l'agriculture (CFTC-AGRI) ;<br/>
<br/>
              - les représentantes du ministre de l'agriculture, de l'agroalimentaire et de la forêt ;<br/>
<br/>
              et à l'issue de laquelle l'instruction a été close ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              2. Considérant que la condition d'urgence à laquelle est subordonné le prononcé d'une mesure de suspension doit être regardée comme remplie lorsque la décision administrative contestée préjudicie de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés, saisi d'une demande tendant à la suspension d'une telle décision, d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de celle-ci sur la situation de ce dernier ou, le cas échéant, des personnes concernées, sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ;<br/>
<br/>
              3. Considérant que pour demander la suspension du I de l'article 8 du décret du 29 juin 2012 imposant qu'un candidat sur trois présenté par un syndicat pour l'élection des membres représentants les salariés des conseils d'administration des chambres d'agriculture soit d'un autre sexe que les deux autres, les syndicats requérants soutiennent que cette disposition les empêche, en raison de la trop faible proportion de femmes salariées dans un emploi agricole susceptibles de figurer sur une liste dans un grand nombre de départements, de présenter des listes dans 47 départements ; qu'il ressort cependant de l'instruction que la proportion de femmes salariées dans l'agriculture est en moyenne de 30%, et que les syndicats, informés depuis juillet 2012 de l'exigence de représentation féminine disposaient du temps suffisant pour se mettre en conformité avec les dispositions critiquées qu'aucune impossibilité matérielle ne les empêchait de respecter ; qu'ainsi, les difficultés qu'ils rencontrent pour composer des listes ne découlent pas, ainsi qu'il résulte notamment de l'audience qui n'a permis d'établir aucune corrélation entre la proportion de femmes salariées dans l'agriculture dans un département et les difficultés des syndicats pour y constituer une liste, des contraintes imposées par les dispositions contestées, mais de la situation que ces dispositions ont précisément pour objet de corriger afin d'assurer la mise en oeuvre de l'objectif constitutionnel de promotion de l'égalité entre femmes et hommes dans l'exercice des responsabilités professionnelles et sociales énoncé par l'article 1er de la Constitution ; qu'en l'absence, dès lors, de toute urgence, dans ces circonstances, à suspendre les dispositions critiquées, la requête des deux syndicats ne peut qu'être rejetée ;<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la Fédération CFTC de l'agriculture (CFTC-AGRI) et de la Fédération générale des travailleurs de l'agriculture, de l'alimentation, des tabacs et des services annexes Force ouvrière (FGTA-FO) est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à la Fédération CFTC de l'agriculture (CFTC-AGRI), à la Fédération générale des travailleurs de l'agriculture, de l'alimentation, des tabacs et des services annexes Force ouvrière (FGTA-FO), au Premier ministre et au ministre de l'agriculture, de l'agroalimentaire et de la forêt.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
