<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043080021</ID>
<ANCIEN_ID>JG_L_2021_01_000000435279</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/08/00/CETATEXT000043080021.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 28/01/2021, 435279</TITRE>
<DATE_DEC>2021-01-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>435279</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Sébastien Gauthier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:435279.20210128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et trois nouveaux mémoires enregistrés le 10 octobre 2019, les 10 janvier et 4 décembre 2020, et les 4 et 7 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, M. D... B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la décision implicite de rejet née du silence gardé par le ministre de l'intérieur sur la demande, qu'il lui a adressée le 10 avril 2019, de retrait du décret du 3 août 1977 portant libération de ses liens d'allégeance avec la France ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre et au ministre de l'intérieur de retirer ce décret du 3 août 1977 ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le code de la nationalité française ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sébastien Gauthier, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme A... C..., rapporteure publique,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 91 du code de la nationalité française, en vigueur à la date du décret attaqué : " Perd la nationalité française, le Français même mineur, qui, ayant une nationalité étrangère, est autorisé, sur sa demande, par le Gouvernement français, à perdre la qualité de Français. / Cette autorisation est accordée par décret ". <br/>
<br/>
              2. Aux termes de l'article 24 du code civil : " La réintégration dans la nationalité française des personnes qui établissent avoir possédé la qualité de Français résulte d'un décret ou d'une déclaration suivant les distinctions fixées aux articles ci-après ". L'article 24-1 du même code dispose que " La réintégration par décret peut être obtenue à tout âge et sans condition de stage. Elle est soumise, pour le surplus, aux conditions et aux règles de la naturalisation ". Enfin, aux termes de l'article 24-2 du même code : " Les personnes qui ont perdu la nationalité française à raison du mariage avec un étranger ou de l'acquisition par mesure individuelle d'une nationalité étrangère peuvent, sous réserve des dispositions de l'article 21-27, être réintégrées par déclaration souscrite, en France ou à l'étranger, conformément aux articles 26 et suivants. / Elles doivent avoir conservé ou acquis avec la France des liens manifestes, notamment d'ordre culturel, professionnel, économique ou familial ".<br/>
<br/>
              3. Les dispositions du code civil, qui régissent aujourd'hui l'acquisition et la perte de la nationalité française, n'organisant aucune procédure d'abrogation ni de retrait d'un décret autorisant la perte de la qualité de Français, il appartient à celui qui a été l'objet d'une telle décision, s'il souhaite recouvrer la nationalité française, de solliciter sa réintégration dans la nationalité française dans le cadre de l'une des deux procédures mentionnées au point 2. L'intéressé peut toutefois, eu égard aux effets d'une telle décision, demander à l'administration à tout moment de la retirer s'il s'avère qu'elle n'a pas été effectivement prise sur sa demande ou qu'elle est entachée d'un vice du consentement.<br/>
<br/>
              4. Il ressort des pièces du dossier que le décret du 3 août 1977, libérant M. B... de ses liens avec la France a été pris sur la demande formulée le 14 avril 1977 par l'intéressé, alors majeur, après qu'il eut acquis le 14 mai 1975 la nationalité suisse par naturalisation, et conformément à sa volonté d'être libéré de ses liens d'allégeance avec la France. La circonstance que la renonciation à sa nationalité française a constitué, en application de la loi suisse alors en vigueur, une condition de sa naturalisation, ne saurait, contrairement à ce qui est soutenu, caractériser un vice de consentement. Dans ces conditions, sa requête tendant à l'annulation de la décision implicite de rejet née du silence gardé par le ministre de l'intérieur sur la demande, qu'il lui a adressée le 10 avril 2019, de retrait de ce décret, pris sur sa demande, n'est pas recevable et doit être rejetée. Il s'ensuit que ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative doivent également être rejetées. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. D... B... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-09-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DISPARITION DE L'ACTE. RETRAIT. - DEMANDE DE RETRAIT D'UN DÉCRET LIBÉRANT UN CITOYEN FRANÇAIS DE SES LIENS D'ALLÉGEANCE AVEC LA FRANCE - CONDITIONS DE RECEVABILITÉ [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-01-01-015 DROITS CIVILS ET INDIVIDUELS. ÉTAT DES PERSONNES. NATIONALITÉ. PERTE DE LA NATIONALITÉ. - DÉCRET LIBÉRANT UN CITOYEN FRANÇAIS DE SES LIENS D'ALLÉGEANCE AVEC LA FRANCE - INTÉRESSÉ SOUHAITANT RECOUVRER LA NATIONALITÉ FRANÇAISE - 1) PRINCIPE - DEMANDE DE RÉINTÉGRATION DANS LA NATIONALITÉ FRANÇAISE (ART. 24-1 ET 24-2 DU CODE CIVIL) - 2) TEMPÉRAMENT - DEMANDE DE RETRAIT DU DÉCRET - CONDITIONS DE RECEVABILITÉ [RJ1].
</SCT>
<ANA ID="9A"> 01-09-01 Les dispositions du code civil, qui régissent aujourd'hui l'acquisition et la perte de la nationalité française, n'organisant aucune procédure d'abrogation ni de retrait d'un décret autorisant la perte de la qualité de Français, il appartient à celui qui a été l'objet d'une telle décision, s'il souhaite recouvrer la nationalité française, de solliciter sa réintégration dans la nationalité française dans le cadre de l'une des deux procédures prévues par les articles 24-1 et 24-2 du code civil.... ,,L'intéressé peut toutefois, eu égard aux effets d'une telle décision, demander à l'administration à tout moment de la retirer s'il s'avère qu'elle n'a pas été effectivement prise sur sa demande ou qu'elle est entachée d'un vice du consentement.</ANA>
<ANA ID="9B"> 26-01-01-015 1) Les dispositions du code civil, qui régissent aujourd'hui l'acquisition et la perte de la nationalité française, n'organisant aucune procédure d'abrogation ni de retrait d'un décret autorisant la perte de la qualité de Français, il appartient à celui qui a été l'objet d'une telle décision, s'il souhaite recouvrer la nationalité française, de solliciter sa réintégration dans la nationalité française dans le cadre de l'une des deux procédures prévues par les articles 24-1 et 24-2 du code civil.... ,,2) L'intéressé peut toutefois, eu égard aux effets d'une telle décision, demander à l'administration à tout moment de la retirer s'il s'avère qu'elle n'a pas été effectivement prise sur sa demande ou qu'elle est entachée d'un vice du consentement.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de la recevabilité d'un recours pour excès de pouvoir contre un tel décret, CE, 26 avril 2006, Marschalik, n°s 278730 281325, p. 205.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
