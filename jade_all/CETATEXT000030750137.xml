<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030750137</ID>
<ANCIEN_ID>JG_L_2015_06_000000353857</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/75/01/CETATEXT000030750137.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère SSJS, 17/06/2015, 353857, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-06-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353857</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Yannick Faure</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:353857.20150617</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une décision du 4 octobre 2013, le Conseil d'Etat, statuant au contentieux sur la requête de la société Les laboratoires Servier tendant à l'annulation pour excès de pouvoir de l'arrêté du ministre du travail, de l'emploi et de la santé et du ministre du budget, des comptes publics et de la réforme de l'Etat du 12 septembre 2011 en tant qu'il modifie les conditions d'inscription de la spécialité Protelos sur la liste des spécialités remboursables aux assurés sociaux, a sursis à statuer jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur la question de savoir si les dispositions du point 2 de l'article 6 de la directive 89/105/CEE du Conseil du 21 décembre 1988 concernant la transparence des mesures régissant la fixation des prix des médicaments à usage humain et leur inclusion dans le champ d'application des systèmes d'assurance-maladie imposent la motivation des décisions d'inscription ou de renouvellement d'inscription sur la liste des médicaments ouvrant droit au remboursement par les caisses d'assurance maladie qui, soit en restreignant par rapport à la demande présentée les indications thérapeutiques ouvrant droit au remboursement, soit en assortissant ce dernier de conditions tenant notamment à la qualification des prescripteurs, à l'organisation des soins ou au suivi des patients, ou de toute autre manière, n'ouvrent droit au remboursement par les caisses d'assurance maladie qu'à une partie des patients susceptibles de bénéficier du médicament ou seulement dans certaines circonstances.<br/>
<br/>
              Par un arrêt C-691/13 du 26 février 2015, la Cour de justice de l'Union européenne s'est prononcée sur cette question.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier, y compris celles visées par la décision du Conseil d'Etat du 4 octobre 2013 ;<br/>
<br/>
              Vu :<br/>
              - la directive 89/105/CEE du Conseil du 21 décembre 1988 ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yannick Faure, auditeur,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la société Les laboratoires Servier ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le point 2 de l'article 6 de la directive 89/105/CEE du Conseil du 21 décembre 1988 concernant la transparence des mesures régissant la fixation des prix des médicaments à usage humain et leur inclusion dans le champ d'application des systèmes d'assurance-maladie dispose que :  " Toute décision de ne pas inscrire un médicament sur la liste des produits couverts par le système d'assurance-maladie comporte un exposé des motifs fondé sur des critères objectifs et vérifiables (...) ".<br/>
<br/>
              2. Dans l'arrêt du 26 février 2015 par lequel elle s'est prononcée sur la question dont le Conseil d'Etat, statuant au contentieux, l'avait saisie à titre préjudiciel, après avoir écarté les autres moyens de la requête de la société Les laboratoires Servier, la Cour de justice de l'Union européenne a dit pour droit que le point 2 de l'article 6 de la directive citée ci-dessus du Conseil du 21 décembre 1988 doit être interprété en ce sens que l'obligation de motivation qu'il prévoit est applicable à une décision qui renouvelle l'inscription d'un produit sur la liste des médicaments couverts par le système d'assurance maladie, mais qui restreint le remboursement de ce produit à une certaine catégorie de patients.<br/>
<br/>
              3. L'arrêté attaqué du 12 septembre 2011, par lequel le ministre du travail, de l'emploi et de la santé et le ministre du budget, des comptes publics et de la réforme de l'Etat ont renouvelé l'inscription de la spécialité Protelos sur la liste des spécialités pharmaceutiques remboursables aux assurés sociaux mentionnée à l'article L. 162-17 du code la sécurité sociale, limite la prise en charge et le remboursement par l'assurance maladie de cette spécialité au traitement de l'ostéoporose post-ménopausique chez les patientes à risque élevé de fracture ayant une contre-indication ou une intolérance aux bisphosphonates ou n'ayant pas d'antécédent d'événement thrombo-embolique veineux ou d'autres facteurs de risque d'événement thrombo-embolique veineux, notamment un âge supérieur à 80 ans.<br/>
<br/>
              4. Le pouvoir réglementaire ne pouvait légalement, après l'expiration des délais impartis pour sa transposition, édicter des dispositions méconnaissant les objectifs définis par la directive du Conseil du 21 décembre 1988. Or il résulte de l'interprétation donnée par la Cour de justice de l'Union européenne que le point 2 de l'article 6 de cette directive imposait la motivation d'un arrêté tel que celui attaqué par la société requérante, qui renouvelle l'inscription de la spécialité Protelos sur la liste des médicaments remboursables aux assurés sociaux en restreignant son remboursement à une certaine catégorie de patients. <br/>
<br/>
              5. Ainsi que le Conseil d'Etat, statuant au contentieux, l'a relevé dans sa décision du 4 octobre 2013, les ministres, qui se sont bornés à viser dans l'arrêté litigieux l'avis rendu par la commission de la transparence de la Haute Autorité de santé le 11 mai 2011, ne peuvent être regardés, même si cet avis a été communiqué préalablement à la société fabriquant la spécialité, comme ayant satisfait à l'obligation de motivation prévue par la directive. Par suite, la société requérante est fondée à soutenir que cet arrêté est illégal pour ce motif. <br/>
<br/>
              6. Il suit de là que la société Les laboratoires Servier est fondée à demander l'annulation pour excès de pouvoir de l'arrêté du ministre du travail, de l'emploi et de la santé et du ministre du budget, des comptes publics et de la réforme de l'Etat du 12 septembre 2011 en tant qu'il a modifié les conditions d'inscription de la spécialité Protelos sur la liste des spécialités remboursables aux assurés sociaux. <br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à la société Les laboratoires Servier d'une somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêté du ministre du travail, de l'emploi et de la santé et du ministre du budget, des comptes publics et de la réforme de l'Etat du 12 septembre 2011 est annulé en tant qu'il modifie les conditions d'inscription de la spécialité Protelos sur la liste des spécialités remboursables aux assurés sociaux.<br/>
Article 2 : L'Etat versera à la société Les laboratoires Servier une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Les laboratoires Servier, au ministre des finances et des comptes publics et à la ministre des affaires sociales, de la santé et des droits des femmes.<br/>
Copie en sera adressée pour information à la Haute Autorité de santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
