<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043648193</ID>
<ANCIEN_ID>JG_L_2021_06_000000449279</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/64/81/CETATEXT000043648193.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 09/06/2021, 449279</TITRE>
<DATE_DEC>2021-06-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>449279</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:449279.20210609</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Commission nationale des comptes de campagne et des financements politiques (CNCCFP) a saisi le tribunal administratif de Strasbourg, en application de l'article L. 52-15 du code électoral, sur le fondement de sa décision du 1er octobre 2020 par laquelle elle a constaté l'absence de dépôt du compte de campagne de M. D... C.... <br/>
<br/>
              Par un jugement n° 2006362 du 30 décembre 2020, le tribunal administratif de Strasbourg a déclaré M. C... inéligible à toute élection pendant douze mois à compter de la date à laquelle ce jugement deviendrait définitif, annulé l'élection de M. C... en qualité de conseiller municipal de la commune de Creutzwald, proclamé élues Mme E... en qualité de conseillère municipale de la même commune et Mme B... A... en qualité de conseillère communautaire.<br/>
<br/>
              Par une requête, enregistrée le 1er février 2021 au secrétariat du contentieux du Conseil d'Etat, M. C... demande au Conseil d'Etat d'annuler ce jugement.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - la loi n° 2019-1269 du 2 décembre 2019 ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteure publique ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 2 juin 2021, présentée par M. C... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 118-3 du code électoral, dans sa rédaction antérieure à la loi du 2 décembre 2019 visant à clarifier diverses dispositions du droit électoral : " Saisi par la commission instituée par l'article L. 52-14, le juge de l'élection peut prononcer l'inéligibilité du candidat dont le compte de campagne, le cas échéant après réformation, fait apparaître un dépassement du plafond des dépenses électorales. (...) / Saisi dans les mêmes conditions, le juge de l'élection peut prononcer l'inéligibilité du candidat ou des membres du binôme de candidats qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12.  / Il prononce également l'inéligibilité du candidat ou des membres du binôme de candidats dont le compte de campagne a été rejeté à bon droit en cas de volonté de fraude ou de manquement d'une particulière gravité aux règles relatives au financement des campagnes électorales. ". Aux termes de ce même article dans sa rédaction issue de cette loi : " Lorsqu'il relève une volonté de fraude ou un manquement d'une particulière gravité aux règles de financement des campagnes électorales, le juge de l'élection, saisi par la Commission nationale des comptes de campagne et des financements politiques, peut déclarer inéligible : 1° Le candidat qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12 ; (...) ".<br/>
<br/>
              2. Aux termes du premier alinéa de l'article 15 de la loi du 2 décembre 2019 : " La présente loi, à l'exception de l'article 6, entre en vigueur le 30 juin 2020 ". Aux termes du XVI de l'article 19 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 : " A l'exception de son article 6, les dispositions de la loi n° 2019-1269 du 2 décembre 2019 visant à clarifier diverses dispositions du droit électoral ne sont pas applicables au second tour de scrutin régi par la présente loi ". Il résulte de ces dispositions que les dispositions de la loi du 2 décembre 2019 modifiant celles du code électoral, à l'exception de son article 6, ne sont pas applicables aux opérations électorales en vue de l'élection des conseillers municipaux et communautaires organisées les 15 mars et 28 juin 2020, y compris en ce qui concerne les comptes de campagne. <br/>
<br/>
              3. Toutefois, l'inéligibilité prévue par les dispositions de l'article L. 1183 du code électoral constitue une sanction ayant le caractère d'une punition. Il incombe, dès lors au juge de l'élection, lorsqu'il est saisi de conclusions tendant à ce qu'un candidat dont le compte de campagne est rejeté soit déclaré inéligible et à ce que son élection soit annulée, de faire application, le cas échéant, d'une loi nouvelle plus douce entrée en vigueur entre la date des faits litigieux et celle à laquelle il statue. Le législateur n'ayant pas entendu, par les dispositions citées au point 2, faire obstacle à ce principe, le juge doit faire application aux opérations électorales mentionnées à ce même point des dispositions de cet article dans sa rédaction issue de la loi du 2 décembre 2019. En effet, cette loi nouvelle laisse désormais au juge, de façon générale, une simple faculté de déclarer inéligible un candidat en la limitant aux cas où il relève une volonté de fraude ou un manquement d'une particulière gravité aux règles de financement des campagnes électorales, alors que l'article L. 118-3 dans sa version antérieure, d'une part, prévoyait le prononcé de plein droit d'une inéligibilité lorsque le compte de campagne avait été rejeté à bon droit en cas de volonté de fraude ou de manquement d'une particulière gravité et, d'autre part, n'imposait pas cette dernière condition pour que puisse être prononcée une inéligibilité lorsque le candidat n'avait pas déposé son compte de campagne dans les conditions et le délai prescrit par l'article L. 52-12 de ce même code.<br/>
<br/>
              4. Aux termes de l'article L. 52-12 du code électoral, dans sa rédaction antérieure à l'intervention de la loi du 2 décembre 2019 : " Chaque candidat ou candidat tête de liste soumis au plafonnement prévu à l'article L. 52-11 et qui a obtenu au moins 1 % des suffrages exprimés est tenu d'établir un compte de campagne retraçant, selon leur origine, l'ensemble des recettes perçues et, selon leur nature, l'ensemble des dépenses engagées ou effectuées en vue de l'élection, hors celles de la campagne officielle par lui-même ou pour son compte, au cours de la période mentionnée à l'article L. 52-4 (...). Le compte de campagne est présenté par un membre de l'ordre des experts-comptables et des comptables agréés (...) ". Aux termes de l'article L. 52-15 du même code : " La Commission nationale des comptes de campagne et des financements politiques approuve et, après procédure contradictoire, rejette ou réforme les comptes de campagne. (...) Lorsque la commission a constaté que le compte de campagne n'a pas été déposé dans le délai prescrit, si le compte a été rejeté (...), la commission saisit le juge de l'élection (...) ". En vertu du 4° du XII de l'article 19 de la loi du 23 mars 2020, la date limite pour déposer le compte de campagne a été fixée, par dérogation aux dispositions de l'article L. 52-12 du code électoral, au 10 juillet 2020 à 18 heures, pour les listes de candidats présentes au seul premier tour du 15 mars 2020.<br/>
<br/>
              5. En application des dispositions précitées de l'article L. 118-3 du code électoral, dans sa rédaction issue de la loi du 2 décembre 2019, en dehors des cas de fraude, le juge de l'élection ne peut prononcer l'inéligibilité d'un candidat sur le fondement de ces dispositions que s'il constate un manquement d'une particulière gravité aux règles de financement des campagnes électorales. Il lui incombe à cet effet de prendre en compte l'ensemble des circonstances de l'espèce et d'apprécier s'il s'agit d'un manquement caractérisé à une règle substantielle relative au financement des campagnes électorales et s'il présente un caractère délibéré. <br/>
<br/>
              6. En premier lieu, il résulte de l'instruction que la liste conduite par M. C... a obtenu 35,08% des suffrages exprimés à l'élection municipale de la commune de Creutzwald qui n'a pas connu de second tour. La Commission nationale des comptes de campagne et des financements politiques (CCNFP) a constaté, par une décision du 1er octobre 2020, l'absence de dépôt de compte de campagne par M. C... avant la date limite rappelée au point 4. Si M. C... soutient que son mandataire financier aurait adressé ce compte de campagne dans les délais et que la poste aurait égaré ce courrier, il n'apporte aucun élément permettant d'attester d'un tel envoi. Au demeurant, il résulte de l'instruction que le compte de campagne produit par M. C... est daté du 27 juillet 2020, soit postérieurement à l'expiration du délai précité. La circonstance que M. C... ait déposé son compte de campagne après la saisine du tribunal administratif par la CCNFP ne permet pas de le regarder comme ayant satisfait à l'obligation imposée par l'article L. 52-12 du code électoral. Par suite, c'est à bon droit, ainsi que l'ont estimé les premiers juges, que la CNCCFP a constaté cette irrégularité et saisi, sur le fondement de l'article L. 52-15 du code électoral, le tribunal administratif de Strasbourg. <br/>
<br/>
              7. En deuxième lieu, outre l'absence du dépôt de compte de campagne de M. C... auprès de la CCNFP, il résulte également de l'instruction que le document daté du 27 juillet 2020, bien que comportant le visa d'un membre de l'ordre des experts-comptables, n'est pas assorti des justificatifs des recettes et des dépenses, en méconnaissance de dispositions de l'article L. 52-12 du code électoral. Ce document fait également apparaître un solde différent de celui résultant des pièces produites par le mandataire financier de M. C... devant la CNCCFP le 31 août 2020. <br/>
<br/>
              8. Eu égard à ces manquements caractérisés à des règles substantielles relatives au financement des campagnes électorales, à leur particulière gravité et aux circonstances de l'espèce, M. C... n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Strasbourg a prononcé son inéligibilité pour une durée de douze mois et annulé son élection. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. C... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. D... C... et à la Commission nationale des comptes de campagne et des financements politiques. <br/>
Copie en sera adressée au préfet de la Moselle et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-005-04-02-04 ÉLECTIONS ET RÉFÉRENDUM. DISPOSITIONS GÉNÉRALES APPLICABLES AUX ÉLECTIONS. FINANCEMENT ET PLAFONNEMENT DES DÉPENSES ÉLECTORALES. COMPTE DE CAMPAGNE. DÉPENSES. - INÉLIGIBILITÉ PRÉVUE PAR L'ARTICLE L. 118-3 DU CODE ÉLECTORAL, DANS SA RÉDACTION ISSUE DE LA LOI DU 2 DÉCEMBRE 2019 - 1) MODALITÉS D'APPLICATION - 2) ESPÈCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">28-005-04-04 ÉLECTIONS ET RÉFÉRENDUM. DISPOSITIONS GÉNÉRALES APPLICABLES AUX ÉLECTIONS. FINANCEMENT ET PLAFONNEMENT DES DÉPENSES ÉLECTORALES. PORTÉE DE L'INÉLIGIBILITÉ. - INÉLIGIBILITÉ PRÉVUE PAR L'ARTICLE L. 118-3 DU CODE ÉLECTORAL, DANS SA RÉDACTION ISSUE DE LA LOI DU 2 DÉCEMBRE 2019 - 1) MODALITÉS D'APPLICATION - 2) ESPÈCE [RJ1].
</SCT>
<ANA ID="9A"> 28-005-04-02-04 1) En application de l'article L. 118-3 du code électoral, dans sa rédaction issue de la loi n° 2019-1269 du 2 décembre 2019, en dehors des cas de fraude, le juge de l'élection ne peut prononcer l'inéligibilité d'un candidat sur le fondement de ces dispositions que s'il constate un manquement d'une particulière gravité aux règles de financement des campagnes électorales.... ,,Il lui incombe à cet effet de prendre en compte l'ensemble des circonstances de l'espèce et d'apprécier s'il s'agit d'un manquement caractérisé à une règle substantielle relative au financement des campagnes électorales et s'il présente un caractère délibéré.... ,,2) Liste ayant obtenu 35,08% des suffrages exprimés à l'élection municipale de la commune de Creutzwald qui n'a pas connu de second tour.... ... ...D'une part, la commission nationale des comptes de campagne et des financements politiques (CNCCFP) a constaté l'absence de dépôt du compte de campagne par la tête de liste avant la date limite fixée par le 4° du XII de l'article 19 de la loi n° 2020-290 du 23 mars 2020. Si lL'intéressé soutient que son mandataire financier aurait adressé ce compte de campagne dans les délais et que la poste aurait égaré ce courrier, il n'apporte aucun élément permettant d'attester d'un tel envoi. La circonstance qu'il ait déposé son compte de campagne après la saisine du tribunal administratif par la CNCCFP ne permet pas de le regarder comme ayant satisfait à l'obligation imposée par l'article L.52-12 du code électoral. Par suite, c'est à bon droit que la CNCCFP a constaté cette irrégularité et saisi, sur le fondement de l'article L. 52-15 du code électoral, le tribunal administratif.,,,D'autre part, le compte de campagne produit, bien que comportant le visa d'un membre de l'ordre des experts-comptables, n'est pas assorti des justificatifs des recettes et des dépenses, en méconnaissance de l'article L. 52-12 du code électoral. Ce document fait également apparaître un solde différent de celui résultant des pièces produites par le mandataire financier devant le CNCCFP.,,,Eu égard à ces manquements caractérisés à des règles substantielles relatives au financement des campagnes électorales, à leur particulière gravité et aux circonstances de l'espèce, inéligibilité de la tête de liste pour une durée de douze mois et annulation de son élection.</ANA>
<ANA ID="9B"> 28-005-04-04 1) En application de l'article L. 118-3 du code électoral, dans sa rédaction issue de la loi n° 2019-1269 du 2 décembre 2019, en dehors des cas de fraude, le juge de l'élection ne peut prononcer l'inéligibilité d'un candidat sur le fondement de ces dispositions que s'il constate un manquement d'une particulière gravité aux règles de financement des campagnes électorales.... ,,Il lui incombe à cet effet de prendre en compte l'ensemble des circonstances de l'espèce et d'apprécier s'il s'agit d'un manquement caractérisé à une règle substantielle relative au financement des campagnes électorales et s'il présente un caractère délibéré.... ,,2) Liste ayant obtenu 35,08% des suffrages exprimés à l'élection municipale de la commune de Creutzwald qui n'a pas connu de second tour.... ... ...D'une part, la commission nationale des comptes de campagne et des financements politiques (CNCCFP) a constaté l'absence de dépôt du compte de campagne par la tête de liste avant la date limite fixée par le 4° du XII de l'article 19 de la loi n° 2020-290 du 23 mars 2020. Si lL'intéressé soutient que son mandataire financier aurait adressé ce compte de campagne dans les délais et que la poste aurait égaré ce courrier, il n'apporte aucun élément permettant d'attester d'un tel envoi. La circonstance qu'il ait déposé son compte de campagne après la saisine du tribunal administratif par la CNCCFP ne permet pas de le regarder comme ayant satisfait à l'obligation imposée par l'article L.52-12 du code électoral. Par suite, c'est à bon droit que la CNCCFP a constaté cette irrégularité et saisi, sur le fondement de l'article L. 52-15 du code électoral, le tribunal administratif.,,,D'autre part, le compte de campagne produit, bien que comportant le visa d'un membre de l'ordre des experts-comptables, n'est pas assorti des justificatifs des recettes et des dépenses, en méconnaissance de l'article L. 52-12 du code électoral. Ce document fait également apparaître un solde différent de celui résultant des pièces produites par le mandataire financier devant le CNCCFP.,,,Eu égard à ces manquements caractérisés à des règles substantielles relatives au financement des campagnes électorales, à leur particulière gravité et aux circonstances de l'espèce, inéligibilité de la tête de liste pour une durée de douze mois et annulation de son élection.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ2] Cf. sol. contr. CE, décision du même jour, Elections municipales et communautaires d'Apatou (Guyane), n°s 447336 449019, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
