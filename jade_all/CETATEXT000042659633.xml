<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042659633</ID>
<ANCIEN_ID>JG_L_2020_12_000000429798</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/65/96/CETATEXT000042659633.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 10/12/2020, 429798, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429798</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP LE BRET-DESACHE</AVOCATS>
<RAPPORTEUR>Mme Catherine Brouard-Gallet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:429798.20201210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif d'Orléans d'annuler pour excès de pouvoir la décision du 15 décembre 2015 par laquelle le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a, d'une part, retiré sa décision implicite née le 16 octobre 2015 rejetant le recours hiérarchique de l'association " Entraid' Ouvrière " et annulé la décision du 17 avril 2015 de l'inspecteur du travail de la 9ème section de l'unité territoriale d'Indre-et-Loire refusant à cette association l'autorisation de le licencier et, d'autre part, accordé cette autorisation. Par un jugement n° 1600257 du 13 juillet 2017, le tribunal administratif a annulé cette décision.<br/>
<br/>
              Par un arrêt n° 17NT02767 du 3 décembre 2018, la cour administrative d'appel de Nantes a, sur appel de l'association " Entraid'Ouvrière ", annulé ce jugement et rejeté la demande présentée par M. B... devant le tribunal administratif.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 15 avril et 15 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de l'association " Entraid'Ouvrière " ;<br/>
<br/>
              3°) de mettre à la charge de l'association " Entraid'Ouvrière " le versement à la SCP Piwnica, Molinié, son avocat, de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme C... D..., conseillère d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Piwnica, Molinié, avocat de M. B... et à la SCP Le Bret-Desaché, avocat de l'association " Entraid'Ouvrière ";<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que l'inspecteur du travail de la 9ème section de l'unité territoriale d'Indre-et-Loire a, par une décision du 17 avril 2015, refusé d'autoriser l'association " Entraid'Ouvrière " à licencier pour motif disciplinaire M. B..., salarié protégé. Sur recours hiérarchique de l'association " Entraid'Ouvrière ", le ministre chargé du travail, par décision du 15 décembre 2015, a retiré sa décision implicite née le 16 octobre 2015 rejetant ce recours hiérarchique, annulé la décision de l'inspecteur du travail et autorisé le licenciement de M. B.... Le tribunal administratif d'Orléans a, à la demande de celui-ci, annulé cette décision. M. B... se pourvoit en cassation contre l'arrêt du 3 décembre 2018 par lequel la cour administrative d'appel de Nantes a annulé le jugement et rejeté la demande qu'il avait formée devant le tribunal administratif. <br/>
<br/>
              2. En vertu des dispositions du code du travail, les salariés légalement investis de fonctions représentatives bénéficient, dans l'intérêt de l'ensemble des travailleurs qu'ils représentent, d'une protection exceptionnelle ; lorsque le licenciement d'un de ces salariés est envisagé, ce licenciement ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale de l'intéressé. Dans le cas où la demande de licenciement est motivée par un comportement fautif, il appartient à l'inspecteur du travail et, le cas échéant, au ministre, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si les faits reprochés au salarié sont d'une gravité suffisante pour justifier son licenciement compte tenu de l'ensemble des règles applicables au contrat de travail de l'intéressé et des exigences propres à l'exécution normale du mandat dont il est investi.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que la procédure de licenciement a été engagée par l'association " Entraid'Ouvrière " contre M. B..., salarié en son sein depuis 1989 et salarié protégé à la date de la demande d'autorisation de licenciement formée par cette association, à la suite d'un vol, reconnu par l'intéressé, au sein des ateliers techniques au cours du week-end des 13 et 14 février 2015. Il ressort également des pièces du dossier soumis aux juges du fond qu'une ordonnance pénale en date du 16 juin 2015 a condamné M. B... à une peine d'un mois d'emprisonnement avec sursis et au paiement à son employeur de la somme de 150 euros au titre de la réparation de son préjudice moral, aucun autre chef de préjudice n'ayant été retenu au motif que les objets dérobés avaient été restitués rapidement et sans dégradation. <br/>
<br/>
              4. Selon les termes mêmes de l'arrêt attaqué, pour juger que M. B... avait commis une faute d'une gravité suffisante de nature à justifier son licenciement, la cour administrative d'appel de Nantes s'est fondée exclusivement sur la circonstance qu'une ordonnance pénale avait prononcé à son encontre une peine d'emprisonnement d'un mois avec sursis pour des faits de vol au préjudice de son employeur, sans vérifier si, ainsi que le faisait valoir M. B... dans ses mémoires en défense concluant au rejet de l'appel formé par l'association "Entraid'Ouvrière" contre le jugement du tribunal administratif d'Orléans, les circonstances dans lesquelles était survenue la soustraction frauduleuse, l'absence de préjudice matériel de l'association, son état de santé ainsi que son ancienneté au sein de cette association étaient de nature à avoir des conséquences sur la qualification de la faute reprochée. En statuant ainsi, la cour administrative d'appel a entaché son arrêt d'une erreur de droit.<br/>
<br/>
              5. Il résulte de tout ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que M. B... est fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Piwnica, Molinié, avocat de M. B..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'association " Entraid'Ouvrière " la somme de 3 000 euros à verser à cette société au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 3 décembre 2018 est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
<br/>
Article 3 : L'association " Entraid'Ouvrière " versera à la SCP Piwnica, Molinié, avocat de M. B..., la somme de 3 000 euros au titre de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative, sous réserve qu'elle renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à M. A... B... et à l'association " Entraid'Ouvrière ".<br/>
Copie en sera adressée à la ministre du travail, de l'emploi et de l'insertion. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
