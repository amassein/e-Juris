<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036927129</ID>
<ANCIEN_ID>JG_L_2018_05_000000408288</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/92/71/CETATEXT000036927129.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 04/05/2018, 408288, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-05-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408288</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:408288.20180504</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° Sous le n° 408288, par une requête et un mémoire, enregistrés les 22 février et 22 mars 2017 au secrétariat du contentieux du Conseil d'Etat, le Syndicat des casinos modernes de France (SCMF) et le syndicat " Casinos de France " demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir les lignes directrices conjointes entre le service central des courses et jeux et la cellule de renseignement financier nationale, Tracfin, sur les obligations relatives à la lutte contre le blanchiment des capitaux et le financement du terrorisme auxquelles sont soumis les représentants légaux et directeurs responsables des opérateurs de jeux ou de paris autorisés sur le fondement des articles L. 321-1 et L. 321-3 du code de la sécurité intérieure ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 414774, par une ordonnance n° 1612047 du 15 septembre 2017, enregistrée le 29 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de Cergy-Pontoise a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête du Syndicat des casinos modernes de France (SCMF), du syndicat " Casinos de France " et de l'Association des casinos indépendants de France (ACIF). <br/>
<br/>
              Par cette requête, enregistrée le 22 décembre 2016, le SCMF et autres demandent l'annulation pour excès de pouvoir des mêmes lignes directrices et que soit mise à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code monétaire et financier ;<br/>
              - le code de la sécurité intérieure ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat du Syndicat des casinos modernes de France  et autres.<br/>
<br/>
<br/>
<br/>
<br/>1.	Considérant que par deux requêtes, l'une, n° 408288, présentée devant le Conseil d'Etat, l'autre, n° 414774, présentée devant le tribunal administratif de Cergy-Pontoise, le Syndicat des casinos modernes de France, le syndicat " Casinos de France " et l'Association des casinos indépendants de France demandent l'annulation pour excès de pouvoir des " lignes directrices " conjointes entre le service central des courses et jeux (SCPJ) et la cellule de renseignement financier nationale (Tracfin) sur les obligations relatives à la lutte contre le blanchiment des capitaux et le financement du terrorisme, auxquelles sont soumis les représentants légaux et directeurs responsables des opérateurs de jeux ou de paris autorisés sur le fondement des articles L. 321-1 et L. 321-3 du code de la sécurité intérieure ; que ces deux requêtes présentent à juger des questions identiques ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
<br/>
<br/>
              Sur le cadre juridique applicable :<br/>
<br/>
              2.	Considérant que l'article L. 561-2 du code monétaire et financier, qui figure au chapitre Ier du titre VI du livre V de ce code, relatif aux obligations relatives à la lutte contre le blanchiment des capitaux et le financement du terrorisme, dispose notamment, dans sa rédaction applicable en l'espèce, que : " Sont assujettis aux obligations prévues par les dispositions des sections 2 à 7 du présent chapitre : (...) / 9° Les représentants légaux et directeurs responsables des opérateurs de jeux ou de paris autorisés sur le fondement de l'article 5 de la loi du 2 juin 1891 ayant pour objet de réglementer l'autorisation et le fonctionnement des courses de chevaux, des articles L. 321-1 et L. 321-3 du code de la sécurité intérieure, sous réserve si nécessaire de l'application du troisième alinéa du II du même article L. 321-3, de l'article 47 de la loi du 30 juin 1923 portant fixation du budget général de l'exercice 1923, de l'article 9 de la loi du 28 décembre 1931, de l'article 136 de la loi du 31 mai 1933 portant fixation du budget général de l'exercice 1933 et de l'article 42 de la loi de finances pour 1985 (n° 84-1208 du 29 décembre 1984) ; / (...) " ; que l'article L. 561-32 du même code prévoit que : " Les personnes mentionnées à l'article L. 561-2 mettent en place des systèmes d'évaluation et de gestion des risques de blanchiment des capitaux et de financement du terrorisme. / Les conditions d'application du présent article sont définies par décret en Conseil d'Etat (...) " ; que l'article L. 561-33 du même code dispose que : " Les personnes mentionnées à l'article L. 561-2 assurent la formation et l'information régulière de leurs personnels en vue du respect des obligations prévues aux chapitres Ier et II du présent titre " ;<br/>
<br/>
              3.	Considérant, par ailleurs, qu'en vertu du III de l'article R. 561-38 du code monétaire et financier, pris pour l'application de ces dispositions, les personnes mentionnées au 9° de l'article L. 561-2 du même code " mettent en oeuvre les procédures et les mesures de contrôle interne en matière de lutte contre le blanchiment des capitaux et le financement du terrorisme définies par leurs autorités de contrôle " ; <br/>
<br/>
              Sur les conclusions aux fins d'annulation pour excès de pouvoir :<br/>
<br/>
              4.	Considérant qu'il ressort des pièces des dossiers que les " lignes directrices " litigieuses se présentent comme prises sur le fondement des dispositions du III de l'article R. 561-38 du code monétaire et financier précitées ; que, toutefois, bien qu'ayant pour objet de préciser les conditions de mise en place d'un système d'évaluation et de gestion des risques de blanchiment des capitaux et de financement du terrorisme par les opérateurs de jeux ou de paris conformément à l'article L. 561-32 du code monétaire et financier, elles portent plus largement sur les modalités de mise en oeuvre de l'ensemble des obligations de lutte contre le blanchiment des capitaux et le financement du terrorisme auxquelles ces professionnels sont soumis, en particulier les obligations de vigilance et de déclaration de soupçon, et définissent le cadre d'intervention du service central des courses et jeux à l'occasion des contrôles qu'il est conduit à effectuer les concernant ; <br/>
<br/>
              En ce qui concerne les dispositions des " lignes directrices " litigieuses relatives à la mise en place de systèmes d'évaluation et de gestion des risques de blanchiment des capitaux et de financement du terrorisme :<br/>
<br/>
              5.	Considérant que si l'article L. 561-32 du code monétaire et financier renvoie à un décret en Conseil d'Etat le soin de préciser les conditions dans lesquelles les personnes mentionnées à l'article L. 561-2 du même code mettent en place des systèmes d'évaluation et de gestion des risques de blanchiment des capitaux et de financement du terrorisme, le III de l'article R. 561-38 du même code précité, pris pour l'application de ces dispositions, se borne à prévoir, s'agissant notamment des opérateurs de jeux ou de paris, que ces derniers mettent en oeuvre les procédures et les mesures de contrôle interne " définies par leurs autorités de contrôle " ;  <br/>
<br/>
              6.	Considérant que s'il appartenait éventuellement au Gouvernement, après avoir défini avec une précision suffisante dans le décret en Conseil d'Etat les principes devant guider la mise en place des systèmes d'évaluation et de gestion des risques par les professions assujetties à cette obligation, de confier à l'autorité de contrôle compétente pour chacune des professions concernées le soin de préciser, en fonction des spécificités de ces professions, les modalités concrètes d'application de ces principes, il ne pouvait légalement se décharger de cette mission que lui avait confiée l'article L. 561-32 du code monétaire et financier en se bornant à renvoyer aux procédures et mesures de contrôles internes définies par les autorités de contrôle compétentes sans autre précision ; qu'il suit de là que les dispositions du III de l'article R. 561-38 du code monétaire et financier sont entachées d'illégalité ;<br/>
<br/>
              7.	Considérant qu'il résulte de ce qui précède que les dispositions du III de l'article R. 561-38 du code monétaire et financier sont insusceptibles de fonder la compétence du ministre de l'intérieur ou du ministre de l'économie ni, en tout état de cause, du chef du service central des courses et des jeux ou du directeur de Tracfin agissant par délégation respective de ces ministres ou en vertu de leur compétence propre, pour édicter, par les " lignes directrices " litigieuses, des règles nouvelles impératives, complémentaires à celle posées par le code monétaire et financier, en matière de mise en place de systèmes d'évaluation et de gestion des risques de blanchiment des capitaux et de financement du terrorisme ; <br/>
<br/>
              8.	Considérant, par ailleurs, que ni le ministre de l'intérieur, ni le ministre de l'économie, ni, en tout état de cause, le chef du service central des courses et des jeux ou le directeur de Tracfin, ne tenait d'une autre disposition législative ou réglementaire, le pouvoir d'édicter de telles prescriptions impératives ; que, par suite, les " lignes directrices " litigieuses sont entachées d'incompétence dans cette mesure ; <br/>
<br/>
              En ce qui concerne les autres dispositions des " lignes directrices " litigieuses :<br/>
<br/>
              9.	Considérant que, ainsi qu'il a été dit au point 4, les " lignes directrices " attaquées ne se limitent pas à préciser les procédures et les mesures de contrôle interne qui doivent être mises en place par les professionnels concernés, mais fixent également des règles complémentaires destinées à permettre la vérification des autres obligations qui sont imposées à ces derniers par les dispositions des articles L. 561-1 et suivants du code monétaire et financier en matière de lutte contre le blanchiment des capitaux et de financement du terrorisme, en particulier les obligations de vigilance, de déclaration de soupçon et de formation de leur personnel ; que, cependant, ni le ministre de l'intérieur, ni le ministre de l'économie, ni, en tout état de cause, le chef du service central des courses et des jeux ou le directeur de Tracfin agissant par délégation respective de ces ministres ou en vertu de leur compétence propre, ne tenait d'une disposition législative ou réglementaire le pouvoir de fixer de telles prescriptions complémentaires en ces matières ; que, par suite, les " lignes directrices " litigieuses sont également entachées d'incompétence dans cette mesure ;<br/>
<br/>
              10.	Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens des requêtes, que le syndicat des casinos modernes de France et autres sont fondés à demander l'annulation des " lignes directrices " qu'ils attaquent ; <br/>
<br/>
<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              11.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser conjointement au Syndicat des casinos modernes de France, au syndicat " Casinos de France " et à l'Association des casinos indépendants de France, au titre des dispositions de l'article L. 761-1 ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les lignes directrices conjointes entre le service central des courses et jeux et Tracfin sur les obligations relatives à la lutte contre le blanchiment des capitaux et le financement du terrorisme auxquelles sont soumis les représentants légaux et directeurs responsables des opérateurs de jeux autorisés sur le fondement des articles L. 321-1 et L. 321 3 du code de la sécurité intérieure sont annulées.<br/>
<br/>
Article 2 : L'Etat versera conjointement au Syndicat des casinos modernes de France, au syndicat " Casinos de France " et à l'Association des casinos indépendants de France une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée au syndicat " Casinos de France ", représentant désigné, pour l'ensemble des requérants, au ministre d'Etat, ministre de l'intérieur, et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
