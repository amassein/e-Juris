<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043305773</ID>
<ANCIEN_ID>JG_L_2021_03_000000450208</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/30/57/CETATEXT000043305773.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 19/03/2021, 450208, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450208</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:450208.20210319</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C... A..., agissant au nom de son enfant mineur Mlle B... A..., a demandé au juge des référés du tribunal administratif de Nice, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre à l'Office français de l'immigration et de l'intégration (OFII) de lui accorder le bénéfice des conditions matérielles d'accueil, sous astreinte de 200 euros par jour de retard à l'expiration d'un délai de huit jours à compter de la notification de l'ordonnance. Par une ordonnance n° 2100336 du 29 janvier 2021, le juge des référés du tribunal administratif de Nice a rejeté sa requête.<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 26 février et le 15 mars 2021 au secrétariat du contentieux du Conseil d'Etat, Mme A..., agissant au nom de son enfant mineur Mme B... A..., demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler l'ordonnance du 29 janvier 2021 du tribunal administratif de Nice ;<br/>
<br/>
              2°) d'enjoindre au directeur de l'OFII de rétablir les conditions matérielles d'accueil à sa fille mineure, Mme B... A..., et de lui verser une allocation de demandeur d'asile dans un délai de 8 jours à compter de la notification de l'ordonnance sous astreinte de 200 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'OFII la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - la condition d'urgence est satisfaite eu égard à la situation de détresse dans laquelle la place l'absence de versement depuis le mois de septembre 2020 de l'allocation pour demandeur d'asile, alors qu'elle se trouve isolée et sans ressource avec son enfant de quatre ans ;<br/>
              - la privation du bénéfice des conditions matérielles d'accueil dans de telles conditions porte une atteinte grave et manifestement illégale au droit d'asile de la jeune B... A..., née le 12 juin 2016, dont la demande d'asile, enregistrée le 10 janvier 2019, est en cours d'examen, la circonstance que la demande d'asile de sa mère ait été rejetée par une décision définitive de la Cour nationale du droit d'asile du 28 septembre 2020 restant sans incidence sur le droit de l'enfant à se voir proposer les conditions matérielles d'accueil, qui doivent se traduire par la prise en charge tant de l'enfant que de ses parents ;<br/>
              - le juge des référés du tribunal administratif a commis une erreur de droit en jugeant que Mme B... A... n'avait plus la qualité de demandeur d'asile à la date de son ordonnance ; <br/>
              - aucune impossibilité matérielle ne s'oppose au versement à Mlle A... de l'allocation pour demandeur d'asile. <br/>
<br/>
              Par un mémoire en défense, enregistrés le 11 mars 2021, l'OFII conclut au rejet de la requête. Il soutient qu'il n'est pas porté d'atteinte grave et manifestement illégale aux libertés fondamentales invoquées.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive n° 2013/33/UE du Parlement européen et du Conseil 26 juin 2013 établissant des normes pour l'accueil des personnes demandant la protection internationale ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Les parties ont été informées, sur le fondement de l'article 3 de l'ordonnance n° 2020-1402 du 18 novembre 2020 portant adaptation des règles applicables aux juridictions administratives, de ce qu'aucune audience ne se tiendrait et de ce que la clôture de l'instruction serait fixée le 15 mars 2021 à 18 heures.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". <br/>
<br/>
              2. Il résulte de l'instruction que Mme C... A..., ressortissante nigériane, a formé le 22 juin 2016 une demande d'asile, qui a été rejetée par une décision du directeur général de l'Office français de protection des réfugiés et apatrides du 11 septembre 2019. Le 10 janvier 2019, elle a également présenté pour son enfant Mlle B... A..., née en France le 12 juin 2016, une demande d'asile, dont il ne résulte pas de l'instruction qu'elle ait fait l'objet d'une décision. Le recours de Mme A... contre la décision de l'OFPRA la concernant a en revanche été rejeté par une décision de la Cour nationale du droit d'asile du 28 septembre 2020. Le 28 octobre 2020, l'autorité compétente de l'Office français de l'immigration et de l'intégration (OFII), se fondant sur cette décision définitive défavorable, lui a notifié une décision de sortie du lieu d'hébergement où elle avait été admise le 7 mai 2018, en lui précisant que cet hébergement pourrait être prolongé à sa demande et à titre excpetionnel pour une durée d'un mois. Elle a saisi le juge des référés du tribunal administratif de Nice, au nom de son enfant Mlle B... A..., d'une demande tendant à ce qu'il soit enjoint à l'OFII de lui accorder le bénéfice des conditions matérielles d'accueil pour son enfant à compter du mois d'octobre 2020. Elle relève appel de l'ordonnance par laquelle le juge des référés a rejeté cette demande.<br/>
<br/>
              3. D'une part, aux termes de l'article L. 744-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Les conditions matérielles d'accueil du demandeur d'asile, au sens de la directive 2013/33/UE du Parlement européen et du Conseil, du 26 juin 2013, établissant des normes pour l'accueil des personnes demandant la protection internationale, sont proposées à chaque demandeur d'asile par l'Office français de l'immigration et de l'intégration après l'enregistrement de la demande d'asile par l'autorité administrative compétente, en application du présent chapitre. Les conditions matérielles d'accueil comprennent les prestations et l'allocation prévues au présent chapitre. / (...) ". L'article L. 744-8 du même code prévoit, par ailleurs, que le bénéfice des conditions matérielles d'accueil peut être refusé, notamment, " si le demandeur présente une demande de réexamen de sa demande d'asile (...) ". Il résulte toutefois du point 5 de l'article 20 de la directive 2013/33/UE du Parlement européen et du Conseil du 26 juin 2013 établissant des normes pour l'accueil des personnes demandant la protection internationale qu'un tel refus ne peut être pris qu'au terme d'un examen au cas par cas, fondé sur la situation particulière de la personne concernée, en particulier dans le cas des personnes vulnérables mentionnées à l'article 21 de cette directive, lequel vise notamment les mineurs.<br/>
<br/>
              4. D'autre part, aux termes de l'article L. 741-1 du même code : " Lorsque la demande d'asile est présentée par un étranger qui se trouve en France accompagné de ses enfants mineurs, la demande est regardée comme présentée en son nom et en celui de ses enfants. Lorsqu'il est statué sur la demande de chacun des parents, la décision accordant la protection la plus étendue est réputée prise également au bénéfice des enfants (...) / L'étranger est tenu de coopérer avec l'autorité administrative compétente en vue d'établir son identité, sa ou ses nationalités, sa situation familiale, son parcours depuis son pays d'origine ainsi que, le cas échéant, ses demandes d'asile antérieures. Il présente tous documents d'identité ou de voyage dont il dispose (...) ". Enfin, aux termes de l'article L. 723-15 du même code : " Constitue une demande de réexamen une demande d'asile présentée après qu'une décision définitive a été prise sur une demande antérieure (...) ".<br/>
<br/>
              5. Il résulte de ces dispositions qu'il appartient à l'étranger présent sur le territoire français et souhaitant demander l'asile de présenter une demande en son nom et, le cas échéant, en celui de ses enfants mineurs qui l'accompagnent. En cas de naissance ou d'entrée en France d'un enfant mineur postérieurement à l'enregistrement de sa demande, l'étranger est tenu, tant que l'Office français de protection des réfugiés et apatrides ou, en cas de recours, la Cour nationale du droit d'asile, ne s'est pas prononcé, d'en informer cette autorité administrative ou cette juridiction. La décision rendue par l'office ou, en cas de recours, par la Cour nationale du droit d'asile, est réputée l'être à l'égard du demandeur et de ses enfants mineurs, sauf dans le cas où le mineur établit que la personne qui a présenté la demande n'était pas en droit de le faire. Une demande d'asile présentée au nom d'un enfant mineur par ses parents postérieurement au rejet définitif de leur propre demande doit, dans tous les cas, être regardée comme une demande de réexamen au sens de l'article L. 723-15 du code de l'entrée et du séjour des étrangers et du droit d'asile. La demande ainsi présentée au nom du mineur présentant le caractère d'une demande de réexamen, le bénéfice des conditions matérielles d'accueil peut être refusé à la famille, conformément aux dispositions de l'article L. 744-8, sous réserve d'un examen au cas par cas tenant notamment compte de la présence au sein de la famille du mineur concerné. Lorsque l'Office français de l'immigration et de l'intégration décide de proposer à la famille les conditions matérielles d'accueil et que les parents les acceptent, il est tenu, jusqu'à ce qu'il ait été statué sur cette demande, d'héberger la famille et de verser aux parents l'allocation pour demandeur d'asile, le montant de cette dernière étant calculé, en application des dispositions des articles L. 744-9 et D. 744-26 du code de l'entrée et du séjour des étrangers et du droit d'asile précité, en fonction du nombre de personnes composant le foyer du demandeur d'asile.<br/>
<br/>
              Sur l'urgence : <br/>
<br/>
              6. Il résulte de l'instruction que Mme A..., qui ne perçoit plus l'allocation pour demandeur d'asile depuis septembre 2020, ne dispose d'aucune ressource. Mère isolée, elle ne fait état d'aucune autre famille en France. Ayant reçu notification d'une décision lui imposant de quitter le lieu d'hébergement où elle a vécu avec son enfant depuis mai 2018, elle est également dépourvue de toute solution d'hébergement pour elle et son enfant. Dans ces conditions, elle est fondée à soutenir que la condition d'urgence prévue à l'article L. 521 2 du code de justice administrative est remplie.<br/>
<br/>
              Sur l'atteinte grave et manifestement illégale à une liberté fondamentale :<br/>
<br/>
              7. La privation des conditions matérielles d'accueil qui doivent être assurées au demandeur d'asile jusqu'à ce qu'il soit définitivement statué sur sa demande d'asile peut conduire le juge des référés, lorsque la situation qui en résulte caractérise une méconnaissance manifeste des exigences qui découlent du droit d'asile et emporte des conséquences graves pour le demandeur d'asile, compte tenu notamment de son âge, de son état de santé ou de sa situation de famille, à faire usage des pouvoirs qu'il tient de l'article L. 521-2 du code de justice administrative en ordonnant à l'administration de prendre, compte tenu des moyens dont elle dispose et des mesures qu'elle a déjà prises, les mesures qui lui apparaissent de nature à sauvegarder, dans un délai de quarante-huit heures, la liberté fondamentale à laquelle il est ainsi porté une atteinte grave et manifestement illégale.<br/>
<br/>
              8. La demande d'asile présentée par Mme A... au nom de son enfant mineur Mlle B... A... devant être regardée comme une demande de réexamen, le bénéfice des conditions matérielles d'accueil qu'elle demandait au nom de son enfant pouvait lui être refusé sous la réserve d'un examen au cas par cas tenant notamment compte de la présence au sein de la famille du mineur concerné afin en particulier de tenir compte d'une éventuelle situation de vulnérabilité. Or, la situation de Mlle B... A..., enfant de quatre ans accompagnée de sa mère, elle-même dépourvue de toute autre attache familiale, de toute ressource ainsi que de solution d'hébergement et à qui été notifiée une décision lui imposant de quitter la structure d'hébergement qui les abrite, caractérise manifestement une situation de vulnérabilité au sens du deuxième alinéa de l'article L. 744 6 du même code. Dans ces conditions, la privation des conditions matérielles d'accueil est de nature à porter une atteinte grave et manifestement illégale aux exigences qui découlent du droit d'asile, qui justifie qu'il soit enjoint à l'OFII de rétablir, pour l'avenir, le bénéfice des conditions matérielles d'accueil, attribuées, ainsi qu'il résulte des règles rappelées ci-dessus, à l'enfant et à sa mère, y compris l'allocation pour demandeur d'asile calculée en fonction de la composition de la famille, dans un délai de huit jours à compter de la présente ordonnance. Il n'y a pas lieu, en revanche, de prononcer d'astreinte. <br/>
<br/>
              9. Il résulte de tout ce qui précède que Mme A... est fondée à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Nice a rejeté sa demande tendant à ce que l'OFII lui rétablisse le bénéfice des conditions matérielles d'accueil.<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'OFII la somme de 2 000 euros au titre des dispositions de l'article L. 761 1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : L'ordonnance n° 2100336 du 29 janvier 2021 du juge des référés du tribunal administratif de Nice est annulée.<br/>
Article 2 : Il est enjoint à l'Office français de l'immigration et de l'intégration de rétablir à Mme A... pour elle et pour sa fille le bénéfice des conditions matérielles d'accueil, y compris l'allocation pour demandeur d'asile calculée en fonction de la composition de la famille dans un délai de huit jours à compter de la présente ordonnance. <br/>
Article 3 : L'Office français de l'immigration et de l'intégration versera la somme de 2 000 euros à Mme A..., en application de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 5 : La présente ordonnance sera notifiée à Mme C... A... et à l'Office français de l'immigration et de l'intégration.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
