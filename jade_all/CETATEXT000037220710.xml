<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037220710</ID>
<ANCIEN_ID>JG_L_2018_07_000000412217</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/22/07/CETATEXT000037220710.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 18/07/2018, 412217</TITRE>
<DATE_DEC>2018-07-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412217</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY ; SCP THOUIN-PALAT, BOUCARD ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Marie Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:412217.20180718</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 6 juillet 2017, 19 septembre 2017 et 16 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, la Fédération nationale des syndicats du spectacle, du cinéma, de l'audiovisuel et de l'action culturelle CGT (GCT Spectacle), le Syndicat des professionnels des industries de l'audiovisuel et du cinéma CGT (SPIAC CGT) et le Syndicat national de radiodiffusion et de télévision (SNRT CGT audiovisuel) demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 4 mai 2017 du ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social portant agrément de la convention du 14 avril 2017 relative à l'assurance chômage et de ses textes associés ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code du travail ;<br/>
              - la loi n° 2015-994 du 17 août 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Sirinelli, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la Fédération nationale des syndicats du spectacle, du cinéma, de l'audiovisuel et de l'action culturelle CGT, à la SCP Thouvenin, Coudray, Grevy, avocat de la Confédération française démocratique du travail et à la SCP Thouin-Palat, Boucard, avocat de l'Union des entreprises de proximité.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Par un arrêté du 4 mai 2017, le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a agréé la convention du 14 avril 2017 relative à l'assurance chômage et ses textes associés. La Fédération nationale des syndicats du spectacle, du cinéma, de l'audiovisuel et de l'action culturelle CGT, le Syndicat des professionnels des industries de l'audiovisuel et du cinéma CGT et le Syndicat national de radiodiffusion et de télévision doivent être regardés comme demandant l'annulation pour excès de pouvoir de cet arrêté en tant qu'il agrée les stipulations, divisibles des autres stipulations agréées par le même arrêté, de l'annexe VIII au règlement général annexé à la convention qui exigent la certification sociale de certains employeurs des ouvriers et techniciens de la prestation technique au service de la création et de l'événement, afin que ces derniers puissent bénéficier des règles fixées par cette annexe pour les ouvriers et techniciens intermittents du spectacle.<br/>
<br/>
              Sur la fin de non-recevoir opposée par le ministre du travail :<br/>
<br/>
              2. En l'absence, dans les statuts d'une association ou d'un syndicat, de stipulation réservant expressément à un autre organe la capacité de décider de former une action devant le juge administratif, celle-ci est régulièrement introduite par l'organe tenant des mêmes statuts le pouvoir de représenter cette association ou ce syndicat en justice. Il appartient à la juridiction administrative saisie, qui en a toujours la faculté, de s'assurer, le cas échéant et notamment lorsque cette qualité est contestée sérieusement par l'autre partie ou qu'au premier examen, l'absence de qualité du représentant de la personne morale semble ressortir des pièces du dossier, que le représentant de cette personne morale justifie de sa qualité pour agir au nom de cette partie.<br/>
<br/>
              3. En l'espèce, il ressort des pièces produites par la Fédération nationale des syndicats du spectacle, du cinéma, de l'audiovisuel et de l'action culturelle CGT que sa requête a été régulièrement introduite par son secrétaire général qui, aux termes de l'article 26 de ses statuts, représente cette fédération en justice. En conséquence, sans même qu'il soit besoin d'examiner la qualité pour agir du représentant des deux autres syndicats, le ministre du travail n'est pas fondé à soutenir que la requête n'aurait pas été régulièrement introduite, faute de qualité pour agir des représentants des organisations requérantes.<br/>
<br/>
              Sur le cadre juridique du litige : <br/>
<br/>
              4. Tout d'abord, aux termes de l'article L. 5422-1 du code du travail, dans sa rédaction applicable à l'acte attaqué : " Ont droit à l'allocation d'assurance les travailleurs involontairement privés d'emploi ou dont le contrat de travail a été rompu conventionnellement selon les modalités prévues aux articles L. 1237-11 et suivants (...), aptes au travail et recherchant un emploi qui satisfont à des conditions d'âge et d'activité antérieure ". Aux termes du premier alinéa de l'article L. 5422-2 du même code : " L'allocation d'assurance est accordée pour des durées limitées qui tiennent compte de l'âge des intéressés et de leurs conditions d'activité professionnelle antérieure. Ces durées ne peuvent être inférieures aux durées déterminées par décret en Conseil d'Etat ". Aux termes de l'article L. 5422-3 de ce code : " L'allocation d'assurance est calculée soit en fonction de la rémunération antérieurement perçue dans la limite d'un plafond, soit en fonction de la rémunération ayant servi au calcul des contributions mentionnées aux articles L. 5422-9 et L. 5422-11. / Elle ne peut excéder le montant net de la rémunération antérieurement perçue. / Elle peut comporter un taux dégressif en fonction de l'âge des intéressés et de la durée de l'indemnisation ".<br/>
<br/>
              5. Ensuite, en vertu de l'article L. 5422-20 du code du travail, les mesures d'application de ces dispositions " font l'objet d'accords conclus entre les organisations représentatives d'employeurs et de salariés ". Aux termes de l'article L. 5424-22 inséré dans le code du travail par la loi du 17 août 2015 relative au dialogue social et à l'emploi : " I.- Pour tenir compte des modalités particulières d'exercice des professions de la production cinématographique, de l'audiovisuel ou du spectacle, les accords relatifs au régime d'assurance chômage mentionnés à l'article L. 5422-20 comportent des règles spécifiques d'indemnisation des artistes et des techniciens intermittents du spectacle, annexées au règlement général annexé à la convention relative à l'indemnisation du chômage. / II.- Les organisations d'employeurs et de salariés représentatives de l'ensemble des professions [de la production cinématographique, de l'audiovisuel ou du spectacle] négocient entre elles les règles spécifiques définies au I du présent article. A cette fin, dans le cadre de la négociation des accords relatifs au régime d'assurance chômage mentionnés à l'article L. 5422-20, les organisations professionnelles d'employeurs et les organisations syndicales de salariés représentatives au niveau national et interprofessionnel leur transmettent en temps utile un document de cadrage. / Ce document précise les objectifs de la négociation en ce qui concerne la trajectoire financière et le respect de principes généraux applicables à l'ensemble du régime d'assurance chômage. Il fixe le délai dans lequel cette négociation doit aboutir. / Les règles spécifiques prévues par un accord respectant les objectifs définis par le document de cadrage et conclu dans le délai fixé par le même document sont reprises dans les accords relatifs au régime d'assurance chômage mentionnés à l'article L. 5422-20. A défaut de conclusion d'un tel accord, les organisations professionnelles d'employeurs et les organisations syndicales de salariés représentatives au niveau national et interprofessionnel fixent les règles d'indemnisation du chômage applicables aux artistes et aux techniciens intermittents du spectacle ". <br/>
<br/>
              6. Enfin, en vertu de l'article L. 5422-21 du code du travail, l'agrément des accords relatifs à l'assurance chômage par l'autorité administrative les rend obligatoires pour tous les employeurs et salariés compris dans leur champ d'application professionnel et territorial. L'article L. 5422-22 du même code précise, en outre, que : " Pour pouvoir être agréés, les accords ayant pour objet exclusif le versement d'allocations spéciales aux travailleurs sans emploi et, éventuellement, aux travailleurs partiellement privés d'emploi doivent avoir été négociés et conclus sur le plan national et interprofessionnel entre organisations représentatives d'employeurs et de salariés. / Ces accords ne doivent comporter aucune stipulation incompatible avec les dispositions légales en vigueur (...) ".<br/>
<br/>
              7. Le législateur ayant ainsi prévu que les mesures prises pour l'application de la loi seraient définies par un accord collectif conclu entre les partenaires sociaux, dont l'entrée en vigueur est subordonnée à l'intervention d'un arrêté ministériel d'agrément, il appartient au juge administratif, compétemment saisi d'un recours pour excès de pouvoir dirigé contre cet arrêté, de se prononcer lui-même, compte tenu de la nature particulière d'un tel accord, sur les moyens mettant en cause la légalité de ce dernier.<br/>
<br/>
              Sur la légalité de l'arrêté attaqué :<br/>
<br/>
              8. En vertu des dispositions citées au point 5, les organisations d'employeurs et de salariés représentatives de l'ensemble des professions de la production cinématographique, de l'audiovisuel ou du spectacle ou, lorsque ces organisations ne sont pas parvenues à conclure un accord respectant le document de cadrage mentionné à l'article L. 5424-22 du code du travail, les organisations professionnelles d'employeurs et les organisations syndicales de salariés représentatives au niveau national et interprofessionnel sont compétentes pour négocier entre elles les règles spécifiques d'indemnisation des artistes et des techniciens intermittents du spectacle, annexées au règlement général lui-même annexé à la convention relative à l'indemnisation du chômage, destinées à tenir compte des modalités particulières d'exercice des professions concernées.  <br/>
<br/>
              9. En l'espèce, l'annexe VIII au règlement général annexé à la convention du 14 avril 2017 relative à l'assurance chômage détermine les règles d'indemnisation des ouvriers et techniciens qui, conformément à son article 1er, ont travaillé dans les domaines d'activités et pour des fonctions définis dans une liste jointe à cette annexe, au titre d'un contrat de travail à durée déterminée. Elle exige, en outre, pour les ouvriers et techniciens des prestations techniques au service de la création et de l'événement ayant été employés dans les domaines de la production de films pour le cinéma, de la postproduction de films cinématographiques, de vidéos et de programmes de télévision (sauf studios d'animation) ainsi que de l'enregistrement sonore et de l'édition musicale, la " certification sociale " de l'entreprise qui les a employés. Elle entend ainsi se référer à la convention collective nationale des entreprises techniques au service de la création et de l'événement du 21 février 2008 et à l'accord du 18 juin 2010 portant sur la certification sociale des entreprises, étendus par arrêtés des 21 octobre 2008 et 18 février 2011, en vertu desquels cette " certification " vise à constater le respect de la législation et de la réglementation du travail et des conventions collectives applicables ainsi que des règles de sécurité et constitue une condition pour recourir aux contrats à durée déterminée d'usage, tels que prévus par l'article D. 1242-1 du code du travail.<br/>
<br/>
              10. S'il était loisible aux partenaires sociaux de mettre en place un dispositif de " certification sociale " destiné à encourager le respect par les employeurs de la législation du travail et des conventions collectives applicables, ceux-ci ont toutefois, en subordonnant le versement aux salariés des allocations chômage prévues à l'annexe VIII à la détention par l'employeur d'une telle " certification ", dont l'objet ne se limite pas à identifier les entreprises susceptibles d'employer des techniciens intermittents du spectacle, posé une condition sans rapport direct avec les modalités particulières d'exercice des professions de la production cinématographique, de l'audiovisuel ou du spectacle et qui ne saurait, dès lors, être regardée comme une règle spécifique d'indemnisation des techniciens intermittents du spectacle. Par suite, les organisations requérantes sont fondées à soutenir que les parties à la convention ne pouvaient légalement imposer une telle condition. <br/>
<br/>
              11. La légalité d'un arrêté ministériel portant agrément d'un des accords mentionnés par l'article L. 5422-20 du code du travail est nécessairement subordonnée à la validité des stipulations de l'accord en cause. Par suite, les organisations requérantes sont fondées à demander l'annulation de l'arrêté du 4 mai 2017 portant agrément de la convention du 14 avril 2017 relative à l'assurance chômage et de ses textes associés, en tant qu'il agrée les stipulations de l'annexe VIII au règlement général annexé à cette convention qui subordonnent son bénéfice à la certification sociale de certains employeurs. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens de la requête.<br/>
<br/>
              12. Enfin, il ne ressort pas des pièces du dossier que l'annulation rétroactive, dans cette mesure, des dispositions de l'arrêté du 4 mai 2017 entraînerait des conséquences manifestement excessives, eu égard tant aux effets qu'elles ont produits que des situations qui ont pu se constituer lorsqu'elles étaient en vigueur. Il n'y a pas lieu, par suite, de limiter dans le temps les effets de cette annulation.<br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              13. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 2 000 euros à verser à la Fédération nationale des syndicats du spectacle, du cinéma, de l'audiovisuel et de l'action culturelle CGT, au titre de l'article L. 761-1 du code de justice administrative. En revanche, ces dispositions font obstacle à ce qu'il soit fait droit aux demandes du Mouvement des entreprises de France et de la Confédération des petites et moyennes entreprises, de la Confédération française démocratique du travail et de la fédération Communication conseil culture - CFDT " ainsi que de l'Union des entreprises de proximité présentées au même titre.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêté du 4 mai 2017 du ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social portant agrément de la convention du 14 avril 2017 relative à l'assurance chômage et de ses textes associés est annulé en tant qu'il agrée les stipulations de l'annexe VIII au règlement général annexé à cette convention qui subordonnent son bénéfice à la certification sociale de certains employeurs.<br/>
Article 2 : L'Etat versera une somme de 2 000 euros à la Fédération nationale des syndicats du spectacle, du cinéma, de l'audiovisuel et de l'action culturelle CGT au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Les conclusions du Mouvement des entreprises de France et de la Confédération des petites et moyennes entreprises, de la Confédération française démocratique du travail et de la fédération Communication conseil culture - CFDT ainsi que de l'Union des entreprises de proximité présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée, pour l'ensemble des requérants, à la Fédération nationale des syndicats du spectacle, du cinéma, de l'audiovisuel et de l'action culturelle CGT, première dénommée, à la ministre du travail et, pour l'ensemble des défendeurs, au Mouvement des entreprises de France, à la Confédération française démocratique du travail et à l'Union des entreprises de proximité, premières dénommées.<br/>
Copie en sera adressée à la Confédération française des travailleurs chrétiens, à la Confédération française de l'encadrement CGC et à la Confédération générale du travail Force ouvrière.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-10-02 TRAVAIL ET EMPLOI. POLITIQUES DE L'EMPLOI. INDEMNISATION DES TRAVAILLEURS PRIVÉS D'EMPLOI. - RÈGLES SPÉCIFIQUES D'INDEMNISATION DES TECHNICIENS INTERMITTENTS DU SPECTACLE - CONDITION SUBORDONNANT LE VERSEMENT DES ALLOCATIONS CHÔMAGE À LA CERTIFICATION SOCIALE DE L'ENTREPRISE EMPLOYEUR - CONDITION SANS RAPPORT DIRECT AVEC LES MODALITÉS PARTICULIÈRES D'EXERCICE DES PROFESSIONS CONCERNÉES - EXISTENCE - CONSÉQUENCE - ILLÉGALITÉ.
</SCT>
<ANA ID="9A"> 66-10-02 S'il était loisible aux partenaires sociaux de mettre en place un dispositif de certification sociale destiné à encourager le respect par les employeurs de la législation du travail et des conventions collectives applicables, ils ont toutefois, en subordonnant le versement aux salariés des allocations chômage prévues à l'annexe VIII au règlement général annexé à la convention du 14 avril 2017 relative à l'assurance chômage à la détention d'une telle certification, dont l'objet ne se limite pas à identifier les entreprises susceptibles d'employer des techniciens intermittents du spectacle, posé une condition sans rapport direct avec les modalités particulières d'exercice des professions de la production cinématographique, de l'audiovisuel ou du spectacle et qui ne saurait, dès lors, être regardée comme une règle spécifique d'indemnisation des techniciens intermittents du spectacle. Par suite, les parties à la convention ne pouvaient légalement imposer une telle condition.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
