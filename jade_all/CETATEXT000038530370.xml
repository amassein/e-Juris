<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038530370</ID>
<ANCIEN_ID>JG_L_2019_05_000000411209</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/53/03/CETATEXT000038530370.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 29/05/2019, 411209, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-05-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411209</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:411209.20190529</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La SARL Montisambert a demandé au tribunal administratif de Paris de prononcer la décharge de la cotisation supplémentaire d'impôt sur les sociétés à laquelle elle a été assujettie au titre de l'exercice clos en 2011. Par un jugement n° 1431288 du 22 octobre 2015, le tribunal administratif de Paris a rejeté cette demande. Par un arrêt n° 15PA04767 du 6 avril 2017, la cour administrative d'appel de Paris a rejeté l'appel que la SARL Montisambert a formé contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux mémoires en réplique, enregistrés les 6 juin et 22 août 2017 et les 15 juin et 27 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, la SARL Montisambert demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel; <br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la SARL Montisambert ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la SARL Montisambert, holding de gestion de portefeuille, a acquis, entre le 21 mars et le 3 septembre 2007, des actions de la société Sarenza, représentant 5,17 % de son capital social. Après une augmentation de capital de la société Sarenza, intervenue le 27 mai 2009 et à laquelle la SARL Montisambert n'a pas souscrit, ces actions n'ont plus représenté que 4,34 % du capital social. Par un acte du 22 décembre 2011, la SARL Montisambert a cédé ces titres. A l'issue d'une vérification de comptabilité de la SARL Montisambert, l'administration a estimé que la plus-value résultant de cette cession ne relevait pas des dispositions spéciales du a quinquies du I de 1'article 219 du code général des impôts, mais du taux d'imposition de droit commun. Par un jugement du 22 octobre 2015, le tribunal administratif de Paris a rejeté la demande de la SARL Montisambert tendant à la décharge de la cotisation supplémentaire d'impôt sur les sociétés à laquelle elle a été assujettie au titre de l'exercice clos en 2011. La SARL Montisambert se pourvoit en cassation contre l'arrêt du 6 avril 2017 par lequel la cour administrative d'appel de Paris a rejeté le pourvoi qu'elle a formé contre ce jugement.<br/>
<br/>
              2. Aux termes du I de l'article 219 du code général des impôts, dans sa rédaction applicable au litige : " (...) / Le taux normal de l'impôt est fixé à 33, 1/3 %. / Toutefois: (...) / a quinquies. Pour les exercices ouverts à compter du 1er janvier 2006, le montant net des plus-values à long terme afférentes à des titres de participation fait l'objet d'une imposition séparée au taux de 8 %. Ce taux est fixé à 0 % pour les exercices ouverts à compter du 1er janvier 2007. / (...) / Les titres de participation mentionnés au premier alinéa sont les titres de participation revêtant ce caractère sur le plan comptable, les actions acquises en exécution d'une offre publique d'achat ou d'échange par l'entreprise qui en est l'initiatrice et les titres ouvrant droit au régime des sociétés mères si ces actions ou titres sont inscrits en comptabilité au compte titres de participation ou à une subdivision spéciale d'un autre compte du bilan correspondant à leur qualification comptable, à l'exception des titres des sociétés à prépondérance immobilière définis au troisième alinéa du a. ". Sur le plan comptable, les titres de participation sont ceux dont la possession durable est estimée utile à l'activité de l'entreprise, notamment parce qu'elle permet d'exercer une influence sur la société émettrice des titres ou d'en assurer le contrôle. Une telle utilité peut notamment être caractérisée si les conditions d'achat des titres en cause révèlent l'intention de l'acquéreur d'exercer une influence sur la société émettrice et lui donnent les moyens d'exercer une telle influence.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que M. A..., qui était le gérant et l'unique associé de la SARL Montisambert, a été désigné comme l'un des cinq membres du conseil de surveillance de la société Sarenza en vertu des stipulations du pacte conclu entre les actionnaires de cette société, le 22 mai 2007, à une date correspondant, ainsi que l'a relevé la cour, à la prise de participation de la SARL Montisambert dans la société Sarenza. <br/>
<br/>
              4. Il résulte de ce qui a été dit au point 2 ci-dessus que la cour ne pouvait, sans erreur de droit, pour juger que les titres en cause n'avaient pas le caractère de titres de participation, se fonder sur la circonstance, sans rapport avec les conditions d'achat des titres, qu'aucun élément du dossier ne permettait d'établir que M. A... aurait été désigné au conseil de surveillance de la société Sarenza en tant que représentant de la SARL Montisambert. La cour a également commis une erreur de droit en se déterminant au vu des conditions d'exercice du mandat de M. A... au conseil de surveillance, alors qu'il lui appartenait d'apprécier, au vu des conditions d'achat des titres, l'intention initiale de la SARL Montisambert d'exercer une influence et ses moyens de l'exercer. <br/>
<br/>
              5. Il résulte de ce qui précède que la SARL Montisambert est fondée, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, à demander l'annulation de l'arrêt attaqué. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à la SARL Montisambert de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 6 avril 2017 de la cour administrative d'appel de Paris est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la cour d'appel administrative d'appel de Paris.<br/>
Article 3 : L'Etat versera à la SARL Montisambert la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la SARL Montisambert et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
