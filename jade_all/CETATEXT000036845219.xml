<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036845219</ID>
<ANCIEN_ID>JG_L_2018_04_000000405449</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/84/52/CETATEXT000036845219.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 26/04/2018, 405449, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-04-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>405449</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:405449.20180426</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Paris d'annuler la décision par laquelle la Ville de Paris a implicitement rejeté sa demande du 22 janvier 2014 tendant à la requalification de ses contrats de travail en un contrat de travail à durée indéterminée et de condamner la ville à lui verser une somme de 88 821,99 euros en réparation des préjudices qu'elle estime avoir subis.<br/>
<br/>
              Par un jugement n° 1404735/2-3 du 2 avril 2015, le tribunal administratif de Paris a condamné la Ville de Paris à verser à Mme A...une somme de 2 000 euros pour préjudice moral et rejeté le surplus des conclusions de sa demande.<br/>
<br/>
              Par un arrêt n° 15PA02238 du 27 septembre 2016, la cour administrative d'appel de Paris a rejeté l'appel formé par Mme A...contre ce jugement en tant qu'il n'avait que partiellement fait droit à ses demandes en limitant son indemnisation à 2 000 euros.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés au secrétariat de la section du contentieux du Conseil d'Etat les 28 novembre 2016 et 21 février 2017, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la Ville de Paris la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 83-634 du 13 juillet 1983;<br/>
              - la loi n° 84-53 du 26 janvier 1984;<br/>
              - la loi n° 2005-843 du 26 juillet 2005 ;<br/>
              - la loi n° 2012-347 du 12 mars 2012 ;<br/>
              - le décret n° 94-415 du 24 mai 1994 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de MmeA..., et à la SCP Foussard, Froger, avocat de la Ville de Paris ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il résulte des dispositions de l'article 3 de la loi du 26 janvier 1984, dans leur rédaction issue de la loi du 26 juillet 2005 portant diverses mesures de transposition du droit communautaire à la fonction publique, que les collectivités territoriales ne peuvent recruter par contrat à durée déterminée des agents non titulaires que, d'une part, au titre des premier et deuxième alinéas de cet article, en vue d'assurer des remplacements momentanés ou d'effectuer des tâches à caractère temporaire ou saisonnier définies à ces alinéas et, d'autre part, s'agissant des dérogations, énoncées aux quatrième, cinquième et sixième alinéas du même article, au principe selon lequel les emplois permanents sont occupés par des fonctionnaires, lorsqu'il n'existe pas de cadre d'emplois de fonctionnaires susceptibles d'assurer certaines fonctions, lorsque, pour des emplois de catégorie A, la nature des fonctions ou les besoins des services le justifient et, dans les communes de moins de 1 000 habitants, lorsque la durée de travail de certains emplois n'excède pas la moitié de celle des agents publics à temps complet ; que ces dispositions sont applicables aux personnels des administrations parisiennes en vertu des articles 4 et 6 du décret du 24 mai 1994 portant dispositions statutaires relatives aux personnels des administrations parisiennes ;<br/>
<br/>
              2.	Considérant qu'aux termes des septième et huitième alinéas du même article 3 de la loi du 26 janvier 1984, dans sa rédaction issue de la loi du 26 juillet 2005 : " Les agents recrutés conformément aux quatrième, cinquième, et sixième alinéas sont engagés par des contrats à durée déterminée, d'une durée maximale de trois ans. Ces contrats sont renouvelables, par reconduction expresse. La durée des contrats successifs ne peut excéder six ans. / Si, à l'issue de la période maximale de six ans mentionnée à l'alinéa précédent, ces contrats sont reconduits, ils ne peuvent l'être que par décision expresse et pour une durée indéterminée " ; qu'aux termes de l'article 21 de la loi du 12 mars 2012 relative à l'accès à l'emploi titulaire et à l'amélioration des conditions d'emploi des agents contractuels dans la fonction publique, à la lutte contre les discriminations et portant diverses dispositions relatives à la fonction publique : " A la date de publication de la présente loi, la transformation de son contrat en contrat à durée indéterminée est obligatoirement proposée à l'agent contractuel, employé par une collectivité territoriale ou un des établissements publics mentionnés à l'article 2 de la loi n° 84-53 du 26 janvier 1984 précitée conformément à l'article 3 de la même loi, dans sa rédaction antérieure à celle résultant de la présente loi, qui se trouve en fonction ou bénéficie d'un congé prévu par le décret pris en application de l'article 136 de ladite loi. / Le droit défini au premier alinéa du présent article est subordonné à une durée de services publics effectifs, accomplis auprès de la même collectivité ou du même établissement public, au moins égale à six années au cours des huit années précédant la publication de la présente loi. / Toutefois, pour les agents âgés d'au moins cinquante-cinq ans à cette même date, la durée requise est réduite à trois années au moins de services publics effectifs accomplis au cours des quatre années précédant la même date de publication " ; <br/>
<br/>
              3.	Considérant qu'il résulte de la combinaison de ces dispositions que, pour les agents contractuels de la fonction publique territoriale recrutés sur un emploi permanent, en fonction au moment de la publication de la loi du 12 mars 2012, le renouvellement de contrat régi par l'article 21 de cette loi doit intervenir selon les règles fixées par les septième et huitième alinéas de l'article 3 de la loi du 26 janvier 1984 et ne peut donc concerner que les titulaires de contrats entrant dans les catégories énoncées aux quatrième, cinquième et sixième alinéas de ce même article ; que le droit ainsi reconnu aux agents dont le contrat, correspondant à un besoin permanent, fait l'objet d'une reconduction, d'en bénéficier pour une durée indéterminée n'est subordonné ni par cette disposition, ni par aucune autre disposition régissant la fonction publique territoriale, à la condition que le contrat soit conclu pour un service à temps complet ; que, si l'article 55 du décret du 24 mai 1994 portant dispositions statutaires relatives aux personnels des administrations parisiennes énonce que les fonctions des personnels des administrations parisiennes qui, correspondant à un besoin permanent, impliquent un service à temps non complet, sont assurées par des agents non titulaires, cette disposition réglementaire est sans incidence sur l'application à tous les contrats correspondant à un besoin permanent, qu'ils soient ou non conclus pour un service à temps complet, des dispositions de l'article 21 de la loi du 12 mars 2012 ;<br/>
              4.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme A...a été recrutée à compter de 1979 par la direction des affaires scolaires de la Ville de Paris en qualité de chargée de cours " couleur et chromatologie " en vertu d'un contrat à durée déterminée à temps incomplet conclu au titre de l'année scolaire qui a fait l'objet de renouvellements successifs chaque année et, en dernier lieu, pour la période comprise entre le 1er octobre 2012 et le 10 juin 2013 ; que, par une lettre du 22 janvier 2014, elle a demandé la requalification de son engagement en contrat à durée indéterminée ; que, pour confirmer le jugement du tribunal administratif de Paris du 2 avril 2015 rejetant sa demande qui tendait à l'annulation de la décision implicite par laquelle le maire de Paris avait refusé de lui donner satisfaction, la cour, après avoir cité l'article 55 du décret du 24 mai 1994, a constaté que Mme A... devait être regardée comme un agent non titulaire recruté sur le fondement de ces dispositions et non sur celui de l'article 3 de la loi du 26 janvier 1984 et en a déduit qu'elle ne pouvait, dans ces conditions, utilement se prévaloir des dispositions de l'article 21 de la loi du 12 mars 2012 ; <br/>
<br/>
              5.	Considérant qu'il résulte de ce qui a été dit au point 3 qu'en statuant ainsi la cour a commis une erreur de droit ; que Mme A...est dès lors fondée, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, à demander pour ce motif l'annulation de l'arrêt qu'elle attaque ; <br/>
<br/>
              6.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la Ville de Paris la somme de 3 000 euros à verser à Mme A...au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 27 septembre 2016 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : La Ville de Paris versera la somme de 3 000 euros à Mme A...au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme B...A...et à la Ville de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
