<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037142686</ID>
<ANCIEN_ID>JG_L_2018_06_000000420862</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/14/26/CETATEXT000037142686.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 25/06/2018, 420862, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420862</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:420862.20180625</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 23 mai et 12 juin 2018 au secrétariat du contentieux du Conseil d'Etat, l'association " VIVRE notre LOIRE " demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution de la décision n° CODEP-OLS-2018-012716 du 20 mars 2018 du président de l'Autorité de sûreté nucléaire autorisant la société Electricité de France (EDF) à créer provisoirement une aire d'entreposage de déchets potentiellement pathogènes pour le site de Belleville-sur-Loire (INB n° 127 et 128) ;<br/>
<br/>
              2°) de mettre à la charge de l'Autorité de sûreté nucléaire la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elle soutient que :  <br/>
              - la requête est recevable ;<br/>
              - la condition d'urgence est remplie, dès lors que la décision litigieuse porte une atteinte grave et immédiate aux intérêts qu'elle défend en matière d'environnement et de santé, compte tenu, d'une part, de la nature des déchets en cause, d'autre part, de la localisation et des modalités de leur entreposage ; <br/>
              - il existe un doute sérieux quant à la légalité de la décision contestée ; <br/>
              - elle a été prise par une autorité incompétente, car, en application de l'article L. 492-11 du code de l'environnement, elle aurait dû être délibérée par le collège de l'Autorité de sûreté nucléaire ;<br/>
              - elle a été prise par une autorité incompétente, le président de l'Autorité de sûreté nucléaire ne pouvant déléguer les compétences qui lui ont été confiées par le collège de l'Autorité de sûreté nucléaire et la décision CODEP-CLG-2016-027468 du président du 6 juillet 2016 portant délégation de signature aux agents n'ayant pas été régulièrement publiée et ne visant pas les actes portant dispositions temporaires ; <br/>
              - elle est entachée d'un vice de procédure en ce qu'il n'a été procédé à aucune consultation préalable du public, alors qu'une telle consultation s'impose en vertu, d'une part, des dispositions combinées de l'article L. 593-15 du code de l'environnement et de l'article 26 du décret du 2 novembre 2007, en ce que, notamment, elles renvoient à l'article L. 123-19-2 du même code, d'autre part, des dispositions de la décision n° 2015-DC-0508 de l'Autorité de sûreté nucléaire du 21 avril 2015 relative à l'étude sur la gestion des déchets et au bilan des déchets produits dans les installations nucléaires de base ;  <br/>
              - le dossier de demande d'autorisation transmis par la société EDF à l'Autorité de sûreté nucléaire était incomplet, faute, notamment, de comporter une mise à jour, d'une part, de la partie relative aux " déchets " de l'étude d'impact de l'installation, d'autre part, de l'étude sur la gestion de ses déchets ; <br/>
              - l'Autorité de sûreté nucléaire a commis une erreur manifeste d'appréciation en autorisant un tel projet, compte tenu des dangers qu'il présente pour la santé humaine et pour l'environnement, au regard de sa localisation et de ses caractéristiques et alors qu'aucune mesure compensatoire n'a été prévue.<br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 8 juin 2018, l'Autorité de sûreté nucléaire conclut au rejet de la requête. Elle soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par l'association " VIVRE notre LOIRE " ne sont pas propres à créer un doute sérieux quant à la légalité de la décision contestée.<br/>
<br/>
              Par un mémoire en défense, enregistré le 8 juin 2018, la société EDF conclut au rejet de la requête et à ce que soit mise à la charge de l'association " VIVRE notre LOIRE " la somme de 4 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative. Elle soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par l'association " VIVRE notre LOIRE " ne sont pas propres à créer un doute sérieux quant à la légalité de la décision contestée.<br/>
<br/>
              La requête a été communiquée au ministre de la transition écologique et solidaire qui n'a pas produit d'observations.<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, l'association " VIVRE notre LOIRE " et, d'autre part, l'Autorité de sûreté nucléaire, la société EDF et le ministre de la transition écologique et solidaire ;<br/>
<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 13 juin 2018 à 11 heures au cours de laquelle ont été entendus : <br/>
              - les représentants de l'association " VIVRE notre LOIRE " ;<br/>
<br/>
              - les représentants de l'Autorité de sûreté nucléaire ; <br/>
<br/>
              - Me Molinié, avocat aux conseils, avocat de la société EDF ;<br/>
<br/>
              - les représentants de la société EDF ; <br/>
              et à l'issue de laquelle le juge des référés a différé la clôture de l'instruction au vendredi 15 juin à 18 heures, puis au lundi 18 juin à 18 heures et, enfin, au mercredi 20 juin à 18 heures ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 15 juin 2018 avant la clôture de l'instruction, présenté par l'Autorité de sûreté nucléaire, qui conclut au rejet de la requête ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 15 juin 2018 avant la clôture de l'instruction, présenté par la société EDF, qui conclut au rejet de la requête ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 18 juin 2018 avant la clôture de l'instruction, présenté par l'association " VIVRE notre LOIRE ", qui conclut aux mêmes fins que la requête ; <br/>
              Vu les autres pièces du dossier : <br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ;<br/>
              - le code de l'environnement ;<br/>
              - le décret n° 2007-1557 du 2 novembre 2007 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 593-15 du code de l'environnement : " En dehors des cas mentionnés aux II et III de l'article L. 593-14, les modifications notables d'une installation nucléaire de base, de ses modalités d'exploitation autorisées, des éléments ayant conduit à son autorisation ou à son autorisation de mise en service, ou de ses conditions de démantèlement pour les installations ayant fait l'objet d'un décret mentionné à l'article L. 593-28 sont soumises, en fonction de leur importance, soit à déclaration auprès de l'Autorité de sûreté nucléaire, soit à l'autorisation par cette autorité. Ces modifications peuvent être soumises à consultation du public selon les modalités prévues au titre II du livre Ier. Les conditions d'application du présent article sont définies par décret en Conseil d'Etat " ; qu'aux termes de l'article 26 du décret du 2 novembre 2007 relatif aux installations nucléaires de base et au contrôle, en matière de sûreté nucléaire, du transport de substances radioactives : "  Sauf dans les cas mentionnés à l'article 27, les modifications mentionnées à l'article L. 593-15 du code de l'environnement sont soumises à autorisation./I. - Pour obtenir cette autorisation, l'exploitant dépose auprès de l'Autorité de sûreté nucléaire une demande accompagnée d'un dossier comportant tous les éléments de justification utiles, notamment les mises à jour rendues nécessaires des documents mentionnés aux articles 8 et 20 et, en cas de modification du plan d'urgence interne, l'avis rendu par le comité d'hygiène, de sécurité et des conditions de travail en application de l'article L. 4523-4 du code du travail./L'exploitant indique en outre s'il estime que cette modification nécessite une mise à jour des prescriptions applicables./II. - Si le projet est susceptible de provoquer un accroissement significatif des prélèvements d'eau ou des rejets dans l'environnement, le dossier mentionné au I comprend également le bilan d'une mise à disposition du public effectuée dans les conditions prévues à l'article L. 123-19 du code de l'environnement. Les modalités de cette mise à disposition sont définies par l'Autorité de sûreté nucléaire. Elles respectent les dispositions du I de l'article R. 123-46-1 du code de l'environnement, sous la réserve que la publication de l'avis mentionné à cet article est effectuée par le préfet et qu'un exemplaire du bilan lui est adressé./Pour la mise en oeuvre du III de l'article R. 122-10 du code de l'environnement, les consultations prévues au I de ce même article sont mises en oeuvre par le préfet./III. - En dehors des cas mentionnés au II, lorsque la consultation du public est requise, elle est organisée dans les conditions définies à l'article L. 123-19-2 du code de l'environnement (...) " ;  <br/>
<br/>
              2. Considérant que, par une décision n° CODEP-OLS-2018-012716 du 20 mars 2018, prise sur le fondement des dispositions citées au point 1, le président de l'Autorité de sûreté nucléaire a autorisé la société EDF à créer, pour neuf mois, une aire provisoire d'entreposage de déchets potentiellement pathogènes sur le site de Belleville-sur-Loire, lequel comprend les installations nucléaires de base n° 127 et n° 128 ; que l'association " VIVRE notre LOIRE " demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de cette décision ; <br/>
<br/>
              3. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; <br/>
<br/>
              4. Considérant que les moyens soulevés par l'association requérante, tirés de l'incompétence, à plusieurs titres, du signataire de la décision contestée, de l'irrégularité de la procédure, en raison de l'absence de consultation préalable du public, du caractère incomplet du dossier de demande d'autorisation soumis à l'Autorité de sûreté nucléaire et de l'erreur manifeste d'appréciation entachant la décision litigieuse ne sont pas, en l'état de l'instruction, propres à créer un doute sérieux sur la légalité de la décision du 20 mars 2018 du président de l'Autorité de sûreté nucléaire ; <br/>
<br/>
              5. Considérant qu'il en résulte que, sans qu'il soit besoin de se prononcer sur la condition de l'urgence à suspendre l'exécution de la décision contestée, la requête de l'Association " VIVRE notre LOIRE ", y compris les conclusions qu'elle présente au titre des dispositions de l'article L. 761-1 du code de justice administrative, doit être rejetée ; que, dans les circonstances de l'espèce, il n'y a pas lieu de faire droit aux conclusions présentées au titre des mêmes dispositions par la société EDF ; <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de l'association " VIVRE notre LOIRE " est rejetée.<br/>
Article 2 : Les conclusions de la société Electricité de France (EDF) tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente ordonnance sera notifiée à l'association " VIVRE notre LOIRE ", à l'Autorité de sûreté nucléaire et à la société Electricité de France (EDF).<br/>
Copie en sera adressée au ministre de la transition écologique et solidaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
