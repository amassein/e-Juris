<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026477847</ID>
<ANCIEN_ID>JG_L_2012_10_000000360317</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/47/78/CETATEXT000026477847.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 10/10/2012, 360317</TITRE>
<DATE_DEC>2012-10-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360317</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2012:360317.20121010</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'arrêt n° 11VE03247 du 7 juin 2012, enregistré le 18 juin 2012 au secrétariat du contentieux du Conseil d'Etat, par lequel la cour administrative de Versailles, avant de statuer sur la requête du préfet du Val d'Oise tendant, d'une part, à l'annulation du jugement n° 1106773 du 12 août 2011 par lequel le tribunal administratif de Cergy-Pontoise a annulé ses arrêtés du 9 août 2011 portant reconduite à la frontière de M.A..., fixant le pays de destination et ordonnant son placement en rétention, d'autre part, au rejet de la demande de M. A..., a décidé, par application de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat en soumettant à son examen les questions suivantes :<br/>
<br/>
              1°) Les décisions prises sur le fondement de l'article L. 533-1 du code de l'entrée et du séjour des étrangers et du droit d'asile doivent-elles être regardées comme entrant dans le champ d'application de la directive 2008/115/CE du 16 décembre 2008 alors que, même lorsqu'elles concernent les étrangers en situation irrégulière, elles n'ont ni pour cause cette situation, ni pour objet d'y mettre fin '<br/>
<br/>
              2°) Dans l'affirmative, si ces décisions ont pour motif un travail dissimulé, correspondent-elles, par principe, à l'hypothèse prévue par l'article 7 de la directive d'une dispense de départ volontaire, à raison d'un danger " pour l'ordre public, la sécurité publique ou la sécurité nationale " ' ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la directive 2008/115/CE du Parlement européen et du Conseil du 16 décembre 2008 ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile, notamment son article L. 533-1 ;<br/>
<br/>
              Vu le code du travail, notamment son article L. 5221-5 ;<br/>
<br/>
              Vu la loi n° 2011-672 du 16 juin 2011 ;<br/>
<br/>
              Vu le code de justice administrative, notamment son article L. 113-1 ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, Maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, Rapporteur public,<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              REND L'AVIS SUIVANT<br/>
<br/>
<br/>
              1. Aux termes de l'article L. 533-1 inséré dans le code de l'entrée et du séjour des étrangers et du droit d'asile par la loi du 16 juin 2011 relative à l'immigration, à l'intégration et à la nationalité : " L'autorité administrative compétente peut, par arrêté motivé, décider qu'un étranger, sauf s'il est au nombre de ceux visés à l'article L. 121-4, doit être reconduit à la frontière : / 1° Si son comportement constitue une menace pour l'ordre public. / La menace pour l'ordre public peut s'apprécier au regard de la commission des faits passibles de poursuites pénales sur le fondement des articles du code pénal cités au premier alinéa de l'article L. 313-5 du présent code, ainsi que des 1o, 4o, 6o et 8o de l'article 311-4, de l'article 322-4-1 et des articles 222-14, 224-1 et 227-4-2 à 227-7 du code pénal ; / 2° Si l'étranger a méconnu l'article L. 5221-5 du code du travail. / Le présent article ne s'applique pas à l'étranger qui réside régulièrement en France depuis plus de trois mois. / Les articles L. 511-4, L. 512-1 à L. 512-3, le premier alinéa de l'article L. 512-4, le premier alinéa du I de l'article L. 513-1 et les articles L. 513-2, L. 513-3, L. 514-1, L. 514-2 et L. 561-1 du présent code sont applicables aux mesures prises en application du présent article. ".<br/>
<br/>
              2. L'article L. 5221-5 du code du travail dispose pour sa part que : " Un étranger autorisé à séjourner en France ne peut exercer une activité professionnelle salariée en France sans avoir obtenu au préalable l'autorisation de travail mentionnée au 2° de l'article L. 5221-2. (...)  ".<br/>
<br/>
              3. Le Parlement européen et le Conseil ont pris, le 16 décembre 2008, une directive relative aux normes et procédures communes applicables dans les Etats membres au retour des ressortissants de pays tiers en séjour irrégulier. L'objet de la directive est défini dans les termes suivants par son article 1er : " La présente directive fixe les normes et procédures communes à appliquer dans les Etats membres au retour des ressortissants des pays tiers en séjour irrégulier, conformément aux droits fondamentaux en tant que principes généraux du droit communautaire ainsi qu'au droit international, y compris aux obligations en matière de protection des réfugiés et de droits de l'homme ". Son champ d'application est précisé par son article 2 selon lequel : " 1. La présente directive s'applique aux ressortissants de pays tiers en séjour irrégulier sur le territoire d'un Etat membre. (...) ".<br/>
<br/>
              4. Par ailleurs, l'article 3 paragraphe 2 définit le séjour irrégulier comme : " la présence sur le territoire d'un Etat membre d'un ressortissant d'un pays tiers qui ne remplit pas, ou ne remplit plus, les conditions d'entrée énoncées à l'article 5 du code frontières Schengen, ou d'autres conditions d'entrée, de séjour ou de résidence dans cet Etat membre " et l'article 3 paragraphe 4 définit la décision de retour comme : " une décision ou un acte de nature administrative ou judiciaire déclarant illégal le séjour d'un ressortissant d'un pays tiers et imposant ou énonçant une obligation de retour ". <br/>
<br/>
              5. Enfin, selon l'article 6 de la directive : " 1. Les Etats membres prennent une décision de retour à l'encontre de tout ressortissant d'un pays tiers en séjour irrégulier sur leur territoire (...) " et son article 7, relatif au " départ volontaire ", dispose que : " 1. La décision de retour prévoit un délai approprié allant de sept à trente jours pour le départ volontaire, sans préjudice des exceptions visées aux paragraphes 2 et 4. (...). / 4. S'il existe un risque de fuite, ou si une demande de séjour régulier a été rejetée comme manifestement non fondée ou frauduleuse, ou si la personne concernée constitue un danger pour l'ordre public, la sécurité publique ou la sécurité nationale, les Etats membres peuvent s'abstenir d'accorder un délai de départ volontaire ou peuvent accorder un délai inférieur à sept jours. ".<br/>
<br/>
              6. Il résulte clairement de ces dispositions que la directive n'est applicable qu'aux décisions de retour qui sont prises par les Etats membres au motif que les étrangers sont en situation de séjour irrégulier. En revanche, la directive n'a pas vocation à régir les procédures d'éloignement qui reposent sur des motifs distincts, notamment la menace à l'ordre public ou la méconnaissance d'autres normes de portée générale, telle que l'obligation de détenir une autorisation de travail pour exercer une activité professionnelle.<br/>
<br/>
              7. Il résulte de ce qui précède que les décisions prises sur le fondement de l'article L. 533-1 du code de l'entrée et du séjour des étrangers et du droit d'asile ne relèvent pas de la directive 2008/115/CE du 16 décembre 2008, alors même qu'elles peuvent légalement intervenir à l'encontre d'étrangers en situation irrégulière, dès lors que le motif qui fonde ces décisions n'est pas l'irrégularité du séjour des intéressés.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Le présent avis sera notifié à la cour administrative de Versailles, à M. B...A...et au ministre de l'intérieur.<br/>
<br/>
              Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">15-02-04 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. PORTÉE DES RÈGLES DU DROIT DE L'UNION EUROPÉENNE. DIRECTIVES. - DIRECTIVE RETOUR - CHAMP D'APPLICATION - DÉCISIONS DE RECONDUITE À LA FRONTIÈRE PRISES SUR LE FONDEMENT DE L'ARTICLE L. 533-1 DU CESEDA - EXCLUSION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">15-03-01-01 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. APPLICATION DU DROIT DE L'UNION EUROPÉENNE PAR LE JUGE ADMINISTRATIF FRANÇAIS. ACTES CLAIRS. INTERPRÉTATION DU DROIT DE L'UNION. - DIRECTIVE RETOUR - CHAMP D'APPLICATION - DÉCISIONS DE RETOUR PRISES AU MOTIF QUE LES ÉTRANGERS SONT EN SITUATION DE SÉJOUR IRRÉGULIER - INCLUSION - DÉCISIONS DE RETOUR PRISES POUR D'AUTRES MOTIFS - EXCLUSION, ALORS MÊME QU'ELLES S'APPLIQUERAIENT À UN ÉTRANGER EN SITUATION IRRÉGULIÈRE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">15-05-045-07 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. - DIRECTIVE RETOUR - CHAMP D'APPLICATION - DÉCISIONS DE RETOUR PRISES AU MOTIF QUE LES ÉTRANGERS SONT EN SITUATION DE SÉJOUR IRRÉGULIER - INCLUSION - DÉCISIONS DE RETOUR PRISES POUR D'AUTRES MOTIFS, NOTAMMENT SUR LE FONDEMENT DE L'ARTICLE L. 533-1 DU CESEDA - EXCLUSION, ALORS MÊME QU'ELLES PEUVENT S'APPLIQUER À UN ÉTRANGER EN SITUATION IRRÉGULIÈRE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">335 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. - MESURE D'ÉLOIGNEMENT PRÉVUE À L'ARTICLE L. 533-1 DU CESEDA - 1) CHAMP D'APPLICATION - ETRANGERS EN SITUATION IRRÉGULIÈRE - INCLUSION - 2) INCLUSION DANS LE CHAMP DE LA DIRECTIVE RETOUR - ABSENCE.
</SCT>
<ANA ID="9A"> 15-02-04 Il résulte clairement des dispositions des articles 1 à 3 de la directive 2008/115/CE du 16 décembre 2008 que celle-ci n'est applicable qu'aux décisions de retour qui sont prises par les Etats membres au motif que les étrangers sont en situation de séjour irrégulier. Dès lors, les décisions prises sur le fondement de l'article L. 533-1 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), qui prévoit qu'un étranger peut être reconduit à la frontière pour menace à l'ordre public ou pour exercice d'une activité salariée sans autorisation, ne relèvent pas de la directive retour, alors même qu'elles peuvent légalement intervenir à l'encontre d'étrangers en situation irrégulière, dès lors que le motif qui fonde ces décisions n'est pas l'irrégularité du séjour des intéressés.</ANA>
<ANA ID="9B"> 15-03-01-01 Il résulte clairement des dispositions des articles 1 à 3 de la directive 2008/115/CE du 16 décembre 2008 que celle-ci n'est applicable qu'aux décisions de retour qui sont prises par les Etats membres au motif que les étrangers sont en situation de séjour irrégulier. En revanche, la directive n'a pas vocation à régir les procédures d'éloignement qui reposent sur des motifs distincts, notamment la menace à l'ordre public ou la méconnaissance d'autres normes de portée générale, telle que l'obligation de détenir une autorisation de travail pour exercer une activité professionnelle.</ANA>
<ANA ID="9C"> 15-05-045-07 Il résulte clairement des dispositions des articles 1 à 3 de la directive 2008/115/CE du 16 décembre 2008 que celle-ci n'est applicable qu'aux décisions de retour qui sont prises par les Etats membres au motif que les étrangers sont en situation de séjour irrégulier. En revanche, la directive n'a pas vocation à régir les procédures d'éloignement qui reposent sur des motifs distincts, notamment la menace à l'ordre public ou la méconnaissance d'autres normes de portée générale, telle que l'obligation de détenir une autorisation de travail pour exercer une activité professionnelle. Il en va notamment ainsi des décisions prises sur le fondement de l'article L. 533-1 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), qui prévoit qu'un étranger puisse être reconduit à la frontière pour menace à l'ordre public ou pour exercice d'une activité salariée sans autorisation, alors même qu'elle peuvent légalement être prises à l'encontre d'étrangers en situation irrégulière, dès lors que le motif qui fonde ces décisions n'est pas l'irrégularité du séjour des intéressés.</ANA>
<ANA ID="9D"> 335 L'article L. 533-1 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), qui prévoit qu'un étranger puisse être reconduit à la frontière pour menace à l'ordre public ou pour exercice d'une activité salariée sans autorisation, précise qu'il ne s'applique pas à l'étranger qui réside régulièrement en France depuis plus de trois mois. 1) Ces dispositions s'appliquent notamment aux étrangers en situation irrégulière. 2) Pour autant, les décisions prises sur le fondement de l'article L. 533-1, dès lors que le motif qui les fonde n'est pas l'irrégularité du séjour des intéressés, ne relèvent pas de la directive 2008/115/CE du 16 décembre 2008 (dite directive retour), qui n'est applicable qu'aux décisions de retour qui sont prises par les Etats membres au motif que les étrangers sont en situation de séjour irrégulier.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
