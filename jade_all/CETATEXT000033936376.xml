<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033936376</ID>
<ANCIEN_ID>JG_L_2017_01_000000402079</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/93/63/CETATEXT000033936376.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Formation spécialisée, 27/01/2017, 402079, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-01-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402079</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Formation spécialisée</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR>Mme Catherine de Salins</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEFSP:2017:402079.20170127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 2 août et 2 novembre 2016, M. A...B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision, révélée par le courrier de la présidente de la Commission nationale de l'informatique et des libertés (CNIL) du 2 juin 2016, par laquelle le ministre de la défense lui a refusé l'accès aux données susceptibles de le concerner figurant dans le traitement automatisé de données de la direction générale de la sécurité extérieure (DGSE) ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Par un mémoire, enregistré le 2 novembre 2016 et présenté en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, M. B...demande, à l'appui de sa requête, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 773-8 du code de justice administrative, dans sa rédaction issue de la loi du 24 juillet 2015 relative au renseignement.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
   - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique, le ministre de la défense et la Commission nationale de l'informatique et des libertés, qui ont été mis à même de prendre la parole avant et après les conclusions ;<br/>
<br/>
              Et après avoir entendu en séance :<br/>
<br/>
              - le rapport de Mme Catherine de Salins, conseiller d'Etat,  <br/>
<br/>
              - et, les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. M. B...soutient que l'article L. 773-8 du code de justice administrative, en ne prévoyant qu'une simple faculté pour le juge de sanctionner une irrégularité commise dans la tenue de fichiers couverts par le secret de la défense nationale, méconnaît le droit au recours effectif garanti par l'article 16 de la Déclaration des droits de l'homme et du citoyen.<br/>
<br/>
              3. L'article L. 773-8 du code de justice administrative, issu de la loi du 24 juillet 2015, dispose que : " Lorsqu'elle traite des requêtes relatives à la mise en oeuvre de l'article 41 de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, la formation de jugement se fonde sur les éléments contenus, le cas échéant, dans le traitement sans les révéler ni révéler si le requérant figure ou non dans le traitement. Toutefois, lorsqu'elle constate que le traitement ou la partie de traitement faisant l'objet du litige comporte des données à caractère personnel le concernant qui sont inexactes, incomplètes, équivoques ou périmées, ou dont la collecte, l'utilisation, la communication ou la conservation est interdite, elle en informe le requérant, sans faire état d'aucun élément protégé par le secret de la défense nationale. Elle peut ordonner que ces données soient, selon les cas, rectifiées, mises à jour ou effacées. Saisie de conclusions en ce sens, elle peut indemniser le requérant ". <br/>
<br/>
              4. Il résulte de ces dispositions que, lorsqu'il a été constaté que des données figurent illégalement dans un fichier, l'autorité gestionnaire du fichier a l'obligation de les effacer ou de les rectifier, dans la mesure du nécessaire. Dans ces conditions, le moyen tiré de ce que, en laissant au juge, lorsqu'il a constaté une illégalité et en a informé le demandeur, la faculté d'enjoindre à l'autorité compétente de procéder à cet effacement ou cette rectification, le législateur aurait méconnu le droit à un recours juridictionnel effectif garanti par l'article 16 de la Déclaration des droits de l'homme et du citoyen ne soulève aucune question nouvelle ou sérieuse. Par suite, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par le requérant. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M.B.... <br/>
Article 2 : La présente décision sera notifiée à M. A...B...et au ministre de la défense. <br/>
Copie en sera adressée au Premier ministre, au Conseil constitutionnel et à la Commission nationale de l'informatique et des libertés.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
