<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033194840</ID>
<ANCIEN_ID>JG_L_2016_10_000000395711</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/19/48/CETATEXT000033194840.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 05/10/2016, 395711, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-10-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395711</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Manon Perrière</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:395711.20161005</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° Sous le numéro 395711, par une requête enregistrée le 30 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, l'association des comptables publics demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la note de service n° 2015/07/6653 du 21 juillet 2015 relative aux mouvements sur les postes comptables de catégorie C2/C3 et sur les emplois administratifs d'inspecteurs divisionnaires, en tant qu'elle prévoit, au sein de la direction générale des finances publiques, certaines priorités d'accès à ces postes,  ensemble la décision implicite par laquelle le ministre des finances et des comptes publics et le ministre de l'économie, de l'industrie et du numérique ont refusé de retirer les dispositions litigieuses de cette note ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° Sous le numéro 398340, par une requête enregistrée le 29 mars 2016 au secrétariat du contentieux du Conseil d'Etat, l'association des comptables publics demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la note de service 2016/01/5833 du 29 janvier 2016, relative aux mouvements sur les postes comptables de catégories C2/C3 et sur les emplois administratifs d'inspecteurs divisionnaires, en tant qu'elle prévoit, au sein de la direction générale des finances publiques, certaines priorités d'accès à ces postes ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le décret n° 2006-814 du 7 juillet 2006 ;<br/>
              - le décret n° 2009-208 du 20 février 2009 ;<br/>
              - le décret n° 2010-986 du 26 août 2010 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Manon Perrière, auditeur,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
<br/>
<br/>1. Considérant que, par deux notes de service du 21 juillet 2015 et du 29 janvier 2016, le directeur général des finances publiques a défini, respectivement pour le premier semestre et le second semestre de l'année 2016, les modalités d'organisation des mouvements sur les postes comptables de deuxième et de troisième catégorie, que les agents du corps des personnels de catégorie A de la direction générale des finances publiques, régi par le décret du 26 août 2010 portant statut particulier des personnels de catégorie A de la direction générale des finances publiques, ont vocation à occuper ; que le guide pratique annexé à ces notes de service, auxquelles elles renvoient, prévoit notamment, en cas de restructuration ou de fusion de postes comptables de catégories C2/C3, ou de création d'un service des impôts des particuliers, que priorité devra être donnée au " cadre occupant le grade le plus élevé (AFIPA, IP/IDIV ex IP, IDIV HC) " ; que, sous le numéro 395711, l'association des comptables publics demande l'annulation pour excès de pouvoir de la note de service du 21 juillet 2015 et du guide pratique qui y est annexé en tant qu'ils instituent, par ces dispositions, une priorité en faveur des administrateurs des finances publiques adjoints par rapport aux inspecteurs principaux des finances publiques et en faveur de ces derniers par rapport aux inspecteurs divisionnaires des finances publiques, voire, au sein du grade d'inspecteur divisionnaire des finances publiques, une priorité en faveur des inspecteurs divisionnaires anciens inspecteurs principaux par rapport aux inspecteurs divisionnaires hors classe ; qu'elle demande également, sous le même numéro, l'annulation de la décision implicite par laquelle le ministre des finances et des comptes publics et le ministre de l'économie, de l'industrie et du numérique ont refusé de retirer ces dispositions ; que, sous le numéro 398340, l'association des comptables publics demande l'annulation pour excès de pouvoir, dans la même mesure et pour les mêmes motifs, de la note de service du 29 janvier 2016 et du guide pratique qui y est annexé ;<br/>
<br/>
              2. Considérant que les deux requêtes présentées par l'association requérante présentent à juger les mêmes questions ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              Sur les fins de non-recevoir opposées par le ministre des finances et des comptes publics et le ministre de l'économie, de l'industrie et du numérique : <br/>
<br/>
              3. Considérant, d'une part, qu'en l'absence, dans les statuts d'une association ou d'un syndicat, de stipulation réservant expressément à un autre organe la capacité de décider de former une action devant le juge administratif, celle-ci est régulièrement engagée par l'organe tenant des mêmes statuts le pouvoir de représenter cette association ou ce syndicat en justice ; qu'une habilitation à représenter une association ou un syndicat dans les démarches qu'elle entreprend doit être regardée comme habilitant à le représenter en justice ; qu'il ressort des pièces du dossier qu'aux termes de l'alinéa 2 de l'article 4 du règlement intérieur, auquel renvoie l'article 13 des statuts de l'association des comptables publics, le président " représente l'association dans toutes les démarches qu'elle entreprend " ; qu'ainsi, le président de l'association requérante doit être regardé comme ayant été habilité à ester en justice pour celle-ci ; que la fin de non-recevoir invoquée à ce titre, dans le n° 395711, par les ministres des finances et des comptes publics et de l'économie, de l'industrie et du numérique, doit donc être écartée ;<br/>
<br/>
              4. Considérant, d'autre part, qu'il ressort des pièces du dossier que le guide pratique annexé aux notes de service fixe les règles à respecter par les destinataires de celles-ci pour le mouvement national sur les postes comptables de catégories C2/C3, respectivement pour le premier et le second semestre de l'année 2016, et que les dispositions attaquées énoncent en termes impératifs un ordre de priorité défini, comme il a été rappelé au point 1 ci-dessus, en fonction du grade, voire, au sein du grade d'inspecteur divisionnaire des finances publiques, en fonction du parcours professionnel antérieur ; que, dans cette mesure, contrairement à ce que soutiennent les ministres, ces actes, ainsi que le refus de les retirer ou de les abroger, font grief et sont susceptibles d'être contestés devant le juge de l'excès de pouvoir ; <br/>
<br/>
              Sur la légalité des dispositions attaquées : <br/>
<br/>
              5. Considérant qu'aux termes de l'article 4 du décret du 7 juillet 2006 relatif aux emplois de chef de service comptable au ministère de l'économie, des finances et de l'industrie : " Peuvent être nommés aux emplois de chef de service comptable de 2e catégorie et 3e catégorie : 1° Les administrateurs des finances publiques adjoints ayant atteint au moins le 5e échelon de leur grade ; 2° Les inspecteurs principaux de la direction générale des finances publiques ayant atteint au moins le 8e échelon de leur grade ; 3° Les inspecteurs divisionnaires hors classe de la direction générale des finances publiques ayant atteint le 3e échelon de leur grade ; (...) " ; que ni ce décret, ni le décret du 26 août 2010 portant statut particulier des personnels de catégorie A de la direction générale des finances publiques n'établissent d'autres règles de priorité entre les agents ayant ainsi vocation à être nommés sur des emplois de chef de service comptable de catégories C2/C3, pour la nomination sur l'un de ces emplois ; qu'en l'absence de toute disposition statutaire en ce sens applicable aux agents des corps concernés par les notes de services attaquées, qu'elle résulte des décrets mentionnés ci-dessus ou de l'article 60 de la loi du 11 janvier 1984, le directeur général des finances publiques n'avait pas compétence pour fixer lui-même les règles de priorité énoncées par les dispositions attaquées, telles que rappelées au point 1 ci-dessus ; que, pour ce motif, et sans qu'il soit besoin d'examiner les autres moyens des requêtes, l'association requérante est fondée à demander l'annulation de ces dispositions, ainsi que celle de la décision implicite par laquelle le ministre des finances et des comptes publics et le ministre de l'économie, de l'industrie et du numérique ont refusé de retirer les dispositions litigieuses de la note de service du 21 juillet 2015 ;<br/>
<br/>
              Sur les conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              6. Considérant qu'il y lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros au titre de ces dispositions à verser à l'association des comptables publics ;<br/>
<br/>
<br/>
<br/>                            D E C I D E :<br/>
                                          --------------<br/>
<br/>
Article 1er : Les notes de service du 21 juillet 2015 et du 29 janvier 2016 et les guides pratiques qui y sont annexés sont annulés en tant qu'ils prévoient, en cas de restructuration ou de fusion de postes comptables de catégories C2/C3, ou de création d'un service des impôts des particuliers, que priorité devra être donnée au " cadre occupant le grade le plus élevé (AFIPA, IP/IDIV ex IP, IDIV HC) ", de même que la décision implicite par laquelle le ministre des finances et des comptes publics et le ministre de l'économie, de l'industrie et du numérique ont refusé de retirer ces dispositions de la note de service du 21 juillet 2015.<br/>
<br/>
Article 2 : L'Etat versera à l'association des comptables publics la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 3 : La présente décision sera notifiée à l'association des comptables publics et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
