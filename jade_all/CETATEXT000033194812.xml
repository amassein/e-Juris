<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033194812</ID>
<ANCIEN_ID>JG_L_2016_10_000000386802</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/19/48/CETATEXT000033194812.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 05/10/2016, 386802</TITRE>
<DATE_DEC>2016-10-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386802</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:386802.20161005</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Procédure contentieuse antérieure :<br/>
<br/>
              Mlle A...B...a demandé au tribunal administratif de Lille, d'une part, d'annuler l'arrêté du 9 février 2009 par lequel le président du syndicat intercommunal de la région de Flines à Guesnain (SIRFAG) a mis fin à son stage à compter du 9 février 2009 et, d'autre part, de condamner le SIRFAG à lui verser une somme de 22 000 euros en réparation des préjudices qu'elle a subis du fait de l'illégalité de la décision de licenciement et de l'absence de versement des indemnités de perte d'emploi pour la période de février à septembre 2009 et pour la période de mai à septembre 2010. <br/>
<br/>
              Par un jugement n° 1101625 du 2 avril 2013, le tribunal administratif de Lille a condamné le SIRFAG à verser à Mlle B...la somme de 1 000 euros en réparation du préjudice causé par le versement tardif de ses indemnités de perte d'emploi pour la période de février à septembre 2009 et rejeté le surplus de sa demande.<br/>
<br/>
              Par un arrêt n° 13DA00878 du 30 octobre 2014, la cour administrative d'appel de Douai a, d'une part, annulé l'arrêté du 9 février 2009 et réformé le jugement du tribunal administratif en ce qu'il a de contraire à son arrêt et a, d'autre part, rejeté le surplus des conclusions d'appel de Mlle B...ainsi que l'appel incident du SIRFAG.<br/>
<br/>
              Procédure devant le Conseil d'Etat :<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 30 décembre 2014 et 30 mars 2015 au secrétariat du contentieux du Conseil d'Etat, la communauté d'agglomération du Douaisis, venant aux droits du SIRFAG, demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 13DA00878 du 30 octobre 2014 de la cour administrative d'appel de Douai en ce qu'il lui est défavorable ; <br/>
<br/>
              2°) de mettre à la charge de Mlle B...une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
                     	Vu les autres pièces du dossier ;<br/>
<br/>
                     Vu :<br/>
                     - la loi n° 84-53 du 26 janvier 1984 ;<br/>
                     - le décret n° 92-1194 du 4 novembre 1992 ;<br/>
                     - le code de justice administrative ; <br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de la communauté d'agglomération du Douaisis ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 9 février 2009, le président du syndicat intercommunal de la région de Flines à Guesnain (SIRFAG) a mis fin à compter du 31 décembre 2008, pour motif économique, au stage de Mlle B..., adjoint administratif de 2ème classe stagiaire. Mlle B... a demandé au tribunal administratif de Lille, d'une part, l'annulation de cet arrêté et, d'autre part, la réparation des préjudices qu'elle a subis du fait de l'illégalité de cet arrêté et des retards apportés au versement de ses indemnités pour perte d'emploi. Par un jugement du 2 avril 2013, le tribunal administratif de Lille a rejeté ses conclusions à fin d'annulation de l'arrêté du 9 février 2009 mais a condamné le SIRFAG à verser à Mlle B...une somme de 1 000 euros en réparation du préjudice subi à raison du retard de versement d'un revenu de remplacement pendant sept mois. La communauté d'agglomération du Douaisis, venant aux droits du SIRFAG, se pourvoit en cassation contre l'arrêt du 30 octobre 2014 en tant que la cour administrative d'appel de Douai a annulé l'arrêté du 9 février 2009, réformé le jugement du tribunal administratif de Lille en ce qu'il a de contraire à son arrêt et rejeté l'appel incident du SIRFAG.<br/>
<br/>
              Sur les conclusions relatives à l'arrêté du 9 février 2009 :<br/>
<br/>
              2. Si, en vertu d'un principe général du droit dont s'inspirent tant les dispositions du code du travail relatives à la situation des salariés dont l'emploi est supprimé que les règles du statut général de la fonction publique, qui imposent de donner, dans un délai raisonnable, aux fonctionnaires en activité dont l'emploi est supprimé une nouvelle affectation correspondant à leur grade, il incombe à l'administration avant de pouvoir prononcer le licenciement de proposer à l'intéressé un emploi de niveau équivalent ou, à défaut d'un tel emploi et si l'intéressé le demande, de tout autre emploi et, en cas d'impossibilité, de prononcer le licenciement dans les conditions qui lui sont applicables, ce principe général ne confère aux fonctionnaires stagiaires, qui se trouvent dans une situation probatoire et provisoire, aucun droit à être reclassés dans l'attente d'une titularisation en cas de suppression de leur emploi. En revanche, lorsqu'il est mis fin au stage par l'autorité territoriale en raison de la suppression de l'emploi ou pour toute autre cause ne tenant pas à la manière de servir, le fonctionnaire territorial stagiaire est, le cas échéant, en application de l'article 44 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, réinscrit de droit, à sa demande, sur la liste d'aptitude prévue à cet article. Par suite, en jugeant qu'un tel principe général du droit était applicable aux fonctionnaires stagiaires et que sa mise en oeuvre impliquait que l'administration, lorsqu'elle entend supprimer un emploi occupé par un fonctionnaire stagiaire pour des motifs d'économie, propose à ce fonctionnaire stagiaire un emploi de niveau équivalent, ou à défaut d'un tel emploi et si l'intéressé le demande, tout autre emploi, et ne puisse le licencier que si le reclassement s'avère impossible, faute d'emploi vacant ou si l'intéressé refuse la proposition qui lui est faite, la cour administrative d'appel a commis une erreur de droit.<br/>
<br/>
              Sur les conclusions indemnitaires :<br/>
<br/>
              3. Après son licenciement, Mlle B...a été privée de tout revenu de remplacement entre le mois de février et le mois de septembre 2009 en raison du retard apporté par le SIRFAG au versement des allocations pour perte d'emploi. Si le SIRFAG a fait valoir devant la cour administrative d'appel, que ce retard était dû à l'envoi tardif par le centre de gestion de la fonction publique territoriale du Nord de l'état liquidatif des sommes dues à Mlle B..., il ressort toutefois des pièces du dossier soumis aux juges du fond que ce centre de gestion, qui n'exerçait qu'un rôle de conseil, a été consulté tardivement par le SIRFAG. Par suite, en jugeant, par une décision suffisamment motivée, que le SIRFAG ne pouvait utilement se prévaloir du retard de transmission de l'état liquidatif des sommes dues à MlleB..., la cour administrative d'appel n'a commis aucune erreur de droit.<br/>
<br/>
              4. Il résulte de tout ce qui précède de la communauté d'agglomération du Douaisis n'est fondée, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, à demander l'annulation de l'arrêt qu'elle attaque qu'en tant qu'il statue sur ses conclusions à fin d'annulation de l'arrêté du 9 février 2009.<br/>
<br/>
              5. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la communauté d'agglomération du Douaisis sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Douai du 30 octobre 2014 est annulé en tant qu'il statue sur ses conclusions aux fins d'annulation de l'arrêté du 9 février 2009.<br/>
<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Douai.<br/>
<br/>
Article 3 : Les conclusions présentées par la communauté d'agglomération du Douaisis au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la communauté d'agglomération du Douaisis et à Mlle A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07-04 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. GARANTIES DIVERSES ACCORDÉES AUX AGENTS PUBLICS. - PRINCIPE GÉNÉRAL DU DROIT OBLIGEANT L'ADMINISTRATION À CHERCHER À RECLASSER UN AGENT AVANT DE POUVOIR LE LICENCIER - APPLICABILITÉ AUX FONCTIONNAIRES STAGIAIRES DONT L'EMPLOI EST SUPPRIMÉ - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-10-06-01 FONCTIONNAIRES ET AGENTS PUBLICS. CESSATION DE FONCTIONS. LICENCIEMENT. STAGIAIRES. - PRINCIPE GÉNÉRAL DU DROIT OBLIGEANT L'ADMINISTRATION À CHERCHER À RECLASSER UN AGENT AVANT DE POUVOIR LE LICENCIER - APPLICABILITÉ AUX FONCTIONNAIRES STAGIAIRES DONT L'EMPLOI EST SUPPRIMÉ - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 01-04-03-07-04 Si, en vertu d'un principe général du droit dont s'inspirent tant les dispositions du code du travail relatives à la situation des salariés dont l'emploi est supprimé que les règles du statut général de la fonction publique, qui imposent de donner, dans un délai raisonnable, aux fonctionnaires en activité dont l'emploi est supprimé une nouvelle affectation correspondant à leur grade, il incombe à l'administration avant de pouvoir prononcer le licenciement de proposer à l'intéressé un emploi de niveau équivalent ou, à défaut d'un tel emploi et si l'intéressé le demande, tout autre emploi et, en cas d'impossibilité, de prononcer le licenciement dans les conditions qui lui sont applicables, ce principe général ne confère aux fonctionnaires stagiaires, qui se trouvent dans une situation probatoire et provisoire, aucun droit à être reclassés dans l'attente d'une titularisation en cas de suppression de leur emploi.</ANA>
<ANA ID="9B"> 36-10-06-01 Si, en vertu d'un principe général du droit dont s'inspirent tant les dispositions du code du travail relatives à la situation des salariés dont l'emploi est supprimé que les règles du statut général de la fonction publique, qui imposent de donner, dans un délai raisonnable, aux fonctionnaires en activité dont l'emploi est supprimé une nouvelle affectation correspondant à leur grade, il incombe à l'administration avant de pouvoir prononcer le licenciement de proposer à l'intéressé un emploi de niveau équivalent ou, à défaut d'un tel emploi et si l'intéressé le demande, tout autre emploi et, en cas d'impossibilité, de prononcer le licenciement dans les conditions qui lui sont applicables, ce principe général ne confère aux fonctionnaires stagiaires, qui se trouvent dans une situation probatoire et provisoire, aucun droit à être reclassés dans l'attente d'une titularisation en cas de suppression de leur emploi.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., s'agissant des agents contractuels, CE, Section, avis, 25 septembre 2013, Mme,, n° 365139, p. 223 ; CE, 18 décembre 2013, Ministre de l'éducation nationale c/ Mme,, n° 366369, T. p. 643. Rappr., s'agissant d'un fonctionnaire stagiaire atteint d'une inaptitude physique définitive, CE, 17 févier 2016, Ministre de l'intérieur c/ M.,, n° 381429, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
