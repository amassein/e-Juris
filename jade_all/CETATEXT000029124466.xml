<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029124466</ID>
<ANCIEN_ID>JG_L_2014_06_000000375929</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/12/44/CETATEXT000029124466.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème / 10ème SSR, 23/06/2014, 375929</TITRE>
<DATE_DEC>2014-06-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375929</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème / 10ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Maïlys Lange</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:375929.20140623</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 28 février 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par la commune de Cysoing, représentée par son maire ; la commune de Cysoing demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir, à titre principal, le décret n° 2014-167 du 17 février 2014 portant délimitation des cantons dans le département du Nord ou, à titre subsidiaire, ce décret en tant que, par son article 37, il désigne la commune de Templeuve en qualité de chef-lieu du canton n° 36 du département et son bureau centralisateur comme bureau centralisateur de ce canton ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code électoral ; <br/>
<br/>
              Vu le code général des collectivités territoriales ; <br/>
<br/>
              Vu le décret n° 2013-938 du 18 octobre 2013 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Maïlys Lange, auditeur,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
<br/>
<br/>1. Considérant, en premier lieu, qu'aux termes du I de l'article L. 3113-2 du code général des collectivités territoriales, dans sa version actuellement en vigueur : " Les modifications des limites territoriales des cantons, les créations et suppressions de cantons et le transfert du siège de leur chef-lieu sont décidés par décret en Conseil d'Etat après consultation du conseil général (...) " ; que l'article L. 3121-19 de ce code prévoit que : " Douze jours au moins avant la réunion du conseil général, le président adresse aux conseillers généraux un rapport, sous quelque forme que ce soit, sur chacune des affaires qui doivent leur être soumises (...) " ; qu'il ressort des pièces du dossier qu'en application de ces dispositions, le président du conseil général du Nord a adressé aux conseillers généraux, le 4 décembre 2013, en vue de la réunion du conseil général prévue le 16 décembre, le projet de décret délimitant les nouveaux cantons du département, accompagné d'un rapport ainsi que de plusieurs annexes, comportant des cartes et des tableaux ; que, contrairement à ce que soutient la commune, le rapport complété par ces documents a permis de donner aux conseillers généraux une information suffisante sur le projet de décret soumis à la délibération du conseil général et, ainsi, a répondu aux exigences, y compris de délai, fixées par l'article L. 3121-19 du code général des collectivités territoriales ;  <br/>
<br/>
              2. Considérant, en deuxième lieu, que la commune requérante, qui est actuellement chef-lieu du canton n° 20, demande, à titre subsidiaire, l'annulation de l'article 37 du décret attaqué " en tant qu'il désigne la commune de Templeuve comme chef-lieu du nouveau canton n° 36 ", auquel elle sera désormais également rattachée ; qu'il ressort toutefois des termes mêmes de ce décret que le second alinéa de son article 37 désigne non pas la commune de Templeuve comme chef-lieu du nouveau canton n° 36 mais le bureau centralisateur de cette commune comme bureau centralisateur de ce canton ; qu'en vertu de l'article R. 69 du code électoral, lorsque les électeurs de la commune sont répartis en plusieurs bureaux de vote, le bureau centralisateur est chargé d'opérer le recensement général des votes en présence des présidents des autres bureaux ; que si l'article R. 112 du même code, dans sa version actuellement en vigueur, prévoit que le recensement général des votes est fait par le bureau du chef-lieu de canton, la version de cet article issue du décret du 18 octobre 2013 portant application de la loi n° 2013-403 du 17 mai 2013 relative à l'élection des conseillers départementaux, des conseillers municipaux et des conseillers communautaires, et modifiant le calendrier électoral, qui est applicable, comme le décret attaqué, à compter du prochain renouvellement général des assemblées départementales, confie ce rôle au bureau centralisateur du canton  ; qu'ainsi, la qualité de bureau centralisateur d'un canton sera, à compter de l'entrée en vigueur de ces nouvelles dispositions, dépourvue de tout lien avec celle de chef-lieu de canton ; que, dès lors, en désignant les bureaux centralisateurs des nouveaux cantons, les décrets portant délimitation des cantons d'un département n'ont ni pour objet ni pour effet de procéder au transfert du siège des chefs-lieux de canton ; qu'il suit de là que le moyen de la commune de Cysoing tiré de ce que la désignation de la commune de Templeuve comme chef-lieu de canton, en lui faisant perdre cette qualité et les dotations qui y sont actuellement attachées, entacherait d'erreur d'appréciation l'article 37 du décret attaqué, ne peut qu'être écarté ;  <br/>
<br/>
              3. Considérant, en troisième lieu, qu'il ressort des pièces du dossier que la commune de Templeuve est, dans le canton n° 36, parmi les communes les mieux desservies, la plus peuplée ; que, par suite, la désignation de son bureau centralisateur comme bureau centralisateur du canton n'est pas entachée d'erreur manifeste d'appréciation ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède, que, sans qu'il soit besoin de statuer sur sa recevabilité, la requête de la commune de Cysoing doit être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de la commune de Cysoing est rejetée. <br/>
Article 2 : La présente décision sera notifiée à la commune de Cysoing et au ministre de l'intérieur.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-03-01-01 ÉLECTIONS ET RÉFÉRENDUM. ÉLECTIONS AU CONSEIL GÉNÉRAL. OPÉRATIONS PRÉLIMINAIRES À L'ÉLECTION. REMODELAGE DES CIRCONSCRIPTIONS CANTONALES. - DÉSIGNATION DES BUREAUX CENTRALISATEURS DES NOUVEAUX CANTONS - 1) NOUVELLE VERSION DE L'ARTICLE R. 112 DU CODE ÉLECTORAL (EN VIGUEUR À COMPTER DU PROCHAIN RENOUVELLEMENT GÉNÉRAL DES ASSEMBLÉES DÉPARTEMENTALES) - DÉCONNEXION ENTRE LA QUALITÉ DE BUREAU CENTRALISATEUR ET CELLE DE CHEF-LIEU DE CANTON - CONSÉQUENCE - PORTÉE DE LA DÉSIGNATION DES BUREAUX CENTRALISATEURS DES NOUVEAUX CANTONS - TRANSFERT DU SIÈGE DES CHEFS-LIEUX DE CANTON - ABSENCE - 2) CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR - CONTRÔLE RESTREINT.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-02-04 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE RESTREINT. - DÉSIGNATION DU BUREAU CENTRALISATEUR D'UN CANTON.
</SCT>
<ANA ID="9A"> 28-03-01-01 1) Si l'article R. 112 du code électoral, dans sa version actuellement en vigueur, prévoit que le recensement général des votes est fait par le bureau du chef-lieu de canton, la qualité de bureau centralisateur d'un canton sera, à compter de l'entrée en vigueur des nouvelles dispositions de cet article issues du décret n° 2013-938 du 18 octobre 2013, applicables à compter du prochain renouvellement général des assemblées départementales, dépourvue de tout lien avec celle de chef-lieu de canton.... ,,Dès lors, les décrets portant délimitation des cantons d'un département n'ont, en désignant les bureaux centralisateurs des nouveaux cantons, ni pour objet ni pour effet de procéder au transfert du siège des chefs-lieux de canton.,,,2) Le juge de l'excès de pouvoir exerce un contrôle restreint sur la désignation du bureau centralisateur d'une commune comme bureau centralisateur du canton.</ANA>
<ANA ID="9B"> 54-07-02-04 Le juge de l'excès de pouvoir exerce un contrôle restreint sur la désignation du bureau centralisateur d'une commune comme bureau centralisateur du canton.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
