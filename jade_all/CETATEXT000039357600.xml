<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039357600</ID>
<ANCIEN_ID>JG_L_2019_11_000000429675</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/35/76/CETATEXT000039357600.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 08/11/2019, 429675, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-11-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429675</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEORD:2019:429675.20191108</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Nogent Musée a demandé au tribunal administratif de Châlons-en-Champagne, d'une part, d'annuler le titre exécutoire n° 1013 émis le 16 octobre 2015 à son encontre par la commune de Nogent-sur-Seine et, en conséquence, de la décharger de l'obligation de payer la somme de 114 000 euros, d'autre part, d'annuler les titres exécutoires émis le 16 octobre 2015 à son encontre par la commune correspondant à la restitution des loyers R3 payés entre le 2 janvier et le 15 juillet 2015 et de la décharger de l'obligation de payer les sommes correspondantes. Par un jugement n°s 1502620, 1600212 du 24 mai 2017, le tribunal administratif de Châlons-en-Champagne a, d'une part, annulé l'ensemble des titres exécutoires en litige et, d'autre part, déchargé la société Nogent Musée du paiement de la somme de 114 000 euros au titre des pénalités qui lui ont été infligées au titre du contrat de partenariat et, pour un montant total de 139 646,43 euros, des sommes prélevées par compensation par la commune au titre des loyers R3. Le tribunal administratif a également enjoint à la commune de Nogent-sur-Seine de restituer les sommes prélevées par compensation sur les loyers pour des montants respectifs de 91 662,47 euros et de 139 646, 43 euros dans un délai de deux mois à compter de la date de notification du jugement et a rejeté le surplus des conclusions de la société Nogent Musée.<br/>
<br/>
              Par un arrêt n°s 17NC01814, 17NC01815 du 4 décembre 2018, la cour administrative d'appel de Nancy a, sur appel de la commune de Nogent-sur-Seine, notamment d'une part, annulé ce jugement en tant qu'il se prononce sur les conclusions de la demande n° 1600212 de la société Nogent Musée, d'autre part, annulé les titres exécutoires émis le 16 octobre 2015 pour un montant total de 70 551,60 euros, enfin, enjoint à la commune de Nogent-sur-Seine de restituer les sommes prélevées par compensation sur les loyers en recouvrement des titres exécutoires ainsi annulés, dans un délai de deux mois à compter de la date de notification de l'arrêt, puis rejeté le surplus des conclusions des parties.<br/>
<br/>
              Par une requête et deux nouveaux mémoires, enregistrés les 11 avril, 13 septembre et 21 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, la commune de Nogent-sur-Seine demande au Conseil d'Etat qu'il soit sursis à l'exécution de cet arrêt.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Sirinelli, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la commune de Nogent-sur-Seine, et à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la société Nogent Musée ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. La commune de Nogent-sur-Seine et la société Nogent Musée ont conclu, le 8 mars 2012, un contrat de partenariat portant sur le transfert, la restructuration et l'agrandissement du musée Dubois-Boucher sur le site de l'îlot Saint-Epoing, la réhabilitation et la restructuration de la maison Claudel et l'aménagement complet du site. Cette opération comprenait également la conception et la réalisation d'une partie des équipements muséographiques et scénographiques et la maintenance, l'entretien et l'exploitation technique des ouvrages. Les bâtiments ont été mis à la disposition de la commune de Nogent-sur-Seine à la date, prévue par avenant, du 2 janvier 2015 et la commune a commencé à verser à la société les loyers prévus par les stipulations du contrat de partenariat correspondant aux parties R2 (grosses réparations et renouvellement des équipements techniques) et R3 (maintenance des installations). S'agissant, en revanche, des éléments muséographiques, des installations techniques et de la maison Claudel, cette mise à disposition n'était pas intervenue à l'échéance contractuelle du 20 mai 2015 et la commune de Nogent-sur-Seine a dans ces conditions, émis, le 16 octobre 2015, un titre exécutoire pour un montant de 114 000 euros correspondant à des pénalités de retard pour la période courant du 21 mai au 16 juillet 2015, ainsi que trois mandats d'annulation-réduction valant titres exécutoires, pour un total de 70 551,60 euros, en vue de la récupération des loyers R3 versés du 2 janvier au 15 juillet 2015. Elle a également décidé de suspendre le versement des loyers R3. En vue de la mise en recouvrement de ses créances, elle a, s'agissant de la créance de 114 000 euros, procédé par compensation, à des prélèvements à hauteur d'une somme de 91 662,47 euros sur les loyers R1, R2 et R4 de la période du 15 juillet au 15 octobre 2015. Elle a poursuivi le recouvrement partiel de ses créances par compensation sur les loyers ultérieurs en récupérant la somme de 69 094,83 euros sur les loyers de la période courant du 15 juillet 2015 au 15 janvier 2016. La société Nogent Musée a demandé au tribunal administratif de Châlons-en-Champagne d'être déchargée du paiement des sommes faisant l'objet des titres exécutoires émis par la commune de Nogent-sur-Seine et la restitution des sommes qui lui ont été prélevées. La commune de Nogent-sur-Seine a relevé appel du jugement par lequel ce tribunal a, d'une part, annulé les titres exécutoires du 16 octobre 2015 et a, d'autre part, déchargé la société Nogent Musée de l'obligation de payer la somme de 114 000 euros au titre des pénalités mises à sa charge et les sommes correspondant aux loyers R3. Par un arrêt du 4 décembre 2018, la cour administrative d'appel de Nancy a, d'une part, annulé le jugement en tant qu'il se prononce sur les conclusions de la demande n° 1600212 de la société Nogent Musée, d'autre part, annulé les titres exécutoires émis le 16 octobre 2015 pour un montant total de 70 551,60 euros, enfin, enjoint à la commune de Nogent-sur-Seine de restituer les sommes prélevées par compensation sur les loyers en recouvrement des titres exécutoires ainsi annulés, dans un délai de deux mois à compter de la date de notification de l'arrêt. La commune de Nogent-sur-Seine se pourvoit en cassation contre cet arrêt dont elle demande le sursis à exécution. Eu égard aux moyens invoqués, sa demande doit être regardée comme tendant à ce qu'il soit sursis à l'exécution de l'arrêt attaqué en tant qu'il a, en premier lieu, annulé les titres exécutoires émis le 16 octobre 2015 pour un montant total de 70 551,60 euros et enjoint à la commune de Nogent-sur-Seine de restituer les sommes prélevées par compensation sur les loyers en recouvrement des titres exécutoires ainsi annulés, en deuxième lieu, rejeté l'appel de la commune contre le jugement du tribunal administratif de Châlons-en-Champagne en tant que celui-ci a annulé le titre exécutoire n° 1013 du 16 octobre 2015, déchargé la société Nogent Musée de l'obligation de payer la somme de 114 000 euros et enjoint à la commune de restituer cette somme à la société et, en dernier lieu, mis à la charge de la commune la somme de 1 500 euros à verser à la société Nogent Musée au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article R. 821-5 du code de justice administrative : " La formation de jugement peut, à la demande de l'auteur du pourvoi, ordonner qu'il soit sursis à l'exécution d'une décision juridictionnelle rendue en dernier ressort si cette décision risque d'entraîner des conséquences difficilement réparables et si les moyens invoqués paraissent, en l'état de l'instruction, sérieux et de nature à justifier, outre l'annulation de la décision juridictionnelle, l'infirmation de la solution retenue par les juges du fond ".<br/>
<br/>
              3. D'une part, l'arrêt attaqué a pour effet la restitution par la commune de Nogent-sur-Seine à la société Nogent Musée des sommes prélevées par compensation sur les loyers en recouvrement des titres exécutoires émis le 16 octobre 2015. Eu égard à la situation financière très dégradée de la société Nogent Musée et en l'absence de garantie financière écrite de l'actionnaire de cette société concernant les sommes en litige, l'exécution de cet arrêt risquerait d'exposer la commune de Nogent-sur-Seine à la perte définitive de sommes qui ne devraient pas rester à sa charge au cas où le Conseil d'Etat ferait droit à son pourvoi et ainsi d'entraîner des conséquences difficilement réparables pour la requérante.<br/>
<br/>
              4. D'autre part, les moyens tirés de ce que la cour administrative d'appel de Nancy a commis des erreurs de droit en estimant, d'une part, que la commune de Nogent-sur-Seine ne pouvait pas émettre de titres exécutoires sans avoir préalablement respecté la procédure de règlement amiable des différends prévue au contrat de partenariat alors que cette procédure n'était pas applicable en cas de compensation de créances et, d'autre part,  que l'annulation des titres en litige impliquait nécessairement la restitution des sommes prélevées par compensation sur les loyers dus à la société Nogent Musée apparaissent, en l'état de l'instruction, sérieux et de nature à justifier, outre l'annulation de l'arrêt attaqué, l'infirmation de la solution retenue par les juges du fond.<br/>
<br/>
              5. Dans ces conditions, il y a lieu d'ordonner le sursis à exécution de l'arrêt de la cour administrative d'appel de Nancy en tant qu'il a, en premier lieu, annulé les titres exécutoires émis le 16 octobre 2015 pour un montant total de 70 551,60 euros et enjoint à la commune de Nogent-sur-Seine de restituer les sommes prélevées par compensation sur les loyers en recouvrement des titres exécutoires ainsi annulés, en deuxième lieu, rejeté l'appel de la commune contre le jugement du tribunal administratif de Châlons-en-Champagne en tant que celui-ci a annulé le titre exécutoire n° 1013 du 16 octobre 2015, déchargé la société Nogent Musée de l'obligation de payer la somme de 114 000 euros et enjoint à la commune de restituer cette somme à la société et, en dernier lieu, mis à la charge de la commune la somme de 1 500 euros à verser à la société Nogent Musée au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Nogent-sur-Seine qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Jusqu'à ce qu'il ait été statué sur le pourvoi de la commune de Nogent-sur-Seine contre l'arrêt du 4 décembre 2018 de la cour administrative d'appel de Nancy, il sera sursis à l'exécution de cet arrêt en tant qu'il a, en premier lieu, annulé les titres exécutoires émis le 16 octobre 2015 pour un montant total de 70 551,60 euros et enjoint à la commune de Nogent-sur-Seine de restituer les sommes prélevées par compensation sur les loyers en recouvrement des titres exécutoires ainsi annulés, en deuxième lieu, rejeté l'appel de la commune contre le jugement du tribunal administratif de Châlons-en-Champagne en tant que celui-ci a annulé le titre exécutoire n° 1013 du 16 octobre 2015, déchargé la société Nogent Musée de l'obligation de payer la somme de 114 000 euros et enjoint à la commune de restituer cette somme à la société et, en dernier lieu, mis à la charge de la commune la somme de 1 500 euros à verser à la société Nogent Musée au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 2 : Les conclusions présentées par la société Nogent Musée au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la commune de Nogent-sur-Seine et à la société Nogent Musée.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
