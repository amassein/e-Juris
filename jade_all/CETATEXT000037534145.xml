<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037534145</ID>
<ANCIEN_ID>JG_L_2018_10_000000408663</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/53/41/CETATEXT000037534145.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 22/10/2018, 408663, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-10-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408663</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Julie Burguburu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:408663.20181022</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La SARL MDC Hydro a demandé au tribunal administratif de Rouen d'annuler l'arrêté du préfet de l'Eure du 4 décembre 2012 constatant l'arrêt de l'exploitation de la centrale hydraulique dite du Val Anglier, située sur le territoire de la commune de Perriers-sur-Andelle, et précisant les conditions de sa gestion temporaire, ainsi que de la décision implicite de rejet de son recours gracieux du 7 février 2013. Par un jugement n° 1301246 du 25 novembre 2014, le tribunal administratif de Rouen a rejeté ses demandes. <br/>
<br/>
              Par un arrêt n° 15DA00135 du 22 décembre 2016, la cour administrative d'appel de Douai a rejeté son appel formé contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 6 mars et le 6 juin 2017 au secrétariat du contentieux du Conseil d'Etat, la SARL MDC Hydro demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - la loi n° 2017-227 du 24 février 2017 ; <br/>
              - le décret n° 89-804 du 27 octobre 1989 ;<br/>
              - le décret du 27 avril 1995 portant classement des cours d'eau, parties de cours d'eau et canaux en application de l'article L. 232-6 du code rural ;<br/>
              - l'arrêté du 18 avril 1997 fixant par bassin ou sous-bassin, dans certains cours d'eau classés au titre de l'article L. 232-6 du code rural, la liste des espèces migratrices de poissons ;<br/>
              - l'arrêté du 4 décembre 2012 établissant la liste des cours d'eau mentionnée au 2° du I de l'article L. 214-17 du code de l'environnement sur le bassin Seine-Normandie ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Julie Burguburu, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de la SARL MDC Hydro.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1.	Il ressort des pièces du dossier soumis aux juges du fond que la SARL MDC Hydro a acquis en 2004 une centrale hydroélectrique, dite " du Val Anglier ", sur le territoire de la commune de Perrier-sur-Andelle, sur le cours d'eau l'Andelle, installation initialement autorisée par une ordonnance royale du 30 janvier 1839. Par un arrêté du 4 décembre 2012, le préfet de l'Eure a constaté l'arrêt de l'exploitation de la centrale depuis le 29 mars 2004, précisé les conditions de sa gestion temporaire notamment aux fins de protéger la dévalaison des poissons migrateurs et fixé les conditions de reprise de l'activité. Le 7 février 2013, la SARL MDC Hydro a formé un recours gracieux contre cet arrêté, auquel le préfet n'a pas répondu. La SARL MDC Hydro a demandé l'annulation de l'arrêté du 4 décembre 2012 ainsi que de la décision implicite de rejet de son recours gracieux au tribunal administratif de Rouen qui a rejeté sa requête par un jugement du 25 novembre 2014. Elle se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Douai du 22 décembre 2016 qui a rejeté son appel formé contre ce jugement.<br/>
<br/>
              2.	D'une part, avant son abrogation par l'ordonnance du 18 septembre 2000 relative à la partie législative du code de l'environnement, l'article L. 232-6 du code rural, introduit par le décret du 27 octobre 1989 portant révision du code rural en ce qui concerne les dispositions législatives relatives à la protection de la nature, disposait que : " Dans les cours d'eau ou parties de cours d'eau et canaux dont la liste est fixée par décret, après avis des conseils généraux rendus dans un délai de six mois, tout ouvrage doit comporter des dispositifs assurant la circulation des poissons migrateurs. L'exploitant de l'ouvrage est tenu d'assurer le fonctionnement et l'entretien de ces dispositifs. Les ouvrages existants doivent être mis en conformité, sans indemnité, avec les dispositions du présent article dans un délai de cinq ans à compter de la publication d'une liste d'espèces migratrices par bassin ou sous-bassin fixée par le ministre chargé de la pêche en eau douce et, le cas échéant, par le ministre chargé de la mer ". A compter de l'ordonnance du 18 septembre 2000 précitée, ces dispositions ont été reprises à l'identique à l'article L. 432-6 du code de l'environnement. Sur le fondement de ces dispositions, le décret du 27 avril 1995 portant classement des cours d'eau, parties de cours d'eau et canaux en application de l'article L. 232-6 du code rural a classé l'Andelle et ses affluents, et un arrêté du 18 avril 1997, publié au Journal officiel du 16 mai 1997, a établi la liste des espèces migratrices de poissons. Il résulte de la combinaison de ces textes que, sauf dispositions particulières, à compter du 17 mai 2002, tous les ouvrages implantés sur l'Andelle devaient avoir mis en place un dispositif permettant la circulation des poissons migrateurs.<br/>
<br/>
              3.	D'autre part, la loi du 30 décembre 2006 sur l'eau et les milieux aquatiques a introduit dans le code de l'environnement un nouvel article L. 214-17 aux termes duquel : " I. - Après avis des conseils généraux intéressés, des établissements publics territoriaux de bassin concernés, des comités de bassins et, en Corse, de l'Assemblée de Corse, l'autorité administrative établit, pour chaque bassin ou sous-bassin : (...) / 2° Une liste de cours d'eau, parties de cours d'eau ou canaux dans lesquels il est nécessaire d'assurer le transport suffisant des sédiments et la circulation des poissons migrateurs. Tout ouvrage doit y être géré, entretenu et équipé selon des règles définies par l'autorité administrative, en concertation avec le propriétaire ou, à défaut, l'exploitant. / III. - Les obligations résultant du I s'appliquent à la date de publication des listes. Celles découlant du 2° du I s'appliquent, à l'issue d'un délai de cinq ans après la publication des listes, aux ouvrages existants régulièrement installés. / Le cinquième alinéa de l'article 2 de la loi du 16 octobre 1919 relative à l'utilisation de l'énergie hydraulique et l'article L. 432-6 du présent code demeurent.applicables jusqu'à ce que ces obligations y soient substituées, dans le délai prévu à l'alinéa précédent A l'expiration du délai précité, et au plus tard le 1er janvier 2014, le cinquième alinéa de l'article 2 de la loi du 16 octobre 1919 précitée est supprimé et l'article L. 432-6 précité est abrogé. / Les obligations résultant du I du présent article n'ouvrent droit à indemnité que si elles font peser sur le propriétaire ou l'exploitant de l'ouvrage une charge spéciale et exorbitante ". En application de ces dispositions, un arrêté du 4 décembre 2012 établissant la liste des cours d'eau mentionnée au 2° du I de l'article L. 214-17 du code de l'environnement sur le bassin Seine-Normandie, publié au Journal officiel du 18 décembre 2012, a classé une partie de l'Andelle.<br/>
<br/>
              4.	Il résulte de la combinaison de ces dispositions, et notamment de celles du III de l'article L. 214-17 du code de l'environnement cité au point 3, telles qu'éclairées par les travaux parlementaires, que si un délai de cinq ans après la publication des listes prévues au 2° du I du même article L. 214-17 est accordé aux exploitants d' " ouvrages existants régulièrement installés " pour mettre en oeuvre les obligations qu'il instaure, ce délai n'est pas ouvert aux exploitants d'ouvrages antérieurement soumis à une obligation de mise en conformité en application de l'article L. 232-6 du code rural, devenu l'article L. 432-6 du code de l'environnement, qui n'auraient pas respecté le délai de cinq ans qui leur avait été octroyé par ces dispositions pour mettre en oeuvre cette obligation. Ces ouvrages existants ne peuvent être regardés comme " régulièrement installés ", au sens du III de l'article L. 214-17 du code de l'environnement, et sont donc soumis aux obligations résultant du I de cet article dès la publication des listes qu'il prévoit.<br/>
<br/>
              5.	Il ressort des pièces soumises aux juges du fond que, pour contester la légalité de l'arrêté du préfet de l'Eure du 4 décembre 2012, la SARL MDC Hydro a soutenu, en se fondant sur les dispositions du III de l'article L. 214-17 du code de l'environnement, que les obligations d'entretien et de mise en conformité qui résultaient des anciennes dispositions de l'article L. 232-6 du code rural, reprises par la suite à l'article L. 432-6 du code de l'environnement, ne lui étaient plus applicables depuis le 1er janvier 2014 et que les obligations similaires résultant du I de l'article L. 214-17 du même code ne lui seraient applicables qu'à compter du 17 décembre 2017, soit cinq ans après la publication de l'arrêté du 4 décembre 2012 mentionné au point 3.<br/>
<br/>
              6.	Pour écarter ce moyen, la cour administrative d'appel de Douai a retenu que la centrale hydraulique appartenant à la société MDC Hydro sur l'Andelle n'était plus en activité depuis 2004, que les prescriptions de l'article 4 de l'arrêté du préfet de l'Eure attaqué se bornaient à prévoir pour l'avenir, dans l'hypothèse d'une reprise d'activité, une mise en conformité de l'ouvrage avec les dispositions législatives en vigueur et qu'il ne résultait pas de l'instruction qu'une telle reprise d'activité serait envisageable avant le 18 décembre 2017. Cependant, en se fondant sur des circonstances inopérantes pour apprécier le bien-fondé du moyen soulevé devant elle, la cour a commis une erreur de droit.<br/>
<br/>
              7.	Il résulte de ce qui précède, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, que la SARL MDC Hydro est fondée à demander l'annulation de l'arrêt de la cour administrative d'appel de Douai qu'elle attaque. <br/>
<br/>
              8.	Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la l'Etat la somme de 3 000 euros à verser à la SARL MDC Hydro au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Douai du 22 décembre 2016 est annulé.<br/>
<br/>
      Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Douai.<br/>
<br/>
Article 3 : L'Etat versera à la SARL MDC Hydro une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la SARL MDC Hydro et au ministre d'Etat, ministre de la transition écologique et solidaire.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
