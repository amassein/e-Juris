<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041800418</ID>
<ANCIEN_ID>JG_L_2020_04_000000422178</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/80/04/CETATEXT000041800418.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 03/04/2020, 422178</TITRE>
<DATE_DEC>2020-04-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422178</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUTET-HOURDEAUX</AVOCATS>
<RAPPORTEUR>Mme Fanélie Ducloz</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:422178.20200403</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par un jugement n° 1609292/2-1 du 10 juillet 2018, enregistré le 11 juillet 2018 au secrétariat du contentieux du Conseil d'Etat sous le n° 422178, le tribunal administratif de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée à ce tribunal par la société Financière Taulane.<br/>
<br/>
              Par cette requête, deux mémoires en réplique et une note en délibéré, enregistrés au greffe du tribunal administratif de Paris les 14 juin et 21 novembre 2016 et les 4 juin et 9 juillet 2018, la société Financière Taulane demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 14 avril 2016 par laquelle l'Autorité des marchés financiers a refusé de faire usage de son pouvoir d'injonction à l'encontre de deux actionnaires qui auraient franchi individuellement, en novembre 2014, le seuil de 30 % du capital de la société de droit luxembourgeois Orco Property Group afin que soit déposée une offre publique d'acquisition des titres de cette société ;<br/>
<br/>
              2°) de condamner l'Autorité des marchés financiers à lui verser la somme de 769 075,71 euros en réparation du préjudice qu'elle estime avoir subi ;<br/>
<br/>
              3°) de mettre à la charge de l'Autorité des marchés financiers le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code monétaire et financier ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Fanélie Ducloz, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boutet-Hourdeaux, avocat de la société Financière Taulane ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier que la société Financière Taulane est porteur de bons de souscription d'actions remboursables émis en 2007 par la société Orco Property Group, domiciliée au Luxembourg, dont les titres ont été admis aux négociations sur le marché réglementé Euronext Paris jusqu'au 18 février 2016. La société Orco Property Group a, par un communiqué de presse du 18 novembre 2014, informé le public du franchissement à la hausse, par les sociétés Aspley Venture Limited et Fetumar Developpement Limited, du seuil de 30 % de détention de son capital ou de ses droits de vote. La société Financière Taulane a, par trois courriers des 10 juillet et 22 octobre 2015 et du 9 mars 2016, demandé à l'Autorité des marchés financiers d'enjoindre aux actionnaires ayant individuellement franchi à la hausse le seuil de 30 % de détention du capital ou des droits de vote de la société Orco Property Group, de déposer une offre publique d'acquisition obligatoire visant les bons de souscription d'actions remboursables émis par cette société. Par un courrier du 14 avril 2016, le secrétaire général de l'Autorité des marchés financiers a indiqué à la société Financière Taulane qu'elle pouvait saisir la Commission de surveillance du secteur financier, autorité de marché luxembourgeoise, dont il a précisé qu'elle était seule compétente pour apprécier s'il y avait lieu de déposer une offre publique d'acquisition obligatoire. La société Financière Taulane demande l'annulation de cette décision et la condamnation de l'Autorité des marchés financiers à lui verser une somme de 769 075,71 euros en réparation du préjudice qu'elle estime avoir subi en raison de la faute qu'aurait commise cette dernière en refusant de faire usage de ses pouvoirs d'injonction. <br/>
<br/>
              2. L'article L. 621-30 du code monétaire et financier dispose que : " L'examen des recours formés contre les décisions individuelles de l'Autorité des marchés financiers autres que celles, y compris les sanctions prononcées à leur encontre, relatives aux personnes et entités mentionnées au II de l'article L. 621-9 est de la compétence du juge judiciaire. ". Cette disposition réserve à l'autorité judiciaire la compétence pour connaître des recours formés contre les décisions individuelles de l'Autorité des marchés financiers autres que celles qui concernent les personnes et entités mentionnées au II de l'article L. 621-9 du code monétaire et financier. Il en va de même pour les actions tendant à la réparation des conséquences dommageables nées de telles décisions.<br/>
<br/>
              3. La décision attaquée, par laquelle le secrétaire général de l'Autorité des marchés financiers a indiqué à la société Financière Taulane qu'il lui appartenait de saisir l'autorité de marché luxembourgeoise, dont il estime qu'elle est seule compétente pour apprécier s'il y a lieu de déposer une offre publique obligatoire, doit être regardée comme une décision prise au nom de l'Autorité des marchés financiers par laquelle cette autorité refuse de faire droit à la demande de la société requérante tendant à ce qu'elle fasse usage des pouvoirs d'injonction, qu'elle tient de l'article L. 621-14 du code monétaire et financier, à l'encontre de certains actionnaires de la société Orco Property Group détenant plus de 30 % de son capital ou de ses droits de vote afin qu'ils déposent une offre publique d'acquisition obligatoire visant les titres de cette société. Il ressort des pièces du dossier que ni la société requérante, ni les actionnaires en cause ne sont au nombre des personnes et entités mentionnées au II de l'article L. 621-9 du code monétaire et financier. Par suite, le litige qui oppose la société Financière Taulane à l'Autorité des marchés financiers, relatif à la légalité de la décision attaquée de l'Autorité des marchés financiers, relève de la compétence du juge judiciaire. Il en va de même des conclusions tendant à la réparation du préjudice que la société estime avoir subi du fait de cette décision. Il n'appartient dès lors pas au Conseil d'Etat d'en connaître.<br/>
<br/>
              4. Il résulte de ce qui précède que la requête de la société Financière Taulane doit être rejetée comme portée devant une juridiction incompétente pour en connaître.<br/>
<br/>
              5. Il n'y a pas lieu de mettre à la charge de l'Autorité des marchés financiers la somme que demande la société Financière Taulane au titre des frais exposés par elle et non compris dans les dépens.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société Financière Taulane est rejetée comme portée devant une juridiction incompétente pour en connaître.<br/>
Article 2 : La présente décision sera notifiée à la société Financière Taulane et à l'Autorité des marchés financiers.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">13-01-02-01 CAPITAUX, MONNAIE, BANQUES. CAPITAUX. OPÉRATIONS DE BOURSE. AUTORITÉ DES MARCHÉS FINANCIERS. - DÉCISION PAR LAQUELLE L'AMF REFUSE DE FAIRE DROIT À UNE DEMANDE DE FAIRE USAGE DE SES POUVOIRS D'INJONCTION À L'ENCONTRE D'UN TIERS - AUTEUR DE LA DEMANDE ET TIERS CONCERNÉ N'ÉTANT PAS DES PROFESSIONNELS SOUMIS AU CONTRÔLE DE L'AMF - COMPÉTENCE DU JUGE JUDICIAIRE - 1) RECOURS CONTRE CETTE DÉCISION - INCLUSION - 2) CONCLUSIONS TENDANT À LA RÉPARATION DU PRÉJUDICE SUBI DU FAIT DE CETTE DÉCISION - INCLUSION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-03-01-02-05 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR DES TEXTES SPÉCIAUX. ATTRIBUTIONS LÉGALES DE COMPÉTENCE AU PROFIT DES JURIDICTIONS JUDICIAIRES. DIVERS CAS D`ATTRIBUTIONS LÉGALES DE COMPÉTENCE AU PROFIT DES JURIDICTIONS JUDICIAIRES. - DÉCISION PAR LAQUELLE L'AMF REFUSE DE FAIRE DROIT À UNE DEMANDE DE FAIRE USAGE DE SES POUVOIRS D'INJONCTION À L'ENCONTRE D'UN TIERS - AUTEUR DE LA DEMANDE ET TIERS CONCERNÉ N'ÉTANT PAS DES PROFESSIONNELS SOUMIS AU CONTRÔLE DE L'AMF - COMPÉTENCE DU JUGE JUDICIAIRE - 1) RECOURS CONTRE CETTE DÉCISION - INCLUSION - 2) CONCLUSIONS TENDANT À LA RÉPARATION DU PRÉJUDICE SUBI DU FAIT DE CETTE DÉCISION - INCLUSION [RJ1].
</SCT>
<ANA ID="9A"> 13-01-02-01 1) Société ayant demandé à l'Autorité des marchés financiers (AMF) de faire usage des pouvoirs d'injonction qu'elle tient de l'article L. 621-14 du code monétaire et financier (CMF) à l'encontre de certains de ses actionnaires afin qu'ils déposent une offre publique d'acquisition obligatoire visant ses titres.,,,La décision par laquelle le secrétaire général de l'AMF a indiqué à la société qu'il lui appartenait de saisir une autorité de marché étrangère, dont il estime qu'elle est seule compétente pour apprécier s'il y a lieu de déposer une offre publique obligatoire, doit être regardée comme une décision prise au nom de l'AMF par laquelle cette autorité refuse de faire droit à la demande de la société requérante.,,,Ni la société requérante ni les actionnaires en cause n'étant au nombre des personnes et entités mentionnées au II de l'article L. 621-9 du CMF, le litige qui oppose la société à l'AMF, relatif à la légalité de la décision attaquée de l'AMF, relève de la compétence du juge judiciaire.... ,,2) Il en va de même des conclusions tendant à la réparation du préjudice que la société estime avoir subi du fait de cette décision..</ANA>
<ANA ID="9B"> 17-03-01-02-05 1) Société ayant demandé à l'Autorité des marchés financiers (AMF) de faire usage des pouvoirs d'injonction qu'elle tient de l'article L. 621-14 du code monétaire et financier (CMF) à l'encontre de certains de ses actionnaires afin qu'ils déposent une offre publique d'acquisition obligatoire visant ses titres.,,,La décision par laquelle le secrétaire général de l'AMF a indiqué à la société qu'il lui appartenait de saisir une autorité de marché étrangère, dont il estime qu'elle est seule compétente pour apprécier s'il y a lieu de déposer une offre publique obligatoire, doit être regardée comme une décision prise au nom de l'AMF par laquelle cette autorité refuse de faire droit à la demande de la société requérante.,,,Ni la société requérante ni les actionnaires en cause n'étant au nombre des personnes et entités mentionnées au II de l'article L. 621-9 du CMF, le litige qui oppose la société à l'AMF, relatif à la légalité de la décision attaquée de l'AMF, relève de la compétence du juge judiciaire.... ,,2) Il en va de même des conclusions tendant à la réparation du préjudice que la société estime avoir subi du fait de cette décision.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. TC, 22 juin 1992,,, n° 02671, p. 486. Comp., lorsqu'est en cause la responsabilité de l'AMF à raison de son fonctionnement, TC, 2 mai 2011, Société Europe Finance et Industrie c/ Autorité des marchés financiers, n° 3766, p. 685.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
