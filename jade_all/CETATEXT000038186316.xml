<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038186316</ID>
<ANCIEN_ID>JG_L_2019_02_000000425105</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/18/63/CETATEXT000038186316.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 28/02/2019, 425105, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425105</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Bertrand Mathieu</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:425105.20190228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 29 octobre 2018, 6 décembre 2018 et 7 février 2019 au secrétariat du contentieux du Conseil d'Etat, M. A...B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 2 août 2018 accordant son extradition aux autorités monténégrines ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement à la SCP David Gaschignard de la somme de 2 500 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne d'extradition du 13 décembre 1957 ; <br/>
              - le code des relations entre le public et l'administration ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bertrand Mathieu, conseiller d'Etat en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de M. B...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	M. A...B...conteste le décret du 2 août 2018 par lequel le Premier ministre a accordé aux autorités monténégrines son extradition pour l'exécution d'une peine de trois ans et six mois d'emprisonnement infligée par un jugement du tribunal de grande instance de Bijole Poljie du 10 juillet 2017 pour des faits qualifiés de production non autorisée, détention et mise en circulation de stupéfiants. <br/>
<br/>
              2.	En premier lieu, le décret attaqué comporte l'énoncé des considérations de fait et de droit qui en constituent le fondement et satisfait ainsi à l'exigence de motivation prévue à l'article L. 211-2 du code des relations entre le public et l'administration.<br/>
<br/>
              3.	En deuxième lieu, aux termes du 2 de l'article 12 de la convention européenne d'extradition du 13 décembre 1957 : " Il sera produit à l'appui de la requête : / a) L'original ou l'expédition authentique soit d'une décision de condamnation exécutoire, soit d'un mandat d'arrêt ou de tout autre acte ayant la même force, délivré dans les formes prescrites par la loi de la Partie requérante / b) Un exposé des faits pour lesquels l'extradition est demandée. Le temps et le lieu de leur perpétration, leur qualification légale et les références aux dispositions légales qui leur sont applicables seront indiqués le plus exactement possible ; et / c) Une copie des dispositions légales applicables ou, si cela n'est pas possible, une déclaration sur le droit applicable, ainsi que le signalement aussi précis que possible de l'individu réclamé et tous autres renseignements de nature à déterminer son identité et sa nationalité ". Si M. B...soutient que le décret attaqué aurait fait droit à une demande d'extradition sans que les autorités françaises aient disposé des éléments que l'État requérant devait leur présenter en vertu de ces stipulations, il ressort des pièces du dossier que la demande d'extradition était accompagnée des pièces requises par l'article 12 de la convention. Ainsi, le moyen tiré de la méconnaissance de cet article doit être écarté.<br/>
<br/>
              4.	En troisième lieu, M. B...se prévaut de la réserve formulée par la France à l'article 1er de la convention européenne d'extradition, selon laquelle : " L'extradition pourra être refusée si la remise est susceptible d'avoir des conséquences d'une gravité exceptionnelle pour la personne réclamée, notamment en raison de son âge ou de son état de santé. ". Il soutient que le Gouvernement aurait dû refuser son extradition car l'exécution du décret l'exposerait à des conséquences d'une exceptionnelle gravité en raison des menaces de représailles de la part d'organisations impliquées dans le trafic de stupéfiants qui entretiennent une situation de violence généralisée dans les prisons du Monténégro. Toutefois, les pièces du dossier ne permettent pas d'établir la réalité de ces menaces. Il soutient également que l'exécution du décret l'exposerait à des conséquences d'une exceptionnelle gravité sur sa santé, dès lors qu'il est suivi en consultation pour une affection nécessitant un traitement médicamenteux dont il ne pourrait pas bénéficier en détention au Monténégro. Il ressort des pièces du dossier que M.B..., actuellement placé sous écrou extraditionnel, bénéficie d'un suivi médical à l'occasion duquel un traitement médicamenteux de première intention pour la prise en charge des troubles dépressifs lui a été prescrit. Si, en novembre 2018, après l'intervention du décret litigieux, il a connu un épisode dépressif majeur, un certificat médical en date du 21 janvier 2019 atteste de l'amélioration et de la stabilisation de son état. L'ensemble de ces circonstances ne permettent pas d'établir qu'il serait atteint d'une affection qui ne pourrait pas être prise en charge dans le cadre d'une détention au Monténégro et que l'exécution du décret attaqué serait susceptible d'avoir des conséquences d'une exceptionnelle gravité. Ainsi, le Gouvernement, qui n'était pas tenu de demander des garanties complémentaires au Monténégro, n'a pas commis d'erreur manifeste d'appréciation en ne faisant pas usage de la réserve formulée par la France à l'article 1er précité de la convention européenne d'extradition. <br/>
<br/>
              5.	Il résulte de tout ce qui précède que M. B...n'est pas fondé à demander l'annulation du décret du 2 août 2018 accordant son extradition aux autorités monténégrines. En conséquence, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
