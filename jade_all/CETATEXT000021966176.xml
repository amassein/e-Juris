<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000021966176</ID>
<ANCIEN_ID>JG_L_2010_03_000000307235</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/21/96/61/CETATEXT000021966176.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 12/03/2010, 307235, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2010-03-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>307235</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Stirn</PRESIDENT>
<AVOCATS>SCP PEIGNOT, GARREAU</AVOCATS>
<RAPPORTEUR>Mme Marie-Astrid  Nicolazo de Barmon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olléon Laurent</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2010:307235.20100312</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 6 juillet et 5 octobre 2007 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SOCIETE IMAGIN'ACTION LUXEMBOURG, dont le siège est 51, rue Haute à Luxembourg (L 1718), Luxembourg ; la SOCIETE IMAGIN'ACTION LUXEMBOURG demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt du 24 avril 2007 par lequel la cour administrative d'appel de Versailles a rejeté sa requête tendant à l'annulation du jugement du 18 mai 2004 du tribunal administratif de Versailles rejetant sa demande tendant à la décharge des cotisations d'impôt sur les sociétés et de contribution de 10 % sur cet impôt auxquelles elle a été assujettie au titre des exercices clos en 1995, 1996 et 1997, de la retenue à la source mise à sa charge au titre des années 1996 et 1997 et de la taxe sur la valeur ajoutée qui lui a été réclamée au titre de la période du 1er janvier 1996 au 31 décembre 1997, ainsi que des intérêts de retard et des pénalités correspondantes ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa requête d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention fiscale conclue entre la France et le Grand Duché de Luxembourg tendant à éviter les doubles impositions et à établir des règles d'assistance administrative réciproque en matière d'impôts sur le revenu et sur la fortune, signée le 1er avril 1958, modifiée par l'avenant du 8 septembre 1970 ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Astrid Nicolazo de Barmon, Auditeur,  <br/>
<br/>
              - les observations de la SCP Peignot, Garreau, avocat de la SOCIETE IMAGIN'ACTION LUXEMBOURG, <br/>
<br/>
              - les conclusions de M. Laurent Olléon, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Peignot, Garreau, avocat de la SOCIETE IMAGIN'ACTION LUXEMBOURG ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SOCIETE IMAGIN'ACTION LUXEMBOURG, dont le siège social est au Luxembourg, exerce une activité de commerce de gros et d'exportation de parfums, de produits cosmétiques et d'accessoires de mode ; qu'à l'issue d'une vérification de comptabilité portant sur la période du 1er janvier 1995 au 31 décembre 1997, l'administration fiscale a estimé que cette société disposait en France d'un établissement stable, situé dans les locaux de la SARL Imagin'Action France sis 10, côte de la Jonchère à Bougival (Yvelines) ; qu'invitée à souscrire des déclarations de résultats et de chiffres d'affaires, la société a adressé des documents portant la mention "néant" ; que, par notifications de redressements en date des 23 décembre 1998 et 22 décembre 1999 portant respectivement sur l'année 1995 et sur les années 1996 et 1997, l'administration a reconstitué le chiffre d'affaires et le bénéfice imposable suivant la procédure contradictoire de redressements ; qu'elle a soumis le résultat imposable dégagé par cet établissement à l'impôt sur les sociétés et à la contribution de 10 % sur cet impôt prévue à l'article 235 ter ZA du code général des impôts au titre des exercices clos en 1995, 1996 et 1997 ; qu'à la suite d'une erreur dont était entaché un premier avis de mise en recouvrement en date du 12 novembre 2001 et compte tenu des règles relatives à la prescription, elle a, par avis de mise en recouvrement du 10 décembre 2002, assujetti les ventes réalisées par cet établissement à la taxe sur la valeur ajoutée au titre de la seule période comprise entre le 1er janvier 1996 et le 31 décembre 1997 et a appliqué aux bénéfices réalisés en 1996 et 1997 une retenue à la source au taux de 5 % en application des dispositions des articles 115 quinquies et 119 bis du même code et de l'article 7-1  de la convention fiscale signée le 1er avril 1958 entre la France et le Luxembourg ; que les droits mis à la charge de la société ont été assortis des intérêts de retard et d'une pénalité pour mauvaise foi de 40 %, à l'exception des droits dus au titre de la contribution de 10 % et de la retenue à la source qui ont été assortis d'une majoration de 10 % ; que la SOCIETE IMAGIN'ACTION LUXEMBOURG se pourvoit en cassation contre l'arrêt du 24 avril 2007 par lequel la cour administrative d'appel de Versailles a confirmé le jugement du 18 mai 2004 par lequel le tribunal administratif de Versailles a rejeté ses demandes tendant à la décharge de ces impositions, des intérêts de retard et des pénalités correspondantes ;<br/>
<br/>
              Sur les motifs de l'arrêt attaqué relatifs à l'impôt sur les sociétés, la contribution de 10 % sur cet impôt et la retenue à la source :<br/>
<br/>
              Considérant, en premier lieu, qu'aux termes de l'article 209 du code général des impôts : "I. Sous réserve des dispositions de la présente section, les bénéfices passibles de l'impôt sur les sociétés sont déterminés (...) en tenant compte uniquement des bénéfices réalisés dans les entreprises exploitées en France ainsi que ceux dont l'imposition est attribuée à la France par une convention internationale relative aux doubles impositions (...)" ; qu'aux termes du 1 de l'article 4 de la convention fiscale franco-luxembourgeoise précitée : "Les revenus des entreprises (...) commerciales (...) ne sont imposables que dans l'Etat sur le territoire duquel se trouve un établissement stable" ; qu'aux termes du 3 de l'article 2 de la même convention : "1) Le terme "établissement stable" désigne une installation fixe d'affaires dans laquelle l'entreprise exerce tout ou partie de son activité. / 2) Au nombre des établissements stables figurent notamment : (...) / b) les succursales ; c) les bureaux (...)" ; qu'aux termes de l'article 7 de la même convention : "1) Une société qui a son domicile fiscal au Luxembourg et qui a en France un établissement stable au sens du 3) de l'article 2, est soumise en France à la retenue à la source dans les conditions prévues par la législation interne française, étant toutefois entendu que le taux applicable est de 5 %" ; <br/>
<br/>
              Considérant que la cour a relevé que la procédure de visite et la saisie de pièces effectuées par l'administration sur le fondement de l'article L. 16 B du livre des procédures fiscales avaient révélé que cette société utilisait les locaux de la SARL Imagin'Action France situés dans la commune de Bougival et à partir desquels elle développait la plus grande partie de son activité d'importation et d'exportation de parfums, de produits cosmétiques et de bijoux fantaisie, que l'administration fiscale avait saisi plus de trois cents exemplaires de documents à en-tête de la société requérante, dont des factures adressées à des clients étrangers, des correspondances à des fournisseurs russes ou ukrainiens, ainsi que des balances clients et des extraits de compte clients et qu'une salariée de la SARL Imagin'Action France avait signé de nombreux documents émanant de la société requérante, avait été destinataire de correspondances adressées à cette dernière et avait donné des ordres de livraison et d'enlèvement de marchandises ; qu'elle a également relevé que la SOCIETE IMAGIN'ACTION LUXEMBOURG possédait plusieurs comptes bancaires en France ayant fait l'objet de nombreux mouvements tout au long de la période vérifiée et qu'elle n'avait employé au Luxembourg pendant la période vérifiée qu'un comptable à mi-temps en 1997 ; que, contrairement à ce que la société requérante soutient, la cour n'a pas omis de se prononcer sur le moyen tiré de ce que la société française, dans laquelle elle détenait une participation, agissait pour son compte dans le cadre d'un contrat de prestations de services et moyennant rémunération ; qu'en effet la cour a expressément rejeté ce moyen au motif que la société ne justifiait ni de l'existence de cette convention ni d'une comptabilisation de la rémunération ; qu'en déduisant des faits qu'elle a ainsi souverainement relevés que la SOCIETE IMAGIN'ACTION LUXEMBOURG avait une installation fixe d'affaires dans les locaux situés à Bougival au sens des stipulations précitées de l'article 4 de la convention fiscale franco-luxembourgeoise et que cette seule circonstance suffisait à caractériser l'existence d'un établissement stable localisé en France, de sorte que l'administration avait à bon droit estimé que les revenus provenant de cette activité étaient imposables à l'impôt sur les sociétés en France, et qu'elle avait pu appliquer la retenue à la source aux bénéfices réalisés par l'établissement stable en France en application du 2 de l'article 119 bis du code général des impôts et des stipulations de l'article 7-1 de cette convention, la cour, qui n'avait pas à rechercher si la salariée de la SARL Imagin'Action France agissait en tant qu'agent commercial indépendant ou si l'activité exercée depuis la commune de Bougival l'était dans des conditions d'autonomie suffisante, et notamment si elle se traduisait par la négociation et la conclusion de contrats commerciaux au nom de la société luxembourgeoise, n'a pas commis d'erreur de droit au regard des stipulations précitées de la convention ni entaché son arrêt d'insuffisance de motivation ou d'erreur de qualification juridique ; <br/>
<br/>
              Considérant, en second lieu, que la cour a d'abord jugé que la société supportait la charge de la preuve de l'exagération des impositions en vertu de l'article L. 192 du livre des procédures fiscales dès lors qu'elle n'avait présenté aucune comptabilité lors du contrôle et que l'administration avait suivi l'avis de la commission départementale des impôts directs et des taxes sur le chiffre d'affaires ; que la cour a, ensuite, d'une part, rejeté le moyen tiré de ce que le vérificateur aurait omis de soustraire des résultats imposables de la société, reconstitués à partir des crédits portés sur les comptes bancaires ouverts à son nom, des sommes correspondant à des virements de compte à compte, en jugeant que les pièces produites devant les juges du fond par la SOCIETE IMAGIN'ACTION LUXEMBOURG ne justifiaient ni de l'origine ni de la nature de ces sommes qui devaient dès lors être regardées comme des recettes soumises à l'impôt ; que, d'autre part, la cour, après avoir constaté que l'administration avait, en l'absence de comptabilité et de toute pièce justificative des charges de l'entreprise, retenu pour les trois exercices en litige des pourcentages forfaitaires des produits hors taxes reconstitués afin de prendre en compte les charges de la société, a jugé que si la société requérante faisait valoir qu'auraient également dû être pris en compte des frais généraux portés dans la comptabilité tenue au Luxembourg, tels qu'ils figurent sur des balances générales et des comptes de résultat, elle n'établissait pas la part de ces frais engagés au soutien de l'activité exercée en France ; qu'en statuant ainsi, la cour, qui a suffisamment motivé son arrêt, a porté sur les pièces du dossier qui lui était soumis une appréciation souveraine exempte de dénaturation et n'a pas méconnu l'article L. 192 du livre des procédures fiscales ;<br/>
<br/>
              Sur les motifs de l'arrêt attaqué relatifs à la taxe sur la valeur ajoutée :<br/>
<br/>
              Considérant, en premier lieu, qu'aux termes de l'article R. 256-1 du livre des procédures fiscales : "L'avis de mise en recouvrement prévu à l'article L. 256 indique pour chaque impôt ou taxe le montant global des droits, des pénalités et des intérêts de retard qui font l'objet de cet avis. / Lorsque l'avis de mise en recouvrement est consécutif à une procédure de redressement contradictoire, il fait référence soit à la notification prévue à l'article L. 57 et, le cas échéant, aux différentes pièces de procédure adressées par le service informant le contribuable d'une modification des rehaussements, soit au document adressé au contribuable qui comporte l'information prévue au premier alinéa de l'article L. 48" ; <br/>
<br/>
              Considérant, d'une part, que, contrairement à ce que soutient la société requérante, la cour n'a pas commis d'erreur de droit en se fondant sur les dispositions précitées de l'article R. 256-1 du livre des procédures fiscales dont la rédaction était celle applicable à la date de l'avis de mise en recouvrement le 10 décembre 2002 ; <br/>
<br/>
              Considérant, d'autre part, qu'en jugeant que, si cet avis mentionnait à tort une notification de redressement en date du 31 décembre 1999 alors que celle-ci est datée du 22 décembre 1999 et une période d'imposition correspondant aux trois années 1995, 1996 et 1997 alors que la taxe sur la valeur ajoutée mise à la charge de la société ne portait que sur la période allant du 1er janvier 1996 au 31 décembre 1997, ces erreurs matérielles étaient sans incidence sur le bien-fondé de cette imposition au motif qu'elles n'avaient pu induire en erreur la société sur le montant ou l'origine des droits, intérêts de retard et pénalités mis en recouvrement et en en déduisant que la société n'était pas fondée à soutenir que ces erreurs constituaient des erreurs substantielles devant conduire à la décharge des droits en litige, la cour n'a pas méconnu les dispositions précitées de l'article R. 256-1 du livre des procédures fiscales, dès lors que cet avis comportait le montant des droits, intérêts de retard et pénalités mis en recouvrement  et qu'il faisait expressément référence à la notification de redressement portant la mention exacte de la période d'imposition concernée ; <br/>
<br/>
              Considérant, enfin, que le moyen invoqué par la société requérante et tiré de ce que la cour aurait méconnu les dispositions précitées de l'article L. 80 CA du livre des procédures fiscales est nouveau en cassation et à ce titre, dès lors qu'il n'est pas d'ordre public, irrecevable ;<br/>
<br/>
              Considérant, en deuxième lieu et d'une part, que le moyen invoqué par la SOCIETE IMAGIN'ACTION LUXEMBOURG et tiré de ce que la cour aurait commis une erreur de droit au regard de l'article 259 du code général des impôts en retenant comme critère d'assujettissement à la taxe sur la valeur ajoutée le critère de l'établissement stable correspondant aux prestations de services et non aux livraisons de biens, alors que son activité relevant de la livraison de biens, il lui appartenait de s'interroger sur le lieu de départ des livraisons, est nouveau en cassation, ainsi que le soutient le ministre en défense, et à ce titre, dès lors qu'il n'est pas d'ordre public, irrecevable ; <br/>
<br/>
              Considérant, d'autre part, qu'en déduisant des éléments de fait mentionnés ci-dessus, qui révélaient la disposition personnelle et permanente d'une installation comportant les moyens humains et techniques nécessaires à son activité en France, que la société avait réalisé des opérations imposables en France par l'intermédiaire de son établissement stable situé dans les locaux sis dans la commune de Bougival, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              Considérant, par ailleurs, que la société critique les motifs retenus par la cour pour rejeter sa contestation du montant du chiffre d'affaires reconstitué par l'administration en reprenant les mêmes moyens que ceux présentés en matière d'impôt sur les sociétés ; qu'il y a lieu, pour les motifs énoncés précédemment, d'écarter ces moyens ; <br/>
<br/>
              Considérant, enfin, qu'aux termes du II de l'article 271 du code général des impôts : "1. (...) la taxe dont les redevables peuvent opérer la déduction est (...) / a) celle qui figure sur les factures d'achat qui leur sont délivrées par leurs vendeurs dans la mesure où ces derniers étaient légalement autorisés à la faire figurer sur lesdites factures. (...) 2. La déduction ne peut être opérée si les redevables ne sont pas en possession (...) desdites factures" ; que la cour n'a pas commis d'erreur de droit ni dénaturé les pièces du dossier en jugeant que la SOCIETE IMAGIN'ACTION LUXEMBOURG n'était pas fondée à demander la réduction de la taxe sur la valeur ajoutée mise à sa charge, aux motifs que, contrairement à ce qu'elle prétend, elle n'avait produit devant le juge aucune facture d'achat et que, si elle avait communiqué les copies des balances générales de sa comptabilité tenue au Luxembourg, les éléments chiffrés relatifs au montant des achats de marchandises et aux charges externes qu'elle invoquait concernaient tant son activité déployée au Luxembourg que celle déployée en France sans qu'il soit possible de procéder à l'individualisation des charges ;<br/>
<br/>
              Sur les motifs de l'arrêt relatifs à l'existence alléguée d'une double imposition :<br/>
<br/>
              Considérant qu'aux termes de l'article 24 de la convention fiscale franco-luxembourgeoise : "Tout contribuable qui prouve que les mesures prises par les autorités fiscales des deux Etats contractants ont entraîné pour lui une double imposition en ce qui concerne les impôts visés par la présente convention, peut adresser une demande soit aux autorités compétentes de l'Etat sur le territoire duquel il a son domicile fiscal, soit à celles de l'autre Etat. Si le bien fondé de cette demande est reconnu, les autorités compétentes des deux Etats s'entendent pour éviter de façon équitable la double imposition." ; qu'il résulte de ces stipulations qu'il appartient au contribuable, au soutien de sa demande d'ouverture d'une procédure d'accord amiable entre les deux Etats contractants, d'établir que les mesures prises par les autorités fiscales de ces deux Etats ont entraîné pour lui une situation de double imposition et que la demande peut être rejetée par les autorités fiscales de l'un ou l'autre des deux Etats si son bien-fondé n'est pas reconnu ;<br/>
<br/>
              Considérant qu'en relevant que la comptabilité de la société tenue au Luxembourg retraçait son activité tant au Luxembourg qu'en France, la cour n'a nullement reconnu que les redressements résultant de son imposition en France auraient entraîné une double imposition pour la société ; qu'en jugeant que les autorités fiscales luxembourgeoises avaient refusé d'ouvrir une procédure amiable au motif que les stipulations précitées de l'article 24 de la convention ne pouvaient être utilisées pour éliminer une double imposition dans des situations abusives et que, dans ces conditions, la SOCIETE IMAGIN'ACTION LUXEMBOURG, qui n'établissait pas qu'elle avait subi une double imposition, n'était pas fondée à demander la décharge des impositions en litige, la cour, qui n'avait pas à répondre au moyen inopérant tiré de ce qu'aucune décision des autorités fiscales françaises n'était intervenue, n'a pas commis d'erreur de droit au regard des stipulations de l'article 24 de la convention ni entaché son arrêt d'insuffisance de motivation ;<br/>
<br/>
              Sur les motifs de l'arrêt relatifs à la pénalité de 40 % :<br/>
<br/>
              Considérant qu'aux termes de l'article 1727 du code général des impôts : "Le défaut ou l'insuffisance dans le paiement ou le versement tardif de l'un des impôts, droits, taxes, redevances ou sommes établis ou recouvrés par la direction générale des impôts donnent lieu au versement d'un intérêt de retard qui est dû indépendamment de toutes sanctions. (...)" ; qu'aux termes de l'article 1728 du même code : "1. Lorsqu'une personne physique ou morale ou une association tenue de souscrire une déclaration ou de présenter un acte comportant l'indication d'éléments à retenir pour l'assiette ou la liquidation de l'un des impôts, droits, taxes, redevances ou sommes établis ou recouvrés par la direction générale des impôts s'abstient de souscrire cette déclaration ou de présenter cet acte dans les délais, le montant des droits mis à la charge du contribuable ou résultant de la déclaration ou de l'acte déposé tardivement est assorti de l'intérêt de retard visé à l'article 1727 et d'une majoration de 10 %. (...) / 3. La majoration visée au 1 est portée à : 40 % lorsque le document n'a pas été déposé dans les trente jours suivant la réception d'une mise en demeure notifiée par pli recommandé d'avoir à le produire dans ce délai ; 80 % lorsque le document n'a pas été déposé dans les trente jours suivant la réception d'une deuxième mise en demeure notifiée dans les mêmes formes que la première (...)" ; qu'aux termes de l'article 1729 du même code : "1. Lorsque la déclaration ou l'acte mentionnés à l'article 1728 font apparaître une base d'imposition ou des éléments servant à la liquidation de l'impôt insuffisants, inexacts ou incomplets, le montant des droits mis à la charge du contribuable est assorti de l'intérêt de retard visé à l'article 1727 et d'une majoration de 40 % si la mauvaise foi de l'intéressé est établie ou de 80% s'il s'est rendu coupable de manoeuvres frauduleuses ou d'abus de droits au sens de l'article L. 64 du livre des procédures fiscales (...)" ; <br/>
<br/>
              Considérant qu'en réponse au moyen soulevé devant elle par lequel la SOCIETE IMAGIN'ACTION LUXEMBOURG se bornait à contester l'intention délibérée d'éluder l'impôt retenue par l'administration pour justifier la pénalité de 40 % pour mauvaise foi qui a majoré les droits mis à sa charge au titre de l'impôt sur les sociétés et de la taxe sur la valeur ajoutée, la cour, après avoir rappelé les dispositions précitées des articles 1727, 1728 et 1729 du code général des impôts et jugé que, lorsqu'un contribuable n'avait pas satisfait, dans les délais légaux, aux obligations déclaratives auxquelles il était tenu, l'ensemble des droits dus à ce titre, qu'ils fassent ou non l'objet d'une déclaration ultérieure, pouvait être soumis, outre l'intérêt de retard, aux majorations prévues par l'article 1728 du code et que, par ailleurs, les droits correspondant aux insuffisances, inexactitudes ou omissions afférentes aux déclarations produites tardivement pouvaient également être soumis, le cas échéant, aux majorations prévues par l'article 1729 du même code, a estimé que l'administration, en invoquant les carences déclaratives et comptables graves et répétées de la société, qui ne pouvait ignorer qu'elle disposait d'un établissement stable en France où ses opérations étaient imposables, devait être regardée comme ayant apporté la preuve qui lui incombe de la mauvaise foi de la société ; qu'en statuant ainsi, la cour n'a pas omis de préciser le fondement sur lequel l'administration avait infligé cette pénalité à la société et n'a pas commis d'erreur de droit au regard des articles 1728 et 1729 du code général des impôts ni entaché son arrêt d'une contradiction de motifs ; <br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que la SOCIETE IMAGIN'ACTION LUXEMBOURG n'est pas fondée à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que ces dispositions font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante, la somme demandée par la SOCIETE IMAGIN'ACTION LUXEMBOURG au titre des frais exposés par elle et non compris dans les dépens ;	<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Le pourvoi de la SOCIETE IMAGIN'ACTION LUXEMBOURG est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la SOCIETE IMAGIN'ACTION LUXEMBOURG et au ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
