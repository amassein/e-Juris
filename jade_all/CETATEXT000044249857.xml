<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044249857</ID>
<ANCIEN_ID>JG_L_2021_10_000000451496</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/24/98/CETATEXT000044249857.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 25/10/2021, 451496, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>451496</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI, REBEYROL</AVOCATS>
<RAPPORTEUR>M. Alexis Goin</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:451496.20211025</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Société de travaux publics forestiers et agricoles (STPFA) a demandé au tribunal administratif de Marseille, d'une part, avant-dire-droit, d'ordonner une expertise, d'autre part, de dresser le décompte général définitif du marché de travaux conclu le 12 novembre 2011, dont le lot n° 1 " Terrassement généraux - VRD ", et de condamner la commune de Ceyreste à lui verser la somme de 162 505,63 euros toutes taxes comprises pour les travaux supplémentaires exécutés dans le cadre de ce marché. Par un jugement n° 1502689 du 21 septembre 2017, le tribunal administratif de Marseille a condamné la commune de Ceyreste à lui verser la somme de 6 612 euros toutes taxes comprises et rejeté le surplus des conclusions.<br/>
<br/>
              Par un arrêt n° 17MA04536 du 8 février 2021, la cour administrative d'appel de Marseille a, sur appel de la STPFA, réformé l'article 1er de ce jugement en portant de 6 612 euros toutes taxes comprises à 71 229,60 euros toutes taxes comprises la somme que la commune de Ceyreste est condamnée à payer à la société et rejeté le surplus des conclusions d'appel de la société et l'appel incident formé par la commune contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 7 avril et 6 juillet 2021 au secrétariat du contentieux du Conseil d'Etat, la STFPA demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il a rejeté le surplus des conclusions de sa requête d'appel ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Ceyreste la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des marchés publics ;<br/>
              - la loi n° 75-1334 du 31 décembre 1975 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alexis Goin, auditeur,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Gatineau, Fattaccini, Rebeyrol, avocat de la société de travaux publics forestiers et agricoles STPFA ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              2. Pour demander l'annulation de l'arrêt qu'elle attaque, la STPFA soutient que la cour administrative d'appel de Marseille a :<br/>
              - insuffisamment motivé son arrêt en se bornant à reproduire aux points 8 et 9 la même motivation ;<br/>
              - omis de statuer sur la demande tendant à ce que la commune de Ceyreste, maître d'ouvrage, soit condamnée à l'indemniser du préjudice subi en raison des fautes quasi-délictuelles qu'elle a commises en manquant à ses obligations légales relatives à l'acceptation de sous-traitant ;  <br/>
              - commis une erreur de droit en rejetant la demande de paiement des prestations qu'elle a réalisées au motif que celles-ci avaient été payées aux entreprises titulaires du lot alors que le maître de l'ouvrage n'avait pas mis en demeure ces entreprises de régulariser l'acte de sous-traitance ;<br/>
              - inexactement qualifié les faits et dénaturé les faits et pièces du dossier en jugeant qu'elle n'alléguait pas que le maître d'ouvrage aurait commis une faute dans la direction du marché ; <br/>
              - inexactement qualifié les faits et dénaturé les faits et pièces du dossier en jugeant que les travaux réalisés à la suite de la défaillance des entreprises titulaires des lots n° 2 " Gros œuvre " et n° 12 " Équipements et sols sportifs " ne présentaient pas un caractère indispensable ; <br/>
              - dénaturé les pièces du dossier en estimant que les travaux litigieux relevant contractuellement des lots n° 2 et n° 12 n'avaient pas été demandés par le maître d'œuvre et le maître d'ouvrage ;<br/>
              - insuffisamment motivé son arrêt et dénaturé les faits et pièces du dossier en estimant que, sans qu'il soit besoin d'ordonner une expertise, le décompte produit ne suffisait pas à démontrer les surcoûts invoqués du fait du recours à la technique du fraisage.<br/>
<br/>
              3 Eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi qui sont dirigées contre l'arrêt attaqué en tant qu'il s'est prononcé sur le règlement des prestations que la société requérante soutient avoir effectuées en lieu et place des entreprises titulaires du lot n° 2 " Gros œuvre " et du lot n° 12 " Equipements et sols sportifs ". En revanche, s'agissant des autres conclusions du pourvoi, aucun des moyens soulevés n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions du pourvoi de la STPFA qui sont dirigées contre l'arrêt attaqué en tant qu'il s'est prononcé sur le règlement des prestations que la société requérante soutient avoir effectuées en lieu et place des entreprises titulaires du lot n° 2 " Gros œuvre " et du lot n° 12 " Equipements et sols sportifs " sont admises.<br/>
Article 2 : Le surplus des conclusions du pourvoi de la STPFA n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à la Société de travaux publics forestiers et agricoles.<br/>
Copie en sera adressée à la commune de Ceyreste.<br/>
              Délibéré à l'issue de la séance du 7 octobre 2021 où siégeaient : M. Gilles Pellissier, assesseur, présidant ; M. Mathieu Herondart, conseiller d'Etat et M. Alexis Goin, auditeur-rapporteur. <br/>
<br/>
              Rendu le 25 octobre 2021.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Gilles Pellissier<br/>
 		Le rapporteur : <br/>
      Signé : M. Alexis Goin<br/>
                 La secrétaire :<br/>
                 Signé : Mme A... B...<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
