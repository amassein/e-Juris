<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036411929</ID>
<ANCIEN_ID>JG_L_2017_12_000000415038</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/41/19/CETATEXT000036411929.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 28/12/2017, 415038, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415038</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Louise Bréhier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:415038.20171228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. A...B..., à l'appui de sa demande tendant à l'annulation pour excès de pouvoir de l'arrêté du préfet de la Haute-Vienne du 23 septembre 2016 par lequel il a été assigné à résidence dans la commune de Saint-Junien, astreint à se présenter quatre fois par jour à la brigade de gendarmerie et à demeurer tous les jours, de 21 heures à 7 heures dans les locaux où il réside, qui lui interdit de se déplacer en dehors de son lieu d'assignation à résidence sans autorisation écrite préalable du préfet de la Haute-Vienne et lui fait obligation d'entreprendre toutes démarches utiles en vue de son admission éventuelle dans un pays d'accueil de son choix, a produit un mémoire, enregistré le 5 septembre 2017 au greffe du tribunal administratif de Limoges, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lequel il soulève une question prioritaire de constitutionnalité mettant en cause la conformité aux droits et libertés garantis par la Constitution des mots " ni à ceux mentionnés aux articles L. 523-3 à L. 523-5 du présent code " figurant à la dernière phrase du huitième alinéa et les deux premières phrases du neuvième alinéa de l'article L. 561-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction résultant de la loi n° 2015-925 du 29 juillet 2015. <br/>
<br/>
              Par une ordonnance n° 1601523 du 13 octobre 2017, enregistrée le 16 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de Limoges, avant qu'il soit statué sur la demande de M.B..., a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question prioritaire de constitutionnalité ainsi soulevée. <br/>
<br/>
              Par la question prioritaire de constitutionnalité transmise, par un mémoire et un mémoire en réplique, enregistrés les 20 novembre et 16 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, M. B...soutient que les mots " ni à ceux mentionnés aux articles L. 523-3 à L. 523-5 du présent code " figurant à la dernière phrase du huitième alinéa et les deux premières phrases du neuvième alinéa de l'article L. 561-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction issue de la loi n° 2015-925 du 29 juillet 2015, applicables au litige, méconnaissent les articles 2 et 4 de la Déclaration des droits de l'homme et du citoyen et l'article 66 de la Constitution, ainsi que la liberté d'aller et venir, l'interdiction de la détention arbitraire et le principe de légalité des délits et des peines. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu : <br/>
              -   la Constitution, notamment son article 61-1 ; <br/>
              -   l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              -   le code de l'entrée et du séjour des étrangers et du droit d'asile, notamment son article L. 561-1 dans sa rédaction résultant de la loi n° 2015-925 du 29 juillet 2015 ; <br/>
              - la décision du Conseil constitutionnel n° 2017-674 QPC du 1er décembre 2017 ; <br/>
              -   le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Louise Bréhier, auditrice,  <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2.	Considérant que, sur le fondement de ces dispositions, M. B...demande, à l'appui du recours pour excès de pouvoir qu'il a formé contre l'arrêté du 23 septembre 2016 par lequel le préfet de la Haute-Vienne l'a assigné à résidence dans la commune de Saint-Junien, que soit renvoyée au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des mots " ni à ceux mentionnés aux articles L. 523-3 à L. 523-5 du présent code " figurant à la dernière phrase du huitième alinéa et des deux premières phrases du neuvième alinéa de l'article L. 561-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans leur rédaction résultant de la loi n° 2015-925 du 29 juillet 2015 ;  <br/>
<br/>
              3.	Considérant que selon l'article L. 523-3 du code de l'entrée et du séjour des étrangers et du droit d'asile, l'étranger " qui fait l'objet d'un arrêté d'expulsion et qui justifie être dans l'impossibilité de quitter le territoire français en établissant qu'il ne peut ni regagner son pays d'origine ni se rendre dans aucun autre pays peut faire l'objet d'une mesure d'assignation à résidence dans les conditions prévues à l'article L. 561-1 " ; qu'en vertu du premier alinéa de l'article L. 561-1 du même code : " Lorsque l'étranger justifie être dans l'impossibilité de quitter le territoire français ou ne peut ni regagner son pays d'origine ni se rendre dans aucun autre pays, l'autorité administrative peut, jusqu'à ce qu'existe une perspective raisonnable d'exécution de son obligation, l'autoriser à se maintenir provisoirement sur le territoire français en l'assignant à résidence " ; qu'aux termes des huitième et neuvième alinéas de l'article L. 561-1 du même code, dans sa rédaction résultant notamment de la loi n° 2015-925 du 29 juillet 2015, applicable au litige : " La décision d'assignation à résidence est motivée. Elle peut être prise pour une durée maximale de six mois, et renouvelée une fois ou plus dans la même limite de durée, par une décision également motivée. Par exception, cette durée ne s'applique ni aux cas mentionnés au 5° du présent article ni à ceux mentionnés aux articles L. 523-3 à L. 523-5 du présent code. / L'étranger astreint à résider dans les lieux qui lui sont fixés par l'autorité administrative doit se présenter périodiquement aux services de police ou aux unités de gendarmerie. L'étranger qui fait l'objet d'un arrêté d'expulsion ou d'une interdiction judiciaire ou administrative du territoire prononcés en tout point du territoire de la République peut, quel que soit l'endroit où il se trouve, être astreint à résider dans des lieux choisis par l'autorité administrative dans l'ensemble du territoire de la République. L'autorité administrative peut prescrire à l'étranger la remise de son passeport ou de tout document justificatif de son identité dans les conditions prévues à l'article L. 611-2. Si l'étranger présente une menace d'une particulière gravité pour l'ordre public, l'autorité administrative peut le faire conduire par les services de police ou de gendarmerie jusqu'aux lieux d'assignation " ; <br/>
<br/>
              4.	Considérant que le Conseil constitutionnel a, dans les motifs et le dispositif de sa décision n° 2017-674 QPC du 1er décembre 2017, déclaré conformes à la Constitution les dispositions de la dernière phrase du huitième alinéa, à l'exception des mots " au 5° du présent article ", ainsi que la troisième phrase du neuvième alinéa de l'article L. 561-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans la rédaction de cet article qui résulte de la loi du 7 mars 2016, sous les réserves d'interprétation mentionnées respectivement aux points 11 et 15 et aux points 12 et 15 de sa décision ; que, selon les réserves énoncées aux points 11 et 12, il appartient à l'autorité administrative de retenir des conditions et des lieux d'assignation à résidence tenant compte, dans la contrainte qu'ils imposent à l'intéressé, du temps passé sous ce régime et des liens familiaux et personnels noués par ce dernier ; que, selon la réserve énoncée au point 15, si la mesure d'assignation à résidence est susceptible d'inclure une astreinte à domicile, la plage horaire de cette dernière ne saurait dépasser douze heures par jour sans que l'assignation à résidence soit alors regardée comme une mesure privative de liberté, contraire aux exigences de l'article 66 de la Constitution dans la mesure où elle n'est pas soumise au contrôle du juge judiciaire ; <br/>
<br/>
              5.	Considérant, en premier lieu, que la dernière phrase du huitième alinéa de l'article L. 561-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction résultant de la loi du 29 juillet 2015, est similaire dans sa substance et dans sa rédaction à la dernière phrase du huitième alinéa de cet article, dans sa rédaction résultant de la loi du 7 mars 2016, sur laquelle le Conseil constitutionnel s'est prononcé par sa décision du 1er décembre 2017 ; qu'il y a lieu, dès lors, de regarder la dernière phrase du huitième alinéa de l'article L. 561-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction résultant de la loi du 29 juillet 2015, à l'exception des mots " au 5° du présent article " qui ne sont pas applicables au présent litige, comme conforme à la Constitution sous les réserves énoncées par le Conseil constitutionnel ;<br/>
<br/>
              6.	Considérant, en deuxième lieu, que les obligations de se présenter périodiquement aux services de police ou aux unités de gendarmerie, susceptibles d'être imparties par l'autorité administrative en vertu de la première phrase du neuvième alinéa de l'article L. 561-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction applicable au présent litige, doivent être adaptées, nécessaires et proportionnées aux finalités qu'elles poursuivent et ne sauraient, sous le contrôle du juge administratif, porter une atteinte disproportionnée à la liberté d'aller et venir ; que ces obligations, distinctes d'une astreinte à domicile dont la durée ne saurait excéder douze heures par jour, ne peuvent être regardées comme une mesure privative de liberté soumise aux exigences de l'article 66 de la Constitution ; que, présentant le caractère d'une mesure de police et non d'une punition, le principe de légalité des délits et des peines ne peut être utilement invoqué à leur encontre ; qu'il s'ensuit que la question de constitutionnalité soulevée à l'encontre de cette phrase ne présente pas de caractère sérieux ; que, dès lors, le moyen tiré de ce que la première phrase du neuvième alinéa de l'article L. 561-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction résultant de la loi du 29 juillet 2015, porte atteinte aux droits et libertés que la Constitution garantit doit être écarté ; <br/>
<br/>
              7.	Considérant, en troisième lieu, que si la deuxième phrase du neuvième alinéa de l'article L. 561-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction résultant de la loi du 29 juillet 2015 est devenue la troisième phrase de cet alinéa dans sa rédaction issue de la loi du 7 mars 2016, cette phrase n'a pas été modifiée par la loi du 7 mars 2016 ; qu'elle a été déclarée conforme à la Constitution sous réserves par les motifs et le dispositif de la décision du Conseil constitutionnel du 1er décembre 2017 ; qu'en l'absence de changement de circonstances, les dispositions de cette phrase ne peuvent, dès lors, être à nouveau renvoyées au Conseil constitutionnel ; <br/>
<br/>
              8.	Considérant qu'il résulte de ce qui précède qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. B... ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M.B....<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et au ministre d'Etat, ministre de l'intérieur.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre et au tribunal administratif de Limoges. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
