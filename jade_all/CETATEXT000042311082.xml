<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042311082</ID>
<ANCIEN_ID>JG_L_2020_08_000000443073</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/31/10/CETATEXT000042311082.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 24/08/2020, 443073, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-08-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>443073</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:443073.20200824</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 20 août 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... C... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner la suspension de l'exécution de toutes les dispositions nationales, régionales et locales portant obligation du port du masque dans le cadre de la lutte contre l'épidémie de covid-19, de limiter les consignes relatives au port du masque à une simple recommandation et d'informer la population de ses effets dangereux.<br/>
<br/>
<br/>
<br/>
              Il soutient que l'obligation de port du masque n'est pas justifiée, dès lors qu'elle n'est pas efficace pour lutter contre la propagation du virus covid-19 et peut avoir des effets dangereux, qu'elle trompe le public en lui donnant l'illusion de bénéficier d'une protection efficace, qu'elle risque de dissuader de nombreuses personnes de se rendre dans des établissements de soins et qu'elle met en péril la reprise économique en raison du caractère dissuasif de son utilisation dans les lieux publics.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2020-856 du 9 juillet 2020 ;<br/>
              - le décret n° 2020-860 du 10 juillet 2020 modifié ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". Selon le premier alinéa de l'article R. 522-1 du même code : " La requête visant au prononcé de mesures d'urgence doit contenir l'exposé au moins sommaire des faits et moyens et justifier de l'urgence de l'affaire ". Enfin, en vertu de l'article L. 522-3 du code précité, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. <br/>
<br/>
              2. Le 2° du I de l'article 1er de la loi du 9 juillet 2020 organisant la sortie de l'état d'urgence sanitaire autorise le Premier ministre, hormis sur les territoires dans lesquels l'article 2 de la même loi proroge l'état d'urgence sanitaire, à compter du 11 juillet 2020 et jusqu'au 30 octobre 2020, par décret pris sur le rapport du ministre chargé de la santé, dans l'intérêt de la santé publique et aux seules fins de lutter contre la propagation de l'épidémie de covid-19, à " réglementer l'ouverture au public, y compris les conditions d'accès et de présence, d'une ou de plusieurs catégories d'établissements recevant du public ainsi que des lieux de réunion, à l'exception des locaux à usage d'habitation, en garantissant l'accès des personnes aux biens et services de première nécessité ". Le III de cet article prévoit que : " Les mesures prescrites en application du présent article sont strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu. Il y est mis fin sans délai lorsqu'elles ne sont plus nécessaires ". Aux termes du IV du même article : " Les mesures prises en application du présent article peuvent faire l'objet, devant le juge administratif, des recours présentés, instruits et jugés selon les procédures prévues aux articles L. 521-1 et<br/>
L. 521-2 du code de justice administrative ". Enfin, en vertu du VII du même article, la violation des mesures prescrites en application du I est punie de l'amende prévue pour les contraventions de la 4ème classe, d'un montant forfaitaire de 135 euros.<br/>
<br/>
              3. Sur le fondement de ces dispositions, le décret du 10 juillet 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans les territoires sortis de l'état d'urgence sanitaire et dans ceux où il a été prorogé a défini au niveau national, à son article 1er, des règles d'hygiène et de distanciation sociale, dites " barrières ", et prévu que, notamment, les rassemblements, réunions et déplacements, ainsi que l'usage des moyens de transports qui n'étaient pas interdits en vertu de ce décret devaient être organisés en veillant au strict respect de ces mesures. S'agissant des établissements et activités, il a, au III de l'article 27 de ce décret, prévu, sans préjudice des autres obligations de port du masque fixées par le même décret, notamment dans les transports, dans les établissements d'enseignement ou d'accueil d'enfants ainsi que dans les établissements sportifs, artistiques ou de loisirs, ainsi que de la possibilité pour l'exploitant de tout type d'établissement recevant du public de l'imposer, que toute personne de onze ans ou plus porte un masque de protection dans les établissements recevant du public de type L, X, PA, CTS, V, Y et S, ainsi que, s'agissant de leurs espaces permettant des regroupements, dans les établissements de type O. Ces catégories correspondent, selon le classement opéré par l'arrêté du 25 juin 1980 visé ci-dessus, aux " Salles d'auditions, de conférences, de réunions, de spectacles ou à usage multiple " (L), aux " Etablissements sportifs couverts " (X), aux " Etablissements de plein air " (PA), aux " Chapiteaux, tentes et structures " (CTS), aux " Etablissements de culte " (V), aux " Musées " (Y), aux "Bibliothèques, centres de documentation " (S) et aux " Hôtels et pensions de famille " (A...). Puis, par un décret du 17 juillet 2020, cette obligation a été étendue aux établissements de catégorie M, c'est-à-dire aux<br/>
" Magasins de vente, centres commerciaux " et W, c'est-à-dire aux " Administrations, banques, bureaux ", à l'exception des bureaux. Le même décret a également inséré à l'article 38 du décret du 10 juillet 2020 un alinéa aux termes duquel " Le port du masque est obligatoire dans les marchés couverts ".<br/>
<br/>
              4. En premier lieu, en tant qu'elles sont dirigées contre toutes les mesures nationales relatives à l'obligation du port du masque, les conclusions de la requête de<br/>
M. C... doivent être regardées comme demandant au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, de suspendre l'exécution des dispositions du décret du 17 juillet 2020 détaillées ci-dessus, ainsi que d'enjoindre au Premier ministre de leur substituer de simples recommandations et d'attirer l'attention du public sur les risques que présenterait, selon lui, le port du masque.<br/>
<br/>
              5. Toutefois, d'une part, sa requête, ne comporte, contrairement aux prescriptions du code de justice administrative rappelées au point 1, aucune indication relative à l'urgence de ces mesures. D'autre part, si M. C... allègue que le port du masque serait inefficace, présenterait un danger pour les personnes concernées et entraînerait des risques indirects, il n'assortit ses propos d'aucun élément sérieux et précis susceptible de caractériser une atteinte grave et manifestement illégale à une liberté fondamentale.<br/>
              6. En deuxième lieu, les conclusions de M. C... tendant à la suspension de l'exécution des dispositions régionales et locales portant obligation du port du masque dans le cadre de la lutte contre l'épidémie de covid-19, dont il ne précise au demeurant ni les auteurs ni la teneur, ne sont, en tout état de cause, pas de celles dont il appartient au Conseil d'Etat de connaître en premier et dernier ressort en vertu des dispositions de l'article R. 311-1 du code de justice administrative. <br/>
<br/>
              7. Il résulte de ce qu'il précède qu'il y a lieu de rejeter la requête de M. C..., selon la procédure prévue à l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. C... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B... C....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
