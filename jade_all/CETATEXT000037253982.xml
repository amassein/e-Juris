<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037253982</ID>
<ANCIEN_ID>JG_L_2018_07_000000412365</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/25/39/CETATEXT000037253982.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 26/07/2018, 412365, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412365</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:412365.20180726</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 11 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, M. B...A...demande au Conseil d'État d'annuler la décision implicite par laquelle le Premier ministre a rejeté sa demande d'abrogation de l'article 1er du décret n° 77-742 du 30 juin 1977 en tant qu'il soumet au droit de préemption du locataire ou de l'occupant de bonne foi prévu à l'article 10 de la loi du 31 décembre 1975 toute première vente d'un appartement et de ses locaux accessoires, depuis la division de l'immeuble dont ils dépendent sans exclure de ce droit de préemption les lots qui, avant une telle première vente, ont fait l'objet d'une transmission à titre gratuit ne portant pas sur l'ensemble de l'immeuble et qui sont loués ou occupés par des personnes dont le bail est postérieur à la division initiale de l'immeuble ainsi qu'aux partages, aux legs particuliers ou aux donations dont ces lots ont été l'objet.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son article 62 ; <br/>
              - la loi n° 75-1351 du 31 décembre 1975 ;<br/>
              - la décision n° 412365 du 6 octobre 2017 par laquelle le Conseil d'Etat statuant au contentieux a renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M.A... ;<br/>
              - la décision du Conseil constitutionnel n° 2017-683 QPC du 9 janvier 2018 statuant sur la question prioritaire de constitutionnalité soulevée par M. A...;<br/>
	- le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
- les conclusions de M. Nicolas Polge, rapporteur public.<br/>
              Vu la note en délibéré, enregistrée le 6 juillet 2018, présentée par le ministre de la cohésion des territoires.<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article 10 de la loi du 31 décembre 1975, dans sa rédaction en vigueur à la date de la décision attaquée : " I- Préalablement à la conclusion de toute vente d'un ou plusieurs locaux à usage d'habitation ou à usage mixte d'habitation et professionnel, consécutive à la division initiale ou à la subdivision de tout ou partie d'un immeuble par lots, le bailleur doit, à peine de nullité de la vente, faire connaître par lettre recommandée avec demande d'avis de réception, à chacun des locataires ou occupants de bonne foi, l'indication du prix et des conditions de la vente projetée pour le local qu'il occupe. Cette notification vaut offre de vente au profit de son destinataire. / L'offre est valable pendant une durée de deux mois à compter de sa réception. Le locataire qui accepte l'offre ainsi notifiée dispose, à compter de la date d'envoi de sa réponse au bailleur, d'un délai de deux mois pour la réalisation de l'acte de vente. Si dans sa réponse, il notifie au bailleur son intention de recourir à un prêt, son acceptation de l'offre de vente est subordonnée à l'obtention du prêt et, en ce cas, le délai de réalisation es porté à quatre mois. Passé le délai de réalisation de l'acte de vente, l'acceptation par le locataire de l'offre de vente est nulle de plein droit. / Dans le cas où le propriétaire décide de vendre à des conditions ou à un prix plus avantageux pour l'acquéreur, le notaire doit, lorsque le propriétaire n'y a pas préalablement procédé, notifier au locataire ou occupant de bonne foi ces conditions et prix à peine de nullité de la vente. Cette notification vaut offre de vente au profit du locataire ou occupant de bonne foi. Cette offre est valable pendant une durée d'un mois à compter de sa réception. L'offre qui n'a pas été acceptée dans le délai d'un mois est caduque. / Le locataire ou occupant de bonne foi qui accepte l'offre ainsi notifiée dispose, à compter de la date d'envoi de sa réponse au propriétaire ou au notaire, d'un délai de deux mois pour la réalisation de l'acte de vente. Si, dans sa réponse, il notifie son intention de recourir à un prêt, l'acceptation par le locataire ou occupant de bonne foi de l'offre de vente est subordonnée à l'obtention du prêt et le délai de réalisation de la vente est porté à quatre mois. Si, à l'expiration de ce délai, la vente n'a pas été réalisée, l'acceptation de l'offre de vente est nulle de plein droit. Les termes des cinq alinéas qui précèdent doivent être reproduits, à peine de nullité, dans chaque notification./ ... / III- Le présent article s'applique aux ventes de parts ou actions des sociétés dont l'objet est la division d'un immeuble par fractions destinées à être attribuées aux associés en propriété ou en jouissance à temps complet. Il ne s'applique pas aux actes intervenant entre parents ou alliés jusqu'au quatrième degré inclus. ... " ;<br/>
<br/>
              2. Considérant que les quatre premiers alinéas du paragraphe I de l'article 10 de la loi du 31 décembre 1975 instaurent un droit de préemption au profit du locataire ou de l'occupant de bonne foi d'un local d'habitation ou à usage mixte d'habitation et professionnel, lorsque la mise en vente de ce local est consécutive à la division ou à la subdivision de l'immeuble qui l'inclut ; que ce droit de préemption ne peut toutefois s'exercer qu'à l'occasion de la première vente faisant suite à cette division ou subdivision ; qu'en vertu du paragraphe III de l'article 10, le droit de préemption ne s'applique pas à la vente intervenant entre parents ou alliés jusqu'au quatrième degré inclus ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article 1er du décret du 30 juin 1977, pris pour l'application des dispositions précitées : " La première vente d'un appartement et de ses locaux accessoires, depuis la division de l'immeuble dont ils dépendent et l'identification de chaque lot par un état descriptif publié au fichier immobilier, doit être, préalablement à sa conclusion, notifiée au locataire ou à l'occupant de bonne foi au sens de l'article 4 de la loi du 1er septembre 1948 susvisée, et occupant effectivement les lieux " ; <br/>
<br/>
              4. Considérant que la requête de M. A... doit être regardée comme tendant à l'annulation du refus implicite opposé par le Premier ministre à sa demande tendant à l'abrogation de ces dispositions en tant qu'elles n'excluent du bénéfice du droit de préemption institué par la loi du 31 décembre 1975 ni les locataires et occupants de bonne foi dont le bail est postérieur à la division initiale de l'immeuble, ni les locataires et occupants de bonne foi occupant des lots qui, avant une première vente, ont fait l'objet d'une transmission à titre gratuit entre parents ou alliés jusqu'au quatrième degré inclus ;<br/>
<br/>
              5. Considérant, que par sa décision n° 2017-683 QPC du 9 janvier 2018, le Conseil constitutionnel a déclaré les dispositions des quatre premiers alinéas du I de l'article 10 de cette même loi du 31 décembre 1975 et de la deuxième phrase du III de ce même article conformes à la Constitution, sous la réserve que la protection apportée par le législateur ne saurait, sauf à méconnaître le droit de propriété, bénéficier à un locataire ou à un occupant de bonne foi dont le bail ou l'occupation sont postérieurs à la division ou la subdivision de l'immeuble et qui, pour cette raison, ne sont pas exposés au risque de se voir signifier un congé par le nouvel acquéreur à l'échéance du bail ou à l'expiration du titre d'occupation de l'immeuble à la suite d'une opération spéculative facilitée par la division de l'immeuble ;<br/>
<br/>
              6. Considérant, d'une part, qu'il résulte de la réserve d'interprétation dont le Conseil constitutionnel a assorti sa décision du 9 janvier 2018, qui est revêtue de l'autorité absolue de la chose jugée et qui lie le juge pour l'application et l'interprétation de la loi, que l'article 1er du décret est entaché d'illégalité en tant qu'il n'exclut pas du bénéfice du droit de préemption institué par cette loi les locataires et occupants de bonne foi dont le bail ou l'occupation est postérieur à la division ou la subdivision de l'immeuble ; que la décision implicite attaquée doit par suite, être annulée en tant qu'elle refuse d'abroger l'article 1er du décret du 30 juin 1977 dans cette mesure ;<br/>
<br/>
              7. Considérant, d'autre part, que dans sa décision précitée, le Conseil constitutionnel n'a pas remis en cause la constitutionnalité de la loi du 31 décembre 1975 en tant qu'elle n'exclut pas l'application du droit de préemption lorsque la première vente consécutive à la division fait suite à une cession à titre gratuit du lot entre parents ou alliés jusqu'au quatrième degré inclus ; que la demande de M. A...n'est fondée sur aucun vice propre dont seraient entachés le décret du 30 juin 1977 ou la décision implicite du Premier ministre refusant de l'abroger ; que, par suite, M. A...n'est pas fondé à demander l'annulation de cette dernière décision en tant qu'elle rejette sa demande tendant à l'abrogation de l'article 1er du décret du 30 juin 1977 en tant que ces dispositions n'excluent pas du bénéfice du droit de préemption institué par la loi du 31 décembre 1975 les locataires et occupants de bonne foi d'un lot dont la première vente consécutive à la division fait suite à une cession à titre gratuit entre parents ou alliés jusqu'au quatrième degré inclus ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La décision implicite par laquelle le Premier ministre a refusé d'abroger l'article 1er du décret du 30 juin 1977 en tant que ces dispositions n'excluent pas du bénéfice du droit de préemption institué par la loi du 31 décembre 1975 les locataires et occupants de bonne foi dont le bail ou l'occupation est postérieur à la division ou la subdivision de l'immeuble est annulée.<br/>
<br/>
		Article 2 : Le surplus des conclusions de la requête est rejeté.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. B...A..., au Premier ministre et au ministre de la cohésion des territoires.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
