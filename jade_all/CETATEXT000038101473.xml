<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038101473</ID>
<ANCIEN_ID>JG_L_2019_02_000000426251</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/10/14/CETATEXT000038101473.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 06/02/2019, 426251, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426251</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD</AVOCATS>
<RAPPORTEUR>M. Vincent Uher</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:426251.20190206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée Rio Tinto France, à l'appui de sa requête d'appel contre le jugement n° 0511548 du 31 mars 2014 par lequel le tribunal administratif de Cergy-Pontoise n'a que partiellement fait droit à la demande de la société Pechiney, à laquelle elle s'est substituée, tendant à la restitution des sommes versées en 2002 et 2003 par cette société au titre du précompte mobilier à raison de la distribution de produits de participations dans des filiales d'Etats membres de l'Union européenne, a produit deux mémoires, enregistrés les 7 et 26 novembre 2018 au greffe de la cour administrative d'appel de Versailles, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lesquels elle soulève une question prioritaire de constitutionnalité.<br/>
<br/>
              Par une ordonnance n° 14VE01606 du 11 décembre 2018, enregistrée le même jour au secrétariat du contentieux du Conseil d'Etat, le président de la 3ème chambre de cette cour, avant qu'il soit statué sur la requête d'appel de la société Rio Tinto France, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des dispositions des articles 223 sexies et 1679 ter du code général des impôts.<br/>
<br/>
              Dans la question prioritaire de constitutionnalité transmise et dans un mémoire enregistré le 8 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, la société Rio Tinto France soutient que les dispositions des articles 223 sexies et 1679 ter du code général des impôts, applicables au litige, méconnaissent le droit à un recours effectif, garanti par l'article 16 de la Déclaration des droits de l'homme et du citoyen. <br/>
<br/>
              Par trois mémoires, enregistrés les 16 novembre et 28 décembre 2018 et le 10 janvier 2019, le ministre de l'action et des comptes publics soutient que les conditions posées par l'article 23-2 de l'ordonnance du 7 novembre 1958 ne sont pas remplies et, en particulier, que la question soulevée ne présente pas un caractère sérieux. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;  <br/>
              - la loi n° 65-566 du 12 juillet 1965 ;<br/>
              - la loi n° 2000-1352 du 30 décembre 2000 ;<br/>
              - la loi n° 2002-1575 du 30 décembre 2002 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le décret n° 2002-923 du 6 juin 2002 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Uher, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, au cabinet Briard, avocat de la société Rio Tinto France.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes du 1 de l'article 223 sexies du code général des impôts, dans ses rédactions successives issues de la loi du 30 décembre 2000 de finances pour 2001, du décret du 6 juin 2002 (...) procédant à un nouveau décompte des alinéas et de la loi du 30 décembre 2002 de finances pour 2003 : " (...) lorsque les produits distribués par une société sont prélevés sur des sommes à raison desquelles elle n'a pas été soumise à l'impôt sur les sociétés au taux normal prévu au deuxième alinéa du I de l'article 219, cette société est tenue d'acquitter un précompte égal au crédit d'impôt calculé dans les conditions prévues au I de l'article 158 bis. (...) ". <br/>
<br/>
              3. Aux termes de l'article 1679 ter du même code dans sa rédaction issue de la loi du 12 juillet 1965 modifiant l'imposition des entreprises et des revenus de capitaux mobiliers : " Le précompte visé à l'article 223 sexies doit être versé au Trésor dans le mois qui suit la mise en paiement des revenus et sous les mêmes sanctions que la retenue perçue à la source sur les produits d'obligations. / Un décret fixe les modalités d'application du présent article ".<br/>
<br/>
              4. La société Rio Tinto France soutient que les dispositions combinées des articles 223 sexies et 1679 ter précités méconnaissent le droit à un recours effectif garanti par l'article 16 de la Déclaration des droits de l'homme et du citoyen de 1789 en ce que, en méconnaissance de la compétence que le législateur tire de l'article 34 de la Constitution, elles ne déterminent pas les modalités de recouvrement du précompte mobilier.<br/>
<br/>
              5. La méconnaissance par le législateur de sa propre compétence ne peut être invoquée à l'appui d'une question prioritaire de constitutionnalité que dans le cas où cette méconnaissance affecte par elle-même un droit ou une liberté que la Constitution garantit.<br/>
<br/>
              6. Aux termes de l'article 34 de la Constitution : " La loi fixe les règles concernant (...) l'assiette, le taux et les modalités de recouvrement des impositions de toutes natures (...) ". Il s'ensuit que, lorsqu'il définit une imposition, le législateur doit déterminer ses modalités de recouvrement, lesquelles comprennent les règles régissant le contrôle, le recouvrement, le contentieux, les garanties et les sanctions applicables à cette imposition.<br/>
<br/>
              7. La méconnaissance, par le législateur, de l'étendue de sa compétence dans la détermination des modalités de recouvrement d'une imposition affecte par elle-même le droit à un recours effectif garanti par l'article 16 de la Déclaration des droits de l'Homme et du citoyen de 1789.<br/>
<br/>
              8. Toutefois, selon une jurisprudence constante du Conseil d'Etat, statuant au contentieux, le précompte mobilier institué par l'article 223 sexies précité constituait un impôt direct, distinct de l'impôt sur les sociétés. Il résulte de l'article 1679 ter du code général des impôts que cet impôt était recouvré par voie de versement spontané par les redevables, auxquels il incombait d'en calculer le montant, dans le mois suivant la mise en paiement des revenus distribués dont le versement constituait son fait générateur et qu'à défaut de paiement à la date d'exigibilité, le recouvrement s'effectuait par application des dispositions de droit commun du titre IV du livre des procédures fiscales et, notamment, par la voie d'un avis de mise en recouvrement en application de l'article L. 256 de ce livre, dans sa rédaction alors en vigueur, sous les sûretés et privilèges prévus aux articles 1920 et 1929 ter et suivants du code général des impôts. Les règles relatives au pouvoir de contrôle et de rectification de l'administration étaient  définies par les dispositions de droit commun du titre II du livre des procédures fiscales, par son article L. 80 pour ce qui concerne le droit à compensation, ainsi que par son article L. 169, rendu expressément applicable au précompte mobilier par renvoi de l'article L. 169 A, pour ce qui concerne le délai de reprise. Les règles relatives aux sanctions étaient définies par l'article 1679 ter, lequel renvoyait à cette fin aux dispositions applicables à la retenue à la source sur les produits d'obligations codifiée à l'article 119 bis du code général des impôts. Enfin, le contentieux du précompte mobilier était régi par le livre des procédures fiscales, dont l'article L. 204 déterminait expressément les règles applicables s'agissant du droit à compensation, et relevait, conformément à l'article L. 199 de ce livre, s'agissant d'un impôt direct, de la compétence de la juridiction administrative.<br/>
<br/>
              9. Il résulte de ce qui précède que, contrairement à ce que soutient la société Rio Tinto France, la loi définissait sans ambiguïté et avec suffisamment de précision les modalités de recouvrement du précompte mobilier, au sens rappelé au point 6 ci-dessus.<br/>
<br/>
              10. Ainsi la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Par suite, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée.<br/>
<br/>
<br/>
<br/>                   D E C I D E :<br/>
                                --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par la cour administrative d'appel de Versailles.<br/>
Article 2 : La présente décision sera notifiée à la société par actions simplifiée Rio Tinto France et au ministre de l'action et des comptes publics.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre et à la cour administrative d'appel de Versailles.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
