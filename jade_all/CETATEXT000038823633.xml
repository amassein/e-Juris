<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038823633</ID>
<ANCIEN_ID>JG_L_2019_07_000000412453</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/82/36/CETATEXT000038823633.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 24/07/2019, 412453, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412453</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY ; SCP FABIANI, LUC-THALER, PINATEL ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Jean-Yves Ollier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:412453.20190724</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Nîmes de condamner solidairement la commune de Châteauneuf-de-Gadagne et la société Groupama Méditerranée à lui payer la somme de 124 434 euros en réparation des préjudices subis du fait d'un dysfonctionnement du réseau communal d'eaux pluviales et d'ordonner à la commune de réaliser les travaux propres à faire cesser les dommages. Par un jugement n° 1202110 du 3 juillet 2014, le tribunal administratif de Nîmes a condamné solidairement la commune et la société Groupama Méditerranée à payer à Mme A...les sommes de 46 992,24 euros hors taxes et 18 662,16 euros et a condamné la société Groupama Méditerranée à garantir la commune des condamnations prononcées à son encontre.<br/>
<br/>
              Par un arrêt n° 14MA03801 du 11 mai 2017, la cour administrative d'appel de Marseille a rejeté l'appel formé par la société Groupama Méditerranée contre ce jugement et, sur appel incident de MmeA..., réformé celui-ci en portant les sommes que cette société a été condamnée à payer à Mme A...par ce jugement à la somme totale de 102 011 euros.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 13 juillet et 13 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il a ramené à 90 % la part imputable à la commune dans la survenance des dommages ;<br/>
<br/>
              2°) de mettre solidairement à la charge de la commune de Châteauneuf-de-Gadagne et de la société Groupama Méditerranée la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des assurances ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Yves Ollier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de MmeA..., à la SCP Thouvenin, Coudray, Grevy, avocat de la commune de Châteauneuf-de-Gadagne, et à la SCP Didier, Pinet, avocat de la société Groupama Méditerranée.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme A...est propriétaire d'une maison de village ancienne, à Châteauneuf-de-Gadagne, dont l'une des façades est construite sur un rempart du XIVème siècle appartenant à la commune. Dans le courant de l'année 2010, la fissuration des murs de sa maison a été constatée. Estimant que ces dommages étaient dus à des fuites de canalisations d'eaux pluviales dont la commune est maître d'ouvrage et à la vétusté du rempart, Mme A...a demandé au tribunal administratif de Nîmes de condamner solidairement la commune et l'assureur de celle-ci, la société Groupama Méditerranée, à réparer les dommages subis et d'enjoindre à la commune de réaliser les travaux nécessaires à la consolidation du rempart qui soutient son habitation. La société Groupama Méditerranée a relevé appel du jugement du 3 juillet 2014 en tant que le tribunal administratif l'a condamnée solidairement avec la commune à verser à Mme A...les sommes de 46 992,24 euros hors taxes et 18 662,16 euros et qu'il l'a condamnée à garantir la commune des condamnations prononcées à son encontre. Par un arrêt du 11 mai 2017, la cour administrative d'appel de Marseille a rejeté cet appel et, sur appel incident de MmeA..., porté les sommes que la société a été condamnée à payer à Mme A...par ce jugement à la somme totale de 102 011 euros. Mme A... se pourvoit en cassation contre cet arrêt en tant qu'il a ramené à 90 % la part imputable à la commune dans la survenance des dommages. La commune et la société Groupama Méditerrannée ont présenté respectivement un pourvoi incident et un pourvoi provoqué.<br/>
<br/>
              Sur le pourvoi de Mme A...:<br/>
<br/>
              2. Le maître de l'ouvrage est responsable, même en l'absence de faute, des dommages que les ouvrages publics dont il a la garde peuvent causer aux tiers tant en raison de leur existence que de leur fonctionnement. Il ne peut dégager sa responsabilité que s'il établit que ces dommages résultent de la faute de la victime ou d'un cas de force majeure, sans pouvoir utilement invoquer le fait du tiers.<br/>
<br/>
              3. Il ressort des énonciations de l'arrêt attaqué que la cour administrative d'appel de Marseille a jugé que la responsabilité de la commune de Châteauneuf-de-Gadagne était engagée à l'égard de Mme A...qui avait la qualité de tiers par rapport au réseau d'évacuation des eaux pluviales de la commune. Après avoir relevé que les fuites de celui-ci constituaient la cause principale du dommage subi par MmeA..., elle a estimé que celui-ci était également imputable aux fuites d'une canalisation privée enterrée, causées par les racines d'un figuier planté sur la parcelle voisine ainsi qu'à l'action de cet arbre sur les maçonneries de la maison et le rempart sur lequel elle est construite. En déduisant de cette circonstance imputable à un tiers qu'il y avait lieu de réduire de 10 % la part du dommage imputable à la commune, la cour administrative d'appel a entaché son arrêt d'une erreur de droit. Dès lors, Mme A...est fondée à en demander l'annulation en tant qu'il a ramené à 90 % la part imputable à la commune dans la survenance des dommages.<br/>
<br/>
              Sur le pourvoi incident de la commune de Châteauneuf-de-Gadagne :<br/>
<br/>
              4. En premier lieu, en jugeant qu'eu égard à la nature du dommage en litige, Mme A...avait la qualité de tiers par rapport au réseau communal d'évacuation des eaux de pluies, alors même que sa maison y était raccordée, la cour n'a pas inexactement qualifié les faits qui lui étaient soumis.<br/>
<br/>
              5. En second lieu, dans le cas d'un dommage causé à un immeuble, la fragilité ou la vulnérabilité de celui-ci ne peuvent être prises en compte pour atténuer la responsabilité du maître de l'ouvrage, sauf lorsqu'elles sont elles-mêmes imputables à une faute de la victime. En dehors de cette hypothèse, de tels éléments ne peuvent être retenus que pour évaluer le montant du préjudice indemnisable.<br/>
<br/>
              6. Après avoir relevé que la vétusté de la construction appartenant à Mme A...a été retenue par l'expert comme ayant amplifié les désordres en litige, la cour administrative d'appel de Marseille a estimé que la fragilité ou la vulnérabilité de la maison de Mme A...ne résultait pas d'une faute de la victime et qu'en conséquence elle ne constituait pas une cause exonératoire de la responsabilité de la commune. En statuant ainsi, elle n'a entaché son arrêt ni d'insuffisance de motivation, ni d'une erreur de droit, ni d'une dénaturation des faits de l'espèce. <br/>
<br/>
              Sur le pourvoi provoqué de la société Groupama Méditerranée :<br/>
<br/>
              7. Il ressort des pièces du dossier soumis aux juges du fond que selon l'article 8.10 du cahier des clauses particulières du contrat d'assurance des responsabilités et risques annexes, conclu entre la commune de Châteauneuf-de-Gadagne et la société Groupama Méditerranée, les dommages causés par les installations servant à l'évacuation des eaux pluviales et usées sont exclus de la garantie s'il est établi que le risque n'a pas de caractère aléatoire du fait notamment d'un " défaut d'entretien caractérisé ". Si l'expert a relevé un défaut d'entretien du réseau d'évacuation des eaux pluviales, la cour a estimé, par une appréciation souveraine des faits exempte de dénaturation, que ce défaut d'entretien n'était pas caractérisé au sens des stipulations du contrat d'assurance.<br/>
<br/>
              8. Il résulte de tout ce qui précède, sans qu'il soit besoin d'examiner les fins de non-recevoir opposées par MmeA..., que le pourvoi incident de la commune de Châteauneuf-de-Gadagne et le pourvoi provoqué de la société Groupama Méditerranée doivent être rejetés et que l'arrêt de la cour administrative d'appel de Marseille doit être annulé en tant qu'il a ramené à 90 % la part imputable à la commune dans la survenance des dommages.<br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Groupama Méditerranée et de la commune de Châteauneuf-de-Gadagne la somme de 1 500 euros chacune à verser à Mme A...au titre des dispositions de l'article L. 761-1 du code de justice administrative. En revanche, ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de Mme A...qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 11 mai 2017 de la cour administrative d'appel de Marseille est annulé en tant qu'il a ramené à 90 % la part imputable à la commune dans la survenance des dommages.<br/>
Article 2 : Le pourvoi incident de la commune de Châteauneuf-de-Gadagne et le pourvoi provoqué de la société Groupama sont rejetés.<br/>
Article 3 : L'affaire est renvoyée, dans la mesure de la cassation ainsi prononcée, à la cour administrative d'appel de Marseille.<br/>
Article 4 : La commune de Châteauneuf-de-Gadagne et la société Groupama Méditerranée verseront à Mme A...une somme de 1 500 euros chacune au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Les conclusions de la commune de Châteauneuf-de-Gadagne et de la société Groupama Méditerranée présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à Mme B...A..., à la commune de Châteauneuf-de-Gadagne et à la société Groupama Méditerranée.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
