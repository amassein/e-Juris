<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032889630</ID>
<ANCIEN_ID>JG_L_2016_07_000000400723</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/88/96/CETATEXT000032889630.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 11/07/2016, 400723, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400723</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MEIER-BOURDEAU, LECUYER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2016:400723.20160711</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 16 juin 2016 au secrétariat du contentieux du Conseil d'Etat, M. D...C..., M. B...A...et l'association pour la promotion et la défense du notariat demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution du décret n° 2016-661 du 20 mai 2016 relatif aux officiers publics et ministériels ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Ils soutiennent que : <br/>
              - ils ont qualité pour agir ;<br/>
              - la condition d'urgence est remplie dès lors que, d'une part, les dispositions du décret contesté entreront en vigueur le 1er août 2016, que, d'autre part, l'application de ces dispositions aura pour conséquence d'obliger les notaires atteints par la limite d'âge à cesser leurs fonctions sans qu'il leur soit laissé un délai raisonnable pour exercer leur droit de présentation afin d'organiser la transmission de leur office, avec une très importante perte de revenus ainsi qu'une perturbation du fonctionnement du service public de l'authentification ;<br/>
              - il existe un doute sérieux quant à la légalité du décret contesté ;<br/>
              - le décret méconnaît l'article 22 de la Constitution faute de comporter le contreseing du ministre de l'économie, de l'industrie et du numérique ;<br/>
              - il est entaché d'incompétence à moins d'établir qu'il est conforme au projet du Gouvernement soumis au Conseil d'Etat ou bien à l'avis de celui-ci ;<br/>
              - il méconnaît le principe de non discrimination à raison de l'âge garanti par le droit de l'Union européenne, et notamment la directive du Conseil n° 2000/78/CE du 27 novembre 2000, en ce qu'il ne comporte pas une entrée en vigueur plus progressive de la limite d'âge à l'égard des professionnels dont l'âge est le plus proche de celle-ci et constitue ainsi, par la brutalité de son intervention, une mesure qui n'est ni appropriée au regard de ses conséquences, notamment sur la situation des notaires concernés, ni nécessaire à l'objectif poursuivi pour permettre l'installation de plus jeunes que la loi libéralise par d'autres mesures ;<br/>
              - il porte atteinte au droit de propriété et au droit au respect des biens garantis par les dispositions combinées de l'article 1er du protocole n° 1 annexé à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et l'article 14 de cette même convention, dès lors qu'il ne prévoit pas les mesures transitoires nécessaires à la cession des offices ou des parts dans la société titulaire de l'office des notaires dans des conditions économiques normales et qu'il introduit une discrimination dans la jouissance du droit de propriété ;<br/>
              - il est entaché d'illégalité en tant qu'il ne prévoit pas l'intervention d'une décision individuelle de cessation de fonction pour chacun des notaires ayant atteint la limite d'âge, de sorte que le ministre de la justice n'est pas en mesure de s'assurer du respect du principe de continuité du service public de la justice auquel les notaires collaborent ;<br/>
              - il est incompatible avec les dispositions de l'article 53 de la loi du 6 août 2015 en interdisant aux notaires âgés de 71 ans de solliciter le bénéfice d'une prolongation d'activité d'un an ;<br/>
              - il méconnaît le principe de sécurité juridique à défaut de prévoir des dispositions transitoires adaptées aux situations individuelles, au regard des préjudices auxquels les professionnels concernés risquent d'être immédiatement confrontés, et en particulier d'avoir introduit un dispositif de départ échelonné comparable à celui prévu pour les notaires d'Alsace-Moselle.<br/>
<br/>
              Par une mesure d'instruction supplémentaire, le juge des référés a demandé au garde des sceaux, ministre de la justice, le 28 juin 2016, de produire une étude d'impact sur l'instauration de la limite d'âge de la profession de notaire ou tout élément en tenant lieu précisant le nombre de professionnels de plus de 68, 69, 70 et 71 ans ainsi qu'une évaluation du nombre de professionnels susceptibles de se porter acquéreurs d'offices. <br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M.C..., M. A... et l'association pour la promotion et la défense du notariat et, d'autre part, le Premier ministre et le garde des sceaux, ministre de la justice ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 28 juin 2016 à 10 heures au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Lécuyer, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. C..., de M. A... et de l'association pour la promotion et la défense du notariat ;<br/>
<br/>
              - M. C... et M. A... ;<br/>
<br/>
              - les représentants de M.C..., de M.A..., et de l'Association pour la promotion et la défense du notariat ;<br/>
<br/>
              - les représentants du garde des sceaux, ministre de la justice ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a différé la clôture de l'instruction jusqu'au 6 juillet 2016 à 12 heures ; <br/>
<br/>
              Vu la mesure d'instruction supplémentaire par laquelle le juge des référé a, lors de l'audience publique, demandé au garde des sceaux, ministre de la justice, de produire des éléments relatifs en premier lieu, à la situation démographique des notaires, en deuxième lieu, au dispositif de la suppléance des officiers publics et ministériels, et, en troisième lieu, aux délais de traitement d'un dossier de cession par les services de la Chancellerie ;<br/>
<br/>
              Vu le mémoire, enregistré le 5 juillet 2016, par lequel le garde des sceaux, ministre de la justice, produit les éléments demandés dans le cadre du supplément d'instruction et persiste dans ses écritures ;<br/>
<br/>
              Vu les mémoires, enregistrés les 5 et 6 juillet 2016, par lesquels M. C..., M. A... et l'association pour la promotion et la défense du notariat persistent dans leurs conclusions et demandent en outre au juge des référés d'enjoindre à l'administration de prendre les mesures qu'impliquent la suspension de l'exécution du décret contesté ;<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la Charte des droits fondamentaux de l'Union européenne ;<br/>
              - la directive du Conseil n° 2000/78/CE du 27 novembre 2000 ;<br/>
              - la loi du 25 ventôse an XI ;<br/>
              - la loi n° 2015-990 du 6 août 2015 ;<br/>
              - l'ordonnance n° 45-2590 du 2 novembre 1945 ;<br/>
              - le décret n° 55-604 du 20 mai 1955 ;<br/>
              - le décret n° 71-942 du 26 novembre 1971 ;<br/>
              - le décret n° 73-609 du 5 juillet 1973 ;<br/>
              - le décret n° 88-814 du 12 juillet 1988 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; <br/>
<br/>
              2. Considérant que l'article 53 de la loi du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques instaure notamment une limite d'âge à l'exercice de la profession de notaire et modifie en conséquence l'article 2 de la loi du 25 ventôse an XI contenant organisation du notariat qui prévoyait un exercice à vie de la profession ; qu'aux termes de cet article dans sa rédaction issue de la loi du 6 août 2015, " les notaires cessent leurs fonctions lorsqu'ils atteignent l'âge de soixante-dix ans. Sur autorisation du ministre de la justice, ils peuvent continuer d'exercer leurs fonctions jusqu'au jour où leur successeur prête serment, pour une durée qui ne peut excéder douze mois " ; que le II de l'article 53 précise que ces dispositions entrent en vigueur le premier jour du douzième mois suivant celui de sa promulgation ; qu'en application de ces dispositions, l'article 3 du décret du 20 mai 2016 insère, après le titre Ier du décret du 5 juillet 1973 relatif à la formation professionnelle dans le notariat et aux conditions d'accès aux fonctions de notaire, un titre Ier bis précisant que " (...) Le délai de douze mois prévu pour la prolongation d'activité court à compter du soixante-dixième anniversaire de l'intéressé. " ; que l'article 2 de ce décret modifie l'article 4 du décret du 26 novembre 1971 relatif aux créations, transferts et suppressions d'offices de notaire, à la compétence d'instrumentation et à la résidence des notaires, à la garde et à la transmission des minutes et registres professionnels des notaires et prévoit que " Les suppressions d'offices ne peuvent intervenir qu'à la suite : /1° Du décès, de la démission ou de la destitution de leur titulaire ; / 2° De l'atteinte, par leur titulaire, de la limite d'âge fixée pour l'exercice des fonctions de notaire ou, le cas échéant, de l'expiration de l'autorisation de prolongation d'activité prévue par les articles 2 et 52 de la loi du 25 ventôse an XI susvisée ; / 3° Si le titulaire de l'office est une société, de sa dissolution. " ; que l'article 12 du décret du 20 mai 2016 modifie également le décret du 20 mai 1955 relatif aux officiers publics ou ministériels et à certains auxiliaires de justice en remplaçant l'article 5 par les dispositions suivantes : " la gestion des offices publics et ministériels dépourvus de titulaire, notamment en raison du décès ou de la démission, volontaire ou d'office, de celui-ci, de la survenance de la limite d'âge ou, le cas échéant, de l'expiration de l'autorisation de prolongation d'activité délivrée par le garde des sceaux, ministre de la justice, est provisoirement assurée par un ou plusieurs suppléants. Il en est de même lorsque le titulaire est temporairement empêché, par cas de force majeure, d'exercer ses fonctions. " ; qu'enfin, l'article 16 du décret du 20 mai 2016 prévoit à titre transitoire que " Par dérogation aux dispositions des articles 58-1 du décret n° 73 -609 du 5 juillet 1973 susvisé, 37 du décret n° 75-770 du 14 août 1975 susvisé et 35-1 du décret n° 73-541 du 19 juin 1973 susvisé, les notaires, huissiers de justice et commissaires-priseurs judiciaires nés entre le 2 août 1945 et le 1er octobre 1946 peuvent solliciter l'autorisation de prolongation d'activité jusqu'au 30 septembre 2016. Ils bénéficient, jusqu'à cette date, d'une autorisation de plein droit de poursuivre leur activité. En cas de demande formée avant cette date, l'autorisation est automatiquement prorogée jusqu'à la date de notification de la réponse du garde des sceaux, ministre de la justice. Les deux alinéas précédents ne peuvent avoir pour effet de permettre aux personnes visées au premier alinéa d'exercer leurs fonctions au-delà de la date de leur soixante-et-onzième anniversaire. " ; que M. C..., M. A...et l'Association pour la promotion et la défense du notariat demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, d'ordonner la suspension de l'exécution du décret du 20 mai 2016 afin de faire obstacle à ce que les professionnels âgés de soixante-et-onze ans et plus au 1er août 2016 puissent être atteints par la limite d'âge aussi longtemps que cette suspension produira ses effets et d'enjoindre à l'administration de prendre les mesures réglementaires qu'appelle cette suspension ;<br/>
<br/>
              3. Considérant, en premier lieu, qu'il ressort des pièces du dossier que le décret contesté est conforme à l'avis émis par le Conseil d'Etat ; qu'en outre, il n'apparaît pas, en l'état de l'instruction, que ce décret appellerait une mesure d'exécution que le ministre de l'économie, de l'industrie et du numérique devrait signer ou contresigner ; qu'ainsi, les moyens tirés de l'irrégularité de la procédure d'adoption du décret et de l'incompétence de son auteur n'apparaissent pas, en l'état de l'instruction, de nature à créer un doute sérieux sur la légalité du décret du 20 mai 2016 ; <br/>
<br/>
              4. Considérant, en deuxième lieu, que, dans sa décision 2015-715 DC du 5 août 2015, le Conseil constitutionnel a estimé qu'en fixant à soixante-dix ans l'âge limite pour l'exercice de la profession de notaire et en permettant une prolongation d'activité pendant une durée maximale d'un an avant que le successeur ne prête serment, le législateur a poursuivi un objectif d'intérêt général et n'a pas porté une atteinte disproportionnée à la liberté d'entreprendre de ces professionnels ni à aucune autre exigence constitutionnelle ; qu'il a, en conséquence, déclaré conformes à la Constitution les dispositions de l'article 53 de la loi du 6 août 2015 en tant qu'elle modifie l'article 2 de la loi du 25 ventôse an XI ; que les dispositions de cet article font par elles-mêmes obstacle, à compter de la date de leur entrée en vigueur, à la poursuite de l'exercice de la profession de notaire au-delà de l'âge de soixante-et-onze ans ; qu'en ne prévoyant pour aucun des professionnels concernés, pas même ceux âgés de plus de soixante-dix ans à la date de l'entrée en vigueur de la loi, dans le cadre de dispositions transitoires, la possibilité de poursuivre l'exercice de cette profession à compter du 1er août 2016 au-delà de la date de leur soixante-et-onzième anniversaire, le décret contesté a, par suite, fait une exacte application de la loi ; qu'il en résulte que les moyens tirés de ce que le décret contesté méconnaîtrait l'article 53 de la loi ainsi que le principe de sécurité juridique tel qu'il est garanti en droit interne, le principe d'égalité devant la loi et le droit de propriété tel que garanti par la Déclaration des droits de l'homme et du citoyen ne sont pas de nature à faire naître un doute sérieux sur sa légalité ;<br/>
<br/>
              5. Considérant, en troisième lieu, que si ces dispositions font obstacle à la poursuite par un professionnel de son activité au-delà de l'âge de soixante-et-onze ans et hors le cas de la suppléance, dont le décret du 20 mai 1955 prévoit qu'elle peut notamment être confiée à des notaires à la retraite, elles n'ont ni pour objet ni pour effet de priver le professionnel atteint par la limite d'âge de la propriété de l'office ou des parts qu'il détient dans la société qui en est propriétaire, pas plus que de son droit de présentation d'un successeur ; que si les requérants font cependant valoir que l'insuffisante durée de la période transitoire qui leur aurait permis d'obtenir, dans des conditions financières " normales ", l'agrément de leur successeur avant d'être atteint par la limite d'âge a porté atteinte à la valeur patrimoniale de ce bien et à l'espérance légitime qu'ils tenaient jusqu'alors des textes quant à la possibilité de poursuivre à vie l'exercice de leur activité, il n'apparaît pas, en l'état de l'instruction et eu égard à l'office qui est celui du juge des référés, que cette atteinte serait telle qu'elle excèderait de façon manifeste la marge d'appréciation dont le législateur disposait au regard de l'intérêt général légitime, et au demeurant non contesté, poursuivi par l'entrée en vigueur d'un âge limite d'exercice de la profession de notaire ; <br/>
<br/>
              6. Considérant, en quatrième lieu, que les requérants invoquent également la méconnaissance du principe de non discrimination garanti par la Charte des droits fondamentaux et précisé par la directive n° 2000/78/CE du Conseil du 27 novembre 2000 portant création d'un cadre général en faveur de l'égalité de traitement en matière d'emploi et de travail ; qu'ils admettent que l'instauration pour leur profession d'une limite d'âge répond à un objectif d'intérêt général de la nature de ceux qui permettent à un Etat membre d'instaurer une différence de traitement à raison de l'âge sans qu'elle constitue une discrimination mais font valoir que la méconnaissance de la directive résulte du caractère inapproprié et non nécessaire des moyens mis en oeuvre pour l'atteindre, au sens du §1 de l'article 6 de la directive qui dispose que " Nonobstant l'article 2, paragraphe 2, les États membres peuvent prévoir que des différences de traitement fondées sur l'âge ne constituent pas une discrimination lorsqu'elles sont objectivement et raisonnablement justifiées, dans le cadre du droit national, par un objectif légitime, notamment par des objectifs légitimes de politique de l'emploi, du marché du travail et de la formation professionnelle, et que les moyens de réaliser cet objectif sont appropriés et nécessaires ", en l'absence de dispositions permettant une entrée en vigueur progressive de cette limite d'âge à l'égard des professionnels les plus proches de celle-ci par un dispositif de départ échelonné comparable à celui prévu pour les notaires d'Alsace-Moselle ; que si le délai de près de douze mois laissé par le législateur avant l'entrée en vigueur de cette limite d'âge pourrait, dans certains cas, s'avérer insuffisamment long, au regard notamment des délais habituels pour trouver un successeur, alors au surplus que les textes pris pour l'application d'autres dispositions de la loi du 5 août 2015 qui ont modifié les conditions d'exercice de cette profession n'ont été publiés qu'en juin 2016, puis pour obtenir son agrément par le ministre, il n'en résulte pas pour autant, en l'état de l'instruction et eu égard à l'office du juge des référés, qu'il serait manifeste que les moyens mis en oeuvre pour atteindre l'objectif légitime poursuivi par le législateur ne seraient pas appropriés ni nécessaires ; qu'il n'en résulte pas non plus une méconnaissance manifeste des principes de sécurité juridique et de confiance légitime protégés par le droit de l'Union européenne ; <br/>
<br/>
              7. Considérant, enfin, que, dans la mesure où les notaires susceptibles d'être atteints par la limite d'âge à compter du 1er août 2016 ont la possibilité de saisir, dès maintenant, le ministre de la justice, selon le cas, d'une demande d'agrément de leur successeur, d'une demande de prorogation de leur activité ou d'une demande de suppléance, le moyen tiré de l'illégalité de l'absence de mesure individuelle constatant qu'un notaire est atteint par la limite d'âge n'apparaît pas, en l'état de l'instruction, propre à faire naître un doute sérieux sur la légalité du décret ; <br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède qu'aucun des moyens invoqués par M.C..., M. A...et l'association pour la promotion et la défense du notariat n'apparaît, en l'état de l'instruction, propre à créer un doute sérieux sur la légalité du décret du 20 mai 2016 ; que, par suite, et sans qu'il soit besoin d'examiner si la condition d'urgence est remplie, leurs conclusions à fin de suspension de l'exécution de ce décret et d'injonction ne peuvent être accueillies ; que doivent être également rejetées leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;	    <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M.C..., M. A...et l'association pour la promotion et la défense du notariat est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. D...C..., à M. B...A..., à l'association pour la promotion et la défense du notariat et au garde des sceaux, ministre de la justice.<br/>
Copie en sera adressée, pour information, au Premier ministre. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
