<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039456963</ID>
<ANCIEN_ID>JG_L_2019_12_000000391000</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/45/69/CETATEXT000039456963.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 06/12/2019, 391000</TITRE>
<DATE_DEC>2019-12-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391000</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEORD:2019:391000.20191206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 24 février 2017, le Conseil d'Etat, statuant au contentieux sur la requête de Mme A... tendant à l'annulation, pour excès de pouvoir, de la décision par laquelle la Commission nationale de l'informatique et des libertés (CNIL) a clôturé sa plainte tendant au déréférencement d'un lien renvoyant vers une vidéo du site internet " YouTube " dans les résultats obtenus sur la base d'une recherche effectuée à partir de son nom sur le moteur de recherche exploité par la société Google, qui lui a été notifiée par un courrier du 24 avril 2014, a sursis à statuer jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur les questions de savoir si : <br/>
<br/>
              1°) Eu égard aux responsabilités, aux compétences et aux possibilités spécifiques de l'exploitant d'un moteur de recherche, l'interdiction faite aux autres responsables de traitement de traiter des données relevant des paragraphes 1 et 5 de l'article 8 de la directive du 24 octobre 1995, sous réserve des exceptions prévues par ce texte, est-elle également applicable à cet exploitant en tant que responsable du traitement que constitue ce moteur '<br/>
<br/>
              2°) En cas de réponse positive à la question posée au 1°:<br/>
              - les dispositions de l'article 8 paragraphes 1 et 5 de la directive du 24 octobre 1995 doivent-elles être interprétées en ce sens que l'interdiction ainsi faite, sous réserve des exceptions prévues par cette directive, à l'exploitant d'un moteur de recherche de traiter des données relevant de ces dispositions l'obligerait à faire systématiquement droit aux demandes de déréférencement portant sur des liens menant vers des pages web qui traitent de telles données '<br/>
              - dans une telle perspective, comment s'interprètent les exceptions prévues à l'article 8 paragraphe 2, sous a) et e), de la directive du 24 octobre 1995, lorsqu'elles s'appliquent à l'exploitant d'un moteur de recherche, eu égard à ses responsabilités, ses compétences et ses possibilités spécifiques ' En particulier, un tel exploitant peut-il refuser de faire droit à une demande de déréférencement lorsqu'il constate que les liens en cause mènent vers des contenus qui, s'ils comportent des données relevant des catégories énumérées au paragraphe 1 de l'article 8, entrent également dans le champ des exceptions prévues par le paragraphe 2 de ce même article, notamment le a) et le e) '<br/>
              - de même, les dispositions de la directive du 24 octobre 1995 doivent-elles être interprétées en ce sens que, lorsque les liens dont le déréférencement est demandé mènent vers des traitements de données à caractère personnel effectués aux seules fins de journalisme ou d'expression artistique ou littéraire qui, à ce titre, en vertu de l'article 9 de la directive du 24 octobre 1995, peuvent collecter et traiter des données relevant des catégories mentionnées à l'article 8, paragraphes 1 et 5, de cette directive, l'exploitant d'un moteur de recherche peut, pour ce motif, refuser de faire droit à une demande de déréférencement '<br/>
<br/>
              3°) En cas de réponse négative à la question posée au 1° :<br/>
              - à quelles exigences spécifiques de la directive du 24 octobre 1995 l'exploitant d'un moteur de recherche, compte tenu de ses responsabilités, de ses compétences et de ses possibilités, doit-il satisfaire '<br/>
              - lorsqu'il constate que les pages web, vers lesquelles mènent les liens dont le déréférencement est demandé, comportent des données dont la publication, sur lesdites pages, est illicite, les dispositions de la directive du 24 octobre 1995 doivent-elles être interprétées en ce sens :<br/>
              - qu'elles imposent à l'exploitant d'un moteur de recherche de supprimer ces liens de la liste des résultats affichés à la suite d'une recherche effectuée à partir du nom du demandeur '<br/>
              - ou qu'elles impliquent seulement qu'il prenne en compte cette circonstance pour apprécier le bien-fondé de la demande de déréférencement '<br/>
              - ou que cette circonstance est sans incidence sur l'appréciation qu'il doit porter '<br/>
              En outre, si cette circonstance n'est pas inopérante, comment apprécier la licéité de la publication des données litigieuses sur des pages web qui proviennent de traitements n'entrant pas dans le champ d'application territorial de la directive du 24 octobre 1995 et, par suite, des législations nationales la mettant en oeuvre '<br/>
<br/>
              4°) Quelle que soit la réponse apportée à la question posée au 1° :<br/>
              - indépendamment de la licéité de la publication des données à caractère personnel sur la page web vers laquelle mène le lien litigieux, les dispositions de la directive du 24 octobre 1995 doivent-elles être interprétées en ce sens que :<br/>
              - lorsque le demandeur établit que ces données sont devenues incomplètes ou inexactes, ou qu'elles ne sont plus à jour, l'exploitant d'un moteur de recherche est tenu de faire droit à la demande de déréférencement correspondante '<br/>
              - plus spécifiquement, lorsque le demandeur démontre que, compte tenu du déroulement de la procédure judiciaire, les informations relatives à une étape antérieure de la procédure ne correspondent plus à la réalité actuelle de sa situation, l'exploitant d'un moteur de recherche est tenu de déréférencer les liens menant vers des pages web comportant de telles informations '<br/>
              - les dispositions de l'article 8 paragraphe 5 de la directive du 24 octobre 1995 doivent-elles être interprétées en ce sens que les informations relatives à la mise en examen d'un individu ou relatant un procès, et la condamnation qui en découle, constituent des données relatives aux infractions et aux condamnations pénales ' De manière générale, lorsqu'une page web comporte des données faisant état des condamnations ou des procédures judiciaires dont une personne physique a été l'objet, entre-t-elle dans le champ de ces dispositions '<br/>
<br/>
              Par un arrêt C-136/17 du 24 septembre 2019, la Cour de justice de l'Union européenne s'est prononcée sur ces questions. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier, y compris celles visées par la décision du Conseil d'Etat du 24 février 2017 ; <br/>
<br/>
              Vu : <br/>
              - la Charte des droits fondamentaux de l'Union européenne ; <br/>
              - le règlement (UE) n° 2016/679 du Parlement européen et du Conseil du 27 avril 2016 ; <br/>
              - la loi n° 78-17 du 6 janvier 1978 ;<br/>
              - l'arrêt de la Cour de justice de l'Union européenne du 13 mai 2014, Google Spain SL, Google Inc. contre Agencia Espanola de Proteccion de Datos, Mario Costeja Gonzalez (C-131/12) ;<br/>
              - l'arrêt de la Cour de justice de l'Union européenne du 24 septembre 2019, GC, AF, BH et ED contre CNIL (C-136/17) ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Myriam Benlolo Carabot, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la société Google LLC ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier que Mme A... a demandé à la société Google de procéder au déréférencement, dans les résultats affichés par le moteur de recherche qu'elle exploite à la suite d'une recherche portant sur son nom, d'un lien hypertexte renvoyant vers une vidéo publiée sur le site " YouTube ". A la suite du refus opposé par la société Google, elle a saisi la Commission nationale de l'informatique et des libertés (CNIL) d'une plainte tendant à ce qu'il soit enjoint à cette société de procéder au déréférencement du lien en cause. Par un courrier du 24 avril 2015, la présidente de la CNIL l'a informée de la clôture de sa plainte. Mme A... demande l'annulation pour excès de pouvoir du refus de la CNIL de mettre en demeure la société Google de procéder au déréférencement demandé.<br/>
<br/>
              2. L'effet utile de l'annulation pour excès de pouvoir du refus de la CNIL de mettre en demeure l'exploitant d'un moteur de recherche de procéder au déréférencement de liens vers des pages web réside dans l'obligation, que le juge peut prescrire d'office en vertu des dispositions de l'article L. 911-1 du code de justice administrative, pour la CNIL de procéder à une telle mise en demeure afin que disparaissent de la liste de résultats affichée à la suite d'une recherche les liens en cause.<br/>
<br/>
              3. En premier lieu, il résulte de ce qui a été dit au point précédent que lorsqu'il est saisi de conclusions aux fins d'annulation du refus de la CNIL de mettre en demeure l'exploitant d'un moteur de recherche de procéder au déréférencement de liens, le juge de l'excès de pouvoir est conduit à apprécier la légalité d'un tel refus au regard des règles applicables et des circonstances prévalant à la date de sa décision.<br/>
<br/>
              4. En second lieu, dans l'hypothèse où il apparaît que les liens litigieux ont été déréférencés à la date à laquelle il statue, soit à la seule initiative de l'exploitant du moteur de recherche, soit pour la mise en oeuvre d'une mise en demeure, le juge de l'excès de pouvoir doit constater que le litige porté devant lui a perdu son objet. <br/>
<br/>
              5. Il ressort des pièces du dossier qu'ainsi qu'elle le fait valoir dans ses écritures devant le Conseil d'Etat, la société Google a, postérieurement à l'introduction de la présente requête, procédé au déréférencement du lien en litige. Il découle des motifs énoncés au point précédent que les conclusions dirigées contre le refus de la CNIL d'ordonner à la société Google de procéder au déréférencement de ce lien ont perdu leur objet.<br/>
<br/>
              6. Il résulte de ce qui précède qu'il n'y a plus lieu de statuer sur la requête de Mme X.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                            --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions de la requête de Mme X.<br/>
Article 2 : La présente décision sera notifiée à Mme A..., à la Commission nationale de l'informatique et des libertés et à la société Google LLC.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-07-05 DROITS CIVILS ET INDIVIDUELS. - DROIT AU DÉRÉFÉRENCEMENT [RJ1] - REFUS DE LA CNIL DE METTRE L'EXPLOITANT D'UN MOTEUR DE RECHERCHE EN DEMEURE DE DÉRÉFÉRENCER DES LIENS [RJ2] - 1) REP CONTRE CE REFUS - EFFET UTILE DE L'ANNULATION - INJONCTION À LA CNIL DE PROCÉDER À CETTE MISE EN DEMEURE - 2) CONSÉQUENCES SUR L'OFFICE DU JUGE - A) APPRÉCIATION DE LA LÉGALITÉ DU REFUS AU REGARD DES CIRCONSTANCES ET DES RÈGLES APPLICABLES À LA DATE À LAQUELLE LE JUGE STATUE [RJ3] - B) NON-LIEU SI LE DÉRÉFÉRENCEMENT LITIGIEUX EST INTERVENU ENTRETEMPS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-07 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. DEVOIRS DU JUGE. - DROIT AU DÉRÉFÉRENCEMENT [RJ1] - REFUS DE LA CNIL DE METTRE L'EXPLOITANT D'UN MOTEUR DE RECHERCHE EN DEMEURE DE DÉRÉFÉRENCER DES LIENS [RJ2] - 1) REP CONTRE CE REFUS - EFFET UTILE DE L'ANNULATION - INJONCTION À LA CNIL DE PROCÉDER À CETTE MISE EN DEMEURE - 2) CONSÉQUENCES SUR L'OFFICE DU JUGE - A) APPRÉCIATION DE LA LÉGALITÉ DU REFUS AU REGARD DES CIRCONSTANCES ET DES RÈGLES APPLICABLES À LA DATE À LAQUELLE LE JUGE STATUE [RJ3] - B) NON-LIEU SI LE DÉRÉFÉRENCEMENT LITIGIEUX EST INTERVENU ENTRETEMPS.
</SCT>
<ANA ID="9A"> 26-07-05 1) L'effet utile de l'annulation pour excès de pouvoir du refus de la Commission nationale de l'informatique et des libertés (CNIL) de mettre en demeure l'exploitant d'un moteur de recherche de procéder au déréférencement de liens vers des pages web réside dans l'obligation, que le juge peut prescrire d'office en vertu des dispositions de l'article L. 911-1 du code de justice administrative (CJA), pour la CNIL de procéder à une telle mise en demeure afin que disparaissent de la liste de résultats affichée à la suite d'une recherche les liens en cause.,,,2) a) Il en résulte que lorsqu'il est saisi de conclusions aux fins d'annulation du refus de la CNIL de mettre en demeure l'exploitant d'un moteur de recherche de procéder au déréférencement de liens, le juge de l'excès de pouvoir est conduit à apprécier la légalité d'un tel refus au regard des règles applicables et des circonstances prévalant à la date de sa décision.,,,b) Dans l'hypothèse où il apparaît que les liens litigieux ont été déréférencés à la date à laquelle il statue, soit à la seule initiative de l'exploitant du moteur de recherche, soit pour la mise en oeuvre d'une mise en demeure, le juge de l'excès de pouvoir doit constater que le litige porté devant lui a perdu son objet.</ANA>
<ANA ID="9B"> 54-07-01-07 1) L'effet utile de l'annulation pour excès de pouvoir du refus de la Commission nationale de l'informatique et des libertés (CNIL) de mettre en demeure l'exploitant d'un moteur de recherche de procéder au déréférencement de liens vers des pages web réside dans l'obligation, que le juge peut prescrire d'office en vertu des dispositions de l'article L. 911-1 du code de justice administrative (CJA), pour la CNIL de procéder à une telle mise en demeure afin que disparaissent de la liste de résultats affichée à la suite d'une recherche les liens en cause.,,,2) a) Il en résulte que lorsqu'il est saisi de conclusions aux fins d'annulation du refus de la CNIL de mettre en demeure l'exploitant d'un moteur de recherche de procéder au déréférencement de liens, le juge de l'excès de pouvoir est conduit à apprécier la légalité d'un tel refus au regard des règles applicables et des circonstances prévalant à la date de sa décision.,,,b) Dans l'hypothèse où il apparaît que les liens litigieux ont été déréférencés à la date à laquelle il statue, soit à la seule initiative de l'exploitant du moteur de recherche, soit pour la mise en oeuvre d'une mise en demeure, le juge de l'excès de pouvoir doit constater que le litige porté devant lui a perdu son objet.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur l'existence d'un droit au déréférencement, CJUE, 13 mai 2014, Google Spain SL, Google Inc. contre Agencia Espanola de Proteccion de Datos, Mario Costeja Gonzalez, aff. C-131/12., ,[RJ2] Cf., sur la compétence de la CNIL et le contrôle du juge de l'excès de pouvoir, CE, Assemblée, 24 février 2017,,, n°s 391000 393769 399999 401258, p. 59., ,[RJ3] Rappr., s'agissant du refus d'abroger un acte réglementaire, CE, Assemblée, 19 juillet 2019, Association des Américains accidentels, n°s 424216 424217, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
