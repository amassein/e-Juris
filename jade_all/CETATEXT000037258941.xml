<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037258941</ID>
<ANCIEN_ID>JG_L_2018_07_000000412558</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/25/89/CETATEXT000037258941.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 26/07/2018, 412558</TITRE>
<DATE_DEC>2018-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412558</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>M. Paul Bernard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:412558.20180726</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...Gwabana Massalaa demandé au tribunal administratif de Grenoble d'annuler l'arrêté du 27 mai 2016 par lequel le préfet de l'Isère a refusé de lui délivrer un titre de séjour et l'a obligée à quitter le territoire national en fixant la République démocratique du Congo comme pays de destination en cas d'exécution d'office. Par un jugement n° 1603432 du 7 septembre 2016, le tribunal a fait droit à sa demande. <br/>
<br/>
              Par un arrêt n° 16LY03438 du 18 mai 2017, la cour administrative d'appel de Lyon a rejeté l'appel du préfet contre ce jugement. <br/>
<br/>
              Par un pourvoi, enregistré le 18 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, le ministre d'Etat, ministre de l'intérieur, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à l'appel du préfet. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Paul Bernard, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de Mme Gwabana Massala;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 27 mai 2016, le préfet de l'Isère a rejeté la demande de titre de séjour déposée en qualité de conjoint de Français par MmeB..., ressortissante de la République démocratique du Congo et l'a obligée à quitter le territoire français sans délai ; que, par un jugement du 7 septembre 2016, le tribunal administratif de Grenoble a annulé cet arrêté et enjoint à l'autorité administrative de délivrer à Mme Gwabana Massalaune carte de séjour temporaire sur le fondement du 4° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans le délai de deux mois à compter de la notification du jugement, sous astreinte de 100 euros par jour de retard ; que, par un arrêt du 18 mai 2017, la cour administrative d'appel de Lyon a rejeté l'appel du préfet contre ce jugement ; que le ministre d'Etat, ministre de l'intérieur se pourvoit contre cet arrêt ;<br/>
<br/>
              2.	Considérant qu'aux termes de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction alors en vigueur : " Sauf si sa présence constitue une menace pour l'ordre public, la carte de séjour temporaire portant la mention "vie privée et familiale" est délivrée de plein droit : / (...) 4° A l'étranger ne vivant pas en état de polygamie, marié avec un ressortissant de nationalité française, à condition que la communauté de vie n'ait pas cessé depuis le mariage, que le conjoint ait conservé la nationalité française et, lorsque le mariage a été célébré à l'étranger, qu'il ait été transcrit préalablement sur les registres de l'état civil français (...) " ; qu'aux termes de l'article L. 311-7 du même code alors en vigueur : " Sous réserve des engagements internationaux de la France et des exceptions prévues par les dispositions législatives du présent code, l'octroi de la carte de séjour temporaire et celui de la carte de séjour "compétences et talents" sont subordonnés à la production par l'étranger d'un visa pour un séjour d'une durée supérieure à trois mois. " ; qu'aux termes de l'article L. 211-2-1 du même code : " (...) Le visa de long séjour ne peut être refusé à un conjoint de Français qu'en cas de fraude, d'annulation du mariage ou de menace à l'ordre public. (...) Lorsque la demande de visa de long séjour émane d'un étranger entré régulièrement en France, marié en France avec un ressortissant de nationalité française et que le demandeur séjourne en France depuis plus de six mois avec son conjoint, la demande de visa de long séjour est présentée à l'autorité administrative compétente pour la délivrance d'un titre de séjour (...) " ; qu'enfin, aux termes de l'article R. 311-2 du même code : " la demande est présentée par l'intéressé dans les deux mois de son entrée en France. S'il y séjournait déjà, il présente sa demande : (...) / 4° Soit dans le courant des deux derniers mois précédant l'expiration de la carte de séjour dont il est titulaire. (...). A l'échéance de ce délai et en l'absence de présentation de demande de renouvellement de sa carte de séjour, il justifie à nouveau des conditions requises pour l'entrée sur le territoire national lorsque la possession d'un visa est requise pour la première délivrance de la carte de séjour. " ;<br/>
<br/>
              3.	Considérant qu'en vertu de ces dispositions, la première délivrance d'une carte de séjour temporaire est en principe, sous réserve des engagements internationaux de la France et des exceptions prévues par la loi, subordonnée à la production par l'étranger d'un visa d'une durée supérieure à trois mois ; qu'il en va différemment pour l'étranger déjà admis à séjourner en France et qui sollicite le renouvellement, même sur un autre fondement, de la carte de séjour temporaire dont il est titulaire ; que lorsqu'un étranger présente, après l'expiration du délai de renouvellement du titre qu'il détenait précédemment, une nouvelle demande de titre de séjour, cette demande de titre doit être regardée comme une première demande à laquelle la condition de la détention d'un visa de long séjour peut être opposée ; que, s'agissant d'un conjoint de Français, l'octroi de ce visa est de droit, sauf en cas de fraude, d'annulation du mariage ou de menace à l'ordre public ; qu'enfin, lorsque la durée de séjour en France de l'étranger avec son conjoint de nationalité française est supérieure à six mois et qu'il justifie d'une entrée régulière, sa demande de visa est déposée en France auprès de l'autorité compétente pour examiner sa demande de titre de séjour ; <br/>
<br/>
              4.	Considérant qu'il ressort des énonciations de l'arrêt attaqué que Mme Gwabana Massala a déclaré être entrée irrégulièrement en France le 13 août 2008 ; qu'elle a alors présenté une demande d'asile, qui a été définitivement rejetée par une décision du 11 juin 2010 de la Cour nationale du droit d'asile ; que, par un arrêté du 2 septembre 2010, le préfet de l'Isère a refusé de lui délivrer un titre de séjour et l'a obligée à quitter le territoire français ; que la demande dirigée par Mme Gwabana Massalacontre cet arrêté devant le tribunal administratif de Grenoble a été rejetée par un jugement du 23 décembre 2010 devenu définitif ;  qu'une carte de séjour temporaire pour raison de santé lui a été délivrée à compter du 29 novembre 2011 et a été renouvelée jusqu'au 28 novembre 2013 ; que, toutefois, sa demande de renouvellement de ce titre a été rejetée par un arrêté du 12 février 2015 du préfet de l'Isère, qui a assorti son  refus d'une obligation de quitter le territoire français ; que Mme Gwabana Massalaa contesté la légalité de cet arrêté devant le tribunal administratif de Grenoble, qui a rejeté sa demande par un jugement du 10 décembre 2015 devenu définitif ; que, s'étant mariée avec un ressortissant français, elle a, le 1er avril 2016, sollicité la délivrance d'un titre de séjour en qualité de conjoint de Français ;<br/>
<br/>
              5.	Considérant que, pour confirmer l'annulation du refus du préfet de l'Isère de délivrer ce titre de séjour, la cour, qui avait relevé que Mme Gwabana Massalaavait présenté une demande de titre de séjour en qualité de conjoint de Français plus d'un an après l'arrêté du préfet de l'Isère refusant de renouveler le titre d'étranger malade dont elle était titulaire et lui ordonnant de quitter la France, a jugé que la délivrance des titres en qualité d'étranger malade avait eu pour effet de régulariser sa situation quant aux conditions de son entrée en France pour l'application de l'article L. 211-2-1 du code de l'entrée et du séjour des étrangers et du droit d'asile et que le préfet ne pouvait plus lui opposer son entrée irrégulière en France pour refuser de lui délivrer un visa de long séjour sur le fondement de ces dispositions ni, ensuite, lui opposer l'absence d'un tel visa ; qu'il ressort des pièces du dossier soumis aux juges du fond qu'une carte de séjour temporaire a été délivrée à Mme Gwabana Massalaen 2011 au titre du 11° de l'article L. 313-11 précité du code de l'entrée et du séjour des étrangers et du droit d'asile ; que ce titre a fait l'objet d'un renouvellement ; que l'intéressée a ensuite continûment séjourné en France ; que, par suite, la cour administrative d'appel de Lyon a pu, sans commettre d'erreur de droit, juger que, pour l'application des dispositions précitées de l'article L. 211-2-1, la condition de régularité de l'entrée en France était remplie ; que, dès lors, le ministre d'Etat, ministre de l'intérieur, n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ; <br/>
<br/>
              6.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'État, une somme de 3 000 euros à verser à Mme Gwabana Massalaau titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi du ministre d'Etat, ministre de l'intérieur est rejeté. <br/>
<br/>
Article 2 : L'État versera une somme de 3 000 euros à Mme Gwabana Massalaau titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre d'Etat, ministre de l'intérieur et à Mme A...Gwabana Massala. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-01-02 ÉTRANGERS. SÉJOUR DES ÉTRANGERS. AUTORISATION DE SÉJOUR. - VISA DE LONG SÉJOUR DÉLIVRÉ AUX CONJOINTS DE FRANÇAIS RÉGULIÈREMENT ENTRÉS SUR LE TERRITOIRE (ART. L. 211-2-1 DU CESEDA) - REQUÉRANTE ENTRÉE IRRÉGULIÈREMENT EN FRANCE MAIS AYANT ENSUITE BÉNÉFICIÉ D'UNE CARTE DE SÉJOUR TEMPORAIRE - RÉGULARISATION DE LA SITUATION DE L'INTÉRESSÉE QUANT AUX CONDITIONS DE SON ENTRÉE EN FRANCE - EXISTENCE.
</SCT>
<ANA ID="9A"> 335-01-02 Recours dirigé contre un refus de délivrance d'un titre de séjour en qualité de conjointe de Français opposé à une requérante étant par le passé entrée irrégulièrement sur le territoire français, ayant ensuite bénéficié d'un titre de séjour d'étranger malade et s'étant vue opposer un refus de renouvellement de ce titre assorti d'une obligation de quitter le territoire français.... ,,La délivrance d'un titre de séjour en qualité d'étranger malade a eu pour effet de régulariser la situation de la requérante quant aux conditions de son entrée en France pour l'application de l'article L. 211-2-1 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA). Le préfet ne pouvait plus, par suite, lui opposer son entrée irrégulière en France pour refuser de lui délivrer un visa de long séjour sur le fondement de ces dispositions, ni, ensuite, lui opposer l'absence d'un tel visa pour refuser de lui délivrer un titre de séjour en qualité de conjointe de Français sur le fondement du 4° de l'article L. 313-11 du CESEDA.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
