<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028717833</ID>
<ANCIEN_ID>JG_L_2014_03_000000350646</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/71/78/CETATEXT000028717833.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 12/03/2014, 350646</TITRE>
<DATE_DEC>2014-03-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>350646</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROGER, SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Florian Blazy</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:350646.20140312</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 6 juillet 2011 au secrétariat du contentieux du Conseil d'Etat, présenté pour M. B...A..., demeurant au ...; M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10BX01902 du 8 février 2011 par lequel la cour administrative d'appel de Bordeaux a annulé le jugement du 22 juin 2010 du tribunal administratif de Toulouse en tant qu'il a annulé la décision du préfet de la Haute-Garonne du 24 février 2010 refusant sa demande de titre de séjour ; <br/>
<br/>
              2°) de rejeter l'appel formé par le préfet de la Haute-Garonne contre ce jugement ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros au titre de article 37 de la loi du 10 juillet 1991 relative à l'aide juridique ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu l'accord franco-algérien modifié du 27 décembre 1968 ;<br/>
<br/>
              Vu la convention sur les droits de l'enfant signée à New-York le 26 janvier 1990 ; <br/>
<br/>
              Vu le code d'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu la loi n° 321-2000 du 12 avril 2000 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Blazy, Maître des Requêtes,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Roger, Sevaux, Mathonnet, avocat de M. A...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.A..., ressortissant algérien né le 8 juillet 1966, est entré régulièrement sur le territoire français le 27 janvier 1990 ; qu'il est revenu dans les mêmes conditions le 9 mars 1992 et le 9 juillet 2001 ; qu'après le rejet d'une demande d'asile conventionnel le 10 juin 2002 et d'une demande d'asile territorial le 29 novembre 2002, il a fait l'objet d'un arrêté préfectoral de refus de séjour assorti d'une invitation à quitter le territoire français qui lui a été notifié le 12 juin 2003 ; qu'après six années passées en Algérie, M. A...est revenu en France le 28 février 2009, accompagné de sa femme et de l'un de ses enfants mineurs ; que le 20 avril 2009, en raison d'un état dépressif sévère, il a sollicité son admission au séjour en France en tant qu'étranger malade, sur le fondement du 7° de l'article 6 de l'accord franco-algérien ; que le médecin inspecteur de santé publique a rendu le 20 janvier 2010, au vu du dossier de M. A..., un avis aux termes duquel : " L'état de santé du demandeur nécessite une prise en charge médicale./ Le défaut de prise en charge peut entraîner des conséquences d'une exceptionnelle gravité. /L'offre de soins pour la pathologie dont souffre l'intéressé existe dans son pays d'origine. /Les soins nécessités par son état de santé doivent, en l'état actuel, être poursuivis pendant un an. " ; que cet avis n'indiquait pas si l'état de santé de M. A...lui permettait de voyager sans risque vers son pays de renvoi ;<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              2. Considérant qu'aux termes du 7° de l'article 6 de l'accord franco-algérien du 27 décembre 1968 " Le certificat de résidence d'un an portant la mention " vie privée et familiale " est délivré de plein droit : (...) 7) au ressortissant algérien, résidant habituellement en France, dont l'état de santé nécessite une prise en charge médicale dont le défaut pourrait entraîner pour lui des conséquences d'une exceptionnelle gravité, sous réserve qu'il ne puisse pas effectivement bénéficier d'un traitement approprié dans son pays. " ; qu'aux termes de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Sauf si sa présence constitue une menace pour l'ordre public, la carte de séjour temporaire portant la mention "vie privée et familiale" est délivrée de plein droit : (...) 11° A l'étranger résidant habituellement en France dont l'état de santé nécessite une prise en charge médicale dont le défaut pourrait entraîner pour lui des conséquences d'une exceptionnelle gravité, sous réserve qu'il ne puisse effectivement bénéficier d'un traitement approprié dans le pays dont il est originaire (...). La décision de délivrer la carte de séjour est prise par l'autorité administrative, après avis du médecin inspecteur de santé publique compétent au regard du lieu de résidence de l'intéressé ou, à Paris, du médecin, chef du service médical de la préfecture de police. " ; qu'aux termes de l'article R. 313-22 du même code en vigueur à la date de la décision litigieuse : " Pour l'application du 11° de l'article L. 313-11, le préfet délivre la carte de séjour temporaire au vu d'un avis émis par le médecin inspecteur départemental de santé publique compétent au regard du lieu de résidence de l'intéressé et, à Paris, par le médecin, chef du service médical de la préfecture de police. / L'avis est émis dans les conditions fixées par arrêté du ministre chargé de la population et des migrations, du ministre chargé de la santé et du ministre de l'intérieur, au vu, d'une part, d'un rapport médical établi par un médecin agréé ou un praticien hospitalier et, d'autre part, des informations disponibles sur les possibilités de traitement dans le pays d'origine de l'intéressé. " ; qu'aux termes de l'article 1er de l'arrêté du 8 juillet 1999, pris pour l'application de ces dispositions : " L'étranger qui a déposé une demande de délivrance ou de renouvellement de carte de séjour temporaire en application de l'article 12 bis (11°) ou qui invoque les dispositions de l'article 25 (8°) de l'ordonnance n° 45-2658 du 2 novembre 1945 est tenu de faire établir un rapport médical relatif à son état de santé par un médecin agréé ou un praticien hospitalier. " ; qu'aux termes de l'article 3 du même arrêté : " (...) le médecin agréé ou le praticien hospitalier établit un rapport précisant le diagnostic des pathologies en cours, le traitement suivi et sa durée prévisible ainsi que les perspectives d'évolution et, éventuellement, la possibilité de traitement dans le pays d'origine. Ce rapport médical est transmis, sous pli confidentiel, au médecin inspecteur de santé publique de la direction départementale des affaires sanitaires et sociales dont relève la résidence de l'intéressé " ; que l'article 4 du même arrêté prévoit que : " Au vu de ce rapport médical et des informations dont il dispose, le médecin inspecteur de santé publique de la direction départementale des affaires sanitaires et sociales émet un avis précisant : / - si l'état de santé de l'étranger nécessite ou non une prise en charge médicale ; / - si le défaut de cette prise en charge peut ou non entraîner des conséquences d'une exceptionnelle gravité sur son état de santé ; / - si l'intéressé peut effectivement ou non bénéficier d'un traitement approprié dans le pays dont il est originaire ; /- et la durée prévisible du traitement. / Il indique, en outre, si l'état de santé de l'étranger lui permet de voyager sans risque vers son pays de renvoi. /. Cet avis est transmis au préfet par le directeur départemental des affaires sanitaires et sociales. (...) " ;<br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que le moyen tiré de ce que l'avis du médecin inspecteur de la santé publique serait insuffisamment motivé peut être utilement invoqué pour contester la légalité tant d'un refus de délivrance ou de renouvellement d'un titre de séjour que d'une mesure d'éloignement ; que, par suite, en jugeant que le moyen tiré de ce que l'avis du médecin inspecteur de santé publique était insuffisamment motivé, ne pouvait être utilement invoqué à l'encontre d'une décision refusant la délivrance d'un titre de séjour,  la cour administrative d'appel a commis une erreur de droit ; qu'il s'ensuit que M. A...est fondé à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant que, pour annuler la décision de refus de titre de séjour du 24 février 2010, les premiers juges se sont fondés sur le motif tiré de ce que l'avis du médecin inspecteur de santé publique ne comportait aucune indication sur la possibilité pour l'intéressé de voyager sans risque vers l'Algérie ; que, toutefois, il ressort des pièces du dossier que le médecin inspecteur de santé publique n'avait pas, en l'espèce, à motiver son avis sur la capacité de l'intéressé à supporter ce voyage, en l'absence de toute contestation portant sur ce point ; qu'ainsi, contrairement à ce que soutient M.A..., le préfet de la Haute-Garonne pouvait légalement prendre une décision de refus de séjour au vu de l'avis rendu par le médecin inspecteur de santé publique, même si celui-ci ne comportait pas d'indication sur la possibilité pour M. A...de voyager sans risque vers son pays ; que, par suite, le préfet de la Haute-Garonne est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Toulouse s'est fondé sur le moyen tiré de l'insuffisance de motivation de l'avis du médecin inspecteur de santé publique pour annuler son arrêté du 24 février 2010 refusant la demande de titre de séjour de M. A...;<br/>
<br/>
              6. Considérant toutefois qu'il appartient au Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner les autres moyens soulevés par M. A...devant le tribunal administratif de Toulouse ;<br/>
<br/>
              Sur les conclusions à fin d'annulation : <br/>
<br/>
              7. Considérant, en premier lieu, que la décision de refus de titre de séjour contestée comporte l'énoncé des considérations de droit et de fait sur lesquelles elle se fonde ; que, dès lors, le moyen tiré de ce que cette décision serait insuffisamment motivée et méconnaîtrait ainsi l'article 3 de la loi du 11 juillet 1979 doit être écarté ;<br/>
<br/>
              8. Considérant, en deuxième lieu, qu'aux termes de l'article 24 de la loi du 12 avril 2000 : " Exception faite des cas où il est statué sur une demande, les décisions individuelles qui doivent être motivées en application des articles 1er et 2 de la loi n° 79-587 du 11 juillet 1979 (...) n'interviennent qu' après que la personne intéressée a été mise à même de présenter des observations écrites, et, le cas échéant, des observations orales (...) " ; que la décision contestée a été prise sur une demande de M. A...formée auprès de la préfecture de la Haute-Garonne le 20 avril 2009 ; que, par suite, M. A...n'est pas fondé à se prévaloir de la méconnaissance de la procédure prévue par les dispositions précitées, qui ne lui étaient pas applicables ;<br/>
<br/>
              9. Considérant, en troisième lieu, que la circonstance que l'arrêté contesté soit numéroté 2009-31-226 alors que l'avis du médecin inspecteur de santé publique a été émis le 20 janvier 2010 ne suffit pas à révéler un défaut d'examen de la situation personnelle de M. A..., dès lors que ce même arrêté, pris le 24 février 2010, reprend avec précision les éléments de sa situation personnelle et familiale ; <br/>
<br/>
              10. Considérant, en quatrième lieu, qu'il résulte des dispositions précédemment citées qu'il appartient à l'autorité administrative, lorsqu'elle envisage de refuser la délivrance d'un titre de séjour à un étranger qui en fait la demande au titre des stipulations du 7) de l'article 6 de l'accord franco-algérien, de vérifier, au vu de l'avis émis par le médecin mentionné à l'article R. 313-22 du code de l'entrée et du séjour des étrangers et du droit d'asile, que cette décision ne peut avoir de conséquences d'une exceptionnelle gravité sur l'état de santé de l'intéressé et, en particulier, d'apprécier, sous le contrôle du juge de l'excès de pouvoir, la nature et la gravité des risques qu'entraînerait un défaut de prise en charge médicale dans le pays dont l'étranger est originaire ; que lorsque le défaut de prise en charge médicale risque d'avoir des conséquences d'une exceptionnelle gravité sur la santé de l'intéressé, l'autorité administrative ne peut légalement refuser le titre de séjour sollicité que s'il existe des possibilités de traitement approprié de l'affection en cause dans son pays d'origine ; que si de telles possibilités existent mais que l'étranger fait valoir qu'il ne peut en bénéficier, soit parce qu'elles ne sont pas accessibles à la généralité de la population, eu égard notamment aux coûts du traitement ou à l'absence de modes de prise en charge adaptés, soit parce qu'en dépit de leur accessibilité, des circonstances exceptionnelles tirées des particularités de sa situation personnelle l'empêcheraient d'y accéder effectivement, il appartient à cette même autorité, au vu de l'ensemble des informations dont elle dispose, d'apprécier si l'intéressé peut ou non bénéficier effectivement d'un traitement approprié dans son pays d'origine ;<br/>
<br/>
              11. Considérant que, si M. A...soutient qu'il ne peut bénéficier d'aucun traitement et être suivi par un spécialiste en Algérie, il ressort des pièces du dossier, notamment de l'avis du médecin inspecteur de santé publique en date du 20 janvier 2010 transmis au préfet de la Haute-Garonne, lequel est suffisamment motivé ainsi qu'il a été indiqué précédemment, que l'offre de soins pour la pathologie dont M. A...est atteint existe dans son pays d'origine ; que l'intéressé fait également valoir qu'en raison de son coût estimé à 10 000 dinars, soit les deux tiers du salaire minimum algérien et de son absence de revenus liée à une incapacité totale de travail, il ne pourra pas effectivement bénéficier du traitement approprié ; que, toutefois, il ressort des pièces du dossier que le régime algérien de sécurité sociale prévoit la prise en charge des soins des personnes handicapées n'exerçant aucune activité ; qu'ainsi, en estimant que M. A...  pourrait effectivement bénéficier de soins en Algérie alors que son état de santé ne s'est dégradé que postérieurement à la décision contestée, le préfet de la Haute-Garonne n'a pas entaché sa décision d'une erreur de droit et n'a pas fait une inexacte application du 7) de l'article 6 de l'accord franco-algérien ;<br/>
<br/>
              12. Considérant, en cinquième lieu, qu'aux termes de l'article 6 de l'accord franco-algérien : " Le certificat de résidence d'un an portant la mention " vie privée et familiale " est délivré de plein droit : (...) 5) au ressortissant algérien, qui n'entre pas dans les catégories précédentes ou dans celles qui ouvrent droit au regroupement familial, dont les liens personnels et familiaux en France sont tels que le refus d'autoriser son séjour porterait à son droit au respect de sa vie privée et familiale une atteinte disproportionnée au regard des motifs du refus " ; qu'aux termes de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " 1° Toute personne a droit au respect de sa vie privée et familiale, de son domicile et de sa correspondance ; 2° Il ne peut y avoir ingérence d'une autorité publique dans l'exercice de ce droit que pour autant que cette ingérence est prévue par la loi et qu'elle constitue une mesure qui, dans une société démocratique, est nécessaire à la sécurité nationale, à la sûreté publique, au bien-être économique du pays, à la défense de l'ordre et à la prévention des infractions pénales, à la protection de la santé ou de la morale ou à la protection des droits et libertés d'autrui. " ;<br/>
<br/>
              13. Considérant que, si M. A...soutient qu'il vit en France avec son épouse, ressortissante algérienne, qu'ils ont eu un enfant né le 20 janvier 2010 et qu'il est bien intégré en France, il ressort des pièces du dossier que son entrée en France est récente et que son épouse est également en situation irrégulière ; qu'aucun élément ne fait obstacle à ce qu'il soit renvoyé avec son épouse et leur enfant en Algérie où résident deux de leurs enfants ; qu'ainsi, la décision n'a pas porté au droit de M. A...au respect de sa vie privée et familiale une atteinte disproportionnée au regard des buts en vue desquels elle a été prise ; que, par suite, elle n'a méconnu ni les stipulations du 5) de l'article 6 de l'accord franco-algérien ni celles de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; qu'elle n'est pas davantage entachée d'une erreur manifeste quant à l'appréciation des conséquences sur sa situation personnelle ;<br/>
<br/>
              14. Considérant, en sixième et dernier lieu, qu'aux termes de l'article 3-1 de la convention signée à New York le 26 janvier 1990 : " Dans toutes les décisions qui concernent les enfants, (...) l'intérêt de l'enfant doit être une considération primordiale " ; qu'il ressort des pièces du dossier que, ainsi qu'il a été dit ci-dessus, l'épouse de M. A...est également en situation irrégulière et que rien fait obstacle à ce que leur enfant et elle-même repartent avec lui ; que, dès lors, M. A...n 'est pas fondé à soutenir que l'intérêt supérieur de son enfant n'aurait pas été suffisamment pris en compte ;<br/>
<br/>
              15. Considérant qu'il résulte de tout ce qui précède que le ministre de l'intérieur est fondé à demander l'annulation du jugement attaqué en tant qu'il a annulé son arrêté du 24 février 2010 refusant à M. A...la délivrance d'un titre de séjour ;<br/>
<br/>
              Sur les conclusions aux fins d'injonction et d'astreinte :<br/>
<br/>
              16. Considérant que la présente décision, qui rejette les conclusions à fin d'annulation, n'appelle aucune mesure d'exécution ; que, par suite, les conclusions de M. A... aux fins d'injonction et d'astreinte doivent être rejetées ;<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative et de l'article 37 alinéa 2 de la loi du 10 juillet 1991 :<br/>
<br/>
              17. Considérant que les dispositions de ces articles font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas dans la présente instance la partie perdante, la somme que M. A...demande, au profit de son avocat, au titre des frais exposés et non compris dans les dépens ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt du 8 février 2011 de la cour administrative d'appel de Bordeaux et les articles 2 et 4 du jugement du 22 juin 2010 du tribunal administratif de Toulouse sont annulés. <br/>
Article 2 : La demande présentée par M. A...devant le Tribunal administratif de Toulouse tendant à l'annulation de l'arrêté du préfet de Haute-Garonne en date du 24 juin 2010 est rejetée. <br/>
Article 3 : Les conclusions de M. A...aux fins d'injonction et d'astreinte et celles tendant à l'application de l'article 37 alinéa 2 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à Monsieur B...A...et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-01-02-04 ÉTRANGERS. SÉJOUR DES ÉTRANGERS. AUTORISATION DE SÉJOUR. REFUS DE RENOUVELLEMENT. - AVIS DU MÉDECIN INSPECTEUR DE LA SANTÉ PUBLIQUE - MOTIVATION - 1) MOYEN TIRÉ DE L'INSUFFISANCE DE MOTIVATION DE CET AVIS - OPÉRANCE À L'ÉGARD D'UN REFUS DE RENOUVELLEMENT D'UN TITRE DE SÉJOUR - EXISTENCE - 2) OBLIGATION, À PEINE D'IRRÉGULARITÉ, DE COMPORTER DES INDICATIONS SUR LA CAPACITÉ DE L'INTÉRESSÉ À SUPPORTER LE VOYAGE DE RETOUR - ABSENCE, EN L'ABSENCE DE TOUTE CONTESTATION SUR CE POINT.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">335-01-03-02 ÉTRANGERS. SÉJOUR DES ÉTRANGERS. REFUS DE SÉJOUR. PROCÉDURE. - AVIS DU MÉDECIN INSPECTEUR DE LA SANTÉ PUBLIQUE - MOTIVATION - 1) MOYEN TIRÉ DE L'INSUFFISANCE DE MOTIVATION DE CET AVIS - OPÉRANCE À L'ÉGARD D'UN REFUS DE DÉLIVRANCE D'UN TITRE DE SÉJOUR - EXISTENCE - 2) OBLIGATION, À PEINE D'IRRÉGULARITÉ, DE COMPORTER DES INDICATIONS SUR LA CAPACITÉ DE L'INTÉRESSÉ À SUPPORTER LE VOYAGE DE RETOUR - ABSENCE, EN L'ABSENCE DE TOUTE CONTESTATION SUR CE POINT.
</SCT>
<ANA ID="9A"> 335-01-02-04 1) L'insuffisance de motivation de l'avis du médecin inspecteur de la santé publique peut être utilement invoquée non seulement à l'égard d'une mesure d'éloignement, mais aussi à l'encontre d'un refus de délivrance ou de renouvellement d'un titre de séjour.,,,2) Cet avis n'a toutefois pas à comporter, à peine d'irrégularité de ce refus, des indications sur la capacité de l'intéressé à supporter le voyage de retour en l'absence de toute contestation sur ce point.</ANA>
<ANA ID="9B"> 335-01-03-02 1) L'insuffisance de motivation de l'avis du médecin inspecteur de la santé publique peut être utilement invoquée non seulement à l'égard d'une mesure d'éloignement, mais aussi à l'encontre d'un refus de délivrance ou de renouvellement d'un titre de séjour.,,,2) Cet avis n'a toutefois pas à comporter, à peine d'irrégularité de ce refus, des indications sur la capacité de l'intéressé à supporter le voyage de retour en l'absence de toute contestation sur ce point.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
