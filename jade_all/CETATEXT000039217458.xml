<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039217458</ID>
<ANCIEN_ID>JG_L_2019_10_000000432543</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/21/74/CETATEXT000039217458.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 14/10/2019, 432543</TITRE>
<DATE_DEC>2019-10-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432543</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2019:432543.20191014</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement n° 1809634 du 11 juillet 2019, enregistré le lendemain au secrétariat du contentieux du Conseil d'Etat, le tribunal administratif de Lille, avant de statuer sur la demande de M. A... B... tendant à l'annulation de la décision du 23 août 2018 par laquelle le président du conseil départemental du Nord a confirmé la récupération d'un indu de revenu de solidarité active de 8 255,02 euros pour la période de mai 2015 à janvier 2017, a décidé, en application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen les questions suivantes :<br/>
<br/>
              1°) En matière de contentieux sociaux, le défendeur peut-il, à l'appui de la communication, par l'intermédiaire de l'application Télérecours, du dossier constitué pour l'instruction de la demande tendant à l'attribution de la prestation ou de l'allocation ou à la reconnaissance du droit dont la transmission au tribunal administratif lui incombe en application des dispositions de l'article R. 772 8 du code de justice administrative, se dispenser d'en établir un inventaire détaillé '<br/>
<br/>
              2°) Ce dossier peut-il être regardé comme constituant, eu égard à l'objet du litige, une série homogène dont chacune des pièces qui le composent n'aurait pas à être individuellement répertoriée par un signet '<br/>
<br/>
              3°) Dans la négative, le dossier dans son ensemble doit-il, en application des dispositions de l'article R. 611-8-2 du code de justice administrative, être écarté des débats à défaut de régularisation par le défendeur dans le délai imparti par la juridiction '<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>REND L'AVIS SUIVANT :<br/>
<br/>
              1. D'une part, les articles R. 772-5 à R 772-10 du code de justice administrative comportent des dispositions particulières applicables à la présentation, à l'instruction et au jugement des requêtes relatives aux prestations, allocations ou droits attribués au titre de l'aide ou de l'action sociale, du logement ou en faveur des travailleurs privés d'emploi. A ce titre, en particulier, l'article R. 772-8 prévoit que : " Lorsque la requête lui est notifiée, le défendeur est tenu de communiquer au tribunal administratif l'ensemble du dossier constitué pour l'instruction de la demande tendant à l'attribution de la prestation ou de l'allocation ou à la reconnaissance du droit, objet de la requête (...) ".<br/>
<br/>
              2. D'autre part, aux termes de l'article R. 412-2 du code de justice administrative : " Lorsque les parties joignent des pièces à l'appui de leurs requêtes et mémoires, elles en établissent simultanément un inventaire détaillé. (...) Ces obligations sont prescrites aux parties sous peine de voir leurs pièces écartées des débats après invitation à régulariser non suivie d'effet ". Aux termes des deux derniers alinéas de l'article R. 611-8-2 du même code, les parties et mandataires inscrits dans l'application Télérecours " doivent adresser tous leurs mémoires et pièces au moyen de celle-ci, sous peine de voir leurs écritures écartées des débats à défaut de régularisation dans un délai imparti par la juridiction. (...) Si les caractéristiques de certains mémoires ou pièces font obstacle à leur communication par voie électronique, ils sont transmis sur support matériel, accompagnés de copies en nombre égal à celui des autres parties augmenté de deux. L'inventaire des pièces transmis par voie électronique en fait mention. / Lorsque les parties et mandataires inscrits dans l'application transmettent, à l'appui de leur mémoire, un fichier unique comprenant plusieurs pièces, chacune d'entre elles doit être répertoriée par un signet la désignant conformément à l'inventaire qui en est dressé. S'ils transmettent un fichier par pièce, l'intitulé de chacun d'entre eux doit être conforme à cet inventaire. Ces obligations sont prescrites aux parties et mandataires inscrits dans l'application sous peine de voir leurs écritures écartées des débats à défaut de régularisation dans un délai imparti par la juridiction ".<br/>
<br/>
              3. Les dispositions, citées au point 2, relatives à l'établissement d'un inventaire détaillé et à la présentation des pièces adressées à la juridiction par le moyen de l'application informatique Télérecours s'appliquent à la transmission des pièces que les parties produisent à l'appui de leurs écritures. Elles n'imposent pas au défendeur qui communique au tribunal administratif, en application de l'article R. 772-8 du code de justice administrative, le dossier constitué pour l'instruction administrative de la demande du requérant d'établir un inventaire des pièces contenues dans ce dossier ni, pour sa communication au moyen de l'application Télérecours, de transmettre un fichier par pièce ou de répertorier chacune de ces pièces, au sein du fichier transmis, par un signet la désignant. <br/>
<br/>
              4. Compte tenu de la réponse apportée aux deux premières questions, la troisième question est dépourvue d'objet.<br/>
<br/>
<br/>
<br/>Le présent avis sera notifié au tribunal administratif de Lille, à M. A... B... et au département du Nord.<br/>
Copie en sera adressée à la ministre des solidarités et de la santé.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-04 AIDE SOCIALE. CONTENTIEUX DE L'AIDE SOCIALE ET DE LA TARIFICATION. - DÉFENDEUR TENU DE COMMUNIQUER LES PIÈCES EN SA POSSESSION (ART. R. 772-8 DU CJA) - DÉFENDEUR SOUMIS À L'EXIGENCE D'UN INVENTAIRE DÉTAILLÉ DES PIÈCES (ART. R. 412-2 DU CJA) [RJ1] - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-08 PROCÉDURE. INTRODUCTION DE L'INSTANCE. FORMES DE LA REQUÊTE. - PRÉSENTATION DE LA REQUÊTE PAR VOIE ÉLECTRONIQUE - FORMES IMPOSÉES À LA REQUÊTE ET AUX PIÈCES QUI Y SONT JOINTES [RJ1] - EXIGENCE D'UN INVENTAIRE DÉTAILLÉ DES PIÈCES JOINTES À LA REQUÊTE (ART. R. 412-2 DU CJA) [RJ1] - EXIGENCE S'IMPOSANT, DANS LES CONTENTIEUX SOCIAUX (ART. R. 772-5 DU CJA), AU DÉFENDEUR TENU DE COMMUNIQUER LES PIÈCES EN SA POSSESSION (ART. R. 772-8 DU CJA) - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-04-01-03 PROCÉDURE. INSTRUCTION. POUVOIRS GÉNÉRAUX D'INSTRUCTION DU JUGE. PRODUCTION ORDONNÉE. - DÉFENDEUR TENU, DANS LES CONTENTIEUX SOCIAUX (ART. R. 772-5 DU CJA), DE COMMUNIQUER LES PIÈCES EN SA POSSESSION (ART. R. 772-8 DU CJA) - DÉFENDEUR SOUMIS À L'EXIGENCE D'UN INVENTAIRE DÉTAILLÉ DES PIÈCES (ART. R. 412-2 DU CJA) [RJ1] - ABSENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-04-03-01 PROCÉDURE. INSTRUCTION. CARACTÈRE CONTRADICTOIRE DE LA PROCÉDURE. COMMUNICATION DES MÉMOIRES ET PIÈCES. - DÉFENDEUR TENU, DANS LES CONTENTIEUX SOCIAUX (ART. R. 772-5 DU CJA), DE COMMUNIQUER LES PIÈCES EN SA POSSESSION (ART. R. 772-8 DU CJA) - DÉFENDEUR SOUMIS À L'EXIGENCE D'UN INVENTAIRE DÉTAILLÉ DES PIÈCES (ART. R. 412-2 DU CJA) [RJ1] - ABSENCE.
</SCT>
<ANA ID="9A"> 04-04 Les dispositions de l'article R. 412-2 et des deux derniers alinéas de l'article R. 611-8-2 du code de justice administrative (CJA), relatives à l'établissement d'un inventaire détaillé et à la présentation des pièces adressées à la juridiction par le moyen de l'application informatique Télérecours, s'appliquent à la transmission des pièces que les parties produisent à l'appui de leurs écritures. Elles n'imposent pas au défendeur qui communique au tribunal administratif, en application de l'article R. 772-8 du CJA qui en matière de contentieux sociaux impose à l'administration de produire les pièces en sa possession, le dossier constitué pour l'instruction administrative de la demande du requérant d'établir un inventaire des pièces contenues dans ce dossier ni, pour sa communication au moyen de l'application Télérecours, de transmettre un fichier par pièce ou de répertorier chacune de ces pièces, au sein du fichier transmis, par un signet la désignant.</ANA>
<ANA ID="9B"> 54-01-08 Les dispositions de l'article R. 412-2 et des deux derniers alinéas de l'article R. 611-8-2 du code de justice administrative (CJA), relatives à l'établissement d'un inventaire détaillé et à la présentation des pièces adressées à la juridiction par le moyen de l'application informatique Télérecours, s'appliquent à la transmission des pièces que les parties produisent à l'appui de leurs écritures. Elles n'imposent pas au défendeur qui communique au tribunal administratif, en application de l'article R. 772-8 du CJA qui en matière de contentieux sociaux impose à l'administration de produire les pièces en sa possession, le dossier constitué pour l'instruction administrative de la demande du requérant d'établir un inventaire des pièces contenues dans ce dossier ni, pour sa communication au moyen de l'application Télérecours, de transmettre un fichier par pièce ou de répertorier chacune de ces pièces, au sein du fichier transmis, par un signet la désignant.</ANA>
<ANA ID="9C"> 54-04-01-03 Les dispositions de l'article R. 412-2 et des deux derniers alinéas de l'article R. 611-8-2 du code de justice administrative (CJA), relatives à l'établissement d'un inventaire détaillé et à la présentation des pièces adressées à la juridiction par le moyen de l'application informatique Télérecours, s'appliquent à la transmission des pièces que les parties produisent à l'appui de leurs écritures. Elles n'imposent pas au défendeur qui communique au tribunal administratif, en application de l'article R. 772-8 du CJA qui en matière de contentieux sociaux impose à l'administration de produire les pièces en sa possession, le dossier constitué pour l'instruction administrative de la demande du requérant d'établir un inventaire des pièces contenues dans ce dossier ni, pour sa communication au moyen de l'application Télérecours, de transmettre un fichier par pièce ou de répertorier chacune de ces pièces, au sein du fichier transmis, par un signet la désignant.</ANA>
<ANA ID="9D"> 54-04-03-01 Les dispositions de l'article R. 412-2 et des deux derniers alinéas de l'article R. 611-8-2 du code de justice administrative (CJA), relatives à l'établissement d'un inventaire détaillé et à la présentation des pièces adressées à la juridiction par le moyen de l'application informatique Télérecours, s'appliquent à la transmission des pièces que les parties produisent à l'appui de leurs écritures. Elles n'imposent pas au défendeur qui communique au tribunal administratif, en application de l'article R. 772-8 du CJA qui en matière de contentieux sociaux impose à l'administration de produire les pièces en sa possession, le dossier constitué pour l'instruction administrative de la demande du requérant d'établir un inventaire des pièces contenues dans ce dossier ni, pour sa communication au moyen de l'application Télérecours, de transmettre un fichier par pièce ou de répertorier chacune de ces pièces, au sein du fichier transmis, par un signet la désignant.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 5 octobre 2018, M.,et autres, n° 418233, p. 367.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
