<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036506401</ID>
<ANCIEN_ID>JG_L_2018_01_000000417084</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/50/64/CETATEXT000036506401.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 15/01/2018, 417084, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-01-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417084</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GOUZ-FITOUSSI, RIDOUX</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:417084.20180115</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...D...et Mme B...C...ont demandé au juge des référés du tribunal administratif de Nice de les admettre à l'aide juridictionnelle provisoire et, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner à l'administration de prendre les dispositions nécessaires à leur mise à l'abri immédiate au titre de l'urgence sociale ou dans le cadre du dispositif d'hébergement des demandeurs d'asile, sous astreinte de 100 euros par jour de retard, et d'enjoindre à l'Office français de l'immigration et de l'intégration ou au préfet de les héberger avec leurs enfants. Par une ordonnance n° 1705661 du 3 janvier 2018, le juge des référés du tribunal administratif de Nice a admis les demandeurs à l'aide juridictionnelle provisoire et a rejeté leurs conclusions présentées au titre de l'article L. 521-2 du code de justice administrative.<br/>
<br/>
              Par une requête, enregistrée le 5 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, M. A...D...et Mme B...C...demandent au juge des référés du Conseil d'Etat de les admettre à l'aide juridictionnelle provisoire et, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à leurs conclusions de première instance présentées au titre de l'article L. 521-2 du code de justice administrative ;<br/>
<br/>
              3°) de mettre à la charge de l'Office français de l'immigration et de l'intégration et de l'Etat la somme de 2 000 euros à verser à chacun des requérants au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Les requérants soutiennent que : <br/>
              - le juge des référés du tribunal administratif de Nice a omis de se prononcer sur l'atteinte, qu'ils invoquaient, portée à leur droit d'accès à un dispositif d'hébergement d'urgence au titre de l'article L. 345-2-2 du code de l'action sociale et des familles ;<br/>
              - c'est à tort que le juge des référés du tribunal administratif de Nice a jugé qu'ils ne pouvaient utilement, avant la date qui leur avait été fixée pour l'enregistrement de leur demande d'asile, invoquer l'atteinte portée à leur droit d'asile que constituerait la privation du bénéfice des conditions matérielles d'accueil qu'il incombe à l'Office français d'immigration et d'intégration d'assurer aux demandeurs d'asile ;<br/>
              - eu égard à leur situation de détresse et de vulnérabilité, le comportement de l'administration revêt le caractère d'une carence caractérisée, constitutive d'une atteinte à leur droit d'accès à un dispositif d'hébergement d'urgence et au droit d'asile ;<br/>
              - ils reprennent l'ensemble des moyens qu'ils avaient soulevés en première instance.<br/>
<br/>
              Par un mémoire en défense, enregistré le 11 janvier 2018, le ministre d'Etat, ministre de l'intérieur conclut au rejet de la requête. Il soutient qu'il n'a qualité que pour présenter des observations et que les moyens présentés par les requérants ne sont pas fondés.<br/>
<br/>
              Par un mémoire en défense, enregistré le 11 janvier 2018, l'Office français de l'immigration et de l'intégration conclut au rejet de la requête. Il soutient que les moyens soulevés par les requérants ne sont pas fondés. <br/>
<br/>
              Par un mémoire en défense, enregistré le 11 janvier 2018, le ministre des solidarités et de la santé conclut à ce qu'il n'y ait pas lieu de statuer sur la requête, les requérants s'étant vu attribuer un hébergement à compter du 11 janvier 2018 et jusqu'à la date fixée pour leur rendez-vous au guichet unique des demandeurs d'asile.<br/>
<br/>
              Par un mémoire en intervention, enregistré le 11 janvier 2018, l'association " la Cimade " demande au Conseil d'Etat de faire droit aux conclusions de la requête.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M.D..., Mme C...et la Cimade, d'autre part, le directeur de l'Office français de l'immigration et de l'intégration, le ministre d'Etat, ministre de l'intérieur et le ministre des solidarités et de la santé ;<br/>
              Vu le procès-verbal de l'audience publique du 12 janvier 2018 à 11 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Gouz-Fitoussi, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. D...et de MmeC... ;<br/>
<br/>
              - le représentant de la Cimade ;<br/>
<br/>
              - les représentants du directeur de l'Office français de l'immigration et de l'intégration ;<br/>
<br/>
              - les représentants du ministre d'Etat, ministre de l'intérieur ;<br/>
<br/>
              - les représentants du ministre des solidarités et de la santé ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 12 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, présentée par M. D...et Mme C...;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. La Cimade, qui intervient au soutien des conclusions de la requête, justifie, eu égard à son objet statutaire et à la nature du litige, d'un intérêt suffisant pour intervenir dans la présente instance. Son intervention est, par suite, recevable.<br/>
<br/>
              2. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures". <br/>
<br/>
              3. Il résulte de l'instruction que M. A...D...et Mme B...C..., de nationalité géorgienne, sont entrés en France avec leurs deux enfants nés en 2007 et 2008 et se sont vu fixer, le 22 décembre 2017, un rendez-vous pour le 31 janvier 2018 au " guichet unique " de la préfecture des Alpes-Maritimes en vue de l'enregistrement de leur demande d'asile. Eu égard aux moyens et à l'objet de leur requête, M. D...et MmeC..., doivent être regardés comme relevant appel de l'article 2 de l'ordonnance du 3 janvier 2018 par lequel le juge des référés du tribunal administratif de Nice a rejeté leurs conclusions, présentées au titre de l'article L. 521-2 du code de justice administrative, tendant à ce qu'il soit ordonné à l'administration de prendre les dispositions nécessaires à leur mise à l'abri immédiate au titre de l'urgence sociale ou dans le cadre du dispositif d'hébergement des demandeurs d'asile sous astreinte de 100 euros par jour de retard et, à ce titre, à ce qu'il soit enjoint à l'Office français de l'immigration et de l'intégration ou au préfet de les héberger avec leurs enfants.<br/>
<br/>
              4. D'une part, le droit constitutionnel d'asile, qui a le caractère d'une liberté fondamentale, a pour corollaire le droit de solliciter le statut de réfugié. L'article L. 741-1 du code de l'entrée et du séjour des étrangers et du droit d'asile prévoit que l'enregistrement de la demande d'asile " a lieu au plus tard trois jours ouvrés après la présentation de la demande à l'autorité administrative compétente, sans condition préalable de domiciliation. Toutefois, ce délai peut être porté à dix jours ouvrés lorsqu'un nombre élevés d'étrangers demandent l'asile simultanément ". Il résulte par ailleurs des dispositions des articles L. 744-1 à L. 744-9 du code de l'entrée et du séjour des étrangers et du droit d'asile que seules les personnes ayant enregistré leur demande d'asile et s'étant vu remettre l'attestation prévue à l'article L. 741-1 du même code sont susceptibles de bénéficier du dispositif national d'accueil proposé à chaque demandeur d'asile par l'Office français de l'immigration et de l'intégration et, notamment, les prestations d'hébergement, d'information, d'accompagnement social et administratif, ainsi que, sous réserve d'en remplir les conditions, l'allocation pour demandeur d'asile et l'accès au marché du travail. Par suite, la privation du bénéfice de ces dispositions en raison d'un délai d'enregistrement de la demande d'asile qui excède les délais légaux mentionnés ci-dessus peut conduire le juge des référés à faire usage des pouvoirs qu'il tient de l'article L. 521-2 du code de justice administrative cité ci-dessus, lorsqu'elle est manifestement illégale et qu'elle comporte en outre des conséquences graves pour le demandeur d'asile.<br/>
<br/>
              5. D'autre part, il appartient aux autorités de l'Etat, sur le fondement des articles L. 345-2, L. 345-2-2, L. 345-2-3 et L. 121-7 du code de l'action sociale et des familles, de mettre en oeuvre le droit à l'hébergement d'urgence reconnu par la loi à toute personne sans abri qui se trouve en situation de détresse médicale, psychique ou sociale. Une carence caractérisée dans l'accomplissement de cette mission peut faire apparaître, pour l'application de l'article L. 521-2 du code de justice administrative, une atteinte grave et manifestement illégale à une liberté fondamentale lorsqu'elle entraîne des conséquences graves pour la personne intéressée. <br/>
<br/>
              6. Toutefois, il résulte de l'instruction que le 11 janvier 2018, soit postérieurement à l'introduction de la requête, l'administration a procuré à M. D...et Mme C...et à leurs enfants une prise en charge au titre de l'hébergement d'urgence à compter du 11 janvier 2018 et jusqu'à la date fixée pour leur rendez-vous au guichet unique des demandeurs d'asile. Dans ces conditions, les conclusions d'appel de M. D...et Mme C... tendant à ce que le juge des référés fasse usage aux mêmes fins des pouvoirs qu'il tient de l'article L. 521-2 du code de justice administrative sont devenues sans objet. Il n'y a, dès lors, pas lieu d'y statuer.<br/>
<br/>
              7. Il n'y a pas lieu d'admettre provisoirement les requérants au bénéfice de l'aide juridictionnelle ni, dans les circonstances de l'espèce, de faire droit à leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'intervention de la Cimade est admise.<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions de M. D...et Mme C...dirigées contre l'article 2 de l'ordonnance du juge des référés du tribunal administratif de Nice du 3 janvier 2018 et tendant à ce que le juge des référés fasse usage des pouvoirs qu'il tient de l'article L. 521-2 du code de justice administrative.<br/>
Article 3 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 4 : La présente décision sera notifiée à M. A...D...et Mme B...C..., au ministre d'Etat, ministre de l'intérieur, à la ministre des affaires sociales et des solidarités, à l'Office français de l'immigration et de l'intégration et à la Cimade.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
