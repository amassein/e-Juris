<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031464417</ID>
<ANCIEN_ID>JG_L_2015_11_000000359548</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/46/44/CETATEXT000031464417.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Assemblée, 09/11/2015, 359548, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-11-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359548</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Assemblée</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Yannick Faure</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEASS:2015:359548.20151109</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La Mutuelle assurance des instituteurs de France (MAIF) et l'association Centre lyrique d'Auvergne ont demandé au tribunal administratif de Clermont-Ferrand de condamner la commune de Clermont-Ferrand, d'une part, à verser à la MAIF la somme de 5 000 euros correspondant au montant de la provision mise à la charge de son assuré, l'association Centre lyrique d'Auvergne, au titre de l'indemnisation des préjudices subis par M. A... B...en raison de l'accident dont il a été victime à la maison de la culture de Clermont-Ferrand le 19 mars 2006 et, d'autre part, à garantir la MAIF de toutes condamnations qui pourraient être mises à sa charge à la suite de cet accident au titre des préjudices subis par M. B.... Par un jugement n° 0902099 du 21 juin 2011, le tribunal administratif de Clermont-Ferrand a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 11LY02021 du 22 mars 2012, la cour administrative d'appel de Lyon a rejeté l'appel formé par la MAIF et l'association Centre lyrique d'Auvergne contre le jugement du tribunal administratif de Clermont-Ferrand du 21 juin 2011.<br/>
<br/>
               Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 21 mai et 17 août 2012 au secrétariat du contentieux du Conseil d'Etat, la MAIF et l'association Centre lyrique d'Auvergne demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt de la cour administrative d'appel de Lyon du 22 mars 2012 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur requête ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Clermont-Ferrand la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yannick Faure, auditeur,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la MAIF et de l'Association Centre Lyrique d'Auvergne et à la SCP Monod, Colin, Stoclet, avocat de la commune de Clermont-Ferrand ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A... B..., employé comme électricien par l'association Centre lyrique d'Auvergne, a été victime le 19 mars 2006 d'un accident du travail dans la salle Jean-Cocteau de la Maison de la culture, mise à la disposition de cette association par la commune de Clermont-Ferrand ; que, par un jugement du 12 novembre 2008, le tribunal de grande instance de Clermont-Ferrand statuant en matière correctionnelle a reconnu l'association et la commune coupables du délit d'atteinte involontaire à l'intégrité de la personne envers M. B... ; que, par un jugement du 26 février 2009, le tribunal des affaires de sécurité sociale du Puy-de-Dôme a jugé que l'accident du travail dont M. B... avait été victime procédait de la faute inexcusable, au sens des dispositions de l'article L. 452-1 du code de la sécurité sociale, de son employeur le Centre lyrique d'Auvergne et a alloué à M. B... une provision de 5 000 euros à régler par la caisse primaire d'assurance maladie, à charge pour cette dernière d'en récupérer le montant auprès du Centre lyrique d'Auvergne ; que par un jugement du 21 juin 2011, le tribunal administratif de Clermont-Ferrand a rejeté la demande de la Mutuelle assurance des instituteurs de France (MAIF), assureur du Centre lyrique d'Auvergne, et de cette association, tendant à ce que la commune de Clermont-Ferrand soit condamnée à verser à cette mutuelle une somme correspondant au montant de la provision mise à la charge de son assuré et à les garantir des condamnations qui pourraient être mises à la charge de ce dernier au titre des préjudices subis par M. B... ; que la MAIF et le Centre lyrique d'Auvergne se pourvoient en cassation contre l'arrêt du 22 mars 2012 par lequel la cour administrative d'appel de Lyon a rejeté leur appel contre ce jugement ;<br/>
<br/>
              2. Considérant qu'en principe, la responsabilité de l'administration peut être engagée à raison de la faute qu'elle a commise, pour autant qu'il en soit résulté un préjudice direct et certain ; que lorsque cette faute et celle d'un tiers ont concouru à la réalisation d'un même dommage, le tiers co-auteur qui, comme en l'espèce, a été condamné par le juge judiciaire à indemniser la victime peut se retourner contre l'administration en invoquant la faute de cette dernière, y compris lorsqu'il a commis une faute inexcusable au sens de l'article L.452-1 du code de la sécurité sociale ; que sa demande a le caractère d'une action subrogatoire fondée sur les droits de la victime à l'égard de l'administration et qu'il peut donc se voir opposer l'ensemble des moyens de défense qui auraient pu l'être à la victime ; qu'en outre, eu égard à l'objet d'une telle action, qui vise à assurer la répartition de la charge de la réparation du dommage entre ses co-auteurs, sa propre faute lui est également opposable ; qu'à ce titre, dans le cas où il a délibérément commis une faute d'une particulière gravité, il ne peut se prévaloir de la faute que l'administration aurait elle-même commise en négligeant de prendre les mesures qui auraient été de nature à l'empêcher de commettre le fait dommageable ; <br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 452-1 du code de la sécurité sociale : " Lorsque l'accident est dû à la faute inexcusable de l'employeur (...), la victime ou ses ayants droit ont droit à une indemnisation complémentaire dans les conditions définies aux articles suivants " ; qu'aux termes de l'article L. 452-2 du même code, dans sa rédaction applicable au litige dont la cour était saisie : " Dans le cas mentionné à l'article précédent, la victime ou ses ayants droit reçoivent une majoration des indemnités qui leur sont dues en vertu du présent livre. (...) La majoration est payée par la caisse, qui en récupère le montant par l'imposition d'une cotisation complémentaire dont le taux et la durée sont fixés par la caisse régionale d'assurance maladie ", devenue la caisse d'assurance retraite et de la santé au travail, " sur la proposition de la caisse primaire, en accord avec l'employeur, sauf recours devant la juridiction de la sécurité sociale compétente (...) " ; qu'aux termes de l'article L. 452-3 du même code : " Indépendamment de la majoration de rente qu'elle reçoit en vertu de l'article précédent, la victime a le droit de demander à l'employeur devant la juridiction de sécurité sociale la réparation du préjudice causé par les souffrances physiques et morales par elle endurées, de ses préjudices esthétiques et d'agrément ainsi que celle du préjudice résultant de la perte ou de la diminution de ses possibilités de promotion professionnelle. (...) / La réparation de ces préjudices est versée directement aux bénéficiaires par la caisse qui en récupère le montant auprès de l'employeur " ; qu'aux termes de l'article L. 452-4 du même code : " A défaut d'accord amiable entre la caisse et la victime ou ses ayants droit d'une part, et l'employeur d'autre part, sur l'existence de la faute inexcusable reprochée à ce dernier, ainsi que sur le montant de la majoration et des indemnités mentionnées à l'article L. 452-3, il appartient à la juridiction de la sécurité sociale compétente, saisie par la victime ou ses ayants droit ou par la caisse primaire d'assurance maladie, d'en décider. (...) / L'auteur de la faute inexcusable est responsable sur son patrimoine personnel des conséquences de celle-ci. / L'employeur peut s'assurer contre les conséquences financières de sa propre faute inexcusable ou de la faute de ceux qu'il s'est substitués dans la direction de l'entreprise ou de l'établissement (...)" ; qu'il résulte de ces dispositions, telles qu'elles sont interprétées par la jurisprudence de la Cour de cassation, que le manquement de l'employeur à l'obligation de sécurité de résultat à laquelle il est tenu envers son salarié, notamment en ce qui concerne les accidents du travail, a le caractère d'une faute inexcusable, au sens de l'article L. 452-1 du code de la sécurité sociale, lorsqu'il avait ou aurait dû avoir conscience du danger auquel était exposé le salarié, et qu'il n'a pas pris les mesures nécessaires pour l'en préserver ;<br/>
<br/>
              4. Considérant que ces dispositions ne font pas obstacle par elles-mêmes, en cas de partage de responsabilité d'un accident du travail avec un tiers, à ce que l'employeur, auteur d'une faute inexcusable, ou son assureur, obtienne le remboursement par ce tiers de la fraction, correspondant à sa part de responsabilité, de la cotisation complémentaire d'accident du travail qui lui a été réclamée à la suite de l'accident en application de l'article L. 452-2 du code de la sécurité sociale, ainsi que de l'indemnisation complémentaire mentionnée à l'article L. 452-3 du même code ; que ce n'est que dans l'hypothèse où l'employeur aurait délibérément commis une faute d'une particulière gravité et où il reprocherait à l'administration d'avoir négligé de prendre les mesures de nature à l'empêcher de commettre le fait dommageable qu'il ne pourrait utilement demander que la charge de la réparation soit partagée par l'administration ; que, par suite, en jugeant, au seul motif que la faute de l'employeur à l'origine de l'accident avait été qualifiée d'inexcusable par le tribunal des affaires de sécurité sociale, que le complément d'indemnisation accordé à M. B...devait demeurer exclusivement à la charge de l'employeur et que la MAIF et le Centre lyrique d'Auvergne n'étaient pas fondés à demander à la commune de Clermont-Ferrand le remboursement de tout ou partie de ce complément, la cour administrative d'appel de Lyon a commis une erreur de droit ; que son arrêt doit, pour ce motif, être annulé ;<br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la MAIF et du Centre lyrique d'Auvergne, qui ne sont pas, dans la présente instance, les parties perdantes ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la commune de Clermont-Ferrand, sur le fondement des mêmes dispositions, le versement à la MAIF et au Centre lyrique d'Auvergne d'une somme de 1 500 euros chacun, au titre des frais exposés par eux et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>
              D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 22 mars 2012 est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
Article 3 : La commune de Clermont-Ferrand versera à la MAIF et au Centre lyrique d'Auvergne une somme de 1 500 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la commune de Clermont-Ferrand présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la Mutuelle assurance des instituteurs de France (MAIF), à l'association Centre lyrique d'Auvergne et à la commune de Clermont-Ferrand.<br/>
Copie en sera adressée à la ministre des affaires sociales, de la santé et des droits des femmes et à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-05-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RECOURS OUVERTS AUX DÉBITEURS DE L'INDEMNITÉ, AUX ASSUREURS DE LA VICTIME ET AUX CAISSES DE SÉCURITÉ SOCIALE. SUBROGATION. - ACTION DE L'AUTEUR D'UN DOMMAGE CONDAMNÉ PAR LE JUGE JUDICIAIRE CONTRE LA COLLECTIVITÉ PUBLIQUE CO-AUTEUR DU DOMMAGE [RJ1] - 1) OUVERTURE - EXISTENCE, Y COMPRIS LORSQU'IL A COMMIS UNE FAUTE INEXCUSABLE AU SENS DE L'ART. L. 452-1 DU CSS [RJ2] - 2) CARACTÈRE SUBROGATOIRE - EXISTENCE - CONSÉQUENCES [RJ3] - 3) OPPOSABILITÉ DE SA PROPRE FAUTE - EXISTENCE - 4) CAS OÙ IL A DÉLIBÉRÉMENT COMMIS UNE FAUTE D'UNE PARTICULIÈRE GRAVITÉ - REJET.
</SCT>
<ANA ID="9A"> 60-05-03 1) Lorsque la faute de l'administration et celle d'un tiers ont concouru à la réalisation d'un même dommage, le tiers co-auteur qui a été condamné par le juge judiciaire à indemniser la victime peut se retourner contre l'administration en invoquant la faute de cette dernière, y compris lorsqu'il a commis une faute inexcusable au sens de l'article L. 452-1 du code de la sécurité sociale (CSS).,,,2) La demande du tiers co-auteur a le caractère d'une action subrogatoire fondée sur les droits de la victime à l'égard de l'administration. Il peut donc se voir opposer l'ensemble des moyens de défense qui auraient pu l'être à la victime.,,,3) Eu égard à l'objet d'une telle action, qui vise à assurer la répartition de la charge de la réparation du dommage entre ses co-auteurs, la propre faute du tiers co-auteur lui est opposable.,,,4) Dans le cas où le tiers co-auteur a délibérément commis une faute d'une particulière gravité, il ne peut se prévaloir de la faute que l'administration aurait elle-même commise en négligeant de prendre les mesures qui auraient été de nature à l'empêcher de commettre le fait dommageable.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. décision du même jour, SAS Constructions mécaniques de Normandie, n° 342468, à publier au Recueil.,,[RJ2]Ab. jur. CE, 18 avril 1984, Société Souchon, n° 34967, p. 167. Cf., sur la faculté de l'employeur auteur d'une faute inexcusable de se retourner contre le tiers coauteur du dommage, Cass. soc., 18 janvier 1996, M. Briotet c/ L'Union des assurances de Paris (UAP), n° 93-15.675, Bull. civ. V, n° 18.,,[RJ3]Cf. CE, 31 décembre 2008, Société Foncière Ariane, n° 294078, p. 498.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
