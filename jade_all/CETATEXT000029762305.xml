<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029762305</ID>
<ANCIEN_ID>JG_L_2014_11_000000357999</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/76/23/CETATEXT000029762305.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 14/11/2014, 357999</TITRE>
<DATE_DEC>2014-11-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357999</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Jean-Baptiste de Froment</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:357999.20141114</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 12VE00447 du 20 mars 2012, enregistrée le 27 mars 2012 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président de la cour administrative d'appel de Versailles a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi présenté à cette cour par Mme B...A...; <br/>
<br/>
              Vu le pourvoi, enregistré le 6 février 2012 au greffe de la cour administrative d'appel de Versailles et le mémoire complémentaire, enregistré le 8 juin 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour MmeA..., demeurant ...; Mme A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement n° 1000325-1000326 du 24 novembre 2011 du tribunal administratif de Montreuil, en tant qu'il a rejeté sa demande tendant à la condamnation de l'Etat à lui verser les sommes de 4 000 et 1 600 euros à titre d'indemnité en réparation des préjudices subis du fait de l'accident de service dont elle a été victime le 11 octobre 2005 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit, dans cette mesure, à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des pensions civiles et militaires de retraite, notamment ses articles 27 et 28 ; <br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984, notamment son article 65 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Baptiste de Froment, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que MmeA..., alors professeur des écoles titulaire, a été victime le 11 octobre 2005 d'un accident dans l'exercice de ses fonctions, une étagère étant tombée sur elle alors qu'elle déplaçait une armoire dans la salle de classe ; que l'accident a été reconnu imputable au service par une décision de l'inspecteur d'académie de la Seine-Saint-Denis du 5 décembre 2005 ; que l'état de santé de l'intéressée s'étant dégradé, compte tenu notamment d'une perte d'audition, la commission de réforme a estimé, le 1er juillet 2008, que cette aggravation n'était pas imputable à l'accident de service ; que, par une décision du 7 juillet 2008, l'inspecteur d'académie de la Seine-Saint-Denis a refusé de reconnaître une telle imputabilité ; que Mme A...a sollicité le 7 décembre 2009 du ministre de l'éducation nationale l'indemnisation de préjudices extra-patrimoniaux, comprenant notamment, d'une part, les troubles dans les conditions d'existence et les souffrances endurées dans la période suivant immédiatement l'accident de service, et, d'autre part, les préjudices liés aux troubles auditifs survenus postérieurement ; que, par un jugement du 24 novembre 2011, le tribunal administratif de Montreuil a, d'une part, annulé la décision du 7 juillet 2008 de l'inspecteur d'académie de la Seine-Saint-Denis pour incompétence et la décision de rejet du recours hiérarchique contre cette décision et, d'autre part, rejeté les conclusions de l'intéressée tendant à ce que l'Etat soit condamné à lui verser une indemnité en réparation des préjudices qu'elle estime avoir subis du fait de son accident de service ; que Mme A...se pourvoit en cassation contre ce jugement en tant qu'il rejette ses conclusions indemnitaires ; <br/>
<br/>
              2. Considérant que les dispositions des articles L. 27 et L. 28 du code des pensions civiles et militaires de retraite et 65 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat qui instituent, en faveur des fonctionnaires victimes d'accidents de service ou de maladies professionnelles, une rente viagère d'invalidité en cas de mise à la retraite et une allocation temporaire d'invalidité en cas de maintien en activité doivent être regardées comme ayant pour objet de réparer les pertes de revenus et l'incidence professionnelle résultant de l'incapacité physique causée par un accident de service ou une maladie professionnelle ; que les dispositions instituant ces prestations déterminent forfaitairement la réparation à laquelle les fonctionnaires concernés peuvent prétendre, au titre de ces chefs de préjudice, dans le cadre de l'obligation qui incombe aux collectivités publiques de garantir leurs agents contre les risques qu'ils peuvent courir dans l'exercice de leurs fonctions ; que ces dispositions ne font en revanche obstacle ni à ce que le fonctionnaire qui subit, du fait de l'invalidité ou de la maladie, des préjudices patrimoniaux d'une autre nature ou des préjudices personnels, obtienne de la personne publique qui l'emploie, même en l'absence de faute de celle-ci, une indemnité complémentaire réparant ces chefs de préjudice ni à ce qu'une action de droit commun pouvant aboutir à la réparation intégrale de l'ensemble du dommage soit engagée contre la collectivité, dans le cas notamment où l'accident ou la maladie serait imputable à une faute de nature à engager la responsabilité de cette collectivité ; <br/>
<br/>
              3. Considérant que la circonstance que le fonctionnaire victime d'un accident de service ou d'une maladie professionnelle ne remplit pas les conditions auxquelles les dispositions mentionnées ci-dessus subordonnent l'obtention d'une rente ou d'une allocation temporaire d'invalidité fait obstacle à ce qu'il prétende, au titre de l'obligation de la collectivité qui l'emploie de le garantir contre les risques courus dans l'exercice de ses fonctions, à une indemnité réparant des pertes de revenus ou une incidence professionnelle ; qu'en revanche, elle ne saurait le priver de la possibilité d'obtenir de cette collectivité la réparation de préjudices d'une autre nature, dès lors qu'ils sont directement liés à l'accident ou à la maladie ; qu'ainsi, en se fondant, pour rejeter la demande indemnitaire de Mme A...tendant à l'indemnisation de ses souffrances physiques et de troubles dans les conditions d'existence liés à l'accident de service qu'elle avait subi, sur la seule circonstance qu'elle n'entrait pas dans le champ des dispositions des articles L. 27 et L. 28 du code des pensions civiles et militaires de retraite et qu'elle n'avait pas été radiée des cadres en raison d'infirmités résultant de blessures contractées en service, le tribunal administratif a commis une erreur de droit ; que Mme A...est par suite fondée à demander l'annulation du jugement attaqué en tant qu'il rejette ses conclusions indemnitaires ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 24 novembre 2011 du tribunal administratif de Montreuil est annulé en tant qu'il rejette les conclusions indemnitaires de MmeA.... <br/>
Article 2 : L'affaire est renvoyée, dans la limite de la cassation ainsi prononcée, au tribunal administratif de Montreuil.<br/>
Article 3 : L'Etat versera à Mme A...la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à Mme B...A...et au ministre de l'éducation nationale.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">48-02-02-04-01 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. PENSIONS CIVILES. PENSIONS OU ALLOCATIONS POUR INVALIDITÉ. ALLOCATION TEMPORAIRE D'INVALIDITÉ PRÉVUE À L'ARTICLE 23 BIS DU STATUT GÉNÉRAL. - FONCTIONNAIRE VICTIME D'UN ACCIDENT DE SERVICE OU ATTEINT D'UNE MALADIE PROFESSIONNELLE - CONDITIONS D'ATTRIBUTION DE L'ALLOCATION TEMPORAIRE D'INVALIDITÉ NON REMPLIES - CONSÉQUENCE - CIRCONSTANCE FAISANT OBSTACLE À LA RÉPARATION, AU TITRE DE LA GARANTIE QUI LUI EST DUE PAR SON EMPLOYEUR, DES PERTES DE REVENU OU DE L'INCIDENCE PROFESSIONNELLE DU DOMMAGE - EXISTENCE - CIRCONSTANCE FAISANT OBSTACLE À LA RÉPARATION D'AUTRES PRÉJUDICES DIRECTEMENT LIÉS À LA MALADIE OU À L'ACCIDENT - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">48-02-02-04-02 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. PENSIONS CIVILES. PENSIONS OU ALLOCATIONS POUR INVALIDITÉ. RENTE VIAGÈRE D'INVALIDITÉ (ARTICLES L. 27 ET L. 28 DU NOUVEAU CODE). - FONCTIONNAIRE VICTIME D'UN ACCIDENT DE SERVICE OU ATTEINT D'UNE MALADIE PROFESSIONNELLE - CONDITIONS D'ATTRIBUTION D'UNE RENTE VIAGÈRE D'INVALIDITÉ NON REMPLIES - CONSÉQUENCE - CIRCONSTANCE FAISANT OBSTACLE À LA RÉPARATION, AU TITRE DE LA GARANTIE QUI LUI EST DUE PAR SON EMPLOYEUR, DES PERTES DE REVENU OU DE L'INCIDENCE PROFESSIONNELLE DU DOMMAGE - EXISTENCE - CIRCONSTANCE FAISANT OBSTACLE À LA RÉPARATION D'AUTRES PRÉJUDICES DIRECTEMENT LIÉS À LA MALADIE OU À L'ACCIDENT - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-01-02-01-02-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. FONDEMENT DE LA RESPONSABILITÉ. RESPONSABILITÉ SANS FAUTE. RESPONSABILITÉ FONDÉE SUR LE RISQUE CRÉÉ PAR CERTAINES ACTIVITÉS DE PUISSANCE PUBLIQUE. RESPONSABILITÉ FONDÉE SUR L'OBLIGATION DE GARANTIR LES COLLABORATEURS DES SERVICES PUBLICS CONTRE LES RISQUES QUE LEUR FAIT COURIR LEUR PARTICIPATION À L'EXÉCUTION DU SERVICE. - FONCTIONNAIRE VICTIME D'UN ACCIDENT DE SERVICE OU ATTEINT D'UNE MALADIE PROFESSIONNELLE - CONDITIONS D'ATTRIBUTION D'UNE RENTE VIAGÈRE D'INVALIDITÉ OU D'UNE ALLOCATION TEMPORAIRE D'INVALIDITÉ NON REMPLIES - CONSÉQUENCE - CIRCONSTANCE FAISANT OBSTACLE À LA RÉPARATION, AU TITRE DE LA GARANTIE QUI LUI EST DUE PAR SON EMPLOYEUR, DES PERTES DE REVENU OU DE L'INCIDENCE PROFESSIONNELLE DU DOMMAGE - EXISTENCE - CIRCONSTANCE FAISANT OBSTACLE À LA RÉPARATION D'AUTRES PRÉJUDICES DIRECTEMENT LIÉS À LA MALADIE OU À L'ACCIDENT - ABSENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">60-04-04-05 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. MODALITÉS DE LA RÉPARATION. CARACTÈRE FORFAITAIRE DE LA PENSION. - FONCTIONNAIRE VICTIME D'UN ACCIDENT DE SERVICE OU ATTEINT D'UNE MALADIE PROFESSIONNELLE - CONDITIONS D'ATTRIBUTION D'UNE RENTE VIAGÈRE D'INVALIDITÉ OU D'UNE ALLOCATION TEMPORAIRE D'INVALIDITÉ NON REMPLIES - CONSÉQUENCE - CIRCONSTANCE FAISANT OBSTACLE À LA RÉPARATION, AU TITRE DE LA GARANTIE QUI LUI EST DUE PAR SON EMPLOYEUR, DES PERTES DE REVENU OU DE L'INCIDENCE PROFESSIONNELLE DU DOMMAGE - EXISTENCE - CIRCONSTANCE FAISANT OBSTACLE À LA RÉPARATION D'AUTRES PRÉJUDICES DIRECTEMENT LIÉS À LA MALADIE OU À L'ACCIDENT - ABSENCE.
</SCT>
<ANA ID="9A"> 48-02-02-04-01 La circonstance que le fonctionnaire victime d'un accident de service ou d'une maladie professionnelle ne remplit pas les conditions auxquelles les dispositions de l'article 65 de la loi n° 84-16 du 11 janvier 1984 subordonnent l'obtention de l'allocation temporaire d'invalidité, fait obstacle à ce qu'il prétende, au titre de l'obligation de la collectivité qui l'emploie de le garantir contre les risques courus dans l'exercice de ses fonctions, à une indemnité réparant des pertes de revenus ou une incidence professionnelle.... ,,En revanche, elle ne saurait le priver de la possibilité d'obtenir de cette collectivité la réparation de préjudices d'une autre nature, dès lors qu'ils sont directement liés à l'accident ou à la maladie.</ANA>
<ANA ID="9B"> 48-02-02-04-02 La circonstance que le fonctionnaire victime d'un accident de service ou d'une maladie professionnelle ne remplit pas les conditions auxquelles les dispositions des articles L. 27 et L. 28 du code des pensions civiles et militaires de retraite subordonnent l'obtention de la rente viagère d'invalidité, fait obstacle à ce qu'il prétende, au titre de l'obligation de la collectivité qui l'emploie de le garantir contre les risques courus dans l'exercice de ses fonctions, à une indemnité réparant des pertes de revenus ou une incidence professionnelle.... ,,En revanche, elle ne saurait le priver de la possibilité d'obtenir de cette collectivité la réparation de préjudices d'une autre nature, dès lors qu'ils sont directement liés à l'accident ou à la maladie.</ANA>
<ANA ID="9C"> 60-01-02-01-02-02 La circonstance que le fonctionnaire victime d'un accident de service ou d'une maladie professionnelle ne remplit pas les conditions auxquelles les dispositions des articles L. 27 et L. 28 du code des pensions civiles et militaires de retraite, s'agissant de la rente viagère d'invalidité, et de l'article 65 de la loi n° 84-16 du 11 janvier 1984, s'agissant de l'allocation temporaire d'invalidité, subordonnent l'obtention de ces prestations, fait obstacle à ce qu'il prétende, au titre de l'obligation de la collectivité qui l'emploie de le garantir contre les risques courus dans l'exercice de ses fonctions, à une indemnité réparant des pertes de revenus ou une incidence professionnelle.... ,,En revanche, elle ne saurait le priver de la possibilité d'obtenir de cette collectivité la réparation de préjudices d'une autre nature, dès lors qu'ils sont directement liés à l'accident ou à la maladie.</ANA>
<ANA ID="9D"> 60-04-04-05 La circonstance que le fonctionnaire victime d'un accident de service ou d'une maladie professionnelle ne remplit pas les conditions auxquelles les dispositions des articles L. 27 et L. 28 du code des pensions civiles et militaires de retraite, s'agissant de la rente viagère d'invalidité, et de l'article 65 de la loi n° 84-16 du 11 janvier 1984, s'agissant de l'allocation temporaire d'invalidité, subordonnent l'obtention de ces prestations, fait obstacle à ce qu'il prétende, au titre de l'obligation de la collectivité qui l'emploie de le garantir contre les risques courus dans l'exercice de ses fonctions, à une indemnité réparant des pertes de revenus ou une incidence professionnelle.... ,,En revanche, elle ne saurait le priver de la possibilité d'obtenir de cette collectivité la réparation de préjudices d'une autre nature, dès lors qu'ils sont directement liés à l'accident ou à la maladie.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
