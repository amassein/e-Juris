<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043677261</ID>
<ANCIEN_ID>JG_L_2021_06_000000432549</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/67/72/CETATEXT000043677261.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 14/06/2021, 432549, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432549</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>Mme Pearl Nguyên Duy</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:432549.20210614</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Mme B... C... a demandé au tribunal administratif de Versailles de condamner l'Office public interdépartemental de l'Essonne, Val-d'Oise et des Yvelines (OPIEVOY), représenté par son liquidateur, à lui verser la somme de 12 000 euros en réparation des préjudices qu'elle estime avoir subis du fait de la décision du 18 mars 2015 par laquelle sa commission d'attribution a rejeté sa candidature à l'attribution d'un logement social. Par un jugement n° 1704036 du 27 mai 2019, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 19VE02308 du 11 juillet 2019 enregistrée le 11 juillet 2019 au secrétariat du Conseil d'Etat, le président de la cour administrative d'appel de Versailles a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 24 juin 2019 au greffe de cette cour, présenté par Mme C.... Par ce pourvoi et un nouveau mémoire, enregistré le 9 mars 2020 au secrétariat du contentieux du Conseil d'Etat, Mme C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande :<br/>
<br/>
              3°) de mettre à la charge de l'OPIEVOY la somme de 1 750 euros à verser à Me A..., son avocat, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la construction et de l'habitation ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pearl Nguyên Duy, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de Mme C... et à Me Le Prado, avocat de l'Office public interdépartemental de l'Essonne du Val-d'Oise et des Yvelines ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond qu'ayant obtenu par un jugement du 20 juillet 2016 du tribunal administratif de Melun, l'annulation de la décision du 18 mars 2015 par laquelle la commission d'attribution des logements de l'Office public interdépartemental de l'Essonne, du Val-d'Oise et des Yvelines (OPIEVOY) a rejeté sa candidature à l'attribution d'un logement social à Villeneuve-le-Roi, Mme C... a demandé en vain à cet office, représenté par son liquidateur, de l'indemniser des préjudices qu'elle estime avoir subis du fait de cette décision illégale puis a saisi le tribunal administratif de Versailles d'un recours tendant à la condamnation de l'OPIEVOY qui a été rejeté par un jugement du 27 mai 2019. La requête par laquelle Mme C... a demandé l'annulation de ce jugement a été transmise au Conseil d'Etat par une ordonnance du président de la cour administrative d'appel de Versailles devant laquelle elle a été initialement enregistrée. <br/>
<br/>
              Sur la compétence du Conseil d'Etat pour connaître du recours de Mme C...: <br/>
<br/>
              2. L'article R. 811-1 du code de justice administrative dispose que : " (...) le tribunal administratif statue en premier et dernier ressort : / 1° Sur les litiges relatifs aux prestations, allocations ou droits attribués au titre de l'aide ou de l'action sociale, du logement ou en faveur des travailleurs privés d'emploi, mentionnés à l'article R. 772-5, y compris le contentieux du droit au logement défini à l'article R. 778-1 ; / (...) ; /  8° Sauf en matière de contrat de la commande publique sur toute action indemnitaire ne relevant pas des dispositions précédentes, lorsque le montant des indemnités demandées n'excède pas le montant déterminé par les articles R. 222-14 et R. 222-15 ; / (...) ".<br/>
<br/>
              3. Des conclusions tendant à l'indemnisation des préjudices résultant de la décision par laquelle un bailleur social refuse d'attribuer un logement social relèvent des litiges relatifs aux droits attribués au titre du logement, au sens du 1° de l'article R. 811-1 du code de justice administrative, sur lesquels le tribunal administratif statue en premier et dernier ressort, quel que soit le montant des indemnités demandées. Par suite, la requérante n'est pas fondée à soutenir que le jugement attaqué est susceptible d'appel.<br/>
<br/>
              Sur le bien-fondé du jugement attaqué :<br/>
<br/>
              4. En premier lieu, lorsqu'une personne sollicite le versement d'une indemnité en réparation du préjudice subi du fait de l'illégalité d'une décision administrative entachée d'un vice de forme, il appartient au juge administratif de rechercher, en forgeant sa conviction au vu de l'ensemble des éléments produits par les parties, si la même décision aurait pu légalement être prise. Si tel est le cas, le préjudice allégué ne peut alors être regardé comme trouvant sa cause directe dans le vice de forme entachant la décision administrative illégale. <br/>
<br/>
              5. Il ressort des termes du jugement attaqué que, pour rejeter les conclusions tendant à l'indemnisation des préjudices que la requérante estimait avoir subis en raison de la décision du 18 mars 2015 annulée pour insuffisance de motivation, le tribunal administratif a estimé que le refus était justifié au fond compte tenu notamment du courrier du 18 mars 2015 adressé au préfet du Val de Marne par la commission d'attribution de l'OPIEVOY fondant le rejet de la candidature de Mme C... sur une inadéquation entre le loyer et les ressources de cette dernière. Il doit ainsi être regardé comme ayant recherché, en forgeant sa conviction au vu de l'ensemble des éléments produits par les parties, si la même décision aurait pu légalement être prise. La requérante n'est, dès lors, pas fondée à soutenir que le tribunal a entaché son jugement d'insuffisance de motivation et d'erreur de droit. <br/>
<br/>
              6. En second lieu, en estimant qu'il résultait de l'instruction que les ressources de Mme C... étaient insuffisantes au regard du montant du loyer demandé et que, par suite, le rejet de la candidature de l'intéressée était justifié au fond, le tribunal administratif a porté, sur les pièces du dossier qui lui étaient soumis, une appréciation souveraine exempte de dénaturation.<br/>
<br/>
              7. Il résulte de tout ce qui précède que le pourvoi de Mme C... doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme C... la somme que l'OPIEVOY demande au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de Mme C... est rejeté.<br/>
Article 2 : Les conclusions présentées par l'Office public interdépartemental de l'Essonne, Val-d'Oise et des Yvelines au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme B... C..., à l'Office public interdépartemental de l'Essonne, Val-d'Oise et des Yvelines et à la Fédération nationale des offices publics de l'habitat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
