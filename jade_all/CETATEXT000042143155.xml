<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042143155</ID>
<ANCIEN_ID>JG_L_2020_07_000000440206</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/14/31/CETATEXT000042143155.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 22/07/2020, 440206, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440206</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE CHAISEMARTIN, DOUMIC-SEILLER</AVOCATS>
<RAPPORTEUR>M. Pierre Ramain</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:440206.20200722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n°440206, par une requête, enregistrée le 21 avril 2020 au secrétariat du contentieux du Conseil d'Etat, le syndicat de la fonction publique (SFP), la confédération des syndicats des travailleurs de Polynésie-force ouvrière (CSTP-FO), la confédération des syndicats indépendants de Polynésie (CSIP), la confédération Otahi et la confédération O oe to oe rima demandent au Conseil d'Etat de déclarer l'article LP 6 de la " loi du pays " n° 2020-9 du 27 mars 2020 portant modification du contrat de soutien à l'emploi (CSE) et portant création des dispositifs de sauvegarde de l'emploi mobilisables en cas de circonstances exceptionnelles non conforme au bloc de légalité défini au III de l'article 176 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française.<br/>
<br/>
<br/>
<br/>
              2° Sous le n°440235, par une requête et un mémoire en réplique, enregistrés les 23 avril et 31 mai 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... A... demande au Conseil d'Etat :<br/>
<br/>
              1°) de déclarer les alinéas 2 à 4 de l'article LP 6 de la " loi du pays " n° 2020-9 du 27 mars 2020 portant modification du contrat de soutien à l'emploi (CSE) et portant création des dispositifs de sauvegarde de l'emploi mobilisables en cas de circonstances exceptionnelles non conformes au bloc de légalité défini au III de l'article 176 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française ;<br/>
<br/>
              2°) de mettre à la charge de la Polynésie française la somme de 3 000 euros à lui verser au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
             Vu les autres pièces des dossiers ;<br/>
<br/>
             Vu :<br/>
             - la Constitution, notamment son Préambule et son article 74 ;<br/>
             - la loi organique n°2004-192 du 27 février 2004 ;<br/>
             - le traité sur le fonctionnement de l'Union européenne ;<br/>
             - la convention n°24 de l'OIT du 15 juin 1927 ;<br/>
             - la convention n°95 de l'OIT du 1er juillet 1949 ;<br/>
             - la loi n° 2020-290 du 23 mars 2020 ;<br/>
             - le décret n° 2020-293 du 23 mars 2020 ;<br/>
             - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Ramain, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Chaisemartin, Doumic-Seiller, avocat de la Présidence de la Polynésie française et de l'assemblée de la Polynésie française ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Sous le n°440206, le syndicat de la fonction publique et autres demandent de déclarer l'article LP 6 de la " loi du pays " n° 2020-9 du 27 mars 2020 portant modification du contrat de soutien à l'emploi (CSE) et portant création des dispositifs de sauvegarde de l'emploi mobilisables en cas de circonstances exceptionnelles non conforme au bloc de légalité défini au III de l'article 176 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française. Sous le n°440235, M. A... présente une demande analogue dirigée contre les alinéas 2 à 4 du même article LP 6. Il y a lieu de joindre ces deux requêtes pour statuer par une même décision.<br/>
<br/>
              Sur l'exercice des recours :<br/>
<br/>
              2. De façon générale, l'article 176 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française dispose que : " (...) II. - A l'expiration de la période de huit jours suivant l'adoption d'un acte prévu à l'article 140 dénommé "loi du pays" ou au lendemain du vote intervenu à l'issue de la nouvelle lecture prévue à l'article 143, l'acte prévu à l'article 140 dénommé " loi du pays " est publié au Journal officiel de la Polynésie française à titre d'information pour permettre aux personnes physiques ou morales, dans le délai d'un mois à compter de cette publication, de déférer cet acte au Conseil d'Etat./ Le recours des personnes physiques ou morales est recevable si elles justifient d'un intérêt à agir./ Dès sa saisine, le greffe du Conseil d'Etat en informe le président de la Polynésie française avant l'expiration du délai de dix jours prévu à l'article 178./ III. - Le Conseil d'Etat se prononce sur la conformité des actes prévus à l'article 140 dénommés "lois du pays" au regard de la Constitution, des lois organiques, des engagements internationaux et des principes généraux du droit. Il se prononce sur l'ensemble des moyens de la requête qu'il estime susceptibles de fonder l'annulation, en l'état du dossier. La procédure contentieuse applicable au contrôle juridictionnel spécifique de ces actes est celle applicable en matière de recours pour excès de pouvoir devant le Conseil d'Etat. / Les actes prévus à l'article 140 dénommés " lois du pays " ne peuvent plus être contestés par voie d'action devant aucune autre juridiction ". L'article 177 prévoit que : " I.- Le Conseil d'Etat se prononce dans les trois mois de sa saisine. Sa décision est publiée au Journal officiel de la République française et au Journal officiel de la Polynésie française. / Si le Conseil d'Etat constate qu'un acte prévu à l'article 140 dénommé "loi du pays" contient une disposition contraire à la Constitution, aux lois organiques, ou aux engagements internationaux ou aux principes généraux du droit, et inséparable de l'ensemble de l'acte, celle-ci ne peut être promulguée. / Si le Conseil d'Etat décide qu'un acte prévu à l'article 140 dénommé " loi du pays " contient une disposition contraire à la Constitution, aux lois organiques ou aux engagements internationaux, ou aux principes généraux du droit, sans constater en même temps que cette disposition est inséparable de l'acte, seule cette dernière disposition ne peut être promulguée. / II.-A l'expiration du délai de trois mois mentionné au premier alinéa du I du présent article, le président de la Polynésie française peut promulguer l'acte prévu à l'article 140 dénommé " loi du pays ", dans les conditions mentionnées au second alinéa de l'article 178. Le Conseil d'Etat reste toutefois saisi des recours formés contre l'acte. /Dans ce cas, lorsque l'acte contient une disposition contraire à la Constitution, aux lois organiques, aux engagements internationaux ou aux principes généraux du droit, et inséparable de l'ensemble de l'acte, le Conseil d'Etat en prononce l'annulation totale./ Si le Conseil d'Etat estime qu'une disposition est contraire à la Constitution, aux lois organiques, aux engagements internationaux ou aux principes généraux du droit, sans constater en même temps que cette disposition est inséparable de l'acte, il prononce l'annulation de cette seule disposition.". Aux termes de l'article 178, "  A l'expiration du délai d'un mois mentionné au II de l'article 176 pour saisir le Conseil d'Etat ou à la suite de la publication au Journal officiel de la Polynésie française de la décision de ce conseil constatant la conformité totale ou partielle de l'acte prévu à l'article 140 dénommé " loi du pays " aux normes mentionnées au deuxième alinéa du I de l'article 177, le président de la Polynésie française dispose d'un délai de dix jours pour le promulguer, sous les réserves énoncées aux troisième et quatrième alinéas du I dudit article./ Il transmet l'acte de promulgation au haut-commissaire. L'acte prévu à l'article 140 dénommé " loi du pays " est publié, pour information, au Journal officiel de la République française ". Aux termes de l'article 180, " Sans préjudice de l'article 180-1, les actes prévus à l'article 140 dénommés " lois du pays " ne sont susceptibles d'aucun recours par voie d'action après leur promulgation ". <br/>
<br/>
              3. Pour sa part, l'article 180-1 de la loi organique du 27 février 2004 dispose que " Par dérogation au premier alinéa des I et II de l'article 176 et au premier alinéa des articles 178 et 180, les actes dénommés " lois du pays " relatifs aux impôts et taxes peuvent faire l'objet d'un recours devant le Conseil d'Etat à compter de la publication de leur acte de promulgation. " Aux termes de l'article 180-2 de la même loi, " Les actes prévus à l'article 140 dénommés " lois du pays " relatifs aux impôts et taxes sont publiés au Journal officiel de la Polynésie française et promulgués par le président de la Polynésie française au plus tard le lendemain de leur adoption. / Le président de la Polynésie française transmet l'acte de promulgation au haut-commissaire de la République. " Selon l'article 180-3 : " (...) II. _ A compter de la publication de l'acte de promulgation, les personnes physiques ou morales justifiant d'un intérêt à agir disposent d'un délai d'un mois pour déférer l'acte dénommé " loi du pays " relatif aux impôts et taxes au Conseil d'Etat ". L'article 180-4 prévoit que " Le Conseil d'Etat se prononce dans un délai de trois mois à compter de sa saisine. Il annule toute disposition contraire à la Constitution, aux lois organiques, aux engagements internationaux ou aux principes généraux du droit. "<br/>
<br/>
              4. Il résulte des dispositions citées aux points 2 et 3 que les actes dits " lois du pays " qui ne sont pas relatifs aux impôts et aux taxes ne peuvent, en principe, pas faire l'objet d'un recours par voie d'action après leur promulgation par le président de la Polynésie française. Il en va toutefois différemment quand l'acte dit " loi du pays " a été prématurément promulgué, que cette promulgation intervienne avant l'expiration du délai d'un mois prévu au premier alinéa de l'article 178 de la loi organique ou, si le Conseil d'Etat a été saisi, avant l'expiration du délai de trois mois prévu au I de l'article 177. <br/>
<br/>
              5. En cas de promulgation prématurée, si le Conseil d'Etat est saisi d'un recours dirigé seulement contre l'acte de promulgation, lequel peut être contesté au motif qu'il méconnait les exigences qui découlent de l'article 177 de la loi organique ou qu'il est entaché d'un vice propre, et si le Conseil d'Etat prononce l'annulation de cet acte, la " loi du pays " cesse d'être exécutoire et la publication qui a été faite de la " loi du pays " promulguée vaut publication pour information, ouvrant le délai de recours par voie d'action prévu par les dispositions citées au point 2 de l'article 176 de la loi organique. <br/>
<br/>
              6. Si, en cas de promulgation prématurée, le Conseil d'Etat est simultanément saisi de conclusions dirigées contre l'acte de promulgation et contre la " loi du pays " promulguée et s'il annule l'acte de promulgation, le recours dirigé contre la " loi du pays " est alors regardé comme un recours tendant à déclarer non conforme au bloc de légalité défini au III de l'article 176 de la loi organique la délibération adoptée par l'assemblée de la Polynésie française. S'il rejette les conclusions dirigées contre l'acte de promulgation, le recours dirigé contre la " loi du pays " présente le caractère d'un recours en annulation. <br/>
<br/>
              7. Enfin, si le Conseil d'Etat n'est saisi, dans le délai d'un mois suivant la publication de la " loi du pays " prématurément promulguée, que d'un recours par voie d'action contre la " loi du pays ", ce recours présente le caractère d'un recours en annulation. Il appartient alors au Conseil d'Etat d'annuler les dispositions de la " loi du pays " qu'il juge contraires au bloc de légalité voire, si ces dispositions ne sont pas séparables des autres dispositions de l'acte, d'en prononcer l'annulation totale.<br/>
<br/>
              8. Il ressort des pièces du dossier que la délibération n° 2020-1 relative à la " loi du pays " portant modification du contrat de soutien à l'emploi et portant création des dispositifs de sauvegarde de l'emploi mobilisables en cas de circonstances exceptionnelles a été adoptée par l'assemblée de la Polynésie française le 26 mars 2020. Le président de la Polynésie française a promulgué la " loi du pays " n° 2020-9 qui en procède dès le 27 mars 2020, laquelle a été publiée le même jour au Journal officiel de la Polynésie française. Cette " loi du pays " n° 2020-9 du 27 mars 2020 a été promulguée prématurément, avant l'expiration des délais prévus par la loi organique. Dans ces conditions, il résulte de ce qui a été dit précédemment que les requêtes des syndicats requérants et de M. A..., mettant en cause l'article LP 6 de cette " loi du pays ", doivent être regardées comme tendant à l'annulation de cet article.<br/>
<br/>
              Sur le recours en annulation contre l'article LP 6 de la " loi du pays " attaquée :<br/>
<br/>
              9. Aux termes de l'article LP 6 de la " loi du pays " attaquée : " Lorsque l'autorité compétente prend un arrêté de restriction des déplacements pouvant entrainer un confinement pour raisons sanitaires, les employeurs déterminent, dans les conditions de cet arrêté, la liste des salariés qui peuvent poursuivre l'exécution de leur contrat de travail, soit dans les locaux de l'entreprise, soit dans leur lieu de travail habituel, soit depuis leur lieu de confinement. /Le salarié confiné qui ne figure pas sur la liste établie par l'employeur est tenu de prendre les congés payés qu'il a acquis, dans le but d'éviter une suspension de son contrat de travail, sous réserve de la capacité financière de l'entreprise à absorber cette charge./ Le contrat de travail du salarié confiné qui ne dispose plus de droit à congés payés est suspendu./ Dans ce cas, un revenu exceptionnel de solidarité sera versé par la Polynésie française pendant la durée du confinement, dans la limite des crédits disponibles./ Le revenu exceptionnel de solidarité versé aux salariés n'est pas assujetti aux prélèvements fiscaux et sociaux. Le salarié conserve le bénéfice de son régime d'assurance maladie-invalidité et de prestations familiales./ Le revenu exceptionnel de solidarité ne pourra se cumuler avec un revenu tiré d'une autre activité professionnelle, salariée ou non, les conditions de ce non cumul étant prévues par arrêté pris en Conseil des ministres./ Le montant, les conditions ainsi que les modalités de versement du revenu exceptionnel de solidarité seront définis par un arrêté pris en Conseil des ministres./ En cas de déclaration fausse et mensongère, le salarié sera contraint de reverser à la Polynésie française tout ou partie des sommes perçues au titre du revenu exceptionnel de solidarité./ Lapériode de suspension du contrat de travail donnant lieu au versement du revenu exceptionnel de solidarité est assimilée à du travail effectif au sens de l'article Lp. 3231-3 du code du travail pour la détermination des droits à congés payés ".<br/>
<br/>
              En ce qui concerne la légalité externe de la " loi du pays " attaquée :<br/>
<br/>
              10. La promulgation est l'acte par lequel, en le revêtant de sa signature, le président de la Polynésie française atteste l'existence juridique de la " loi du pays " et lui confère un caractère exécutoire. Si l'acte qui promulgue une " loi du pays " peut être contesté devant le Conseil d'Etat, au motif qu'il méconnaît les exigences qui découlent de l'article 177 de la loi organique du 27 février 2004 ou qu'il est entaché d'un vice propre, son irrégularité est sans incidence sur la légalité des dispositions de la " loi du pays " ainsi promulguée. Par suite, les requérants ne peuvent utilement faire valoir à l'appui de leurs conclusions en annulation de l'article LP 6 de la " loi du pays " attaquée que cet acte a été irrégulièrement promulgué.  <br/>
<br/>
              11. S'il est soutenu que les alinéas 2 et 3 de l'article LP 6 de la " loi du pays " attaquée auraient été pris par une autorité incompétente au motif qu'un acte réglementaire ne peut imposer à des salariés de prendre des congés payés, ce moyen ne peut qu'être écarté dès lors que les " lois du pays " interviennent, en application de l'article 140 de la loi organique du 27 février 2004, dans le domaine de la loi. <br/>
<br/>
              12. Aux termes de l'article 151 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française : " (...) II.- Le conseil économique, social, environnemental et culturel est consulté sur les projets et propositions d'actes prévus à l'article 140 dénommés " lois du pays " à caractère économique ou social (...) ".<br/>
<br/>
              13. Si ces dispositions de l'article 151 de la loi organique imposent que le conseil économique, social, environnemental et culturel soit saisi de l'ensemble des dispositions d'un projet ou d'une proposition de " loi du pays " à caractère économique ou social, elles ne font pas obstacle à ce que des amendements, y compris d'origine gouvernementale, soient déposés en cours de discussion devant l'assemblée de la Polynésie française, dès lors que ces amendements ne sont pas dépourvus de tout lien avec le texte soumis à celle-ci. Il ressort des pièces du dossier que si le conseil économique, social, environnemental et culturel, consulté sur les dispositions du projet initial de " loi du pays " portant modification du contrat de soutien à l'emploi, n'a pas été saisi des dispositions de l'article LP. 6, les dispositions de cet article résultent d'un amendement non dépourvu de tout lien avec le texte soumis à l'assemblée. Dès lors, l'absence de consultation du conseil économique, social, environnemental et culturel n'entache pas d'irrégularité l'adoption de ces dispositions. <br/>
<br/>
              En ce qui concerne la légalité interne de la " loi du pays " attaquée :<br/>
<br/>
              14. En premier lieu, les requérants ne sauraient utilement se prévaloir de ce que le premier alinéa de l'article LP 6, qui donne compétence aux employeurs pour arrêter la liste des salariés qui peuvent, en période de confinement pour raisons sanitaires, poursuivre l'exécution de leur contrat de travail, méconnaîtrait les stipulations de l'article 198 du traité sur le fonctionnement de l'Union européenne, dont l'objet est de maintenir et de renforcer, pour les pays et territoires d'outre-mer (PTOM) dont la Polynésie française fait partie, le soutien de l'Union européenne à leur développement économique. <br/>
<br/>
              15. En deuxième lieu, le deuxième alinéa de l'article LP 6 qui conditionne le bénéfice du revenu exceptionnel de solidarité qu'il institue à l'épuisement des droits à congés annuels des salariés qui ne figurent pas sur la liste, fixée par l'employeur, de ceux qui peuvent poursuivre leur activité en dépit du confinement, a le caractère d'une mesure temporaire visant à préserver l'emploi pendant l'épidémie de covid-19. D'une part, eu égard à la portée du dispositif mis en place par le deuxième alinéa de l'article LP 6, les requérants ne sont pas fondés à soutenir que la condition d'épuisement des droits aux congés annuels méconnaîtrait le 11ème alinéa du Préambule de la Constitution du 27 octobre 1946 et porterait atteinte au principe d'égalité. Ces dispositions ne méconnaissent pas davantage, eu égard à leur caractère transitoire et exceptionnel, le 8ème alinéa du Préambule de la Constitution de 1946. Elles pouvaient, en outre, ne pas préciser les modalités d'appréciation de la capacité de l'entreprise à financer ces congés payés. Par ailleurs, elles ne sauraient, en tout état de cause, méconnaître les stipulations de l'article 10 de la convention n° 95 de l'Organisation internationale du travail qui régissent la saisie et la cession des salaires. En outre, il n'appartient au Conseil d'Etat, dans le cadre du contrôle défini par l'article 177 de la loi organique, d'apprécier la légalité des " lois du pays " qu'au regard de la Constitution, des lois organiques, des engagements internationaux et des principes généraux du droit. Est par suite inopérant le moyen tiré de ce que le deuxième alinéa de l'article LP 6 méconnaitrait les dispositions des articles Lp. 3231-12 et Lp. 3231-15 du code du travail polynésien sur la consultation des instances représentatives du personnel en matière de congés payés. Enfin, les requérants ne sauraient utilement se prévaloir de la jurisprudence de la Cour de justice de l'Union européenne sur les congés payés portant sur l'interprétation de la directive 2003/88/CE du 4 novembre 2003 concernant certains aménagements du temps de travail, laquelle ne s'applique pas en Polynésie française.<br/>
<br/>
              16. En troisième lieu, si le troisième alinéa de l'article LP 6, qui prévoit que le contrat de travail du salarié confiné qui ne dispose plus de droit à congés payés est suspendu, ne précise pas ce qu'il advient du contrat de travail du salarié placé en congé maladie, cette disposition n'a ni pour objet ni pour effet de modifier la situation du salarié placé en congé maladie. <br/>
<br/>
              17. En quatrième lieu, la mention, au quatrième alinéa de l'article LP 6, de ce que le revenu exceptionnel de solidarité sera versé par la Polynésie française pendant la durée du confinement dans la limite des crédits disponibles, ne peut, en tout état de cause, avoir pour objet ou pour effet de porter atteinte au principe d'égalité et de justifier légalement le refus de verser le revenu exceptionnel de solidarité à un salarié dont le contrat a été suspendu dans les conditions fixées à l'article LP 6 et qui remplit l'ensemble des conditions pour l'obtenir.<br/>
<br/>
              18. En cinquième lieu, l'absence d'assujettissement du revenu exceptionnel de solidarité aux prélèvements sociaux, prévu par le cinquième alinéa de l'article LP 6, n'a, en tout état de cause, eu égard aux montants en jeu, ni pour objet ni pour effet de porter atteinte au principe posé par l'article 6 de la convention n° 24 de l'Organisation internationale du travail du 15 juin 1927, aux termes duquel : " 1. L'assurance-maladie doit être gérée par des institutions autonomes placées sous le contrôle administratif et financier des pouvoirs publics et ne poursuivant aucun but lucratif. (...) 2. Les assurés doivent être appelés à participer à la gestion des institutions autonomes d'assurance dans des conditions déterminées par la législation nationale (...) ". <br/>
<br/>
              19. En sixième lieu, les auteurs de la " loi du pays " attaquée n'ont pas méconnu l'étendue de leur compétence en ne précisant pas, au huitième alinéa de l'article LP 6 qui prévoit le remboursement des sommes perçues au titre du revenu exceptionnel de solidarité en cas de déclaration fausse ou mensongère, les critères selon lesquels doivent être appréciés d'une part, le caractère faux et mensonger des déclarations faites par les salariés et d'autre part, le montant des sommes perçues au titre du revenu exceptionnel de solidarité qu'ils doivent reverser pour ce motif. <br/>
<br/>
              20. En dernier lieu, le neuvième alinéa de l'article LP 6 qui prévoit que la période de suspension du contrat de travail donnant lieu au versement du revenu exceptionnel de solidarité est assimilée à du travail effectif au sens de l'article Lp. 3231-3 du code du travail de la Polynésie française pour la détermination des droits à congés payés ne méconnaît pas l'objectif à valeur constitutionnelle de clarté et d'intelligibilité de la norme.<br/>
<br/>
              21. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur la fin de non-recevoir opposée en défense par la Polynésie française et l'assemblée de la Polynésie française, que les requérants ne sont pas fondés à demander au Conseil d'Etat d'annuler les dispositions de l'article LP 6 de la " loi du pays " portant modification du contrat de soutien à l'emploi (CSE) et portant création des dispositifs de sauvegarde de l'emploi mobilisables en cas de circonstances exceptionnelles. <br/>
<br/>
              22. Dans les circonstances de l'espèce, il y a lieu de mettre à la charge des syndicats requérants, d'une part, et de M. A..., d'autre part, la somme globale de 1 500 euros à verser à la Polynésie française et à l'assemblée de la Polynésie française, au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que la somme que M. A... demande soit mise, à ce titre, à la charge de la Polynésie française qui n'est pas la partie perdante dans la présente instance. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes du syndicat de la fonction publique et autres, et de M. A... sont rejetées.<br/>
Article 2 : Le syndicat de la fonction publique et autres d'une part, et M. A... d'autre part, verseront la somme globale de 1 500 euros à la Polynésie française et à l'assemblée de la Polynésie française au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au syndicat de la fonction publique, premier dénommé, à M. B... A..., au président de la Polynésie française, au président de l'assemblée de la Polynésie française, au haut-commissaire de la République en Polynésie française et au ministre des outre-mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
