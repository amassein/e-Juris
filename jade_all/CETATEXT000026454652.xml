<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026454652</ID>
<ANCIEN_ID>JG_L_2012_10_000000352817</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/45/46/CETATEXT000026454652.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 03/10/2012, 352817, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-10-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352817</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Bernard Stirn</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>Mme Natacha Chicot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:352817.20121003</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 19 septembre 2011 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du Gouvernement ; le ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du Gouvernement demande au Conseil d'Etat d'annuler le jugement n° 0904034 du 11 juillet 2011 par lequel le tribunal administratif de Strasbourg, d'une part, a annulé sa décision du 6 juillet 2009 fixant à 10 % le taux servant de base à la liquidation de l'allocation temporaire d'invalidité de M. Ludovic A et, d'autre part, lui a enjoint de prendre, dans un délai de deux mois à compter de la notification du jugement, une nouvelle décision fixant ce taux à 18 % ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu le décret n° 60-1089 du 6 octobre 1960 ;<br/>
<br/>
              Vu le décret n° 84-960 du 25 octobre 1984 ;<br/>
<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Natacha Chicot, Auditeur,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A, fonctionnaire de l'Etat, victime de deux accidents de service en 1996 et 1999 puis, ultérieurement, d'une maladie professionnelle reconnue imputable au service et sans lien avec les accidents antérieurs, a bénéficié d'une allocation temporaire d'invalidité au taux global de 25 % entre le 26 septembre 2003 et le 25 septembre 2008 ; qu'à la suite de la révision intervenue, conformément à la réglementation en vigueur, à l'issue de cette période de cinq ans, le ministre du budget, des comptes publics et de la réforme de l'Etat, après saisine de la commission de réforme, a attribué à l'intéressé un taux d'allocation temporaire d'invalidité sans limitation de durée, de 10 % ; que le tribunal administratif de Strasbourg a, par son jugement du 11 juillet 2011, annulé cette décision et enjoint au ministre de prendre une nouvelle décision fixant à 18 % le taux de l'allocation de M. A ; que le ministre se pourvoit en cassation contre ce jugement ;<br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article 65 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat : " Le fonctionnaire qui a été atteint d'une invalidité résultant d'un accident de service ayant entraîné une incapacité permanente d'au moins 10 p. 100 ou d'une maladie professionnelle peut prétendre à une allocation temporaire d'invalidité cumulable avec son traitement dont le montant est fixé à la fraction du traitement minimal de la grille mentionnée à l'article 15 du titre 1er du statut général, correspondant au pourcentage d'invalidité " ; qu'aux termes de l'article 1er du décret du 6 octobre 1960 en vigueur pour l'application de l'article 65 de la loi du 11 janvier 1984 précité : " L'allocation temporaire d'invalidité prévue à l'article 65 de la loi n° 84-16 du 11 janvier 1984 modifiée portant dispositions statutaires relatives à la fonction publique de l'Etat est attribuée aux agents maintenus en activité qui justifient d'une invalidité permanente résultant : a) Soit d'un accident de service ayant entraîné une incapacité permanente d'un taux rémunérable au moins égal à 10 % ; b) Soit de l'une des maladies d'origine professionnelle énumérées dans les tableaux mentionnés à l'article L. 461-2 du code de la sécurité sociale ; c) Soit d'une maladie reconnue d'origine professionnelle dans les conditions prévues par les troisième et quatrième alinéas de l'article L. 461-1 du code de la sécurité sociale ; " ; qu'aux termes de l'article 3 du même décret : " La réalité des infirmités invoquées par le fonctionnaire, leur imputabilité au service, la reconnaissance du caractère professionnel des maladies, les conséquences ainsi que le taux d'invalidité qu'elles entraînent sont appréciés par la commission de réforme prévue à l'article L. 31 du code des pensions civiles et militaires de retraite. Le pouvoir de décision appartient dans tous les cas au ministre dont relève l'agent et au ministre du budget " ;  qu'aux termes de l'article 5 de ce décret : " L'allocation temporaire d'invalidité est accordée pour une période de cinq ans. A l'expiration de cette période, les droits du fonctionnaire font l'objet d'un nouvel examen (...) et l'allocation est attribuée sans limitation de durée (...), sur la base du nouveau taux d'invalidité constaté ou, le cas échéant supprimée. (...) " ; qu'il résulte de ces dispositions, d'une part, qu'en cas de survenance d'accidents de service, puis d'une maladie professionnelle sans lien avec ceux-ci, les taux d'incapacité afférents à ces événements doivent, à l'occasion du nouvel examen des droits du fonctionnaire effectué à l'issue de la période de cinq ans expirant après la plus récente fixation du taux d'invalidité qui lui a été reconnu, être appréciés séparément et, d'autre part, que leur prise en compte pour justifier l'attribution de l'allocation temporaire d'invalidité sans limitation de durée, ou son éventuelle suppression, obéit aux règles propres à chacune des deux causes d'invalidité et ne peut, par suite, s'apprécier de manière globale ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'allocation temporaire d'invalidité attribuée à M. A a fait l'objet d'un nouvel examen à l'issue de la période quinquennale expirant le 25 septembre 2008, en application des dispositions précitées de l'article 5 du décret du 6 octobre 1960 ; qu'à cette occasion, la commission de réforme a proposé de ramener à 6 % et à 3 % les taux d'invalidité résultant des accidents de service, antérieurement fixés à 11 % au total, et à 10 % le taux d'invalidité au titre de la maladie professionnelle, antérieurement fixé à 15 % ; que le ministre, qui s'est conformé à l'appréciation de la commission de réforme, a fixé respectivement à 9 % et à 10 % les nouveaux taux d'invalidité de M. A résultant d'une part des accidents de service, d'autre part de la maladie professionnelle ; qu'à la suite de cette révision du taux d'incapacité reconnu à l'intéressé au titre de chacune des causes d'invalidité, le seuil de 10 % mentionné au a) de l'article 1er du décret du 6 octobre 1960 n'étant plus atteint en ce qui concerne les suites des accidents de service, seule l'incapacité résultant de la maladie professionnelle pouvait être prise en considération, conformément aux dispositions précitées, pour déterminer le nouveau taux de l'allocation à servir à M. A sans limitation de durée ; qu'il suit de là que le tribunal administratif de Strasbourg a commis une erreur de droit en jugeant illégale la décision du ministre de retenir un taux de 10 %, correspondant au seul taux d'invalidité entraîné par la maladie professionnelle dont souffre M. A, pour servir de base à la liquidation de son allocation, et en lui enjoignant de fixer ce taux à 18 % ; que le ministre est, par suite, et sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, fondé à demander l'annulation du jugement du 11 juillet 2011 ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant que, comme il vient d'être dit, le nouvel examen des droits de M. A auquel il a été procédé à l'expiration, de la période de cinq ans écoulée depuis la précédente détermination de son taux d'invalidité, a pu légalement conduire le ministre, au vu de la proposition de la commission de réforme de ramener le taux d'invalidité à 6 % et 3 % pour les accidents de service, et à 10 % pour la maladie professionnelle, à ne prendre en compte que cette dernière pour justifier l'attribution sans limitation de durée à l'intéressé d'une d'allocation temporaire d'invalidité, le total des taux d'incapacité entraînés par les accidents de service n'atteignant pas le seuil de 10 % prévu à l'article 65 de la loi du 11 janvier 1984 ; que, dès lors, la demande de M. A tendant à l'annulation de la décision ministérielle du 6 juillet 2009 fixant la base de l'allocation temporaire d'invalidité à 10 % ne peut qu'être rejetée ; qu'il y a lieu, par voie de conséquence, de rejeter ses conclusions à fin d'injonction et celles présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 11 juillet 2011 du tribunal administratif de Strasbourg est annulé.<br/>
Article 2 : La demande de M. A au tribunal administratif de Strasbourg est rejetée.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'économie et des finances et à M. Ludovic A.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
