<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034455149</ID>
<ANCIEN_ID>JG_L_2017_04_000000394606</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/45/51/CETATEXT000034455149.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 21/04/2017, 394606</TITRE>
<DATE_DEC>2017-04-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394606</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GADIOU, CHEVALLIER ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:394606.20170421</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société FM Projet a demandé au tribunal administratif de Paris d'annuler, pour excès de pouvoir, la décision implicite de rejet née le 12 mai 2014 par laquelle le directeur général de l'autorité de régulation des communications électroniques et des postes (ARCEP) a rejeté sa demande tendant à ce que lui soit communiqués les documents administratifs contenant la décomposition des coûts des offres de référence de la société Orange sur le marché de la boucle locale filaire. Par un jugement n°1410192/5-2 du 17 septembre 2015, le tribunal administratif de Paris a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 17 novembre 2015 et 17 février 2016 au secrétariat du contentieux du Conseil d'Etat, la société FM Projet demande au Conseil d'Etat :  <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de l'ARCEP la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - le code des postes des et communications électroniques ;<br/>
              - la loi n°78-753 du 17 juillet 1978 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la Société Fm Projet  ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société FM projet, opérateur aménageur de communications électroniques intervenant dans les opérations de montée en débit, a saisi l'Autorité de régulation des communications électroniques et des postes (ARCEP) le 29 janvier 2014 d'une demande de communication de la décomposition détaillée des coûts des offres de référence de la société Orange, destinées aux opérateurs de réseaux ouverts au public, d'une part pour l'accès aux infrastructures de génie civil et d'appuis aériens et, d'autre part, pour la création de points de raccordements mutualisés. L'ARCEP a rejeté cette demande de communication par un courrier du 3 mars 2014 au motif que les informations visées étaient couvertes par le secret en matière industrielle et commerciale. Saisie par un courrier enregistré le 11 mars 2014 à son secrétariat, la commission d'accès aux documents administratifs (CADA) a, le 29 avril 2014, estimé que les documents détenus par l'ARCEP, qui lui permettent de s'assurer, dans le cadre de sa mission de régulation économique des marchés de gros de communications électroniques, que la société Orange se conforme à ses obligations, revêtaient un caractère administratif, mais a émis un avis défavorable à leur communication " dès lors que la divulgation de ces données serait susceptible de révéler la structure des coûts de l'entreprise qui, sur d'autres marchés, exerce son activité en milieu concurrentiel ". La société FM Projet se pourvoit en cassation contre le jugement du 17 septembre 2015 par lequel le tribunal administratif de Paris a rejeté sa demande d'annulation de la décision de refus de communication de l'ARCEP, née du silence gardé par cette autorité après l'avis défavorable de la CADA.<br/>
<br/>
              2. La société Orange SA, dont les données constituent l'objet de la demande de communication de la requérante, justifie d'un intérêt suffisant au maintien de la décision attaquée. Ainsi, son intervention est recevable.<br/>
<br/>
              3. Aux termes du II de l'article 6 de la loi du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public et diverses dispositions d'ordre administratif, social et fiscal, repris par l'article L. 311-6 du code des relations entre le public et l'administration, " Ne sont communicables qu'à l'intéressé les documents administratifs (...) / - dont la communication porterait atteinte à la protection de la vie privée, au secret médical et au secret en matière commerciale et industrielle... ". <br/>
<br/>
              4. En premier lieu, il ressort des pièces du dossier soumis au  juge du fond, en particulier de la décision n°2011-0668 de l'ARCEP du 14 juin 2011 portant sur la définition du marché de gros pertinent des offres d'accès aux infrastructures constitutives de la boucle locale filaire, que si la société Orange possède la presque totalité de la boucle locale cuivre et une part très substantielle des infrastructures de génie civil permettant de déployer de façon continue une boucle locale optique, ce qui justifie que ces éléments soient qualifiés d' " infrastructures essentielles ", il existe de rares alternatives aux offres de la société Orange. Dans ces conditions, le tribunal administratif de Paris, qui a suffisamment motivé son jugement sur ce point, n'a pas dénaturé les pièces du dossier en jugeant qu'il existait une concurrence marginale dans le domaine concerné par les offres de référence en cause.<br/>
<br/>
              5. En deuxième lieu, contrairement à ce qui est soutenu, le tribunal administratif n'a pas commis d'erreur de droit en jugeant que la protection du secret en matière commerciale et industrielle pouvait légalement fonder le refus de communiquer à des tiers des informations se rapportant directement à un secteur dans lequel la concurrence est quasi-inexistante, mais qui comportent des données relatives au fonctionnement de l'entreprise dans d'autres secteurs d'activité où elle intervient sur un marché concurrentiel. Au demeurant, la circonstance qu'une entreprise exerce son activité sur un marché en situation de monopole ou de quasi-monopole n'est pas, par elle-même, de nature à faire obstacle à ce que le secret en matière industrielle et commerciale soit opposée à une demande de communication de documents administratifs relatifs à cette activité.<br/>
<br/>
              6. En troisième lieu, le tribunal administratif de Paris n'a pas entaché son jugement d'inexacte qualification juridique des faits en jugeant que les documents litigieux, relatifs au détail des coûts ayant permis à la société Orange de déterminer les prix de ses offres de gros, étaient protégés par le secret industriel et commercial. <br/>
<br/>
              7. En quatrième lieu, si le juge administratif a la faculté d'ordonner avant dire droit la production devant lui, par les administrations compétentes, des documents dont le refus de communication constitue l'objet même du litige, sans que la partie à laquelle ce refus a été opposé n'ait le droit d'en prendre connaissance au cours de l'instance, il ne commet d'irrégularité en s'abstenant de le faire que si l'état de l'instruction ne lui permet pas de déterminer, au regard des contestations des parties, le caractère légalement communicable ou non de ces documents. Il s'ensuit que c'est sans entacher son jugement d'erreur de droit ni de dénaturation, compte tenu de la nature des documents litigieux et des éléments dont il  disposait, que le tribunal a statué sans se faire communiquer au préalable ces documents par l'ARCEP.<br/>
<br/>
              8. Il résulte de tout ce qui précède que la société FM Projet n'est pas fondée à demander l'annulation du jugement qu'elle attaque. Son pourvoi doit donc être rejeté y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de la société Orange SA est admise.<br/>
<br/>
Article 2 : Le pourvoi de la société FM Projet est rejeté.<br/>
Article 3 : La présente décision sera notifiée à la société FM projet, à l'ARCEP et à la société Orange SA.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-06-01-02-03 DROITS CIVILS ET INDIVIDUELS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS AU TITRE DE LA LOI DU 17 JUILLET 1978. DROIT À LA COMMUNICATION. DOCUMENTS ADMINISTRATIFS NON COMMUNICABLES. - DOCUMENT ADMINISTRATIF DONT LA COMMUNICATION PORTERAIT ATTEINTE AU SECRET EN MATIÈRE INDUSTRIELLE ET COMMERCIALE - ENTREPRISE EN SITUATION DE MONOPOLE OU DE QUASI-MONOPOLE - CIRCONSTANCE N'ÉTANT PAS DE NATURE À FAIRE OBSTACLE À UN REFUS DE COMMUNICATION POUR CE MOTIF.
</SCT>
<ANA ID="9A"> 26-06-01-02-03 La circonstance qu'une entreprise exerce son activité sur un marché en situation de monopole ou de quasi-monopole n'est pas, par elle-même, de nature à faire obstacle à ce que le secret en matière industrielle et commerciale soit opposé à une demande de communication de documents administratifs relatifs à cette activité.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
