<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037022295</ID>
<ANCIEN_ID>JG_L_2018_06_000000410403</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/02/22/CETATEXT000037022295.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème chambre jugeant seule, 06/06/2018, 410403, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410403</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème chambre jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Coralie Albumazard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:410403.20180606</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 410403, par une requête, enregistrée le 10 mai 2017 au secrétariat du contentieux du Conseil d'Etat, l'union syndicale des magistrats demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir, d'une part, le décret n° 2017-634 du 25 avril 2017 modifiant le décret n° 2008-689 du 9 juillet 2008 relatif à l'organisation du ministère de la justice et le décret n° 2016-1675 du 5 décembre 2016 portant création de l'inspection générale de la justice, d'autre part, l'arrêté ministériel du 25 avril 2017 relatif à l'organisation du secrétariat général des directions du ministère de la justice ;<br/>
<br/>
              2°) à titre subsidiaire, d'annuler pour excès de pouvoir l'article 7 de ce décret et les articles 15 et 16 de cet arrêté en tant qu'ils prévoient que la direction de la protection judiciaire de la jeunesse " anime et contrôle l'action du ministère public en matière de protection de l'enfance et suit la formation de la jurisprudence correspondante ", ainsi que l'article 16 de cet arrêté en tant qu'il prévoit que le bureau de la législation et des affaires juridiques de cette direction " contribue, en relation avec la direction des affaires criminelles et des grâces, au suivi de l'action publique exercée auprès des juridictions dans les dossiers impliquant des mineurs ".<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 411823, par une requête et un nouveau mémoire, enregistrés le 23 juin 2017 et le 2 février 2018 au secrétariat du contentieux du Conseil d'Etat, le syndicat de la magistrature demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir les mêmes décret et arrêté ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
	Vu les autres pièces des dossiers ;<br/>
Vu : <br/>
- la Constitution, notamment son article 64 ; <br/>
- l'ordonnance n° 58-1270 du 22 décembre 1958 ; <br/>
- le code de procédure pénale, notamment son article 30 ;<br/>
- la loi n° 84-16 du 11 janvier 1984 ;<br/>
- le décret n° 82-453 du 28 mai 1982 ;<br/>
- le décret n° 2008-689 du 9 juillet 2008 ;<br/>
- le décret n° 2011-184 du 15 février 2011 ;<br/>
- le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Coralie Albumazard, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Sevaux, Mathonnet, avocat du Syndicat de la magistrature.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              Sur les consultations préalables :<br/>
<br/>
              1.	En premier lieu, les articles 15 et 16 de la loi du 11 janvier 1984 prévoient que, dans toutes les administrations de l'Etat et dans les établissements publics ne présentant pas un caractère industriel et commercial, les comités techniques " connaissent des questions relatives à l'organisation et au fonctionnement des services " et les comités d'hygiène de sécurité et des conditions de travail ont " pour mission de contribuer à la protection de la santé physique et mentale et de la sécurité des agents dans leur travail, à l'amélioration des conditions de travail et de veiller à l'observation des prescriptions légales prises en ces matières ". L'article 34 du décret du 15 février 2011 relatif aux comités techniques dans les administrations et les établissements publics de l'Etat, pris pour l'application de l'article 15 de la loi du 11 janvier 1984, énumère les questions et projets de textes sur lesquels les comités techniques sont obligatoirement consultés, qui incluent ceux relatifs à l'organisation et au fonctionnement des administrations, établissements ou services. <br/>
<br/>
              2.	D'une part, l'article 1er de l'arrêté du garde des sceaux, ministre de la justice et des libertés, du 7 juin 2011 relatif à la création d'un comité technique spécial de service placé auprès du directeur des services judiciaires prévoit qu' " Il est créé, auprès du directeur des services judiciaires, un comité technique spécial de service ayant compétence, dans le cadre des dispositions du titre III du décret du 15 février 2011 susvisé, pour connaître de toutes les questions concernant l'ensemble des services déconcentrés placés sous son autorité ". Si l'article 2 du décret attaqué définit les compétences du secrétaire général du ministère de la justice, notamment en prévoyant que celui-ci " assure une mission générale de coordination des services et de modernisation du ministère " et qu'il est responsable des ressources humaines et des affaires financières du ministère ainsi que " du pilotage des travaux de modernisation et d'expertise transverses au ministère ", ces dispositions n'ont pas pour objet de régir l'organisation ou le fonctionnement des services des juridictions judiciaires placés sous l'autorité du directeur des services judiciaires et n'emportent pas de conséquences directes sur l'organisation et le fonctionnement de ceux-ci. Dès lors, l'union syndicale des magistrats n'est pas fondée à soutenir que l'édiction du décret et de l'arrêté attaqué aurait dû être précédée de la consultation du comité spécial de service placé auprès de ce directeur.<br/>
<br/>
              3.	D'autre part, en vertu de l'article 34 du décret du 15 février 2011 précité, les comités techniques ne sont consultés sur les questions relatives à l'hygiène, à la sécurité et aux conditions de travail que lorsqu'aucun comité d'hygiène, de sécurité et de conditions de travail (CHSCT) n'est placé auprès d'eux et les comités techniques, qui bénéficient du concours du CHSCT, peuvent le saisir de toute question. L'article 47 du décret du 28 mai 1982, pris pour l'application de l'article 16 de la loi du 11 janvier 1984, précise que les comités d'hygiène, de sécurité et des conditions de travail exercent leurs missions " sous réserve des compétences des comités techniques ". Le 1° de l'article 57 du même décret prévoit que le comité d'hygiène, de sécurité et des conditions de travail est notamment consulté " sur les projets d'aménagement importants modifiant les conditions de santé et de sécurité ou les conditions de travail (...) ". <br/>
<br/>
              4.	Il résulte de ces dispositions qu'une question ou un projet de disposition ne doit être soumis à la consultation du comité d'hygiène, de sécurité et des conditions de travail que si le comité technique ne doit pas lui-même être consulté sur la question ou le projet de disposition en cause. Le comité d'hygiène, de sécurité et des conditions de travail ne doit ainsi être saisi que d'une question ou d'un projet de disposition concernant exclusivement la santé, la sécurité ou les conditions de travail. En revanche, lorsqu'une question ou un projet de disposition concerne ces matières et l'une des matières énumérées à l'article 34 du décret du 15 février 2011, seul le comité technique doit être obligatoirement consulté. Ce comité peut, le cas échéant, saisir le comité d'hygiène, de sécurité et des conditions de travail de toute question qu'il juge utile de lui soumettre. En outre, l'administration a toujours la faculté de consulter le comité d'hygiène, de sécurité et des conditions de travail.<br/>
<br/>
              5.	Par suite, dès lors que le décret et l'arrêté attaqués devaient, eu égard à leur objet, être soumis à la consultation du comité technique ministériel du ministère de la justice, lequel a, d'ailleurs, émis un avis le 23 mars 2017 sur ces deux textes, la consultation du comité d'hygiène, de sécurité et des conditions de travail ministériel du même ministère revêtait un caractère facultatif. Le moyen tiré de ce que le décret et l'arrêté attaqué auraient été édictés au terme d'une procédure irrégulière, faute pour ce comité d'avoir été consulté, ne peut donc qu'être écarté. <br/>
<br/>
<br/>
<br/>
              Sur les attributions de la direction de la protection judiciaire de la jeunesse :<br/>
<br/>
              6.	L'article 7 du décret attaqué modifie l'article 7 du décret du 9 juillet 2008 relatif à l'organisation du ministère de la justice pour prévoir que la direction de la protection judiciaire de la jeunesse " anime et contrôle l'action du ministère public en matière de protection de l'enfance et suit la formation de la jurisprudence correspondante ". Les articles 15 et 16 de l'arrêté attaqué fixent la sous-direction et le bureau compétents, au sein de cette direction, pour la mise en oeuvre de ces missions. Son article 15 modifie ainsi l'arrêté du 9 juillet 2008 fixant l'organisation en sous-directions de la direction de la protection judiciaire de la jeunesse en insérant ces mêmes dispositions parmi les attributions de la sous-direction des missions de protection judiciaire et d'éducation. Son article 16 modifie l'arrêté du 9 juillet 2008 fixant l'organisation en bureaux de la direction de la protection judiciaire de la jeunesse pour prévoir que le bureau de la législation et des affaires juridiques " contribue, en relation avec la direction des affaires criminelles et des grâces, au suivi de l'action publique exercée auprès des juridictions dans les dossiers impliquant des mineurs " et " anime et contrôle l'action du ministère public en matière de protection de l'enfance et suit la formation de la jurisprudence correspondante. "<br/>
<br/>
              7.	En vertu de l'article 20 de la Constitution, le Gouvernement détermine et conduit la politique de la Nation, notamment en ce qui concerne les domaines d'action du ministère public. L'article 5 de l'ordonnance du 22 décembre 1958 portant loi organique relative au statut de la magistrature prévoit que : " Les magistrats du parquet sont placés sous la direction et le contrôle de leurs chefs hiérarchiques et sous l'autorité du garde des sceaux, ministre de la justice. A l'audience, leur parole est libre. " En application du deuxième alinéa de l'article 30 du code de procédure pénale, le ministre de la justice peut adresser aux magistrats du ministère public des instructions générales de politique pénale, au regard notamment de la nécessité d'assurer sur tout le territoire de la République l'égalité des citoyens devant la loi. Conformément aux dispositions des articles 39-1 et 39-2 du même code, il appartient au ministère public de mettre en oeuvre ces instructions. En application du troisième alinéa de ce même article 30, le ministre de la justice ne peut adresser aux magistrats du parquet aucune instruction dans des affaires individuelles. En vertu de l'article 31 du même code, le ministère public exerce l'action publique et requiert l'application de la loi, dans le respect du principe d'impartialité auquel il est tenu. En application de l'article 33, il développe librement les observations orales qu'il croit convenables au bien de la justice. L'article 39-3 confie au procureur de la République la mission de veiller à ce que les investigations de police judiciaire tendent à la manifestation de la vérité et qu'elles soient accomplies à charge et à décharge, dans le respect des droits de la victime, du plaignant et de la personne suspectée. Conformément à l'article 40-1 du code de procédure pénale, le procureur de la République décide librement de l'opportunité d'engager des poursuites.<br/>
<br/>
              8.	D'une part, en prévoyant que la direction de la protection judiciaire de la jeunesse anime et contrôle l'action du ministère public en matière de protection de l'enfance et en attribuant les missions qui en découlent à la sous-direction des missions de protection judiciaire et d'éducation et au bureau de la législation et des affaires juridiques, le décret et l'arrêté attaqués n'ont ni pour objet ni pour effet de permettre qu'il soit dérogé aux règles prévues par les dispositions citées au point précédent, qui encadrent les modalités selon lesquelles le ministre de la justice peut adresser aux magistrats du ministère public des instructions générales, notamment en matière pénale, non plus qu'aux dispositions précitées qui prévoient la compétence de leurs chefs hiérarchiques en matière de direction et de contrôle. Les moyens tirés de ce que le décret et l'arrêté attaqués méconnaîtraient, pour ce motif, le principe d'indépendance des magistrats du parquet prévu par l'article 64 de la Constitution ainsi que l'article 30 du code de procédure pénale et seraient entachés d'incompétence en tant qu'ils modifieraient le statut des magistrats du parquet doivent donc être écartés.<br/>
<br/>
              9.	D'autre part et contrairement à ce qui est soutenu, il ne résulte ni des dispositions rappelées au point 7 ni d'aucun principe ou disposition législative que la mission d'animation et de contrôle de l'action du ministère public en matière de protection de l'enfance ne pourrait être exercée, au nom du ministre de la justice et par délégation de celui-ci, que par une direction dédiée exclusivement au fonctionnement des juridictions judiciaires. Les moyens tirés de ce que le décret et l'arrêté attaqués méconnaîtraient, pour ce motif, l'article 5 de l'ordonnance du 22 décembre 1958 précitée ainsi que la cohérence de l'application de la politique pénale doivent donc être écartés. <br/>
<br/>
              10.	Enfin, il résulte des dispositions, rappelées au point 6, que le décret et l'arrêté attaqués confient à un bureau de l'administration centrale de la direction de la protection judiciaire de la jeunesse la mission d'animer et de contrôler l'action du ministère public en matière de protection de l'enfance. Ces dispositions sont donc, par elles-mêmes, sans incidence sur le rôle des établissements et services déconcentrés de la protection judiciaire de la jeunesse dans l'exécution des décisions de l'autorité judiciaire. Par suite, le moyen tiré de ce que les dispositions précitées seraient entachées d'une erreur manifeste d'appréciation résultant du défaut de cohérence de l'organisation administrative qui en résulte doit être écarté. <br/>
<br/>
              11.	Il résulte de tout ce qui précède que l'union syndicale des magistrats et le syndicat de la magistrature ne sont pas fondés à demander l'annulation du décret et de l'arrêté qu'ils attaquent. Leurs requêtes ne peuvent donc qu'être rejetées, y compris les conclusions présentées par le syndicat de la magistrature au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes de l'union syndicale des magistrats et du syndicat de la magistrature sont rejetées.<br/>
Article 2 : La présente décision sera notifiée à l'union syndicale des magistrats, au syndicat de la magistrature et à la garde des sceaux, ministre de la justice.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
