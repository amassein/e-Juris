<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035775009</ID>
<ANCIEN_ID>JG_L_2017_10_000000403911</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/77/50/CETATEXT000035775009.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 11/10/2017, 403911, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-10-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403911</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Simon Chassard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Yohann Bénard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:403911.20171011</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 403911, par une requête et un mémoire en réplique, enregistrés les 30 septembre 2016 et 31 août 2017 au secrétariat du contentieux du Conseil d'Etat, les sociétés Japan Tobacco International SA et Japan Tobacco International France SAS demandent au Conseil d'Etat : <br/>
<br/>
              1) d'annuler pour excès de pouvoir les dispositions de la section III de la lettre adressée le 6 avril 2016 aux fabricants et fournisseurs agréés de produits du tabac, ainsi que les décisions implicites de refus nées du silence gardé pendant plus de deux mois par le ministre chargé du budget sur leurs recours hiérarchique et gracieux ;<br/>
<br/>
              2) de mettre à la charge de l'Etat la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 407045, par une requête et un mémoire en réplique, enregistrés les 20 janvier et 31 août 2017 au secrétariat du contentieux du Conseil d'Etat, les sociétés Japan Tobacco International SA et Japan Tobacco International France SAS demandent au Conseil d'Etat :<br/>
<br/>
              1) d'annuler pour excès de pouvoir les dispositions de la section III ainsi que les annexes de la lettre adressée le 22 novembre 2016 aux fabricants et fournisseurs agréés de produits du tabac ;<br/>
<br/>
              2) de mettre à la charge de l'Etat la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le traité sur le fonctionnement de l'Union européenne ; <br/>
              - la directive 2011/64/UE du Parlement et du Conseil du 21 juin 2011 ;<br/>
              - le code général des impôts ;  <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Simon Chassard, auditeur,<br/>
<br/>
              - les conclusions de M. Yohann Bénard, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat du ministre de l'action et des comptes publics ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les requêtes visées ci-dessus présentent à juger les mêmes questions. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              Sur la recevabilité de la requête :<br/>
<br/>
              2. L'interprétation que par voie, notamment, de circulaires ou d'instructions, l'autorité administrative donne des lois et règlements qu'elle a pour mission de mettre en oeuvre n'est pas susceptible d'être déférée au juge de l'excès de pouvoir lorsque, étant dénuée de caractère impératif, elle ne saurait, quel qu'en soit le bien-fondé, faire grief. En revanche, les dispositions impératives à caractère général d'une circulaire ou d'une instruction doivent être regardées comme faisant grief alors même qu'elles se borneraient à réitérer une règle déjà contenue dans une norme juridique supérieure. Le recours formé à leur encontre doit être accueilli si ces dispositions fixent, dans le silence des textes, une règle nouvelle entachée d'incompétence, ou s'il est soutenu à bon droit que l'interprétation qu'elles prescrivent d'adopter, soit méconnaît le sens et la portée des dispositions législatives ou réglementaires qu'elle entendait expliciter, soit réitère une règle contraire à une norme juridique supérieure.<br/>
<br/>
              3. Les lettres adressées le 6 avril et le 22 novembre  2016 aux fabricants et fournisseurs agréés de produits du tabac sont relatives aux modalités d'homologation des prix de vente au détail des tabacs manufacturés en France métropolitaine. Le premier paragraphe de la section III de ces lettres intitulé " appellation du produit " dispose notamment que " conformément à l'article 572 du code général des impôts, un produit du tabac (marque commerciale) doit respecter pour l'ensemble de la gamme un prix unique aux 1 000 unités ou aux 1 000 grammes " et que " l'arrêté d'homologation liste, pour chaque fournisseur agréé, le prix des références de chaque produit, étant précisé qu'une référence s'entend comme une unité de conditionnement du produit ". Le second paragraphe de cette même section intitulé " le calcul du prix unique " dispose en outre qu'en vertu du même article 572 du code général des impôts " le prix d'un produit doit être unique lorsqu'il est rapporté aux 1 000 unités ou aux 1 000 grammes, arrondi à l'euro près  " et que " si la règle du prix unique n'est pas respectée (...) les produits concernés ne seront pas homologués et ne pourront donc pas être commercialisés auprès des débitants de tabac ". Enfin, l'annexe à la lettre du 22 novembre 2016 intitulée " Procédure de calcul selon la règle du prix unique " précise, pour chaque type de produit, les modalités de calcul du prix unique et prescrit également une règle d'arrondi optionnelle à la demi-dizaine d'euro supérieure. Les dispositions impératives à caractère général de ces lettres qui, d'une part, réitèrent les règles prescrites par l'article 572 du code général des impôts et, d'autre part, fixent des règles d'arrondi non expressément prévues par cet article, doivent être regardées comme faisant grief et susceptibles, à ce titre, de faire l'objet d'un recours pour excès de pouvoir. <br/>
<br/>
              Sur la légalité externe des lettres attaquées :<br/>
<br/>
              En ce qui concerne la compétence du ministre chargé du budget pour édicter la section III de la lettre du 6 avril 2016 :<br/>
<br/>
              4. Aux termes de l'article 572 du code général des impôts, dans sa rédaction issue de la loi du 26 janvier 2016 de modernisation de notre système de santé : " Le prix de détail de chaque produit exprimé aux 1 000 unités ou aux 1 000 grammes, est unique pour l'ensemble du territoire et librement déterminé par les fabricants et les fournisseurs agréés. Il est applicable après avoir été homologué par arrêté conjoint des ministres chargés de la santé et du budget, dans des conditions définies par décret en Conseil d'Etat. Il ne peut toutefois être homologué s'il est inférieur à la somme du prix de revient et de l'ensemble des taxes. / (...) ". Aux termes de l'article 284 de l'annexe II à ce code dans sa version applicable à la date de signature de la lettre du 6 avril 2016 : " Les fabricants et les fournisseurs agréés communiquent leur prix de vente au détail des tabacs manufacturés, pour chacun de leurs produits, à la direction générale des douanes et droits indirects. / Les prix sont homologués par arrêté du ministre chargé du budget et publiés au Journal officiel de la République française. ". Cet article 284 a été modifié par le décret du 7 juin 2016 relatif à la procédure d'homologation du prix de vente au détail des tabacs manufacturés qui a prévu, par application des dispositions de l'article 572 du code général des impôts dans leur version issue de la loi du 26 janvier 2016, que les prix seraient désormais " homologués par arrêté conjoint du ministre chargé du budget et du ministre chargé de la santé et publiés au Journal officiel de la République française ". <br/>
<br/>
              5. Si les dispositions de l'article 572 du code général des impôts, dans leur rédaction issue de la loi du 26 janvier 2016, renvoient à un décret le soin de définir les modalités de leur mise en oeuvre, l'application de la règle selon laquelle les arrêtés d'homologation des prix des produits du tabac sont conjointement adoptés par les ministres chargés du budget et de la santé n'était pas manifestement impossible en l'absence de mesures réglementaires d'application, lesquelles ne sont intervenues, ainsi qu'il a été ci-dessus, qu'avec le décret du 7 juin 2016 modifiant l'article 284 de l'annexe II au code général des impôts. Ainsi, les ministres, auxquels il appartenait de tirer toutes les conséquences de ces dispositions législatives, étaient conjointement compétents, à compter de l'entrée en vigueur de la loi du 26 janvier 2016, pour adopter les arrêtés d'homologation des prix des produits du tabac ainsi, en conséquence, que les lettres par lesquelles sont précisées aux fabricants les modalités procédurales à suivre en vue de cette homologation.<br/>
<br/>
              6. En l'espèce, la lettre du 6 avril 2016 n'a été signée, ainsi que le relèvent les requérantes, que par le sous-directeur des droits indirects, agissant par délégation du ministre chargé du budget. Il en découle que la lettre du 6 avril 2016 a été prise en méconnaissance de la règle de compétence énoncée à l'article 572 du code général des impôts dans sa rédaction issue de la loi du 26 janvier 2016. Il résulte de ce qui précède que les requérantes sont fondées à demander l'annulation pour incompétence de la section III de la lettre du 6 avril 2016 qu'elles attaquent. <br/>
<br/>
              En ce qui concerne la compétence des ministres pour édicter les règles d'arrondi prescrites par la lettre  du 22 novembre 2016 :<br/>
<br/>
              7. Le second paragraphe de la section III de la lettre du 22 novembre 2016 ainsi que son annexe prévoient, d'une part, que le prix des produits du tabac exprimé aux 1 000 unités ou aux 1 000 grammes est unique une fois arrondi à l'euro près et, d'autre part que, sur option des fabricants de produits du tabac, le prix aux 1 000, une fois rapporté au conditionnement, peut être arrondi au centime près ou à la demi-dizaine de centimes d'euros supérieure. Ces règles d'arrondi, qui ne résultent pas de l'article 572 du code général des impôts, ne relèvent pas non plus du pouvoir réglementaire propre dévolu aux ministres chargés de l'homologation des prix des produits du tabac. Il suit de là qu'elles ont été incompétemment adoptées et que les requérantes sont donc fondées à demander l'annulation du second paragraphe de la section III de la lettre du 22 novembre 2016 ainsi que de son annexe intitulée " Procédure de calcul selon la règle du prix unique ".<br/>
<br/>
              En ce qui concerne la compétence d'agents de la direction générale des douanes et des droits indirects et de la direction générale de la santé pour signer la lettre du 22 novembre 2016 :<br/>
<br/>
              8. Ainsi qu'il a été dit au point 5 ci-dessus, les ministres chargés du budget et de la santé étaient conjointement compétents, à compter de l'intervention de l'article 34 de la loi du 26 janvier 2016, pour adopter l'arrêté d'homologation des prix des produits du tabac. Il suit de là que des agents de la direction générale des douanes et des droits indirects et de la direction générale de la santé, agissant sur délégation de signature de ces ministres, pouvaient compétemment signer la lettre du 22 novembre 2016 en tant qu'elle précisait les modalités procédurales, autres que les règles d'arrondis dont les requérantes sont fondées à demander l'annulation, à suivre en vue de cette homologation. Sous réserve de ce qui est dit au point 7, le moyen tiré de ce que cette lettre serait entachée d'incompétence doit donc être écarté.<br/>
<br/>
              Sur la légalité interne de la lettre du 22 novembre 2016 :<br/>
<br/>
              En ce qui concerne la méconnaissance des dispositions de l'article 572 du code général des impôts en tant qu'elles prévoient que les prix des produits du tabac sont uniques aux 1 000 unités ou aux 1 000 grammes :<br/>
<br/>
              9. Selon les termes de la lettre du 22 novembre 2016, le produit du tabac s'entend, pour l'application des dispositions de l'article 572 du code général des impôts citées au point 4 ci-dessus, de l'association d'une marque et d'une dénomination commerciale et chaque produit du tabac peut être distribué sous plusieurs références, dont chacune correspond à un conditionnement donné, c'est-à-dire à la quantité de cigarettes ou de cigares ou de tabac pour le tabac à rouler, contenue dans ce conditionnement. Il ressort également des termes de cette lettre que le prix de chaque produit du tabac, exprimé aux 1 000 unités ou aux 1 000 grammes, doit être unique, ce qui implique nécessairement, ainsi qu'il ressort des pièces du dossier, que le prix d'une cigarette ou d'un cigare vendus en conditionnement soit unique pour chaque référence du produit, indépendamment de la quantité conditionnée. Les sociétés requérantes soutiennent qu'en retenant de telles définitions du " produit " et de la " référence ", la lettre attaquée a méconnu le sens et la portée des dispositions de l'article 572 du code général des impôts qui n'imposaient, selon elles, que l'unicité du prix pratiqué pour chaque référence de produit sur tout le territoire métropolitain.<br/>
<br/>
              10. Il ressort néanmoins des dispositions de l'article 572 du code général des impôts citées au point 4, éclairées par les travaux préparatoires de la loi du 30 décembre 1997 de finances pour 1998 dont elles sont issues, que la règle selon laquelle les prix proposés à l'homologation par les fabricants et les fournisseurs agréés doivent être uniques, exprimés aux 1 000 unités ou aux 1 000 grammes, a pour finalité d'éviter le développement de la consommation de tabac qui pourrait résulter de la baisse des prix de certains produits lorsqu'ils sont conditionnés en plus grandes quantités. Il en résulte qu'en adoptant ces dispositions, et contrairement à ce que soutiennent les requérantes, le législateur a entendu imposer que le prix d'une cigarette ou d'un cigare vendus en conditionnement soit unique pour chaque référence du produit concerné, et ce quelle que soit la quantité conditionnée. Il suit de là que la lettre attaquée, en retenant les définitions du " produit " et de la " référence " indiquées au point 9, n'a méconnu ni le sens ni la portée des dispositions de l'article 572 du code général des impôts.<br/>
<br/>
              En ce qui concerne la méconnaissance du principe de libre détermination des prix des produits du tabac tel qu'il découle de l'article 15 de la directive 2011/64/UE du 21 juin 2011 :<br/>
<br/>
              11. Il résulte des dispositions de l'article 572 du code général des impôts citées au point 4 ci-dessus que le prix d'un  produit du tabac, entendu d'une catégorie de cigarettes ou de tabac à rouler, doit être identique aux 1 000 unités ou aux 1 000 grammes de ce produit, ce dont il résulte qu'une fois exprimé par conditionnement, entendu d'un paquet de cigarettes ou d'un paquet de tabac à rouler, ce prix reste identique, par produit, pour chaque quantité conditionnée. Ainsi, l'application des dispositions de l'article 572 du code général des impôts a pour effet que les fabricants et fournisseurs agréés de produits du tabac ne peuvent pas faire varier le prix de détail des produits du tabac en fonction de la contenance de leur conditionnement. Les sociétés requérantes soutiennent, à l'appui de leurs conclusions tendant à l'annulation de la lettre du 22 novembre 2016 qu'elles attaquent, que cette règle d'expression des prix des produits du tabac, en tant qu'elle ne permet pas la prise en compte d'éventuelles différences de coûts de conditionnement liées aux quantités conditionnées, méconnaît le principe de libre détermination des prix des produits du tabac tel qu'il découle notamment de l'article 15 de la directive 2011/64/UE du 21 juin 2011. <br/>
<br/>
              12. La réponse à ces moyens dépend des questions de savoir, en premier lieu, si la détermination des prix des produits du tabac en conditionnement est régie, ou non, par les stipulations de la directive du 21 juin 2011 et, en second lieu, en cas de réponse positive à cette première question, si la règle d'expression du prix des produits du tabac aux 1 000 unités ou aux 1 000 grammes, en tant qu'elle interdit aux fabricants de faire varier les prix des produits du tabac en fonction de la quantité conditionnée, méconnaît le principe de libre détermination de ces prix tel qu'il découle de l'article 15 de la directive du 21 juin 2011.<br/>
<br/>
              S'agissant du champ d'application de l'article 15 de la directive du 21 juin 2011 :<br/>
<br/>
              13. Aux termes du 1 de l'article 2 de la directive du 21 juin 2011 : " Aux fins de la présente directive, on entend par tabacs manufacturés : / a) les cigarettes ; / b) les cigares et les cigarillos ; / le tabac à fumer : / i) le tabac fine coupe destiné à rouler les cigarettes ; / ii) les autres tabacs à fumer. ". Aux termes de l'article 3 de cette directive : " Aux fins de la présente directive, on entend par cigarettes : / a) les rouleaux de tabac susceptibles d'être fumés en l'état et qui ne sont pas cigares ou des cigarillos au sens de l'article 4, paragraphe 1 ; / b) les rouleaux de tabac qui, par une simple manipulation non industrielle, sont glissés dans des tubes à cigarettes ; / c) les rouleaux de tabac qui, par une simple manipulation non industrielle, sont enveloppés dans des feuilles de papier à cigarettes / 2. Un rouleau de tabac visé au paragraphe 1 est considéré, aux fins de l'application de l'accise, comme deux cigarettes lorsqu'il a une longueur, filtre et embout non compris, supérieure à 8 centimètres sans dépasser 11 centimètres (...) ". Par ailleurs, aux termes de l'article 4 de la même directive : " Aux fins de la présente directive, sont considérés comme cigares ou cigarillos, s'ils peuvent être fumés en l'état et, compte tenu de leurs caractéristiques et des attentes normales des consommateurs, sont exclusivement destinés à l'être : / a) les rouleaux de tabac munis d'une cape extérieure en tabac naturel ; / b) les rouleaux de tabac remplis d'un mélange battu et munis d'une cape extérieure en tabac reconstitué, de la couleur normale des cigares, couvrant entièrement le produit, y compris le filtre le cas échéant, mais non l'embout dans le cas des cigares avec embout (...) " et aux termes de l'article 7 de cette directive : " 1. Les cigarettes fabriquées dans l'Union et celles importées de pays tiers sont soumises à une accise ad valorem calculée sur le prix maximal de vente au détail, droits de douane inclus, ainsi qu'à une accise spécifique calculée par unité de produit. ".<br/>
<br/>
              14. Ces stipulations de la directive du 21 juin 2011, relative à la structure et aux taux des accises applicables aux tabacs manufacturés, peuvent être interprétées en ce sens que cette dernière, conformément à son objet fiscal, régirait seulement les prix des produits du tabac en tant qu'ils sont sujets à accises, c'est-à-dire le prix des cigarettes, cigares, cigarillos et tabacs à fumer tels qu'ils sont définis aux articles 2, 3 et 4 cités ci-dessus. Ainsi, si seuls les prix des produits du tabac entendus en tant que tels, indépendamment du mode et de la quantité conditionnée, entraient dans le champ de la directive du 21 juin 2011, le principe de libre détermination des prix des produits du tabac, énoncé à l'article 15 de cette directive et dont la portée a été précisée par la Cour de justice de l'Union européenne, notamment, s'agissant de la République française, dans ses arrêts Commission c/ République française du 27 février 2002 (C-302/00) et du 4 mars 2010 (C-197/08), ne trouverait à s'appliquer qu'à ces seuls prix, que les fabricants peuvent par ailleurs déterminer librement, ainsi qu'il résulte de la lettre même de l'article 572 du code général des impôts. Dans une telle hypothèse, ce principe ne trouvant pas à s'appliquer à la quantité conditionnée de produit, la règle d'expression du prix aux 1 000 unités ou aux 1 000 grammes, en tant qu'elle interdit aux fabricants de faire varier les prix du tabac en fonction de la quantité conditionnée de produits, ne saurait, en tout état de cause, porter atteinte à la libre détermination de ces mêmes prix.<br/>
<br/>
              15. Toutefois, dans ses arrêts Commission c/ République française du 27 février 2002 (C-302/00) et du 4 mars 2010 (C-197/08), la Cour de justice de l'Union européenne a déclaré contraires à l'article 15 de la directive du 21 juin 2011 cité ci-dessus deux versions antérieures de l'article 572 du code général des impôts qui instituaient un prix minimal de vente au détail des produits du tabac au motif qu'un tel prix minimal était susceptible de porter atteinte aux règles de la concurrence en empêchant certains producteurs ou importateurs de tirer profit de prix de revient inférieurs. Les dispositifs de prix minimaux de l'article 572 du code général des impôts prévoyaient, en sus de l'expression du prix des produits du tabac aux 1 000 unités ou aux 1 000 grammes, l'arrimage des prix des produits d'un fabricant sur celui du produit le plus vendu de la marque ou sur une référence égale à 95% de la moyenne constatée sur les produits comparables. Ces arrêts, qui ne se prononcent pas sur le champ d'application de la directive du 21 juin 2011, et notamment pas sur la question de savoir si cette dernière régit la détermination du prix des produits du tabac en conditionnement, peuvent néanmoins être interprétés en ce sens que la Cour de justice de l'Union européenne aurait entendu faire prévaloir, s'agissant des produits du tabac, le principe de libre concurrence sur l'objet fiscal spécifique de la directive du 21 juin 2011, de telle façon que se pose la question de savoir si la liberté de fixation des prix des produits du tabac en fonction de leur conditionnement n'entre pas, nonobstant les termes et définitions que retient la directive du 21 juin 2011, dans le champ d'application de cette dernière.<br/>
<br/>
              16. Ainsi, la question de savoir si la directive du 21 juin 2011 doit être interprétée en ce sens qu'elle ne régit que la détermination du prix des produits du tabac indépendamment de leur conditionnement, ou, au contraire, du prix de ces produits en conditionnement, soulève une difficulté sérieuse.<br/>
<br/>
              S'agissant de la méconnaissance du principe de libre détermination des produits du tabac :<br/>
<br/>
              17. Aux termes de l'article 15 de la directive du 21 juin 2011 : " Les fabricants ou, le cas échéant, leurs représentants ou mandataires dans l'Union, ainsi que les importateurs de pays tiers déterminent librement le prix minimal de vente au détail de chacun de leurs produits pour chaque Etat membre dans lequel ils sont destinés à être mis à la consommation. / La disposition du premier alinéa ne peut, toutefois, faire obstacle à l'application des législations nationales sur le contrôle du niveau des prix ou le respect des prix imposés, pour autant qu'elles soient compatibles avec la réglementation de l'Union ". Il résulte de la jurisprudence de la Cour de justice de l'Union européenne, notamment de son arrêt Commission c/ République française du 4 mars 2010 (C-197/08) cité au point 15 ci-dessus, que ces stipulations, qui sont issues de l'article 9 de la directive 95/59 du 27 novembre 1995 concernant les impôts autres que les taxes sur le chiffre d'affaires frappant la consommation des tabacs manufacturés, doivent être interprétées en ce sens qu'elles visent, d'une part, à assurer que la détermination de l'assiette de l'accise proportionnelle sur les produits du tabac, à savoir le prix maximal de vente au détail de ces produits, soit soumise aux mêmes règles dans tous les Etats membres, et, d'autre part, à préserver la liberté des opérateurs économiques qui leur permet de bénéficier effectivement de l'avantage concurrentiel résultant d'éventuels prix de revient inférieurs. Par ailleurs, aux points 38 et 41 de son arrêt du 4 mars 2010 (C-197/08), la Cour de justice de l'Union européenne a jugé que serait conforme à la directive du 21 juin 2011 un système de prix minimal qui serait aménagé de façon à exclure, en toute hypothèse, qu'il soit porté atteinte à l'avantage concurrentiel qui pourrait résulter, pour certains producteurs ou importateurs, de prix de revient inférieurs.<br/>
<br/>
              18. La règle d'expression du prix des produits aux 1 000 unités ou aux 1 000 grammes interdit seulement aux fabricants, ainsi qu'il a été dit au point 11 ci-dessus, de différencier les prix présentés à l'homologation en fonction d'éventuelles différences de coûts de revient liées au conditionnement de leurs produits en plus grandes quantités. Ainsi, cette règle n'interdit pas aux fabricants de répercuter dans leurs prix leurs différences de coût de revient, quelle qu'en soit l'origine, mais seulement de moduler les prix en question en fonction de la taille des conditionnements. Dans cette mesure, se pose la question de savoir si l'impossibilité d'opérer une telle modulation est, ou non, contraire au principe de libre détermination des prix des produits du tabac tel qu'il découle de l'article 15 de la directive du 21 juin 2011 et de l'interprétation qu'en donne constamment la Cour de justice de l'Union européenne.<br/>
<br/>
              19. Ces questions sont déterminantes pour la solution du litige que doit trancher le Conseil d'Etat. Elles présentent une difficulté sérieuse. Il y a lieu, par suite, d'en saisir la Cour de justice de l'Union européenne en application de l'article 267 du traité sur le fonctionnement de l'Union européenne et, jusqu'à ce que celle-ci se soit prononcée, de surseoir à statuer sur les conclusions de la société JTI France SAS tendant à l'annulation du surplus de la section III de la lettre du 22 novembre 2016. <br/>
<br/>
              20. Il résulte de tout ce qui précède que les sociétés requérantes sont fondées à demander l'annulation, d'une part, de la section III de la lettre du 6 avril 2016 et, d'autre part, du second paragraphe de la section III de la lettre du 22 novembre 2016 ainsi que de son annexe intitulée " Procédure de calcul selon la règle du prix unique " et qu'il y a lieu de surseoir à statuer, ainsi qu'il a été dit au point 19 ci-dessus, sur le surplus des conclusions de la requête n° 407045 demandant l'annulation, pour excès de pouvoir, de la lettre du 22 novembre 2016.<br/>
<br/>
              21. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme globale de 4 500 euros à verser aux sociétés requérantes, au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La section III de la lettre du 6 avril 2016 ainsi que le second paragraphe de la section III et l'annexe de la lettre du 22 novembre 2016 sont annulées.<br/>
Article 2 : Il est sursis à statuer sur les conclusions de la requête des sociétés Japan Tobacco International SA et Japan Tobacco International France SAS qui tendent à l'annulation du surplus de la section III de la lettre du 22 novembre 2016 jusqu'à ce qu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur les questions suivantes :<br/>
1. La directive 2015/64/UE du 21 juin 2011 doit-elle être interprétée en ce sens qu'elle régit, au regard des définitions des produits du tabac qu'elle retient à ses articles 2, 3 et 4, comme régissant également le prix des produits du tabac en conditionnement '<br/>
2. En cas de réponse positive à la question précédente, l'article 15 de la directive du 21 juin 2011, en tant qu'il énonce le principe de libre détermination des prix des produits du tabac, doit-il être interprété comme prohibant une règle de fixation des prix de ces produits aux 1 000 unités ou aux 1 000 grammes qui a pour effet d'interdire aux fabricants de produits du tabac de moduler leurs prix en fonction d'éventuelles différences dans le coût de conditionnement de ces produits '<br/>
Article 3 : L'Etat versera aux sociétés Japan Tobacco International SA et Japan Tobacco International France SAS la somme globale de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée aux sociétés Japan Tobacco International SA et Japan Tobacco International France SAS, au Premier ministre, au ministre de l'action et des comptes publics, à la ministre des solidarités et de la santé ainsi qu'au greffe de la Cour de justice de l'Union européenne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
