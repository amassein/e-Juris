<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032528060</ID>
<ANCIEN_ID>JG_L_2016_05_000000385788</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/52/80/CETATEXT000032528060.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 11/05/2016, 385788</TITRE>
<DATE_DEC>2016-05-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385788</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>COPPER-ROYER</AVOCATS>
<RAPPORTEUR>M. Clément Malverti</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:385788.20160511</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé à la Cour nationale du droit d'asile d'annuler la décision du 10 janvier 2013 par laquelle le directeur général de l'Office français de protection des réfugiés et apatrides a rejeté sa demande d'asile et de lui reconnaître la qualité de réfugié ou, à défaut, de lui accorder le bénéfice de la protection subsidiaire. <br/>
<br/>
              Par une décision n° 13012931 du 8 avril 2014, la Cour nationale du droit d'asile a annulé la décision du directeur général l'Office français de protection des réfugiés et apatrides, a rejeté la demande d'asile de Mme A...mais lui a accordé le bénéfice de la protection subsidiaire.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat le 18 novembre 2014 et le 10 février 2015, Mme A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la décision de la Cour nationale du droit d'asile en tant qu'elle a rejeté sa demande d'asile ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande d'asile ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à Me Copper-Royer, son avocat, au titre des dispositions des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier :<br/>
<br/>
              Vu : <br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New-York le 31 janvier 1967 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Clément Malverti, auditeur, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Copper-Royer, avocat de Mme A...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que les principes généraux du droit applicables aux réfugiés, résultant notamment des stipulations de la Convention de Genève, imposent, en vue d'assurer pleinement au réfugié la protection prévue par la convention, que la même qualité soit reconnue, à raison des risques de persécutions qu'ils encourent également, à la personne de même nationalité qui était unie par le mariage au réfugié à la date à laquelle il a demandé son admission au statut ou qui avait avec lui une liaison suffisamment stable et continue pour former avec lui une famille, ainsi qu'aux enfants de ce réfugié qui étaient mineurs au moment de leur entrée en France ; que dès lors qu'il ressort des éléments qui lui sont soumis que ces conditions sont réunies, il appartient à la Cour nationale du droit d'asile d'accorder à la personne qui lui demande protection le bénéfice du statut de réfugié sur le fondement de ce principe ;<br/>
<br/>
              2.	Considérant qu'il ressort des pièces du dossier soumis au juge du fond que Mme B...A...a épousé M. D...C...en 2009 ; que les deux époux ont chacun présenté une demande d'admission au statut de réfugié auprès de l'Office français de protection des réfugiés et apatrides le 14 octobre 2011 ; que, par deux décisions du 10 janvier 2013, le directeur général de l'Office a rejeté ces demandes ; que, sur recours de M.C..., la Cour nationale du droit d'asile, par une décision du 8 avril 2014, a annulé la décision du directeur général de l'Office le concernant et lui a reconnu la qualité de réfugié ; que la Cour, sur recours de MmeA..., par une décision du même jour rendue au cours de la même audience, a annulé la décision du directeur général de l'Office la concernant et lui a accordé le bénéfice de la protection subsidiaire, mais a refusé de lui reconnaître la qualité de réfugié pour des motifs tenant aux craintes de persécution qu'elle faisait valoir en propre, sans rechercher si la décision prise sur le recours de M. C...devait conduire à accorder le statut de réfugié à Mme A...sur le fondement du principe précédemment rappelé ; que Mme A...est, par suite, fondée à soutenir que la Cour a commis une erreur de droit en omettant d'examiner si elle pouvait se voir reconnaître la qualité de réfugié sur le fondement de ce principe alors qu'il ressortait des éléments soumis à la Cour qu'elle était mariée à M. C...depuis 2009 et que le statut de réfugié était accordé à ce dernier par décision du même jour, et à demander, pour ce motif, l'annulation de la décision qu'elle attaque ;<br/>
<br/>
              3.	Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas partie à la présente instance ; que, par suite, les conclusions présentées sur le fondement de l'article 37 de la loi du 10 juillet 1991 par Me Copper-Royer, avocat de MmeA..., ne peuvent qu'être rejetées ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La décision de la Cour nationale du droit d'asile du 8 avril 2014 est annulée.<br/>
<br/>
Article 2 : L'affaire est renvoyée devant la Cour nationale du droit d'asile.<br/>
<br/>
Article 3 : Le surplus des conclusions du pourvoi est rejeté.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme B...A...et à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-08-05-01-06 - CAS OÙ LA CNDA ACCORDE LE STATUT DE RÉFUGIÉ À UN DEMANDEUR D'ASILE - OBLIGATION D'EXAMINER, MÊME D'OFFICE, L'APPLICATION DU PRINCIPE D'UNITÉ DE LA FAMILLE POUR STATUER SUR LE RECOURS DU CONJOINT DE CE DEMANDEUR - EXISTENCE.
</SCT>
<ANA ID="9A"> 095-08-05-01-06 Après avoir accordé à un demandeur d'asile le statut de réfugié, la Cour nationale du droit d'asile ne peut, sans erreur de droit, refuser à son conjoint le statut de réfugié sans s'interroger sur l'application du principe d'unité de la famille.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
