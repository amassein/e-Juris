<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027988907</ID>
<ANCIEN_ID>JG_L_2013_09_000000350799</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/98/89/CETATEXT000027988907.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 23/09/2013, 350799</TITRE>
<DATE_DEC>2013-09-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>350799</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROGER, SEVAUX, MATHONNET ; SCP GHESTIN ; LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Leïla Derouich</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:350799.20130923</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 11 juillet 2011 et 11 octobre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour le centre hospitalier universitaire de Saint-Etienne dont le siège est à Saint-Etienne Cedex 2 (42055) ; le centre hospitalier universitaire de Saint-Etienne demande au Conseil d'Etat d'annuler l'arrêt n° 09LY01837-09LY01838-09LY02013 de la cour administrative d'appel de Lyon du 7 avril 2011, en tant que cet arrêt a fixé le montant de la rente pour frais futurs d'assistance par tierce personne allouée à M. A...B... ; <br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu la note en délibéré, enregistrée le 13 septembre 2013, présentée pour le centre hospitalier universitaire de Saint-Etienne ;<br/>
	Vu le code de l'action sociale et des familles ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Leïla Derouich, Auditeur,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat du centre hospitalier universitaire de Saint-Etienne, à la SCP Ghestin, avocat de M. B... et à la SCP Roger, Sevaux, Mathonnet, avocat de l'Office national de l'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à  la suite d'une intervention dite ventriculocisternostomie, pratiquée le 14 novembre 2005 au centre hospitalier universitaire de Saint-Etienne  sur M. A...B..., celui-ci a présenté une lésion thalamique à l'origine de graves séquelles neurologiques et a recherché la responsabilité de cet établissement public hospitalier devant le tribunal administratif de Lyon, qui a statué par un jugement du 17 juin 2008 ; que, statuant en appel de ce jugement par un arrêt du 7 avril 2011, la cour administrative d'appel de Lyon a jugé que la responsabilité fautive du centre hospitalier universitaire de Saint-Etienne était engagée et a alloué à M. B...un capital de 329 080,92 euros, ainsi qu'une rente d'un montant annuel de 24 000 euros destinée à couvrir les frais futurs d'assistance par tierce personne ; que le centre hospitalier universitaire de Saint-Etienne se pourvoit en cassation à l'encontre de cet arrêt en tant que la cour n'a pas déduit de la rente ainsi allouée, pour la période postérieure au 7 avril 2011, la prestation de compensation du handicap servie par le département de la Loire à M.B... ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 245-1 du code de l'action sociale et des familles : " Toute personne handicapée résidant de façon stable et régulière en France (...) dont le handicap répond à des critères définis par décret prenant notamment en compte la nature et l'importance des besoins de compensation au regard de son projet de vie, a droit à une prestation de compensation (...) " ; qu'aux termes de l'article L. 245-3  : " La prestation de compensation peut être affectée, dans des conditions définies par décret, à des charges 1° liées à un besoin d'aides humaines y compris, le cas échéant, celles apportées par des aidants familiaux (...) " ;  qu'aux termes de l'article L. 245-4 : " L'élément de la prestation relevant du 1° de l'article L. 245-3 est accordé à toute personne handicapée (...)  lorsque son état nécessite l'aide effective d'une tierce personne pour les actes essentiels de l'existence ou requiert une surveillance régulière (..). Le montant attribué à la personne handicapée est évalué en fonction du nombre d'heures de présence requis par sa situation et fixé en équivalent-temps plein, en tenant compte du coût réel de rémunération des aides humaines en application de la législation du travail et de la convention collective en vigueur. " ; qu'aux termes de l'article L. 245-7 : " (...) Les sommes versées au titre de cette prestation ne font pas l'objet d'un recouvrement à l'encontre du bénéficiaire lorsque celui-ci est revenu à meilleure fortune (...) " ; <br/>
<br/>
              3. Considérant qu'en vertu des principes qui régissent l'indemnisation par une personne publique des victimes d'un dommage dont elle doit répondre, il y a lieu de déduire d'une rente allouée à la victime du dommage dont un établissement public hospitalier est responsable, au titre de l'assistance par tierce personne,  les prestations versées par ailleurs à cette victime et ayant le même objet  ; qu'il en va ainsi tant pour les sommes déjà versées que pour les frais futurs ; que cette déduction n'a toutefois pas lieu d'être lorsqu'une disposition particulière permet à l'organisme qui a versé la prestation d'en réclamer le remboursement si le bénéficiaire revient à meilleure fortune ; <br/>
<br/>
              4. Considérant que la prestation de compensation du handicap, servie par le département de la Loire à M. B...en application de l'article L. 245-1 du code de l'action sociale et des familles, a notamment pour objet de couvrir les frais d'assistance par tierce personne ; qu'en vertu des dispositions de l'article  L. 245-7 du même code, cette prestation ne peut donner lieu à remboursement en cas de retour à meilleure fortune du bénéficiaire ; que, dès lors, c'est à juste titre que la cour administrative d'appel de Lyon,  pour évaluer la somme allouée à M. B...au titre des frais d'assistance par tierce personne échus au 7 avril 2011, a déduit les sommes déjà versées à celui-ci par le département de la Loire au titre de cette prestation de l'indemnité mise à la charge du centre hospitalier universitaire de Saint-Etienne ; qu'en revanche, en ne prévoyant pas que devraient être déduites de la rente annuelle de 24 000 euros, mise à la charge du centre hospitalier universitaire de Saint-Etienne aux fins d'indemniser les frais futurs d'assistance par tierce personne, les sommes qui seraient versées après le 7 avril 2011 par le département de la Loire au titre de cette même prestation, après avoir au besoin prescrit les mesures d'instruction nécessaires auprès du débiteur de l'indemnité, la cour administrative d'appel de Lyon a entaché son arrêt d'une erreur de droit ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que le centre hospitalier universitaire de Saint-Etienne est fondé à demander l'annulation de l'arrêt attaqué, en tant qu'il a fixé le montant de la rente pour frais futurs d'assistance par tierce personne allouée à M. B... ; <br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions formées à ce titre par l'ONIAM et M. B...à l'encontre du centre hospitalier universitaire de Saint-Etienne, qui n'est pas la partie perdante dans la présente instance ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 7 avril 2011 est annulé en tant qu'il a fixé le montant de la rente pour frais futurs d'assistance par tierce personne allouée à M. A... B.... <br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon, dans la mesure de la cassation prononcée. <br/>
<br/>
Article 3 : Les conclusions présentées par l'ONIAM et M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée au centre hospitalier universitaire de Saint-Etienne, à M. A...B..., à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales et à la caisse primaire d'assurance maladie de la Loire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04 AIDE SOCIALE. - POSSIBILITÉ DE RÉCUPÉRATION D'UNE PRESTATION EN CAS DE RETOUR À UNE MEILLEURE FORTUNE - CONSÉQUENCE - DÉDUCTION DE LA PRESTATION DU MONTANT DE LA RENTE AYANT LE MÊME OBJET - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-04-03-07 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. ÉVALUATION DU PRÉJUDICE. MODALITÉS DE FIXATION DES INDEMNITÉS. - RENTE ALLOUÉE AU TITRE DE L'ASSISTANCE PAR TIERCE PERSONNE - MONTANT DE LA RENTE - DÉDUCTION DES PRESTATIONS AYANT LE MÊME OBJET - PRINCIPE - EXISTENCE, TANT POUR LES SOMMES DÉJÀ VERSÉES QUE POUR LES FRAIS FUTURS - EXCEPTION - CAS OÙ L'ORGANISME DÉBITEUR DE LA PRESTATION PEUT EN RÉCLAMER LE REMBOURSEMENT EN CAS DE RETOUR À UNE MEILLEURE FORTUNE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-04-04-02-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. MODALITÉS DE LA RÉPARATION. FORMES DE L'INDEMNITÉ. RENTE. - RENTE ALLOUÉE AU TITRE DE L'ASSISTANCE PAR TIERCE PERSONNE - MONTANT DE LA RENTE - DÉDUCTION DES PRESTATIONS AYANT LE MÊME OBJET - PRINCIPE - EXISTENCE, TANT POUR LES SOMMES DÉJÀ VERSÉES QUE POUR LES FRAIS FUTURS - EXCEPTION - CAS OÙ L'ORGANISME DÉBITEUR DE LA PRESTATION PEUT EN RÉCLAMER LE REMBOURSEMENT EN CAS DE RETOUR À UNE MEILLEURE FORTUNE [RJ1].
</SCT>
<ANA ID="9A"> 04 En vertu des principes qui régissent l'indemnisation par une personne publique des victimes d'un dommage dont elle doit répondre, il y a lieu de déduire d'une rente allouée à la victime du dommage dont un établissement public hospitalier est responsable, au titre de l'assistance par tierce personne, les prestations versées par ailleurs à cette victime et ayant le même objet. Il en va ainsi tant pour les sommes déjà versées que pour les frais futurs. Cette déduction n'a toutefois pas lieu d'être lorsqu'une disposition particulière permet à l'organisme qui a versé la prestation d'en réclamer le remboursement si le bénéficiaire revient à meilleure fortune.</ANA>
<ANA ID="9B"> 60-04-03-07 En vertu des principes qui régissent l'indemnisation par une personne publique des victimes d'un dommage dont elle doit répondre, il y a lieu de déduire d'une rente allouée à la victime du dommage dont un établissement public hospitalier est responsable, au titre de l'assistance par tierce personne, les prestations versées par ailleurs à cette victime et ayant le même objet. Il en va ainsi tant pour les sommes déjà versées que pour les frais futurs. Cette déduction n'a toutefois pas lieu d'être lorsqu'une disposition particulière permet à l'organisme qui a versé la prestation d'en réclamer le remboursement si le bénéficiaire revient à meilleure fortune.</ANA>
<ANA ID="9C"> 60-04-04-02-01 En vertu des principes qui régissent l'indemnisation par une personne publique des victimes d'un dommage dont elle doit répondre, il y a lieu de déduire d'une rente allouée à la victime du dommage dont un établissement public hospitalier est responsable, au titre de l'assistance par tierce personne, les prestations versées par ailleurs à cette victime et ayant le même objet. Il en va ainsi tant pour les sommes déjà versées que pour les frais futurs. Cette déduction n'a toutefois pas lieu d'être lorsqu'une disposition particulière permet à l'organisme qui a versé la prestation d'en réclamer le remboursement si le bénéficiaire revient à meilleure fortune.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 2 octobre 1970, Epoux Pol, n° 76987, p. 543.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
