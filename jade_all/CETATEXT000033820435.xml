<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033820435</ID>
<ANCIEN_ID>JG_L_2016_12_000000392878</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/82/04/CETATEXT000033820435.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 28/12/2016, 392878, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392878</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LEVIS ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Frédéric Pacoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:392878.20161228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Rennes d'annuler pour excès de pouvoir l'arrêté du 14 septembre 2006 par lequel le maire de Pont-Aven (Finistère) a délivré à Mme C...D...un permis de construire pour la réalisation d'une maison d'habitation. Par un jugement nos 0804281, 0804283 du 30 août 2011, le tribunal administratif de Rennes a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 11NT02805 du 5 avril 2013, la cour administrative d'appel de Nantes, sur l'appel de MmeB..., a annulé le jugement du tribunal administratif de Rennes du 30 août 2011 ainsi que l'arrêté du maire de Pont-Aven du 14 septembre 2006.<br/>
<br/>
              Par une décision n° 369147 du 12 novembre 2014, le Conseil d'Etat a, sur le pourvoi de la commune de Pont-Aven, annulé cet arrêt et renvoyé l'affaire à la cour administrative d'appel de Nantes. <br/>
<br/>
              Par un arrêt n° 14NT02987 du 26 juin 2015, statuant sur renvoi du Conseil d'Etat, la cour administrative d'appel de Nantes a rejeté l'appel formé par Mme B...contre le jugement du tribunal administratif de Rennes du 30 août 2011.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 24 août 2015, 24 novembre 2015 et 11 août 2016 au secrétariat du contentieux du Conseil d'Etat, Mme A... B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt de la cour administrative d'appel de Nantes du 26 juin 2015 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Pont-Aven la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code de l'environnement ;<br/>
              - le décret du 21 février 1852 relatif à la fixation des limites des affaires maritimes dans les fleuves et rivières affluant à la mer et sur le domaine public maritime ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Pacoud, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lévis, avocat de MmeB..., et à la SCP Gaschignard, avocat de la commune de Pont-Aven.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 146-1 du code de l'urbanisme, dans sa rédaction applicable à la date du permis de construire en litige, dispose que : " Les dispositions du présent chapitre déterminent les conditions d'utilisation des espaces terrestres, maritimes et lacustres : / - dans les communes littorales définies à l'article 2 de la loi n° 86-2 du 3 janvier 1986 relative à l'aménagement, la protection et la mise en valeur du littoral (...) ". L'article 2 de la loi du 3 janvier 1986, désormais codifié à l'article L. 321-2 du code de l'environnement, dispose que : " Sont considérées comme communes littorales, au sens du présent chapitre, les communes de métropole et des départements d'outre-mer : / 1° Riveraines des mers et océans, des étangs salés, des plans d'eau intérieurs d'une superficie supérieure à 1 000 hectares ; / 2° Riveraines des estuaires et des deltas lorsqu'elles sont situées en aval de la limite de salure des eaux et participent aux équilibres économiques et écologiques littoraux. La liste de ces communes est fixée par décret en Conseil d'Etat, après consultation des conseils municipaux intéressés ". Aux termes de l'article L. 146-4 du code de l'urbanisme alors applicable : " I - L'extension de l'urbanisation doit se réaliser soit en continuité avec les agglomérations et villages existants, soit en hameaux nouveaux intégrés à l'environnement. (...). / II - L'extension limitée de l'urbanisation des espaces proches du rivage (...) doit être justifiée et motivée, dans le plan local d'urbanisme, selon des critères liés à la configuration des lieux ou à l'accueil d'activités économiques exigeant la proximité immédiate de l'eau. / Toutefois, ces critères ne sont pas applicables lorsque l'urbanisation est conforme aux dispositions d'un schéma de cohérence territoriale ou d'un schéma d'aménagement régional ou compatible avec celles d'un schéma de mise en valeur de la mer. / En l'absence de ces documents, l'urbanisation peut être réalisée avec l'accord du représentant de l'Etat dans le département. Cet accord est donné après que la commune a motivé sa demande et après avis de la commission départementale compétente en matière de nature, de paysages et de sites appréciant l'impact de l'urbanisation sur la nature. Les communes intéressées peuvent également faire connaître leur avis dans un délai de deux mois suivant le dépôt de la demande d'accord. Le plan local d'urbanisme doit respecter les dispositions de cet accord. (...) ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 14 septembre 2006, le maire de Pont-Aven a délivré à Mme D...un permis de construire pour la réalisation d'une maison d'habitation au lieu-dit Coteau du Bourgneuf. Par un premier arrêt du 5 avril 2013, statuant sur l'appel de MmeB..., voisine du terrain d'assiette du projet, la cour administrative d'appel de Nantes a annulé ce permis de construire au motif qu'il méconnaissait les dispositions du I de l'article L. 146-4 du code de l'urbanisme. Pour juger que la commune de Pont-Aven devait être regardée comme une commune littorale au sens et pour l'application des articles L. 146-1 et suivants du code de l'urbanisme, elle s'est fondée sur ce qu'une partie de son territoire jouxtait l'Aven en aval de la limite transversale de la mer telle que cette limite avait été fixée à l'embouchure de la rivière Aven, par un décret du 3 juin 1899. Par une décision du 12 novembre 2014, le Conseil d'Etat, statuant au contentieux, a annulé cet arrêt au motif qu'en statuant ainsi, sans mettre les parties à même de débattre de la portée du décret du 3 juin 1899 qu'aucune d'entre elles n'avait invoqué, alors que la délimitation de la mer pouvait être utilement discutée à l'occasion du litige dont elle était saisie, la cour avait méconnu les exigences du caractère contradictoire de la procédure. Par un second arrêt du 26 juin 2015, statuant de nouveau sur l'appel de Mme B...après renvoi de l'affaire par le Conseil d'Etat, la cour l'a rejeté en jugeant, notamment, que le permis ne méconnaissait pas, en tout état de cause, les dispositions des I et II de l'article L. 146-4 du code de l'urbanisme.<br/>
<br/>
              3. En premier lieu, en écartant les moyens tirés de la méconnaissance des dispositions des I et II de l'article L. 146-4 du code de l'urbanisme sans prendre expressément parti sur l'application à la commune de Pont-Aven des dispositions des articles L. 146-1 et suivants du code de l'urbanisme particulières au littoral, la cour n'a ni méconnu l'autorité de chose jugée s'attachant à la décision du Conseil d'Etat du 12 novembre 2014, qui avait annulé son premier arrêt pour un vice de procédure, ni insuffisamment motivé son arrêt.<br/>
<br/>
              4. En deuxième lieu, à la suite de l'annulation totale de son premier arrêt, prononcée par le Conseil d'Etat le 12 novembre 2014, la cour retrouvait sa plénitude de juridiction pour statuer sur l'affaire qui lui était renvoyée. Par suite, elle pouvait, sans méconnaître aucun principe, lors du nouvel examen du dossier, porter sur les faits de l'espèce une appréciation différente de celle à laquelle elle s'était livrée précédemment. La requérante ne saurait ainsi soutenir que la cour aurait méconnu la chose jugée et le principe de sécurité juridique en adoptant, par l'arrêt attaqué, une solution différente de celle de son arrêt du 5 avril 2013.<br/>
<br/>
              5. En troisième lieu, il résulte des dispositions du I de l'article L. 146-4 du code de l'urbanisme, éclairées par les travaux préparatoires de la loi du 3 janvier 1986 dont elles sont issues, que les constructions peuvent être autorisées dans les communes littorales en continuité avec les agglomérations et villages existants, c'est-à-dire avec les zones déjà urbanisées, caractérisées par un nombre et une densité significative de constructions, mais qu'aucune construction ne peut en revanche être autorisée, même en continuité avec d'autres, dans les zones d'urbanisation diffuse éloignées de ces agglomérations et villages. <br/>
<br/>
              6. Pour juger que le projet litigieux se situait en continuité avec les agglomérations et villages existants, la cour a relevé que le terrain d'assiette du projet s'insérait dans un secteur dans lequel la quasi-totalité des parcelles étaient déjà bâties et la densité des constructions était significative, et qu'il s'inscrivait dans le prolongement du quartier de Bourgneuf, au nord, en continuité avec le centre-bourg de Pont-Aven. En écartant ainsi le moyen tiré de la méconnaissance du I de l'article L. 146-4 du code de l'urbanisme, par un arrêt suffisamment motivé, la cour, qui, contrairement à ce qui est soutenu, ne s'est pas fondée sur le coefficient d'occupation des sols déterminé par le plan local d'urbanisme, n'a commis aucune erreur de droit. Si elle a relevé que le secteur considéré était délimité au sud par la rue de la Côte du Bourgneuf, alors que cette rue se situe au nord du terrain, cette circonstance est restée sans effet sur l'appréciation souveraine des faits de l'espèce à laquelle elle s'est livrée et qui est exempte de dénaturation.<br/>
<br/>
              7. En quatrième lieu, d'abord, il résulte des dispositions du II de l'article L. 146-4 du code de l'urbanisme cité au point 1 qu'en l'absence tant d'un plan local d'urbanisme fixant des critères spécifiques justifiant l'extension limitée de l'urbanisation des espaces proches du rivage que d'un schéma de cohérence territoriale, d'un schéma d'aménagement régional ou d'un schéma de mise en valeur de la mer, une construction constituant une telle extension ne peut être autorisée qu'avec l'accord du préfet et le plan local d'urbanisme doit respecter les dispositions de cet accord. Par suite, la cour n'a pas commis d'erreur de droit en jugeant que l'extension de l'urbanisation avait été rendue possible par l'accord du préfet, alors même que la commune avait approuvé, le 26 janvier 2004, un plan local d'urbanisme. Ensuite, en estimant que le préfet du Finistère avait ainsi donné son accord à l'extension limitée de l'urbanisation, la cour ne s'est pas méprise sur la portée de l'acte du 4 octobre 2001 par lequel le préfet a communiqué au maire de Pont-Aven, sans émettre aucune réserve, l'avis favorable que la commission départementale des sites avait adopté au vu du rapport en ce sens du directeur départemental de l'équipement. Enfin, si la requérante soutient que l'avis de la commission départementale des sites reposait sur une appréciation manifestement erronée des caractéristiques de la zone, elle ne critique ainsi aucun des motifs de l'arrêt attaqué.<br/>
<br/>
              8. En dernier lieu, la cour n'a pas entaché son arrêt de contradiction de motifs en jugeant, d'une part, que l'extension limitée de l'urbanisation résultant du projet litigieux avait reçu l'accord du préfet après avis de la commission départementale des sites et en regardant, d'autre part, comme sans incidence sur la légalité du permis de construire attaqué la méconnaissance de cet avis en tant qu'il se rapportait à des aspects du projet ultérieurement modifiés par MmeD....<br/>
<br/>
              9. Il résulte de tout ce qui précède que Mme B...n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              10. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme B...la somme que la commune de Pont-Aven demande au titre de l'article L. 761-1 du code de justice administrative. Les mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Pont-Aven, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme B...est rejeté.<br/>
Article 2 : Les conclusions de la commune de Pont-Aven présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à Mme A...B..., à la commune de Pont-Aven et à Mme C...D.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
