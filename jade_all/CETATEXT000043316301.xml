<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043316301</ID>
<ANCIEN_ID>JG_L_2021_03_000000437967</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/31/63/CETATEXT000043316301.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 31/03/2021, 437967, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437967</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MELKA - PRIGENT</AVOCATS>
<RAPPORTEUR>M. Laurent Roulaud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:437967.20210331</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé à la Cour nationale du droit d'asile d'annuler la décision du 28 avril 2017 par laquelle l'Office français de protection des réfugiés et apatrides a rejeté sa demande d'asile. <br/>
<br/>
              Par une décision n° 17023309 du 28 août 2019, la Cour nationale du droit d'asile a rejeté sa demande.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 27 janvier et 11 juin 2020 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) de mettre à la charge de l'Office français de protection des réfugiés et apatrides une somme de 3 500 euros à verser à la SCP Melka-Prigent, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
              Mme A... soutient que la Cour nationale du droit d'asile a entaché sa décision : <br/>
<br/>
              - de dénaturation des pièces du dossier en estimant, d'une part, que son arrestation en octobre 2015 pour avoir hébergé sa cousine était une circonstance dont elle n'avait pas fait état précédemment et, d'autre part, que le certificat médical du 15 mars 2017 attestant de mauvais traitements n'était pas accompagné d'un certificat de prise en charge psychologique et médicamenteuse ;<br/>
              - d'erreur de droit en écartant le certificat du 15 mars 2017 au motif qu'il ne permettait pas de définir les circonstances et les auteurs des cicatrices relevées ;<br/>
              - d'insuffisance de motivation en ne se prononçant pas sur les pièces produites à l'appui de son mémoire du 10 juillet 2019 attestant des menaces pesant sur sa famille et de son engagement en faveur des tamouls depuis son arrivée en France ; <br/>
              - de dénaturation des pièces du dossier en estimant non établis les faits allégués et non fondées les craintes énoncées.<br/>
<br/>
<br/>
              Le pourvoi a été communiqué à l'Office français de protection des réfugiés et apatrides qui n'a pas produit de mémoire.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New York le 31 janvier 1967 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Roulaud, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Melka - Prigent, avocat de Mme A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Mme A..., de nationalité sri-lankaise et d'origine tamoule, se pourvoit en cassation contre la décision du 28 août 2019 par laquelle la Cour nationale du droit d'asile a rejeté son recours contre la décision du 28 avril 2017 de l'Office français de protection des réfugiés et apatrides rejetant sa demande d'asile.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis à la Cour nationale du droit d'asile que Mme A... avait produit un certificat médical en date du 15 mars 2017 mentionnant un certain nombre de cicatrices et un syndrome post traumatique. Ce certificat, qui indiquait qu'il était remis à l'intéressée " une lettre d'adressage pour une prise en charge psychothérapeutique au centre F. Minkowska ", était assorti de plusieurs fiches de rendez-vous pris auprès de ce centre au cours des années 2017 à 2019 ainsi que d'ordonnances prescrivant des médicaments dans le cadre de cette prise en charge. Dès lors, en se fondant, pour écarter ce certificat médical, sur le fait qu'il n'était pas accompagné d'un certificat de prise en charge psychologique et médicamenteuse de la requérante, la Cour a dénaturé les pièces du dossier qui lui était soumis. <br/>
<br/>
              3. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que Mme A... est fondée à demander l'annulation de la décision qu'elle attaque. <br/>
<br/>
              4. Mme A... a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Melka-Prigent, avocat de Mme A..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Office français de protection des réfugiés et apatrides la somme de 3 000 euros à verser à cette société.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision de la Cour nationale du droit d'asile du 28 août 2019 est annulée.<br/>
Article 2 : L'affaire est renvoyée à la Cour nationale du droit d'asile.<br/>
Article 3 : L'Office français de protection des réfugiés et apatrides versera la somme de 3 000 euros à la SCP Melka-Prigent, avocat de Mme A..., au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à Mme B... A... et à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
