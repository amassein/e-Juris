<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042164476</ID>
<ANCIEN_ID>JG_L_2020_07_000000441228</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/16/44/CETATEXT000042164476.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 08/07/2020, 441228, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441228</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLEZ</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:441228.20200708</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 16 et 30 juin 2020 au secrétariat du contentieux du Conseil d'Etat, l'Association des avocats conseils d'entreprises demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) à titre principal, d'ordonner la suspension de l'exécution de l'ordonnance n° 2020-115 du 12 février 2020 renforçant le dispositif national de lutte contre le blanchiment de capitaux et le financement du terrorisme ;<br/>
<br/>
              2°) à titre subsidiaire, de renvoyer à la Cour de justice de l'Union européenne à titre préjudiciel la question de savoir si l'ordonnance attaquée est conforme au droit de l'Union européenne en tant qu'elle ajoute l'activité de conseil en matière fiscale à la liste des opérations à raison desquelles les avocats sont soumis aux obligations de la lutte contre le blanchiment des capitaux et le financement du terrorisme ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - elle justifie d'un intérêt à agir ;<br/>
              - la condition d'urgence est remplie eu égard, en premier lieu, à l'atteinte grave et immédiate que porte l'ordonnance attaquée aux droits et libertés des avocats dont elle défend les intérêts en ce que, d'une part, elle méconnaît le droit de l'avocat au secret professionnel tel que garanti par les articles 6 et 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et, d'autre part, elle est incompatible avec les obligations déontologiques leur incombant, en deuxième lieu, à l'imprévisibilité de ses dispositions quant au périmètre de l'assujettissement aux obligations de lutte contre le blanchiment de capitaux et quant à la portée des exonérations, prévues au II de l'article L. 561-3 du code monétaire et financier et, en dernier lieu, à l'éventualité de sanctions disciplinaires, pénales ou propres aux manquements aux obligations déclaratives ;<br/>
              - il existe un doute sérieux sur la légalité de l'ordonnance attaquée ;<br/>
              - l'ordonnance attaquée méconnaît l'étendue de l'habilitation reçue de l'article 203 de la loi n° 2019-846 du 22 mai 2019 pour la transposition de la directive 2018/843 du 30 mai 2018 ;<br/>
              - elle méconnaît la circulaire NOR PRMX1721468C du 26 juillet 2017 dès lors qu'il n'est fait état d'aucune circonstance justifiant d'aller au-delà des exigences posées par cette directive ;<br/>
              - elle méconnaît la directive 2018/843 du 30 mai 2018 en ce qu'elle étend le périmètre d'assujettissement aux obligations de lutte contre le blanchiment des capitaux et le financement du terrorisme défini de façon contraignante par le législateur européen ;<br/>
              - elle méconnaît les dispositions de la directive 2018/958 du 28 juin 2018 et celles de l'article 15 de la directive 2006/123/CE du 12 décembre 2006, dès lors que, du fait de la violation du secret professionnel qu'elle entraîne, elle est disproportionnée au regard de l'objectif poursuivi de lutte contre le blanchiment des capitaux et le financement du terrorisme ;<br/>
              - elle méconnaît les exigences de sécurité juridique, de clarté et de précision découlant notamment de l'article 7 la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales compte tenu, d'une part, du caractère large et indéterminé de la notion d'interposition de personne et, d'autre part, de l'incertitude qu'elle crée quant à la portée des exonérations d'obligations déclaratives en faveur de la consultation juridique prévues au II de l'article L. 561-3 du code monétaire financier, l'activité de conseil fiscal, qui participe par sa nature même de l'activité consultative, ne bénéficiant plus de l'effet exonératoire attaché au secret professionnel ;<br/>
              - elle méconnaît, d'une part, les principes de responsabilité personnelle et de personnalité des peines garantis par les articles 8 et 9 de la Déclaration des droits de l'homme et du citoyen et, d'autre part, le principe de l'indépendance des avocats, dès lors qu'un avocat est désormais tenu d'établir une déclaration de soupçon, sous peine de sanction pénale, à raison de la simple fourniture d'un conseil fiscal par une personne interposée avec laquelle il serait lié.<br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 19 juin 2020, le ministre de l'économie et des finances conclut au rejet de la requête. Il soutient que l'urgence n'est pas caractérisée et qu'aucun des moyens soulevés n'est de nature à faire naître un doute sérieux sur la légalité des dispositions contestées.<br/>
<br/>
              La requête a été communiquée au Premier ministre, au garde des sceaux, ministre de la justice, et au ministre de l'action et des comptes publics, qui n'ont pas présenté d'observations.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la directive 2006/123/CE du Parlement européen et du Conseil du 12 décembre 2006 ;<br/>
              - la directive (UE) 2015/849 du Parlement européen et du Conseil du 20 mai 2015 ;<br/>
              - la directive (UE) 2018/843 du Parlement européen et du Conseil du 30 mai 2018 ;<br/>
              - la directive (UE) 2018/958 du Parlement européen et du Conseil du 28 juin 2018 ;<br/>
              - le code monétaire et financier ; <br/>
              - loi n° 2019-486 du 22 mai 2019, notamment son article 203 ;<br/>
              - le décret n° 91-1197 du 27 novembre 1991 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
              Les parties ont été informées, sur le fondement de l'article 9 de l'ordonnance du 25 mars 2020 portant adaptation des règles applicables devant les juridictions de l'ordre administratif, de ce qu'aucune audience ne se tiendrait et de ce que la clôture de l'instruction serait fixée le 7 juillet 2020 à 16 heures.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              2. La directive (UE) 2015/849 du Parlement et du Conseil du 20 mai 2015 relative à la prévention de l'utilisation du système financier aux fins du blanchiment de capitaux ou du financement du terrorisme, modifiant le règlement (UE) n° 648/2012 du Parlement européen et du Conseil et abrogeant la directive 2005/60/CE du Parlement européen et du Conseil et la directive 2006/70/CE de la Commission assujettit les " entités " qu'elle désigne à des obligations de plusieurs natures en vue de prévenir l'utilisation du système financier de l'Union aux fins du blanchiment de capitaux et du financement du terrorisme. Ces entités sont notamment soumises, en premier lieu et en vertu du chapitre II de la directive, à des obligations de vigilance à l'égard de leur clientèle, en deuxième lieu et en vertu du chapitre IV, à des obligations de déclaration à l'égard d'une cellule de renseignements financier (CRF) et, en troisième lieu et en vertu du chapitre V, à des obligations de conservation de données.<br/>
<br/>
              3. Dans sa rédaction initiale, la directive s'appliquait, d'une part, en vertu du a) du 3) du 1. de son article 2, aux " auditeurs, experts-comptables externes et conseillers fiscaux " agissant dans l'exercice de leur activité professionnelle et, d'autre part, en vertu du b) du 3) du 1. de ce même article, aux " notaires et autres membres de professions juridiques indépendantes " soit lorsqu'ils participent, au nom de leur client et pour le compte de celui-ci, à toute transaction financière ou immobilière, soit lorsqu'ils assistent leur client dans la préparation ou l'exécution de transactions portant sur des objets limitativement énumérés : achat et vente de biens immeubles ou d'entreprises commerciales ; gestion de fonds, de titres ou d'autres actifs appartenant au client ; ouverture ou gestion de comptes bancaires, d'épargne ou de portefeuilles ; organisation des apports nécessaires à la constitution, à la gestion ou à la direction de sociétés ; constitution, gestion ou direction de fiducies/trusts, de sociétés, de fondations ou de structures similaires.<br/>
<br/>
              4. L'article 34 de la directive prévoit toutefois que les Etats membres n'appliquent pas les obligations de déclaration auprès de la CRF prévues à l'article 33 " aux notaires, aux membres des autres professions juridiques indépendantes, aux auditeurs, aux experts-comptables externes ni aux conseillers fiscaux " pour ce qui concerne " les informations qu'ils reçoivent de l'un de leurs clients ou obtiennent sur l'un de leurs clients, lors de l'évaluation de la situation juridique de ce client ou dans l'exercice de leur mission de défense ou de représentation de ce client dans une procédure judiciaire ou concernant une telle procédure, y compris dans le cadre de conseils relatifs à la manière d'engager ou d'éviter une procédure, que ces informations soient reçues ou obtenues avant, pendant ou après cette procédure ".<br/>
<br/>
              5. La directive (UE) 2018/843 du 30 mai 2018 a, par le a) du 1. de son article 1er, modifié le a) du 3) du 1. de l'article 2 de la directive du 20 mai 2015 pour prévoir que cette directive d'applique désormais non seulement aux " auditeurs, experts-comptables externes et conseillers fiscaux " mais également à toute autre personne qui " s'engage à fournir, directement ou par le truchement d'autres personnes auxquelles cette autre personne est liée, une aide matérielle, une assistance ou des conseils en matière fiscale comme activité économique ou professionnelle principale "<br/>
<br/>
              6. Le code monétaire et financier comporte au titre VI de son livre V un chapitre Ier intitulé " Obligations relatives à la lutte contre le blanchiment des capitaux et le financement du terrorisme " dont les dispositions ont notamment pour objet de transposer les directives relatives à la prévention de l'utilisation du système financier aux fins du blanchiment de capitaux ou du financement du terrorisme. L'article L. 561-2 du code et monétaire et financier fixe la liste des personnes et institutions qui sont assujetties à ces obligations, dont le contenu est défini par les sections 2 à 7 de ce chapitre. Il découle du 13° de cet article qu'y sont soumis " Les avocats au Conseil d'Etat et à la Cour de cassation, les avocats, les notaires, les huissiers de justice, les administrateurs judiciaires, les mandataires judiciaires et les commissaires-priseurs judiciaires, dans les conditions prévues à l'article L. 561-3 ". <br/>
<br/>
              7. Dans sa rédaction antérieure à la transposition de la directive du 30 mai 2018, l'article L. 561-3 du code monétaire et financier disposait que les personnes mentionnées au 13° de l'article L. 561-2 étaient soumises aux dispositions du chapitre Ier du titre VI du livre V lorsque, dans le cadre de leur activité professionnelle, soit elles participaient au nom et pour le compte de leur client à toute transaction financière ou immobilière ou agissaient en qualité de fiduciaire, soit elles assistaient leur client dans la préparation ou la réalisation de transactions ayant un objet compris dans une liste limitative correspondant à celle fixée par le b) du 3) du 1. de l'article 2 de la directive du 20 mai 2015.<br/>
<br/>
              8. Par le b) du 3° de son article 2, l'ordonnance attaquée du 12 février 2020 renforçant le dispositif national de lutte contre le blanchiment de capitaux et le financement du terrorisme, prise sur habilitation de l'article 203 de la loi du 22 mai 2019 relative à la croissance et la transformation des entreprises et pour la transposition de la directive du 20 mai 2015 modifiée par celle du 30 mai 2018, a ajouté au I de l'article L. 561-3 du code monétaire et financier un alinéa ayant pour objet de soumettre aux dispositions du chapitre Ier du titre VI du livre V du code les personnes mentionnées au 13° de l'article L. 561-2 qui, dans le cadre de leur activité professionnelle, " fournissent, directement ou par toute personne interposée à laquelle elles sont liées, des conseils en matière fiscale ".<br/>
<br/>
              9. L'association requérante soutient qu'en soumettant ainsi aux obligations découlant de la directive du 20 mai 2015 les avocats lorsqu'ils ont pour activité la fourniture à leurs clients de conseils en matière fiscale, l'ordonnance contestée méconnaitrait la portée de la modification résultant de l'article 1er de la directive du 30 mai 2018, qui n'aurait pas pour objet d'étendre au-delà de l'énumération prévue au b) du 3) du 1. de l'article 2 de la directive du 20 mai 2015 le champ des activités et opérations au titre desquelles les " notaires et autres membres de professions juridiques indépendantes ", et notamment les avocats, sont soumis aux obligations découlant de cette directive.<br/>
<br/>
              10. Il résulte toutefois de la lettre même des dispositions de la directive du 30 mai 2018 que ses auteurs ont entendu inclure dans le champ des obligations posées par la directive du 20 mai 2015 en matière de prévention de l'utilisation du système financier aux fins du blanchiment de capitaux ou du financement du terrorisme l'ensemble des prestations d'assistance ou de conseil en matière fiscale, quelle que soit la personne qui les fournit et sans exclure, par suite, ces prestations lorsqu'elles sont rendues par des notaires ou d'autres professions juridiques indépendantes, et notamment les avocats. Il en résulte que les notaires et autres professions juridiques sont soumis à ces obligations, alors mêmes que l'activité de fourniture d'assistance ou de conseil en matière fiscale ne figure pas dans la liste prévue par le b) du 3) du 1. de l'article 2 de la directive du 20 mai 2015, sans préjudice de l'application à ces mêmes professions de la dispense d'obligation de transmission à la CRF prévue par l'article 34 de cette même directive pour ce qui concerne les informations obtenues avant, pendant ou après une procédure judiciaire ou lors de l'évaluation de la situation juridique d'un client, ce y compris en matière fiscale, dont le II de l'article L. 561-3 du code monétaire et financier assure la transposition en droit interne. <br/>
<br/>
              11. Il résulte de ce qui précède que ne saurait être regardé comme de nature à faire naître un doute sérieux sur la légalité du b) du 3° de l'article 2 de l'ordonnance du 12 février 2020 le moyen tiré de ce qu'il aurait inséré au 3° du I de l'article L. 561-3 du code monétaire et financier des dispositions allant au-delà de celles des directives qu'il a pour objet de transposer. Il en va de même, par suite, des moyens tirés de ce que l'ordonnance aurait méconnu les limites de l'habilitation reçue du Parlement et de ce que cette ordonnance serait intervenue dans des conditions irrégulières faute d'étude d'impact.<br/>
<br/>
              12. Ne peuvent davantage être regardés comme de nature à faire naître un tel doute, compte tenu de la nécessaire combinaison des dispositions du 3° du I de l'article L. 561-3 du code monétaire et financier avec celles du II du même article, dont la portée ne se trouve pas restreinte par les premières, les moyens tirés de ce que les dispositions contestées méconnaîtraient les stipulations de l'article 7 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, qui garantissent le respect du principe de légalité des délits et des peines, ainsi que les exigences liées au respect du secret professionnel de l'avocat, qui découlent de l'article 66-5 de la loi du 31 décembre 1971 portant réforme de certaines professions judiciaires et juridiques et des stipulations de l'article 8 de la même convention. Doit également être regardé comme non sérieux, dès lors que seule est susceptible de d'être sanctionnée une méconnaissance par l'avocat des obligations que la directive fait peser sur lui, le moyen tiré de la méconnaissance du principe de personnalité des peines, qui découle des articles 8 et 9 de la Déclaration des droits de l'homme et du citoyen de 1789. <br/>
<br/>
              13. Enfin, dès lors que les dispositions contestées de l'ordonnance du 12 février 2020 se bornent à opérer la transposition en droit interne de dispositions d'une directive, ne peut qu'être regardé comme non sérieux le moyen tiré de ce qu'elles méconnaîtraient le principe de proportionnalité des dispositions législatives, réglementaires ou administratives limitant l'accès à des professions réglementées ou leur exercice, qui découle de l'article 15 de la directive n° 2006/123/CE du 12 décembre 2006 relative aux services dans le marché intérieur et de la directive n° 2018/958 du 28 juin 2018 relative à un contrôle de proportionnalité avant l'adoption d'une nouvelle réglementation de professions. <br/>
<br/>
              14. Il résulte de ce que précède, sans qu'il soit besoin de se prononcer sur la condition d'urgence, que les conclusions à fin de suspension du b) du 3° de l'article 2 de l'ordonnance du 12 février 2020 doivent être rejetées.<br/>
<br/>
              15. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat, qui n'est pas la partie perdante dans ce litige, au titre des frais exposés et non compris dans les dépens.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de l'Association des avocats conseils d'entreprises est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à l'Association des avocats conseils d'entreprises et au ministre de l'économie des finances et de la relance.<br/>
Copie en sera adressée au Premier ministre et au garde des sceaux, ministre de la justice. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
