<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044806264</ID>
<ANCIEN_ID>JG_L_2021_12_000000456292</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/80/62/CETATEXT000044806264.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 30/12/2021, 456292, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>456292</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD</AVOCATS>
<RAPPORTEUR>Mme Françoise Tomé</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:456292.20211230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Mme E... B..., aux droits de laquelle est venue sa fille Mme C... B..., a porté plainte contre M. A... D... devant de la chambre disciplinaire de première instance d'Ile-de-France de l'ordre des médecins. Le conseil départemental des Hauts-de-Seine de l'ordre des médecins s'est associé à la plainte. Par une décision du 6 juin 2019, la chambre disciplinaire de première instance a infligé à M. D... la sanction de l'interdiction d'exercer la médecine pendant une durée de trois mois. <br/>
<br/>
              Par une décision du 21 juillet 2021, la chambre disciplinaire nationale de l'ordre des médecins a rejeté l'appel de M. D... contre cette décision et ordonné que la sanction soit exécutée du 1er novembre 2021 au 31 janvier 2022. <br/>
<br/>
              1° Sous le n° 456292, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 3 septembre et 26 novembre 2021 au secrétariat du contentieux du Conseil d'Etat, M. D... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ; <br/>
<br/>
              2°) de mettre à la charge de Mme B... et du conseil départemental des Hauts-de-Seine de l'ordre des médecins la somme de 3 500 euros au titre de l'article L. 761-1 du code justice administrative.<br/>
<br/>
              2° Sous le n° 456915, par une requête et un mémoire complémentaire, enregistrés les 21 septembre et 26 novembre 2021 au secrétariat du contentieux du Conseil d'Etat, M. D... demande au Conseil d'Etat de suspendre l'exécution de la même décision du 21 juillet 2021 de la chambre disciplinaire nationale de l'ordre des médecins. <br/>
<br/>
              M. D... soutient que cette décision risque d'entraîner pour lui des conséquences difficilement réparables et que les moyens soulevés à l'appui de son pourvoi sont de nature à justifier, outre l'annulation de la décision, l'infirmation de la solution retenue par les juges du fond.<br/>
<br/>
              La requête a été communiquée à Mme B... et au conseil départemental des Hauts-de-Seine de l'ordre des médecins qui n'ont pas présenté de mémoire en défense. <br/>
<br/>
<br/>
              Le Conseil national de l'ordre des médecins a présenté des observations, enregistrées les 21 octobre et 1er décembre 2021<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces des dossiers ;<br/>
	Vu : <br/>
	- le code de la santé publique ;<br/>
	- le code de justice administrative ; <br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Françoise Tomé, conseillère d'Etat, <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Richard, avocat de M. D... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le pourvoi par lequel M. D... demande l'annulation de la décision du 21 juillet 2021 de la chambre disciplinaire nationale de l'ordre des médecins et la requête par laquelle il demande qu'il soit sursis à l'exécution de cette décision présentent à juger les mêmes questions. Il y a lieu d'y statuer par une seule décision.<br/>
<br/>
              2. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              3. Pour demander l'annulation de la décision de la chambre disciplinaire nationale de l'ordre des médecins qu'il attaque, M. D... soutient qu'elle est entachée :<br/>
              - d'irrégularité, en ce qu'en méconnaissance du principe du contradictoire et de l'égalité des armes, il n'a pas été régulièrement convoqué à l'audience ;<br/>
              - de dénaturation des pièces du dossier en ce qu'elle juge qu'il a refusé de communiquer à la patiente son dossier médical alors qu'il résulte des pièces du dossier qu'il était en sa possession en novembre 2016 ;<br/>
              - de dénaturation des pièces du dossier en ce qu'elle estime qu'il s'est borné à prescrire à sa patiente " de la Lamaline et du repos " ;<br/>
               - d'insuffisance de motivation faute d'indiquer la teneur des propos blessants qui lui sont reprochés ;<br/>
               - d'inexacte qualification juridique des faits, d'insuffisance de motivation et de dénaturation des pièces du dossier en ce qu'elle juge qu'il n'a pas prodigué à sa patiente des soins consciencieux, alors qu'elle a fait l'objet d'une complication tardive, non détectable et rare. <br/>
<br/>
              Il soutient, en outre, que la sanction est hors de proportion avec les faits reprochés. <br/>
<br/>
              4. Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
              5. Le pourvoi formé par M. D... contre la décision du 21 juillet 2021 de la chambre disciplinaire nationale de l'ordre des médecins n'étant pas admis, les conclusions qu'il présente aux fins de sursis à exécution de cette décision sont devenues sans objet.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. D... n'est pas admis.<br/>
Article 2 : Il n'y a pas lieu de statuer sur la requête de M. D... tendant à ce qu'il soit sursis à l'exécution de la décision du 21 juillet 2021 de la chambre disciplinaire nationale de l'ordre des médecins.<br/>
Article 3 : La présente décision sera notifiée à M. A... D..., au conseil départemental des Hauts-de-Seine de l'ordre des médecins et à Mme C... B....<br/>
Copie en sera adressée au Conseil national de l'ordre des médecins.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
