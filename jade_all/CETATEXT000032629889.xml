<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032629889</ID>
<ANCIEN_ID>JG_L_2014_03_000000376151</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/62/98/CETATEXT000032629889.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 31/03/2014, 376151, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-03-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>376151</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2014:376151.20140331</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 7 mars 2014 au secrétariat du contentieux du Conseil d'Etat, présentée pour la société SARIA Industries, dont le siège social est situé 24, rue de Martre à Clichy (92110), la société ARVAL Sud-est, dont le siège social est situé sis Les Bouillots à Bayet (03500), la société SIFDDA Bretagne, dont le siège social est situé Usines des Veaux à Guer (56380), la société SIFDDA Centre, dont le siège social est situé sis Route de Niort à Benet (85490), agissant par leurs représentants légaux en exercice ; les sociétés requérantes demandent au juge des référés du Conseil d'Etat : <br/>
<br/>
              1°) d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution du point 4 de la note du 27 décembre 2013 du directeur général de l'alimentation du ministère de l'agriculture destinée aux directeurs départementaux de la protection des populations et aux directeurs départementaux de la cohésion sociale;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              elles soutiennent que :<br/>
              - la condition d'urgence est remplie, dès lors que la note litigieuse porte atteinte à leurs intérêts ainsi qu'à l'intérêt public tiré du respect des conditions sanitaires ;<br/>
              - la note litigieuse comporte des dispositions impératives et peut être attaquée ;<br/>
              - elle méconnaît les dispositions du règlement n° 1069/2009 du 21 octobre 2009 et celles du règlement n° 142/ 2011 du 25 février 2011 ;<br/>
<br/>
<br/>
              Vu la note dont la suspension de l'exécution est demandée ;<br/>
<br/>
               Vu la copie de la requête à fin d'annulation de cette décision ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 21 mars 2014, présenté par le ministre de l'agriculture, de l'agroalimentaire et de la pêche, qui conclut au rejet de la requête ; <br/>
<br/>
              il soutient que :<br/>
              - la requête est irrecevable dès lors que, d'une part, la note litigieuse ne comporte pas de dispositions impératives et que, d'autre part, les sociétés requérantes sont dépourvues d'intérêt à agir ; <br/>
              -	la condition d'urgence n'est pas remplie ;<br/>
              -	l'urgence n'est pas caractérisée dès lors que la suspension de la note litigieuse n'aurait aucun effet sur la situation des sociétés requérantes ou sur la santé publique ; <br/>
              -	aucun moyen soulevé n'est de nature à créer un doute sérieux quant à la légalité de la décision contestée ;<br/>
<br/>
              		Vu le mémoire en réplique et en intervention, enregistré le 26 mars 2014, présenté pour les sociétés requérantes et la société SIRAM, qui concluent aux mêmes fins que leur requête et aux mêmes moyens ; elles soutiennent en outre que le règlement du 21 octobre 2009 n'a pas eu pour effet de faire disparaître l'obligation d'agrément pour les aires d'entreposage temporaire ;<br/>
<br/>
<br/>
               Après avoir convoqué à une audience publique, d'une part, la société SARIA Industries et autres et, d'autre part, le ministre de l'agriculture, de l'agroalimentaire et de la pêche ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 26 mars 2014 à 14 heures 30 au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Pinet, avocat au Conseil d'Etat et à la Cour de cassation, avocat des sociétés requérantes ;<br/>
<br/>
              - les représentants des sociétés requérantes ;<br/>
<br/>
              - les représentants du ministre de l'agriculture, de l'agroalimentaire et de la pêche ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clôturé l'instruction ;<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
	Vu le règlement (CE) n° 1069/2009 du Parlement européen et du Conseil du 21 octobre 2009 ;<br/>
	Vu le règlement (UE) n° 142/ 2011 de la Commission du 25 février 2011 ;<br/>
	Vu le code rural et de la pêche maritime ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; <br/>
<br/>
              2. Considérant que le point 4 de la note du 27 décembre 2013 dont les sociétés requérantes demandent la suspension prescrit aux directeurs départementaux de la protection des populations et aux directeurs départementaux de la cohésion sociale d'adopter une interprétation des dispositions en vigueur relatives à l'équarrissage selon laquelle l'entreposage de containers d'animaux morts, sans ouverture de ceux-ci ni manipulation de leur contenu, sur des plateformes logistiques non soumises aux contraintes de localisation ni aux obligations de comporter les aménagements et équipements particuliers prévus par cette règlementation pour les établissements, locaux et installations qui en relèvent, ne contrevient à aucune règle sanitaire prévue par les textes en vigueur ; que, eu égard à leur activité, les sociétés requérantes justifient d'un intérêt leur donnant qualité pour agir ;<br/>
<br/>
              Sur l'intervention de la société SIRAM : <br/>
<br/>
              3. Considérant que la société SIRAM a intérêt à la suspension de l'exécution de la note contestée ; qu'ainsi son intervention est recevable ;<br/>
<br/>
              Sur la condition tenant à l'existence d'un moyen propre à faire naître un doute sérieux sur la légalité de la note du 27 décembre 2013 : <br/>
<br/>
              4. Considérant qu'aux termes du 1. de l'article 24 du règlement (CE) n° 1069/2009 du Parlement européen et du Conseil du 21 octobre 2009 : " Les exploitants veillent à ce que les établissements ou usines sous leur contrôle soient agréés par l'autorité compétente lorsque ces établissements et usines effectuent une ou plusieurs des activités suivantes : (...) i) L'entreposage de sous-produits animaux ; j) l'entreposage de produits dérivés destinés 	à être éliminés ...à être utilisés comme combustibles ...comme aliments pour animaux ...comme engrais organiques (...) " ; qu'aux termes de l'article 19 du règlement d'application (UE) n° 142/ 2011 de la Commission du 25 février 2011 : " Les exploitants veillent à ce que les établissements et usines sous leur contrôle qui ont été agréés par l'autorité compétente satisfassent aux exigences énoncées aux chapitres de l'annexe IX du présent règlement mentionnés ci-après lorsqu'ils exercent une ou plusieurs des activités suivantes visées à l'article 24, paragraphe 1, du règlement (CE) n° 1069/2009: ...b) le chapitre II, lorsqu'ils entreposent des sous-produits animaux, comme visé à l'article 24, paragraphe 1, point i), dudit règlement ... " ; qu'aux termes de la section 1 du chapitre II de l'annexe IX au même règlement : " 1. Les locaux et installations servant à l'accomplissement d'opérations intermédiaires doivent satisfaire au moins aux exigences suivantes : a) ils doivent être convenablement séparés des voies de communication par lesquelles une contamination peut se propager et d'autres locaux tels que des abattoirs. L'aménagement des usines doit garantir la séparation totale entre les matières de catégorie 1 et de catégorie 2, d'une part, et les matières de catégorie 3, d'autre part, de la réception à l'expédition, sauf dans des bâtiments totalement séparés ; b) l'usine doit disposer d'une aire couverte pour la réception et l'expédition des sous-produits animaux, sauf si ceux-ci sont déchargés au moyen d'équipements qui empêchent la propagation de risques pour la santé publique et animale, tels des tubes fermés dans le cas de sous-produits animaux liquides; c) l'établissement doit être construit de manière à pouvoir être aisément nettoyé et désinfecté. Les sols doivent être conçus de manière à faciliter l'écoulement des liquides ; (...) " ; qu'il résulte de ces dispositions que le moyen tiré de ce que l'entreposage de caissons contenant des animaux morts, même assuré dans les conditions fixées au point 4 de la note litigieuse énoncées au considérant 2 ci-dessus, constitue une activité d'entreposage au sens des règlements communautaires du 21 octobre 2009 et du 25 février 2011, est de nature à créer, en l'état de l'instruction, un doute sérieux sur sa légalité ; que la note prescrivant aux services de l'Etat chargés de contrôler le respect de la réglementation en la matière d'adopter une interprétation paraissant, en l'état de l'instruction, méconnaître le sens et la portée des dispositions applicables qu'elle entendait expliciter, la fin de non recevoir opposée à la requête par le ministre, tirée de ce que la note contestée ne ferait pas grief, doit être écartée ;<br/>
<br/>
              Sur la condition d'urgence :<br/>
<br/>
              5. Considérant que l'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celle-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ; que l'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire ;<br/>
<br/>
              6. Considérant que, si les sociétés SARIA industries, SARVAL Sud-est et SIFFDA Bretagne n'établissent pas l'existence d'un préjudice résultant pour elles de l'exécution de  la note  litigieuse, il ressort de l'instruction et de l'audience que la même note permet l'existence et le fonctionnement de la plateforme d'entreposage d'animaux morts mise en place par le concurrent de la société SIFFDA Centre dans le département de la Dordogne qui a conduit à la perte par celle-ci du marché d'enlèvement des animaux morts dans ce département ; que cette perte risque de conduire à la fermeture du site de Chalagnac de la société SIFFDA Centre qui emploie 15 personnes ; que l'atteinte grave et immédiate que l'exécution de la décision contestée porte aux intérêts de la société requérante traduit une situation d'urgence au sens de l'article L. 521-1 du code de justice administrative ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que les conditions posées par l'article L. 521-1 du code de justice administrative, pour que le juge des référés puisse prononcer la suspension d'une décision contestée, sont remplies en l'espèce ; qu'il y a, dès lors, lieu de suspendre l'exécution du point 4 de la note du 27 décembre 2013 du directeur général de l'alimentation du ministère de l'agriculture destinée aux directeurs départementaux de la protection des populations et aux directeurs départementaux de la cohésion sociale ;<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              8. Considérant que la SIRAM, intervenante en demande, n'étant pas partie à la présente instance, les dispositions de l'article L.761-1 du code de justice administrative font obstacle à la condamnation de l'Etat à lui payer les sommes qu'elle demande au titre de ces dispositions ; qu'il y a lieu en revanche, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement de la somme de 4 000 euros à la société SARIA Industries, à la société ARVAL Sud-est, à la société SIFDDA Centre, à la société SIFDDA Bretagne au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1 : L'intervention de la société SIRAM est admise.<br/>
Article 2 : L'exécution du point 4 de la note du 27 décembre 2013 du ministre de l'agriculture, de l'agroalimentaire et de la pêche est suspendue. <br/>
Article 3 : L'Etat versera la somme de 4 000 euros à la société SARIA Industries, à la société ARVAL Sud-est, à la société SIFDDA Centre et à la société SIFDDA Bretagne au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Les conclusions de la société SIRAM tendant à l'application de l'article L. 761-1 sont rejetées.<br/>
Article 5 : La présente ordonnance sera notifiée à la société SARIA Industries, à la société ARVAL Sud-est, à la société SIFDDA Centre, à la société SIFDDA Bretagne, à la société SIRAM et au ministre de l'agriculture, de l'agroalimentaire et de la pêche. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
