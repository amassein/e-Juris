<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029835083</ID>
<ANCIEN_ID>JG_L_2014_12_000000356281</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/83/50/CETATEXT000029835083.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème sous-section jugeant seule, 03/12/2014, 356281, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-12-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356281</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Romain Godet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:356281.20141203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 30 janvier 2012 au secrétariat du contentieux du Conseil d'Etat, la SA Sadajup, représentée par Me B...A..., demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la décision nos 1147 T et 1179 T du 7 décembre 2011 par laquelle la Commission nationale d'aménagement commercial a rejeté son recours tendant à l'annulation de la décision du 18 août 2011 de la commission départementale d'aménagement commercial de Vaucluse autorisant la SAS Atac à créer un ensemble commercial " Simply Market" d'une surface de vente totale de 1 690 m² au Pontet, comprenant un supermarché " Simply Market " d'une surface de vente de 1 600 m² et deux magasins non-alimentaires de 45 m² chacun ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ; <br/>
              - le code de commerce, dans sa rédaction issue de la loi n° 2008-776 du 4 août 2008 ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Romain Godet, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 750-1 du code de commerce, dans sa rédaction issue de la loi du 4 août 2008 de modernisation de l'économie : " Les implantations, extensions, transferts d'activités existantes et changements de secteur d'activité d'entreprises commerciales et artisanales doivent répondre aux exigences d'aménagement du territoire, de protection de l'environnement et de qualité de l'urbanisme. Ils doivent en particulier contribuer au maintien des activités dans les zones rurales et de montagne ainsi qu'au rééquilibrage des agglomérations par le développement des activités en centre-ville et dans les zones de dynamisation urbaine. / Dans le cadre d'une concurrence loyale, ils doivent également contribuer à la modernisation des équipements commerciaux, à leur adaptation à l'évolution des modes de consommation et des techniques de commercialisation, au confort d'achat du consommateur et à l'amélioration des conditions de travail des salariés ". <br/>
<br/>
              2. Aux termes de l'article L. 752-6 du code de commerce, issu de la même loi du 4 août 2008 : " Lorsqu'elle statue sur l'autorisation d'exploitation commerciale visée à l'article L. 752-1, la commission départementale d'aménagement commercial se prononce sur les effets du projet en matière d'aménagement du territoire, de développement durable et de protection des consommateurs. Les critères d' évaluation sont : / 1° En matière d'aménagement du territoire : / a) L'effet sur l'animation de la vie urbaine, rurale et de montagne ; / b) L'effet du projet sur les flux de transport ; / c) Les effets découlant des procédures prévues aux articles L. 303-1 du code de la construction et de l'habitation et L. 123-11 du code de l'urbanisme ; / 2° En matière de développement durable : / a) La qualité environnementale du projet ; / b) Son insertion dans les réseaux de transports collectifs. " ; <br/>
<br/>
              3. Il résulte de ces dispositions combinées que l'autorisation d'aménagement commercial ne peut être refusée que si, eu égard à ses effets, le projet contesté compromet la réalisation des objectifs énoncés par la loi. Il appartient aux commissions d'aménagement commercial, lorsqu'elles statuent sur les dossiers de demande d'autorisation, d'apprécier la conformité du projet à ces objectifs, au vu des critères d'évaluation mentionnés à l'article L. 752-6 du code de commerce. <br/>
<br/>
              Sur les moyens tirés de l'appréciation de la commission nationale d'aménagement commercial : <br/>
<br/>
              En ce qui concerne la prise en compte de l'évolution démographique de la zone de chalandise : <br/>
<br/>
              4. Si la Commission nationale d'aménagement commercial fait référence à l'évolution de la population de la zone de chalandise, il ressort des termes de sa décision que celle-ci est fondée sur l'appréciation des critères fixés par le code de commerce et non sur un critère démographique.<br/>
<br/>
              En ce qui concerne le respect du critère d'animation de la vie urbaine prévu au 1° de l'article L. 752-6 du code de commerce : <br/>
<br/>
              5. Depuis l'entrée en vigueur de la loi du 4 août 2008, la densité d'équipement commercial de la zone de chalandise concernée ne figure plus au nombre des critères au vu desquels les commissions d'aménagement commercial statuent sur les dossiers de demande d'autorisation. <br/>
<br/>
              6. Il ressort des pièces du dossier, ainsi que l'a relevé la Commission nationale d'aménagement commercial, que le projet en litige s'inscrit dans le cadre de la requalification d'une ancienne friche industrielle et qu'il permet de renouveler l'offre commerciale de proximité de la zone de chalandise. Il ressort également des pièces du dossier que la construction de cet ensemble commercial accompagne la création de nouveaux logements dans cette zone. Par suite, la société requérante n'est pas fondée à soutenir que la commission aurait inexactement apprécié le critère de contribution à l'animation de la vie urbaine. <br/>
<br/>
              En ce qui concerne le respect des critères d'insertion dans les réseaux de transports collectifs et de qualité environnementale du projet prévus au 2° de l'article L. 752-6 du code de commerce : <br/>
<br/>
              7. En premier lieu, il ressort des pièces du dossier que deux lignes de bus desservent un arrêt situé à 150 mètres de l'ensemble commercial, que trois autres lignes desservent des arrêts situés entre 250 et 300 mètres de ce dernier et qu'une navette municipale a été mise en place pour accéder au site. Il ressort également des pièces du dossier que la réalisation de travaux a permis de faciliter l'accès des piétons et des cyclistes. La commission a, par suite, correctement apprécié l'insertion du projet en litige dans les réseaux de transports collectifs. <br/>
<br/>
              8. En second lieu, il ressort des pièces du dossier que la création d'une " maison du canal " permet la mise en valeur effective du cours d'eau situé à proximité de l'ensemble commercial en litige et qu'aucun bâtiment n'est situé à proximité directe de ce dernier ; ainsi, l'exigence d'un espace libre non constructible entre le nouvel ensemble commercial et les berges est respectée. Il ressort notamment du procès-verbal de la réunion de la Commission nationale d'aménagement commercial du 7 décembre 2011 que la végétalisation de la toiture ainsi que l'habillage bois de la structure métallique de l'entrée du supermarché permettent une meilleure insertion paysagère du projet et que des dispositifs de réduction des consommations énergétiques ont été mis en place. Par suite, la société requérante n'est pas fondée à soutenir que la commission aurait inexactement apprécié la qualité environnementale du projet. <br/>
<br/>
              Sur la compatibilité de l'autorisation accordée avec les dispositions du schéma de cohérence territoriale : <br/>
<br/>
              9. En vertu de l'article L. 122-1-15 du code de l'urbanisme, les autorisations délivrées par la commission nationale d'aménagement commercial doivent être compatibles avec les schémas de cohérence territoriale. Il résulte de ce qui a été rappelé au point 8 que le moyen tiré de ce que l'ensemble commercial en litige ne serait pas compatible avec la trame bleue prévue par le schéma de cohérence territoriale du bassin de vie d'Avignon n'est, en tout état de cause, pas fondé. <br/>
<br/>
              10. Il résulte de tout ce qui précède que la SA Sadajup n'est pas fondée à demander l'annulation de la décision qu'elle attaque. <br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              11. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la SAS Atac, qui n'est pas, dans la présente instance, la partie perdante. En revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la SA Sadajup la somme de 1500 euros à verser à la SAS Atac. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la SA Sadajup est rejetée.<br/>
Article 2 : La SA Sadajup versera à la SAS Atac la somme de 1500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la SA Sadajup, à la SAS Atac et à la Commission nationale d'aménagement commercial. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
