<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030200548</ID>
<ANCIEN_ID>JG_L_2015_01_000000366152</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/20/05/CETATEXT000030200548.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème SSJS, 29/01/2015, 366152, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-01-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366152</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>Mme Maryline Saleix</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:366152.20150129</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>		Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure :<br/>
<br/>
              La SNC Invest Hôtel Blanc Mesnil a demandé au tribunal administratif de Montreuil de prononcer la réduction des cotisations de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre des années 2006 à 2010 à raison d'un immeuble dont elle est propriétaire 2, rue Edouard Renault au Blanc-Mesnil.<br/>
<br/>
              Par un jugement n° 1201296 du 17 décembre 2012, le tribunal administratif de Montreuil a rejeté cette demande.<br/>
<br/>
Procédure devant le Conseil d'Etat :<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 février et 21 mai 2013 au secrétariat du contentieux du Conseil d'Etat, la SNC Invest Hôtel Blanc Mesnil demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du 17 décembre 2012 du tribunal administratif de Montreuil ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              elle soutient que le tribunal administratif a :<br/>
              - insuffisamment motivé son jugement en se bornant à dire, pour écarter le local-type n° 118 du procès-verbal du 19ème arrondissement de Paris qu'elle proposait à titre de terme de comparaison, qu'il ne résultait pas de l'instruction que le local-type situé dans le quartier du Père Lachaise dans le 20ème arrondissement de Paris, qui avait lui-même servi de référence pour évaluer le local proposé, avait été régulièrement évalué, sans préciser sur quels éléments il se fondait pour porter cette appréciation ;<br/>
              - insuffisamment motivé son jugement, dénaturé les pièces du dossier, méconnu son office et fait une inexacte application du b du 2° de l'article 1498 du code général des impôts en jugeant que le local-type n° 43 de la commune de Villejuif ne pouvait être retenu comme terme de comparaison en raison du caractère anormal du loyer, au motif qu'elle n'apportait pas la preuve que le bail le concernant ait été conclu à des conditions normales,  alors qu'il ne pouvait faire peser sur elle la charge d'une preuve qui incombe à l'administration et qu'il n'a pas recherché si ces conditions avaient conduit à la fixation d'un loyer anormal au regard des prix pratiqués pour des locaux comparables ;<br/>
              - insuffisamment motivé son jugement, dénaturé les pièces du dossier et commis une erreur de droit en écartant les locaux-types n° 90 du procès-verbal de la commune d'Issy-les-Moulineaux, n° 57 du procès-verbal de la commune de Boulogne-Billancourt, n° 34 du procès-verbal de la commune de Vincennes et n° 210 du procès-verbal du quartier Petit Montrouge dans le 14ème arrondissement de Paris au motif inopérant de leur conception vétuste ;<br/>
                       				    - méconnu l'article 1498 du code général des impôts en jugeant que la commune du Blanc-Mesnil et le quartier de la Chaussée d'Antin dans le 9ème arrondissement de Paris ne présentaient pas, du point de vue économique, une situation analogue au sens de ces dispositions.    <br/>
<br/>
<br/>
              Vu :<br/>
<br/>
              - les autres pièces du dossier ;<br/>
<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Maryline Saleix, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, Trichet, avocat de la SNC Invest Hotel Blanc Mesnil.<br/>
<br/>
<br/>
<br/>
<br/>
              1.  Considérant qu'aux termes de l'article 1498 du code général des impôts : "La valeur locative de tous les biens autres que les locaux visés au I de l'article 1496 et que les établissements industriels visés à l'article 1499 est déterminée au moyen de l'une des méthodes indiquées ci-après : 1° Pour les biens donnés en location à des conditions de prix normales, la valeur locative est celle qui ressort de cette location ;  2° a. Pour les biens loués à des conditions de prix anormales ou occupés par leur propriétaire, occupés par un tiers à un autre titre que la location, vacants ou concédés à titre gratuit, la valeur locative est déterminée par comparaison. / Les termes de comparaison sont choisis dans la commune. Ils peuvent être choisis hors de la commune pour procéder à l'évaluation des immeubles d'un caractère particulier ou exceptionnel ; / b. La valeur locative des termes de comparaison est arrêtée : / Soit en partant du bail en cours à la date de référence de la révision lorsque l'immeuble type était loué normalement à cette date, / Soit, dans le cas contraire, par comparaison avec des immeubles similaires situés dans la commune ou dans une localité présentant, du point de vue économique, une situation analogue à celle de la commune en cause et qui faisaient l'objet à cette date de locations consenties à des conditions de prix normales ; 3° A défaut de ces bases, la valeur locative est déterminée par voie d'appréciation directe " ;<br/>
<br/>
              2. Considérant, en premier lieu, que pour écarter le local-type n° 118 du procès-verbal du 19ème arrondissement de Paris que la société Paris Nord Invest Hôtel proposait à titre de terme de comparaison pour l'évaluation de la valeur locative de l'immeuble à usage d'hôtel dont elle est propriétaire au Blanc-Mesnil, le tribunal administratif de Montreuil a relevé que ce local était de conception ancienne et n'avait pas fait l'objet d'un classement touristique ; que s'il a également jugé qu'il ne résultait pas de l'instruction que le local-type situé dans le quartier du Père Lachaise dans le 20ème arrondissement de Paris, qui avait lui-même servi de référence pour évaluer le local-type proposé, avait été régulièrement évalué, ces motifs présentent un caractère surabondant ; que, par suite, le moyen tiré de l'insuffisante motivation du jugement doit être écarté ; <br/>
<br/>
              3. Considérant, en deuxième lieu, que pour écarter le local-type n° 43 du procès-verbal des opérations d'évaluation foncière de la commune de Villejuif, proposé par la société, en raison du caractère anormal des conditions du bail en cours au 1er janvier 1970, le tribunal, après avoir relevé que ce bail avait été conclu entre une société, propriétaire des locaux, et son gérant et qu'il mettait à la charge du preneur toutes les dépenses de grosses réparations mentionnées à l'article 606 du code civil, a jugé qu'il n'était pas démontré par les éléments produits par la société qu'il avait été conclu à des conditions de prix normales ; qu'en statuant ainsi, de manière suffisamment motivée, le tribunal, qui n'a pas dénaturé les faits qui lui étaient soumis, n'a ni fait une inexacte application de l'article 1498 du code général des impôts ni méconnu son office ; <br/>
<br/>
              4. Considérant, en troisième lieu, que pour écarter les locaux-types n° 90 du procès-verbal de la commune d'Issy-les-Moulineaux, n° 57 du procès-verbal de la commune de Boulogne-Billancourt, n° 34 du procès-verbal de la commune de Vincennes et n° 210 du procès-verbal du quartier Petit Montrouge dans le 14ème arrondissement de Paris, le tribunal a relevé qu'ils correspondaient à des immeubles de conception classique et ancienne qui avaient été édifiés respectivement en 1926, 1925, 1915 et 1926 et qui n'étaient pas comparables au 1er janvier des années d'imposition avec des hôtels modernes de chaîne comme celui de la société requérante édifié en 1993, alors même qu'ils avaient fait l'objet d'aménagements ayant permis d'améliorer le niveau de leurs équipements et prestations depuis le 1er janvier 1970 ; qu'en statuant ainsi, de manière suffisamment motivée et sans se référer, contrairement à ce que soutient la société, à un motif inopérant tiré de la conception vétuste de ces locaux, il n'a ni dénaturé les faits ni commis d'erreur de droit ;<br/>
<br/>
              5. Considérant, enfin, que pour écarter le local-type constitué de l'hôtel Intercontinental inscrit au procès-verbal des maisons exceptionnelles du 9ème arrondissement de Paris proposé par la société, le tribunal a estimé, sans dénaturer les faits qui lui étaient soumis, que la commune du Blanc-Mesnil et le quartier de la Chaussée d'Antin dans le 9ème arrondissement de Paris ne présentaient pas, du point de vue économique, une situation analogue, dès lors que, compte tenu de leurs caractéristiques propres, ils  n'attiraient pas une clientèle équivalente et ne comportaient pas de zones de chalandise comparables ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la société Paris Nord Invest Hôtel n'est pas fondée à demander l'annulation du jugement qu'elle attaque ; qu'il y a lieu, par voie de conséquence, de rejeter ses conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>                 D E C I D E :<br/>
                                --------------<br/>
<br/>
Article 1er : Le pourvoi de la société Invest Hôtel Blanc Mesnil est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la SNC Invest Hôtel Blanc Mesnil et au ministre des finances et des comptes publics. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
