<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029288219</ID>
<ANCIEN_ID>JG_L_2014_07_000000359900</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/28/82/CETATEXT000029288219.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 23/07/2014, 359900, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359900</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:359900.20140723</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              La société France Aero Garage, venant aux droits de la société France Auto Garage, a demandé au tribunal administratif de Paris de la décharger de la cotisation d'impôt sur les sociétés, de la contribution additionnelle à cet impôt ainsi que des pénalités correspondantes auxquelles elle a été assujettie au titre de l'exercice clos le 31 décembre 2004. Par un jugement n° 0713082/2 du 21 mai 2010, le tribunal administratif de Paris a fait droit à cette demande.<br/>
<br/>
              Par un arrêt n° 10PA04858 du 30 mars 2012, la cour administrative d'appel de Paris a rejeté l'appel formé par le ministre de l'économie et des finances contre l'article 1er de ce jugement.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi, un nouveau mémoire et un mémoire en réplique, enregistrés les 1er juin 2012, 31 mai 2013 et 14 février 2014, au secrétariat du contentieux du Conseil d'Etat, le ministre de l'économie et des finances demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10PA04858 du 30 mars 2012 de la cour administrative d'appel de Paris ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la société Z Immobilier ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que l'administration a remis en cause, sur le fondement des dispositions de l'article L. 64 du livre des procédures fiscales relatives à la répression des abus de droit, l'application du régime fiscal des sociétés-mères à la perception, par la société France Auto Garage, de dividendes versés par la société Sovilla JC. Le ministre de l'économie et des finances se pourvoit en cassation contre l'arrêt du 30 mars 2012 par lequel la cour administrative d'appel de Paris a rejeté son appel dirigé contre le jugement du tribunal administratif de Paris du 21 mai 2010 qui a fait droit à la demande de la société France Aero Garage, venant aux droits de la société France Auto Garage, de la décharger des impositions supplémentaires mises à sa charge au titre de l'exercice clos le 31 décembre 2004.<br/>
<br/>
              2. D'une part, aux termes de l'article 145 du code général des impôts, dans sa rédaction applicable aux impositions litigieuses : " 1. Le régime fiscal des sociétés mères, tel qu'il est défini aux articles 146 et 216, est applicable aux sociétés et autres organismes soumis à l'impôt sur les sociétés au taux normal qui détiennent des participations satisfaisant aux conditions ci-après : a. les titres de participation doivent revêtir la forme nominative ou être déposés dans un établissement désigné par l'administration ; b. les titres de participation doivent représenter au moins 5 % du capital de la société émettrice (...) ; c. les titres de participation doivent avoir été souscrits à l'émission. A défaut, la personne morale participante doit avoir pris l'engagement de les conserver pendant un délai de deux ans. (...). ". Aux termes de l'article 216 du même code : "  I. Les produits nets des participations, ouvrant droit à l'application du régime fiscal des sociétés mères et visées à l'article 145, touchés au cours d'un exercice par une société mère, peuvent être retranchés du bénéfice net total de celle-ci, défalcation faite d'une quote-part de frais et charges (...) ". Aux termes du I de l'article 219 du code général des impôts, dans sa rédaction alors applicable : " (...) a ter. Le régime des plus-values et moins-values à long terme cesse de s'appliquer au résultat de la cession de titres du portefeuille réalisée au cours d'un exercice ouvert à compter du 1er janvier 1994 à l'exclusion des parts ou actions de sociétés revêtant le caractère de titres de participation (...). Pour les exercices ouverts à compter de la même date, le régime des plus ou moins-values à long terme cesse également de s'appliquer en ce qui concerne les titres de sociétés dont l'actif est constitué principalement par des titres exclus de ce régime ou dont l'activité consiste de manière prépondérante en la gestion des mêmes valeurs pour leur propre compte. Pour l'application des premier et deuxième alinéas, constituent des titres de participation les parts ou actions de sociétés revêtant ce caractère sur le plan comptable. Il en va de même (...) des titres ouvrant droit au régime fiscal des sociétés mères, (...) si ces (...) titres sont inscrits en comptabilité au compte de titres de participation ou à une subdivision spéciale d'un autre compte du bilan correspondant à leur qualification comptable (...) ".<br/>
<br/>
              3. D'autre part, aux termes de l'article L. 64 du livre des procédures fiscales, dans sa rédaction alors applicable : " Ne peuvent être opposés à l'administration des impôts les actes qui dissimulent la portée véritable d'un contrat ou d'une convention à l'aide de clauses : (...) b) (...) qui déguisent soit une réalisation, soit un transfert de bénéfices ou de revenus ; (...) L'administration est en droit de restituer son véritable caractère à l'opération litigieuse. En cas de désaccord sur les redressements notifiés sur le fondement du présent article, le litige est soumis, à la demande du contribuable, à l'avis du comité consultatif pour la répression des abus de droit. L'administration peut également soumettre le litige à l'avis du comité dont les avis rendus feront l'objet d'un rapport annuel. Si l'administration ne s'est pas conformée à l'avis du comité, elle doit apporter la preuve du bien-fondé du redressement. ". Il résulte de ces dispositions que, lorsque l'administration use de la faculté qu'elles lui confèrent dans des conditions telles que la charge de la preuve lui incombe, elle est fondée à écarter comme ne lui étant pas opposables certains actes passés par le contribuable, dès lors que ces actes ont un caractère fictif, ou que, recherchant le bénéfice d'une application littérale des textes à l'encontre des objectifs poursuivis par leurs auteurs, ils n'ont pu être inspirés par aucun autre motif que celui d'éluder ou d'atténuer les charges fiscales que l'intéressé, s'il n'avait pas passé ces actes, aurait normalement supportées, eu égard à sa situation ou à ses activités réelles.<br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que la société France Auto Garage a acquis, le 7 décembre 2004, des titres composant le capital de la société Sovilla JC. La société France Auto Garage s'est engagée à conserver ces titres pendant une durée de deux ans et les a inscrits en comptabilité à son actif en tant que valeurs mobilières de placement. Le 29 décembre 2004, elle a perçu des dividendes de cette société à hauteur de 211 950 euros, en bénéficiant du régime des sociétés-mères prévu aux articles 145 et 216 précités du code général des impôts. A la clôture du même exercice, la société France Auto Garage a constitué une provision pour dépréciation des titres de la même société, d'un montant de 173 326 euros. Elle a ainsi dégagé, à la clôture de l'exercice 2004, un déficit fiscal reportable de 49 314 euros.<br/>
<br/>
              5. Il résulte de l'ensemble des travaux préparatoires du régime fiscal des sociétés-mères, en particulier des travaux préparatoires de l'article 27 de la loi du 31 juillet 1920 portant fixation du budget général de l'exercice 1920, de l'article 53 de la loi du 31 décembre 1936 portant réforme fiscale, de l'article 45 de la loi du 14 avril 1952 portant loi de finances pour 1952, des articles 20 et 21 de la loi du 12 juillet 1965 modifiant l'imposition des entreprises et des revenus de capitaux mobiliers et de l'article 9 de la loi de finances pour 2001, ainsi que de la circonstance que le bénéfice de ce régime fiscal a toujours été subordonné à une condition de détention des titres depuis l'origine ou de durée minimale de détention, et, depuis 1936, à une condition de seuil de participation minimale dans le capital des sociétés émettrices, que le législateur, en cherchant à supprimer ou à limiter la succession d'impositions susceptibles de frapper les produits que les sociétés-mères perçoivent de leurs participations dans des sociétés-filles et ceux qu'elles redistribuent à leurs propres actionnaires, a eu comme objectif de favoriser l'implication de sociétés-mères dans le développement économique de sociétés-filles pour les besoins de la structuration et du renforcement de l'économie française. Le fait d'acquérir une société ayant cessé son activité initiale et liquidé ses actifs dans le but d'en récupérer les liquidités par le versement de dividendes exonérés d'impôt sur les sociétés en application du régime de faveur des sociétés-mères, sans prendre aucune mesure de nature à lui permettre de reprendre et développer son ancienne activité ou d'en trouver une nouvelle, va à l'encontre de cet objectif.<br/>
<br/>
              6. Pour juger que l'opération décrite ci-dessus ne constituait pas un abus de droit au sens des dispositions de l'article L. 64 précité du livre des procédures fiscales, la cour administrative d'appel de Paris a relevé, en premier lieu, que la société France Auto Garage, ainsi que la société Sovilla JC, existaient avant l'opération de distribution des dividendes et que l'avantage fiscal litigieux n'avait pas été obtenu par l'interposition d'une société spécialement créée à cet effet, puis, en second lieu, que l'opération en cause n'avait pas méconnu les objectifs de l'article 216 du code général des impôts, qui a institué le régime fiscal des sociétés-mères dans le but d'éliminer la double imposition des dividendes. En statuant ainsi, la cour a méconnu les objectifs de ce régime et commis une erreur de droit. En en déduisant que les opérations litigieuses n'étaient pas constitutives d'un abus de droit, elle a entaché son arrêt d'une erreur de qualification juridique des faits. Par suite, le ministre est fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              7. Dans les circonstances de l'espèce, il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au fond.<br/>
<br/>
              8. Il résulte de l'instruction que la société Sovilla JC, dont la société France Auto Garage a acquis les titres en décembre 2004, était dépourvue de moyens matériels et humains et que ses actifs étaient constitués uniquement de liquidités. La distribution de dividendes à laquelle cette société a procédé au profit de la société France Auto Garage dans les jours qui ont suivi son acquisition a eu pour effet de priver la société Sovilla JC des moyens qui auraient pu lui permettre de retrouver une activité. Si la société France Auto Garage remplissait les conditions légales pour bénéficier du régime fiscal des sociétés-mères prévu par les articles 145 et 216 du code général des impôts alors applicables, si elle a pris l'engagement de conserver les titres pendant deux ans et si l'opération litigieuse n'a pas été rendue possible par l'interposition d'une société spécialement créée à cette fin, il résulte des circonstances rappelées ci-dessus que la société France Auto Garage n'a pris aucune mesure de nature à favoriser le développement de la société dont elle venait d'acquérir les titres. Les opérations litigieuses ont en revanche, grâce à la déduction immédiate d'une provision correspondant à la dépréciation des titres et à l'exonération d'impôt dont ont bénéficié, à l'exception d'une quote-part, les dividendes reçus de la filiale en application du régime fiscal des sociétés-mères, permis à la société France Auto Garage de dégager un important déficit fiscal imputable sur son bénéfice imposable et reportable sur les exercices suivants. Il suit de là que le ministre est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Paris a jugé que l'administration n'apportait pas la preuve, qui lui incombait, de ce que l'opération litigieuse avait été inspirée par un but exclusivement fiscal et avait méconnu les objectifs poursuivis par le législateur quand il a institué le régime fiscal des sociétés-mères, et de ce qu'elle constituait ainsi un abus de droit.<br/>
<br/>
              9. Il appartient au Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner les autres moyens soulevés devant le tribunal administratif par la société France Aero Garage, venant aux droits de la société France Auto Garage.<br/>
<br/>
              10. L'intérêt de retard institué par l'article 1727 du code général des impôts vise essentiellement à réparer les préjudices de toute nature subis par l'Etat à raison du non-respect par les contribuables de leurs obligations de déclarer et de payer l'impôt aux dates légales. Si l'évolution des taux du marché a conduit à une hausse relative de cet intérêt depuis son institution, cette circonstance ne lui confère pas pour autant la nature d'une sanction, dès lors que son niveau n'est pas devenu manifestement excessif au regard du taux moyen pratiqué par les prêteurs privés pour un découvert non négocié. Il suit de là que la société requérante ne peut utilement soutenir que le taux annuel de l'intérêt de retard devait être limité au taux annuel de l'intérêt légal.<br/>
<br/>
              11. Aux termes de l'article 1729 du code général des impôts dans sa rédaction alors applicable : " 1. Lorsque la déclaration ou l'acte mentionnés à l'article 1728 font apparaître une base d'imposition ou des éléments servant à la liquidation de l'impôt insuffisants, inexacts ou incomplets, le montant des droits mis à la charge du contribuable est assorti de l'intérêt de retard visé à l'article 1727 et d'une majoration de 40 % si la mauvaise foi de l'intéressé est établie ou de 80 % s'il s'est rendu coupable de manoeuvres frauduleuses ou d'abus de droit au sens de l'article L. 64 du livre des procédures fiscales ". Eu égard aux objectifs de prévention et de répression de la fraude et de l'évasion fiscales auxquels répondent les pénalités fiscales, le principe de personnalité des peines ne fait pas obstacle à ce que, à l'occasion d'une opération de fusion ou de scission, ces sanctions pécuniaires soient mises, compte tenu de la transmission universelle du patrimoine, à la charge de la société absorbante, d'une nouvelle société créée pour réaliser la fusion ou de sociétés issues de la scission, à raison des manquements commis, avant cette opération, par la société absorbée ou fusionnée ou par la société scindée. La société requérante n'est, dès lors, pas fondée à soutenir que la pénalité de 80 % qui lui a été infligée en application de l'article 1729 précité ne pouvait l'être, faute qu'elle ait commis les faits reprochés.<br/>
<br/>
              12. Il résulte de tout ce qui précède que le ministre de l'économie et des finances est fondé à soutenir que c'est à tort que le tribunal administratif de Paris a déchargé la société France Aero Garage, venant aux droits de la société France Auto Garage, de la cotisation d'impôt sur les sociétés, de la contribution additionnelle à cet impôt ainsi que des pénalités correspondantes auxquelles elle a été assujettie au titre de l'exercice clos le 31 décembre 2004.<br/>
<br/>
              13. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 30 mars 2012 de la cour administrative d'appel de Paris et l'article 1er du jugement du 21 mai 2010 du tribunal administratif de Paris sont annulés.<br/>
Article 2 : La cotisation d'impôt sur les sociétés, la contribution additionnelle à cet impôt ainsi que les pénalités correspondantes auxquelles la société France Aero Garage, venant aux droits de la société France Auto Garage, a été assujettie au titre de l'exercice clos le 31 décembre 2004 sont remises à la charge de la société Z Immobilier, venant aux droits de ces sociétés.<br/>
Article 3 : La demande présentée par la société France Aero Garage, venant aux droits de la société France Auto Garage, devant le tribunal administratif de Paris, et les conclusions présentées devant le Conseil d'Etat et la cour administrative d'appel de Paris par la société Z Immobilier, venant aux droits de ces sociétés, au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre des finances et des comptes publics et à la société Z Immobilier.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
