<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028583858</ID>
<ANCIEN_ID>JG_L_2014_02_000000352254</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/58/38/CETATEXT000028583858.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 10/02/2014, 352254, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-02-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352254</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:352254.20140210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 29 août et 29 novembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. et Mme A..., demeurant... ; M. et Mme A... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0806626/4 du 16 juin 2011 par lequel le tribunal administratif de Melun a rejeté leur demande tendant à la condamnation de l'Etat à leur verser une somme de 8 489,35 euros, augmentée des intérêts à compter du 1er février 2008, en réparation des pertes locatives résultant du refus du préfet du Val-de-Marne de leur apporter le concours de la force publique pour l'exécution d'une décision de justice ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à cette demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la construction et de l'habitation ; <br/>
<br/>
              Vu la loi n° 91-650 du 9 juillet 1991 ; <br/>
<br/>
              Vu le décret n° 92-755 du 31 juillet 1992 ;  <br/>
<br/>
              Vu la décision du 22 février 2012 par laquelle le Conseil d'Etat statuant au contentieux n'a pas renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. et MmeA... ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat des consorts A...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article 62 de la loi du 9 juillet 1991 portant réforme des procédures civiles d'exécution, applicable au litige soumis aux juges du fond et dont les dispositions ont été reprises aux articles L. 412-1 et L. 412-5 du code des procédures civiles d'exécution : " Si l'expulsion porte sur un local affecté à l'habitation principale de la personne expulsée ou de tout occupant de son chef, elle ne peut avoir lieu, sans préjudice des dispositions des articles L. 613-1 à L. 613-5 du code de la construction et de l'habitation, qu'à l'expiration d'un délai de deux mois qui suit le commandement. (...) Dès le commandement d'avoir à libérer les locaux à peine de suspension du délai avant l'expiration duquel l'expulsion ne peut avoir lieu, l'huissier de justice chargé de l'exécution de la mesure d'expulsion doit en informer le représentant de l'Etat dans le département en vue de la prise en compte de la demande de relogement de l'occupant dans le cadre du plan départemental (...) " ; qu'aux termes du premier alinéa de l'article 197 du décret du 31 juillet 1992 également applicable au litige et dont les dispositions ont été reprises à l'article R. 412-2 du code des procédures civiles d'exécution : " L'huissier de justice envoie au préfet du département du lieu de situation de l'immeuble, par lettre recommandée avec demande d'accusé de réception, copie du commandement d'avoir à quitter les locaux " ; <br/>
<br/>
              2. Considérant que, pour rejeter les conclusions présentées devant lui par M. et MmeA..., tendant à ce que l'Etat soit condamné à réparer les conséquences dommageables du rejet par le préfet de la Seine-et-Marne de leur demande de concours de la force publique, le tribunal administratif de Melun a estimé que cette demande ne pouvait être regardée comme ayant été régulièrement présentée dès lors qu'en dépit d'une mesure d'instruction diligentée par le tribunal, les intéressés n'avaient pas versé au dossier le justificatif de la notification au préfet, préalablement à la réquisition de la force publique, du commandement de quitter les lieux délivré aux occupants de leur bien ; qu'en fondant ainsi sa décision sur l'absence de production d'un justificatif de la notification de cette pièce, alors que les intéressés avaient produit une lettre informant le préfet de la délivrance de cette notification et que le préfet qui, en dépit d'une mise en demeure, n'avait pas produit de défense devant le tribunal et devait ainsi être regardé comme ayant acquiescé aux faits, le tribunal administratif a commis une erreur de droit ; <br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. et Mme A... sont fondés à demander l'annulation du jugement du tribunal administratif de Melun du 16 juin 2011 ; qu'il y a lieu de mettre à la charge de l'Etat une somme de 3000 euros à leur verser au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Melun du 16 juin 2011 est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée devant le tribunal administratif de Melun.<br/>
<br/>
Article 3 : L'Etat versera à M. et Mme A...la somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. et Mme A...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
