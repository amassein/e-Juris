<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038355033</ID>
<ANCIEN_ID>JG_L_2019_04_000000422426</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/35/50/CETATEXT000038355033.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 05/04/2019, 422426, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-04-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422426</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LE BRET-DESACHE</AVOCATS>
<RAPPORTEUR>Mme Stéphanie Vera</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:422426.20190405</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 20 juillet et 28 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, M. A...B...demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret du 13 avril 2018 accordant son extradition aux autorités du Royaume du Maroc.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la convention d'extradition entre la République française et le Royaume du Maroc signée à Rabat le 18 avril 2008 ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Stéphanie Vera, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Le Bret-Desache, avocat de M. B...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	M. A...B...conteste le décret du 13 avril 2018 par lequel le Premier ministre a accordé son extradition aux autorités marocaines pour l'exécution d'un mandat d'arrêt décerné le 31 janvier 2017 par le procureur du Roi près le tribunal de première instance de Tetouan (Maroc) pour des faits qualifiés de détention de drogues, complicité au transport de drogues, complicité à l'exportation de stupéfiants, trafic de drogues, détention de drogues au ressort des douanes et complicité à 1'exportation de stupéfiants sans autorisation ni déclaration.<br/>
<br/>
              2.	En premier lieu, il ressort des mentions de l'ampliation du décret attaqué figurant au dossier et certifiée conforme par le secrétaire général du Gouvernement que le décret a été signé par le Premier ministre et contresigné par la garde des sceaux, ministre de la justice. L'ampliation notifiée à M. B...n'avait pas, pour sa part, à être revêtue de ces signatures.<br/>
<br/>
              3.	En deuxième lieu, le décret attaqué comporte l'énoncé des considérations de fait et de droit qui en constituent le fondement et satisfait ainsi à l'exigence de motivation posée à l'article L. 211-5 du code des relations entre le public et l'administration.<br/>
<br/>
              4.	En troisième lieu, si M. B...soutient qu'en cas d'exécution du décret qu'il attaque, il risque d'être exposé à des traitements inhumains ou dégradants, en raison des conditions d'incarcération au Maroc, il n'apporte, au soutien de ce moyen, aucun élément permettant d'établir l'existence des risques qu'il courrait à titre personnel. Par suite, les moyens tirés de la méconnaissance de l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, de l'erreur de droit et de l'erreur manifeste d'appréciation dont serait entaché le décret attaqué au regard des conséquences d'une exceptionnelle gravité auxquelles son exécution pourrait l'exposer doivent être écartés.<br/>
<br/>
              5.	En quatrième lieu, si une décision d'extradition est susceptible de porter atteinte, au sens de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, au droit au respect de la vie privée et familiale, cette mesure trouve, en principe, sa justification dans la nature même de la procédure d'extradition, qui est de permettre, dans l'intérêt de l'ordre public et sous les conditions fixées par les dispositions qui la régissent, tant le jugement de personnes se trouvant en France qui sont poursuivies à l'étranger pour des crimes ou des délits commis hors de France que l'exécution, par les mêmes personnes, des condamnations pénales prononcées contre elles à l'étranger pour de tels crimes ou délits. En l'espèce, la circonstance que l'intéressé réside en France depuis 2003 et que sa famille y est établie n'est pas de nature à faire obstacle, dans l'intérêt de l'ordre public, à l'exécution de son extradition. Dès lors, le moyen tiré de la méconnaissance des stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ne peut qu'être écarté.<br/>
<br/>
              6.	Il résulte de ce qui précède que M. B...n'est pas fondé à demander l'annulation du décret qu'il attaque. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à  M. A...B...et à la garde des sceaux, ministre de la justice.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
