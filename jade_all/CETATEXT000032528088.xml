<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032528088</ID>
<ANCIEN_ID>JG_L_2016_05_000000390351</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/52/80/CETATEXT000032528088.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 11/05/2016, 390351</TITRE>
<DATE_DEC>2016-05-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390351</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Tristan Aureau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:390351.20160511</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. F...E..., Mme B...C...et Mme A...D...ont demandé à la Cour nationale du droit d'asile d'annuler les décisions en date du 31 décembre 2013 et du 4 avril 2014 par lesquelles le directeur général de l'Office français de protection des réfugiés et apatrides (OFPRA) a refusé de les admettre au bénéfice de l'asile.<br/>
<br/>
              Par une décision nos 14014931, 14014933 et 14015186 du 17 février 2015, la Cour nationale du droit d'asile a rejeté leurs recours. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 22 mai et 10 août 2015 au secrétariat du contentieux du Conseil d'Etat, M. E...et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) de mettre à la charge de l'OFPRA la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés ; <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Tristan Aureau, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M. E...et autres ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que le premier alinéa de l'article L. 733-5 du code de l'entrée et du séjour des étrangers et du droit d'asile dispose que : " Saisie d'un recours contre une décision du directeur général de l'Office français de protection des réfugiés et apatrides, la Cour nationale du droit d'asile statue, en qualité de juge de plein contentieux, sur le droit du requérant à une protection au titre de l'asile au vu des circonstances de fait dont elle a connaissance au moment où elle se prononce " ; qu'aux termes de l'article R. 733-13 du même code : " Le président de la formation de jugement ou, avant enrôlement du dossier, le président de la cour peut fixer la date de clôture de l'instruction écrite par une ordonnance notifiée aux parties quinze jours au moins avant cette date. L'ordonnance n'est pas motivée et ne peut faire l'objet d'aucun recours. L'instruction écrite peut être rouverte dans les mêmes formes. / Dans le cas où les parties sont informées de la date de l'audience deux mois au moins avant celle-ci, l'instruction écrite est close dix jours francs avant la date de l'audience. Cette information, qui indique la date de clôture de l'instruction, est valablement faite à l'avocat constitué à la date de son envoi ou, le cas échéant, à l'avocat désigné au titre de l'aide juridictionnelle à cette même date. Elle ne vaut pas avis d'audience au sens de l'article R. 733-19. / S'il n'a pas été fait application du premier ou du deuxième alinéa, l'instruction écrite est close cinq jours francs avant la date de l'audience. / Lorsque l'instruction écrite est close, seule la production des originaux des documents communiqués préalablement en copie demeure recevable jusqu'à la fin de l'audience " ; qu'aux termes de l'article R. 733-13-1 du même code : " Pour les affaires relevant de sa compétence en application du deuxième alinéa de l'article L. 731-2, le président de la cour ou le président désigné peut, dès l'enregistrement du recours, par une décision qui tient lieu d'avis d'audience, fixer la date à laquelle l'affaire sera appelée à l'audience. Dans ce cas, l'instruction écrite est close trois jours avant la date de l'audience. / La décision prévue à l'alinéa précédent est adressée aux parties par tout moyen quinze jours au moins avant le jour où l'affaire sera appelée à l'audience. Elle informe les parties de la clôture de l'instruction prévue par cet alinéa " ; qu'aux termes des dispositions de l'article R. 733-16 du même code : " La formation de jugement ne peut se fonder sur des éléments d'information extérieurs au dossier relatifs à des circonstances de fait propres au demandeur d'asile ou spécifiques à son récit, sans en avoir préalablement informé les parties. / Les parties sont préalablement informées lorsque la formation de jugement est susceptible de fonder sa décision sur un moyen soulevé d'office (...) / Un délai est fixé aux parties pour déposer leurs observations, sans qu'y fasse obstacle la clôture de l'instruction écrite " ;<br/>
<br/>
              2.	Considérant que pour rejeter les demandes de M.E..., Mme C... et MmeD..., la Cour nationale du droit d'asile a tout d'abord relevé qu'ils étaient en droit de se prévaloir, outre leur nationalité russe, de la nationalité arménienne, compte tenu des dispositions de la loi sur la citoyenneté de la République d'Arménie du 16 novembre 1995, modifiée le 26 février 2007 ; que la Cour a, ensuite, relevé qu'ils n'alléguaient aucune crainte crédible de persécution de la part des autorités actuelles de la République d'Arménie ni n'établissaient que celles-ci n'étaient pas en mesure de leur offrir une protection ; que la Cour en a déduit qu'ils devaient être regardés comme s'étant privés sans raison valable de la protection des autorités de la République d'Arménie ; qu'il ressort, toutefois, des pièces du dossier soumis aux juges du fond que l'administration n'avait jamais contesté que M.E..., Mme C...et Mme D...étaient uniquement en droit de se prévaloir de la nationalité russe dont ils s'étaient prévalus à l'appui de leur demande d'asile et qu'elle n'avait à aucun moment examiné leur situation au regard des lois arméniennes ; qu'ainsi, la Cour nationale du droit d'asile a soulevé d'office ce moyen, sans en avoir informé les parties préalablement à la tenue de l'audience, en méconnaissance des dispositions précitées de l'article R. 733-16 du code de l'entrée et du séjour des étrangers et du droit d'asile ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la décision attaquée doit être annulée ; <br/>
<br/>
              3.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'OFPRA la somme de 3 000 euros à verser à M. E...et autres au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                 -------------- <br/>
<br/>
Article 1er : La décision du 17 février 2015 de la Cour nationale du droit d'asile est annulée.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la Cour nationale du droit d'asile. <br/>
<br/>
Article 3 : L'OFPRA versera une somme de 3 000 euros à M. E...et autres au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. F...E..., Mme B...C...et Mme A...D...et à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-08-02-03-02 - MOYEN D'ORDRE PUBLIC NON SOUMIS AU DÉBAT CONTRADICTOIRE AVANT L'AUDIENCE PAR LA CNDA - IRRÉGULARITÉ DE LA PROCÉDURE.
</SCT>
<ANA ID="9A"> 095-08-02-03-02 La Cour nationale du droit d'asile (CNDA) a soulevé d'office le moyen tiré de ce que l'intéressé pouvait se prévaloir de la protection d'un autre pays dont il a également la nationalité, sans en informer les parties préalablement à la tenue de l'audience. Méconnaissance de l'article R. 733-16 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA).</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
