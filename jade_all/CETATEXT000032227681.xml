<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032227681</ID>
<ANCIEN_ID>JG_L_2016_03_000000389361</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/22/76/CETATEXT000032227681.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème SSR, 14/03/2016, 389361</TITRE>
<DATE_DEC>2016-03-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389361</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Grégory Rzepski</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:389361.20160314</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés au secrétariat du contentieux du Conseil d'Etat le 10 avril 2015, le 29 octobre 2015 et le 23 février 2016, M. B...A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le décret du Président de la République du 21 janvier 2015 le plaçant en retrait d'emploi par mise en non-activité pour une durée de neuf mois ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la  défense ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Grégory Rzepski, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'en vertu de l'article L. 4137-2 du code de la défense : " Les sanctions disciplinaires applicables aux militaires sont réparties en trois groupes : / 1° Les sanctions du premier groupe sont : / a) L'avertissement ; / b) La consigne ; / c) La réprimande ; / d) Le blâme ; / e) Les arrêts ; / f) Le blâme du ministre ; / 2° Les sanctions du deuxième groupe sont : / a) L'exclusion temporaire de fonctions pour une durée maximale de cinq jours privative de toute rémunération ; / b) L'abaissement temporaire d'échelon ; / c) La radiation du tableau d'avancement ; / 3° Les sanctions du troisième groupe sont : / a) Le retrait d'emploi, défini par les dispositions de l'article L. 4138-15 ; / b) La radiation des cadres ou la résiliation du contrat " ; qu'aux termes de l'article L. 4138-15 du même code : " Le retrait d'emploi par mise en non-activité est prononcé pour une durée qui ne peut excéder douze mois. A l'expiration de la période de non-activité, le militaire en situation de retrait d'emploi est replacé en position d'activité. / Le temps passé dans la position de non-activité par retrait d'emploi ne compte ni pour l'avancement ni pour l'ouverture et la liquidation des droits à pension de retraite. Dans cette position, le militaire cesse de figurer sur la liste d'ancienneté ; il a droit aux deux cinquièmes de sa solde augmentée de l'indemnité de résidence et du supplément familial de solde " ;<br/>
<br/>
              2. Considérant qu'il appartient au juge de l'excès de pouvoir, saisi de moyens en ce sens, de rechercher si les faits reprochés à un agent public ayant fait l'objet d'une sanction disciplinaire constituent des fautes de nature à justifier une sanction et si la sanction retenue est proportionnée à la gravité de ces fautes ;<br/>
<br/>
              3. Considérant que, par le décret attaqué du 21 janvier 2015, le Président de la République a infligé à M.A..., capitaine dans l'armée de terre, une sanction de retrait d'emploi par mise en non-activité pour une durée de neuf mois ; que cette décision, prise après que le conseil d'enquête réuni le 9 juillet 2014 eut proposé une sanction d'exclusion temporaire de fonctions pour une durée de cinq jours, est fondée sur les motifs que, à l'occasion d'une mission de reconnaissance effectuée dans la région de Gao au Mali, dans le cadre de l'opération " SERVAL ", M.A..., alors lieutenant, dont la section avait découvert des roquettes appartenant à l'ennemi a, dans un premier temps, proposé à son commandant d'unité de coordonner lui-même l'opération de destruction de ces munitions alors qu'il ne disposait pas des qualifications requises pour diriger une telle intervention puis a, dans un second temps, par une accumulation de fautes, compromis la sécurité des hommes qu'il commandait, dont l'un a été grièvement blessé lors de la destruction des roquettes par explosifs ; <br/>
<br/>
              4. Considérant, en ce qui concerne le premier de ces motifs, qu'il ressort des pièces du dossier que la décision de confier au lieutenant A...la direction de cette intervention a été prise par le poste de commandement de Gao, après que la proposition formulée par l'intéressé lui eut été soumise par le commandant de l'unité dont faisait partie la section du lieutenantA..., et que cette décision a fait l'objet d'un ordre transmis au lieutenant par son commandant d'unité ; que cet ordre ne correspondait pas à la décision de faire intervenir une équipe spécialisée, prise, à un niveau hiérarchique plus élevé, par le poste de commandement interarmées de théâtre (PCIAT) de Bamako, auquel le poste de commandement de Gao avait signalé la découverte des munitions ; que cette décision du PCIAT a été portée à la connaissance du poste de commandement de Gao, qui ne l'a pas communiquée à l'unité dont faisait partie la section du lieutenant ; qu'il résulte de ce qui précède qu'il ne peut être fait reproche à M. A... d'avoir décidé de son seul chef de réaliser une intervention pour laquelle il ne disposait pas de toutes les qualifications requises ; <br/>
<br/>
              5. Considérant, en ce qui concerne les motifs de la décision relatifs aux manquements dans l'exécution de l'intervention, qu'il ressort des pièces du dossier que le lieutenant A...a commis plusieurs erreurs ou maladresses dans la mise en place des moyens de destruction des roquettes, n'a pas procédé à une ultime vérification du dispositif avant la mise à feu et n'a pas fait respecter l'obligation, pour les militaires concernés, de porter un casque, en méconnaissance des instructions du ministère de la défense relatives aux explosifs ; que, si un tel comportement est fautif, il ressort toutefois des motifs mêmes du décret attaqué que l'environnement de l'intervention présentait des contraintes particulières ; que, par ailleurs, l'accident survenu au cours de la phase de destruction des explosifs a trouvé sa cause immédiate dans une incompréhension entre les différents participants à l'opération, qui a conduit à ce que la mise à feu électrique soit déclenchée avant la mise à feu pyrotechnique, à un moment où le caporal chargé de cette dernière n'avait pas eu le temps de se mettre à l'abri ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que si les erreurs commises par le lieutenant A...étaient de nature à justifier une sanction disciplinaire, la décision de retrait d'emploi par mise en non-activité pour une durée de neuf mois prise à l'encontre de l'intéressé, qui n'était par ailleurs officier que depuis le 1er août 2011 et dont les très bons états de service ne sont pas contestés, doit être regardée, dans les circonstances de l'espèce, comme n'étant pas proportionnée à la gravité de sa faute ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens de la requête, M. A...est fondé à demander l'annulation du décret du 21 janvier 2015 ;<br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 4 000 euros à verser à M. A...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le décret du 21 janvier 2015 est annulé.<br/>
Article 2 : L'Etat versera à M. A...la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à M. B...A..., au Premier ministre et au ministre de la défense.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">08-01-01-05 ARMÉES ET DÉFENSE. PERSONNELS MILITAIRES ET CIVILS DE LA DÉFENSE. QUESTIONS COMMUNES À L'ENSEMBLE DES PERSONNELS MILITAIRES. DISCIPLINE. - CONTRÔLE DE LA PROPORTIONNALITÉ DE LA SANCTION DISCIPLINAIRE D'UN MILITAIRE À LA GRAVITÉ DES FAITS [RJ1] - SANCTION DISPROPORTIONNÉE EN L'ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-02-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE NORMAL. - CONTRÔLE DE LA PROPORTIONNALITÉ DE LA SANCTION DISCIPLINAIRE D'UN MILITAIRE À LA GRAVITÉ DES FAITS [RJ1] - SANCTION DISPROPORTIONNÉE EN L'ESPÈCE.
</SCT>
<ANA ID="9A"> 08-01-01-05 Sanction de retrait d'emploi par mise en non-activité pendant neuf mois d'un lieutenant de l'armée de terre pour des fautes commises dans la direction d'une opération de destruction de munitions, au motif que le lieutenant a commis plusieurs erreurs ou maladresses dans la mise en place des moyens de destruction des munitions, n'a pas procédé à une ultime vérification du dispositif avant la mise à feu et n'a pas fait respecter l'obligation, pour les militaires concernés, de porter un casque, en méconnaissance des instructions du ministère de la défense relatives aux explosifs. Si un tel comportement est fautif, l'environnement de l'intervention présentait des contraintes particulières et l'accident survenu au cours de la phase de destruction des explosifs a trouvé sa cause immédiate dans une incompréhension entre les différents participants à l'opération, qui a conduit à ce que la mise à feu électrique soit déclenchée avant la mise à feu pyrotechnique, à un moment où le caporal chargé de cette dernière n'avait pas eu le temps de se mettre à l'abri. Dès lors, si les erreurs commises par le lieutenant étaient de nature à justifier une sanction disciplinaire, la décision de retrait d'emploi par mise en non-activité pour une durée de neuf mois prise à l'encontre de l'intéressé, qui n'était par ailleurs officier que depuis deux ans à la date de l'accident et dont les très bons états de service ne sont pas contestés, doit être regardée, dans les circonstances de l'espèce, comme n'étant pas proportionnée à la gravité de sa faute.</ANA>
<ANA ID="9B"> 54-07-02-03 Sanction de retrait d'emploi par mise en non-activité pendant neuf mois d'un lieutenant de l'armée de terre pour des fautes commises dans la direction d'une opération de destruction de munitions, au motif que le lieutenant a commis plusieurs erreurs ou maladresses dans la mise en place des moyens de destruction des munitions, n'a pas procédé à une ultime vérification du dispositif avant la mise à feu et n'a pas fait respecter l'obligation, pour les militaires concernés, de porter un casque, en méconnaissance des instructions du ministère de la défense relatives aux explosifs. Si un tel comportement est fautif, l'environnement de l'intervention présentait des contraintes particulières et l'accident survenu au cours de la phase de destruction des explosifs a trouvé sa cause immédiate dans une incompréhension entre les différents participants à l'opération, qui a conduit à ce que la mise à feu électrique soit déclenchée avant la mise à feu pyrotechnique, à un moment où le caporal chargé de cette dernière n'avait pas eu le temps de se mettre à l'abri. Dès lors, si les erreurs commises par le lieutenant étaient de nature à justifier une sanction disciplinaire, la décision de retrait d'emploi par mise en non-activité pour une durée de neuf mois prise à l'encontre de l'intéressé, qui n'était par ailleurs officier que depuis deux ans à la date de l'accident et dont les très bons états de service ne sont pas contestés, doit être regardée, dans les circonstances de l'espèce, comme n'étant pas proportionnée à la gravité de sa faute.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. sol. contr. CE, Assemblée, 13 novembre 2013, M. Dahan, n° 347704, p. 279 ; CE, 25 janvier 2016, M. Parent, n° 391178, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
