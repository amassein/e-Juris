<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028036192</ID>
<ANCIEN_ID>JG_L_2013_10_000000355299</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/03/61/CETATEXT000028036192.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 04/10/2013, 355299</TITRE>
<DATE_DEC>2013-10-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>355299</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Fabrice Benkimoun</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:355299.20131004</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 28 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présenté pour le syndicat des producteurs de carottes de Créances, dont le siège est 30, place de la Liberté à Créances (50710) ; le syndicat  demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté des ministres chargés de l'agriculture et de l'économie du 28 septembre 2011 portant extension des règles édictées par l'association d'organisations de producteurs Jardins de Normandie aux producteurs de choux-fleurs, de poireaux et de carottes de Basse-Normandie, ainsi que l'arrêté du ministre de l'agriculture, du 31 octobre 2011 fixant les conditions de perception des cotisations au bénéfice de l'association d'organisations de producteurs Jardins de Normandie du fait de l'extension des règles pour les carottes, et l'arrêté du même ministre du 14 novembre 2011 portant abrogation de l'arrêté du 31 octobre 2011 fixant les conditions de perception des cotisations au bénéfice de l'association d'organisations de producteurs Jardins de Normandie du fait de l'extension des règles pour les poireaux et portant fixation de nouvelles conditions de perception de ces cotisations ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 20 septembre 2013, présentée par le ministre de l'agriculture, de l'agroalimentaire et de la forêt ;<br/>
<br/>
              Vu le règlement (CE) n° 1182/2007 du Conseil du 26 septembre 2007 ;<br/>
<br/>
              Vu le règlement (CE) n° 1234/2007 du Conseil du 22 octobre 2007 ;<br/>
<br/>
              Vu le code rural et de la pêche maritime ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabrice Benkimoun, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Sur la légalité externe de l'arrêté du 28 septembre 2011 <br/>
<br/>
              1. Considérant, d'une part, que M. D...A...a reçu délégation de signature, par arrêté du 11 juillet 2011, à l'effet de signer au nom du ministre chargé de l'économie, des finances et de l'industrie tous actes, arrêtés ou décisions dans la limite des attributions de la sous-direction des produits alimentaires et des marchés agricoles et alimentaires ; que, d'autre part, M. B...C..., ingénieur en chef des ponts, des eaux et des forêts, a reçu délégation de signature, par décision du 3 décembre 2010 modifiée par décision du 31 août 2011, à l'effet de signer au nom du ministre chargé de l'agriculture, à l'exclusion des décrets, tous actes, arrêtés ou décisions dans la limite des attributions de la sous-direction de l'organisation économique, des industries agroalimentaires et de l'emploi ; que, par suite, ces fonctionnaires étaient respectivement compétents pour signer, au nom des ministres chargés de l'économie et de l'agriculture, l'arrêté du 28 septembre 2011 ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 125 decies du règlement (CE) 1234/2007 du Conseil du 22 octobre 2007 portant organisation commune des marchés dans le secteur agricole et dispositions spécifiques en ce qui concerne certains produits de ce secteur : " l'État membre concerné peut décider, sur présentation des pièces justificatives, que les producteurs non membres sont redevables à l'organisation de producteurs de la partie des contributions financières versées par les producteurs membres, dans la mesure où elles sont destinées à couvrir : a) les frais administratifs résultant de l'application du régime visé à l'article 125 septies, paragraphe 1 ; b) les frais résultant des actions de recherche, d'étude de marché et de promotion des ventes entreprises par l'organisation ou l'association et bénéficiant à l'ensemble des producteurs de la circonscription. " ; qu'aux termes de l'article L. 551-7 du code rural et de la pêche maritime : " Dans le secteur des fruits et légumes, les organisations de producteurs et associations d'organisations de producteurs reconnues en application des articles L. 551-1 et L. 551-2 peuvent être autorisées, dans les conditions prévues par l'article 125 decies du règlement (CE) 1234/2007 du Conseil, précisées par décret en Conseil d'État, à percevoir des contributions financières de producteurs non membres, assises soit sur la valeur des produits, soit sur les superficies, soit sur ces deux éléments combinés. / Ces cotisations sont rendues obligatoires par arrêté du ministre chargé de l'agriculture pour une durée qui ne peut excéder une campagne de commercialisation. " ; <br/>
<br/>
              3. Considérant que, s'il résulte de l'article L. 551-7 du code rural et de la pêche maritime que le ministre chargé de l'agriculture est compétent pour rendre obligatoires les cotisations destinées à couvrir les frais visés par l'article 125 decies du règlement du 22 octobre 2007, ce ministre ne tire en revanche ni de ces dispositions ni de la loi ou d'un décret, la compétence pour déterminer conjointement avec le ministre chargé de l'économie, l'association d'organisations de producteurs habilitée à prélever ces cotisations obligatoires et, d'autre part, l'autorité compétente pour en arrêter, chaque année, le montant ; que, par suite, l'article 2 de l'arrêté attaqué, divisible de ses autres dispositions, doit être annulé pour avoir été pris par une autorité incompétente ;<br/>
<br/>
              Sur la légalité interne de l'arrêté du 28 septembre 2011 : <br/>
<br/>
              4. Considérant, en premier lieu, que si l'acte par lequel, conformément aux articles L. 551-1 et L. 551-2 du code rural et de la pêche maritime, l'autorité administrative compétente reconnaît comme association d'organisations de producteurs une association constituée notamment par des organisations de producteurs a un caractère réglementaire, les actes par lesquels sont étendues, en vertu des dispositions de l'article L. 551-6 du même code, les règles adoptées au sein d'une telle association n'en constituent pas une mesure d'application ; que, par suite, le requérant ne peut utilement exciper ni de l'illégalité dont serait entachée, selon lui, l'arrêté du 7 janvier 2010 portant reconnaissance de l'association Jardins de Normandie en qualité d'association d'organisations de producteurs dans le secteur des fruits et légumes ni se prévaloir de l'objet social figurant dans les statuts de l'association, pour contester la légalité des règles adoptées par celle-ci et dont l'arrêté attaqué a pour objet l'extension ;<br/>
<br/>
              5. Considérant, en deuxième lieu, que l'article 125 undecies du règlement du 22 octobre 2007 a notamment pour effet de rendre applicable aux associations d'organisations de producteurs les circonscriptions économiques définies au deuxième paragraphe de l'article 125 septies du même règlement ; qu'aux termes de l'article D. 551-35 du code rural et de la pêche maritime : "  Conformément aux dispositions des articles 125 septies et 125 undecies du règlement (CE) no 1234/2007 du 22 octobre 2007 susmentionné, et dans les conditions prévues par ces articles, toute (...) association d'organisations de producteurs reconnue dans le secteur des fruits et légumes peut demander au ministre chargé de l'agriculture l'extension à l'ensemble des producteurs établis dans sa circonscription économique des règles relatives à la connaissance de la production, à la production, à la commercialisation ou à la protection de l'environnement.  (...) / La circonscription économique visée à l'article 125 septies du règlement (CE) n° 1234/2007 du Conseil est définie comme un département ou une somme de départements, ou encore l'ensemble du territoire national " ; qu'il ressort des pièces du dossier que la Commission européenne a procédé à la publication de la liste des circonscriptions économiques déterminée par la France en vue de l'extension des règles des organisations de producteurs du secteur des fruits et légumes ; qu'il en résulte que la circonscription économique sur laquelle opère l'association d'organisations de producteurs Jardins de Normandie est définie comme constituée des départements du Calvados, de la Manche et de l'Orne et que la délimitation de cette circonscription économique a été effectuée conformément aux prescriptions communautaires ; que, dès lors, le moyen tiré du défaut de définition légale de la circonscription économique dont il est fait application doit être écarté ;<br/>
<br/>
              6. Considérant, en troisième lieu, que les conditions dans lesquelles l'arrêté attaqué a été publié au Journal officiel du 28 octobre 2011, avec l'indication que certaines des règles étendues font l'objet d'une définition dans une annexe dont les modalités de consultation sont indiquées, sont sans incidence sur sa légalité ; <br/>
<br/>
              7. Considérant, en quatrième lieu, qu'il ressort des pièces du dossier, d'une part, que la Commission européenne a procédé à la publication sur son site internet de l'arrêté portant extension des règles édictées par l'association d'organisations de producteurs Jardins de Normandie et, d'autre part, que ces mêmes règles avaient déjà fait l'objet d'une application aux producteurs membres de l'association d'organisations de producteurs Jardins de Normandie lors de la campagne 2009/2010 et d'une application à l'ensemble des producteurs de la circonscription considérée lors de la campagne 2010/2011 en vertu de l'arrêté du 6 août 2010 portant extension des règles édictées par l'association d'organisations de producteurs Jardins de Normandie ; que, par suite, les moyens tirés du non respect des dispositions des articles 125 terdecies 3b et quaterdecies du règlement du 22 octobre 2007, qui doivent être compris comme invoquant les dispositions des articles 125 septies et 125 octies prévoyant les mêmes obligations pour les associations d'organisations de producteurs, doivent être écartés ; <br/>
<br/>
              8. Considérant enfin que, compte tenu des conditions particulières dans lesquelles s'exercent les activités agricoles, les dispositions réglementaires fixant le régime applicable à un produit agricole pour une campagne déterminée doivent nécessairement produire effet pour l'ensemble de la campagne considérée ; que, par suite, la circonstance que l'arrêté attaqué a été pris le 28 septembre 2011 ne faisait pas obstacle à ce que son article 1er mentionne qu'il s'applique à la campagne de commercialisation 2011/2012 ;<br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que le syndicat requérant est seulement fondé à demander l'annulation de l'article 2 de l'arrêté du 28 septembre 2011 ;<br/>
<br/>
              Sur la légalité des arrêtés du 31 octobre 2011 et du 14 novembre 2011 :<br/>
<br/>
              10. Considérant qu'il résulte de l'illégalité de l'article 2 de l'arrêté du 28 septembre 2011 que l'arrêté du 31 octobre 2011 et l'article 2 de l'arrêté du 14 novembre 2011 par lesquels le ministre chargé de l'agriculture a fixé les conditions de perception des cotisations au bénéfice de l'association d'organisations de producteurs Jardins de Normandie du fait de l'extension des règles fixées, respectivement, pour les carottes et pour les poireaux, ont été pris par une autorité incompétente ; que, dès lors, et sans qu'il soit besoin d'examiner les autres moyens de la requête, le syndicat requérant est fondé à en demander l'annulation ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              11. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 2.000 euros à verser au syndicat des producteurs de carottes de Créances au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article  1er : L'article 2 de l'arrêté du 28 septembre 2011 portant extension des règles édictées par l'association d'organisations de producteurs Jardins de Normandie aux producteurs de choux-fleurs, de poireaux et de carottes de Basse-Normandie, est annulé. <br/>
Article  2 : L'arrêté du 31 octobre 2011 fixant les conditions de perception des cotisations au bénéfice de l'association d'organisations de producteurs Jardins de Normandie du fait de l'extension des règles pour les carottes et l'article 2 de l'arrêté du 14 novembre 2011 portant abrogation de l'arrêté du 31 octobre 2011 fixant les conditions de perception des cotisations au bénéfice de l'association d'organisations de producteurs Jardins de Normandie du fait de l'extension des règles pour les poireaux et portant fixation de nouvelles conditions de perception de ces cotisations sont annulés.<br/>
Article 3 : L'Etat versera au syndicat des producteurs de carottes de Créances une somme de 2.000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Le surplus des conclusions du syndicat des producteurs de carottes de Créances est rejeté.<br/>
Article 5 : La présente décision sera notifiée au syndicat des producteurs de carottes de Créances, au ministre de l'agriculture, de l'agroalimentaire et de la forêt et au ministre de l'économie et des finances. <br/>
Copie en sera adressée à l'association des organisations de producteurs Jardins de Normandie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">03-05-01-01-02 AGRICULTURE ET FORÊTS. PRODUITS AGRICOLES. GÉNÉRALITÉS. ORGANISATION DES MARCHÉS. - CONTRIBUTIONS FINANCIÈRES DESTINÉES À COUVRIR LES FRAIS ADMINISTRATIFS ET LES FRAIS RÉSULTANT D'ACTIONS DE RECHERCHE, D'ÉTUDE DE MARCHÉ ET DE PROMOTION DES VENTES BÉNÉFICIANT À L'ENSEMBLE DES PRODUCTEURS DE LA CIRCONSCRIPTION (ART. 125 SEPTIES DU RÈGLEMENT DU 22 OCTOBRE 2007) - COMPÉTENCE DU MINISTRE CHARGÉ DE L'AGRICULTURE POUR RENDRE CES COTISATIONS OBLIGATOIRES POUR LES PRODUCTEURS NON MEMBRES - EXISTENCE (ART. L. 551-7 DU CRPM) - COMPÉTENCE DE CE MINISTRE POUR DÉTERMINER, CONJOINTEMENT AVEC LE MINISTRE CHARGÉ DE L'ÉCONOMIE, L'ASSOCIATION D'ORGANISATIONS DE PRODUCTEURS HABILITÉE À PRÉLEVER CES COTISATIONS ET L'AUTORITÉ COMPÉTENTE POUR EN ARRÊTER LE MONTANT - ABSENCE.
</SCT>
<ANA ID="9A"> 03-05-01-01-02 S'il résulte de l'article L. 551-7 du code rural et de la pêche maritime (CRPM) que le ministre chargé de l'agriculture est compétent pour rendre obligatoires les cotisations destinées à couvrir les frais visés par l'article 125 decies du règlement (CE) 1234/2007 du Conseil du 22 octobre 2007 portant organisation commune des marchés dans le secteur agricole et dispositions spécifiques en ce qui concerne certains produits de ce secteur, ce ministre ne tire en revanche ni de ces dispositions, ni de la loi ou d'un décret, la compétence pour déterminer conjointement avec le ministre chargé de l'économie, d'une part, l'association d'organisations de producteurs habilitée à prélever ces cotisations obligatoires et, d'autre part, l'autorité compétente pour en arrêter, chaque année, le montant.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
