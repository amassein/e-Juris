<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038438691</ID>
<ANCIEN_ID>JG_L_2019_04_000000419286</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/43/86/CETATEXT000038438691.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 30/04/2019, 419286, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-04-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419286</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:419286.20190430</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 22 octobre 2018, le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi de la société en nom collectif Cannes Estérel dirigées contre l'arrêt de la cour administrative d'appel de Marseille n° 15MA03990 du 24 janvier 2018 en tant seulement qu'il se prononce sur l'indemnisation du préjudice subi par cette société au titre des frais de remise en état du bâtiment B dont la construction avait été autorisée au 55, avenue Maurice Chevalier à Cannes par un permis de construire du 13 juin 1989. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de la SNC Cannes Esterel, et à la SCP Lyon-Caen, Thiriez, avocat de la commune de Cannes ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 13 juin 1989, le maire de Cannes a accordé à M. B...le permis de construire trois immeubles à usage commercial, industriel, artisanal et de bureaux, représentant au total une surface hors oeuvre nette de 18 907 mètres carrés, sur un terrain situé 55, avenue Maurice Chevalier. Par un arrêté du 9 janvier 1991, le maire a transféré ce permis à la SNC Cannes Estérel. Le maire de Cannes, en raison du défaut de conformité d'un des immeubles en cours de réalisation, a toutefois ordonné la suspension des travaux le 9 juin 1992. La SNC Cannes Estérel a alors déposé une demande de permis de construire modificatif, qui a été rejetée par le maire de Cannes le 28 juillet 1994. Ce refus a été annulé par une décision du Conseil d'Etat, statuant au contentieux, du 28 juillet 2000.  Le 9 septembre 2002, le maire de la commune a délivré à la société requérante le permis modificatif qu'elle sollicitait et levé le 9 décembre 2002 l'arrêté d'interruption des travaux. La SNC Cannes Estérel a demandé au tribunal administratif de Nice l'indemnisation des conséquences dommageables ayant résulté du refus de délivrance du permis modificatif, qui l'a l'empêchée d'achever les travaux du bâtiment B et de commencer la construction des bâtiments A et C. Par un jugement du 16 juillet 2015, le tribunal administratif de Nice a condamné la commune de Cannes à lui verser la somme de 8 217 675 euros, assortie des intérêts au taux légal et de leur capitalisation, et a rejeté le surplus de sa demande. Par un arrêt du 24 janvier 2018, la cour administrative d'appel de Marseille a, sur l'appel de la société Cannes Estérel et l'appel incident de la commune de Cannes, ramené à 6 144 467,89 euros la somme, portant intérêts à compter du 5 février 2002, à verser par la commune de Cannes à la société Cannes Estérel. Par une décision du 22 octobre 2018, le Conseil d'Etat statuant au contentieux a prononcé l'admission des conclusions du pourvoi de la société Cannes Estérel dirigées contre cet arrêt en tant seulement qu'il s'est prononcé sur l'indemnisation du préjudice lié aux frais de remise en état du bâtiment B. Par la voie du pourvoi incident, la commune de Cannes demande l'annulation de l'arrêt en tant qu'il met à sa charge l'indemnisation du préjudice résultant des loyers non perçus sur le bâtiment B, du fait de l'impossibilité d'en achever les travaux.  <br/>
<br/>
              Sur le pourvoi incident de la commune de Cannes : <br/>
<br/>
              2. La cour administrative d'appel de Marseille a relevé, d'une part, que le bâtiment B était déjà construit à 88 % lorsque le maire de Cannes a pris, le 9 juin 1992, un arrêté interruptif de travaux et, d'autre part, que l'affectation de ce bâtiment à la location était établie par la signature dès le 30 juillet 1990 d'un mandat non exclusif de recherche de locataires avec la société Auguste Thouard ainsi que par les conditions particulières de l'acte de crédit conclu le 11 janvier 1991 avec la société Banque et investissement. Elle a ensuite jugé que la SNC Cannes Estérel pouvait être indemnisée du préjudice résultant de la perte de loyers escomptés, en tenant compte à la fois du délai nécessaire à la recherche de locataires et d'un taux de remplissage qu'elle a apprécié en se fondant sur les éléments relevés par l'expertise ordonnée par le juge des référés du tribunal administratif de Nice. La commune de Cannes n'est ainsi pas fondée à soutenir que la cour aurait insuffisamment motivé son arrêt ni commis une erreur de droit en la condamnant à indemniser la SNC Cannes Estérel d'un préjudice qui ne serait qu'éventuel. Son pourvoi incident doit donc être rejeté.<br/>
<br/>
              Sur les conclusions du pourvoi principal restant en litige : <br/>
<br/>
              3. Contrairement à ce que soutient la commune de Cannes, la circonstance que la SNC Cannes Estérel aurait pu, sur le fondement de l'article R. 833-1 du code de justice administrative, introduire devant la cour administrative d'appel de Marseille un recours en rectification ne la rend pas irrecevable à soulever, à l'appui de son pourvoi, un moyen tiré de l'inexactitude matérielle qui entacherait l'arrêt qu'elle attaque.   <br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond, et notamment de l'annexe 12 du rapport d'expertise ordonnée par le juge des référés, que les frais de remise en état du bâtiment B avaient été estimés par l'expert à 1 322 967, 76 euros hors taxes. Dès lors, en retenant que l'expert avait évalué le coût de la remise en état du bâtiment à seulement 1 322 967,76 francs hors taxes, la cour administrative d'appel de Marseille s'est fondée sur des faits matériellement inexacts. Son arrêt doit, dès lors, être annulé en tant qu'il évalue le préjudice subi par la SNC Cannes Estérel au titre des frais de remise en état du bâtiment B à la somme de 100 842,57 euros et fixe en conséquence, par son article 1er, le montant total de la somme que la commune de Cannes est condamnée à verser à la SNC Cannes Estérel. <br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              6. Il résulte de l'instruction que le rapport d'expertise établi par M. A...en 2007 a évalué à 1 322 967,76 euros le coût des travaux de remise en état du bâtiment B, comprenant les travaux de reprise liés au vandalisme et aux dégradations et les frais concomitants. Eu égard au partage de responsabilité par moitié entre la commune et la société requérante que la cour a définitivement retenu, il y a lieu de mettre à la charge de la commune une somme de 661 483,88 euros en réparation du préjudice lié aux frais de remise en état du bâtiment B, sans qu'il soit besoin de demander une nouvelle expertise.<br/>
<br/>
              Sur les frais liés à l'instance :<br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la SNC Cannes Estérel, qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions de cette société présentées au même titre.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 24 janvier 2018 est annulé en tant qu'il évalue le préjudice subi par la SNC Cannes Estérel au titre des frais de remise en état du bâtiment B et fixe en conséquence, par son article 1er, le montant total, hors intérêts, de la somme que la commune de Cannes est condamnée à verser à la SNC Cannes Estérel.<br/>
Article 2 : La somme que la commune de Cannes est condamnée à verser à la SNC Cannes Estérel en réparation des préjudices subis par cette société du fait des conséquences dommageables de l'arrêté du 28 juillet 1994 est ramenée, hors intérêts, de 8 217 675 euros (huit millions deux cent dix-sept mille six cent soixante-quinze euros) à 6 705 109,20 euros (six millions sept cent cinq mille cent neuf euros et vingt centimes).<br/>
Article 3 : Le surplus des conclusions de la SNC Cannes Estérel est rejeté. <br/>
Article 4 : Le pourvoi incident de la commune de Cannes est rejeté, ainsi que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
Article 5 : La présente décision sera notifiée à la SNC Cannes Estérel et à la commune de Cannes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
