<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028842866</ID>
<ANCIEN_ID>JG_L_2014_04_000000361369</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/84/28/CETATEXT000028842866.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème SSJS, 11/04/2014, 361369, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-04-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361369</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Christophe Eoche-Duval</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:361369.20140411</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 26 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présentée pour la société Bricorama France, dont le siège est rue du Moulin Paillasson à Roanne (42300), représentée par son président directeur général en exercice ; la société Bricorama France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 1296 T du 18 avril 2012 par laquelle la Commission nationale d'aménagement commercial a accordé à la société Leroy Merlin France et à la société l'Immobilière Leroy Merlin France l'autorisation préalable requise en vue de créer un magasin de bricolage, jardinage, décoration et matériaux à l'enseigne Leroy Merlin de 14 000 m² de surface de vente, à Valence (Drôme) ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat, de la société Leroy Merlin France et de la société l'Immobilière Leroy Merlin France la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu la loi n° 73-1193 du 27 décembre 1973 ;<br/>
<br/>
              Vu la loi n° 2008-776 du 4 août 2008 ;<br/>
<br/>
              Vu le décret n° 2008-1212 du 24 novembre 2008 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Eoche-Duval, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Sur la légalité de la décision attaquée :<br/>
<br/>
              En ce qui concerne la procédure devant la commission nationale :<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier que le commissaire du gouvernement a recueilli les avis des ministres en charge de l'urbanisme, de l'environnement et du commerce ; que ces avis ont été signés par des personnes dûment habilitées à le faire ; que dès lors le moyen tiré de l'irrégularité de l'avis des ministres intéressés manque en fait et doit être écarté ;<br/>
<br/>
              2. Considérant qu'il ne résulte d'aucune disposition législative ou réglementaire ni d'aucun principe que la commission nationale soit tenue de communiquer aux auteurs d'un recours contestant une autorisation accordée à une société pétitionnaire les conclusions et le rapport d'instruction du commissaire du gouvernement, ainsi que les avis des ministres intéressés ; <br/>
<br/>
              En ce qui concerne la motivation de la décision attaquée :<br/>
<br/>
              3. Considérant que si, eu égard à la nature, à la composition et aux attributions de la Commission nationale d'aménagement commercial, les décisions qu'elle prend doivent être motivées, cette obligation n'implique pas que la commission soit tenue de prendre explicitement parti sur le respect, par le projet qui lui est soumis, de chacun des objectifs et critères d'appréciation fixés par le législateur ; qu'en l'espèce, la décision de la commission nationale, qui mentionne les éléments de droit et de fait qui en sont le support, est suffisamment motivée ; <br/>
<br/>
              En ce qui concerne l'appréciation de la commission nationale :<br/>
<br/>
              4. Considérant qu'il appartient aux commissions d'aménagement commercial, lorsqu'elles se prononcent sur un projet d'exploitation commerciale soumis à autorisation en application de l'article L. 752-1 du code de commerce, d'apprécier la conformité de ce projet aux objectifs prévus à l'article 1er de la loi du 27 décembre 1973 et à l'article L. 750-1 du code de commerce, au vu des critères d'évaluation mentionnés à l'article L. 752-6 du même code ; que l'autorisation ne peut être refusée que si, eu égard à ses effets, le projet compromet la réalisation de ces objectifs ;<br/>
<br/>
              5. Considérant que si le requérant soutient que la décision attaquée méconnaît l'objectif fixé par le législateur en matière d'aménagement du territoire, il ressort des pièces du dossier, que le projet autorisé, qui complète l'offre commerciale dans la zone de chalandise, se situe à proximité d'équipements publics et sur des terrains qui ont vocation à accueillir des activités de nature commerciale, et qu'il participera à l'animation de la vie urbaine de la commune de Valence ; que les flux de véhicules supplémentaires provoqués par son ouverture ne sont pas excessifs au regard des capacités des infrastructures routières desservant le site d'implantation ; que le moyen tiré de ce que l'autorisation contestée violerait les règles relatives à la protection du consommateur n'est pas assorti de précisions permettant d'en apprécier le bien-fondé ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              6. Considérant que ces dispositions font obstacle à ce qu'il soit fait droit aux conclusions présentées par la société Bricorama France au titre des frais exposés par elle et non compris dans les dépens ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la société Leroy Merlin France et à la société l'Immobilière Leroy Merlin France au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
              				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Les conclusions de la requête présentée par la société Bricorama France sont rejetées.<br/>
Article 2 : Les conclusions de la société Leroy Merlin France et de la société l'Immobilière Leroy Merlin France présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la société Bricorama France, à la société Leroy Merlin France, à la société l'Immobilière Leroy Merlin France et à la Commission nationale d'aménagement commercial.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
