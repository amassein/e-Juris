<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044945869</ID>
<ANCIEN_ID>JG_L_2021_12_000000459754</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/94/58/CETATEXT000044945869.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 24/12/2021, 459754, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>459754</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:459754.20211224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 22 décembre 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution des dispositions du décret n° 2021-1521 du 25 novembre 2021 modifiant le décret n° 2021-699 du 1er juin 2021 prescrivant les mesures générales nécessaires à la gestion de la sortie de crise sanitaire en tant que le b du 1° de son article 1er modifie le a du 2° de l'article 2-2 du décret du 1er juin.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est satisfaite dès lors que son passe sanitaire n'est plus valide depuis le 22 décembre 2021, ce qui ne lui permet plus d'accéder aux lieux soumis à sa présentation et porte atteinte à sa liberté d'aller et venir ;<br/>
              - il existe un doute sérieux quant à la légalité de la décision contestée puisqu'elle porte atteinte au principe d'égalité en ce que, d'une part, rien ne permet d'affirmer que les personnes âgées de plus de soixante-cinq ans sont plus contagieuses que les autres et, d'autre part, elle ne saurait légalement être fondée sur un objectif de réduction de la saturation des hôpitaux.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - le code de la santé publique ; <br/>
              - la loi n° 2021-689 du 31 mai 2021 ; <br/>
              - la loi n° 2021-1040 du 5 août 2021 ;<br/>
              - la loi n° 2021-1465 du 10 novembre 2021 ;<br/>
              - le décret n° 2021-699 du 1er juin 2021 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              Sur le cadre juridique du litige :<br/>
<br/>
              2. Le A du II de l'article 1er de la loi du 31 mai 2021 relative à la gestion de la sortie de crise sanitaire, dans sa rédaction issue de la loi du 5 août 2021, prévoit que : " A compter du 2 juin 2021 et jusqu'au 15 novembre 2021 inclus, le Premier ministre peut, par décret pris sur le rapport du ministre chargé de la santé, dans l'intérêt de la santé publique et aux seules fins de lutter contre la propagation de l'épidémie de covid-19 :1° Imposer aux personnes âgées d'au moins douze ans souhaitant se déplacer à destination ou en provenance du territoire hexagonal, de la Corse ou de l'une des collectivités mentionnées à l'article 72-3 de la Constitution, ainsi qu'aux personnels intervenant dans les services de transport concernés, de présenter le résultat d'un examen de dépistage virologique ne concluant pas à une contamination par la covid-19, un justificatif de statut vaccinal concernant la covid-19 ou un certificat de rétablissement à la suite d'une contamination par la covid-19 ; / 2° Subordonner à la présentation soit du résultat d'un examen de dépistage virologique ne concluant pas à une contamination par la covid-19, soit d'un justificatif de statut vaccinal concernant la covid-19, soit d'un certificat de rétablissement à la suite d'une contamination par la covid-19 l'accès à certains lieux, établissements, services ou évènements (...) ". Aux termes de l'article 2-2 du décret du 1er juin 2021 prescrivant les mesures générales nécessaires à la gestion de la sortie de crise sanitaire, dans sa rédaction résultant du décret du 14 octobre 2021 : " Pour l'application du présent décret : / 1° Sont de nature à justifier de l'absence de contamination par la covid-19 un examen de dépistage RT-PCR ou un test antigénique d'au plus 72 heures dans les conditions prévues par le présent décret. Le type d'examen admis peut être circonscrit aux seuls examens de dépistage RT-PCR ou à certains tests antigéniques si la situation sanitaire, et notamment les variants du SARS-CoV-2 en circulation, l'exige. / 2° Un justificatif du statut vaccinal est considéré comme attestant d'un schéma vaccinal complet : a) De l'un des vaccins contre la covid-19 ayant fait l'objet d'une autorisation de mise sur le marché délivrée par la Commission européenne après évaluation de l'Agence européenne du médicament ou dont la composition et le procédé de fabrication sont reconnus comme similaires à l'un de ces vaccins par l'Agence nationale de sécurité des médicaments et des produits de santé :- s'agissant du vaccin "COVID-19 Vaccine Janssen", 28 jours après l'administration d'une dose ; / - s'agissant des autres vaccins, 7 jours après l'administration d'une deuxième dose, sauf en ce qui concerne les personnes ayant été infectées par la covid-19, pour lesquelles ce délai court après l'administration d'une dose ;/ b) D'un vaccin dont l'utilisation a été autorisée par l'Organisation mondiale de la santé et ne bénéficiant pas de l'autorisation ou de la reconnaissance mentionnées au a, à condition que toutes les doses requises aient été reçues, 7 jours après l'administration d'une dose complémentaire d'un vaccin à acide ribonucléique (ARN) messager bénéficiant d'une telle autorisation ou reconnaissance ; / 3° Un certificat de rétablissement à la suite d'une contamination par la covid-19 est délivré sur présentation d'un document mentionnant un résultat positif à un examen de dépistage RT-PCR ou à un test antigénique réalisé plus de onze jours et moins de six mois auparavant. Ce certificat n'est valable que pour une durée de six mois à compter de la date de réalisation de l'examen ou du test mentionnés à la phrase précédente. ". Aux termes du I de l'article 47-1 : " Les personnes majeures et, à compter du 30 septembre 2021, les personnes mineures âgées d'au moins douze ans et deux mois doivent, pour être accueillies dans les établissements, lieux, services et évènements mentionnés aux II et III, présenter l'un des documents suivants : / 1° Le résultat d'un examen de dépistage ou d'un test mentionné au 1° de l'article 2-2 réalisé moins de 72 heures avant l'accès à l'établissement, au lieu, au service ou à l'évènement. Les seuls tests antigéniques pouvant être valablement présentés pour l'application du présent 1° sont ceux permettant la détection de la protéine N du SARS-CoV-2 ; / 2° Un justificatif du statut vaccinal délivré dans les conditions mentionnées au 2° de l'article 2-2 ; / 3° Un certificat de rétablissement délivré dans les conditions mentionnées au 3° de l'article 2-2. / La présentation de ces documents est contrôlée dans les conditions mentionnées à l'article 2-3. / A défaut de présentation de l'un de ces documents, l'accès à l'établissement, au lieu, au service ou à l'évènement est refusé, sauf pour les personnes justifiant d'une contre-indication médicale à la vaccination dans les conditions prévues à l'article 2-4. " En vertu du IV de ce même article : " IV.- Le présent article est applicable, à compter du 30 août 2021, aux salariés, agents publics, bénévoles et aux autres personnes qui interviennent dans les lieux, établissements, services ou évènements concernés, lorsque leur activité se déroule dans les espaces et aux heures où ils sont accessibles au public, à l'exception des activités de livraison et sauf intervention d'urgence. ". Le b du 1° de l'article 1er du décret du 25 novembre 2021 dont le requérant demande la suspension dispose que  " Pour l'application de l'article 47-1, les personnes de soixante-cinq ans ou plus ayant reçu le vaccin mentionné au présent alinéa doivent, pour que leur schéma vaccinal reste reconnu comme complet à partir du 15 décembre 2021, avoir reçu une dose complémentaire d'un vaccin à acide ribonucléique (ARN) messager remplissant les conditions mentionnées au premier alinéa du présent a entre 5 et 7 mois suivant l'injection de la dernière dose requise. Pour celles ayant reçu cette dose complémentaire au-delà du délai de 7 mois mentionné à la phrase précédente, le schéma vaccinal est reconnu comme complet 7 jours après son injection. Pour celles ayant reçu cette dose complémentaire avant le 15 décembre 2021, le schéma vaccinal est reconnu comme complet à cette date, ou 7 jours après son injection si elle a été réalisée entre le 10 et le 14 décembre 2021 ". <br/>
<br/>
              Sur la demande en référé :<br/>
              3. M. B... soutient qu'en imposant aux personnes de plus de 65 ans justifiant d'un schéma vaccinal complet de recevoir une troisième dose pour que leur schéma vaccinal reste reconnu comme complet à compter du 15 décembre, le Premier ministre aurait méconnu le principe d'égalité dès lors que ces personnes ne présentent pas un risque plus important que les autres de transmettre le virus. Il fait aussi valoir qu'une telle discrimination ne pourrait légalement être fondée sur le risque de saturation des services de santé. Toutefois, la nécessité de ce rappel vaccinal, recommandé par toutes les autorités de santé, dont la Haute Autorité de santé dans son avis du 18 novembre, répond à la diminution progressive des défenses immunitaires apportées par le vaccin. En l'imposant ainsi pour des raisons de santé publique d'abord aux personnes de plus de 65 ans auxquelles la vaccination avait été ouverte plus tôt du fait de leur vulnérabilité particulière au virus, le Premier ministre ne saurait être regardé comme ayant porté atteinte au principe d'égalité. Ce moyen n'est par suite, en l'état de l'instruction, pas de nature à faire naître un doute sérieux sur la légalité de cette disposition. <br/>
<br/>
              4. Il résulte de l'ensemble de ce qui précède, sans qu'il soit besoin de se prononcer sur la condition d'urgence, que la demande du requérant tendant à la suspension partielle de l'exécution du décret du 25 novembre 2021 ne peut qu'être rejetée selon la procédure prévue à l'article L. 522-3 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. A... B....<br/>
Copie en sera adressée au Premier ministre et au ministre des solidarités et de la santé.<br/>
Fait à Paris, le 24 décembre 2021<br/>
Signé : Nathalie Escaut<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
