<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038490074</ID>
<ANCIEN_ID>JG_L_2019_05_000000427381</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/49/00/CETATEXT000038490074.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 17/05/2019, 427381, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-05-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427381</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET</AVOCATS>
<RAPPORTEUR>Mme Sophie Baron</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHS:2019:427381.20190517</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              Le médecin-conseil régional, chef du service médical d'Ile-de-France, a porté plainte contre M. A...B...devant la section des assurances sociales de la chambre disciplinaire de première instance d'Ile-de-France de l'ordre des chirurgiens-dentistes. Par une décision du 30 novembre 2016, la section des assurances sociales a infligé à M. B...la sanction de l'interdiction du droit de donner des soins aux assurés sociaux pendant une durée d'un an, dont quatre mois assortis du sursis et lui a ordonné de rembourser à la caisse primaire d'assurance maladie des Yvelines la somme de 5 008,10 euros et à la caisse primaire d'assurance maladie de l'Essonne la somme de 1 920 euros. <br/>
<br/>
              Par une décision n° 1776 du 6 décembre 2018, la section des assurances sociales du Conseil national de l'ordre des chirurgiens-dentistes a, sur appel de M.B..., annulé cette décision et lui a infligé la sanction de l'interdiction du droit de donner des soins aux assurés sociaux pendant une durée de huit mois, dont cinq mois assortis du sursis, lui a ordonné de rembourser à la caisse primaire d'assurance maladie des Yvelines la somme de 4 019,35 euros et à la caisse primaire d'assurance maladie de l'Essonne la somme de 1 458,88 euros et dit que la sanction serait exécutée du 1er mars au 31 mai 2019. <br/>
<br/>
              Procédures devant le Conseil d'Etat :<br/>
<br/>
              1° Sous le n° 427381, par un pourvoi, enregistré le 25 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) de mettre à la charge du Conseil national de l'ordre des chirurgiens-dentistes la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              2° Sous le n° 427383, par une requête, enregistrée le 25 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat d'ordonner qu'il soit sursis à l'exécution de cette même décision.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Baron, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de M. A...B...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le pourvoi par lequel M. B...demande l'annulation de la décision du 6 décembre 2018 de la section des assurances sociales du Conseil national de l'ordre des chirurgiens-dentistes et sa requête tendant à ce qu'il soit sursis à l'exécution de cette même décision présentent à juger les mêmes questions. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              3. Pour demander l'annulation de la décision de la section des assurances sociales du Conseil national de l'ordre des chirurgiens-dentistes qu'il attaque, M. B...soutient qu'elle est entachée :<br/>
              - d'insuffisance de motivation, en ce qu'elle juge fondés les griefs présentés dans la plainte ; <br/>
              - d'insuffisance de motivation, faute de répondre au moyen tiré de ce qu'il n'a pas commis certains des faits qui lui sont reprochés.<br/>
<br/>
              M B...soutient enfin que cette décision prononce une sanction hors de proportion avec les fautes qui lui sont reprochées et que la période d'exécution qu'elle retient constitue, par elle-même, une seconde sanction.<br/>
<br/>
              4. Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
              5. Le pourvoi formé par M. B...contre la décision de la section des assurances sociales du Conseil national de l'ordre des chirurgiens-dentistes du 6 décembre 2018 n'étant pas admis, les conclusions qu'il présente aux fins de sursis à exécution de cette même décision sont devenues sans objet.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B... n'est pas admis.<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions de la requête de M. B... tendant à ce qu'il soit sursis à l'exécution de la décision de la section des assurances sociales du Conseil national de l'ordre des médecins du 6 décembre 2018.<br/>
Article 3 : La présente décision sera notifiée à M. A...B....<br/>
Copie en sera adressée au médecin-conseil régional, chef du service médical d'Ile-de-France. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
