<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034078386</ID>
<ANCIEN_ID>JG_L_2017_02_000000395274</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/07/83/CETATEXT000034078386.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 23/02/2017, 395274</TITRE>
<DATE_DEC>2017-02-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395274</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CORLAY ; SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT</AVOCATS>
<RAPPORTEUR>M. Clément Malverti</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:395274.20170223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme B...A...et la SARL Côte d'Opale ont demandé au tribunal administratif de Poitiers d'annuler pour excès de pouvoir l'arrêté du 20 mai 2011 par lequel le maire de Saint-Georges d'Oléron a refusé de leur accorder un permis de construire une résidence de tourisme comportant cinquante unités d'habitation sur un terrain situé sur le territoire de la commune. Par un jugement n° 1101556 du 10 décembre 2013, le tribunal administratif a rejeté  leur demande.<br/>
<br/>
              Par un arrêt n° 14BX00486 du 13 octobre 2015, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par M. et Mme A...et la SARL Côte d'Opale contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 14 décembre 2015 et 14 mars 2016 au secrétariat du contentieux du Conseil d'Etat, M. et Mme A... et la SARL Côte d'Opale demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°)  réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Saint-Georges d'Oléron la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Clément Malverti, auditeur, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de La Varde, Buk Lament, Robillot, avocat de M. et Mme A...et de la SARL Côte d'Opale, et à Me Corlay, avocat de la commune de Saint-Georges d'Oléron. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Côte d'Opale a sollicité le 23 mai 2005 la délivrance d'un permis de construire une résidence de tourisme sur deux parcelles appartenant à M. et Mme A...sur le territoire de la commune de Saint-Georges d'Oléron ; que, par un arrêté du 14 décembre 2006, le maire de cette commune a sursis à statuer sur cette demande pour une durée de deux ans en raison de la révision du plan local d'urbanisme ; qu'une première délibération du 4 décembre 2008 du conseil municipal de Saint-Georges d'Oléron a approuvé les nouvelles dispositions du plan local d'urbanisme ; que la demande de permis de construire de la société a été rejetée par un arrêté du maire de Saint-Georges d'Oléron du 30 janvier 2009 au motif que le plan local d'urbanisme approuvé le 4 décembre 2008 classait les deux parcelles du terrain d'assiette dans des zones ne permettant plus la réalisation du projet ; que postérieurement à cet arrêté, le conseil municipal, par une délibération du 30 avril 2009, a retiré sa délibération du 4 décembre 2008 et a approuvé, après rectification, le plan local d'urbanisme ; que le tribunal administratif de Poitiers, par un jugement du 24 mars 2011, a annulé pour excès de pouvoir l'arrêté du 30 janvier 2009 refusant le permis de construire sollicité par la société Côte d'Opale au motif qu'il se trouvait privé de base légale à la suite du retrait de la délibération du 4 décembre 2008 ; que, dans ce même jugement, elle a enjoint au maire de la commune de Saint-Georges d'Oléron de réexaminer la demande de permis de construire présentée par la société Côte d'Opale ; que cette demande a été une nouvelle fois rejetée par un arrêté du 20 mai 2011 fondé sur le plan local d'urbanisme approuvé par délibération du conseil municipal de Saint-Georges d'Oléron du 30 avril 2009 ; que les époux A...et la société Côte d'Opale se pourvoient en cassation contre l'arrêt de la cour administrative d'appel de Bordeaux rejetant leur appel formé contre le jugement du 10 décembre 2013 du tribunal administratif de Poitiers rejetant leur demande tendant à l'annulation pour excès de pouvoir de l'arrêté du 20 mai 2011 ;  <br/>
<br/>
              2.	Considérant qu'aux termes de l'article L. 600-2 du code de l'urbanisme, dans sa rédaction issue de la loi n° 94-112  du 9 février 1994 : " Lorsqu'un refus opposé à une demande d'autorisation d'occuper ou d'utiliser le sol (...) a fait l'objet d'une annulation juridictionnelle, la demande d'autorisation (...) confirmée par l'intéressé ne peut faire l'objet d'un nouveau refus ou être assortie de prescriptions spéciales sur le fondement de dispositions d'urbanisme intervenues postérieurement à l'intervention de la décision annulée, sous réserve que l'annulation soit devenue définitive et que la confirmation de la demande ou de la déclaration soit effectuée dans les six mois suivant la notification de l'annulation au pétitionnaire. " ; qu'aux termes de l'article L. 911-2 du code de justice administrative, dans sa rédaction issue de la loi n° 95-125 du 8 février 1995 : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne à nouveau une décision après une nouvelle instruction, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision juridictionnelle, que cette nouvelle décision doit intervenir dans un délai déterminé " ; que lorsqu'une juridiction, à la suite de l'annulation d'un refus opposé à une demande d'autorisation d'occuper ou d'utiliser le sol, fait droit à des conclusions tendant à ce qu'il soit enjoint à l'administration de réexaminer cette demande, ces conclusions aux fins d'injonction du requérant doivent être regardées comme confirmant sa demande initiale ; que, par suite, la condition posée par l'article L. 600-2 du code de l'urbanisme imposant que la demande ou la déclaration soit confirmée dans les six mois suivant la notification de l'annulation au pétitionnaire doit être regardée comme remplie lorsque la juridiction enjoint à l'autorité administrative de réexaminer la demande présentée par le requérant ; que, dans un tel cas, l'autorité administrative compétente doit, sous réserve que l'annulation soit devenue définitive et que le pétitionnaire ne dépose pas une demande d'autorisation portant sur un nouveau projet, réexaminer la demande initiale sur le fondement des dispositions d'urbanisme applicables à la date de la décision annulée, en application de l'article L. 600-2 du code de l'urbanisme ;<br/>
<br/>
              3.	Considérant qu'il résulte de ce qui a été dit au point 2 qu'en jugeant que l'injonction de réexaminer la demande de permis de construire sollicité par la société Côte d'Opale prononcée par le tribunal administratif de Poitiers ne dispensait pas le pétitionnaire de confirmer sa demande dans un délai de six mois pour bénéficier des dispositions de l'article L. 600-2 du code de l'urbanisme, la cour administrative d'appel de Bordeaux a commis une erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'arrêt attaqué doit être annulé ;<br/>
<br/>
              4.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Saint-Georges d'Oléron la somme de 3 500 euros à verser à la SARL Côte d'Opale et à M. et MmeA..., au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la SARL Côte d'opale et de M. et Mme A...qui ne sont pas, dans la présente instance, les parties perdantes ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt du 13 octobre 2015 de la cour administrative de Bordeaux est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative de Bordeaux.<br/>
<br/>
Article 3 : La commune de Saint-Georges d'Oléron versera à M. et Mme A...et à la SARL Côte d'Opale une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions de la commune de Saint-Georges d'Oléron présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. et MmeA..., à la SARL Côte d'Opale et à la commune de Saint-Georges d'Oléron.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-03-02-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. PROCÉDURE D'ATTRIBUTION. DEMANDE DE PERMIS. - CRISTALLISATION DES DISPOSITIONS APPLICABLES EN CAS D'ANNULATION D'UN REFUS DE PERMIS DE CONSTRUIRE ET DE RENOUVELLEMENT DE LA DEMANDE DANS UN DÉLAI DE SIX MOIS (ART. L. 600-2 DU CODE DE L'URBANISME) - CAS OÙ LE JUGE ENJOINT À L'ADMINISTRATION DE RÉEXAMINER LA DEMANDE - EFFET - CONFIRMATION DE LA DEMANDE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-06-05 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. EFFETS DES ANNULATIONS. - CAS OÙ LE JUGE, APRÈS AVOIR ANNULÉ UN REFUS D'AUTORISATION D'URBANISME, ENJOINT À L'ADMINISTRATION DE RÉEXAMINER LA DEMANDE - EFFET - CONFIRMATION DE LA DEMANDE AU SENS DE L'ART. L. 600-2 DU CODE DE L'URBANISME - EXISTENCE.
</SCT>
<ANA ID="9A"> 68-03-02-01 Cristallisation des dispositions applicables en cas d'annulation d'un refus de permis de construire et de renouvellement de la demande dans un délai de six mois (art. L. 600-2 du code de l'urbanisme).,,,Lorsqu'une juridiction, à la suite de l'annulation d'un refus opposé à une demande d'autorisation d'occuper ou d'utiliser le sol, fait droit à des conclusions tendant à ce qu'il soit enjoint à l'administration de réexaminer cette demande, ces conclusions aux fins d'injonction du requérant doivent être regardées comme confirmant sa demande initiale. Par suite, la condition posée par l'article L. 600-2 du code de l'urbanisme imposant que la demande ou déclaration soit confirmée dans les six mois suivant la notification de l'annulation au pétitionnaire doit être regardée comme remplie lorsque la juridiction enjoint à l'autorité administrative de réexaminer la demande présentée par le requérant. Dans un tel cas, l'autorité administrative compétente doit, sous réserve que l'annulation soit devenue définitive et que le pétitionnaire ne dépose pas une demande d'autorisation portant sur un nouveau projet, réexaminer la demande initiale sur le fondement des dispositions d'urbanisme applicables à la date de la décision annulée, en application de l'article L. 600-2 du code de l'urbanisme.</ANA>
<ANA ID="9B"> 68-06-05 Cristallisation des dispositions applicables en cas d'annulation d'un refus de permis de construire et de renouvellement de la demande dans un délai de six mois (art. L. 600-2 du code de l'urbanisme).,,,Lorsqu'une juridiction, à la suite de l'annulation d'un refus opposé à une demande d'autorisation d'occuper ou d'utiliser le sol, fait droit à des conclusions tendant à ce qu'il soit enjoint à l'administration de réexaminer cette demande, ces conclusions aux fins d'injonction du requérant doivent être regardées comme confirmant sa demande initiale. Par suite, la condition posée par l'article L. 600-2 du code de l'urbanisme imposant que la demande ou déclaration soit confirmée dans les six mois suivant la notification de l'annulation au pétitionnaire doit être regardée comme remplie lorsque la juridiction enjoint à l'autorité administrative de réexaminer la demande présentée par le requérant. Dans un tel cas, l'autorité administrative compétente doit, sous réserve que l'annulation soit devenue définitive et que le pétitionnaire ne dépose pas une demande d'autorisation portant sur un nouveau projet, réexaminer la demande initiale sur le fondement des dispositions d'urbanisme applicables à la date de la décision annulée, en application de l'article L. 600-2 du code de l'urbanisme.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
