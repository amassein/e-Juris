<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031861278</ID>
<ANCIEN_ID>JG_L_2015_12_000000384382</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/86/12/CETATEXT000031861278.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère SSJS, 23/12/2015, 384382, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384382</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT</AVOCATS>
<RAPPORTEUR>M. Frédéric Puigserver</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:384382.20151223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante:<br/>
<br/>
              M. et Mme C...A...ont demandé au tribunal administratif de Nîmes d'annuler pour excès de pouvoir l'arrêté du 21 août 2010 par lequel le maire de Chusclan (Gard) a délivré, au nom de l'Etat, un permis de construire à Mme D...en vue de la rénovation d'un bâtiment d'habitation. Par un jugement n° 1002639 du 25 mai 2012, le tribunal administratif de Nîmes a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 12LY22948 du 8 juillet 2014, la cour administrative d'appel de Lyon, à la demande de M. et MmeA..., a annulé le jugement du tribunal administratif de Nîmes du 25 mai 2012 et l'arrêté du 21 août 2010. <br/>
<br/>
              Par un pourvoi, enregistré le 10 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, le ministre du logement, de l'égalité des territoires et de la ruralité demande au Conseil d'Etat d'annuler cet arrêt de la cour administrative d'appel de Lyon du 8 juillet 2014.<br/>
<br/>
            Vu les autres pièces du dossier ;<br/>
<br/>
            Vu :<br/>
            - le code de l'urbanisme ;<br/>
            - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Puigserver, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Balat, avocat de M. et Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 422-1 du code de l'urbanisme, dans sa rédaction applicable à la date de la décision attaquée : " L'autorité compétente pour délivrer le permis de construire, d'aménager ou de démolir et pour se prononcer sur un projet faisant l'objet d'une déclaration préalable est : / a) Le maire, au nom de la commune, dans les communes qui se sont dotées d'un plan local d'urbanisme ou d'un document d'urbanisme en tenant lieu, ainsi que, lorsque le conseil municipal l'a décidé, dans les communes qui se sont dotées d'une carte communale ; lorsque le transfert de compétence à la commune est intervenu, ce transfert est définitif ; / b) Le préfet ou le maire au nom de l'Etat dans les autres communes ". Aux termes de l'article L. 422-5 du même code : " Lorsque le maire ou le président de l'établissement public de coopération intercommunale est compétent, il recueille l'avis conforme du préfet si le projet est situé : / a) Sur une partie du territoire communal non couverte par une carte communale, un plan local d'urbanisme ou un document d'urbanisme en tenant lieu (...) ". Ces dernières dispositions ne s'appliquent que lorsque le maire est compétent, en vertu du a de l'article L. 422-1, pour délivrer le permis de construire au nom de la commune, et non lorsqu'il est compétent pour ce faire au nom de l'Etat.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 21 août 2010, le maire de la commune de Chusclan, laquelle était à cette date dépourvue de tout document d'urbanisme, a accordé, au nom de l'Etat, un permis de construire à Mme D.... Par son arrêt du 8 juillet 2014, la cour administrative d'appel de Lyon a annulé l'arrêté du 21 août 2010 au motif que le maire, en délivrant le permis de construire sans avoir recueilli l'avis favorable explicite du préfet du Gard,  avait méconnu le a de l'article L. 422-5 du code de l'urbanisme.<br/>
<br/>
              3. En faisant application de ces dispositions à une commune qui n'était dotée d'aucun document local d'urbanisme, alors qu'ainsi qu'il a été dit, elles ne s'appliquent qu'aux communes dans lesquelles la compétence pour délivrer le permis de construire a été transférée au maire en application du a de l'article L. 422-1 du code de l'urbanisme, la cour administrative d'appel de Lyon a méconnu le champ d'application de la loi. Ce moyen, qui est d'ordre public, peut être utilement soulevé pour la première fois en cassation. <br/>
<br/>
              4. Aux termes de l'article R. 422-2 du code de l'urbanisme : " Le préfet est compétent pour délivrer le permis de construire, d'aménager ou de démolir et pour se prononcer sur un projet faisant l'objet d'une déclaration préalable dans les communes visées au b de l'article L. 422-1 et dans les cas prévus par l'article L. 422-2 dans les hypothèses suivantes : (...) e) En cas de désaccord entre le maire et le responsable du service de l'Etat dans le département chargé de l'instruction mentionné à l'article R. 423-16 (...) ". L'article R. 423-74 du même code dispose que le chef du service de l'Etat dans le département chargé de l'instruction adresse un projet de décision au maire ou, dans les cas prévus à l'article R. 422-2, au préfet. L'accord mentionné au e de l'article R. 422-2 peut ainsi résulter de la transmission d'un projet de décision au maire par le responsable du service de l'Etat chargé de l'instruction. <br/>
<br/>
              5. M. et Mme A...demandent que soit substitué au motif retenu par la cour celui tiré de ce que le maire n'avait pas compétence, en application du e de l'article R. 422-2 du code de l'urbanisme, pour délivrer le permis de construire au nom de l'Etat en l'absence d'avis favorable du directeur départemental des territoires versé au dossier. Toutefois, l'examen de ce motif supposerait de porter une nouvelle appréciation sur les faits de l'espèce. Par suite, il n'appartient pas au Conseil d'Etat, statuant sur un pourvoi en cassation, de substituer un tel motif à celui, erroné en droit, sur lequel la cour s'est fondée.<br/>
<br/>
              6. Il résulte de ce qui précède que le ministre du logement, de l'égalité des territoires et de la ruralité est fondé à demander l'annulation de l'arrêt attaqué. <br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative de Lyon du 8 juillet 2014 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
Article 3 : Les conclusions présentées par M. et Mme A...au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la ministre du logement, de l'égalité des territoires et de la ruralité et à M. et Mme C...A....<br/>
Copie en sera adressée à Mme B...D.... <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
