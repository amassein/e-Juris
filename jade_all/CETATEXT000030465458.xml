<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030465458</ID>
<ANCIEN_ID>JG_L_2015_04_000000379403</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/46/54/CETATEXT000030465458.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 09/04/2015, 379403, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-04-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>379403</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Yannick Faure</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:379403.20150409</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 5 mai, 25 juillet et 9 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, l'association Parents contre la drogue demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2014-322 du 11 mars 2014 relatif à la mission interministérielle de lutte contre les drogues et les conduites addictives ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier.<br/>
<br/>
              Vu :<br/>
              - la Constitution ;<br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le décret n° 2011-184 du 14 février 2011 ;<br/>
              - le code de justice administrative, notamment son article R. 611-8.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yannick Faure, auditeur,  <br/>
<br/>
- les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 13 mars 2015, présentée par l'association Parents contre la drogue.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur la légalité externe du décret attaqué :<br/>
<br/>
              1. Le décret attaqué a pour objet, d'une part, de substituer au comité interministériel de lutte contre la drogue et la toxicomanie et de prévention des dépendances le comité interministériel de lutte contre les drogues et les conduites addictives et, d'autre part, de substituer à la mission interministérielle de lutte contre la drogue et la toxicomanie la mission interministérielle de lutte contre les drogues et les conduites addictives, chargée d'animer et de coordonner les actions de l'Etat en matière de lutte contre l'usage nocif des substances psychoactives et les conduites addictives. <br/>
<br/>
              2. En premier lieu, ces dispositions ne sont pas des " décisions fixant (...) le montant des objectifs de dépenses d'assurance maladie et des dotations nationales " mentionnées par l'article R. 162-22 du code de la sécurité sociale, ne sont pas relatives " aux conditions d'implantation des activités de soins et des équipements matériels lourds " ou " aux conditions techniques de fonctionnement " mentionnées par l'article R. 6122-2 du code de la santé publique, ne définissent pas les conditions auxquelles sont soumises " la production, la fabrication, le transport, l'importation, l'exportation, la détention, l'offre, la cession, l'acquisition et l'emploi de plantes, de substances ou de préparations classées comme vénéneuses " mentionnées par l'article L. 5132-8 du même code, n'instituent pas " des modes particuliers de soins préventifs ou curatifs " au sens de l'article L. 161-37 du code de la sécurité sociale et ne sont pas " mesures propres à préserver la santé de l'homme " mentionnées par l'article L. 1311-1 du code de la santé publique. Par suite, l'association Parents contre la drogue n'est pas fondée à soutenir que le décret qu'elle attaque aurait dû être précédé, en vertu de ces dispositions, de la consultation, respectivement, du conseil de l'hospitalisation, de la section sanitaire du comité national de l'organisation sanitaire et sociale, du conseil national de l'ordre des pharmaciens, de la Haute Autorité de santé et du haut conseil de la santé publique.<br/>
<br/>
              3. En deuxième lieu, aux termes de l'article 34 du décret du 15 février 2011 relatif aux comités techniques dans les administrations et les établissements publics de l'Etat : " Les comités techniques sont consultés (...) sur les questions et projets de textes relatifs : / 1° A l'organisation et au fonctionnement des administrations, établissements ou services ; / 2° A la gestion prévisionnelle des effectifs, des emplois et des compétences ; / (...) / 6° A la formation et au développement des compétences et qualifications professionnelles (...) ". Si le décret attaqué modifie le nom et le champ de compétence de la mission interministérielle de lutte contre les drogues et les conduites addictives, qui bénéficie, en vertu de l'article D. 3411-16 du code de la santé publique, d'emplois permanents et de personnels mis à sa disposition par les départements ministériels ou les établissements publics, il n'affecte ni l'organisation ni le fonctionnement des services du Premier ministre ou du ministère des affaires sociales et de la santé et ne relève à aucun autre titre des compétences dévolues aux comités techniques par les dispositions du décret du 15 février 2011. Dès lors, l'association requérante n'est pas fondée à soutenir que le décret attaqué aurait dû être précédé de la consultation des comités techniques ministériels placés auprès du Premier ministre et auprès des ministres chargés de la santé, de la jeunesse, de la vie associative, des solidarités, de la cohésion sociale, de la ville et des sports.<br/>
<br/>
              4. En troisième lieu, aux termes de l'article 22 de la Constitution : " Les actes du Premier ministre sont contresignés, le cas échéant, par les ministres chargés de leur exécution ". Les ministres chargés de l'exécution d'un acte sont ceux qui ont compétence pour signer ou contresigner les mesures réglementaires ou individuelles que comporte nécessairement son exécution. Le décret attaqué, qui modifie les dispositions applicables à un comité interministériel présidé par le Premier ministre et à une mission interministérielle placée sous l'autorité de ce dernier, n'appelle aucune mesure que le garde des sceaux, ministre de la justice, ou le ministre des affaires sociales et de la santé seraient compétents pour signer ou contresigner. Il suit de là que le moyen tiré du défaut de contreseing de ces ministres doit être écarté.<br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              5. Aux termes de l'article L. 3411-1 du code de la santé publique : " Une personne usant d'une façon illicite de substances ou plantes classées comme stupéfiants bénéficie d'une prise en charge sanitaire organisée par l'agence régionale de santé ". Ni ces dispositions, ni la circonstance que le livre quatrième de la troisième partie du code de la santé publique est relatif à la lutte contre la toxicomanie, ne faisaient obstacle à ce que le Premier ministre, ainsi qu'il lui était loisible de le faire, décide de confier à une même mission interministérielle le soin d'animer et de coordonner les actions de l'Etat en matière de lutte contre la drogue et les autres conduites addictives, liées notamment à l'alcool, au tabac et aux médicaments psychotropes. Par suite, l'association requérante n'est pas fondée à soutenir que le décret attaqué serait entaché d'erreur de droit ni, en tout état de cause, d'erreur manifeste d'appréciation.<br/>
<br/>
              6. Il résulte de ce qui précède que l'association Parents contre la drogue n'est pas fondée à demander l'annulation du décret qu'elle attaque. Par suite, sa requête doit être rejetée, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : La requête de l'association Parents contre la drogue est rejetée.<br/>
Article 2 : La présente décision sera notifiée à l'association Parents contre la drogue et au Premier ministre.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
