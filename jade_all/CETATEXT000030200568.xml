<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030200568</ID>
<ANCIEN_ID>JG_L_2015_01_000000374146</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/20/05/CETATEXT000030200568.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème SSJS, 26/01/2015, 374146, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-01-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374146</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:374146.20150126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 374146, la requête sommaire et le mémoire complémentaire, enregistrés les 20 décembre 2013 et 20 mars 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SARL Mascareignes Kino, dont le siège est 145 avenue du Stade-Etang à Saint-Paul (97460), représentée par son gérant en exercice ; la SARL Mascareignes Kino demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 204 du 28 juin 2013 par laquelle la Commission nationale d'aménagement commercial, statuant en matière cinématographique, après avoir admis le recours de la SARL Ecran Sud dirigé contre la décision du 29 janvier 2013 de la commission départementale d'aménagement commercial de la Réunion, statuant en matière cinématographique, refusant à la SARL Ecran Sud la création d'un établissement de 10 salles de spectacles cinématographiques regroupant 1 652 places à l'enseigne " Cinépalmes " à Saint-Pierre, a délivré à cette société l'autorisation préalable requise pour la création de cet établissement, ainsi que la décision implicite de cette même commission, rejetant son recours gracieux formé contre cette décision ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Vu 2°, sous le n° 374147, la requête sommaire et le mémoire complémentaire, enregistrés les 20 décembre 2013 et 20 mars 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SARL Foncière de la Plaine, dont le siège est 7 rue Saint-Louis BP 159 à Saint-Pierre (97454), représentée par son gérant en exercice ; la SARL Foncière de la Plaine demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir les décisions analysées sous le n° 374146 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ; elle soulève les mêmes moyens que la SARL Mascareignes Kino sous le n° 374146 ;<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu les notes en délibéré, enregistrées le 21 janvier 2015, présentées par la SARL Ecran Sud ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 21 janvier 2015, présentée pour la SARL Foncière de la Plaine ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 21 janvier 2015, présentée pour la SARL Mascareignes Kino ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu le code du cinéma et de l'image animée ;<br/>
<br/>
              Vu la loi n° 2008-776 du 4 août 2008 ;<br/>
<br/>
              Vu le décret n° 2008-1212 du 24 novembre 2008 ;<br/>
<br/>
              Vu le décret n° 2009-1393 du 11 novembre 2009 ;<br/>
<br/>
              Vu le décret n° 2011-921 du 1er août 2011 ; <br/>
<br/>
              Vu l'arrêté du 5 décembre 2008 pris pour l'application du III de l'article R. 752-7 du code de commerce et relatif à la demande portant sur les projets d'aménagement cinématographique ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de la SARL Mascareignes Kino et de la SARL Foncière de la Plaine ;<br/>
<br/>
<br/>
<br/>1. Considérant que les requêtes visées ci-dessus sont dirigées contre les mêmes décisions ; qu'il y a lieu de les joindre pour statuer par une seule décision ; <br/>
<br/>
              2. Considérant que les sociétés requérantes sont titulaires d'une autorisation préalable pour la réalisation d'un complexe cinématographique dans la même commune que celle où doit être réalisé le projet autorisé par les décisions attaquées ; que la circonstance que le complexe ainsi autorisé n'a pas encore été réalisé et que sa réalisation pourrait être entravée par des difficultés liées à la maîtrise foncière ou à l'obtention d'un permis de construire ne les prive pas d'un intérêt à poursuivre l'annulation des décisions attaquées ; <br/>
<br/>
              3. Considérant qu'aux termes de l'article R. 752-6 du code de commerce : " La demande d'autorisation prévue à l'article L. 752-1 et à l'article 30-2 du code de l'industrie cinématographique est présentée soit par le propriétaire de l'immeuble, soit par une personne justifiant d'un titre l'habilitant à construire sur le terrain ou à exploiter commercialement l'immeuble " ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier que la SARL Ecran Sud a présenté, à l'appui de sa demande d'autorisation d'exploitation commerciale, et pour justifier de la maîtrise foncière requise par les dispositions de l'article R. 752-6, une attestation notariale faisant état de la vente par la SARL Foncière 2001 à la SNC Cininvest du terrain destiné à accueillir le projet, ainsi qu'une habilitation émanant de la SNC Cininvest l'autorisant à présenter la demande d'autorisation ; que, toutefois, l'acte de vente notarié dont disposait la commission nationale faisait état de deux contentieux relatifs au terrain d'assiette du projet  et, par un jugement du 29 mars 2013 porté à la connaissance de la commission nationale avant sa délibération, le tribunal de grande instance de Saint-Pierre a prononcé la régularisation forcée de cette vente au profit de la SCCV Sobeca et jugé qu'en l'absence d'une telle régularisation, le jugement vaudrait titre de propriété pour cette société ; que, dans ces conditions, la société pétitionnaire n'ayant pas justifié de sa maîtrise foncière, la commission nationale ne pouvait légalement accorder l'autorisation sollicitée par la SARL Ecran Sud ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens des requêtes, les sociétés requérantes sont fondées à demander l'annulation des décisions attaquées ;<br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que des sommes soient mises à ce titre à la charge des sociétés requérantes, qui ne sont pas, dans les présentes instances, les parties perdantes ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de l'Etat les sommes de 1 000 euros chacune à verser au titre de ces mêmes dispositions ; <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La décision du 28 juin 2013 et la décision implicite rejetant le recours gracieux du 27 août 2013 de la Commission nationale d'aménagement commercial, statuant en matière cinématographique, sont annulées.<br/>
<br/>
Article 2 : L'Etat versera la somme de 1 000 euros chacune à la SARL Mascareignes Kino et à la SARL Foncière de la plaine au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : Les conclusions présentées par la SARL Ecran Sud au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la SARL Mascareignes Kino, à la SARL Foncière de la Plaine, à la SARL Ecran Sud et à la Commission nationale d'aménagement commercial, statuant en matière cinématographique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
