<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039217414</ID>
<ANCIEN_ID>JG_L_2019_10_000000414682</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/21/74/CETATEXT000039217414.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème chambre jugeant seule, 14/10/2019, 414682, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414682</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème chambre jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Nicolas Agnoux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:414682.20191014</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée (SAS) Montchapet Automobiles a demandé au tribunal administratif de Dijon de prononcer la décharge des rappels de taxe sur les surfaces commerciales auxquelles elle a été assujettie au titre des années 2010, 2011 et 2012, ainsi que les pénalités correspondantes. Par un jugement n° 1401403 du 9 février 2017, le tribunal administratif de Dijon a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 17LY01501 du 28 septembre 2017, enregistré le même jour au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Lyon, ayant rejeté l'appel formé par la société contre ce jugement en tant qu'il a statué sur les impositions établies au titre de l'année 2010, a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi formé par la société Montchapet Automobiles contre ce jugement en ce qui concerne les années 2011 et 2012. <br/>
<br/>
              Par ce pourvoi enregistré le 6 avril 2017 au greffe de la cour administrative d'appel de Lyon, et un mémoire complémentaire, enregistré le 19 février 2018 au secrétariat du contentieux du Conseil d'Etat, la SAS Montchapet Automobiles demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il se prononce sur les impositions établies au titre des années 2011 et 2012 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - la loi n° 72-657 du 13 juillet 1972 ;<br/>
              - le décret n° 95-85 du 26 janvier 1995 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Agnoux, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la société Montchapet Automobiles ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Montchapet Automobiles, qui exerce une activité de vente de véhicules automobiles dans un établissement situé à Dijon (Côte-d'Or), a fait l'objet d'une vérification de comptabilité à l'issue de laquelle l'administration fiscale l'a assujettie à des rappels de taxe sur les surfaces commerciales au titre des années 2010 à 2012. Elle se pourvoit en cassation contre le jugement du 9 février 2017 par lequel le tribunal administratif de Dijon a rejeté sa demande, en tant qu'elle tendait à la décharge des impositions établies au titre des années 2011 et 2012.<br/>
<br/>
              Sur les moyens relatifs à la régularité de la procédure d'imposition<br/>
<br/>
              2. Aux termes de l'article L. 76 du livre des procédures fiscales : " Les bases ou éléments servant au calcul des impositions d'office et leurs modalités de détermination sont portées à la connaissance du contribuable trente jours au moins avant la mise en recouvrement des impositions ". Par une appréciation souveraine des faits exempte de dénaturation, le tribunal administratif de Dijon a estimé que la proposition de rectification adressée le 23 mai 2013 à la société requérante répondait aux exigences de cet article. Le moyen tiré de ce qu'il aurait omis de répondre à un moyen sur ce point manque en fait.<br/>
<br/>
              Sur le bien-fondé de l'imposition<br/>
<br/>
              3. Aux termes de l'article 3 de la loi du 13 juillet 1972 instituant des mesures en faveur de certaines catégories de commerçants et artisans âgés, dans sa rédaction applicable aux années d'imposition en litige : " Il est institué une taxe sur les surfaces commerciales assise sur la surface de vente des magasins de commerce de détail, dès lors qu'elle dépasse 400 mètres carrés des établissements ouverts à partir du 1er janvier 1960 quelle que soit la forme juridique de l'entreprise qui les exploite. (...) / La surface de vente des magasins de commerce de détail, prise en compte pour le calcul de la taxe, et celle visée à l'article L. 720-5 du code de commerce, s'entendent des espaces affectés à la circulation de la clientèle pour effectuer ses achats, de ceux affectés à l'exposition des marchandises proposées à la vente, à leur paiement, et de ceux affectés à la circulation du personnel pour présenter les marchandises à la vente. (...)  / Si ces établissements, à l'exception de ceux dont l'activité principale est la vente ou la réparation de véhicules automobiles, ont également une activité de vente au détail de carburants, l'assiette de la taxe comprend en outre une surface calculée forfaitairement en fonction du nombre de position de ravitaillement dans la limite de 70 mètres carrés par position de ravitaillement. (...) / Pour les établissements dont le chiffre d'affaires au mètre carré est inférieur à 3 000 euros, le taux de cette taxe est de 5,74 euros au mètre carré de surface définie au troisième alinéa. Pour les établissements dont le chiffre d'affaires au mètre carré est supérieur à 12 000 euros, le taux est fixé à 34,12 euros. / A l'exclusion des établissements qui ont pour activité principale la vente ou la réparation de véhicules automobiles, les taux mentionnés à l'alinéa précédent sont respectivement portés à 8,32 euros ou 35,70 euros (...). Un décret prévoira, par rapport aux taux ci-dessus, des réductions pour les professions dont l'exercice requiert des superficies de vente anormalement élevées ou, en fonction de leur chiffre d'affaires au mètre carré, pour les établissements dont la surface des locaux de vente destinés à la vente au détail est comprise entre 400 et 600 mètres carrés ". Aux termes de l'article 3 du décret du 26 janvier 1995 : " A. - La réduction de taux prévue au troisième alinéa de l'article 3 de la loi du 13 juillet 1972 susvisée en faveur des professions dont l'exercice requiert des superficies de vente anormalement élevées est fixée à 30 p. 100 en ce qui concerne la vente exclusive des marchandises énumérées ci-après (...) - véhicules automobiles ".<br/>
<br/>
              4. En premier lieu, il résulte de la lettre même des dispositions citées au point 3 que les établissements dont l'activité principale est la vente ou la réparation de véhicules automobiles, qu'ils soient neufs ou d'occasion, sont inclus dans le champ d'application de la taxe sur les surfaces commerciales. Le tribunal administratif, dont le jugement n'est pas insuffisamment motivé sur ce point, n'a par suite, contrairement à ce que soutient la société requérante, pas entaché son arrêt d'erreur de droit en jugeant que la vente de véhicules automobiles constituait une activité de commerce de détail au sens et pour l'application des dispositions précitées de la loi du 13 juillet 1972, sans qu'aient d'incidence à cet égard la circonstance que le bien vendu soit adapté aux exigences du client, ni la fourniture de services complémentaires. Le tribunal, qui n'a pas méconnu la portée des écritures de la société, n'a pas commis d'erreur de droit en jugeant que le décret du 26 janvier 1995 n'ajoutait pas à la loi sur ce point.<br/>
<br/>
              5. En deuxième lieu, après avoir relevé, par une appréciation souveraine exempte de dénaturation, que les espaces d'accueil, le comptoir des professionnels et le hall d'exposition des véhicules utilitaires légers étaient affectés à la circulation des clients pour effectuer leurs achats, le tribunal administratif a pu en déduire, sans erreur de droit ni erreur de qualification juridique, que cette partie des locaux de l'établissement était incluse dans les surfaces de vente devant être prises en compte pour l'assujettissement à la taxe.  <br/>
<br/>
              6. En troisième lieu, il résulte des dispositions de la loi du 13 juillet 1972 et de celles du décret du 26 janvier 1995 que le chiffre d'affaires à prendre en compte pour la détermination du taux de la taxe sur les surfaces commerciales est celui des ventes au détail en l'état, sans qu'il y ait lieu de distinguer selon que l'acheteur est un particulier ou un professionnel. Il s'en déduit que les ventes au détail en l'état à des professionnels, tant pour leurs besoins propres que lorsqu'ils incorporent les produits qu'ils ont ainsi achetés dans les produits qu'ils vendent ou les prestations qu'ils fournissent, doivent être prises en compte pour la détermination du chiffre d'affaires par mètre carré, à la différence des ventes à des professionnels revendant en l'état, l'activité de ces derniers relevant alors d'une activité de grossiste ou d'intermédiaire. Il résulte de ce qui précède que le tribunal n'a pas commis d'erreur de droit en jugeant que le montant des ventes à des professionnels pour les besoins de leur activité devait être pris en compte pour l'application des dispositions de l'article 1er du décret du 26 janvier 1995. <br/>
<br/>
              7. En quatrième lieu, il résulte des dispositions de la loi du 13 juillet 1972 et du décret du 26 janvier 1995 que le chiffre d'affaires à prendre en compte pour le calcul de la taxe sur les surfaces commerciales s'entend de celui qui correspond à l'ensemble des ventes au détail en l'état réalisées par l'établissement, sans qu'il y ait lieu de distinguer selon que ces ventes concernent ou non des biens qui sont présentés ou stockés dans cet établissement. Le tribunal n'a, par suite, pas commis d'erreur de droit en jugeant qu'il y avait lieu, pour déterminer le chiffre d'affaires à prendre en compte pour la détermination du taux de la taxe sur les surfaces commerciales, de retenir les ventes réalisées par l'établissement correspondant à des véhicules qui, commandés et livrés ultérieurement, ne figuraient pas dans ses stocks.<br/>
<br/>
              8. Il résulte de tout ce qui précède que la société Montchapet Automobiles n'est pas fondée à demander l'annulation du jugement qu'elle attaque. Ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de la société Montchapet Automobiles est rejeté. <br/>
Article 2 : La décision sera notifiée à la société par actions simplifiée Montchapet Automobiles et au ministre de l'action et des comptes publics.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
