<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036164728</ID>
<ANCIEN_ID>JG_L_2017_12_000000403217</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/16/47/CETATEXT000036164728.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 07/12/2017, 403217, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403217</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>Mme Dominique Bertinotti</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:403217.20171207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Marseille la condamnation de la société d'exploitation du réseau d'assainissement de Marseille à lui verser une indemnité de 8 380 euros en réparation des préjudices résultant de l'accident dont il a été victime le 13 novembre 2011. <br/>
<br/>
              Par un jugement n° 1504692 du 4 juillet 2016, le tribunal administratif de Marseille a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 16MA03344 du 30 août 2016, enregistrée au secrétariat du contentieux du Conseil d'Etat le 1er septembre 2016, la présidente de la cour administrative d'appel de Marseille a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi formé par M. B...contre ce jugement. Par ce pourvoi et un mémoire complémentaire, enregistrés le 16 janvier 2017 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 1er du jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de la société d'exploitation du réseau d'assainissement de Marseille Métropole (SERAMM) le versement de la somme de 3 000 euros à la SCP Nicolaÿ, de Lanouvelle, Hannotin, son avocat, sur le fondement des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dominique Bertinotti, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M.B..., et à la SCP Célice, Soltner, Texidor, Périer, avocat de la société d'exploitation du réseau d'assainissement de Marseille ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 27 novembre 2017, présentée pour la société d'exploitation du réseau d'assainissement de Marseille ;  <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que, pour rejeter la demande indemnitaire que lui avait présentée M. B...en vue d'obtenir la réparation des préjudices causés par la chute dans un regard d'égout dont il avait été victime le 13 novembre 2011 rue Eugène Pottier à Marseille, le tribunal administratif de Marseille a relevé que la chute s'était produite à proximité du domicile de l'intéressé à une heure de la journée où l'état défectueux de la bouche d'égout était visible et qu'il était " loisible à l'intéressé de passer à côté de cette bouche qui n'occupait qu'un faible espace de la chaussée " ; que le tribunal en a déduit que la défectuosité présentée par le regard d'égout ne pouvant échapper à un usager de la voie publique normalement attentif à sa marche et ne créant pas pour les piétons de risques excédant ceux auxquels doivent s'attendre ces usagers et contre lesquels il leur appartient de se prémunir et que  l'accident était ainsi exclusivement imputable à la faute de la victime ; <br/>
<br/>
              2.	Considérant, toutefois, qu'il ne ressort pas des pièces du dossier soumis aux juges du fond que les circonstances que M. B...connaissait les lieux et que la chute a eu lieu en plein jour auraient été de nature, s'agissant d'un désordre dont l'intéressé n'était pas censé avoir connaissance, à caractériser une imprudence fautive de sa part, à laquelle l'accident aurait été entièrement imputable ; qu'ainsi, en écartant tout lien de causalité avec l'état de l'ouvrage public, le tribunal administratif de Marseille a inexactement qualifié les faits de l'espèce ; que M. B...est, par suite, fondé à demander l'annulation de l'article 1er du jugement qu'il attaque ; <br/>
<br/>
              3.	Considérant qu'il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au fond ; <br/>
<br/>
              4.	Considérant qu'il résulte de l'instruction que M. B...a fait une chute, le 13 novembre 2011, dans un regard d'égout situé sur le trottoir de la rue Eugène Pottier à Marseille momentanément ouvert, alors que l'absence de couvercle sur le regard n'avait pas fait l'objet d'un dispositif de protection et n'était pas signalée ; que la société d'exploitation du réseau d'assainissement de Marseille n'apporte aucun élément de nature à établir l'entretien normal de l'ouvrage ; que, dans les circonstances de l'espèce, aucune imprudence fautive de la part de M. B...n'est de nature à atténuer la responsabilité de la société d'exploitation du réseau d'assainissement de Marseille ; <br/>
<br/>
              5.	Considérant qu'il résulte de l'instruction, et notamment du rapport d'expertise effectué en exécution d'une ordonnance du 2 janvier 2013 du tribunal administratif de Marseille, que M. B...a subi un déficit fonctionnel temporaire évalué à 25 % durant le mois suivant l'accident du 13 novembre 2011, puis à 10 % du 14 décembre 2011 à la date de consolidation le 29 avril 2013, puis à 1 % à titre permanent ; qu'il sera fait une juste appréciation de l'ensemble de ses préjudices en condamnant la société d'exploitation du réseau d'assainissement de Marseille à lui verser une indemnité de 8 380 euros, assortie des intérêts au taux légal au jour de sa demande ;<br/>
<br/>
              6.	Considérant, qu'aux termes de l'article L. 376-1 du code de la sécurité sociale : " Lorsque, sans entrer dans les cas régis par les dispositions législatives applicables aux accidents du travail, la lésion dont l'assuré social ou son ayant droit est atteint est imputable à un tiers, l'assuré ou ses ayants droit conserve contre l'auteur de l'accident le droit de demander la réparation du préjudice causé, conformément aux règles du droit commun, dans la mesure où ce préjudice n'est pas réparé par application du présent livre ou du livre Ier. / Les caisses de sécurité sociale sont tenues de servir à l'assuré ou à ses ayants droit les prestations prévues par le présent livre et le livre Ier, sauf recours de leur part contre l'auteur responsable de l'accident dans les conditions ci-après. / Les recours subrogatoires des caisses contre les tiers s'exercent poste par poste sur les seules indemnités qui réparent des préjudices qu'elles ont pris en charge, à l'exclusion des préjudices à caractère personnel (...) / En contrepartie des frais qu'elle engage pour obtenir le remboursement mentionné au troisième alinéa ci-dessus, la caisse d'assurance maladie à laquelle est affilié l'assuré social victime de l'accident recouvre une indemnité forfaitaire à la charge du tiers responsable et au profit de l'organisme national d'assurance maladie. Le montant de cette indemnité est égal au tiers des sommes dont le remboursement a été obtenu, dans les limites d'un montant maximum de 910 euros et d'un montant minimum de 91 euros. A compter du 1er janvier 2007, les montants mentionnés au présent alinéa sont révisés chaque année, par arrêté des ministres chargés de la sécurité sociale et du budget, en fonction du taux de progression de l'indice des prix à la consommation hors tabac prévu dans le rapport économique, social et financier annexé au projet de loi de finances pour l'année considérée " ; <br/>
<br/>
              7.	Considérant qu'il résulte de l'instruction que la caisse primaire d'assurance-maladie des Bouches-du-Rhône a exposé des dépenses pour un montant non contesté de 1 412,26 euros ; qu'il y a lieu de condamner la société d'exploitation du réseau d'assainissement de Marseille à lui verser cette somme assortie des intérêts au taux légal ; qu'il y a lieu également de lui accorder, en application  des dispositions précitées de l'article L. 376-1 du code de la sécurité sociale, une indemnité forfaitaire de gestion d'un montant de 470,75 euros ;<br/>
<br/>
              8.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société d'exploitation du réseau d'assainissement de Marseille la somme de 3 000 euros à verser à la SCP Nicolaÿ-Lanouvelle-Hannotin, avocat de M.B..., sur le fondement des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat ; que les dispositions de l'article L. 761-1 du code de justice administrative font, en revanche, obstacle à ce que soit mise à la charge de M.B..., qui n'est pas la partie perdante dans la présente instance, la somme demandée à ce titre par la société d'exploitation du réseau d'assainissement de Marseille ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'article 1er du jugement n° 1504692 du 4 juillet 2016 du tribunal administratif de Marseille est annulé.<br/>
<br/>
Article 2 : La société d'exploitation du réseau d'assainissement de Marseille est condamnée à verser à M. B...une somme de 8 380 euros. Cette somme portera intérêts au taux légal à compter du 17 juin 2015.<br/>
<br/>
Article 3 : La société d'exploitation du réseau d'assainissement de Marseille est condamnée à verser à la caisse primaire d'assurance-maladie des Bouches-du-Rhône la somme de 1 412,26 euros qui portera intérêts au taux légal à compter du 22 juillet 2015 et une indemnité forfaitaire de gestion d'un montant de 470,75 euros.<br/>
<br/>
Article 4 : La société d'exploitation du réseau d'assainissement de Marseille versera la somme de 3 000 euros à la SCP Nicolaÿ-Lanouvelle-Hannotin, avocat de M.B..., au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat. <br/>
<br/>
Article 5 : Le surplus des conclusions de la demande de M. B...et les conclusions présentées par la société d'exploitation du réseau d'assainissement de Marseille au titre de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
<br/>
Article 6 : La présente décision sera notifiée à M. A...B..., à la caisse primaire d'assurance-maladie des Bouches-du-Rhône, à la société d'exploitation du réseau d'assainissement de Marseille et à la métropole Aix-Marseille Provence.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
