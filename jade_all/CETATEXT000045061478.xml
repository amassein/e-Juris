<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000045061478</ID>
<ANCIEN_ID>JG_L_2021_12_000000459177</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/45/06/14/CETATEXT000045061478.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 15/12/2021, 459177, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>459177</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:459177.20211215</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administrative de suspendre l'exécution de l'arrêté du 16 octobre 2021 par lequel le ministre de l'intérieur a prononcé son expulsion, lui a imposé de remettre sur demande ses documents d'identité et de voyage à l'autorité administrative ou aux services de police ou de gendarmerie et lui a retiré son titre de séjour et celle de l'arrêté du même jour par lequel le ministre de l'intérieur a fixé l'Algérie comme pays de destination.<br/>
<br/>
              Par une ordonnance n° 2122830 du 29 octobre 2021, le juge des référés du tribunal administratif de Paris a rejeté sa requête. <br/>
<br/>
              Par une requête enregistrée le 6 décembre 2021 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de suspendre l'exécution de l'arrêté du 16 octobre 2021 par lequel le ministre de l'intérieur a prononcé son expulsion, lui a imposé de remettre sur demande ses documents d'identité et de voyage à l'autorité administrative ou aux services de police ou de gendarmerie et lui a retiré son titre de séjour ;  <br/>
<br/>
              3°) de suspendre l'exécution de l'arrêté du 16 octobre 2021 par lequel le ministre de l'intérieur a fixé l'Algérie comme pays de destination ; <br/>
<br/>
              4°) de régler l'affaire au fond sur le fondement de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              5°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - l'ordonnance est entachée de vice de forme, en méconnaissance de l'article R. 742-5 du code de justice administrative, en ce que, d'une part, la copie de l'ordonnance rendue par le juge des référés dont l'exposant a reçu notification ne comporte pas la signature de ce magistrat et que, d'autre part, la seule mention de son nom dactylographié ne saurait valoir signature ; <br/>
              - la condition d'urgence est satisfaite dès lors que, d'une part, il est actuellement placé en rétention administrative au sein du centre de rétention de Marseille et son expulsion est susceptible d'intervenir à tout moment, et, d'autre part, les arrêtés attaqués ne peuvent faire l'objet d'une contestation juridictionnelle en mesure d'être tranchée avant leur mise à exécution ;   <br/>
              - les arrêtés contestés portent une atteinte grave et manifestement illégale à son droit à une vie privée et familiale dès lors que sa famille se trouve en France et qu'il est dépourvu d'attaches familiales en Algérie ; <br/>
              - il ne représente pas une menace pour l'ordre public, comme l'établit notamment l'expertise psychiatrique estimant qu'il n'est pas dangereux pour autrui.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              Sur la procédure et la nature du présent recours :<br/>
<br/>
              2. Il résulte de l'instruction que le juge des référés du tribunal administratif de Paris, après avoir instruit l'affaire dans le respect du caractère contradictoire de la procédure et tenu une audience conformément aux dispositions de l'article L. 522-1 du code de justice administrative, a rendu son ordonnance sur le fondement de l'article L. 521-2 précité, sans faire usage du pouvoir que lui donne l'article L. 522-3 du même code. Par suite, et contrairement à ce qui est soutenu, la requête de M. B... présente le caractère d'un appel contre cette ordonnance et non d'un pourvoi en cassation. <br/>
<br/>
              Sur la régularité de l'ordonnance attaquée :   <br/>
<br/>
              3. Le moyen tiré de ce que la minute de l'ordonnance attaquée ne serait pas signée manque en fait. <br/>
<br/>
              Sur le bien-fondé de l'ordonnance attaquée : <br/>
<br/>
              4. Aux termes de l'article L. 631-3 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Ne peut faire l'objet d'une décision d'expulsion qu'en cas de comportements de nature à porter atteinte aux intérêts fondamentaux de l'Etat, ou liés à des activités à caractère terroriste, ou constituant des actes de provocation explicite et délibérée à la discrimination, à la haine ou à la violence contre une personne déterminée ou un groupe de personnes : 1° L'étranger qui justifie par tous moyens résider habituellement en France depuis qu'il a atteint au plus l'âge de treize ans (...) ".<br/>
<br/>
              5. Eu égard à son office, il appartient au juge des référés, saisi sur le fondement de l'article L. 521-2 du code de justice administrative, d'un litige relatif à une mesure d'expulsion du territoire français, de concilier les exigences de la protection de la sûreté de l'Etat et de la sécurité publique avec la sauvegarde des libertés fondamentales, au nombre desquelles figure le droit de mener une vie privée et familiale normale. La condition d'illégalité manifeste posée par l'article L. 521-2 ne peut être regardée comme remplie que dans le cas où il est justifié d'une atteinte manifestement disproportionnée aux buts en vue desquels la mesure attaquée a été prise.   <br/>
<br/>
              6. En premier lieu, il résulte de l'instruction que M. B..., ressortissant algérien né en 1968, est entré en France avec sa famille à l'âge de 4 ans et a bénéficié de titres de séjour régulièrement renouvelés. Il souffre de troubles psychiatriques et d'addictions toxicologiques qui ont justifié son placement en curatelle renforcée par une décision du 20 janvier 2020. Dix de ses frères et sœurs, ainsi que sa mère, vivent en France. Il résulte également de l'instruction, comme l'a relevé le juge des référés du tribunal administratif de Paris, qu'il n'est pas totalement dépourvu d'attaches familiales en Algérie, et ne justifie pas de relations habituelles avec sa fratrie vivant en France. <br/>
<br/>
              7. En second lieu, il résulte des motifs, non contestés sur ce point, de l'arrêté d'expulsion litigieux que M. B... a été condamné à quatorze reprises entre 1988 et 2003, pour des faits de de violence avec usage ou menace d'armes et infractions à la législation des stupéfiants, puis, après l'abrogation de plein droit, en application de l'article 86 de la loi du 26 novembre 2003, d'un premier arrêté d'expulsion pris à son encontre le 10 septembre 2001, de nouveau condamné à sept reprises pour des faits de vol, de recel et de port d'armes. Si le médecin psychiatre qui a examiné M. B... à la demande de l'administration le 23 octobre 2020 a estimé qu'il ne présentait plus, d'un point de vue médical, de danger immédiat pour lui-même ou pour autrui, il résulte également de l'instruction, et en particulier des constatations opérées par le juge pénal, que M. B... a adressé à quinze reprises, en septembre et octobre 2020, sur un réseau social, des menaces de mort à l'avocate de certaines parties civiles au procès des attentats terroristes de janvier 2015 contre le journal Charlie Hebdo, menaces assorties de propos haineux à l'égard de la France, légitimant ces attentats ainsi que l'assassinat du professeur Samuel Paty. Ces faits ont donné lieu à une condamnation de l'intéressé par le tribunal correctionnel de Marseille à une peine d'emprisonnement de trois ans, dont 18 mois assortis d'un sursis probatoire. <br/>
<br/>
              8. Compte-tenu du parcours de délinquant récidiviste de M. B... et de son adhésion, récemment confirmée par sa dernière condamnation pénale, à une idéologie violente, haineuse et discriminatoire, dans un contexte de menace terroriste élevée sur le territoire national, c'est à bon droit que le juge des référés du tribunal administratif de Paris a jugé que la décision d'expulsion de M. B... ne porte pas une atteinte grave et manifestement illégale à son droit de mener une vie privée et familiale normale.  <br/>
<br/>
              9. Il résulte de l'ensemble de ce qui précède, sans qu'il soit besoin de se prononcer sur la condition d'urgence, que la requête de M. B... est manifestement mal fondée, et qu'il y a lieu de la rejeter selon la procédure prévue à l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. A... B....<br/>
Copie en sera adressée au ministre de l'intérieur. <br/>
Fait à Paris, le 15 décembre 2021<br/>
Signé : Cyril Roger-Lacan<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
