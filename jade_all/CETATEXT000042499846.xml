<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042499846</ID>
<ANCIEN_ID>JG_L_2020_11_000000429502</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/49/98/CETATEXT000042499846.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 04/11/2020, 429502, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429502</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET ; SCP RICARD, BENDEL-VASSEUR, GHNASSIA</AVOCATS>
<RAPPORTEUR>M. Martin Guesdon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:429502.20201104</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Le Syndicat SUD des sapeurs-pompiers professionnels, agents techniques et administratifs du service départemental d'incendie et de secours (SDIS) de la Drôme a demandé au tribunal administratif de Grenoble, d'une part, d'annuler la délibération du 18 décembre 2013 par laquelle le bureau du SDIS de la Drôme a approuvé la modification du guide de gestion du temps de travail des personnels du SDIS de la Drôme à compter du 1er janvier 2014 et, d'autre part, d'enjoindre au conseil d'administration du SDIS de la Drôme de délibérer sur le règlement du temps de travail dans un délai de quinze jours. Par un jugement n° 1400950 du 31 octobre 2016, le tribunal administratif de Grenoble a annulé la délibération du 18 décembre 2013 en ce qu'elle approuve les dispositions qui prévoient que l'écart constaté en moins entre le service annuel horaire effectué par un agent et le volume annuel de 1 583 heures auquel il est soumis est défalqué sur le compte épargne-temps de l'agent et rejeté le surplus des conclusions du syndicat.<br/>
<br/>
              Par un arrêt n° 17LY00124 du 5 février 2019, la cour administrative d'appel de Lyon a rejeté l'appel formé par le syndicat SUD des sapeurs-pompiers professionnels, agents techniques et administratifs du SDIS de la Drôme contre ce jugement, ainsi que les conclusions incidentes présentées par le SDIS de la Drôme.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 5 avril et 8 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, le syndicat SUD des sapeurs-pompiers professionnels, agents techniques et administratifs du SDIS de la Drôme demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge du SDIS de la Drôme la somme de 3 000 euros au titre de l'article L.761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive 2003/88/CE du Parlement européen et du Conseil du 4 novembre 2003 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 2013-1186 du 18 décembre 2013 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Martin Guesdon, auditeur,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Sevaux, Mathonnet, avocat du syndicat SUD des sapeurs-pompiers professionnels agents techniques et administratifs du SDIS de la Drôme et à la SCP Ricard, Bendel-Vasseur, Ghnassia, avocat du service départemental d'incendie et de secours de la Drôme ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. A la suite de la modification du décret du 31 décembre 2001 relatif au temps de travail des sapeurs-pompiers professionnels par le décret du 18 décembre 2013, le bureau du conseil d'administration du service départemental d'incendie et de secours (SDIS) de la Drôme a, par une délibération du 18 décembre 2013, modifié les dispositions du " Guide de gestion du temps de travail des personnels du SDIS de la Drôme ", avec effet au 1er janvier 2014. Le syndicat SUD des sapeurs-pompiers professionnels, agents techniques et administratifs du SDIS de la Drôme a demandé l'annulation de cette délibération devant le tribunal administratif de Grenoble. Par un jugement du 31 octobre 2016, ce tribunal a annulé la délibération en tant qu'elle approuve les dispositions prévoyant que l'écart constaté en moins entre le service annuel horaire effectué par un agent et le volume annuel de travail de 1 583 heures auquel il est soumis est défalqué du compte épargne-temps de l'agent l'année suivante et rejeté le surplus des conclusions du syndicat. Le syndicat SUD des personnels du SDIS de la Drôme se pourvoit en cassation contre l'arrêt par lequel la cour administrative d'appel de Lyon a rejeté son appel. Eu égard aux moyens soulevés, le syndicat doit être regardé comme ne demandant l'annulation de cet arrêt qu'en tant qu'il statue sur le régime horaire d'équivalence valorisant les gardes de 24 heures à hauteur de 16,6 heures et sur le report des heures non effectuées sur l'année suivante. <br/>
<br/>
              2. Aux termes de l'article 1er du décret du 31 décembre 2001 modifié : " La durée de travail effectif des sapeurs-pompiers professionnels est définie conformément à l'article 1er du décret du 25 août 2000 susvisé auquel renvoie le décret du 12 juillet 2001 et comprend : 1. Le temps passé en intervention ; 2. Les périodes de garde consacrées au rassemblement qui intègre les temps d'habillage et de déshabillage, à la tenue des registres, à l'entraînement physique, au maintien des acquis professionnels, à des manoeuvres de la garde, à l'entretien des locaux, des matériels et des agrès ainsi qu'à des tâches administratives et techniques, aux pauses destinées à la prise des repas ; 3. Le service hors rang, les périodes consacrées aux actions de formation définies par arrêté du ministre de l'intérieur dont les durées sont supérieures à 8 heures, et les services de sécurité ou de représentation ". Aux termes de l'article 2 du même décret : " La durée de travail effectif journalier définie à l'article 1er ne peut pas excéder 12 heures consécutives. Lorsque cette période atteint une durée de 12 heures, elle est suivie obligatoirement d'une interruption de service d'une durée au moins égale ". Aux termes de l'article 3 du même décret : " Par dérogation aux dispositions de l'article 2 relatives à l'amplitude journalière, une délibération du conseil d'administration du service d'incendie et de secours peut, eu égard aux missions des services d'incendie et de secours et aux nécessités de service, et après avis du comité technique, fixer le temps de présence à vingt-quatre heures consécutives. / Dans ce cas, le conseil d'administration fixe une durée équivalente au décompte semestriel du temps de travail, qui ne peut excéder 1 128 heures sur chaque période de six mois ".<br/>
<br/>
              3. Ces dispositions n'ont ni pour objet ni pour effet d'assimiler tout ou partie du temps de présence des sapeurs-pompiers à du temps de repos. Sans introduire de pondération qui minorerait la durée de travail effectivement prise en compte, elles imposent que la totalité de la durée effective de travail des agents ne dépasse pas 1 128 heures sur chaque période de six mois, soit l'équivalent de 48 heures par semaine. Si, pour le calcul de la durée effective du travail des agents, la présence au cours d'une garde est ainsi assimilable à du travail effectif, dès lors que les intéressés doivent se tenir en permanence prêts à intervenir, ces mêmes dispositions n'empêchent en revanche pas, pour l'établissement de la rémunération des sapeurs-pompiers pendant ces gardes, de fixer des équivalences en matière de durée du travail, afin de tenir compte des périodes d'inaction que comportent ces périodes de garde.<br/>
<br/>
              4. Cependant, si le SDIS de la Drôme était en droit de prévoir un régime d'horaire d'équivalence valorisant les gardes de 24 heures à hauteur de 16,6 heures en vue de calculer la rémunération des sapeurs-pompiers et de fixer à 1583 heures annuelles le temps de travail effectif, il ne pouvait légalement mettre en oeuvre ce régime que pour autant qu'elle n'aboutissait pas à un dépassement des limites horaires, soit 1 128 heures par semestre et 2 256 heures par an. En jugeant que la mise en oeuvre de ce régime de travail pouvait légalement conduire à imposer à un sapeur-pompier réalisant quatre-vingt-quatorze gardes de 24 heures annuelles, soit 1 560,4 heures de travail effectif, un complément de 22,6 heures de travail effectif, pour atteindre les 1583 heures annuelles de travail effectif, alors qu'une telle mise en oeuvre a pour effet de porter à 2 278,6 heures le temps de travail annuel, au-delà de la limite de 2 256 heures, la cour a commis une erreur de droit. <br/>
<br/>
              5. Par ailleurs, pour écarter le moyen soulevé devant elle par le syndicat requérant, tiré de ce que le principe du report en année N+1 d'heures qui n'ont pas été effectuées en année N, du fait, notamment, de la planification des horaires de service, était dépourvu de tout fondement législatif ou réglementaire, la cour a estimé qu'il n'était pas assorti de précisions suffisantes lui permettant d'en apprécier le bien-fondé. En statuant ainsi, alors qu'il ressort des pièces du dossier qui lui était soumis que l'argumentation du syndicat était suffisamment précise, la cour a méconnu son office.<br/>
<br/>
              6. Il résulte de ce qui précède que le syndicat requérant est fondé à demander l'annulation de l'arrêt qu'il attaque en tant qu'il statue sur le régime horaire d'équivalence valorisant les gardes de 24 heures à hauteur de 16,6 heures et sur le report des heures non effectuées sur l'année suivante.<br/>
<br/>
              7. Dans les circonstances de l'espèce, il y a lieu de mettre à la charge du SDIS de la Drôme la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. Les dispositions de cet article font en revanche obstacle à ce qu'une somme soit mise à la charge du syndicat requérant qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 5 février 2019 est annulé en tant qu'il statue sur le régime d'horaire d'équivalence valorisant les gardes de 24 heures à hauteur de 16,6 heures et sur le report des heures non effectuées sur l'année suivante.<br/>
<br/>
Article 2 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Lyon.<br/>
<br/>
Article 3 : Le SDIS de la Drôme versera la somme de 3 000 euros au syndicat SUD des sapeurs-pompiers professionnels, agents techniques et administratifs du SDIS de la Drôme au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées par le SDIS de la Drôme au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée au syndicat SUD des sapeurs-pompiers professionnels, agents techniques et administratifs du service départemental d'incendie et de secours de la Drôme et au service département d'incendie et de secours du département de la Drôme.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
