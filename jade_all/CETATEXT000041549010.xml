<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041549010</ID>
<ANCIEN_ID>JG_L_2020_02_000000426160</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/54/90/CETATEXT000041549010.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 05/02/2020, 426160</TITRE>
<DATE_DEC>2020-02-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426160</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2020:426160.20200205</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le préfet de l'Hérault a déféré au tribunal administratif de Montpellier la décision du 14 juin 2015 par laquelle le maire de Cazevieille a délivré un permis de construire à la société civile immobilière (SCI) de l'Aire et du Cros. Par un jugement n° 1600056 du 21 septembre 2016, le tribunal administratif a annulé cette décision.<br/>
<br/>
              Par un arrêt n° 16MA03877 du 9 octobre 2018, la cour administrative d'appel de Marseille a rejeté l'appel formé par la SCI de l'Aire et du Cros contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 10 décembre 2018 et 11 mars 2019 au secrétariat du contentieux du Conseil d'Etat, la SCI de l'Aire et du Cros demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 800 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Zribi et Texier, avocat de la SCI de l'Aire et du Cros.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'après avoir refusé à la SCI de l'Aire et du Cros, le 29 décembre 2014, le permis de construire qu'elle sollicitait, le maire de Cazevieille (Hérault) a retiré, le 7 avril 2015, cette décision de refus. La société ayant renouvelé sa demande de permis le 13 avril 2015, elle a acquis, le 14 juin 2015, un permis de construire tacite, dont le maire lui a délivré certificat le 30 juillet 2015. Le préfet de l'Hérault a déféré ce permis tacite au tribunal administratif de Montpellier, qui l'a annulé par un jugement du 21 septembre 2016. La SCI de l'Aire et du Cros se pourvoit en cassation contre l'arrêt du 9 octobre 2018 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant à l'annulation de ce jugement.<br/>
<br/>
              2. Aux termes de l'article L. 2131-1 du code général des collectivités territoriales : " Les actes pris par les autorités communales sont exécutoires de plein droit dès qu'il a été procédé à leur publication ou affichage (...) ainsi qu'à leur transmission au représentant de l'Etat dans le département (...) ". Aux termes de l'article L. 2131-6 du même code : " Le représentant de l'Etat dans le département défère au tribunal administratif les actes mentionnés à l'article L. 2131-2 qu'il estime contraires à la légalité dans les deux mois suivant leur transmission (...) ". Parmi les actes mentionnés par l'article L. 2131-2 de ce code figure, au 6° : " Le permis de construire et les autres autorisations d'utilisation du sol et le certificat d'urbanisme délivrés par le maire ". Par ailleurs, l'article R. 424-1 du code de l'urbanisme prévoit que, à défaut d'une décision expresse dans le délai d'instruction, le silence gardé par l'autorité compétente vaut permis de construire et l'article L. 424-8 dispose qu'un tel permis tacite est exécutoire à compter de la date à laquelle il est acquis. Enfin, aux termes de l'article R. 423-7 du même code : " Lorsque l'autorité compétente pour délivrer le permis ou pour se prononcer sur un projet faisant l'objet d'une déclaration préalable est le maire au nom de la commune, celui-ci transmet un exemplaire de la demande ou de la déclaration préalable au préfet dans la semaine qui suit le dépôt ".<br/>
<br/>
              3. En premier lieu, dans le cas de la délivrance tacite d'un permis de construire, la commune est réputée avoir effectué la transmission prévue par l'article L. 2131-1 du code général des collectivités territoriales si le maire a, conformément aux dispositions de l'article R. 423-7 du code de l'urbanisme, transmis au préfet l'entier dossier de demande. Le délai dans lequel doit s'exercer le déféré prévu par l'article L. 2131-6 du code général des collectivités territoriales court alors à compter de la date à laquelle le permis est acquis ou, dans l'hypothèse où la commune ne satisfait à l'obligation de transmission que postérieurement à cette date, à compter de la réception de cette transmission par le préfet. <br/>
<br/>
              4. En second lieu, le retrait par l'autorité compétente d'une décision refusant un permis de construire ne rend pas le pétitionnaire titulaire d'un permis de construire tacite. L'autorité administrative doit statuer à nouveau sur la demande, le délai de nature à faire naître une décision tacite ne courant qu'à compter de la confirmation de cette demande par le pétitionnaire. Dans une telle hypothèse, pour l'application des dispositions de l'article L. 2131-1 du code général des collectivités territoriales, il appartient à la commune d'informer le préfet de la confirmation de sa demande par le pétitionnaire, en lui indiquant sa date de réception. Le délai de deux mois imparti au préfet par les dispositions de l'article L. 2131-6 du même code court alors, sous réserve que le préfet soit en possession de l'entier dossier de demande, à compter de la date du permis tacite si le préfet a eu connaissance de la confirmation de la demande avant la naissance du permis. Dans le cas contraire, sous la même réserve que le préfet soit en possession de l'entier dossier de demande, le délai court à compter de la date à laquelle le préfet est informé par la commune de l'existence du permis tacite, soit par la transmission du certificat délivré le cas échéant par le maire en application de l'article R. 424-13 du code de l'urbanisme, soit par la transmission, postérieurement à la naissance du permis, de la confirmation de sa demande par le pétitionnaire.<br/>
<br/>
              5. Par suite, en jugeant que, dès lors que le préfet de l'Hérault n'avait pas eu connaissance de l'existence de la confirmation de sa demande par la SCI de l'Aire et du Cros avant le 14 juin 2015, date de naissance du permis tacite en litige, le délai dont il disposait pour déférer ce permis avait commencé à courir, non le 14 juin 2015 mais à la date, postérieure, à laquelle il avait eu pour la première fois communication de la confirmation de la demande de la société, la cour administrative d'appel de Marseille n'a pas commis d'erreur de droit.<br/>
<br/>
              6. En revanche, en se fondant sur ce que, tant en première instance que devant elle, la commune avait reconnu n'avoir communiqué au préfet de l'Hérault cette confirmation de la demande de la société que le 24 août 2015, alors qu'il ressortait des pièces du dossier qui lui était soumis que ces indications ne figuraient pas dans les mémoires de la commune, la cour a entaché son arrêt de dénaturation. La SCI de l'Aire et du Cros est par suite fondée, sans qu'il soit besoin de se prononcer sur les autres moyens de son pourvoi, à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à la société requérante au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 9 octobre 2018 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : L'Etat versera à la SCI de l'Aire et du Cros une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la SCI de l'Aire et du Cros, à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales et à la commune de Cazevieille.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-01-015-02-02 COLLECTIVITÉS TERRITORIALES. DISPOSITIONS GÉNÉRALES. CONTRÔLE DE LA LÉGALITÉ DES ACTES DES AUTORITÉS LOCALES. DÉFÉRÉ PRÉFECTORAL. DÉLAI DU DÉFÉRÉ. - RETRAIT D'UNE DÉCISION DE REFUS D'UN PERMIS DE CONSTRUIRE - POINT DE DÉPART DU DÉLAI DU DÉFÉRÉ - 1) CAS OÙ LE PRÉFET A EU CONNAISSANCE DE LA CONFIRMATION DE LA DEMANDE AVANT LA NAISSANCE DU PERMIS - DATE DU PERMIS TACITE, SOUS RÉSERVE QUE LE PRÉFET SOIT EN POSSESSION DE L'ENTIER DOSSIER DE DEMANDE [RJ1] - 2) CAS OÙ LE PRÉFET N'A PAS EU CONNAISSANCE DE LA CONFIRMATION DE LA DEMANDE - DATE À LAQUELLE LE PRÉFET EST INFORMÉ PAR LA COMMUNE DE L'EXISTENCE DU PERMIS TACITE, SOUS LA MÊME RÉSERVE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-03-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. PROCÉDURE D'ATTRIBUTION. - RETRAIT D'UNE DÉCISION DE REFUS D'UN PERMIS DE CONSTRUIRE - 1) POINT DE DÉPART DU DÉLAI DE NATURE À FAIRE NAÎTRE UN PERMIS TACITE - CONFIRMATION DE LA DEMANDE PAR LE PÉTITIONNAIRE - 2) POINT DE DÉPART DU DÉLAI DU DÉFÉRÉ PRÉFECTORAL - A) CAS OÙ LE PRÉFET A EU CONNAISSANCE DE LA CONFIRMATION DE LA DEMANDE AVANT LA NAISSANCE DU PERMIS - DATE DU PERMIS TACITE, SOUS RÉSERVE QUE LE PRÉFET SOIT EN POSSESSION DE L'ENTIER DOSSIER DE DEMANDE [RJ1] - B) CAS OÙ LE PRÉFET N'A PAS EU CONNAISSANCE DE LA CONFIRMATION DE LA DEMANDE - DATE À LAQUELLE LE PRÉFET EST INFORMÉ PAR LA COMMUNE DE L'EXISTENCE DU PERMIS TACITE, SOUS LA MÊME RÉSERVE.
</SCT>
<ANA ID="9A"> 135-01-015-02-02 1) Le retrait par l'autorité compétente d'une décision refusant un permis de construire ne rend pas le pétitionnaire titulaire d'un permis de construire tacite. L'autorité administrative doit statuer à nouveau sur la demande, le délai de nature à faire naître une décision tacite ne courant qu'à compter de la confirmation de cette demande par le pétitionnaire.... ,,Dans une telle hypothèse, pour l'application des dispositions de l'article L. 2131-1 du code général des collectivités territoriales (CGCT), il appartient à la commune d'informer le préfet de la confirmation de sa demande par le pétitionnaire, en lui indiquant sa date de réception.,,,Le délai de deux mois imparti au préfet par les dispositions de l'article L. 2131-6 du même code court alors, sous réserve que le préfet soit en possession de l'entier dossier de demande, à compter de la date du permis tacite si le préfet a eu connaissance de la confirmation de la demande avant la naissance du permis.... ,,2) Dans le cas contraire, sous la même réserve que le préfet soit en possession de l'entier dossier de demande, le délai court à compter de la date à laquelle le préfet est informé par la commune de l'existence du permis tacite, soit par la transmission du certificat délivré le cas échéant par le maire en application de l'article R. 424-13 du code de l'urbanisme, soit par la transmission, postérieurement à la naissance du permis, de la confirmation de sa demande par le pétitionnaire.</ANA>
<ANA ID="9B"> 68-03-02 1) Le retrait par l'autorité compétente d'une décision refusant un permis de construire ne rend pas le pétitionnaire titulaire d'un permis de construire tacite. L'autorité administrative doit statuer à nouveau sur la demande, le délai de nature à faire naître une décision tacite ne courant qu'à compter de la confirmation de cette demande par le pétitionnaire.... ,,2) Dans une telle hypothèse, pour l'application des dispositions de l'article L. 2131-1 du code général des collectivités territoriales (CGCT), il appartient à la commune d'informer le préfet de la confirmation de sa demande par le pétitionnaire, en lui indiquant sa date de réception.,,,a) Le délai de deux mois imparti au préfet par les dispositions de l'article L. 2131-6 du même code court alors, sous réserve que le préfet soit en possession de l'entier dossier de demande, à compter de la date du permis tacite si le préfet a eu connaissance de la confirmation de la demande avant la naissance du permis.... ,,b) Dans le cas contraire, sous la même réserve que le préfet soit en possession de l'entier dossier de demande, le délai court à compter de la date à laquelle le préfet est informé par la commune de l'existence du permis tacite, soit par la transmission du certificat délivré le cas échéant par le maire en application de l'article R. 424-13 du code de l'urbanisme, soit par la transmission, postérieurement à la naissance du permis, de la confirmation de sa demande par le pétitionnaire.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur cette réserve, CE, 22 octobre 2018, M. de,, n° 400779, T. pp. 575-956.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
