<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042065717</ID>
<ANCIEN_ID>JG_L_2020_06_000000419734</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/06/57/CETATEXT000042065717.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 29/06/2020, 419734, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419734</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP L. POULET-ODENT</AVOCATS>
<RAPPORTEUR>Mme Catherine Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:419734.20200629</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La commune d'Oberbruck a demandé au tribunal administratif de Strasbourg d'annuler pour excès de pouvoir l'arrêté du 30 avril 2014 du préfet du Haut-Rhin approuvant le plan de prévention des risques d'inondation sur le bassin versant de la Doller ainsi que la décision du 29 septembre 2014 rejetant son recours gracieux. Par un jugement n° 1406613 du 27 avril 2016, le tribunal administratif de Strasbourg a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 16NC01262 du 8 février 2018, la cour administrative d'appel de Nancy a, sur appel de la commune d'Oberbruck, annulé ce jugement et l'arrêté du 30 avril 2014.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 9 avril et 9 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, le ministre d'Etat, ministre de la transition écologique et solidaire demande au Conseil d'Etat d'annuler cet arrêt. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Moreau, conseiller d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP L. Poulet, Odent, avocat de la commune d'Oberbruck ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 562-3 du code de l'environnement : " Le préfet définit les modalités de la concertation relative à 1'élaboration du projet de plan de prévention des risques naturels prévisibles. / Sont associés à l'élaboration de ce projet les collectivités territoriales et les établissements publics de coopération intercommunale concernés. Après enquête publique réalisée conformément au chapitre III du titre II du livre Ier et après avis des conseils municipaux des communes sur le territoire desquelles il doit s'appliquer, le plan de prévention des risques naturels prévisibles est approuvé par arrêté préfectoral. Au cours de cette enquête, sont entendus, après avis de leur conseil municipal, les maires des communes sur le territoire desquelles le plan doit s'appliquer. " En vertu de l'article R. 562-2 du même code, l'arrêté prescrivant 1'établissement d'un plan de prévention des risques naturels prévisibles " définit également les modalités de la concertation et de l'association des collectivités territoriales et des établissements publics de coopération intercommunale concernés, relatives à 1'élaboration du projet ". Aux termes de l'article R. 562-7 du même code : " Le projet de plan de prévention des risques naturels prévisibles est soumis à l'avis des conseils municipaux des communes et des organes délibérants des établissements publics de coopération intercommunale compétents pour 1'élaboration des documents d'urbanisme dont le territoire est couvert, en tout ou partie, par le plan. / (...) / Si le projet de plan concerne des terrains agricoles ou forestiers, les dispositions relatives à ces terrains sont soumises à 1'avis de la chambre d'agriculture et du centre national de la propriété forestière. (...) ". Aux termes de l'article R. 562-8 du même code : " Le projet de plan est soumis par le préfet à une enquête publique dans les formes prévues par les articles R. 123-6 à R. 123-23, sous réserve des dispositions des deux alinéas qui suivent. / Les avis recueillis en application des trois premiers alinéas de 1'article R. 562-7 sont consignés ou annexés aux registres de l'enquête dans les conditions prévues par l'article R. 123-1-7. " <br/>
<br/>
              2. En premier lieu, il résulte des dispositions précitées que la procédure d'élaboration du plan de prévention des risques naturels prévisibles comporte deux phases distinctes, l'une de concertation et l'autre d'enquête publique. S'il appartient au préfet, en vertu de l'article R. 562-2 du code de l'environnement, de définir les modalités de la concertation, qui a lieu aux premiers stades du projet, et, à ce titre, d'arrêter la liste des personnes qu'elle implique, la liste des personnes obligatoirement associées à la phase de l'enquête publique, par la voie du recueil de leur avis qui est joint au dossier mis à la disposition du public, est définie à l'article R. 562-7 du même code. Cependant, il ressort des pièces du dossier soumis au juge du fond que, ainsi que l'a relevé la cour, le préfet du Haut-Rhin a, par l'article 5 de l'arrêté du 7 octobre 2011 prescrivant l'élaboration du plan de prévention litigieux, fixé la liste des personnes associées à l'élaboration du plan et décidé que le projet de plan serait soumis, avant enquête publique, à toutes les personnes publiques associées, dont les avis seraient consignés ou annexés aux registres d'enquête publique. Par suite, alors même que le recueil de l'avis de l'ensemble des personnes énumérées et le versement de leur avis au dossier de l'enquête publique n'étaient pas obligatoires en vertu des dispositions des articles R. 562-7 et R. 562-8, ils s'imposaient en l'espèce du fait de la décision du préfet. Il y a lieu de substituer ce motif, dont l'examen n'implique l'appréciation d'aucune circonstance de fait et qui justifie sur ce point le dispositif de l'arrêt attaqué, au motif retenu par la cour pour juger la procédure irrégulière.<br/>
<br/>
              3. En second lieu, le moyen tiré de ce que la cour a commis une erreur de droit en jugeant que la consultation du syndicat mixte pour le SCOT de la région mulhousienne et du syndicat mixte du pays Thur Doller était requise, alors que l'article R. 562-7 du code de l'environnement ne rend obligatoire que la consultation des communes et des établissements publics de coopération intercommunale compétents pour 1'élaboration des documents d'urbanisme dont le territoire est couvert par le plan de prévention est nouveau en cassation et est, à ce titre, inopérant. <br/>
<br/>
              4. Il résulte de ce qui précède que le pourvoi du ministre d'Etat, ministre de la transition écologique et solidaire doit être rejeté.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la commune d'Oberbruck, au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre d'Etat, ministre de la transition écologique et solidaire est rejeté.<br/>
Article 2 : L'Etat versera à la commune d'Oberbruck une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la ministre de la transition écologique et solidaire et à la commune d'Oberbruck. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
