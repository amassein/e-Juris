<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029626754</ID>
<ANCIEN_ID>JG_L_2014_10_000000378066</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/62/67/CETATEXT000029626754.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère SSJS, 24/10/2014, 378066, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-10-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>378066</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Julia Beurton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:378066.20141024</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              1° Par une requête et un mémoire en réplique, enregistrés sous le n° 378066 les 18 avril et 10 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, la commune de Jours-en-Vaux demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2014-175 du 18 février 2014 portant délimitation des cantons dans le département de la Côte-d'Or ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              2° Par une requête et un mémoire en réplique, enregistrés sous le n° 378160 les 18 avril et 3 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, la commune d'Ivry-en-Montagne demande au Conseil d'Etat d'annuler pour excès de pouvoir le même décret n° 2014-175 du 18 février 2014.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu :<br/>
              - les autres pièces des dossiers ; <br/>
              - le code électoral ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Julia Beurton, Auditeur,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les requêtes des communes de Jours-en-Vaux et d'Ivry-en-Montagne sont dirigées contre le même décret. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Aux termes de l'article L. 191-1 du code électoral, résultant de la loi du 17 mai 2013 relative à l'élection des conseillers départementaux, des conseillers municipaux et des conseillers communautaires et modifiant le calendrier électoral : " Le nombre de cantons dans lesquels sont élus les conseillers départementaux est égal, pour chaque département, à la moitié du nombre de cantons existant au 1er janvier 2013, arrondi à l'unité impaire supérieure si ce nombre n'est pas entier impair. / Le nombre de cantons dans chaque département comptant plus de 500 000 habitants ne peut être inférieur à dix-sept. Il ne peut être inférieur à treize dans chaque département comptant entre 150 000 et 500 000 habitants ". Aux termes de l'article L. 3113-2 du code général des collectivités territoriales, dans sa rédaction issue de la même loi, applicable à la date du décret attaqué : " I. - Les modifications des limites territoriales des cantons, les créations et suppressions de cantons et le transfert du siège de leur chef-lieu sont décidés par décret en Conseil d'Etat après consultation du conseil général qui se prononce dans un délai de six semaines à compter de sa saisine. A l'expiration de ce délai, son avis est réputé rendu. (...) / III. - La modification des limites territoriales des cantons effectuée en application du I est conforme aux règles suivantes : / a) Le territoire de chaque canton est défini sur des bases essentiellement démographiques ; / b) Le territoire de chaque canton est continu ; / c) Est entièrement comprise dans le même canton toute commune de moins de 3 500 habitants ; / IV. - Il n'est apporté aux règles énoncées au III que des exceptions de portée limitée, spécialement justifiées, au cas par cas, par des considérations géographiques (...) ou par d'autres impératifs d'intérêt général ".<br/>
<br/>
              3. Le décret attaqué a, sur le fondement de l'article L. 3113-2 du code général des collectivités territoriales, procédé à une nouvelle délimitation des cantons du département de la Côte-d'Or, compte tenu de l'exigence de réduction du nombre des cantons de ce département de quarante-trois à vingt-trois résultant de l'article L. 191-1 du code électoral.<br/>
<br/>
              Sur la légalité externe du décret attaqué : <br/>
<br/>
              4. Il résulte des termes mêmes des dispositions de l'article L. 3113-2 du code général des collectivités territoriales citées ci-dessus qu'il appartenait au Premier ministre de procéder, par décret en Conseil d'Etat, à une nouvelle délimitation territoriale de l'ensemble des cantons. Par suite, le moyen tiré de ce que cette délimitation relevait de la compétence du législateur ne peut qu'être écarté.<br/>
<br/>
              Sur la légalité interne du décret attaqué : <br/>
<br/>
              5. En premier lieu, les communes requérantes critiquent le choix opéré par le décret attaqué de rattacher onze communes de la communauté d'agglomération " Beaune, Côte et Sud - communauté Beaune-Chagny-Nolay " au canton n° 1 (Arnay-le-Duc) plutôt qu'au canton n° 17 (Ladoix-Serrigny), en faisant notamment valoir que ce rattachement remettrait en cause l'action de la communauté d'agglomération, serait préjudiciable aux zones rurales et serait la cause d'une complexification du travail des élus et de surcoûts.<br/>
<br/>
              6. Toutefois, d'abord, il résulte des dispositions de l'article L. 3113-2 du code général des collectivités territoriales que le territoire de chaque canton doit être établi sur des bases essentiellement démographiques, qu'il doit être continu et que toute commune de moins de 3 500 habitants doit être entièrement comprise dans un même canton, seules des exceptions de portée limitée et spécialement justifiées pouvant être apportées à ces règles. Ni ces dispositions, ni aucun autre texte non plus qu'aucun principe n'imposent au Premier ministre de prévoir que les limites des cantons, qui sont des circonscriptions électorales, coïncident avec les limites des périmètres des établissements publics de coopération intercommunale ou celles des " bassins de vie " définis par l'Institut national de la statistique et des études économiques. De même, ni les dispositions de l'article L. 3113-2 du code général des collectivités territoriales, ni aucun autre texte non plus qu'aucun principe n'imposent au Premier ministre de prendre comme critères de délimitation de ces circonscriptions électorales les limites des anciens cantons ou l'absence de disparité de superficie entre cantons.<br/>
<br/>
              7. Ensuite, il ressort des pièces du dossier, d'une part, que le rattachement critiqué au canton d'Arnay-le-Duc, dont la population aurait été, à défaut, inférieure de 28,9 % à la moyenne départementale, est justifié par le respect de l'exigence tenant aux bases essentiellement démographiques de la délimitation des cantons, conformément au III de l'article L. 3113-2 du code général des collectivités territoriales, et, d'autre part, que le Premier ministre n'a pas commis d'erreur manifeste d'appréciation en estimant que les considérations géographiques tenant à l'inclusion dans ce canton d'une partie du massif du Morvan ne justifiaient pas qu'il soit dérogé à cette exigence, sur le fondement du IV du même article. Par suite, et alors même que les conditions d'exercice de leur mandat par les élus départementaux et municipaux pourraient en être affectées, les communes requérantes ne sont pas fondées à soutenir que le choix auquel le décret procède dans la délimitation du canton d'Arnay-le-Duc serait entaché d'erreur manifeste d'appréciation. <br/>
<br/>
              8. Enfin, la modification des limites territoriales des cantons doit être effectuée selon les règles prévues aux III et IV de l'article L. 3113-2 du code général des collectivités territoriales, en respectant le nombre de cantons résultant de l'application des dispositions de l'article L. 191-1 du code électoral. Par suite, les communes requérantes ne peuvent utilement invoquer la circonstance que la nouvelle délimitation conduit à des disparités de superficie très importantes entre cantons ruraux et cantons urbains et accroît la taille d'un canton tel que celui d'Arnay-le-Duc ainsi que la distance à parcourir pour accéder à la permanence du conseiller élu dans la circonscription, au détriment des communes rurales et de leurs habitants.<br/>
<br/>
              9. En second lieu, la commune de Jours-en-Vaux critique le choix opéré par le décret attaqué de dissocier la ville de Beaune, qui compose à elle seule le canton n° 3, et les communes qui l'entourent, regroupées au sein du canton n° 17 (Ladoix-Serrigny). Toutefois, d'une part, cette délimitation respecte les critères définis par le III de l'article L. 3113-2 du code général des collectivités territoriales, notamment l'exigence de continuité du territoire de chaque canton. D'autre part, il n'est pas contesté qu'il n'existe pas de continuité de l'habitat entre la ville de Beaune et les communes qui l'entourent, ce qui a d'ailleurs conduit l'Institut national de la statistique et des études économiques à estimer que Beaune devait être, à elle seule, regardée comme une " unité urbaine ". Par suite, il ne ressort pas des pièces du dossier que le choix auquel il a ainsi été procédé reposerait sur des considérations arbitraires ou serait entaché d'erreur manifeste d'appréciation. <br/>
<br/>
              10. Il résulte de tout ce qui précède, sans qu'il soit besoin de statuer sur la fin de non-recevoir soulevée par le ministre de l'intérieur à l'encontre de la requête de la commune d'Ivry-en-Montagne, que les communes de Jours-en-Vaux et d'Ivry-en-Montagne ne sont pas fondées à demander l'annulation du décret qu'elles attaquent. <br/>
<br/>
              11. Par suite, les conclusions de la commune de Jours-en-Vaux présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent qu'être également rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes des communes de Jours-en-Vaux et d'Ivry-en-Montagne sont rejetées.<br/>
Article 2 : La présente décision sera notifiée à la commune de Jours-en-Vaux, à la commune d'Ivry-en-Montagne et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
