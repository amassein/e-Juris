<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039442422</ID>
<ANCIEN_ID>JG_L_2019_12_000000423936</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/44/24/CETATEXT000039442422.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 02/12/2019, 423936</TITRE>
<DATE_DEC>2019-12-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423936</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COLIN-STOCLET</AVOCATS>
<RAPPORTEUR>M. Marc Firoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:423936.20191202</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Par une décision du 29 mars 2019, le Conseil d'Etat, statuant au contentieux a prononcé l'admission des conclusions du pourvoi du groupement de coopération sanitaire du Nord-Ouest Touraine, dirigées contre l'arrêt n° 17NT01247 du 6 juillet 2018 de la cour administrative d'appel de Nantes en tant seulement qu'il a statué sur les conclusions indemnitaires formées par la société Valeurs Culinaires au titre de son manque à gagner, au-delà de la période d'exécution initiale de douze mois du marché litigieux.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Firoud, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Colin-Stoclet, avocat du groupement de coopération sanitaire du Nord-Ouest Touraine ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un avis d'appel public à la concurrence publié au bulletin officiel des marchés publics le 4 juin 2015, le groupement de coopération sanitaire du Nord-Ouest Touraine a lancé une procédure d'appel d'offres ouvert en vue de l'attribution d'un marché public ayant pour objet la fourniture de tous éléments bruts ou cuisinés et produits consommables et l'exécution d'une mission d'assistance technique aux opérations de restauration, pour un nombre de repas annuel prévisionnel de 390 000 et pour une durée de douze mois renouvelable deux fois à compter du 1er septembre 2015. Quatre sociétés, dont la société Sogeres et la société Valeurs Culinaires, ont présenté des offres. Par un courrier électronique du 31 juillet 2015, la société Valeurs Culinaires a été informée qu'elle avait été classée en deuxième position et que le marché avait été attribué à la société Sogeres. La société Valeurs Culinaires a saisi le tribunal administratif d'Orléans de conclusions tendant à l'annulation du marché conclu le 11 août 2015 entre le groupement sanitaire et la société Sogeres et à la condamnation du groupement sanitaire à lui verser la somme de 209 292,60 euros correspondant à son manque à gagner sur trois ans. Par un jugement du 16 février 2017, le tribunal administratif d'Orléans a rejeté sa demande. La cour administrative d'appel de Nantes, par un arrêt du 6 juillet 2018, a annulé ce jugement et condamné le groupement de coopération sanitaire du Nord-Ouest Touraine à verser à la société Valeurs Culinaires une somme de 200 000 euros. Par une décision du 29 mars 2019, le Conseil d'Etat, statuant au contentieux, n'a admis les conclusions du pourvoi du groupement de coopération sanitaire du Nord-Ouest Touraine dirigé contre cet arrêt qu'en tant seulement qu'il a prononcé sa condamnation à indemniser le manque à gagner de la société Valeurs Culinaires pour une durée supérieure à douze mois.<br/>
<br/>
              2. Aux termes de l'article 16 du code des marchés publics applicable en l'espèce, repris depuis à l'article R. 2112-4 du code de la commande publique : " Un marché peut prévoir une ou plusieurs reconductions à condition que ses caractéristiques restent inchangées et que la mise en concurrence ait été réalisée en prenant en compte sa durée totale. Sauf stipulation contraire, la reconduction prévue dans le marché est tacite et le titulaire ne peut s'y opposer ".<br/>
<br/>
              3. Lorsqu'un candidat à l'attribution d'un contrat public demande la réparation du préjudice né de son éviction irrégulière de ce contrat et qu'il existe un lien direct de causalité entre la faute résultant de l'irrégularité et les préjudices invoqués par le requérant à cause de son éviction, il appartient au juge de vérifier si le candidat était ou non dépourvu de toute chance de remporter le contrat. En l'absence de toute chance, il n'a droit à aucune indemnité. Dans le cas contraire, il a droit en principe au remboursement des frais qu'il a engagés pour présenter son offre. Il convient en outre de rechercher si le candidat irrégulièrement évincé avait des chances sérieuses d'emporter le contrat conclu avec un autre candidat. Si tel est le cas, il a droit à être indemnisé de son manque à gagner, incluant nécessairement, puisqu'ils ont été intégrés dans ses charges, les frais de présentation de l'offre, lesquels n'ont donc pas à faire l'objet, sauf stipulation contraire du contrat, d'une indemnisation spécifique. En revanche, le candidat ne peut prétendre à une indemnisation de ce manque à gagner si la personne publique renonce à conclure le contrat pour un motif d'intérêt général.<br/>
<br/>
<br/>
              4. Lorsqu'il est saisi par une entreprise qui a droit à l'indemnisation de son manque à gagner du fait de son éviction irrégulière à l'attribution d'un marché, il appartient au juge d'apprécier dans quelle mesure ce préjudice présente un caractère certain. Dans le cas où le marché est susceptible de faire l'objet d'une ou de plusieurs reconductions si le pouvoir adjudicateur ne s'y oppose pas, le manque à gagner ne revêt un caractère certain qu'en tant qu'il porte sur la période d'exécution initiale du contrat, et non sur les périodes ultérieures qui ne peuvent résulter que d'éventuelles reconductions.<br/>
<br/>
              5. Il ressort des énonciations de l'arrêt attaqué que, statuant sur les conclusions indemnitaires présentées par la société Valeurs Culinaires en raison de son éviction, par le groupement de coopération sanitaire du Nord-Ouest Touraine, de l'attribution d'un marché public ayant pour objet des prestations de restauration, la cour administrative d'appel de Nantes a jugé que cette société, irrégulièrement évincée alors qu'elle avait des chances sérieuses de l'emporter, pouvait prétendre à être indemnisée de son manque à gagner. Alors qu'il ressort des pièces du dossier soumis au juge du fond que le marché faisant l'objet de la procédure de passation litigieuse était conclu pour une période d'exécution initiale de douze mois, renouvelable deux fois, la cour a retenu que l'indemnisation du manque à gagner de la société Valeurs Culinaires devait être calculée sur une période totale de trois ans correspondant à la période d'exécution initiale ainsi qu'aux deux années supplémentaires susceptibles de faire l'objet de reconductions. Il résulte de ce qui a été dit précédemment qu'en statuant ainsi, alors que le manque à gagner susceptible de donner lieu à l'indemnisation de la société Valeurs Culinaires ne pouvait revêtir de caractère certain que pour la période initiale de douze mois, la cour a commis une erreur de droit.<br/>
<br/>
              6. Il résulte de ce qui précède que l'arrêt attaqué doit être annulé en tant qu'il a prononcé la condamnation du groupement de coopération sanitaire du Nord-Ouest Touraine à indemniser le manque à gagner de la société Valeurs Culinaires pour une durée supérieure à douze mois, soit, compte tenu des motifs de l'arrêt devenus irrévocables, pour un montant supérieur à 66 666,66 euros. <br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              8. Ainsi qu'il a été dit au point 5, le manque à gagner susceptible de donner lieu à l'indemnisation de la société Valeurs Culinaires ne revêt un caractère certain que pour la période initiale de douze mois du marché litigieux. Les conclusions de la société tendant à l'indemnisation du manque à gagner au-delà de cette période ne peuvent, par suite, qu'être rejetées. <br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Valeurs Culinaires la somme de 3 000 euros à verser au groupement de coopération sanitaire du Nord-Ouest Touraine au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 2 de l'arrêt du 6 juillet 2018 de la cour administrative d'appel de Nantes est annulé en tant qu'il fait droit aux conclusions de la requête d'appel de la société Valeurs Culinaires tendant à l'indemnisation de son manque à gagner pour une durée supérieure à douze mois. <br/>
Article 2 : Les conclusions de la requête d'appel de la société Valeurs Culinaires tendant à l'indemnisation de son manque à gagner pour une durée supérieure à douze mois sont rejetées. Le montant de la condamnation du groupement de coopération sanitaire du Nord-Ouest Touraine est ramené à la somme de 66 666,66 euros.<br/>
Article 3 : La société Valeurs Culinaires versera au groupement de coopération sanitaire du Nord-Ouest Touraine la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée au groupement de coopération sanitaire du Nord-Ouest Touraine et à la société Valeurs Culinaires.<br/>
Copie en sera adressée pour information à la société Sogeres.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-08-03 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS ET OBLIGATIONS DU JUGE. - PRÉJUDICE NÉ DE L'ÉVICTION IRRÉGULIÈRE D'UNE ENTREPRISE CANDIDATE À L'ATTRIBUTION D'UN MARCHÉ PUBLIC - RÉPARATION DU MANQUE À GAGNER DU CANDIDAT QUI AVAIT UNE CHANCE SÉRIEUSE DE REMPORTER LE MARCHÉ [RJ1] - CAS D'UN MARCHÉ SUSCEPTIBLE DE RECONDUCTION - MANQUE À GAGNER ÉVALUÉ AU REGARD DE LA SEULE PÉRIODE D'EXÉCUTION INITIALE DU CONTRAT.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-04-03-02-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. ÉVALUATION DU PRÉJUDICE. PRÉJUDICE MATÉRIEL. PERTE DE REVENUS. - PRÉJUDICE NÉ DE L'ÉVICTION IRRÉGULIÈRE D'UNE ENTREPRISE CANDIDATE À L'ATTRIBUTION D'UN MARCHÉ PUBLIC - RÉPARATION DU MANQUE À GAGNER DU CANDIDAT QUI AVAIT UNE CHANCE SÉRIEUSE DE REMPORTER LE MARCHÉ [RJ1] - CAS D'UN MARCHÉ SUSCEPTIBLE DE RECONDUCTION - MANQUE À GAGNER ÉVALUÉ AU REGARD DE LA SEULE PÉRIODE D'EXÉCUTION INITIALE DU CONTRAT.
</SCT>
<ANA ID="9A"> 39-08-03 Lorsqu'il est saisi par une entreprise qui a droit à l'indemnisation de son manque à gagner du fait de son éviction irrégulière à l'attribution d'un marché, il appartient au juge d'apprécier dans quelle mesure ce préjudice présente un caractère certain. Dans le cas où le marché est susceptible de faire l'objet d'une ou de plusieurs reconductions si le pouvoir adjudicateur ne s'y oppose pas, le manque à gagner ne revêt un caractère certain qu'en tant qu'il porte sur la période d'exécution initiale du contrat, et non sur les périodes ultérieures qui ne peuvent résulter que d'éventuelles reconductions.</ANA>
<ANA ID="9B"> 60-04-03-02-01 Lorsqu'il est saisi par une entreprise qui a droit à l'indemnisation de son manque à gagner du fait de son éviction irrégulière à l'attribution d'un marché, il appartient au juge d'apprécier dans quelle mesure ce préjudice présente un caractère certain. Dans le cas où le marché est susceptible de faire l'objet d'une ou de plusieurs reconductions si le pouvoir adjudicateur ne s'y oppose pas, le manque à gagner ne revêt un caractère certain qu'en tant qu'il porte sur la période d'exécution initiale du contrat, et non sur les périodes ultérieures qui ne peuvent résulter que d'éventuelles reconductions.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur le principe et les modalités de cette réparation, CE, 18 juin 2003, Groupement d'entreprises solidaires ETPO Guadeloupe, Société Biwater et Société Aqua TP, n° 249630, T. pp. 865-909 ; CE, 8 février 2010, Commune de La Rochelle, n° 314075, p. 14 ; CE, 19 janvier 2015, Société Spie Est, n° 384653, T. pp. 760-872.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
