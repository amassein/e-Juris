<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032744690</ID>
<ANCIEN_ID>JG_L_2016_06_000000383986</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/74/46/CETATEXT000032744690.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 16/06/2016, 383986</TITRE>
<DATE_DEC>2016-06-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383986</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Lionel Collet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:383986.20160616</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par un jugement n° 1204766 du 20 juillet 2012, le tribunal administratif de Cergy-Pontoise a enjoint au préfet du Val d'Oise d'assurer l'hébergement de M. B... A...avant le 6 août 2012, sous une astreinte de 150 euros par jour de retard. Par une ordonnance n° 1206528 du 10 décembre 2013, le magistrat désigné par le président du tribunal administratif a mis à la charge de l'Etat le versement au fonds national d'accompagnement vers et dans le logement de la somme de 73 500 euros au titre de la liquidation provisoire de l'astreinte. Par une ordonnance n° 1206528 du 2 avril 2014, le magistrat désigné par le président du tribunal administratif a, à la demande du préfet du Val d'Oise, jugé qu'il n'y avait pas lieu de liquider l'astreinte.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 27 août et 26 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'ordonnance du 2 avril 2014 ;<br/>
<br/>
              2°) de renvoyer l'affaire au tribunal administratif de Cergy Pontoise ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros à verser à la SCP Baraduc-Duhamel-Rameix par application des dispositions des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la construction et de l'habitation ;<br/>
<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Lionel Collet, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, dans le cadre du droit opposable au logement, la commission de médiation du Val-d'Oise a, par une décision du 6 avril 2012, déclaré M. A...prioritaire et devant être accueilli dans une structure d'hébergement, un logement de transition, un logement-foyer ou une résidence hôtelière à vocation sociale ; qu'en l'absence d'offre d'hébergement, le tribunal administratif de Cergy-Pontoise a, par un jugement du 20 juillet 2012, enjoint au  préfet du Val-d'Oise d'assurer l'hébergement de l'intéressé sous une astreinte de 150 euros par jour de retard à compter du 6 août 2012 ; que, par une première ordonnance du 10 décembre 2013, le magistrat désigné par le président du tribunal administratif, constatant que le préfet n'avait pas justifié avoir proposé à M. A...une offre d'hébergement, a ordonné une liquidation provisoire de l'astreinte pour la période comprise entre le 6 août 2012 et le 10 décembre 2013 ; que M. A...se pourvoit en cassation contre l'ordonnance du 2 avril 2014 par laquelle le magistrat désigné a jugé qu'il n'y avait pas lieu de liquider l'astreinte au motif que l'intéressé n'ayant pas actualisé son dossier auprès du service intégré d'accueil et d'orientation (SIAO) Insertion depuis le 29 mai 2012, il devait être regardé comme ayant fait obstacle à la poursuite de la procédure d'hébergement ; <br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa du III de l'article L. 441-2-3 du code de la construction et de l'habitation, dans sa rédaction applicable au litige : " La commission de médiation peut également être saisie, sans condition de délai, par toute personne qui, sollicitant l'accueil dans une structure d'hébergement, un logement de transition, un logement-foyer ou une résidence hôtelière à vocation sociale, n'a reçu aucune proposition adaptée en réponse à sa demande. La commission de médiation transmet au représentant de l'Etat dans le département la liste des demandeurs pour lesquels doit être prévu un tel accueil et précise, le cas échéant, les mesures de diagnostic ou d'accompagnement social nécessaires " ; qu'aux termes du II de l'article L. 441-2-3-1 du même code : "Le demandeur qui a été reconnu par la commission de médiation comme prioritaire et comme devant être accueilli dans une structure d'hébergement, un établissement ou logement de transition, un logement-foyer ou une résidence hôtelière à vocation sociale et qui n'a pas été accueilli, dans un délai fixé par décret, dans l'une de ces structures peut introduire un recours devant la juridiction administrative tendant à ce que soit ordonné son accueil dans une structure d'hébergement, un établissement ou logement de transition, un logement-foyer ou une résidence hôtelière à vocation sociale./ (...)/ Le président du tribunal administratif ou le magistrat qu'il désigne, lorsqu'il constate que la demande a été reconnue prioritaire par la commission de médiation et que n'a pas été proposée au demandeur une place dans une structure d'hébergement, un établissement ou logement de transition, un logement foyer   ou une résidence hôtelière à vocation sociale, ordonne l'accueil dans l'une de ces structures et peut assortir son injonction d'une astreinte./ (...) " ; qu'aux termes de l'article R. 778-8 du code de justice administrative : " Lorsque le président du tribunal administratif ou le magistrat désigné à cet effet constate, d'office ou sur la saisine du requérant, que l'injonction prononcée n'a pas été exécutée, il procède à la liquidation de l'astreinte en faveur du fonds prévu au dernier alinéa de l'article L.300-2  du code de la construction et de l'habitation. Le président du tribunal administratif ou le magistrat désigné à cet effet peut statuer par ordonnance, dans les conditions prévues par le chapitre II du titre IV du livre VII du présent code, après avoir invité les parties à présenter leurs observations sur l'exécution de l'injonction prononcée. Il liquide l'astreinte en tenant compte de la période pendant laquelle, postérieurement à l'expiration du délai imparti par le jugement, l'injonction est demeurée inexécutée par le fait de l'administration. Il peut, eu égard aux circonstances de l'espèce, modérer le montant dû par l'Etat voire, à titre exceptionnel, déclarer qu'il n'y a pas lieu de liquider l'astreinte " ;<br/>
<br/>
              3. Considérant que le préfet peut se trouver délié de l'obligation qui pèse sur lui en vertu d'une décision de la commission de médiation et d'un jugement lui enjoignant d'exécuter cette décision si, par son comportement, l'intéressé a fait obstacle à cette exécution ; <br/>
<br/>
              4. Considérant qu'en se bornant à relever que le préfet du Val-d'Oise soutenait, sans être contredit, que M. A...n'avait pas actualisé son dossier auprès du SIAO Insertion depuis le 29 mai 2012, l'ordonnance attaquée n'a pas caractérisé une entrave à l'exécution, par le préfet, de son obligation ; que, par suite, en se fondant sur cette seule circonstance pour juger que M. A...devait être regardé comme ayant fait obstacle à la poursuite de la procédure d'hébergement et en déduire qu'il n'y avait pas lieu de liquider l'astreinte prononcée contre l'Etat, le magistrat désigné du tribunal administratif de Cergy-Pontoise a commis une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'ordonnance attaquée doit être annulée ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement d'une somme de 2 000 euros à la SCP Baraduc-Duhamel-Rameix, avocat de M.A..., en application des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance n° 1206528 du tribunal administratif de Cergy-Pontoise du 2 avril 2014 est annulée.<br/>
<br/>
		Article 2 : L'affaire est renvoyée au tribunal administratif de Cergy-Pontoise.<br/>
<br/>
Article 3 : L'Etat versera à la SCP Baraduc-Duhamel-Rameix, avocat de M. A..., une somme de 2 000 euros en application des dispositions des articles L. 761-1 du code de justice administrative et du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. B... A..., au préfet du Val-d'Oise et à la ministre du logement et de l'habitat durable.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">38-07-01 LOGEMENT. - OBLIGATION PESANT SUR LE PRÉFET EN VERTU D'UNE DÉCISION DE LA COMMISSION DE MÉDIATION ET D'UN JUGEMENT LUI ENJOIGNANT D'EXÉCUTER CETTE DÉCISION - 1) PRINCIPE - CAS OÙ L'INTÉRESSÉ FAIT OBSTACLE À CETTE EXÉCUTION - EXTINCTION DE L'OBLIGATION - 2) ESPÈCE - DEMANDEUR N'AYANT PAS ACTUALISÉ SON DOSSIER AUPRÈS DU SIAO INSERTION - ABSENCE D'ENTRAVE À L'EXÉCUTION.
</SCT>
<ANA ID="9A"> 38-07-01 1) Le préfet peut se trouver délié de l'obligation qui pèse sur lui en vertu d'une décision de la commission de médiation et d'un jugement lui enjoignant d'exécuter cette décision si, par son comportement, l'intéressé a fait obstacle à cette exécution.,,,2) La seule circonstance que l'intéressé n'ait pas actualisé son dossier auprès du service intégré d'accueil et d'orientation (SIAO) - Insertion ne caractérise pas une entrave à l'exécution, par le préfet, de son obligation. L'intéressé ne peut donc, pour ce seul motif, être regardé comme ayant fait obstacle à la poursuite de la procédure d'hébergement.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
