<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038860062</ID>
<ANCIEN_ID>JG_L_2019_07_000000420054</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/86/00/CETATEXT000038860062.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 31/07/2019, 420054, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420054</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Pierre Boussaroque</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:420054.20190731</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé à la commission départementale d'aide sociale de Paris :<br/>
              - d'annuler la décision du 11 avril 2008 par laquelle la caisse d'allocations familiales de Paris a mis fin à ses droits au revenu minimum d'insertion et décidé de récupérer un indu de revenu minimum d'insertion d'un montant de 10 995,85 euros pour la période du 1er septembre 2006 au 31 mars 2008 ;<br/>
              - d'annuler la décision du 20 juin 2008 par laquelle la commission de recours amiable de la caisse d'allocations familiales de Paris a rejeté sa demande de remise gracieuse et confirmé l'indu ;<br/>
              - d'annuler le titre de recettes émis le 19 avril 2012 par le président du conseil de Paris pour le recouvrement de cette somme ou, subsidiairement, de condamner la caisse d'allocations familiales de Paris à lui verser la somme de 10 995,85 euros en réparation du préjudice qu'elle estime avoir subi. <br/>
<br/>
              Par une décision n° 281304 du 19 février 2016, la commission départementale d'aide sociale de Paris a rejeté sa demande.<br/>
<br/>
              Par une décision n° 160389 du 23 janvier 2018, la Commission centrale d'aide sociale a rejeté l'appel formé par Mme A...contre la décision de la commission départementale d'aide sociale de Paris.<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 23 avril 2018 et 13 mai 2019 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision de la Commission centrale d'aide sociale ;  <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le décret n° 2018-928 du 29 octobre 2018 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Boussaroque, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat de MmeA..., et à la SCP Foussard, Froger, avocat du département de Paris ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 11 avril 2008, la caisse d'allocations familiales de Paris a mis fin au droit de Mme A... au revenu minimum d'insertion et a décidé de récupérer un indu de cette prestation d'un montant de 10 995,85 euros pour la période du 1er septembre 2006 au 31 mars 2008 et que, par une décision du 20 juin 2008, la commission de recours amiable de la caisse d'allocations familiales a rejeté sa demande de remise gracieuse et confirmé la récupération de cet indu. Par une décision du 19 février 2016, la commission départementale d'aide sociale de Paris a rejeté la demande de Mme A...tendant à l'annulation de ces décisions et du titre de recettes émis le 19 avril 2012 par le président du conseil de Paris pour le recouvrement de l'indu, ainsi que ses conclusions subsidiaires tendant à la condamnation de la caisse d'allocations familiales de Paris à lui verser la somme de 10 995,85 euros en réparation du préjudice qu'elle estime avoir subi. Mme A...se pourvoit en cassation contre la décision du 23 janvier 2018 par laquelle la Commission centrale d'aide sociale a rejeté son appel.<br/>
<br/>
              2. Le caractère contradictoire de la procédure interdit en principe au juge administratif de se fonder sur des mémoires ou des pièces si les parties n'ont pas été effectivement mises à même d'en prendre connaissance et de faire connaître les observations qu'ils appellent de leur part, le cas échéant dans des conditions adaptées aux délais dans lesquels le juge doit statuer. La Commission centrale d'aide sociale a relevé que MmeA..., d'une part, avait reçu le courrier du 27 août 2008 accusant réception de son recours et l'informant de la possibilité de demander à être entendue lors de l'examen de son dossier et, d'autre part, avait été avisée, le 17 avril 2014 puis le 31 décembre 2015, de la date de l'audience à laquelle serait appelé son recours et aurait pu à cette occasion obtenir communication du dossier constitué auprès de la commission départementale. En en déduisant que la commission départementale d'aide sociale de Paris avait respecté le caractère contradictoire de la procédure, alors qu'elle fondait sa décision sur des éléments produits en défense par le département de Paris sans que Mme A...en soit informée pour qu'elle puisse en prendre connaissance, alors même qu'elle avait, par un courrier et un mémoire enregistré le 14 juin 2012, sollicité la communication de " toutes les pièces du dossier ", la Commission centrale d'aide sociale a commis une erreur de droit. <br/>
<br/>
              3. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, la requérante est fondée à demander l'annulation de la décision de la Commission centrale d'aide sociale qu'elle attaque.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision de la Commission centrale d'aide sociale du 23 janvier 2018 est annulée. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : La présente décision sera notifiée à Mme B...A...et à la Ville de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
