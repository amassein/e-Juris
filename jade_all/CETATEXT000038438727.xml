<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038438727</ID>
<ANCIEN_ID>JG_L_2019_04_000000427309</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/43/87/CETATEXT000038438727.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 30/04/2019, 427309, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-04-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427309</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:427309.20190430</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a saisi, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, le juge des référés du tribunal administratif de Lille d'une demande tendant à la suspension de la décision du 12 octobre 2018 par laquelle le maire de Calais l'a révoqué. Par une ordonnance n° 1811574 du 7 janvier 2019, le juge des référés du tribunal administratif de Lille a fait droit à sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 23 janvier, 5 février et 10 avril 2019 au secrétariat du contentieux du Conseil d'Etat, la commune de Calais demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de M. A...;<br/>
<br/>
              3°) de mettre à la charge de M. A...la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la commune de Calais et à la SCP Lyon-Caen, Thiriez, avocat de M. A...;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Lille que M. A...a été recruté par contrat en 1997 par la commune de Calais, puis titularisé en 2006 en tant qu'adjoint technique territorial de 2ème classe, et a été affecté, en dernier lieu, au service des sports en tant que gardien de stade. Par un jugement du 12 janvier 2017, le tribunal de grande instance de Boulogne-sur-Mer l'a condamné à une peine d'emprisonnement de quatre mois pour des faits d'agression à l'arme blanche en état d'ébriété. Par un jugement du 30 janvier 2018, le même tribunal l'a condamné pour des faits de violence aggravée, commis le 9 mars 2016, à une peine de quatre ans d'emprisonnement dont un an avec sursis, sans inscription au bulletin n° 2 du casier judiciaire, assortie d'une mise à l'épreuve de trois ans. Par une décision du 12 octobre 2018, le maire de Calais a prononcé à l'encontre de M. A... la sanction de la révocation. Sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, M. A...a saisi le juge des référés du tribunal administratif de Lille, le 18 décembre 2018, d'une demande de suspension de l'exécution de cette décision. Par une ordonnance du 7 janvier 2019, contre laquelle la commune de Calais se pourvoit en cassation, le juge des référés a fait droit à sa demande.<br/>
<br/>
              2. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              3. L'urgence justifie que soit prononcée la suspension de l'exécution d'un acte administratif lorsque celle-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. L'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire.<br/>
<br/>
              4. Il ressort des énonciations de l'ordonnance attaquée que, pour apprécier l'urgence à suspendre l'exécution de la décision de révocation prise à l'encontre de M.A..., le juge des référés du tribunal administratif de Lille s'est borné à considérer que l'exécution de cette décision était de nature à priver M. A...de son traitement et à lui causer un trouble grave et immédiat dans ses conditions d'existence, sans prendre en compte le moyen de défense de la commune de Calais tiré de ce que, eu égard au passé violent de l'intéressé, la suspension de sa révocation et sa réintégration risquaient de compromettre le bon fonctionnement du service et de ce que, dès lors, un intérêt public s'opposait à la suspension de l'exécution de la même décision. Il suit de là que le juge des référés du tribunal administratif de Lille a entaché son ordonnance d'erreur de droit et d'insuffisance de motivation en n'appréciant la condition d'urgence qu'au regard de la seule atteinte aux intérêts de M.A....<br/>
<br/>
              5. Il résulte de ce qui précède que la commune de Calais est fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de l'ordonnance attaquée.<br/>
<br/>
              6. Dans les circonstances de l'espèce, il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée.<br/>
<br/>
              7. La décision prononçant la révocation de M. A...affecte directement sa situation, puisqu'il n'est pas contesté que ses ressources se limitent désormais au revenu de solidarité active et qu'il contribue à subvenir aux besoins de ses enfants. Toutefois, eu égard aux faits qui lui sont reprochés et au risque pour le bon fonctionnement du service qu'est susceptible de provoquer la suspension de la mesure attaquée, quel que soit le poste sur lequel cet agent serait affecté en cas de réintégration, et alors même qu'il fait valoir qu'il ne présenterait plus d'addiction à l'alcool, la condition d'urgence requise par l'article L. 521-1 du code de justice administrative n'est pas remplie.<br/>
<br/>
              8. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur l'existence de moyens propres à susciter un doute sérieux quant à la légalité de l'acte attaqué, que la demande de M. A...tendant à la suspension de la décision du 12 octobre 2018 doit être rejetée.<br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la commune de Calais, qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances particulières de l'espèce, de faire droit aux conclusions présentées par la commune de Calais au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 7 janvier 2019 du juge des référés du tribunal administratif de Lille est annulée.<br/>
<br/>
Article 2 : La demande présentée par M. A...devant le juge des référés du tribunal administratif de Lille est rejetée.<br/>
Article 3 : Les conclusions présentées par M. A...au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, ainsi que les conclusions présentées pas la commune de Calais au titre de l'article L. 761-1 du code de justice administrative, sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la commune de Calais et à M. B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
