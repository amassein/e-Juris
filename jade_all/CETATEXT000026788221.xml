<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026788221</ID>
<ANCIEN_ID>JG_L_2012_12_000000363246</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/78/82/CETATEXT000026788221.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 13/12/2012, 363246, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-12-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>363246</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:363246.20121213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 5 octobre 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par le ministre de l'éducation nationale ; le ministre demande au Conseil d'Etat d'ordonner le sursis à exécution de l'article 1er de l'arrêt n° 12VE00939 du 5 juillet 2012 par lequel la cour administrative d'appel de Versailles lui a enjoint de réaffecter M. A dans ses fonctions de proviseur du lycée de Prony d'Asnières-sur-Seine, dans le délai de trois mois à compter de la notification de la décision, sous peine d'une astreinte de 150 euros par jour de retard ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 30 novembre 2012, présentée pour M. A ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Coutard, Munier-Apaire, avocat de M. Charles A,<br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Coutard, Munier-Apaire, avocat de M. Charles A ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article R. 821-5 du code de justice administrative : " La formation de jugement peut, à la demande de l'auteur du pourvoi, ordonner qu'il soit sursis à l'exécution d'une décision juridictionnelle rendue en dernier ressort si cette décision risque d'entraîner des conséquences difficilement réparables et si les moyens invoqués paraissent, en l'état de l'instruction, sérieux et de nature à justifier, outre l'annulation de la décision juridictionnelle, l'infirmation de la solution retenue par les juges du fond. " ;<br/>
<br/>
              2. Considérant, d'une part, que, saisie sur le fondement de l'article L. 911-4 du code de justice administrative pour assurer l'exécution de l'arrêt du 3 juin 2010 par lequel elle a annulé la sanction de déplacement d'office que le ministre de l'éducation nationale avait infligée le 12 juillet 2006 à M. A en sa qualité de proviseur du lycée de Prony d'Asnières-sur-Seine, la cour administrative d'appel de Versailles a, dans l'article 1er de l'arrêt attaqué, enjoint au ministre de rétablir l'intéressé dans ses fonctions de proviseur du lycée de Prony dans un délai de trois mois à compter de la notification de cette décision ; qu'une telle mesure aurait toutefois nécessairement pour effet de porter atteinte à la situation professionnelle et personnelle de l'agent exerçant actuellement les fonctions de proviseur au sein de cet établissement ; qu'eu égard aux fonctions de l'intéressé, un tel changement, intervenant au surplus au cours de l'année scolaire, serait également de nature à perturber le bon fonctionnement de l'administration du lycée ; que, dès lors, l'exécution de cet arrêt risquerait d'entraîner des conséquences difficilement réparables pour les agents et usagers du service public concerné ;<br/>
<br/>
              3. Considérant, d'autre part, que le moyen tiré de ce que la cour administrative d'appel aurait commis une erreur de droit en accueillant, par l'article 1er de l'arrêt attaqué, les conclusions à fin d'exécution de l'article 3 de son arrêt du 3 juin 2010, qui a enjoint au ministre de l'éducation nationale de rétablir M. A dans ses fonctions de proviseur du lycée de Prony d'Asnières-sur-Seine sous réserve d'une modification, en droit ou en fait, des circonstances prises en compte par cet arrêt qui serait de nature à faire obstacle à cette mesure, alors que l'administration a, postérieurement à ce premier arrêt et après avoir recueilli les voeux puis l'accord de l'intéressé, affecté M. A sur un emploi équivalent à la tête d'un établissement en région parisienne et en aurait ainsi déjà assuré l'exécution, paraît, en l'état de l'instruction, sérieux et de nature à justifier, outre l'annulation de l'arrêt attaqué, l'infirmation de la solution retenue par les juges du fond ;<br/>
<br/>
              4. Considérant que, dans ces conditions, il y a lieu d'ordonner le sursis à exécution de l'article 1er de l'arrêt de la cour administrative d'appel de Versailles du 5 juillet 2012 ;<br/>
<br/>
              5. Considérant, enfin, que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Jusqu'à ce qu'il ait été statué sur le pourvoi du ministre de l'éducation nationale contre l'arrêt du 5 juillet 2012 de la cour administrative d'appel de Versailles, il sera sursis à l'exécution de l'article 1er de cet arrêt.<br/>
Article 2 : Les conclusions de M. A présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée au ministre de l'éducation nationale et à M. Charles A.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
