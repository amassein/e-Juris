<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038231020</ID>
<ANCIEN_ID>JG_L_2019_03_000000428031</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/23/10/CETATEXT000038231020.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 12/03/2019, 428031, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-03-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428031</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP KRIVINE, VIAUD</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:428031.20190312</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A...B...et Mme D...C...ont demandé au juge des référés du tribunal administratif de Lille, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre, d'une part, au préfet du Nord de les orienter vers une structure d'hébergement d'urgence, dans un délai de 24 heures, sous astreinte de 150 euros par jour de retard et, d'autre part, au directeur de l'Office français de l'immigration et de l'intégration (OFII) de les orienter vers une structure d'hébergement pour demandeurs d'asile, dans un délai de 24 heures, sous astreinte de 150 euros par jour de retard. Par une ordonnance nos 1900577, 1900578 du 25 janvier 2019, le juge des référés du tribunal administratif de Lille a enjoint à l'OFII d'orienter M. B...et Mme C...vers une structure d'hébergement pour demandeurs d'asile, dans un délai de 96 heures à compter de la notification de l'ordonnance et rejeté le surplus des conclusions de leur demande. <br/>
<br/>
              L'Office français de l'immigration et de l'intégration a demandé au juge des référés du tribunal administratif de Lille, statuant sur le fondement de l'article L. 521-4 du code de justice administrative, de mettre fin à l'injonction prononcée par cette ordonnance. Par une ordonnance n° 1900904 du 8 février 2019, le juge des référés du tribunal administratif de Lille a rejeté cette demande et a assorti l'injonction d'orienter M. B...et Mme C...vers une structure d'hébergement d'une astreinte de 200 euros par jour de retard à l'expiration d'un délai de 72 heures à compter de la notification de l'ordonnance. <br/>
<br/>
              M. B...et Mme C...ont demandé au juge des référés du tribunal administratif de Lille d'ordonner la liquidation de l'astreinte prononcée par cette ordonnance. Par une nouvelle ordonnance n° 1901329 du 18 février 2019, le juge des référés du tribunal administratif de Lille a condamné l'OFII à verser à M. B...et MmeC..., chacun, la somme de 400 euros au titre de l'astreinte.<br/>
              1) Par une requête, enregistrée le 15 février 2019 sous le n° 428031 au secrétariat du contentieux du Conseil d'Etat, l'OFII demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-4 du code de justice administrative :<br/>
<br/>
              1°) d'annuler l'ordonnance du 8 février 2019 ;<br/>
<br/>
              2°) de mettre fin à l'injonction prononcée par l'ordonnance du 25 janvier 2019.<br/>
<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence n'est pas remplie dès lors que M. B...et Mme C..., âgés de 23 et 25 ans et sans enfants, ne peuvent être regardés comme étant dans un état de vulnérabilité caractérisant une situation d'urgence particulière ; <br/>
              - le juge des référés du tribunal administratif de Lille a commis une première erreur de droit en fondant sa décision sur le règlement n° 604/2013 du 26 juin 2013 dit " Dublin III " relatif à la procédure de détermination de l'Etat responsable de l'examen d'une demande d'asile alors que, d'une part, rien ne permet à ce stade d'établir que les intéressés relèvent de la procédure Dublin et, d'autre part, que ce même règlement est sans incidence sur l'octroi des conditions matérielles d'accueil ;<br/>
              - le juge des référés du tribunal administratif de Lille a entaché son ordonnance d'une seconde erreur de droit relative à l'interprétation de l'article L. 774-1 du code de l'entrée et du séjour des étrangers et du droit d'asile dès lors qu'il a considéré que le rendez-vous auprès de la structure de premier accueil des demandeurs d'asile (SPADA) valait enregistrement de la demande d'asile ;<br/>
              - le juge des référés du tribunal administratif a méconnu son office en confirmant l'injonction d'orienter les intéressés vers une structure d'hébergement sans prendre en compte l'état de saturation du dispositif national d'accueil à la date de sa décision, alors que le code de l'entrée et du séjour des étrangers et du droit d'asile met à la charge de l'OFII une obligation de moyens.<br/>
              Par un mémoire en défense, enregistré le 22 février 2019 au secrétariat du contentieux du Conseil d'Etat, M. B...et Mme C...concluent au rejet de la requête de l'OFII. Ils soutiennent qu'elle n'est pas fondée.<br/>
<br/>
              Par un mémoire en défense, enregistré le 25 février 2019 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur conclut à l'annulation de l'ordonnance du 8 février 2019.<br/>
<br/>
<br/>
<br/>
              2) Par une requête, enregistrée le 21 février 2019 sous le n° 428294 au secrétariat du contentieux du Conseil d'Etat, l'OFII demande au juge des référés du Conseil d'Etat d'annuler l'ordonnance du 18 février 2019 du juge des référés du tribunal administratif de Lille.<br/>
<br/>
<br/>
<br/>
              Il fait valoir les mêmes moyens que ceux invoqués au soutien de la requête qu'il a présentée sous le n° 428031.<br/>
<br/>
              Par un mémoire en défense, enregistré le 22 février 2019 au secrétariat du contentieux du Conseil d'Etat, M. B...et Mme C...concluent au rejet de la requête de l'OFII. Ils soutiennent qu'elle n'est pas fondée. <br/>
<br/>
              Par un mémoire en défense, enregistré le 25 février 2019 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur conclut à l'annulation de l'ordonnance du 18 février 2019.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le règlement UE n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - la directive n° 2013/33/UE du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, le directeur de l'Office français de l'immigration et de l'intégration et le ministre de l'intérieur, d'autre part, M. B... et MmeC... ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 26 février 2019 à 15 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - la représentante de l'Office français de l'immigration et de l'intégration ;<br/>
<br/>
              - Me Viaud, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. B... et MmeC... ;<br/>
<br/>
              - les représentantes du ministre de l'intérieur ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
              1. Les requêtes n° 428031 et 428294, présentées par l'Office français de l'immigration et de l'intégration, sont dirigées contre des ordonnances du juge des référés du tribunal administratif de Lille relatives à une même procédure. Par suite, il y a lieu de les joindre pour qu'il soit statué par une seule ordonnance.<br/>
<br/>
              2. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". Aux termes de l'article L. 521-4 du même code : " Saisi par toute personne intéressée, le juge des référés peut, à tout moment, au vu d'un élément nouveau, modifier les mesures qu'il avait ordonnées ou y mettre fin ".<br/>
<br/>
              3. Il résulte de l'instruction que M.B..., ressortissant sierra-léonais né en octobre 1996, et MmeC..., ressortissante nigériane née en novembre 1994, sont entrés en France le 11 janvier 2019. Ils se sont présentés à la structure de premier accueil des demandeurs d'asile de Lille le 16 janvier 2019. La structure de premier accueil des demandeurs d'asile les a convoqués le 23 janvier 2019 afin de pré-enregistrer leur demande d'asile. Le 21 janvier 2019, M. B...et Mme C...ont demandé au juge des référés du tribunal administratif de Lille, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, à titre principal, d'enjoindre au préfet du Nord de les orienter vers une structure d'hébergement d'urgence et, à titre subsidiaire, au directeur de l'OFII de les orienter vers une structure d'hébergement pour demandeurs d'asile. Par une ordonnance du 25 janvier 2019, le juge des référés du tribunal administratif de Lille a enjoint à l'Office français de l'immigration et de l'intégration de les orienter vers une structure d'hébergement pour demandeurs d'asile dans un délai de 96 heures. Saisi par l'OFII sur le fondement de l'article L. 521-4 du code de justice administrative d'une demande tendant à ce qu'il mette fin à cette injonction, le juge des référés du tribunal administratif de Lille a, par une ordonnance du 8 février 2019, rejeté cette demande et assorti l'injonction ainsi confirmée d'une astreinte de 200 euros par jour de retard à l'expiration d'un délai de 72 heures. Par une nouvelle ordonnance du 18 février 2019, le juge des référés du tribunal administratif de Lille a condamné l'Office français de l'immigration et de l'intégration à verser à M. B...et Mme C...chacun la somme de 400 euros au titre de l'astreinte. L'OFII relève appel de ces deux dernières ordonnances.<br/>
<br/>
              4. Pour demander qu'il soit mis fin à l'injonction d'orienter M. B...et Mme C... vers une structure d'hébergement d'urgence prononcée par l'ordonnance du 25 janvier 2019 du juge des référés du tribunal administratif de Lille, l'OFII invoque le fait qu'à la date de cette ordonnance, il n'était pas possible que les demandes d'asile de M. B...et Mme C... aient été enregistrées dès lors que le rendez-vous qui leur avait été fixé le 23 janvier 2019 se tenait auprès de la structure de premier accueil des demandeurs d'asile (SPADA) de Lille qui n'est pas compétent pour enregistrer les demandes d'asile ni délivrer les attestations d'enregistrement. L'OFII produit à l'appui de sa demande la convocation des deux intéressés pour se présenter le 21 février 2019 au guichet unique des demandeurs d'asile en vue de cet enregistrement. Si M. B...et Mme C...soutiennent pour la première fois en appel que cette circonstance et le document qui en atteste ne constituent pas un élément nouveau, il ressort des termes de l'ordonnance du 25 janvier 2019 que le juge des référés du tribunal administratif de Lille s'est notamment fondé sur la circonstance " qu'il n'était pas contesté que la demande d'asile de chacun d'eux avait été enregistrée auprès des autorités compétentes " pour prononcer l'injonction. Dans ces conditions, cette convocation, dont aucune pièce des dossiers 1900577 et 1900578 ne fait apparaître qu'elle avait été portée à la connaissance du juge des référés du tribunal administratif de Lille, constitue un élément nouveau au sens de l'article<br/>
L. 521-4 du code de justice administrative.  <br/>
<br/>
              5. Si la privation du bénéfice des mesures prévues par la loi afin de garantir aux demandeurs d'asile des conditions matérielles d'accueil décentes, jusqu'à ce qu'il ait été statué sur leur demande, est susceptible de constituer une atteinte grave et manifestement illégale à la liberté fondamentale que constitue le droit d'asile, le caractère grave et manifestement illégal d'une telle atteinte s'apprécie en tenant compte des moyens dont dispose l'autorité administrative compétente et de la situation du demandeur. Ainsi, le juge des référés, qui apprécie si les conditions prévues par l'article L. 521-2 du code de justice administrative sont remplies à la date à laquelle il se prononce, ne peut faire usage des pouvoirs qu'il tient de cet article en adressant une injonction à l'administration que dans le cas où, d'une part, le comportement de celle-ci fait apparaître une méconnaissance manifeste des exigences qui découlent du droit d'asile et où, d'autre part, il résulte de ce comportement des conséquences graves pour le demandeur d'asile, compte tenu notamment de son âge, de son état de santé ou de sa situation de famille.<br/>
<br/>
              6. Or, il ressort des documents produits notamment par l'OFII dans le cadre de la présente procédure et il n'est d'ailleurs pas contesté que, contrairement à ce que le juge des référés du tribunal administratif de Lille a estimé dans le cadre de l'ordonnance du 25 janvier 2019, les demandes de protection internationale de M. B...et Mme C...n'ont été enregistrées que le 21 février 2019 et non dès le 23 janvier 2019. En outre, il résulte de l'instruction qu'à ce jour, le dispositif d'hébergement des demandeurs d'asile est saturé, notamment dans le Nord où 70 couples sans enfants sont en attente d'être hébergés, malgré les efforts déployés pour remédier à cette situation. Enfin, les pièces du dossier ne font pas apparaître que M. B...et sa compagne MmeC..., âgés de 23 et 25 ans, sans enfant, se trouvent dans une situation de vulnérabilité particulière qui aurait justifié à la date de l'ordonnance du 25 janvier 2019 ou justifierait à la date de la présente ordonnance de les orienter de façon prioritaire vers un hébergement d'urgence.  <br/>
<br/>
              7. Il résulte de tout ce qui précède que l'OFII est fondé à soutenir que c'est à tort que, par l'ordonnance attaquée n° 1900904 du 8 février 2019, le juge des référés du tribunal administratif de Lille a refusé de prendre en compte l'élément nouveau invoqué pour mettre fin à l'injonction d'orienter M. B...et Mme C...vers une structure d'hébergement d'urgence. Par suite, il y a lieu d'annuler cette ordonnance et de mettre fin, à compter du 8 février 2019, à l'injonction prononcée par l'ordonnance n° 1900577, 1900578 du 25 janvier 2019 du juge des référés de ce même tribunal. Il y a également lieu d'annuler, par voie de conséquence, l'ordonnance n° 1901329 du 21 février 2019 qui condamne l'OFII à payer une astreinte pour n'avoir pas exécuté cette injonction. <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : Les ordonnances du juge des référés du tribunal administratif de Lille n° 1900904 du 8 février 2019 et n° 1901329 du 18 février 2019 sont annulées. <br/>
Article 2 : Il est mis fin aux effets de l'ordonnance n° 1900577, 1900578 du 25 janvier 2019 du juge des référés du tribunal administratif de Lille en tant qu'elle a enjoint à l'Office français de l'immigration et de l'intégration d'orienter M. B...et Mme C...vers une structure d'hébergement pour demandeurs d'asile, dans le délai de 96 heures à compter de la notification de l'ordonnance. <br/>
Article 3 : La présente ordonnance sera notifiée à l'Office français de l'immigration et de l'intégration, à M. A...B...et Mme D...C...et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
