<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029562762</ID>
<ANCIEN_ID>JG_L_2014_10_000000356878</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/56/27/CETATEXT000029562762.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème / 10ème SSR, 10/10/2014, 356878, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-10-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356878</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème / 10ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:356878.20141010</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 17 février et 14 mai 2012 au secrétariat du contentieux du Conseil d'État, présentés pour la caisse régionale de crédit agricole mutuel du Finistère, dont le siège est 7, route du Loch à Quimper (29555) ; la requérante demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10VE01186 du 5 décembre 2011 par lequel la cour administrative d'appel de Versailles a rejeté sa requête tendant à l'annulation du jugement n° 0808700 du 28 janvier 2010 du tribunal administratif de Montreuil rejetant sa demande tendant à la réduction des cotisations d'impôt sur les sociétés auxquelles elle a été assujettie au titre des exercices clos en 2004, 2005 et 2006 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention fiscale franco-allemande du 21 juillet 1959 ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bastien Lignereux, auditeur,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la Caisse régionale de crédit agricole Mutuel du Finistère ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'en vertu de l'article 9 de la convention fiscale franco-allemande : " 1. Les dividendes payés par une société qui est un résident d'un État contractant à un résident de l'autre État contractant sont imposables dans cet autre État. / 2. Chacun des États contractants conserve le droit de percevoir l'impôt sur les dividendes par voie de retenue à la source, conformément à sa législation. Toutefois, ce prélèvement ne peut excéder 15 p. cent du montant brut des dividendes. / (...) 6. Le terme " dividendes " employé dans le présent article désigne les revenus provenant d'actions, actions ou droits de jouissance, parts de mine, parts de fondateur ou autres parts bénéficiaires à l'exception des créances. Nonobstant toute autre disposition de la présente convention, sont également considérés comme dividendes aux fins des paragraphes 2 à 5 : / a) Les revenus soumis au régime des distributions par la législation fiscale de l'État contractant dont la société distributrice est un résident ; et / b) En République fédérale : /- les revenus qu'un " stiller Gesellschafter " tire de sa participation comme tel ; /- les revenus provenant de " partiarische Darlehen ", de " Gewinnobligationen " et les rémunérations similaires liées aux bénéfices ainsi que les distributions afférentes à des parts dans un fonds d'investissement. / (...) 9. Les revenus visés au paragraphe 6, provenant de droits ou parts bénéficiaires participant aux bénéfices (y compris les actions ou droits de jouissance et, dans le cas de la République fédérale d'Allemagne, les revenus qu'un " stiller Gesellschafter " tire de sa participation comme tel, ou d'un " partiarisches Darlehen " et de " Gewinnobligationen ") qui sont déductibles pour la détermination des bénéfices du débiteur sont imposables dans l'État contractant d'où ils proviennent, selon la législation de cet État. " ; qu'aux termes du paragraphe 2 de l'article 20 de la même convention : " En ce qui concerne les résidents de France, la double imposition est évitée de la façon suivante : / a) Les bénéfices et autres revenus positifs qui proviennent de la République fédérale et qui y sont imposables conformément aux dispositions de la présente convention sont également imposables en France lorsqu'ils reviennent à un résident de France. L'impôt allemand n'est pas déductible pour le calcul du revenu imposable en France. Mais le bénéficiaire a droit à un crédit d'impôt imputable sur l'impôt français dans la base duquel ces revenus sont compris. Ce crédit d'impôt est égal à : / aa) Pour les revenus visés à l'article 9, paragraphe 2, à un montant égal au montant de l'impôt payé en République fédérale, conformément aux dispositions de ce paragraphe. L'excédent éventuel est remboursé au contribuable selon les modalités prévues par la législation française en matière d'avoir fiscal ; / bb) Pour les revenus visés à l'article 9, paragraphes 5 et 9, à l'article 11, paragraphe 2, et à l'article 13, paragraphe 6, au montant de l'impôt payé en République fédérale, conformément aux dispositions de ces articles. Il ne peut toutefois excéder le montant de l'impôt français correspondant à ces revenus ; (...) " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la caisse régionale du crédit agricole mutuel du Finistère a perçu au cours des années 2004 à 2006 des produits de titres, dénommés " Genussscheine " par leur contrat d'émission, émis par la Landesbank Sachsen ; qu'en vertu de ce contrat, ces revenus correspondent à un montant annuel de 6,6 % de la valeur nominale des titres, sauf dans le cas et dans la mesure où leur versement se serait traduit dans les comptes du débiteur par la constatation ou l'aggravation d'une perte, ainsi que dans le cas où, à la suite d'une diminution du capital des titres résultant de pertes du débiteur, le capital ne serait pas encore reconstitué à sa valeur nominale globale ; que ce document mentionnait également que les revenus des " Genussscheine " étaient déductibles des résultats de leur émetteur ; que ces revenus ont été soumis, en application de la législation fiscale allemande, à la retenue à la source au titre de l'impôt sur les sociétés et de l'impôt de solidarité applicables dans cet État ; que l'administration fiscale a refusé l'imputation de ces retenues à la source sur les cotisations d'impôt sur les sociétés auxquelles la requérante a été assujettie en France au titre des exercices clos en 2004, 2005 et 2006 ; que la société requérante se pourvoit en cassation contre l'arrêt du 5 décembre 2011 par lequel la cour administrative d'appel de Versailles a rejeté sa requête tendant à l'annulation du jugement du 28 janvier 2010 du tribunal administratif de Montreuil rejetant sa demande tendant à la réduction de ces cotisations ;<br/>
<br/>
              3. Considérant que la cour a jugé qu'au regard des stipulations précitées du paragraphe 6 de l'article 9 de la convention fiscale franco-allemande, les revenus perçus par la société requérante et tirés de " Genussscheine " émis par la Landesbank Sachsen selon les conditions définies par cette banque le 25 juin 2001 ne constituaient pas des dividendes, dès lors que ces titres devaient être regardés comme des titres de créance au vu du document établissant ces conditions ; qu'en statuant ainsi, alors qu'il n'était pas contesté que les revenus litigieux provenaient de " Genussscheine " au sens de la législation allemande et que les revenus tirés de " Genussscheine " sont expressément mentionnés comme constituant des dividendes aux paragraphes 6 et 9 de l'article 9 de la convention précitée dans sa version allemande, dont la convention précise qu'elle fait également foi, la cour a donné aux faits ainsi énoncés une qualification juridique erronée au regard des mêmes stipulations ; que par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'État la somme de 3 000 euros à verser à la caisse régionale de crédit agricole mutuel du Finistère au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : L'arrêt du 5 décembre 2011 de la cour administrative d'appel de Versailles est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : L'État versera une somme de 3 000 euros à la caisse régionale de crédit agricole mutuel du Finistère au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la caisse régionale de crédit agricole mutuel du Finistère et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
