<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037824159</ID>
<ANCIEN_ID>JG_L_2018_12_000000422282</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/82/41/CETATEXT000037824159.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 17/12/2018, 422282, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422282</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX ; SCP LE GRIEL</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:422282.20181217</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association " Comité de défense des auditeurs de Radio Solidarité " (CDARS) a demandé au juge des référés de la cour administrative d'appel de Paris d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de la décision du 25 avril 2018 par laquelle le Conseil supérieur de l'audiovisuel a rejeté le recours administratif préalable obligatoire qu'elle a formé le 18 janvier 2018 contre la décision du comité territorial de l'audiovisuel de Caen du 27 novembre 2017 refusant de déclarer reconductible, hors appel aux candidatures, l'autorisation d'émettre qui lui avait été préalablement accordée pour la zone de Caen, Chartres, Cherbourg, Le Mans et Le Havre. Par une ordonnance n° 18PA01651 du 2 juillet 2018, le juge des référés a fait droit à cette demande.<br/>
<br/>
              Par un pourvoi, enregistré au secrétariat du contentieux du Conseil d'Etat le 16 juillet 2018, le Conseil supérieur de l'audiovisuel (CSA) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de l'association CDARS ;<br/>
<br/>
              3°) de mettre à la charge de l'association CDARS une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la loi n° 86-1067 du 30 septembre 1986 ;<br/>
<br/>
              - le décret n° 2011-732 du 24 juin 2011 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat du Conseil supérieur de l'audiovisuel et à la SCP Le Griel, avocat de l'association Comité de défense des auditeurs de radio Solidarité.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 28-1 de la loi du 30 septembre 1986 relative à la liberté de communication : " I.- La durée des autorisations délivrées en application des articles 29, 29 1, 30, 30-1 et 30-2 ne peut excéder dix ans. Toutefois, pour les services de radio en mode analogique, elle ne peut excéder cinq ans. (...) / Les autorisations délivrées en application des articles 29, 29-1, 30 et 30-1 sont reconduites par le Conseil supérieur de l'audiovisuel, hors appel aux candidatures, dans la limite de deux fois en sus de l'autorisation initiale, et chaque fois pour cinq ans, sauf : (...) / 2° Si une sanction, une astreinte liquidée ou une condamnation dont le titulaire de l'autorisation a fait l'objet sur le fondement de la présente loi, ou une condamnation prononcée à son encontre, sur le fondement des articles 23, 24 et 24 bis de la loi du 29 juillet 1881 sur la liberté de la presse ou des articles 227-23 ou 227-24 du code pénal est de nature à justifier que cette autorisation ne soit pas reconduite hors appel aux candidatures (...) ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis au juge des référés de la cour administrative d'appel de Paris que, par sa décision n° 2008-1109 du 25 novembre 2008,  le Conseil supérieur de l'audiovisuel (CSA) a autorisé l'association " Comité de défense des auditeurs de Radio Solidarité " (CDARS), pour une durée de cinq ans, à exploiter le service radiophonique de catégorie A dénommé " Radio Courtoisie " dans les zones de Caen, Chartres, Cherbourg, Le Mans et Le Havre, situées dans le ressort du comité territorial de l'audiovisuel (CTA) de Caen. Par une décision du 15 avril 2013, le CTA de Caen, faisant application des dispositions citées ci-dessus de l'article 28-1 de la loi du 30 septembre 1986, a reconduit hors appel aux candidatures, pour une durée de cinq ans expirant le 4 décembre 2018, les autorisations délivrées dans ces zones. Par sa décision n° 2017-724 du 4 octobre 2017, le CSA a infligé à l'association CDARS une sanction pécuniaire de 25 000 euros en raison de manquements aux obligations lui incombant en sa qualité d'éditeur du service Radio Courtoisie. Par une décision du 27 novembre 2017, le CTA de Caen a décidé, en raison de cette sanction, de ne pas reconduire à nouveau les autorisations hors appel aux candidatures. Par une décision implicite née le 18 mars 2018, à laquelle il a ensuite substitué une décision expresse délibérée le 25 avril 2018, le CSA a rejeté le recours administratif préalable obligatoire dont l'association l'avait saisi, en application de l'article 20 du décret du 24 juin 2011 visé ci-dessus, contre la décision du CTA de Caen du 27 novembre 2017. L'association a présenté devant la cour administrative d'appel de Paris un recours tendant à l'annulation pour excès de pouvoir de cette décision et une demande en référé tendant à ce que son exécution soit suspendue dans l'attente du jugement de ce recours. Par l'ordonnance attaquée du 2 juillet 2018, le juge des référés de la cour administrative d'appel a fait droit à cette demande.<br/>
<br/>
              3. Aux termes de l'article R. 311-1 du code de justice administrative : " Le Conseil d'Etat est compétent pour connaître en premier et dernier ressort : (...) /  4° Des recours dirigés contre les décisions prises par les organes des autorités suivantes, au titre de leur mission de contrôle ou de régulation : (...) le Conseil supérieur de l'audiovisuel, sous réserve des dispositions de l'article R. 311-2 ". Aux termes de l'article R. 311-2 du même code :  " La cour administrative d'appel de Paris est compétente pour connaître en premier et dernier ressort : (...) / 2° Des litiges relatifs aux décisions prises par le Conseil supérieur de l'audiovisuel en application des articles 28-1, 28-3 et 29 à 30-7 de la loi n° 86-1067 du 30 septembre 1986 relative à la liberté de communication, à l'exception de celles concernant les services de télévision à vocation nationale ". Il résulte de ces dispositions, d'une part, que des conclusions dirigées contre une sanction prononcée par le CSA contre l'éditeur d'un service de radio, sur le fondement de l'article 42-1 de la loi du 30 septembre 1986 ou de la convention définissant les obligations particulières de ce service conclue en application de l'article 28 de la loi, relèvent de la compétence de premier et dernier ressort du Conseil d'Etat et, d'autre part, que des conclusions dirigées contre la décision par laquelle le CSA statue, en application des dispositions de l'article 28-1 de la même loi, sur la reconduction hors appel aux candidatures de l'autorisation de ce service relèvent de la compétence en premier et dernier ressort de la cour administrative d'appel de Paris. <br/>
<br/>
              4. Toutefois, aux termes du premier alinéa de l'article R. 341-2 du même code : " Dans le cas où un tribunal administratif ou une cour administrative d'appel est saisi de conclusions relevant normalement de sa compétence mais connexes à des conclusions présentées devant le Conseil d'Etat et relevant de la compétence en premier et dernier ressort de celui-ci, son président renvoie au Conseil d'Etat lesdites conclusions ". Au sens de ces dispositions, il existe un lien de connexité entre des conclusions dirigées contre une sanction prononcée par le CSA et des conclusions dirigées contre la décision refusant, en raison de cette sanction, le renouvellement, hors appel aux candidatures, d'une autorisation. <br/>
<br/>
              5. Il ressort des pièces du dossier que, par une requête enregistrée au secrétariat du Conseil d'Etat le 6 décembre 2017 sous le n° 416631, l'association CDARS a demandé l'annulation de la décision du 4 octobre 2017 par laquelle le CSA lui a infligé une sanction. Le CSA s'étant fondé sur cette sanction pour refuser, par sa décision du 25 avril 2018, la reconduction, hors appel aux candidatures, des autorisations délivrées à l'association, les conclusions dirigées contre ce refus, présentées devant la cour administrative d'appel de Paris, sont connexes à celles de la requête n° 416631 et relèvent également de la compétence du Conseil d'Etat en application des dispositions citées au point 4. Il suit de là qu'en retenant qu'il n'existait pas de lien de connexité entre la contestation de la sanction portée devant le Conseil d'Etat et celle de la décision de non renouvellement des autorisation portée devant la cour administrative d'appel et en s'estimant, par suite, compétent pour statuer sur la demande de suspension dont il était saisi, le juge des référés de la cour administrative d'appel a commis une erreur de droit qui justifie, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'annulation de son ordonnance du 2 juillet 2018.<br/>
<br/>
              6. Il résulte de ce qui précède qu'il y a lieu pour le Conseil d'Etat de statuer sur la demande tendant à la suspension de l'exécution de la décision du 25 avril 2018.<br/>
<br/>
              7. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              8. L'exécution de la décision du 25 avril 2018 aurait pour effet de priver l'association CDARS du bénéfice des autorisations d'émettre dont elle est titulaire dans les zones considérées, dont la validité expire le 4 décembre 2018. Dans ces conditions, la condition d'urgence prévue à l'article L. 521-1 du code de justice administrative doit être regardée comme remplie.<br/>
              9. Le moyen tiré de ce que le CSA a commis une erreur d'appréciation en estimant que, malgré la révocation avec effet immédiat, le 1er juillet 2017, du président de l'association CDARS, animateur de l'émission au cours de laquelle avaient été tenus les propos sanctionnés, dont il était pour partie l'auteur, et la circonstance que ce dernier n'exerce plus désormais aucune responsabilité dans le cadre de l'exploitation du service Radio Courtoisie, la sanction prononcée justifiait que l'autorisation ne soit pas reconduite hors appel aux candidatures est de nature, en l'état de l'instruction, à créer un doute sérieux quant à la légalité de la décision attaquée. <br/>
<br/>
              10. Il résulte de ce qui précède que l'association CDARS est fondée à demander que l'exécution de la décision attaquée soit suspendue jusqu'à ce qu'il soit statué au fond sur le recours tendant à son annulation.<br/>
<br/>
              11. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du CSA une somme de 3 000 euros à verser à l'association CDARS au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions font, en revanche, obstacle à ce que la somme demandée par le CSA soit mise à la charge de l'association, qui n'est pas la partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés de la cour administrative d'appel de Paris du 2 juillet 2018 est annulée.<br/>
<br/>
		Article 2 : L'exécution de la décision du CSA du 25 avril 2018 est suspendue.<br/>
<br/>
Article 3 : Le CSA versera à l'association CDARS une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées par le CSA au titre de l'article L.761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée au Conseil supérieur de l'audiovisuel et à l'association Comité de défense des auditeurs de Radio Solidarité.<br/>
		Copie pour information en sera adressée au ministre de la culture.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
