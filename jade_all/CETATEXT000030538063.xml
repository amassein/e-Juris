<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030538063</ID>
<ANCIEN_ID>JG_L_2015_04_000000368808</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/53/80/CETATEXT000030538063.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 29/04/2015, 368808, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-04-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368808</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:368808.20150429</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme A...B...ont demandé au tribunal administratif de Paris de prononcer la restitution à leur profit d'une somme complémentaire de 16 962 euros au titre du plafonnement à 50 % des impôts directs auxquels ils ont été assujettis au titre de l'année 2007. Par un jugement n° 1012850 du 6 février 2012, le tribunal administratif de Paris a rejeté cette demande. <br/>
<br/>
              Par un arrêt n° 12PA01564 du 22 mars 2013, sur l'appel formé par M. et Mme B... contre ce jugement, la cour administrative d'appel de Paris a annulé le jugement du tribunal administratif de Paris et accordé à M. et Mme B...la restitution d'une somme de 16 962 euros au titre du plafonnement à 50 % de leurs revenus des impôts directs auxquels ils ont été assujettis au titre de l'année 2007. <br/>
<br/>
              Par un pourvoi, enregistré le 24 mai 2013 au secrétariat du contentieux du Conseil d'Etat, le ministre délégué, chargé du budget, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 1er à 3 de l'arrêt n° 12PA01564 du 22 mars 2013 de la cour administrative d'appel de Paris ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. et Mme B....<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat de M. et Mme A...B...;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, après avoir admis que M. et Mme B...avait droit à la restitution d'une somme de 123 876 euros au titre du plafonnement à 50 % de leurs revenus des impôts directs auxquels ils avaient été assujettis au titre de l'année 2007 prévu par les dispositions des articles 1er et 1649-0 A du code général des impôts, l'administration a rejeté comme tardivement présentée une demande complémentaire, en date du 4 février 2010, tendant à la restitution au même titre d'une somme totale portée à 140 838 euros. M. et MmeB..., après avoir saisi le tribunal administratif de Paris de cette demande de restitution de cette somme, ont interjeté appel du jugement du 6 février 2012 par lequel ce tribunal l'a rejetée. Le ministre délégué, chargé du budget se pourvoit en cassation contre l'arrêt du 22 mars 2013 de la cour administrative d'appel de Paris en tant qu'il a, après avoir annulé le jugement du tribunal administratif, accordé à M. et Mme B...la restitution de la somme complémentaire de 16 962 euros au titre du plafonnement de leurs impôts directs à 50 % de leurs revenus de l'année 2007.<br/>
<br/>
              2. Aux termes de l'article L. 190 du livre des procédures fiscales, dans sa rédaction applicable au litige : " Les réclamations relatives aux impôts, contributions, droits, taxes, redevances, soultes et pénalités de toute nature, établis ou recouvrés par les agents de l'administration, relèvent de la juridiction contentieuse lorsqu'elles tendent à obtenir (...) le bénéfice d'un droit résultant d'une disposition législative ou réglementaire. / (...) / Sont instruites et jugées selon les règles du présent chapitre toutes actions tendant à (...) la réduction d'une imposition (...) fondées sur la non-conformité de la règle de droit dont il a été fait application à une règle de droit supérieure. / Lorsque cette non-conformité a été révélée par une décision juridictionnelle (...), l'action en restitution des sommes versées (...) ne peut porter que sur la période postérieure au 1er janvier de la troisième année précédant celle où la décision (...) révélant la non-conformité est intervenu[e]. / Pour l'application du quatrième alinéa, sont considérés comme des décisions juridictionnelles (...) les décisions du Conseil d'Etat (...) ". Aux termes de l'article R. 196-1 du même livre : " Pour être recevables, les réclamations relatives aux impôts autres que les impôts directs locaux et les taxes annexes à ces impôts, doivent être présentées à l'administration au plus tard le 31 décembre de la deuxième année suivant celle, selon le cas : / (...) / c) De la réalisation de l'événement qui motive la réclamation / (...) ". <br/>
<br/>
              3. Une décision du Conseil d'Etat statuant au contentieux qui révèle l'illégalité d'une instruction fiscale ne révèle pas la non-conformité d'une règle de droit dont il a été fait application à une règle de droit supérieure dès lors que l'imposition ne saurait être fondée sur l'interprétation de la loi fiscale que l'administration exprime dans ses instructions. Ainsi, en jugeant que la décision n° 321416 du 13 janvier 2010 par laquelle le Conseil d'Etat statuant au contentieux a annulé les alinéas 2 à 5 du paragraphe 34 ainsi que le paragraphe 38 de l'instruction 13 A-I-08 était de nature à constituer le point de départ du délai dans lequel sont recevables les réclamations motivées par la réalisation d'un événement, au sens et pour l'application du c de l'article R. 196-1 du livre des procédures fiscales, portant sur la période sur laquelle l'action en restitution peut s'exercer en application de l'article L. 190 du même livre, la cour a commis une erreur de droit.<br/>
<br/>
              4. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, le ministre requérant est fondé à demander l'annulation des articles 1, 2 et 3 de l'arrêt qu'il attaque. <br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              6. Aux termes de l'article 1er du code général des impôts, alors applicable : " Les impôts directs payés par un contribuable ne peuvent être supérieurs à 50% de ses revenus. / Les conditions d'application de ce droit sont définies à l'article 1649-0 A. ". Aux termes de l'article 1649-0 A, alors applicable : " 1. Le droit à restitution de la fraction des impositions qui excède le seuil mentionné à l'article 1er est acquis par le contribuable au 1er janvier de la deuxième année suivant celle de la réalisation des revenus mentionnés au 4. / (...) 4. Le revenu à prendre en compte pour la détermination du droit à restitution s'entend de celui réalisé par le contribuable, à l'exception des revenus en nature non soumis à l'impôt sur le revenu en application du II de l'article 15. (...) / 8. Les demandes de restitution doivent être déposées avant le 31 décembre de la deuxième année suivant celle de la réalisation des revenus mentionnés au 4 (...) ". <br/>
<br/>
              7. Une demande de restitution de la fraction des impositions qui excède 50 % des revenus constitue une réclamation tendant à obtenir le bénéfice d'un droit au sens de l'article L. 190 du livre des procédures fiscales. Le délai particulier prévu par le 8 de l'article 1649-0 A du code général des impôts pour formuler une demande de restitution ne fait pas obstacle à ce que soit invoquée, après l'expiration de ce délai, la réalisation d'un événement qui motive la réclamation, au sens de l'article R. 196-1 du livre des procédures fiscales. <br/>
<br/>
              8. Il ressort des pièces du dossier que les demandes de restitution complémentaire de M. et Mme B...ont été présentées le 4 février 2010, après l'expiration, le 31 décembre de la deuxième année suivant celle de la réalisation des revenus en cause, du délai prévu par le 8 de l'article 1649-0 A du code général des impôts. Il résulte de ce qui a été dit au point 3 que la décision n° 321416 du 13 janvier 2010 du Conseil d'Etat statuant au contentieux n'a pu constituer la réalisation d'un événement ouvrant un nouveau délai de réclamation. Leurs demandes ayant été présentées tardivement à l'administration fiscale, M. et Mme B...ne sont pas recevables à réclamer une restitution complémentaire au titre du plafonnement de leurs impôts directs à 50 % de leurs revenus de l'année 2007. <br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1, 2 et 3 de l'arrêt de la cour administrative d'appel de Paris du 22 mars 2013 sont annulés.<br/>
Article 2 : Les conclusions présentées par M. et MmeB...  tendant à la restitution d'une somme complémentaire de 16 962 euros au titre du plafonnement de leurs impôts directs à 50 % de leurs revenus de l'année 2007 et au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée au ministre des finances et des comptes publics et à M. et Mme A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
