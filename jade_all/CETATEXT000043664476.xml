<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043664476</ID>
<ANCIEN_ID>JG_L_2021_06_000000429536</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/66/44/CETATEXT000043664476.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 14/06/2021, 429536, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429536</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Catherine Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:429536.20210614</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Par une requête, enregistrée le 7 avril 2019 au secrétariat du contentieux du Conseil d'Etat, M. B... C... demande au Conseil d'Etat d'annuler pour excès de pouvoir la décision implicite par laquelle la garde des sceaux, ministre de la justice, a refusé de faire droit à sa demande tendant à l'abrogation des articles 8 et 9 du décret n° 71-942 du 26 novembre 1971 relatif aux créations, transferts et suppressions d'office de notaire, à la compétence d'instrumentation et à la résidence des notaires, à la garde et à la transmission des minutes et registres professionnels des notaires.<br/>
<br/>
<br/>
<br/>
     	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - l'ordonnance n° 45-2590 du 2 novembre 1945 ;<br/>
              - le décret n° 71-942 du 26 novembre 1971 ;<br/>
              - le décret n° 91-152 du 7 février 1991 ;<br/>
              - l'arrêté du 6 décembre 2004 relatif à l'exercice des attributions notariales des agents diplomatiques et consulaires ;<br/>
              - l'arrêté du 18 décembre 2017 fixant la liste des postes diplomatiques et consulaires dans lesquels sont exercées des attributions notariales ;<br/>
              - l'arrêté du 28 septembre 2018 portant abrogation de l'arrêté du 18 décembre 2017 fixant la liste des postes diplomatiques et consulaires dans lesquels sont exercées des attributions notariales ;<br/>
              - l'arrêté du 17 décembre 2018 fixant la liste des postes diplomatiques et consulaires dans lesquels sont exercées des attributions notariales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme D... A..., conseillère d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. M. C..., qui exerce la profession de notaire à Nancy, a demandé à la garde des sceaux, ministre de la justice, d'abroger les articles 8 et 9 du décret du 26 novembre 1971 relatif aux créations, transferts et suppressions d'office de notaire, à la compétence d'instrumentation et à la résidence des notaires, à la garde et à la transmission des minutes et registres professionnels des notaires. Il demande au Conseil d'Etat d'annuler pour excès de pouvoir la décision de refus née du silence gardée sur cette demande.<br/>
<br/>
              2. Aux termes de l'article 8 du décret du 26 novembre 1971 relatif aux créations, transferts et suppressions d'office de notaire, à la compétence d'instrumentation et à la résidence des notaires, à la garde et à la transmission des minutes et registres professionnels des notaires : " Les notaires exercent leurs fonctions sur l'ensemble du territoire national, à l'exclusion de la Nouvelle-Calédonie, de la Polynésie française et des îles Wallis et Futuna ". Aux termes de l'article 9 de ce même décret : " Tout acte reçu en dehors du territoire où les notaires sont autorisés à instrumenter est nul s'il n'est pas revêtu de la signature de toutes les parties. Lorsque l'acte est revêtu de la signature de toutes les parties contractantes, il ne vaut que comme écrit sous signature privée ".<br/>
<br/>
              3. Aux termes de l'article 1er du décret du 7 février 1991 relatif aux attributions notariales des agents diplomatiques et consulaires : " Le ministre des affaires étrangères désigne par arrêté les postes diplomatiques et consulaires dans lesquels sont exercées des attributions notariales. Dans ces postes peuvent seuls exercer ces attributions : / 1° Le chef de mission diplomatique pourvue d'une circonscription consulaire ; / 2° Le chef de poste consulaire ; / 3° Le chef de section consulaire auprès du poste diplomatique ; / 4° Le chef de chancellerie auprès du poste consulaire ; / 5° Le gérant d'un poste diplomatique ou consulaire sous réserve qu'il soit de catégorie A ou B ". L'article 2 de ce décret dispose que " Les agents mentionnés à l'article 1er ont qualité pour instrumenter à l'égard de tous les Français, dans la limite de leur circonscription consulaire sauf force majeure. (...) ". En vertu des arrêtés du 6 décembre 2004, du 18 décembre 2017, du 28 septembre 2018 et du 17 décembre 2018 pris en application de ces dispositions, seuls, depuis le 1er janvier 2019, les consulats généraux de France à Abidjan et à Dakar exercent des attributions notariales. <br/>
<br/>
              4. Si les Français établis hors de France n'ont, pour la plupart, à l'exception de ceux établis dans la circonscription consulaire des consulats d'Abidjan et de Dakar, pas accès auprès des postes diplomatiques et consulaires aux services d'un officier public compétent pour conférer un caractère d'authenticité à leurs actes et contrats, la différence de traitement qui en découle par rapport aux Français qui résident sur le territoire national, qui résulte au demeurant non du décret contesté mais des arrêtés pris par le ministre des affaires étrangères en application de l'article 1er du décret du 7 février 1991, ne méconnaît, en tout état de cause, pas le principe d'égalité, et en particulier l'égal accès au service public, dès lors qu'elle concerne des situations différentes et que cette différence est en rapport direct avec l'objet de la norme, qui est de garantir les conditions d'établissement et de conservation des actes authentiques.<br/>
<br/>
              5. Il résulte de ce qui précède que le requérant n'est pas fondé à demander l'annulation de la décision implicite de rejet qu'il attaque. Par suite, sa requête doit être rejetée.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. C... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. B... C... et au garde des sceaux, ministre de la justice.<br/>
Copie en sera adressée au Premier ministre. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
