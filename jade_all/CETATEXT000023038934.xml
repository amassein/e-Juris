<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023038934</ID>
<ANCIEN_ID>JG_L_2010_11_000000327062</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/03/89/CETATEXT000023038934.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 10/11/2010, 327062</TITRE>
<DATE_DEC>2010-11-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>327062</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Stéphanie  Gargoullaud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Lenica Frédéric</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2010:327062.20101110</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°/, sous le n° 327062, la requête sommaire et le mémoire complémentaire, enregistrés les 14 avril et 13 juillet 2009 au secrétariat du contentieux du Conseil d'Etat, présentés par la FEDERATION FRANCAISE DES TELECOMMUNICATIONS ET DES COMMUNICATIONS ELECTRONIQUES (FFTCE), dont le siège est 11-17 rue Hamelin à Paris (75783 Cedex 16) ; la FEDERATION FRANCAISE DES TELECOMMUNICATIONS ET DES COMMUNICATIONS ELECTRONIQUES demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le décret n° 2009-167 du 12 février 2009 relatif à la communication d'informations à l'Etat et aux collectivités territoriales sur les infrastructures et réseaux établis sur leur territoire ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 4 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu 2°/, sous le n° 330408, la requête, enregistrée le 3 août 2009 au secrétariat du contentieux du Conseil d'Etat, présentée pour le SYNDICAT INTERCOMMUNAL DE LA PERIPHERIE DE PARIS POUR L'ELECTRICITE ET LES RESEAUX DE COMMUNICATION (SIPPEREC), dont le siège est Tour Gamma B 193-197 rue de Bercy à Paris (75582 Cedex 12) ; le SYNDICAT INTERCOMMUNAL DE LA PERIPHERIE DE PARIS POUR L'ELECTRICITE ET LES RESEAUX DE COMMUNICATION demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision implicite par laquelle le Premier ministre a rejeté sa demande tendant à l'abrogation ou à la modification de certaines dispositions du décret n° 2009-167 du 12 février 2009 relatif à la communication d'informations à l'Etat et aux collectivités territoriales sur les infrastructures et réseaux établis sur leur territoire ; <br/>
<br/>
              2°) d'annuler le décret n° 2009-167 du 12 février 2009 en tant que :<br/>
<br/>
              a) il impose aux collectivités territoriales et à leurs groupements de communiquer préalablement au préfet de région les demandes d'informations destinées aux gestionnaires d'infrastructures et aux opérateurs de communications électroniques ; <br/>
<br/>
              b) il prévoit que les données communiquées par les gestionnaires d'infrastructures et les opérateurs de communications électroniques sont, dans leur intégralité, considérées comme des données non communicables au public en application de l'article 6 de la loi n° 78-753 du 17 juillet 1978 ;<br/>
<br/>
              c) il permet aux gestionnaires d'infrastructures et aux opérateurs de communications électroniques, lorsque la demande d'informations porte sur l'état d'occupation des infrastructures d'accueil, de communiquer des données incomplètes en laissant aux demandeurs le soin de réaliser, à leurs frais, des relevés complémentaires sur le terrain ; <br/>
<br/>
              d) il fixe au 1er juillet 2011 la date d'entrée en vigueur de l'obligation faite aux gestionnaires d'infrastructures de communiquer les informations relatives aux infrastructures d'accueil ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu le code général des collectivités territoriales ; <br/>
<br/>
              Vu le code des postes et des communications électroniques ;<br/>
<br/>
              Vu la loi n° 78-753 du 17 juillet 1978, notamment son article 6 ;<br/>
<br/>
              Vu la décision du 8 juin 2010 par laquelle le Conseil d'Etat statuant au contentieux n'a pas renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la FEDERATION FRANCAISE DES TELECOMMUNICATIONS ET DES COMMUNICATIONS ELECTRONIQUES à l'encontre de l'article L. 33-7 du code des postes et des communications électroniques ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Stéphanie Gargoullaud, chargée des fonctions de Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Piwnica, Molinié, avocat du SYNDICAT INTERCOMMUNAL DE LA PERIPHERIE DE PARIS POUR L'ELECTRICITE ET LES RESEAUX DE COMMUNICATION,<br/>
<br/>
              - les conclusions de M. Frédéric Lenica, Rapporteur public ;<br/>
<br/>
              La parole ayant à nouveau été donnée à la SCP Piwnica, Molinié, avocat du SYNDICAT INTERCOMMUNAL DE LA PERIPHERIE DE PARIS POUR L'ELECTRICITE ET LES RESEAUX DE COMMUNICATION ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant que les requêtes de la FEDERATION FRANCAISE DES TELECOMMUNICATIONS ET DES COMMUNICATIONS ELECTRONIQUES et du SYNDICAT INTERCOMMUNAL DE LA PERIPHERIE DE PARIS POUR L'ELECTRICITE ET LES RESEAUX DE COMMUNICATION sont dirigées contre le même décret ; qu'il y a lieu de les joindre pour statuer par une seule décision ; <br/>
<br/>
              Considérant qu'aux termes de l'article L. 33-7 du code des postes et des communications électroniques : " Les gestionnaires d'infrastructures de communications électroniques et les opérateurs de communications électroniques communiquent gratuitement à l'Etat, aux collectivités territoriales et à leurs groupements, à leur demande, les informations relatives à l'implantation et au déploiement de leurs infrastructures et de leurs réseaux sur leur territoire. Un décret précise les modalités d'application du présent article, notamment au regard des règles relatives à la sécurité publique et à la sécurité nationale " ; que le décret attaqué, qui fixe les modalités d'application de cet article, introduit à son article 1er un article D. 98-6-3 dans le code des postes et des communications électroniques, et fixe, à son article 2, les modalités d'entrée en vigueur de ces dispositions ; <br/>
<br/>
              Sur la légalité du II de l'article D. 98-6-3 du code des postes et des communications électroniques :<br/>
<br/>
              Considérant qu'aux termes du troisième alinéa du II de l'article D. 98-6-3 du code des postes et des communications électroniques : " Les demandes des collectivités territoriales ou de leurs groupements font l'objet d'une information préalable du préfet de région concerné par le territoire couvert, au moins deux semaines avant leur transmission à l'opérateur. Cette information indique l'objet précis de la demande au regard de la stratégie numérique poursuivie par la collectivité territoriale pour son territoire " ; qu'une telle information du préfet de région, destinée à favoriser l'élaboration concertée de la stratégie d'aménagement numérique du territoire dans la région, n'est assortie d'aucune sanction et ne constitue ni un système d'autorisation, ni une tutelle sur les collectivités territoriales ; qu'ainsi, le moyen tiré de la violation du principe de libre administration des collectivités territoriales doit être écarté ;<br/>
<br/>
              Sur la légalité du III de l'article D. 98-6-3 du code des postes et des communications électroniques :<br/>
<br/>
              Considérant qu'en prévoyant, au III de l'article D. 98-6-3 du code des postes et communications électroniques, que la demande d'information de l'Etat, des collectivités territoriales et de leurs groupements peut notamment porter, en ce qui concerne les infrastructures d'accueil de réseaux de communications électroniques, sur " les artères de génie civil aériennes et souterraines (fourreaux, conduites, galeries, adductions, cheminements en façade, poteaux et cheminements aériens), les locaux, armoires et chambres techniques, les pylônes et autres sites d'émission ", et, en ce qui concerne les équipements passifs de ces réseaux, sur " les câbles de communications électroniques de toute nature, les éléments de branchement et d'interconnexion " et, dans chacune de ces deux hypothèses, sur la " nature " et les " caractéristiques techniques principales " des installations, le décret n'a pas excédé le périmètre des " informations relatives à l'implantation et au déploiement de leurs infrastructures et de leurs réseaux sur leur territoire " prévu par l'article L. 33-7 du code des postes et des communications électroniques ;<br/>
<br/>
              Sur la légalité du IV de l'article D. 98-6-3 du code des postes et des communications électroniques :<br/>
<br/>
              Considérant, en premier lieu, qu'aux termes du premier alinéa du IV de l'article D. 98-6-3 du code des postes et des communications électroniques : " L'Etat, les collectivités territoriales et leurs groupements veillent à la confidentialité des données qui leur sont transmises par les gestionnaires d'infrastructures de communications électroniques et les opérateurs en application du présent article. Sans préjudice des dispositions des troisième, quatrième et cinquième alinéas du présent IV et en application de l'article 6 de la loi n° 78-753 du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public et diverses dispositions d'ordre administratif, social et fiscal, ces données ne sont pas communicables au public. L'Etat, les collectivités territoriales et leurs groupements prennent les mesures nécessaires, compte tenu des techniques disponibles, pour prévenir l'accès aux données par toute personne non autorisée " ;<br/>
<br/>
              Considérant, d'une part, que les données dont la communication " porterait atteinte à la sûreté de l'Etat, à la sécurité publique ou à la sécurité des personnes " ne sont pas communicables en vertu du d) du 2° du I de l'article 6 de la loi du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public et diverses dispositions d'ordre administratif, social et fiscal ; qu'en prévoyant qu'en application de l'article 6, les données communiquées à l'Etat, aux collectivités territoriales et à leurs groupements ne sont pas communicables au public, le premier alinéa du IV de l'article D. 98-6-3, qui reconnaît implicitement le caractère sensible des informations susceptibles, par leur nature, de porter atteinte à la sûreté de l'Etat, à la sécurité publique ou à la sécurité des personnes, n'a pas commis d'illégalité ; <br/>
<br/>
              Considérant, d'autre part, que, compte tenu des modalités de protection qui sont prévues, le moyen tiré du défaut de mise en oeuvre d'une protection du secret des affaires ne peut qu'être écarté ;<br/>
<br/>
              Considérant, en deuxième lieu, qu'en prévoyant, au quatrième alinéa du IV du même article, la communication des données reçues en application de l'article L. 33-7 du code des postes et des communications électroniques " entre services de l'Etat, d'une part, et entre collectivités territoriales et groupements, d'autre part, après information des opérateurs et gestionnaires concernés ", dans les mêmes conditions de confidentialité et selon les mêmes exigences de procédure et d'information des opérateurs et gestionnaires concernés que la demande initiale, le pouvoir réglementaire a mis en oeuvre les dispositions de l'article L. 33-7 du code des postes et des communications électroniques selon des modalités conformes à la volonté du législateur ;<br/>
<br/>
              Considérant, en troisième lieu, qu'en permettant, au cinquième alinéa du IV, la communication " à un tiers concourant à l'aménagement du territoire avec lequel ils sont en relation contractuelle après information des opérateurs et des gestionnaires d'infrastructures dont elles proviennent", soit à des personnes qui ne sont pas mentionnées à l'article L. 33-7, le pouvoir réglementaire a excédé la compétence qu'il tenait de ce dernier article ; qu'ainsi, et sans qu'il soit besoin de se prononcer sur la légalité externe de cette disposition, la FEDERATION FRANCAISE DES TELECOMMUNICATIONS ET DES COMMUNICATIONS ELECTRONIQUES est fondée à demander l'annulation de l'article 1er du décret attaqué en tant qu'il introduit le cinquième alinéa du IV de l'article D. 98-6-3 du code des postes et des communications électroniques ; <br/>
<br/>
              Sur la légalité du V de l'article D. 98-6-3 du code des postes et des communications électroniques :<br/>
<br/>
              Considérant qu'en prévoyant au V de cet article que : " Si la demande porte sur l'état d'occupation des infrastructures d'accueil, les opérateurs et gestionnaires d'infrastructures transmettent les données dont ils disposent et indiquent, si ces données ne sont pas complètes, les modalités permettant la réalisation par le demandeur de relevés complémentaires sur le terrain ", le pouvoir réglementaire a rempli l'obligation de transmission des données sans mettre en cause la règle de gratuité prévue par l'article L. 33-7 du même code ;<br/>
<br/>
              Considérant, en revanche, qu'en prévoyant que " les informations devant être communiquées en application du présent article sont transmises sous forme de données numériques vectorielles géolocalisées pouvant être reprises dans des systèmes d'informations géographiques et suivant un format largement répandu ", le dernier alinéa du V de l'article D. 98-6-3 impose aux opérateurs un traitement des informations qui va au-delà des obligations posées par le législateur ; que la FEDERATION FRANCAISE DES TELECOMMUNICATIONS ET DES COMMUNICATIONS ELECTRONIQUES est fondée, dans cette mesure, à demander l'annulation de l'article 1er du décret attaqué ;<br/>
<br/>
              Sur la légalité de l'article 2 du décret attaqué :<br/>
<br/>
              Considérant que l'exercice du pouvoir réglementaire implique pour son détenteur la possibilité de modifier à tout moment les normes qu'il définit sans que les personnes auxquelles sont, le cas échéant, imposées de nouvelles contraintes puissent invoquer un droit au maintien de la réglementation existante ; qu'en principe, les nouvelles normes ainsi édictées ont vocation à s'appliquer immédiatement, dans le respect des exigences attachées au principe de non-rétroactivité des actes administratifs ; que, toutefois, il incombe à l'autorité investie du pouvoir réglementaire, agissant dans les limites de sa compétence et dans le respect des règles qui s'imposent à elle, d'édicter, pour des motifs de sécurité juridique, les mesures transitoires qu'implique, s'il y a lieu, cette réglementation nouvelle ; qu'il en va ainsi lorsque l'application immédiate de celle-ci entraîne, au regard de l'objet et des effets de ses dispositions, une atteinte excessive aux intérêts publics ou privés en cause ;<br/>
<br/>
              Considérant qu'en fixant, aux termes de l'article 2 du décret attaqué, l'entrée en vigueur de l'obligation de communication des informations relatives aux infrastructures d'accueil au 1er juillet 2011, le Gouvernement a pris en considération le principe de sécurité juridique et n'a pas commis d'erreur d'appréciation ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la FEDERATION FRANCAISE DES TELECOMMUNICATIONS ET DES COMMUNICATIONS ELECTRONIQUES et le SYNDICAT INTERCOMMUNAL DE LA PERIPHERIE DE PARIS POUR L'ELECTRICITE ET LES RESEAUX DE COMMUNICATION ne sont fondés à demander l'annulation du décret attaqué qu'en tant que son article 1er introduit le cinquième alinéa du IV et le dernier alinéa du V de l'article D. 98-6-3 du code des postes et des communications électroniques ;<br/>
<br/>
              Sur l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application des ces dispositions et de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la FEDERATION FRANCAISE DES TELECOMMUNICATIONS ET DES COMMUNICATIONS ELECTRONIQUES au titre des frais exposés par elle et non compris dans les dépens ; qu'en revanche, ces mêmes dispositions font obstacle à ce que l'Etat, qui n'est pas la partie perdante dans l'affaire n° 330408, verse au SYNDICAT INTERCOMMUNAL DE LA PERIPHERIE DE PARIS POUR L'ELECTRICITE ET LES RESEAUX DE COMMUNICATION la somme qu'il demande au même titre ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'article 1er du décret du 12 février 2009 est annulé en tant qu'il introduit le cinquième alinéa du IV et le dernier alinéa du V de l'article D. 98-6-3 du code des postes et des communications électroniques.<br/>
<br/>
Article 2 : L'Etat versera à  la FEDERATION FRANCAISE DES TELECOMMUNICATIONS ET DES COMMUNICATIONS ELECTRONIQUES la somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : Le surplus des requêtes de la FEDERATION FRANCAISE DES TELECOMMUNICATIONS ET DES COMMUNICATIONS ELECTRONIQUES et du  SYNDICAT INTERCOMMUNAL DE LA PERIPHERIE DE PARIS POUR L'ELECTRICITE ET LES RESEAUX DE COMMUNICATION est rejeté.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la FEDERATION FRANCAISE DES TELECOMMUNICATIONS ET DES COMMUNICATIONS ELECTRONIQUES, au SYNDICAT INTERCOMMUNAL DE LA PERIPHERIE DE PARIS POUR L'ELECTRICITE ET LES RESEAUX DE COMMUNICATION, à l'Autorité de régulation des communications électroniques et des postes, au Premier ministre et à la ministre de l'économie, de l'industrie et de l'emploi.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-02-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. LOI ET RÈGLEMENT. - COMMUNICATION PAR LES GESTIONNAIRES D'INFRASTRUCTURES DE COMMUNICATIONS ÉLECTRONIQUES ET LES OPÉRATEURS DE COMMUNICATIONS ÉLECTRONIQUES, À L'ETAT, AUX COLLECTIVITÉS TERRITORIALES ET À LEURS GROUPEMENTS, DES INFORMATIONS RELATIVES À L'IMPLANTATION ET AU DÉPLOIEMENT DE LEURS INFRASTRUCTURES ET DE LEURS RÉSEAUX SUR LEUR TERRITOIRE (ART. L. 33-7 DU CODE DE POSTES ET DES COMMUNICATIONS ÉLECTRONIQUES) - MODALITÉS DE TRANSMISSION DES INFORMATIONS - ILLÉGALITÉ, EN RAISON D'UNE OBLIGATION EXCESSIVE DE TRAITEMENT DES INFORMATIONS IMPOSÉE PAR LE POUVOIR RÉGLEMENTAIRE AUX OPÉRATEURS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">135-01 COLLECTIVITÉS TERRITORIALES. DISPOSITIONS GÉNÉRALES. - COMMUNICATION PAR LES GESTIONNAIRES D'INFRASTRUCTURES DE COMMUNICATIONS ÉLECTRONIQUES ET LES OPÉRATEURS DE COMMUNICATIONS ÉLECTRONIQUES, À L'ETAT, AUX COLLECTIVITÉS TERRITORIALES ET À LEURS GROUPEMENTS, DES INFORMATIONS RELATIVES À L'IMPLANTATION ET AU DÉPLOIEMENT DE LEURS INFRASTRUCTURES ET DE LEURS RÉSEAUX SUR LEUR TERRITOIRE (ART. L. 33-7 DU CODE DES POSTES ET DES COMMUNICATIONS ÉLECTRONIQUES) - POSSIBILITÉ DE COMMUNICATION DES DONNÉES ENTRE PERSONNES PUBLIQUES - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">26-06-01-02-03 DROITS CIVILS ET INDIVIDUELS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS AU TITRE DE LA LOI DU 17 JUILLET 1978. DROIT À LA COMMUNICATION. DOCUMENTS ADMINISTRATIFS NON COMMUNICABLES. - COMMUNICATION PAR LES GESTIONNAIRES D'INFRASTRUCTURES DE COMMUNICATIONS ÉLECTRONIQUES ET LES OPÉRATEURS DE COMMUNICATIONS ÉLECTRONIQUES, À L'ETAT, AUX COLLECTIVITÉS TERRITORIALES ET À LEURS GROUPEMENTS, DES INFORMATIONS RELATIVES À L'IMPLANTATION ET AU DÉPLOIEMENT DE LEURS INFRASTRUCTURES ET DE LEURS RÉSEAUX SUR LEUR TERRITOIRE (ART. L. 33-7 DU CODE DES POSTES ET DES COMMUNICATIONS ÉLECTRONIQUES) - POSSIBILITÉ DE COMMUNICATION AU PUBLIC - ABSENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">51-02 POSTES ET COMMUNICATIONS ÉLECTRONIQUES. COMMUNICATIONS ÉLECTRONIQUES. - COMMUNICATION PAR LES GESTIONNAIRES D'INFRASTRUCTURES DE COMMUNICATIONS ÉLECTRONIQUES ET LES OPÉRATEURS DE COMMUNICATIONS ÉLECTRONIQUES, À L'ETAT, AUX COLLECTIVITÉS TERRITORIALES ET À LEURS GROUPEMENTS, DES INFORMATIONS RELATIVES À L'IMPLANTATION ET AU DÉPLOIEMENT DE LEURS INFRASTRUCTURES ET DE LEURS RÉSEAUX SUR LEUR TERRITOIRE (ART. L. 33-7 DU CODE DES POSTES ET DES COMMUNICATIONS ÉLECTRONIQUES) - 1) POSSIBILITÉ DE COMMUNICATION DES DONNÉES ENTRE PERSONNES PUBLIQUES - EXISTENCE - 2) POSSIBILITÉ DE COMMUNICATION AU PUBLIC - ABSENCE - 3) MODALITÉS DE TRANSMISSION DES INFORMATIONS - ILLÉGALITÉ D'UNE OBLIGATION EXCESSIVE DE TRAITEMENT DES INFORMATIONS IMPOSÉE PAR LE POUVOIR RÉGLEMENTAIRE AUX OPÉRATEURS.
</SCT>
<ANA ID="9A"> 01-02-01 En prévoyant, pour l'application de l'article  L. 33-7 du code des postes et des communications électroniques, aux termes duquel : « Les gestionnaires d'infrastructures de communications électroniques et les opérateurs de communications électroniques communiquent gratuitement à l'Etat, aux collectivités territoriales et à leurs groupements, à leur demande, les informations relatives à l'implantation et au déploiement de leurs infrastructures et de leurs réseaux sur leur territoire », que les informations à communiquer devaient être transmises sous forme de données numériques vectorielles géolocalisées pouvant être reprises dans des systèmes d'informations géographiques et suivant un format largement répandu, le pouvoir réglementaire a imposé aux opérateurs un traitement des informations qui va au-delà des obligations posées par le législateur.</ANA>
<ANA ID="9B"> 135-01 Le pouvoir réglementaire a légalement pu prévoir, pour la mise en oeuvre de l'article  L. 33-7 du code des postes et des communications électroniques, aux termes duquel : « Les gestionnaires d'infrastructures de communications électroniques et les opérateurs de communications électroniques communiquent gratuitement à l'Etat, aux collectivités territoriales et à leurs groupements, à leur demande, les informations relatives à l'implantation et au déploiement de leurs infrastructures et de leurs réseaux sur leur territoire », qu'était possible la communication entre services de l'Etat, d'une part, et entre collectivités territoriales et groupements, d'autre part, des données reçues en application de cet article après information des opérateurs et gestionnaires concernés, dans les mêmes conditions de confidentialité et selon les mêmes exigences de procédure et d'information des opérateurs et gestionnaires concernés que la demande initiale.</ANA>
<ANA ID="9C"> 26-06-01-02-03 En prévoyant que les données relatives à l'implantation et au déploiement de leurs infrastructures et de leurs réseaux communiquées par les gestionnaires d'infrastructures de communications électroniques et les opérateurs de communications électroniques à l'Etat, aux collectivités territoriales et à leurs groupements ne sont pas communicables au public, le premier alinéa du IV de l'article D. 98-6-3 du code des postes et des communications électroniques, qui reconnaît implicitement le caractère sensible des informations susceptibles, par leur nature, de porter atteinte à la sûreté de l'Etat, à la sécurité publique ou à la sécurité des personnes au sens de l'article 6 de la loi n° 78-753 du 17 juillet 1978, n'a pas méconnu la loi.</ANA>
<ANA ID="9D"> 51-02 1) Le pouvoir réglementaire a légalement pu prévoir, pour la mise en oeuvre de l'article  L. 33-7 du code des postes et des communications électroniques, aux termes duquel : « Les gestionnaires d'infrastructures de communications électroniques et les opérateurs de communications électroniques communiquent gratuitement à l'Etat, aux collectivités territoriales et à leurs groupements, à leur demande, les informations relatives à l'implantation et au déploiement de leurs infrastructures et de leurs réseaux sur leur territoire », qu'était possible la communication entre services de l'Etat, d'une part, et entre collectivités territoriales et groupements, d'autre part, des données reçues en application de cet article après information des opérateurs et gestionnaires concernés, dans les mêmes conditions de confidentialité et selon les mêmes exigences de procédure et d'information des opérateurs et gestionnaires concernés que la demande initiale.,,2) En prévoyant que les données relatives à l'implantation et au déploiement de leurs infrastructures et de leurs réseaux communiquées par les gestionnaires d'infrastructures de communications électroniques et les opérateurs de communications électroniques à l'Etat, aux collectivités territoriales et à leurs groupements ne sont pas communicables au public, le premier alinéa du IV de l'article D. 98-6-3 du code des postes et des communications électroniques, qui reconnaît implicitement le caractère sensible des informations susceptibles, par leur nature, de porter atteinte à la sûreté de l'Etat, à la sécurité publique ou à la sécurité des personnes au sens de l'article 6 de la loi n° 78-753 du 17 juillet 1978, n'a pas méconnu la loi.,,3) En prévoyant que les informations à communiquer devaient être transmises sous forme de données numériques vectorielles géolocalisées pouvant être reprises dans des systèmes d'informations géographiques et suivant un format largement répandu, le pouvoir réglementaire a imposé aux opérateurs un traitement des informations qui va au-delà des obligations posées par le législateur.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
