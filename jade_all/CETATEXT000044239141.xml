<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044239141</ID>
<ANCIEN_ID>JG_L_2021_10_000000446902</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/23/91/CETATEXT000044239141.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 22/10/2021, 446902, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>446902</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Hervé Cassagnabère</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:446902.20211022</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. K... J... a demandé au tribunal administratif de Strasbourg d'annuler les opérations électorales qui se sont déroulées le 28 juin 2020 en vue de la désignation des conseillers municipaux et communautaires de la commune de Réguisheim (Haut-Rhin). Par un jugement n° 2003968 du 22 octobre 2020, ce tribunal a rejeté cette protestation. <br/>
<br/>
              Par une requête d'appel et un mémoire complémentaire, enregistrés les 25 novembre et 14 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. J... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de faire droit à sa protestation ;<br/>
<br/>
              3°) d'entendre, en vertu de l'article R. 623-1 du code de justice administrative, M. I... H..., M. A... N..., Mme L... F... et M. P... Q....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Hervé Cassagnabère, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. A l'issue du second tour des opérations électorales qui se sont déroulées les 15 mars et 28 juin 2020 en vue de la désignation des conseillers municipaux et communautaires de la commune de Réguisheim (Haut-Rhin), la liste " On aime Réguisheim ! " conduite par M. B... E... a recueilli 307 voix et obtenu quatorze sièges au conseil municipal et deux sièges au conseil communautaire, la liste " Hopla Réguisheim ! Pour un nouveau souffle... ", conduite par M. K... J..., a recueilli 306 voix et obtenu trois sièges au conseil municipal et un siège au conseil communautaire et la liste " Ensemble pour Réguisheim ", conduite par M. G... C..., a recueilli 220 voix et obtenu deux sièges au conseil municipal et aucun au conseil communautaire. M. J... relève appel du jugement du 22 octobre 2020 par lequel le tribunal administratif de Strasbourg a rejeté sa protestation tendant à l'annulation de cette élection. <br/>
<br/>
              2. En premier lieu, aux termes du premier alinéa de l'article L. 65 du code électoral : " Dès la clôture du scrutin, il est procédé au dénombrement des émargements. Ensuite, le dépouillement se déroule de la manière suivante : l'urne est ouverte et le nombre des enveloppes est vérifié. Si ce nombre est plus grand ou moindre que celui des émargements, il en est fait mention au procès-verbal (...) ".<br/>
<br/>
              3. Il résulte de l'instruction que, contrairement à ce qui est soutenu, la liste d'émargement comporte 845 signatures, soit le nombre de suffrages mentionnés sur le procès-verbal des opérations électorales. Par ailleurs, il ne résulte d'aucun élément du dossier que le président du bureau de vote de Réguisheim se serait abstenu de procéder à la vérification prescrite par les dispositions de l'article L. 65 du code électoral. <br/>
<br/>
              4. En deuxième lieu, aux termes de l'article L. 74 du code électoral : " Le ou la mandataire participe au scrutin dans les conditions prévues à l'article L. 62. / Il prend une enveloppe électorale après avoir fait constater l'existence d'un mandat de vote par procuration. / Son vote est constaté par sa signature apposée à l'encre sur la liste d'émargement en face du nom du mandant ".<br/>
<br/>
              5. L'erreur purement matérielle ayant consisté pour les mandataires de deux conjoints à signer chacun sur la liste d'émargement dans la case correspondant à l'autre conjoint que celui qu'ils représentaient ne peut être regardée comme ayant été, en l'espèce, de nature à altérer la sincérité du scrutin.<br/>
<br/>
              6. Par ailleurs, la procuration établie par M. D... le 27 juin 2020 auprès de la Gendarmerie d'Ensisheim, si elle ne mentionne aucune date de limite de validité, comporte une croix dans la case correspondant à un vote " pour le second tour seulement ". Elle doit ainsi être regardée, contrairement à ce qui est soutenu, comme ayant été régulièrement établie pour permettre au mandataire désigné de participer aux opérations électorales du 28 juin 2020 pour le compte de son mandant.<br/>
<br/>
              7. En troisième lieu, aux termes des dispositions du II de l'article L. 11 du code électoral : " Sous réserve qu'elles répondent aux autres conditions exigées par la loi, sont inscrites d'office sur la liste électorale de la commune de leur domicile réel, en vue de participer à un scrutin : / 1° Sans préjudice du 3° de l'article L. 30, les personnes qui ont atteint l'âge prévu par la loi pour être électeur à la date de ce scrutin ou, lorsque le mode de scrutin permet un second tour, à la date à laquelle ce second tour a vocation à être organisé (...) ". Il n'appartient pas au juge de l'élection, en l'absence de manœuvre susceptible d'avoir altéré la sincérité du scrutin, d'apprécier la régularité de l'inscription ou de la radiation d'un électeur sur les listes électorales.<br/>
<br/>
              8. Si M. J... soutient que quatre personnes résidant à Réguisheim et ayant atteint l'âge de 18 ans entre le premier tour de scrutin, le 15 mars 2020, et le second tour, le 28 juin suivant, n'ont pas été inscrites sur la liste électorale, en méconnaissance des dispositions précitées du II de l'article L. 11 du code électoral, et n'ont ainsi pas pu participer au vote, il ne fait état d'aucune circonstance permettant de considérer que cette omission procèderait d'une manœuvre. <br/>
<br/>
              9. En quatrième lieu, aux termes de l'article L. 49 du code électoral : " A partir de la veille du scrutin à zéro heure, il est interdit de : (...) / 2° Diffuser ou faire diffuser par tout moyen de communication au public par voie électronique tout message ayant le caractère de propagande électorale (...) ".<br/>
<br/>
              10. Il résulte de l'instruction que si un candidat de la liste " On aime Réguisheim ! ", conduite par M. E..., a partagé sur sa page Facebook un tract de cette liste le samedi 27 juin 2020 à 00 heure 18, ce tract avait déjà fait précédemment l'objet d'une diffusion tant sous forme papier que par voie électronique. Cette publication tardive ne saurait dès lors être regardée, dans les circonstances de l'espèce, comme une irrégularité de nature à altérer la sincérité du scrutin.<br/>
<br/>
              11. En cinquième lieu, si M. J... soutient que la liste " Ensemble pour Réguisheim " conduite par Monsieur C... a fait figurer sur ses affiches électorales et sur la circulaire établie en vue du second tour les logos " Régions et Peuples Solidaires ", " Alliance écologiste indépendante " et " Village cigogne d'Alsace " sans y être autorisée ni avoir reçu les investitures correspondantes, il n'apporte, en tout état de cause, aucun élément au soutien de ces allégations. <br/>
<br/>
              12. En dernier lieu, il résulte de l'instruction que si des membres de la liste " On aime Réguisheim ! " ont participé, début mai 2020, à une opération de distribution de masques aux habitants de la commune organisée sous l'égide et grâce au financement des artisans et commerçants et du club de football de Réguisheim, cette opération n'a donné lieu à aucune mise en avant de cette liste ou de ses membres, le document accompagnant cette distribution ne faisant notamment référence ni à la liste, ni aux opérations électorales à venir. Il en résulte que cette distribution ne saurait être regardée comme un élément de propagande électorale dont le financement contreviendrait aux dispositions de l'article L. 52-8 du code électoral.<br/>
<br/>
              13. Il résulte de tout ce qui précède, sans qu'il soit besoin de prescrire une enquête sur le fondement de l'article R. 623-1 du code de justice administrative, que la requête de M. J... doit être rejetée. <br/>
<br/>
              14. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. E... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. J... est rejetée.<br/>
Article 2 : Les conclusions de M. E... présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. K... J..., à M. B... E..., à M. G... C... et au ministre de l'intérieur.<br/>
              Délibéré à l'issue de la séance du 16 septembre 2021 où siégeaient : M. Pierre Collin, président de chambre, présidant ; M. Jean-Claude Hassan, conseiller d'Etat et M. Hervé Cassagnabère, conseiller d'Etat-rapporteur. <br/>
<br/>
              Rendu le 22 octobre 2021.<br/>
                 Le président : <br/>
                 Signé : M. Pierre Collin<br/>
 		Le rapporteur : <br/>
      Signé : M. Hervé Cassagnabère<br/>
                 La secrétaire :<br/>
                 Signé : Mme O... M...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
