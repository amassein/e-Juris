<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041853441</ID>
<ANCIEN_ID>JG_L_2020_05_000000440173</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/85/34/CETATEXT000041853441.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 04/05/2020, 440173, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-05-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440173</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:440173.20200504</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 20 et 25 avril 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution de la décision d'interdire aux adultes d'utiliser une bicyclette pour les déplacements prévus au 5° du I de l'article 3 du décret n° 2020-293 du 23 mars 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire, révélée par la publication, sur le site internet du gouvernement, de la réponse à une question intitulée " Puis-je continuer de faire une sortie en vélo ' " ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de " 500 001 francs des colonies françaises du Pacifique " au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              M. A... soutient que la condition d'urgence est remplie et que la décision contestée :<br/>
              - est entachée d'incompétence ;<br/>
              - méconnaît les dispositions du 5° du I de l'article 3 du décret du 23 mars 2020.<br/>
<br/>
              Par un mémoire en défense et un nouveau mémoire, enregistrés les 24 et 27 avril 2020, le ministre de l'intérieur conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas remplie et que la décision contestée n'existe pas.<br/>
<br/>
<br/>
              La requête a été communiquée au Premier ministre, à la ministre de la transition écologique et solidaire et à la ministre des sports, qui n'ont pas produit de mémoire en défense.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
- le code de la santé publique ;<br/>
- la loi n° 2020-290 du 23 mars 2020 ;<br/>
- l'ordonnance n°2020-305 du 25 mars 2020 ;<br/>
- le décret n° 2020-293 du 23 mars 2020 ;<br/>
- le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". L'article 9 de l'ordonnance du 25 mars 2020 portant adaptation des règles applicables devant les juridictions de l'ordre administratif dispose par ailleurs que, durant la période comprise entre le 12 mars 2020 et la date de cessation de l'état d'urgence sanitaire déclaré dans les conditions de l'article 4 de la loi du 23 mars 2020, " (...) il peut être statué sans audience, par ordonnance motivée, sur les requêtes présentées en référé (...) ".<br/>
<br/>
              2. L'article L. 3131-15 du code de la santé publique, introduit dans ce code par la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 dispose que, dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le Premier ministre peut notamment : " 1° Restreindre ou interdire la circulation des personnes et des véhicules dans les lieux et aux heures fixés par décret ; 2° Interdire aux personnes de sortir de leur domicile, sous réserve des déplacements strictement indispensables aux besoins familiaux ou de santé ; (...) Les mesures prescrites en application des 1° à 10° du présent article sont strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu (...) ".<br/>
<br/>
              3. Sur le fondement des dispositions citées ci-dessus, l'article 3 du décret du 23 mars 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire, modifié et complété à plusieurs reprises, interdit, en dernier lieu jusqu'au 11 mai 2020, tout déplacement de personne hors de son domicile, à l'exception de certains déplacements obéissant aux motifs qu'il énumère. Au nombre de ceux-ci figurent notamment, au 5° du I de cet article, les " déplacements brefs, dans la limite d'une heure quotidienne et dans un rayon maximal d'un kilomètre autour du domicile, liés soit à l'activité physique individuelle des personnes, à l'exclusion de toute pratique sportive collective et de toute proximité avec d'autres personnes, soit à la promenade avec les seules personnes regroupées dans un même domicile, soit aux besoins des animaux de compagnie ".<br/>
<br/>
              4. Il résulte des termes mêmes de l'article 3 du décret du 23 mars 2020 cité ci-dessus que l'usage, pour un déplacement qu'il autorise, d'un moyen de déplacement particulier, notamment d'une bicyclette, ne saurait, à lui seul, caractériser une violation de l'interdiction qu'il édicte.<br/>
<br/>
              5. M. A... demande la suspension de l'exécution d'une décision du Premier ministre interdisant la pratique de la bicyclette pour les déplacements liés à l'activité physique individuelle, révélée selon lui par une réponse apportée, sur le site www.gouvernement.fr/info-coronavirus, à la question " puis-je continuer de faire une sortie en vélo ' ".<br/>
<br/>
              6. Toutefois, à supposer même qu'une telle décision ait été prise et explique, par suite, la réponse qui figurait sur le site du gouvernement à la date d'introduction de la requête de M. A..., il résulte de l'instruction que le gouvernement a adopté, à la date de la présente ordonnance, une position strictement conforme au principe rappelé au point 4. Les conclusions de la requête tendant à ce que l'exécution de la décision contestée soit suspendue sont, par suite, sans objet et doivent être rejetées.<br/>
<br/>
              7. Enfin, les conclusions présentées par M. A... au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées, dès lors qu'en tout état de cause, il n'établit pas la réalité des sommes exposées par lui dans le cadre de la présente instance.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B... A..., au ministre de l'intérieur, à la ministre de la transition écologique et solidaire, à la ministre des sports et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
