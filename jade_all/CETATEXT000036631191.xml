<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036631191</ID>
<ANCIEN_ID>JG_L_2018_02_000000396363</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/63/11/CETATEXT000036631191.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 21/02/2018, 396363, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-02-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396363</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Yohann Bénard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:396363.20180221</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Lille, d'une part, d'annuler les décisions du 1er août 2014 et du 8 septembre 2014 par lesquelles le recteur de l'académie de Lille a refusé de faire droit à sa demande de départ anticipé à la retraite pour longue carrière et, d'autre part, d'enjoindre à l'administration de réexaminer sa demande.<br/>
<br/>
              Par un jugement n° 1406280 du 1er décembre 2015, le tribunal administratif de Lille a fait droit à sa demande.<br/>
<br/>
              Par un pourvoi, enregistré le 25 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche demande au Conseil d'Etat d'annuler ce jugement.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Yohann Bénard, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du I de l'article D. 16-1 du code des pensions civiles et militaires : " I.- L'âge d'ouverture du droit à une pension de retraite est abaissé à soixante ans, en application de l'article L. 25 bis, pour les fonctionnaires ayant débuté leur activité avant l'âge de vingt ans et qui justifient, dans le régime des pensions civiles et militaires de retraite et, le cas échéant, dans un ou plusieurs autres régimes obligatoires, d'une durée d'assurance ayant donné lieu à cotisations à leur charge au moins égale à la durée d'assurance définie à l'article L. 14 et applicable l'année où ils atteignent l'âge de soixante ans. ". Aux termes des dixième et onzième alinéas de l'article L. 5 de ce code : " Pour les fonctionnaires titularisés au plus tard le 1er janvier 2013, peuvent également être pris en compte pour la constitution du droit à pension les services d'auxiliaire, de temporaire, d'aide ou de contractuel, y compris les périodes de congé régulier pour longue maladie, accomplis dans les administrations centrales de l'Etat, les services extérieurs en dépendant et les établissements publics de l'Etat ne présentant pas un caractère industriel et commercial, si la validation des services de cette nature a été autorisée pour cette administration par un arrêté conjoint du ministre intéressé et du ministre des finances et si elle est demandée dans les deux années qui suivent la date de la titularisation ou d'entrée en service pour les militaires sous contrat. / Le délai dont dispose l'agent pour accepter ou refuser la notification de validation est d'un an. ". Aux termes, enfin, de la seconde phrase du cinquième alinéa de l'article R. 7 du même code : " L'annulation des sommes acquittées pendant la durée des services à valider, au titre du régime général de l'assurance vieillesse et de l'institution de retraite complémentaire des agents non titulaires de l'Etat et des collectivités publiques, est effectuée au profit du Trésor public. ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond, d'une part, que par une décision 8 novembre 2010, les services effectués par Mme B...du 24 octobre 1975 au 13 septembre 1977 en qualité d'agent non titulaire ont été admis à validation, pour une durée de 11 mois et 10 jours, soit quatre trimestres et, d'autre part, que, par une décision du 8 septembre 2014, le recteur de l'académie de Lille a refusé à Mme B...le bénéfice des dispositions de l'article D. 16-1 du code des pensions civiles et militaires citées au point 1, au motif qu'elle ne totalisait pas les 165 trimestres d'assurance cotisés nécessaires pour être éligible à ces dispositions et a rejeté, en conséquence, sa demande d'admission à la retraite anticipée pour carrière longue. Le ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche se pourvoit en cassation contre le jugement du 1er décembre 2015 par lequel le tribunal administratif de Lille a annulé la décision du 8 septembre 2014 et a enjoint au recteur de l'académie de Lille de se prononcer à nouveau, dans un délai d'un mois, sur la demande de Mme B....<br/>
<br/>
              3. Pour annuler la décision de refus opposée à MmeB..., le tribunal administratif de Lille, après avoir estimé que les dispositions de l'article D. 16-1 du code des pensions civiles et militaires de retraite se bornent à évoquer une durée d'assurance ayant donné lieu à cotisation, tant au titre du régime des pensions de la fonction publique qu'au titre du régime général, a jugé que la durée à prendre en compte au titre de la période du 24 octobre 1975 au 13 septembre 1977 était de huit trimestres, en se fondant, notamment, sur un relevé de carrière du régime général en date du 18 septembre 2009, fourni par MmeB.... En statuant ainsi, le tribunal administratif n'a pas dénaturé les pièces du dossier qui lui était soumis, quand bien même figuraient également à ce dossier, d'une part, un relevé de carrière en date du 25 mai 2014, prenant en compte la validation des services effectuée en 2010, mentionnant pour la période considérée " période non retenue régime général " et indiquant un nombre de trimestres nul et, d'autre part, la décision de validation du 8 novembre 2010 citée au point 2, qui contient des éléments relatifs à la durée d'assurance correspondant aux services validés au titre de la période du 24 octobre 1975 au 13 septembre 1977.<br/>
<br/>
              4. Il résulte de ce qui précède que le ministre n'est pas fondé, par le seul moyen qu'il invoque, à demander l'annulation du jugement attaqué.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche est rejeté.<br/>
Article 2 : La présente décision sera notifiée au ministre de l'éducation nationale et à Mme A...B....<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
