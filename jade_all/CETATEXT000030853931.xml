<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030853931</ID>
<ANCIEN_ID>JG_L_2015_07_000000361236</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/85/39/CETATEXT000030853931.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème SSJS, 03/07/2015, 361236, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361236</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:361236.20150703</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La SAS Jean Floc'h a demandé au tribunal administratif de Rennes de réduire sa base d'imposition à la taxe professionnelle au titre de l'année 2007 dans les rôles de la commune de Baud à concurrence du montant des immobilisations correspondant à une opération d'extension réalisée en avril 2000 et inscrites à l'actif de son bilan de l'exercice clos le 31 mars 2001 et de la décharger des cotisations supplémentaires et pénalités correspondant à cette réduction de base imposable. Par un jugement n° 0804866 du 6 janvier 2011, le tribunal administratif de Rennes a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 11NT00744 du 31 mai 2012, la cour administrative de Nantes a réduit la base imposable de la SAS Jean Floc'h à la taxe professionnelle du montant des immobilisations correspondant à l'opération d'extension réalisée en avril 2000 inscrites à l'actif du bilan de cette société de l'exercice clos le 31 mars 2001, l'a déchargée des cotisations supplémentaires et pénalités correspondant à cette réduction de base imposable et a réformé en conséquence le jugement du 6 janvier 2011 du tribunal administratif de Rennes.<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 20 juillet 2012 et 27 septembre 2013 au secrétariat du contentieux du Conseil d'État, le ministre délégué chargé du budget demande au Conseil d'État d'annuler cet arrêt.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la SAS Jean Floc'h ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à raison de l'extension de son établissement industriel situé à Baud, achevée en avril 2000, la SAS Jean Floc'h, dont les exercices sociaux en cause dans le présent litige ont été clos au 31 mars de chaque année, s'est, sur le fondement de l'article 1465 du code général des impôts, prévalue d'une exonération temporaire de taxe professionnelle au titre des années 2002 à 2007. L'administration fiscale a remis en cause, à la suite d'une vérification de comptabilité, l'exonération dont la SAS Jean Floc'h entendait se prévaloir au titre de l'année 2007, au motif qu'elle aurait alors excédé la durée maximale de cinq ans prévue par cet article 1465. Après avoir vainement réclamé contre les cotisations supplémentaires de taxe professionnelle ainsi mises à sa charge, la SAS Jean Floc'h a soumis le litige au tribunal administratif de Rennes qui, par un jugement du 6 janvier 2011, a rejeté sa demande de décharge. Le ministre délégué chargé du budget se pourvoit en cassation contre l'arrêt du 31 mai 2012 par lequel la cour administrative d'appel de Nantes a accordé cette décharge à la SAS Jean Floc'h.<br/>
<br/>
              2. Aux termes de l'article 1467 du code général des impôts, dans sa version applicable à la taxe professionnelle due au titre des années 2001 et 2002 : " La taxe professionnelle a pour base : / 1° Dans le cas des contribuables autres que les titulaires de bénéfices non commerciaux, les agents d'affaires et les intermédiaires de commerce employant moins de cinq salariés : / a. la valeur locative (...) des immobilisations corporelles dont le redevable a disposé pour les besoins de son activité professionnelle pendant la période de référence définie aux articles 1467 A et 1478 (...) / b. les salaires au sens du 1 de l'article 231 (...) ". Les dispositions précitées du b de cet article ont été abrogées pour la taxe due à compter de l'année 2003. Par ailleurs, aux termes de l'article 1467 A du même code, dans sa version applicable aux cotisations en litige : " La période de référence retenue pour déterminer les bases de taxe professionnelle est l'avant-dernière année précédant celle de l'imposition ou, pour les immobilisations et les recettes imposables, le dernier exercice de douze mois clos au cours de cette même année lorsque cet exercice ne coïncide pas avec l'année civile ". Il s'en déduit que, s'agissant d'une entreprise dont l'exercice ne coïncide pas avec l'année civile, les salaires versés lors d'une année civile sont susceptibles de donner lieu à imposition au titre d'une année différente de celle au titre de laquelle seront imposées les immobilisations dont elle a disposé lors de cette même année civile, dès lors que ces immobilisations n'auraient pas encore figuré au bilan d'un exercice clos lors de l'année en question.<br/>
<br/>
              3. En vertu de l'article 1465 du code général des impôts dans sa version applicable aux cotisations en litige, dans certaines zones où l'aménagement du territoire le rend utile, les collectivités territoriales et leurs groupements dotés d'une fiscalité propre peuvent, par une délibération de portée générale, exonérer de taxe professionnelle, en totalité ou en partie, les entreprises qui procèdent sur leur territoire à des extensions d'activités industrielles, sous certaines conditions fixées par décret tenant notamment compte du volume des investissements et du nombre des emplois créés. Le premier alinéa de cet article précise que " cette délibération ne peut avoir pour effet de reporter de plus de cinq ans l'application du régime d'imposition de droit commun ". Son cinquième alinéa précise en outre, d'une part, que " l'exonération porte sur l'augmentation nette des bases d'imposition (...) appréciée par rapport à la dernière année précédant l'opération ou par rapport à la moyenne des trois dernières années si celle-ci est supérieure " et, d'autre part, que " l'entreprise ne peut bénéficier d'une exonération non soumise à agrément qu'à condition de l'avoir indiqué au service des impôts au plus tard lors du dépôt de la première déclaration dans laquelle doivent figurer les éléments nouveaux concernés ". Par ailleurs, selon l'article 322 J de l'annexe III au code général des impôts, pris pour l'application de l'article 1465 précité de ce code, la réalisation des conditions tenant au volume des investissements et au nombre d'emplois créés " s'apprécie au 31 décembre de la première année au cours de laquelle l'entreprise a procédé à l'une des opérations mentionnées à cet article. Toutefois, lorsque la période de référence servant à la détermination des bases de taxe professionnelle correspondantes ne coïncide pas avec l'année civile, la réalisation de ces conditions s'apprécie, en ce qui concerne les investissements, à la date de clôture de l'exercice retenu comme période de référence ".<br/>
<br/>
              4. Il résulte de ces dispositions combinées, d'une part, que l'opération d'extension doit être regardée comme réalisée l'année au cours de laquelle l'entreprise a disposé d'immobilisations dans son nouvel établissement et a versé des salaires au personnel employé dans ce même établissement, cette année étant celle au titre de laquelle elle doit déclarer pour la première fois les éléments nouveaux concernés par l'exonération qu'elle demande. Il résulte également de ces dispositions que, si la circonstance que l'exercice social d'une entreprise ne coïncide pas avec l'année civile est prise en compte pour la fixation des dates auxquelles doit s'apprécier la réalisation des conditions requises pour le bénéfice de l'exonération aux termes de l'article 322 J de l'annexe III au code général des impôts, ni le législateur ni le pouvoir réglementaire n'ont prévu que cette dernière puisse prendre effet à une date différente selon que sont en cause les salaires versés par l'entreprise ou les immobilisations inscrites à son bilan correspondant à une seule et même opération d'extension.<br/>
<br/>
              5. Il suit de là que la cour administrative d'appel de Nantes, en jugeant, pour accorder à la SAS Jean Floc'h la décharge des cotisations supplémentaires de taxe professionnelle en litige, qu'il résultait de l'instruction que la demande d'exonération que cette société avait déposée au titre de l'année 2002 ne portait que sur les salaires relatifs aux emplois créés et non sur les immobilisations afférentes à l'extension, lesquelles avaient été comptabilisées à l'actif du bilan de l'exercice clos le 31 mars 2001 et n'étaient donc susceptibles d'être imposées à la taxe professionnelle qu'à compter de l'année 2003, la période d'exonération de cinq ans courant ainsi, s'agissant des immobilisations en litige, du 1er janvier 2003 au 31 décembre 2007, a commis une erreur de droit. Par suite et sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, le ministre est fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              6. L'article L. 761-1 du code de justice administrative fait obstacle à ce qu'une somme soit mise à ce titre à la charge de l'État, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 31 mai 2012 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
Article 3 : Les conclusions présentées par la SAS Jean Floc'h au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre des finances et des comptes publics et à la SAS Jean Floc'h.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
