<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044513318</ID>
<ANCIEN_ID>JG_L_2021_12_000000458872</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/51/33/CETATEXT000044513318.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 15/12/2021, 458872, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>458872</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP HEMERY, THOMAS-RAQUIN, LE GUERER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:458872.20211215</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. E... A... B... a demandé au juge des référés du tribunal administratif de Marseille, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, de l'admettre au bénéfice de l'aide juridictionnelle provisoire et, d'autre part, de suspendre l'exécution de l'arrêté du 27 janvier 2012 par lequel le préfet des Bouches-du-Rhône a prononcé son expulsion du territoire français. Par une ordonnance n° 2109955 du 18 novembre 2021, le juge des référés du tribunal administratif de Marseille a, d'une part, admis M. A... B... au bénéfice de l'aide juridictionnelle provisoire et, d'autre part, rejeté le surplus des conclusions de sa demande.<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 26 novembre et 6 décembre 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de l'admettre au bénéfice de l'aide juridictionnelle provisoire ;<br/>
<br/>
              2°) d'annuler cette ordonnance ;<br/>
<br/>
              3°) de suspendre l'exécution de l'arrêté du 27 janvier 2012 jusqu'à ce qu'il soit statué sur la demande d'abrogation de cet arrêté, ou au plus tard jusqu'au 27 mars 2022 ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 1 500 euros dans l'hypothèse où il serait fait droit à la demande, au profit de Me Daïmallah, qui renoncerait alors au bénéfice de la partie contributive de l'Etat de l'aide juridictionnelle de sorte qu'il soit fait application à son profit des articles 37 et 75 de la loi du 10 juillet 1991, ou, à défaut d'attribution de l'aide juridictionnelle, au titre de l'article L. 761- 1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - l'ordonnance du juge des référés du tribunal administratif de Marseille, en tant qu'elle a considéré irrecevable la demande de suspension de l'exécution de l'arrêté en date du 27 janvier 2012, est mal fondée dès lors que les changements survenus dans sa situation personnelle et familiale, ainsi que les garanties de réinsertion professionnelle et sociale qu'il présente sont de nature à faire obstacle à l'exécution de l'arrêté contesté ;<br/>
              - la condition d'urgence est satisfaite dès lors que la mesure d'éloignement contestée aurait pour conséquence de le séparer de sa concubine et de leur enfant, de nationalité française ; <br/>
              - il est porté une atteinte grave et manifestement illégale à son droit de mener une vie familiale normale dès lors, d'une part, que les faits pénalement répréhensibles visés par l'arrêté contesté remontent au plus tard au 14 mai 2011 et qu'il n'a pas commis d'acte illicite depuis lors et, d'autre part, que le centre de ses attaches personnelles et familiales est désormais situé en France en ce que, en premier lieu, il forme avec sa concubine depuis 2018 une famille composée de leur enfant née le 10 septembre 2020, de son fils âgé de six ans ainsi que des deux enfants de sa concubine, en deuxième lieu, sa compagne et l'enfant qu'ils ont eu ensemble possèdent la nationalité française et, en troisième lieu, son père est le seul membre de sa famille qui réside encore en Tunisie.<br/>
<br/>
              Par un mémoire en défense, enregistré le 3 décembre 2021, le ministre de l'intérieur conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas satisfaite, et que les moyens soulevés ne sont pas fondés. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. D... B..., et d'autre part, le ministre de l'intérieur ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 6 décembre 2021, à 15 heures : <br/>
<br/>
              - Me Le Guerer, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. A... B... ;<br/>
<br/>
              - les représentantes du ministre de l'intérieur ; <br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". <br/>
<br/>
              2. Aux termes de l'article L. 521-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Sous réserve des dispositions des articles L. 521-2, L. 521-3 et L. 521-4, l'expulsion peut être prononcée si la présence en France d'un étranger constitue une menace grave pour l'ordre public ". Aux termes de l'article L. 521-2 du même code : " Ne peuvent faire l'objet d'une mesure d'expulsion que si cette mesure constitue une nécessité impérieuse pour la sûreté de l'Etat ou la sécurité publique et sous réserve que les dispositions de l'article L. 521-3 n'y fassent pas obstacle : 1° L'étranger, ne vivant pas en état de polygamie, qui est père ou mère d'un enfant français mineur résidant en France, à condition qu'il établisse contribuer effectivement à l'entretien et à l'éducation de l'enfant dans les conditions prévues par l'article 371-2 du code civil depuis la naissance de celui-ci ou depuis au moins un an ".<br/>
<br/>
              3. En ce qu'il a pour objet de préserver des ingérences excessives de l'autorité publique la liberté qu'a toute personne de vivre avec sa famille, le droit de mener une vie familiale normale constitue une liberté fondamentale au sens des dispositions précitées de l'article L. 521-2 du code de justice administrative. Toutefois, une demande présentée au titre de la procédure particulière de l'article L. 521-2 du code de justice administrative implique, pour qu'il y soit fait droit, qu'il soit justifié, non seulement d'une situation d'urgence, mais encore d'une atteinte grave portée à la liberté fondamentale invoquée ainsi que de l'illégalité manifeste de cette atteinte. La condition de gravité de l'atteinte portée à la liberté de vivre avec sa famille doit être regardée comme remplie dans le cas où la mesure contestée peut faire l'objet d'une exécution d'office par l'autorité administrative, n'est pas susceptible de recours suspensif devant le juge de l'excès de pouvoir, et fait directement obstacle à la poursuite de la vie en commun des membres d'une famille. Tel est le cas d'une mesure d'expulsion du territoire français, susceptible d'une exécution d'office, s'opposant au retour en France de la personne qui en fait l'objet, et prononcée à l'encontre d'un ressortissant étranger qui justifie qu'il mène une vie familiale en France. La condition d'illégalité manifeste de la décision contestée, au regard du droit à une vie familiale normale ne peut être regardée comme remplie que dans le cas où il est justifié d'une atteinte manifestement disproportionnée aux buts en vue desquels la mesure contestée a été prise.<br/>
<br/>
              4. M. A... B..., ressortissant tunisien né en février 1992 et entré en France en 2007 à l'âge de 15 ans, a fait l'objet d'un arrêté d'expulsion pris par le préfet des Bouches-du Rhône le 27 janvier 2012 sur le fondement de l'article L. 521-1 du code de l'entrée et du séjour des étrangers et du droit d'asile au motif que sa présence en France constitue une menace grave pour l'ordre public, qui a été exécuté le 7 septembre 2013. Revenu illégalement en France en 2018, l'intéressé a déposé une demande de titre de séjour rejetée par décision du préfet des Bouches-du Rhône du 25 janvier 2021. M. A... B... a formé un recours contre cette décision devant le tribunal administratif de Marseille, qui a rejeté sa demande par une ordonnance du 20 avril 2021. Interpellé dans le cadre d'un contrôle d'identité le 23 octobre 2021, M. A... B... a fait l'objet d'une mesure de rétention au centre du Canet. Saisi par le préfet des Bouches-du Rhône d'une demande de prolongation de la mesure de rétention, le juge des libertés et de la détention du tribunal judiciaire de Marseille a assigné l'intéressé à résidence à son domicile par ordonnance du 27 octobre 2021. Faisant état de sa vie commune depuis février 2018 avec Mme C..., avec laquelle il a eu une fille née le 10 septembre 2020, M. A... B... a demandé au juge des référés du tribunal administratif de Marseille, sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative, de suspendre l'exécution de l'arrêté du 27 janvier 2012 par lequel le préfet des Bouches-du Rhône a décidé de l'expulser du territoire français. Il relève appel devant le juge des référés du Conseil d'Etat de l'ordonnance du 18 novembre 2021 par laquelle le juge des référés du tribunal administratif de Marseille a rejeté sa demande. <br/>
<br/>
              5. Il résulte de l'instruction que M. A... B... a fait l'objet de plusieurs condamnations pénales entre novembre 2010 et mai 2011, notamment pour tentative de vol avec destruction et dégradation, conduite d'un véhicule sans permis, vol aggravé en réunion avec destruction et dégradation, au titre desquelles il a exécuté une peine de 30 mois d'emprisonnement. Par jugement du tribunal correctionnel de Marseille du 4 septembre 2012, M. A... B... a été une nouvelle fois condamné à 18 mois d'emprisonnement et cinq ans d'interdiction du territoire français pour des faits d'aide, d'assistance ou de protection de la prostitution d'autrui et de partage des produits ou profits tirés de la prostitution d'autrui. <br/>
<br/>
              6. Alors même que M. A... B... est, depuis le mois de septembre 2020 père d'un enfant de nationalité française et indique vivre en concubinage avec la mère de ce dernier en assumant par ailleurs la charge d'un fils né en 2015 d'une précédente union, la mesure d'expulsion dont la suspension est demandée, eu égard à la gravité des faits pour lesquels l'intéressé a été pénalement condamné et à leur caractère récent, ne porte pas à son droit au respect de la vie privée et familiale une atteinte manifestement disproportionnée aux buts en vue desquels la mesure contestée a été prise. <br/>
<br/>
              7. Il résulte de ce qui précède que M. A... B... n'est pas fondé à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Marseille a rejeté sa demande présentée sur le fondement de l'article L. 521-2 du code de justice administrative. Il y a lieu, par suite, de rejeter la requête de M. A... B..., y compris ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. A... B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. E... A... B... et au ministre de l'intérieur. <br/>
<br/>
Fait à Paris, le 15 décembre 2021<br/>
Signé : Benoît Bohnert<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
