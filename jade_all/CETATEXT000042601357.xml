<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042601357</ID>
<ANCIEN_ID>JG_L_2020_11_000000440254</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/60/13/CETATEXT000042601357.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 30/11/2020, 440254, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440254</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Thomas Pez-Lavergne</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:440254.20201130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Par deux mémoires, enregistrés le 13 octobre 2020 au secrétariat du contentieux du Conseil d'Etat, présenté en application de l'article 23-5 de l'ordonnance du 7 novembre 1958, M. A... B... demande au Conseil d'Etat, à l'appui de sa requête tendant à l'annulation pour excès de pouvoir des articles 2 à 5, 7, 10, 12 à 18, 51 et 58 du décret du 3 avril 2015 fixant le statut des fonctionnaires de la direction générale de la sécurité extérieure ainsi que de la décision implicite par laquelle le Premier ministre a rejeté sa demande tendant à l'abrogation, à l'exception de l'article 3, de l'ensemble de ces dispositions, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions, d'une part, de l'article 2 de la loi du 3 février 1953 relative au développement des crédits affectés aux dépenses de fonctionnement des services civils pour l'exercice 1953 et, d'autre part, des articles 2 à 5, 7, 10, 12 à 18, 51 et 58 du décret du 3 avril 2015.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son article 61-1 ;<br/>
              - l'ordonnance n° 58-1270 du 22 décembre 1958 ;<br/>
              - la loi n° 53-39 du 3 février 1953 modifiée par la loi n° 2012-347 du 12 mars 2012 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Pez-Lavergne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes de l'article 2 de la loi du 3 février 1953 relative au développement des crédits affectés aux dépenses de fonctionnement des services civils pour l'exercice 1953, dans sa rédaction résultant de la loi du 12 mars 2012 relative à l'accès à l'emploi titulaire et à l'amélioration des conditions d'emploi des agents contractuels dans la fonction publique, à la lutte contre les discriminations et portant diverses dispositions relatives à la fonction publique : " Il est créé, pour les besoins permanents du service de documentation extérieure et de contre-espionnage, des cadres de fonctionnaires titulaires, qui ne sont pas soumis aux dispositions de la loi n° 83-634 du 13 juillet 1983 portant droits et obligations des fonctionnaires et de la loi n° 84-16 du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat. / Un décret en Conseil d'Etat, pris en application de la présente loi, déterminera le statut de son personnel ". Sur le fondement de cette disposition, le pouvoir réglementaire a pris le décret du 3 avril 2015 fixant le statut des fonctionnaires de la direction générale de la sécurité extérieure. M. B... soutient que les dispositions de l'article 2 de la loi du 3 février 1953 et celles du décret du 3 avril 2015 méconnaissent l'article 34 de la Constitution.<br/>
<br/>
              3. Toutefois, d'une part, la méconnaissance par le législateur de sa propre compétence ne peut être invoquée à l'appui d'une question prioritaire de constitutionnalité que dans le cas où cette méconnaissance affecte par elle-même un droit ou une liberté que la Constitution garantit. En conséquence et en tout état de cause, M. B..., qui n'invoque aucun droit ni aucune liberté qui aurait été méconnu, ne peut utilement soutenir que les dispositions législatives qu'il critique méconnaissent l'article 34 de la Constitution.<br/>
<br/>
              4. D'autre part, les dispositions des articles 2 à 5, 7, 10, 12 à 18, 51 et 58 du décret du 3 avril 2015, qui ont un caractère réglementaire, ne sont donc pas au nombre des dispositions législatives visées par l'article 61-1 de la Constitution et l'article 23-5 de l'ordonnance du 7 novembre 1958. Elles ne sont, en conséquence, pas susceptibles de faire l'objet d'une question prioritaire de constitutionnalité.<br/>
<br/>
              5. Il résulte de ce qui précède qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel les questions prioritaires de constitutionnalité invoquées.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel les questions prioritaires de constitutionnalité soulevées par M. B....<br/>
Article 2 : La présente décision sera notifiée à M. A... B... et à la ministre des armées.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre et à la ministre de la transformation et de la fonction publiques.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
