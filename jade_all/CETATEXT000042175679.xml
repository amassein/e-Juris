<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042175679</ID>
<ANCIEN_ID>JG_L_2020_07_000000428603</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/17/56/CETATEXT000042175679.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 29/07/2020, 428603, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428603</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER ; SARL MEIER-BOURDEAU, LECUYER ET ASSOCIES</AVOCATS>
<RAPPORTEUR>Mme Bénédicte Fauvarque-Cosson</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:428603.20200729</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme F... A...'B... a demandé au tribunal administratif de Montpellier d'annuler les décisions implicites par lesquelles le président du conseil départemental de l'Hérault a rejeté ses demandes d'octroi en urgence de la prestation de compensation du handicap formées les 24 août et 6 décembre 2016 et de condamner le département de l'Hérault à l'indemniser du préjudice ayant résulté de ces décisions. Par un jugement n°s 1606458, 1701058 du 28 décembre 2018, le tribunal administratif de Montpellier a rejeté ses demandes comme portées devant un ordre de juridiction incompétent pour en connaître.<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 4 mars et 3 juin 2019 au secrétariat du contentieux du Conseil d'État, Mme A...'B... demande au Conseil d'État : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge du département de l'Hérault la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme D... E..., conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Zribi, Texier, avocat de Mme A...'B..., et à la SARL Meier-Bourdeau, Lecuyer et associés, avocat du département de l'Hérault ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article R. 351-5-1 du code de justice administrative : " Lorsque le Conseil d'Etat est saisi de conclusions se rapportant à un litige qui ne relève pas de la compétence de la juridiction administrative, il est compétent, nonobstant les règles relatives aux voies de recours et à la répartition des compétences entre les juridictions administratives, pour se prononcer sur ces conclusions et décliner la compétence de la juridiction administrative ".<br/>
<br/>
              2. Il résulte de l'instruction que Mme A...'B... bénéficiait, depuis le 1er octobre 2007, de la prestation de compensation du handicap prévue à l'article L. 245-1 du code de l'action sociale et des familles. Le bénéfice de cette prestation lui avait été renouvelé en dernier lieu par une décision de la commission des droits et de l'autonomie des personnes handicapées de l'Hérault du 12 octobre 2015, qui lui attribuait un montant mensuel au titre de l'élément lié à un " besoin d'aides humaines " correspondant à six heures d'aide par jour. Par deux courriers des 24 août et 6 décembre 2016, Mme A...'B... a demandé en urgence au président du conseil départemental de l'Hérault, sur le fondement des dispositions du troisième alinéa de l'article L. 245-2 du code de l'action sociale et des familles, de modifier à titre provisoire le montant mensuel qui lui était attribué au titre de cet élément de la prestation, pour qu'il corresponde à vingt-quatre heures d'aide par jour. Elle a demandé au tribunal administratif de Montpellier d'annuler les décisions nées du silence gardé par le président du conseil départemental de l'Hérault sur ses demandes et de condamner le département à l'indemniser du préjudice ayant résulté de ces décisions. Par un jugement du 28 décembre 2018 dont Mme A...'B... demande l'annulation, le tribunal a rejeté ces demandes comme portées devant un ordre de juridiction incompétent pour en connaître.<br/>
<br/>
              Sur la régularité du jugement attaqué :<br/>
<br/>
              3.  Aux termes du premier alinéa de l'article R. 732-1 du même code : " Après le rapport qui est fait sur chaque affaire par un membre de la formation de jugement ou par le magistrat mentionné à l'article R. 222-13, le rapporteur public prononce ses conclusions lorsque le présent code l'impose. Les parties peuvent ensuite présenter, soit en personne, soit par un avocat au Conseil d'Etat et à la Cour de cassation, soit par un avocat, des observations orales à l'appui de leurs conclusions écrites ".<br/>
<br/>
              4. Il ressort des pièces de la procédure devant le tribunal administratif et en particulier de la fiche d'audience présente au dossier que Mme A...'B... n'était ni présente ni représentée lors de l'audience publique qui s'est tenue le 18 décembre 2018. Par suite, elle n'est pas fondée à soutenir que le jugement aurait été rendu au terme d'une procédure irrégulière faute pour son curateur d'avoir été invité à prendre la parole lors de l'audience publique.<br/>
<br/>
              Sur l'ordre de juridiction compétent pour connaître du litige :<br/>
<br/>
              5. Il résulte de l'article L. 245-1 du code de l'action sociale et des familles que les personnes handicapées remplissant certaines conditions tenant à leur âge et à leur handicap ont " droit à une prestation de compensation qui a le caractère d'une prestation en nature qui peut être versée, selon le choix du bénéficiaire, en nature ou en espèces ". L'article L. 245-2 du même code prévoit que la prestation de compensation est accordée par la commission des droits et de l'autonomie des personnes handicapées et servie par le département et dispose, à ses troisième et quatrième alinéas dans leur rédaction applicable au litige : " Toutefois, en cas d'urgence attestée, le président du conseil départemental peut attribuer la prestation de compensation à titre provisoire et pour un montant fixé par décret. Il dispose d'un délai de deux mois pour régulariser cette décision (...). / Les décisions relatives à l'attribution de la prestation par la commission (...) peuvent faire l'objet d'un recours devant la juridiction du contentieux technique de la sécurité sociale. Les décisions du président du conseil départemental relatives au versement de la prestation peuvent faire l'objet d'un recours devant les commissions départementales " d'aide sociale. En vertu de l'article D. 245-34 du même code, la date d'ouverture des droits à la prestation de compensation du handicap est le premier jour du mois du dépôt de la demande. Enfin, aux termes de l'article R. 245-36 de ce code : " En cas d'urgence attestée, l'intéressé peut, à tout moment de l'instruction de sa demande de prestation de compensation, joindre une demande particulière sur laquelle le président du conseil départemental statue en urgence dans un délai de quinze jours ouvrés en arrêtant le montant provisoire de la prestation de compensation. Le ministre chargé des personnes handicapées peut fixer par arrêté les conditions particulières dans lesquelles l'urgence est attestée (...) ". <br/>
<br/>
              6. Il résulte de ces dispositions que, lors de l'introduction d'une demande de prestation de compensation du handicap ou au cours de son instruction, le président du conseil départemental peut être saisi en urgence aux fins d'accorder la prestation à titre provisoire. Il appartient à la commission départementale des droits et de l'autonomie des personnes handicapées, appelée à se prononcer sur la situation de l'intéressé postérieurement à la décision prise en urgence par le président du conseil départemental, de fixer définitivement ses droits à la prestation de compensation depuis la date à laquelle elle a été demandée. La décision de la commission des droits et de l'autonomie des personnes handicapées se substitue à celle du président du conseil départemental sans pouvoir remettre en cause les versements déjà effectués. Par suite, il résulte des dispositions du quatrième alinéa de l'article L. 245-2 du code de l'action sociale et des familles, dans leur rédaction applicable au litige, qui attribuaient à la juridiction du contentieux technique de la sécurité sociale les recours contre les décisions de la commission des droits et de l'autonomie des personnes handicapées relatives à l'attribution de la prestation de compensation, que cette compétence s'étend aux décisions prises à titre provisoire par le président du conseil départemental, ainsi qu'aux demandes de réparation des conséquences dommageables de ces décisions. <br/>
<br/>
              7. Il résulte de ce qui précède que la requête présentée par Mme A...'B..., relative aux refus du président du conseil départemental, statuant en urgence, de modifier à titre provisoire le montant de la prestation de compensation, ainsi qu'à la réparation des préjudices pouvant en être résultés, se rapporte à un litige qui, ainsi que l'a jugé le tribunal administratif de Montpellier, ne relève pas de la compétence de la juridiction administrative. Elle ne peut, dès lors, qu'être rejetée. <br/>
<br/>
              8. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions du département de l'Hérault présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de Mme A...'B... est rejetée.<br/>
Article 2 : Les conclusions du département de l'Hérault présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme F... A...'B... et au département de l'Hérault.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
