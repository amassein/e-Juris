<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042043621</ID>
<ANCIEN_ID>JG_L_2020_06_000000421399</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/04/36/CETATEXT000042043621.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 25/06/2020, 421399</TITRE>
<DATE_DEC>2020-06-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421399</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:421399.20200625</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme A... B... a demandé au tribunal administratif de Melun d'annuler la décision du 26 décembre 2013 par laquelle le maire de Champigny-sur-Marne a prononcé son licenciement, de condamner la commune à l'indemniser pour les préjudices qu'elle a subis du fait de ce licenciement et d'ordonner sa réintégration et la reconstitution de sa carrière. Par un jugement n° 1402136 du 16 décembre 2015, le tribunal administratif de Melun a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 16PA00647 du 10 avril 2018, la cour administrative d'appel de Paris a rejeté l'appel qu'elle a formé contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 11 juin et 11 septembre 2018 et 12 juin 2019 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Champigny-sur-Marne la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 86-68 du 13 janvier 1986 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de Mme B... et à la SCP Foussard, Froger, avocat de la commune de Champigny-sur-Marne ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme B..., adjoint administratif de la commune de Champigny-sur-Marne qui avait été placée en disponibilité pour convenances personnelles pour une durée d'un an, a sollicité sa réintégration anticipée le 9 novembre 2011. Après l'avoir placée en position de disponibilité d'office, faute de poste disponible, le maire de la commune, par un arrêté du 26 décembre 2013, a prononcé, après avis de la commission administrative paritaire, son licenciement. Par un jugement du 16 décembre 2015, le tribunal administratif de Melun a rejeté la demande de Mme B... tendant à l'annulation de cette décision, à ce que la commune soit condamnée à réparer le préjudice qu'elle a subi du fait de son licenciement et à ce que soient ordonnées sa réintégration et la reconstitution de sa carrière. Mme B... se pourvoit en cassation contre l'arrêt du 10 avril 2018 par lequel la cour administrative d'appel de Paris a rejeté l'appel qu'elle a formé contre ce jugement.<br/>
<br/>
              2. Aux termes de la seconde phrase du deuxième alinéa de l'article 72 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " Le fonctionnaire mis en disponibilité qui refuse successivement trois postes qui lui sont proposés dans le ressort territorial de son cadre d'emploi, emploi ou corps en vue de la réintégration peut être licencié après avis de la commission administrative paritaire ". Aux termes du III de l'article 97 de la même loi: " Après trois refus d'offre d'emploi correspondant à son grade, à temps complet ou à temps non complet selon la nature de l'emploi d'origine, transmise par une collectivité ou un établissement au Centre national de la fonction publique territoriale ou au centre de gestion, le fonctionnaire est licencié ou, lorsqu'il peut bénéficier de la jouissance immédiate de ses droits à pension, admis à faire valoir ses droits à la retraite ; (...) / L'offre d'emploi doit être ferme et précise, prenant la forme d'une proposition d'embauche comportant les éléments relatifs à la nature de l'emploi et à la rémunération. Le poste proposé doit correspondre aux fonctions précédemment exercées ou à celles définies dans le statut particulier du cadre d'emplois de l'agent (...) ". Aux termes du troisième alinéa de l'article 26 du décret du 13 janvier 1986 relatif aux positions de détachement, hors cadres, de disponibilité, de congé parental des fonctionnaires territoriaux et à l'intégration : " Le fonctionnaire qui a formulé avant l'expiration de la période de mise en disponibilité une demande de réintégration est maintenu en disponibilité jusqu'à ce qu'un poste lui soit proposé dans les conditions prévues à l'article 97 de la loi du 26 janvier 1984 précitée ". <br/>
<br/>
              3. Il résulte de ces dispositions que le fonctionnaire territorial bénéficiant d'une disponibilité pour convenances personnelles qui sollicite sa réintégration mais refuse successivement trois offres d'emploi fermes et précises peut être licencié après avis de la commission administrative paritaire. Chacune de ces offres d'emploi prend la forme d'une proposition d'embauche comportant les éléments relatifs à la nature de l'emploi et à la rémunération.<br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que la commune de Champigny-sur-Marne a adressé à Mme B..., le 7 février 2012, un courrier l'informant de la vacance de trois postes correspondant à son statut. Ce courrier, auquel étaient jointes trois fiches de poste diffusées au personnel communal et datées des 17 et 24 janvier et 6 février 2012, l'invitait à adresser à la commune un curriculum vitae et une lettre de motivation afin de " faciliter l'examen de votre candidature au regard des aptitudes requises pour ces postes par les chefs de service concernés " et précisait " qu'un entretien avec chacun d'eux sera alors organisé pour apprécier l'adéquation entre votre profil de compétences et les exigences des postes à pourvoir, ainsi que votre motivation pour ces postes ". Dans ces conditions, alors que le courrier du 7 février 2012 subordonnait le recrutement de Mme B... à la réalisation de différentes conditions soumises à l'appréciation de la commune et ne constituait donc pas une proposition d'embauche, la cour administrative d'appel a commis une erreur de qualification juridique en jugeant qu'il pouvait être regardé comme une offre d'emploi ferme et précise au sens des dispositions précitées. Dès lors, Mme B... est fondée, sans qu'il soit besoin de se prononcer sur l'autre moyen du pourvoi, à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Champigny-sur-Marne une somme de 3 000 euros à verser à Mme B... au titre de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce que soit mise à la charge de Mme B..., qui n'est pas, dans la présente instance, la partie perdante, la somme que demande, au même titre, la commune de Champigny-sur-Marne.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 10 avril 2018 de la cour administrative d'appel de Paris est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : La commune de Champigny-sur-Marne versera la somme de 3 000 à Mme B... au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la commune de Champigny-sur-Marne au titre des mêmes dispositions sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme A... B... et à la commune de Champigny-sur-Marne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-08-02-02-01-02 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. QUALIFICATION JURIDIQUE DES FAITS. - CARACTÈRE FERME ET PRÉCIS DES OFFRES D'EMPLOI DONT LE REFUS A JUSTIFIÉ LE LICENCIEMENT D'UN FONCTIONNAIRE TERRITORIAL EN DISPONIBILITÉ AYANT SOLLICITÉ SA RÉINTÉGRATION (III DE L'ART. 97 DE LA LOI DU 26 JANVIER 1984).
</SCT>
<ANA ID="9A"> 54-08-02-02-01-02 Le juge de cassation exerce un contrôle de qualification juridique des faits sur le caractère ferme et précis, au sens du III de l'article 97 de la loi n° 84-53 du 26 janvier 1984, des trois offres d'emploi devant être présentées à un fonctionnaire territorial en disponibilité pour convenances personnelles sollicitant sa réintégration, dont le refus permet de prononcer le licenciement de l'intéressé.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
