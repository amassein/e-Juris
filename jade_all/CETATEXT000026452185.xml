<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026452185</ID>
<ANCIEN_ID>JG_L_2012_09_000000362405</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/45/21/CETATEXT000026452185.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 18/09/2012, 362405, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-09-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362405</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Edmond Honorat</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2012:362405.20120918</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée le 31 août 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par Mme Erdenepurev B épouse C, domiciliée ... ; Mme C demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1207972 du 17 août 2012 par laquelle le juge des référés du tribunal administratif de Nantes, statuant sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative, a rejeté sa demande tendant à ce qu'il soit enjoint au préfet de la Loire-Atlantique de faire bénéficier la famille C d'une solution d'hébergement dans les 24 heures suivant le prononcé de l'ordonnance à intervenir, sous astreinte de cent euros par jour de retard ;<br/>
<br/>
              2°) de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) de l'admettre à titre provisoire au bénéfice de l'aide juridictionnelle ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat le versement de la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              elle soutient que : <br/>
              - en retenant qu'il n'était pas établi qu'elle et sa famille étaient totalement démunies de solution d'hébergement, l'ordonnance attaquée est entachée d'une erreur manifeste d'appréciation ;<br/>
              - la condition d'urgence est remplie ;<br/>
              - sa famille se trouve dans une situation d'extrême précarité, sans hébergement ;<br/>
              - elle est atteinte d'une maladie la rendant particulièrement vulnérable ;<br/>
              - il existe une atteinte grave et manifestement illégale à une liberté fondamentale ;<br/>
              - elle et sa famille se trouvent dans une situation de détresse médicale, psychique ou sociale ;<br/>
              - il existe une carence de l'Etat dans la mise en oeuvre du droit à l'hébergement d'urgence ;<br/>
<br/>
<br/>
              Vu l'ordonnance attaquée ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 12 septembre 2012, présenté par le ministre des affaires sociales et de la santé, qui conclut au non-lieu à statuer et au rejet des conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative ; il soutient que, compte tenu des nouveaux éléments recueillis par l'administration, qui attestent la situation de détresse sociale de la famille C, le préfet de la Loire-Atlantique est parvenu à ce qu'un hébergement soit trouvé, deux chambres ayant ainsi été réservées dans un hôtel pour la période du 11 au 27 septembre 2012 ; <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'action sociale et des familles ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, Mme C, d'autre part, le ministre des affaires sociales et de la santé ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 13 septembre 2012 à 14 heures 30 au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Uzan-Sarano, avocat au Conseil d'Etat et à la Cour de cassation, avocat de Mme C ;<br/>
<br/>
              - les représentantes du ministre des affaires sociales et de la santé ;<br/>
<br/>
              et à l'issue de laquelle l'instruction a été prolongée jusqu'au vendredi 14 septembre à 12 heures ;<br/>
<br/>
<br/>
<br/>
              1. Considérant que Mme C demande qu'il soit enjoint au préfet de la Loire-Atlantique de lui fournir, ainsi qu'à sa famille, un hébergement d'urgence ; qu'il résulte de l'instruction, notamment des éléments versés au cours et à l'issue de l'audience, que la requérante et sa famille bénéficient, depuis le 11 septembre et jusqu'au 27 septembre 2012, d'un hébergement d'urgence dans un hôtel et que l'administration a engagé un réexamen de la situation des intéressés en vue de la prolongation éventuelle de cet hébergement, compte tenu notamment de l'état de santé de la requérante et de la présence au foyer d'un enfant en bas âge ; que, dans ces conditions, les conclusions de Mme C aux fins d'injonction sont devenues sans objet ; qu'il n'y a plus lieu, en conséquence, d'y statuer ; <br/>
<br/>
              2. Considérant qu'il n'y a pas lieu d'admettre provisoirement Mme C au bénéfice de l'aide juridictionnelle, ni, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge de l'Etat la somme que demande Mme Tsedendori au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1 : Il n'y a pas lieu de statuer sur les conclusions aux fins d'injonction de Mme C.<br/>
Article 2 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 3 : La présente ordonnance sera notifiée à Mme Erdenepurev B épouse C et à la ministre des affaires sociales et de la santé.<br/>
Copie en sera adressée au préfet de la Loire-Atlantique.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
