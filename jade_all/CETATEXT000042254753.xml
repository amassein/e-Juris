<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042254753</ID>
<ANCIEN_ID>JG_L_2020_08_000000442738</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/25/47/CETATEXT000042254753.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 14/08/2020, 442738, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-08-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>442738</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:442738.20200814</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au juge des référés du tribunal administratif de Marseille, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au préfet des Bouches du Rhône d'enregistrer sa demande d'asile et de lui remettre une attestation de demandeur d'asile en procédure normale, ainsi que de lui remettre le dossier destiné à l'Office français de protection des réfugiés et apatrides (OFPRA), sous astreinte de 100 euros par jour de retard à compter de la notification de l'ordonnance.<br/>
<br/>
              Par une ordonnance n° 2005135 du 22 juillet 2020, le juge des référés du tribunal administratif de Marseille a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 12 août 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à ses demandes de première instance, dans un délai de sept jours à compter de la présente ordonnance ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - le juge des référés du tribunal administratif de Marseille a dénaturé la notion de fuite en estimant que son seul refus d'embarquement constituait une soustraction intentionnelle à son transfert justifiant cette qualification, en dépit de l'absence de caractère systématique d'une telle obstruction et de ses explications sur le motif de son refus d'embarquer ;<br/>
              - le juge des référés, en relevant que son comportement laissait présager des refus ultérieurs d'accepter son transfert, s'est fondé à tort sur un évènement hypothétique.<br/>
<br/>
<br/>
<br/>
              	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement (CE) n° 1560/2003 de la Commission du 2 septembre 2003 ;<br/>
              - le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le règlement (UE) n° 118/2014 de la Commission du 30 janvier 2014 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. M. A... B..., ressortissant afghan, après avoir déposé une demande d'asile en Autriche le 5 décembre 2015, est entré en France en juin 2019 où il s'est vu délivrer une attestation de demandeur d'asile en procédure Dublin par la préfecture des Bouches-du-Rhône le 13 juin 2019. Les autorités autrichiennes ayant donné leur accord le 1er août 2019, le préfet des Bouches-du-Rhône a, par un arrêté du 27 août 2019, décidé du transfert vers l'Autriche de M. B.... Par un jugement du 9 septembre 2019, le tribunal administratif de Marseille a rejeté sa demande tendant à l'annulation de cet arrêté. L'appel formé par l'intéressé contre ce jugement devant la cour administrative d'appel de Marseille a été rejeté le 15 juillet 2020. M. B... relève appel de l'ordonnance du 22 juillet 2020 par laquelle le juge des référés du tribunal administratif de Marseille, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté sa demande tendant à ce qu'il soit enjoint au préfet des Bouches du Rhône d'enregistrer sa demande d'asile et de lui remettre une attestation de demandeur d'asile en procédure normale, ainsi que de lui remettre le dossier destiné à l'Office français de protection des réfugiés et apatrides (OFPRA). <br/>
              3. Le droit constitutionnel d'asile, qui a le caractère d'une liberté fondamentale, a pour corollaire le droit de solliciter le statut de réfugié.  S'il implique que l'étranger qui sollicite la reconnaissance de la qualité de réfugié soit en principe autorisé à demeurer sur le territoire jusqu'à ce qu'il ait été statué sur sa demande, ce droit s'exerce dans les conditions définies par les articles L. 741-1 et suivants du code de l'entrée et du séjour des étrangers et du droit d'asile. En vertu de l'article L. 742-3 de ce code, l'étranger dont l'examen de la demande d'asile relève de la responsabilité d'un autre Etat peut faire l'objet d'un transfert vers l'Etat qui est responsable de cet examen en application des dispositions du règlement du Parlement européen et du Conseil du 26 juin 2013 établissant les critères et mécanismes de détermination de l'Etat membre responsable de l'examen d'une demande de protection internationale introduite dans l'un des Etats membres par un ressortissant de pays tiers ou un apatride.<br/>
<br/>
              4. L'article 29 du règlement du 26 juin 2013 prévoit que le transfert peut avoir lieu pendant une période de six mois à compter de l'acceptation de la demande de prise en charge, cette période étant susceptible d'être portée à dix-huit mois si l'intéressé " prend la fuite ". Aux termes de l'article 7 du règlement de la Commission du 2 septembre 2003 : " 1. Le transfert vers l'Etat responsable s'effectue de l'une des manières suivantes : a) à l'initiative du demandeur, une date limite étant fixée ; b) sous la forme d'un départ contrôlé, le demandeur étant accompagné jusqu'à l'embarquement par un agent de l'Etat requérant et le lieu, la date et l'heure de son arrivée étant notifiées à l'Etat responsable dans un délai préalable convenu ; c) sous escorte, le demandeur étant accompagné par un agent de l'Etat requérant, ou par le représentant d'un organisme mandaté par l'Etat requérant à cette fin, et remis aux autorités de l'Etat responsable (...) ". Ainsi, le transfert d'un demandeur d'asile vers un Etat membre qui a accepté sa prise ou sa reprise en charge, sur le fondement du règlement du 26 juin 2013, s'effectue selon l'une des trois modalités définies à l'article 7 cité ci-dessus : à l'initiative du demandeur, sous la forme d'un départ contrôlé ou sous escorte.<br/>
<br/>
              5. Il résulte clairement des dispositions mentionnées au point précédent que, d'une part, la notion de fuite doit s'entendre comme visant le cas où un ressortissant étranger se serait soustrait de façon intentionnelle et systématique au contrôle de l'autorité administrative en vue de faire obstacle à une mesure d'éloignement le concernant. D'autre part, dans l'hypothèse où le transfert du demandeur d'asile s'effectue sous la forme d'un départ contrôlé, il appartient, dans tous les cas, à l'Etat responsable de ce transfert d'en assurer effectivement l'organisation matérielle et d'accompagner le demandeur d'asile jusqu'à l'embarquement vers son lieu de destination. Une telle obligation recouvre la prise en charge du titre de transport permettant de rejoindre l'Etat responsable de l'examen de la demande d'asile depuis le territoire français ainsi que, le cas échéant et si nécessaire, celle du pré-acheminement du lieu de résidence du demandeur au lieu d'embarquement. Enfin, dans l'hypothèse où le demandeur d'asile se soustrait intentionnellement à l'exécution de son transfert ainsi organisé, il doit être regardé comme en fuite au sens des dispositions de l'article 29 du règlement du 26 juin 2013 rappelées au point 4.<br/>
<br/>
              6. Il résulte de l'instruction conduite par le juge des référés du tribunal administratif de Marseille, et il n'est d'ailleurs pas contesté, que M. B..., interpellé le 14 janvier 2020 et placé en rétention pour permettre l'exécution de l'arrêté de transfert, a refusé d'embarquer le lendemain sur un vol prévu à destination de Vienne, alors que son départ avait été organisé. L'intéressé se borne à expliquer son refus par l'existence de l'appel alors pendant qu'il avait introduit devant la cour administrative d'appel contre le jugement du tribunal administratif de Marseille ayant rejeté sa demande d'annulation de l'arrêté de transfert et qu'il avait assorti d'une demande de sursis à exécution. Un tel appel ne revêt cependant pas de caractère suspensif. Compte tenu de ce qui a été dit au point 5, M. B... n'est donc pas fondé à soutenir que c'est à tort que le juge des référés du tribunal administratif de Marseille a estimé qu'il devait être regardé comme ayant pris la fuite, et donc, par voie de conséquence, que le délai de son transfert vers l'Autriche avait été porté à dix-huit mois et que la France n'était pas devenue responsable du traitement de sa demande d'asile. Il est à cet égard sans incidence que le juge des référés ait relevé, par un motif surabondant, que son comportement laissait en outre présager des refus ultérieurs.<br/>
<br/>
              7. Il résulte de ce qui précède qu'il est manifeste que l'appel de M. B... ne peut être accueilli. Il y a donc lieu de rejeter sa requête, y compris les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative, selon la procédure prévue par l'article L. 522-3 de ce code.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
