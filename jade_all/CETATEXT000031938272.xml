<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031938272</ID>
<ANCIEN_ID>JG_L_2016_01_000000377907</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/93/82/CETATEXT000031938272.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème SSR, 20/01/2016, 377907, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-01-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>377907</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:377907.20160120</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Strasbourg de prononcer la décharge des pénalités, restant à sa charge au terme d'une remise gracieuse partielle prononcée par l'administration, appliquées aux cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles elle a été assujettie au titre de l'année 2008. Par un jugement n° 0905768 du 6 décembre 2012, le tribunal administratif de Strasbourg a, d'une part, prononcé la décharge des intérêts de retard mis à sa charge et, d'autre part, rejeté le surplus des conclusions de sa demande.<br/>
<br/>
              Par un arrêt n° 13NC00206 du 18 février 2014, la cour administrative d'appel de Nancy a, sur l'appel de MmeB..., prononcé la décharge du solde maintenu à sa charge de la majoration prévue par l'article 1758 A du code général des impôts, réformé le jugement attaqué du tribunal administratif de Strasbourg en ce qu'il avait de contraire à son arrêt et rejeté le surplus de ses conclusions. <br/>
<br/>
              Par un pourvoi enregistré le 16 avril 2014 au secrétariat du contentieux du Conseil d'Etat, le ministre des finances et des comptes publics demande au Conseil d'Etat d'annuler cet arrêt en tant qu'il a prononcé la décharge du solde de la majoration prévue par l'article 1758 A du code général des impôts et réformé le jugement attaqué du tribunal administratif de Strasbourg en ce qu'il avait de contraire à son arrêt.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ;<br/>
              - le code général des impôts ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bastien Lignereux, auditeur,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de Mme B...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 202 du code général des impôts : " 1. Dans le cas de cessation de l'exercice d'une profession non commerciale, l'impôt sur le revenu dû en raison des bénéfices provenant de l'exercice de cette profession (...) est immédiatement établi. / Les contribuables doivent, dans un délai de soixante jours déterminé comme il est indiqué ci-après, aviser l'administration de la cessation et lui faire connaître la date à laquelle elle a été ou sera effective, ainsi que, s'il y a lieu, les nom, prénoms et adresse du successeur. / Ce délai de soixante jours commence à courir : / a. lorsqu'il s'agit de la cessation de l'exercice d'une profession autre que l'exploitation d'une charge ou d'un office, du jour où la cessation a été effective ; / (...) 2. Les contribuables sont tenus de faire parvenir à l'administration dans le délai prévu au 1 la déclaration visée à l'article 97 ou au 2 de l'article 102 ter. / Si les contribuables ne produisent pas la déclaration visée au premier alinéa, les bases d'imposition sont arrêtées d'office. (...) " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société civile professionnelle créée entre les docteurs Rothgerber, Tritschler et Wattebled-Phillips, médecins anesthésistes, a été transformée le 25 septembre 2008 en une société d'exercice libéral à responsabilité limitée ayant les mêmes associés ; que, sur le fondement des dispositions précitées de l'article 202 du code général des impôts, l'administration fiscale a immédiatement établi, entre les mains de ses associés, l'impôt sur le revenu et les contributions sociales dues à raison des bénéfices réalisés par la société civile professionnelle du 1er janvier au 30 septembre 2008 ; qu'en l'absence de dépôt par Mme B...de la déclaration prévue au 2 de l'article 202 de ce code, les impositions mises à sa charge ont été assorties des majorations de 10 % prévues respectivement par les articles 1728 et 1758 A du code général des impôts ; qu'après que l'administration fiscale, statuant sur sa réclamation, a prononcé une remise gracieuse partielle des pénalités afférentes aux cotisations d'impôt sur le revenu, elle a demandé au tribunal administratif de Strasbourg de prononcer la décharge du solde des pénalités litigieuses ; que, par un jugement du 6 décembre 2012, ce tribunal a accordé la décharge des intérêts de retard et rejeté le surplus de cette demande ; que le ministre des finances et des comptes publics se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Nancy du 18 février 2014 en tant que par cet arrêt, avant de rejeter le surplus de l'appel de Mme B..., la cour a prononcé la décharge du solde maintenu à sa charge de la majoration prévue par l'article 1758 A du code général des impôts et réformé le jugement attaqué en ce qu'il avait de contraire ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article 1728 du code général des impôts, dans sa rédaction applicable à la date des pénalités litigieuses : " 1. Le défaut de production dans les délais prescrits d'une déclaration ou d'un acte comportant l'indication d'éléments à retenir pour l'assiette ou la liquidation de l'impôt entraîne l'application, sur le montant des droits mis à la charge du contribuable ou résultant de la déclaration ou de l'acte déposé tardivement, d'une majoration de : / a. 10 % en l'absence de mise en demeure ou en cas de dépôt de la déclaration ou de l'acte dans les trente jours suivant la réception d'une mise en demeure, notifiée par pli recommandé, d'avoir à le produire dans ce délai ; (...) " ; qu'aux termes de l'article 1758 A du même code : " I.-Le retard ou le défaut de souscription des déclarations qui doivent être déposées en vue de l'établissement de l'impôt sur le revenu ainsi que les inexactitudes ou les omissions relevées dans ces déclarations, qui ont pour effet de minorer l'impôt dû par le contribuable ou de majorer une créance à son profit, donnent lieu au versement d'une majoration égale à 10 % des droits supplémentaires ou de la créance indue. / II.-Cette majoration n'est pas applicable : / a) En cas de régularisation spontanée ou lorsque le contribuable a corrigé sa déclaration dans un délai de trente jours à la suite d'une demande de l'administration ; / b) Ou lorsqu'il est fait application des majorations prévues par les b et c du 1 de l'article 1728, par l'article 1729 ou par le a de l'article 1732. " ;<br/>
<br/>
              4. Considérant, en premier lieu, que contrairement à ce que soutient le ministre, la cour n'a pas, pour faire partiellement droit à l'appel dont elle était saisie, soulevé d'office le moyen tiré de l'inapplicabilité des dispositions de l'article 1758 A du code général des impôts, qui était soulevé devant elle ; que, par suite, le moyen tiré de ce que son arrêt aurait été rendu au terme d'une procédure irrégulière, faute d'avoir informé les parties de ce que sa décision était susceptible d'être fondée sur ce moyen, ne peut être accueilli ;<br/>
<br/>
              5. Considérant, en second lieu, que le quantum de la sanction prévue par l'article 1758 A du code général des impôts est défini en fonction des droits supplémentaires ou de la créance indue ; que, dès lors et alors même que cette sanction entend réprimer non seulement les inexactitudes ou les omissions relevées dans les déclarations qu'elle mentionne, mais également leur retard ou défaut de souscription, cette sanction ne peut être appliquée, compte tenu de la lettre de cet article, qu'à des droits supplémentaires, à l'exclusion des impositions initiales ; que, par suite, la cour n'a pas commis d'erreur de droit en jugeant, pour accorder la décharge de cette majoration, que celle-ci ne peut être cumulée avec celle prévue par les dispositions du a du 1 de l'article 1728 du même code lorsque les impositions contestées sont, comme en l'espèce, des impositions initiales ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que le ministre des finances et des comptes publics n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à Mme B... d'une somme de 3 500 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi présenté par le ministre des finances et des comptes publics est rejeté.<br/>
Article 2 : L'Etat versera à Mme B...une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au ministre des finances et des comptes publics et à Mme A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
