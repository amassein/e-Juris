<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029691339</ID>
<ANCIEN_ID>JG_L_2014_11_000000382413</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/69/13/CETATEXT000029691339.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème sous-section jugeant seule, 03/11/2014, 382413, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-11-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382413</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET ; SCP HEMERY, THOMAS-RAQUIN</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:382413.20141103</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et les mémoires complémentaires, enregistrés les 9, 22 et 25 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'Office national des forêts (ONF), dont le siège est 2, avenue de Saint-Mandé à Paris (75570), représenté par son directeur général ; l'Office national des forêts demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1401214 du 25 juin 2014 par laquelle le juge des référés du tribunal administratif de Châlons-en-Champagne, statuant sur le fondement de l'article L. 551-1 du code de justice administrative, sur la demande de la société MPF, en ce qu'elle a enjoint à la communauté de communes de la vallée du Rognon de ne pas signer le marché public de travaux de restauration de la vallée du Rognon, de l'abbaye de Lacrète jusqu'au pont d'Andelot ; <br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société MPF ;<br/>
<br/>
              3°) de mettre à la charge de la société MPF le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des marchés publics ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Nervo, Poupet, avocat de l'Office national des forêts (ONF), et à la SCP Hémery, Thomas-Raquin, avocat de la société MPF ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation, ou la délégation d'un service public (...) " ; qu'aux termes de l'article L. 551-2 de ce code : " I. Le juge peut ordonner à l'auteur du manquement de se conformer à ses obligations et suspendre l'exécution de toute décision qui se rapporte à la passation du contrat, sauf s'il estime, en considération de l'ensemble des intérêts susceptibles d'être lésés et notamment de l'intérêt public, que les conséquences négatives de ces mesures pourraient l'emporter sur leurs avantages. (...) " ; que, selon l'article L. 551-10 du même code : " Les personnes habilitées à engager les recours prévus aux articles L. 551-1 et L. 551-5 sont celles qui ont un intérêt à conclure le contrat et qui sont susceptibles d'être lésées par le manquement invoqué (...) " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Châlons-en-Champagne que la communauté de communes de la vallée du Rognon a lancé une procédure d'appel d'offres en vue de l'attribution d'un marché public de travaux de restauration d'un tronçon des berges du Rognon ; que la société MPF, candidate à ce marché, a été informée que l'offre de l'Office national des forêts (ONF) était retenue ; que par l'ordonnance attaquée, le juge des référés, saisi par la société MPF, a, sur le fondement de l'article L. 551-1 du code de justice administrative, enjoint à la communauté de communes de ne pas signer le marché avec l'ONF ; <br/>
<br/>
              Sur les conclusions à fins de non-lieu présentées par la société MPF :<br/>
<br/>
              3. Considérant qu'il résulte de l'instruction que la communauté de communes de la vallée du Rognon a indiqué dans un courrier du 11 juillet 2014 adressé à l'ONF qu'elle décidait de ne pas donner suite au marché en cause à raison des manquements aux obligations de mises en concurrence relevés par le juge des référés du tribunal administratif de Châlons-en-Champagne dans l'ordonnance attaquée et qu'elle lancerait ultérieurement un nouveau marché ; que cette renonciation à conclure le contrat résulte ainsi seulement de l'ordonnance attaquée et n'est pas fondée sur un motif d'intérêt général ; que par ailleurs, il n'est pas établi ni même allégué qu'un nouveau marché aurait été signé par le pouvoir adjudicateur ; que, dans ces conditions, les conclusions de la société MPF tendant à ce que soit prononcé dans la présente instance un non-lieu à statuer doivent être rejetées ;<br/>
<br/>
              Sur les conclusions de l'ONF tendant à l'annulation de l'ordonnance attaquée :<br/>
<br/>
              4. Considérant qu'aux termes de l'article 1er du code des marchés publics, ceux-ci respectent les principes de liberté d'accès à la commande publique, d'égalité de traitement des candidats et de transparence des procédures ; que selon l'article 53 du même code : " I. - Pour attribuer le marché au candidat qui a présenté l'offre économiquement la plus avantageuse, le pouvoir adjudicateur se fonde : / 1° Soit sur une pluralité de critères non discriminatoires et liés à l'objet du marché (...) 2° Soit, compte tenu de l'objet du marché, sur un seul critère, qui est celui du prix. / (...) III. - Les offres inappropriées, irrégulières et inacceptables sont éliminées. Les autres offres sont classées par ordre décroissant. L'offre la mieux classée est retenue (...) " ; que l'article 55 de ce même code dispose que : " Si une offre paraît anormalement basse, le pouvoir adjudicateur peut la rejeter par décision motivée après avoir demandé par écrit les précisions qu'il juge utiles et vérifié les justifications fournies (...) Peuvent être prises en considération des justifications tenant notamment aux aspects suivants : / 1° Les modes de fabrication des produits, les modalités de la prestation des services, les procédés de construction ; / 2° Les conditions exceptionnellement favorables dont dispose le candidat pour exécuter les travaux, pour fournir les produits ou pour réaliser les prestations de services ; 3° L'originalité de l'offre ; / 4° Les dispositions relatives aux conditions de travail en vigueur là où la prestation est réalisée ; / 5° L'obtention éventuelle d'une aide d'Etat par le candidat. (...) " ;<br/>
<br/>
              5. Considérant que pour juger que la communauté de communes avait commis une erreur manifeste d'appréciation en retenant l'offre anormalement basse de l'ONF, le juge des référés du tribunal administratif de Châlons-en-Champagne s'est borné à relever que cette offre était très inférieure au prix proposé par la société MPF, à l'estimation du coût des travaux faite par le pouvoir adjudicateur ainsi qu'au prix des précédents marchés de restauration des autres tronçons de berges du Rognon conclus les années précédentes ; qu'en statuant ainsi, pour juger que l'offre de l'ONF était anormalement basse, par la seule comparaison de cette offre avec des offres concurrentes ou passées ou encore avec les estimations de prix du pouvoir adjudicateur, sans rechercher si le prix proposé par l'ONF était en lui-même manifestement sous-évalué et susceptible de compromettre la bonne exécution du marché en cause, le juge des référés a commis une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'ONF est fondé à demander l'annulation des articles 1er, 2 et 4 de l'ordonnance attaquée ; <br/>
<br/>
              6. Considérant que, dans les circonstances de l'espèce, il y a lieu, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée par la société MPF, dans la mesure de la cassation prononcée ;<br/>
<br/>
              7. Considérant qu'il résulte de l'instruction que la société MPF, pour établir que la communauté de communes aurait commis une erreur manifeste d'appréciation en retenant l'offre anormalement basse de l'ONF, se borne à comparer son montant avec le montant de sa propre offre ainsi qu'avec le montant des marchés de restauration des autres portions du Rognon conclus les années précédentes et avec les estimations du prix du marché en alléguant que les prestations proposées par l'ONF seraient de mauvaise qualité ; que la requérante n'apporte pas de précision ou justification de nature à justifier que l'offre de l'ONF puisse être regardée comme manifestement sous-évaluée et de nature, ainsi, à compromettre la bonne exécution du marché, notamment en ce qui concerne les composantes du prix relatives au volume d'heure de travail et ressources en personnel et matériel requises, compte tenu des prestations exigées au cahier des clauses techniques et particulières annexé au marché tant en ce qui concerne la nature des berges concernées que les arbres à traiter avec leurs caractéristiques et situation ; que par suite, la requérante n'est pas fondée à soutenir que la communauté de communes de la vallée du Rognon aurait commis une erreur manifeste d'appréciation en retenant une offre anormalement basse ; que ses conclusions tendant à ce qu'il soit enjoint à la communauté de communes de la vallée du Rognon de ne pas signer le marché public de travaux de restauration de la vallée du Rognon, de l'abbaye de Lacrète jusqu'au pont d'Andelot ne peuvent donc qu'être rejetées ;  <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède, que les conclusions présentées par la société MPF au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées, l'ONF n'étant pas la partie perdante dans la présente instance ; qu'il y a lieu en revanche de mettre à la charge de la société MPF, en application de ces mêmes dispositions, la somme de 4 500 euros à verser à l'ONF au titre des frais exposés par lui et non compris dans les dépens, tant devant le tribunal administratif de Châlons-en-Champagne que devant le Conseil d'Etat, ainsi que la somme de 2 000 euros à verser à la communauté de communes du Rognon pour la procédure suivie devant le tribunal administratif ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er, 2 et 4 de l'ordonnance du juge des référés du tribunal administratif de Châlons-en-Champagne sont annulés.<br/>
Article 2 : La demande de la société MPF tendant à ce qu'il soit enjoint à la communauté de communes de la vallée du Rognon de ne pas signer le marché public de travaux de restauration de la vallée du Rognon, de l'abbaye de Lacrète jusqu'au pont d'Andelot est rejetée.<br/>
Article 3 : La société MPF versera à l'ONF une somme de 4 500 euros et à la communauté de communes de la vallée du Rognon une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à l'Office national des forêts, à la société MPF et à la communauté des communes de la vallée du Rognon.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
