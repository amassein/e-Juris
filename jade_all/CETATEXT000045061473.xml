<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000045061473</ID>
<ANCIEN_ID>JG_L_2021_12_000000445667</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/45/06/14/CETATEXT000045061473.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 28/12/2021, 445667, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445667</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Myriam Benlolo Carabot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Domingo</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:445667.20211228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 26 octobre 2020 au secrétariat du contentieux du Conseil d'Etat, l'association Le Cercle droit et liberté, M. BF... AS..., M. BM... R..., M. C... AX..., M. K... BD..., M. BG... AP..., Mme AD... BB..., Mme Y... AV..., M. BN... A..., M. F... E..., M. Q... BI..., M. K... BL..., M. AC... AG..., Mme AW... W..., M. AU... BH..., Mme AA... O..., M. BE... H..., Mme U... AB..., Mme N... BK..., M. AM... Z..., M. P... AK..., M. AJ... AI... , Mme BC... BA..., M. G... V...,  M. AF... AT..., M. B... S..., M. T... I..., M. AZ... AH..., Mme X... L...,  M. BP..., M. AL... D..., M. AJ... AN..., M. BO... M..., Mme BJ... AO..., Mme AY... AR... et M. J... d'Anthouard demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir les dispositions du I de l'article 51 du décret n° 2020-1262 du 16 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 10 000 euros au titre de l'article L. 761 1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le décret n° 2020-1257 du 14 octobre 2020 ;<br/>
              - le décret n° 2020-1262 du 16 octobre 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Myriam Benlolo Carabot, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Domingo, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre des solidarités et de la santé puis le Premier ministre à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Pour faire face à l'aggravation de l'épidémie, le législateur, par l'article 4 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19, a déclaré l'état d'urgence sanitaire pour une durée de deux mois à compter du 24 mars 2020, puis, par l'article 1er de la loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ses dispositions, a prorogé cet état d'urgence sanitaire jusqu'au 10 juillet 2020 inclus. Au vu de l'évolution de la situation sanitaire, les mesures générales adoptées par décret ont assoupli progressivement les sujétions imposées afin de faire face à l'épidémie. Enfin, par un décret du 10 juillet 2020, pris sur le fondement de la loi du 9 juillet 2020 organisant la sortie de l'état d'urgence sanitaire, le Premier ministre a prescrit les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans les territoires sortis de l'état d'urgence sanitaire et dans ceux au sein desquels il a été prorogé.<br/>
<br/>
              2. Par un décret n° 2020-1257 du 14 octobre 2020, pris en conseil des ministres et sur le rapport du ministre des solidarités et de la santé, le Président de la République a déclaré l'état d'urgence sanitaire sur l'ensemble du territoire de la République à compter du 17 octobre 2020, sur le fondement des dispositions de l'article L. 3131-13 du code de la santé publique.<br/>
<br/>
              3. Aux termes de l'article L. 3131-15 du code de la santé publique, dans sa rédaction issue de la loi du 11 mai 2020 : " I. - Dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le Premier ministre peut, par décret réglementaire pris sur le rapport du ministre chargé de la santé, aux seules fins de garantir la santé publique : (...) 2° Interdire aux personnes de sortir de leur domicile, sous réserve des déplacements strictement indispensables aux besoins familiaux ou de santé ; (...) III. - Les mesures prescrites en application du présent article sont strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu. Il y est mis fin sans délai lorsqu'elles ne sont plus nécessaires. " <br/>
<br/>
              4. Dans ce cadre, par un décret n° 2020-1262 du 16 octobre 2020, le Premier ministre a prescrit de nouvelles mesures générales nécessaires pour faire face à l'épidémie de covid-19. En particulier, le I de l'article 51 de ce décret prévoit que " I. - Dans les départements mentionnés à l'annexe 2, le préfet de département interdit, dans les zones qu'il définit, aux seules fins de lutter contre la propagation du virus, les déplacements de personnes hors de leur lieu de résidence entre 21 heures et 6 heures du matin à l'exception des déplacements pour les motifs suivants, en évitant tout regroupement de personnes : 1° Déplacements entre le domicile et le lieu d'exercice de l'activité professionnelle ou le lieu d'enseignement et de formation ; 2° Déplacements pour des consultations et soins ne pouvant être assurés à distance et ne pouvant être différés ou pour l'achat de produits de santé ; 3° Déplacements pour motif familial impérieux, pour l'assistance aux personnes vulnérables ou précaires ou pour la garde d'enfants ; 4° Déplacements des personnes en situation de handicap et de leur accompagnant ; 5° Déplacements pour répondre à une convocation judiciaire ou administrative ; 6° Déplacements pour participer à des missions d'intérêt général sur demande de l'autorité administrative ; 7° Déplacements liés à des transferts ou transits vers ou depuis des gares ou aéroports dans le cadre de déplacements de longue distance ; 8° Déplacements brefs, dans un rayon maximal d'un kilomètre autour du domicile pour les besoins des animaux de compagnie. Les personnes souhaitant bénéficier de l'une des exceptions mentionnées au présent I se munissent, lors de leurs déplacements hors de leur domicile, d'un document leur permettant de justifier que le déplacement considéré entre dans le champ de l'une de ces exceptions. Les mesures prises en vertu du présent I ne peuvent faire obstacle à l'exercice d'une activité professionnelle sur la voie publique dont il est justifié dans les conditions prévues à l'alinéa précédent ".<br/>
<br/>
              5. L'association Le Cercle droit et liberté et autres demandent l'annulation pour excès de pouvoir des dispositions du I de l'article 51 du décret n° 2020-1262 du 16 octobre 2020, qui permettent au préfet d'interdire, dans les zones qu'il définit, le déplacement de toute personne hors de son domicile, sous réserve d'exceptions limitativement énumérées et devant être dûment justifiées, entre 21 heures et 6 heures du matin. Les conclusions présentées par les mêmes requérants tendant à la suspension de ces dispositions, présentées sur le fondement des dispositions des articles L. 521-1 et L. 521-2 du code de justice administrative, ont été rejetées par le juge des référés du Conseil d'Etat par une décision du 28 octobre 2020.<br/>
<br/>
              6. En période d'état d'urgence sanitaire, il appartient aux différentes autorités compétentes de prendre, en vue de sauvegarder la santé de la population, toutes dispositions de nature à prévenir ou à limiter les effets de l'épidémie. Ces mesures, qui peuvent restreindre l'exercice des droits et libertés fondamentaux, doivent, dans cette mesure, être nécessaires, adaptées et proportionnées à l'objectif de sauvegarde de la santé publique qu'elles poursuivent. Tel est en particulier le cas d'une mesure d'interdiction faite aux personnes de sortir de leur domicile durant certaines heures.<br/>
<br/>
              7. Les requérants soutiennent, en se fondant pour l'essentiel sur des considérations de portée générale qui ne sont pas utilement étayées, que les mesures dites de " couvre-feu " mentionnés au 4° auraient été disproportionnées et injustifiées au regard de l'objectif de lutte contre l'épidémie de covid-19. Ils invoquent la circonstance que les risques de contamination encourus dans les lieux privés seraient faibles, que le couvre-feu favorise la contamination dans les transports en commun aux heures de pointe, que les mesures de couvre-feu précédemment décidées auraient révélé leur inefficacité et que des mesures moins contraignantes, telles que le contrôle aux frontières, la fermeture des établissements recevant du public, l'interdiction des rassemblements dans les lieux privés de plus de dix personnes, la généralisation du télétravail ou du chômage partiel pour les personnes partageant le domicile d'une personne vulnérable et la distribution de masques FFP2 auraient dû être prévues. <br/>
<br/>
              8. Toutefois, il ressort des pièces du dossier qu'à la date à laquelle a été pris le décret attaqué, était survenue une nette aggravation de la crise sanitaire, tout particulièrement dans certaines zones à forte densité de population, sans que les mesures instituées sur le fondement de la loi du 9 juillet 2020 aient été en mesure d'empêcher durablement la reprise de l'épidémie. Entre la semaine 40 (du 28 septembre 2020 au 4 octobre 2020) et la semaine 41 (du 5 au 11 octobre), le nombre de nouveaux cas confirmés de covid-19 avait augmenté de 53%. Le taux de positivité calculé avait également sensiblement augmenté entre la semaine 40 et la semaine 41, passant de 9,2 à 12,3%. Le taux de reproduction du virus était quant à lui significativement supérieur à 1 (1,35). Au 13 octobre 2020, 8 949 patients contaminés étaient hospitalisés dont 1 642 en réanimation, avec une augmentation des nouvelles hospitalisations de 19 % par rapport à la semaine précédente. Le nombre de décès survenus en milieu hospitalier avait augmenté de 12 % d'une semaine à l'autre. Selon les indications fournies par Santé publique France, il fallait s'attendre, si la dynamique de l'épidémie se poursuivait, à un doublement du nombre hebdomadaire de nouveaux cas confirmés sous 15 jours et à un doublement du nombre hebdomadaire de nouveaux patients admis à l'hôpital dans un délai de 26 jours.<br/>
<br/>
              9. Il ressort également des pièces du dossier que cette aggravation de la crise sanitaire trouvait essentiellement sa source, selon les données scientifiques disponibles, dans les rencontres dans les lieux privés ainsi que les bars et restaurants, dans un cadre familial ou à l'occasion de regroupements sociaux avec une forte densité de personnes. Il apparaît également que l'analyse de la mesure de " couvre-feu " instituée en Guyane au printemps 2020 avait permis de mesurer son efficacité, diminuant de façon importante le taux de reproduction du virus.<br/>
<br/>
              10. Compte tenu des dérogations prévues pour les déplacements indispensables, du manque d'efficacité des mesures moins contraignantes mises en place jusque-là face à la détérioration de la situation sanitaire et du risque de voir celle-ci se dégrader encore davantage à brève échéance, le moyen tiré de ce que les dispositions contestées, à la date à laquelle elles ont été édictées, n'auraient pas revêtu un caractère nécessaire, adapté et proportionné ne peut être accueilli. Par ailleurs, si les requérants soutiennent que des activités favorables à la propagation du virus seraient demeurées autorisées, et que, par suite, les dispositions contestées méconnaîtraient le principe d'égalité, ce moyen n'est, en tout état de cause, pas assorti des précisions permettant d'en apprécier le bien-fondé.<br/>
<br/>
              11. Il résulte de ce qui précède que la requête de l'association Le Cercle droit et liberté et autres doit être rejetée, y compris les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'association Le Cercle droit et liberté et autres est rejetée. <br/>
Article 2 : La présente décision sera notifiée à l'association Le Cercle droit et liberté, première dénommée, pour l'ensemble des requérants et au ministre des solidarités et de la santé. <br/>
Copie en sera adressée au Premier ministre, au ministre de l'intérieur et au ministre des outre-mer.<br/>
              Délibéré à l'issue de la séance du 15 décembre 2021 où siégeaient : M. Bertrand Dacosta, président de chambre, présidant ; Mme Nathalie Escaut, conseillère d'Etat et Mme Myriam Benlolo Carabot, maître des requêtes en service extraordinaire-rapporteure.<br/>
<br/>
              Rendu le 28 décembre 2021.<br/>
Le président : <br/>
Signé : M. Bertrand Dacosta<br/>
La rapporteure : <br/>
Signé : Mme Myriam Benlolo Carabot<br/>
La secrétaire :<br/>
Signé : Mme AE... AQ...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
