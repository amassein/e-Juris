<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033157836</ID>
<ANCIEN_ID>JG_L_2016_09_000000384798</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/15/78/CETATEXT000033157836.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 26/09/2016, 384798, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-09-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384798</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GADIOU, CHEVALLIER ; SCP MARLANGE DE LA BURGADE</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne Von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:384798.20160926</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              La SCI Cypris et la SARL Transports Sud-Inter ont demandé au tribunal administratif de Marseille d'annuler pour excès de pouvoir les permis de construire délivrés à la SCI les Frères Réunis et la SCI Les Deux Outils par le maire de Saint-Chamas par deux arrêtés du 3 septembre 2009. Par un jugement nos 0907768, 0907855 du 22 mars 2012, le tribunal administratif de Marseille a fait droit à ces demandes. <br/>
<br/>
              Par un arrêt n° 12MA01827 du 21 juillet 2014, la cour administrative d'appel de Marseille a rejeté l'appel formé par les SCI Les Frères Réunis et Les Deux Outils contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux mémoires en réplique, enregistrés les 25 septembre et 26 décembre 2014 et les 24 mars et 31 août 2016 au secrétariat du contentieux du Conseil d'Etat, les SCI Les Frères Réunis et Les Deux Outils demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de la SCI Cypris et de la SARL Transports Sud-Inter la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gadiou, Chevallier, avocat de la SCI Les Frères Réunis et de la SCI Les Deux Outils et à la SCP Marlange de la Burgade, avocat de la SCI Cypris et de la SARL Transport Sud-Inter ;<br/>
<br/>
<br/>
<br/>
<br/>1.	Considérant qu'il ressort des énonciations de l'arrêt attaqué que, par deux arrêtés du 3 septembre 2009, le maire de Saint-Chamas a délivré, d'une part, à la SCI Les Frères Réunis, propriétaire des lots n° 2 et 3 du lotissement dénommé " zone d'activité Castellamare ", un permis de construire portant sur l'agrandissement d'un bâtiment existant à usage de bureau d'étude et la réalisation d'un local matériel et, d'autre part, à la SCI Les Deux Outils, propriétaire du lot n° 1 du même lotissement, un permis de construire portant notamment sur la création d'un vestiaire, d'un bureau et d'un mur antibruit ; que, par un jugement 22 mars 2012, à la demande de la SCI Cypris et de la SARL Transport Sud-Inter, le tribunal administratif de Marseille a annulé pour excès de pouvoir ces deux arrêtés ; que, par un arrêt du 21 juillet 2014, contre lequel la SCI Les Frères Réunis et la SCI les Deux Outils se pourvoient en cassation, la cour administrative d'appel de Marseille a rejeté leur requête dirigée contre ce jugement ; <br/>
<br/>
              2. Considérant, en premier lieu, que le moyen tiré de ce que l'arrêt n'aurait pas suffisamment analysé les moyens des parties ne peut qu'être écarté, dès lors qu'il n'est pas assorti de précisions de nature à permettre d'en apprécier le bien-fondé ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'aux termes de l'article UE 9 du règlement du plan d'occupation des sols de Saint-Chamas : " L'emprise maximale au sol est fixée à 50 % de la superficie du terrain " ; qu'en jugeant qu'en l'absence de définition précise donnée par les dispositions du plan d'occupation des sols, il y avait lieu de considérer que l'emprise au sol au sens de ces dispositions se définissait comme la projection verticale de tous les éléments susceptibles d'être qualifiés de constructions au sens du code de l'urbanisme, sans se limiter aux bâtiments ou plus généralement aux édifices clos et couverts, la cour n'a pas entaché son arrêt d'une erreur de droit ; <br/>
<br/>
              4. Considérant, en troisième lieu, qu'en jugeant que, pour l'application des dispositions de l'article UE 9 relatives à la condition d'emprise au sol, une dalle de béton servant d'aire de stockage d'une hauteur de 60 centimètres revêtait le caractère d'une construction, la cour a porté une appréciation souveraine sur les pièces du dossier, qui est exempte de dénaturation ;<br/>
<br/>
              5. Considérant, en dernier lieu, que la cour a estimé, en se livrant à une appréciation souveraine, que la dalle de béton constituait une surface construite et devait dès lors être réintégrée dans le calcul de la surface totale construite tel qu'effectué par le géomètre-expert diligenté par les sociétés requérantes et joint au dossier ; qu'elle en a déduit que l'obligation prescrite par l'article UE 9 avait été méconnue ; qu'en statuant ainsi, la cour n'a pas dénaturé les pièces du dossier, ni méconnu la portée des écritures des requérantes ; qu'en se prononçant par ces motifs, elle a suffisamment répondu à l'argumentation des parties et n'a pas omis de statuer sur un moyen ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que la SCI Les Frères Réunis et la SCI Les Deux Outils ne sont pas fondées à demander l'annulation de l'arrêt qu'elles attaquent ;<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la SCI Cypris et de la SARL Transport Sud-inter, qui ne sont pas, dans la présente instance, la partie perdante, la somme demandée à ce titre par la SCI Les Frères Réunis et la SCI Les Deux Outils ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la SCI Les Frères Réunis et de la SCI Les Deux Outils la somme de 750 euros à verser chacune, d'une part, à la SCI Cypris et, d'autre part, à la SARL Transport Sud-inter, au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la SCI Les Frères Réunis et de la SCI Les Deux Outils est rejeté.<br/>
Article 2 : La SCI Les Frères Réunis et la SCI Les Deux Outils verseront chacune une somme de 750 euros, d'une part, à la SCI Cypris et, d'autre part, à la SARL Transport Sud-Inter, au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la SCI Les Frères Réunis, à la SCI Les Deux Outils, à la SCI Cypris, à la SARL Transport Sud-Inter et à la commune de Saint-Chamas.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
