<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041808358</ID>
<ANCIEN_ID>JG_L_2020_03_000000439691</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/80/83/CETATEXT000041808358.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 23/03/2020, 439691, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-03-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439691</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:439691.20200323</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. et Mme B... A... ont demandé au juge des référés du tribunal administratif de Dijon de les admettre au bénéfice de l'aide juridictionnelle provisoire et, sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au préfet de Saône-et-Loire de les orienter vers une structure d'hébergement d'urgence dans un délai de vingt-quatre heures, sous astreinte de 100 euros par jour de retard. Par une ordonnance n° 2000718 du 19 mars 2020, le juge des référés du tribunal administratif de Dijon a accordé à M. et Mme A... le bénéfice de l'aide juridictionnelle provisoire et rejeté le surplus des conclusions de leur demande. <br/>
<br/>
              Par une requête, enregistrée le 20 mars 2020 au secrétariat du contentieux du Conseil d'Etat, M. et Mme A... demandent au juge des référés du Conseil d'Etat de les admettre au bénéfice de l'aide juridictionnelle provisoire et, sur le fondement de l'article L. 521-2 du code de justice administrative : <br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à leur demande de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
              - la condition d'urgence est remplie eu égard à la circonstance qu'ils dorment à la rue avec leurs enfants âgés de sept et neuf ans depuis un mois dans des conditions précaires exposant leurs enfants à des risques sanitaires ;<br/>
              - Mme A... et l'enfant de sept ans présentent des problèmes de santé ;<br/>
              - ni la saturation du dispositif d'accueil ni un ordre de priorité ne peuvent leur être opposés dès lors que leur maintien à la rue dans des conditions précaires les expose à l'épidémie de covid-19.<br/>
<br/>
<br/>
              Vu l'ordonnance attaquée ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - la convention internationale relative aux droits de l'enfant ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;  <br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. Il appartient au juge des référés saisi en appel de porter son appréciation sur ce point au regard de l'ensemble des pièces du dossier, et notamment des éléments recueillis par le juge de première instance dans le cadre de la procédure écrite et orale qu'il a diligentée. <br/>
<br/>
              2.  Il appartient aux autorités de l'Etat de mettre en oeuvre le droit à l'hébergement d'urgence reconnu par la loi à toute personne sans abri qui se trouve en situation de détresse médicale, psychique et sociale. Une carence caractérisée dans l'accomplissement de cette tâche peut faire apparaître, pour l'application de l'article L. 521-2 du code de justice administrative, une atteinte grave et manifestement illégale à une liberté fondamentale lorsqu'elle entraîne des conséquences graves pour la personne intéressée. Il incombe au juge des référés d'apprécier dans chaque cas les diligences accomplies par l'administration en tenant compte des moyens dont elle dispose ainsi que de l'âge, de l'état de la santé et de la situation de famille de la personne intéressée.<br/>
<br/>
<br/>
              3. Il résulte de l'instruction diligentée par le juge des référés du tribunal administratif de Dijon que M. et Mme A..., ressortissants albanais, qui avaient été bénéficiaires en 2019 de l'aide au retour volontaire après le rejet définitif d'une première demande d'asile, sont revenus en France avec leurs deux enfants âgés de sept et neuf ans et ont déposé une première demande de réexamen le 9 octobre 2019, puis une seconde, actuellement en cours d'examen en procédure accélérée après son enregistrement le 10 mars 2020, non assorti du bénéfice des conditions matérielles d'accueil. M. et Mme A... ont saisi le juge des référés du tribunal administratif de Dijon d'une demande tendant, sur le fondement de l'article L. 521-2 du code de justice administrative, à ce qu'il soit enjoint au préfet de Saône-et-Loire de les orienter vers une structure d'hébergement d'urgence dans un délai de vingt-quatre heures, sous astreinte de 100 euros par jour de retard. Par une ordonnance du 19 mars 2020, dont ils relèvent appel, le juge des référés du tribunal administratif de Dijon a rejeté leur demande au motif, d'une part, que le dispositif d'accueil restait saturé dans le département de Saône-et-Loire, avec un taux de remplissage avoisinant les 100 % en dépit de l'augmentation du nombre de places, et, d'autre part, que la famille ne présentait pas, en dépit des problèmes de santé de Mme A... et de l'un des enfants, un degré de vulnérabilité tel que les intéressés devraient être regardés comme prioritaires sur d'autres familles en attente d'un hébergement, sans que les intéressés puissent utilement soutenir que l'épidémie de covid-19, concernant toute la population, rendrait toute saturation du dispositif d'accueil et tout ordre de priorité inopposables.<br/>
<br/>
<br/>
              4. Les requérants se bornent, en appel, à produire à nouveau les pièces et l'argumentation présentées en première instance. Ils n'apportent ainsi aucun élément de nature à infirmer l'appréciation du juge des référés de première instance quant à l'absence d'atteinte grave et manifestement illégale portée par l'Etat à une liberté fondamentale, notamment, au regard des critères mentionnés au point 2, du fait d'une carence dans la mise en oeuvre du droit à l'hébergement d'urgence.<br/>
<br/>
<br/>
              5. Il résulte de tout ce qui précède qu'il est manifeste que l'appel de M. et Mme A... ne peut être accueilli. Leur requête, y compris les conclusions présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, ne peut dès lors qu'être rejetée, selon la procédure prévue par l'article L. 522-3 du code de justice administrative, sans qu'il y ait lieu de les admettre provisoirement au bénéfice de l'aide juridictionnelle.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. et Mme A... est rejetée.<br/>
<br/>
Article 2 : La présente ordonnance sera notifiée à M. et Mme B... A....<br/>
Copie en sera adressée au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
