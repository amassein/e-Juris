<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042065805</ID>
<ANCIEN_ID>JG_L_2020_06_000000434089</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/06/58/CETATEXT000042065805.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 29/06/2020, 434089, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434089</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Yaël Treille</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHS:2020:434089.20200629</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, d'une part, de suspendre l'exécution de la décision du 4 juillet 2019 par laquelle le proviseur du lycée Louis-le-Grand a fixé son service pour l'année scolaire 2019-2020 et, d'autre part, d'enjoindre au proviseur de la rétablir dans son enseignement de philosophie dans la classe de Khâgne 1, avec un demi-service dans l'enseignement de spécialité. Par une ordonnance n° 1916276 du 19 août 2019, le juge des référés a suspendu l'exécution de cette décision et enjoint au proviseur du lycée Louis-le-Grand de réexaminer sa situation dans un délai de quinze jours suivant la date de notification de l'ordonnance. <br/>
<br/>
              Par un pourvoi et deux nouveaux mémoires enregistrés le 30 août 2019 et les 6 février et 22 mai 2020, au secrétariat du contentieux du Conseil d'Etat, le ministre de l'éducation nationale et de la jeunesse demande au Conseil d'Etat d'annuler cette ordonnance.<br/>
<br/>
              Le ministre de l'éducation nationale et de la jeunesse soutient que l'ordonnance attaquée est entachée : <br/>
              - d'erreur de droit, au regard des dispositions du décret du 30 mai 1968 et d'inexacte qualification juridique des faits en ce qu'elle estime que la décision du proviseur fait grief à Mme A... ; <br/>
              - de dénaturation des pièces du dossier en ce qu'elle estime que les cours attribués à Mme A... ne relèvent pas de l'enseignement de philosophie.<br/>
<br/>
              Par un mémoire en défense et un nouveau mémoire, enregistrés le 25 novembre 2019 et le 20 mai 2020, Mme A... conclut au rejet de la requête et à ce que la somme de 4 000 euros soit mise à la charge de l'Etat au titre de l'article L. 761-1 du code de justice administrative. Elle soutient que les moyens soulevés par le pourvoi ne sont pas fondés et que la décision du proviseur est entachée d'incompétence.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le décret n°50-581 du 25 mai 1950 ;<br/>
              - le décret n°68-503 du 30 mai 1968 ; <br/>
              - le code de l'éducation ; <br/>
              - l'arrêté du 24 octobre 1994 ; <br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Yaël Treille, auditeur,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de Mme A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Paris que Mme A..., professeur agrégé de philosophie depuis le 1er septembre 1984 et professeur de chaire supérieure de cette discipline depuis le 1er septembre 2005, a, par un arrêté du 21 avril 2016, été affectée au lycée général Louis-le-Grand à Paris. A compter du 1er septembre 2016, elle y a assuré l'enseignement de philosophie en classe préparatoire de seconde année de lettre, dite khâgne Ulm ou K1. Pour l'année scolaire 2019-2020, le proviseur ne lui a attribué que cinq heures hebdomadaires de service dans une classe préparatoire économique et commerciale de première année et une classe de mathématiques supérieures MPSI (" mathématiques, physique et sciences de l'ingénieur "). Sur la demande de Mme A..., le juge des référés du tribunal administratif de Paris a suspendu l'exécution de cette décision et enjoint au proviseur du lycée Louis-le-Grand de réexaminer sa situation, dans un délai de quinze jours suivant la date de notification de l'ordonnance. Le ministre de l'éducation nationale et de la jeunesse se pourvoit en cassation contre cette ordonnance, en faisant valoir que, contrairement à ce que le juge des référés a retenu, la décision contestée par Mme A... n'est qu'une mesure d'ordre intérieur insusceptible de recours contentieux, de sorte que sa demande de suspension ne pouvait qu'être rejetée.  <br/>
<br/>
              2. Aux termes de l'article 1er du décret du décret du 30 mai 1968 portant statut particulier des professeurs de chaires supérieures des établissements classiques, modernes et techniques : " Il est constitué un corps de professeurs de chaires supérieures. Ses membres ont vocation à être affectés, pour y assurer les enseignements prévus, dans les chaires supérieures créées, dans la limite des emplois figurant au budget, dans les classes préparatoires aux grandes écoles des établissements de second degré (...) ".  Aux termes de l'arrêté du 24 octobre 1994 du ministre de l'éducation nationale, du ministre du budget, porte-parole du Gouvernement et du ministre de la fonction publique fixant la liste des disciplines pour lesquelles peuvent être créées des chaires supérieures instituées par le décret n° 68-503 du 30 mai 1968 modifié portant statut particulier des professeurs de chaires supérieures des établissements classiques, modernes et techniques : " Des chaires supérieures peuvent être créées pour les disciplines suivantes : arts plastiques, biochimie, biologie, chimie, économie et gestion, français, géographie, géologie, histoire, informatique, langues anciennes, langues vivantes étrangères, mathématiques, musique, philosophie, physique, sciences économiques et sociales, sciences et technologies industrielles ".<br/>
<br/>
              3. Le ministre de l'éducation nationale et de la jeunesse soutient que, pour estimer que la décision en litige n'était pas une mesure d'ordre intérieur, le juge des référés s'est fondé sur ce qu'un professeur de chaire supérieure tient des dispositions du décret du 30 mai 1968 le droit d'enseigner sa discipline dans une classe préparatoire de deuxième année de la filière correspondante et qu'il a, ce faisant, commis une erreur de droit. Toutefois, le juge des référés, pour juger que la décision en litige faisait grief, a retenu, en l'état de l'instruction conduite devant lui et par une appréciation souveraine exempte de dénaturation, que les enseignements confiés à Mme A... par cette décision n'étaient pas des enseignements de philosophie, alors qu'elle est professeur de chaire supérieure en philosophie. Dès lors, le moyen d'erreur de droit invoqué par le ministre ne peut, en tout état de cause, qu'être écarté. Enfin, en estimant, dans ces conditions, que la décision contestée faisait grief à l'intéressée, le juge des référés du tribunal administratif de Paris n'a pas inexactement apprécié les faits qui lui étaient soumis. <br/>
<br/>
              4. Il résulte de ce qui précède que le pourvoi du ministre de l'éducation nationale et de la jeunesse ne peut qu'être rejeté. <br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à Mme A... au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre de l'éducation nationale et de la jeunesse est rejeté. <br/>
Article 2 : L'Etat versera à Mme A... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée au ministre de l'éducation nationale et de la jeunesse et à Mme B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
