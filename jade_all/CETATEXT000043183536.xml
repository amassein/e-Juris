<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043183536</ID>
<ANCIEN_ID>JG_L_2021_02_000000423709</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/18/35/CETATEXT000043183536.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 19/02/2021, 423709, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423709</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. François Charmont</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:423709.20210219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... D... et Mme E... D... ont demandé au  tribunal administratif de Nice de condamner l'Etat à leur verser la somme de 50 000 euros en réparation du préjudice résultant du refus du préfet des Alpes-Maritimes de leur accorder le concours de la force publique pour l'expulsion de Mme F... C..., occupante du logement dont ils sont propriétaires dans la résidence " La fontaine aux merles ", située " Chemin de Fontmerle " à Antibes et d'enjoindre sous astreinte à l'Etat de prêter le concours de la force publique en vue de l'expulsion de Mme F... C.... Par un jugement n° 1600622 en date du 26 juin 2018, le tribunal administratif de Nice a condamné l'Etat à leur verser la somme de 2 089,60 euros et rejeté le surplus de leur demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux nouveaux mémoires, enregistrés les 29 août et 29 novembre 2018, 3 mars et 6 aout 2020 au secrétariat du contentieux du Conseil d'Etat, M. et Mme D... demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la construction et de l'habitation ;<br/>
              - le code des procédures civiles d'exécution ;<br/>
              - le code civil ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Charmont, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme B... G..., rapporteure publique.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M. et Mme D....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que M. et Mme D... ont obtenu, par une ordonnance du 18 décembre 2012 du tribunal d'instance d'Antibes, l'expulsion d'un locataire d'un logement leur appartenant. Ils ont demandé en vain, à plusieurs reprises, le concours de la force publique pour l'exécution de cette ordonnance. Ils se pourvoient en cassation contre le jugement du 26 juin 2018 par lequel le tribunal administratif de Nice a, d'une part, limité à 2 089,60 euros la condamnation de l'Etat au titre des préjudices subis du fait de ces refus et, d'autre part, rejeté leur demande tendant à ce qu'il soit enjoint à l'Etat de prêter le concours de la force publique à l'exécution de l'ordonnance d'expulsion.<br/>
<br/>
              2. Aux termes de l'article L. 153-1 du code des procédures civiles d'exécution : " L'Etat est tenu de prêter son concours à l'exécution des jugements et des autres titres exécutoires. Le refus de l'Etat de prêter son concours ouvre droit à réparation ". Aux termes de l'article R. 153-1 du même code : " Si l'huissier de justice est dans l'obligation de requérir le concours de la force publique, il s'adresse au préfet (...). Toute décision de refus de l'autorité compétente est motivée. Le défaut de réponse dans un délai de deux mois équivaut à un refus (...) ". S'il résulte de ces dispositions que toute décision de justice ayant force exécutoire peut donner lieu à une exécution forcée, la force publique devant, si elle est requise, prêter main forte à cette exécution, des considérations impérieuses tenant à la sauvegarde de l'ordre public peuvent toutefois légalement justifier le refus de prêter le concours de la force publique. <br/>
<br/>
              3. En premier lieu, il ressort des pièces du dossier soumis aux juges du fond que M. et Mme D... n'ont, pour établir le montant du préjudice de 50 000 euros dont ils demandaient réparation, présenté que des tableaux chiffrés portant sur une dette locative de 2 089,60 euros. Par suite, en limitant leur indemnisation à cette somme, sans y ajouter d'indemnisation au titre de préjudices nés de la dégradation du bien occupé, de l'abandon d'un projet immobilier ou de la privation de jouissance du bien, lesquels n'étaient ni justifiés ni même précisément énoncés devant lui, le tribunal administratif n'a pas insuffisamment motivé son jugement et ne l'a pas entaché d'erreur de droit ou de dénaturation des pièces du dossier.<br/>
<br/>
              4. En second lieu, il ressort des termes mêmes du jugement attaqué qu'il se fonde, pour indemniser les requérants, non sur le caractère fautif du refus de concours de la force publique allégué par ces derniers, mais sur l'existence d'une responsabilité sans faute de l'Etat, conformément au principe rappelé au point 2. Par suite, les moyens tirés de ce que le tribunal administratif ne pouvait, dès lors qu'il avait reconnu le caractère fautif du refus de concours de la force publique, refuser d'enjoindre à l'Etat d'accorder le concours de la force publique sans se méprendre sur la portée de leurs écritures et commettre une erreur de droit, sont inopérants.<br/>
<br/>
              5. Il résulte de tout ce qui précède que le pourvoi de M. et Mme D... doit être rejeté, y compris, par voie de conséquence, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. et Mme D... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. A... D..., à Mme E... D... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
