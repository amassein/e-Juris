<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028416435</ID>
<ANCIEN_ID>JG_L_2013_12_000000347459</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/41/64/CETATEXT000028416435.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 30/12/2013, 347459, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>347459</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Leïla Derouich</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:347459.20131230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et les mémoires complémentaires, enregistrés les 14 mars 2011, 15 juin 2011 et 20 janvier 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme D...A..., M. E...A..., Mlle C...A...et M. B...A...demeurant... ; les consorts A...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08MA02507-08MA04394 en date du 30 novembre 2010 de la cour administrative d'appel de Marseille en tant qu'il a rejeté leur requête dirigée contre le jugement n° 0503438-0504652 du tribunal administratif de Montpellier du 13 mars 2008 rejetant leur demande tendant à ce que la réparation de dommages imputés à la vaccination de Mme A... soit mise à la charge du centre hospitalier régional universitaire de Montpellier ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit aux conclusions de leur requête d'appel et de condamner le centre hospitalier régional universitaire de Montpellier à verser une somme de 107 000 euros à Mme D...A..., une somme de 7 000 euros à M. E...A...et une somme de 3 000 euros à chacun de leurs deux enfants Mélanie et BenoîtA... ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier régional universitaire de Montpellier le versement d'une somme de 7 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de la santé publique ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Leïla Derouich, Auditeur,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de MmeA..., et à la SCP Lyon-Caen, Thiriez, avocat du centre hospitalier régional universitaire de Montpellier ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme D...A..., cadre de santé au centre hospitalier régional universitaire de Montpellier, a reçu le 19 janvier 2004 au sein du service de médecine préventive de cet établissement une injection de rappel du vaccin Revaxis contre la diphtérie, le tétanos et la poliomyélite ; qu'à la suite de cette vaccination, elle a présenté des myalgies associées à un syndrome asthénique ; qu'une biopsie musculaire réalisée le 28 octobre 2004 a conduit à diagnostiquer une myofasciite à macrophages ; que les consortsA..., imputant cette affection et les troubles qui en résultent à la vaccination administrée au centre hospitalier universitaire régional de Montpellier, ont recherché la responsabilité de cet établissement public en faisant valoir que le vaccin contenait un adjuvant aluminique et que Mme A...présentait des antécédents d'allergies ; que leur recours indemnitaire a été rejeté par un jugement du 13 mars 2008, confirmé par un arrêt du 30 novembre 2010 de la cour administrative d'appel de Marseille contre lequel ils se pourvoient en cassation ; <br/>
<br/>
              2. Considérant que, dans le dernier état des connaissances scientifiques, l'existence d'un lien de causalité entre une vaccination contenant un adjuvant aluminique et la combinaison de symptômes constitués notamment par une fatigue chronique, des douleurs articulaires et musculaires et des troubles cognitifs n'est pas exclue et revêt une probabilité suffisante pour que ce lien puisse, sous certaines conditions, être regardé comme établi ; que tel est le cas lorsque la personne vaccinée, présentant des lésions musculaires de myofasciite à macrophages à l'emplacement des injections, est atteinte de tels symptômes, soit que ces symptômes sont apparus postérieurement à la vaccination, dans un délai normal pour ce type d'affection, soit, si certains de ces symptômes préexistaient, qu'ils se sont aggravés à un rythme et avec une ampleur qui n'étaient pas prévisibles au vu de l'état de santé antérieur à la vaccination, et qu'il ne ressort pas des expertises versées au dossier que les symptômes pourraient résulter d'une autre cause que la vaccination ; <br/>
<br/>
              3. Considérant qu'en écartant l'existence d'un lien de causalité direct entre les troubles présentés par Mme A...et la vaccination subie au motif que l'état des connaissances scientifiques ne permettait pas de démontrer un lien entre l'administration de vaccins contenant un adjuvant aluminique et la survenue d'un syndrome clinique spécifique, alors qu'elle avait relevé que l'intéressée ne présentait aucun antécédent médical, qu'il existait une proximité temporelle entre la vaccination et les premiers troubles et qu'aucun autre facteur ne permettait de penser qu'elle en était déjà atteinte lors des injections, la cour a commis une erreur de droit ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, les consorts A...sont fondés à demander l'annulation de l'arrêt attaqué en tant qu'il rejette leur requête d'appel ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier régional universitaire de Montpellier une somme de 3 500 euros  à verser aux consorts A...en application des dispositions de l'article L. 761-1 du code de justice administrative ; que ces mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge des consorts A...qui ne sont pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille n° 08MA02507-08MA04394 du 30 novembre 2010 est annulé en tant qu'il rejette la requête d'appel des consortsA....<br/>
<br/>
Article 2 : L'affaire est renvoyée, dans les limites de la cassation prononcée, à la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : Le centre hospitalier régional universitaire de Montpellier versera aux consorts A...une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions du centre hospitalier régional universitaire de Montpellier tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à Mme D...A..., à M. E...A..., à Mlle C...A..., à M. B...A..., au centre hospitalier régional universitaire de Montpellier, à la caisse primaire d'assurance maladie de Montpellier et à la Caisse des dépôts et consignations. <br/>
Copie pour information en sera adressée à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
