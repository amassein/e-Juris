<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031464452</ID>
<ANCIEN_ID>JG_L_2015_11_000000380864</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/46/44/CETATEXT000031464452.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 09/11/2015, 380864</TITRE>
<DATE_DEC>2015-11-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380864</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jacques Reiller</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:380864.20151109</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<p>Mme A... B...a demandé au tribunal administratif de Paris, d'une part, d'annuler pour excès de pouvoir l'arrêté en date du 30 janvier 2013 par lequel le préfet de police a refusé de lui délivrer un titre de séjour et lui a fait obligation de quitter le territoire français, d'autre part, d'enjoindre au préfet de police de lui délivrer le titre de séjour sollicité ou, à défaut, de procéder au réexamen de sa situation. Par un jugement n° 1303060 du 18 juillet 2013, le tribunal administratif de Paris a annulé cet arrêté et enjoint au préfet de police de réexaminer la situation de Mme B...dans un délai de trois mois à compter de la notification du jugement.<br clear="none"/>
<br clear="none"/>Par un arrêt n° 13PA03322 du 18 mars 2014, la cour administrative d'appel de Paris a rejeté l'appel formé contre ce jugement du tribunal administratif de Paris par le préfet de police. <br clear="none"/>
<br clear="none"/>Par un pourvoi, enregistré le 2 juin 2014 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat d'annuler cet arrêt.<br clear="none"/>
<br clear="none"/>
<br clear="none"/>Vu les autres pièces du dossier ;<br clear="none"/>
<br clear="none"/>Vu :<br clear="none"/>- le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br clear="none"/>- la loi n° 2000-321 du 12 avril 2000 ;<br clear="none"/>- le décret n° 2001-492 du 6 juin 2001 ;<br clear="none"/>- l'arrêté du 9 novembre 2011 relatif aux conditions d'établissement et de transmission des avis rendus par les agences régionales de santé en application de l'article R. 313-22 en vue de la délivrance d'un titre de séjour pour raison de santé ;<br clear="none"/>- le code de justice administrative ;<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>Après avoir entendu en séance publique :<br clear="none"/>
<br clear="none"/>- le rapport de M. Jacques Reiller, conseiller d'Etat, <br clear="none"/>
<br clear="none"/>- les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B..., ressortissante ghanéenne, a sollicité un titre de séjour en qualité d'étranger malade sur le fondement des dispositions du 11° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile ; qu'après un premier avis du médecin chef du service médical de la préfecture de police, le préfet de police lui a délivré, le 8 novembre 2011, une autorisation de séjour pour une durée de trois mois en raison de sa pathologie, qui a été renouvelée une fois ; que, le 15 mars 2012, la préfecture de police a demandé à l'intéressée que soit adressé au médecin chef un nouveau rapport médical ; qu'à la suite de la réception, le 29 mars 2012, du dossier médical de Mme B...adressé par le médecin agréé qu'elle avait choisi, le médecin chef a demandé à ce dernier, par courrier du 2 mai 2012, de compléter son rapport en lui communiquant une copie du suivi hospitalier de la patiente et trois certificats médicaux ; qu'en l'absence de suite donnée à cette demande, le médecin chef a indiqué au préfet de police, par courrier du 7 janvier 2013, qu'il n'était pas en mesure de se prononcer sur l'état de santé de Mme B... ; que, par un arrêté du 30 janvier 2013, le préfet de police a refusé de renouveler le titre de séjour de Mme B...au motif qu'elle ne remplissait pas les conditions prévues par le 11° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile et a assorti ce refus d'une obligation de quitter le territoire français ; que le ministre de l'intérieur se pourvoit contre l'arrêt du 18 mars 2014 par lequel la cour administrative d'appel de Paris a rejeté l'appel formé par le préfet de police contre le jugement du 18 juillet 2013 du tribunal administratif de Paris annulant l'arrêté du 30 janvier 2013 et enjoignant au préfet de police de réexaminer la situation de Mme B...;<br clear="none"/>
<br clear="none"/>2. Considérant qu'aux termes de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Sauf si sa présence constitue une menace pour l'ordre public, la carte de séjour temporaire portant la mention "vie privée et familiale" est délivrée de plein droit : / (...) 11° A l'étranger résidant habituellement en France dont l'état de santé nécessite une prise en charge médicale dont le défaut pourrait entraîner pour lui des conséquences d'une exceptionnelle gravité, sous réserve de l'absence d'un traitement approprié dans le pays dont il est originaire, sauf circonstance humanitaire exceptionnelle appréciée par l'autorité administrative après avis du directeur général de l'agence régionale de santé, sans que la condition prévue à l'article L. 311-7 soit exigée. La décision de délivrer la carte de séjour est prise par l'autorité administrative, après avis du médecin de l'agence régionale de santé de la région de résidence de l'intéressé, désigné par le directeur général de l'agence, ou, à Paris, du médecin, chef du service médical de la préfecture de police. Le médecin de l'agence régionale de santé ou, à Paris, le chef du service médical de la préfecture de police peut convoquer le demandeur pour une consultation médicale devant une commission médicale régionale dont la composition est fixée par décret en Conseil d'Etat " ; que l'article R. 313-22 du même code dispose que : " Pour l'application du 11° de l'article L. 313-11, le préfet délivre la carte de séjour temporaire au vu d'un avis émis par le médecin de l'agence régionale de santé compétente au regard du lieu de résidence de l'intéressé, désigné par le directeur général. Par dérogation, à Paris, ce médecin est désigné par le préfet de police. / L'avis est émis dans les conditions fixées par arrêté du ministre chargé de l'immigration et du ministre chargé de la santé au vu, d'une part, d'un rapport médical établi par un médecin agréé ou un médecin praticien hospitalier et, d'autre part, des informations disponibles sur les possibilités de traitement dans le pays d'origine de l'intéressé " ; que l'arrêté du 9 novembre 2011 relatif aux conditions d'établissement et de transmission des avis rendus par les agences régionales de santé en application de l'article R. 313-22 en vue de la délivrance d'un titre de séjour pour raison de santé précise, à son article 1er, que " L'étranger qui a déposé une demande de délivrance ou de renouvellement de carte de séjour temporaire est tenu de faire établir un rapport médical relatif à son état de santé ou par un médecin praticien hospitalier visé au 1° de l'article L. 6152-1 du code de la santé publique ", à son article 3, que : " Au vu des informations médicales qui lui sont communiquées par l'intéressé ou, à la demande de celui-ci, par tout autre médecin, et au vu de tout examen qu'il jugera utile de prescrire, le médecin agréé ou le médecin praticien hospitalier mentionné à l'article 1er établit un rapport précisant le diagnostic des pathologies en cours, le traitement suivi et sa durée prévisible ainsi que les perspectives d'évolution " et, à son article 6, que : " A Paris, le médecin agréé ou le médecin praticien hospitalier visé à l'article 1er adresse son rapport médical, sous pli confidentiel, au médecin désigné par le préfet de police. Celui-ci émet l'avis comportant l'ensemble des précisions mentionnées à l'article 4 ci-dessus et le transmet au préfet de police " ; <br clear="none"/>
<br clear="none"/>3. Considérant qu'il résulte de ces dispositions combinées que, dans le cas où le médecin chargé d'émettre un avis destiné au préfet auquel a été adressée une demande de titre de séjour en qualité d'étranger malade n'est pas à même de se prononcer sur l'état de santé du demandeur, faute d'avoir reçu, de la part du médecin agréé choisi par le demandeur, le rapport médical que celui-ci doit établir ou les pièces complémentaires à ce rapport qui lui ont été réclamées, il appartient au médecin de l'agence régionale de santé ou, à Paris, au médecin chef du service médical de la préfecture de police d'en informer l'autorité préfectorale ; qu'il incombe alors à cette dernière de porter cet élément, qui fait obstacle à la poursuite de l'instruction de la demande de séjour, à la connaissance de l'étranger afin de le mettre à même soit d'obtenir du médecin agréé qu'il a choisi qu'il accomplisse les diligences nécessaires soit, le cas échéant, de choisir un autre médecin agréé ; qu'il suit de là qu'en fondant l'illégalité de l'arrêté du 30 janvier 2013 sur la méconnaissance de l'article 2 du décret du 6 juin 2001 pris pour l'application du chapitre II du titre II de la loi n° 2000-321 du 12 avril 2000 qui, définissant les conditions dans lesquelles l'autorité administrative doit inviter un demandeur à compléter son dossier en lui fournissant les pièces manquantes indispensables à l'instruction de la demande qui sont en sa possession, n'est pas applicable à la situation particulière de l'étranger tenu de faire établir un rapport médical pour l'instruction de sa demande de séjour présentée sur le fondement du 11° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, laquelle est entièrement régie par les dispositions précitées, la cour administrative d'appel de Paris a entaché son arrêt d'erreur de droit ; qu'il résulte de ce qui précède, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que le ministre de l'intérieur est fondé à demander l'annulation de l'arrêt qu'il attaque ; <br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
</p>
<p>D E C I D E :<br clear="none"/>--------------<br clear="none"/>Article 1er : L'arrêt de la cour administrative d'appel de Paris du 18 mars 2014 est annulé.<br clear="none"/>Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br clear="none"/>Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à Mme A...B....<br clear="none"/>
</p>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-01-02-02-01 ÉTRANGERS. SÉJOUR DES ÉTRANGERS. AUTORISATION DE SÉJOUR. OCTROI DU TITRE DE SÉJOUR. DÉLIVRANCE DE PLEIN DROIT. - ETRANGER MALADE - DÉLIVRANCE DE PLEIN DROIT D'UNE CARTE VIE PRIVÉE ET FAMILIALE (11° DE L'ART. L. 313-11 DU CESEDA) - INSTRUCTION DE LA DEMANDE - 1) CARENCE DU MÉDECIN AGRÉÉ CHOISI PAR LE DEMANDEUR - OBLIGATION DU PRÉFET D'EN AVISER L'ÉTRANGER AFIN QUE CELUI-CI PUISSE PALLIER CETTE CARENCE - EXISTENCE - 2) APPLICABILITÉ DE L'ARTICLE 2 DU DÉCRET DU 6 JUIN 2001 PRIS POUR L'APPLICATION DE LA LOI DCRA (INVITATION DU DEMANDEUR À COMPLÉTER SON DOSSIER) - ABSENCE.
</SCT>
<ANA ID="9A"> 335-01-02-02-01 1) Il résulte des dispositions combinées des articles L. 313-11 et R. 313-22 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) ainsi que de l'arrêté d'application du 9 novembre 2011 que, dans le cas où le médecin chargé d'émettre un avis destiné au préfet auquel a été adressée une demande de titre de séjour en qualité d'étranger malade n'est pas à même de se prononcer sur l'état de santé du demandeur, faute d'avoir reçu, de la part du médecin agréé choisi par le demandeur, le rapport médical que celui-ci doit établir ou les pièces complémentaires à ce rapport qui lui ont été réclamées, il appartient au médecin de l'agence régionale de santé ou, à Paris, au médecin chef du service médical de la préfecture de police d'en informer l'autorité préfectorale. Il incombe alors à cette dernière de porter cet élément, qui fait obstacle à la poursuite de l'instruction de la demande de séjour, à la connaissance de l'étranger afin de le mettre à même soit d'obtenir du médecin agréé qu'il a choisi qu'il accomplisse les diligences nécessaires soit, le cas échéant, de choisir un autre médecin agréé.,,,2) L'article 2 du décret n° 2001-492 du 6 juin 2001 pris pour l'application du chapitre II du titre II de la loi n° 2000-321 du 12 avril 2000 (dite DCRA), qui définit les conditions dans lesquelles l'autorité administrative doit inviter un demandeur à compléter son dossier en lui fournissant les pièces manquantes indispensables à l'instruction de la demande qui sont en sa possession, n'est pas applicable à la situation particulière de l'étranger tenu de faire établir un rapport médical pour l'instruction de sa demande de séjour présentée sur le fondement du 11° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, laquelle est entièrement régie par les dispositions du CESEDA et de l'arrêté du 9 novembre 2011.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>



</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
