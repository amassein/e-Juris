<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000021630816</ID>
<ANCIEN_ID>JG_L_2009_12_000000325459</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/21/63/08/CETATEXT000021630816.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 30/12/2009, 325459</TITRE>
<DATE_DEC>2009-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>325459</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Daël</PRESIDENT>
<AVOCATS>SCP BACHELLIER, POTIER DE LA VARDE</AVOCATS>
<RAPPORTEUR>Mme Laure  Bédier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mlle Courrèges Anne</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 20 février 2009 au secrétariat du contentieux du Conseil d'Etat, présenté pour M. André A, demeurant ... ; M. A demande au Conseil d'Etat: <br/>
<br/>
              1°) d'annuler le jugement du 30 décembre 2008 par lequel le tribunal administratif de Rennes, après avoir annulé à sa demande l'arrêté du 8 mars 1993 lui concédant sa pension civile de retraite en tant qu'il ne prend pas en compte la bonification mentionnée au b) de l'article L. 12 du code des pensions civiles et militaires de retraite, a enjoint à l'administration de modifier les conditions dans lesquelles sa pension lui a été concédée en y intégrant cette bonification, mais seulement à compter du 1er janvier 2003 ;<br/>
<br/>
              2°) réglant l'affaire au fond, d'enjoindre à l'administration de revaloriser sa pension à compter de l'entrée en jouissance initiale de celle-ci ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code civil ;<br/>
<br/>
              Vu le code des pensions civiles et militaires de retraite ;<br/>
<br/>
              Vu la loi n° 68-1250 du 31 décembre 1968 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laure Bédier, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Bachellier, Potier de la Varde, avocat de M. A, <br/>
<br/>
              - les conclusions de Mlle Anne Courrèges, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Bachellier, Potier de la Varde, avocat de M. A ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant, d'une part, qu'aux termes de l'article 1er de la loi du 31 décembre 1968 :  Sont prescrites, au profit de l'Etat (...), sans préjudice des déchéances particulières édictées par la loi, (...) toutes créances qui n'ont pas été payées dans un délai de quatre ans à partir du premier jour de l'année suivant celle au cours de laquelle les droits ont été acquis (...)  ; que, d'autre part, aux termes de l'article L. 53 du code de pensions civiles et militaires de retraite :  Lorsque, par suite du fait personnel du pensionné, la demande de liquidation ou de révision de la pension est déposée postérieurement à l'expiration de la quatrième année qui suit celle de l'entrée en jouissance normale de la pension, le titulaire ne peut prétendre qu'aux arrérages afférents à l'année au cours de laquelle la demande a été déposée et aux quatre années antérieures  ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par arrêté du 8 mars 1993, M. A a été admis au bénéfice d'une pension de retraite ; qu'il a présenté, le 19 février 2003, une demande de révision de sa pension pour tenir compte de la bonification pour enfants prévue au b) de l'article L. 12 du code des pensions civiles et militaires de retraite, que l'administration n'a pas accueillie ; que, s'avisant que son titre initial de pension ne lui avait pas été notifié avec indications des voies de recours, il a directement saisi le tribunal administratif de Rennes, le 1er septembre 2007, d'une demande d'annulation de l'arrêté du 8 mars 1993, en tant qu'il ne prenait pas en compte la bonification prévue au b) de l'article L. 12 ; que, par le jugement attaqué du 30 décembre 2008, le tribunal administratif a fait droit à la demande d'annulation de cet arrêté, mais n'a enjoint à l'administration de revaloriser la pension du requérant pour tenir compte de cette bonification qu'à compter du 1er janvier 2003 ; que M. A, qui se pourvoit en cassation contre ce jugement, doit être regardé comme n'en contestant que l'article 3, qui retient cette limitation et qui seul lui fait grief ;<br/>
<br/>
              Considérant que, pour fixer ainsi la date d'effet de cette revalorisation, le tribunal administratif de Rennes s'est fondé sur la prescription quadriennale résultant des dispositions de la loi du 31 décembre 1968 ; que, toutefois, un recours contentieux directement formé contre un arrêté de concession de pension en vue d'en remettre en cause le montant implique nécessairement, s'il est accueilli, que l'administration procède, en prenant un nouvel arrêté, à une nouvelle liquidation de la pension ; que par suite lorsque, comme en l'espèce, le titulaire d'une pension est recevable à saisir directement le juge d'un recours contre un arrêté de concession qui n'avait pas fait l'objet d'une notification comportant l'indication des voies de recours, la demande ainsi présentée doit être regardée comme une demande de liquidation de pension, au sens de l'article L. 53 de ce code ; qu'ainsi, en se fondant, pour déterminer l'étendue des droits à revalorisation de M. A, sur les dispositions générales de la loi du 31 décembre 1968, alors que les dispositions spéciales de l'article L. 53 étaient seules applicables, le tribunal administratif de Rennes a commis une erreur de droit ; que, par suite, l'article 3 du jugement attaqué doit, pour ce motif et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, être annulé ;<br/>
<br/>
              Considérant qu'il y lieu, dans les circonstances de l'espèce, de régler dans cette mesure l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              Considérant que, si l'administration ne peut opposer à une demande présentée dans les conditions ci-dessus décrites le délai d'un an prévu à l'article L. 55 du code de pensions civiles et militaires de retraite pour les demandes de révision qui lui sont directement présentées, elle est en droit, ainsi qu'il a été dit, de se prévaloir de la règle de prescription des arrérages fixée par l'article L. 53 de ce code, hormis le cas où le délai mis par l'intéressé à présenter une telle demande ne serait pas imputable à son fait personnel ;<br/>
<br/>
              Considérant qu'il résulte de l'instruction qu'aucune circonstance ne faisait obstacle à ce que M. A présentât, dès la notification de son titre de pension, un recours tendant à obtenir une nouvelle liquidation de pension, en contestant l'absence de la bonification dont il revendique le bénéfice ; que le délai de prescription prévu par l'article L. 53 lui est donc applicable ; que l'intéressé avait, ainsi qu'il a été dit, adressé dès le 19 février 2003 au service des pensions une demande tendant à la prise en compte de la bonification pour enfants prévue au b) de  l'article L. 12  du code des pensions civiles et militaires de retraite ; que, dans ces conditions, et alors même qu'il n'a ensuite saisi le tribunal administratif de Rennes que le 1er septembre 2007, la date à retenir pour le délai de prescription des arrérages est celle du 19 février 2003 ; que M. A peut en conséquence prétendre à la revalorisation de sa pension à compter du 1er janvier 1999 ; qu'il y a lieu, dès lors, de prescrire au ministre de modifier les conditions dans lesquelles la pension de M. A lui a été concédée et de revaloriser rétroactivement cette pension à compter du 1er janvier 1999 ; <br/>
<br/>
              Considérant que M. A a droit aux intérêts des sommes qui lui sont dues à compter de la réception, par l'administration, de sa demande du 19 février 2003 ; qu'à la date du 1er septembre 2007, à laquelle M. A a présenté des conclusions à fin de capitalisation des intérêts, il était dû plus d'une année d'intérêts ; que, dès lors, conformément aux dispositions de l'article 1154 du code civil, il y a lieu de faire droit à cette demande, tant à cette date qu'à chaque échéance annuelle à compter de cette date ; <br/>
<br/>
              Considérant, enfin, qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à M. A de la somme de 1 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
     --------------<br/>
<br/>
Article 1er : L'article 3 du jugement du tribunal administratif de Rennes du 30 décembre 2008 est annulé.<br/>
Article 2 : Le ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat revalorisera rétroactivement la pension de M. A à compter du 1er janvier 1999. Ces sommes porteront intérêt au taux légal à compter de la réception, par l'administration, de sa demande du 19 février 2003. Les intérêts échus à la date du 1er septembre 2007, puis à chaque échéance annuelle à compter de cette date, seront capitalisés à chacune de  ces dates pour produire eux-mêmes intérêts.<br/>
Article 3 : L'Etat versera à M. A la somme de 1 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. André A et au ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">48-02-01-05-01 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. QUESTIONS COMMUNES. AVANTAGES FAMILIAUX. MAJORATION POUR ENFANTS. - DEMANDE DE RÉVISION DE LA PENSION POUR TENIR COMPTE DE LA BONIFICATION POUR ENFANTS PRÉVUE AU B) DE L'ARTICLE L. 12 DU CODE DES PENSIONS CIVILES ET MILITAIRES DE RETRAITE - ARRÊTÉ DE CONCESSION DE PENSION N'AYANT PAS FAIT L'OBJET D'UNE NOTIFICATION COMPORTANT L'INDICATION DES VOIES DE RECOURS - INTRODUCTION D'UN RECOURS EN EXCÈS DE POUVOIR - RECOURS DEVANT ÊTRE REGARDÉ COMME UNE DEMANDE DE LIQUIDATION DE PENSION - CONSÉQUENCE - RÈGLES DE PRESCRIPTION APPLICABLES - ARTICLE 1ER DE LA LOI DU 31 DÉCEMBRE 1968 - ABSENCE - ARTICLE L. 53 DU CODE DES PENSIONS CIVILES ET MILITAIRES DE RETRAITE - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">48-02-04-02 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. CONTENTIEUX DES PENSIONS CIVILES ET MILITAIRES DE RETRAITE. INTRODUCTION DE L'INSTANCE. - DEMANDE DE RÉVISION DE LA PENSION POUR TENIR COMPTE DE LA BONIFICATION POUR ENFANTS PRÉVUE AU B) DE L'ARTICLE L. 12 DU CODE DES PENSIONS CIVILES ET MILITAIRES DE RETRAITE - ARRÊTÉ DE CONCESSION DE PENSION N'AYANT PAS FAIT L'OBJET D'UNE NOTIFICATION COMPORTANT L'INDICATION DES VOIES DE RECOURS - INTRODUCTION D'UN RECOURS EN EXCÈS DE POUVOIR - RECOURS DEVANT ÊTRE REGARDÉ COMME UNE DEMANDE DE LIQUIDATION DE PENSION - CONSÉQUENCE - RÈGLES DE PRESCRIPTION APPLICABLES - ARTICLE 1ER DE LA LOI DU 31 DÉCEMBRE 1968 - ABSENCE - ARTICLE L. 53 DU CODE DES PENSIONS CIVILES ET MILITAIRES DE RETRAITE - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 48-02-01-05-01 Un recours contentieux directement formé contre un arrêté de concession de pension en vue d'en remettre en cause le montant implique nécessairement, s'il est accueilli, que l'administration procède, en prenant un nouvel arrêté, à une nouvelle liquidation de la pension. Par suite, lorsque le titulaire d'une pension est recevable à saisir directement le juge d'un recours contre un arrêté de concession qui n'avait pas fait l'objet d'une notification comportant l'indication des voies de recours, la demande ainsi présentée doit être regardée comme une demande de liquidation de pension, au sens de l'article L. 53 de ce code. Erreur de droit du tribunal administratif à s'être fondé, pour déterminer l'étendue des droits à revalorisation du requérant, sur les dispositions générales de la loi n° 68-1250 du 31 décembre 1968, alors que les dispositions spéciales de l'article L. 53 étaient seules applicables.</ANA>
<ANA ID="9B"> 48-02-04-02 Un recours contentieux directement formé contre un arrêté de concession de pension en vue d'en remettre en cause le montant implique nécessairement, s'il est accueilli, que l'administration procède, en prenant un nouvel arrêté, à une nouvelle liquidation de la pension. Par suite, lorsque le titulaire d'une pension est recevable à saisir directement le juge d'un recours contre un arrêté de concession qui n'avait pas fait l'objet d'une notification comportant l'indication des voies de recours, la demande ainsi présentée doit être regardée comme une demande de liquidation de pension, au sens de l'article L. 53 de ce code. Erreur de droit du tribunal administratif à s'être fondé, pour déterminer l'étendue des droits à revalorisation du requérant, sur les dispositions générales de la loi n° 68-1250 du 31 décembre 1968, alors que les dispositions spéciales de l'article L. 53 étaient seules applicables.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., pour les pensions militaires d'invalidité et des victimes de la guerre, 4 mars 2009, Zaheg, n° 305429, p. 73 ; 24 juillet 2009, Grandry, n° 322806, à publier aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
