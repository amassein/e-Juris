<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041763111</ID>
<ANCIEN_ID>JG_L_2020_03_000000421833</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/76/31/CETATEXT000041763111.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 27/03/2020, 421833, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-03-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421833</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER ; SCP COLIN-STOCLET ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Yohann Bouquerel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:421833.20200327</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le département de l'Orne a demandé au tribunal administratif de Caen, à titre principal, de condamner solidairement les sociétés Signalisation France, Signaux Girod, Nadia Signalisation, Lacroix Signalisation et Franche-Comté Signaux à lui payer la somme de 2 239 819 euros, assortie des intérêts et de leur capitalisation, en réparation du préjudice subi du fait des pratiques anticoncurrentielles caractérisées par l'Autorité de la concurrence dans sa décision n° 10-D-39 du 22 décembre 2010. Par un jugement n° 1500227 du 6 avril 2017, le tribunal administratif de Caen a condamné solidairement les sociétés Signalisation France, Signaux Girod, Lacroix Signalisation et Franche-Comté Signaux à verser au département de l'Orne la somme de 2 239 819 euros, assortie des intérêts à compter du 29 janvier 2015 et de leur capitalisation au 29 janvier 2016 et à chaque échéance annuelle ultérieure.<br/>
<br/>
              Par un arrêt n°s 17NT01719, 17NT01740, 17NT01741, 17NT01770 du 27 avril 2018, la cour administrative d'appel de Nantes a rejeté l'appel formé par la société Signaux Girod contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 28 juin et 28 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, la société Signaux Girod demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge du département de l'Orne la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le code de commerce ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yohann Bouquerel, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Texidor, Perier, avocat de la société Signaux Girod, à la SCP Colin-Stoclet, avocat de la société Signalisation France, et à la SCP Piwnica, Molinié, avocat du département de l'Orne ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le département de l'Orne a conclu le 28 avril 1999, le 3 avril 2002 et le 1er avril 2005 avec la société Signature S.A., devenue la société Signalisation France, trois marchés à bons de commande en vue de l'acquisition de panneaux de signalisation routière et d'équipements annexes. La société Signaux Girod, qui n'a pas soumissionné aux marchés de 1999 et 2005, a présenté une offre pour le marché passé en 2002. Par une décision n° 10-D-39 du 22 décembre 2010, l'Autorité de la concurrence a condamné huit entreprises, dont les sociétés Signature, Signaux Girod, Lacroix Signalisation et Franche-Comté Signaux, pour s'être entendues entre 1997 et 2006 sur la répartition et le prix de marchés de signalisation routière verticale. Par une ordonnance du 30 juillet 2013, le juge des référés du tribunal administratif de Caen a ordonné une expertise afin de déterminer le surcoût qu'ont entraîné pour le département de l'Orne les pratiques anticoncurrentielles de la société Signalisation France lors de la passation des trois marchés litigieux. Dans son rapport du 31 mars 2014, l'expert a estimé ce surcoût à la somme de 2 239 819 euros. Par un jugement du 6 avril 2017, le même tribunal a condamné les sociétés Signalisation France, Lacroix Signalisation, Signaux Girod et Franche-Comté Signaux, d'une part, à verser au département de l'Orne la somme de 2 239 819 euros et, d'autre part, à prendre en charge les frais et honoraires de l'expertise à hauteur de 26 461,08 euros. La société Signaux Girod se pourvoit en cassation contre l'arrêt du 27 avril 2018 par lequel la cour administrative d'appel de Nantes a rejeté son appel contre ce jugement.<br/>
<br/>
              2. En premier lieu, lorsqu'une personne publique est victime, à l'occasion de la passation d'un marché public, de pratiques anticoncurrentielles, il lui est loisible de mettre en cause la responsabilité quasi-délictuelle non seulement de l'entreprise avec laquelle elle a contracté, mais aussi des entreprises dont l'implication dans de telles pratiques a affecté la procédure de passation de ce marché, et de demander au juge administratif leur condamnation solidaire. Ainsi qu'il a été dit au point 1, par une décision du 22 décembre 2010, l'Autorité de la concurrence a condamné la société requérante, ainsi que sept autres entreprises, pour avoir participé entre 1997 et 2006 à une entente visant à se répartir au niveau national les marchés publics de signalisation routière et à en augmenter les prix. Par un arrêt, devenu définitif, en date du 29 mars 2012, la cour d'appel a confirmé cette sanction dans son principe en se bornant à diminuer son quantum pour certaines des entreprises concernées, mais non pour la société Signaux Girod. En déduisant de ces constats, par un arrêt suffisamment motivé, d'une part, que le comportement fautif de la société Signaux Girod était en lien direct avec le surcoût supporté par le département de l'Orne lors de l'exécution des marchés à bons de commande passés en 1999, 2002 et 2005 et, d'autre part, que sa responsabilité solidaire était engagée, alors même qu'elle n'avait présenté qu'une offre en 2002 et aucune en 1999 et 2005, la cour n'a pas commis d'erreur de droit.<br/>
<br/>
              3. En deuxième lieu, en relevant que le marché conclu par le département de l'Orne le 1er avril 2005, renouvelable chaque année civile pour une durée maximale de quatre ans, avait produit ses effets jusqu'en 2008, et qu'il convenait de retenir cette date pour évaluer le préjudice subi par celle collectivité, alors même que l'entente dans le secteur de la signalisation routière avait pris fin en 2006, la cour administrative d'appel, qui a implicitement mais nécessairement jugé que le département ne pouvait être regardé comme ayant eu une connaissance suffisamment certaine de l'existence et du démantèlement de cette entente dès 2006, n'a pas entaché son arrêt d'insuffisance de motivation, d'erreur de qualification juridique ou de dénaturation des pièces du dossier.<br/>
<br/>
              4. En troisième lieu, en évaluant, au vu de l'ensemble des pièces du dossier, et notamment du rapport d'expertise, à la somme de 2 239 819 euros le préjudice subi par le département de l'Orne, après avoir effectué une comparaison à prix et produits constants entre les marchés conclus entre 1999 et 2005 et le marché conclu en 2009, et en tenant compte notamment de la baisse du prix de certaines matières premières après le démantèlement de l'entente, la cour n'a pas entaché son arrêt de dénaturation. <br/>
<br/>
              5. En dernier lieu, en jugeant que les montants respectifs des amendes prononcées par l'Autorité de la concurrence à l'encontre des entreprises sanctionnées pour leur participation à une entente dans le secteur de la signalisation routière, qui avaient été fixés compte tenu de leur part de responsabilité dans l'atteinte globale à l'économie, ne pouvaient, eu égard à leur objet, servir de clef de répartition pour déterminer la part de chacune d'entre elles dans le préjudice effectivement subi par le département de l'Orne, la cour n'a pas commis d'erreur de droit. <br/>
<br/>
              6. Il résulte de tout ce qui précède que la société Signaux Girod n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge du département de l'Orne, qui n'est pas la partie perdante, le versement d'une somme au titre des frais exposés par la requérante et non compris dans les dépens. Il y a lieu en revanche, dans les circonstances de l'espèce, de mettre à la charge de la société Signaux Girod la somme de 3 000 euros à verser au département de l'Orne au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Signaux Girod est rejeté.<br/>
Article 2 : La société Signaux Girod versera au département de l'Orne une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Signaux Girod et au département de l'Orne. <br/>
Copie en sera adressé aux sociétés Signalisation France, Nadia Signalisation, Signature Vertical et Mobility Solutions, Lacroix Signalisation et Franche-Comté Signaux.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
