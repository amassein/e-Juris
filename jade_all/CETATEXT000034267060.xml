<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034267060</ID>
<ANCIEN_ID>JG_L_2017_03_000000392213</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/26/70/CETATEXT000034267060.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 20/03/2017, 392213, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392213</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:392213.20170320</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme A...B...ont demandé au tribunal administratif d'Amiens la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquels ils ont été assujettis au titre des années 2005 et 2006, et des pénalités y afférentes. Par un jugement n° 1101358 du 28 novembre 2013, le tribunal administratif a prononcé la décharge des pénalités pour manquement délibéré, prévues par l'article 1729 du code général des impôts, appliquées au titre des deux années d'imposition et rejeté le surplus de la demande.<br/>
<br/>
              Par un arrêt n° 14DA00254 du 2 juillet 2015, la cour administrative d'appel de Douai, sur appel interjeté par le ministre des finances et des comptes publics, et appel incident formé par M. et Mme  B..., a annulé ce jugement en tant qu'il prononce la décharge des pénalités pour manquement délibéré appliquées au titre de l'année 2006, remis ces pénalités à la charge de M. et MmeB..., déchargé ces derniers des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquels ils ont été assujettis au titre de l'année 2005 et rejeté le surplus des conclusions du ministre et de l'appel incident de M. et Mme B....<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés le 30 juillet 2015 et le 22 décembre 2016 au secrétariat du contentieux du Conseil d'Etat, le ministre des finances et des comptes publics demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler les articles 3, 4 et 5 de cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel et de rejeter l'appel incident formé par M. et MmeB....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de M. et Mme A...B...;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à l'issue de l'examen contradictoire de leur situation fiscale personnelle, M. et Mme B...ont été assujettis à des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales, ainsi qu'aux pénalités correspondantes, au titre des années 2005 et 2006. Le ministre des finances et des comptes publics se pourvoit contre l'arrêt du 2 juillet 2015 de la cour administrative d'appel de Douai, en tant qu'il décharge M. et Mme B...des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles ils ont été assujettis au titre de l'année 2005, réforme dans cette mesure le jugement du 28 novembre 2013 du tribunal administratif d'Amiens et rejette le surplus de ses conclusions tendant à ce que les pénalités dues au titre de 2005 soient remises à la charge des épouxB....<br/>
<br/>
              2. Aux termes de l'article L. 169 du livre des procédures fiscales, dans sa rédaction applicable au présent litige : " Pour l'impôt sur le revenu (...), le droit de reprise de l'administration des impôts s'exerce jusqu'à la fin de la troisième année qui suit celle au titre de laquelle l'imposition est due (...) ". Aux termes de l'article 189 du même livre : " La prescription est interrompue par la notification d'une proposition de rectification, par la déclaration ou la notification d'un procès-verbal, de même que par tout acte comportant reconnaissance de la part des contribuables et par tous les autres actes interruptifs de droit commun./ La prescription des sanctions fiscales autres que celles visées au troisième alinéa de l'article L. 188 est interrompue par la mention portée sur la proposition de rectification qu'elles pourront être éventuellement appliquées ".<br/>
<br/>
              3. Eu égard à l'objet de ces dispositions, relatives à la détermination du délai dont dispose l'administration pour exercer son droit de reprise, la date d'interruption de la prescription est celle à laquelle le pli contenant la proposition de rectification a été présenté à l'adresse du contribuable. Il en va de même lorsque le pli n'a pu lui être remis lors de sa présentation et que, avisé de sa mise en instance, il l'a retiré ultérieurement ou a négligé de le retirer.<br/>
<br/>
              4. Par ailleurs, il résulte de la réglementation postale, et notamment de l'instruction postale du 6 septembre 1990, qu'en cas d'absence du destinataire d'une lettre remise contre signature, le préposé du service des postes doit, en premier lieu, porter la date de vaine présentation sur le volet " preuve de distribution " de la liasse postale, cette date se dupliquant sur les autres volets, en deuxième lieu, détacher de la liasse l'avis de passage et y mentionner le motif de non distribution, la date et l'heure à partir desquelles le pli peut être retiré au bureau d'instance et le nom et l'adresse de ce bureau, cette dernière indication pouvant résulter de l'apposition d'une étiquette adhésive, en troisième lieu, déposer l'avis ainsi complété dans la boîte aux lettres du destinataire et, enfin, reporter sur le pli le motif de non distribution et le nom du bureau d'instance. <br/>
<br/>
              5. Compte tenu de ces modalités, et notamment de la circonstance que la date de dépôt d'un avis de passage est nécessairement la même que la date de présentation apposée sur la preuve de distribution qui se duplique sur les autres volets de la liasse, le volet " avis de réception " sur lequel a été apposé par voie de duplication la date de vaine présentation du courrier, ainsi que la date de distribution et la signature du destinataire, retourné à l'expéditeur après que le destinataire a retiré le pli qui lui est destiné, atteste de la date à laquelle celui-ci a été régulièrement avisé de sa mise en instance. <br/>
<br/>
              6. Lorsque le contribuable a retiré le pli contenant la proposition de rectification au bureau de poste après une vaine présentation à son domicile, il résulte de ce qui a été dit aux points 3 et 5 que la date de vaine présentation qui figure sur le volet " avis de réception " d'une lettre remise contre signature doit être regardée comme celle à laquelle le contribuable a été avisé à son adresse de la mise en instance du pli contenant la proposition de rectification adressée par l'administration fiscale et, par suite, comme la date d'interruption de la prescription.<br/>
<br/>
              7. Il ressort des pièces du dossier soumis aux juges du fond que le ministre a produit la copie d'un avis de réception à l'adresse de M. et MmeB..., portant la date manuscrite de présentation du 28 décembre 2008 et de distribution du 7 janvier 2009, ainsi que la signature du destinataire. Par suite, en estimant que, dès lors que cet avis de réception ne comportait aucune mention selon laquelle les intéressés auraient été avisés, dès la date de présentation du pli, par le dépôt d'un avis de passage, de sa mise en instance, il n'était pas établi que M. et MmeB..., qui ne l'ont retiré que le 7 janvier 2009, aient été à même de prendre connaissance de la proposition de rectification qui leur était adressée avant le 1er janvier 2009, date à laquelle l'administration ne pouvait plus exercer son droit de reprise sur l'année 2005, la cour a entaché son arrêt d'une erreur de droit. Dès lors, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, l'arrêt attaqué doit être annulé en tant qu'il décharge M. et Mme B...des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles ils ont été assujettis au titre de l'année 2005, qu'il réforme, dans cette mesure, le jugement du 28 novembre 2013 du tribunal administratif d'Amiens et confirme la décharge des pénalités dues au titre de l'année 2005.<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante, la somme que M. et Mme B...demandent au titre des frais exposés par eux et non compris dans les dépens.<br/>
<br/>
<br/>
<br/>
<br/>
              D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 2 juillet 2015 de la cour administrative d'appel de Douai est annulé en tant qu'il décharge M. et Mme B...des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles ils ont été assujettis au titre de l'année 2005, qu'il réforme, dans cette mesure, le jugement du 28 novembre 2013 du tribunal administratif d'Amiens et confirme la décharge des pénalités dues au titre de l'année 2005.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Douai.<br/>
Article 3 : Les conclusions de M. et Mme B...tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'économie et des finances et à M. et Mme A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
