<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030509776</ID>
<ANCIEN_ID>JG_L_2015_04_000000366726</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/50/97/CETATEXT000030509776.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème SSJS, 17/04/2015, 366726, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-04-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366726</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>RICARD ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Guillaume Déderen</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:366726.20150417</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 11 mars et 11 juin 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour M.B..., demeurant ... ; M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 12BX00047 du 10 janvier 2013 par lequel la cour administrative d'appel de Bordeaux a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0805358 du 8 novembre 2011 par lequel le tribunal administratif de Bordeaux a rejeté sa demande tendant à l'annulation de la décision implicite du maire de Toulenne rejetant sa demande d'abrogation de la délibération du conseil municipal du 4 mars 2008 approuvant le nouveau plan local d'urbanisme et, d'autre part, à l'annulation de la décision attaquée et à ce qu'il soit enjoint au maire de Toulenne de convoquer le conseil municipal et de le saisir de cette demande d'abrogation dans un délai de deux mois à compter de la notification de la décision à intervenir, sous astreinte de 100 euros par jour de retard ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Toulenne la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Déderen, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M. A...et à Me Ricard, avocat de la commune de Toulenne ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article R. 611-1 du code de justice administrative : " La requête et les mémoires, ainsi que les pièces produites par les parties, sont déposés ou adressés au greffe. / La requête, le mémoire complémentaire annoncé dans la requête et le premier mémoire de chaque défendeur sont communiqués aux parties avec les pièces jointes dans les conditions prévues aux articles R. 611-3, R. 611-5 et R. 611-6. / Les répliques, autres mémoires et pièces sont communiqués s'ils contiennent des éléments nouveaux." ; qu'aux termes du premier alinéa de l'article R. 613-2 du même code : " Si le président de la formation de jugement n'a pas pris une ordonnance de clôture, l'instruction est close trois jours francs avant la date de l'audience indiquée dans l'avis d'audience prévu à l'article R. 711-2. Cet avis le mentionne. " ; qu'aux termes de l'article R. 613-3 du même code : " Les mémoires produits après la clôture de l'instruction ne donnent pas lieu à communication et ne sont pas examinés par la juridiction. / Si les parties présentent avant la clôture de l'instruction des conclusions nouvelles ou des moyens nouveaux, la juridiction ne peut les adopter sans ordonner un supplément d'instruction. " ; qu'aux termes de l'article R. 613-4 du même code : " Le président de la formation de jugement peut rouvrir l'instruction par une décision qui n'est pas motivée et ne peut faire l'objet d'aucun recours. (...) / La réouverture de l'instruction peut également résulter d'un jugement ou d'une mesure d'investigation ordonnant un supplément d'instruction. / Les mémoires qui auraient été produits pendant la période comprise entre la clôture et la réouverture de l'instruction sont communiqués aux parties. " ;  <br/>
<br/>
              2. Considérant qu'il résulte de ces dispositions que lorsqu'il décide de soumettre au contradictoire une production de l'une des parties après la clôture de l'instruction, même si cette production a été enregistrée avant celle-ci, le président de la formation de jugement du tribunal administratif ou de la cour administrative d'appel doit être regardé comme ayant rouvert l'instruction ; que lorsque le délai qui reste à courir jusqu'à la date de l'audience ne permet plus l'intervention de la clôture automatique trois jours francs avant l'audience prévue par l'article R. 613-2 du code de justice administrative mentionné ci-dessus, il appartient à ce dernier, qui, par ailleurs, peut toujours, s'il l'estime nécessaire, fixer une nouvelle date d'audience, de clore l'instruction ainsi rouverte ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier d'appel que, le 12 décembre 2012, soit à la veille de l'audience publique à l'issue de laquelle a été rendu l'arrêt attaqué et après clôture de l'instruction, intervenue trois jours francs avant l'audience en application des dispositions de l'article R. 613-2 du code de justice administrative, le greffe de la cour administrative d'appel de Bordeaux a communiqué à M.A..., qui avait la qualité de partie requérante dans cette instance, un nouveau mémoire en défense, enregistré le 10 décembre 2012, de la commune de Toulenne ; qu'il résulte de ce qui a été dit plus haut que cette communication a eu pour effet de rouvrir l'instruction ; que, par suite, en s'abstenant de clore à nouveau l'instruction alors que le délai de trois jours francs prévu par l'article R. 613-2 était expiré, la cour administrative d'appel a rendu son arrêt au terme d'une procédure irrégulière, alors même que le requérant a produit une note en délibéré et que le mémoire en défense aurait fait l'objet d'un échange direct entre les parties ; que, dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, le requérant est fondé à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Toulenne la somme de 2 000 euros à verser à M. A...en application des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font, en revanche, obstacle à ce qu'une somme soit mise à ce titre à la charge de M. A... qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 10 janvier 2013 de la cour administrative d'appel de Bordeaux est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux.<br/>
<br/>
Article 3 : La commune de Toulenne versera à M. A...la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : Les conclusions de la commune de Toulenne présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 5 : La présente décision sera notifiée à M. B...et à la commune de Toulenne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
