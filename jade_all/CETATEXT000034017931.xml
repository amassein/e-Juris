<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034017931</ID>
<ANCIEN_ID>JG_L_2017_02_000000402420</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/01/79/CETATEXT000034017931.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 08/02/2017, 402420, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-02-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402420</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Clément Malverti</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:402420.20170208</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par trois mémoires, enregistrés les 14 novembre 2016, 16 et 27 janvier 2017 au secrétariat du contentieux du Conseil d'Etat, présentés en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, la Section française de l'Observatoire international des prisons demande au Conseil d'Etat, à l'appui de sa requête tendant à l'annulation pour excès de pouvoir de l'arrêté du 9 juin 2016 portant création de traitements de données à caractère personnel relatifs à la vidéoprotection de cellules de détention, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions des articles 58-1 de la loi du 24 novembre 2009 pénitentiaire et 716-1 A du code de procédure pénale résultant de l'article 9 de la loi du 21 juillet 2016.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de procédure pénale ;<br/>
              - la loi n° 2009-1436 du 24 novembre 2009 ;<br/>
              - la loi n° 2016-987 du 21 juillet 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Clément Malverti, auditeur, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la Section française de l'Observatoire international des prisons ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2.	Considérant que, par un arrêté du 9 juin 2016, le garde des sceaux, ministre de la justice a autorisé " la mise en oeuvre par la direction de l'administration pénitentiaire de traitements de données à caractère personnel relatifs aux systèmes de vidéoprotection de cellules de détention au sein des établissements pénitentiaires ", ayant " pour finalité le contrôle sous vidéoprotection de cellules de détention dans lesquelles sont affectées les personnes placées sous main de justice, faisant l'objet d'une mesure d'isolement, dont l'évasion ou le suicide pourraient avoir un impact important sur l'ordre public eu égard aux circonstances particulières à l'origine de leur incarcération et l'impact de celles-ci sur l'opinion publique " ; qu'à l'appui du recours pour excès de pouvoir qu'elle a formé pour demander l'annulation de cet arrêté, la Section française de l'Observatoire international des prisons demande que soit renvoyée au Conseil constitutionnel, en application de l'article 23-5 de l'ordonnance du 7 novembre 1958, la question de la conformité aux droits et libertés garantis par la Constitution des dispositions des articles 58-1 de la loi du 24 novembre 2009 pénitentiaire et 716-1 A du code de procédure pénale, telles qu'elles résultent de l'article 9 de la loi du 21 juillet 2016 prorogeant l'application de la loi n° 55-385 du 3 avril 1955 relative à l'état d'urgence et portant mesures de renforcement de la lutte antiterroriste ;<br/>
<br/>
              3.	Considérant que la requête de la Section française de l'Observatoire international des prisons tend à l'annulation pour excès de pouvoir de l'arrêté du 9 juin 2016 ; que les dispositions législatives mises en cause par la question prioritaire de constitutionnalité sont issues de la loi du 21 juillet 2016 ; que ces dispositions sont dépourvues de caractère rétroactif ; que, n'étant pas en vigueur à la date de l'arrêté attaqué, elles ne sont pas applicables au litige ;<br/>
<br/>
              4.	Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par le garde des sceaux, ministre de la justice, qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la Section française de l'Observatoire international des prisons.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la Section française de l'Observatoire international des prisons et au garde des sceaux, ministre de la justice. Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
