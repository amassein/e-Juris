<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043215966</ID>
<ANCIEN_ID>JG_L_2021_02_000000449424</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/21/59/CETATEXT000043215966.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 25/02/2021, 449424, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>449424</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:449424.20210225</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au juge des référés de tribunal administratif de Nice, statuant sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative, d'enjoindre au préfet des Alpes-Maritimes de lui délivrer sans délai une autorisation provisoire de séjour avec autorisation de travail, sous astreinte de 100 euros par jour de retard. Par une ordonnance n° 2100034 du 11 janvier 2021, le juge des référés du tribunal administratif de Nice a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 4 février 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative : <br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) d'enjoindre au préfet des Alpes-Maritimes de lui délivrer une carte de séjour temporaire " vie privée et familiale " ou une autorisation provisoire de séjour dans un délai de 48 heures ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la situation d'urgence est caractérisée, dès lors que la dernière autorisation provisoire de séjour dont il était titulaire a expiré le 7 janvier 2021, ne lui permettant plus de justifier de la régularité de sa situation administrative, de faire valoir ses droits aux prestations sociales, de rechercher efficacement un emploi et de circuler librement ;<br/>
              - le juge des référés du tribunal administratif de Nice a commis une erreur de droit en se fondant, pour rejeter sa demande, sur ce que les procédures tendant à obtenir l'exécution d'une décision juridictionnelle et le prononcé de mesures d'urgence susceptibles d'avoir le même effet étaient exclusives l'une de l'autre ;<br/>
              - en s'abstenant de renouveler son autorisation provisoire de séjour en exécution d'un jugement du 18 mars 2020, rectifié, du tribunal administratif de Nice, le préfet des Alpes-Maritimes a porté une atteinte grave et manifestement illégale à des libertés fondamentales, notamment à sa liberté de travailler et à sa liberté d'aller et venir.<br/>
<br/>
              Par un mémoire en défense, enregistré le 18 février 2021, le ministre de l'intérieur conclut au non-lieu à statuer. Il soutient que le requérant a été invité, par courrier du 17 février 2021, à se présenter le 26 février suivant à la préfecture des Alpes-Maritimes afin de se voir délivrer une autorisation provisoire de séjour avec droit au travail, dans l'attente de la fabrication de son titre de séjour en exécution du jugement du tribunal administratif de Nice du 18 mars 2020.<br/>
<br/>
              Par un mémoire en réplique, enregistré le 23 février 2021, M. A... conclut au maintien de l'ensemble de ses conclusions. Il soutient que le préfet des Alpes-Maritimes n'a pas exécuté le jugement du 18 mars 2020 du tribunal administratif de Nice malgré ses nombreuses sollicitations, qu'il importe de se prononcer sur l'office du juge des référés en cas d'inexécution d'une décision juridictionnelle et que sa demande au titre de l'article L. 761-1 du code de justice administrative est justifiée par les démarches qu'il a dû accomplir pour obtenir un document de séjour valide.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative, l'ordonnance n° 2020-1402 du 18 novembre 2020 et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Les parties ont été informées, sur le fondement de l'article 3 de l'ordonnance n° 2020-1402 du 18 novembre 2020 portant adaptation des règles applicables aux juridictions administratives, de ce qu'aucune audience ne se tiendrait et de ce que la clôture de l'instruction serait fixée le 23 février 2021 à 18 heures, puis le 24 février 2021 à 16 heures.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". <br/>
<br/>
              2. Si la circonstance que la partie intéressée ait saisi le tribunal administratif selon la procédure prévue par l'article L. 911-4 du code de justice administrative ne fait pas, par elle-même, obstacle à ce qu'elle présente au juge des référés une demande tendant à ce qu'il ordonne, sur le fondement de l'article L. 521-2 du code de justice administrative, une mesure d'urgence susceptible d'avoir le même effet, il résulte de l'instruction que postérieurement à l'introduction de la requête, le préfet des Alpes-Maritimes a, par courrier du 17 février 2021, invité M. A... à se présenter le 26 février suivant à la préfecture afin de se voir délivrer une autorisation provisoire de séjour l'autorisant à travailler, comme demandé au juge des référés de première instance. Par le même courrier, le préfet a indiqué que cette délivrance intervenait dans l'attente de la fabrication du titre de séjour qui sera remis à M. A... en exécution d'un jugement du tribunal administratif de Nice ayant annulé la décision lui refusant la délivrance d'un tel titre. Dans ces conditions, les conclusions du requérant tendant à ce que le juge des référés fasse usage des pouvoirs qu'il tient de l'article L. 521-2 du code de justice administrative afin d'enjoindre au préfet des Alpes-Maritimes de lui octroyer un document de séjour lui permettant notamment de travailler sont devenues sans objet. Il n'y a donc plus lieu d'y statuer.<br/>
<br/>
              3. Il y a lieu, dans les circonstances de l'espèce, de mettre une somme de 1 000 euros à la charge de l'Etat à verser à M. A... au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions de la requête présentées par M. A... sur le fondement de l'article L. 521-2 du code de justice administrative.<br/>
Article 2 : L'Etat versera une somme de 1 000 euros à M. A... en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente ordonnance sera notifiée à M. B... A... et au ministre de l'intérieur.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
