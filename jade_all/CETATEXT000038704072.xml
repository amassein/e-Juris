<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038704072</ID>
<ANCIEN_ID>JG_L_2019_06_000000415922</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/70/40/CETATEXT000038704072.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 28/06/2019, 415922</TITRE>
<DATE_DEC>2019-06-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415922</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>Mme Déborah Coricon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:415922.20190628</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Clermont-Ferrand d'annuler la décision du 30 octobre 2014 du directeur de l'école nationale des finances publiques la déclarant redevable de l'indemnité de rupture d'engagement d'un montant de 18 098 euros ainsi que la décision du 23 janvier 2015 rejetant son recours gracieux. Par un jugement n°150835 du 4 novembre 2015, le tribunal administratif de Clermont-Ferrand a annulé ces décisions. <br/>
<br/>
              Mme B...a également demandé au tribunal administratif de Clermont-Ferrand d'annuler la décision du 15 septembre 2015 du directeur général des finances publiques ramenant le montant de cette indemnité à la somme de 14 620,57 euros ainsi que la décision du 22 septembre 2015 du directeur confirmant ce montant et la décision implicite de rejet de son recours hiérarchique formé contre les décisions des 30 octobre 2014 et 23 janvier 2015. Par un jugement n° 1502110 du 29 décembre 2016, le tribunal administratif de Clermont-Ferrand a annulé ces décisions.<br/>
<br/>
              Par un arrêt n° 15LY03959, 17LY00360 du 21 septembre 2017, la cour administrative d'appel de Lyon a joint les deux affaires, a annulé les jugements des 4 novembre 2015 et 29 décembre 2016, a déclaré sans objet la demande de Mme B...à hauteur de 3 477,43 euros et a rejeté le surplus de ses demandes.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 23 novembre 2017, 31 janvier et 7 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 1er et 3 de cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit, dans cette mesure, à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code monétaire et financier ;<br/>
              - le décret n° 2010-986 du 26 août 2010 ;<br/>
              - le décret n° 2012-1246 du 7 novembre 2012 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Déborah Coricon, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat de Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme B..., après son admission au concours d'inspecteur des finances publiques, a signé le 23 mars 2013 un engagement de servir l'Etat pour une période de huit années, sous peine de devoir reverser au Trésor public une indemnité de rupture d'engagement. Reçue en juin 2014 au concours externe de rédacteur de la Banque de France, elle a présenté, le 18 juillet 2014, à l'issue de son année de scolarité rémunérée, sa démission des cadres de la direction générale des finances publiques. Par deux jugements des 4 novembre 2015 et 29 décembre 2016, le tribunal administratif de Paris a annulé les décisions des 30 octobre 2014 et 22 septembre 2015 du directeur de l'école nationale des finances publiques lui réclamant le paiement d'une indemnité de rupture d'engagement de servir l'Etat évaluée à 18 098 euros, la décision du 23 janvier 2015 du directeur de la formation initiale de cet établissement rejetant son recours gracieux et la décision du 15 septembre 2015 ramenant le montant de l'indemnité de rupture de 18 098 euros à 14 620,57 euros. La cour administrative d'appel de Lyon, sur appel du ministre des finances et des comptes publics, a annulé les deux jugements, a constaté le non-lieu à statuer à hauteur de 3 477,43 euros et a rejeté le surplus des demandes et des conclusions d'appel de MmeB.... Celle-ci demande l'annulation des articles 1er et 3 de cet arrêt.  <br/>
<br/>
              2. Aux termes de l'article 12 du décret du 26 août 2010 portant statut particulier des personnels de catégorie A de la direction générale des finances publiques : " Les inspecteurs des finances publiques stagiaires sont astreints à rester au service de l'Etat ou de ses établissements publics à caractère administratif pendant une période minimum de huit ans, la durée de la formation professionnelle mentionnée à l'article 11 ne pouvant être prise en compte au titre de cette période que dans la limite d'un an. En cas de manquement à cette obligation plus de quatre mois après la date de prise de fonctions en qualité d'inspecteur des finances publiques stagiaire, les intéressés doivent, sauf si le manquement ne leur est pas imputable, verser au Trésor une somme correspondant au traitement et à l'indemnité de résidence perçus en qualité d'inspecteur des finances publiques stagiaire ainsi qu'aux dépenses de toute nature résultant de leur séjour à l'école. Le montant de cette somme est fixé par arrêté du ministre chargé du budget. / La durée de service effectuée dans un emploi relevant de la fonction publique territoriale ou de la fonction publique hospitalière ou au sein des services de l'Union européenne ou dans l'administration d'un Etat membre de l'Union européenne ou d'un autre Etat partie à l'accord sur l'Espace économique européen est prise en compte au titre de l'engagement de servir mentionné à l'alinéa précédent (...) ". <br/>
<br/>
              3. Aux termes de l'article L. 142-1 du code monétaire et financier : " La Banque de France est une institution dont le capital appartient à l'Etat ". Si la Banque de France constitue une personne publique chargée par la loi de missions de service public, elle n'a pas le caractère d'un établissement public, mais revêt une nature particulière et présente des caractéristiques propres. Ses agents sont notamment des agents publics régis par des statuts agréés par l'Etat, alors même qu'ils sont aussi soumis aux dispositions du code du travail en ce qu'elles ne sont pas contraires à celles des statuts particuliers. <br/>
<br/>
              4. Il résulte de ces dispositions que les services accomplis par les rédacteurs de la Banque de France, recrutés par la voie d'un concours national et titularisés à l'issue d'un stage probatoire, doivent être regardés, compte tenu de la qualité de personne publique de la Banque de France et de la nature de ses missions, comme assimilables aux services mentionnés au 1er alinéa de l'article 12 du décret du 26 août 2010 précité pour déterminer la durée de l'engagement de servir auquel sont soumis les personnels de catégories A de la direction générale des finances publiques. Par suite, en jugeant que la durée des services accomplis par Mme B...à la Banque de France ne pouvait être prise en compte pour l'application de ces dispositions du décret du 26 août 2010, la cour a commis une erreur de droit. <br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à Mme B...au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er et 3 de l'arrêt du 21 septembre 2017 de la cour administrative d'appel de Lyon sont annulés.<br/>
Article 2 : L'affaire est renvoyée, dans la limite de la cassation prononcée, à la cour administrative d'appel de Lyon. <br/>
Article 3 : L'Etat versera une somme de 3 000 euros à Mme B...au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à Mme A...B...et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-07-11-005 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. OBLIGATIONS DES FONCTIONNAIRES. ENGAGEMENT DE SERVIR L'ÉTAT. - NOTION DE SERVICE DE L'ETAT (ART. 12 DU DÉCRET DU 26 AOÛT 2010) - SERVICES ACCOMPLIS PAR LES RÉDACTEURS DE LA BANQUE DE FRANCE [RJ1] - INCLUSION.
</SCT>
<ANA ID="9A"> 36-07-11-005 Si la Banque de France constitue une personne publique chargée par la loi de missions de service public, elle n'a pas le caractère d'un établissement public, mais revêt une nature particulière et présente des caractéristiques propres. Ses agents sont notamment des agents publics régis par des statuts agréés par l'Etat, alors même qu'ils sont aussi soumis aux dispositions du code du travail en ce qu'elles ne sont pas contraires à celles des statuts particuliers.,,,Les services accomplis par les rédacteurs de la Banque de France, recrutés par la voie d'un concours national et titularisés à l'issue d'un stage probatoire, doivent être regardés, compte tenu de la qualité de personne publique de la Banque de France et de la nature de ses missions, comme assimilables aux services mentionnés au 1er alinéa de l'article 12 du décret n° 2010-986 du 26 août 2010 pour déterminer la durée de l'engagement de servir auquel sont soumis les personnels de catégories A de la direction générale des finances publiques.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur la nature juridique de la banque de France, CE, 22 mars 2000, Syndicat national autonome du personnel et de la banque de France et autres, n°s 203854 203855 204029, p. 125.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
