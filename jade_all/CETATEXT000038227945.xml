<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038227945</ID>
<ANCIEN_ID>JG_L_2019_03_000000400059</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/22/79/CETATEXT000038227945.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 13/03/2019, 400059, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-03-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400059</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>M. Aurélien Caron</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:400059.20190313</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société anonyme Leroy Merlin France a demandé au tribunal administratif de Besançon de prononcer la restitution d'une partie de la taxe sur les surfaces commerciales qu'elle a acquittée au titre des années 2010, 2011 et 2012, pour son établissement situé à Besançon (Doubs). Par un jugement n° 1400492 du 4 décembre 2014, le tribunal a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15NC00083 du 24 mars 2016, la cour administrative d'appel de Nancy a rejeté l'appel formé par la société Leroy Merlin France contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un mémoire en réplique et des observations complémentaires, enregistrés les 24 mai et 24 août 2016 et les 26 avril et 2 octobre 2018 au secrétariat du contentieux du Conseil d'Etat, la société Leroy Merlin France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) d'ordonner la communication du rescrit fiscal adressé à la Fédération française du négoce de l'ameublement et de l'équipement de la maison pour ses adhérents en matière de taxe sur les surfaces commerciales ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la loi n° 72-657 du 13 juillet 1972 ;<br/>
              - le décret n° 95-85 du 26 janvier 1995 ; <br/>
              - vu la décision n° 400059 du 28 septembre 2018 du Conseil d'Etat statuant au contentieux ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Aurélien Caron, auditeur,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de la société Leroy Merlin France.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 21 février 2019, présentée par la société Leroy Merlin France.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Leroy Merlin France a été assujettie à la taxe sur les surfaces commerciales au titre des années 2010, 2011 et 2012 à raison d'un établissement situé à Besançon (Doubs). Par une décision du 31 janvier 2014, l'administration fiscale a rejeté sa demande de restitution de la somme totale de 66 624 euros, correspondant à l'application de la réduction de taux de 30 % prévue par les dispositions du A de l'article 3 du décret du 26 janvier 1995 en ce qui concerne la vente exclusive de certaines marchandises par des professions exerçant une activité nécessitant des surfaces de vente anormalement élevées. La société se pourvoit en cassation contre l'arrêt du 24 mars 2016 par lequel la cour administrative d'appel de Nancy a rejeté l'appel qu'elle avait formé contre le jugement du 4 décembre 2014 du tribunal administratif de Besançon confirmant ce refus.<br/>
<br/>
              2. Aux termes de l'article 3 de la loi du 13 juillet 1972 instituant des mesures en faveur de certaines catégories de commerçants et artisans âgés, dans sa rédaction applicable aux années d'imposition en litige : " Il est institué une taxe sur les surfaces commerciales assise sur la surface de vente des magasins de commerce de détail, dès lors qu'elle dépasse quatre cents mètres carrés des établissements ouverts à partir du 1er janvier 1960 quelle que soit la forme juridique de l'entreprise qui les exploite. (...) / Un décret prévoira (...) des réductions pour les professions dont l'exercice requiert des superficies de vente anormalement élevées (...) ". Aux termes de l'article 3 du décret du 26 janvier 1995 relatif à la taxe sur les surfaces commerciales, pris pour l'application de ces dispositions, dans sa rédaction applicable aux années d'imposition en litige : " A. - La réduction de taux prévue au dix-septième alinéa de l'article 3 de la loi du 13 juillet 1972 susvisé en faveur des professions dont l'exercice requiert des superficies de vente anormalement élevées est fixée à 30 p. 100 en ce qui concerne la vente exclusive des marchandises énumérées ci-après : / - meubles meublants ; / (...) - matériaux de construction (...) ".<br/>
<br/>
              Sur la taxe sur les surfaces commerciales acquittée au titre de l'année 2010 :<br/>
<br/>
              3. Il résulte des dispositions citées au point 2 qu'en subordonnant, par le A de l'article 3 du décret du 26 janvier 1995, le bénéfice de la réduction de taux, fixée à 30 %, à la condition que l'activité de vente des marchandises qu'il énumère soit exercée à titre exclusif, le pouvoir réglementaire s'est borné à déterminer le champ d'application de la mesure de réduction de taux prévue par le législateur en faveur des professions dont l'exercice requiert des superficies de vente anormalement élevées, sans excéder les compétences qu'il tenait des dispositions législatives citées au point 2. <br/>
<br/>
              4. Par suite, la cour administrative d'appel de Nancy a entaché son arrêt d'une erreur de droit en se fondant, avant de rejeter la demande de la requérante tendant au bénéfice de cette réduction, sur ce que le pouvoir réglementaire avait, en posant une condition d'exclusivité non prévue par la loi, entaché les dispositions du A de l'article 3 du décret du 26 janvier 1995 d'illégalité. Dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société Leroy-Merlin France est fondée à demander l'annulation de l'arrêt qu'elle attaque en tant qu'il statue sur l'imposition acquittée au titre de l'année 2010.<br/>
<br/>
              Sur la taxe sur les surfaces commerciales acquittée au titre des années 2011 et 2012 :<br/>
<br/>
              5. En vertu du 4° de l'article R. 811-1 du code de justice administrative, le tribunal administratif statue en premier et dernier ressort " sur les litiges relatifs aux impôts locaux et à la contribution à l'audiovisuel public, à l'exception des litiges relatifs à la contribution économique territoriale ". Pour l'application de ces dispositions, doit être regardé comme un impôt local tout impôt dont le produit, pour l'année d'imposition en cause, est majoritairement affecté aux collectivités territoriales, à leurs groupements ou aux établissements publics qui en dépendent.<br/>
<br/>
              6. La taxe sur les surfaces commerciales due au titre des années d'imposition 2011 et suivantes constitue, du fait de son affectation aux communes et établissements publics de coopération intercommunale, un impôt local au sens du 4° de l'article R. 811-1 du code de justice administrative. Le tribunal administratif statue donc en premier et dernier ressort sur les litiges relatifs à la taxe sur les surfaces commerciales. La cour administrative d'appel de Nancy était par suite incompétente pour statuer sur les conclusions de la société Leroy Merlin France en tant qu'elles concernaient les années 2011 et 2012. Dès lors, son arrêt doit, dans cette mesure, être annulé. Ce moyen suffisant à entraîner l'annulation de l'arrêt dans cette mesure, il n'est pas nécessaire d'examiner les moyens du pourvoi.<br/>
<br/>
              7. Il appartient au Conseil d'Etat de statuer, en tant que juge de cassation, sur les conclusions, présentées par la société Leroy Merlin France devant la cour administrative d'appel de Nancy, dirigées contre le jugement du 4 décembre 2014 du tribunal administratif de Besançon en tant qu'il a statué sur ces impositions.<br/>
<br/>
              8. Pour les mêmes motifs que ceux qui ont été exposés au point 3, le tribunal administratif n'a, en premier lieu, pas entaché son jugement d'erreur de droit en jugeant que les dispositions du A de l'article 3 du décret du 26 janvier 1995 prévoyant une condition d'exclusivité de l'activité de vente des marchandises qu'il énumère ne méconnaissent pas la portée de l'article 3 de la loi du 13 juillet 1972.<br/>
<br/>
              9. En deuxième lieu, les moyens tirés de ce que les dispositions du décret du 26 janvier 1995 seraient contraires au principe constitutionnel d'égalité devant les charges publiques, à la liberté d'entreprendre, à l'objectif à valeur constitutionnelle de bon emploi des deniers publics, ainsi qu'aux stipulations combinées de l'article 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et de l'article 1er de son protocole additionnel, qui ne sont pas d'ordre public et n'ont pas été présentés devant le tribunal administratif de Besançon, ne peuvent pas être utilement présentés, en cassation, à l'appui de conclusions dirigées contre le jugement de ce tribunal.<br/>
<br/>
              10. En dernier lieu, le tribunal administratif, après avoir relevé, par des motifs non argués de dénaturation, qu'il était constant que la société requérante commercialisait, outre des matériaux de construction et des meubles meublants, des articles de jardinage, de quincaillerie, d'outillage, de décoration, d'électricité, de plomberie et estimé, sans entacher son jugement d'inexacte qualification des faits, que ces articles ne constituaient pas de simples accessoires à des matériaux de construction, a pu juger sans méconnaitre les dispositions de l'article 3 de la loi du 13 juillet 1972 et du A de l'article 3 du décret du 26 janvier 1995 que la société ne respectait pas la condition de vente exclusive de meubles meublants et de matériaux de construction à laquelle est subordonné le bénéfice de la réduction de taux prévue par les dispositions précitées du A de l'article 3 du décret du 26 janvier 1995.<br/>
<br/>
              11. Il résulte de ce qui précède que le pourvoi de la société Leroy Merlin France dirigé contre le jugement du tribunal administratif en tant qu'il statue sur les impositions dues au titre des années 2011 et 2012 ne peut qu'être rejeté.<br/>
<br/>
              12. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 2 000 euros à verser à la société Leroy Merlin France au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 24 mars 2016 est annulé.<br/>
<br/>
Article 2 : Le jugement des conclusions présentées en appel par la société Leroy Merlin France et tendant à la restitution d'une partie de la taxe sur les surfaces commerciales qu'elle a acquittée au titre de l'année 2010 est renvoyé à la cour administrative d'appel de Nancy. <br/>
<br/>
Article 3 : Le pourvoi de la société Leroy Merlin dirigé contre le jugement du tribunal administratif de Besançon du 4 décembre 2014 en tant qu'il statue sur les impositions acquittées au titre des années 2011 et 2012 est rejeté.<br/>
<br/>
Article 4 : L'Etat versera une somme de 2 000 euros à la société Leroy Merlin France au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la société Leroy Merlin France et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
