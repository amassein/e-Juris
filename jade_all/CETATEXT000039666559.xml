<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039666559</ID>
<ANCIEN_ID>JG_L_2019_12_000000427686</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/66/65/CETATEXT000039666559.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 24/12/2019, 427686, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-12-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427686</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Olivier  Fuchs</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:427686.20191224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société Allis a demandé au tribunal administratif de Caen d'annuler pour excès de pouvoir la décision de l'inspecteur du travail de la 5ème section de l'unité territoriale du Calvados du 8 avril 2013 lui refusant l'autorisation de licencier Mme A... B... et celle du ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social du 19 septembre 2013 rejetant son recours dirigé contre cette décision. Par un jugement n° 1301986 du 17 décembre 2014, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15NT00107 du 29 mars 2016, la cour administrative d'appel de Nantes a, sur appel de la société Allis, dit n'y avoir pas lieu de statuer sur la demande formée par la société Allis.<br/>
<br/>
              Par une ordonnance n° 399809 du 22 décembre 2017, le président de la 4ème chambre du Conseil d'Etat a annulé l'arrêt de la cour administrative d'appel de Nantes et lui a renvoyé l'affaire.<br/>
<br/>
              Par un arrêt n° 17NT04012 du 3 décembre 2018, la cour administrative d'appel de Nantes a annulé la décision de l'inspecteur du travail du 8 avril 2013, réformé l'article 1er du jugement du tribunal administratif de Caen du 17 décembre 2014 en ce sens et rejeté le surplus des conclusions de la requête d'appel de la société Allis.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 5 février et 6 mai 2019 au secrétariat du contentieux du Conseil d'Etat, la société Allis demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il lui fait grief ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit au surplus des conclusions de sa requête d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Fuchs, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société Allis ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 8 avril 2013, l'inspecteur du travail de la 5ème section de l'unité territoriale du Calvados a refusé d'autoriser la société Allis à licencier Mme B..., salariée protégée. Par une décision du 19 septembre 2013, le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a rejeté le recours formé par l'employeur contre ce refus. Saisie en appel du jugement du 17 décembre 2014 par lequel le tribunal administratif de Caen a rejeté la demande de la société Allis tendant à l'annulation de ces deux décisions, la cour administrative d'appel de Nantes a, par un arrêt du 29 mars 2016, jugé qu'il n'y avait plus lieu de statuer sur ce litige. Par une ordonnance du 22 décembre 2017, le président de la 4ème chambre du Conseil d'Etat a annulé cet arrêt. La société Allis se pourvoit en cassation contre l'arrêt du 3 décembre 2018 par lequel la cour administrative d'appel de Nantes, statuant sur renvoi du Conseil d'Etat, a annulé la décision de l'inspecteur du travail du 8 avril 2013, réformé l'article 1er du jugement du tribunal administratif de Caen du 17 décembre 2014 en ce sens et rejeté le surplus des conclusions de la requête d'appel de la société Allis.<br/>
<br/>
              2. Lorsqu'il est saisi d'un recours hiérarchique contre une décision d'un inspecteur du travail statuant sur une demande d'autorisation de licenciement d'un salarié protégé, le ministre chargé du travail doit, si celle-ci est illégale, l'annuler. Par suite, lorsque le ministre rejette le recours hiérarchique dont il est saisi, il appartient au juge administratif, statuant sur un recours dirigé contre ces deux décisions, d'annuler, le cas échéant, celle du ministre par voie de conséquence de l'annulation de celle de l'inspecteur du travail.<br/>
<br/>
              3. Il ressort des énonciations de l'arrêt attaqué que, ainsi qu'il a été dit au point 1, la cour administrative d'appel a, sur appel de la société Allis, d'une part, annulé la décision de l'inspecteur du travail du 8 avril 2013 et réformé l'article 1er du jugement du tribunal administratif de Caen du 17 décembre 2014 en ce sens et, d'autre part, rejeté les conclusions de cette société tendant à l'annulation du jugement du tribunal rejetant sa demande d'annulation de la décision par laquelle le ministre chargé du travail a rejeté le recours hiérarchique formé contre cette décision. Il résulte de ce qui a été dit au point précédent qu'en statuant ainsi, alors qu'il lui appartenait, le cas échéant d'office, d'annuler la décision du ministre par voie de conséquence de l'illégalité de la décision de l'inspecteur du travail, la cour a commis une erreur de droit. Sans qu'il y ait lieu d'examiner les moyens du pourvoi, les articles 3 et 4 de son arrêt doivent, par suite, être annulés.<br/>
<br/>
              4. Aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire ". Le Conseil d'Etat étant saisi, en l'espèce, d'un second pourvoi en cassation, il lui incombe de régler l'affaire au fond.<br/>
<br/>
              5. En tant qu'il annule la décision de l'inspecteur du travail du 8 avril 2013, l'arrêt de la cour administrative d'appel de Nantes n'a pas été frappé de pourvoi en cassation. L'annulation de cette décision est, par suite, devenue définitive. Ainsi qu'il a été dit ci-dessus, son illégalité entraîne, par voie de conséquence, l'illégalité de la décision du 19 septembre 2013 du ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social rejetant le recours formé contre elle par la société Allis. Sans qu'il y ait lieu de se prononcer sur les moyens de sa requête, la société Allis est, par suite, fondée à soutenir que c'est à tort que, par son jugement du 17 décembre 2014, le tribunal administratif de Caen a rejeté sa demande d'annulation de la décision du ministre du 19 septembre 2013.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 250 euros à verser à la société Allis au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Allis qui, dans la présente instance, n'est pas la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Les articles 3 et 4 de l'arrêt de la cour administrative d'appel de Nantes du 3 décembre 2018 sont annulés.<br/>
Article 2 : Le jugement du tribunal administratif de Caen du 17 décembre 2014 est annulé en tant qu'il rejette les conclusions de la société Allis tendant à l'annulation de la décision du ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social du 19 septembre 2013.<br/>
Article 3 : La décision du ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social du 19 septembre 2013 est annulée.<br/>
Article 4 : L'Etat versera à la société Allis une somme de 250 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Les conclusions de Mme B... présentées en appel au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à la société Allis, à Mme A... B... et à la ministre du travail.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
