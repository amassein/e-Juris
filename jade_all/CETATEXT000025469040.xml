<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025469040</ID>
<ANCIEN_ID>JG_L_2012_03_000000331510</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/46/90/CETATEXT000025469040.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 09/03/2012, 331510</TITRE>
<DATE_DEC>2012-03-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>331510</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Christian Vigouroux</PRESIDENT>
<AVOCATS>FOUSSARD ; BALAT</AVOCATS>
<RAPPORTEUR>Mme Emilie Bokdam-Tognetti</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:331510.20120309</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 3 septembre 2009 au secrétariat du contentieux du Conseil d'Etat, présenté pour la SOCIETE D'INTERET COLLECTIF AGRICOLE FRUCA, dont le siège est Route du Pont Blanc à Cheval Blanc (84460), représentée par ses dirigeants ; la SOCIETE D'INTERET COLLECTIF AGRICOLE FRUCA demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 07MA04106 du 2 juillet 2009 par lequel la cour administrative d'appel de Marseille, faisant droit à l'appel de l'Office national interprofessionnel des fruits, des légumes, des vins et de l'horticulture (VINIFLHOR), venu aux droits de l'Office national interprofessionnel des fruits, des légumes et de l'horticulture (ONIFLHOR), a d'une part, annulé le jugement n° 0525615 du 23 juillet 2007 par lequel le tribunal administratif de Nîmes a annulé la décision du 6 juillet 2005 de l'ONIFLHOR refusant le versement de l'aide communautaire qu'elle avait sollicitée au titre du fonds opérationnel 2004 et, d'autre part, rejeté sa demande devant ce tribunal ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de VINIFLHOR ;<br/>
<br/>
              3°) d'enjoindre à VINIFLHOR, à titre principal, sur le fondement de l'article L. 911-1 du code de justice administrative, de procéder au versement de l'aide communautaire due au titre du fonds opérationnel 2004, et, à titre subsidiaire, sur le fondement de l'article L. 911-2 du même code, de procéder à une nouvelle instruction de sa demande de versement ; <br/>
<br/>
              4°) de mettre à la charge de VINIFHLOR la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le règlement (CE) n° 2200/96 du Conseil du 28 octobre 1996 ;<br/>
<br/>
              Vu le règlement (CE) n° 609/2001 de la Commission du 28 mars 2001 ;<br/>
<br/>
              Vu le règlement (CE) n° 1433/2003 de la Commission du 11 août 2003 ;<br/>
<br/>
              Vu le règlement (CE) n° 1582/2003 de la Commission du 10 septembre 2003 ;<br/>
<br/>
              Vu le code rural ;<br/>
<br/>
              Vu l'arrêté du 16 juillet 2001 portant modalités de mise en oeuvre du règlement (CE) n° 609/2001 de la Commission portant modalités d'application du règlement (CE) n° 2200/96 du Conseil relatif aux programmes opérationnels, aux fonds opérationnels et à l'aide financière communautaire des organisations de producteurs dans le secteur des fruits et légumes ;<br/>
<br/>
              Vu l'arrêté du 15 octobre 2003 portant modalités de mise en oeuvre du règlement (CE) n° 1433/2003 de la Commission portant modalités d'application du règlement (CE) n° 2200/96 du Conseil en ce qui concerne les fonds opérationnels, les programmes opérationnels et l'aide financière ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Emilie Bokdam-Tognetti, Auditeur,<br/>
<br/>
              - les observations de Me Foussard, avocat de la SOCIETE D'INTERET COLLECTIF AGRICOLE FRUCA et de Me Balat, avocat de l'Etablissement national des produits de l'agriculture et de la mer (FranceAgriMer), <br/>
<br/>
              - les conclusions de M. Edouard Geffray, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Foussard, avocat de la SOCIETE D'INTERET COLLECTIF AGRICOLE FRUCA et à Me Balat, avocat de l'Etablissement national des produits de l'agriculture et de la mer (FranceAgriMer) ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SOCIETE D'INTERET COLLECTIF AGRICOLE FRUCA, organisation de producteurs de fruits, a déposé, le 28 septembre 2001, un programme opérationnel pour la période 2002 à 2004, qui a été approuvé par le directeur départemental de l'agriculture et de la forêt du Vaucluse ; que, par une décision du 6 juillet 2005 prise à la suite d'un contrôle du fonds opérationnel 2004, l'Office national interprofessionnel des fruits, des légumes et de l'horticulture (ONIFLHOR) a rejeté la demande présentée par cette société tendant au versement de l'aide communautaire qu'elle avait sollicitée au titre de ce fonds, au motif notamment que ce dernier avait été alimenté de manière irrégulière ; que, par un jugement du 23 juillet 2007, le tribunal administratif de Nîmes, faisant droit à la demande de la SOCIETE D'INTERET COLLECTIF AGRICOLE FRUCA, a annulé cette décision ; que, par un arrêt du 2 juillet 2009, contre lequel la société se pourvoit en cassation, la cour administrative d'appel de Marseille a, sur appel de l'Office national interprofessionnel des fruits, des légumes, des vins et de l'horticulture (VINIFLHOR), venu aux droits de l'ONIFLHOR, annulé ce jugement et rejeté la demande de la société devant le tribunal administratif de Nîmes ;<br/>
<br/>
              Considérant, en premier lieu, qu'aux termes de l'article 28 du règlement (CE) n° 1433/2003 de la Commission du 11 août 2003 portant modalités d'application du règlement (CE) n° 2200/96 du Conseil en ce qui concerne les fonds opérationnels, les programmes opérationnels et l'aide financière, modifié par le règlement (CE) n° 1582/2003 de la Commission du 10 septembre 2003 : " Dispositions transitoires / Les programmes opérationnels approuvés par les États membres avant l'entrée en vigueur du présent règlement et dont la mise en oeuvre se poursuit en 2004 doivent se conformer au présent règlement. Les organisations de producteurs demandent les modifications nécessaires le 15 octobre 2003 au plus tard. / Les États membres peuvent prévoir le maintien de programmes approuvés avant l'entrée en vigueur du présent règlement. " ; qu'aux termes de l'article 29 de l'arrêté du 15 octobre 2003 portant modalités de mise en oeuvre de ce règlement : " Les dispositions du règlement n° 1433/2003 susvisé sont applicables à compter du 15 août 2003. Toutefois les organisations de producteurs sont autorisées à maintenir en l'état leurs programmes opérationnels approuvés, y compris les modifications approuvées avant cette date (...) " ; que l'article 30 du même arrêté dispose : " Les organisations de producteurs sont autorisées à maintenir en l'état leurs programmes opérationnels approuvés au titre de l'année 2004. / Si une organisation de producteurs présente une modification de son programme au titre de l'année 2004, l'intégralité de son fonds 2004 devra se conformer aux dispositions du règlement n° 1433/2003 susvisé " ; qu'enfin, aux termes de l'article 31 du même arrêté : " L'arrêté du 16 juillet 2001 portant modalités de mise en oeuvre du règlement (CE) n° 609/2001 de la Commission portant modalités d'application du règlement (CE) n° 2200/96 du Conseil relatif aux programmes opérationnels, aux fonds opérationnels et à l'aide communautaire des organisations de producteurs dans le secteur des fruits et légumes est abrogé./ Toutefois, ses dispositions continuent de s'appliquer lorsque les organisations de producteurs sont autorisées conformément aux articles 29 et 30 à maintenir en état leurs programmes opérationnels mis en oeuvre en 2003 et 2004 " ;<br/>
<br/>
              Considérant qu'en jugeant qu'il résultait des dispositions précitées que l'application, pour la période 2004, des dispositions du règlement (CE) n° 1433/2003 du 11 août 2003 et de l'arrêté du 15 octobre 2003 aux programmes opérationnels approuvés avant l'entrée en vigueur de ce règlement et dont l'exécution s'est poursuivie en 2004 était subordonnée à une demande, par l'organisme bénéficiaire, de modification du programme opérationnel au titre duquel l'aide communautaire prévue par ces dispositions était sollicitée, et en en déduisant qu'en l'absence d'une telle demande par la SOCIETE D'INTERET COLLECTIF AGRICOLE FRUCA, l'aide sollicitée par celle-ci demeurait soumise, pour le fonds 2004, aux dispositions du règlement (CE) n° 609/2001 de la Commission du 28 mars 2001 et de l'arrêté ministériel du 16 juillet 2001 portant modalités de mise en oeuvre de ce règlement, la cour qui, contrairement à ce que soutient la société requérante, n'a pas jugé que celle-ci aurait manifesté " l'intention " de bénéficier des dispositions du règlement (CE) n° 609/2001 de la Commission du 28 mars 2001 et de l'arrêté du 16 juillet 2001, n'a pas dénaturé les faits qui lui étaient soumis ; qu'en statuant ainsi, elle n'a pas commis d'erreur de droit et a fait une exacte application des dispositions par lesquelles l'arrêté ministériel du 15 octobre 2003 a défini les modalités de mise en oeuvre de la faculté ouverte aux Etats membres, par les dispositions précitées du règlement n° 1433/2003, de prévoir le " maintien " de programmes opérationnels approuvés avant son entrée en vigueur ;<br/>
<br/>
              Considérant, en second lieu, que l'article 15 du règlement (CE) n° 2200/96 du Conseil du 28 octobre 1996 portant organisation commune des marchés dans le secteur des fruits et légumes pose le principe suivant lequel le fonds opérationnel constitué par une organisation de producteurs " est alimenté par des contributions financières effectives des producteurs associés, assises sur les quantités ou la valeur des fruits et légumes effectivement commercialisées sur le marché, et par l'aide financière " communautaire ; que l'article 3 du règlement (CE) n° 609/2001 de la Commission du 28 mars 2001 portant modalités d'application du règlement (CE) du Conseil n° 2200/96 en ce qui concerne les programmes opérationnels, les fonds opérationnels et l'aide financière communautaire dispose : " (...) 2. Les contributions financières aux fonds opérationnels sont collectées conformément à l'article 15, paragraphe 1, du règlement (CE) n° 2200/96./ (...) / 4. Dans les conditions établies par les Etats membres, les organisations de producteurs peuvent faire l'avance à leurs associés de contributions au fonds pour une année donnée de mise en oeuvre du programme opérationnel, à condition d'en récupérer le montant auprès de leurs associés avant le 31 janvier de l'année suivante. /  5. Toute contribution au fonds opérationnel autre que les contributions des membres est inéligible à l'aide financière (...) " ; que l'article 10 de l'arrêté ministériel du 16 juillet 2001 dispose : " 1. Le fonds opérationnel est alimenté par les contributions effectives des producteurs adhérents de l'organisation de producteurs établies en application de l'article 13 du présent arrêté et par une aide financière communautaire (...) " ; qu'enfin, aux termes de l'article 13 du même arrêté : " 1. Les contributions financières des adhérents sont définies sur la base : / - du volume de la production commercialisée ; / - ou de la valeur de la production commercialisée ; /- ou sur une combinaison du volume et de la valeur de la production commercialisée. / Tous les producteurs de l'organisation de producteurs doivent contribuer à la constitution du fonds opérationnel (...) /2. Les organisations de producteurs peuvent faire l'avance des contributions de leurs membres sur leurs ressources propres, pour une année donnée, à condition d'en récupérer le montant auprès des producteurs avant le 31 janvier de l'année suivante (...) " ; <br/>
<br/>
              Considérant qu'il ressort de l'arrêt attaqué que la cour n'a pas jugé, contrairement à ce que soutient la société requérante, qu'un prélèvement direct, à la source, des contributions des adhérents sur le produit des ventes de leurs productions serait nécessairement contraire aux dispositions du règlement (CE) n° 609/2001 de la Commission du 28 mars 2001 et de l'arrêté du 16 juillet 2001, mais s'est bornée à relever, en portant sur les faits qui lui étaient soumis une appréciation souveraine exempte de dénaturation, que le fonds opérationnel 2004 de la SOCIETE D'INTERET COLLECTIF AGRICOLE FRUCA avait été alimenté par une contribution globale préalable prélevée sur les ressources propres de l'organisation de producteurs et qu'aucun prélèvement sur la valeur des ventes de la production commercialisée par chacun des adhérents de cette organisation de producteurs, pourtant prévu par l'article 10 de son règlement intérieur, n'avait été opéré au titre de cette année avant le 31 janvier 2005 pour compenser l'avance  ainsi consentie ; que le moyen tiré de ce que la cour aurait commis une erreur de droit en jugeant que les dispositions des règlements (CE) n° 2200/96 du 28 octobre 1996 et (CE) n° 609/2001 du 28 mars 2001 interdiraient que les contributions financières au fonds opérationnel soient collectées par un prélèvement effectué, à la source, sur la valeur de la production commercialisée de chacun des adhérents, ne peut, dès lors, qu'être écarté ; que doivent, de même, être écartés les moyens tirés de ce que la cour aurait insuffisamment motivé son arrêt et commis une erreur de droit en jugeant que l'article 10 du règlement intérieur de la SOCIETE D'INTERET COLLECTIF AGRICOLE FRUCA aurait instauré un système d'avance sur ressources propres ; <br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que la SOCIETE D'INTERET COLLECTIF AGRICOLE FRUCA n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; que ses conclusions à fin d'injonction ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etablissement national des produits de l'agriculture et de la mer (FranceAgriMer), venu aux droits de VINIFLHOR, qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la SOCIETE D'INTERET COLLECTIF AGRICOLE FRUCA la somme de 3 000 euros à verser à FranceAgriMer au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la SOCIETE D'INTERET COLLECTIF AGRICOLE FRUCA est rejeté.<br/>
Article 2 : La SOCIETE D'INTERET COLLECTIF AGRICOLE FRUCA versera 3 000 euros à FranceAgriMer au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la SOCIETE D'INTERET COLLECTIF AGRICOLE et à l'Etablissement national des produits de l'agriculture et de la mer (FranceAgriMer).<br/>
Copie en sera adressé pour information au ministre de l'agriculture, de l'alimentation, de la pêche, de la ruralité et de l'aménagement du territoire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">03-05-04 AGRICULTURE, CHASSE ET PÊCHE. PRODUITS AGRICOLES. FRUITS ET LÉGUMES. - PROGRAMMES OPÉRATIONNELS - APPLICABILITÉ, POUR LA PÉRIODE 2004, DU RÈGLEMENT N° 609/2001 AUX PROGRAMMES OPÉRATIONNELS CONCLUS AVANT L'ENTRÉE EN VIGUEUR DU NOUVEAU RÈGLEMENT N° 1433/2003 - EXISTENCE, EN L'ABSENCE DE DEMANDE DE LA SOCIÉTÉ TENDANT À L'APPLICATION DU NOUVEAU RÈGLEMENT.
</SCT>
<ANA ID="9A"> 03-05-04 Pour les programmes opérationnels de 3 ans prévus par les règlements communautaires  n° 609/2001 du 28 mars 2001 et n° 1433/2003 du 11 août 2003, l'arrêté ministériel du 15 octobre 2003 a pu prévoir que les organisations de producteurs sont autorisées, pour la période 2004, à « maintenir en l'état leurs programmes opérationnels » sans devoir les rendre conformes au nouveau règlement de 2003. En l'absence d'une demande par la société d'application de ce dernier règlement, l'aide sollicitée demeurait ainsi soumise au règlement de 2001.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
