<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043261196</ID>
<ANCIEN_ID>JG_L_2021_03_000000448010</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/26/11/CETATEXT000043261196.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 16/03/2021, 448010</TITRE>
<DATE_DEC>2021-03-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448010</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BUK LAMENT - ROBILLOT</AVOCATS>
<RAPPORTEUR>M. Joachim Bendavid</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:448010.20210316</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société d'exploitation de l'Arena, à l'appui de sa demande tendant à l'annulation pour excès de pouvoir des titres de recette émis à son encontre par le Préfet de police les 7 juin et 8 juillet 2019 et des décisions implicites par lesquelles le Préfet de police a rejeté ses recours, a produit un mémoire, enregistré le 30 avril 2020 au greffe du tribunal administratif de Cergy-Pontoise, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lequel elle soulève une question prioritaire de constitutionnalité. <br/>
<br/>
              Par un jugement n° 2003779 du 18 décembre 2020, enregistré le 21 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, le tribunal administratif de Cergy-Pontoise, avant qu'il soit statué sur la demande de la société d'exploitation de l'Arena, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution du deuxième alinéa de l'article L. 211-11 du code de la sécurité intérieure.<br/>
<br/>
              Dans la question prioritaire de constitutionnalité transmise et dans deux nouveaux mémoires, enregistrés les 15 février et 4 mars 2021, la société d'exploitation de l'Arena soutient que la portée effective donnée à ces dispositions par une jurisprudence constante du Conseil d'Etat méconnaît les articles 12 et 13 de la Déclaration des droits de l'homme et du citoyen et l'objectif de valeur constitutionnelle d'accessibilité et d'intelligibilité de la loi. Elle demande au Conseil d'Etat de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par deux mémoires, enregistrés les 1er février et 1er mars 2021, le ministre de l'intérieur soutient que les conditions posées par l'article 23-4 de l'ordonnance du 7 novembre 1958 ne sont pas remplies.<br/>
<br/>
              En application de l'article R. 611-7 du code de justice administrative, les parties ont été informées que la décision du Conseil d'Etat était susceptible d'être fondée sur un moyen relevé d'office, tiré de de ce que les conclusions présentées par la société d'exploitation de l'Arena au titre de l'article L. 761-1 du même code ne peuvent être portées que devant le juge saisi du litige à l'occasion duquel la question prioritaire de constitutionnalité a été soulevée et sont, par suite, irrecevables au stade de la décision statuant sur la seule demande de transmission de la question prioritaire de constitutionnalité.<br/>
<br/>
              La question a été transmise au Premier ministre qui n'a pas produit d'observations.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - le code de la sécurité intérieure ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Joachim Bendavid, auditeur,  <br/>
<br/>
              - les conclusions de Mme A... B..., rapporteure publique.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Buk-Lament-Robillot, avocat de la société d'exploitation de l'Arena.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances et que la question soit nouvelle ou présente un caractère sérieux. <br/>
<br/>
              2. Le deuxième alinéa de l'article L. 211-11 du code de la sécurité intérieure, dont la constitutionnalité est contestée par la société requérante, dispose que : " Les personnes physiques ou morales pour le compte desquelles sont mis en place par les forces de police ou de gendarmerie des services d'ordre qui ne peuvent être rattachés aux obligations normales incombant à la puissance publique en matière de maintien de l'ordre sont tenues de rembourser à l'Etat les dépenses supplémentaires qu'il a supportées dans leur intérêt ".<br/>
<br/>
              3. En premier lieu, la circonstance que le ministre de l'intérieur a, par une circulaire du 15 mai 2018, donné des dispositions contestées une interprétation qui, selon la société requérante, n'est pas conforme à la Constitution, est sans incidence sur leur constitutionnalité.<br/>
<br/>
              4. En deuxième lieu, la circonstance que, saisi d'un recours pour excès de pouvoir contre cette circulaire du 15 mai 2018, le Conseil d'Etat statuant au contentieux a, par une décision du 31 décembre 2019, écarté les moyens qui contestaient certains éléments d'interprétation retenus par cette circulaire et rejeté sur ce point les conclusions de la requête dont il était saisi, n'a conféré aux termes de cette circulaire aucune autorité de chose jugée. La société requérante n'est, par suite, pas fondée à soutenir que les dispositions contestées du deuxième alinéa de l'article L. 211-11 du code de la sécurité intérieure seraient contraires à la Constitution en raison d'une portée effective qui leur aurait été donnée, pour ce motif, par cette décision du Conseil d'Etat.<br/>
<br/>
              5. En troisième lieu, aux termes de l'article 12 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789 : " La garantie des droits de l'Homme et du Citoyen nécessite une force publique : cette force est donc instituée pour l'avantage de tous, et non pour l'utilité particulière de ceux auxquels elle est confiée ". Si les dispositions contestées prévoient que les forces de police ou de gendarmerie peuvent mettre en place un service d'ordre " pour le compte " de personnes privées, elles n'ont, en tout état de cause, ni pour objet ni pour effet de soumettre les forces de police ou de gendarmerie exerçant de telles missions à l'autorité de ces personnes privées. Ainsi, sans qu'ait à cet égard d'incidence la circonstance que les dispositions réglementaires d'application de ces dispositions ont prévu la signature d'une convention entre la personne privée et l'autorité compétente de l'Etat, la société d'exploitation de l'Arena n'est pas fondée à soutenir que le deuxième alinéa de l'article L. 211-11 du code de la sécurité intérieure méconnaît pour ce motif les dispositions précitées de l'article 12 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789.<br/>
<br/>
              6. En quatrième lieu, les dispositions contestées ne prévoient pas d'obligation, pour les personnes physiques ou morales qu'elles mentionnent, de confier aux forces de police ou de gendarmerie les services d'ordre qu'elles mettent en place pour leurs propres besoins et ne prévoient, lorsqu'elles décident d'y avoir recours, le remboursement à l'Etat que des seules dépenses correspondant aux missions qui, exercées dans leur intérêt, excèdent les besoins normaux de sécurité auxquels la collectivité est tenue de pourvoir dans l'intérêt général. La société d'exploitation de l'Arena n'est, par suite, pas fondée à soutenir que les dispositions contestées du deuxième alinéa de l'article L.211-11 du code de la sécurité intérieure font peser sur des personnes privées des dépenses qui incombent à l'Etat et qu'elles méconnaissent, pour ce motif, les dispositions de l'article 13 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789 relatives à l'égalité devant les charges publiques.<br/>
<br/>
              7. Enfin, la méconnaissance de l'objectif de valeur constitutionnelle d'accessibilité et d'intelligibilité de la loi ne peut, en elle-même, être invoquée à l'appui d'une question prioritaire de constitutionnalité sur le fondement de l'article 61-1 de la Constitution. <br/>
<br/>
              8. Il résulte de tout ce qui précède que la question soulevée, qui, contrairement à ce que soutient la société requérante, n'est pas nouvelle au sens des dispositions de l'article 23-4 de l'ordonnance organique du 7 novembre 1958, ne présente pas de caractère sérieux. Par suite, il n'y a pas lieu de la renvoyer au Conseil constitutionnel.<br/>
<br/>
              9. Les conclusions présentées par la société d'exploitation de l'Arena au titre de l'article L. 761-1 du code de justice administrative, qui ne peuvent être portées que devant le juge saisi du litige à l'occasion duquel la question prioritaire de constitutionnalité a été soulevée, sont irrecevables et doivent, par suite, être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le tribunal administratif de Cergy-Pontoise. <br/>
Article 2 :  Le surplus des conclusions de la société d'exploitation de l'Arena, présenté au titre de l'article L. 761-1 du code de justice administrative, est rejeté.<br/>
Article 3 : La présente décision sera notifiée à la société d'exploitation de l'Arena, au Premier ministre et au ministre de l'intérieur. <br/>
Copie en sera adressée au Conseil constitutionnel et au tribunal administratif de Cergy-Pontoise.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">49-04 POLICE. POLICE GÉNÉRALE. - MISE EN PLACE D'UN SERVICE D'ORDRE POUR LE COMPTE DE PERSONNES PRIVÉES (ART. L. 211-11 DU CSI) - 1) A) SOUMISSION DE LA FORCE PUBLIQUE À L'AUTORITÉ DE CES PERSONNES PRIVÉES - ABSENCE - B) CONSÉQUENCE - MÉCONNAISSANCE À CE TITRE DE L'ARTICLE 12 DE LA DDHC - ABSENCE - 2) A) OBLIGATION DE CONFIER À LA FORCE PUBLIQUE LES SERVICES D'ORDRE PRIVÉS - ABSENCE - B) REMBOURSEMENT PAR LES PERSONNES PRIVÉES DES SEULES MISSIONS QUI EXCÈDENT LES BESOINS NORMAUX DE SÉCURITÉ - C) CONSÉQUENCE - MÉCONNAISSANCE À CE TITRE DE L'ARTICLE 13 DE LA DDHC - ABSENCE.
</SCT>
<ANA ID="9A"> 49-04 1) a) Si le deuxième alinéa de l'article L. 211-11 du code de la sécurité intérieure (CSI) prévoit que les forces de police ou de gendarmerie peuvent mettre en place un service d'ordre pour le compte de personnes privées, elles n'ont, en tout état de cause, ni pour objet ni pour effet de soumettre les forces de police ou de gendarmerie exerçant de telles missions à l'autorité de ces personnes privées.,,,  b) Par suite et à ce titre, le deuxième alinéa de l'article L. 211-1 du CSI ne méconnaît pas l'article 12 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789 (DDHC).,,,2) a) Le deuxième alinéa de l'article L. 211-1 du CSI ne prévoit pas d'obligation, pour les personnes physiques ou morales qu'il mentionne, de confier aux forces de police ou de gendarmerie les services d'ordre qu'elles mettent en place pour leurs propres besoins.,,,b) Il ne prévoit, lorsque ces personnes décident d'y avoir recours, le remboursement à l'Etat que des seules dépenses correspondant aux missions qui, exercées dans leur intérêt, excèdent les besoins normaux de sécurité auxquels la collectivité est tenue de pourvoir dans l'intérêt général.... ,,c) Par suite, l'article L. 211-1 du CSI ne fait pas peser sur des personnes privées des dépenses qui incombent à l'Etat. Dès lors, il ne méconnaît pas, pour ce motif, l'article 13 de la DDHC relatif à l'égalité devant les charges publiques.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
