<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043289898</ID>
<ANCIEN_ID>JG_L_2021_03_000000431132</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/28/98/CETATEXT000043289898.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 24/03/2021, 431132</TITRE>
<DATE_DEC>2021-03-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431132</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:431132.20210324</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... C... a demandé à la commission du contentieux du stationnement payant d'annuler l'avis de paiement du forfait de post-stationnement d'un montant de 17 euros mis à la charge de la société Groupama Supports et Services le 16 janvier 2018 par la ville de Marseille. Par une décision n° 1800207 du 6 février 2019, la commission du contentieux du stationnement payant a fait droit à sa demande.<br/>
<br/>
              Par un pourvoi et un mémoire complémentaire, enregistrés les 27 mai et 18 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, la commune de Marseille demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M. C... ;<br/>
<br/>
              3°) de mettre à la charge de M. C... une somme de 2 400 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 2016-1321 du 7 octobre 2016 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme A... D..., rapporteure publique.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à Me Haas, avocat de la ville de Marseille ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que M. C... a demandé à la commission du contentieux du stationnement payant d'annuler l'avis de paiement d'un forfait de post-stationnement de 17 euros, mis à la charge de son employeur mais dont il avait acquitté le montant, à raison du stationnement de son véhicule de fonction, le 16 janvier 2018, à Marseille. Par une décision du 6 février 2019, la commission du contentieux du stationnement payant a fait droit à sa demande. La commune de Marseille se pourvoit en cassation contre cette décision.<br/>
<br/>
              2. Aux termes de l'article L. 241-3-2 du code de l'action sociale et des familles, en vigueur jusqu'au 31 décembre 2016 : " Toute personne (...) atteinte d'un handicap qui réduit de manière importante et durable sa capacité et son autonomie de déplacement à pied ou qui impose qu'elle soit accompagnée par une tierce personne dans ses déplacements, peut recevoir une carte de stationnement pour personnes handicapées (...) / La carte de stationnement pour personnes handicapées permet à son titulaire ou à la tierce personne l'accompagnant d'utiliser, à titre gratuit et sans limitation de la durée de stationnement, toutes les places de stationnement ouvertes au public. Toutefois, les autorités compétentes en matière de circulation et de stationnement peuvent fixer une durée maximale de stationnement qui ne peut être inférieure à douze heures. (...) / Les mêmes autorités peuvent également prévoir que, pour les parcs de stationnement disposant de bornes d'entrée et de sortie accessibles aux personnes handicapées depuis leur véhicule, les titulaires de cette carte sont soumis au paiement de la redevance de stationnement en vigueur ". Aux termes du IX de l'article 107 de la loi du 7 octobre 2016 pour une République numérique : " Les cartes d'invalidité, de priorité et de stationnement délivrées en application des articles L. 241-3 à L. 241-3-2 du code de l'action sociale et des familles, dans leur rédaction antérieure à la présente loi, demeurent valables jusqu'à leur date d'expiration et, au plus tard, jusqu'au 31 décembre 2026. Les titulaires de ces cartes peuvent demander une carte " mobilité inclusion " avant cette date. Cette carte se substitue aux cartes délivrées antérieurement ". Enfin, aux termes du I de l'article L. 241-3 du même code, dans sa rédaction issue de cette même loi : " La carte " mobilité inclusion " destinée aux personnes physiques (...) peut porter une ou plusieurs des mentions prévues aux 1° à 3° du présent I, à titre définitif ou pour une durée déterminée. (...) / 3° La mention " stationnement pour personnes handicapées " est attribuée à toute personne atteinte d'un handicap qui réduit de manière importante et durable sa capacité et son autonomie de déplacement à pied ou qui impose qu'elle soit accompagnée par une tierce personne dans ses déplacements. (...) / La mention " stationnement pour personnes handicapées " permet à son titulaire ou à la tierce personne l'accompagnant d'utiliser, à titre gratuit et sans limitation de la durée de stationnement, toutes les places de stationnement ouvertes au public. Toutefois, les autorités compétentes en matière de circulation et de stationnement peuvent fixer une durée maximale de stationnement qui ne peut être inférieure à douze heures (...)./ Les mêmes autorités peuvent également prévoir que, pour les parcs de stationnement disposant de bornes d'entrée et de sortie accessibles aux personnes handicapées depuis leur véhicule, les titulaires de cette mention sont soumis au paiement de la redevance de stationnement en vigueur ".<br/>
<br/>
              3. Il résulte des dispositions citées au point précédent que les personnes qui sont titulaires, soit de la carte de stationnement pour personnes handicapées, soit de la carte mobilité inclusion avec mention " stationnement pour personnes handicapées " qui s'y est substituée, bénéficient, pour eux-mêmes ou la tierce personne qui les accompagne, du stationnement à titre gratuit et sans limitation de durée sur les places de stationnement ouvertes au public, sauf si l'autorité locale compétente en matière de circulation et de stationnement impose une durée maximale de stationnement gratuit, laquelle ne peut être inférieure à douze heures, ou supprime cette gratuité dans les parcs de stationnement disposant de bornes d'entrée et de sortie accessibles aux personnes handicapées. Dans le cas où l'autorité compétente a fixé une durée maximale de stationnement gratuit et aux fins d'assurer le respect de cette réglementation, cette même autorité peut imposer aux personnes qui sont titulaires de la carte de stationnement pour personnes handicapées ou de la carte mobilité inclusion avec mention " stationnement pour personnes handicapées ", ou aux tierces personnes les accompagnant, d'établir l'heure du début de leur stationnement par un dispositif mis à leur disposition, dont la mise en place doit être prévue par voie réglementaire. A cette fin, elle peut notamment leur imposer l'apposition, derrière le pare-brise du véhicule utilisé pour le déplacement de la personne handicapée, d'une vignette de stationnement délivrée à titre gratuit, ou l'enregistrement, à titre gratuit, du numéro de la plaque d'immatriculation sur un horodateur ou sur une application mobile de paiement de la redevance de stationnement.<br/>
<br/>
              4. Il résulte des termes mêmes de la décision attaquée que, pour décharger M. C... du paiement du forfait de post-stationnement litigieux, la commission du contentieux du stationnement payant a jugé que la circonstance que son fils était titulaire d'une carte de stationnement pour personnes handicapées et que le véhicule était utilisé pour les besoins de ce dernier dispensait M. C... de l'obligation d'enregistrer son stationnement par horodateur ou système dématérialisé. Il résulte de ce qui a été dit ci-dessus qu'en statuant ainsi, alors qu'il ressort des pièces du dossier qui lui était soumis que la commune de Marseille avait fixé une durée maximale de stationnement gratuit de vingt-quatre heures pour les personnes titulaires d'une carte de stationnement pour personnes handicapées ou pour les tierces personnes les accompagnant et imposé à celles-ci de déclarer le début de leur stationnement par horodateur ou système dématérialisé, la commission a commis une erreur de droit.<br/>
<br/>
              5. La commune de Marseille est, par suite, fondée à demander l'annulation de la décision qu'elle attaque. Il n'y a en revanche pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. C... la somme qu'elle demande au titre de l'article L. 761-1.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La décision de la commission du contentieux du stationnement payant du 6 février 2019 est annulée.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la commission du contentieux du stationnement payant.<br/>
<br/>
Article 3 : Le surplus des conclusions du pourvoi, présenté au titre de l'article L. 761-1 du code de justice administrative, est rejeté.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la commune de Marseille et à M. B... C.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-04 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. AIDE SOCIALE AUX PERSONNES HANDICAPÉES. - CARTE DE STATIONNEMENT POUR PERSONNES HANDICAPÉES OU CARTE &#147;MOBILITÉ INCLUSION&#148; PORTANT CETTE MÊME MENTION - 1) A) PRINCIPE - GRATUITÉ DU STATIONNEMENT POUR LE TITULAIRE OU LA PERSONNE L'ACCOMPAGNANT - B) EXCEPTION - RÉGLEMENTATION LOCALE SPÉCIALE - 2) AUTORITÉ LOCALE AYANT FIXÉ UNE DURÉE MAXIMALE DE STATIONNEMENT GRATUIT - A) POSSIBILITÉ D'IMPOSER L'INDICATION DE L'HEURE DU DÉBUT DU STATIONNEMENT - CONDITIONS - PRÉVISION ET MISE À DISPOSITION DU DISPOSITIF ADÉQUAT - B) MODALITÉS D'ÉTABLISSEMENT DE CETTE HEURE DE DÉBUT DE STATIONNEMENT - INCLUSION - APPOSITION D'UNE VIGNETTE GRATUITE DERRIÈRE LE PARE-BRISE [RJ1] - ENREGISTREMENT DU NUMÉRO MINÉRALOGIQUE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">135-02-03-02-04-02 COLLECTIVITÉS TERRITORIALES. COMMUNE. ATTRIBUTIONS. POLICE. POLICE DE LA CIRCULATION ET DU STATIONNEMENT. RÉGLEMENTATION DU STATIONNEMENT. - CARTE DE STATIONNEMENT POUR PERSONNES HANDICAPÉES OU CARTE &#147;MOBILITÉ INCLUSION&#148; PORTANT CETTE MÊME MENTION - 1) A) PRINCIPE - GRATUITÉ DU STATIONNEMENT POUR LE TITULAIRE OU LA PERSONNE L'ACCOMPAGNANT - B) EXCEPTION - RÉGLEMENTATION LOCALE SPÉCIALE - 2) AUTORITÉ LOCALE AYANT FIXÉ UNE DURÉE MAXIMALE DE STATIONNEMENT GRATUIT - A) POSSIBILITÉ D'IMPOSER L'INDICATION DE L'HEURE DU DÉBUT DU STATIONNEMENT - CONDITIONS - PRÉVISION ET MISE À DISPOSITION DU DISPOSITIF ADÉQUAT - B) MODALITÉS D'ÉTABLISSEMENT DE CETTE HEURE DE DÉBUT DE STATIONNEMENT - INCLUSION - APPOSITION D'UNE VIGNETTE GRATUITE DERRIÈRE LE PARE-BRISE [RJ1] - ENREGISTREMENT DU NUMÉRO MINÉRALOGIQUE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">49-04-01-02 POLICE. POLICE GÉNÉRALE. CIRCULATION ET STATIONNEMENT. RÉGLEMENTATION DU STATIONNEMENT. - CARTE DE STATIONNEMENT POUR PERSONNES HANDICAPÉES OU CARTE &#147;MOBILITÉ INCLUSION&#148; PORTANT CETTE MÊME MENTION - 1) A) PRINCIPE - GRATUITÉ DU STATIONNEMENT POUR LE TITULAIRE OU LA PERSONNE L'ACCOMPAGNANT - B) EXCEPTION - RÉGLEMENTATION LOCALE SPÉCIALE - 2) AUTORITÉ LOCALE AYANT FIXÉ UNE DURÉE MAXIMALE DE STATIONNEMENT GRATUIT - A) POSSIBILITÉ D'IMPOSER L'INDICATION DE L'HEURE DU DÉBUT DU STATIONNEMENT - CONDITIONS - PRÉVISION ET MISE À DISPOSITION DU DISPOSITIF ADÉQUAT - B) MODALITÉS D'ÉTABLISSEMENT DE CETTE HEURE DE DÉBUT DE STATIONNEMENT - INCLUSION - APPOSITION D'UNE VIGNETTE GRATUITE DERRIÈRE LE PARE-BRISE [RJ1] - ENREGISTREMENT DU NUMÉRO MINÉRALOGIQUE.
</SCT>
<ANA ID="9A"> 04-02-04 1) a) Il résulte, en ce qui concerne la carte de stationnement pour personnes handicapées, de l'article L. 241-3-2 du code de l'action sociale et des familles (CASF) dans sa rédaction antérieure à l'intervention de la loi n° 2016-1321 du 7 octobre 2016, et en ce qui concerne la carte mobilité inclusion avec mention stationnement pour personnes handicapées qui s'y est substituée, de l'article L. 241-3 du même code dans sa rédaction issue de la même loi, que les personnes qui en sont titulaires bénéficient, pour elles-mêmes ou la tierce personne qui les accompagne, du stationnement à titre gratuit et sans limitation de durée sur toutes les places de stationnement ouvertes au public.,,,b) Il en va ainsi sauf si l'autorité locale compétente en matière de circulation et de stationnement impose une durée maximale de stationnement gratuit, laquelle ne peut être inférieure à douze heures, ou supprime cette gratuité dans les parcs de stationnement disposant de bornes d'entrée et de sortie accessibles aux personnes handicapées.,,,2) a) Dans le cas où l'autorité compétente a fixé une durée maximale de stationnement gratuit et aux fins d'assurer le respect de cette réglementation, cette même autorité peut imposer aux personnes qui sont titulaires de la carte de stationnement pour personnes handicapées ou de la carte mobilité inclusion avec mention stationnement pour personnes handicapées, ou aux tierces personnes les accompagnant, d'établir l'heure du début de leur stationnement par un dispositif mis à leur disposition, dont la mise en place doit être prévue par voie réglementaire.,,,b) A cette fin, elle peut notamment leur imposer l'apposition, derrière le pare-brise du véhicule utilisé pour le déplacement de la personne handicapée, d'une vignette de stationnement délivrée à titre gratuit, ou l'enregistrement, à titre gratuit, du numéro de la plaque d'immatriculation sur un horodateur ou sur une application mobile de paiement de la redevance de stationnement.</ANA>
<ANA ID="9B"> 135-02-03-02-04-02 1) a) Il résulte, en ce qui concerne la carte de stationnement pour personnes handicapées, de l'article L. 241-3-2 du code de l'action sociale et des familles (CASF) dans sa rédaction antérieure à l'intervention de la loi n° 2016-1321 du 7 octobre 2016, et en ce qui concerne la carte mobilité inclusion avec mention stationnement pour personnes handicapées qui s'y est substituée, de l'article L. 241-3 du même code dans sa rédaction issue de la même loi, que les personnes qui en sont titulaires bénéficient, pour elles-mêmes ou la tierce personne qui les accompagne, du stationnement à titre gratuit et sans limitation de durée sur toutes les places de stationnement ouvertes au public.,,,b) Il en va ainsi sauf si l'autorité locale compétente en matière de circulation et de stationnement impose une durée maximale de stationnement gratuit, laquelle ne peut être inférieure à douze heures, ou supprime cette gratuité dans les parcs de stationnement disposant de bornes d'entrée et de sortie accessibles aux personnes handicapées.,,,2) a) Dans le cas où l'autorité compétente a fixé une durée maximale de stationnement gratuit et aux fins d'assurer le respect de cette réglementation, cette même autorité peut imposer aux personnes qui sont titulaires de la carte de stationnement pour personnes handicapées ou de la carte mobilité inclusion avec mention stationnement pour personnes handicapées, ou aux tierces personnes les accompagnant, d'établir l'heure du début de leur stationnement par un dispositif mis à leur disposition, dont la mise en place doit être prévue par voie réglementaire.,,,b) A cette fin, elle peut notamment leur imposer l'apposition, derrière le pare-brise du véhicule utilisé pour le déplacement de la personne handicapée, d'une vignette de stationnement délivrée à titre gratuit, ou l'enregistrement, à titre gratuit, du numéro de la plaque d'immatriculation sur un horodateur ou sur une application mobile de paiement de la redevance de stationnement.</ANA>
<ANA ID="9C"> 49-04-01-02 1) a) Il résulte, en ce qui concerne la carte de stationnement pour personnes handicapées, de l'article L. 241-3-2 du code de l'action sociale et des familles (CASF) dans sa rédaction antérieure à l'intervention de la loi n° 2016-1321 du 7 octobre 2016, et en ce qui concerne la carte mobilité inclusion avec mention stationnement pour personnes handicapées qui s'y est substituée, de l'article L. 241-3 du même code dans sa rédaction issue de la même loi, que les personnes qui en sont titulaires bénéficient, pour elles-mêmes ou la tierce personne qui les accompagne, du stationnement à titre gratuit et sans limitation de durée sur toutes les places de stationnement ouvertes au public.,,,b) Il en va ainsi sauf si l'autorité locale compétente en matière de circulation et de stationnement impose une durée maximale de stationnement gratuit, laquelle ne peut être inférieure à douze heures, ou supprime cette gratuité dans les parcs de stationnement disposant de bornes d'entrée et de sortie accessibles aux personnes handicapées.,,,2) a) Dans le cas où l'autorité compétente a fixé une durée maximale de stationnement gratuit et aux fins d'assurer le respect de cette réglementation, cette même autorité peut imposer aux personnes qui sont titulaires de la carte de stationnement pour personnes handicapées ou de la carte mobilité inclusion avec mention stationnement pour personnes handicapées, ou aux tierces personnes les accompagnant, d'établir l'heure du début de leur stationnement par un dispositif mis à leur disposition, dont la mise en place doit être prévue par voie réglementaire.,,,b) A cette fin, elle peut notamment leur imposer l'apposition, derrière le pare-brise du véhicule utilisé pour le déplacement de la personne handicapée, d'une vignette de stationnement délivrée à titre gratuit, ou l'enregistrement, à titre gratuit, du numéro de la plaque d'immatriculation sur un horodateur ou sur une application mobile de paiement de la redevance de stationnement.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., s'agissant de l'absence d'obligation d'apposition derrière le pare-brise de la carte de stationnement pour personnes handicapées afin de justifier du droit à la gratuité, CE, décision du même jour, Commune de Tours, n° 428742, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
