<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039434414</ID>
<ANCIEN_ID>JG_L_2019_11_000000435901</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/43/44/CETATEXT000039434414.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 28/11/2019, 435901, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-11-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>435901</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:435901.20191128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au juge des référés du tribunal de Nice, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner à l'Office français de l'immigration et de l'intégration (OFII) de rétablir immédiatement ses droits à l'allocation pour demandeur d'asile, sous astreinte de 100 euros par jour de retard. Par une ordonnance n° 1905086 du 29 octobre 2019, le juge des référés du tribunal de Nice a rejeté sa demande.<br/>
<br/>
              Par une requête, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 12, 20, 21 et 25 novembre 2019 au secrétariat du contentieux du Conseil d'Etat, M. B... A... demande au juge des référés du Conseil d'Etat, dans le dernier état de ses écritures, de l'admettre à titre provisoire au bénéfice de l'aide juridictionnelle et, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à ses conclusions de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'OFII la somme de 2 000 euros à lui verser au titre des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - le juge des référés du tribunal administratif de Nice a, à tort, considéré que la demande n'était pas justifiée par l'urgence, alors qu'il ne bénéficie d'aucune aide financière depuis avril 2018, est sans ressource, a un enfant et est atteint de l'hépatite B, et que cette situation risque de perdurer pendant encore plusieurs mois ;<br/>
              - le juge des référés du tribunal administratif de Nice a retenu qu'il était en fuite à la date du 28 février 2018, en se fondant sur les affirmations non établies de l'OFII et alors qu'une simple absence à une convocation ne constitue pas, à elle seule, une telle fuite ;<br/>
              - même en cas de fuite, il incombe à l'OFII lorsque, après ne pas avoir respecté une obligation de se présenter aux autorités, un demandeur d'asile se présente volontairement à ces autorités, de réexaminer sa décision de suspendre le bénéfice des conditions matérielles d'accueil de ce demandeur d'asile ;<br/>
              - le juge des référés du tribunal administratif de Nice a entaché son ordonnance d'une insuffisance de motivation en ne se prononçant par sur l'existence d'une atteinte grave et manifestement illégale au droit d'asile ;<br/>
              - une telle atteinte existe dès lors, d'une part, qu'il n'est pas établi qu'il aurait été en fuite, d'autre part, qu'il est en situation de vulnérabilité, compte tenu de son absence de ressources, de sa situation de famille et de sa maladie ;<br/>
              - le refus de rétablir, à son profit, le versement de l'allocation pour demandeur d'asile méconnaît la jurisprudence de la Cour de justice de l'Union européenne du 12 novembre 2019, par laquelle la Cour a jugé que le retrait total des conditions matérielles d'accueil d'un demandeur d'asile était inconciliable avec l'article 20 de la directive 2013/33/UE du 26 juin 2013 ;<br/>
              - contrairement à ce que soutient l'OFII en défense, il a effectivement présenté une demande tendant à ce que la décision de suspension du versement de l'allocation pour demandeur d'asile soit réexaminée ;<br/>
              - l'OFII a méconnu ses obligations en ne procédant pas à ce réexamen.<br/>
<br/>
              Par un mémoire en défense, enregistré le 20 novembre 2019, l'OFII conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas remplie et qu'il n'est porté aucune atteinte grave et manifestement illégale à une liberté fondamentale.<br/>
<br/>
              Par une intervention, enregistrée le 22 novembre 2019, l'association<br/>
La Cimade demande que le juge des référés du Conseil d'Etat fasse droit aux conclusions de la requête d'appel de M. A.... Elle soutient que le refus de l'OFII de rétablir les conditions matérielles d'accueil de M. A... méconnaît la directive du 26 juin 2013 et ne respecte pas les conditions posées par la jurisprudence de la Cour de justice de l'Union européenne.<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. A... et l'association La Cimade et, d'autre part, l'Office français de l'immigration et de l'intégration ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 22 novembre 2019 à 15 heures au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Molinié, avocat au Conseil d'Etat et à la Cour de cassation, avocat de<br/>
M. A... ;<br/>
              - le représentant de M. A... ;<br/>
              - la représentante de l'Office français de l'immigration et de l'intégration ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a reporté la clôture de l'instruction au 26 novembre 2019 à 18 heures ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive 2013/33/UE du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - l'arrêt C-233/18 du 12 novembre 2019 de la Cour de justice de l'Union européenne ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              Sur l'intervention de l'association La Cimade :<br/>
<br/>
              1. La Cimade justifie, eu égard à l'objet et à la nature du litige, d'un intérêt suffisant pour intervenir dans la présente instance au soutien des conclusions présentées par<br/>
M. A.... Son intervention est, par suite, recevable.<br/>
<br/>
              Sur le cadre juridique du litige :<br/>
<br/>
              2. La directive du Parlement européen et du Conseil du 26 juin 2013 établissant des normes pour l'accueil des personnes demandant la protection internationale vise à harmoniser les conditions matérielles d'accueil des demandeurs d'asile en leur garantissant un niveau de vie digne et des conditions de vie comparables dans l'ensemble des Etats membres de l'Union européenne. Aux termes, toutefois, de l'article 20 de cette directive : " 1. Les États membres peuvent limiter ou, dans des cas exceptionnels et dûment justifiés, retirer le bénéfice des conditions matérielles d'accueil lorsqu'un demandeur : a) abandonne le lieu de résidence fixé par l'autorité compétente sans en avoir informé ladite autorité ou, si une autorisation est nécessaire à cet effet, sans l'avoir obtenue ; ou b) ne respecte pas l'obligation de se présenter aux autorités, ne répond pas aux demandes d'information ou ne se rend pas aux entretiens personnels concernant la procédure d'asile dans un délai raisonnable fixé par le droit national (...) En ce qui concerne les cas visés aux points a) et b), lorsque le demandeur est retrouvé ou se présente volontairement aux autorités compétentes, une décision dûment motivée, fondée sur les raisons de sa disparition, est prise quant au rétablissement du bénéfice de certaines ou de l'ensemble des conditions matérielles d'accueil retirées ou réduites. (...) 5. Les décisions portant limitation ou retrait du bénéfice des conditions matérielles d'accueil ou les sanctions visées aux paragraphes 1, 2, 3 et 4 du présent article sont prises au cas par cas, objectivement et impartialement et sont motivées. Elles sont fondées sur la situation particulière de la personne concernée, en particulier dans le cas des personnes visées à l'article 21, compte tenu du principe de proportionnalité. Les États membres assurent en toutes circonstances l'accès aux soins médicaux conformément à l'article 19 et garantissent un niveau de vie digne à tous les demandeurs (...) ".<br/>
<br/>
              3. Aux termes de l'article L. 741-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Tout étranger présent sur le territoire français et souhaitant demander l'asile se présente en personne à l'autorité administrative compétente, qui enregistre sa demande et procède à la détermination de l'Etat responsable (...). / Lorsque l'enregistrement de sa demande d'asile a été effectué, l'étranger se voit remettre une attestation de demande d'asile (...) ". L'article L. 742-1 du même code prévoit que : " Lorsque l'autorité administrative estime que l'examen d'une demande d'asile relève de la compétence d'un autre Etat qu'elle entend requérir, l'étranger bénéficie du droit de se maintenir sur le territoire français jusqu'à la fin de la procédure de détermination de l'Etat responsable de l'examen de sa demande et, le cas échéant, jusqu'à son transfert effectif à destination de cet Etat. L'attestation délivrée en application de l'article L. 741-1 mentionne la procédure dont il fait l'objet. Elle est renouvelable durant la procédure de détermination de l'Etat responsable et, le cas échéant, jusqu'à son transfert effectif à destination de cet Etat ". L'article L. 744-1 du même code dispose que les conditions matérielles d'accueil du demandeur d'asile, au sens de la directive du 26 juin 2013, " sont proposées à chaque demandeur d'asile par l'Office français de l'immigration et de l'intégration après l'enregistrement de la demande d'asile (...). Les conditions matérielles d'accueil comprennent les prestations et l'allocation prévues au présent chapitre (...) ". L'article L. 744-9 de ce même code prévoit que " Le demandeur d'asile qui a accepté les conditions matérielles d'accueil proposées en application de l'article L. 744-1 bénéficie d'une allocation pour demandeur d'asile s'il satisfait à des conditions d'âge et de ressources. L'Office français de l'immigration et de l'intégration ordonne son versement dans l'attente de la décision définitive lui accordant ou lui refusant une protection au titre de l'asile ou jusqu'à son transfert effectif vers un autre Etat responsable de l'examen de sa demande d'asile (...) ".<br/>
<br/>
              4. Aux termes de l'article L. 744-8 du code de l'entrée et du séjour des étrangers et du droit d'asile dans sa rédaction résultant de la loi du 29 juillet 2015 relative à la réforme du droit d'asile : " Le bénéfice des conditions matérielles d'accueil peut être : /<br/>
1° Suspendu si, sans motif légitime, le demandeur d'asile a abandonné son lieu d'hébergement déterminé en application de l'article L. 744-7, n'a pas respecté l'obligation de se présenter aux autorités, n'a pas répondu aux demandes d'informations ou ne s'est pas rendu aux entretiens personnels concernant la procédure d'asile (...) ". Si les termes de cet article ont été modifiés par différentes dispositions du I de l'article 13 de la loi du 10 septembre 2018 pour une immigration maîtrisée, un droit d'asile effectif et une intégration réussie, il résulte du III de l'article 71 de cette loi que ces modifications, compte tenu de leur portée et du lien qui les unit, ne sont entrées en vigueur ensemble qu'à compter du 1er janvier 2019 et ne s'appliquent qu'aux décisions initiales, prises à compter de cette date, relatives au bénéfice des conditions matérielles d'accueil proposées et acceptées après l'enregistrement de la demande d'asile. Les décisions relatives à la suspension et au rétablissement de conditions matérielles d'accueil accordées avant le 1er janvier 2019 restent régies par les dispositions antérieures à la loi du 10 septembre 2018.<br/>
<br/>
              5. Il résulte des dispositions précédemment citées que les conditions matérielles d'accueil sont proposées au demandeur d'asile par l'Office français de l'immigration et de l'intégration après l'enregistrement de la demande d'asile auquel il est procédé en application de l'article L. 741-1 du code de l'entrée et du séjour des étrangers et du droit d'asile. Si, par la suite, les conditions matérielles proposées et acceptées initialement peuvent être modifiées, en fonction notamment de l'évolution de la situation du demandeur ou de son comportement, la circonstance que, postérieurement à l'enregistrement de sa demande, l'examen de celle-ci devienne de la compétence de la France n'emporte pas l'obligation pour l'Office de réexaminer, d'office et de plein droit, les conditions matérielles d'accueil qui avaient été proposées et acceptées initialement par le demandeur. Dans le cas où les conditions matérielles d'accueil ont été suspendues sur le fondement de l'article L. 744-8, dans sa rédaction issue de la loi du 29 juillet 2015, le demandeur peut, notamment dans l'hypothèse où la France est devenue responsable de l'examen de sa demande d'asile, en demander le rétablissement. Il appartient alors à l'Office français de l'immigration et de l'intégration, pour statuer sur une telle demande de rétablissement, d'apprécier la situation particulière du demandeur à la date de la demande de rétablissement au regard notamment de sa vulnérabilité, de ses besoins en matière d'accueil ainsi que, le cas échéant, des raisons pour lesquelles il n'a pas respecté les obligations auxquelles il avait consenti au moment de l'acceptation initiale des conditions matérielles d'accueil.<br/>
<br/>
              Sur l'office du juge des référés : <br/>
<br/>
              6. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ".<br/>
<br/>
              7. D'une part, les dispositions de l'article L. 521-2 du code de justice administrative, citées ci-dessus, confèrent au juge administratif des référés le pouvoir d'ordonner toute mesure dans le but de faire cesser une atteinte grave et manifestement illégale portée à une liberté fondamentale par une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public. Il résulte tant des termes de cet article que du but dans lequel la procédure qu'il instaure a été créée que doit exister un rapport direct entre l'illégalité relevée à l'encontre de l'autorité administrative et la gravité de ses effets au regard de l'exercice de la liberté fondamentale en cause.<br/>
<br/>
              8. D'autre part, si la privation du bénéfice des mesures prévues par la loi afin de garantir aux demandeurs d'asile des conditions matérielles d'accueil décentes, jusqu'à ce qu'il ait été statué sur leur demande, est susceptible de constituer une atteinte grave et manifestement illégale à la liberté fondamentale que constitue le droit d'asile, le caractère grave et manifestement illégal d'une telle atteinte s'apprécie en tenant compte des moyens dont dispose l'autorité administrative compétente et de la situation du demandeur. Ainsi, le juge des référés ne peut faire usage des pouvoirs qu'il tient de l'article L. 521-2 du code de justice administrative en adressant une injonction à l'administration que dans le cas où, d'une part, le comportement de celle-ci fait apparaître une méconnaissance manifeste des exigences qui découlent du droit d'asile et où, d'autre part, il résulte de ce comportement des conséquences graves pour le demandeur d'asile, compte tenu notamment de son âge, de son état de santé ou de sa situation familiale. Il incombe au juge des référés d'apprécier, dans chaque situation, les diligences accomplies par l'administration en tenant compte des moyens dont elle dispose ainsi que de l'âge, de l'état de santé et de la situation familiale de la personne intéressée.<br/>
<br/>
              Sur la demande de M. A... : <br/>
<br/>
              9. M. A..., ressortissant nigérian né le 25 septembre 1994, est entré en France en vue d'y demander l'asile. Le 1er décembre 2017, sa demande d'asile a été enregistrée par la préfecture des Alpes-Maritimes, et il a alors pu bénéficier des conditions matérielles d'accueil proposées le même jour par l'Office français de l'immigration et de l'intégration. Par une décision du 5 février 2018, le préfet des Alpes maritimes a décidé son transfert aux autorités italiennes, responsables de l'examen de sa demande d'asile, et lui a enjoint de se présenter aux services de la police de l'air et des frontières qui ont ensuite constaté, les 13 et 20 février, son défaut de présentation. Par un courrier en date du 17 mai 2018, l'OFII lui a notifié, en application des dispositions de l'article L. 744-8 du code de l'entrée et du séjour des étrangers et du droit d'asile citées au point 4, la suspension de son allocation pour demandeur d'asile, qui n'a pas été rétablie depuis lors. Au terme du délai de transfert, M. A... a fait valoir que la France était devenue responsable de l'examen de sa demande d'asile, qui a été requalifiée en procédure normale le 8 février 2019. Il relève appel de l'ordonnance du 29 octobre 2019 par laquelle le juge des référés du tribunal administratif de Nice, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté sa demande tendant à ce qu'il soit enjoint à l'OFII de rétablir ses droits à l'allocation pour demandeur d'asile.<br/>
<br/>
              10. Tout d'abord, il résulte de l'instruction que M. A... ne fournit aucun motif valable pour s'être soustrait aux convocations mentionnées ci-dessus au point 9 et qu'au contraire, dans un courrier du 25 février 2019 adressé à l'OFII, il a admis n'avoir pas déféré à celles-ci par crainte d'être ensuite remis aux autorités italiennes pour l'examen de sa demande d'asile.<br/>
<br/>
              11. Ensuite, si M. A... fait valoir qu'il souffre d'une affection nécessitant des soins médicaux, il se borne à fournir un document qui, certes, établit l'existence de cette affection, mais dont il ressort aussi que celle-ci ne fait l'objet, à ce stade, que d'un suivi médical tous les six mois. Par ailleurs, il résulte de l'instruction que la mère de son enfant, qui bénéficie du statut de réfugié, est éligible de ce fait à l'ensemble des prestations sociales et perçoit, à ce titre, le revenu de solidarité active, l'allocation de logement familiale, l'allocation de soutien familial et la prestation d'accueil jeune enfant, lui permettant, ainsi, de subvenir aux besoins de son enfant sans contribution de M. A....<br/>
<br/>
              12. Enfin, ce dernier ne peut utilement se prévaloir de l'arrêt C-233/18 du<br/>
12 novembre 2019 de la Cour de justice de l'Union européenne, qui statue sur le régime des sanctions prévues au paragraphe 4 de l'article 20 de la directive du 26 juin 2013 susvisée et non sur celui des décisions de retrait du bénéfice des conditions matérielles d'accueil mentionnées au paragraphe 1 du même article.<br/>
<br/>
              13. En conséquence de ce qui est dit aux points 10 à 12 ci-dessus, l'absence de rétablissement de l'allocation pour demandeur d'asile de M. A..., qui ne peut être regardé comme en situation de particulière vulnérabilité, ne porte pas une atteinte grave et manifestement illégale au droit d'asile à laquelle il appartiendrait au juge des référés, statuant sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative, de mettre fin.<br/>
<br/>
              14. Il résulte de ce qui précède que M. A... n'est pas fondé à demander l'annulation de l'ordonnance du juge des référés du tribunal administratif de Nice. En conséquence, ses conclusions, y compris celles tendant à l'application de l'article L. 761-1 du code de justice administrative, ne peuvent, sans qu'il soit besoin de statuer sur sa demande d'admission à titre provisoire au bénéfice de l'aide juridictionnelle, qu'être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'intervention de l'association La Cimade est admise. <br/>
Article 2 : La requête de M. A... est rejetée.<br/>
Article 3 : La présente ordonnance sera notifiée à M. B... A... et à l'Office français de l'immigration et de l'intégration.<br/>
Copie en sera adressée à l'association La Cimade.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
