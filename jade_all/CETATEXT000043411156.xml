<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043411156</ID>
<ANCIEN_ID>JG_L_2021_04_000000439606</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/41/11/CETATEXT000043411156.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 19/04/2021, 439606, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-04-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439606</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Sébastien Ferrari</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:439606.20210419</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Paris de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre de l'année 2005. Par un jugement n° 1602057 du 17 avril 2018, ce tribunal a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 18PA02006 du 21 novembre 2019, la cour administrative d'appel de Paris a rejeté l'appel formé par M. B... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux nouveaux mémoires, enregistrés les 16 mars 2020, 20 août 2020, 28 décembre 2020 et 2 mars 2021, au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention conclue le 9 septembre 1966 entre la France et la Suisse en vue d'éliminer les doubles impositions en matière d'impôt sur le revenu et sur la fortune ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sébastien Ferrari, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Piwnica, Molinie, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. et Mme B... ont quitté la France pour s'installer en Suisse en juin 2005 avant de céder leurs titres de participation dans la société Adonix le 15 novembre 2005 pour un montant de 63 544 044 euros. A la suite d'un examen contradictoire de leur situation fiscale personnelle, l'administration fiscale a estimé qu'ils avaient conservé le statut de résidents fiscaux français au titre de l'année 2005 et ne pouvaient être regardés comme des résidents fiscaux de Suisse. L'administration fiscale les a assujettis à l'impôt sur le revenu et aux contributions sociales au titre de l'année 2005 à raison des gains retirés de cette cession de titres. Après avoir vainement contesté ces impositions supplémentaires et les pénalités correspondantes, M. et Mme B... ont porté le litige devant le tribunal administratif de Paris qui, par un jugement du 17 avril 2018, a rejeté leur demande de décharge. M. et Mme B... se pourvoient en cassation contre l'arrêt du 21 novembre 2019 par lequel la cour administrative d'appel de Paris a rejeté leur appel contre ce jugement.<br/>
<br/>
              2. Aux termes de l'article 4 de la convention fiscale franco-suisse du 9 septembre 1966 : " 1. Au sens de la présente Convention, l'expression "résident d'un Etat contractant" désigne toute personne qui, en vertu de la législation dudit Etat, est assujettie à l'impôt dans cet Etat en raison de son domicile, de sa résidence, de son siège de direction ou de tout autre critère de nature analogue. / (...) 6. N'est pas considérée comme résident d'un Etat contractant au sens du présent article : / (...) b) Une personne physique qui n'est imposable dans cet Etat que sur une base forfaitaire déterminée d'après la valeur locative de la ou des résidences qu'elle possède sur le territoire de cet Etat ". Aux termes de l'article 31 de la même convention : " (...) 2. Pour obtenir dans un Etat contractant les avantages prévus par la présente convention, les résidents de l'autre Etat contractant doivent, à moins que les autorités compétentes en disposent autrement, présenter un formulaire d'attestation de résidence indiquant en particulier la nature ainsi que le montant ou la valeur des revenus ou de la fortune concernés, et comportant la certification des services fiscaux de cet autre Etat ". <br/>
<br/>
              3. En application des stipulations du 3 de l'article 27 de la convention, les autorités françaises et suisses ont convenu d'une interprétation commune des stipulations du b du 6 de l'article 4 de la convention. L'administration fiscale française a publié à la documentation administrative de base référencée 14 B-2211 n° 7, mise à jour le 10 décembre 1972, cette interprétation, qui prévoit qu'une personne physique assujettie à l'impôt à forfait en Suisse peut avoir la qualité de résident dans ce pays au sens de l'article 4 de la convention si la base d'imposition fédérale, cantonale et communale est supérieure à cinq fois la valeur locative de l'habitation du contribuable ou à une fois et demie le prix de pension qu'il paie et si la base d'imposition cantonale et communale ne s'écarte pas notablement de celle qui est déterminante pour l'impôt fédéral direct, cette base cantonale et communale devant, en tout état de cause, être égale ou supérieure aux éléments du revenu du contribuable qui proviennent de Suisse et de France, pour les revenus de source française, lorsqu'ils sont privilégiés par la convention, notamment les dividendes, intérêts et redevances de licences.<br/>
<br/>
              4. Pour refuser en l'espèce aux contribuables la possibilité de se prévaloir de cette interprétation administrative, la cour administrative d'appel s'est fondée sur ce que l' attestation produite par les époux B..., établie le 10 mai 2007 par l'administration fiscale cantonale des impôts de Genève, qui indique qu'ils sont assujettis de manière illimitée, tant pour les revenus que pour la fortune, aux impôts fédéraux, cantonaux, et communaux depuis le 1er juin 2005, qu'ils ne sont pas assujettis sur une base forfaitaire déterminée par la valeur locative de leur logement, et qu'ils ont par conséquent la qualité de résident fiscal suisse au sens des stipulations de l'article 4 de la convention fiscale franco-suisse, ne répond pas aux prescriptions de l'article 31 de la convention fiscale franco-suisse à défaut de préciser la nature ainsi que le montant ou la valeur des revenus ou de la fortune concernés. <br/>
<br/>
              5. Toutefois, la présentation du formulaire d'attestation de résidence prévu à l'article 31 de la convention, dont il résulte de la lettre même de ces stipulations qu'elle n'est requise que pour l'obtention des " avantages " prévus par la convention, à l'exclusion de la détermination du pays de résidence du contribuable qui ne constitue pas un tel avantage, n'est pas davantage évoquée par la doctrine administrative précédemment mentionnée. Par suite, en jugeant que M. et Mme B... devaient produire un formulaire d'attestation de résidence dans les formes prévues au second paragraphe de l'article 31 de la convention fiscale franco-suisse pour pouvoir bénéficier de l'interprétation favorable donnée du b du 6 de son article 4 par la doctrine administrative, la cour administrative d'appel de Paris a entaché son arrêt d'une erreur de droit. <br/>
<br/>
              6. Il résulte de ce qui précède que M. et Mme B... sont fondés à demander l'annulation de l'arrêt qu'ils attaquent, sans qu'il soit besoin de se prononcer sur les autres moyens de leur pourvoi.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 21 novembre 2019 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris. <br/>
Article 3 : L'Etat versera à M. et Mme B... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. et Mme A... B... et au ministre de l'économie, des finances et de la relance.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
