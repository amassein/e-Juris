<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028717844</ID>
<ANCIEN_ID>JG_L_2014_03_000000354174</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/71/78/CETATEXT000028717844.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 12/03/2014, 354174, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-03-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354174</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. Olivier Japiot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:354174.20140312</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 21 novembre 2011 et 17 février 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B...A..., demeurant... ; M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09PA04296 du 21 septembre 2011 par lequel la cour administrative d'appel de Paris, sur recours du ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat a, d'une part, annulé le jugement n° 0512883/2 du 31 mars 2009 du tribunal administratif de Paris et, d'autre part, remis à sa charge les cotisations supplémentaires d'impôt sur le revenu et de contributions sociales, ainsi que les pénalités correspondantes, auxquelles il a été assujetti au titre de l'année 2000 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Japiot, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Barthélemy, Matuchansky, Vexliard, avocat de M. A...;<br/>
<br/>
<br/>
<br/>1. Considérant, en premier lieu, qu'il ressort des pièces du dossier transmis par la cour administrative d'appel de Paris qu'après avoir, par une première ordonnance du 13 septembre 2010, fixé la clôture de l'instruction au 13 octobre 2010, le président de la deuxième chambre de cette cour, a, par deux ordonnances du 13 octobre 2010, rouvert l'instruction pour permettre au requérant de répondre au mémoire en réplique produit par le ministre du budget le 7 octobre 2010, puis fixé la nouvelle date de clôture de l'instruction au 15 novembre 2010 ; que le nouveau mémoire présenté pour M. A...en réponse à la communication du mémoire en réplique a été enregistré le 8 mars 2011 au greffe de la cour administrative d'appel ; que la cour a, dans l'arrêt attaqué, visé ce mémoire comme enregistré après la clôture de l'instruction, sans l'analyser ; que si le requérant soutient à l'appui de son pourvoi en cassation que l'arrêt attaqué est ainsi intervenu au terme d'une procédure irrégulière, il ne soutient ni que son mémoire contenait l'exposé d'une circonstance de fait dont il n'aurait pas été en mesure de faire état avant la clôture de l'instruction et que le juge n'aurait pu ignorer sans fonder sa décision sur des faits matériellement inexacts ni qu'il contenait un moyen de droit nouveau ou que le juge aurait dû relever d'office ; qu'il suit de là que le moyen tiré de ce que la cour a entaché son arrêt d'un vice de procédure en visant le mémoire enregistré le 8 mars 2011 sans l'analyser ni le communiquer au ministre ne peut qu'être écarté ;<br/>
<br/>
              2. Considérant, en second lieu, d'une part, qu'aux termes de l'article L. 64 du livre des procédures fiscales, dans sa rédaction applicable à l'année d'imposition en litige : " Ne peuvent être opposés à l'administration des impôts les actes qui dissimulent la portée véritable d'un contrat ou d'une convention à l'aide de clauses : (...) qui déguisent soit une réalisation, soit un transfert de bénéfices ou de revenus (...). / L'administration est en droit de restituer son véritable caractère à l'opération litigieuse. En cas de désaccord sur les redressements notifiés sur le fondement du présent article, le litige est soumis, à la demande du contribuable, à l'avis du comité consultatif pour la répression des abus de droit. L'administration peut également soumettre le litige à l'avis du comité dont les avis rendus feront l'objet d'un rapport annuel. / Si l'administration ne s'est pas conformée à l'avis du comité, elle doit apporter la preuve du bien-fondé du redressement " ; qu'il résulte de ces dispositions que, lorsque l'administration use des pouvoirs que lui confère ce texte dans des conditions telles que la charge de la preuve lui incombe, elle est fondée à écarter comme ne lui étant pas opposables certains actes passés par le contribuable dès lors qu'elle établit que ces actes ont un caractère fictif ou bien, à défaut, recherchent le bénéfice d'une application littérale des textes à l'encontre des objectifs poursuivis par leurs auteurs et n'ont pu être inspirés par aucun autre motif que celui d'éluder ou d'atténuer les charges fiscales que l'intéressé, s'il n'avait pas passé ces actes, aurait normalement supportées eu égard à sa situation et à ses activités réelles ; <br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes du premier alinéa de l'article 150 A bis du code général des impôts, dans sa rédaction alors applicable : " Les gains nets retirés de cessions à titre onéreux de valeurs mobilières ou de droits sociaux de sociétés non cotées dont l'actif est principalement constitué d'immeubles ou de droits portant sur ces biens relèvent exclusivement du régime d'imposition prévu pour les biens immeubles. Pour l'application de cette disposition, ne sont pas pris en considération les immeubles affectés par la société à sa propre exploitation industrielle, commerciale, agricole ou à l'exercice d'une profession non commerciale " ; que ces dispositions ont pour objectif de permettre que les plus-values réalisées à l'occasion de la cession d'un immeuble détenu par une société à prépondérance immobilière soient imposées dans les mêmes conditions, que le cessionnaire acquière les titres de la société ou l'immeuble lui-même ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société anonyme Studio des Plantes (SDP), fondée en 1977, a cédé son fonds de commerce en 1996 et cessé toute activité en 1999 ; que les associés de cette société, dont M.A..., voulant prendre leur retraite, ont signé en 1999 avec la société Seurlin Immobilier, spécialisée dans le désinvestissement, une promesse de vente de leurs actions, sous la condition suspensive que les cédants trouveraient un acquéreur pour le bien immobilier détenu par la société SDP rue du Château d'Eau à Paris, qui constituait l'unique actif immobilisé de la société ; que, les cédants ayant trouvé un acquéreur pour ce bien, la cession des titres est intervenue le 24 février 2000, pour un montant notablement inférieur à la valeur nette des actifs sans, pour autant, que les passifs ne présentent un quelconque risque ; que, le lendemain, la société SDP a cédé l'immeuble ; qu'elle n'a repris aucune activité économique mais a été absorbée par la société Fracoflex, le 15 novembre 2000, cette dernière étant elle-même absorbée, le 15 décembre 2000, par une autre société, laquelle a fait l'objet d'une liquidation amiable le 30 décembre 2001 ; que la plus-value résultant de la cession par M. A... des titres de la société SDP a été placée par lui, en application des dispositions alors en vigueur de l'article 150 A bis du code général des impôts, sous le régime des plus-values de cessions à titre onéreux de titres de sociétés à prépondérance immobilière ; qu'en application des dispositions de l'article 150 M du même code, dans leur rédaction alors applicable, la plus-value ainsi réalisée a bénéficié d'une réduction de 5 % pour chaque année de détention des titres au-delà de la deuxième, de telle sorte que le montant imposable a été réduit à zéro ; <br/>
<br/>
              5. Considérant que l'administration, faisant application des dispositions précitées de l'article L. 64 du livre des procédures fiscales, a estimé que l'acte de cession des titres à la société Seurlin Immobilier avait eu pour seul objet, par une application littérale des dispositions des articles 150 A bis et 150 M du code général des impôts, de faire échapper la plus-value réalisée lors de la cession de l'immeuble à toute imposition, alors qu'en l'absence d'un tel montage, le produit de la vente de l'immeuble par la société SDP aurait été, lors de la liquidation de cette société, appréhendé par M. A...et son associée en tant que boni de liquidation et soumis, sous déduction du montant de leurs apports, au barème progressif de l'impôt sur le revenu en application des dispositions alors en vigueur de l'article 161 du code général des impôts ; <br/>
<br/>
              6. Considérant que, par l'arrêt attaqué, la cour administrative d'appel de Paris a jugé que l'administration établissait que la cession des titres de la société SDP à la société Seurlin Immobilier avait eu pour seul objet, par la recherche du bénéfice d'une application littérale des textes relatifs aux plus-values de cession des sociétés à prépondérance immobilière, d'éluder les charges fiscales que le requérant, s'il n'avait pas passé cet acte, aurait normalement supportées en cas de dissolution de la société ; que, contrairement à ce que soutient M. A..., la cour n'a pas omis de rechercher si l'acte en cause était inspiré par la volonté de bénéficier d'une application littérale des textes relatifs aux plus-values de cession des sociétés à prépondérance immobilière à l'encontre des objectifs poursuivis par leurs auteurs, dès lors qu'elle a relevé que l'unique immeuble détenu par la société SDP avait fait l'objet d'une promesse de vente peu de temps avant la cession des titres de cette société et avait été vendu dès le lendemain de cette cession ; qu'elle n'a entaché son arrêt sur ce point ni d'une erreur de droit, ni d'une insuffisance de motivation ; <br/>
<br/>
              7. Considérant qu'alors même que les cessions de titres représentent une modalité habituelle de transmission des sociétés, la cour a pu, sans commettre d'erreur sur la qualification juridique des faits, estimer qu'en l'espèce, la cession des titres de la société SDP, opérée dans le cadre du montage décrit au point 4 et qui n'avait pas pour objet la cession d'un immeuble, avait un but exclusivement fiscal et qu'elle était, dès lors, constitutive d'un abus de droit ; <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que M. A...n'est pas fondé à demander l'annulation de l'arrêt du 21 septembre 2011 de la cour administrative d'appel de Paris ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
 Article 1er : Le pourvoi de M. A...est rejeté.<br/>
<br/>
 Article 2 : La présente décision sera notifiée à M. B...A...et au ministre de l'économie et des finances.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
