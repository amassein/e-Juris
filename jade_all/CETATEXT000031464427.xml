<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031464427</ID>
<ANCIEN_ID>JG_L_2015_11_000000371423</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/46/44/CETATEXT000031464427.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème SSJS, 09/11/2015, 371423, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-11-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>371423</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Mireille Le Corre</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:371423.20151109</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés les 19 août 2013 et 21 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 31 mai 2013 par laquelle la commission d'avancement a émis un avis défavorable à son intégration directe aux fonctions judiciaires du premier grade au titre de l'article 23 de l'ordonnance du 22 décembre 1958, à l'issue d'un stage probatoire de six mois ;<br/>
<br/>
              2°) d'enjoindre à la commission d'avancement de réexaminer son dossier dans un délai d'un mois à compter de la décision à intervenir ; <br/>
<br/>
              3°) de décider que son arrêt sera publié de façon anonyme sur les sites internet et les bases de données jurisprudentielles ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - l'ordonnance n° 58-1270 du 22 décembre 1958 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Mireille Le Corre, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'en application de l'article 23 de l'ordonnance du 22 décembre 1958 portant loi organique relative au statut de la magistrature, les personnes justifiant de dix-sept années au moins d'exercice professionnel les qualifiant particulièrement pour exercer des fonctions judiciaires peuvent être nommées directement aux fonctions du premier grade de la hiérarchie judiciaire ; qu'aux termes de l'article 25-3 de la même ordonnance : " Les candidats à une intégration au titre des articles 22 et 23 suivent, s'ils sont admis par la commission prévue à l'article 34, une formation probatoire organisée par l'Ecole nationale de la magistrature comportant un stage en juridiction effectué selon les modalités prévues à l'article 19. / (...) Le directeur de l'Ecole nationale de la magistrature établit, sous la forme d'un rapport, le bilan de la formation probatoire de chaque candidat qu'il adresse au jury prévu à l'article 21. / Après un entretien avec le candidat, le jury se prononce sur son aptitude à exercer des fonctions judiciaires et transmet son avis à la commission prévue à l'article 34. Toute décision de la commission d'avancement défavorable à l'intégration d'un candidat admis à la formation probatoire visée au premier alinéa est motivée. (...) " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que Mme B...a sollicité son intégration directe au premier grade de la hiérarchie judiciaire ; qu'à l'issue du stage probatoire, accompli au sein du tribunal d'instance, du tribunal de grande instance et de la cour d'appel de Poitiers, la commission d'avancement a, après que le jury d'aptitude a émis un avis défavorable à sa candidature, rejeté la demande d'intégration de l'intéressée ;<br/>
<br/>
              Sur les moyens tirés de l'irrégularité de la procédure préalable à l'avis de la commission d'avancement : <br/>
<br/>
              3. Considérant, en premier lieu, que la requérante ne peut utilement se prévaloir, pour contester la régularité des conditions de déroulement de son stage, de la méconnaissance des dispositions de la circulaire du 23 février 2012 de l'Ecole nationale de la magistrature relative au stage probatoire des candidats à l'intégration directe, dès lors que ce document émanant de la direction des stages, qui se borne à formuler des recommandations aux responsables de stage, est dépourvu de caractère impératif ; que si la requérante soutient qu'elle a été confrontée à diverses difficultés imputables à l'administration au cours de son stage, notamment une affectation en juridiction éloignée de son domicile, un effectif de stagiaires excessif, un encadrement ainsi que des moyens informatiques insuffisants, un recours inadapté au référentiel de formation des auditeurs de justice et une organisation de son stage manquant de cohérence, il ne ressort pas des pièces du dossier que l'intéressée n'aurait pas été en mesure de réaliser sa période probatoire dans des conditions lui permettant de faire la preuve de son aptitude aux fonctions de magistrat du premier grade ; que, contrairement à ce que soutient la requérante, il ne ressort pas des pièces du dossier que certains directeurs de centre de stage ou maîtres de stage auraient adopté une attitude partiale ou discriminatoire à son encontre ; que les allégations de l'intéressée selon lesquelles elle aurait été victime d'un climat général d'hostilité ne sont pas étayées ; qu'il en va de même de ses allégations concernant des faits de harcèlement moral dont elle aurait été victime, qui ne sont corroborées par aucun élément de fait susceptible de faire présumer l'existence d'un tel harcèlement ; qu'aucun texte ni aucun principe ne fait obstacle à ce qu'un candidat à l'intégration directe aux fonctions judiciaires du premier grade soit encadré et évalué, lors de son stage probatoire, par un magistrat du second grade ; que, par suite, les divers moyens tirés de l'irrégularité des conditions de déroulement du stage probatoire de la requérante doivent être écartés ;<br/>
<br/>
              4. Considérant, en second lieu, qu'aucun texte ni aucun principe n'impose que les candidats à l'intégration directe à la magistrature soient préalablement informés des modalités de déroulement de l'entretien avec le jury d'aptitude ; qu'il ne ressort pas des pièces du dossier que la présidente du jury ou ses membres auraient fait preuve de partialité à l'égard de la requérante ; qu'aucun texte ni aucun principe n'impose que l'avis du jury d'aptitude soit motivé ; que, par suite, les divers moyens dirigés contre l'avis du jury d'aptitude, qui n'est pas, comme cela a été dit ci-dessus, intervenu au terme d'une procédure irrégulière, doivent être écartés ; <br/>
<br/>
              Sur la légalité interne de l'avis de la commission d'avancement : <br/>
<br/>
              5. Considérant qu'en décidant, au vu de l'ensemble des évaluations réalisées lors du stage de MmeA..., dont une majorité faisait état de sérieuses réserves ou d'insuffisances, et de l'avis très défavorable du jury d'aptitude, de déclarer l'intéressée inapte à l'exercice des fonctions judiciaires, la commission d'avancement, qui ne s'est pas fondée sur des faits matériellement inexacts, n'a pas entaché sa décision d'une erreur manifeste d'appréciation et n'a pas commis d'erreur de droit ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que les conclusions de la requête dirigées contre la décision de la commission d'avancement du 31 mai 2013, ainsi que les conclusions à fins d'injonction, doivent être rejetées ;<br/>
<br/>
              Sur les conclusions tendant à l'anonymisation de la version de la présente décision publiée sur les sites internet et les bases de données jurisprudentielles : <br/>
<br/>
              7. Considérant qu'aux termes de l'article L. 6 du code de justice administrative, " Les débats ont lieu en audience publique " et qu'aux termes de l'article L. 10 du même code, "  Les jugements sont publics. (...) " ; qu'il n'appartient pas au Conseil d'Etat, statuant au contentieux, de se prononcer sur les conditions dans lesquelles les décisions qu'il rend sont diffusées sur internet ou par tout autre moyen ; que, par suite, les conclusions de Mme A...tendant à l'anonymisation de la version de la présente décision publiée sur les sites internet et les bases de données jurisprudentielles ne peuvent qu'être rejetées ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante, la somme que demande Mme A...au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme A...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à MadameB..., à la garde des sceaux, ministre de la justice et à l'Ecole nationale de la magistrature.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
