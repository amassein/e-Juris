<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029170329</ID>
<ANCIEN_ID>JG_L_2014_06_000000366667</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/17/03/CETATEXT000029170329.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 30/06/2014, 366667, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-06-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366667</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Pascal Trouilly</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:366667.20140630</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              Le préfet de la Drôme a demandé au tribunal administratif de Grenoble d'annuler pour excès de pouvoir l'arrêté du 29 avril 2011 par lequel le maire de Clérieux a délivré à M. A...B...un permis de construire une maison d'habitation au lieu-dit " Les Clodits ". Par un jugement n° 1104424 du 3 mai 2012, le tribunal administratif a rejeté son déféré.<br/>
<br/>
              Par un arrêt n° 12LY01728 du 5 février 2013, la cour administrative d'appel de Lyon, à la demande du préfet de la Drôme, a annulé le jugement du tribunal administratif de Grenoble du 3 mai 2012 ainsi que l'arrêté du maire de Clérieux du 29 avril 2011.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 7 mars et 10 juin 2013 au secrétariat du contentieux du Conseil d'Etat, la commune de Clérieux demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt n° 12LY01728 de la cour administrative d'appel de Lyon du 5 février 2013 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code de l'urbanisme ;<br/>
              - le décret n° 2004-374 du 29 avril 2004 ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pascal Trouilly, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de la commune de Clérieux.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué, en tant qu'il se prononce sur la recevabilité du déféré du préfet :<br/>
<br/>
              1. Aux termes du premier alinéa du I de l'article 45 du décret du 29 avril 2004 relatif aux pouvoirs des préfets, à l'organisation et à l'action des services de l'Etat dans les régions et départements : " En cas d'absence ou d'empêchement du préfet, sans que ce dernier ait désigné par arrêté un des sous-préfets en fonction dans le département pour assurer sa suppléance, celle-ci est exercée de droit par le secrétaire général de la préfecture ".<br/>
<br/>
              2. Pour juger que le secrétaire général de la préfecture de la Drôme avait qualité pour introduire devant le tribunal administratif de Grenoble le déféré à l'encontre de l'arrêté du maire de Clérieux du 29 avril 2011 délivrant un permis de construire à M.B..., la cour administrative d'appel de Lyon a jugé qu'il résultait de ces dispositions qu'en l'absence du préfet, le secrétaire général exerçait de plein droit l'ensemble des pouvoirs dévolus à celui-ci, sans qu'il soit besoin qu'il dispose d'une délégation. En s'abstenant de procéder à une mesure d'instruction pour vérifier, sans que ce point soit en débat, si le préfet avait désigné par arrêté un des sous-préfets en fonction dans le département pour assurer sa suppléance, la cour n'a pas commis d'erreur de droit.<br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué, en tant qu'il se prononce sur la légalité du permis de construire délivré à M.B... :<br/>
<br/>
              3. En vertu de l'article NC 1 du règlement du plan d'occupation des sols de la commune de Clérieux (Drôme), qui limite les occupations et les utilisations du sol admises en zone NC, sont autorisées " les constructions et installations liées et nécessaires à l'activité agricole ; / les constructions à usage d'habitation sous réserve qu'elles soient directement liées et nécessaires aux exploitations agricoles et dans la limite d'une surface hors oeuvre nette de 250 mètres carrés ".<br/>
<br/>
              4. En premier lieu, il ressort des pièces du dossier soumis aux juges du fond que, par l'arrêté litigieux du 29 avril 2011, le maire de Clérieux a délivré à M.B..., exploitant agricole, le permis de construire qu'il avait sollicité pour la construction d'une maison individuelle sur un terrain situé en zone NC du plan d'occupation des sols. La cour administrative d'appel de Lyon n'a pas dénaturé les pièces du dossier en estimant que le permis de construire avait pour objet une maison d'habitation. Elle n'a, dès lors, ni insuffisamment motivé son arrêt ni commis d'erreur de droit en se bornant à examiner si la construction projetée relevait des " constructions à usage d'habitation (...) directement liées et nécessaires aux exploitations agricoles ", sans rechercher si elle pouvait être qualifiée, par elle-même, de construction ou d'installation liée et nécessaire à l'activité agricole au sens de l'alinéa précédent de l'article NC 1 du règlement du plan d'occupation des sols de la commune.<br/>
<br/>
              5. En deuxième lieu, il ressort des pièces du dossier soumis aux juges du fond que si M. B...et la commune ont mentionné, dans leurs écritures, l'accueil d'installations frigorifiques bruyantes en sous-sol, destinées au stockage de fruits, ils ne se sont prévalus ni de l'existence d'une exploitation fruitière que M. B...aurait entreprise ni de la nécessité d'une construction à usage d'habitation pour une telle exploitation. Par suite, la cour n'a pas insuffisamment motivé son arrêt, n'a pas dénaturé les pièces du dossier et n'a pas commis d'erreur de droit en s'abstenant de rechercher si l'activité de production fruitière de M. B...devait être regardée comme une exploitation agricole justifiant son logement à proximité.<br/>
<br/>
              6. En troisième lieu, la cour n'a pas commis d'erreur de droit en jugeant que ni M. B...ni la commune de Clérieux ne justifiaient des raisons pour lesquelles l'élevage de poules pondeuses de l'intéressé impliquerait une surveillance rapprochée et permanente, rendant nécessaire la présence à proximité de cette exploitation de la maison d'habitation projetée. Si la cour a également relevé que la distance d'au moins 500 mètres séparant le projet de construction et l'exploitation ne saurait autoriser une telle surveillance, à la supposer nécessaire, cette mention surabondante de son arrêt ne peut être utilement critiquée en cassation.<br/>
<br/>
              7. En dernier lieu, la cour n'a pas commis d'erreur de droit en jugeant que la circonstance que M. B...pourrait domicilier le siège de son exploitation dans sa  nouvelle habitation était sans incidence sur la question de savoir si le projet était directement lié et nécessaire à l'exploitation agricole, seule hypothèse dans laquelle la construction d'une maison d'habitation peut être autorisée en zone NC.<br/>
<br/>
              8. Il résulte de tout ce qui précède que la commune de Clérieux n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font, dès lors, obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas la partie perdante, la somme que la commune de Clérieux demande à ce titre.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la commune de Clérieux est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la commune de Clérieux et à la ministre du logement et de l'égalité des territoires.<br/>
Copie en sera adressée pour information à M. A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
