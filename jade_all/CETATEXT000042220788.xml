<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042220788</ID>
<ANCIEN_ID>JG_L_2020_08_000000442268</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/22/07/CETATEXT000042220788.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 06/08/2020, 442268, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-08-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>442268</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:442268.20200806</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme C... D..., M. B... D..., Mme F... E..., agissant en son nom propre et en celui de sa fille Nicoletta E..., et Mme H... A... ont demandé au juge des référés du tribunal administratif de Paris, sur le fondement de l'article L. 521-2 du code de justice administrative, en premier lieu, de suspendre l'exécution de la décision d'arrêt des traitements de M. G... D..., leur fils, compagnon, père et frère, en deuxième lieu, d'enjoindre à l'Assistance publique - Hôpitaux de Paris de poursuivre les traitements, les soins, la nutrition et l'hydratation de M. G... D... et, en troisième lieu, d'ordonner une expertise en vue de déterminer la situation médicale de celui-ci.<br/>
<br/>
              Par une ordonnance n° 2010943 du 27 juillet 2020, le juge des référés du tribunal administratif de Paris a, d'une part, dit n'y avoir pas lieu de statuer sur les conclusions de la demande tendant à la suspension de l'exécution de la décision contestée et, d'autre part, rejeté les conclusions tendant à ce qu'une expertise soit ordonnée.<br/>
<br/>
              Par une requête, enregistrée le 28 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, M. et Mme D..., Mme E..., agissant en son nom propre et en celui de sa fille Nicoletta, et Mme A... demandent au juge des référés du Conseil d'Etat :<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à leur demande de première instance ; <br/>
<br/>
              3°) de mettre à la charge de l'Assistance publique - Hôpitaux de Paris la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
              - le juge des référés a omis de statuer sur les conclusions de la demande à fin d'injonction de poursuite des traitements de M. D... ; <br/>
              - ce juge ne pouvait, au vu du dossier, estimer que les conclusions aux fins de suspension et d'injonction avaient perdu leur objet, ainsi que le confirme la nouvelle réunion collégiale prévue le 29 juillet 2020 ;<br/>
              - la condition d'urgence est remplie, eu égard, d'une part, à la possibilité d'une nouvelle décision d'arrêt des soins à l'issue de la réunion collégiale prévue le 29 juillet 2020 et, d'autre part, au risque de décès de M. D... en raison de la décision du 29 juin 2020 de limiter les thérapeutiques actives, conduisant à ne plus le soigner pour les graves troubles pulmonaires contractés durant son hospitalisation ;<br/>
              - la décision de fin de traitement de M. D..., qui ne leur a pas été notifiée, porte une atteinte grave et manifestement illégale au droit au respect de la vie et au droit du patient de consentir à un traitement médical ;<br/>
              - elle n'a pas été précédée de la consultation, prévue par le code de la santé publique, de l'instance collégiale et de la famille et des proches de M. D... ; <br/>
              - elle n'est pas motivée ;<br/>
              - elle est entachée d'incompétence, dès lors qu'il n'est pas établi qu'elle ait été prise par une personne habilitée à prononcer l'arrêt des soins et traitements de M. D... ;<br/>
              - le maintien des soins et traitements de M. D... ne constitue pas une obstination déraisonnable eu égard notamment au défaut de communication à sa famille et à ses proches des pièces permettant de le constater, au délai très court dans lequel l'état de santé de M. D... a été apprécié et aux réponses de M. D... à certains contacts.<br/>
<br/>
              Par un mémoire en défense, enregistré le 30 juillet 2020, l'Assistance publique - Hôpitaux de Paris conclut au rejet de la requête. Elle soutient que :<br/>
              - les requérants ne justifient pas d'un intérêt à faire appel, dès lors que l'ordonnance attaquée a constaté le retrait de la décision qu'ils contestaient ;<br/>
              - la décision contestée d'arrêt des traitements a été retirée.<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. et Mme B... D... et les autres requérants et, d'autre part, l'Assistance publique - Hôpitaux de Paris ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 31 juillet 2020 à 10 heures :<br/>
<br/>
              - Me Coudray, avocat au Conseil d'Etat et à la Cour de Cassation, avocat de M. et Mme B... D... et des autres requérants ;<br/>
              - les représentants de M. et Mme B... D... et des autres requérants ;<br/>
              - les représentants de l'Assistance publique - Hôpitaux de Paris ;<br/>
<br/>
              le juge des référés ayant, à l'issue de l'audience, différé la clôture de l'instruction au 3 août 2020 à 19 heures, puis au 4 août 2020 à 12 heures 30 ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 3 août 2020, présenté par l'Assistance publique - Hôpitaux de Paris, qui précise les conditions dans lesquelles une expertise pourrait être ordonnée ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 4 août 2020, présenté par M. et Mme B... D... et les autres requérants, qui indiquent les conditions dans lesquelles ils souhaitent qu'une expertise soit ordonnée ;<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code civil ;<br/>
              - le code de la santé publique ;<br/>
              -  la décision du Conseil constitutionnel n° 2017-632 QPC du 2 juin 2017 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". Le juge administratif des référés, saisi d'une demande en ce sens justifiée par une urgence particulière, peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une autorité administrative aurait porté une atteinte grave et manifestement illégale. Ces dispositions législatives confèrent au juge des référés, qui se prononce en principe seul et qui statue, en vertu de l'article L. 511-1 du code de justice administrative, par des mesures qui présentent un caractère provisoire, le pouvoir de prendre, dans les délais les plus brefs et au regard de critères d'évidence, les mesures de sauvegarde nécessaires à la protection des libertés fondamentales.<br/>
<br/>
              2. Toutefois, il appartient au juge des référés d'exercer ses pouvoirs de manière particulière, lorsqu'il est saisi, comme en l'espèce, sur le fondement de l'article L. 521-2 du code de justice administrative, d'une décision, prise par un médecin, dans le cadre défini par le code de la santé publique, et conduisant à arrêter ou ne pas mettre en oeuvre, au titre du refus de l'obstination déraisonnable, un traitement qui apparaît inutile ou disproportionné ou sans autre effet que le seul maintien artificiel de la vie, dans la mesure où l'exécution de cette décision porterait de manière irréversible une atteinte à la vie. Il doit alors prendre les mesures de sauvegarde nécessaires pour faire obstacle à son exécution lorsque cette décision pourrait ne pas relever des hypothèses prévues par la loi, en procédant à la conciliation des libertés fondamentales en cause, que sont le droit au respect de la vie et le droit du patient de consentir à un traitement médical et de ne pas subir un traitement qui serait le résultat d'une obstination déraisonnable.<br/>
<br/>
              Sur le cadre juridique applicable au litige :<br/>
<br/>
              3. Aux termes de l'article L. 1110-1 du code la santé publique : " Le droit fondamental à la protection de la santé doit être mis en oeuvre par tous moyens disponibles au bénéfice de toute personne. (...) ". L'article L. 1110-2 de ce code dispose que : " La personne malade a droit au respect de sa dignité ".<br/>
<br/>
              4. Aux termes de l'article L. 1110-5 du même code : " Toute personne a, compte tenu de son état de santé et de l'urgence des interventions que celui-ci requiert, le droit de recevoir, sur l'ensemble du territoire, les traitements et les soins les plus appropriés et de bénéficier des thérapeutiques dont l'efficacité est reconnue et qui garantissent la meilleure sécurité sanitaire et le meilleur apaisement possible de la souffrance au regard des connaissances médicales avérées. Les actes de prévention, d'investigation ou de traitements et de soins ne doivent pas, en l'état des connaissances médicales, lui faire courir de risques disproportionnés par rapport au bénéfice escompté. (...) ". Aux termes de l'article L. 1110-5-1 du même code : " Les actes mentionnés à l'article L. 1110-5 ne doivent pas être mis en oeuvre ou poursuivis lorsqu'ils résultent d'une obstination déraisonnable. Lorsqu'ils apparaissent inutiles, disproportionnés ou lorsqu'ils n'ont d'autre effet que le seul maintien artificiel de la vie, ils peuvent être suspendus ou ne pas être entrepris, conformément à la volonté du patient et, si ce dernier est hors d'état d'exprimer sa volonté, à l'issue d'une procédure collégiale définie par voie réglementaire. (...) ". Aux termes de l'article L. 1111-4 du même code : " (...) Lorsque la personne est hors d'état d'exprimer sa volonté, la limitation ou l'arrêt de traitement susceptible d'entraîner son décès ne peut être réalisé sans avoir respecté la procédure collégiale mentionnée à l'article L. 1110-5-1 et les directives anticipées ou, à défaut, sans que la personne de confiance prévue à l'article L. 1111-6 ou, à défaut la famille ou les proches, aient été consultés. La décision motivée de limitation ou d'arrêt de traitement est inscrite dans le dossier médical. (...) ". L'article R. 4127-37-2 du même code précise que : " (...) II. - Le médecin en charge du patient peut engager la procédure collégiale de sa propre initiative. (...) La personne de confiance ou, à défaut, la famille ou l'un des proches est informé, dès qu'elle a été prise, de la décision de mettre en oeuvre la procédure collégiale. / III. - La décision de limitation ou d'arrêt de traitement est prise par le médecin en charge du patient à l'issue de la procédure collégiale. Cette procédure collégiale prend la forme d'une concertation avec les membres présents de l'équipe de soins, si elle existe, et de l'avis motivé d'au moins un médecin, appelé en qualité de consultant. Il ne doit exister aucun lien de nature hiérarchique entre le médecin en charge du patient et le consultant. L'avis motivé d'un deuxième consultant est recueilli par ces médecins si l'un d'eux l'estime utile. / (...) / IV. - La décision de limitation ou d'arrêt de traitement est motivée. La personne de confiance, ou, à défaut, la famille, ou l'un des proches du patient est informé de la nature et des motifs de la décision de limitation ou d'arrêt de traitement. La volonté de limitation ou d'arrêt de traitement exprimée dans les directives anticipées ou, à défaut, le témoignage de la personne de confiance, ou de la famille ou de l'un des proches de la volonté exprimée par le patient, les avis recueillis et les motifs de la décision sont inscrits dans le dossier du patient ".<br/>
<br/>
               5. Il résulte des dispositions législatives citées au point 4, ainsi que de l'interprétation que le Conseil constitutionnel en a donnée dans sa décision n° 2017-632 QPC du 2 juin 2017, qu'il appartient au médecin en charge d'un patient hors d'état d'exprimer sa volonté d'arrêter ou de ne pas mettre en oeuvre, au titre du refus de l'obstination déraisonnable, les traitements qui apparaissent inutiles, disproportionnés ou sans autre effet que le seul maintien artificiel de la vie. En pareille hypothèse, le médecin ne peut prendre une telle décision qu'à l'issue d'une procédure collégiale, destinée à l'éclairer sur le respect des conditions légales et médicales d'un arrêt du traitement et, sauf dans les cas mentionnés au troisième alinéa de l'article L. 1111-11 du code de la santé publique, dans le respect des directives anticipées du patient ou, à défaut de telles directives, après consultation de la personne de confiance désignée par le patient ou, à défaut, de sa famille ou de ses proches, ainsi que, le cas échéant, de son ou ses tuteurs.<br/>
<br/>
              Sur les circonstances du litige :<br/>
<br/>
              6. Il résulte de l'instruction que M. G... D..., âgé de 31 ans, a été retrouvé par sa famille à proximité de son domicile le 11 mai 2020 au soir, le visage tuméfié et présentant des troubles de la conscience. Son état s'étant aggravé, il a été pris en charge dans la nuit par une structure mobile d'urgence et de réanimation et amené, alors qu'il était dans le coma, à l'hôpital Bicêtre de l'Assistance publique - Hôpitaux de Paris. Un premier scanner a montré un hématome sous-dural aigu hémisphérique droit, une contusion frontale et temporale droite, une hémorragie méningée gauche ainsi qu'au niveau de la faux du cerveau et une fracture de la paroi inférieure de l'orbite gauche et les examens complémentaires ont révélé de multiples fractures. Une intervention chirurgicale a été réalisée en urgence pour évacuer l'hématome sous-dural à l'origine d'une hypertension intracrânienne. L'état du patient s'est ensuite amélioré et, lorsque les sédations ont pu être arrêtées le 4 juin, il a été évalué au niveau 8 sur 15 de l'échelle de Glasgow. Toutefois, un scanner de contrôle réalisé le 9 juin ayant montré l'apparition d'un hygrome sous-dural hémisphérique gauche compressif, une nouvelle intervention chirurgicale a été réalisée le 13 juin. Dans les jours suivants, l'état du patient a été évalué au niveau 4 ou 5 pour se stabiliser au niveau 4 sur 15 de l'échelle de Glasgow, sans qu'aucune amélioration soit ensuite enregistrée sur le plan neurologique. L'hospitalisation a par ailleurs été marquée par un syndrome de détresse respiratoire aigu lié à une première pneumopathie acquise sous ventilation mécanique, puis par une nouvelle pneumopathie le 2 juin et un pneumothorax droit découvert le 21 juin. <br/>
<br/>
              7. Le 29 juin 2020, une procédure collégiale a été engagée et une réunion entre les médecins du service, le neuropsychologue attaché au service, l'équipe paramédicale s'occupant du patient et un médecin du service de rééducation post-réanimation a conduit à retenir, en considération du coma persistant et de l'évolution prévisible vers un état végétatif persistant, une limitation puis un arrêt des thérapeutiques actives. L'un des médecins en a informé la famille, qui a refusé dans un premier temps l'extubation. Des réunions ont été organisées avec la famille les 17 et 20 juillet, au cours desquelles la décision de procéder à l'extubation du patient, sauf amélioration de l'examen neurologique au cours des jours suivants, a paru au médecin en charge du patient être acceptée par elle. Un nouvel examen dans sa langue maternelle le 22 juillet a montré que M. D... ne répondait pas aux ordres simples et manifestait seulement un léger mouvement de la main droite paraissant aléatoire par rapport aux ordres. Une nouvelle réunion le même jour avec la famille et un ami parlant français a conduit à évoquer une extubation le surlendemain 24 juillet, pouvant conduire soit, en cas d'autonomie respiratoire du patient, à une poursuite de sa prise en charge dans un centre accueillant les patients en état végétatif chronique ou en état pauci-relationnel, soit, dans l'hypothèse inverse, à son décès. Le 23 juillet, la famille s'est rapprochée d'un avocat qui a demandé à l'hôpital Bicêtre de poursuivre les soins et de suspendre la mesure d'arrêt des soins. Le 24 juillet, les parents de M. D..., sa compagne, agissant en son nom propre et en celui de leur fille mineure, et sa soeur ont saisi le juge des référés du tribunal administratif de Paris, sur le fondement de l'article L. 521-2 du code de justice administrative, d'une demande tendant à ce que ce juge suspende l'exécution de la décision d'arrêt de traitement, enjoigne à l'Assistance publique - Hôpitaux de Paris de poursuivre les traitements et les soins et ordonne une expertise en vue de déterminer la situation médicale de M. D.... Ils relèvent appel de l'ordonnance du 27 juillet 2020 par laquelle le juge des référés a dit n'y avoir pas lieu de statuer sur leurs conclusions à fin de suspension, compte tenu du retrait de la décision contestée, et rejeté leurs conclusions à fin d'expertise, regardées comme ne présentant pas un caractère d'urgence relevant du juge des référés statuant sur le fondement de l'article L. 521-2 du code de justice administrative.<br/>
<br/>
              Sur la fin de non-recevoir opposée par l'Assistance publique - Hôpitaux de Paris :<br/>
<br/>
              8. Dans l'hypothèse où le juge du premier degré décide qu'il n'y a pas lieu de statuer sur la demande dont il a été saisi par suite de la disparition de l'objet de cette dernière, l'intérêt à relever appel de la décision juridictionnelle doit être apprécié au regard des conclusions des parties à l'instance. Si la partie ayant elle-même conclu en cours d'instance à ce qu'il n'y ait pas lieu de statuer sur la demande est sans intérêt à relever appel, il en va différemment pour la partie qui n'a pas présenté de conclusions en ce sens. Par suite, l'Assistance publique - Hôpitaux de Paris n'est pas fondée à soutenir que les requérants, qui contestaient devant le juge des référés du tribunal administratif de Paris que le litige ait perdu son objet, n'auraient pas intérêt à relever appel de l'ordonnance du 27 juillet 2020. <br/>
<br/>
              Sur la décision de limitation et d'arrêt de traitement, en tant qu'elle concerne l'arrêt de traitement :<br/>
<br/>
              9. Il résulte de l'instruction que, le 24 juillet, le médecin anesthésiste réanimateur en charge du patient a attesté que la procédure d'arrêt des thérapeutiques actives était " annulée ", procédant ainsi au retrait de la décision d'arrêt de traitement. Si, le 27 juillet, le même médecin a informé la famille d'une nouvelle réunion avec les médecins et les infirmières du service dès le 29 juillet, pour rendre un avis sur l'arrêt des traitements, il a été décidé lors de la réunion tenue à cette date de décaler la réunion collégiale multidisciplinaire devant se prononcer sur l'arrêt des thérapeutiques actives et de confirmer le retrait de la décision d'arrêt de traitement prise le 29 juin. La famille a été reçue le 30 juillet par le médiateur central de l'Assistance publique - Hôpitaux de Paris, avec le médecin en charge de M. D... et le chef du service de réanimation chirurgicale où celui-ci est hospitalisé, en présence d'un interprète moldave. Les représentants de l'Assistance publique - Hôpitaux de Paris ont fait état, à l'audience, de leur choix, alors même qu'ils n'y sont pas tenus légalement, de ne pas prendre de décision d'arrêt de traitement sans recueillir un assentiment suffisant de la famille et, en l'espèce, de ne pas prendre de nouvelle décision avant le résultat d'une expertise par un médecin tiers, qui permette à la famille de disposer d'un autre avis médical quant au risque de traitement inutile, disproportionné ou n'ayant d'autre objet que la seule prolongation artificielle de la vie.<br/>
<br/>
              10. Il suit de là que les requérants ne sont pas fondés à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Paris a dit n'y avoir plus lieu à statuer sur leur demande tendant à la suspension de la décision d'arrêter les thérapeutiques actives au profit de M. D.... <br/>
<br/>
              Sur la décision de limitation et d'arrêt de traitement, en tant qu'elle concerne la limitation de traitement :<br/>
<br/>
              11. Il résulte en revanche de l'instruction que la décision de limitation et d'arrêt de traitement du 29 juin n'a pas été retirée en tant qu'elle prévoit la limitation des thérapeutiques actives. Les professionnels qui se sont réunis le 29 juillet 2020 ont, d'ailleurs, confirmé le choix de ne pas traiter une éventuelle complication. Par suite, les requérants, qui ont pris connaissance de la portée exacte de la décision du 29 juin 2020 par les pièces produites au cours de l'instance, sont fondés à soutenir que c'est à tort que le juge des référés du tribunal administratif de Paris a dit n'y avoir plus lieu à statuer sur leur demande dans cette mesure. Son ordonnance doit, dès lors, être annulée dans la même mesure.<br/>
<br/>
              12. Il y a lieu d'évoquer dans cette limite et de statuer immédiatement sur les conclusions à fin de suspension de la décision de limitation et d'arrêt de traitement, en tant qu'elle concerne la limitation de traitement.<br/>
<br/>
              13. Il résulte de l'instruction que la décision du 29 juin 2020, confirmée le 29 juillet, n'a pas été motivée ni inscrite avec ses motifs dans le dossier du patient, en méconnaissance des articles L. 1111-4 et R. 4127-37-2 du code de la santé publique. Par suite, cette décision ne peut être tenue pour légale et, eu égard à l'importance de la garantie ainsi prévue par le législateur, il y a lieu d'en suspendre l'exécution.<br/>
<br/>
              Sur la demande d'expertise :<br/>
<br/>
              14. Il résulte de l'instruction et des échanges lors de l'audience publique que, d'une part, l'Assistance publique - Hôpitaux de Paris souhaite permettre tant à l'équipe médicale en charge de M. D... qu'à la famille de celui-ci de disposer de tous les éléments utiles pour que les décisions médicales prises à son égard soient guidées par le souci de la plus grande bienfaisance et pour que soit rétabli le lien de confiance nécessaire entre les soignants et la famille et que, d'autre part, si une nouvelle décision de limitation ou d'arrêt de traitement était prise à très brève échéance, il est vraisemblable qu'elle ferait l'objet d'une nouvelle contestation par la famille de M. D... devant le juge des référés. Dans ces conditions, tant la famille de M. D... que les représentants de l'Assistance publique - Hôpitaux de Paris s'accordent sur l'utilité, en complément de la médiation confiée au professeur Devictor, ancien chef de service à l'hôpital Bicêtre, de disposer de l'avis d'un ou plusieurs praticiens extérieurs, rendu dans le cadre d'une expertise ordonnée par le juge des référés, susceptible d'apporter tous éléments utiles sur l'état actuel de M. D... et sur les perspectives d'évolution de cet état.<br/>
<br/>
              15. Dans les circonstances de l'espèce, il y a lieu de faire droit à cette demande, dans le cadre de la présente instance engagée sur le fondement de l'article L. 521-2 du code de justice administrative. Il y a lieu, en conséquence, de réformer l'ordonnance du juge des référés du tribunal administratif de Paris sur ce point et de prescrire une expertise médicale, confiée à un collège de deux médecins disposant des compétences appropriées, aux fins de se prononcer, après avoir examiné le patient, rencontré l'équipe médicale et le personnel soignant en charge de ce dernier ainsi que sa famille et pris connaissance de l'ensemble de son dossier médical, sur l'état actuel de M. D... et de donner toutes indications utiles, en l'état de la science, sur les perspectives d'évolution qu'il pourrait connaître, de façon à éclairer le choix de prendre ou non une décision de limitation puis, le cas échéant, d'arrêt de traitement à son égard.   <br/>
<br/>
              Sur les conclusions aux fins d'injonction :<br/>
<br/>
              16. Le juge des référés du tribunal administratif de Paris n'ayant pas statué sur ces conclusions de la demande de première instance, il y a lieu d'annuler son ordonnance dans cette mesure et d'y statuer immédiatement par la voie de l'évocation.<br/>
<br/>
              17. Il résulte des échanges au cours de l'audience publique que le médecin en charge de M. D... ne prendra pas de nouvelle décision de limitation ou d'arrêt de traitement avant que les parties disposent du rapport de l'expertise médicale mentionnée au point 15. Compte tenu de l'engagement ainsi pris, il n'y a pas lieu de prononcer à l'encontre de l'Assistance publique - Hôpitaux de Paris une injonction de poursuivre les traitements de M. D.... Par ailleurs, la présente décision ne fait pas obstacle à ce que l'équipe médicale mette en oeuvre, dans le respect du code de la santé publique, les traitements, tels qu'une trachéotomie, qui lui paraissent les plus appropriés en fonction de l'évolution de l'état de santé du patient.<br/>
<br/>
<br/>
              Sur les conclusions relatives aux frais liés au litige :<br/>
<br/>
              18. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Assistance publique - Hôpitaux de Paris une somme de 2 000 euros à verser aux requérants au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Paris du 27 juillet 2020 est annulée en tant qu'elle dit n'y avoir pas lieu de statuer sur les conclusions de la demande tendant à la suspension de la décision contestée, en ce qu'elle concerne la limitation de traitement, statue sur les conclusions tendant à ce qu'une expertise soit ordonnée et omet de statuer sur les conclusions aux fins d'injonction.<br/>
Article 2 : L'exécution de la décision de limitation et d'arrêt de traitement du 29 juin 2020 est suspendue en tant qu'elle concerne la limitation de traitement. <br/>
Article 3 : Il sera procédé à une expertise, diligentée de manière contradictoire, aux fins :<br/>
- de décrire l'état clinique actuel de M. G... D..., sa capacité à communiquer et son niveau de souffrance ;<br/>
- de se prononcer sur le caractère irréversible des lésions cérébrales de M. D... et sur le pronostic clinique.<br/>
Article 4 : Les experts devront procéder à l'examen de M. G... D..., rencontrer l'équipe médicale et le personnel soignant en charge de celui-ci ainsi que sa famille et prendre connaissance de l'ensemble de son dossier médical. Ils pourront consulter tous documents, procéder à tous examens et vérifications utiles et entendre toute personne compétente. Ils accompliront leur mission dans les conditions prévues par les articles R. 621-2 à R. 621-14 du code de justice administrative, à l'exception du troisième alinéa de l'article R. 621-9 et de l'article R. 621-10. La fixation et la charge de leurs frais et honoraires seront déterminées conformément aux dispositions de l'article R. 621-13.<br/>
Article 5 : Cette expertise est confiée au professeur Thomas Geeraerts, chef du service de réanimation neurochirurgicale au centre hospitalier universitaire de Toulouse, et au professeur Jean-Michel Gracies, chef du service de rééducation neurolocomotrice aux hôpitaux universitaires Henri Mondor de l'Assistance publique - Hôpitaux de Paris, qui prêteront serment par écrit ou devant le secrétaire du contentieux du Conseil d'Etat et déposeront leur rapport avant le 15 septembre 2020 au secrétariat du contentieux.<br/>
Article 6 : L'Assistance publique - Hôpitaux de Paris versera une somme de 2 000 euros aux requérants au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 7 : Le surplus des conclusions de la requête est rejeté<br/>
Article 8 : La présente ordonnance sera notifiée à Mme C... D..., première dénommée, pour l'ensemble des requérants, à l'Assistance publique - Hôpitaux de Paris, au professeur Thomas Geeraerts et au professeur Jean-Michel Gracies.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
