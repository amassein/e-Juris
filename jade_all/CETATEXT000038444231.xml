<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038444231</ID>
<ANCIEN_ID>JG_L_2019_04_000000429645</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/44/42/CETATEXT000038444231.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 12/04/2019, 429645, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-04-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429645</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>GOLDMAN ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:429645.20190412</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés les 11 et 12 avril 2019 au secrétariat du contentieux du Conseil d'Etat, Mme B...A...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative : <br/>
<br/>
              1°) d'ordonner la suspension de la décision du 9 avril 2019 par laquelle l'Agence française de lutte contre le dopage (AFLD) a prononcé à son encontre une mesure de suspension provisoire à titre conservatoire consistant, en premier lieu, à la participation directe ou indirecte à l'organisation et au déroulement de toute manifestation sportive donnant lieu à la remise de prix en argent ou en nature, et à des manifestations sportives autorisées par une fédération professionnelle ainsi qu'aux entrainements y préparant organisés par une fédération agréée ou une ligue professionnelle ou l'un des membres de celles-ci, en deuxième lieu, à l'exercice des fonction définies à l'article L. 212-1 du code du sport, en troisième lieu, à l'exercice des fonctions de personnel d'encadrement ou de toute activité administrative au sein d'une fédération agréée ou d'une ligue professionnelle, ou de l'un des membres de celles-ci et, en dernier lieu, à la participation à toute autre activité organisée par une fédération sportive, une ligue professionnelle ou l'un de leur membre, ou le comité olympique et sportif français, ainsi qu'aux activités sportives impliquant des sportifs de niveau national ou international et financées par une personne publique, à moins que ces activités ne s'inscrivent dans des programmes ayant pour objet la prévention du dopage ;<br/>
<br/>
              2°) de mettre à la charge de l'Agence française de lutte contre le dopage la somme de 3 500 euros en application des dispositions de l'article L. 761-1  du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - la condition d'urgence est remplie dès lors que, en premier lieu, la décision litigieuse l'empêche de participer au Marathon de Paris prévu le dimanche 14 avril 2019 et, par suite, d'exercer son activité de sportive de haut niveau, en deuxième lieu, elle rend impossible toute pratique de son activité sportive de haut niveau dans l'attente de la décision définitive de l'AFLD, en troisième lieu, la décision contestée porte atteinte à ses intérêts financiers générés à titre essentiel par sa profession et, en dernier lieu, elle porte atteinte à son image et à sa réputation ;<br/>
              - il existe un doute sérieux quant à la légalité de la décision contestée ;<br/>
              - l'article L. 232-23-4 du code du sport méconnaît les droits de la défense dès lors qu'en permettant à l'intéressée de ne formuler des observations qu'après l'intervention d'une mesure de suspension provisoire, ses observations sont privées de tout effet utile ;<br/>
              - la décision a été prise en méconnaissance du principe du contradictoire ;<br/>
              - l'article L. 232-23-4 méconnaît le principe de séparation des pouvoirs dès lors qu'il fait du président de l'AFLD tout à la fois un organe de poursuite et de sanction ; <br/>
              - la décision litigieuse est entachée d'une insuffisance de motivation ;<br/>
              - la décision litigieuse repose sur une procédure de contrôle irrégulière dès lors que les rapports des agents contrôleurs sont contradictoires, que ces derniers n'ont pas décliné leur identité et qualité et n'étaient pas habilités pour opérer un contrôle, et qu'ils n'ont pas procédé à la notification du contrôle ;<br/>
              - la décision litigieuse est entachée d'une erreur de qualification juridique des faits dès lors qu'on ne saurait lui reprocher d'avoir pris la fuite ;<br/>
              - la décision litigieuse méconnaît le secret professionnel, les éléments du dossier ayant été exposés dans la presse préalablement à date de son intervention ;<br/>
              - la décision litigieuse est disproportionnée ;<br/>
              - la décision litigieuse est entachée d'un détournement de procédure.<br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 12 avril 2019, l'Agence française de lutte contre le dopage conclut au rejet de la requête. Elle soutient que la condition d'urgence n'est pas remplie, que les moyens soulevés par la requérante ne sont pas propres à créer un doute sérieux quant à la légalité de la décision contestée.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code du sport ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique d'une part, Mme A...et, d'autre part, l'Agence française de lutte contre le dopage ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du vendredi 12 avril 2019 à 15 heures au cours de laquelle ont été entendus : <br/>
              - Me Goldman, avocat au Conseil d'Etat et à la Cour de cassation, avocat de Mme A...;<br/>
<br/>
              - MmeA... ; <br/>
<br/>
              - les représentants de MmeA... ;<br/>
<br/>
              - Me Valdelièvre, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'Agence française de lutte contre le dopage ;<br/>
<br/>
              - les représentants de l'Agence française de lutte contre le dopage ;<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              2. Aux termes de l'article L. 211-2 du code des relations entre le public et l'administration : " Les personnes physiques ou morales ont le droit d'être informées sans délai des motifs des décisions administratives individuelles défavorables qui les concernent. A cet effet, doivent être motivées les décisions qui : / 1° Restreignent l'exercice des libertés publiques ou, de manière générale, constituent une mesure de police (...) ". Aux termes des dispositions de l'article L. 121-1 du même code : " Exception faite des cas où il est statué sur une demande, les décisions individuelles qui doivent être motivées en application de l'article L. 211-2, ainsi que les décisions qui, bien que non mentionnées à cet article, sont prises en considération de la personne, sont soumises au respect d'une procédure contradictoire préalable ". Aux termes des dispositions de l'article L. 121-2 du même code : " Les dispositions de l'article L. 121-1 ne sont pas applicables : (...) 3°Aux décisions pour lesquelles des dispositions législatives ont instauré une procédure contradictoire particulière ; (...) ". Aux termes de l'article L. 232-23-4 du code du sport, dans sa version en vigueur au 1er mars 2019 : " (...) La décision de suspension provisoire est motivée. L'intéressé est convoqué par le président de l'agence, dans les meilleurs délais, pour faire valoir ses observations sur cette mesure (...) ".<br/>
<br/>
              3. Il résulte de l'instruction qu'un contrôle anti-dopage a été diligenté par l'Agence française de lutte contre le dopage à l'encontre de MmeA..., à Marrakech, le 27 mars 2019. Aux termes de cette procédure de contrôle, Mme A...a fait l'objet d'une décision du 9 avril 2019 par laquelle l'Agence a prononcé sa suspension provisoire à titre conservatoire.<br/>
<br/>
              4. En premier lieu, eu égard aux effets de la mesure de suspension prononcée, qui empêche MmeA..., athlète professionnelle, de participer à toute manifestation sportive et notamment au Marathon de Paris, prévu le dimanche 14 avril 2019, qui constitue une étape importante de son calendrier sportif, la condition d'urgence doit être regardée comme remplie.<br/>
<br/>
              5. En second lieu, il est constant que la décision de suspension du 9 avril 2019, fondée sur les faits s'étant déroulés le 27 mars 2019, a été notifiée à Mme A...le 10 avril 2019 mentionnant la possibilité de faire valoir des observations sur la mesure prononcée à son encontre en se présentant dans les locaux de l'Agence le lundi 15 avril 2019 ou le jeudi 18 avril 2019, alors qu'il résulte de l'instruction que la mesure litigieuse était destinée à produire des effets à son égard dès le dimanche 14 avril en la privant de la possibilité de participer au Marathon de Paris. Par suite et, à défaut d'urgence justifiant que Mme A...n'ait pas été mise à même de présenter des observations en temps utile, le moyen tiré de ce que l'Agence aurait méconnu le principe du contradictoire est de nature, en l'état de l'instruction, à créer un doute sérieux quant à la légalité de la décision contestée.<br/>
<br/>
              6. Il résulte de ce qui précède qu'il y a lieu d'ordonner la suspension de l'exécution de la décision contestée jusqu'à ce qu'il soit statué sur les conclusions tendant à son annulation. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Agence française de lutte contre le dopage le versement à Mme A...d'une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'exécution de la décision de l'Agence française de lutte contre le dopage du 9 avril 2019 prononcée à l'encontre Mme A...est suspendue jusqu'à ce qu'il soit statué sur les conclusions de cette dernière tendant à l'annulation de cette décision.<br/>
Article 2 : L'Agence française de lutte contre le dopage versera la somme de 3 000 euros à Mme A... en application des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente ordonnance sera notifiée à Mme B...A...et à l'Agence française de lutte contre le dopage.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
