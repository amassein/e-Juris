<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030853959</ID>
<ANCIEN_ID>JG_L_2015_07_000000378238</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/85/39/CETATEXT000030853959.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème SSJS, 03/07/2015, 378238, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>378238</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Dominique Chelle </RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:378238.20150703</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A... B...a demandé au tribunal administratif de Nancy d'annuler la décision du 31 mai 2012 du ministre de l'intérieur l'informant de la perte de points de son permis de conduire, constatant la perte de validité de ce titre pour solde de points nul et lui enjoignant de le restituer.<br/>
<br/>
              Par un jugement n° 1201535-3 du 4 février 2014, le tribunal administratif a prononcé un non lieu à statuer concernant le retrait d'un point du permis de conduire de M. B... à la suite de l'infraction commise le 16 juillet 2009, annulé les décisions du ministre de l'intérieur portant retrait de points à la suite des infractions commises les 9 février 2004, 25 mars 2004 et 17 juin 2005 et enjoint au ministre de l'intérieur de rétablir sept points dans le capital affecté au permis de conduire de l'intéressé, et d'en tirer les conséquences sur la validité de ce permis.<br/>
<br/>
              Par un pourvoi enregistré le 22 avril 2014 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M. B... ; <br/>
<br/>
<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la route ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dominique Chelle, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B...a demandé au tribunal administratif de Nancy d'annuler des décisions du ministre de l'intérieur portant retrait de points de son permis de conduire à la suite d'infractions au code de la route, ainsi qu'une décision du 31 mai 2012 constatant la perte de validité du permis pour solde de points nul ; que, par un jugement du 4 février 2014, le tribunal administratif a constaté que le point retiré à la suite d'une infraction commise le 16 juillet 2009 avait été rétabli ultérieurement et en a déduit que tant les conclusions dirigées contre la décision retirant ce point que celles dirigées contre la décision du 31 mai 2012 avaient perdu leur objet ; que le tribunal a par ailleurs annulé les décisions portant retrait de points à la suite des infractions commises les 9 février 2004, 25 mars 2004 et 17 juin 2005 et a enjoint au ministre de l'intérieur de rétablir sept points dans le capital affecté au permis de conduire de M. B... ; que le ministre de l'intérieur se pourvoit en cassation contre ce jugement ;<br/>
<br/>
              En ce qui concerne la décision du 31 mai 2012 constatant la perte de validité du permis de conduire :<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que, si le point retiré à la suite de l'infraction du 16 juillet 2009 avait ultérieurement été rétabli, la décision du 31 mai 2012 constatant la perte de validité du permis, qui était justifiée par les autres décisions de retrait de points, portant au total sur un nombre de points excédant le capital du permis, n'avait pas été retirée ; que, par suite, en énonçant au point 2 de son jugement que les conclusions de M. B...tendant à l'annulation de cette décision avaient perdu leur objet, le tribunal administratif a commis une erreur de droit ; <br/>
<br/>
              En ce qui concerne les décisions de retrait de point consécutives aux infractions commises les 9 février et 25 mars 2004 et le 17 juin 2005 :<br/>
<br/>
              3. Considérant que les dispositions portant application des articles R. 49-1 et R. 49-10 du code de procédure pénale en vigueur à la date des infractions litigieuses, notamment celles des articles A. 37 à A. 37-4 de ce code, issues de l'arrêté du 5 octobre 1999 relatif aux formulaires utilisés pour la constatation et le paiement des contraventions soumises à la procédure de l'amende forfaitaire, prévoient que, lorsqu'une amende soumise à cette procédure est relevée avec interception du véhicule mais sans que l'amende soit payée immédiatement entre les mains de l'agent verbalisateur, ce dernier utilise un formulaire réunissant, en une même liasse autocopiante, le procès-verbal conservé par le service verbalisateur, une carte de paiement matériellement indispensable pour procéder au règlement de l'amende et de l'avis de contravention, également remis au contrevenant pour servir de justificatif du paiement ultérieur, qui comporte une information suffisante au regard des exigences résultant des articles L. 223-3 et R. 223-3 du code de la route ; que le titulaire d'un permis de conduire à l'encontre duquel une infraction au code de la route est relevée au moyen d'un formulaire conforme à ce modèle et dont il est établi, notamment par la mention qui en est faite au système national des permis de conduire, qu'il a payé l'amende forfaitaire correspondant à cette infraction a nécessairement reçu l'avis de contravention ; qu'eu égard aux mentions dont cet avis est réputé être revêtu, l'administration doit être regardée comme s'étant acquittée envers le titulaire du permis de son obligation de lui délivrer les informations requises préalablement au paiement de l'amende, à moins que l'intéressé, à qui il appartient à cette fin de produire l'avis qu'il a nécessairement reçu, ne démontre s'être vu remettre un avis inexact ou incomplet ;<br/>
<br/>
              4. Considérant, par ailleurs, que si l'intervention de l'arrêté du 5 octobre 1999 ne garantit pas, à elle seule, que des formulaires établis selon un modèle antérieur, dans lequel le document comportant les informations requises et celui nécessaire au paiement étaient entièrement distincts, n'aient pas continué à être utilisés pour la constatation des infractions, il résulte tant du règlement du Conseil du 3 mai 1998 concernant l'introduction de l'euro que des mesures législatives et règlementaires prises pour sa mise en oeuvre, s'agissant notamment du montant des amendes, que de tels formulaires, libellés en francs, n'ont pu être employés après le 1er janvier 2002 ; que, pour les infractions relevées avec interception du véhicule à compter de cette date, la mention au système national des permis de conduire du paiement ultérieur de l'amende forfaitaire permet au juge d'estimer que le titulaire du permis s'est vu remettre un avis de contravention comportant les informations requises ;<br/>
<br/>
              5. Considérant qu'après avoir relevé que les infractions commises par M. B... les 9 février  et 25 mars 2004 et le 17 juin 2005, constatées avec interception du véhicule, avaient donné lieu au paiement ultérieur d'amendes forfaitaires, le tribunal administratif a jugé qu'il n'était pas établi que ces infractions aient été relevées au moyen d'un formulaire conforme aux dispositions mentionnées ci-dessus, de sorte que le paiement de l'amende n'était pas de nature à établir que l'intéressé avait bénéficié des informations requises ; qu'en statuant ainsi, alors qu'il ressortait de ses constatations que les infractions avaient été commises après le 1er janvier 2002, le tribunal a entaché son jugement d'une erreur de droit ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que le ministre de l'intérieur est fondé à demander l'annulation du jugement attaqué ;<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Le jugement du 4 février 2014 du tribunal administratif de Nancy est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Nancy.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
