<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027362530</ID>
<ANCIEN_ID>JG_L_2013_04_000000357223</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/36/25/CETATEXT000027362530.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 26/04/2013, 357223, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-04-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357223</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. Christophe Pourreau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:357223.20130426</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 29 février et 29 mai 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Thénac (Charente-Maritime), représentée par son maire ; la commune de Thénac demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2011-1994 du 27 décembre 2011 authentifiant les chiffres des populations de métropole, des départements d'outre-mer de la Guadeloupe, de la Guyane, de la Martinique et de La Réunion, de Saint-Barthélemy, de Saint-Martin et de Saint-Pierre-et-Miquelon en tant qu'il fixe sa population totale, pour l'application des lois et règlements à compter du 1er janvier 2012, à un chiffre de 1 860 habitants, qu'elle estime inférieur à la réalité ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre de prendre un nouveau décret authentifiant les résultats du recensement de la population de la commune au 1er janvier 2009, dans un délai de deux mois à compter de la notification de la décision à intervenir ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Barthélemy, Matuchansky, Vexliard, avocat de la commune de Thénac,<br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Barthélemy, Matuchansky, Vexliard, avocat de la commune de Thénac ;<br/>
<br/>
<br/>
<br/>
              1. Considérant, en premier lieu, qu'aux termes de l'article R. 2151-1 du code général des collectivités territoriales : " I. - Les personnes prises en compte dans les catégories de population définies ci-dessous sont les personnes résidant dans les logements d'une commune, celles résidant dans les communautés telles que définies aux V et VI du présent article, les personnes sans abri et les personnes résidant habituellement dans des habitations mobiles. / II. - Les catégories de population sont : / 1. La population municipale ; / 2. La population comptée à part ; / 3. La population totale, qui est la somme des deux précédentes. / III. - La population municipale d'une commune, mentionnée au 1 du II du présent article, comprend : / 1. Les personnes ayant leur résidence habituelle sur le territoire de la commune. La résidence habituelle, au sens du présent décret, d'une personne ayant plusieurs résidences en France métropolitaine, dans les départements d'outre-mer ou à Saint-Pierre-et-Miquelon est : / a) Pour une personne mineure résidant ailleurs du fait de ses études, la résidence de sa famille ; / (...) c) Pour une personne majeure résidant dans une communauté appartenant à la catégorie 4 définie au VI du présent article, la communauté ; / (...) V. - Une communauté est un ensemble de locaux d'habitation relevant d'une même autorité gestionnaire et dont les habitants partagent à titre habituel un mode de vie commun. La population de la communauté comprend les personnes qui résident dans la communauté, à l'exception de celles résidant dans des logements de fonction. / VI. - Les catégories de communautés sont : / (...) 4. Les établissements hébergeant des élèves ou des étudiants, y compris les établissements militaires d'enseignement ; (...) " ; que, pour l'application de ces dispositions aux personnes résidant dans un établissement militaire d'enseignement implanté sur le territoire de plusieurs communes, il appartient à l'institut national de la statistique et des études économiques (INSEE) de répartir la population recensée entre ces communes en tenant compte de la situation des locaux d'habitation de l'établissement et de l'utilisation par ces personnes des principaux services publics ; que, dans le cas où ces personnes utiliseraient principalement des services publics à la charge d'une ou de plusieurs communes autres que celles sur le territoire desquelles les locaux d'habitation sont situés, la répartition de la population correspondant à la situation des locaux d'habitation devrait être pondérée en conséquence ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que l'école d'enseignement technique de l'armée de l'air (EETAA) est située pour partie sur le territoire de la commune de Thénac et pour partie sur celui de la commune voisine des Gonds ; que le chiffre de 158 personnes résidant à l'EETAA et rattachées à la population de la commune de Thénac qui a été retenu par l'INSEE correspond au nombre de personnes résidant dans des locaux d'habitation de l'école situés sur le territoire de la commune de Thénac ; qu'à la date du décret attaqué, ni la distribution d'eau, ni l'assainissement, ni la collecte des déchets provenant de l'EETAA, qui faisait l'objet d'un contrat conclu directement entre l'école et un prestataire externe, n'étaient assurés par la commune de Thénac ; qu'en outre, les principales routes permettant d'accéder à l'école, notamment celle reliant l'école à Saintes, étaient des routes départementales, dont l'entretien incombe au département ; qu'ainsi, les personnes résidant à l'EETAA n'utilisaient pas principalement des services publics à la charge de la commune de Thénac ; que, par suite, la répartition des personnes résidant à l'EETAA entre les communes des Gonds et de Thénac n'est pas entachée d'erreur de droit ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'il ressort des pièces du dossier que les volontaires militaires du rang et les militaires techniciens de l'air se succèdent à l'EETAA pour suivre une formation d'une durée n'excédant pas six semaines et demie ; que, dès lors, ils ne peuvent pas être regardés comme résidant à titre habituel à l'EETAA au sens des dispositions précitées du V de l'article R. 2151-1 du code général des collectivités territoriales ; que, par suite, ces personnes ne peuvent pas être incluses à ce titre dans la population municipale de la commune de Thénac ; qu'ainsi, l'INSEE n'a commis aucune erreur de droit en ne tenant pas compte de ces personnes pour fixer le chiffre de la population de la commune de Thénac ;<br/>
<br/>
              4. Considérant, en troisième lieu, que, si la commune de Thénac soutient que c'est à tort que des élèves techniciens et des volontaires militaires du rang mineurs en formation à l'EETAA n'ont pas été inclus dans la population comptée à part, dont le chiffre a été fixé à 216 pour l'application des lois et règlements à compter du 1er janvier 2012, elle n'assortit en tout état de cause ce moyen d'aucun élément de nature à en établir le bien-fondé ; qu'il ne peut, par suite, qu'être écarté ;<br/>
<br/>
              5. Considérant, en quatrième lieu, que, si la commune de Thénac soutient que le chiffre de 216 personnes qui lui a été attribué au titre de sa population municipale dans la catégorie " communautés " est sans rapport avec les 406 bulletins collectés par l'INSEE dans les deux communautés qui ont leur siège sur son territoire, il ressort des pièces du dossier que ce chiffre de 216 résulte de l'addition des bulletins collectés à l'EHPAD, soit 58, et des bulletins collectés à l'EETAA et correspondant à des élèves majeurs et qui entrent donc dans le champ d'application des dispositions précitées du c du 1 du III de l'article R. 2151-1 du code général des collectivités territoriales, soit 158 ; que le moyen tiré de ce que l'INSEE aurait commis une erreur de fait dans l'évaluation de la population municipale dans la catégorie " communautés " ne peut, par suite, qu'être écarté ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que la commune de Thénac n'est pas fondée à demander l'annulation pour excès de pouvoir du décret attaqué ; que, par voie de conséquence, il y a lieu de rejeter ses conclusions tendant à ce qu'il soit enjoint au Premier ministre d'édicter un nouveau décret ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la commune de Thénac est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la commune de Thénac, au ministre de l'économie et des finances, au ministre de l'intérieur et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
