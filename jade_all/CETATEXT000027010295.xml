<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027010295</ID>
<ANCIEN_ID>JG_L_2013_01_000000344152</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/01/02/CETATEXT000027010295.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 30/01/2013, 344152, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-01-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>344152</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:344152.20130130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 3 novembre 2010 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre du budget, des comptes publics et de la réforme de l'Etat ; le ministre du budget, des comptes publics et de la réforme de l'Etat demande au Conseil d'Etat d'annuler l'arrêt de la cour administrative d'appel de Paris n° 08PA06126 du 22 septembre 2010 en tant qu'il n'a que partiellement fait droit aux conclusions de son recours tendant à l'annulation du jugement du 3 juillet 2008 par lequel le tribunal administratif de Melun a prononcé la réduction des cotisations de taxe professionnelle mises à la charge de la SNC DGR Ile-de-France, au titre des années 2003 et 2004, pour son établissement exploité, sous l'enseigne " Mercure ", dans la commune de Gentilly ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Delaporte, Briard, Trichet, avocat de la société NMP France,<br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Delaporte, Briard, Trichet, avocat de la société NMP France ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'en vertu du 1° de l'article 1469 du code général des impôts, dans sa rédaction applicable aux impositions en litige, la valeur locative des immobilisations entrant dans l'assiette de la taxe professionnelle est calculée, pour les biens passibles d'une taxe foncière, suivant les règles fixées pour l'établissement de cette taxe ; qu'aux termes de l'article 1498 du même code, applicable en matière de taxe foncière : " La valeur locative de tous les biens autres que les locaux visés au I de l'article 1496 et que les établissements industriels visés à l'article 1499 est déterminée au moyen de l'une des méthodes indiquées ci-après : / 1° Pour les biens donnés en location à des conditions de prix normales, la valeur locative est celle qui ressort de cette location ; / 2° a. Pour les biens loués à des conditions de prix anormales ou occupés par leur propriétaire, occupés par un tiers à un autre titre que la location, vacants ou concédés à titre gratuit, la valeur locative est déterminée par comparaison. / Les termes de comparaison sont choisis dans la commune. Ils peuvent être choisis hors de la commune pour procéder à l'évaluation des immeubles d'un caractère particulier ou exceptionnel ; / b. La valeur locative des termes de comparaison est arrêtée : / Soit en partant du bail en cours à la date de référence de la révision lorsque l'immeuble type était loué normalement à cette date,/ Soit, dans le cas contraire, par comparaison avec des immeubles similaires situés dans la commune ou dans une localité présentant, du point de vue économique, une situation analogue à celle de la commune en cause et qui faisaient l'objet à cette date de locations consenties à des conditions de prix normales ; / 3° A défaut de ces bases, la valeur locative est déterminée par voie d'appréciation directe. " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond, que la société NMP France, anciennement dénommée DGR Ile-de-France, qui exploite un immeuble à usage d'hôtel, sous l'enseigne " Mercure ", sur le territoire de la commune de Gentilly (Val-de-Marne), a sollicité la décharge des cotisations de taxe professionnelle auxquelles elle a été assujettie, au titre des années 2003 et 2004, à raison de cette exploitation, en invoquant l'irrégularité de l'évaluation de la valeur locative de l'immeuble, qui avait été déterminée, au terme d'une chaîne de comparaisons, par comparaison avec le local-type n°4 de la commune d'Evry ; que, par jugement du 3 juin 2008, le tribunal administratif de Melun a partiellement fait droit à sa demande aux motifs que le local-type n°4 de la commune d'Evry ne pouvait être retenu comme terme de comparaison dès lors qu'il n'était pas loué au 1er janvier 1970 et que la valeur locative de l'immeuble devait être déterminée par comparaison avec celle du local-type n° 43 de la commune de Villejuif, dont le montant était inférieur ; que, saisie en appel par le ministre du budget, des comptes publics et de la fonction publique, la cour administrative d'appel de Paris a toutefois jugé, par un arrêt du 22 septembre 2010, que le local-type n° 43 de Villejuif ne pouvait être retenu comme terme de comparaison, dès lors qu'il était loué à des conditions de prix anormales au 1er janvier 1970 et que la valeur locative de l'immeuble devait être déterminée par comparaison avec celle du local-type n°55 du procès-verbal complémentaire de la commune de Villeneuve-Saint-Georges ; qu'il en est résulté une augmentation de la valeur locative devant servir de base à la taxe professionnelle, sans que cette valeur n'atteigne toutefois celle qui avait été initialement retenue par l'administration ; que seule une fraction des impositions auxquelles la société avait été initialement assujettie a, en conséquence, été remise à sa charge ; que le ministre du budget, des comptes publics et de la réforme de l'Etat se pourvoit en cassation contre l'arrêt de la cour en tant qu'il n'a pas fait intégralement droit à son appel ;<br/>
<br/>
              3. Considérant qu'il résulte des dispositions précitées de l'article 1498 du code général des impôts que le juge de l'impôt, saisi d'une contestation portant sur la méthode d'évaluation, a l'obligation, lorsqu'il estime irrégulière la méthode d'évaluation retenue pour déterminer les impositions en litige, de lui substituer la méthode d'évaluation qu'il juge régulière ; que, dans le cas où il retient une évaluation par comparaison, il doit, en outre, pour l'application des dispositions du 2° de l'article 1498 précité du code général des impôts, statuer d'office sur le terme de comparaison qu'il estime, par une appréciation souveraine, pertinent et dont il a vérifié la régularité, au vu des éléments dont il dispose ou qu'il a sollicités par un supplément d'instruction ;<br/>
<br/>
              4. Considérant qu'en présence d'éléments contradictoires, le juge ne saurait regarder un local-type comme ayant été régulièrement évalué et le retenir, par suite, comme terme de comparaison ; que, pour retenir le local-type n°55 de la commune de Villeneuve-Saint-Georges, correspondant à un hôtel classé dans la catégorie " deux étoiles " exploité sous l'enseigne Campanile, la cour a notamment jugé que si l'administration soutenait que ce local-type avait fait l'objet d'une évaluation irrégulière, par comparaison avec le local-type n° 10 de la commune de Chennevières-sur-Marne, lequel aurait lui même été évalué par la méthode d'appréciation directe, les pièces figurant au dossier, notamment la fiche de calcul de la valeur locative du local type n° 10 de la commune de Chennevières-sur-Marne, comportaient des mentions contradictoires et que celles-ci ne permettaient pas d'établir l'irrégularité alléguée ; qu'il résulte de ce qui a été dit ci-dessus qu'en statuant ainsi, la cour a commis une erreur de droit ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que le ministre du budget, des comptes publics et de la réforme de l'Etat est fondé à demander l'annulation de l'arrêt qu'il attaque en tant qu'il n'a que partiellement fait droit aux conclusions de son appel ;<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 22 septembre 2010 est annulé en tant qu'il n'a que partiellement fait droit aux conclusions de l'appel du ministre chargé du budget .<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Paris.<br/>
Article 3 : Les conclusions de la société NMP France  présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'économie et des finances et à la société NMP France.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
