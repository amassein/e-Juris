<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032892405</ID>
<ANCIEN_ID>JG_L_2016_07_000000381636</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/89/24/CETATEXT000032892405.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 13/07/2016, 381636, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>381636</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>Mme Florence Chaltiel-Terral</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:381636.20160713</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir la décision du 6 avril 2011 par laquelle l'inspecteur du travail a autorisé la société Seris Security à le licencier. Par un jugement n° 1110018 du 27 juin 2013, le tribunal administratif a annulé cette décision.<br/>
<br/>
              Par un arrêt n° 13PA03390 du 22 avril 2014, la cour administrative d'appel de Paris, sur l'appel de la société Seris Security, a annulé ce jugement et rejeté la demande présentée à ce tribunal par M.B....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 juin et 23 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la requête de la société Seris Security ; <br/>
<br/>
              3°) de mettre à la charge de la société Seris Security la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Chaltiel-Terral, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. B...et à la SCP Delaporte, Briard, avocat de la société Seris Security ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Seris Security a sollicité le 10 février 2011 l'autorisation de licencier pour motif disciplinaire M.B..., salarié protégé, en lui reprochant son comportement inconvenant et perturbateur lors de réunions des institutions représentatives du personnel, des agissements incontrôlés ainsi que des menaces proférées à l'encontre d'un salarié lors de visites de sites dans le cadre de ses heures de délégation et, enfin, un comportement agressif à l'occasion de demandes de documents se rapportant au fonctionnement des institutions représentatives du personnel ; que, par une décision en date du 6 avril 2011, l'inspecteur du travail, se fondant sur ces seuls derniers faits, a accordé l'autorisation sollicitée au motif qu'ils rendaient impossible le maintien du salarié dans l'entreprise ; que, par un jugement en date du 27 juin 2013, le tribunal administratif de Paris a annulé cette décision au motif que les faits en cause n'étaient pas, à eux seuls, de nature à rendre impossible le maintien du salarié dans l'entreprise ; que par un arrêt du 22 avril 2014 contre lequel M. B...se pourvoit en cassation, la cour administrative d'appel de Paris a annulé ce jugement en jugeant que les actes reprochés étaient de nature à rendre impossible le maintien du salarié dans l'entreprise ;<br/>
<br/>
              2. Considérant qu'en vertu des dispositions du code du travail, le licenciement des salariés légalement investis de fonctions représentatives, qui bénéficient d'une protection exceptionnelle dans l'intérêt de l'ensemble des travailleurs qu'ils représentent, ne peut intervenir que sur autorisation de l'inspecteur du travail ; que, dans le cas où la demande de licenciement est motivée par un comportement fautif, il appartient à l'inspecteur du travail saisi et, le cas échéant, au ministre compétent, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si les faits reprochés au salarié sont d'une gravité suffisante pour justifier le licenciement, compte tenu de l'ensemble des règles applicables au contrat de travail de l'intéressé et des exigences propres à l'exécution normale du mandat dont il est investi ; que dans le cas où la demande de licenciement est motivée par un acte ou un comportement du salarié qui, ne méconnaissant pas les obligations découlant pour lui de son contrat de travail, ne constitue pas une faute, il appartient à l'inspecteur du travail, et le cas échéant au ministre, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si les faits en cause sont établis et de nature, compte tenu de leur répercussion sur le fonctionnement de l'entreprise, à rendre impossible le maintien du salarié dans l'entreprise ; que lorsqu'un employeur demande à l'inspecteur du travail l'autorisation de licencier un salarié protégé, il lui appartient de faire précisément état dans sa demande des motifs justifiant, selon lui, le licenciement, l'inspecteur du travail ne pouvant, pour accorder l'autorisation demandée, se fonder sur d'autres motifs que ceux énoncés dans la demande ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis à la cour administrative d'appel de Paris que M. B...avait notamment soutenu devant le tribunal administratif de Paris, à l'appui de son recours contre la décision autorisant son licenciement, que, ayant été saisi par l'employeur d'une demande fondée sur un comportement fautif, l'inspecteur du travail ne pouvait légalement délivrer l'autorisation sollicitée en se fondant sur l'impossibilité du maintien du salarié dans l'entreprise ; que M. B...est fondé à soutenir que la cour administrative d'appel, saisie de ce moyen, qui n'était pas inopérant, par l'effet dévolutif de l'appel a, en omettant d'y répondre, entaché son arrêt d'insuffisance de motivation ; que M. B... est par suite fondé, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à en demander l'annulation ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Seris Security une somme de 3 500 euros à verser à M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font en revanche obstacle à ce qu'une somme soit mise à ce titre à la charge de M. B...qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 22 avril 2014 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris. <br/>
<br/>
Article 3 : La société Seris Security versera la somme de 3 500 euros à M. B...au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : Les conclusions présentées par la société Seris Security au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. A...B...et à la société Seris Security.<br/>
Copie en sera adressée à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
