<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029103377</ID>
<ANCIEN_ID>JG_L_2014_06_000000374370</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/10/33/CETATEXT000029103377.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 18/06/2014, 374370, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-06-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374370</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Romain Godet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:374370.20140618</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 3 janvier 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par le Syndicat de la fonction publique, dont le siège est BP 42105 à Papeete (98714) ; le Syndicat de la fonction publique demande au Conseil d'Etat :<br/>
              1°) de déclarer illégale la " loi du pays " de Polynésie française n° 2013-26 LP/APF du 29 novembre 2013 relative aux concours d'intégration des agents non titulaires des services et établissements publics administratifs de la Polynésie française ;<br/>
<br/>
              2°) d'annuler l'éventuel acte de promulgation de cette " loi du pays " ;<br/>
<br/>
              3°) de mettre à la charge de la Polynésie française la somme de 100 euros au titre des articles L. 761-1 et R. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 74 ;<br/>
<br/>
              Vu la loi organique n° 2004-192 du 27 février 2004 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Romain Godet, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que, sur le fondement de l'article 140 de la loi organique du 27 février 2004, l'assemblée de la Polynésie française a adopté, le 29 novembre 2013, une " loi du pays " relative aux concours d'intégration des agents non titulaires des services et établissements publics administratifs de la Polynésie française ; que cette loi du pays a été publiée au Journal officiel de la Polynésie française, à titre d'information, le 9 décembre 2013 ; que, dans le cadre du contrôle juridictionnel spécifique défini au chapitre II du titre VI de cette loi organique, le Syndicat de la fonction publique a saisi le Conseil d'Etat d'une requête tendant à ce que cette " loi du pays " soit déclarée illégale et que son éventuel acte de promulgation soit annulé ;<br/>
<br/>
              2. Considérant que la " loi du pays " litigieuse n'a fait l'objet d'aucun acte de promulgation ; que, dès lors, les conclusions tendant à l'annulation d'un éventuel acte de promulgation de cette loi du pays n'ont pas à être examinées ;<br/>
<br/>
              Sur la légalité externe de la " loi du pays " contestée :<br/>
<br/>
              3. Considérant, en premier lieu, que, si les membres du conseil supérieur de la fonction publique de la Polynésie française n'ont pas reçu, dans le délai prévu par l'article 13 de la délibération n° 95-216 AT du 14 décembre 1995 portant organisation et fonctionnement des organismes consultatifs dans la fonction publique du territoire de la Polynésie française, le procès-verbal de la séance au cours de laquelle le projet de " loi du pays " contestée a été examiné, cette circonstance, postérieure à la consultation pour avis de cet organe, est sans incidence sur la régularité de l'avis ; que le moyen est donc inopérant ;<br/>
<br/>
              4. Considérant, en deuxième lieu, que l'absence de publication du compte rendu intégral des séances au cours desquelles la " loi du pays " litigieuse a été adoptée par l'assemblée de la Polynésie française est également sans incidence sur sa légalité ;<br/>
<br/>
              5. Considérant, en troisième lieu, que le syndicat requérant soutient que le rapport de présentation de la " loi du pays " mentionne, à tort, que l'avis rendu par le conseil économique, social et culturel de la Polynésie française était favorable et que l'assemblée de la Polynésie française n'a pas examiné les préconisations faites par le conseil ; que, toutefois, d'une part, le contenu de ce rapport de présentation, dès lors qu'il n'est pas tel qu'il doive conduire à regarder le rapport comme inexistant, est sans incidence sur la régularité de la procédure d'adoption de la " loi du pays " ; que, d'autre part, l'avis en cause, qui a été publié au Journal officiel de la Polynésie française du 8 novembre 2013, est un avis purement consultatif qui ne s'impose pas à l'assemblée ; que, par suite, le moyen invoqué doit être écarté ;<br/>
<br/>
              6. Considérant, en quatrième lieu, que la circonstance que l'avis donné par le haut conseil de la Polynésie française sur ce projet n'a pas fait l'objet d'une publicité et d'une communication au public, n'a pu, en tout état de cause, exercer une influence sur le sens de la " loi du pays " adoptée et n'a privé les personnes intéressées d'aucune garantie ; qu'elle n'a donc pas vicié la procédure ;<br/>
<br/>
              Sur la légalité interne de la " loi du pays " contestée :<br/>
<br/>
              7. Considérant que si le principe d'égal accès aux emplois publics énoncé à l'article 6 de la Déclaration des droits de l'homme et du citoyen ne s'oppose pas à ce que des règles de recrutement destinées à permettre l'appréciation des aptitudes et qualités des candidats à l'entrée dans un corps ou cadre d'emplois de fonctionnaires soient différenciées pour tenir compte de la variété des situations, et en particulier des expériences professionnelles antérieures ainsi que des besoins du service public, il impose toutefois à l'autorité compétente de ne fonder ses décisions de recrutement que sur les vertus, talents et capacités des intéressés à remplir leurs missions, au regard de la nature du service public considéré et des règles, le cas échéant statutaires, régissant l'organisation et le fonctionnement de ce service ;<br/>
<br/>
              8. Considérant que l'article LP. 1er de la " loi du pays " litigieuse met en place une voie d'intégration pérenne à la fonction publique du territoire de la Polynésie française au profit des agents non fonctionnaires de la Polynésie française et des agents non titulaires des services et des établissements publics administratifs de la Polynésie française ; que le concours d'intégration qu'il prévoit est ouvert à ceux de ces agents justifiant, à sa date d'ouverture, " d'une ancienneté au moins égale à trois ans de durée de service effectif dans des fonctions correspondant au cadre d'emplois pour lequel est ouvert le concours et sous réserve de détenir les diplômes et l'expérience professionnelle requis pour se présenter au concours externe " ; que le même article réserve aux agents recrutés par cette voie spécifique 50 % des places mises au concours par l'autorité compétente ; <br/>
<br/>
              9. Considérant qu'en réservant ainsi, sans limitation de durée, à une catégorie d'agents recrutés par contrat et n'ayant qu'une ancienneté d'au moins trois ans dans les fonctions correspondant au cadre d'emplois pour lequel est ouvert le concours, une voie d'accès spécifique portant sur la moitié des postes à pourvoir, alors que l'effectif de ces agents est significativement inférieur à celui des autres catégories de candidats susceptibles de se présenter aux concours ouverts pour les mêmes postes, les auteurs de la " loi du pays " contestée ont méconnu le principe d'égal accès aux emplois publics énoncé à l'article 6 de la Déclaration des droits de l'homme et du citoyen ; que l'article LP. 1er de " loi du pays " contestée doit, en conséquence, être déclaré illégal ; qu'il en va de même de l'article LP. 3 de cette même " loi du pays ", qui se borne à tirer les conséquences, dans d'autres textes relevant du domaine des " lois de pays ", des dispositions prévues à l'article LP. 1er ;<br/>
<br/>
              10. Considérant, en deuxième lieu, que le principe d'égalité ne s'oppose ni à ce qu'une " loi du pays " règle de façon différente des situations différentes ni à ce qu'elle déroge à l'égalité pour des raisons d'intérêt général, pourvu que, dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet du texte qui l'établit et ne soit pas manifestement disproportionnée au regard des motifs susceptibles de la justifier ; que les salariés relevant du code du travail polynésien et les agents non titulaires des services et établissements publics administratifs de la Polynésie française sont placés dans des situations différentes ; qu'en conséquence, doit être regardé comme inopérant le moyen tiré de ce que l'article LP. 2 de la " loi du pays " contestée, en portant de deux ans à trois ans la durée maximale pour laquelle des agents non titulaires peuvent être recrutés afin de faire face temporairement, et dans l'intervalle des concours après épuisement de la liste complémentaire, à la vacance d'un emploi devant immédiatement être pourvu afin d'assurer la continuité du service public, porterait atteinte au principe d'égalité entre ces agents et les salariés recrutés dans le cadre de contrats à durée déterminée ; qu'ainsi, le syndicat requérant n'est pas fondé à demander que l'article LP. 2 de la " loi du pays " contestée soit déclaré illégal ;<br/>
<br/>
              11. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le Syndicat de la fonction publique au titre des dispositions des articles L. 761-1 et R. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Les articles LP. 1er et LP. 3 de la " loi du pays " n° 2013-26 LP/APF du 29 novembre 2013 relative aux concours d'intégration des agents non titulaires des services et établissements publics administratifs de la Polynésie française sont déclarés illégaux et ne peuvent être promulgués.<br/>
Article 2 : Le surplus des conclusions de la requête du Syndicat de la fonction publique est rejeté.<br/>
Article 3 : La présente décision sera notifiée au Syndicat de la fonction publique, au président de la Polynésie française, au président de l'assemblée de la Polynésie française, au haut-commissaire de la République en Polynésie française et au ministre des outre-mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
