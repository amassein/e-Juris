<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027333023</ID>
<ANCIEN_ID>JG_L_2013_04_000000340093</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/33/30/CETATEXT000027333023.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section, 19/04/2013, 340093, Publié au recueil Lebon</TITRE>
<DATE_DEC>2013-04-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>340093</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. Maxime Boutron</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2013:340093.20130419</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 31 mai et 30 août 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la chambre de commerce et d'industrie d'Angoulême, dont le siège est situé 27, place Bouillaud à Angoulême (16021) ; elle demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 2 de l'arrêt n° 08BX03152 du 1er avril 2010 par lequel, statuant par la voie de l'évocation après annulation du jugement n° 0700263 du 16 octobre 2008 du tribunal administratif de Poitiers, la cour administrative d'appel de Bordeaux a rejeté sa demande tendant à l'annulation de la décision implicite par laquelle le ministre de l'équipement a rejeté sa demande du 9 novembre 2006 tendant au versement d'une somme de 6 169 494 euros et à la condamnation de l'Etat à lui verser cette somme augmentée des intérêts à compter de la réception de sa demande préalable ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit intégralement à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'aviation civile ;<br/>
<br/>
              Vu le décret n° 53-893 du 24 septembre 1953 ;<br/>
<br/>
              Vu la décision du Conseil d'Etat, statuant au contentieux du 28 décembre 2009 n° 304802 commune de Béziers ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Maxime Boutron, Auditeur,  <br/>
<br/>
              - les observations de la SCP Rocheteau, Uzan-Sarano, avocat de la chambre de commerce et d'industrie d'Angoulême et de la SCP Barthélemy, Matuchansky, Vexliard, avocat du ministre de l'écologie, du développement durable et de l'énergie,<br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Rocheteau, Uzan-Sarano, avocat de la chambre de commerce et d'industrie d'Angoulême et à la SCP Barthélemy, Matuchansky, Vexliard, avocat du ministre de l'écologie, du développement durable et de l'énergie ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que, saisie en appel du jugement du tribunal administratif de Poitiers en date du 16 octobre 2008 du litige opposant la chambre de commerce et d'industrie d'Angoulême à l'Etat au sujet des avances que cet établissement public soutenait avoir consenties à l'Etat,  au titre de la gestion, à partir de 1984, sur le fondement d'arrêtés du préfet de la Charente, de l'aérodrome de Brie-Champniers, la cour, après avoir, par l'article 1er non contesté de son arrêt,  annulé, en raison d'un vice de procédure, le jugement du tribunal administratif, a statué par la voie de l'évocation et réglé le litige sur le terrain contractuel, en jugeant que l'Etat et la chambre de commerce et d'industrie étaient liés par une concession d'outillage public portant sur l'exploitation et l'entretien de l'aérodrome et que le vice tenant à l'incompétence du préfet pour accorder une telle concession, laquelle aurait dû résulter d'un arrêté pris par les ministres chargés de l'économie et de l'aviation marchande, n'était pas, dès lors qu'il résultait du dossier soumis à l'examen des juges du fond que les ministres concernés avaient approuvé l'opération, d'une gravité  telle que le juge dût écarter le contrat ;<br/>
<br/>
              2. Considérant que pour statuer ainsi, la cour a fait à bon droit application, par l'arrêt attaqué du 1er avril 2010, de la règle énoncée par la décision du Conseil d'Etat, statuant au contentieux n° 304802 du 28 décembre 2009, commune de Béziers, selon laquelle, eu égard à l'exigence de loyauté des relations contractuelles, il incombe en principe au juge, saisi d'un litige relatif à l'exécution d'un contrat, de faire application du contrat et qu'en conséquence, c'est seulement dans le cas où il constate une irrégularité invoquée par une partie ou relevée d'office par lui, tenant au caractère illicite du contenu du contrat ou à un vice d'une particulière gravité relatif notamment aux conditions dans lesquelles les parties ont donné leur consentement, qu'il ne peut régler le litige sur le terrain contractuel ;<br/>
<br/>
              3. Considérant toutefois que, devant le tribunal administratif comme devant la cour, les parties, qui s'accordaient pour constater que le préfet n'avait pas compétence pour accorder une concession relative à l'entretien et à l'exploitation d'un aérodrome, avaient exclusivement débattu, compte tenu des règles applicables avant la décision du Conseil d'Etat, statuant au contentieux du 28 décembre 2009, sur le terrain de la responsabilité quasi-contractuelle et sur celui de la responsabilité quasi-délictuelle ; que la clôture de l'instruction avait été fixée devant la cour au 3 septembre 2009 ; que si, en faisant application des règles issues d'une décision du Conseil d'Etat, statuant au contentieux postérieure à cette dernière date, la cour s'est bornée à exercer son office en situant le litige sur le terrain juridiquement approprié et  n'a pas soulevé un moyen d'ordre public qu'elle aurait dû communiquer aux parties en application de l'article R. 611-7 du code de justice administrative, elle ne pouvait, eu égard aux exigences de la procédure contradictoire, régler l'affaire sur un terrain dont les parties n'avaient pas débattu sans avoir mis celles-ci à même de présenter leurs observations sur ce point ; qu'il lui incombait à cette fin soit de rouvrir l'instruction en invitant les parties à s'exprimer sur les conséquences à tirer de la décision du Conseil d'Etat, statuant au contentieux en date du 28 décembre 2009, soit de juger, par un arrêt avant-dire droit, qu'elle entendait régler le litige, compte-tenu de cette décision, sur le terrain contractuel et en demandant en conséquence aux parties de formuler leurs observations sur ce terrain ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède qu'en jugeant l'affaire au fond, sans avoir, selon l'une ou l'autre des procédures indiquées au point 3, permis aux parties de s'exprimer sur le terrain sur lequel la décision du Conseil d'Etat, statuant au contentieux du 28 décembre 2009 la conduisait à situer le litige, la cour administrative d'appel de Bordeaux a méconnu le  caractère contradictoire de la procédure ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, la chambre de commerce et d'industrie d'Angoulême est fondée à demander l'annulation de l'article 2 de l'arrêt attaqué ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la chambre de commerce et d'industrie d'Angoulême au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font en revanche obstacle à ce qu'une somme soit mise à ce titre à la charge de la chambre de commerce et d'industrie d'Angoulême qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
      --------------<br/>
Article 1er : L'article 2 de l'arrêt de la cour administrative d'appel de Bordeaux du 1er avril 2010 est annulé.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Bordeaux.<br/>
Article 3 : L'Etat versera à la chambre de commerce et d'industrie d'Angoulême la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de l'Etat présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la chambre de commerce et d'industrie d'Angoulême et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">37-03-02-01 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. RÈGLES GÉNÉRALES DE PROCÉDURE. INSTRUCTION. CARACTÈRE CONTRADICTOIRE DE LA PROCÉDURE. - DÉCISION DU CONSEIL D'ETAT POSTÉRIEURE À LA CLÔTURE DE L'INSTRUCTION AYANT POUR CONSÉQUENCE QUE LE JUGE DOIVE RÉGLER LE LITIGE SUR UN AUTRE TERRAIN JURIDIQUE QUE CELUI SUR LEQUEL LES PARTIES ONT DÉBATTU - 1) A) RÈGLEMENT DU LITIGE SUR LE TERRAIN JURIDIQUEMENT APPROPRIÉ - EXERCICE PAR LE JUGE DE SON OFFICE - EXISTENCE - MOYEN D'ORDRE PUBLIC QU'IL DEVRAIT COMMUNIQUER AUX PARTIES (ART. R. 611-7 DU CJA) - ABSENCE - B) FACULTÉ DU JUGE DE RÉGLER L'AFFAIRE SUR UN TERRAIN DONT LES PARTIES N'AVAIENT PAS DÉBATTU SANS AVOIR MIS CELLES-CI À MÊME DE PRÉSENTER LEURS OBSERVATIONS - ABSENCE, EU ÉGARD AUX EXIGENCES DE LA PROCÉDURE CONTRADICTOIRE - CONSÉQUENCE - NÉCESSITÉ DE METTRE LES PARTIES À MÊME DE S'EXPRIMER SUR LES CONSÉQUENCES À TIRER DE LA DÉCISION DU CONSEIL D'ETAT - MODALITÉS - I) RÉOUVERTURE DE L'INSTRUCTION EN INVITANT LES PARTIES À S'EXPRIMER SUR CE POINT - II) AVANT DIRE DROIT ET DEMANDE AUX PARTIES DE FORMULER LEURS OBSERVATIONS SUR LE TERRAIN AINSI RETENU - 2) ESPÈCE - COUR AYANT APPLIQUÉ LES RÈGLES ISSUES DE LA JURISPRUDENCE  BÉZIERS I  [RJ1], POSTÉRIEURE À LA CLÔTURE DE L'INSTRUCTION, SANS AVOIR PERMIS AUX PARTIES DE S'EXPRIMER SUR LE TERRAIN SUR LEQUEL CETTE JURISPRUDENCE LA CONDUISAIT À SITUER LE LITIGE, ALORS QUE CELLES-CI AVAIENT EXCLUSIVEMENT DÉBATTU SUR D'AUTRES TERRAINS COMPTE TENU DES RÈGLES APPLICABLES AVANT CETTE DÉCISION - CARACTÈRE CONTRADICTOIRE DE LA PROCÉDURE - MÉCONNAISSANCE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-04-03 PROCÉDURE. INSTRUCTION. CARACTÈRE CONTRADICTOIRE DE LA PROCÉDURE. - DÉCISION DU CONSEIL D'ETAT POSTÉRIEURE À LA CLÔTURE DE L'INSTRUCTION AYANT POUR CONSÉQUENCE QUE LE JUGE DOIVE RÉGLER LE LITIGE SUR UN AUTRE TERRAIN JURIDIQUE QUE CELUI SUR LEQUEL LES PARTIES ONT DÉBATTU - 1) A) RÈGLEMENT DU LITIGE SUR LE TERRAIN JURIDIQUEMENT APPROPRIÉ - EXERCICE PAR LE JUGE DE SON OFFICE - EXISTENCE - MOYEN D'ORDRE PUBLIC QU'IL DEVRAIT COMMUNIQUER AUX PARTIES (ART. R. 611-7 DU CJA) - ABSENCE - B) FACULTÉ DU JUGE DE RÉGLER L'AFFAIRE SUR UN TERRAIN DONT LES PARTIES N'AVAIENT PAS DÉBATTU SANS AVOIR MIS CELLES-CI À MÊME DE PRÉSENTER LEURS OBSERVATIONS - ABSENCE, EU ÉGARD AUX EXIGENCES DE LA PROCÉDURE CONTRADICTOIRE - CONSÉQUENCE - NÉCESSITÉ DE METTRE LES PARTIES À MÊME DE S'EXPRIMER SUR LES CONSÉQUENCES À TIRER DE LA DÉCISION DU CONSEIL D'ETAT - MODALITÉS - I) RÉOUVERTURE DE L'INSTRUCTION EN INVITANT LES PARTIES À S'EXPRIMER SUR CE POINT - II) AVANT DIRE DROIT ET DEMANDE AUX PARTIES DE FORMULER LEURS OBSERVATIONS SUR LE TERRAIN AINSI RETENU - 2) ESPÈCE - COUR AYANT APPLIQUÉ LES RÈGLES ISSUES DE LA JURISPRUDENCE  BÉZIERS I  [RJ1], POSTÉRIEURE À LA CLÔTURE DE L'INSTRUCTION, SANS AVOIR PERMIS AUX PARTIES DE S'EXPRIMER SUR LE TERRAIN SUR LEQUEL CETTE JURISPRUDENCE LA CONDUISAIT À SITUER LE LITIGE, ALORS QUE CELLES-CI AVAIENT EXCLUSIVEMENT DÉBATTU SUR D'AUTRES TERRAINS COMPTE TENU DES RÈGLES APPLICABLES AVANT CETTE DÉCISION - CARACTÈRE CONTRADICTOIRE DE LA PROCÉDURE - MÉCONNAISSANCE - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-01 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. - DÉCISION DU CONSEIL D'ETAT POSTÉRIEURE À LA CLÔTURE DE L'INSTRUCTION AYANT POUR CONSÉQUENCE QUE LE JUGE DOIVE RÉGLER LE LITIGE SUR UN AUTRE TERRAIN JURIDIQUE QUE CELUI SUR LEQUEL LES PARTIES ONT DÉBATTU - 1) A) RÈGLEMENT DU LITIGE SUR LE TERRAIN JURIDIQUEMENT APPROPRIÉ - EXERCICE PAR LE JUGE DE SON OFFICE - EXISTENCE - MOYEN D'ORDRE PUBLIC QU'IL DEVRAIT COMMUNIQUER AUX PARTIES (ART. R. 611-7 DU CJA) - ABSENCE - B) FACULTÉ DU JUGE DE RÉGLER L'AFFAIRE SUR UN TERRAIN DONT LES PARTIES N'AVAIENT PAS DÉBATTU SANS AVOIR MIS CELLES-CI À MÊME DE PRÉSENTER LEURS OBSERVATIONS - ABSENCE, EU ÉGARD AUX EXIGENCES DE LA PROCÉDURE CONTRADICTOIRE - CONSÉQUENCE - NÉCESSITÉ DE METTRE LES PARTIES À MÊME DE S'EXPRIMER SUR LES CONSÉQUENCES À TIRER DE LA DÉCISION DU CONSEIL D'ETAT - MODALITÉS - I) RÉOUVERTURE DE L'INSTRUCTION EN INVITANT LES PARTIES À S'EXPRIMER SUR CE POINT - II) AVANT DIRE DROIT ET DEMANDE AUX PARTIES DE FORMULER LEURS OBSERVATIONS SUR LE TERRAIN AINSI RETENU - 2) ESPÈCE - COUR AYANT APPLIQUÉ LES RÈGLES ISSUES DE LA JURISPRUDENCE  BÉZIERS I  [RJ1], POSTÉRIEURE À LA CLÔTURE DE L'INSTRUCTION, SANS AVOIR PERMIS AUX PARTIES DE S'EXPRIMER SUR LE TERRAIN SUR LEQUEL CETTE JURISPRUDENCE LA CONDUISAIT À SITUER LE LITIGE, ALORS QUE CELLES-CI AVAIENT EXCLUSIVEMENT DÉBATTU SUR D'AUTRES TERRAINS COMPTE TENU DES RÈGLES APPLICABLES AVANT CETTE DÉCISION - CARACTÈRE CONTRADICTOIRE DE LA PROCÉDURE - MÉCONNAISSANCE - EXISTENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-07-01-04-01-01 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS D'ORDRE PUBLIC À SOULEVER D'OFFICE. ABSENCE. - APPLICATION PAR LE JUGE DES RÈGLES ISSUES D'UNE DÉCISION DU CONSEIL D'ETAT POSTÉRIEURE À LA CLÔTURE DE L'INSTRUCTION - PARTIES AYANT EXCLUSIVEMENT DÉBATTU, COMPTE TENU DES RÈGLES APPLICABLES AVANT CETTE DÉCISION, SUR UN AUTRE TERRAIN JURIDIQUE - EXERCICE PAR LE JUGE DE SON OFFICE - EXISTENCE - MOYEN D'ORDRE PUBLIC QU'IL DEVRAIT COMMUNIQUER AUX PARTIES EN APPLICATION DE L'ARTICLE R. 611-7 DU CJA - ABSENCE.
</SCT>
<ANA ID="9A"> 37-03-02-01 1) a) En situant le litige sur le terrain juridiquement approprié en application des règles issues d'une décision du Conseil d'Etat, statuant au contentieux postérieure à la date de la clôture de l'instruction, alors que les parties avaient exclusivement débattu, compte tenu des règles applicables avant cette décision, sur un autre terrain juridique, le juge se borne à exercer son office et ne soulève pas un moyen d'ordre public qu'il devrait communiquer aux parties en application de l'article R. 611-7 du code de justice administrative (CJA).,,b) Toutefois, il ne peut, eu égard aux exigences de la procédure contradictoire, régler l'affaire sur un terrain dont les parties n'avaient pas débattu sans avoir mis celles-ci à même de présenter leurs observations sur ce point. Il lui incombe à cette fin : i) soit de rouvrir l'instruction en invitant les parties à s'exprimer sur les conséquences à tirer de la décision du Conseil d'Etat, ii) soit de juger, par un arrêt avant dire droit, qu'il entend régler le litige, compte tenu de cette décision, sur le terrain juridiquement approprié et en demandant en conséquence aux parties de formuler leurs observations sur ce terrain.,,,2) En l'espèce, affaire dans laquelle les parties avaient exclusivement débattu sur le terrain de la responsabilité quasi-contractuelle et sur celui de la responsabilité quasi-délictuelle, compte tenu des règles applicables avant la décision du Conseil d'Etat du 28 décembre 2009, Commune de Béziers ( Béziers I ), postérieure à la clôture de l'instruction.,,En jugeant cette affaire sans avoir, selon l'une ou l'autre des procédures indiquées au point 1-b, permis aux parties de s'exprimer sur le terrain sur lequel cette décision du Conseil d'Etat la conduisait à situer le litige, une cour méconnaît le caractère contradictoire de la procédure.</ANA>
<ANA ID="9B"> 54-04-03 1) a) En situant le litige sur le terrain juridiquement approprié en application des règles issues d'une décision du Conseil d'Etat, statuant au contentieux postérieure à la date de la clôture de l'instruction, alors que les parties avaient exclusivement débattu, compte tenu des règles applicables avant cette décision, sur un autre terrain juridique, le juge se borne à exercer son office et ne soulève pas un moyen d'ordre public qu'il devrait communiquer aux parties en application de l'article R. 611-7 du code de justice administrative (CJA).,,b) Toutefois, il ne peut, eu égard aux exigences de la procédure contradictoire, régler l'affaire sur un terrain dont les parties n'avaient pas débattu sans avoir mis celles-ci à même de présenter leurs observations sur ce point. Il lui incombe à cette fin : i) soit de rouvrir l'instruction en invitant les parties à s'exprimer sur les conséquences à tirer de la décision du Conseil d'Etat, ii) soit de juger, par un arrêt avant dire droit, qu'il entend régler le litige, compte tenu de cette décision, sur le terrain juridiquement approprié et en demandant en conséquence aux parties de formuler leurs observations sur ce terrain.,,,2) En l'espèce, affaire dans laquelle les parties avaient exclusivement débattu sur le terrain de la responsabilité quasi-contractuelle et sur celui de la responsabilité quasi-délictuelle, compte tenu des règles applicables avant la décision du Conseil d'Etat du 28 décembre 2009, Commune de Béziers ( Béziers I ), postérieure à la clôture de l'instruction.,,En jugeant cette affaire sans avoir, selon l'une ou l'autre des procédures indiquées au point 1-b, permis aux parties de s'exprimer sur le terrain sur lequel cette décision du Conseil d'Etat la conduisait à situer le litige, une cour méconnaît le caractère contradictoire de la procédure.</ANA>
<ANA ID="9C"> 54-07-01 1) a) En situant le litige sur le terrain juridiquement approprié en application des règles issues d'une décision du Conseil d'Etat, statuant au contentieux postérieure à la date de la clôture de l'instruction, alors que les parties avaient exclusivement débattu, compte tenu des règles applicables avant cette décision, sur un autre terrain juridique, le juge se borne à exercer son office et ne soulève pas un moyen d'ordre public qu'il devrait communiquer aux parties en application de l'article R. 611-7 du code de justice administrative (CJA).,,b) Toutefois, il ne peut, eu égard aux exigences de la procédure contradictoire, régler l'affaire sur un terrain dont les parties n'avaient pas débattu sans avoir mis celles-ci à même de présenter leurs observations sur ce point. Il lui incombe à cette fin : i) soit de rouvrir l'instruction en invitant les parties à s'exprimer sur les conséquences à tirer de la décision du Conseil d'Etat, ii) soit de juger, par un arrêt avant dire droit, qu'il entend régler le litige, compte tenu de cette décision, sur le terrain juridiquement approprié et en demandant en conséquence aux parties de formuler leurs observations sur ce terrain.,,,2) En l'espèce, affaire dans laquelle les parties avaient exclusivement débattu sur le terrain de la responsabilité quasi-contractuelle et sur celui de la responsabilité quasi-délictuelle, compte tenu des règles applicables avant la décision du Conseil d'Etat du 28 décembre 2009, Commune de Béziers ( Béziers I ), postérieure à la clôture de l'instruction.,,En jugeant cette affaire sans avoir, selon l'une ou l'autre des procédures indiquées au point 1-b, permis aux parties de s'exprimer sur le terrain sur lequel cette décision du Conseil d'Etat la conduisait à situer le litige, une cour méconnaît le caractère contradictoire de la procédure.</ANA>
<ANA ID="9D"> 54-07-01-04-01-01 En situant le litige sur le terrain juridiquement approprié en application des règles issues d'une décision du Conseil d'Etat, statuant au contentieux postérieure à la date de la clôture de l'instruction, alors que les parties avaient exclusivement débattu, compte tenu des règles applicables avant cette décision, sur un autre terrain juridique, le juge se borne à exercer son office et ne soulève pas un moyen d'ordre public qu'il devrait communiquer aux parties en application de l'article R. 611-7 du code de justice administrative (CJA).</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 28 décembre 2009, Commune de Béziers, n° 304802, p. 509.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
