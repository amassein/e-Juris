<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033255699</ID>
<ANCIEN_ID>JG_L_2016_10_000000400375</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/25/56/CETATEXT000033255699.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 17/10/2016, 400375</TITRE>
<DATE_DEC>2016-10-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400375</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>Mme Charline Nicolas</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2016:400375.20161017</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par un arrêt n° 15PA01889 du 2 juin 2016, la cour administrative d'appel de Paris, avant de statuer sur la demande d'appel de la caisse de prévoyance sociale de la Polynésie française tendant en premier lieu, à l'annulation du jugement n° 1300351 du 10 février 2015 par lequel le tribunal de la Polynésie française a, sur la demande de Mme B...veuveA..., d'une part, annulé la décision du 28 mai 2013 par laquelle le ministre de la défense a refusé de l'indemniser sur le fondement de la loi n° 2010-2 du 5 janvier 2010 relative à la reconnaissance de l'indemnisation des victimes des essais nucléaires français, en qualité d'ayant droit de son époux défunt, d'autre part, à ce qu'il soit enjoint à l'Etat de saisir le comité d'indemnisation des victimes des essais nucléaires (CIVEN) pour qu'il procède à l'évaluation des préjudices subis, et enfin a rejeté ses conclusions tendant à la condamnation de l'Etat à lui verser la somme totale de 2 596 203 F CFP correspondant aux frais médicaux exposés pour le traitement de la maladie de M. A...et aux indemnités journalières qu'elle a versées et, en second lieu, de faire droit à ses conclusions de première instance, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen les questions suivantes : <br/>
<br/>
              - Quelle est la nature du régime d'indemnisation spécial institué par la loi du 5 janvier 2010 : l'Etat indemnise-t-il les victimes des essais nucléaires français en tant que personne responsable du dommage ou en tant que garant de la solidarité nationale ' <br/>
              - Quelle est la nature du contentieux relatif à la mise en oeuvre de ce régime d'indemnisation spécial et quelles sont les conséquences qu'il convient d'en tirer quant à la recevabilité d'une action subrogatoire : ce contentieux relève-t-il exclusivement de l'excès de pouvoir, ou de plein contentieux, ou la victime dispose-t-elle d'un droit d'option à cet égard ' <br/>
              - Les dispositions spécifiques précitées de l'article 42 de la délibération de l'assemblée territoriale de la Polynésie française n° 74-22 du 14 février 1974 modifiée instituant un régime d'assurance maladie invalidité au profit des travailleurs salariés permettent-elles à la caisse de prévoyance sociale de la Polynésie française de former une action subrogatoire contre l'Etat pour le remboursement de ses débours ' <br/>
<br/>
              Des observations, enregistrées le 30 juin 2016, ont été présentées par le Comité d'indemnisation des victimes des essais nucléaires.<br/>
<br/>
              Des observations, enregistrées le 8 juillet 2016, ont été présentées pour la Caisse de prévoyance sociale de la Polynésie française. <br/>
<br/>
              Des observations, enregistrées le 11 juillet 2016, ont été présentées par le ministre de la défense.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu : <br/>
- la loi n° 2010-2 du 5 janvier 2010 modifiée ;<br/>
- le décret n° 2014-1049 du 15 septembre 2014 ;<br/>
- le code de justice administrative, et notamment son article L. 113-1 ;<br/>
<br/>
               Après avoir entendu en séance publique :<br/>
<br/>
- le rapport de Mme Charline Nicolas, auditeur, <br/>
- les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
<br/>REND L'AVIS SUIVANT<br/>
<br/>
              1. Aux termes de l'article L. 376-1 du code de sécurité sociale : " Lorsque, sans entrer dans les cas régis par les dispositions législatives applicables aux accidents du travail, la lésion dont l'assuré social ou son ayant droit est atteint est imputable à un tiers, l'assuré ou ses ayants droit conserve contre l'auteur de l'accident le droit de demander la réparation du préjudice causé, conformément aux règles du droit commun, dans la mesure où ce préjudice n'est pas réparé par application du présent livre. Les caisses de sécurité sociale sont tenues de servir à l'assuré ou à ses ayants droit les prestations prévues par le présent livre, sauf recours de leur part contre l'auteur responsable de l'accident dans les conditions ci-après. Les recours subrogatoires des caisses contre les tiers s'exercent poste par poste sur les seules indemnités qui réparent des préjudices qu'elles ont pris en charge, à l'exclusion des préjudices à caractère personnel ". Aux termes de l'article 42 de la délibération de l'assemblée territoriale de la Polynésie française n° 74-22 du 14 février 1974 modifiée instituant un régime d'assurance maladie invalidité au profit des travailleurs salariés : " Lorsque, sans entrer dans les cas régis par les dispositions législatives applicables aux accidents du travail, l'incident ou la blessure dont l'assurée est victime est imputable à un tiers, l'organisme de gestion est subrogé de plein droit à l'intéressé ou à ses ayants droit dans leur action contre le tiers responsable pour le remboursement des dépenses que lui occasionne l'accident ou la blessure (...) "<br/>
<br/>
              Les recours des tiers payeurs ayant versé des prestations à la victime d'un dommage corporel, organisés par l'article L. 376-1 du code de la sécurité sociale s'agissant des caisses de sécurité sociale et par la délibération du 14 février 1974 de l'assemblée territoriale de la Polynésie française s'agissant des organismes de sécurité sociale de cette collectivité, s'exercent à l'encontre des auteurs responsables du dommage dont souffre la victime. <br/>
<br/>
              2. L'article 1er de la loi du 5 janvier 2010 modifiée relative à la reconnaissance et à l'indemnisation des victimes des essais nucléaires français modifiée dispose : " Toute personne souffrant d'une maladie radio-induite résultant d'une exposition à des rayonnements ionisants dus aux essais nucléaires français et inscrite sur une liste fixée par décret en Conseil d'Etat conformément aux travaux reconnus par la communauté scientifique internationale peut obtenir réparation intégrale de son préjudice dans les conditions prévues par la présente loi. Si la personne est décédée, la demande de réparation peut être présentée par ses ayants droit ". L'article 2 de cette même loi définit les conditions de temps et de lieu de séjour ou de résidence que le demandeur doit remplir. Il résulte de ces dispositions, éclairées par les travaux préparatoires de la loi, que le législateur, prenant acte de ce que la mise en oeuvre des différents régimes de responsabilité n'avait pas permis d'assurer une indemnisation satisfaisante des victimes des essais nucléaires français, a entendu faciliter l'indemnisation des personnes souffrant d'une maladie radio-induite en raison de leur exposition aux rayonnements ionisants dus aux essais nucléaires français. <br/>
<br/>
              La loi du 5 janvier 2010 a chargé le Comité d'indemnisation des victimes des essais nucléaires (CIVEN), qui a le statut d'autorité administrative indépendante depuis la loi n° 2013-1168 du 18 décembre 2013, d'instruire les demandes d'indemnisation reçues au titre de la loi. En vertu du V de l'article 4 de la loi modifiée : " Ce comité examine si les conditions de l'indemnisation sont réunies. Lorsqu'elles le sont, l'intéressé bénéficie d'une présomption de causalité à moins qu'au regard de la nature de la maladie et des conditions de son exposition le risque attribuable aux essais nucléaires puisse être considéré comme négligeable. Le comité le justifie auprès de l'intéressé (...) ". Le III du même article dispose que : " Les crédits nécessaires à l'accomplissement des missions du comité d'indemnisation des victimes des essais nucléaires sont inscrits au budget des services généraux du Premier ministre ".<br/>
<br/>
              L'article 6 de la loi du 5 janvier 2010 précise que : " L'acceptation de l'offre d'indemnisation vaut transaction au sens de l'article 2044 du code civil et désistement de toute action juridictionnelle en cours. Elle rend irrecevable toute autre action juridictionnelle visant à la réparation des mêmes préjudices ". L'étude d'impact du projet de loi indique que : " La création d'un tel dispositif devrait induire une réduction du nombre des contentieux, notamment les recours en responsabilité dirigés contre l'Etat ". <br/>
<br/>
              Il résulte de l'ensemble de ces éléments qu'en confiant au CIVEN la mission d'indemniser, selon une procédure amiable exclusive de toute recherche de responsabilité, les dommages subis par les victimes de ces essais, le législateur a institué un dispositif assurant l'indemnisation des victimes concernées au titre de la solidarité nationale.<br/>
<br/>
              3. Le contentieux relatif à la mise en oeuvre de ce régime d'indemnisation relève exclusivement du plein contentieux.<br/>
<br/>
              4. Le législateur a chargé le CIVEN, qui a le statut d'autorité administrative indépendante depuis la loi n° 2013-1168 du 18 décembre 2013, d'instruire les demandes d'indemnisation reçues au titre de la loi. En vertu du V de l'article 4 de la loi modifiée : " Ce comité examine si les conditions de l'indemnisation sont réunies. Lorsqu'elles le sont, l'intéressé bénéficie d'une présomption de causalité à moins qu'au regard de la nature de la maladie et des conditions de son exposition le risque attribuable aux essais nucléaires puisse être considéré comme négligeable. Le comité le justifie auprès de l'intéressé (...) ". L'indemnisation qui incombe sous certaines conditions au CIVEN, en vertu des dispositions de la loi du 5 janvier 2010 modifiée, a pour objet d'assurer, au titre de la solidarité nationale, la réparation du dommage subi par les victimes des essais nucléaires français, et non de reconnaître que l'Etat, représenté par le CIVEN, aurait la qualité d' " auteur responsable " ou de " tiers responsable " des dommages. Par suite, les recours des tiers payeurs ayant versé des prestations à la victime d'un dommage corporel, organisés par l'article L. 376-1 du code de la sécurité sociale s'agissant des caisses de sécurité sociale et par la délibération du 14 février 1974 de l'assemblée territoriale de la Polynésie française s'agissant des organismes de sécurité sociale de cette collectivité, ne peuvent être exercés devant le CIVEN sur le fondement de la loi du 5 janvier 2010. <br/>
<br/>
<br/>
<br/>Le présent avis sera notifié à la cour administrative d'appel de Paris, à la caisse de prévoyance sociale de la Polynésie française et au ministre de la défense. Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-05-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RECOURS OUVERTS AUX DÉBITEURS DE L'INDEMNITÉ, AUX ASSUREURS DE LA VICTIME ET AUX CAISSES DE SÉCURITÉ SOCIALE. SUBROGATION. - INDEMNISATION DES VICTIMES DES ESSAIS NUCLÉAIRES - RECOURS SUBROGATOIRE DES CAISSES DE SÉCURITÉ SOCIALE (ART. L. 376-1 DU CSS) - RECOURS SUBROGATOIRE DEVANT LE CIVEN - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 60-05-03 L'indemnisation qui incombe sous certaines conditions au comité d'indemnisation des victimes des essais nucléaires (CIVEN), en vertu des dispositions de la loi n° 2010-2 du 5 janvier 2010, a pour objet d'assurer, au titre de la solidarité nationale, la réparation du dommage subi par les victimes des essais nucléaires français, et non de reconnaître que l'Etat, représenté par le CIVEN, aurait la qualité d'auteur responsable des dommages.... ,,Par suite, les recours des tiers payeurs ayant versé des prestations à la victime d'un dommage corporel, organisés par l'article L. 376-1 du code de la sécurité sociale s'agissant des caisses de sécurité sociale et par la délibération du 14 février 1974 de l'assemblée territoriale de la Polynésie française s'agissant des organismes de sécurité sociale de cette collectivité, qui s'exercent à l'encontre des auteurs responsables du dommage dont souffre la victime, ne peuvent être exercés devant le CIVEN sur le fondement de la loi du 5 janvier 2010.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Rappr. CE, 17 février 2016, Caisse primaire d'assurance maladie de l'Artois, n° 384349, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
