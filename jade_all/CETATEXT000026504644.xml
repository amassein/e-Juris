<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026504644</ID>
<ANCIEN_ID>JG_L_2012_10_000000354354</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/50/46/CETATEXT000026504644.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 17/10/2012, 354354</TITRE>
<DATE_DEC>2012-10-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354354</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Camille Pascal</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:354354.20121017</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu, la requête, enregistrée le 25 novembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentée par l'Association de défense des fonctionnaires de l'Etat PTT (ADFE-PTT), dont le siège est au 25 bis rue de Metz au Perreux-sur-Marne (94170) ; l'ADFE-PTT demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet, née du silence gardé par le Premier ministre sur sa demande tendant à ce que soient prises les mesures réglementaires nécessaires pour créer des commissions administratives paritaires et comités techniques paritaires pour les fonctionnaires de l'Etat relevant des corps dont elle défend les intérêts ; <br/>
<br/>
              2°) d'enjoindre au Premier ministre de prendre ces mesures dans un délai d'un mois à compter de la notification de la décision à intervenir, sous astreinte de 150 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 5 octobre 2012, présentée par l'ADFE-PTT ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu la loi n° 90-568 du 2 juillet 1990 ;<br/>
<br/>
              Vu la loi n° 96-660 du 26 juillet 1996 ;<br/>
<br/>
              Vu la loi n° 2003-1365 du 31 décembre 2003 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Camille Pascal, Conseiller d'Etat, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, Rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que la loi du 2 juillet 1990 relative à l'organisation du service public de la poste et des télécommunications a créé, aux termes de son article 1er, les exploitants publics La Poste et France Télécom ; que selon l'article 29 de cette loi : " Les personnels de La Poste et de France Télécom sont régis par des statuts particuliers, pris en application de la loi n° 83-634 du 13 juillet 1983 portant droit et obligations des fonctionnaires et de la loi n° 84-16 du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, qui comportent des dispositions spécifiques dans les conditions prévues aux alinéas ci-après, ainsi que dans les conditions de l'article 29-1 " ; qu'en vertu de l'article 44 de la même loi, les fonctionnaires en activité affectés au 31 décembre 1990 dans des emplois relevant de la direction générale de la poste ou de la direction générale des télécommunications ont été placés de plein droit respectivement sous l'autorité du président du conseil d'administration de la Poste ou celui de France Télécom à compter du 1er janvier 1991, sans changement de leur position statutaire ;<br/>
<br/>
              2. Considérant que les corps de fonctionnaires dont l'association requérante soutient qu'ils auraient été privés de certaines des garanties applicables aux fonctionnaires de l'Etat sont d'anciens corps de fonctionnaires du ministère chargé de la poste et des télécommunications antérieurement à la réforme effectuée par la loi du 2 juillet 1990 ; que les statuts de ces corps ont été remplacés, à partir de l'entrée en vigueur de la loi du 2 juillet 1990, par ceux des corps dits " de reclassement " rattachés aux deux exploitants publics ; que les fonctionnaires des anciens corps du ministère de la poste et des télécommunications ont été intégrés d'office dans les corps de reclassement par l'effet des dispositions des statuts particuliers de ces corps ; <br/>
<br/>
              3. Considérant, d'une part, que si l'association requérante soutient que les fonctionnaires des anciens corps ne pouvaient être intégrés d'office dans les corps de reclassement sans demande de leur part, les fonctionnaires ne peuvent invoquer aucun droit acquis au maintien de leur statut, lequel peut être modifié à tout moment, dans le respect des dispositions législatives en vigueur ; que les dispositions transitoires de l'article 44 de la loi du 2 juillet 1990 n'interdisaient pas que soient modifiées les règles statutaires applicables aux corps de fonctionnaires auxquels ces personnels continuaient à appartenir après le 1er janvier 1991 ;<br/>
<br/>
              4. Considérant, d'autre part, que l'illégalité d'un acte administratif, qu'il soit ou non réglementaire, ne peut être utilement invoquée à l'appui de conclusions dirigées contre une décision administrative que si cette dernière a été prise pour son application ou s'il en constitue la base légale ; que la décision implicite résultant du silence gardé, en 2011, sur la demande de l'association requérante tendant à ce que soient prises des mesures réglementaires en vue de créer des commissions administratives paritaires ou des comités techniques paritaires pour les fonctionnaires des anciens corps du ministère de la poste et des télécommunications n'a pas été prise pour l'application des décrets qui ont, à partir du 1er janvier 1991, fixé les statuts particuliers des corps de reclassement, lesquels ne constituent pas davantage la base légale de la décision contestée ; que, par suite, l'association requérante ne peut pas utilement soutenir que ces décrets seraient illégaux à l'appui de ses conclusions dirigées contre la décision implicite résultant du silence gardé sur sa demande ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que l'association requérante n'est pas fondée à demander l'annulation pour excès de pouvoir de la décision qu'elle attaque ; que ses conclusions à fin d'injonction ainsi que celles tendant à l'application de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de l'Association de défense des fonctionnaires de l'Etat PTT est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à l'Association de défense des fonctionnaires de l'Etat PTT, au Premier ministre et au ministre du redressement productif.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-13-01 FONCTIONNAIRES ET AGENTS PUBLICS. CONTENTIEUX DE LA FONCTION PUBLIQUE. CONTENTIEUX DE L'ANNULATION. - DEMANDE D'ANNULATION DE LA DÉCISION IMPLICITE DE REFUS DE PRENDRE DES MESURES RÉGLEMENTAIRES EN VUE DE CRÉER DES CAP OU DES CTP POUR LES FONCTIONNAIRES DES ANCIENS CORPS DU MINISTÈRE DE LA POSTE ET DES TÉLÉCOMMUNICATIONS - MOYEN TIRÉ DE L'ILLÉGALITÉ DES DÉCRETS QUI ONT, À PARTIR DU 1ER JANVIER 1991, FIXÉ LES STATUTS PARTICULIERS DES CORPS DE RECLASSEMENT - INOPÉRANCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-04-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS INOPÉRANTS. - MOYEN TIRÉ DE L'ILLÉGALITÉ DES DÉCRETS AYANT, À PARTIR DU 1ER JANVIER 1991, FIXÉ LES STATUTS PARTICULIERS DES CORPS DE RECLASSEMENT, SOULEVÉ À L'ENCONTRE DE LA DÉCISION IMPLICITE DE REFUS DE PRENDRE DES MESURES RÉGLEMENTAIRES EN VUE DE CRÉER DES CAP OU DES CTP POUR LES FONCTIONNAIRES DES ANCIENS CORPS DES POSTES ET TÉLÉCOMMUNICATIONS [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-01-04-04-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. EXCEPTION D'ILLÉGALITÉ. - DEMANDE D'ANNULATION DE LA DÉCISION IMPLICITE DE REFUS DE PRENDRE DES MESURES RÉGLEMENTAIRES EN VUE DE CRÉER DES CAP OU DES CTP POUR LES FONCTIONNAIRES DES ANCIENS CORPS DU MINISTÈRE DE LA POSTE ET DES TÉLÉCOMMUNICATIONS - MOYEN TIRÉ DE L'ILLÉGALITÉ DES DÉCRETS QUI ONT, À PARTIR DU 1ER JANVIER 1991, FIXÉ LES STATUTS PARTICULIERS DES CORPS DE RECLASSEMENT [RJ1].
</SCT>
<ANA ID="9A"> 36-13-01 La décision implicite résultant du silence gardé par l'administration sur une demande tendant à ce que soient prises des mesures réglementaires en vue de créer des commissions administratives paritaires (CAP) ou des comités techniques paritaires (CTP) pour les fonctionnaires des anciens corps du ministère de la poste et des télécommunications n'a pas été prise pour l'application des décrets qui ont, à partir du 1er janvier 1991, fixé les statuts particuliers des corps de reclassement, lesquels ne constituent pas davantage la base légale de cette décision. Par suite, inopérance de l'exception d'illégalité de ces décrets à l'appui de conclusions dirigées contre la décision implicite résultant du silence gardé sur cette demande.</ANA>
<ANA ID="9B"> 54-07-01-04-03 La décision implicite résultant du silence gardé par l'administration sur une demande tendant à ce que soient prises des mesures réglementaires en vue de créer des commissions administratives paritaires (CAP) ou des comités techniques paritaires (CTP) pour les fonctionnaires des anciens corps du ministère de la poste et des télécommunications n'a pas été prise pour l'application des décrets qui ont, à partir du 1er janvier 1991, fixé les statuts particuliers des corps de reclassement, lesquels ne constituent pas davantage la base légale de cette décision. Par suite, inopérance de l'exception d'illégalité de ces décrets à l'appui de conclusions dirigées contre la décision implicite résultant du silence gardé sur cette demande.</ANA>
<ANA ID="9C"> 54-07-01-04-04-03 La décision implicite résultant du silence gardé par l'administration sur une demande tendant à ce que soient prises des mesures réglementaires en vue de créer des commissions administratives paritaires (CAP) ou des comités techniques paritaires (CTP) pour les fonctionnaires des anciens corps du ministère de la poste et des télécommunications n'a pas été prise pour l'application des décrets qui ont, à partir du 1er janvier 1991, fixé les statuts particuliers des corps de reclassement, lesquels ne constituent pas davantage la base légale de cette décision. Par suite, inopérance de l'exception d'illégalité de ces décrets à l'appui de conclusions dirigées contre la décision implicite résultant du silence gardé sur cette demande.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur l'affirmation du principe suivant lequel l'illégalité d'un acte administratif, qu'il soit ou non réglementaire, ne peut être utilement invoquée à l'appui de conclusions dirigées contre une décision administrative que si cette dernière a été prise pour son application ou s'il en constitue la base légale, CE, Section, 11 juillet 2011, Société d'équipement du département de Maine-et-Loire Sodemel et ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration, n°s 320735 320854, p. 346.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
