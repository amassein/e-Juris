<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032189039</ID>
<ANCIEN_ID>JG_L_2016_03_000000394395</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/18/90/CETATEXT000032189039.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème SSJS, 09/03/2016, 394395, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-03-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394395</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Romain Victor</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:394395.20160309</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
<br/>
              Mme K...L..., M. I...J..., Mme O... E...et M. N...H...ont demandé au tribunal administratif de Lille d'annuler les opérations électorales qui ont eu lieu les 22 et 29 mars 2015 pour l'élection des conseillers départementaux du canton d'Arras-1. Par un jugement du 5 octobre 2015, le tribunal administratif de Lille a fait droit à leur protestation et annulé les opérations électorales litigieuses.<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 4 novembre 2015 et 19 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, Mme D...M..., M. C...B..., Mme A...F...et M. N... G...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de rejeter la protestation de Mme L...et autres ;<br/>
<br/>
              3°) de mettre à la charge de Mme L...la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Romain Victor, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il résulte de l'instruction qu'à l'issue du second tour des opérations électorales qui se sont déroulées le 29 mars 2015 pour l'élection des conseillers départementaux du canton d'Arras-1 (Pas-de-Calais), Mme M...et M. B...ont été proclamés élus par 5 374 voix contre 5 370 voix pour Mme L...et M.J... ; que MmeM..., M.B..., Mme F...et M. G...relèvent appel du jugement du 5 octobre 2015 par lequel le tribunal administratif de Lille a, sur la protestation de Mme L..., de M.J..., de Mme E...et de M.H..., annulé ces opérations électorales aux motifs que le résultat de l'élection contestée ne pouvait être déterminé avec certitude, compte tenu du nombre de suffrages irrégulièrement émis, et que l'envoi d'un matériel de propagande erroné à certains électeurs de la commune de Roclincourt, ainsi que l'absence de diffusion des documents de propagande électorale à d'autres électeurs, avaient été de nature à altérer la sincérité du scrutin ;<br/>
<br/>
              2.  Considérant, d'une part, qu'aux termes de l'article R. 66-2 du code électoral : " Sont nuls et n'entrent pas en compte dans le résultat du dépouillement : (...) 2° Les bulletins établis au nom d'un candidat, d'un binôme de candidats ou d'une liste dont la candidature n'a pas été enregistrée ; (...) " ; qu'il résulte de l'instruction que la commission de propagande a adressé par erreur à certains électeurs de la commune de Roclincourt des documents de propagande électorale relatifs à l'élection des conseillers départementaux du canton d'Arras-2, en lieu et place de ceux du canton d'Arras-1 ; que cinq bulletins de vote d'électeurs de cette commune correspondant à des bulletins du canton d'Arras-2 ont été déclarés nuls, en application du 2° de l'article R. 66-2 du code électoral ; que s'il n'est nullement établi que l'envoi d'un matériel de propagande erroné résulterait d'une manoeuvre, l'irrégularité constatée a été, compte tenu du nombre de bulletins litigieux déclarés nuls et de l'écart de quatre voix entre les candidats arrivés en tête du scrutin, de nature à altérer la sincérité du résultat ;<br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes de l'article R. 34 du code électoral : " La commission de propagande reçoit du préfet le matériel nécessaire à l'expédition des circulaires et bulletins de vote et fait préparer les libellés d'envoi. Elle est chargée : - d'adresser, au plus tard le mercredi précédant le premier tour de scrutin et, en cas de ballottage, le jeudi précédant le second tour, à tous les électeurs de la circonscription, une circulaire et un bulletin de vote de chaque candidat, binôme de candidats ou liste ; (...) " ; qu'il résulte de l'instruction que, du fait d'un mouvement de grève dans les services postaux, plusieurs électeurs de six communes du canton n'ont pas reçu les circulaires et bulletins de vote avant le début des opérations électorales ; que, compte tenu du faible écart de voix entre les candidats arrivés en tête du scrutin, et même en l'absence de manoeuvre, l'irrégularité constatée a été, dans les circonstances de l'espèce, de nature à altérer la sincérité du résultat ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner l'autre moyen de leur requête, que Mme M...et autres ne sont pas fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Lille a annulé les opérations électorales qui se sont déroulées les 22 et 29 mars 2015 pour l'élection des conseillers départementaux du canton d'Arras-1 ;<br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de Mme L..., qui n'est pas la partie perdante dans la présente instance ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au titre de ces mêmes dispositions par MmeL... ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de MmeM..., M.B..., Mme F...et M. G...est rejetée.<br/>
Article 2 : Les conclusions de Mme L...présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme D...M..., à M. C...B..., à Mme A...F..., à M. N...G..., à Mme K...L..., premier défendeur dénommé et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
