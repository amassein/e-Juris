<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027288059</ID>
<ANCIEN_ID>JG_L_2013_04_000000360669</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/28/80/CETATEXT000027288059.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 08/04/2013, 360669, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-04-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360669</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD ; SCP BLANC, ROUSSEAU</AVOCATS>
<RAPPORTEUR>Mme Julia Beurton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:360669.20130408</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 2 et 13 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Morne-à-l'Eau (97111), représentée par son maire ; la commune requérante demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1200353 du 14 mai 2012 par laquelle le juge des référés du tribunal administratif de Basse-Terre, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a rejeté sa demande tendant à la suspension de l'avis du 1er mars 2012 par lequel le conseil de discipline de recours placé auprès du centre de gestion de la fonction publique territoriale de la Guadeloupe a préconisé à l'encontre de M. B...A...une sanction d'exclusion d'un an, assortie d'un sursis de six mois ; <br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande de suspension ; <br/>
<br/>
              3°) de mettre à la charge du centre de gestion de la fonction publique territoriale de la Guadeloupe le versement de la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ; <br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984 ;<br/>
<br/>
              Vu le décret n° 89-677 du 18 septembre 1989 ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Julia Beurton, Auditeur,  <br/>
<br/>
              - les observations de la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune de Morne-à-l'Eau, de la SCP Blanc, Rousseau, avocat de M. A...et de la SCP Barthélemy, Matuchansky, Vexliard, avocat du centre départemental de gestion de la fonction publique territoriale de la Guadeloupe,<br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune de Morne-à-l'Eau, à la SCP Blanc, Rousseau, avocat de M. A... et à la SCP Barthélemy, Matuchansky, Vexliard, avocat du centre départemental de gestion de la fonction publique territoriale de la Guadeloupe ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Basse-Terre que, par un arrêté du 18 octobre 2011, le maire de Morne-à-l'Eau a décidé de prononcer, pour motif disciplinaire, la mise à la retraite d'office de M. A... ; que, saisi par ce denier, le conseil de discipline de recours, dans sa séance du 1er mars 2012, a été d'avis que soit substituée à la sanction prononcée une sanction d'exclusion temporaire d'un an, assortie d'un sursis de six mois ; que la commune de Morne-à-l'Eau se pourvoit en cassation contre l'ordonnance du 14 mai 2012 par laquelle le juge des référés du tribunal administratif de Basse-Terre a rejeté sa demande de suspension de l'exécution de cet avis ;  <br/>
<br/>
              Sur la régularité de l'ordonnance attaquée :<br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article R. 742-2 du code de justice administrative, applicable aux ordonnances du juge des référés statuant en urgence en vertu de l'article R. 522-1 du même code : " Les ordonnances mentionnent (...) les visas des dispositions législatives et réglementaires dont elles font application " ;<br/>
<br/>
              3. Considérant que la commune requérante avait notamment soulevé devant le juge des référés du tribunal administratif de Basse-Terre un moyen tiré de l'insuffisante motivation de l'avis attaqué, en violation de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public ; que, toutefois, la loi du 11 juillet 1979, qui impose la motivation de certaines décisions administratives individuelles, n'est pas applicable à l'avis rendu par un conseil de discipline de recours ; que si la commune invoquait également la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires et le décret du 18 septembre 1989 relatif à la procédure disciplinaire applicable aux fonctionnaires territoriaux à l'appui de ses moyens tirés de l'erreur manifeste d'appréciation dans le choix de la sanction et de l'impossibilité de réintégrer M. A... du fait de sa mise à la retraite et de la liquidation de sa pension, le juge des référés n'a pas eu à faire application de ces textes pour apprécier le bien-fondé de ces moyens ; que, par suite, la commune requérante n'est pas fondée à soutenir que l'ordonnance qu'elle attaque serait irrégulière faute de mentionner les lois des 11 juillet 1979 et 13 juillet 1983 et le décret du 18 septembre 1989 dans l'analyse des mémoires échangés par les parties, parmi les textes visés ou dans les motifs de son ordonnance ;<br/>
<br/>
              Sur le bien-fondé de l'ordonnance attaquée :<br/>
<br/>
              4. Considérant qu'aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              5. Considérant que l'article 89 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale répartit les sanctions disciplinaires en quatre groupes, dont le troisième comprend les sanctions de rétrogradation et d'exclusion temporaire de fonctions pour une durée de seize jours à deux ans et le quatrième les sanctions de mise à la retraite d'office et de révocation ; qu'aux termes de l'article 91 de la même loi : " Les fonctionnaires qui ont fait l'objet d'une sanction des deuxième, troisième et quatrième groupes peuvent introduire un recours auprès du conseil de discipline départemental ou interdépartemental (...) / L'autorité territoriale ne peut prononcer de sanction plus sévère que celle proposée par le conseil de discipline de recours " ; <br/>
<br/>
              6. Considérant, en premier lieu, que la circonstance qu'un agent mis à la retraite d'office ait déjà été admis à la retraite et que sa pension ait déjà été liquidée au moment où le conseil de discipline de recours se prononce ne fait pas obstacle à ce que ce dernier propose une sanction moins sévère ; que, par suite, la commune requérante n'est pas fondée à soutenir que le juge des référés aurait commis une erreur de droit en estimant que le moyen tiré de l'impossibilité de réintégrer M. A...compte tenu de sa mise à la retraite à compter du 1er novembre 2011 et de la liquidation de sa pension n'était pas de nature à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de l'avis litigieux ;<br/>
<br/>
              7. Considérant, en second lieu, que c'est au terme d'une appréciation souveraine non entachée de dénaturation que le juge des référés a estimé que le moyen tiré de l'erreur manifeste d'appréciation dans le choix de la sanction n'était pas de nature à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de l'avis ;  <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que les conclusions de la commune de Morne-à-l'Eau aux fins d'annulation doivent être rejetées ;<br/>
<br/>
              9. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font, en tout état de cause, obstacle à ce que soit mise à la charge de l'Etat la somme que la commune de Morne-à-l'Eau demande sur ce fondement ; qu'elles font également obstacle à ce que soit mise à la charge de cette commune la somme que demande au même titre le centre de gestion de la fonction publique territoriale de Guadeloupe, qui a d'ailleurs demandé à être mis hors de cause et doit être regardé comme n'ayant été appelé en la cause que pour produire des observations ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la commune de Morne-à-l'Eau le versement à M. A...de la somme de 3 000 euros au titre des mêmes dispositions ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la commune de Morne-à-l'Eau est rejeté.<br/>
Article 2 : La commune de Morne-à-l'Eau versera à M. A...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Les conclusions du centre de gestion de la fonction publique territoriale de Guadeloupe présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la commune de Morne-à-l'Eau et à M. B...A....<br/>
Copie en sera adressée au centre de gestion de la fonction publique territoriale de Guadeloupe.<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
