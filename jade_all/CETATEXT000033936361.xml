<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033936361</ID>
<ANCIEN_ID>JG_L_2017_01_000000396178</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/93/63/CETATEXT000033936361.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 27/01/2017, 396178, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-01-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396178</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>Mme Marie-Anne Lévêque</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:396178.20170127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Melun d'annuler la décision du 15 décembre 2011 par laquelle le ministre de la défense a refusé de faire droit à son recours administratif du 26 octobre 2011 tendant au rétablissement de sa rémunération à son niveau antérieur. Par un jugement n° 1201579/2 du 31 octobre 2013, le tribunal administratif de Melun a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13PA04700 du 17 novembre 2015, la cour administrative d'appel de Paris a rejeté l'appel formé par M. B...contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 janvier et 15 avril 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 79-587 du 11 juillet 1979 ;<br/>
              - le décret n° 51-582 du 22 mai 1951 ;<br/>
              - l'arrêté du 8 février 2007 fixant le régime de maintien de la rémunération du personnel à statut ouvrier du ministère de la défense muté dans le cadre des restructurations ;<br/>
              - l'arrêté du 28 novembre 2008 fixant le régime de rémunération des personnels ouvriers de l'Etat mensualisés du ministère de la défense ;<br/>
              - l'instruction du 26 juillet 2002 relative à la durée du travail effectif des ouvriers de l'Etat du ministère de la défense ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Anne Lévêque, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public. <br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M.B....<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.B..., ouvrier de l'Etat, recruté au ministère de la défense en qualité de conducteur de véhicules par un contrat conclu le 9 octobre 1980, a été muté, à la suite de la restructuration du centre automobile de la défense du site de Maisons-Laffitte où il était affecté, au service parisien de soutien de l'administration centrale à compter du 28 juin 2010 ; qu'il a signé, le 6 juillet 2010, une convention de mobilité prévoyant le maintien du bénéfice de la rémunération de 33,83 heures supplémentaires au taux de 125 % et de 31,33 heures supplémentaires au taux de 150 %, soit de 65,16 heures supplémentaires mensuelles, dont il avait bénéficié pour la période de mai 2009 à avril 2010 ; qu'ayant constaté une diminution de sa rémunération en septembre 2011, il a saisi le ministre de la défense d'un recours administratif tendant au rétablissement de celle-ci ; que le ministre a rejeté sa demande par une décision du 15 décembre 2011 ; que la cour administrative d'appel de Paris a rejeté l'appel formé par M. B...contre le jugement du 31 octobre 2013 par lequel le tribunal administratif de Melun a rejeté sa demande tendant à l'annulation de la décision du ministre de la défense ; <br/>
<br/>
              2. Considérant qu'il résulte de la combinaison des dispositions du deuxième alinéa de l'article R. 811-1 du code de justice administrative et de celles de l'article R. 222-13 du même code, dans leur rédaction en vigueur à la date du jugement attaqué, que le tribunal administratif statue en premier et dernier ressort dans les litiges relatifs à la situation individuelle des agents publics, à l'exception de ceux qui concernent l'entrée au service, la discipline ou la sortie du service, sauf pour les recours comportant des conclusions tendant au versement ou à la décharge de sommes d'un montant supérieur au montant déterminé par les articles R. 222-14 et R. 222-15 de ce code ; que l'article R. 222-14 fixe ce montant à 10 000 euros ; que l'article R. 222-15 précise que le montant à comparer à ce seuil est déterminé par la valeur totale des sommes demandées dans la requête introductive d'instance ;<br/>
<br/>
              3. Considérant que la demande présentée par M. B...devant le tribunal administratif de Melun n'était pas relative à l'entrée au service, à la discipline ou à la sortie du service et ne tendait pas au versement d'une somme excédant 10 000 euros ; qu'il suit de là, d'une part, que le jugement du 31 octobre 2013 n'était pas susceptible d'appel et, d'autre part, que les conclusions tendant à son annulation, présentées devant la cour administrative d'appel de Paris, revêtaient le caractère d'un pourvoi en cassation ; qu'en rejetant les conclusions dont M. B... l'avait saisie, alors qu'il lui appartenait de les renvoyer au Conseil d'Etat, la cour administrative d'appel de Paris a méconnu les règles régissant sa compétence ; que, dès lors, son arrêt doit être annulé ; <br/>
<br/>
              4. Considérant qu'il appartient au Conseil d'Etat de statuer, en tant que juge de cassation, sur les conclusions présentées par M. B...tendant à l'annulation du jugement du 31 octobre 2013 du tribunal administratif de Melun ;<br/>
<br/>
              5. Considérant, d'une part, qu'aux termes de l'article 1er de l'arrêté interministériel du 8 février 2007 fixant le régime de maintien de la rémunération du personnel à statut ouvrier du ministère de la défense muté dans le cadre des restructurations : " Tout ouvrier de l'Etat qui fait l'objet d'une mutation dans le cadre des restructurations conserve son groupe de rémunération (...) " ; qu'aux termes de l'article 3 de cet arrêté : " Le régime des heures supplémentaires effectuées au cours de l'année qui précède la mutation est fixé ainsi qu'il suit : les heures qui correspondent à des heures exceptionnelles dues à une charge ou à une organisation de travail inhabituelle n'ont pas à être prises en compte dans la conservation de la rémunération ; - les heures qui correspondent à la charge de travail normale dans l'emploi occupé par l'ouvrier avant sa mutation sont conservées. Le nombre d'heures maintenu est déterminé par la moyenne annuelle des heures supplémentaires effectuées par l'ouvrier pendant l'année qui précède sa mutation. De nouvelles heures supplémentaires ne peuvent être versées que lorsque les heures réellement effectuées excèdent le nombre d'heures correspondant à celles ainsi rémunérées " ; qu'il résulte de l'article 7 de ce même arrêté que la rémunération ainsi déterminée figure dans un " contrat de mobilité " ; que ces dispositions ont pour objet d'instituer en faveur des ouvriers de l'Etat mutés dans le cadre d'une restructuration une garantie de maintien de la rémunération des heures supplémentaires, correspondant à la charge de travail normale de l'agent, perçue durant l'année précédant la mutation, indépendamment du nombre d'heures supplémentaires effectuées par les intéressés après cette mutation ; que le " contrat de mobilité " fixant l'avantage financier ainsi déterminé crée des droits au profit des ouvriers de l'Etat qui en bénéficient ; <br/>
<br/>
              6. Considérant que, sous réserve de dispositions législatives ou réglementaires contraires, et hors le cas où il est satisfait à une demande du bénéficiaire, l'administration ne peut retirer ou abroger une décision expresse individuelle créatrice de droits que dans le délai de quatre mois suivant l'intervention de cette décision et si elle est illégale ; qu'une décision administrative explicite accordant un avantage financier crée des droits au profit de son bénéficiaire alors même que l'administration avait l'obligation de refuser cet avantage ; <br/>
<br/>
              7. Considérant que le tribunal administratif de Melun a estimé qu'une partie des heures supplémentaires dont le contrat de mobilité de M. B...prévoyait que la rémunération serait conservée avait été effectuée en méconnaissance de la durée maximale du travail, fixée à quarante-huit heures au cours d'une même semaine ou quarante-quatre heures en moyenne sur une période quelconque de douze semaines consécutives par l'article 1er de l'arrêté interministériel du 28 novembre 2008 fixant le régime de rémunération des personnels ouvriers de l'Etat mensualisés du ministère de la défense et les dispositions de l'instruction du 26 juillet 2002 relative à la durée du travail effectif des ouvriers de l'Etat du ministère de la défense ; qu'il en a déduit que M. B...ne pouvait se prévaloir d'aucun droit acquis au maintien de cette rémunération, en relevant que le caractère créateur de droits de l'attribution d'un avantage financier ne fait pas obstacle à ce que la décision soit abrogée si l'intéressé ne remplit plus les conditions auxquelles cet avantage est subordonné ou si l'administration modifie l'appréciation qui avait justifié son attribution ; que, toutefois, s'il appartient à l'autorité compétente de cesser d'attribuer un avantage financier donnant lieu à des versements réguliers lorsque son maintien est subordonné à des conditions qui doivent être régulièrement vérifiées et qu'elle constate que celles-ci ne sont plus remplies, tel n'est pas le cas de la décision de garantir à un agent, dans le cadre d'un contrat de mobilité, qu'il conservera son niveau de rémunération antérieur, décision dont le caractère créateur de droits exclut qu'elle puisse être remise en cause au-delà du délai de quatre mois en raison de son illégalité ; qu'ainsi, à supposer même que le " contrat de mobilité "  de M. B...doive être regardé comme entaché d'illégalité en tant qu'il prévoit le maintien de la rémunération d'heures supplémentaires effectuées en méconnaissance de l'arrêté et de l'instruction précités, la garantie de rémunération qu'il institue ne pouvait être remise en cause, ainsi qu'il a été dit au point 6, que dans le délai de quatre mois à compter de son entrée en vigueur ; que, par suite, M. B...est fondé à soutenir que le tribunal administratif de Melun a commis une erreur de droit ; <br/>
<br/>
              8. Considérant que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, le jugement du tribunal administratif de Melun doit être annulé ; <br/>
<br/>
              9. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 17 novembre 2015 est annulé. <br/>
Article 2 : Le jugement du tribunal administratif de Melun du 31 octobre 2013 est annulé. <br/>
Article 3 : L'affaire est renvoyée au tribunal administratif de Melun. <br/>
Article 4 : L'Etat versera à M. B...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à M. A...B...et au ministre de la défense.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
