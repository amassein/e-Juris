<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033163036</ID>
<ANCIEN_ID>JG_L_2016_09_000000377190</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/16/30/CETATEXT000033163036.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 28/09/2016, 377190</TITRE>
<DATE_DEC>2016-09-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>377190</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:377190.20160928</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. E...C...a demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir l'arrêté du 22 octobre 2007 par lequel le garde des sceaux, ministre de la justice, a nommé M. F...B..., notaire en résidence à Paris 14ème, en remplacement de M. D...A.... Par un jugement n° 0720629/6-2 du 23 juillet 2010, le tribunal administratif a rejeté la demande de M.C....<br/>
<br/>
              Par un arrêt n° 10PA04744 du 26 mars 2012, la cour administrative d'appel de Paris a annulé le jugement du tribunal administratif et rejeté la demande présentée par M. C...devant ce tribunal.<br/>
<br/>
              Par une décision n° 359707 du 26 avril 2013, le Conseil d'État statuant au contentieux a annulé l'arrêt de la cour administrative d'appel de Paris en tant qu'il a rejeté la demande de M.C..., et renvoyé l'affaire à cette cour.<br/>
<br/>
              Par un arrêt n° 13PA01697 du 6 février 2014, la cour administrative d'appel de Paris a rejeté la demande de M. C...et les conclusions qu'il a présentées devant elle tendant à ce qu'il soit enjoint au ministre de la justice de lui communiquer les pièces de nature à démontrer que la procédure suivie pour nommer Me B...a été régulière, et notamment le traité de cession conclu entre lui et MeA..., et de l'agréer en qualité de notaire ou, à défaut, de réexaminer sa situation dans un délai d'un mois à compter de l'arrêt à intervenir, sous astreinte de 150 euros par jour de retard.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 7 avril et 2 juillet 2014 au secrétariat du contentieux du Conseil d'État, M. C...demande au Conseil d'État : <br/>
<br/>
              1°) d'annuler cet arrêt de la cour administrative d'appel de Paris ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'État la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - l'ordonnance n° 45-2590 du 2 novembre 1945 ;<br/>
              - le décret n° 73-609 du 5 juillet 1973 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M. C...et à la SCP Piwnica, Molinié, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 44 du décret du 5 juillet 1973 relatif à la formation professionnelle dans le notariat et aux conditions d'accès aux fonctions de notaire, dans sa rédaction applicable au litige : " Les nominations de notaires sont prononcées par arrêté du garde des sceaux, ministre de la justice, dans les conditions définies au présent chapitre " ; que l'article 45 du même décret dispose : " Le candidat à la succession d'un notaire sollicite l'agrément du garde des sceaux, ministre de la justice, dans les formes prévues aux articles suivants " ; que l'article 46 du même décret précise : " La demande de nomination est présentée au procureur de la République près le tribunal de grande instance dans le ressort duquel est situé l'office. Elle est accompagnée de toutes pièces justificatives et notamment des conventions intervenues entre le titulaire de l'office ou ses ayants droit et le candidat. Lorsque ce dernier doit contracter un emprunt, elle est en outre accompagnée du plan de financement prévoyant de manière détaillée les conditions dans lesquelles il entend faire face à ses échéances en fonction de l'ensemble de ses revenus et d'un budget prévisionnel " ; qu'aux termes de l'article 47 du même décret : " Le procureur de la République recueille l'avis motivé de la chambre des notaires sur la moralité et sur les capacités professionnelles de l'intéressé ainsi que sur ses possibilités financières au regard des engagements contractés. La chambre recueille, s'il y a lieu, tous renseignements utiles auprès, notamment, d'une autre chambre ou d'un conseil régional, du centre de formation professionnelle ou de l'école de notariat. Si, quarante-cinq jours après sa saisine par lettre recommandée avec demande d'avis de réception, la chambre n'a pas adressé au procureur de la République l'avis qui lui a été demandé, elle est réputée avoir émis un avis favorable et il est passé outre " ; qu'aux termes de l'article 48 du même décret : " Le procureur de la République transmet le dossier au garde des sceaux, ministre de la justice, avec son avis motivé. Le garde des sceaux, ministre de la justice, demande, le cas échéant, au bureau du conseil supérieur du notariat ou à tout autre organisme professionnel des renseignements sur les activités antérieures du candidat " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'en application des dispositions citées au point précédent, le garde des sceaux, ministre de la justice, a refusé, par une décision du 1er juin 2006, de nommer M. E...C..., notaire à la résidence de Paris, comme successeur de M. D...A...; que, par une décision du 22 octobre 2007, il a nommé M. F...B...en remplacement de M. A...; que par un jugement du tribunal administratif de Paris du 23 juillet 2010, confirmé par un arrêt de la cour administrative d'appel de Paris du 26 mars 2012 devenu irrévocable, la décision du 1er juin 2006 refusant la nomination de M. C...a été annulée ; que, par un autre jugement du 23 juillet 2010, le tribunal administratif de Paris a rejeté la demande de M. C...aux fins d'annulation de la décision du 22 octobre 2007 ; que, par un arrêt du 26 mars 2012, la cour administrative d'appel de Paris, après avoir annulé le jugement précité, a rejeté, dans l'article 2 de son arrêt, la demande de M. C...dirigée contre la nomination de M. B...au motif qu'il aurait été dépourvu d'intérêt à agir contre cette nomination ; que, par une décision du 26 avril 2013, le Conseil d'Etat statuant au contentieux a annulé l'article 2 de l'arrêt précité et renvoyé l'affaire devant la même cour ; que, par un arrêt du 6 février 2014 contre lequel M. C...se pourvoit en cassation, la cour administrative d'appel de Paris a jugé que celui-ci n'était pas fondé à demander l'annulation de la décision de nomination de M. B...;<br/>
<br/>
              3. Considérant qu'en raison des effets qui s'y attachent, l'annulation pour excès de pouvoir d'un acte administratif, qu'il soit ou non réglementaire, emporte, lorsque le juge est saisi de conclusions recevables, l'annulation par voie de conséquence des décisions administratives consécutives qui n'auraient pu légalement être prises en l'absence de l'acte annulé ou qui sont en l'espèce intervenues en raison de l'acte annulé ; qu'il en va ainsi, notamment, des décisions qui ont été prises en application de l'acte annulé et de celles dont l'acte annulé constitue la base légale ; qu'il incombe au juge de l'excès de pouvoir, lorsqu'il est saisi de conclusions recevables dirigées contre de telles décisions consécutives, de prononcer leur annulation par voie de conséquence, le cas échéant en relevant d'office un tel moyen, qui découle de l'autorité absolue de chose jugée qui s'attache à l'annulation du premier acte ;<br/>
<br/>
              4. Considérant que le ministre de la justice n'a pu se prononcer sur la demande de M. B...tendant à être nommé notaire en résidence à Paris en remplacement de M. A...et décider de le nommer qu'après avoir répondu à la demande d'être nommé aux mêmes fonctions présentée antérieurement par M. C...et avoir refusé de nommer ce dernier ; qu'ainsi, la décision de nomination de M. B...n'aurait légalement pu être prise en l'absence de décision du garde des sceaux refusant de nommer M. C...aux mêmes fonctions à la suite de la demande présentée par ce dernier ; que, sauf à priver de sa portée l'annulation de la décision de refus de nomination de M.C..., une telle annulation implique, par voie de conséquence, l'annulation de la décision non définitive de nomination de M. B...; que, dès lors, en retenant que les motifs qui s'attachent à l'annulation de la décision refusant de nommer M.C..., tirés de l'irrégularité de la procédure de consultation suivie, n'impliquaient nullement la délivrance à M. C...de l'agrément du garde des sceaux pour en déduire qu'une telle annulation n'impliquait pas l'annulation de la décision non définitive de nomination de M.B..., la cour a entaché son arrêt d'erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. C...est fondé à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              5. Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; qu'il y a lieu, par suite, de régler l'affaire au fond ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui a été dit au point 4 que, sans qu'il soit besoin d'examiner les autres moyens de la requête, M. C...est fondé à demander l'annulation du jugement du tribunal administratif de Paris en tant qu'il a rejeté ses conclusions tentant à l'annulation de la décision qu'il attaque, ainsi que l'annulation de cette décision ;<br/>
<br/>
              7. Considérant que l'annulation d'un acte administratif implique, en principe, que cet acte est réputé n'être jamais intervenu ; que, toutefois, s'il apparaît que cet effet rétroactif de l'annulation est de nature à emporter des conséquences manifestement excessives en raison tant des effets que cet acte a produits et des situations qui ont pu se constituer lorsqu'il était en vigueur que de l'intérêt général pouvant s'attacher à un maintien temporaire de ses effets, il appartient au juge administratif, après avoir recueilli sur ce point les observations des parties et examiné l'ensemble des moyens, d'ordre public ou invoqués devant lui, pouvant affecter la légalité de l'acte en cause, de prendre en considération, d'une part, les conséquences de la rétroactivité de l'annulation pour les divers intérêts publics ou privés en présence et, d'autre part, les inconvénients que présenterait, au regard du principe de légalité et du droit des justiciables à un recours effectif, une limitation dans le temps des effets de l'annulation ; qu'il lui revient d'apprécier, en rapprochant ces éléments, s'ils peuvent justifier qu'il soit dérogé à titre exceptionnel au principe de l'effet rétroactif des annulations contentieuses et, dans l'affirmative, de prévoir dans sa décision d'annulation que tout ou partie des effets de cet acte antérieur à l'annulation devront être regardés comme définitifs ou même, le cas échéant, que l'annulation ne prendra effet qu'à une date ultérieure qu'il détermine ;<br/>
<br/>
              8. Considérant qu'eu égard, d'une part, à l'intérêt général qui s'attache à l'autorité des actes authentiques auxquels M. B...a concouru en sa qualité de notaire à Paris et à la durée d'exercice de ses fonctions, et, d'autre part, à la nature du motif d'annulation retenu, et alors qu'aucun autre moyen n'est de nature à justifier l'annulation prononcée par le présent arrêt, la disparition rétroactive de la nomination de M. B...porterait une atteinte manifestement excessive au fonctionnement du service public notarial ; que, dès lors, il y a lieu, dans les circonstances de l'espèce, de ne prononcer l'annulation de la nomination de M. B...qu'à l'expiration d'un délai de neuf mois à compter de la date de la présente décision ;<br/>
<br/>
              9. Considérant qu'il résulte des dispositions des articles L. 911-1 et L. 911-2 du code de justice administrative que, lorsque sa décision l'implique nécessairement, il appartient à la juridiction saisie de conclusions en ce sens de prescrire, selon le cas, une mesure d'exécution dans un sens déterminé ou l'intervention, après une nouvelle instruction, d'une nouvelle décision, le cas échéant en assortissant cette injonction d'un délai d'exécution ; que si M. C...demande au Conseil d'État d'enjoindre au ministre de la justice, d'une part, de lui communiquer les pièces de nature à démontrer que la procédure suivie pour nommer M. B...a été régulière et notamment le traité de cession conclu entre lui et Me A...et, d'autre part, de l'agréer en qualité de notaire ou à défaut de réexaminer sa situation dans un délai d'un mois à compter de l'arrêt à intervenir, sous astreinte de 150 euros par jour de retard, l'annulation de la décision de nomination de M. B...n'implique toutefois pas que le ministre de la justice prenne les mesures d'exécution sollicitées ;<br/>
<br/>
              10. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M.C..., qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de l'État la somme de 5 000 euros à verser à M. C..., au titre des frais exposés par lui et non compris dans les dépens, tant en appel qu'en cassation ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 6 février 2014 de la cour administrative d'appel de Paris est annulé.<br/>
<br/>
Article 2 : L'arrêté du 27 octobre 2007 du garde des sceaux, ministre de la justice, est annulé à compter de l'expiration d'un délai de neuf mois à compter de la date de la présente décision.<br/>
<br/>
Article 3 : L'État versera à M. C...une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
      Article 4 : Le surplus des conclusions présentées par M. C...est rejeté.<br/>
<br/>
Article 5 : Les conclusions présentées par M. B...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 6 : La présente décision sera notifiée à M. E...C..., à M. F...B...et au garde des sceaux, ministre de la justice.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-05-06 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - MOTIFS. ANNULATION PAR VOIE DE CONSÉQUENCE. - ANNULATION DE LA NOMINATION NON DÉFINITIVE D'UN NOTAIRE DANS UNE CHARGE NOTARIALE PAR VOIE DE CONSÉQUENCE DE L'ANNULATION DU REFUS DE NOMMER LE CANDIDAT PRÉCÉDANT POUR LA MÊME SUCCESSION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-07-005 PROCÉDURE. JUGEMENTS. EXÉCUTION DES JUGEMENTS. EFFETS D'UNE ANNULATION. - ANNULATION PAR VOIE DE CONSÉQUENCE - ANNULATION DE LA NOMINATION NON DÉFINITIVE D'UN NOTAIRE DANS UNE CHARGE NOTARIALE PAR VOIE DE CONSÉQUENCE DE L'ANNULATION DU REFUS DE NOMMER LE CANDIDAT PRÉCÉDANT POUR LA MÊME SUCCESSION - EXISTENCE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">55-03-05-03 PROFESSIONS, CHARGES ET OFFICES. CONDITIONS D'EXERCICE DES PROFESSIONS. PROFESSIONS S'EXERÇANT DANS LE CADRE D'UNE CHARGE OU D'UN OFFICE. NOTAIRES. - ANNULATION DU REFUS DE NOMMER UNE PERSONNE POUR SUCCÉDER À UN NOTAIRE DANS SA CHARGE - ANNULATION PAR VOIE DE CONSÉQUENCE DE LA NOMINATION NON DÉFINITIVE DU NOTAIRE NOMMÉ POSTÉRIEUREMENT AUX MÊMES FONCTIONS - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 01-05-06 Litige relatif à la nomination de M. B pour remplacer dans sa charge le notaire C après annulation du refus de nommer M. A comme remplaçant.... ,,Le ministre de la justice n'a pu se prononcer sur la demande de M. B tendant à être nommé notaire en résidence à Paris en remplacement de M. C et décider de le nommer qu'après avoir répondu à la demande d'être nommé aux mêmes fonctions présentée antérieurement par M. A et avoir refusé de nommer ce dernier. Ainsi, la décision de nomination de M. B n'aurait légalement pu être prise en l'absence de décision du garde des sceaux refusant de nommer M. A aux mêmes fonctions à la suite de la demande présentée par ce dernier. Sauf à priver de sa portée l'annulation de la décision de refus de nomination de M. A, une telle annulation implique, par voie de conséquence, l'annulation de la décision non définitive de nomination de M. B.</ANA>
<ANA ID="9B"> 54-06-07-005 Litige relatif à la nomination de M. B pour remplacer dans sa charge le notaire C après annulation du refus de nommer M. A comme remplaçant.... ,,Le ministre de la justice n'a pu se prononcer sur la demande de M. B tendant à être nommé notaire en résidence à Paris en remplacement de M. C et décider de le nommer qu'après avoir répondu à la demande d'être nommé aux mêmes fonctions présentée antérieurement par M. A et avoir refusé de nommer ce dernier. Ainsi, la décision de nomination de M. B n'aurait légalement pu être prise en l'absence de décision du garde des sceaux refusant de nommer M. A aux mêmes fonctions à la suite de la demande présentée par ce dernier. Sauf à priver de sa portée l'annulation de la décision de refus de nomination de M. A, une telle annulation implique, par voie de conséquence, l'annulation de la décision non définitive de nomination de M. B.</ANA>
<ANA ID="9C"> 55-03-05-03 Litige relatif à la nomination de M. B pour remplacer dans sa charge le notaire C après annulation du refus de nommer M. A comme remplaçant.... ,,Le ministre de la justice n'a pu se prononcer sur la demande de M. B tendant à être nommé notaire en résidence à Paris en remplacement de M. C et décider de le nommer qu'après avoir répondu à la demande d'être nommé aux mêmes fonctions présentée antérieurement par M. A et avoir refusé de nommer ce dernier. Ainsi, la décision de nomination de M. B n'aurait légalement pu être prise en l'absence de décision du garde des sceaux refusant de nommer M. A aux mêmes fonctions à la suite de la demande présentée par ce dernier. Sauf à priver de sa portée l'annulation de la décision de refus de nomination de M. A, une telle annulation implique, par voie de conséquence, l'annulation de la décision non définitive de nomination de M. B.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur les critères de l'annulation par voie de conséquence, CE, Section, 30 décembre 2013, Mme,, n° 367615, p. 342.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
