<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042671484</ID>
<ANCIEN_ID>JG_L_2020_12_000000445900</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/67/14/CETATEXT000042671484.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 02/12/2020, 445900, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445900</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Autorisation</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:445900.20201202</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et des nouveaux mémoires, enregistrés les 3, 11, 13 et 19 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, Mme A... B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) à titre principal, d'ordonner la suspension de l'exécution du décret n° 2020-1310 du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 en tant que, d'une part, il impose aux enfants des écoles élémentaires le port du masque de protection à l'école et dans le cadre des activités périscolaires, d'autre part, il impose aux enfant de plus de six ans le porte du masque de protection dans la mesure du possible dans tous les cas où il est imposé aux enfants de plus de 11 ans et aux adultes, enfin, il impose le port systématique du masque par tous dès lors que les règles de distanciation physique ne peuvent être garanties.<br/>
<br/>
              2°) à titre subsidiaire, d'ordonner les mesures d'urgence propres à sauvegarder les libertés fondamentales auxquelles il est porté atteinte.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - la condition d'urgence est remplie compte tenu de la gravité des atteintes portées aux libertés fondamentales et à la présomption d'urgence générée par la crise sanitaire ;<br/>
              - les dispositions du décret contesté du 29 octobre 2020 portent une atteinte grave et manifestement illégale, en premier lieu, à la liberté personnelle des enfants, en deuxième lieu, à leur droit à l'instruction, dès lors que le port du masque à l'école, en limitant les interactions avec l'enseignant, limite substantiellement les bénéfices que sont susceptibles d'en tirer les enfants, en troisième lieu, à l'intérêt supérieur de l'enfant, dès lors que cette mesure comporte des risques non négligeables sur leur santé physique et psychique et, en dernier lieu, au principe de légalité des délits et des peines ;<br/>
              - le décret contesté n'est ni nécessaire, ni adéquate, ni proportionné dès lors, en premier lieu, que les enfants de moins de onze ans ne développent que très exceptionnellement des formes sévères de la maladie, en deuxième lieu, qu'en période de confinement, ils ne fréquentent généralement que leurs parents, en troisième lieu, que le masque de protection n'est efficace que s'il est utilisé correctement, ce qui ne peut être raisonnablement attendu des enfants de moins de onze ans et, en dernier lieu, que le port du masque pour les enfants de moins de onze ans n'est obligatoire qu'à l'école et lors des activités périscolaires.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son préambule ;<br/>
              - le code de la santé publique ;<br/>
              - le décret n° 2020-1257 du 14 octobre 2020 ;<br/>
              - le décret n° 2020-1310 du 29 octobre 2020 ;<br/>
              - l'ordonnance du juge des référés du Conseil d'Etat n° 445821 et suivants du 7 novembre 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures. " En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Aux termes de l'article L. 3131-12 du code de la santé publique, issu de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 : " L'état d'urgence sanitaire peut être déclaré sur tout ou partie du territoire métropolitain ainsi que du territoire des collectivités régies par les articles 73 et 74 de la Constitution et de la Nouvelle-Calédonie en cas de catastrophe sanitaire mettant en péril, par sa nature et sa gravité, la santé de la population ". L'article L. 3131-13 du même code précise que " L'état d'urgence sanitaire est déclaré par décret en conseil des ministres pris sur le rapport du ministre chargé de la santé. Ce décret motivé détermine la ou les circonscriptions territoriales à l'intérieur desquelles il entre en vigueur et reçoit application. Les données scientifiques disponibles sur la situation sanitaire qui ont motivé la décision sont rendues publiques. / (...) / La prorogation de l'état d'urgence sanitaire au-delà d'un mois ne peut être autorisée que par la loi, après avis du comité de scientifiques prévu à l'article L. 3131-19 ". Aux termes de l'article L. 3131-15 du même code : " Dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le Premier ministre peut, par décret réglementaire pris sur le rapport du ministre chargé de la santé, aux seules fins de garantir la santé publique " prendre un certain nombre de mesures de restriction ou d'interdiction des déplacements, activités et réunions, notamment " Interdire aux personnes de sortir de leur domicile, sous réserve des déplacements strictement indispensables aux besoins familiaux ou de santé (...) / Ordonner la fermeture provisoire et réglementer l'ouverture, y compris les conditions d'accès et de présence, d'une ou plusieurs catégories d'établissements recevant du public ainsi que des lieux de réunion, en garantissant l'accès des personnes aux biens et services de première nécessité " à condition d'être " strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu ".<br/>
<br/>
              3. L'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre chargé de la santé puis le Premier ministre à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Pour faire face à l'aggravation de l'épidémie, la loi du 23 mars 2020 a créé un régime d'état d'urgence sanitaire, défini aux articles L. 3131-12 à L. 3131-20 du code de la santé publique, et a déclaré l'état d'urgence sanitaire pour une durée de deux mois à compter du 24 mars 2020. La loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ces dispositions, a prorogé cet état d'urgence sanitaire jusqu'au 10 juillet 2020. L'évolution de la situation sanitaire a conduit à un assouplissement des mesures prises et la loi du 9 juillet 2020 a organisé un régime de sortie de cet état d'urgence.<br/>
<br/>
              4. Une nouvelle progression de l'épidémie au cours des mois de septembre et d'octobre, dont le rythme n'a cessé de s'accélérer au cours de cette période, a conduit le Président de la République à prendre le 14 octobre dernier, sur le fondement des articles L. 3131-12 et L. 3131-13 du code de la santé publique, un décret déclarant l'état d'urgence à compter du 17 octobre sur l'ensemble du territoire national. Le 29 octobre 2020, le Premier ministre a pris, sur le fondement de l'article L. 3131-15 du code de la santé publique, le décret contesté, prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire.<br/>
<br/>
              5. Le juge des référés du Conseil d'Etat a, par une ordonnance n° 445821 du 7 novembre 2020, relevé que la circulation du virus sur le territoire métropolitain s'est fortement amplifiée au cours des dernières semaines, malgré les mesures prises, conduisant à une situation particulièrement dangereuse pour la santé de l'ensemble de la population française. Il en résulte d'ailleurs qu'à la date du 11 novembre 2020, plus de 1 860 000 cas ont été confirmés positifs à la covid-19, en augmentation de près de 35 000 dans les dernières vingt-quatre heures, le taux d'incidence national étant de 428 cas pour 100 000 habitants contre 246 au 20 octobre et 118 au 28 septembre, le taux de positivité des tests réalisés étant de 19,5 % au 11 novembre contre 13,2 % au 18 octobre et 9 % au 28 septembre, 42 435 décès de la covid-19 sont à déplorer au 11 novembre 2020, en hausse de 441 cas en vingt-quatre heures. Enfin, le taux d'occupation des lits en réanimation par des patients atteints de la covid-19 est passé de 43 % au 20 octobre à près de 70 % au 1er novembre et à près de 95 % au 11 novembre, mettant sous tension l'ensemble du système de santé et rendant nécessaire, au cours des derniers jours, des transferts de patients entre régions et avec des pays voisins ainsi que des déprogrammations d'hospitalisations non urgentes.<br/>
<br/>
              6. Mme B..., en tant que mère d'une fille âgée de six ans, soutient que l'obligation du port du masque aux enfants de moins de onze ans est disproportionnée et porte atteinte à leurs libertés fondamentales. Toutefois, il a été relevé que les enfants de moins de onze ans, quoi que moins exposés que d'autres tranches d'âge à une contamination par la covid-19, n'en sont néanmoins pas immunisés. Ainsi, il n'est pas contesté que plus d'une centaine d'enfants de cette classe d'âge a dû être hospitalisée à ce titre. Au demeurant, plusieurs pays européens appliquent une obligation comparable. En outre, le port du masque permet de limiter les hospitalisations dues à d'autres pathologies habituellement fréquentes en cette saison de l'année, permettant ainsi de limiter la pression qui s'exerce sur le système de santé. Dans ces conditions, eu égard à l'aggravation rapide au cours des dernières semaines de la propagation de l'épidémie sur l'ensemble du territoire, à la nécessité de casser cette propagation afin de préserver les structures hospitalières et globalement le système santé, à l'objectif primordial que les enfants de moins onze ans puissent continuer à avoir accès à l'éducation dans les établissements scolaires, l'obligation qui leur est faite de porter le masque, sous l'encadrement et la supervision d'adultes ainsi que le recommandent l'Organisation Mondiale de la Santé et l'UNICEF, dans les établissements scolaires et, dans la mesure du possible, dans les autres lieux, n'apparaît pas comme portant à leurs droits une atteinte grave et manifestement illégale.<br/>
<br/>
              7. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur la condition d'urgence, que la requête de Mme B... doit être rejetée, par application de l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>			O R D O N N E :<br/>
              			------------------<br/>
<br/>
Article 1er : La requête de Mme B... est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à Mme A... B.... <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
