<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027435062</ID>
<ANCIEN_ID>JG_L_2013_05_000000351028</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/43/50/CETATEXT000027435062.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 17/05/2013, 351028, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-05-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>351028</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Bruno Chavanat</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier De Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:351028.20130517</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 19 juillet et 12 octobre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B...A..., demeurant ...; M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 05/01213 du 4 novembre 2010 par lequel la cour régionale des pensions de Pau a rejeté l'ensemble de ses demandes tendant à la révision de sa pension militaires d'invalidité concédée le 8 avril 2002 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des pensions militaires d'invalidité et des victimes de la guerre;<br/>
<br/>
              Vu le décret n° 59-327 du 20 février 1959 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Chavanat, Maître des Requêtes,<br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'une pension militaire d'invalidité définitive au taux de 95 % a été attribuée à M. A...par arrêté du ministre de la défense du 8 avril 2002, au titre de diverses infirmités résultant d'un événement de guerre ; que, par une demande du 10 juin 2002, l'intéressé a sollicité la révision de cette pension en excipant de l'aggravation de certaines des infirmités au titre desquelles elle lui avait été accordée ; que, par décision du 2 février 2004, le ministre de la défense a rejeté sa demande ; que, par un jugement du 10 mars 2005, le tribunal départemental des pensions des Pyrénées-Atlantiques a rejeté les conclusions de l'intéressé tendant à la dissociation de la deuxième infirmité et ordonné une mesure d'expertise afin de déterminer s'il y a avait eu aggravation des deuxième et cinquième infirmités ; que, par un arrêt du 4 novembre 2010, contre lequel M. A...se pourvoit en cassation, la cour régionale des pensions de Pau, après avoir, par trois arrêts avant dire droit des 5 octobre 2006, 3 avril 2008 et 1er octobre 2009, prescrit des mesures d'expertise, a rejeté la requête de M. A...tendant à la révision de sa pension ; <br/>
<br/>
              2. Considérant, en premier lieu, que, pour écarter le moyen tiré des insuffisances ou contradictions affectant les expertises médicales, la cour a, au terme d'une appréciation souveraine exempte de dénaturation, fait état, après un exposé des rapports du premier expert, du travail rigoureux d'investigation et d'analyse ainsi réalisé et de l'absence d'élément exploitable dans le rapport du deuxième expert permettant de l'infirmer, et a enfin relevé qu'il n'était nullement démontré qu'une troisième expertise, huit ans après l'introduction de la demande, serait de nature à éclairer le débat ; qu'elle a ainsi suffisamment motivé son arrêt ; <br/>
              3. Considérant, en deuxième lieu, qu'aux termes de l'article L. 6 du code des pensions militaires d'invalidité et des victimes de la guerre : "La pension prévue par le présent code est attribuée sur demande de l'intéressé après examen, à son initiative, par une commission de réforme (...). L'entrée en jouissance est fixée à la date du dépôt de la demande" ; qu'il résulte de ces dispositions que c'est à cette date qu'il faut se placer pour évaluer le taux des infirmités à raison desquelles la pension ou sa révision est demandée ; que, par suite, la cour n'a pas entaché son arrêt d'erreur de droit en appréciant le degré des invalidités de M. A...au 10 juin 2002, qui est la date à laquelle ce dernier a présenté sa demande de révision à l'administration, et non à celle à laquelle elle a statué ; <br/>
<br/>
              4. Considérant, en troisième lieu, qu'en jugeant, compte tenu des éléments rappelés au point 2 et en se fondant notamment sur les deux rapports du premier expert et sur la complexité de la situation physiologique de l'intéressé, que ses infirmités n'avaient pas connu une aggravation de nature à justifier une révision de sa pension, la cour a, sans les dénaturer, porté une appréciation souveraine sur les pièces du dossier ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que M. A...n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ; <br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance, la somme que demande M. A...au titre des frais exposés par lui et non compris dans les dépens ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
  Article 1er : Le pourvoi de M. A...est rejeté. <br/>
<br/>
  Article 2 : La présente décision sera notifiée à M. B...A...et au ministre de la défense.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
