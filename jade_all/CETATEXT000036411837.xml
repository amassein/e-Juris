<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036411837</ID>
<ANCIEN_ID>JG_L_2017_12_000000399629</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/41/18/CETATEXT000036411837.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 28/12/2017, 399629, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>399629</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:399629.20171228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
       Vu la procédure suivante :<br/>
<br/>
       Le préfet de la Charente-Maritime a déféré au tribunal administratif de Poitiers l'arrêté du 24 octobre 2012 par lequel le maire des Portes-en-Ré a délivré un permis de construire à Mme B...pour l'édification d'une maison d'habitation et d'un garage, l'agrandissement d'une annexe et la démolition des constructions existantes sur un terrain situé 11, route du Champ Cloppé. Par un jugement n°1202735 du 30 janvier 2014, le tribunal administratif a fait droit à déféré.<br/>
<br/>
       Par un arrêt n°14BX01021 du 8 mars 2016, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par Mme B...contre ce jugement ainsi que les conclusions de la commune des Portes-en-Ré.<br/>
<br/>
       Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 9 mai et 22 juin 2016 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat : <br/>
<br/>
       1°) d'annuler cet arrêt ;<br/>
<br/>
       2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
       3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
       Vu les autres pièces du dossier ;<br/>
<br/>
       Vu :<br/>
       - le code de l'urbanisme ;<br/>
       - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
       Après avoir entendu en séance publique :<br/>
<br/>
       - le rapport de Mme Isabelle Lemesle, conseiller d'Etat,  <br/>
<br/>
       - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
       La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de Mme A...B...;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 600-4-1 du code de l'urbanisme : " Lorsqu'elle annule pour excès de pouvoir un acte intervenu en matière  d'urbanisme ou en ordonne la suspension, la juridiction administrative se prononce sur l'ensemble des moyens de la requête qu'elle estime susceptibles de fonder l'annulation ou la suspension, en l'état du dossier ".<br/>
<br/>
              2. Saisi d'un pourvoi dirigé contre une décision juridictionnelle reposant sur plusieurs motifs dont l'un est erroné, le juge de cassation, à qui il n'appartient pas de rechercher si la juridiction aurait pris la même décision en se fondant uniquement sur les autres motifs, doit, hormis le cas où ce motif erroné présenterait un caractère surabondant, accueillir le pourvoi. Il en va cependant autrement lorsque la décision juridictionnelle attaquée prononce l'annulation pour excès de pouvoir d'un acte administratif, dans la mesure où l'un quelconque des moyens retenus par le juge du fond peut suffire alors à justifier son dispositif d'annulation. En pareille hypothèse - et sous réserve du cas où la décision qui lui est déférée aurait été rendue dans des conditions irrégulières - il appartient au juge de cassation, si l'un des moyens reconnus comme fondés par cette décision en justifie légalement le dispositif, de rejeter le pourvoi. Toutefois, en raison de l'autorité de chose jugée qui s'attache aux motifs constituant le soutien nécessaire du dispositif de la décision juridictionnelle déférée, le juge de cassation ne saurait, sauf à méconnaître son office, prononcer ce rejet sans avoir, au préalable, censuré celui ou ceux de ces motifs qui étaient erronés. Eu égard à l'objet des dispositions précitées de l'article L. 600-4-1 du code de l'urbanisme, cette règle trouve en particulier à s'appliquer lorsque la pluralité des motifs du juge du fond découle de l'obligation qui lui est faite de se prononcer sur l'ensemble des moyens susceptibles de fonder l'annulation.<br/>
<br/>
              3. Pour rejeter l'appel dirigé contre le jugement du 30 janvier 2014 du tribunal administratif de Poitiers qui, sur déféré du préfet de la Charente-Maritime, a annulé le permis de construire délivré le 24 octobre 2012 par le maire des Portes-en-Ré à M. et MmeB..., en vue de la démolition de constructions existantes et de l'édification d'une maison d'habitation et d'un garage, ainsi que de l'agrandissement de l'annexe de la piscine sur un terrain qui a été submergé le 28 février 2010 lors de la tempête " Xynthia ", la cour administrative d'appel de Bordeaux, dans son arrêt du 8 mars 2016, a retenu deux motifs entachant l'illégalité du permis de construire contesté, d'une part, une méconnaissance des dispositions de l'article UL2 du plan d'occupation des sols des Portes en Ré, aux termes duquel : " (...) Sont interdits dans les secteurs (...) ULas (...):/ (...) - les affouillements et exhaussements du sol ;/ - les remblais " et d'autre part, une violation des dispositions de l'article R. 111-2 du code de l'urbanisme aux termes desquelles : " Le projet peut être refusé ou n'être accepté que sous réserve de l'observation de prescriptions spéciales s'il est de nature à porter atteinte à la salubrité ou à la sécurité publique du fait de sa situation, de ses caractéristiques, de son importance ou de son implantation à proximité d'autres installations ".<br/>
<br/>
              Sur la méconnaissance des dispositions du plan d'occupation des sols :<br/>
<br/>
              4. Aux termes de l'article L. 421-1 du code de l'urbanisme : " Les constructions, même ne comportant pas de fondations, doivent être précédées de la délivrance d'un permis de construire (...) ". L'article L. 421-2 du même code prévoit que : " Les travaux, installations et aménagements affectant l'utilisation des sols et figurant sur une liste arrêtée par décret en Conseil d'Etat doivent être précédés de la délivrance d'un permis d'aménager ". L'article R. 421-23 dispose que : " Doivent être précédés d'une déclaration préalable les travaux, installations et aménagements suivants :/ (...) f) A moins qu'ils ne soient nécessaires à l'exécution d'un permis de construire, les affouillements et exhaussements du sol dont la hauteur, s'il s'agit d'un exhaussement, ou la profondeur dans le cas d'un affouillement, excède deux mètres et qui portent sur une superficie supérieure ou égale à cent mètres carrés ; (...) ".<br/>
<br/>
              5. Il résulte de ces dispositions du code de l'urbanisme que les prescriptions de l'article UL2 du plan d'occupation des sols des Portes-en-Ré, citées au point 3 et relatives aux seuls affouillements, exhaussements et remblais, concernent des travaux soumis non à la réglementation du permis de construire mais à celle du permis d'aménager, lorsque les règlements d'urbanisme ne les interdisent pas. Par suite, elles ne sont pas applicables aux travaux de mise en état des terrains d'assiette des bâtiments et autres ouvrages dont la construction fait l'objet d'un permis de construire, lequel est délivré conformément à d'autres dispositions du même code et tient compte d'éventuels affouillements, exhaussements du sol et remblais. <br/>
<br/>
              6. Il suit de là que si le projet de construction litigieux a rendu nécessaires des affouillements, exhaussements du sol et remblais, le moyen tiré de ce que le permis de construire attaqué méconnaîtrait les dispositions de l'article UL 2 du plan d'occupation des sols est inopérant, alors même que les affouillements, exhaussements du sol et remblais réalisés auraient excédé les mouvements du sol nécessaires à l'édification de la construction autorisée. Ainsi, en jugeant que le permis de construire accordé à M. et Mme B...avait été délivré en méconnaissance de ces dispositions la cour administrative d'appel a entaché d'erreur de droit le premier motif retenu pour confirmer l'annulation de ce permis. <br/>
<br/>
              Sur la méconnaissance des dispositions du code de l'urbanisme :<br/>
<br/>
              7. Pour apprécier si les risques d'atteinte à la salubrité ou à la sécurité publique justifient un refus de permis de construire sur le fondement des dispositions de l'article R. 111-2 du code de l'urbanisme, il appartient à l'autorité compétente en matière d'urbanisme, sous le contrôle du juge de l'excès de pouvoir, de tenir compte tant de la probabilité de réalisation de ces risques que de la gravité de leurs conséquences, s'ils se réalisent, et pour l'application de cet article en matière de risque de submersion marine, il appartient à l'autorité administrative d'apprécier, en l'état des données scientifiques disponibles, ce risque de submersion en prenant en compte notamment le niveau marin de la zone du projet, le cas échéant, sa situation à l'arrière d'un ouvrage de défense contre la mer ainsi qu'en pareil cas, la probabilité de rupture ou de submersion de cet ouvrage au regard de son état, de sa solidité et des précédents connus de rupture ou de submersion.<br/>
<br/>
              8. Il ressort des énonciations de l'arrêt attaqué que dans le cadre de la révision du plan de prévention des risques naturels, dont l'inadaptation aux dangers potentiels avait été mise en évidence par les phénomènes de submersion mesurés lors de la tempête Xynthia qui est survenue dans la nuit du 27 au 28 février 2010, et au cours de laquelle la construction de M. et Mme B...a été inondée, les études réalisées incluent le terrain d'assiette des requérants dans un secteur où il existe un risque de submersion de plus d'un mètre. D'une part, la cour a pu, sans erreur de droit, faire application des dispositions de l'article R. 111-2 du code de l'urbanisme dès lors que, contrairement à ce que soutient la requérante, elle s'est fondée sur l'ensemble des pièces produites et n'a pas exigé que soit justifiée l'existence d'un risque nul de submersion. D'autre part, en jugeant, au vu de l'ensemble des caractéristiques de la situation d'espèce qui lui était soumise et du projet pour lequel l'autorisation de construire avait été délivrée, que le projet de construction présente, compte tenu de sa nature, de son implantation en zone inondable et de la configuration des lieux, un risque pour la sécurité publique justifiant que soit opposé un refus de permis sur le fondement de l'article R. 111-2 du code de l'urbanisme, la cour administrative d'appel s'est livrée à une appréciation souveraine des faits de l'espèce qui, exempte de dénaturation, n'est pas susceptible d'être remise en cause par le juge de cassation. Par suite, Mme B...n'est pas fondée à contester le second motif retenu par la cour pour confirmer l'annulation du permis de construire qui lui avait été délivré. <br/>
<br/>
              9. Il résulte de tout ce qui précède que, l'un des motifs de l'arrêt attaqué justifiant l'annulation pour excès de pouvoir du permis contesté, le pourvoi de Mme B... doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
       --------------<br/>
<br/>
Article 1er : Le pourvoi de Mme B...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme A...B...et au ministre de la cohésion des territoires.<br/>
Copie en sera adressée à la commune des Portes-en-Ré.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
