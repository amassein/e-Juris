<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038498639</ID>
<ANCIEN_ID>JG_L_2019_05_000000430796</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/49/86/CETATEXT000038498639.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 20/05/2019, 430796, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-05-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430796</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:430796.20190520</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 16 et 19 mai 2019 au secrétariat du contentieux du Conseil d'Etat, l'association " La France insoumise " demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) à titre principal, d'enjoindre au Conseil supérieur de l'audiovisuel d'accorder à la liste de candidats soutenue par l'association " La France insoumise " une durée d'émission supplémentaire de 10 minutes et de réduire, dans la même proportion, la durée attribuée à la liste de candidats soutenue par le parti " Rassemblement national " ;<br/>
<br/>
              2°) à titre subsidiaire, d'enjoindre au Conseil supérieur de l'audiovisuel de reprendre la détermination de la durée des émissions de la campagne officielle ;<br/>
<br/>
              3°) de mettre à la charge du Conseil supérieur de l'audiovisuel la somme de 5 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - le Conseil d'Etat est compétent pour connaître de sa requête ;<br/>
              - la condition d'urgence est remplie dès lors que, d'une part, la décision contestée du 9 mai 2019 préjudicie de façon suffisamment grave et immédiate aux intérêts qu'elle entend défendre et, d'autre part, les émissions consacrées à l'expression des candidats doivent être diffusées du 14 au 23 mai 2019 ;<br/>
              - il est porté une atteinte grave et manifestement illégale à la liberté fondamentale que constitue le principe du caractère pluraliste de l'expression des courants de pensée et d'opinion ;<br/>
              - par la durée qu'il lui a attribuée, le Conseil supérieur de l'audiovisuel a porté une atteinte grave dès lors qu'elle revient à priver d'un temps d'émission suffisant le courant de pensée et d'opinion qu'elle représente ;<br/>
              - la décision contestée du 9 mai 2019 est entachée d'une illégalité manifeste eu égard à l'application faite par le Conseil supérieur de l'audiovisuel des critères prévus par le IV de l'article 19 de la loi modifiée du 7 juillet 1977 ;<br/>
              - le Conseil supérieur de l'audiovisuel a entaché sa décision d'une erreur de droit, d'une part, en attribuant à vingt-sept autres listes une durée d'intervention de 33 secondes, d'autre part, en tenant compte du nombre de conseillers régionaux et départementaux alors que le critère du soutien apporté par les élus est prévu au III de l'article 19 de la loi précitée du 7 juillet 1977, enfin, en attribuant un temps total d'émission supplémentaire excédant de 33 secondes la durée légalement prévue ;<br/>
              - l'intérêt des autres listes en présence ne s'oppose pas à ce que le juge des référés du Conseil d'Etat ordonne la mesure demandée dès lors que celle-ci ne concerne que les listes de " La France insoumise " et du " Rassemblement national ".<br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 17 mai 2019, le Conseil supérieur de l'audiovisuel conclut au rejet de la requête. Il soutient, à titre principal, qu'il n'est porté aucune atteinte grave et manifestement illégale à la liberté fondamentale invoquée par l'association requérante et, à titre subsidiaire, que l'accueil éventuel des prétentions de la requête s'accompagnerait de contraintes matérielles et calendaires particulièrement difficiles compte tenu des délais dans lesquels serait enserrée une opération conduisant au retrait d'une partie du temps accordé au parti " Rassemblement national " au profit de l'association " La France insoumise ".<br/>
<br/>
              La requête a été communiquée au ministre de l'intérieur qui n'a pas produit d'observations.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ;<br/>
              - la loi n° 77-729 du 7 juillet 1977 ;<br/>
              - la loi n° 86-1067 du 30 septembre 1986 ;<br/>
              - la loi n° 2018-509 du 25 juin 2018 ;<br/>
              - le décret n° 79-160 du 28 février 1979 ;<br/>
              - le décret n° 2019-188 du 13 mars 2019 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, l'association " La France insoumise " et, d'autre part, le Conseil supérieur de l'audiovisuel et le ministre de l'intérieur.<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 20 mai 2019 à 9 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Thiriez, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'association " La France insoumise " ;<br/>
<br/>
              - les représentants de l'association " La France insoumise " ; <br/>
<br/>
              - les représentants du Conseil supérieur de l'audiovisuel ; <br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ".<br/>
<br/>
              2. Par décret du 13 mars 2019, le Premier ministre a convoqué les électeurs les 25 et 26 mai 2019 en vue de procéder à l'élection des représentants français au Parlement européen. <br/>
<br/>
              3. En vertu de l'article 19 de la loi du 7 juillet 1977 relative à cette élection, il est prévu que durant la campagne électorale, qui débute deux semaines avant les élections, soit le lundi 13 mai 2019, un temps d'émission sur les antennes des sociétés nationales de programmes soit mis à la disposition des listes dont la candidature a été régulièrement enregistrée. Selon ces dispositions, la durée des émissions mises à la disposition de ces listes résulte de l'addition de trois fractions. Chaque liste dispose en premier lieu de 3 minutes. En deuxième lieu, une durée de deux heures est répartie entre les listes au prorata du nombre de députés, de sénateurs et de représentants français au Parlement européen ayant déclaré les soutenir. En troisième et dernier lieu, une durée d'une heure et demie est répartie " entre les listes " afin que, selon le IV de l'article 19, " les durées d'émission attribuées à chacune des listes en application du présent article ne soient pas hors de proportion avec la participation à la vie démocratique de la Nation des partis et groupements politiques qui les soutiennent ". Si le VI de cet article prévoit que le Conseil supérieur de l'audiovisuel (CSA) se borne à constater l'attribution des durées correspondant aux deux premières fractions, il appartient à celui-ci de procéder à l'attribution de la troisième fraction. Pour cette attribution, le CSA tient compte d'une part des deux premières fractions, d'autre part de la " représentativité des listes de candidats, appréciée, en particulier, en fonction des résultats obtenus lors du dernier renouvellement général du Parlement européen et aux plus récentes élections par les candidats de la liste ou par les partis et groupements politiques qui les soutiennent et en fonction des indications de sondages d'opinion " et, enfin, de la " contribution de chacune des listes de candidats et des partis ou groupements qui les soutiennent à l'animation du débat électoral ". Il résulte de ces dispositions qu'une fois cette troisième fraction attribuée, la répartition de la durée des émissions entre les différentes listes doit, autant qu'il est possible et sans disproportion, refléter la part prise par les partis et groupements politiques qui les soutiennent à la vie démocratique de la Nation.<br/>
<br/>
              4. Le 9 mai 2019, le CSA a, sur le fondement de ces dispositions législatives, pris une décision " sur les émissions de la campagne électorale en vue des élections européennes 2019 ". Constatant l'attribution de la première fraction et, au vu des soutiens apportés à certaines listes selon les conditions fixées par le décret du 28 février 1979 portant application de la loi du<br/>
7 juillet 1977, de la deuxième fraction, il a réparti le temps correspondant à la troisième fraction et, ainsi, d'une part fixé la durée totale d'émission allouée à chaque parti ou groupement, d'autre part, précisé le nombre et la durée des émissions mises à leur disposition. Par une décision du<br/>
10 mai 2019, le CSA, pour tenir compte de l'enregistrement tardif d'une trente-quatrième liste, a constaté l'attribution à cette liste de 3 minutes au titre de la première fraction et lui a attribué, au titre de la troisième fraction, une durée supplémentaire de 33 secondes, comme il l'avait fait dans sa décision du 9 mai pour d'autres listes, et modifié en conséquence la décision précédente. <br/>
<br/>
              5. En vertu de la décision du 9 mai, la liste " Renaissance soutenue par la République en marche, le MoDem et ses partenaires " dispose de 55 minutes et 53 secondes, " Prenez le pouvoir, liste soutenue par Marine le Pen ", de 48 minutes et 11 secondes, " Union de la droite et du centre " de 38 minutes et 20 secondes, " Envie d'Europe écologique et sociale " de 19 minutes et 43 secondes, " La France insoumise ", de 18 minutes et 37 secondes et les autres listes de durées allant d'environ 14 minutes à 3 minutes et trente-trois secondes pour vingt-et-une d'entre elles. <br/>
<br/>
              6. L'association " La France insoumise " regarde la durée totale qui a été attribuée à la liste qu'elle soutient comme hors de proportion avec sa participation à la vie démocratique de la Nation, qui justifierait, selon elle, l'attribution de 10 minutes supplémentaires. Elle estime en conséquence que la décision du CSA porte à cet égard une atteinte grave et manifestement illégale au caractère pluraliste de l'expression des courants de pensée et d'opinion.<br/>
<br/>
              7. La requérante considère qu'en tenant compte, pour la répartition de la troisième fraction, du nombre de conseillers régionaux et départementaux, le CSA a retenu un critère non prévu par le législateur. Elle fait valoir en outre que les résultats qu'elle a obtenus aux récentes élections présidentielle et législatives sont équivalents à ceux de formations politiques bénéficiant, du fait de la décision litigieuse, de durées très largement supérieures à la sienne, que son écart avec ces listes, dans les récents sondages, est bien plus faible que le rapport de ces durées et que sa contribution à l'animation du débat électoral, mesurée, selon le choix du CSA, en tenant compte notamment de l'organisation de réunions publiques importantes ou l'activité sur les réseaux sociaux, est supérieure à celle de ces listes. Elle soutient enfin que le CSA ne pouvait, sans méconnaître les dispositions ci-dessus rappelées de la loi du 7 juillet 1977, attribuer une durée au titre de la troisième fraction à chacune des listes en présence, y compris, à la<br/>
trente-quatrième liste, portant ainsi la durée totale de la troisième fraction à une heure, trente minutes et trente-trois secondes, en méconnaissance du IV de l'article 19 de cette loi. <br/>
<br/>
              8. Le CSA, à partir d'une estimation de la représentativité de l'association<br/>
" La France insoumise " de l'ordre de 8,5 % selon la méthode qu'il a retenue, a attribué à la liste qu'elle soutient 12 minutes et 43 secondes au titre de la troisième fraction une fois constatée la durée d'émission de 5 minutes et 54 secondes au titre des deux premières. D'une part, il ne résulte pas de l'instruction que la méthode retenue par le CSA pour l'attribution de la troisième fraction, qui repose sur la représentativité des listes au regard des plus récentes élections, parmi lesquelles pouvaient figurer les élections départementales et régionales de 2015,  les indications des sondages d'opinion ainsi que sur la contribution de chacune des listes de candidats et des partis ou groupements qui les soutiennent à l'animation du débat électoral inclurait des critères différents de ceux fixés au IV de l'article 19 de la loi du 7 juillet 1977 cité au point 3. D'autre part, le CSA a à juste titre, s'agissant des résultats aux récentes élections, tenu compte des résultats aux élections au Parlement européen en 2014 de la liste sur laquelle figurait alors<br/>
M.A..., de ce que l'intéressé était soutenu lors de l'élection présidentielle de 2017 par l'association " La France insoumise ", mais également par le Parti communiste français comme des résultats aux élections législatives de 2017. Il a pris en considération la moyenne - 8,5 % - des estimations de vote pour la liste par les différents sondages d'opinion disponibles à la date de sa décision ainsi que sa participation à l'animation du débat électoral, ce qui l'a conduit à rehausser légèrement le temps alloué à l'association requérante. Enfin, si le CSA a estimé que les dispositions législatives citées ci-dessus lui faisaient obligation d'attribuer une part de la troisième fraction à chacune des listes en présence, sa décision consistant, pour répartir cette fraction, à augmenter les durées d'émission des six listes, dont celle soutenue par la requérante, disposant d'un temps d'antenne insuffisant au regard de leur représentativité, puis, une fois la compensation opérée, à répartir, de manière égale le temps restant, entre les vingt-sept autres listes, soit 33 secondes à chacune, ainsi qu'à attribuer 33 secondes à la trente-quatrième liste n'a pas eu, en tout état de cause, d'incidence significative, en termes relatifs, sur la durée accordée à la requérante par rapport à celle attribuée aux autres listes.<br/>
<br/>
              9. Ainsi, au regard de l'ensemble des critères d'appréciation, il n'apparaît pas qu'en fixant à 18 minutes et 37 secondes la durée totale des émissions dont bénéficie l'association " La France insoumise ", le CSA lui ait attribué une durée hors de proportion avec la participation de cette formation politique à la vie démocratique de la Nation constitutive d'une atteinte grave et manifestement illégale au caractère pluraliste de l'expression des courants de pensée et d'opinion. Dans ces conditions, les conclusions de la requête tendant à ce qu'il soit enjoint au CSA de modifier les durées arrêtées par les décisions contestées doivent être rejetées, ainsi que, par voie de conséquence, celles présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de l'association " La France insoumise " est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à l'association " La France insoumise ", au Conseil supérieur de l'audiovisuel et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
