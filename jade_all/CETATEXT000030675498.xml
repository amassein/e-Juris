<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030675498</ID>
<ANCIEN_ID>JG_L_2015_05_000000389865</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/67/54/CETATEXT000030675498.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 05/05/2015, 389865, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-05-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389865</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>FOUSSARD</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2015:389865.20150505</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme D...A...a demandé au juge des référés du tribunal administratif de Nantes, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au préfet de la Mayenne d'enregistrer son dossier de demande de titre de séjour et de lui délivrer une autorisation provisoire de séjour l'autorisant à travailler. Par une ordonnance n° 1503058 du 13 avril 2015, le juge des référés du tribunal administratif de Nantes a rejeté sa demande.<br/>
<br/>
              Par une requête enregistrée le 29 avril 2015 au secrétariat du contentieux du Conseil d'Etat, MmeA..., représentée par Me C...B..., demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de faire droit à sa demande de première instance dans un délai de huit jours à compter de la décision intervenir ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - l'ordonnance attaquée est entachée d'une irrégularité, dès lors qu'elle ne comporte pas la signature du juge des référés du tribunal administratif de Nantes ;<br/>
              - la condition d'urgence est remplie ;<br/>
              - le refus du préfet d'enregistrer sa demande de titre de séjour porte une atteinte grave et manifestement illégale à sa liberté d'aller et venir et à son droit à une vie familiale normale.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ; qu'en vertu de l'article L. 522-3 du même code, le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée ; qu'à cet égard, il appartient au juge d'appel de prendre en considération les éléments recueillis par le juge du premier degré dans le cadre de la procédure écrite et orale qu'il a diligentée ;<br/>
<br/>
              2. Considérant que, contrairement à ce que soutient MmeA..., la minute de l'ordonnance attaquée a été signée par le juge des référés du tribunal administratif de Nantes ;<br/>
<br/>
              3. Considérant que pour justifier de l'urgence qui s'attacherait à ce qu'il soit enjoint au préfet de la Mayenne d'enregistrer son dossier de demande de titre de séjour et de lui délivrer une autorisation provisoire de séjour l'autorisant à travailler, Mme A...fait valoir, en premier lieu, qu'en l'absence d'une telle autorisation, elle est susceptible de faire à tout moment l'objet d'une mesure d'éloignement ; que, toutefois, aux termes de l'article L. 511-4 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Ne peuvent faire l'objet d'une obligation de quitter le territoire français : (...) / 2° L'étranger qui justifie par tous moyens résider habituellement en France depuis qu'il a atteint au plus l'âge de treize ans " ; qu'il résulte de ces dispositions que la requérante, qui affirme pouvoir justifier d'une résidence habituelle en France depuis l'âge de onze ans, n'est pas susceptible de faire l'objet d'une mesure d'éloignement ; que Mme A...soutient, en deuxième lieu, que le refus que lui a opposé le préfet de la Mayenne l'a contrainte à interrompre son contrat d'apprentissage ; que toutefois le document de circulation pour étranger mineur dont elle bénéficiait depuis le 15 juillet 2013 n'avait pas le caractère d'une autorisation de travail et qu'au demeurant elle n'établit pas que son contrat d'apprentissage aurait été interrompu ; que Mme A...soutient enfin, en troisième lieu, que l'expiration imminente de son passeport provisoire la prive de ses chances d'obtenir à l'avenir un titre de séjour ; que, toutefois, la circonstance que le " certificat d'identité et de voyage valant passeport provisoire " dont elle est titulaire arrive à expiration est sans incidence sur le litige, dès lors qu'elle n'allègue ni que ce document ne pourrait pas être renouvelé, ni qu'elle serait dans l'impossibilité de se voir délivrer un passeport par les autorités guinéennes ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que la condition d'urgence prévue par l'article L. 521-2 du code de justice administrative ne peut pas être regardée comme remplie ; que, par suite, Mme A...n'est pas fondée à se plaindre de ce que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Nantes a rejeté sa demande ; que son appel ne peut en conséquence qu'être rejeté selon la procédure prévue à l'article L. 522-3 du code de justice administrative, y compris les conclusions présentées sur le fondement de l'article L. 761-1 du même code ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de Mme A...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme D...A....<br/>
Copie en sera adressée au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
