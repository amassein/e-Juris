<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037882303</ID>
<ANCIEN_ID>JG_L_2018_12_000000419063</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/88/23/CETATEXT000037882303.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 28/12/2018, 419063, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419063</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Laure Durand-Viel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Julie Burguburu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:419063.20181228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 18 mars et 7 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, la Ligue française pour la protection des oiseaux (LPO) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet résultant du silence gardé par le ministre d'Etat, ministre de la transition écologique et solidaire, sur sa demande du 5 décembre 2017 tendant à l'abrogation de l'arrêté du 17 août 1989 relatif à l'emploi des gluaux pour la capture des grives et des merles destinés à servir d'appelants dans les départements des Alpes-de-Haute-Provence, des Alpes-Maritimes, des Bouches-du-Rhône, du Var et du Vaucluse ; <br/>
<br/>
              2°) d'enjoindre au ministre d'Etat, ministre de la transition écologique et solidaire, d'abroger le décret précité dans un délai de deux mois à compter de la notification de la décision du Conseil d'Etat, sous astreinte de 100 euros par jour de retard, ou, à titre subsidiaire, d'enjoindre au ministre d'Etat, ministre de la transition écologique et solidaire, de réexaminer sa demande tendant à l'abrogation de l'arrêté précité, dans le même délai et sous la même astreinte ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive n° 2009/147/CE du Parlement européen et du Conseil du 30 novembre 2009 ;<br/>
              - le code de l'environnement ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - l'arrêt de la Cour de Justice des Communautés européennes du 9 décembre 2004 Commission des Communautés européennes contre Royaume d'Espagne (C-79/03) ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 11 décembre 2018, présentée par la Ligue française pour la protection des oiseaux.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laure Durand-Viel, auditeur,  <br/>
<br/>
              - les conclusions de Mme Julie Burguburu, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la Fédération nationale des chasseurs.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. La ligue française pour la protection des oiseaux (LPO) demande l'annulation pour excès de pouvoir de la décision implicite de rejet née du silence gardé par le ministre d'Etat, ministre de la transition écologique et solidaire, sur sa demande du 5 décembre 2017 tendant à l'abrogation de l'arrêté du 17 août 1989 relatif à l'emploi des gluaux pour la capture des grives et des merles destinés à servir d'appelants dans les départements des Alpes-de-Haute-Provence, des Alpes-Maritimes, des Bouches-du-Rhône, du Var et du Vaucluse.<br/>
<br/>
              2. La Fédération nationale des chasseurs justifie d'un intérêt suffisant au maintien des arrêtés attaqués. Ainsi, son intervention est recevable.<br/>
<br/>
              3. D'une part, l'article 8 de la directive 2009/147/CE du Parlement européen et du Conseil du 30 novembre 2009 concernant la conservation des oiseaux sauvages, dite directive " oiseaux ", impose aux Etats membres d'interdire " le recours à tous moyens, installations ou méthodes de capture ou de mise à mort massive ou non sélective ou pouvant entraîner localement la disparition d'une espèce, et en particulier à ceux énumérés à l'annexe IV, point a) ". Cette annexe vise notamment en son a) les gluaux, qui constituent un mode de chasse traditionnel consistant à capturer les oiseaux grâce à des pièges enduits de glu. L'article 9 de la directive autorise toutefois les Etats membres à déroger à ces dispositions " s'il n'existe pas d'autre solution satisfaisante " pour un certain nombre de motifs, et notamment " pour permettre, dans des conditions strictement contrôlées et de manière sélective, la capture, la détention ou toute autre exploitation judicieuse de certains oiseaux en petites quantités. ". L'article 9 prévoit également, en son paragraphe 2, que les dérogations doivent mentionner les espèces concernées, les moyens, installations ou méthodes de capture ou de mise à mort autorisés, les conditions de risque et les circonstances de temps et de lieu dans lesquelles ces dérogations peuvent être prises, l'autorité habilitée à déclarer que les conditions exigées sont réunies, à décider quels moyens, installations ou méthodes peuvent être mis en oeuvre, dans quelles limites et par quelles personnes, enfin les contrôles qui seront opérés.  <br/>
<br/>
              4. D'autre part, aux termes de l'article L. 424-4 du code de l'environnement  : " Dans le temps où la chasse est ouverte, le permis donne à celui qui l'a obtenu le droit de chasser de jour, soit à tir, soit à courre, à cor et à cri, soit au vol, suivant les distinctions établies par des arrêtés du ministre chargé de la chasse (...) / Pour permettre, dans des conditions strictement contrôlées et de manière sélective, la chasse de certains oiseaux de passage en petites quantités, le ministre chargé de la chasse autorise, dans les conditions qu'il détermine, l'utilisation des modes et moyens de chasse consacrés par les usages traditionnels, dérogatoires à ceux autorisés par le premier alinéa (...) / Les gluaux sont posés une heure avant le lever du soleil et enlevés avant onze heures. (...) ". <br/>
<br/>
              5. En vertu de l'article 1er de l'arrêté du 17 août 1989 dont il est demandé l'abrogation : " L'emploi des gluaux pour la capture des grives draines, litornes, mauvis et musiciennes et des merles noirs, destinés à servir d'appelants à des fins personnelles, est autorisé dans les départements des Alpes-de-Haute-Provence, des Alpes-Maritimes, des Bouches-du-Rhône, du Var et de Vaucluse et dans les conditions strictement contrôlées définies ci-après afin de permettre la capture sélective et en petites quantités de ces oiseaux, puisqu'il n'existe pas d'autre solution satisfaisante ". L'article 2 de l'arrêté n'autorise l'utilisation de gluaux que pendant la période fixée annuellement par le préfet. L'arrêté précise, à ses articles 3 à 6, les spécifications techniques applicables, qui imposent en particulier que les gluaux soient posés à l'aube et enlevés avant onze heures et ne puissent demeurer posés qu'en présence du chasseur, le port du fusil étant interdit pendant les opérations, réglementent les conditions d'utilisation des appelants et prévoient que le nombre maximum d'oiseaux pouvant être capturés pendant la campagne ainsi que, le cas échéant, les spécifications techniques propres à un département sont fixés chaque année par le ministre chargé de la chasse. L'arrêté subordonne par ailleurs, en vertu de ses articles 7 à 11, l'utilisation des gluaux à un régime d'autorisation annuelle qui n'est délivrée par le préfet qu'au détenteur du droit de chasse sur le territoire où ils sont installés et à la condition que des gluaux aient été licitement utilisés sur ce territoire au cours de la campagne précédente et prévoit que cette autorisation doit pouvoir être présentée à tout instant sur les lieux, que les gluaux ne peuvent être utilisés que par les titulaires d'un permis de chasser dûment visé et validé dans le département ou dans une commune limitrophe, que tout utilisateur de gluaux doit tenir à jour un état de ses captures, lequel doit pouvoir être présenté à tout instant sur les lieux et doit être transmis au préfet avant le 31 décembre, et que les animaux autres que ceux visés par l'arrêté capturés accidentellement doivent être immédiatement nettoyés et libérés. L'article 12 interdit la commercialisation des grives et des merles noirs ainsi capturés. Enfin, l'article 13 de l'arrêté confie aux agents habilités en matière de police de la chasse, sous la responsabilité du préfet, le contrôle de l'ensemble de ces dispositions. <br/>
<br/>
              6. Ces dispositions, qui ne permettent l'utilisation des gluaux, à titre dérogatoire, que dans cinq départements, édictent ainsi des spécifications techniques et un régime d'autorisation et de contrôle rigoureux. Elles mentionnent, ainsi que le prévoit la directive, les espèces concernées et déterminent les conditions d'utilisation des gluaux, les circonstances dans lesquelles des dérogations peuvent intervenir, les personnes susceptibles d'en bénéficier, l'autorité compétente pour accorder les autorisations et les modalités des contrôles, en désignant les catégories d'agents en charge de ces derniers. Enfin, elles confient, sous le contrôle du juge de l'excès de pouvoir, d'une part, au ministre chargé de la chasse la charge de prévoir chaque année par arrêté, compte tenu notamment des données les plus récentes dont il dispose sur la population des espèces concernées et leur répartition, le nombre maximum d'oiseaux pouvant être capturés pendant la campagne ainsi, le cas échéant, que les spécifications techniques propres à un département et, d'autre part, aux préfets la charge d'apprécier, en fonction des circonstances locales, si les autorisations sollicitées pourront être accordées, en veillant au respect des objectifs ci-dessus rappelés. Il ne ressort pas des pièces du dossier que c'est à tort que l'auteur de l'arrêté attaqué a estimé qu'il n'existait pas de solution satisfaisante alternative à l'emploi des gluaux pour la capture de spécimens des espèces mentionnées destinés à servir d'appelants à des fins personnelles ni que le recours aux gluaux dans le respect des préconisations édictées par cet arrêté conduirait par lui-même à méconnaître les critères, d'une part, de sélectivité des captures, et d'autre part, d'un nombre limité d'individus capturés, fixés par l'article L. 424-4 du code de l'environnement pour la transposition de l'article 9 de la directive du 30 novembre 2009. Par suite, le moyen tiré de la méconnaissance par l'arrêté contesté des objectifs de la directive du 30 novembre 2009 doit être écarté. <br/>
<br/>
              7. Il résulte de tout ce qui précède que la requête doit être rejetée, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ou tendant au prononcé d'une injonction. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de la Fédération nationale des chasseurs est admise.<br/>
Article 2 : La requête de la Ligue française pour la protection des oiseaux est rejetée.<br/>
Article 3 : La présente décision sera notifiée à la Ligue française pour la protection des oiseaux, au ministre d'Etat, ministre de la transition écologique et solidaire et à la Fédération nationale des chasseurs.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
