<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026477813</ID>
<ANCIEN_ID>JG_L_2012_10_000000338752</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/47/78/CETATEXT000026477813.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  8ème sous-section jugeant seule, 10/10/2012, 338752, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-10-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>338752</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 8ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Contentieux répressif</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Gilles Bachelier</PRESIDENT>
<AVOCATS>BALAT ; SCP BOUTET</AVOCATS>
<RAPPORTEUR>M. Hervé Cassagnabère</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:338752.20121010</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 19 avril et 19 juillet 2010 au secrétariat du contentieux du Conseil d'Etat, présenté pour l'établissement public Voies navigables de France, dont le siège est au 175 rue Ludovic Boutleux, BP 820 à Béthune (62408) ; il demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 1er de l'arrêt n° 09VE01135 du 28 janvier 2010 par lequel la cour administrative d'appel de Versailles a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0709567 du 5 février 2009 par lequel le tribunal administratif de Versailles, saisi du procès-verbal de contravention de grande voirie établi à l'encontre de Mme Mireille A, a rejeté sa demande tendant à la condamnation de l'intéressée au versement d'une amende de 1 500 euros et à ce qu'il lui soit fait injonction d'enlever son bateau dénommé " Amazone " stationnant sans autorisation sur la Seine, au droit de la commune de Sèvres, dans le délai de 8 jours, sous peine d'une astreinte de 100 euros par jour de retard et, d'autre part, à la condamnation de Mme A au paiement de cette amende, et à ce qu'il lui soit fait injonction d'enlever son bateau du domaine public dans un délai de huit jours sous astreinte de 100 euros par jour de retard ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
               3°) de mettre à la charge de Mme A la somme de 1 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code civil ;<br/>
<br/>
              Vu le code général de la propriété des personnes publiques ;<br/>
<br/>
              Vu la loi n° 91-1385 du 31 décembre 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Hervé Cassagnabère, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Boutet, avocat de Mme A et de Me Balat, avocat de Voies navigables de France,<br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Boutet, avocat de Mme A et à Me Balat, avocat de Voies navigables de France ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant, d'une part, qu'aux termes des dispositions de l'article L. 774-2 du code de justice administrative : " Dans les dix jours qui suivent la rédaction d'un procès-verbal de contravention, le préfet fait faire au contrevenant notification de la copie du procès-verbal. La notification est faite dans la forme administrative, mais elle peut également être effectuée par lettre recommandée avec demande d'avis de réception. La notification indique à la personne poursuivie qu'elle est tenue, si elle veut fournir des défenses écrites, de les déposer dans le délai de quinzaine à partir de la notification qui lui est faite. Il est dressé acte de la notification ; cet acte doit être adressé au tribunal administratif et y être enregistré comme les requêtes introductives d'instance. " ; qu'il résulte de ces dispositions que le juge de la contravention de grande voirie, dès qu'il est saisi par une autorité compétente, doit se prononcer tant sur l'action publique que sur l'action domaniale, que lui soient ou non présentées des conclusions en ce sens ; qu'eu égard aux particularités de son office, il doit vérifier, au besoin d'office, lorsqu'un moyen tiré de l'irrégularité de la notification des poursuites est soulevé, si la procédure n'a pas été régularisée par la saisine régulière du tribunal administratif par l'autorité compétente ; <br/>
<br/>
              2. Considérant, d'autre part, qu'il résulte du paragraphe III de l'article 1er de la loi du 31 décembre 1991 portant diverses dispositions en matière de transports, applicable au litige, que l'établissement public Voies navigables de France est substitué à l'Etat dans l'exercice des pouvoirs dévolus à ce dernier pour la répression des atteintes à l'intégrité et à la conservation du domaine public fluvial qui lui est confié ; qu'en vertu du paragraphe IV de cet article, dans le cas où des atteintes sont constatées, le tribunal administratif territorialement compétent est saisi par le président de Voies navigables de France, le directeur général de cet établissement public s'il a reçu délégation de signature ou les chefs des services extérieurs, qui sont les représentants locaux de l'établissement public, s'ils ont reçu du directeur général une subdélégation de signature ; <br/>
<br/>
              3. Considérant qu'il résulte de la combinaison de ces dispositions que le tribunal administratif est régulièrement saisi du procès-verbal de contravention de grande voirie constatée sur le domaine public fluvial par la transmission par l'une de ces autorités de l'acte de notification du procès-verbal ainsi que de la citation à comparaître dès lors que la délégation de signature, quand elle est nécessaire, a régulièrement reçu publication ; que cette transmission régularise la procédure lorsque le procès-verbal n'a pas été régulièrement notifié au contrevenant ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que si l'acte du 11 mai 2007 portant notification du procès-verbal de contravention de grande voirie dressé le 13 avril par M. Ghislain , chef d'équipe des travaux publics de l'Etat, et citation à comparaître de Mme A a été signé par M. Daniel , chef de la subdivision par intérim de Voies navigables de France, la demande, enregistrée au tribunal administratif de Versailles le 31 août 2007, a été signée par Mme Marie-Anne , directrice interrégionale de Voies navigables de France, chef du service de la navigation de la Seine ; que, par décision du 27 avril 2007, le directeur général de cet établissement public, ayant lui-même reçu délégation de signature du président, a subdélégué sa signature à Mme  pour saisir le tribunal administratif dans le cas où des atteintes à l'intégrité et à la conservation du domaine public fluvial seraient constatées ; que cette décision a été publiée au bulletin officiel du ministère des transports, de l'équipement, du tourisme et de la mer et au bulletin officiel des actes de Voies navigables de France ; <br/>
<br/>
              5. Considérant, par suite, qu'en jugeant, alors qu'elle a relevé que Mme  avait régulièrement reçu une subdélégation de signature, que la procédure n'avait pu être régularisée par la transmission au tribunal administratif par la directrice interrégionale de Voies navigables de France, chef du service de la navigation de la Seine, du procès-verbal constatant l'infraction et la notification de ce document citant le contrevenant à comparaître, la cour administrative d'appel de Versailles a commis une erreur de droit ; que, dès lors et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'établissement public Voies navigables de France est fondé à demander l'annulation de l'article 1er de l'arrêt attaqué ; <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui a été dit ci-dessus que Mme  avait régulièrement reçu une subdélégation de signature pour saisir le tribunal administratif ;  qu'il n'est pas établi qu'elle n'aurait pas signé personnellement la demande présentée au tribunal  ; que, par suite, l'établissement public est fondé à soutenir que c'est à tort que le tribunal administratif de Versailles a accueilli la fin de non-recevoir tirée de l'incompétence de la signataire de la demande l'ayant saisi ; que, par suite, son jugement doit être annulé ; <br/>
<br/>
              8. Considérant qu'il y a lieu d'évoquer et de statuer immédiatement sur la demande présentée par l'établissement public Voies navigables de France devant le tribunal administratif ;<br/>
<br/>
              9. Considérant qu'aux termes de l'article L. 2132-9 du code général de la propriété des personnes publiques : " Les riverains, les mariniers et autres personnes sont tenus de faire enlever les pierres, terres, bois, pieux, débris de bateaux et autres empêchements qui, de leur fait ou du fait de personnes ou de choses à leur charge, se trouveraient sur le domaine public fluvial. Le contrevenant est passible d'une amende de 150 à 12 000 euros, de la confiscation de l'objet constituant l'obstacle et du remboursement des frais d'enlèvement d'office par l'autorité administrative compétente " ;<br/>
<br/>
              10. Considérant que le procès-verbal, établi le 30 avril 2007 sur le fondement des dispositions précitées de l'article L. 2132-9 du code général de la propriété des personnes publiques,  mentionne que le bateau dénommé " Amazone " dont Mme A est propriétaire, stationne, sans autorisation, en rive gauche de la Seine, au point PK 12.8 sur la commune de Sèvres, au garage à bateaux de Saint-Cloud ; que ce fait est constitutif d'une contravention de grande voirie ; <br/>
<br/>
              11. Considérant, en premier lieu, que si Mme A soutient que la procédure de contravention de grande voirie ne pouvait être poursuivie que par la personne publique propriétaire du domaine, l'établissement public Voies navigables de France est substitué à l'Etat pour assurer l'intégrité du domaine public fluvial en vertu des dispositions précitées de la loi du 31 décembre 1991 ; <br/>
<br/>
              12. Considérant, en deuxième lieu, que si Mme A soutient que le procès-verbal de contravention de grande voirie est entachée d'inexactitude matérielle quant à l'emplacement du bateau et en particulier que le garage à bateaux de Saint-Cloud n'existe pas, elle ne l'établit pas ; <br/>
<br/>
              13. Considérant, en troisième lieu, que Mme A n'est pas fondée à invoquer la prescription de l'action publique en raison de l'absence d'acte d'instruction entre le 8 novembre 2007 et le 5 novembre 2009 dès lors qu'entre ces deux dates, et notamment le 7 avril 2008 et le 23 décembre 2008, les mémoires qu'elle a produits devant le tribunal administratif, qui ont été communiqués à l'établissement public, ont interrompu le délai de prescription d'un an applicable aux contraventions de grande voirie  ;<br/>
<br/>
              14. Considérant, en quatrième lieu, que Mme A ne peut utilement invoquer la tolérance dont elle a bénéficié antérieurement de la part de cet établissement public, ni se prévaloir de la circonstance qu'elle lui verse des indemnités d'occupation; que si elle se prévaut de l'absence de publication des actes concernant les règles de stationnement des bateaux-logement, et notamment des règles afférentes à la gestion des listes d'attente, cette omission, à la supposer établie, ne caractérise pas un fait de l'administration de nature à exonérer la contrevenante ; <br/>
<br/>
              15. Considérant, enfin, que l'obligation de déplacer le bateau appartenant à Mme A ne porte pas une atteinte disproportionnée à son droit de mener une vie familiale normale ; que, par suite, le moyen tiré de la violation de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; que, de même, une telle mesure ne porte pas en elle-même atteinte au droit au domicile régi par l'article 102 du code civil ;  <br/>
<br/>
              16. Considérant qu'il résulte de tout ce qui précède que l'établissement public Voies navigables de France est fondé à demander au titre de l'action publique que Mme A soit condamnée au paiement d'une amende de 1500 euros ; qu'en revanche, il résulte de l'instruction et il n'est pas contesté que le bateau dénommé " Amazone" a quitté son emplacement en juillet 2008 ; que, dès lors, il n'y a pas lieu de faire droit aux conclusions présentées par l'établissement public au titre de l'action domaniale et d'enjoindre à Mme A de procéder à l'enlèvement de ce bateau du domaine public fluvial dans un délai de huit jours sous astreinte  ;    <br/>
<br/>
              17. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme A la somme de 1500 euros à verser à l'établissement public Voies navigables de France au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'il soit fait droit aux conclusions présentées au même titre par Mme A ;<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
 Article 1er : L'article 1er de l'arrêt de la cour administrative d'appel de Versailles du 28 janvier 2010 et le jugement du tribunal administratif de Versailles du 5 février 2009 sont annulés.<br/>
<br/>
 Article 2 : Mme A est condamnée au paiement d'une amende de 1 500 euros.<br/>
<br/>
Article 3 : Mme A versera une somme de 1 500 euros à l'établissement public Voies navigables de France au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Le surplus des conclusions de la demande présentée par l'établissement public Voies navigables de France est rejeté.<br/>
<br/>
Article 5 : Les conclusions de Mme A présentées sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 6 : La présente décision sera notifiée à l'établissement public Voies navigables de France  et à Mme Mireille A.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
