<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038670505</ID>
<ANCIEN_ID>JG_L_2019_06_000000420083</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/67/05/CETATEXT000038670505.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 21/06/2019, 420083, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-06-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420083</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CAPRON</AVOCATS>
<RAPPORTEUR>M. Sylvain Humbert</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:420083.20190621</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...C...a demandé au tribunal administratif de Nice de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu et des rappels de taxe sur la valeur ajoutée qui lui ont été réclamés au titre des années 2010, 2011 et 2012 ainsi que des pénalités correspondantes. Par trois jugements n° 1502186, n° 1502185 et nos 1405079, 1405080 du 10 novembre 2016, le tribunal administratif de Nice a rejeté ses demandes.<br/>
<br/>
              Par un arrêt nos 17MA00225, 17MA00256, 17MA00257 du 27 février 2018, la cour administrative d'appel de Marseille a rejeté les appels formés contre ces jugements.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 avril et 20 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, M. C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il statue sur les impositions dues au titre de 2010 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sylvain Humbert, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Capron, avocat de M. C...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'une vérification de comptabilité portant sur les années 2010, 2011 et 2012, M. C..., qui exerce une activité d'agent commercial, s'est vu réclamer, selon la procédure de rectification contradictoire, des cotisations supplémentaires d'impôt sur le revenu et, selon la procédure de taxation d'office, des rappels de taxe sur la valeur ajoutée. Par trois jugements du 10 novembre 2016, le tribunal administratif de Nice a rejeté les demandes présentées par M. C...tendant à la décharge de ces suppléments d'impôt. Compte tenu des termes de son pourvoi, M. C... doit être regardé comme demandant l'annulation de l'arrêt du 27 février 2018 en tant que la cour administrative d'appel de Marseille a rejeté son appel contre le jugement du tribunal administratif de Nice statuant sur les cotisations supplémentaires d'impôt sur le revenu auxquelles il a été assujetti au titre de l'année de 2010 et les rappels de taxe sur la valeur ajoutée qui lui ont été réclamés pour la même période.<br/>
<br/>
              2. Aux termes de l'article L. 57 du livre des procédures fiscales : " L'administration adresse au contribuable une proposition de rectification qui doit être motivée de manière à lui permettre de formuler ses observations ou de faire connaître son acceptation (...) ". Aux termes de l'article L. 76 du même code : " Les bases ou éléments servant au calcul des impositions d'office et leurs modalités de détermination sont portées à la connaissance du contribuable trente jours au moins avant la mise en recouvrement des impositions ". Il résulte de ces dispositions que les rectifications doivent être notifiées au contribuable. En cas de contestation sur ce point, il incombe à l'administration fiscale d'établir qu'une telle notification a été régulièrement adressée au contribuable et, lorsque le pli contenant cette notification a été renvoyé par le service postal au service expéditeur, de justifier de la régularité des opérations de présentation à l'adresse du destinataire. La preuve qui lui incombe ainsi peut résulter soit des mentions précises, claires et concordantes figurant sur les documents, le cas échéant électroniques, remis à l'expéditeur conformément à la règlementation postale soit, à défaut, d'une attestation de l'administration postale ou d'autres éléments de preuve établissant la délivrance par le préposé du service postal d'un avis de passage prévenant le destinataire de ce que le pli est à sa disposition au bureau de poste.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que le pli contenant la proposition de rectification en date du 5 décembre 2012 a été expédié par l'administration fiscale à l'adresse non contestée de M. C...située au 119 avenue de Brancolar à Nice. Si ce pli porte la mention d'une vaine présentation le 12 décembre 2013, confirmée par une attestation du bureau de poste du 31 janvier 2014, et si ce pli a été retourné à l'expéditeur le 30 décembre 2013 faute d'avoir été réclamé après l'expiration du délai de mise en instance, M. C... a produit une attestation datée du 3 janvier 2014 de M. D...A...se présentant comme le facteur ayant procédé à la distribution du courrier le 12 décembre 2013, sans que cette qualité ait été contestée par le ministre, indiquant avoir déposé par erreur l'avis d'instance correspondant au pli litigieux à l'adresse d'un homonyme de M.C..., domicilié.... Il a également produit une attestation de ce dernier, datée du 12 décembre 2016, confirmant l'erreur de distribution commise. Figure également au dossier une copie de la lettre de réclamation du 15 janvier 2014 adressée par le contribuable au bureau de poste de son secteur faisant état d'un courrier recommandé non distribué à la bonne adresse portant le numéro du pli en litige ainsi que les divers courriers de réponse faisant état d'un échec des recherches, en contradiction avec l'attestation du 31 janvier 2014, mentionnée ci-dessus, adressée à l'administration fiscale. En jugeant, au vu de l'ensemble de ces éléments, qu'il n'était pas établi par des pièces présentant un caractère probant suffisant que l'avis de passage avait, par erreur, été déposé dans la boîte aux lettres d'un voisin portant le même nom patronymique que le requérant, la cour a, dans les circonstances particulières de l'espèce, dénaturé les pièces du dossier. Par suite, sans qu'il soit besoin de statuer sur les autres moyens du pourvoi, M. C... est fondé à demander l'annulation de l'arrêt qu'il attaque en tant qu'il a statué sur les cotisations supplémentaires d'impôt sur le revenu au titre de l'année 2010 et les rappels de taxe sur la valeur ajoutée réclamés au titre de la période correspondante.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M.C..., au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 27 février 2018 est annulé en tant qu'il statue sur les cotisations supplémentaires d'impôt sur le revenu au titre de l'année 2010 et les rappels de taxe sur la valeur ajoutée réclamés au titre de la période correspondante.<br/>
Article 2 : L'affaire est renvoyée, dans la mesure de la cassation prononcée à l'article 1er, devant la cour administrative d'appel de Marseille.<br/>
Article 3: L'Etat versera à M. C...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M.B... C... et au ministre de l'action et des comptes publics.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
