<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044487260</ID>
<ANCIEN_ID>JG_L_2021_12_000000458307</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/48/72/CETATEXT000044487260.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 08/12/2021, 458307, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>458307</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET ROUSSEAU ET TAPIE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:458307.20211208</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés les 9, 26 et 30 novembre 2021 au secrétariat du contentieux du Conseil d'Etat, M. B... A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution du décret du Président de la République du 6 septembre 2021 en tant que ce décret ne l'a pas nommé professeur des universités - praticien hospitalier ;<br/>
<br/>
              2°) d'enjoindre à l'Etat de le réintégrer provisoirement dans la liste des professeurs des universités-praticiens hospitaliers nommés et titularisés ou, à titre subsidiaire, de réexaminer son dossier dans un délai de quinze jours à compter de la notification de l'ordonnance à intervenir ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est satisfaite dès lors que le décret contesté préjudicie de manière suffisamment grave et immédiate à sa situation financière et professionnelle en ce que, d'une part, il entraîne une baisse substantielle de sa rémunération et, d'autre part, il porte atteinte à sa réputation professionnelle et est susceptible de compromettre sa future carrière de praticien de l'hôpital public ; <br/>
              - il existe un doute sérieux quant à la légalité du décret contesté ; <br/>
              - ce décret est entaché d'un vice de forme dès lors qu'il n'a pas été contresigné par le ministre de l'enseignement supérieur, de la recherche et de l'innovation ; <br/>
              - la procédure d'enquête réalisée par la commission de déontologie méconnaît les dispositions de l'article 65 de la loi du 22 avril 1905 dès lors qu'il a été privé d'une garantie substantielle en étant mis dans l'impossibilité de présenter des observations utiles pour sa défense aux enquêteurs, en ce qu'il n'a pas pu avoir accès aux informations concernant le dossier d'enquête, malgré de nombreuses sollicitations en ce sens ; <br/>
              - le décret est entaché d'un vice de procédure dès lors que, d'une part, il constitue une sanction disciplinaire déguisée et, d'autre part, les formalités applicables en matière disciplinaire n'ont pas été respectées ; <br/>
              - ce décret est entaché d'un vice de procédure au regard des dispositions de l'arrêté du 17 septembre 1987 dès lors que, d'une part, la procédure de recrutement des professeurs des universités - praticiens hospitaliers n'a pas été respectée et, d'autre part, le conseil de l'unité de formation et de recherche et la commission médicale d'établissement ont rendu des avis favorables ou, s'ils ont rendu des avis défavorables, ceux-ci n'ont pas été joints au dossier et ont été émis dans des conditions irrégulières ; <br/>
              - il est entaché d'un défaut de motivation ; <br/>
              - il est entaché d'incompétence négative dès lors que le Président de la République, qui ne se trouvait pas dans une situation de compétence liée, aurait dû procéder à une analyse individuelle de sa situation professionnelle ; <br/>
              - le décret attaqué est entaché d'un détournement de pouvoir et de procédure dès lors qu'il constitue une sanction disciplinaire déguisée ;<br/>
              - il méconnaît le principe général d'égal accès à l'emploi public dès lors que l'appréciation portée sur sa candidature résulte de la saisine de la commission de déontologie, à l'occasion de laquelle il n'a pas pu faire valoir ses observations ; <br/>
              - ce décret est entaché d'une erreur manifeste d'appréciation en ce que la décision est disproportionnée au regard des faits qui lui sont reprochés, qui n'ont pas été matériellement établis.<br/>
<br/>
              Par un mémoire en défense et un nouveau mémoire, enregistrés les 24 et 30 novembre 2021, la ministre de l'enseignement supérieur, de la recherche et de l'innovation conclut au rejet de la requête. Elle soutient que la condition d'urgence n'est pas satisfaite, et que les moyens soulevés ne sont pas fondés. <br/>
<br/>
              La requête a été communiquée au Premier ministre qui n'a pas produit d'observations. <br/>
<br/>
              La requête a été communiquée au centre hospitalier régional et universitaire de Rouen qui n'a pas produit d'observations.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. B... A..., et d'autre part, le Premier ministre et le ministre de l'enseignement supérieur, de la recherche et de l'innovation ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 26 novembre 2021, à 14 heures 30 : <br/>
<br/>
              - Me Rousseau, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. A... ;<br/>
<br/>
              - les représentants du ministre de l'enseignement supérieur, de la recherche et de l'innovation ; <br/>
<br/>
              à l'issue de laquelle le juge des référés a reporté la clôture de l'instruction au 30 novembre 2021 à 17 heures ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 1er décembre 2021, présentée par M. A... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              2. L'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. L'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'espèce.<br/>
<br/>
              3. M. A... demande, d'une part, la suspension de l'exécution du décret du Président de la République du 6 septembre 2021 en tant qu'il ne le nomme pas professeur des universités - praticien hospitalier au centre hospitalier régional et universitaire de Rouen sur un emploi vacant au sein de ce centre, d'autre part, d'enjoindre à l'administration de le réintégrer provisoirement dans la liste des professeurs universitaires - praticiens hospitaliers nommés et titularisés ou, à titre subsidiaire, de réexaminer son dossier dans un délai de quinze jours à compter de la notification de l'ordonnance à intervenir.<br/>
<br/>
              4. Pour justifier de l'urgence à ordonner cette suspension au sens des dispositions de l'article L. 521-1 du code de justice administrative, M. A... soutient, d'une part, que le décret attaqué engendre une baisse substantielle de sa rémunération et, d'autre part, qu'il porte atteinte à sa réputation professionnelle et est, ainsi, susceptible de compromettre sa future carrière de praticien à l'hôpital public.<br/>
<br/>
              5. D'une part, il résulte de l'instruction et des informations recueillies lors de l'audience que M. A..., à la suite de la non-reconduction de son contrat au centre hospitalier universitaire de Rouen, a retrouvé un emploi en tant que gynécologue-obstétricien, responsable clinique du centre de procréation médicalement assistée, au centre hospitalier Sud Francilien. S'il aurait perçu une rémunération plus élevée s'il avait été nommé professeur des universités - praticien hospitalier au centre hospitalier régional universitaire de Rouen, cet écart de rémunération n'est pas de nature à caractériser une situation d'urgence au sens de l'article L. 521-1 du code de justice administrative.<br/>
<br/>
              6. D'autre part, la décision contestée, dont il ne résulte pas de l'instruction qu'elle aurait été rendue publique et qui n'a pas empêché, comme il est dit ci-dessus, M. A... de retrouver un emploi au centre hospitalier Sud Francilien, ne porte pas à sa réputation professionnelle une atteinte d'une ampleur telle que cette condition d'urgence serait satisfaite.<br/>
<br/>
              7. Il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur la condition tenant à l'existence d'un doute sérieux sur la légalité de la décision contestée, la requête de M. A... doit être rejetée, y compris ses conclusions aux fins d'injonction et ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B... A... et à la ministre de l'enseignement supérieur, de la recherche et de l'innovation.<br/>
Copie en sera adressée au centre hospitalier universitaire de Rouen.<br/>
<br/>
Fait à Paris, le 8 décembre 2021<br/>
Signé : Philippe Josse<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
