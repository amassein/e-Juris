<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041986881</ID>
<ANCIEN_ID>JG_L_2020_06_000000431194</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/98/68/CETATEXT000041986881.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 10/06/2020, 431194</TITRE>
<DATE_DEC>2020-06-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431194</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Jean-Yves Ollier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:431194.20200610</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Les sociétés Erics Associés et Altaris ont demandé au tribunal administratif de Rennes de condamner l'Etat à leur verser la somme de 218 400 euros en réparation du préjudice causé par leur éviction irrégulière du marché à bons de commande conclu le 24 juin 2014 entre le ministre de la défense et la société AFC Formation en vue de la réalisation de prestations de formation " achats publics " au bénéfice du personnel militaire et civil du service du commissariat des armées (SCA) et des commissaires affectés hors SCA. Par un jugement n° 1501876 du 20 avril 2017, le tribunal administratif de Rennes a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 17NT01869 du 29 mars 2019, la cour administrative d'appel de Nantes a, sur appel des sociétés Erics Associés et Altaris, annulé le jugement du tribunal administratif de Rennes, condamné l'Etat à verser à ces sociétés la somme de 4 800 euros et rejeté le surplus de leur requête.<br/>
<br/>
              Par un pourvoi enregistré le 29 mai 2019 au secrétariat du contentieux du Conseil d'Etat, la ministre des armées demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel des sociétés Erics Associés et Altaris. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Yves Ollier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat des sociétés Erics Associés et Altaris ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le ministre de la défense a lancé la passation d'un marché à bons de commande, selon la procédure adaptée, en vue de la réalisation, notamment, de prestations de formation " achats publics ", divisées en sept lots géographiques. Les offres du groupement constitué des sociétés Erics Associés et Altaris ont été rejetées. Par un jugement du 30 avril 2017, le tribunal administratif de Rennes a rejeté les demandes de ces sociétés tendant à la condamnation de l'Etat à leur verser la somme de 218 400 euros en réparation du préjudice causé par leur éviction de ce marché. La ministre des armées se pourvoit en cassation contre l'arrêt du 29 mars 2019 par lequel la cour administrative d'appel de Nantes a, sur appel des sociétés Erics Associés et Altaris, annulé ce jugement, condamné l'Etat à leur verser la somme de 4 800 euros et rejeté le surplus de leur requête d'appel.<br/>
<br/>
              2. D'une part, lorsqu'un candidat à l'attribution d'un marché public demande la réparation du préjudice né de son éviction irrégulière de ce contrat et qu'il existe un lien direct de causalité entre la faute résultant de l'irrégularité et les préjudices invoqués par le requérant à cause de son éviction, il appartient au juge de vérifier si le candidat était ou non dépourvu de toute chance de remporter le contrat. En l'absence de toute chance, il n'a droit à aucune indemnité. Dans le cas contraire, il a droit en principe au remboursement des frais qu'il a engagés pour présenter son offre. Il convient en outre de rechercher si le candidat irrégulièrement évincé avait des chances sérieuses d'emporter le contrat conclu avec un autre candidat. Si tel est le cas, il a droit à être indemnisé de son manque à gagner, qui inclut nécessairement, puisqu'ils ont été intégrés dans ses charges, les frais de présentation de l'offre, lesquels n'ont donc pas à faire l'objet, sauf stipulation contraire du contrat, d'une indemnisation spécifique. En revanche, le candidat ne peut prétendre à une indemnisation de ce manque à gagner si la personne publique renonce à conclure le contrat pour un motif d'intérêt général.<br/>
<br/>
              3. D'autre part, le I de l'article 53 du code des marchés publics, applicable à la date de passation du marché litigieux, disposait que : " Pour attribuer le marché au candidat qui a présenté l'offre économiquement la plus avantageuse, le pouvoir adjudicateur se fonde :/ 1° Soit sur une pluralité de critères non discriminatoires et liés à l'objet du marché, notamment la qualité, le prix, la valeur technique, le caractère esthétique et fonctionnel, les performances en matière de protection de l'environnement, les performances en matière de développement des approvisionnements directs de produits de l'agriculture, les performances en matière d'insertion professionnelle des publics en difficulté, le coût global d'utilisation, les coûts tout au long du cycle de vie, la rentabilité, le caractère innovant, le service après-vente et l'assistance technique, la date de livraison, le délai de livraison ou d'exécution, la sécurité d'approvisionnement, l'interopérabilité et les caractéristiques opérationnelles. D'autres critères peuvent être pris en compte s'ils sont justifiés par l'objet du marché. 2° Soit, compte tenu de l'objet du marché, sur un seul critère, qui est celui du prix ". Il résulte de ces dispositions qu'il appartient au pouvoir adjudicateur de déterminer l'offre économiquement la plus avantageuse en se fondant sur des critères permettant d'apprécier la performance globale des offres au regard de ses besoins. Ces critères doivent être liés à l'objet du marché ou à ses conditions d'exécution, être définis avec suffisamment de précision pour ne pas laisser une marge de choix indéterminée et ne pas créer de rupture d'égalité entre les candidats. Le pouvoir adjudicateur détermine librement la pondération des critères de choix des offres. Toutefois, il ne peut légalement retenir une pondération, en particulier pour le critère du prix ou du coût, qui ne permettrait manifestement pas, eu égard aux caractéristiques du marché, de retenir l'offre économiquement la plus avantageuse.<br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond qu'alors même que le marché en cause était un marché à procédure adaptée, soumis à une simple obligation de hiérarchisation des critères, le ministère des armées avait décidé de procéder à la pondération des critères de choix du marché. Le règlement de la consultation prévoyait que les offres seraient appréciées au regard d'un critère de valeur technique pondéré à 90 % et d'un critère de prix pondéré à 10 %. Il résulte de ce qui a été dit au point 3 ci-dessus qu'en jugeant qu'une telle pondération était irrégulière au motif qu'elle était " particulièrement disproportionnée ", que le ministre de la défense n'en établissait pas la nécessité et qu'elle conduisait à " neutraliser manifestement " le critère du prix, la cour administrative d'appel de Nantes a commis une erreur de droit. <br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la ministre des armées est fondée à demander l'annulation de l'arrêt attaqué.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 29 mars 2019 de la cour administrative d'appel de Nantes est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
Article 3 : Les conclusions des sociétés Erics Associés et Ataris présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la ministre des armées et aux sociétés Erics Associés et Altaris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-02-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. FORMALITÉS DE PUBLICITÉ ET DE MISE EN CONCURRENCE. - MÉTHODE DE NOTATION - SÉLECTION DE L'OFFRE ÉCONOMIQUEMENT LA PLUS AVANTAGEUSE (ART. 53 DU CMP) - CHOIX ET PONDÉRATION DES CRITÈRES - CONDITIONS [RJ1].
</SCT>
<ANA ID="9A"> 39-02-005 Il résulte du I de l'article 53 du code des marchés publics (CMP) qu'il appartient au pouvoir adjudicateur de déterminer l'offre économiquement la plus avantageuse en se fondant sur des critères permettant d'apprécier la performance globale des offres au regard de ses besoins. Ces critères doivent être liés à l'objet du marché ou à ses conditions d'exécution, être définis avec suffisamment de précision pour ne pas laisser une marge de choix indéterminée et ne pas créer de rupture d'égalité entre les candidats. Le pouvoir adjudicateur détermine librement la pondération des critères de choix des offres. Toutefois, il ne peut légalement retenir une pondération, en particulier pour le critère du prix ou du coût, qui ne permettrait manifestement pas, eu égard aux caractéristiques du marché, de retenir l'offre économiquement la plus avantageuse.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de la nécessité que les critères retenus permettent de déterminer l'offre économiquement la plus avantageuse, CE, 28 avril 2006, Commune de Toulouse, n° 280197, T. p. 948 ; s'agissant de la même exigence quant à leur pondération, CJCE, 4 décembre 2003, EVN et Wienstrom, C-448/01, Rec. p. I-14527.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
