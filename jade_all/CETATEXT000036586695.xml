<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036586695</ID>
<ANCIEN_ID>JG_L_2018_02_000000411557</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/58/66/CETATEXT000036586695.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 08/02/2018, 411557, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-02-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411557</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Bertrand Mathieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:411557.20180208</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 15 juin 2017 au secrétariat du contentieux du Conseil d'Etat, Mme A...B...demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret du 3 avril 2017 qui a rapporté le décret du 19 janvier 2005 en ce qu'il lui avait accordé la nationalité française.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le décret n° 93-1362 du 30 décembre 1993 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bertrand Mathieu, conseiller d'Etat en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes de l'article 27-2 du code civil : " Les décrets portant acquisition, naturalisation ou réintégration peuvent être rapportés sur avis conforme du Conseil d'Etat dans le délai de deux ans à compter de leur publication au Journal officiel si le requérant ne satisfait pas aux conditions légales ; si la décision a été obtenue par mensonge ou fraude, ces décrets peuvent être rapportés dans le délai de deux ans à partir de la découverte de la fraude " ; <br/>
<br/>
              2.	Considérant qu'il ressort des pièces du dossier que Mme A...B..., ressortissante marocaine, a déposé une demande de naturalisation le 20 avril 2003 par laquelle elle a indiqué être célibataire, sans enfant et résider au domicile de ses parents à Maubeuge ; qu'elle a réitéré ces déclarations lors de l'entretien d'assimilation le 29 avril 2003 et a déclaré sur l'honneur le 22 juin 2004 qu'aucune modification n'avait affecté sa situation personnelle et familiale depuis le dépôt initial de sa demande de naturalisation ; qu'au vu de ses déclarations, elle a été naturalisée par un décret du 19 janvier 2005 ; que, toutefois, le 10 avril 2015, le ministre des affaires étrangères et du développement international a informé le ministre chargé des naturalisations que Mme B...avait épousé au Maroc, le 20 juillet 1996, un ressortissant marocain résidant habituellement au Maroc ; que, par le décret attaqué, le Premier ministre a rapporté le décret prononçant la naturalisation de Mme B...au motif qu'il avait été pris au vu d'informations mensongères délivrées par l'intéressée quant à sa situation familiale ; <br/>
<br/>
              3.	Considérant qu'aux termes de l'article 21-16 du code civil : " Nul ne peut être naturalisé s'il n'a en France sa résidence au moment de la signature du décret de naturalisation " ; qu'il résulte de ces dispositions que la demande de naturalisation n'est pas recevable lorsque l'intéressé n'a pas fixé en France de manière durable le centre de ses intérêts ; que, pour apprécier si cette dernière condition est remplie, l'autorité administrative peut notamment prendre en compte, sous le contrôle du juge de l'excès de pouvoir, la situation familiale en France de l'intéressé ; que, par suite, la circonstance que la requérante ait été  mariée au Maroc où elle résidait avec le premier de ses enfants était de nature à modifier l'appréciation qui a été portée par l'autorité administrative sur la fixation du centre de ses intérêts ; <br/>
<br/>
              4.	Considérant qu'il ressort en l'espèce des pièces du dossier que Mme B... était mariée depuis le 20 juillet 1996 avec un ressortissant marocain avec lequel elle avait eu un enfant avant qu'elle n'acquière la nationalité française ; qu'elle n'a pas fait connaître à l'administration en charge de l'instruction de sa demande de naturalisation la réalité de sa situation familiale ; que MmeB..., qui maîtrise la langue française ainsi qu'il ressort du procès-verbal d'assimilation établi le 29 avril 2003, ne pouvait se méprendre sur la portée de l'engagement sur l'honneur qu'elle a signé en déposant sa demande de naturalisation ; qu'elle doit ainsi être regardée comme ayant sciemment dissimulé la réalité de sa situation familiale ; que, par suite, en rapportant le décret ayant prononcé sa naturalisation, le Premier ministre n'a pas fait une inexacte application des dispositions de l'article 27-2 du code civil ;<br/>
<br/>
              5.	Considérant que la circonstance alléguée par MmeB..., selon laquelle elle aurait rencontré des difficultés avec son mari avant de demander la nationalité française et aurait eu l'intention de vivre près de sa famille en France, est dépourvue d'incidence sur la légalité du décret attaqué ; <br/>
<br/>
              6.	Considérant qu'il résulte de ce qui précède que Mme B...n'est pas fondée à demander l'annulation pour excès de pouvoir du décret qu'elle attaque ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme B...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme A...B...et au ministre d'Etat, ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
