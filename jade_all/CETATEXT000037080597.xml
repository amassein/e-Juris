<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037080597</ID>
<ANCIEN_ID>JG_L_2018_06_000000414532</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/08/05/CETATEXT000037080597.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 18/06/2018, 414532</TITRE>
<DATE_DEC>2018-06-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414532</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:414532.20180618</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et deux mémoires en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat le 22 septembre 2017 et les 21 février et 18 mai 2018, la société C8 demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du Conseil supérieur de l'audiovisuel (CSA) n° 2017-532 du 26 juillet 2017 lui infligeant une sanction pécuniaire d'un montant de 3 millions d'euros ; <br/>
<br/>
              2°) de mettre à la charge du CSA une somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la Constitution, et notamment son Préambule ;<br/>
<br/>
              - la convention européenne de sauvegarde des droits de l'homme ;<br/>
<br/>
              - la loi n° 86-1067 du 30 septembre 1986 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la société C8  et à la SCP Baraduc, Duhamel, Rameix, avocat du Conseil supérieur de l'audiovisuel.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 30 mai 2018 présentée par le Conseil supérieur de l'audiovisuel.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant, d'une part, qu'aux termes de l'article 3-1 de la loi du 30 septembre 1986 relative à la liberté de communication : " (...) Le Conseil supérieur de l'audiovisuel contribue aux actions en faveur de la cohésion sociale et à la lutte contre les discriminations dans le domaine de la communication audiovisuelle (...) " ; qu'aux termes du premier alinéa  de l'article 42 de la même loi : " Les éditeurs et distributeurs de services de communication audiovisuelle et les opérateurs de réseaux satellitaires peuvent être mis en demeure de respecter les obligations qui leur sont imposées par les textes législatifs et réglementaires et par les principes définis aux articles 1er et 3-1 " ; qu'aux termes de l'article 42-1 : " Si la personne faisant l'objet de la mise en demeure ne se conforme pas à celle-ci, le Conseil supérieur de l'audiovisuel peut prononcer à son encontre, compte tenu de la gravité du manquement, et à la condition que celui-ci repose sur des faits distincts ou couvre une période distincte de ceux ayant déjà fait l'objet d'une mise en demeure, une des sanctions suivantes : / (...) 3° Une sanction pécuniaire assortie éventuellement d'une suspension de l'édition ou de la distribution du ou des services ou d'une partie du programme ; (...) " ; <br/>
<br/>
              2. Considérant, d'autre part, qu'aux termes de l'article 4-2-2 de la convention relative au service de télévision " Direct 8 ", devenu C8, conclue le 10 juin 2003 entre le Conseil supérieur de l'audiovisuel (CSA) et la société Bolloré Médias, aux droits de laquelle est venue la société C8, sur le fondement de l'article 28 de la loi du 30 septembre 1986, le conseil supérieur peut, si l'éditeur ne se conforme pas aux mises en demeure de respecter les obligations prévues par cette convention, " compte tenu de la gravité du manquement, prononcer l'une des sanctions suivantes : 1° une sanction pécuniaire dont le montant ne peut dépasser le montant prévu à l'article 42-2 de la loi du 30 septembre 1986, si le manquement n'est pas constitutif d'une infraction pénale (...) " ; que l'article 4-2-4 de la convention prévoit que les sanctions mentionnées à ses articles 4-2-2 et 4-2-3 sont prononcées dans le respect des garanties fixées par les articles 42 et suivants de la loi du 30 septembre 1986 ;<br/>
<br/>
              3. Considérant que, lors de l'émission " TPMP ! Baba hot line " diffusée le 18 mai 2017 à partir de 23 h 05 par le service C8, l'animateur a diffusé en direct, les conversations téléphoniques qu'il a eues avec des personnes des deux sexes, mais très majoritairement de sexe masculin, répondant à une fausse petite annonce préalablement publiée sur un site de rencontres et présentant l'auteur de l'annonce comme bisexuel ; que, par une décision adoptée lors de sa séance du 26 juillet  2017, le CSA a estimé que ces faits étaient constitutifs d'un manquement aux obligations découlant, d'une part, de l'article 2-3-3 de la convention du 10 juin 2003, aux termes duquel l'éditeur " veille dans son programme à promouvoir les valeurs d'intégration et de solidarité qui sont celles de la République et à lutter contre les discriminations " et, d'autre part, de l'article 2-3-4 de la même convention, aux termes duquel l'éditeur " respecte les droits de la personne relatifs à sa vie privée, son image, son honneur et sa réputation tels qu'ils sont définis par la loi et la jurisprudence " ; que, constatant que des faits de même nature avait donné lieu à une mise en demeure en date du 30 mars 2010, le conseil supérieur a décidé d'infliger à la société C8, en sa qualité d'éditeur du service, une sanction pécuniaire d'un montant de 3 millions d'euros ; que la société C8 demande l'annulation de cette décision ; <br/>
<br/>
              4. Considérant qu'aux termes de l'article 42-7 de la loi du 30 septembre 1986, applicable aux sanctions prises par le CSA en application de l'article 42-1 précité :  " (...) La décision du conseil prise au terme de cette procédure est motivée (...) " ; que le CSA, qui n'avait pas à détailler les motifs pour lesquels il écartait l'invocation de la liberté d'expression, a par la décision attaquée, indiqué de manière suffisamment précise les stipulations de la convention du service " Direct 8 ", devenu C8, exploité par la société requérante, qu'il a estimé avoir été méconnues ainsi que les faits reprochés à la requérante ;<br/>
<br/>
              5. Considérant que, d'une part, le CSA a pu régulièrement s'appuyer, pour prendre à l'égard de C8 la sanction édictée par la décision attaquée, sur la mise en demeure n° 2010-196 du 30 mars 2010, concernant le respect de l'intimité de la vie privée des personnes, nonobstant l'ancienneté de cette décision ; que, d'autre part, la mise en demeure n° 2015-274 du 1er juillet 2015, si elle se rapporte à des faits par nature différents, concerne la mise en oeuvre par le service de son engagement de contribuer à la lutte contre les discriminations et pouvait régulièrement servir de base à l'édiction de la sanction attaquée, qui est notamment relative à l'exécution du même engagement ;<br/>
<br/>
              6. Considérant qu'en l'absence de tout procédé technique destiné à rendre méconnaissables les voix des personnes mises à l'antenne, sans qu'elles y aient consenti ni même qu'elles aient été avisées de la diffusion de conversations qu'elles pouvaient au contraire légitimement croire particulières, ces personnes ont été exposées au risque d'être reconnues, principalement par des membres de leur famille ou de leur entourage, eu égard notamment à certaines informations personnelles qu'elles avaient été engagées à livrer, concernant par exemple leur lieu de résidence, leur âge ou leur profession ; que l'animateur a incité ces personnes à tenir des propos d'une crudité appuyée dévoilant leur intimité et exposant leur vie privée alors même qu'elles ne pouvaient imaginer que leurs propos seraient diffusés publiquement ; que, par ailleurs, l'animateur a constamment adopté, à cette occasion, une attitude visant à donner une image caricaturale des homosexuels qui ne peut qu'encourager les préjugés et la discrimination à leur encontre ; que, compte tenu de la nature et de la gravité de ces faits, le CSA a pu légalement estimer qu'ils devaient être regardés, sans qu'y fasse obstacle le caractère humoristique de l'émission, comme une méconnaissance des prescriptions des articles 2-3-3 et 2-3-4 de la convention du service C8 cités au point 3, et justifiaient ainsi une sanction ; qu'eu égard tant aux pouvoirs dévolus au CSA, auquel le législateur a confié la mission de veiller à ce que les programmes audiovisuels donnent une image de la société française exempte de préjugés, qu'eu égard à la nature des faits décrits ci-dessus au regard des obligations qui s'imposent à la société requérante, la décision de sanctionner cette dernière ne porte pas une atteinte disproportionnée à la liberté d'expression, protégée tant par l'article 11 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789 que par l'article 10 paragraphe 1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              7. Considérant qu'aux termes de l'article 42-2 de la loi du 30 septembre 1986 : " Le montant de la sanction pécuniaire doit être fonction de la gravité des manquements commis et en relation avec les avantages tirés du manquement, sans pouvoir excéder 3 % du chiffre d'affaires hors taxes, réalisé au cours du dernier exercice clos calculé sur une période de douze mois. Ce maximum est porté à 5 % en cas de nouvelle violation de la même obligation " ; qu'en fixant à 3 millions d'euros le montant de la sanction pécuniaire infligée à la société C8, soit à peu près les deux tiers du plafond fixé par ces dispositions compte tenu du chiffre d'affaires de cette société, le CSA n'en a pas fait une inexacte application, eu égard à la gravité des manquements commis ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que la société C8 n'est pas fondée à demander l'annulation de la décision qu'elle attaque ;<br/>
<br/>
              9. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge du CSA qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société C8 la somme de 3 000 euros que le CSA demande au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de la société C8 est rejetée.<br/>
Article 2 : La société C8 versera au CSA une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société C8 et au Conseil supérieur de l'audiovisuel.<br/>
Copie en sera adressée à la ministre de la culture.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">56-01 RADIO ET TÉLÉVISION. CONSEIL SUPÉRIEUR DE L'AUDIOVISUEL. - SANCTION À L'ENCONTRE LA SOCIÉTÉ C8 POUR UNE SÉQUENCE DE L'ÉMISSION TPMP! BABA HOT LINE DU 18 MAI 2017 - MÉCONNAISSANCE DES ARTICLES 2-3-3 ET 2-3-4 DE LA CONVENTION DE SERVICE - EXISTENCE - ATTEINTE DISPROPORTIONNÉE À LA LIBERTÉ D'EXPRESSION - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">59-02-02-03 RÉPRESSION. DOMAINE DE LA RÉPRESSION ADMINISTRATIVE RÉGIME DE LA SANCTION ADMINISTRATIVE. BIEN-FONDÉ. - SANCTION PRONONCÉE PAR LE CSA À L'ENCONTRE LA SOCIÉTÉ C8 POUR UNE SÉQUENCE DE L'ÉMISSION TPMP! BABA HOT LINE DU 18 MAI 2017 - MÉCONNAISSANCE DES ARTICLES 2-3-3 ET 2-3-4 DE LA CONVENTION DE SERVICE - EXISTENCE - ATTEINTE DISPROPORTIONNÉE À LA LIBERTÉ D'EXPRESSION - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 56-01 Emission TPMP ! Baba hot line diffusée le 18 mai 2017 à partir de 23h05 par le service C8 durant laquelle l'animateur a diffusé en direct les conversations téléphoniques qu'il a eues avec des personnes des deux sexes, mais très majoritairement de sexe masculin, répondant à une fausse petite annonce préalablement publiée sur un site de rencontres et présentant l'auteur de l'annonce comme bisexuel.... ,,En l'absence de tout procédé technique destiné à rendre méconnaissables les voix des personnes mises à l'antenne, sans qu'elles y aient consenti ni même qu'elles aient été avisées de la diffusion de conversations qu'elles pouvaient au contraire légitimement croire particulières, ces personnes ont été exposées au risque d'être reconnues, notamment par des membres de leur famille ou de leur entourage, eu égard notamment à certaines informations personnelles qu'elles avaient été engagées à livrer, concernant par exemple leur lieu de résidence, leur âge ou leur profession. L'animateur a incité ces personnes à tenir des propos d'une crudité appuyée dévoilant leur intimité et exposant leur vie privée alors même qu'elles ne pouvaient imaginer que leurs propos seraient diffusés publiquement. Par ailleurs, l'animateur a constamment adopté, à cette occasion, une attitude visant à donner une image caricaturale des homosexuels qui ne peut qu'encourager les préjugés et la discrimination à leur encontre. Compte tenu de la nature et de la gravité de ces faits, le Conseil Supérieur de l'Audiovisuel (CSA) a pu légalement estimer qu'ils devaient être regardés, sans qu'y fasse obstacle le caractère humoristique de l'émission, comme une méconnaissance des prescriptions des articles 2-3-3 et 2-3-4 de la convention du service C8, et justifiaient ainsi une sanction. Eu égard tant aux pouvoirs dévolus au CSA, auquel le législateur a confié la mission de veiller à ce que les programmes audiovisuels donnent une image de la société française exempte de préjugés, qu'à la nature des faits décrits ci-dessus au regard des obligations qui s'imposent à la société requérante, la décision de sanctionner cette dernière ne porte pas une atteinte disproportionnée à la liberté d'expression, protégée tant par l'article 11 de la Déclaration des droits de l'homme et du citoyen (DDHC) du 26 août 1789 que par l'article 10 paragraphe 1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (Conv. EDH).... ,,En fixant à 3 millions d'euros le montant de la sanction pécuniaire infligée à la société C8, soit à peu près les deux tiers du plafond fixé par l'article 42-2 de la loi n° 86-1067 du 30 septembre 1986 compte tenu du chiffre d'affaires de cette société, le CSA n'en a pas fait une inexacte application, eu égard la gravité des manquements commis.</ANA>
<ANA ID="9B"> 59-02-02-03 Emission TPMP ! Baba hot line diffusée le 18 mai 2017 à partir de 23h05 par le service C8 durant laquelle l'animateur a diffusé en direct les conversations téléphoniques qu'il a eues avec des personnes des deux sexes, mais très majoritairement de sexe masculin, répondant à une fausse petite annonce préalablement publiée sur un site de rencontres et présentant l'auteur de l'annonce comme bisexuel.... ,,En l'absence de tout procédé technique destiné à rendre méconnaissables les voix des personnes mises à l'antenne, sans qu'elles y aient consenti ni même qu'elles aient été avisées de la diffusion de conversations qu'elles pouvaient au contraire légitimement croire particulières, ces personnes ont été exposées au risque d'être reconnues, notamment par des membres de leur famille ou de leur entourage, eu égard notamment à certaines informations personnelles qu'elles avaient été engagées à livrer, concernant par exemple leur lieu de résidence, leur âge ou leur profession. L'animateur a incité ces personnes à tenir des propos d'une crudité appuyée dévoilant leur intimité et exposant leur vie privée alors même qu'elles ne pouvaient imaginer que leurs propos seraient diffusés publiquement. Par ailleurs, l'animateur a constamment adopté, à cette occasion, une attitude visant à donner une image caricaturale des homosexuels qui ne peut qu'encourager les préjugés et la discrimination à leur encontre. Compte tenu de la nature et de la gravité de ces faits, le Conseil Supérieur de l'Audiovisuel (CSA) a pu légalement estimer qu'ils devaient être regardés, sans qu'y fasse obstacle le caractère humoristique de l'émission, comme une méconnaissance des prescriptions des articles 2-3-3 et 2-3-4 de la convention du service C8, et justifiaient ainsi une sanction. Eu égard tant aux pouvoirs dévolus au CSA, auquel le législateur a confié la mission de veiller à ce que les programmes audiovisuels donnent une image de la société française exempte de préjugés, qu'à la nature des faits décrits ci-dessus au regard des obligations qui s'imposent à la société requérante, la décision de sanctionner cette dernière ne porte pas une atteinte disproportionnée à la liberté d'expression, protégée tant par l'article 11 de la Déclaration des droits de l'homme et du citoyen (DDHC) du 26 août 1789 que par l'article 10 paragraphe 1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (Conv. EDH).... ,,En fixant à 3 millions d'euros le montant de la sanction pécuniaire infligée à la société C8, soit à peu près les deux tiers du plafond fixé par l'article 42-2 de la loi n° 86-1067 du 30 septembre 1986 compte tenu du chiffre d'affaires de cette société, le CSA n'en a pas fait une inexacte application, eu égard la gravité des manquements commis.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, décision du même jour, Société C8, n° 412071, à mentionner aux Tables. Cf. sol. contr. CE, décision du même jour, Société C8, n° 412074, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
