<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032491628</ID>
<ANCIEN_ID>JG_L_2016_05_000000396332</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/49/16/CETATEXT000032491628.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 04/05/2016, 396332</TITRE>
<DATE_DEC>2016-05-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396332</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:396332.20160504</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif de Marseille, sur le fondement de l'article L. 521-2 du code de justice administrative, de suspendre l'exécution de la décision par laquelle le directeur interdépartemental des routes Méditerranée a refusé de lui accorder la prolongation du bénéfice du concours de chef d'équipe d'exploitation. Par une ordonnance n° 1510582 du 30 décembre 2015, prise en application de l'article L. 522-3 du code de justice administrative, le juge des référés du tribunal administratif de Marseille a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 22 janvier et 8 février 2016 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat d'annuler cette ordonnance.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de M. A...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ; qu'aux termes de l'article L. 522-3 du même code : " Lorsque la demande ne présente pas un caractère d'urgence ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée, le juge des référés peut la rejeter par une ordonnance motivée sans qu'il y ait lieu d'appliquer les deux premiers alinéas de l'article L. 522-1 " ; enfin, qu'aux termes du premier alinéa de l'article R. 412-1 du même code : " La requête doit, à peine d'irrecevabilité, être accompagnée, sauf impossibilité justifiée, de la décision attaquée ou, dans le cas mentionné à l'article R. 421-2, de la pièce justifiant de la date de dépôt de la réclamation " ;<br/>
<br/>
              2. Considérant que M. A...a demandé au juge des référés du tribunal administratif de Marseille d'ordonner, sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative, la suspension de la décision de rejet opposée par le directeur interdépartemental des routes de la circonscription " Méditerranée " à sa demande tendant à la prolongation du bénéfice de son succès au concours de chef d'équipe d'exploitation des travaux publics de l'Etat, dans l'attente que se libère un emploi situé dans une zone plus proche de son domicile que celle où se situait l'emploi qui lui avait été proposé ; que, par une ordonnance en date du 30 novembre 2015, le juge des référés a rejeté cette demande comme manifestement irrecevable, en application des dispositions de l'article L. 522-3, au motif que l'intéressé n'avait pas produit la décision attaquée ni justifié de l'impossibilité de la produire, en méconnaissance de l'obligation imposée par l'article R. 412-1 du même code ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier que l'ordonnance attaquée a été notifiée à M. A...le 8 janvier 2016 ; que, par suite, contrairement à ce qui est soutenu en défense, son pourvoi, qui a été enregistré le 22 janvier 2016, soit dans le délai de quinze jours fixé par l'article R. 523-1 du code de justice administrative, n'est pas tardif ;<br/>
<br/>
              4. Considérant que la recevabilité d'une demande en référé présentée sur le fondement de l'article L. 521-2 de ce code, justifiée par l'urgence et tendant à ce que le juge des référés ordonne à l'administration, sous quarante-huit heures, toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle celle-ci aurait porté une atteinte grave et manifestement illégale ne saurait être soumise, eu égard à son objet et à ses modalités de mise en oeuvre, à la condition que le requérant produise, lorsque celle-ci existe, la décision dont la suspension de l'exécution est demandée, ou justifie de l'impossibilité de la produire ; qu'en rejetant, pour ce motif, comme manifestement irrecevable la demande présentée par M. A..., le juge des référés du tribunal administratif de Marseille a entaché son ordonnance d'erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, M. A...est fondé à en demander l'annulation ;<br/>
<br/>
              5. Considérant qu'il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée par M. A... ;<br/>
<br/>
              6. Considérant que la demande en référé-liberté présentée par M. A...est dépourvue de toute motivation et précision quant à la nature de la liberté fondamentale à laquelle l'administration aurait porté une atteinte grave et manifestement illégale ; que M. A...n'invoque à cet égard aucun texte prévoyant le droit pour les agents reçus au concours de chef d'équipe d'exploitation de solliciter la prolongation du bénéfice de ce concours ; que s'il soutient qu'une prolongation aurait été accordée par le passé à d'autres agents, une méconnaissance du principe d'égalité ne peut, en tout état de cause, révéler, par elle-même, une atteinte à une liberté fondamentale au sens de l'article L. 521-2 du code de justice administrative ; que, par suite, la demande de M. A...doit être rejetée ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que le versement d'une somme soit mis à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Marseille du 30 décembre 2015 est annulée.<br/>
Article 2 : La demande présentée par M. A...devant le tribunal administratif de Marseille et ses conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. B...A..., à la ministre de l'environnement, de l'énergie et de la mer et à la ministre du logement et de l'habitat durable.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-035-03-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ TENDANT AU PRONONCÉ DE MESURES NÉCESSAIRES À LA SAUVEGARDE D'UNE LIBERTÉ FONDAMENTALE (ART. L. 521-2 DU CODE DE JUSTICE ADMINISTRATIVE). RECEVABILITÉ. - CONCLUSIONS TENDANT À LA SUSPENSION D'UNE DÉCISION ADMINISTRATIVE - OBLIGATION DE PRODUIRE LA DÉCISION - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 54-035-03-02 Conclusions tendant à la suspension d'une décision administrative présentées sur le fondement de l'article L. 521-2 du code de justice administrative (CJA). La recevabilité de ces conclusions ne saurait être soumise, eu égard à l'objet de cette voie de droit et à ses modalités de mise en oeuvre, à la condition que le requérant produise, lorsque celle-ci existe, la décision dont la suspension de l'exécution est demandée, ou justifie de l'impossibilité de la produire.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., s'agissant du référé suspension, 12 février 2003, CE, Centre communal d'action sociale de la commune de Castanet-Tolosan et commune de Castanet-Tolosan, n° 249205, T. p. 918.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
