<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035818909</ID>
<ANCIEN_ID>JG_L_2017_10_000000395303</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/81/89/CETATEXT000035818909.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 16/10/2017, 395303</TITRE>
<DATE_DEC>2017-10-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395303</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BRIARD</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:395303.20171016</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Société chimique de Oissel a demandé au tribunal administratif de Rouen d'annuler l'arrêté du 12 septembre 2013 par lequel le préfet de la Seine-Maritime lui a imposé des prescriptions complémentaires pour l'installation classée pour la protection de l'environnement qu'elle exploite à Oissel. Par un jugement n° 1302714 du 8 juillet 2014, le tribunal administratif de Rouen a fait droit à sa demande.<br/>
<br/>
              Par un arrêt n° 14DA01729 du 15 octobre 2015, la cour administrative d'appel de Douai a, sur le recours du ministre de l'écologie, du développement durable et de l'énergie, annulé le jugement du tribunal administratif de Rouen et rejeté la demande formée par la Société chimique de Oissel devant ce tribunal.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 15 décembre 2015, 15 mars 2016 et 24 août 2017 au secrétariat du contentieux du Conseil d'État, la Société chimique de Oissel demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'État la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu<br/>
              -	le code de l'environnement ;<br/>
              -	la loi n° 2000-321 du 12 avril 2000 ;<br/>
              -	le décret n° 2006-672 du 8 juin 2006 ;<br/>
              -	le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Briard, avocat de la Société chimique de Oissel.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le préfet de la Seine-Maritime a, par un arrêté du 12 septembre 2013 pris sur le fondement de l'article L. 512-20 du code de l'environnement, imposé à la Société chimique de Oissel des prescriptions complémentaires pour l'installation classée pour la protection de l'environnement qu'elle exploite à Oissel, en vue de remédier à la pollution de captages destinés à l'alimentation en eau potable de la population ; que, par un jugement du 8 juillet 2014, le tribunal administratif de Rouen a, à la demande de la société, annulé cet arrêté ; que, par un arrêt du 15 octobre 2015, contre lequel la Société chimique de Oissel se pourvoit en cassation, la cour administrative d'appel de Douai a, à la demande du ministre de l'écologie, du développement durable et de l'énergie, annulé le jugement du tribunal administratif et rejeté la demande présentée par la société devant le tribunal administratif ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article 9 du décret du 8 juin 2006 relatif à la création, à la composition et au fonctionnement de commissions administratives à caractère consultatif, dans sa version en vigueur à la date du litige : " Sauf urgence, les membres des commissions reçoivent, cinq jours au moins avant la date de la réunion, une convocation comportant l'ordre du jour et, le cas échéant, les documents nécessaires à l'examen des affaires qui y sont inscrites " ; qu'en estimant qu'aucun texte ou principe n'exige que l'exploitant soit destinataire de l'ensemble des pièces communiquées aux membres du conseil départemental de l'environnement et des risques sanitaires et technologiques, la cour a nécessairement écarté l'application de l'article 9 du décret du 8 juin 2006 invoquée en défense par la société, au demeurant de manière inopérante ; que, dès lors, le moyen tiré de ce que l'arrêt attaqué serait, sur ce point, entaché d'insuffisance de motivation ne peut qu'être écarté ;<br/>
<br/>
              3. Considérant, en deuxième lieu, d'une part, qu'aux termes de l'article 24 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations, aujourd'hui codifié à l'article L. 121-1 du code des relations du public avec l'administration : " Exception faite des cas où il est statué sur une demande, les décisions individuelles qui doivent être motivées en application des articles 1er et 2 de la loi n° 79-587 du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public n'interviennent qu'après que la personne intéressée a été mise à même de présenter ses observations écrites et, le cas échéant, sur sa demande, des observations orales. (...) " ;<br/>
<br/>
              4. Considérant, d'autre part, qu'aux termes de l'article L. 512-20 du code de l'environnement : " En vue de protéger les intérêts visés à l'article L. 511-1, le préfet peut prescrire la réalisation des évaluations et la mise en oeuvre des remèdes que rendent nécessaires soit les conséquences d'un accident ou incident survenu dans l'installation, soit les conséquences entraînées par l'inobservation des conditions imposées en application du présent titre, soit tout autre danger ou inconvénient portant ou menaçant de porter atteinte aux intérêts précités. Ces mesures sont prescrites par des arrêtés pris, sauf cas d'urgence, après avis de la commission départementale consultative compétente. " ; que la procédure au terme de laquelle le préfet peut imposer des prescriptions complémentaires à l'exploitant d'une installation classée pour la protection de l'environnement, en particulier en application de l'article L. 512-20, est précisée par l'article R. 512-31 du même code, alors en vigueur et repris en substance à l'article R. 181-45 de ce code, qui dispose que : " Des arrêtés complémentaires peuvent être pris sur proposition de l'inspection des installations classées et après avis du conseil départemental de l'environnement et des risques sanitaires et technologiques. Ils peuvent fixer toutes les prescriptions additionnelles que la protection des intérêts mentionnés à l'article L. 511-1 rend nécessaires (...). L'exploitant peut se faire entendre et présenter ses observations dans les conditions prévues au troisième alinéa de l'article R. 512-25 et au premier alinéa de l'article R. 512-26. " ; qu'en vertu du troisième alinéa de l'article R. 512-25 du même code, alors en vigueur et repris en substance à l'article R. 181-39 de ce code, relatif à la consultation du conseil départemental de l'environnement et des risques sanitaires et technologiques, " Le demandeur a la faculté de se faire entendre par le conseil ou de désigner, à cet effet, un mandataire. Il est informé par le préfet au moins huit jours à l'avance de la date et du lieu de la réunion du conseil et reçoit simultanément un exemplaire des propositions de l'inspection des installations classées. " ; que le premier alinéa de l'article R. 512-26 du même code, alors en vigueur et repris en substance à l'article R. 181-40 de ce code, précise que : " Le projet d'arrêté statuant sur la demande est porté par le préfet à la connaissance du demandeur, auquel un délai de quinze jours est accordé pour présenter éventuellement ses observations par écrit au préfet, directement ou par mandataire. " ; qu'enfin, dans le cas où sont mis en oeuvre les pouvoirs de contrôle confiés à l'inspection des installations classées par les articles L. 171-1 et suivants, l'article L. 514-5 du code de l'environnement dispose que : " L'exploitant est informé par l'inspecteur des installations classées des suites du contrôle. L'inspecteur des installations classées transmet son rapport de contrôle au préfet et en fait copie simultanément à l'exploitant. Celui-ci peut faire part au préfet de ses observations. " ;<br/>
<br/>
              5. Considérant, d'une part, qu'il résulte des dispositions précitées des articles L. 514-5, R. 512-25 et R. 512-26 du code de l'environnement que, préalablement à l'édiction de prescriptions complémentaires prises sur le fondement de l'article L. 512-20 du code de l'environnement, l'exploitant d'une installation classée pour la protection de l'environnement doit être destinataire du rapport du contrôle le cas échéant réalisé par l'inspection des installations classées, des propositions de l'inspection tendant à ce que des prescriptions complémentaires lui soient imposées et du projet d'arrêté du préfet comportant les prescriptions complémentaires envisagées ; que, d'autre part, il résulte des dispositions combinées du code de l'environnement citées au point 4 et de l'article 24 de la loi du 12 avril 2000 citées au point 3 que l'exploitant doit être mis à même de présenter des observations et d'obtenir également communication, s'il le demande, de celles des pièces du dossier utiles à cette fin ; qu'en jugeant qu'aucun autre texte ou principe n'exige que l'exploitant soit destinataire de l'ensemble des pièces communiquées aux membres du conseil départemental de l'environnement et des risques sanitaires et technologiques, la cour n'a pas entaché son arrêt d'erreur de droit dès lors qu'elle n'a ainsi ni écarté l'application de la loi du 12 avril 2000, ni dénié le droit de l'exploitant d'obtenir, dans les conditions précisées ci-dessus, communication des pièces utiles ; <br/>
<br/>
              6. Considérant, en dernier lieu, que, si les actes administratifs doivent être pris selon les formes et conformément aux procédures prévues par les lois et règlements, un vice affectant le déroulement d'une procédure administrative préalable, suivie à titre obligatoire ou facultatif, n'est de nature à entacher d'illégalité la décision prise que s'il ressort des pièces du dossier qu'il a été susceptible d'exercer, en l'espèce, une influence sur le sens de la décision prise ou qu'il a privé les intéressés d'une garantie ;<br/>
<br/>
              7. Considérant qu'en estimant qu'il ne résultait pas de l'instruction que la réduction du délai de convocation de la société requérante à la réunion du conseil départemental de l'environnement et des risques sanitaires et technologiques, de huit à cinq jours, a été de nature, à elle seule, à priver la société de la garantie que le délai minimum de huit jours tend à protéger, consistant en la possibilité d'être présent, de préparer utilement sa défense en vue de formuler des observations pertinentes et documentées devant le conseil, la cour administrative d'appel, qui a procédé à la vérification à laquelle elle était tenue des conséquences qu'a revêtues en l'espèce l'irrégularité, n'a pas commis d'erreur de droit et a suffisamment motivé son arrêt sur ce point ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que la Société chimique de Oissel n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; qu'il suit de là que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
      Article 1er : Le pourvoi de la Société chimique de Oissel est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la Société chimique de Oissel et au ministre d'État, ministre de la transition écologique et solidaire.<br/>
      Copie en sera adressée à la société Borealis Chimie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">44-02-02-01-02 NATURE ET ENVIRONNEMENT. INSTALLATIONS CLASSÉES POUR LA PROTECTION DE L'ENVIRONNEMENT. RÉGIME JURIDIQUE. POUVOIRS DU PRÉFET. MODIFICATION DES PRESCRIPTIONS IMPOSÉES AUX TITULAIRES. - PRESCRIPTIONS COMPLÉMENTAIRES (ART. L. 512-20 DU CODE DE L'ENVIRONNEMENT) - PIÈCES À COMMUNIQUER À L'EXPLOITANT - RAPPORT DE CONTRÔLE ET PROJET D'ARRÊTÉ - PIÈCES POUVANT LUI ÊTRE COMMUNIQUÉES À SA DEMANDE - PIÈCES DU DOSSIER UTILES POUR LE METTRE À MÊME DE PRÉSENTER DES OBSERVATIONS.
</SCT>
<ANA ID="9A"> 44-02-02-01-02 Il résulte des articles L. 514-5, R. 512-25 et R. 512-26 du code de l'environnement que, préalablement à l'édiction de prescriptions complémentaires prises sur le fondement de l'article L. 512-20 du code de l'environnement, l'exploitant d'une installation classée pour la protection de l'environnement (IPCE) doit être destinataire du rapport du contrôle le cas échéant réalisé par l'inspection des installations classées, des propositions de l'inspection tendant à ce que des prescriptions complémentaires lui soient imposées et du projet d'arrêté du préfet comportant les prescriptions complémentaires envisagées. Il résulte des articles L. 512-20, L. 514-5, R. 512-25, R. 512-26 et R. 512-31 du code de l'environnement et de l'article 24 de la loi n° 2000-321 du 12 avril 2000, désormais codifié à l'article L. 121-1 du code des relations entre le public et l'administration (CRPA), que l'exploitant doit être mis à même de présenter des observations et d'obtenir également communication, s'il le demande, de celles des pièces du dossier utiles à cette fin.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
