<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027002383</ID>
<ANCIEN_ID>JG_L_2013_01_000000348369</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/00/23/CETATEXT000027002383.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  8ème sous-section jugeant seule, 28/01/2013, 348369, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-01-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>348369</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 8ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SPINOSI ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:348369.20130128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 11 avril et 11 juillet 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Rennes, représentée par son maire ; elle demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10NT01178 du 4 février 2011 par lequel la cour administrative d'appel de Nantes, faisant droit à la requête de la société Carrefour Property, en premier lieu, a annulé l'ordonnance n° 092788 du 6 avril 2010 par laquelle le président de la 5ème chambre du tribunal administratif de Rennes a rejeté la demande de cette société tendant à l'annulation, d'une part, de la décision du 3 décembre 2008 du maire de Rennes (Ille-et-Vilaine) lui retirant, à compter du 2 mars 2009, l'autorisation d'occuper une ou plusieurs places de stationnement dans le parking d'Isly et, d'autre part, de la décision implicite de rejet du recours gracieux formé, le 2 février 2009, par l'intéressée et, en second lieu, a annulé ces décisions ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter comme irrecevable la demande de la société Carrefour Property ; <br/>
<br/>
              3°) de mettre à la charge de la société Carrefour Property la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 10 janvier 2013, présentée pour la société Carrefour Property ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, Maître des Requêtes, <br/>
<br/>
              - les observations de Me Spinosi, avocat de la commune de Rennes et de la SCP Gaschignard, avocat de la société Carrefour Property,<br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Spinosi, avocat de la commune de Rennes et à la SCP Gaschignard, avocat de la société Carrefour Property ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par des conventions d'aménagement en date des 20 janvier et 20 février 1960, la commune de Rennes a confié à la Société d'Economie Mixte d'Aménagement et d'Equipement de la Bretagne (SEMAEB) l'opération de rénovation du quartier Colombier comprenant notamment la réalisation d'un parc de stationnement souterrain dénommé " Isly Colombier " ; que la société Carrefour Property, acquéreur de lots immobiliers riverains du parking d'Isly Colombier, a, en même temps qu'elle achetait ces lots auprès de la SEMAEB, obtenu de cette société l'accès à des places de stationnement banalisées moyennant l'adhésion à l'association syndicale libre (ASL) des propriétaires riverains du parking d'Isly Colombier, créée en 1969, et le paiement en une fois à la SEMAEB d'une redevance correspondant à ce droit d'accès ; que le contrat de vente prévoyait que l'ASL des propriétaires riverains du parking d'Isly Colombier, conformément à ses statuts, prendrait à bail le parking pour une durée de soixante-dix ans et en assurerait la gestion, que si aucun contrat n'a été signé, le juge judiciaire a constaté que la SEMAEB et l'ASL avaient néanmoins conclu un bail verbal ; que, par actes des 15 et 22 décembre 2000, en exécution des conventions d' aménagement, la SEMAEB a rétrocédé le parc de stationnement à la commune de Rennes qui a succédé à la SEMAEB dans l'exécution du bail verbal conclu avec l'ASL ; que cette rétrocession a eu pour effet d'incorporer le parking dans le domaine public communal ; que la commune a décidé de mettre fin au bail qui la liait à l'ASL par décision de son maire du 27 novembre 2006 ; que, par une décision du 3 décembre 2008, la commune de Rennes a informé la société Carrefour Property qu'il serait procédé au retrait des droits de stationnement qui lui avaient été conférés sur le parking à compter du 2 mars 2009 ; qu'après avoir vainement formé un recours gracieux le 2 février 2009, la société Carrefour Property a saisi le 3 juin 2009 le tribunal administratif de Rennes d'une demande tendant à l'annulation de la décision du 3 décembre 2008 et de la décision implicite de rejet de son recours gracieux ; que la commune de Rennes se pourvoit en cassation contre l'arrêt par lequel la cour administrative d'appel de Nantes, faisant droit à l'appel de la société Carrefour Property, après avoir annulé l'ordonnance du président de la 5e chambre du tribunal administratif de Rennes, a annulé les décisions litigieuses ; <br/>
<br/>
              2. Considérant que, par un arrêt du 4 décembre 2007, devenu définitif et revêtu de l'autorité absolue de la chose jugée, la cour administrative d'appel de Nantes a annulé les arrêtés des 13 janvier et 13 octobre 2003 du préfet d'Ille-et-Vilaine déclarant cessibles, au profit de la commune de Rennes, les différents droits réels immobiliers détenus par les propriétaires riverains dans le parking d'Isly, au motif que les droits d'accès en litige ne présentaient pas le caractère de droits réels immobiliers pouvant faire l'objet d'une procédure d'expropriation ; qu'il ressort des termes de cet arrêt que la cour s'est prononcée en ce sens pour l'ensemble des droits d'accès aux places de stationnement du parking d'Isly dont étaient titulaires les propriétaires riverains de ce parking, qu'il s'agisse d'emplacements banalisés ou de places privatisées ; que, dès lors, en relevant, pour écarter le moyen tiré de l'autorité de la chose jugée qui s'attachait à cet arrêt, que ce dernier s'était prononcé sur les seules places de stationnement privatisées à l'exclusion des emplacements banalisés objet du litige, la cour s'est méprise sur la portée de cette décision juridictionnelle ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la commune de Rennes est fondée pour ce motif à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              3. Considérant que, dans les circonstances de l'espèce, il y a lieu de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              4. Considérant que le juge du contrat, saisi par une partie d'un litige relatif à une mesure d'exécution d'un contrat, peut seulement, en principe, rechercher si cette mesure est intervenue dans des conditions de nature à ouvrir droit à indemnité ; que, toutefois, une partie à un contrat administratif peut, eu égard à la portée d'une telle mesure d'exécution, former devant le juge du contrat un recours de plein contentieux contestant la validité de la résiliation de ce contrat et tendant à la reprise des relations contractuelles ; qu'elle doit exercer ce recours, y compris si le contrat en cause est relatif à des travaux publics, dans un délai de deux mois à compter de la date à laquelle elle a été informée de la mesure de résiliation ; qu'eu égard aux particularités de ce recours contentieux, à l'étendue des pouvoirs de pleine juridiction dont le juge du contrat dispose et qui peut le conduire, si les conditions en sont satisfaites, à ordonner la reprise des relations contractuelles, l'exercice d'un recours administratif pour contester cette mesure, s'il est toujours loisible au cocontractant d'y recourir, ne peut avoir pour effet d'interrompre le délai de recours contentieux ; qu'il en va ainsi quel que soit le motif de résiliation du contrat, y compris pour un motif d'intérêt général ; <br/>
<br/>
              5. Considérant qu'il résulte de l'instruction que la décision du 3 décembre 2008 du maire de la commune de Rennes a eu pour effet de mettre un terme aux droits d'occupation du parking conventionnellement reconnus à chacun des membres de l'ASL ; que la demande de la société Carrefour Property tendant à l'annulation de la décision du 3 décembre 2008 s'analyse ainsi en un recours de plein contentieux contestant la validité de la résiliation de ce contrat et tendant à la reprise des relations contractuelles ; que cette demande a été enregistrée au greffe du tribunal administratif de Rennes le 3 juin 2009 ; que la société Carrefour Property a eu connaissance de cette mesure au plus tard le 2 février 2009, date à laquelle elle a formé auprès du maire de la commune un recours gracieux contre cette décision ; qu'il résulte de ce qui vient d'être dit que ce recours administratif n'a pu avoir pour effet d'interrompre le délai de recours contentieux qui commence à courir à la date à laquelle le cocontractant de l'administration est informé de la mesure de résiliation, même en l'absence de mention des voies et délais de recours ; que, dès lors, la demande présentée par la société Carrefour Property devant le tribunal administratif de Rennes était tardive et, par suite, irrecevable ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la société Carrefour Property n'est pas fondée à se plaindre de ce que, par l'ordonnance attaquée, le président de la 5e chambre du tribunal administratif de Rennes a rejeté comme irrecevables ses conclusions dirigées contre la décision du 3 décembre 2008 et la décision implicite de rejet de son recours gracieux ; <br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la commune de Rennes qui n'est pas, dans la présente instance, la partie perdante, une somme au titre des frais exposés par la société Carrefour Property et non compris dans les dépens ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Carrefour Property une somme à verser à la commune de Rennes au même titre ;<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
Article 1er : L'arrêt du 4 février 2011 de la cour administrative d'appel de Nantes est annulé.<br/>
Article 2 : La requête présentée par la société Carrefour Property devant la cour administrative d'appel de Nantes est rejetée.<br/>
Article 3 : Les conclusions présentées par la commune de Rennes et par la société Carrefour Property au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la commune de Rennes et à la société Carrefour Property.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
