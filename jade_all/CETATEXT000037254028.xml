<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037254028</ID>
<ANCIEN_ID>JG_L_2018_07_000000417105</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/25/40/CETATEXT000037254028.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 26/07/2018, 417105, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417105</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Alexandre  Koutchouk</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:417105.20180726</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société en nom collectif (SNC) Vauclusienne de Distribution Automobile a demandé au tribunal administratif de Nîmes de prononcer la décharge des rappels de taxe sur les surfaces commerciales auxquelles elle a été assujettie au titre des années 2010 à 2012 à raison des établissements qu'elle exploite à Avignon-Montfaret et Cavaillon (Vaucluse). Par un jugement n° 1300829 du 7 novembre 2014, ce tribunal a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 15MA00080 du 2 octobre 2017, enregistré le 8 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Marseille a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 9 janvier 2015 au greffe de cette cour, présenté par la société Vauclusienne de Distribution Automobile contre ce jugement en tant qu'il statue sur les impositions établies au titre des années 2011 et 2012.<br/>
<br/>
              Par  ce pourvoi, la société Vauclusienne de distribution automobile demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement, en tant qu'il a statué sur les impositions établies au titre des années 2011 et 2012 ;<br/>
<br/>
              2°) réglant l'affaire au fond dans cette mesure, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 10 000 euros au titre de l'article  L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              -  le code général des impôts et le livre des procédures fiscales ; <br/>
              -  la loi n° 72-657 du 13 juillet 1972 ; <br/>
              -  le décret n° 95-85 du 26 janvier 1995 ; <br/>
              -  le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alexandre Koutchouk, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la société Vauclusienne de Distribution Automobile.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. La société en nom collectif (SNC) Vauclusienne de Distribution Automobile, qui exerce une activité de vente de véhicules automobiles au sein de deux établissements situés à Avignon-Montfavet et Cavaillon (Vaucluse), a fait l'objet d'une vérification de comptabilité à l'issue de laquelle l'administration fiscale l'a assujettie à des rappels de taxe sur les surfaces commerciales au titre des années 2010 à 2012. Elle se pourvoit en cassation contre le jugement du 7 novembre 2014 par lequel le tribunal administratif de Nîmes a rejeté sa demande, en tant qu'elle tendait à la décharge des impositions dues au titre des années 2011 et 2012. <br/>
<br/>
              Sur les moyens relatifs à la régularité de la procédure d'imposition : <br/>
<br/>
              2. En premier lieu, aux termes de l'article 3 de la loi du 13 juillet 1972 instituant des mesures en faveur de certaines catégories de commerçants et artisans âgés, dans sa rédaction applicable aux années d'imposition en litige : " Il est institué une taxe sur les surfaces commerciales assise sur la surface de vente des magasins de commerce de détail, dès lors qu'elle dépasse 400 mètres carrés des établissements ouverts à partir du 1er janvier 1960 quelle que soit la forme juridique de l'entreprise qui les exploite. (...) / La surface de vente des magasins de commerce de détail, prise en compte pour le calcul de la taxe, et celle visée à l'article L. 720-5 du code de commerce, s'entendent des espaces affectés à la circulation de la clientèle pour effectuer ses achats, de ceux affectés à l'exposition des marchandises proposées à la vente, à leur paiement, et de ceux affectés à la circulation du personnel pour présenter les marchandises à la vente (...)  / Si ces établissements, à l'exception de ceux dont l'activité principale est la vente ou la réparation de véhicules automobiles, ont également une activité de vente au détail de carburants, l'assiette de la taxe comprend en outre une surface calculée forfaitairement en fonction du nombre de position de ravitaillement dans la limite de 70 mètres carrés par position de ravitaillement. Le décret prévu à l'article 20 fixe la surface forfaitaire par emplacement à un montant compris entre 35 et 70 mètres carrés. / Pour les établissements dont le chiffre d'affaires au mètre carré est inférieur à 3 000 euros, le taux de cette taxe est de 5,74 euros au mètre carré de surface définie au troisième alinéa. Pour les établissements dont le chiffre d'affaires au mètre carré est supérieur à 12 000 euros, le taux est fixé à 34, 12 euros. / A l'exclusion des établissements qui ont pour activité principale la vente ou la réparation de véhicules automobiles, les taux mentionnés à l'alinéa précédent sont respectivement portés à 8,32 euros ou 35,70 euros  (...). Un décret prévoira, par rapport aux taux ci-dessus, des réductions pour les professions dont l'exercice requiert des superficies de vente anormalement élevées ou, en fonction de leur chiffre d'affaires au mètre carré, pour les établissements dont la surface des locaux de vente destinés à la vente au détail est comprise entre 400 et 600 mètres carrés ". Aux termes de l'article 3 du décret du 26 janvier 1995 : " A. - La réduction de taux prévue au troisième alinéa de l'article 3 de la loi du 13 juillet 1972 susvisée en faveur des professions dont l'exercice requiert des superficies de vente anormalement élevées est fixée à 30 p. 100 en ce qui concerne la vente exclusive des marchandises énumérées ci-après (...) - véhicules automobiles ". Il résulte de la lettre même de ces dispositions, qui prévoient notamment des règles propres à ces établissements, que les établissements dont l'activité principale est la vente ou la réparation de véhicules automobiles, qu'ils soient neufs ou d'occasion, sont inclus dans le champ d'application de la taxe sur les surfaces commerciales. La société requérante n'est par suite pas fondée à soutenir que dès lors que l'activité de vente de véhicules neufs et d'occasion qu'elle exerce n'entre pas dans le champ de cette taxe, elle n'était pas soumise à une obligation déclarative, pour en déduire que l'administration aurait irrégulièrement mis en oeuvre une procédure de taxation d'office pour défaut de déclaration. Le moyen tiré de ce que la taxe sur les surfaces commerciales n'étant pas une taxe sur le chiffre d'affaires, l'administration aurait à tort fait application des dispositions du 3° de l'article L. 66 du livre des procédures fiscales, qui n'a pas été soumis aux juges du fond et n'est pas d'ordre public, ne peut utilement être soulevé pour la première fois en cassation.<br/>
<br/>
              3. En second lieu, aux termes de l'article L. 76 du livre des procédures fiscales : " les bases ou éléments servant au calcul des impositions d'office et leurs modalités de détermination sont portées à la connaissance du contribuable trente jours au moins avant la mise en recouvrement des impositions ". Après avoir relevé qu'il ressortait de la proposition de rectification adressée le 31 juillet 2012 à la société que celle-ci indiquait que le service considérait que l'activité de la société entrait dans le champ d'application de la taxe sur les surfaces commerciales, précisait que le vérificateur avait retenu, pour estimer que les établissements de Montfavet et de Cavaillon disposaient de surfaces de vente de, respectivement, 619 mètres carrés et 450 mètres carrés, les espaces utilisés pour l'exposition des marchandises, accessibles à la clientèle et au personnel, correspondant aux zones dédiées à la vente, à l'exclusion des locaux affectés notamment aux salons d'attente de la clientèle et faisait référence aux échanges contradictoires noués durant le contrôle à ce sujet, le tribunal administratif a pu, sans entacher son jugement de dénaturation ou d'erreur de droit, écarter le moyen tiré de ce que la motivation de ce document était insuffisante. <br/>
<br/>
              Sur les moyens relatifs au bien-fondé de l'imposition : <br/>
<br/>
              4. En premier lieu, il résulte, ainsi qu'indiqué au point 2, de la lettre même des dispositions de l'article 3 de la loi du 13 juillet 1972 que les établissements dont l'activité principale est la vente ou la réparation de véhicules automobiles, qu'ils soient neufs ou d'occasion, sont inclus dans le champ d'application de la taxe sur les surfaces commerciales. La société requérante n'est par suite fondée à soutenir ni que le tribunal administratif aurait méconnu ces dispositions en jugeant qu'elle était susceptible, eu égard à son activité, d'être assujettie à la taxe sur les surfaces commerciales, ni qu'il aurait commis une erreur de droit en jugeant que le décret du 26 janvier 1995, en ce qu'il mentionne la vente de véhicules automobiles dans les activités soumises à la taxe, méconnaîtrait ces mêmes dispositions.<br/>
<br/>
              5. En deuxième lieu, le tribunal administratif, qui n'a pas insuffisamment motivé son jugement sur ce point, n'a commis d'erreur de droit ni en jugeant que les dispositions de la loi du 13 juillet 1972 étaient claires et précises ni, en tout état de cause, en en déduisant que la requérante n'était pas fondée à soutenir que ces dispositions seraient incompatibles avec les stipulations de l'alinéa 1er de l'article 7 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, relatif au principe de légalité des délits et des peines. <br/>
<br/>
              6. En troisième lieu, il résulte des dispositions précitées de la loi du 13 juillet 1972, dans leur version applicable au litige, et du décret du 26 janvier 1995, que le chiffre d'affaires à prendre en compte pour le calcul du taux de la taxe sur les surfaces commerciales s'entend de celui correspondant à l'ensemble des ventes au détail en l'état réalisées par l'établissement, sans qu'il y ait lieu notamment de distinguer selon que ces ventes concernent ou non des biens qui sont présentés ou stockés dans cet établissement, ni selon que l'acheteur est un particulier ou un professionnel. Le tribunal n'a par suite pas commis d'erreur de droit en jugeant qu'il y avait lieu en l'espèce, pour calculer le chiffre d'affaires permettant de déterminer le taux applicable, de retenir tant les ventes réalisées par l'établissement correspondant à des véhicules stockés ou exposés à l'intérieur ou à l'extérieur de celui-ci que celles correspondant à des véhicules commandés et livrés ultérieurement. Il n'a davantage méconnu les dispositions applicables ni en jugeant qu'il n'y avait pas lieu de diminuer le chiffre d'affaires des charges correspondant à la reprise des anciens véhicules, ni, dès lors qu'aucun élément, notamment comptable, ne permettait de distinguer les accessoires utilisés pour effectuer des réparations de ceux vendus en l'état aux clients, en écartant l'argumentation de la société relative à la prise en compte des accessoires utilisés pour les besoins de prestations de services. Enfin, la société, qui indiquait explicitement dans ses écritures de première instance que l'administration avait exclu du chiffre d'affaires retenu les ventes aux professionnels et qu'elle lui en donnait acte, ne peut utilement soutenir en cassation que ce même tribunal aurait commis une erreur de droit en admettant la prise en compte de ces ventes.<br/>
<br/>
              7. Il résulte de tout ce qui précède que la société Vauclusienne de Distribution Automobile n'est pas fondée à demander l'annulation du jugement qu'elle attaque. Ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>                    D E C I D E :<br/>
                                 --------------<br/>
<br/>
Article 1er : Le pourvoi de la société Vauclusienne de Distribution Automobile est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société en nom collectif Vauclusienne de Distribution Automobile et au ministre de l'action et des comptes publics. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
