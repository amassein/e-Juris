<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027064716</ID>
<ANCIEN_ID>JG_L_2013_02_000000343380</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/06/47/CETATEXT000027064716.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 13/02/2013, 343380, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-02-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>343380</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>RICARD</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:343380.20130213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 20 septembre et 20 décembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SARL Absis, dont le siège est 26 rue Arthur Rimbaud à Tours (37100), représentée par son gérant ; la SARL Absis demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09NT01608 du 28 juin 2010 par lequel la cour administrative d'appel de Nantes a rejeté sa requête tendant à l'annulation de l'ordonnance n° 09-391 du 6 mai 2009 par laquelle le président de la 3ème chambre du tribunal administratif d'Orléans a rejeté sa demande tendant à l'annulation de la décision du 4 août 2008 du chef du pôle recouvrement des impôts de Tours refusant le plan d'apurement du passif de la société ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le décret du 26 octobre 1849 réglant les formes de procéder du Tribunal des conflits ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, Maître des Requêtes,  <br/>
<br/>
              - les observations de Me Ricard, avocat de la SARL Absis,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Ricard, avocat de la SARL Absis ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 626-5 du code de commerce, relatif au plan de sauvegarde des entreprises, dans sa rédaction applicable à la date de la décision contestée : " Les propositions pour le règlement des dettes sont, au fur et à mesure de leur élaboration et sous surveillance du juge-commissaire, communiquées par l'administrateur au mandataire judiciaire, aux contrôleurs ainsi qu'au comité d'entreprise ou, à défaut, aux délégués du personnel. / Le mandataire judiciaire recueille individuellement ou collectivement l'accord de chaque créancier qui a déclaré sa créance conformément à l'article L. 622-24, sur les délais et remises qui lui sont proposés. En cas de consultation par écrit, le défaut de réponse, dans le délai de trente jours à compter de la réception de la lettre du mandataire judiciaire, vaut acceptation. (...) " ; qu'aux termes de l'article L. 626-6 du même code, dans sa rédaction alors applicable : " Les administrations financières (...) peuvent accepter, concomitamment à l'effort consenti par d'autres créanciers, de remettre tout ou partie de ses dettes au débiteur dans des conditions similaires à celles que lui octroierait, dans des conditions normales de marché, un opérateur économique privé placé dans la même situation. / Dans ce cadre, les administrations financières peuvent remettre l'ensemble des impôts directs perçus au profit de l'Etat et des collectivités territoriales ainsi que des produits divers du budget de l'Etat dus par le débiteur. S'agissant des impôts indirects perçus au profit de l'Etat et des collectivités territoriales, seuls les intérêts de retard, majorations, pénalités ou amendes peuvent faire l'objet d'une remise. / Les conditions de la remise de la dette sont fixées par décret en Conseil d'Etat. / Les créanciers visés au premier alinéa peuvent également décider des cessions de rang de privilège ou d'hypothèque ou de l'abandon de ces sûretés. " ; que ces dispositions sont applicables au plan de redressement élaboré dans le cadre de la procédure de redressement judiciaire en vertu de l'article L. 631-19 du code de commerce ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SARL Absis a fait l'objet d'une vérification de comptabilité portant sur la période du 1er novembre 2002 au 30 avril 2006, à l'issue de laquelle des rappels de taxe sur la valeur ajoutée et des pénalités ont été mis à sa charge ; que la société ayant été placée en redressement judiciaire, par un jugement du tribunal de commerce de Tours du 6 novembre 2007, le comptable chargé du recouvrement de ces impositions a procédé, en janvier 2008, à la déclaration des créances fiscales au passif de cette procédure ; qu'en application de l'article L. 625-5 du code de commerce, le mandataire désigné par le tribunal de commerce a transmis, le 28 juillet 2008, à chaque créancier ayant déclaré sa créance des propositions d'apurement du passif de la société qui prévoyaient notamment un apurement de la dette fiscale en trois ou huit échéances annuelles ; que, par courrier du 4 août 2008, le comptable a indiqué ne pas accepter le plan de règlement ainsi proposé ; que le plan de redressement de la société a été homologué par un jugement du tribunal de commerce de Tours du 14 octobre 2008 ;<br/>
<br/>
              3. Considérant que la SARL Absis a formé, le 17 décembre 2008, un "recours gracieux" à l'encontre de la décision du 4 août 2008 rejetant le plan de règlement proposé ; que cette décision ayant été confirmée par un courrier du 5 janvier 2009 du chef du pôle de recouvrement des impôts de Tours, elle a saisi le 2 février 2009, le tribunal administratif d'Orléans d'un recours pour excès de pouvoir tendant à l'annulation de cette décision ; que, par l'arrêt attaqué du 28 juin 2010, la cour administrative d'appel de Nantes a confirmé l'ordonnance du 6 mai 2009 du président du tribunal administratif d'Orléans rejetant sa demande comme portée devant une juridiction incompétente pour en connaître ;<br/>
<br/>
              Sur la régularité de l'arrêt :<br/>
<br/>
              4. Considérant, en premier lieu, qu'il ressort des pièces du dossier, et notamment de la copie de la minute de l'arrêt attaqué, que le moyen tiré de ce que cette minute ne comporterait pas les signatures requises manque en fait ;<br/>
<br/>
              5. Considérant, en second lieu, que, contrairement à ce que soutient la SARL Absis, l'arrêt attaqué a suffisamment répondu à l'argumentation développée dans son deuxième mémoire en réplique ;<br/>
<br/>
              Sur le bien-fondé :<br/>
<br/>
              6. Considérant qu'aux termes de l'article 35 du décret du 26 octobre 1849, reproduit à l'article R. 771-2 du code de justice administrative : "Lorsque le Conseil d'Etat statuant au contentieux, la Cour de cassation ou toute autre juridiction statuant souverainement et échappant ainsi au contrôle tant du Conseil d'Etat que de la Cour de cassation, est saisi d'un litige qui présente à juger, soit sur l'action introduite, soit sur une exception, une question de compétence soulevant une difficulté sérieuse, et mettant en jeu la séparation des autorités administratives et judiciaires, la juridiction saisie peut, par décision ou arrêt motivé qui n'est susceptible d'aucun recours, renvoyer au Tribunal des conflits le soin de décider sur cette question de compétence " ;<br/>
<br/>
              7. Considérant que la cour administrative d'appel de Nantes a jugé que la décision contestée, qui n'est relative ni à la contestation d'un acte de poursuite ni à l'existence de la créance fiscale, n'était pas détachable du déroulement de la procédure collective alors en cours et ne pouvait faire l'objet d'un recours pour excès de pouvoir alors même qu'elle avait été prise par une autorité administrative ; qu'elle en a déduit que le juge judiciaire était seul compétent pour connaître de la contestation formée à son encontre par la SARL Absis ;<br/>
<br/>
              8. Considérant qu'à l'appui de son pourvoi, la SARL Absis soutient qu'en statuant ainsi la cour a commis une erreur de droit ; qu'elle fait notamment valoir que la décision attaquée est détachable de la procédure collective et qu'en l'absence de toute disposition, dans le code de commerce, prévoyant la possibilité de contester les décisions prises par les créanciers publics, cette décision ne peut faire l'objet que d'un recours pour excès de pouvoir devant le juge administratif ;<br/>
<br/>
              9. Considérant que la détermination de la juridiction compétente pour connaître du litige né de l'action de la SARL Absis présente à juger une question soulevant une difficulté sérieuse, de nature à justifier le recours à la procédure prévue par l'article 35 du décret du 26 octobre 1849 ; que, par suite, il y a lieu de renvoyer au Tribunal des conflits la question de savoir si cette action relève ou non de la compétence de la juridiction administrative et de surseoir à toute procédure jusqu'à la décision de ce tribunal ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'affaire est renvoyée au Tribunal des conflits.<br/>
Article 2 : Il est sursis à statuer sur le pourvoi de la SARL Absis jusqu'à ce que le Tribunal des conflits ait tranché la question de savoir si le litige né de sa demande tendant à l'annulation de la décision du 4 août 2008 du chef du pôle recouvrement des impôts de Tours refusant le plan d'apurement du passif de la société relève ou non de la compétence de la juridiction administrative.<br/>
Article 3 : La présente décision sera notifiée à la SARL Absis et au ministre de l'économie et des finances.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
