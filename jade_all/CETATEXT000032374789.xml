<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032374789</ID>
<ANCIEN_ID>JG_L_2016_04_000000386876</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/37/47/CETATEXT000032374789.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 06/04/2016, 386876, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-04-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386876</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>Mme Angélique Delorme</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:386876.20160406</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
              M. A...B...a demandé au tribunal administratif de Poitiers d'annuler pour excès de pouvoir les titres de recettes n° 69, 70 et 71 émis le 28 décembre 2010 par le président du conseil d'administration de la régie du golf de Royan pour recouvrer un trop-perçu de rémunération. <br/>
<br/>
              Par un jugement n° 1100421 du 7 mars 2013, le tribunal administratif a annulé les titres de recettes litigieux. <br/>
<br/>
              Par un arrêt n° 13BX01248 du 4 novembre 2014, la cour administrative d'appel de Bordeaux, faisant droit à l'appel formé par la régie du golf de Royan, a annulé ce jugement et rejeté la demande de première instance de M.B.... <br/>
<br/>
              Par un pourvoi et un mémoire complémentaire, enregistrés les 5 janvier et 7 avril 2015 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
              2°) réglant l'affaire du fond, de rejeter l'appel de la régie du golf de Royan ;<br/>
<br/>
              3°) de mettre à la charge de la régie du golf de Royan la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
               Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Angélique Delorme, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de M. B...et à la SCP Gaschignard, avocat de la régie du golf de Royan ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B..., qui occupait à titre principal les fonctions de directeur général des services de la commune de Royan, a exercé en outre celles de directeur de la régie du golf de Royan à compter du 8 janvier 2002 ; qu'à ce second titre lui a été allouée une indemnité mensuelle initialement fixée à 534 euros ; que ce montant a toutefois été porté à 1 601 euros par un arrêté du président du conseil d'administration de la régie du 31 mars 2006 ; que, le 17 décembre 2010, le directeur de la régie ayant succédé à M. B... a informé celui-ci que cette majoration avait été perçue à tort, avant d'émettre à son égard le 28 décembre 2010 trois titres exécutoires n° 69, 70 et 71, respectivement d'un montant de 10 575,58 euros, 14 136,89 euros et 11 330,22 euros au titre des années 2006, 2007 et 2008, afin de recouvrer les sommes indûment versées à l'intéressé ; que, M. B...ayant demandé au tribunal administratif de Poitiers l'annulation de ces titres exécutoires, ce tribunal a fait droit à cette demande par un jugement du 7 mars 2013 ; que, sur appel interjeté par la régie du golf de Royan, la cour administrative d'appel de Bordeaux a annulé ce jugement et rejeté la demande de première instance de M.B..., par un arrêt du 4 novembre 2014 contre lequel celui-ci se pourvoit en cassation ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 2221-10 du code général des collectivités territoriales : " Les régies dotées de la personnalité morale et de l'autonomie financière, dénommées établissement public local, sont créées, et leur organisation administrative et financière est déterminée, par délibération du conseil municipal. Elles sont administrées par un conseil d'administration et un directeur désignés dans les mêmes conditions sur proposition du maire. (...) " ; que l'article R. 2221-18 du même code dispose que " le conseil d'administration délibère sur toutes les questions intéressant le fonctionnement de la régie " ; <br/>
<br/>
              3. Considérant, en premier lieu, que pour juger que M. B...ne pouvait ignorer l'illégalité de l'arrêté du 31 mars 2006 lui accordant un avantage pécuniaire injustifié, la cour a énoncé que l'intéressé était chargé de la direction de la régie et de la liquidation des rémunérations de son personnel et que cet acte avait été pris à l'insu du conseil d'administration de la régie et sans être transmis au contrôle de légalité ; qu'en se fondant sur ce faisceau d'indices pour juger que M. B...avait contribué activement et consciemment à la revalorisation irrégulière de sa rémunération, la cour a suffisamment motivé son arrêt ;<br/>
<br/>
              4. Considérant, en deuxième lieu, que la cour a relevé que la régie du golf de Royan était dotée de la personnalité morale et de l'autonomie financière et gérait un service public industriel et commercial ; qu'ainsi qu'il a été dit au point 3 elle a également relevé que l'arrêté du 31 mars 2006 avait été pris à l'insu du conseil d'administration de la régie et n'avait pas été transmis au représentant de l'Etat chargé d'exercer le contrôle de légalité ; qu'en visant à cette fin le code général des collectivités territoriales, la cour s'est implicitement mais nécessairement fondée sur les dispositions de ce code qui régissent ce type de régie communale, et notamment celles qui sont citées au point 2 ci-dessus ; qu'elle n'a dès lors pas entaché son arrêt d'une méconnaissance des dispositions l'article R. 741-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant enfin que la cour a jugé que M.B..., eu égard aux fonctions qu'il exerçait tant au sein de la commune de Royan qu'au sein même de la régie, ne pouvait ignorer le caractère illégal de l'avantage financier consenti à son profit, avait participé consciemment et activement à l'octroi à l'augmentation indue de sa rémunération et devait dès lors être regardé comme s'étant livré à une manoeuvre frauduleuse, pour en déduire que l'arrêté revalorisant sa rémunération, ainsi obtenu par fraude, n'avait pu créer des droits à son profit ; que la cour a sur ce point souverainement apprécié les faits soumis à son appréciation, sans les dénaturer, et n'a pas commis d'erreur de droit ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que le pourvoi de M. B... doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre une somme au même titre à la charge de M. B...à verser à la régie du golf de Royan ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B...est rejeté.<br/>
Article 2 : Les conclusions présentées par la régie du golf de Royan au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. A...B...et à la régie du golf de Royan.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
