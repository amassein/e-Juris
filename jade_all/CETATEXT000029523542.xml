<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029523542</ID>
<ANCIEN_ID>JG_L_2014_09_000000365922</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/52/35/CETATEXT000029523542.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 29/09/2014, 365922, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-09-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365922</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE ; SCP BARADUC, DUHAMEL, RAMEIX ; SCP DELVOLVE</AVOCATS>
<RAPPORTEUR>M. Frédéric Dieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:365922.20140929</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 11 février et 13 mai 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société nationale des chemins de fer français (SNCF), dont le siège est 34 rue du Commandant René Mouchotte à Paris (75014) ; la société nationale des chemins de fer français demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11NT02999-11NT03135 du 6 décembre 2012 par lequel la cour administrative d'appel de Nantes a rejeté son appel tendant, d'une part, à l'annulation du jugement n° 10-4262 du 22 septembre 2011 du tribunal administratif d'Orléans en tant qu'il l'a condamnée, solidairement avec le département d'Eure-et-Loir, à verser à la compagnie Zürich Versicherung la somme de 33 731 euros et, d'autre part, au rejet de la demande de la compagnie Zürich Versicherung ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ; <br/>
<br/>
              3°) de mettre à la charge de la compagnie Zürich Versicherung le versement de la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
                          Vu le code général des collectivités territoriales ; <br/>
<br/>
                          Vu le code de la route ; <br/>
<br/>
                          Vu le code de la voirie routière ;<br/>
<br/>
                          Vu la loi n° 97-135 du 13 février 1997 ;<br/>
<br/>
                          Vu le décret n° 97-444 du 5 mai 1997 ;<br/>
<br/>
                          Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Dieu, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de la société nationale des chemins de fer français (SNCF), à la SCP Coutard, Munier-Apaire, avocat de la compagnie Zürich Versicherung, et à la SCP Delvolvé, avocat du département d'Eure-et-Loir ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, le 30 janvier 2007, au passage à niveau n° 37 situé sur la route départementale n° 6 à proximité de la gare de Saint-Prest (Eure-et-Loir), un poids lourd appartenant à la société de transport ANM est entré en collision avec un train en provenance de Chartres, les barrières du passage à niveau s'étant abaissées entre le tracteur du camion et sa remorque, dès lors immobilisés sur la voie ; que les dommages matériels résultant de cette collision ont été pris en charge par l'assureur du véhicule, la compagnie Zürich Versicherung ; que cette dernière, subrogée dans les droits de son assurée, la société ANM, a saisi le tribunal administratif d'Orléans d'une demande tendant à la condamnation solidaire de la SNCF et du département d'Eure-et-Loir à l'indemniser ; que la SNCF se pourvoit contre l'arrêt par lequel la cour administrative d'appel de Nantes a rejeté son appel dirigé contre le jugement du 22 septembre 2011 par lequel le tribunal administratif d'Orléans a fait droit à cette demande en tant qu'elle la concerne ; <br/>
<br/>
              Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              2. Considérant que le premier alinéa de l'article R. 711-3 du code de justice administrative dispose que : " Si le jugement de l'affaire doit intervenir après le prononcé de conclusions du rapporteur public, les parties ou leurs mandataires sont mis en mesure de connaître, avant la tenue de l'audience, le sens de ces conclusions sur l'affaire qui les concerne " ; <br/>
<br/>
              3. Considérant que la communication aux parties du sens des conclusions, prévue par ces dispositions, a pour objet de mettre les parties en mesure d'apprécier l'opportunité d'assister à l'audience publique, de préparer, le cas échéant, les observations orales qu'elles peuvent y présenter, après les conclusions du rapporteur public, à l'appui de leur argumentation écrite et d'envisager, si elles l'estiment utile, la production, après la séance publique, d'une note en délibéré ; qu'en conséquence, les parties ou leurs mandataires doivent être mis en mesure de connaître, dans un délai raisonnable avant l'audience, l'ensemble des éléments du dispositif de la décision que le rapporteur public compte proposer à la formation de jugement d'adopter, à l'exception de la réponse aux conclusions qui revêtent un caractère accessoire, notamment celles qui sont relatives à l'application de l'article L. 761-1 du code de justice administrative ; que cette exigence s'impose à peine d'irrégularité de la décision rendue sur les conclusions du rapporteur public ; qu'en indiquant aux parties, près de vingt-quatre heures avant l'audience, qu'il conclurait au rejet au fond de la requête de la SNCF, le rapporteur public devant la cour administrative d'appel de Nantes les a informées, dans un délai raisonnable avant l'audience, du sens de ses conclusions en indiquant les éléments du dispositif de la décision qu'il comptait proposer à la formation de jugement d'adopter ; que le moyen tiré de ce que l'arrêt aurait été rendu au terme d'une procédure irrégulière doit, par suite, être écarté ;  <br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué :<br/>
<br/>
              4. Considérant, en premier lieu, qu'aux termes de l'article 1er de la loi du 13 février 1997 portant création de l'établissement public " Réseau ferré de France " en vue du renouveau du transport ferroviaire : " (...) "Réseau ferré de France" (...) est le gestionnaire du réseau ferré national. / Compte tenu des impératifs de sécurité et de continuité du service public, la gestion du trafic et des circulations sur le réseau ferré national ainsi que le fonctionnement et l'entretien des installations techniques et de sécurité de ce réseau sont assurés par la Société nationale des chemins de fer français pour le compte et selon les objectifs et principes de gestion définis par Réseau ferré de France. (...) " ; qu'aux termes de l'article 5 de la même loi : " Les biens constitutifs de l'infrastructure (...) sont (...) apportés en pleine propriété à Réseau ferré de France. Les biens constitutifs de l'infrastructure comprennent les voies, y compris les appareillages fixes associés, les ouvrages d'art et les passages à niveau, les quais à voyageurs et à marchandises, les triages et les chantiers de transport combiné, les installations de signalisation, de sécurité, de traction électrique et de télécommunications liées aux infrastructures, les bâtiments affectés au fonctionnement et à l'entretien des infrastructures " ; qu'enfin, aux termes de l'article 11 du décret du 5 mai 1997 relatif aux missions et aux statuts de Réseau ferré de France : " Dans le cadre des objectifs et principes de gestion du réseau ferré national définis à l'article 7, la SNCF exerce les missions prévues à l'article 1er de la loi du 13 février 1997 susvisée. Ces missions comportent en particulier : / (...) la surveillance, l'entretien régulier, les réparations, dépannages et mesures nécessaires au fonctionnement du réseau et à la sécurité de l'ensemble des plates-formes, ouvrages d'art, voies, quais, réseaux, installations et bâtiments techniques s'y rattachant " ;<br/>
<br/>
              5. Considérant qu'il résulte de ces dispositions que la SNCF, chargée de l'entretien des passages à niveau comme prestataire de services de Réseau ferré de France, peut voir sa responsabilité engagée vis-à-vis des usagers de ces ouvrages publics lorsque ceux-ci ont subi des dommages directement imputables à un défaut d'entretien normal de ces ouvrages ; que l'entretien normal de l'ouvrage inclut notamment la signalisation de ses caractéristiques et de son éventuelle dangerosité, signalisation dont l'insuffisance ou l'absence peut caractériser un défaut d'un tel entretien et être, dès lors, susceptible d'engager la responsabilité de la SNCF ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède, en premier lieu, que la cour administrative d'appel de Nantes, qui a porté sur les faits de l'espèce une appréciation souveraine exempte de dénaturation, n'a pas commis d'erreur de droit en jugeant que le défaut de signalisation, aux abords de celui-ci, de la dangerosité particulière du passage à niveau n° 37 pour les conducteurs de véhicules longs, tel celui accidenté, compte tenu de la très grande brièveté du délai séparant le début de l'alarme sonore et la fermeture des barrières, caractérisait un défaut d'entretien normal, par la SNCF, de cet ouvrage et engageait la responsabilité de celle-ci ;<br/>
<br/>
              7. Considérant, en second lieu, qu'après avoir admis la responsabilité de la SNCF ainsi que celle du département d'Eure-et-Loir en sa qualité de gestionnaire de la voirie départementale, la cour a exactement qualifié les faits en estimant que l'imprudence commise par le chauffeur du poids lourd, dont elle a relevé qu'il n'avait pas choisi d'emprunter l'itinéraire conseillé de contournement de l'agglomération de Chartres qui lui aurait permis d'éviter le passage à niveau, n'était pas la cause exclusive de la collision survenue avec le train et ne pouvait, en conséquence, conduire qu'à une exonération partielle de la responsabilité de la SNCF ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que la SNCF n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de la compagnie Zürich Versicherung qui n'est pas, dans la présente instance, la partie perdante, le versement d'une somme au titre des frais exposés par la SNCF et non compris dans les dépens ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la compagnie Zürich Versicherung en application de ces dispositions et de mettre à la charge de la SNCF le versement d'une somme de 2 500 euros ; qu'il n'y a, en revanche, pas lieu de faire droit aux conclusions présentées par le département d'Eure-et-Loir en application de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société nationale des chemins de fer français est rejeté.<br/>
Article 2 : La société nationale des chemins de fer français versera à la compagnie Zürich Versicherung une somme de 2 500 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Les conclusions présentées par le département d'Eure-et-Loir en application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la société nationale des chemins de fer français, à la compagnie Zürich Versicherung et au département d'Eure-et-Loir.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
