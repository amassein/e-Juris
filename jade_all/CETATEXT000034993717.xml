<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034993717</ID>
<ANCIEN_ID>JG_L_2017_06_000000405932</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/99/37/CETATEXT000034993717.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 21/06/2017, 405932, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>405932</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Cyrille Beaufils</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:405932.20170621</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 405932, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 13 décembre 2016, 10 mars 2017 et 26 mai 2017 au secrétariat du contentieux du Conseil d'Etat, l'Union nationale des professions libérales (UNAPL) et l'Union des entreprises de proximité demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2016-1356 du 11 octobre 2016 relatif aux centres de gestion, associations et organismes mixtes de gestion agréés, aux professionnels de l'expertise comptable et aux certificateurs à l'étranger ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 405943, par une requête et deux mémoires en réplique, enregistrés les 13 décembre 2016, 12 mai 2017 et 29 mai 2017 au secrétariat du contentieux du Conseil d'Etat, l'Association médicale indépendante de gestion agréée (AMIGA), le syndicat des médecins indépendants libéraux européens, le syndicat des mésothérapeutes français, l'Union collégiale, le syndicat de la médecine homéopathique, l'association agréée des chirurgiens dentistes de Provence, l'association de gestion agréée pour l'assistance, la formation et la prévention des difficultés, l'association agréée des chirurgiens dentistes du Haut-Rhin, l'association de gestion des chirurgiens dentistes de Vaucluse et le comité de liaison des associations agréées et associations de gestion et de comptabilité des chirurgiens-dentistes et des professionnels de santé demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le même décret ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ;<br/>
              - le traité sur le fonctionnement de l'Union européenne, notamment ses articles 49 et 56 ;<br/>
              - la directive 2006/123/CE du Parlement européen et du Conseil du 12 décembre 2006 ; <br/>
              - le code de commerce ; <br/>
              - le code général des impôts ;<br/>
              - la loi n° 2015-1786 du 29 décembre 2015 ;<br/>
              - l'ordonnance n° 45-2138 du 19 septembre 1945 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Cyrille Beaufils, auditeur,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que le chapitre I ter du titre premier de la troisième partie du livre premier du code général des impôts organise le régime des centres et associations de gestion agréés dont l'objet est, notamment, de fournir aux industriels, commerçants, artisans et agriculteurs ainsi qu'aux membres des professions libérales et titulaires des charges et offices une assistance en matière de gestion et une analyse des informations économiques, comptables et financières en matière de prévention des difficultés économiques et financières et de faciliter l'accomplissement de leurs obligations administratives et fiscales ; que l'adhésion à ces organismes de gestion agréés emporte, pour leurs membres, divers avantages fiscaux, notamment l'exemption de la majoration de leurs revenus professionnels prévue au point 7 de l'article 158 du code général des impôts ; que les articles 1649 quater C et F du même code prévoient que les conditions d'agrément des centres et associations de gestion agréés sont définies par un décret en Conseil d'Etat ; que le décret du 11 octobre 2016 attaqué a été pris pour préciser, notamment, les modalités d'application de l'article 37 de la loi du 29 décembre 2015 de finances rectificative pour 2015 qui a modifié les missions et conditions d'exercice de ces organismes de gestion agréés ; <br/>
<br/>
              2. Considérant que les requêtes visées ci-dessus sont dirigées contre le même décret ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              Sur la légalité externe du décret attaqué :<br/>
<br/>
              3. Considérant, d'une part, qu'aux termes de l'article 22 de la Constitution : " Les actes du Premier ministre sont contresignés, le cas échéant, par les ministres chargés de leur exécution " ; que l'UNAPL et autre ne font état d'aucune mesure réglementaire ou individuelle que devraient nécessairement signer les ministres de l'intérieur, de la justice et de l'agriculture pour l'exécution du décret attaqué ; que les secrétaires d'Etat chargés de l'industrie et de l'artisanat, de la consommation et de l'économie sociale et solidaire étaient placés, à la date de l'édiction du décret attaqué, auprès du ministre de l'économie et des finances et n'avaient donc pas la qualité de ministre au sens des dispositions de la Constitution ; qu'il suit de là que le moyen tiré de ce que le décret attaqué serait entaché d'irrégularité faute d'avoir été contresigné par ces ministres et secrétaires d'Etat ne peut qu'être écarté ;<br/>
<br/>
              4. Considérant, d'autre part, que l'article L. 462-2 du code de commerce prévoit que " L'Autorité est obligatoirement consultée par le Gouvernement sur tout projet de texte réglementaire instituant un régime nouveau ayant directement pour effet : / 1° De soumettre l'exercice d'une profession ou l'accès à un marché à des restrictions quantitatives ; / 2° D'établir des droits exclusifs dans certaines zones ; / 3° D'imposer des pratiques uniformes en matière de prix ou de conditions de vente. " ; que le décret attaqué n'ayant ni pour objet, ni pour effet, en modifiant les conditions d'agrément des organismes de gestion agréés, de soumettre l'exercice d'une profession ou l'accès à un marché à des restrictions quantitatives, d'établir des droits exclusifs dans certaines zones ou d'imposer des pratiques uniformes en matière de prix ou de conditions de vente, le moyen tiré ce qu'il aurait été édicté en méconnaissance de ces dispositions, faute pour l'Autorité de la concurrence d'avoir été consultée, ne peut qu'être écarté comme inopérant ; <br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              En ce qui concerne les moyens relatifs aux conditions d'agrément des organismes de gestion agréés :<br/>
<br/>
              5. Considérant, d'une part, qu'aux termes de l'article 49 du traité sur le fonctionnement du l'Union européenne : " Dans le cadre des dispositions ci-après, les restrictions à la liberté d'établissement des ressortissants d'un État membre dans le territoire d'un autre État membre sont interdites. (...) " ; qu'aux termes de son article 56 : " Dans le cadre des dispositions ci-après, les restrictions à la libre prestation des services à l'intérieur de l'Union sont interdites à l'égard des ressortissants des États membres établis dans un État membre autre que celui du destinataire de la prestation. (...) " ; que l'article 14 de la directive 2006/123/CE du Parlement européen et du Conseil du 12 décembre 2006 relative aux services dans le marché intérieur prévoit que : " Les États membres ne subordonnent pas l'accès à une activité de services ou son exercice sur leur territoire au respect de l'une des exigences suivantes: / 1) les exigences discriminatoires fondées directement ou indirectement sur la nationalité ou, en ce qui concerne les sociétés, l'emplacement du siège statutaire, en particulier : / a) l'exigence de nationalité pour le prestataire, son personnel, les personnes détenant du capital social ou les membres des organes de gestion ou de surveillance du prestataire, / b) l'exigence d'être résident sur leur territoire pour le prestataire, son personnel, les personnes détenant du capital social ou les membres des organes de gestion ou de surveillance du prestataire; (...) " ; <br/>
<br/>
              6. Considérant que le 8° de l'article 1er du décret attaqué, qui modifie l'article 371 G de l'annexe 2 du code général des impôts, a pour seul objet de remplacer la commission d'agrément instituée au chef-lieu de région par le directeur régional des finances publiques en tant qu'autorité compétente pour délivrer l'agrément des centres de gestion agréés ; que l'article 371 Z nonies de la même annexe, introduit par le décret attaqué, se borne à prévoir que la même autorité est compétente pour délivrer l'agrément des organismes mixtes de gestion agréés ; que, par suite, le moyen tiré de ce que l'article 1er du décret attaqué, en tant respectivement qu'il modifie et insère ces deux articles, méconnaîtrait les principes de la liberté d'établissement et de la libre prestation de services garantis par les dispositions citées au point précédent en réservant aux personnes établies en France l'accès aux activités mentionnées aux articles 1649 quater C, F et K du code général des impôts doit être écarté ;<br/>
<br/>
              7. Considérant, d'autre part, que l'article 1er du décret attaqué modifie l'article 371 B de l'annexe 2 du code général des impôts, pour porter le nombre minimal d'adhérents pour l'agrément d'un centre de gestion de cent à cinq cents et le nombre minimal pour son renouvellement dans un délai de trois ans de trois cents à mille ; qu'il modifie l'article 371 N pour porter le nombre minimal d'adhérents pour l'agrément d'une association de gestion de cinquante à cinq cents, et mille pour son renouvellement ; que l'article 371 Z ter, introduit par l'article 1er du décret attaqué, prévoit les mêmes seuils pour les organismes mixtes agréés ; que ces obligations ne sont, aux termes de ces mêmes articles, pas applicables aux organismes de gestion agréés établis en Corse, en Guadeloupe, en Guyane, en Martinique, à Mayotte et à La Réunion ; que ces dispositions entrent en vigueur, en vertu de l'article 3 du décret attaqué, à compter du 1er janvier 2019 ; <br/>
<br/>
              8. Considérant, en premier lieu, qu'en fixant le nombre minimal d'adhérents d'un centre ou d'une association de gestion agréés, l'article 1er du décret attaqué n'a pas excédé les limites de la délégation accordée par les articles 1649 quater C et F du code général des impôts, rappelés au point 1, qui prévoient que les conditions d'agrément des organismes de gestion sont définies par un décret en Conseil d'Etat ; que le moyen tiré de ce que l'article 1er du décret attaqué serait pour ce motif contraire au principe de la liberté d'association n'est, en tout état de cause, pas fondé ; <br/>
<br/>
              9. Considérant, en deuxième lieu, que le principe d'égalité ne s'oppose pas à ce que l'autorité investie du pouvoir réglementaire règle de façon différente des situations différentes ni à ce qu'elle déroge à l'égalité pour des raisons d'intérêt général, pourvu que la différence de traitement qui en résulte soit, dans l'un comme l'autre cas, en rapport avec l'objet de la norme qui l'établit et ne soit pas manifestement disproportionnée au regard des motifs susceptibles de la justifier ; <br/>
<br/>
              10. Considérant qu'en fixant les nombres minimaux d'adhérents, rappelés au point 7, que doivent compter les organismes de gestion agréés, le décret attaqué n'instaure pas une différence de traitement manifestement disproportionnée entre les organismes situés en-dessous et au-dessus de ces seuils, au regard de l'objet donné à ces organismes par la loi et de l'intérêt général qui s'attache à ce que ces derniers soient d'une taille suffisante pour favoriser l'indépendance et l'expertise nécessaires à la conduite efficace de leurs missions ; que les organismes établis en Corse et dans les départements d'outre-mer, qui ne sont pas soumis à la condition du nombre minimal d'adhérents, sont placés dans une situation différente, au regard de l'objet du décret, de celle du reste des organismes de gestion agréés, en considération des contraintes géographiques qui s'imposent à eux et compte tenu du nombre de professionnels concernés ; que, par suite, le moyen tiré de la méconnaissance du principe d'égalité ne peut qu'être écarté ;<br/>
<br/>
              11. Considérant, en troisième lieu, qu'aux termes de l'article 101 du traité sur le fonctionnement de l'Union européenne : " 1. Sont incompatibles avec le marché intérieur et interdits tous accords entre entreprises, toutes décisions d'associations d'entreprises et toutes pratiques concertées, qui sont susceptibles d'affecter le commerce entre États membres et qui ont pour objet ou pour effet d'empêcher, de restreindre ou de fausser le jeu de la concurrence à l'intérieur du marché intérieur (...). " ; qu'ainsi qu'il a été interprété par la Cour de justice de l'Union européenne, cet article, lu en combinaison avec l'article 4, paragraphe 3, du traité sur l'Union européenne, impose aux Etats membres de ne pas prendre ou maintenir en vigueur des mesures, même de nature législative ou réglementaire, susceptibles d'éliminer son effet utile à l'égard des entreprises ; que constitue notamment une telle mesure tout acte par lequel un Etat membre impose ou favorise la conclusion d'ententes contraires à l'article 101 du traité sur le fonctionnement de l'Union européenne ;<br/>
<br/>
              12. Considérant qu'il ne ressort pas des pièces du dossier que le relèvement du nombre minimal d'adhérents des organismes de gestion agréés, fixé à cinq cents pour l'agrément et mille pour son renouvellement par l'article 1er du décret attaqué, serait susceptible, par lui-même, de favoriser la conclusion d'ententes entre organismes de gestion agréés ; que, par suite, le moyen tiré de ce que le décret attaqué méconnaîtrait les dispositions de l'article 101 du traité sur le fonctionnement de l'Union européenne ne peut qu'être écarté ;<br/>
<br/>
              13. Considérant enfin que le moyen tiré de ce que les dispositions transitoires prévues par le décret attaqué, qui reportent au 1er janvier 2019 l'entrée en vigueur des nouveaux seuils d'adhérents mentionnés ci-dessus, méconnaîtraient le principe de sécurité juridique n'est pas assorti des précisions de nature à permettre d'en apprécier le bien-fondé ; <br/>
<br/>
              En ce qui concerne les moyens relatifs à l'examen périodique de sincérité :<br/>
<br/>
              14. Considérant que les articles 1649 quater E et H du code général des impôts prévoient que les centres et associations de gestion agréés " demandent à leurs adhérents tous renseignements et documents utiles afin de procéder, sous leur propre responsabilité, (...) à un examen périodique de sincérité selon des modalités définies par décret en Conseil d'Etat. Cet examen ne constitue pas le début d'une des procédures mentionnées aux articles L. 12 et L. 13 du livre des procédures fiscales. (...) / Les centres sont tenus d'adresser à leurs adhérents un compte rendu de mission dans les deux mois qui suivent la fin des opérations de contrôle. Dans le même délai, une copie de ce compte rendu est transmise, par le centre, au service des impôts des entreprises dont dépend l'adhérent concerné. (...) " ; <br/>
<br/>
              15. Considérant qu'aux termes de l'article 371 E de l'annexe 2 du code général des impôts, dans sa rédaction résultant du décret attaqué : " (...) 4° Le centre réalise un examen périodique de sincérité de pièces justificatives de ses adhérents dans le but de vérifier que leurs déclarations fiscales sont correctement établies. Cet examen suit une méthode établie par le centre pour l'ensemble de ses adhérents. Pour déterminer les adhérents faisant l'objet, au titre d'une année donnée, d'un examen périodique de pièces justificatives, le centre sélectionne des adhérents selon une méthode fixée par arrêté du ministre chargé du budget assurant la réalisation de cet examen au moins tous les six ans lorsque les comptes de l'adhérent sont tenus ou présentés annuellement par un professionnel de l'expertise comptable et au moins tous les trois ans dans le cas contraire. Le nombre des pièces examinées est modulé selon la taille de l'entreprise. Le choix des pièces examinées prend appui sur la remise, par l'adhérent, d'un document fournissant une vision exhaustive des opérations comptables de l'entreprise. Ce document est détruit par le centre une fois l'examen réalisé. Il n'est en aucun cas fourni par le centre à l'administration fiscale. L'adhérent est mis en mesure de présenter ses observations en réponse aux éventuelles questions et critiques formulées par le centre dans le cadre de cet examen. / Cet examen fait l'objet du compte rendu de mission tel que prévu à l'article 1649 quater E du code général des impôts ; (...) " ; que l'article 1er du décret attaqué a inséré à l'article 371 Q de la même annexe des dispositions semblables applicables aux associations de gestion agréées et, à l'article 371 Z sexies, des dispositions prévoyant que les organismes mixtes de gestion agréés réalisent un examen périodique de sincérité dans les même conditions ; <br/>
<br/>
              16. Considérant, en premier lieu, que le principe de l'examen périodique de sincérité conduit par les organismes de gestion agréés est prévu par les dispositions législatives citées au point 14 ; que le moyen tiré de ce que la mission ainsi définie méconnaîtrait les dispositions de l'article L. 10 du livre des procédures fiscales ne peut, par suite, qu'être écarté comme inopérant ; que les moyens tirés de ce que cette mission porterait atteinte à la liberté d'association et méconnaîtrait le principe de la souveraineté nationale ne peuvent être utilement invoqués en dehors de la procédure prévue à l'article 61-1 de la Constitution ; <br/>
<br/>
              17. Considérant, en deuxième lieu, qu'il résulte des dispositions des articles 1649 quater E et H du code général des impôts rappelées au point 14, éclairées par leurs travaux préparatoires, que le législateur a entendu confier pour mission aux organismes de gestion agréés de contrôler, dans le cadre de l'examen périodique de sincérité dont les modalités sont prévues par le décret attaqué, la sincérité des déclarations fiscales de leurs adhérents en les rapprochant des opérations comptables de l'entreprise et de pièces justificatives ; que ces dispositions prévoient que cet examen fait l'objet d'un compte-rendu de mission qui est transmis à l'administration fiscale ; qu'il résulte en outre de leurs termes mêmes que l'examen ainsi défini ne constitue pas le début d'une des procédures de contrôle fiscal mentionnées aux articles L. 12 et L. 13 du livre des procédures fiscales ni ne saurait en constituer des éléments ; que les requérants ne sont, par suite, pas fondés à soutenir que le décret attaqué aurait, en reprenant ces caractéristiques, donné à l'examen périodique de sincérité le caractère d'un début de vérification de comptabilité, en méconnaissance de ces mêmes dispositions ; <br/>
<br/>
              18. Considérant, en troisième lieu, qu'aux termes de l'article 2 de l'ordonnance du 19 septembre 1945 portant institution de l'ordre des experts-comptables et réglementant le titre et la profession d'expert-comptable : " Est expert comptable ou réviseur comptable au sens de la présente ordonnance celui qui fait profession habituelle de réviser et d'apprécier les comptabilités des entreprises et organismes auxquels il n'est pas lié par un contrat de travail. Il est également habilité à attester la régularité et la sincérité des comptes de résultats. / L'expert-comptable fait aussi profession de tenir, centraliser, ouvrir, arrêter, surveiller, redresser et consolider les comptabilités des entreprises et organismes auxquels il n'est pas lié par un contrat de travail. " ; que l'article 20 de la même ordonnance réserve l'exécution habituelle de ces travaux aux experts-comptables ; que, par suite, les adhérents des organismes de gestion agréés dont les comptes sont tenus ou présentés annuellement par un expert-comptable, professionnel indépendant et spécialement qualifié, sont placés dans une situation différente, eu égard à l'objet de l'examen périodique de sincérité rappelé ci-dessus, de ceux qui tiennent et présentent eux-mêmes leurs comptes, ou font appel au service d'un conseil en gestion ou d'un avocat fiscaliste ; qu'en prévoyant que l'examen périodique de sincérité est réalisé plus fréquemment pour les adhérents placés dans cette dernière situation, la différence de traitement instaurée par le décret attaqué, qui est en rapport avec cet objet, ne méconnaît donc pas le principe d'égalité ;<br/>
<br/>
              19. Considérant, en quatrième lieu, que la différence de fréquence de l'examen périodique de sincérité selon que les comptes de l'entreprise sont ou non tenus ou présentés annuellement par un expert-comptable est uniquement fondée sur la compétence reconnue à ce dernier en matière de tenue des comptes par l'ordonnance du 19 septembre 1945 précitée ; que les dispositions du décret attaqué citées au point 15 n'ont pas pour objet d'encourager les adhérents des organismes de gestion agréés à recourir par ailleurs aux services d'un expert-comptable en matière de conseil fiscal, au détriment des autres professionnels de ce secteur établis dans les autres Etats membres de l'Union européenne ; que le moyen tiré de ce que ces dispositions méconnaîtraient, pour ce motif, la liberté d'établissement et la libre prestation de services garanties par les articles 49 et 56 précités du traité sur le fonctionnement de l'Union européenne ne peut qu'être écarté ; que le moyen tiré de la méconnaissance du principe d'égalité qui résulterait, pour les professionnels établis en France, du constat de cette illégalité n'est, dès lors, pas fondé ; que le moyen tiré de l'atteinte à la liberté d'entreprendre des autres professionnels du conseil fiscal ne peut qu'être écarté pour les mêmes motifs ; <br/>
<br/>
              20. Considérant, en cinquième lieu, que, contrairement à ce qui est soutenu, aucun principe n'impose que " tout collaborateur, même occasionnel du service public ait droit à une rémunération pour le service effectué " ; que, par suite, le moyen tiré de ce que le décret attaqué méconnaîtrait un tel principe en confiant aux organismes de gestion agréés la mission de contrôler les déclarations fiscales de leurs adhérents sans prévoir de compensation financière de la part de l'Etat ne peut, en tout état de cause, qu'être écarté ;<br/>
<br/>
              En ce qui concerne les moyens relatifs à la définition de l'objet des associations de gestion agréées :<br/>
<br/>
              21. Considérant enfin qu'aux termes du quatrième alinéa de l'article 371 M de l'annexe 2 du code général des impôts, dans sa rédaction précédant l'entrée en vigueur du décret attaqué : " Les associations doivent avoir pour objet de développer chez leurs membres l'usage de la comptabilité et de faciliter à ces derniers l'accomplissement de leurs obligations administratives et fiscales. (...) " ; que, dans sa rédaction résultant de l'article 1er du décret attaqué, ce même alinéa dispose : " Les associations doivent avoir pour objet de développer chez leurs membres l'usage de la comptabilité, sous réserve des dispositions de l'ordonnance n° 45-2138 du 19 septembre 1945 portant institution de l'ordre des experts-comptables et réglementant le titre et la profession d'expert-comptable, de faciliter à ces derniers l'accomplissement de leurs obligations administratives et fiscales et de leur fournir une assistance en matière de gestion. (...) " ; qu'en précisant que la mission des associations de gestion agréées consistant à développer l'usage de la comptabilité chez leurs membres ne pouvait s'étendre aux missions réservées aux experts-comptables par l'ordonnance du 19 septembre 1945 citée plus haut, notamment en matière de tenue des comptes pour des tiers, le décret attaqué n'a pas modifié l'état du droit applicable ; qu'il n'a pas méconnu l'objectif à valeur constitutionnelle d'accessibilité et d'intelligibilité de la norme ; <br/>
<br/>
              22. Considérant qu'il résulte de tout ce qui précède l'UNAPL et autre et l'AMIGA et autres ne sont pas fondés à demander l'annulation du décret qu'ils attaquent ; que leurs requêtes doivent, par suite, être rejetées, y compris leurs conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes de l'UNAPL et autre et l'AMIGA et autres sont rejetées.<br/>
<br/>
Article 2 : La présente décision sera notifiée, pour l'ensemble des requérants sous le n° 405932, à l'Union nationale des professions libérales, première dénommée, pour l'ensemble des requérants sous le n° 405943, à l'association médicale indépendante de gestion agréée, première dénommée, et au ministre de l'action et des comptes publics.<br/>
      Copie en sera adressée au Premier ministre. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
