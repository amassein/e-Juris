<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000008253803</ID>
<ANCIEN_ID>JGXLX2006X05X000000259989</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/08/25/38/CETATEXT000008253803.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'Etat, 7ème et 2ème sous-sections réunies, du 3 mai 2006, 259989, inédit au recueil Lebon</TITRE>
<DATE_DEC>2006-05-03</DATE_DEC>
<JURIDICTION>Conseil d'Etat</JURIDICTION>
<NUMERO>259989</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7EME ET 2EME SOUS-SECTIONS REUNIES</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Stirn</PRESIDENT>
<AVOCATS>COSSA ; SCP ROGER, SEVAUX</AVOCATS>
<RAPPORTEUR>M. Francis  Girault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Boulouis</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 3 septembre 2003 et 5 janvier 2004 au secrétariat du contentieux du Conseil d'Etat, présentés pour la CHAMBRE DE COMMERCE ET D'INDUSTRIE DE COLMAR ET DU CENTRE ALSACE, dont le siège est 1, place de la Gare à Colmar (68000)  ; la CHAMBRE DE COMMERCE ET D'INDUSTRIE DE COLMAR ET DU CENTRE ALSACE demande au Conseil d'Etat  :
<br/>
<br/>
     
              1°) d'annuler l'arrêt du 3 juillet 2003 par lequel la cour administrative d'appel de Nancy  a rejeté sa demande tendant  à l'annulation du jugement du 26 février 1998 par lequel le tribunal administratif de Strasbourg a annulé la décision du 4 avril 1996 par laquelle son président a réintégré M. A en qualité de chargé de mission aux antennes de Guebwiller et de Sainte-Marie-aux-Mines, l'a condamnée à verser à ce dernier une somme de 100 000 francs en réparation des préjudices résultant des conditions de sa réintégration et lui a ordonné de reconstituer la carrière de l'intéressé   ;
<br/>
<br/>
     
              2°) de rejeter les demandes de M. A devant le tribunal administratif de Strasbourg  ;
<br/>
<br/>
     
              3°) de mettre à la charge de M. A la somme de 10 000 francs au titre de l'article L. 761-1 du code de justice administrative  ;
<br/>
<br/>
<br/>
     
              Vu les autres pièces du dossier  ;
<br/>
<br/>
     
              Vu le code de justice administrative  ;
<br/>
<br/>
<br/>
     
              Après avoir entendu en séance publique  :
<br/>
<br/>
     
              - le rapport de M. Francis Girault, Maître des Requêtes,  
<br/>
<br/>
     
              - les observations de Me Cossa, avocat de la CHAMBRE DE COMMERCE ET D'INDUSTRIE DE COLMAR ET DU CENTRE ALSACE et de la SCP Roger, Sevaux, avocat de M. A, 
<br/>
<br/>
     
              - les conclusions de M. Nicolas Boulouis, Commissaire du gouvernement  ;
<br/>
<br/>
<br/>
<br/>
<br/>Considérant que M. A, agent titulaire de la CHAMBRE DE COMMERCE ET D'INDUSTRIE DE COLMAR ET DU CENTRE ALSACE, a été réintégré à l'issue d'une période de mise à disposition de 1985 à 1996 auprès de l'association Promaral, au sein des services de la Chambre par décision de son président en date du 4 avril 1996, en qualité de chargé de mission aux antennes de Guebwiller et Sainte-Marie-aux-Mines, avec le grade de chef de section principal  ; que par jugement en date du 26 février 1998, le tribunal administratif de Strasbourg a annulé cette décision et condamné la CHAMBRE DE COMMERCE ET D'INDUSTRIE DE COLMAR ET DU CENTRE ALSACE à indemniser M. A du préjudice moral et des troubles dans les conditions d'existence qu'il a subis du fait du retard mis à le réintégrer et en raison de sa nouvelle affectation qui ne correspondait pas à sa qualification  ; que la cour administrative d'appel de Nancy par son arrêt en date du 3 juillet 2003, a confirmé le jugement du tribunal administratif en rejetant la requête de la chambre consulaire et l'appel incident formé par M. A  ; que la CHAMBRE DE COMMERCE ET D'INDUSTRIE DE COLMAR ET DU CENTRE ALSACE se pourvoit contre cet arrêt  ;
<br/>
<br/>
     
              Considérant que la CHAMBRE DE COMMERCE ET D'INDUSTRIE DE COLMAR ET DU CENTRE ALSACE soutient que la cour a insuffisamment motivé son arrêt en ne répondant pas au moyen, qui est opérant, selon lequel les avancements de M. A durant sa mise à disposition de l'association Promaral, sont intervenus pour ordre  ; que la cour, qui se borne à relever que M. A a été placé lors de sa nouvelle affectation dans le grade de chef de section principal, inférieur à celui de chef de section, 3ème degré, grade qu'il détenait avant sa réintégration, n'a pas répondu au moyen tiré de ce que les promotions dont M. A a bénéficié au cours de sa mise à disposition auprès de l'association Promaral, ont été prononcées pour ordre et doivent être regardées comme nulles et non avenues  ; que, par suite, la cour a entaché son arrêt d'une insuffisance de motivation  ; que, dès lors, la CHAMBRE DE COMMERCE ET D'INDUSTRIE DE COLMAR ET DU CENTRE ALSACE est fondée à demander l'annulation de l'arrêt attaqué  ;
<br/>
<br/>
     
              Considérant qu'aux termes de l'article L. 821-2 du code de justice administrative, le Conseil d'Etat, s'il prononce l'annulation d'une décision d'une juridiction administrative statuant en dernier ressort, peut régler l'affaire au fond si l'intérêt d'une bonne administration de la justice le justifie  ; que, dans les circonstances de l'espèce, il y a lieu de régler l'affaire au fond  ;
<br/>
<br/>
     
              Sur l'appel principal  :
<br/>
<br/>
     
              Sur les conclusions à fin d'annulation  :
<br/>
<br/>
     
              Considérant qu'aux termes de l'article 35 du statut du personnel administratif des chambres de commerce et d'industrie, applicable à la date de la décision contestée  : En cas de suppression d'emploi, l'agent qui, dans la même compagnie consulaire, aura été reclassé dans une situation inférieure à celle qu'il occupait auparavant, aura droit au paiement d'une indemnité différentielle pendant une durée maximum de trois ans  ;
<br/>
<br/>
     
              Considérant qu'il ressort des pièces du dossier qu'à la date à laquelle il a été réintégré au sein de la CHAMBRE DE COMMERCE ET D'INDUSTRIE DE COLMAR ET DU CENTRE ALSACE, M. A détenait le grade de chef de section 3ème degré  ; que par décision du président de cette chambre en date du 4 avril 1996, il a été affecté sur l'emploi de chargé de mission aux antennes de Guebwiller et de Sainte-Marie-aux-Mines qui correspond au grade de chef de section principal, inférieur à celui détenu avant sa réintégration  ; que les dispositions précitées de l'article 35 du statut du personnel administratif des chambres de commerce et d'industrie ne permettaient pas de fonder légalement une telle affectation dès lors qu'elles ne trouvent à s'appliquer qu'en cas de suppression d'emploi au sein de la compagnie consulaire et non à la suite d'une suppression d'emploi au sein d'une association auprès de laquelle un agent de la chambre est mis à disposition  ; que si la compagnie consulaire soutient que, si M. A était resté en fonction au sein des services de la chambre, il n'aurait pas bénéficié des promotions dont il a fait l'objet alors qu'il était mis à la disposition de l'association Promaral, cette allégation, ne saurait suffire à établir que les décisions de promotion de M. A auraient été prises pour ordre  ; qu'ainsi, quand bien même la CHAMBRE DE COMMERCE ET D'INDUSTRIE DE COLMAR ET DU CENTRE ALSACE ne disposait pas d'un emploi vacant correspondant au grade de M. A et a accordé à ce dernier une allocation différentielle, la décision du 4 avril 1996 affectant M. A sur le poste de chargé de mission aux antennes de Guebwiller et de Sainte-Marie-aux-Mines est entachée d'excès de pouvoir  ; que la CHAMBRE DE COMMERCE ET D'INDUSTRIE DE COLMAR ET DU CENTRE ALSACE n'est, par suite, pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Strasbourg l'a annulée  ;
<br/>
<br/>
     
              Sur les conclusions indemnitaires  :
<br/>
<br/>
     
              Considérant qu'il ne résulte pas de l'instruction que les premiers juges auraient inexactement apprécié les troubles dans les conditions d'existence et le préjudice moral subis par M. A en allouant à celui-ci une indemnité de 100 000 francs (15 245 euros)  ; que par suite, ni la CHAMBRE DE COMMERCE ET D'INDUSTRIE DE COLMAR ET DU CENTRE ALSACE ni M. A ne sont fondés à demander la réformation sur ce point du jugement attaqué  ;
<br/>
<br/>
     
              Sur les conclusions à fin d'injonction  : 
<br/>
<br/>
     
              Considérant que si la CHAMBRE DE COMMERCE ET D'INDUSTRIE DE COLMAR ET DU CENTRE ALSACE soutient que les premiers juges ne pouvaient lui enjoindre de reconstituer la carrière de M. A dés lors que ce dernier, ayant perçu une indemnité différentielle, n'avait pas subi de perte de rémunération , le tribunal administratif s'est borné à enjoindre à la chambre de reclasser M. A à un indice correspondant au grade qui devrait être le sien  si, après sa réintégration en avril 1996 sur un emploi correspondant au grade de chef de service 3ème degré, il avait suivi une progression normale de carrière  ;
<br/>
<br/>
     
              Sur la capitalisation des intérêts  :
<br/>
<br/>
     
              Considérant que la capitalisation des intérêts a été demandée par M. A le 4 août 2005  ; qu'à cette date, il était dû au moins une année d'intérêts  ; que dés lors, conformément à l'article 1154 du code civil, il y a lieu de faire droit à cette demande  ;
<br/>
<br/>
     
              Sur les conclusions de la CHAMBRE DE COMMERCE ET D'INDUSTRIE DE COLMAR ET DU CENTRE ALSACE et de M. A tendant à l'application de l'article L. 761-1 du code de justice administrative  :
<br/>
<br/>
     
              Considérant qu'il n'y a pas lieu dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge de M. A la somme que demande la CHAMBRE DE COMMERCE ET D'INDUSTRIE DE COLMAR ET DU CENTRE ALSACE au titre des frais exposés par elle  et non compris dans les dépens  ; qu'il n'y a pas davantage lieu de mettre à la charge de la CHAMBRE DE COMMERCE ET D'INDUSTRIE DE COLMAR ET DU CENTRE ALSACE la somme que demande M. A au titre des frais exposés par lui  et non compris dans les dépens  ; 
<br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E  :
<br/>
     
              				--------------
<br/>
<br/>
     
Article 1er  : L'arrêt en date du 3 juillet 2003 de la cour administrative d'appel de Nancy est annulé.
<br/>
<br/>
     
Article 2  : La requête d'appel de la CHAMBRE DE COMMERCE ET D'INDUSTRIE DE COLMAR ET DU CENTRE ALSACE et le surplus des conclusions de son pourvoi devant le Conseil d'Etat sont rejetés.
<br/>
<br/>
     
Article 3  : Les intérêts afférents à l'indemnité de 100 000 francs (15 245 euros) que la CHAMBRE DE COMMERCE ET D'INDUSTRIE DE COLMAR ET DU CENTRE ALSACE a été condamnée à verser à M. A par jugement du tribunal administratif de Strasbourg en date du 26 février 1998 seront capitalisés à compter du 4 août 2005 pour produire eux-mêmes intérêts.
<br/>
<br/>
     
Article 4  : Le surplus des conclusions de M. A devant la cour administrative d'appel de Nancy et devant le Conseil d'Etat est rejeté.
<br/>
<br/>
     
Article 5  : La présente décision sera notifiée à la CHAMBRE DE COMMERCE ET D'INDUSTRIE DE COLMAR ET DU CENTRE ALSACE et à M. A.
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
