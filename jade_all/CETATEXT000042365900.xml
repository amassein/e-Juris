<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042365900</ID>
<ANCIEN_ID>JG_L_2020_09_000000437524</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/36/59/CETATEXT000042365900.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 25/09/2020, 437524, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-09-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437524</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:437524.20200925</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et quatre mémoires, enregistrés les 8 janvier, 16 mars, 23 mai, 17 et 20 août 2020 au secrétariat du contentieux du Conseil d'Etat, l'Union défense active des forains (UDAF) et France Liberté Voyage demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 99-778 du 10 septembre 1999 instituant une commission pour l'indemnisation des victimes de spoliation intervenues du fait des législations antisémites en vigueur pendant l'Occupation ainsi que la décision implicite par laquelle le Premier ministre a refusé d'abroger ce décret, subsidiairement d'annuler ce décret avec un effet différé et, très subsidiairement, d'annuler, à l'article 1er du décret, les mots " du fait des législations antisémites " ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, et notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son protocole additionnel ;<br/>
              - le code des relations entre le public et l'administration ; <br/>
              - la loi organique n° 2011-333 du 29 mars 2011 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              Sur les interventions : <br/>
<br/>
              1.	Mme A... et l'association Ligue Internationale Contre le Racisme et l'Antisémitisme (LICRA) justifient d'un intérêt suffisant à l'annulation des décisions attaquées. Leurs interventions sont, par suite, recevables.<br/>
<br/>
              Sur les conclusions de la requête :<br/>
<br/>
              2.	Aux termes de l'article 1er du décret du 10 septembre 1999 instituant une commission pour l'indemnisation des victimes de spoliation intervenues du fait des législations antisémites en vigueur pendant l'Occupation: " Il est institué auprès du Premier ministre une commission chargée d'examiner les demandes individuelles présentées par les victimes ou par leurs ayants droit pour la réparation des préjudices consécutifs aux spoliations de biens intervenues du fait des législations antisémites prises, pendant l'Occupation, tant par l'occupant que par les autorités de Vichy./ La commission est chargée de rechercher et de proposer les mesures de réparation, de restitution ou d'indemnisation appropriées ".<br/>
<br/>
              3.	Les organisations requérantes demandent l'annulation de ce décret et de la décision implicite par laquelle le Premier ministre a refusé de l'abroger, en tant qu'il exclut du champ d'application du dispositif qu'il prévoit les victimes de spoliations intervenues pendant l'Occupation autres que celles résultant des législations antisémites, notamment les tsiganes, forains et membres de la communauté des gens du voyage.<br/>
<br/>
              4.	Ainsi que le prévoit le premier alinéa de l'article L. 243-2 du code des relations entre le public et l'administration : " L'administration est tenue d'abroger expressément un acte réglementaire illégal ou dépourvu d'objet, que cette situation existe depuis son édiction ou qu'elle résulte de circonstances de droit ou de fait postérieures, sauf à ce que l'illégalité ait cessé ".<br/>
<br/>
              5.	En premier lieu, la décision par laquelle le Premier ministre a refusé d'abroger le décret du 10 septembre 1999 a un caractère réglementaire. Ni les dispositions des articles L. 211-2 et L. 211-3 du code des relations entre le public et l'administration, qui s'appliquent aux décisions individuelles, ni aucune autre disposition ni aucun principe n'imposaient sa motivation. Les organisations requérantes ne sauraient, par suite, soutenir que cette décision serait illégale faute d'être motivée.<br/>
<br/>
              6.	En deuxième lieu, si le décret attaqué instaure un mécanisme d'examen des demandes de réparation pour les victimes de spoliations du fait des lois antisémites ou leurs ayants droit, il n'a pas pour objet et ne saurait avoir légalement pour effet de modifier les conditions dans lesquelles les personnes qui s'y croient fondées peuvent engager des actions en responsabilité contre l'Etat. Dans ces conditions, ces dispositions, qui n'entraînent pas une privation de propriété au sens de l'article 17 de la Déclaration des droits de l'homme et du citoyen, ne méconnaissent ni les exigences de l'article 2 de cette Déclaration ni celles qui découlent de l'article 1er du protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.<br/>
<br/>
              7.	En troisième lieu, si, comme le font valoir les associations requérantes, pendant l'Occupation de la France, plusieurs catégories de personnes ont été spoliées en application de différentes législations prises tant par l'occupant que par les autorités de Vichy ou à la suite de leur internement dans des camps en France, les personnes victimes de ces mesures dans le cadre de persécutions antisémites ont fait l'objet d'une politique d'extermination systématique. Ainsi, en créant la commission instituée par le décret contesté, le Gouvernement a pu, sans méconnaître le principe d'égalité, en limiter la compétence à l'examen de la situation particulière des personnes persécutées dans ces conditions. Les organisations requérantes ne sauraient, en outre, utilement soutenir que le principe de fraternité exigerait qu'il soit mis fin à la différence de traitement résultant de ce décret et que celui-ci méconnaîtrait l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. <br/>
<br/>
              8.	Il résulte de ce qui précède que les associations requérantes ne sont pas fondées à demander l'annulation pour excès de pouvoir de la décision par laquelle le Premier ministre a refusé d'abroger le décret du 10 septembre 1999 ou de supprimer, à l'article 1er du décret, les mots " du fait des législations antisémites ". Par suite, les conclusions par lesquelles elles demandent l'abrogation de ce décret, qui doivent être regardées comme tendant à ce qu'il soit enjoint au Premier ministre de procéder à une telle abrogation, ne peuvent qu'être rejetées. Il en va de même, sans qu'il soit besoin de se prononcer sur la recevabilité de ces conclusions, de celles tendant à l'annulation du décret.<br/>
<br/>
              9.	Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante, au titre des frais exposés par les associations requérantes à l'occasion de cette instance.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Les interventions de Mme A... et de l'association Ligue Internationale Contre le Racisme et l'Antisémitisme (LICRA) sont admises.<br/>
<br/>
Article 2 : La requête de l'Union défense active des forains (UDAF) et de France Liberté Voyage est rejetée.<br/>
<br/>
Article 3 : La présente décision sera notifiée à l'Union défense active des forains (UDAF) et à France Liberté Voyage, au Premier ministre et à la Défenseure des droits, à Mme B... A... et à l'association Ligue Internationale Contre le Racisme et l'Antisémitisme (LICRA). <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
