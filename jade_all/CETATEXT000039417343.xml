<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039417343</ID>
<ANCIEN_ID>JG_L_2019_11_000000419758</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/41/73/CETATEXT000039417343.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 22/11/2019, 419758, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-11-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419758</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:419758.20191122</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La commune de Monticello a demandé au tribunal administratif de Bastia d'annuler la délibération du conseil communautaire de la communauté de communes du bassin de vie de L'Ile-Rousse, devenue la communauté de communes de l'Ile-Rousse - Balagne du 19 août 2014 relative à la mise en oeuvre de la réforme des rythmes scolaires pour l'année scolaire 2014-2015. Par un jugement n° 1400910 du 17 mars 2016, le tribunal administratif de Bastia a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 16MA01945 du 19 février 2018, la cour administrative d'appel de Marseille a rejeté l'appel formé par la commune de Monticello contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 11 avril, 11 juillet 2018 et 7 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, la commune de Monticello demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la communauté de communes de l'Ile-Rousse - Balagne la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat de la commune de Monticello, et à la SCP Spinosi, Sureau, avocat de la communauté de communes de L'Ile-Rousse - Balagne ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par un arrêt du 19 février 2018, la cour administrative d'appel de Marseille a rejeté l'appel formé par la commune de Monticello contre le jugement du tribunal administratif de Bastia qui avait rejeté sa demande tendant à l'annulation de la délibération du 19 août 2014 du conseil communautaire de la communauté de communes du bassin de vie de L'Ile-Rousse se rapportant à la mise en oeuvre, pour l'année scolaire 2014-2015, du décret du 24 janvier 2013 relatif à l'organisation du temps scolaire dans les écoles maternelles et élémentaires. La commune de Monticello se pourvoit en cassation contre cet arrêt.<br/>
<br/>
              2. Pour rejeter la requête dont elle était saisie, la cour administrative d'appel a retenu que la délibération attaquée avait pour objet, d'une part, d'autoriser la mise à la disposition de la commune de Monticello de trois agents intercommunaux chargés de l'accueil de loisirs sans hébergement et, d'autre part, d'autoriser le président de la communauté de communes à signer des conventions avec les communes de Corbara, Santa Reparata di Balagna et de L'Ile-Rousse, qui avaient décidé de confier à la communauté de communes l'organisation des nouvelles activités périscolaires, et des conventions de prestations de service avec les intervenants. La cour a jugé que la commune n'était pas recevable à demander l'annulation pour excès de pouvoir de cette délibération au motif, d'une part, que la légalité de la délibération autorisant la conclusion d'un contrat et de la décision de le signer ne pouvait être contestée qu'à l'occasion d'un recours en contestation de la validité de ce contrat et, d'autre part, que la délibération attaquée ne faisait pas grief à la commune requérante en tant qu'elle autorisait la mise à disposition à son bénéfice de trois agents intercommunaux.<br/>
<br/>
              3. Il ressort toutefois des pièces du dossier soumis aux juges du fond que, par la délibération contestée, le conseil communautaire de la communauté de communes a décidé d'assurer, pour le compte de trois de ses communes membres, le service public des activité périscolaires afin de mettre en oeuvre la réforme des rythmes scolaires pour l'année 2014-2015, d'autoriser son président à signer les conventions nécessaires à cette fin, de prévoir l'inscription budgétaire correspondante et, par ailleurs, s'agissant de la commune de Monticello, de mettre trois agents intercommunaux à la disposition de cette commune qui entendait assurer elle-même ce service public. La commune de Monticello est, dès lors, fondée à soutenir que la cour administrative d'appel de Marseille s'est fondée sur une inexacte interprétation de la délibération attaquée, en retenant qu'elle avait pour seul objet, outre la mise à disposition des trois agents intercommunaux, l'autorisation de signer les conventions avec les trois autres communes et les intervenants. Par ailleurs, en retenant que la commune de Monticello demandait l'annulation de la délibération attaquée en tant qu'elle mettait à sa disposition trois agents intercommunaux, alors que la commune contestait le principe et les modalités de la prise en charge par la communauté de communes, dont elle est membre, du service public des activités périscolaires pour le compte des trois autres communes membres, la cour a méconnu la portée de ses écritures.<br/>
<br/>
              4. Il résulte de ce qui précède que la commune de Monticello est fondée, sans qu'il soit besoin de se prononcer sur les autres moyens de son pourvoi, à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de la commune de Monticello, qui n'est pas la partie perdante dans la présente instance, le versement des sommes que demande, à ce titre, la communauté de communes de L'Ile-Rousse - Balagne. En revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la communauté de communes de L'Ile-Rousse - Balagne le versement d'une somme de 3 000 euros à la commune de Monticello au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 19 février 2018 de la cour administrative d'appel de Marseille est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : La communauté de communes de l'Ile-Rousse - Balagne versera la somme de 3 000 euros à la commune de Monticello en application des dispositions de l'article L. 761-1 du code de justice administrative. Les conclusions présentées au même titre par la communauté de communes de l'Ile-Rousse - Balagne sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la commune de Monticello et à la communauté de communes de l'Ile-Rousse - Balagne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
