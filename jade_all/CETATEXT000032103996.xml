<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032103996</ID>
<ANCIEN_ID>JG_L_2016_02_000000393748</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/10/39/CETATEXT000032103996.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème SSJS, 24/02/2016, 393748, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-02-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393748</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Arno Klarsfeld</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:393748.20160224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes : <br/>
<br/>
              1° M. A...B...a demandé au tribunal administratif de Nantes d'annuler la décision implicite, née le 30 septembre 2012, par laquelle la commission de recours contre les décisions de refus de visa d'entrée en France a rejeté le recours formé devant elle contre la décision de l'autorité consulaire française à Dar-Es-Salam (Tanzanie) du 5 juin 2012 refusant de lui délivrer un visa de long séjour pour établissement familial en tant que conjoint de réfugié statutaire. Par un jugement n° 1211343 du 17 décembre 2014, le tribunal administratif de Nantes a fait droit à cette demande, et enjoint au ministre de l'intérieur de faire délivrer à l'intéressé le visa de long séjour sollicité.<br/>
<br/>
              Par un arrêt n° 15NT00126 du 24 juillet 2015, la cour administrative d'appel de Nantes a rejeté l'appel formé par le ministre de l'intérieur contre ce jugement.<br/>
<br/>
              Sous le n° 393748, par un pourvoi, enregistré le 24 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat d'annuler cet arrêt. <br/>
<br/>
<br/>
<br/>
              2° Sous le n° 393749, par une requête, enregistrée le 24 septembre 2015, le ministre de l'intérieur demande au Conseil d'Etat d'ordonner qu'il soit sursis à l'exécution de l'arrêt dont il demande l'annulation par le pourvoi enregistré sous le numéro 393748.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arno Klarsfeld, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. A...B...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant que le pourvoi, enregistré sous le n° 393748, et le recours à fin de sursis à exécution, enregistré sous le n° 393749, sont dirigés contre un même arrêt ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B... a demandé à l'autorité consulaire française à Dar-Es-Salam (Tanzanie) de lui délivrer un visa de long séjour pour établissement familial en tant que conjoint de réfugié statutaire ; qu'il a formé un recours pour excès de pouvoir à l'encontre de la décision implicite par laquelle la commission de recours contre les décisions de refus de visa d'entrée en France a rejeté le recours formé devant elle contre la décision de refus de l'autorité consulaire ; que par un arrêt du 24 juillet 2015, la cour administrative d'appel de Nantes a rejeté l'appel formé par le ministre de l'intérieur contre le jugement du 17 décembre 2014, par lequel le tribunal administratif de Nantes a fait droit à la demande de M.B..., et enjoint au ministre de l'intérieur de faire délivrer à l'intéressé le visa de long séjour sollicité ;<br/>
<br/>
              4. Considérant qu'il appartient en principe aux autorités consulaires de délivrer au conjoint et aux enfants mineurs d'un réfugié statutaire les visas qu'ils sollicitent afin de mener une vie familiale normale ; qu'elles ne peuvent opposer un refus à une telle demande que pour un motif d'ordre public ; qu'elles peuvent, sur un tel fondement, opposer un refus aux demandeurs ayant été impliqués dans des crimes graves contre les personnes et dont la venue en France, eu égard aux principes qu'elle mettrait en cause ou au retentissement de leur présence sur le territoire national, serait de nature à porter atteinte à l'ordre public ;<br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier que M. B...a appartenu au cercle restreint des conseillers et membres de la famille du président Habyarimana dénommé " Akazu " ; qu'en outre, le ministre a apporté des éléments circonstanciés, qui sont de notoriété publique, tendant à prouver le rôle de ce premier cercle du pouvoir rwandais dans la préparation et la planification du génocide ; qu'il a également démontré la place influente occupée par M. B...au sein de " l'Akazu ", ainsi que sa présence au Rwanda pendant la durée des massacres perpétrés d'avril à juillet 1994 ; <br/>
<br/>
              6. Considérant, dès lors, qu'en jugeant, en dépit de ces différents éléments, que les motifs d'ordre public invoqués par le ministre pour justifier le refus opposé à la demande de visa de M. B...n'étaient pas établis, alors même que sa présence sur le territoire français, serait de nature à troubler l'ordre public en raison des faits qui lui ont été imputées, la cour administrative d'appel de Nantes a inexactement qualifié les faits qui lui étaient soumis ; que le ministre de l'intérieur est fondé, par suite, à demander l'annulation de l'arrêt attaqué, sans qu'il soit besoin d'examiner les autres moyens du pourvoi ; <br/>
<br/>
              Sur le recours à fin de sursis à exécution :<br/>
<br/>
              7. Considérant qu'aux termes du premier alinéa de l'article R. 821-5 du code de justice administrative : " La formation de jugement peut, à la demande de l'auteur du pourvoi, ordonner qu'il soit sursis à l'exécution d'une décision juridictionnelle rendue en dernier ressort si cette décision risque d'entraîner des conséquences difficilement réparables et si les moyens invoqués paraissent, en l'état de l'instruction, sérieux et de nature à justifier, outre l'annulation de la décision juridictionnelle rendue en dernier ressort, l'infirmation de la solution retenue par les juges du fond. " ;<br/>
<br/>
              8. Considérant que, par la présente décision, le Conseil d'Etat se prononce sur le pourvoi formé par le ministre de l'intérieur contre l'arrêt du 24 juillet 2015 de la cour administrative d'appel de Nantes ; que, par suite, les conclusions à fin de sursis à l'exécution de cet arrêt sont devenues sans objet ; que, dans les circonstances de l'espèce, il n'y a pas lieu de faire droit aux conclusions de M. B...présentées au titre de l'article L. 761-1 du code de justice administrative sous le n° 393749, les mêmes dispositions faisant obstacle à ce que la somme demandée soit versée à M. B...sous le n° 393748 ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1 : L'arrêt de la cour administrative d'appel de Nantes du 24 juillet 2015 est annulé.<br/>
<br/>
      Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
<br/>
Article 3 : Il n'y a pas lieu de statuer sur le recours n° 393749 du ministre de l'intérieur.<br/>
<br/>
Article 4 : Les conclusions de M. B...présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée au ministre de l'intérieur et à M. A...B.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
