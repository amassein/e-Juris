<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000018802775</ID>
<ANCIEN_ID>JG_L_2008_05_000000289745</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/18/80/27/CETATEXT000018802775.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 14/05/2008, 289745, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2008-05-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>289745</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin</PRESIDENT>
<AVOCATS>SCP BORE ET SALVE DE BRUNETON ; SCP GATINEAU</AVOCATS>
<RAPPORTEUR>Mme Laure  Bédier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Derepas Luc</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 1er février et 1er juin 2006 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. et Mme Alain B, demeurant ... ; M. et Mme B demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du 1er décembre 2005 par lequel le tribunal administratif de Lille a rejeté leur demande tendant, d'une part, à l'annulation de l'arrêté du 3 décembre 2002 du maire d'Audresselles accordant à Mme Mona A l'autorisation d'édifier une clôture et, d'autre part, à la condamnation de Mme A à remettre les lieux en l'état sous astreinte de 200 euros par jour de retard à compter du jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, d'annuler l'arrêté du 3 décembre 2002 du maire d'Audresselles ;<br/>
<br/>
              3°) de mettre solidairement à la charge de Mme A et de la commune d'Audresselles le versement de la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu la loi n° 2000-321 du 12 avril 2000 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laure Bédier, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Gatineau, avocat de M. et Mme B et de la SCP Boré et Salve de Bruneton, avocat de Mme A, <br/>
<br/>
              - les conclusions de M. Luc Derepas, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme A a déposé le 4 novembre 2002 une déclaration de travaux afin d'édifier une clôture sur sa propriété sise ... ; que, par un arrêté du 3 décembre 2002, le maire de la commune d'Audresselles a décidé de ne pas s'opposer à ces travaux ; que M. et Mme B, propriétaires d'une maison dans la même rue, se pourvoient en cassation contre le jugement du 1er décembre 2005 par lequel le tribunal administratif de Lille a rejeté leur demande tendant à l'annulation pour excès de pouvoir de cet arrêté ;<br/>
<br/>
              Considérant qu'aux termes de l'article R. 741-7 du code de justice administrative : « Dans les tribunaux administratifs et les cours administratives d'appel, la minute de la décision est signée par le président de la formation de jugement, le rapporteur et le greffier d'audience » ; qu'il ne ressort pas de la minute du jugement rendu le 1er décembre 2005, transmise par le tribunal administratif de Lille, qu'elle ait été signée par le président de la formation de jugement, le rapporteur et le greffier d'audience ; qu'ainsi, le jugement du tribunal administratif de Lille est entaché d'une irrégularité qui, eu égard à l'objet des dispositions de l'article R. 741-7, présente un caractère substantiel ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens de la requête, ce jugement doit être annulé ;<br/>
<br/>
              Considérant que, dans les circonstances de l'espèce, il y a lieu pour le Conseil d'Etat de faire application des dispositions de l'article L. 821-2 du code de justice administrative et de régler l'affaire au fond ;<br/>
<br/>
              Considérant, en premier lieu, que si les requérants soutiennent que les dispositions de l'article R. 422-10 du code de l'urbanisme relatives à l'affichage des déclarations de travaux n'ont pas été respectées, une telle irrégularité, qui aurait pour effet d'empêcher le délai de recours contentieux de courir à l'égard des tiers, est sans incidence sur la légalité de l'arrêté du 3 décembre 2002 ;<br/>
<br/>
              Considérant, en deuxième lieu, qu'il résulte des dispositions combinées des articles L. 441-2, L. 441-3 et L. 422-2 du code de l'urbanisme, dans leur rédaction applicable à la date de l'arrêté attaqué, que l'édification d'une clôture, qui est subordonnée à une simple déclaration préalable, peut être exécutée en l'absence d'opposition dans le délai d'un mois suivant le dépôt de la déclaration, sous réserve, le cas échéant, du respect des prescriptions notifiées dans les mêmes conditions ; que les travaux de cette nature ne donnent donc en principe pas lieu à une décision explicite de l'autorité compétente, sauf en cas d'opposition ou de prescriptions particulières ; que toutefois, à la suite de la déclaration déposée le 4 novembre 2002 par Mme A, le maire de la commune d'Audresselles a pris, le 3 décembre 2002, une décision explicite de non-opposition à travaux, sans imposer le respect de prescriptions particulières, alors même qu'il n'était pas tenu de le faire ; qu'il suit de là que l'irrégularité formelle de l'arrêté du 3 décembre 2002 résultant de ce que n'y figure pas la mention du nom et du prénom de son auteur, comme l'imposent les dispositions de l'article 4 de la loi du 12 avril 2000, ne présente pas, en l'espèce, un caractère substantiel ; que, par suite, le moyen tiré de cette irrégularité doit être écarté ;<br/>
<br/>
              Considérant, en troisième lieu, qu'il résulte des dispositions des articles L. 441-1 et L. 441-2 du code de l'urbanisme que, dans les communes dotées d'un plan d'occupation des sols ou d'un plan local d'urbanisme, l'édification des clôtures est seulement soumise à déclaration préalable ; qu'aux termes de l'article L. 441-3 du même code, dans sa rédaction applicable à la date de l'arrêté attaqué : « L'autorité compétente en matière de permis de construire peut faire opposition à l'édification d'une clôture lorsque celle-ci fait obstacle à la libre circulation des piétons admise par les usages locaux./ L'édification d'une clôture peut faire l'objet, de la part de l'autorité compétente, de prescriptions spéciales concernant la nature, la hauteur ou l'aspect extérieur de la clôture pour des motifs d'urbanisme et d'environnement » ; qu'il résulte de ces dispositions que l'autorisation d'édifier une clôture ne peut être légalement refusée pour des motifs tirés des règles d'urbanisme applicables aux constructions soumises à permis de construire ; que, dès lors, M. et Mme B ne peuvent utilement se prévaloir, à l'encontre de l'arrêté du maire du 3 décembre 2002, de la méconnaissance des dispositions des articles UC1 et UC2 du règlement du plan d'occupation des sols de la commune d'Audresselles qui interdisent dans certains secteurs toute nouvelle construction ; qu'ils ne peuvent pas davantage tirer argument de l'article UC10 de ce même règlement, qui détermine la hauteur des constructions autorisées, ou des dispositions relatives au coefficient d'occupation des sols, qui ne s'appliquent pas aux clôtures ;<br/>
<br/>
              Considérant, en quatrième lieu, qu'il ressort des pièces du dossier que la clôture prévue par Mme A n'est pas située sur rue ; qu'en tout état de cause, la partie minérale de cette clôture, qui consiste en un soubassement en pierre, mesure 0,60 mètre, tandis que sa hauteur totale, en y ajoutant les pilastres, est de 1,20 mètre, de sorte que ne sont pas méconnues les dispositions de l'article UC11 du plan d'occupation des sols qui fixent la hauteur maximale de la partie minérale des clôtures nouvelles sur rue à 0,80 mètre et leur hauteur totale maximale à 2 mètres ;<br/>
<br/>
              Considérant, en cinquième lieu, qu'eu égard aux conditions dans lesquelles il était envisagé d'édifier la clôture, le maire d'Audresselles n'a pas commis d'erreur manifeste d'appréciation en délivrant l'autorisation sollicitée sans l'assortir de prescriptions particulières ;<br/>
<br/>
              Considérant, en sixième lieu, que le plan d'occupation des sols de la commune d'Audresselles a été approuvé par arrêté du 8 juillet 1983 ; que le cahier des charges du lotissement « Luling », dit « Les rochers d'Audresselles », dans lequel est située la propriété de Mme A, a été approuvé par arrêté préfectoral du 10 juin 1962 ; qu'il ne ressort pas des pièces du dossier qu'une majorité des co-lotis du lotissement « Les rochers d'Audresselles » ait demandé le maintien de l'application des règles d'urbanisme contenues dans le cahier des charges approuvé ; qu'ainsi, conformément à l'article L. 315-2-1 du code de l'urbanisme, ces règles ont définitivement cessé d'être applicables à la date d'entrée en vigueur de cet article, soit le 8 juillet 1988 ; que, par suite, les moyens tirés de la méconnaissance des articles 13 et 15 de ce cahier des charges sont inopérants ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner la fin de non-recevoir opposée par la commune d'Audresselles, la requête de M. et Mme B tendant à l'annulation de l'arrêté en date du 3 décembre 2002 du maire de la commune d'Audresselles doit être rejetée, ainsi que, par voie de conséquence, les conclusions qu'ils présentent au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. et Mme B le versement à Mme A d'une somme de 3 000 euros au même titre ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Lille en date du 1er décembre 2005 est annulé.<br/>
Article 2 : La demande présentée par M. et Mme B devant le tribunal administratif de Lille, ainsi que le surplus de leurs conclusions devant le Conseil d'Etat, sont rejetés.<br/>
Article 3 : M. et Mme B verseront à Mme A la somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. et Mme Alain B, à Mme Mona A et à la commune d'Audresselles.<br/>
<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
