<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030853984</ID>
<ANCIEN_ID>JG_L_2015_07_000000388991</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/85/39/CETATEXT000030853984.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème SSJS, 03/07/2015, 388991, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388991</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:388991.20150703</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. F... A...a demandé au tribunal administratif de Lille d'annuler les opérations électorales qui se sont déroulées les 7 et 14 décembre 2014 dans la commune de Bouchain (Nord) en vue de la désignation des conseillers municipaux et communautaires. Par un jugement n° 1409189 du 24 février 2015, le tribunal administratif de Lille a rejeté sa protestation.<br/>
<br/>
              Par une requête et un mémoire en réplique enregistrés les 25 mars et 8 juin 2015 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) d'annuler ces opérations électorales ;<br/>
<br/>
              3°) de mettre à la charge de M. C...le versement de la somme de 5 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - le code électoral ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 23 juin 2015, présentée par M.A....<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. A l'issue du second tour de scrutin des élections qui se sont déroulées les 7 et 14 décembre 2014 en vue de la désignation des conseillers municipaux et communautaires de la commune de Bouchain (Nord), à la suite de l'annulation par le Conseil d'Etat statuant au contentieux des opérations électorales qui avaient eu lieu les 23 et 30 mars 2014, la liste conduite par M. C...a obtenu 878 voix et 20 sièges au conseil municipal, celle conduite par M. A...862 voix et six sièges, et celle conduite par Mme E...149 voix et un siège. Au premier tour de scrutin, la liste conduite par M. A...avait obtenu 719 voix et celle conduite par M. C...671 voix, sur 1 819 suffrages exprimés.<br/>
<br/>
              Sur les conclusions à fin d'annulation des opérations électorales : <br/>
<br/>
              En ce qui concerne le moyen relatif aux listes électorales : <br/>
<br/>
              2. M. A...soutient que des électeurs ont été radiés à tort des listes électorales entre mars et décembre 2014. Toutefois, il n'appartient pas au juge de l'élection, en l'absence de manoeuvre, qui n'est, en l'espèce, pas établie d'apprécier la régularité des inscriptions ou des radiations sur les listes électorales.<br/>
<br/>
              En ce qui concerne les moyens concernant la campagne électorale et les opérations électorales du premier tour de scrutin :<br/>
<br/>
              3. M. A...soutient, d'une part, qu'un tract a été diffusé avant l'ouverture de la campagne électorale du premier tour et qu'un autre tract, diffusé à la fin de la campagne du même tour, aurait eu un caractère diffamatoire et, d'autre part, que des irrégularités ont été commises lors des opérations électorales du 7 décembre 2014, telles que la perte inexpliquée de procurations, le fait que certains électeurs aient pu voter sur la seule présentation d'une photocopie d'une pièce d'identité, la circonstance que des bureaux de vote aient été présidés par des candidats lors de la pause méridienne. Toutefois, ces irrégularités, à les supposer établies, n'ont pas été de nature à influer sur le résultat de l'élection au second tour de scrutin, compte tenu de l'écart séparant, au premier tour, le résultat des listes en présence de celui qu'elles auraient dû atteindre pour obtenir la majorité des suffrages exprimés à ce tour ou pour se maintenir au second tour.<br/>
<br/>
              En ce qui concerne les moyens concernant la campagne électorale et les opérations électorales du second tour de scrutin : <br/>
<br/>
              4. M. A...soutient, en premier lieu, que des membres de la liste conduite par M. C... se seraient livrés à des manoeuvres en promettant des embauches à des électeurs s'ils votaient pour cette liste. Toutefois, cette allégation n'est assortie d'aucune précision permettant d'en apprécier le bien-fondé. Ce moyen ne peut donc qu'être écarté.<br/>
<br/>
              5. Si le requérant soutient, en deuxième lieu, sans l'établir, que des partisans de M. C... ont mené des actions de propagande aux abords d'un établissement scolaire, cette circonstance est, par elle-même, sans incidence sur le résultat du scrutin. Ce moyen doit donc être écarté.<br/>
<br/>
              6. M. A...soutient, en troisième lieu, qu'un tract distribué le vendredi 12 décembre 2014, dans l'après-midi et en soirée, à un moment où il ne pouvait répliquer, comportait des accusations à caractère diffamatoire. Toutefois, ce tract, qui reprenait des critiques contenues dans un tract diffusé avant le premier tour de scrutin, s'il faisait référence à des procédures judiciaires, ne contenait aucune imputation à caractère diffamatoire et ne portait pas atteinte à la présomption d'innocence. Ce moyen doit donc être écarté.<br/>
<br/>
              7. M. A...soutient, en quatrième lieu, qu'un tract également diffusé l'après-midi du vendredi 12 décembre 2014 a introduit des éléments nouveaux de polémique électorale. Cependant, ce tract se bornait à évoquer, sans polémique, plusieurs aspects du fonctionnement des écoles, à un moment où il était possible à M. A...de répondre. Ce moyen doit donc être écarté.<br/>
<br/>
              8. Si le requérant soutient, en cinquième lieu, que plusieurs membres de sa famille ont fait l'objet d'injures et de propos malveillants diffusés sur un réseau social, notamment par M.C..., il ne résulte pas de l'instruction que tel ait été le cas. Ce moyen doit donc être écarté.<br/>
<br/>
              9. En sixième lieu, il ne résulte pas de l'instruction que M. C...aurait bénéficié d'un traitement excessivement favorable par la presse, à l'occasion de la publication de deux articles. Aucune disposition législative ou règlementaire n'interdisait à M. C...de reproduire ces deux articles, dont la teneur n'excédait pas les limites habituelles de la polémique électorale, sur son site Internet. Par suite, le moyen tiré de ce qu'aurait été portée au principe de l'égalité des moyens d'expression des candidats une atteinte de nature à altérer la sincérité du scrutin doit être écarté.<br/>
<br/>
              10. En septième lieu, contrairement à ce que soutient le requérant, le tract diffusé par M.D..., candidat ayant conduit une liste éliminée au premier tour, dans lequel il faisait état de son soutien personnel à la liste conduite par M.C..., ne démentait pas le rapprochement intervenu entre certains de ses anciens colistiers et la liste conduite par M. A.... Le moyen tiré de ce que ce tract révèlerait une manoeuvre de nature à porter atteinte à la sincérité du scrutin doit donc être écarté.<br/>
<br/>
              11. En huitième lieu, l'erreur matérielle ayant entaché la feuille de dépouillement de la quatrième table du troisième bureau de vote de la commune de Bouchain, mentionnée sur le procès-verbal des élections, qui fait état de 1 924 votants et non de 1 925, est sans incidence sur le résultat du scrutin. Ce moyen doit donc être écarté.<br/>
<br/>
              12. En dernier lieu, les allégations de M. A...selon lesquelles des attroupements de partisans de M. C...et des interpellations d'électeurs devant les bureaux de vote auraient constitué des pressions sur les électeurs ne sont pas assorties des précisions permettant d'en apprécier le bien-fondé. <br/>
<br/>
              13. Il résulte de tout ce qui précède que M. A...n'est pas fondé à se plaindre de ce que, par le jugement qu'il attaque, le tribunal administratif de Lille a rejeté sa protestation.<br/>
<br/>
              Sur les conclusions de M. C...tendant à l'application de l'article L. 741-2 du code de justice administrative :<br/>
<br/>
              14. Aux termes de l'article 41 de la loi du 29 juillet 1881 rendu applicable par l'article L. 741-2 du code de justice administrative : " Ne donneront lieu à aucune action en diffamation, injure ou outrage, ni le compte rendu fidèle fait de bonne foi des débats judiciaires, ni les discours prononcés ou les écrits produits devant les tribunaux./ Pourront néanmoins les juges, saisis de la cause et statuant sur le fond, prononcer la suppression des discours injurieux, outrageants ou diffamatoires, et condamner qui il appartiendra à des dommages-intérêts (...) ". La requête d'appel de M. A...comporte des griefs, qui ne sont d'ailleurs assortis d'aucune précision, tirés de ce que M. C...aurait proposé des embauches à des électeurs en échange de leur vote. Ces allégations, pour regrettables qu'elles soient, ne peuvent être regardées comme présentant un caractère injurieux, outrageant ou diffamatoire au sens des dispositions précitées. Par suite, les conclusions de M. C...présentées sur leur fondement ne peuvent qu'être rejetées. <br/>
<br/>
              Sur les conclusions de M. A...tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              15. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. C...qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : Les conclusions de M. C...présentées sur le fondement de l'article L. 741-2 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. G... A..., à M. B...C...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
