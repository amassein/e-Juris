<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027064764</ID>
<ANCIEN_ID>JG_L_2013_02_000000359636</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/06/47/CETATEXT000027064764.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 13/02/2013, 359636, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-02-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359636</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. David Gaudillère</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:359636.20130213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 23 mai 2012 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre des affaires étrangères ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10PA04362 du 20 mars 2012 par lequel la cour administrative d'appel de Paris a, à la demande de Mme B...A..., d'une part, annulé le jugement n° 0804774/5-2 en date du 28 juin 2010 par lequel le tribunal administratif de Paris a rejeté sa demande tendant à l'annulation pour excès de pouvoir de la décision du 3 mars 2008 par laquelle le ministre des affaires étrangères et européennes l'a informée que son contrat à durée déterminée ne serait pas renouvelé à son échéance, le 31 mars 2008 et, d'autre part, annulé pour excès de pouvoir cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de Mme A...;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu le décret n° 2002-1355 du 14 novembre 2002 ;<br/>
<br/>
              Vu le décret n° 2008-273 du 20 mars 2008 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. David Gaudillère, Auditeur,<br/>
<br/>
              - les observations de la SCP Masse-Dessen, Thouvenin, Coudray, avocat de MmeA...,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant à nouveau été donnée à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des énonciations de l'arrêt attaqué que Mme A... a été recrutée le 23 janvier 2002 par le ministre des affaires étrangères, par un contrat à durée déterminée d'une durée de deux ans, pour exercer les fonctions de secrétaire au sein du haut conseil à la coopération internationale ; que son contrat a été expressément renouvelé à deux reprises pour la même durée ; que, par un avenant signé le 21 février 2008, son contrat a été renouvelé jusqu'au 31 mars 2008 ; que, par une décision du 3 mars 2008, le ministre des affaires étrangères a informé l'intéressée que son contrat ne serait pas renouvelé à son échéance du 31 mars 2008, au motif que le poste qu'elle occupait était supprimé à la suite de la suppression du haut conseil à la coopération internationale par décret du 20 mars 2008 ; que, par un jugement du 28 juin 2010, le tribunal administratif a rejeté la demande de Mme A...tendant à l'annulation pour excès de pouvoir de cette décision ; que, par un arrêt du 20 mars 2012, contre lequel le ministre des affaires étrangères se pourvoit en cassation, la cour administrative d'appel de Paris a, à la demande de MmeA..., d'une part, annulé le jugement du tribunal administratif de Paris et, d'autre part, annulé pour excès de pouvoir la décision du 3 mars 2008 ;<br/>
<br/>
              Sur la fin de non-recevoir invoquée par Mme A...:<br/>
<br/>
              2.	Considérant que le pourvoi du ministre des affaires étrangères contre l'arrêt de la cour administrative d'appel de Paris, qui lui a été notifié le 23 mars 2012, a été enregistré le 23 mai 2012 au secrétariat de la section du contentieux du Conseil d'Etat, soit dans le délai de recours contentieux, et n'est ainsi pas tardif ;<br/>
<br/>
              Sur le pourvoi du ministre des affaires étrangères :<br/>
<br/>
              3.	Considérant qu'aux termes de l'article 3 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires : " Sauf dérogation prévue par une disposition législative, les emplois civils permanents de l'Etat (...) sont, à l'exception de ceux réservés aux magistrats de l'ordre judiciaire et aux fonctionnaires des assemblées parlementaires, occupés soit par des fonctionnaires régis par le présent titre, soit par des fonctionnaires des assemblées parlementaires, des magistrats de l'ordre judiciaire ou des militaires dans les conditions prévues par leur statut " ; qu'aux termes de l'article 4 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, dans sa rédaction applicable à la date du litige : " Par dérogation au principe énoncé à l'article 3 du titre Ier du statut général, des agents contractuels peuvent être recrutés dans les cas suivants: / 1° Lorsqu'il n'existe pas de corps de fonctionnaires susceptibles d'assurer les fonctions correspondantes ; / 2° Pour les emplois du niveau de la catégorie A et, dans les représentations de l'Etat à l'étranger, des autres catégories, lorsque la nature des fonctions ou les besoins des services le justifient./ Les agents ainsi recrutés sont engagés par des contrats à durée déterminée, d'une durée maximale de trois ans. Ces contrats sont renouvelables par reconduction expresse. La durée des contrats successifs ne peut excéder six ans. / Si, à l'issue de la période maximale de six ans mentionnée à l'alinéa précédent, ces contrats sont reconduits, ils ne peuvent l'être que par décision expresse et pour une durée indéterminée " ;<br/>
<br/>
              4.	Considérant qu'il résulte des dispositions du 1° de l'article 4 de la loi du 11 janvier 1984 citées ci-dessus que le recrutement d'agents contractuels par l'Etat n'est autorisé que s'il n'existe pas de corps de fonctionnaires susceptibles d'assurer les fonctions correspondantes ; que la cour, pour estimer que les fonctions pour lesquelles Mme A...avait été recrutée constituaient un emploi pour lequel il n'existe pas de corps de fonctionnaires susceptibles d'assurer les fonctions correspondant à celles exercées par l'intéressée, s'est seulement fondée sur les termes des contrats signés par l'intéressée ; qu'en ne recherchant pas s'il existait, notamment au sein du ministères des affaires étrangères, des corps de fonctionnaires susceptibles d'assurer des fonctions de secrétariat auprès d'une commission administrative telle que le haut conseil à la coopération internationale, créé par le décret du 14 novembre 2002, la cour administrative d'appel a commis une erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, le ministre des affaires étrangères est fondé à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              Sur les conclusions de Mme A...présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              5.	Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris en date du 20 mars 2012 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
<br/>
Article 3 : Les conclusions de Mme A...présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée au ministre des affaires étrangères et à Mme B...A....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
