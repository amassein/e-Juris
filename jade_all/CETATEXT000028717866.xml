<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028717866</ID>
<ANCIEN_ID>JG_L_2014_03_000000362752</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/71/78/CETATEXT000028717866.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 12/03/2014, 362752</TITRE>
<DATE_DEC>2014-03-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362752</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Benjamin de Maillard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:362752.20140312</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 13 septembre 2012 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de l'éducation nationale ; le ministre de l'éducation nationale demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11DA01452 du 5 juillet 2012 par lequel la cour administrative d'appel de Douai a annulé, à la demande de M. B...A..., d'une part, le jugement n° 1002787 du 23 juin 2011 du tribunal administratif de Lille rejetant la demande de M. A...tendant à l'annulation de la décision du 1er février 2010 par laquelle le recteur de l'académie de Lille l'a affecté en qualité de professeur de mathématiques remplaçant au collège Georges Brassens de Saint-Venant et à la condamnation de l'Etat à lui verser une indemnité de 15 000 euros en réparation du préjudice qu'il estime avoir subi, d'autre part, la décision du 1er février 2010 par laquelle le recteur de l'académie de Lille l'a affecté en qualité de professeur de mathématiques remplaçant au collège Georges Brassens de Saint-Venant ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. A...;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'éducation ;<br/>
<br/>
              Vu le décret n° 50-581 du 25 mai 1950 ;<br/>
<br/>
              Vu le décret n° 99-823 du 17 septembre 1999 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Benjamin de Maillard, auditeur,  <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 3 du décret du 25 mai 1950 portant règlement d'administration publique pour la fixation des maximums de service hebdomadaire du personnel enseignant des établissements d'enseignement du second degré : " (...) 2° Les professeurs qui n'ont pas leur maximum de service dans l'enseignement de leur spécialité et qui ne peuvent pas le compléter dans un autre établissement d'enseignement public de la même ville peuvent être tenus, si les besoins du service l'exigent, à participer à un enseignement différent. Toutefois, les heures disponibles doivent, autant qu'il est possible, être utilisées de la manière la plus conforme à leurs compétences et à leurs goûts " ; qu'il résulte de ces dispositions que les enseignants du second degré assurent, à titre principal, leurs obligations de service dans l'enseignement de leur spécialité et ne peuvent être amenés à participer à un enseignement différent qu'à titre accessoire, lorsqu'ils ne peuvent assurer leur maximum de service dans leur spécialité ; <br/>
<br/>
              2. Considérant que le décret du 17 septembre 1999 relatif à l'exercice des fonctions de remplacement dans les établissements d'enseignement du second degré prévoit que les personnels enseignants qu'il vise peuvent être chargés, dans le cadre de l'académie et conformément à leur qualification, d'assurer le remplacement de professeurs momentanément absents ou d'occuper un poste provisoirement vacant au sein de la zone de remplacement dans laquelle ils sont affectés, éventuellement de la zone limitrophe ; que si le pouvoir réglementaire a soumis ces personnels à un régime particulier, en permettant notamment qu'ils remplissent, entre deux remplacements, leurs obligations de service par des activités autres que des activités d'enseignement proprement dites, il n'a pas entendu les soustraire à l'obligation statutaire selon laquelle l'activité d'enseignement doit s'effectuer, à titre principal, dans la spécialité de l'enseignant, la participation à un autre enseignement ne pouvant être qu'accessoire ; que, toutefois, les contraintes particulières liées à l'activité de remplacement, notamment le caractère fréquemment discontinu des affectations du fait du caractère provisoire des vacances de poste ou momentané des absences des enseignants titulaires qu'ils sont appelés à remplacer, autorisent le recteur à confier à ces enseignants, même lorsqu'ils n'effectuent aucun enseignement dans leur spécialité faute de poste vacant ou de titulaire absent, un enseignement en dehors de leur spécialité, conformément à leurs qualifications, dès lors que celui-ci demeure accessoire ; <br/>
<br/>
              3. Considérant, par suite, qu'en jugeant que l'arrêté du recteur de l'académie de Lille du 1er février 2010 affectant M.A..., professeur certifié de physique et électricité appliquée dont les obligations de service hebdomadaire s'élevaient à dix-huit heures, à un poste de professeur de technologie pour une durée hebdomadaire de huit heures, étaient contraires aux dispositions du décret du 25 mai 1950 faute que lui ait été confié préalablement ou concomitamment un enseignement, à titre principal, dans sa spécialité, la cour administrative d'appel de Douai a commis une erreur de droit ; que, dès lors, le ministre est fondé à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Douai du 5 juillet 2012 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Douai.<br/>
Article 3 : Les conclusions de M. A...présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'éducation nationale et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">30-02-02-02 ENSEIGNEMENT ET RECHERCHE. QUESTIONS PROPRES AUX DIFFÉRENTES CATÉGORIES D'ENSEIGNEMENT. ENSEIGNEMENT DU SECOND DEGRÉ. PERSONNEL ENSEIGNANT. - ENSEIGNANTS TITULAIRES DE ZONES DE REMPLACEMENT - 1) RÉGIME PARTICULIER LEUR PERMETTANT DE REMPLIR, ENTRE DEUX REMPLACEMENTS, LEURS OBLIGATIONS DE SERVICE PAR DES ACTIVITÉS AUTRES QUE DES ACTIVITÉS D'ENSEIGNEMENT (ART. 5 DU DÉCRET DU 17 SEPTEMBRE 1999) - PORTÉE - SOUSTRACTION À L'OBLIGATION SELON LAQUELLE L'ACTIVITÉ D'ENSEIGNEMENT DOIT S'EFFECTUER, À TITRE PRINCIPAL, DANS LA SPÉCIALITÉ DE L'ENSEIGNANT, LA PARTICIPATION À UN AUTRE ENSEIGNEMENT NE POUVANT ÊTRE QU'ACCESSOIRE - ABSENCE - 2) FACULTÉ DU RECTEUR DE LEUR CONFIER, MÊME LORSQU'ILS N'EFFECTUENT AUCUN ENSEIGNEMENT DANS LEUR SPÉCIALITÉ FAUTE DE POSTE VACANT OU DE TITULAIRE ABSENT, UN ENSEIGNEMENT EN DEHORS DE LEUR SPÉCIALITÉ - EXISTENCE, DÈS LORS QUE CELUI-CI DEMEURE ACCESSOIRE [RJ1].
</SCT>
<ANA ID="9A"> 30-02-02-02 Le décret n° 99-823 du 17 septembre 1999 relatif à l'exercice des fonctions de remplacement dans les établissements d'enseignement du second degré prévoit que les personnels enseignants qu'il vise peuvent être chargés, dans le cadre de l'académie et conformément à leur qualification, d'assurer le remplacement de professeurs momentanément absents ou d'occuper un poste provisoirement vacant au sein de la zone de remplacement dans laquelle ils sont affectés, éventuellement de la zone limitrophe.... ,,1) Si le pouvoir réglementaire a soumis ces personnels à un régime particulier en permettant notamment qu'ils remplissent, entre deux remplacements, leurs obligations de service par des activités autres que des activités d'enseignement proprement dites, il n'a pas entendu les soustraire à l'obligation statutaire selon laquelle l'activité d'enseignement doit s'effectuer, à titre principal, dans la spécialité de l'enseignant, la participation à un autre enseignement ne pouvant être qu'accessoire.,,,2) Toutefois, les contraintes particulières liées à l'activité de remplacement, notamment le caractère fréquemment discontinu des affectations du fait du caractère provisoire des vacances de poste ou momentané des absences des enseignants titulaires qu'ils sont appelés à remplacer, autorisent le recteur à confier à ces enseignants, même lorsqu'ils n'effectuent aucun enseignement dans leur spécialité faute de poste vacant ou de titulaire absent, un enseignement en dehors de leur spécialité, conformément à leurs qualifications, dès lors que celui-ci demeure accessoire.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, 30 novembre 2001, M.,c/ Ministre de l'éducation nationale, n° 224190, p. 616.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
