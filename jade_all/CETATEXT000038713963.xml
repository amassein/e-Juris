<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038713963</ID>
<ANCIEN_ID>JG_L_2019_07_000000429742</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/71/39/CETATEXT000038713963.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 01/07/2019, 429742, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429742</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Charles-Emmanuel Airy</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:429742.20190701</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 12 avril 2019 au secrétariat du contentieux du Conseil d'Etat, l'Association française des entreprises privées (AFEP) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la circulaire du 7 mars 2019 du ministre de l'action et des comptes publics et de la garde des sceaux, ministre de la justice, relative à la réforme de la procédure de poursuite pénale de la fraude fiscale et au renforcement de la coopération entre l'administration fiscale et la justice en matière de lutte contre la fraude fiscale ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
                  Vu les autres pièces du dossier ;<br/>
<br/>
                  Vu : <br/>
                  - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
                  - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
- le code général des impôts et le livre des procédures fiscales, notamment son article L. 228 ; <br/>
                  - la loi n° 2018-898 du 23 octobre 2018 relative à la lutte contre la fraude ; <br/>
                  - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles-Emmanuel Airy, auditeur,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 17 juin 2019 présentée par l'Association française des entreprises privées ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes de l'article L. 228 du livre des procédures fiscales, dans sa rédaction issue de la loi du 23 octobre 2018 relative à la lutte contre la fraude : " I. - Sans préjudice des plaintes dont elle prend l'initiative, l'administration est tenue de dénoncer au procureur de la République les faits qu'elle a examinés dans le cadre de son pouvoir de contrôle prévu à l'article L. 10 qui ont conduit à l'application, sur des droits dont le montant est supérieur à 100 000 &#128; : / 1° Soit de la majoration de 100 % prévue à l'article 1732 du code général des impôts ; / 2° Soit de la majoration de 80 % prévue au c du 1 de l'article 1728, aux b ou c de l'article 1729, au I de l'article 1729-0 A ou au dernier alinéa de l'article 1758 du même code ; / 3° Soit de la majoration de 40 % prévue au b du 1 de l'article 1728 ou aux a ou b de l'article 1729 dudit code, lorsqu'au cours des six années civiles précédant son application le contribuable a déjà fait l'objet lors d'un précédent contrôle de l'application des majorations mentionnées aux 1° et 2° du présent I et au présent 3° ou d'une plainte de l'administration. / (...) L'application des majorations s'apprécie au stade de la mise en recouvrement. Toutefois, lorsqu'une transaction est conclue avant la mise en recouvrement, l'application des majorations s'apprécie au stade des dernières conséquences financières portées à la connaissance du contribuable dans le cadre des procédures prévues aux articles L. 57 et L. 76 du présent livre. / Lorsque l'administration dénonce des faits en application du présent I, l'action publique pour l'application des sanctions pénales est exercée sans plainte préalable de l'administration (...) / II. - Sous peine d'irrecevabilité, les plaintes portant sur des faits autres que ceux mentionnés aux premier à cinquième alinéas du I et tendant à l'application de sanctions pénales en matière d'impôts directs, de taxe sur la valeur ajoutée et autres taxes sur le chiffre d'affaires, de droits d'enregistrement, de taxe de publicité foncière et de droits de timbre sont déposées par l'administration à son initiative, sur avis conforme de la commission des infractions fiscales. / La commission examine les affaires qui lui sont soumises par le ministre chargé du budget. Le contribuable est avisé de la saisine de la commission qui l'invite à lui communiquer, dans un délai de trente jours, les informations qu'il jugerait nécessaires. / Le ministre est lié par les avis de la commission (...) ". <br/>
<br/>
              3. En vertu de ces dispositions, l'administration est tenue de dénoncer au procureur de la République les faits l'ayant conduit à établir des redressements portant sur des droits d'un montant supérieur à 100 000 euros et ayant donné lieu à l'application de la majoration de 100 % pour opposition à contrôle fiscal, de la majoration de 80 % pour découverte d'activités occultes, abus de droit, manoeuvres frauduleuses, défaut de déclaration, présomption de revenus en fonction du niveau de vie ou, en cas de réitération, de la majoration de 40 % pour défaut de production, manquement délibéré ou abus de droit lorsqu'il n'est pas établi que le contribuable a eu l'initiative principale du ou des actes constitutifs de l'abus de droit ou en a été le principal bénéficiaire. Les faits ainsi dénoncés peuvent donner lieu à mise en mouvement de l'action publique sans plainte préalable de l'administration. En revanche, les faits autres que ceux qui viennent d'être mentionnés ne peuvent donner lieu à engagement de poursuites pour fraude fiscale que sur plainte de l'administration fiscale, après avis conforme de la commission des infractions fiscales.<br/>
<br/>
              4. Ces dispositions de l'article L. 228 du livre des procédures fiscales, dans leur rédaction issue de la loi du 23 octobre 2018 relative à la lutte contre la fraude, sont applicables au litige par lequel l'association requérante demande l'annulation pour excès de pouvoir de la circulaire du 7 mars 2019 du ministre de l'action et des comptes publics et de la garde des sceaux, ministre de la justice, relative à la réforme de la procédure de poursuite pénale de la fraude fiscale et au renforcement de la coopération entre l'administration fiscale et la justice en matière de lutte contre la fraude fiscale, qui a pour objet d'en éclairer la portée. Ces dispositions n'ont pas déjà été déclarées conformes à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel. Le moyen tiré de ce qu'elles porteraient atteinte au principe d'égalité devant la loi, énoncé à l'article 6 de la Déclaration des droits de l'homme et du citoyen, soulève une question qui peut être regardée comme présentant un caractère sérieux. Ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article L. 228 du livre des procédures fiscales, dans leur rédaction résultant de la loi du 23 octobre 2018, est renvoyée au Conseil constitutionnel.<br/>
Article 2 : Il est sursis à statuer sur les conclusions de la requête de l'Association française des entreprises privées.<br/>
Article 3 : La présente décision sera notifiée à l'Association française des entreprises privées et au ministre de l'action et des comptes publics.<br/>
Copie en sera adressée au Premier ministre et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
