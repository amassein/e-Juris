<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041920156</ID>
<ANCIEN_ID>JG_L_2020_05_000000440216</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/92/01/CETATEXT000041920156.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 22/05/2020, 440216, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-05-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440216</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:440216.20200522</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 440216, par une requête, enregistrée le 22 avril 2020 au secrétariat du contentieux du Conseil d'Etat, l'association Alliance Vita et l'association Juristes pour l'enfance demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution des dispositions du 3° de l'article 1er de l'arrêté du 14 avril 2020 du ministre des solidarités et de la santé complétant l'arrêté du 23 mars 2020 prescrivant les mesures d'organisation et de fonctionnement du système de santé nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire et de leurs deux annexes ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 1 000 euros à chacune des associations requérantes au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elles soutiennent que :<br/>
             - elles justifient d'un intérêt leur donnant qualité pour agir ;<br/>
             - la condition d'urgence est remplie, compte tenu des risques pour la santé susceptibles d'être causés par les mesures contestées de l'arrêté en litige ;<br/>
             - il existe un doute sérieux quant à la légalité des dispositions contestées de l'arrêté en litige ;<br/>
             - les dispositions contestées de l'arrêté en litige sont entachées d'incompétence en ce qu'elles dérogent à des dispositions législatives et réglementaires, notamment à l'article L. 5121-8 et aux articles R. 2212-10, R. 2212-16 et R. 2212-17 du code de la santé publique ;<br/>
             - elles sont entachées d'incompétence, dès lors que le ministre de la santé n'est habilité par l'article L. 3131-16 du code de la santé publique qu'à prendre des mesures réglementaires visant à mettre fin à la crise sanitaire ;<br/>
             - elles méconnaissent les conditions de fond posées par l'article L. 3131-16 du code de la santé publique, dès lors que les mesures qu'elles comportent ne sont ni nécessaires ni proportionnées aux risques sanitaires encourus et qu'elles ne sont pas davantage appropriées aux circonstances de temps et de lieu ; <br/>
             - elles mettent en danger la santé des femmes et particulièrement des mineures.<br/>
              Par un mémoire en défense, enregistré le 4 mai 2020, le ministre des solidarités et de la santé conclut au rejet de la requête. Il soutient qu'aucun des moyens soulevés n'est propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité des dispositions en litige de l'arrêté contesté. <br/>
<br/>
              La requête a été communiquée au Premier ministre et à la Haute Autorité de santé qui n'ont pas produit d'observations.<br/>
<br/>
              En application des dispositions de l'article R. 611-7 du code de justice administrative, les parties ont été informées que la décision du juge des référés du Conseil d'Etat était susceptible d'être fondée sur le moyen, relevé d'office, tiré de ce que certaines des dispositions de l'arrêté contesté relatives à la possibilité de prescrire des spécialités pharmaceutiques à base de mifepristone et celles à base de misoprostol par dérogation à l'article L. 5121-8 du code de la santé publique en dehors du cadre de leur autorisation de mise sur le marché pourraient être regardées comme entachées d'incompétence, faute d'avoir été édictées par le Premier ministre, sur le fondement du 9° de l'article L. 3131-15 du code de la santé publique.<br/>
<br/>
<br/>
              2° Sous le n° 440317, par une requête et un mémoire en réplique, enregistrés le 28 avril et le 9 mai 2020 au secrétariat du contentieux du Conseil d'Etat, l'association Pharmac'éthique demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution des dispositions du 3° de l'article 1er de l'arrêté du ministre des solidarités et de la santé du 14 avril 2020 complétant l'arrêté du 23 mars 2020 prescrivant les mesures d'organisation et de fonctionnement du système de santé nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire et de leurs deux annexes ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elle soutient que :<br/>
             - elle justifie d'un intérêt lui donnant qualité à agir ;<br/>
             - la condition d'urgence est remplie, compte tenu des risques pour la santé susceptibles d'être causés par les mesures contestées de l'arrêté en litige ;<br/>
             - il existe un doute sérieux quant à la légalité des dispositions contestées de l'arrêté en litige ;<br/>
             - elles sont entachées d'incompétence, dès lors que, d'une part, seul le Premier ministre est habilité par les dispositions du 9° de l'article L. 3131-15 du code de la santé publique à prendre des mesures relatives à la prescription de médicaments hors autorisation de mise sur le marché et, d'autre part, le ministre chargé de la santé ne peut, sur le fondement de l'article L. 3131-16 du même code, arrêter des mesures relatives à l'organisation et au fonctionnement du système de santé qu'en vue de mettre fin à la catastrophe sanitaire ;<br/>
             - elles méconnaissent les conditions de fond posées par l'article L. 3131-16 du code de la santé publique, dès lors que les mesures qu'elles comportent ne visent pas à mettre fin à la catastrophe sanitaire et ne sont ni nécessaires, ni proportionnées ;  <br/>
             - elles portent atteinte au droit à la protection de la santé, compte tenu des risques encourus par les femmes auxquelles les médicaments litigieux sont prescrits aux dosages auxquels elles renvoient ;<br/>
             - elles exposent les pharmaciens au risque de voir leur responsabilité disciplinaire engagée, soit qu'ils délivrent des médicaments nécessaires à l'interruption volontaire de grossesse alors qu'ils font l'objet d'une prescription hors autorisation de mise sur le marché, soit au contraire qu'ils en refusent la délivrance ; <br/>
             - elles méconnaissent le principe d'égalité, faute de prévoir que le pharmacien bénéficie d'une " clause de conscience " telle qu'elle est consacrée au bénéfice de certains professionnels de santé ;<br/>
             - il n'y a pas de date de fin d'application de ces dispositions ; <br/>
             - elles édictent des mesures qui ne sont désormais plus nécessaires.<br/>
              Par un mémoire en défense, enregistré le 4 mai 2020, le ministre des solidarités et de la santé conclut au rejet de la requête. Il soutient qu'aucun des moyens soulevés n'est propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité des dispositions contestées de l'arrêté en litige. <br/>
<br/>
              La requête a été communiquée au Premier ministre et à la Haute Autorité de santé qui n'ont pas produit d'observations.<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, l'association Alliance Vita, l'association Juristes pour l'enfance, l'association Pharmac'éthique et, d'autre part, le Premier ministre, le ministre des solidarités et de la santé et la Haute Autorité de santé ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 12 mai 2020 à 16 heures :<br/>
<br/>
              -	Me Rameix, avocat au Conseil et à la Cour de cassation, avocat des associations Alliance Vita, Juristes pour l'enfance et Pharmac'éthique ;<br/>
<br/>
              - les représentantes des associations Alliance Vita, Juristes pour l'enfance et Pharmac'éthique ;<br/>
<br/>
              -	les représentants du ministre des solidarités et de la santé ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a différé la clôture de l'instruction au 14 mai 2020 à 18 heures, puis au 15 mai 2020 à 18 heures ; <br/>
<br/>
              Vu le mémoire, enregistré sous le n° 440317 le 14 mai 2020, présenté par l'association Pharmac'éthique par lequel elle persiste dans ses précédentes écritures ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré sous le n° 440216 le 14 mai 2020, présenté par le ministre des solidarités et de la santé par lequel il persiste dans ses précédentes écritures ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré sous le n° 440317 le 14 mai 2020, présenté par le ministre des solidarités et de la santé par lequel il persiste dans ses précédentes écritures ;<br/>
<br/>
              Vu le mémoire, enregistré sous le n° 440317 le 15 mai 2020, présenté par l'association Pharmac'éthique par lequel elle persiste dans ses précédentes écritures ;<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son préambule ;<br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - la loi n° 2020-546 du 11 mai 2020 ; <br/>
              - le décret n° 2020-293 du 23 mars 2020 ;<br/>
              - le décret n° 2020-545 du 11 mai 2020 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              Sur les circonstances :<br/>
<br/>
              2. L'émergence d'un nouveau coronavirus (covid-19), de caractère pathogène et particulièrement contagieux et sa propagation sur le territoire français ont conduit le ministre des solidarités et de la santé à prendre, par plusieurs arrêtés à compter du 4 mars 2020, des mesures sur le fondement des dispositions de l'article L. 3131-1 du code de la santé publique. En particulier, par un arrêté du 14 mars 2020, un grand nombre d'établissements recevant du public ont été fermés au public, les rassemblements de plus de 100 personnes ont été interdits et l'accueil des enfants, élèves et étudiants dans les établissements les recevant et les établissements scolaires et universitaires a été suspendu. Puis, par un décret du 16 mars 2020 motivé par les circonstances exceptionnelles découlant de l'épidémie de covid-19, modifié par décret du 19 mars, le Premier ministre a interdit le déplacement de toute personne hors de son domicile, sous réserve d'exceptions limitativement énumérées et devant être dûment justifiées, à compter du 17 mars à 12h, sans préjudice de mesures plus strictes susceptibles d'être ordonnées par le représentant de l'Etat dans le département. Le ministre des solidarités et de la santé a pris des mesures complémentaires par des plusieurs arrêtés successifs.<br/>
<br/>
              3. Le législateur, par l'article 4 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19, a déclaré l'état d'urgence sanitaire pour une durée de deux mois à compter du 24 mars 2020 puis, par l'article 1er de la loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ses dispositions, a prorogé cet état d'urgence sanitaire jusqu'au 10 juillet 2020 inclus. Par un décret du 23 mars 2020 pris sur le fondement de l'article L. 3131-15 du code de la santé publique issu de la loi du 23 mars 2020, plusieurs fois modifié et complété depuis lors, le Premier ministre a réitéré les mesures précédemment ordonnées tout en leur apportant des précisions ou restrictions complémentaires. Par un premier décret du 11 mai 2020, applicable les 11 et 12 mai 2020, le Premier ministre a abrogé l'essentiel des mesures précédemment ordonnées par le décret du 23 mars 2020 et en a pris de nouvelles. Enfin, par un second décret du 11 mai 2020, pris sur le fondement de la loi du 11 mai 2020 et abrogeant le précédent décret, le Premier ministre a prescrit les nouvelles mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire. <br/>
<br/>
              Sur les demandes :<br/>
<br/>
              4. Au 3° de l'article 1er de l'arrêté du 14 avril 2020 modifiant l'arrêté du 23 mars 2020 prescrivant les mesures d'organisation et de fonctionnement du système de santé nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire, le ministre des solidarités et de la santé a édicté des mesures aux fins d'adapter durant cette période les modalités de l'interruption volontaire de grossesse par voie médicamenteuse pratiquée en dehors d'un établissement de santé. Par deux requêtes qu'il y a lieu de joindre, l'association Alliance Vita et l'association Juristes pour l'enfance, d'une part, l'association Pharmac'éthique, d'autre part, demandent au juge des référés du Conseil d'Etat de suspendre l'exécution de ces dispositions. <br/>
<br/>
              En ce qui concerne le cadre juridique de l'interruption volontaire de grossesse pratiquée avant la fin de la douzième semaine de grossesse :<br/>
<br/>
              Quant aux règles générales s'appliquant à l'interruption volontaire de grossesse pratiquée avant la fin de la douzième semaine de grossesse : <br/>
<br/>
              5. Aux termes de l'article L. 2212-1 du code de la santé publique : " La femme enceinte qui ne veut pas poursuivre une grossesse peut demander à un médecin ou à une sage-femme l'interruption de sa grossesse. Cette interruption ne peut être pratiquée qu'avant la fin de la douzième semaine de grossesse (...) ". Aux termes de l'article L. 2212-2 du même code : " L'interruption volontaire d'une grossesse ne peut être pratiquée que par un médecin ou, pour les seuls cas où elle est réalisée par voie médicamenteuse, par une sage-femme. / Elle ne peut avoir lieu que dans un établissement de santé, public ou privé, ou dans le cadre d'une convention conclue entre le praticien ou la sage-femme ou un centre de planification ou d'éducation familiale ou un centre de santé et un tel établissement (...) ". Aux termes de l'article L. 2212-3 de ce code : " Le médecin ou la sage-femme sollicité par une femme en vue de l'interruption de sa grossesse doit, dès la première visite, informer celle-ci des méthodes médicales et chirurgicales d'interruption de grossesse et des risques et des effets secondaires potentiels (...) ". Aux termes de l'article L. 2212-4 du même code : " Il est systématiquement proposé, avant et après l'interruption volontaire de grossesse, à la femme majeure une consultation avec une personne ayant satisfait à une formation qualifiante en conseil conjugal ou toute autre personne qualifiée dans un établissement d'information, de consultation ou de conseil familial, un centre de planification ou d'éducation familiale, un service social ou un autre organisme agréé. (...) / Pour la femme mineure non émancipée, cette consultation préalable est obligatoire (...) ". Aux termes de l'article L. 2212-5 du code de la santé publique : " Si la femme renouvelle, après les consultations prévues aux articles L. 2212-3 et L. 2212-4, sa demande d'interruption de grossesse, le médecin ou la sage-femme doit lui demander une confirmation écrite. Cette confirmation ne peut intervenir qu'après l'expiration d'un délai de deux jours suivant l'entretien prévu à l'article L. 2212-4 ". Aux termes de l'article L. 2212-6 de ce code : " En cas de confirmation, le médecin ou la sage-femme peuvent pratiquer personnellement l'interruption de grossesse dans les conditions fixées au second alinéa de l'article L. 2212-2 (...) ". Aux termes de l'article L. 2212-7 du même code : " Si la femme est mineure non émancipée, le consentement de l'un des titulaires de l'autorité parentale ou, le cas échéant, du représentant légal est recueilli. Ce consentement est joint à la demande qu'elle présente au médecin ou à la sage-femme en dehors de la présence de toute autre personne. / Si la femme mineure non émancipée désire garder le secret, le médecin ou la sage-femme doit s'efforcer, dans l'intérêt de celle-ci, d'obtenir son consentement pour que le ou les titulaires de l'autorité parentale ou, le cas échéant, le représentant légal soient consultés ou doit vérifier que cette démarche a été faite lors de l'entretien mentionné à l'article L. 2212-4. / Si la mineure ne veut pas effectuer cette démarche ou si le consentement n'est pas obtenu, l'interruption volontaire de grossesse ainsi que les actes médicaux et les soins qui lui sont liés peuvent être pratiqués à la demande de l'intéressée, présentée dans les conditions prévues au premier alinéa. Dans ce cas, la mineure se fait accompagner dans sa démarche par la personne majeure de son choix (...) ".<br/>
<br/>
              Quant aux règles particulières s'appliquant aux interruptions volontaires de grossesse par voie médicamenteuse : <br/>
<br/>
              6. Aux termes de l'article R. 2212-10 du code de la santé publique : " Les interruptions volontaires de grossesse pratiquées par un médecin ou une sage-femme dans le cadre de la convention mentionnée à l'article R. 2212-9 sont exclusivement réalisées par voie médicamenteuse et jusqu'à la fin de la cinquième semaine de grossesse (...) ", cette convention étant celle qui est prévue au deuxième alinéa de l'article L. 2212-2 cité au point 5. Aux termes de l'article R. 2212-12 de ce code : " Avant de recueillir le consentement écrit de la femme dont l'âge de la grossesse et dont l'état médical et psycho-social permet la réalisation d'une interruption volontaire de grossesse par mode médicamenteux, le médecin ou la sage-femme l'informe sur les différentes méthodes d'interruption volontaire de grossesse et sur leurs éventuelles complications (...) ". Aux termes de l'article R. 2212-13 : " Le médecin ou la sage-femme précise par écrit à la femme le protocole à respecter pour la réalisation de l'interruption volontaire de grossesse par mode médicamenteux. / La femme est invitée à se faire accompagner par la personne de son choix, notamment à l'occasion des consultations au cours desquelles sont administrés les médicaments ". Aux termes de l'article R. 2212-14 du code de la santé publique : " Le médecin ou la sage-femme informe la femme sur les mesures à prendre en cas de survenance d'effets secondaires et s'assure qu'elle dispose d'un traitement analgésique et qu'elle peut se rendre dans l'établissement de santé signataire de la convention dans un délai de l'ordre d'une heure ". Aux termes de l'article R. 2212-15 de ce code : " Le médecin ou la sage-femme remet à la femme un document écrit dans lequel sont indiqués l'adresse précise et le numéro de téléphone du service concerné de l'établissement de santé signataire de la convention. Le médecin ou la sage-femme lui indique la possibilité d'être accueillie à tout moment par cet établissement (...) ". Aux termes de l'article R. 2212-16 du même code : " Seuls les médecins, les sages-femmes, les centres de planification ou d'éducation familiale et les centres de santé ayant conclu la convention mentionnée à l'article R. 2212-9 peuvent s'approvisionner en médicaments nécessaires à la réalisation d'une interruption volontaire de grossesse par voie médicamenteuse (...) ". Aux termes de l'article R. 2212-17 de ce code : " Le médecin ou la sage-femme procède à la délivrance à la femme des médicaments nécessaires à la réalisation de l'interruption volontaire de grossesse. / La première prise de ces médicaments est effectuée en présence du médecin ou de la sage-femme ". <br/>
<br/>
      En ce qui concerne les dispositions contestées de l'arrêté :<br/>
               7. Le 3° de l'article 1er de l'arrêté du 14 avril 2020 insère dans l'arrêté du 23 mars 2020 prescrivant les mesures d'organisation et de fonctionnement du système de santé nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire des articles 10-4 et 10-5 comportant des mesures relatives à la réalisation, durant l'état d'urgence sanitaire, d'une interruption volontaire de grossesse par voie médicamenteuse en dehors d'un établissement de santé. Ces mesures permettent de déroger sur certains points à des dispositions du code de la santé publique, notamment celles citées aux points 5 et 6 qui, pour le reste, continuent à s'appliquer durant l'état d'urgence sanitaire. D'une part, elles autorisent la réalisation, en dehors d'un établissement de santé, d'une interruption volontaire de grossesse par voie médicamenteuse au-delà du délai de cinq semaines de grossesse prévu à l'article R. 2212-10 du code de la santé publique. Une telle interruption volontaire de grossesse ne peut toutefois être pratiquée que jusqu'à la septième semaine de grossesse et dans le respect du protocole validé par la Haute Autorité de santé le 9 avril 2020, reposant sur l'association médicamenteuse, d'une part, de l'antiprogestérone mifépristone et, d'autre part, d'une prostaglandine, le misoprostol, dans un dosage excédant celui prévu par son autorisation de mise sur le marché. D'autre part, elles permettent la prescription, dans le cadre d'une téléconsultation réalisée par un médecin ou une sage-femme conventionné, des médicaments nécessaires à la réalisation d'une interruption volontaire de grossesse par voie médicamenteuse, sous réserve du consentement libre et éclairé de la femme et, au vu de l'état de santé de celle-ci, de l'accord du professionnel de santé, la délivrance, par le pharmacien d'officine à la patiente, de ces médicaments, dans un conditionnement ajusté à la prescription et la prise du premier de ces médicaments lors d'une téléconsultation avec le médecin ou la sage-femme. <br/>
<br/>
              Quant à leur légalité externe :<br/>
<br/>
              8. Aux termes de l'article L. 3131-15 du code de la santé publique, dans sa rédaction issue de l'article 2 de la loi du 23 mars 2020 : " Dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le Premier ministre, peut, par décret réglementaire pris sur le rapport du ministre chargé de la santé, aux seules fins de garantir la santé publique : (...) / 9° En tant que de besoin, prendre toute mesure permettant la mise à disposition des patients des médicaments appropriés pour l'éradication de la catastrophe sanitaire (...). / Les mesures prescrites en application des 1° à 10° du présent article sont strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu. Il y est mis fin sans délai lorsqu'elles ne sont plus nécessaires ". <br/>
<br/>
              9. Aux termes de l'article L. 3131-16 du code de la santé publique : " Dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le ministre chargé de la santé peut prescrire, par arrêté motivé, toute mesure réglementaire relative à l'organisation et au fonctionnement du dispositif de santé, à l'exception des mesures prévues à l'article L. 3131-15, visant à mettre fin à la catastrophe sanitaire mentionnée à l'article L. 3131-12. / Dans les mêmes conditions, le ministre chargé de la santé peut prescrire toute mesure individuelle nécessaire à l'application des mesures prescrites par le Premier ministre en application des 1° à 9° de l'article L. 3131-15. / Les mesures prescrites en application du présent article sont strictement nécessaires et proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu. Il y est mis fin sans délai lorsqu'elles ne sont plus nécessaires ".<br/>
<br/>
              10. Les requérantes soutiennent que le ministre des solidarités et de la santé n'était pas compétent pour édicter les dispositions de l'arrêté du 14 avril 2020 exposées au point 7 dès lors qu'elles dérogent à des dispositions législatives et réglementaires du code de la santé publique et qu'elles excèdent le champ d'application des mesures visées par l'article L. 3131-16 du même code cité au point précédent. Toutefois, les dispositions de l'article L. 3131-16 du code de la santé publique habilitent, durant l'état d'urgence sanitaire et dans les circonscriptions territoriales où il est déclaré, le ministre chargé de la santé à prescrire toute mesure réglementaire nécessaire pour adapter, de façon temporaire, l'organisation et le fonctionnement du dispositif de santé pour répondre à la situation sanitaire causée par la catastrophe mentionnée à l'article L. 3131-12, y compris en matière de médicaments, dès lors qu'il ne s'agit pas de médicaments destinés à éradiquer l'épidémie à l'origine de cette catastrophe pour lesquels, en vertu du 9° de l'article L. 3131-15 du même code, la compétence est dévolue au Premier ministre. En outre, il résulte de l'instruction que les dispositions contestées de l'arrêté du 14 avril 2020, en ce que, notamment, elles permettent le recours à des téléconsultations en matière d'interruption volontaire de grossesse par voie médicamenteuse, sont de nature à contribuer à la diminution de la circulation du covid-19 et, dès lors, à ce que la catastrophe sanitaire prenne fin. Par suite, le moyen tiré de ce que le ministre des solidarités et de la santé n'était pas compétent pour édicter les dispositions litigieuses de l'arrêté du 14 avril 2020 n'est pas propre à créer, en l'état de l'instruction, un doute sérieux quant à leur légalité. <br/>
<br/>
              Quant à leur légalité interne :<br/>
<br/>
              11. En premier lieu, le moyen présenté par les requérantes et tiré de ce que les dispositions litigieuses de l'arrêté contesté comporteraient des mesures en matière d'interruption volontaire de grossesse par voie médicamenteuse en dehors d'un établissement de santé qui seraient, en méconnaissance des exigences posées à l'article L. 3131-16 du code de la santé publique, ni strictement nécessaires et proportionnées aux risques sanitaires encourus, ni appropriées aux circonstances de temps et de lieu, n'apparaît pas, en l'état de l'instruction, de nature à créer un doute sérieux sur sa légalité, eu égard au contenu des mesures exposé au point 7. <br/>
              12. En deuxième lieu, les requérantes soutiennent que les dispositions de l'arrêté contesté, notamment celles relatives aux interruptions volontaires de grossesse par voie médicamenteuse pratiquées au-delà de la cinquième semaine de grossesse et jusqu'à la septième semaine de grossesse, qui renvoient à un protocole médicamenteux prévoyant la prescription de doses de misoprostol au-delà de celles figurant dans les autorisations de mise sur le marché des spécialités pharmaceutiques à base de misoprostol, exposent les femmes à des risques pour leur santé alors qu'elles ne sont pas prises en charge pour leur interruption volontaire de grossesse dans un établissement de santé. Toutefois, il résulte des dispositions citées au point 6 que le médecin ou la sage-femme conventionné prescrivant une interruption volontaire de grossesse par voie médicamenteuse, que ce soit au cours d'une consultation classique ou d'une téléconsultation, doit informer la femme sur les mesures à prendre en cas de survenance d'effets secondaires, lui prescrire un traitement analgésique approprié et l'informer de ce qu'en cas de toute difficulté, elle peut se rendre à tout moment dans un établissement de santé conventionné dont il lui remet les coordonnées. En outre, il résulte de l'instruction que le protocole validé par la Haute Autorité de santé pour les interruptions volontaires de grossesse par voie médicamenteuse pratiquées au-delà de la cinquième semaine de grossesse et jusqu'à la septième semaine de grossesse est conforme aux principales recommandations nationales et internationales émises par les sociétés savantes de gynécologues et d'obstétriciens et qu'elles sont dans plusieurs pays mises en oeuvre en dehors d'un établissement de santé. Par suite, cette critique des requérantes n'apparaît pas, en l'état de l'instruction, de nature à créer un doute sérieux sur la légalité des dispositions contestées de l'arrêté du 14 avril 2020. <br/>
              13. En troisième lieu, si les dispositions contestées de l'arrêté sont applicables aux mineures demandant l'interruption de leur grossesse, elles n'ont ni pour objet ni pour effet de faire obstacle à l'application des dispositions législatives du code de la santé publique citées au point 5 qui leur sont spécifiques. En outre, les médecins et sages-femmes conventionnés, lesquels justifient, comme le prévoit l'article R. 2212-11 du code de la santé publique, d'une expérience professionnelle adaptée, doivent, en cas de doute sur la datation de la grossesse d'une mineure, adresser la patiente à l'établissement avec lequel ils ont signé une convention, ainsi que le mentionne d'ailleurs cette convention. Dans ces conditions, les critiques portées par les requérantes aux dispositions en litige de l'arrêté du 14 avril 2020 en ce qu'elles s'appliquent aux mineures n'apparaissent pas, en l'état de l'instruction, de nature à susciter un doute sérieux sur leur légalité.<br/>
              14. En quatrième lieu, contrairement à ce que soutient l'association Pharmac'éthique, les dispositions de l'arrêté contesté, en ce qu'elles prévoient notamment que les médicaments nécessaires à une interruption volontaire de grossesse par voie médicamenteuse faisant l'objet d'une prescription non conforme à l'autorisation de mise sur le marché sont délivrés par le pharmacien d'officine désigné par la patiente, ne font pas, par elles-mêmes, obstacle au respect des devoirs déontologiques des pharmaciens énoncés aux articles R. 4235-1 et suivants du code de la santé publique. Par suite, le moyen présenté par les requérantes tiré de ce que les dispositions de l'arrêté en litige exposeraient les pharmaciens au risque de poursuites disciplinaires n'apparaît pas sérieux au sens de l'article L. 521-1 du code de justice administrative.<br/>
              15. En cinquième lieu, l'association Pharmac'éthique ne saurait utilement soutenir que les dispositions contestées de l'arrêté du 14 avril 2020 ont été édictées en méconnaissance du principe d'égalité, faute de prévoir, à l'instar des dispositions figurant à l'article L. 2212-8 du code de la santé publique au bénéfice, notamment, des médecins et des sages-femmes, une " clause de conscience " pour les pharmaciens, lesquels sont, en tout état de cause, dans une situation différente, au regard des dispositions en cause de l'arrêté contesté, de celle des médecins et des sages-femmes. <br/>
<br/>
              16. En dernier lieu, aux termes du troisième alinéa de l'article L. 3131-14 du code de la santé publique : " Les mesures prises en application du présent chapitre cessent d'avoir effet en même temps que prend fin l'état d'urgence sanitaire ". Dès lors, contrairement à ce que soutient l'association Pharmac'éthique, si l'arrêté du 14 avril 2020 ne comporte aucune disposition relative à une date de fin d'application, les mesures qu'il édicte au 3° de son article 1er sur le fondement de l'article L. 3131-16 du code de la santé publique ne sont pas dénuées de date de fin d'application. En outre, si en vertu du dernier alinéa de l'article L. 3131-16, il est mis fin aux mesures prescrites en application de cet article " sans délai lorsqu'elles ne sont plus nécessaires ", il ne résulte pas de l'instruction que l'évolution des circonstances depuis le 14 avril 2020, et en particulier depuis le 11 mai 2020, conduise à regarder les dispositions en litige de l'arrêté comme étant devenues illégales. Par suite, les moyens tirés de ce que les dispositions contestées de cet arrêté seraient illégales en ce qu'elles seraient dépourvues de toute date de fin d'application et de ce qu'elles seraient devenues illégales compte tenu de l'évolution des circonstances depuis leur édiction ne sont pas de nature à créer, en l'état de l'instruction, un doute sérieux quant à leur légalité.<br/>
              17. Il résulte de tout ce qui précède que, sans qu'il soit besoin de se prononcer sur la condition d'urgence, les requêtes des associations Alliance Vita, Juristes pour l'enfance et Pharmac'éthique ne peuvent qu'être rejetées, y compris en ce qu'elles comportent des conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : Les requêtes des associations Alliance Vita, Juristes pour l'enfance et Pharmac'éthique sont rejetées.<br/>
Article 2 : La présente ordonnance sera notifiée à l'association Alliance Vita, à l'association Juristes pour l'enfance, à l'association Pharmac'éthique et au ministre des solidarités et de la santé. <br/>
Copie en sera adressée au Premier ministre et à la Haute Autorité de santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
