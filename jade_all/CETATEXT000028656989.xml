<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028656989</ID>
<ANCIEN_ID>JG_L_2014_02_000000348062</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/65/69/CETATEXT000028656989.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 19/02/2014, 348062, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-02-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>348062</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Olivier Gariazzo</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:348062.20140219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 1er avril 2011 au secrétariat du contentieux du Conseil d'État, présenté par le ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'État, porte-parole du Gouvernement ; le ministre demande au Conseil d'État :<br/>
              1°) d'annuler les articles 1er et 2 de l'arrêt n° 09MA01315 du 3 février 2011 par lesquels la cour administrative d'appel de Marseille a annulé le jugement n° 0607600 du 2 mars 2009 du tribunal administratif de Marseille rejetant la demande de décharge des rappels de taxe sur la valeur ajoutée, ainsi que des pénalités correspondantes, auxquels l'EURL Entreprise Ali B...a été assujettie au titre de la période allant du 1er janvier 2002 au 31 décembre 2003 ;<br/>
              2°) réglant l'affaire au fond, de rétablir les impositions et pénalités en litige ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Gariazzo, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de M. B...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte des pièces du dossier soumis aux juges du fond qu'à la suite d'une vérification de comptabilité, l'EURL Entreprise Ali B...s'est vu notifier des rappels de taxe sur la valeur ajoutée au titre de la période allant du 1er janvier 2002 au 31 décembre 2003, assortis de pénalités ; qu'après avoir vainement réclamé contre ces rappels et pénalités auprès de l'administration, M. A... B..., agissant en qualité de liquidateur de l'EURL, a saisi le tribunal administratif de Marseille qui, par un jugement du 2 mars 2009, a rejeté sa demande de décharge ; que le ministre chargé du budget se pourvoit en cassation contre les articles 1er et 2 de l'arrêt du 3 février 2011 par lesquels la cour administrative d'appel de Marseille a annulé le jugement du tribunal et accordé la décharge des rappels de taxe sur la valeur ajoutée et des pénalités en litige ;<br/>
<br/>
              2. Considérant que si, eu égard aux garanties dont le livre des procédures fiscales entoure la mise en oeuvre d'une vérification de comptabilité, l'administration est tenue, lorsque, faisant usage de son droit de communication, elle consulte au cours d'une vérification tout ou partie de la comptabilité tenue par l'entreprise vérifiée mais se trouvant chez un tiers, de soumettre l'examen des pièces obtenues à un débat oral et contradictoire avec le contribuable, il n'en est pas de même lorsque lui sont communiqués des documents ne présentant pas le caractère de pièces comptables de l'entreprise vérifiée ; que si les doubles des factures originales établies par une entreprise à l'intention de ses clients justifient ses écritures comptables et présentent ainsi le caractère de pièces comptables de l'entreprise qui les a émises, tel n'est pas le cas en revanche des factures originales elles-mêmes, qui n'ont, le cas échéant, le caractère de pièces comptables que pour les clients de cette entreprise ;<br/>
<br/>
              3. Considérant que, pour juger irrégulière la procédure d'imposition de l'EURL, la cour, après avoir rappelé que l'administration avait rejeté sa comptabilité et procédé à la reconstitution de ses recettes, a regardé comme des pièces comptables de cette entreprise se trouvant chez un tiers les factures originales qu'elle avait établies à l'intention d'entreprises clientes et qui avaient été obtenues par l'administration dans le cadre de l'exercice de son droit de communication auprès de celles-ci ; qu'il résulte de ce qui a été dit ci-dessus qu'elle a, sur ce point, entaché son arrêt d'une erreur de droit ; que le ministre est, dès lors, fondé à demander l'annulation des articles 1er et 2 de l'arrêt qu'il attaque ;<br/>
<br/>
              4. Considérant que l'article L. 761-1 du code de justice administrative fait obstacle à ce qu'une somme soit mise à ce titre à la charge de l'État qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Les articles 1er et 2 de l'arrêt du 3 février 2011 de la cour administrative d'appel de Marseille sont annulés.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Marseille.<br/>
Article 3 : Les conclusions de M. B... présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'économie et des finances et à M. A... B....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
