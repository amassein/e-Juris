<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033788963</ID>
<ANCIEN_ID>JG_L_2016_12_000000391079</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/78/89/CETATEXT000033788963.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 30/12/2016, 391079, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391079</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Sandrine Vérité</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:391079.20161230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              La SAS Valette Foie gras a demandé au tribunal administratif de Toulouse, premièrement, d'annuler pour excès de pouvoir la décision du 7 août 2002 par laquelle le directeur régional du travail, de l'emploi et de la formation professionnelle de Midi-Pyrénées lui a refusé toute aide publique à la formation en raison d'une infraction en matière de travail dissimulé et, deuxièmement, de condamner l'Etat à lui verser la somme de 230 597,65 euros en réparation du préjudice qu'elle estime avoir subi du fait de cette décision illégale, majorée des intérêts au taux légal à compter du 1er mars 2004 et de la capitalisation des intérêts. <br/>
<br/>
              Par un jugement n° 0704713 du 29 juillet 2011, le tribunal administratif de Toulouse a, d'une part, annulé la décision du 7 août 2002 rejetant la demande d'aide publique de la société au titre de l'engagement de développement de la formation des industries agro-alimentaires de Midi-Pyrénées ainsi que la décision refusant de lui verser une participation au titre du Fonds social européen révélée par le courrier du préfet de la région Midi-Pyrénées à l'organisme paritaire collecteur agréé Agefos-PME du 8 juillet 2002 et, d'autre part, rejeté le surplus de ses conclusions. <br/>
<br/>
              Par un arrêt n° 11BX02577 du 16 octobre 2012, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par la SAS Valette Foie gras et tendant, premièrement, à la réformation du jugement du tribunal administratif de Toulouse en tant qu'il a rejeté sa demande indemnitaire et, deuxièmement, à ce qu'une somme de 230 597,65 euros soit mise à la charge de l'Etat en réparation du préjudice subi, majorée des intérêts au taux légal et de la capitalisation des intérêts.<br/>
<br/>
              Par une décision n° 364585 du 27 août 2014, le Conseil d'Etat statuant au contentieux a, d'une part, annulé cet arrêt de la cour administrative d'appel de Bordeaux du 16 octobre 2012 en tant qu'il statue sur les conclusions de la SAS Valette Foie gras à fin d'indemnisation du préjudice subi du fait de la perte de chance de bénéficier d'aides publiques au titre de l'engagement de développement de la formation pour les années 2002 et 2003 et du Fonds social européen pour l'année 2002 et, d'autre part, rejeté le surplus des conclusions du pourvoi de la SAS Valette Foie gras.<br/>
<br/>
              Par un arrêt n° 14BX02637 du 16 avril 2015, la cour administrative d'appel de Bordeaux a rejeté, dans la mesure du renvoi après la cassation prononcée, l'appel formé par la SAS Valette Foie gras.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 16 juin et 10 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, la SAS Valette Foie gras demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt de la cour administrative d'appel de Bordeaux  du 16 avril 2015 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sandrine Vérité, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la SAS Valette Foie Gras.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la SAS Valette Foie gras a mis en place un plan de formation à destination de ses salariés pour les années 2001, 2002 et 2003 et sollicité, à cette fin, des aides publiques au titre du Fonds social européen et de l'engagement de développement de la formation conclu avec l'Etat sur le fondement de l'article L. 951-5 du code du travail, alors en vigueur. Par une décision révélée par une lettre du 8 juillet 2002, le préfet de la région Midi-Pyrénées a refusé d'accorder à la société, pour cette année, une aide au titre de l'objectif 3 du Fonds social européen (FSE) puis, par une décision du 7 août 2002, le directeur régional du travail, de l'emploi et de la formation professionnelle de Midi-Pyrénées a refusé de lui accorder, pour cette même année, une aide au titre de l'engagement de développement de la formation des industries agro-alimentaires de la région, en raison de la constatation par les services d'inspection du travail d'une infraction en matière de travail dissimulé. Toutefois, par un jugement du 27 novembre 2003, le tribunal correctionnel de Cahors, ne retenant pas le délit de travail dissimulé, a condamné la SAS Valette Foie gras à une contravention de cinquième classe pour omission de déclaration préalable à l'embauche. Par un jugement du 29 juillet 2011, le tribunal administratif de Toulouse a, à la demande de la SAS Valette Foie gras, annulé pour excès de pouvoir ces décisions mais, en revanche, rejeté sa demande tendant à l'indemnisation des préjudices qu'elle estime avoir subi de leur fait. Par un premier arrêt du 16 octobre 2012, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par la société Valette Foie gras tendant à la réformation de ce jugement en tant qu'il rejetait ses conclusions indemnitaires. Par une décision du 27 août 2014, le Conseil d'Etat statuant au contentieux a, d'une part, annulé cet arrêt en tant qu'il statuait sur les conclusions tendant à l'indemnisation du préjudice subi du fait de la perte de chance de bénéficier d'aides publiques au titre de l'engagement de développement de la formation pour les années 2002 et 2003 et du Fonds social européen pour l'année 2002 et, d'autre part, renvoyé l'affaire, dans la mesure de la cassation ainsi prononcée, devant cette cour. Par un second arrêt du 16 avril 2015, contre lequel la SAS Valette Foie gras se pourvoit en cassation, la cour a rejeté, dans cette même mesure, l'appel de la société.<br/>
<br/>
              2. Pour juger que la SAS Valette Foie gras devait être regardée comme ayant obtenu le financement de l'ensemble de ses dépenses de formation par le fonds d'assurance formation Agefos PME et en déduire qu'elle ne justifiait d'aucun préjudice pour avoir perdu une chance de bénéficier des subventions du Fonds social européen au titre de 2002 et des aides de l'Etat au titre de l'engagement de développement de la formation au titre de 2002 et 2003, la cour a retenu que la société appelante n'établissait pas que les contributions complémentaires mentionnées dans les documents de l'Agefos relatifs au financement de son plan de formation ne constituaient pas des fonds mutualisés par l'organisme de financement, ni qu'elle aurait " financé Agefos " au-delà de ses obligations légales ou conventionnelles de financement. En statuant ainsi, alors que la SAS Valette Foie gras produisait une lettre du directeur d'Agefos PME Midi-Pyrénées du 29 septembre 2008 accompagnée de pièces comptables indiquant, d'une part, que la société avait versé à cet organisme, pour le financement qu'elle lui déléguait des actions de formation effectuées au titre de l'année 2002, outre la contribution légale de 23 664 euros, une contribution complémentaire " versé(e) par l'entreprise " atteignant 171 652,56 euros et, d'autre part, que la " subvention Agefos " d'un montant de 23 000 euros provenait d'un " financement exceptionnel accordé sur les fonds mutualisés " de l'organisme, et que le ministre intimé admettait lui-même que la " société a versé à l'organisme (...) 171 652,56 euros (au titre) de sa contribution volontaire au développement de la formation professionnelle continue de l'année 2002 ", la cour a dénaturé les faits de l'espèce.<br/>
<br/>
              3. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, que la SAS Valette Foie gras est fondée à demander l'annulation de l'arrêt qu'elle attaque. Il y a lieu, en application du second alinéa de l'article L. 821-2 du code de justice administrative, de régler l'affaire au fond dans la mesure de la cassation prononcée par la décision du Conseil d'Etat statuant au contentieux du 27 août 2014 mentionnée au point 1, en statuant sur l'appel de la société dans cette même mesure.<br/>
<br/>
              Sur le préjudice résultant de la perte de chance de bénéficier d'une aide au titre de l'engagement de développement de la formation pour les années 2002 et 2003 :<br/>
<br/>
              4. Il résulte de l'instruction, notamment d'une lettre du directeur régional du travail, de l'emploi, de la formation professionnelle et du dialogue social du 11 mars 2004, et il n'est pas contesté par la SAS Valette Foie gras, que si cette dernière n'a pu obtenir l'aide qu'elle avait sollicitée de l'Etat au titre de l'engagement de développement de la formation pour les années 2002 et 2003, pour intégrer deux de ses salariés, MM. B...D...et A...C..., au sein d'une action collective en vue d'obtenir un certificat de qualification professionnelle d'agent de maîtrise, l'organisme paritaire collecteur agréé Agefaforia a pris en charge l'intégralité des coûts pédagogiques correspondants, par le biais de " fonds mutualisés exceptionnels ". Dès lors, la société ne justifie pas d'un préjudice du fait de la perte d'une chance de bénéficier d'une aide au titre de l'engagement de développement de la formation pour les années 2002 et 2003.<br/>
<br/>
              Sur le préjudice résultant de la perte de chance de bénéficier d'une aide au titre du Fonds social européen pour l'année 2002 :<br/>
<br/>
              En ce qui concerne les fins de non-recevoir : <br/>
<br/>
              5. D'une part, un requérant peut se borner à demander à l'administration réparation d'un préjudice qu'il estime avoir subi pour ne chiffrer ses prétentions que devant le juge administratif. Est ainsi sans incidence la circonstance que la SAS Valette Foie gras avait, par lettre du 25 février 2004, mentionné un montant de 217 625 euros, au demeurant à propos du seul refus d'aide au titre de l'engagement de développement de la formation, à raison duquel elle indiquait avoir subi " un lourd préjudice ", avant de chiffrer ses prétentions indemnitaires, dans sa requête introductive d'instance devant le tribunal administratif de Toulouse, à un montant de 100 246 euros, réévalué dans le dernier état de ses écritures devant ce tribunal à 230 597,65 euros. D'autre part, l'entreprise avait, par lettre du 13 janvier 2005 adressée au directeur régional du travail, de l'emploi et de la formation professionnelle de Midi-Pyrénées, demandé " réparation du préjudice " résultant du refus de subvention au titre du Fonds social européen pour l'année 2002. Dès lors, les fins de non-recevoir opposées à la demande indemnitaire de la SAS Valette Foie gras, devant le tribunal administratif de Toulouse, par le préfet de la région Midi-Pyrénées doivent être écartées.<br/>
<br/>
              En ce qui concerne le bien fondé des conclusions indemnitaires : <br/>
<br/>
              6. Par un jugement du 29 juillet 2011, devenu définitif sur ce point, le tribunal administratif de Toulouse a annulé pour excès de pouvoir la décision, révélée par une lettre du préfet de la région Midi-Pyrénées du 8 juillet 2002, refusant d'accorder à la SAS Valette Foie gras, pour l'année 2002, une aide au titre de l'objectif 3 du Fonds social européen, en jugeant que si le préfet alléguait avoir également pris en compte l'existence d'infractions répétées au code du travail par cette société, cette décision avait été prise au motif principal du délit de travail dissimulé, délit non constitué aux termes du jugement du tribunal correctionnel de Cahors du 27 novembre 2003. L'illégalité de cette décision a le caractère d'une faute de nature à engager la responsabilité de l'Etat.<br/>
<br/>
              7. Ainsi que le Conseil d'Etat statuant au contentieux l'a jugé par sa décision du 27 août 2014, mentionnée ci-dessus, il résulte de l'instruction, notamment du dossier de demande d'aide au titre de l'objectif 3 du Fonds social européen présenté par la SAS Valette Foie gras pour l'année 2002, que celle-ci justifiait de façon suffisamment précise des actions de formation envisagées pour un projet qui serait mis en oeuvre du 1er septembre 2001 au 31 août 2002, visant notamment à améliorer les méthodes de travail par le déploiement de progiciels de gestion intégrée et de paye, le développement de la traçabilité des produits et la mise en place d'un système d'évaluation des compétences. Il résulte également de l'instruction que ces actions étaient susceptibles de répondre aux priorités fixées par le document unique de programmation 2000-2006 de l'objectif 3, au nombre desquelles figure l'objectif de " moderniser les organisations du travail et développer les compétences ", comme en témoigne, au demeurant, le fait que la société a bénéficié de cette aide pour l'année 2001 au cours de laquelle ce projet de formation avait commencé à être mis en oeuvre. Dès lors, la SAS Valette Foie gras est fondée à soutenir que le refus illégal opposé par le préfet de la région Midi-Pyrénées l'a privée d'une chance sérieuse d'obtenir, pour l'année 2002, une aide au titre de l'objectif 3 du Fonds social européen, dont elle avait déjà été admise à bénéficier pour l'année 2001.<br/>
<br/>
              8. Il résulte de l'instruction qu'une aide au titre de l'objectif 3 du Fonds social européen aurait permis à la SAS Valette Foie gras, pour l'année 2002, de réduire le montant de la contribution volontaire qu'elle a versée, en complément de sa contribution obligatoire, à Agefos PME, à qui elle déléguait le financement des actions de formation en faveur de ses salariés dans le cadre d'une " délégation de paiement ", le cas échéant après apport par cet organisme d'une subvention provenant de ses fonds mutualisés. Pour l'année 2002, le document établi par ce même organisme et intitulé " Budget formation Entreprise SAS Valette géré par Agefos PME Midi-Pyrénées pour les années 2001, 2002 et 2003 ", retraçant le financement de ce plan de formation et non contesté en défense par le ministre chargé du travail, indique que le montant total des dépenses du plan a atteint 216 377,80 euros, dont 23 664 euros au titre de la contribution légale de l'entreprise, 171 652,56 euros au titre de sa contribution complémentaire et 23 000 euros issus d'une subvention d'Agefos PME. Par ailleurs, il ne résulte pas de l'instruction que ces dépenses n'auraient pas été, au moins en partie, éligibles à l'objectif 3 du Fonds social européen, de la même façon que l'étaient celles retracées pour l'année 2001. Si la SAS Valette Foie gras fait valoir que le plan de financement prévisionnel établi en 2001 escomptait un taux de co-financement par le Fonds social européen de 40 %, il résulte de l'instruction que la subvention en définitive accordée, le 3 février 2003, pour les actions de formation exécutées au cours de l'année 2001, a correspondu à un moindre taux de co-financement. Dès lors, compte-tenu, d'une part, du montant total des dépenses du plan de formation pour 2002 et, d'autre part, du taux de co-financement effectivement observé pour l'année 2001, il sera fait une juste appréciation du préjudice subi par la société requérante en condamnant l'Etat à lui verser une indemnité d'un montant de 57 000 euros.<br/>
<br/>
              9. Lorsqu'ils ont été demandés, et quelle que soit la date de cette demande, les intérêts moratoires dus en application de l'article 1153 du  code civil courent à compter du jour où la demande de paiement du principal est parvenue au débiteur ou, en l'absence d'une telle demande préalablement à la saisine du juge, à compter du jour de cette saisine. Par suite, la société a droit aux intérêts des sommes qui lui sont dues à compter du 14 janvier 2005, date de réception par le directeur régional du travail, de l'emploi, de la formation professionnelle de Midi-Pyrénées de sa demande préalable du 13 janvier 2005. La société a également présenté des conclusions à fin de capitalisation des intérêts par mémoire enregistré le 23 septembre 2008 au greffe du tribunal administratif de Toulouse. A cette date, il était dû plus d'une année d'intérêts. Dès lors, conformément aux dispositions de l'article 1154 du code civil, il y a lieu de faire droit à cette demande à cette date et à chaque échéance annuelle à compter de cette date.<br/>
<br/>
              10. Il résulte de ce qui précède que la SAS Valette Foie gras est fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Toulouse a rejeté sa demande tendant à l'indemnisation de son préjudice en tant seulement que les premiers juges ont refusé de lui allouer une indemnité réparant la perte de chance de bénéficier d'une aide au titre du Fonds social européen pour l'année 2002. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat, en application de l'article L. 761-1 du code de justice administrative, une somme de 4 000 euros à verser à la SAS Valette Foie gras au titre des frais exposés par elle devant le Conseil d'Etat et la cour administrative d'appel de Bordeaux et non compris dans les dépens.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 16 avril 2015 est annulé.<br/>
Article 2 : L'Etat est condamné à verser à la SAS Valette Foie gras la somme de 57 000 euros avec intérêts au taux légal à compter du 14 janvier 2005. Les intérêts échus le 23 septembre 2008 seront capitalisés à cette date pour produire eux-mêmes intérêts.<br/>
Article 3 : Le jugement du tribunal administratif de Toulouse du 29 juillet 2011 est réformé en ce qu'il a de contraire à la présente décision.<br/>
Article 4 : L'Etat versera à la SAS Valette Foie gras une somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Le surplus des conclusions du pourvoi de la SAS Valette Foie gras et de sa requête d'appel devant la cour administrative d'appel de Bordeaux est rejeté.<br/>
Article 6 : La présente décision sera notifiée à la SAS Valette Foie gras et à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
