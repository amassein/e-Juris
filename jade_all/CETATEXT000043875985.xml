<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043875985</ID>
<ANCIEN_ID>JG_L_2021_07_000000454852</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/87/59/CETATEXT000043875985.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 27/07/2021, 454852, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>454852</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:454852.20210727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 22 juillet 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution de l'article 2-3 du décret n° 2021-699 du 1er juin 2021 modifié prescrivant les mesures générales nécessaires à la gestion de la sortie de crise sanitaire ainsi que le décret n° 2021-955 du 19 juillet 2021 modifiant le décret n° 2021-699 du 1er juin 2021, jusqu'à ce que ces dispositions soient modifiées de manière à ce que les vaccins reconnus en France mais administrés à l'étranger soient reconnus par le système Ameli ; <br/>
<br/>
              2°) à titre subsidiaire, d'enjoindre au Gouvernement, sous astreinte de 500 euros par jour à compter de la notification de la décision, de lui délivrer un " passe sanitaire " dès lors qu'il a reçu les deux doses de vaccins agréés. <br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est satisfaite dès lors que les dispositions contestées, d'une part, sont d'application immédiate et, d'autre part, privent la population de la possibilité de se rendre aux évènements et dans les établissements soumis à la présentation du " passe sanitaire " ;  <br/>
              - il est porté une atteinte grave et manifestement illégale à plusieurs libertés fondamentales ; <br/>
              - le décret contesté porte atteinte à sa liberté d'aller et venir ainsi qu'à sa liberté d'expression dès lors qu'il ne peut ne se rendre, d'une part, à des manifestations de la vie courante et, d'autre part, aux évènements organisés dans le cadre de la parution de son ouvrage " Génocide en Vendée 1793-1794 " ;<br/>
              - les dispositions attaquées portent atteinte au principe d'égalité devant la loi dès lors qu'il ne peut pas obtenir de " passe sanitaire " alors qu'il a été régulièrement vacciné par deux doses de vaccins agréés en France, en raison de l'impossibilité pour l'application informatique de prendre en compte sa deuxième injection, effectuée à l'étranger.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le décret 2021-699 du 1er juin 2021 ; <br/>
              - le décret n°2021-955 du 19 juillet 2021 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Le requérant qui saisit le juge des référés sur le fondement des dispositions de l'article L. 521-2 doit justifier des circonstances particulières caractérisant la nécessité pour lui de bénéficier à très bref délai d'une mesure de la nature de celles qui peuvent être ordonnées sur le fondement de cet article.<br/>
<br/>
              3. Le requérant se borne à faire valoir qu'il est dans l'impossibilité de participer aux divers évènements et d'accéder aux lieux mentionnés à l'article 1er du décret du 19 juillet 2021 faute d'être en mesure de pouvoir faire enregistrer ses vaccinations réalisées à l'étranger dans le système de traitement de données personnelles Ameli à partir duquel les passes sanitaires sont émis et, dès lors, de pouvoir obtenir un justificatif de vaccination. Toutefois, contrairement à ce qui est soutenu, le passe sanitaire ne se limite pas à la présentation d'un justificatif de vaccination et peut également résulter de la présentation du résultat d'un examen de dépistage RT-PCR ou d'un test antigénique qui, en l'état, n'est pas payant. Par suite, il ne justifie pas, ce faisant, que le décret contesté préjudicierait de manière suffisamment grave et immédiate à sa situation.<br/>
<br/>
              4. Il résulte de ce qui précède qu'il est manifeste que la requête de M. B... ne peut être accueillie. Elle doit, par suite, être rejetée, selon la procédure prévue par l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
