<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032002177</ID>
<ANCIEN_ID>JG_L_2016_01_000000396280</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/00/21/CETATEXT000032002177.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 29/01/2016, 396280, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-01-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396280</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2016:396280.20160129</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. H...B...A...a demandé au juge des référés du tribunal administratif de Lyon, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, de suspendre l'exécution de l'arrêté du 21 novembre 2015 par lequel le ministre de l'intérieur l'a assigné à résidence dans le 8ème arrondissement de Lyon. Par une ordonnance n° 1511085 du 8 janvier 2016, le juge des référés du tribunal administratif de Lyon a rejeté sa demande.<br/>
<br/>
              Par une requête enregistrée le 20 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...A...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de faire droit à sa demande de première instance.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est remplie ; <br/>
              - l'arrêté contesté porte une atteinte grave et manifestement illégale à sa liberté d'aller et venir ;<br/>
              - il est entaché d'une erreur de fait dès lors qu'il n'existe aucune raison sérieuse de penser que son comportement constitue une menace pour la sécurité et l'ordre publics.<br/>
<br/>
              Par un mémoire en défense enregistré le 25 janvier 2016, le ministre de l'intérieur conclut au rejet de la requête. Il soutient que les moyens soulevés par le requérant ne sont pas fondés.<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. B... A..., d'autre part, le ministre de l'intérieur ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 26 janvier 2016 à 15 heures au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Rousseau, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. B...A... ;<br/>
<br/>
              - la représentante du ministre de l'intérieur ;<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
              L'instruction ayant été rouverte jusqu'au 28 janvier 2016 à 12 heures pour permettre au ministre de l'intérieur de produire toute pièce relative à la situation actuelle de M. C..., connaissance de M. B...A...qui serait liée à des filières jihadistes ;<br/>
<br/>
              Vu le mémoire, enregistré le 27 janvier 2016, présenté par le ministre de l'intérieur, qui indique que M. C...fait l'objet d'un contrôle judiciaire depuis le 4 août 2015 ;<br/>
<br/>
              Vu le mémoire, enregistré le 27 janvier 2016, présenté par M. B... A..., qui persiste dans ses conclusions ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la loi n° 55-385 du 3 avril 1955 ;<br/>
              - la loi n° 2015-1501 du 20 novembre 2015 ;<br/>
              - le décret n° 2015-1475 du 14 novembre 2015 ;<br/>
              - le décret n° 2015-1476 du 14 novembre 2015 ;<br/>
              - le décret n° 2015-1478 du 14 novembre 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale " ;<br/>
<br/>
              2. Considérant qu'en application de la loi du 3 avril 1955, l'état d'urgence a été déclaré par le décret n° 2015-1475 du 14 novembre 2015, à compter du même jour à zéro heure, sur le territoire métropolitain et en Corse et prorogé pour une durée de trois mois, à compter du 26 novembre 2015, par l'article 1er de la loi du 20 novembre 2015 ; qu'aux termes de l'article 6 de la loi du 3 avril 1955, dans sa rédaction issue de la loi du 20 novembre 2015 : " Le ministre de l'intérieur peut prononcer l'assignation à résidence, dans le lieu qu'il fixe, de toute personne résidant dans la zone fixée par le décret mentionné à l'article 2 et à l'égard de laquelle il existe des raisons sérieuses de penser que son comportement constitue une menace pour la sécurité et l'ordre publics dans les circonscriptions territoriales mentionnées au même article 2. (...) / La personne mentionnée au premier alinéa du présent article peut également être astreinte à demeurer dans le lieu d'habitation déterminé par le ministre de l'intérieur, pendant la plage horaire qu'il fixe, dans la limite de douze heures par vingt-quatre heures. / L'assignation à résidence doit permettre à ceux qui en sont l'objet de résider dans une agglomération ou à proximité immédiate d'une agglomération. (...) / L'autorité administrative devra prendre toutes dispositions pour assurer la subsistance des personnes astreintes à résidence ainsi que celle de leur famille. / Le ministre de l'intérieur peut prescrire à la personne assignée à résidence : / 1° L'obligation de se présenter périodiquement aux services de police ou aux unités de gendarmerie, selon une fréquence qu'il détermine dans la limite de trois présentations par jour, en précisant si cette obligation s'applique y compris les dimanches et jours fériés ou chômés (...) " ; qu'il résulte de l'article 1er du décret n° 2015-1476 du 14 novembre 2015, modifié par le décret n° 2015-1478 du même jour, que les mesures d'assignation à résidence sont applicables à l'ensemble du territoire métropolitain et de la Corse à compter du 15 novembre à minuit ;<br/>
<br/>
              3. Considérant qu'il résulte de l'instruction que, par un arrêté du 21 novembre 2015, le ministre de l'intérieur a astreint M. B...A...à résider dans le 8ème arrondissement de Lyon, lui a fait obligation de se présenter deux fois par jour, à 8 heures et 19 heures, au commissariat de police de cet arrondissement, tous les jours de la semaine, y compris les jours fériés ou chômés, et lui a imposé de demeurer tous les jours, de 20 heures à 6 heures, dans les locaux où il réside à Lyon ; que cet arrêté prévoit que M. B...A...ne peut se déplacer en dehors de son lieu d'assignation à résidence sans avoir obtenu préalablement une autorisation écrite établie par le préfet du Rhône ; que, par un arrêté du 6 janvier 2016, le ministre de l'intérieur a modifié les conditions de l'assignation à résidence de M. B... A... en l'assignant à résider sur le territoire des communes de Lyon, Villeurbanne et Décines-Charpieu, sans préjudice des autres dispositions de l'arrêté initial, et en fixant la fin de l'état d'urgence comme terme à la mesure ; que, par une ordonnance du 8 janvier 2016, le juge des référés du tribunal administratif de Lyon, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté la demande de M. B... A... tendant à la suspension de l'exécution de l'arrêté du 21 novembre 2015 modifié ; que M. B... A... relève appel de cette ordonnance ;<br/>
<br/>
              En ce qui concerne la condition d'urgence :<br/>
<br/>
              4. Considérant qu'eu égard à son objet et à ses effets, notamment aux restrictions apportées à la liberté d'aller et venir, une décision prononçant l'assignation à résidence d'une personne, prise par l'autorité administrative en application de l'article 6 de la loi du 3 avril 1955, porte, en principe et par elle-même, sauf à ce que l'administration fasse valoir des circonstances particulières, une atteinte grave et immédiate à la situation de cette personne, de nature à créer une situation d'urgence justifiant que le juge administratif des référés, saisi sur le fondement de l'article L. 521-2 du code de justice administrative, puisse prononcer dans de très brefs délais, si les autres conditions posées par cet article sont remplies, une mesure provisoire et conservatoire de sauvegarde ; qu'aucun des éléments que le ministre de l'intérieur a fait valoir, dans ses écritures et au cours de l'audience publique, ne conduit à remettre en cause, au cas d'espèce, l'existence d'une situation d'urgence caractérisée de nature à justifier l'intervention du juge des référés dans les conditions d'urgence particulière prévues par l'article L. 521-2 du code de justice administrative ;<br/>
<br/>
              En ce qui concerne la condition tenant à l'atteinte grave et manifestement illégale à une liberté fondamentale :<br/>
<br/>
              5. Considérant qu'il appartient au juge des référés de s'assurer, en l'état de l'instruction devant lui, que l'autorité administrative, opérant la conciliation nécessaire entre le respect des libertés et la sauvegarde de l'ordre public, n'a pas porté d'atteinte grave et manifestement illégale à une liberté fondamentale, que ce soit dans son appréciation de la menace que constitue le comportement de l'intéressé, compte tenu de la situation ayant conduit à la déclaration de l'état d'urgence, ou dans la détermination des modalités de l'assignation à résidence ; que le juge des référés, s'il estime que les conditions définies à l'article L. 521-2 du code de justice administrative sont réunies, peut prendre toute mesure qu'il juge appropriée pour assurer la sauvegarde de la liberté fondamentale à laquelle il a été porté atteinte ;<br/>
<br/>
              6. Considérant, en premier lieu, qu'il ressort des termes mêmes de l'arrêté contesté que le ministre de l'intérieur s'est fondé, pour prendre l'arrêté contesté, sur la circonstance que M. B...A..., " ancien militaire entrainé notamment à l'usage des armes, est un islamiste radical favorable au jihad et susceptible de vouloir se rendre en Syrie pour rejoindre un groupe de combattants " ; <br/>
<br/>
              7. Considérant, en second lieu, qu'il résulte de l'instruction et notamment d'une " note blanche " des services de renseignements qui a été soumise au débat contradictoire que M. B...A..., qui est un ancien militaire ayant obtenu un brevet de parachutiste, un certificat de grenadier-voltigeur et une qualification de tireur de précision, est soupçonné d'être engagé en faveur du jihad et susceptible de vouloir se rendre en Syrie à cette fin ; que plusieurs photographies de lui en compagnie d'individus impliqués dans des filières jihadistes à destination de la Syrie, dont M. E...C..., interpelé, mis en examen et placé sous contrôle judiciaire en août 2015 à son retour d'un séjour en Syrie au sein de l'organisation " Etat islamique ", et M. I...D..., parti rejoindre cette organisation en Syrie en février 2015, ont été trouvées à son domicile à l'occasion d'une perquisition administrative ; que, en outre, le ministre a versé au dossier soumis au contradictoire plusieurs éléments obtenus par l'exploitation des données informatiques recueillies lors de cette perquisition, en particulier des enregistrements audio datant de juillet 2014 dont le ministre soutient sans être sérieusement contredit qu'il s'agit de chants jihadistes, plusieurs photographies du requérant avec différentes personnes soupçonnées de participer à des filières jihadistes notamment MM. C...et D...ainsi que M.G..., par ailleurs assigné à résidence, et enfin deux vidéos dont le ministre soutient sans être sérieusement contredit qu'elles ont été tournées en avril 2014 par le requérant lui-même et où figurent plusieurs personnes soupçonnées par les services de renseignement de participer à des filières jihadistes, notamment M.D..., qui apparaît en possession d'une arme de type 22 long rifle avec lunette, et M.F..., par ailleurs assigné à résidence, effectuant une démonstration de " free fight " ; que si le requérant se borne à soutenir que la présence sur les photographies et vidéos saisies d'individus connus pour leur implication dans des filières jihadistes ne suffit pas à établir qu'il les connaîtrait personnellement et que, à supposer que les affirmations du ministre soient exactes, ces liens étaient trop anciens pour démontrer l'engagement actuel du requérant en faveur du jihad, la configuration des lieux en cause comme les actions et attitudes des personnes filmées ou photographiées ainsi que leur présence récurrente sur des enregistrements espacés dans le temps sont de nature à établir, en l'état de l'instruction, l'existence de liens suffisamment étroits et persistants entre le requérant et ces personnes ; <br/>
<br/>
              8. Considérant qu'eu égard à l'ensemble de ces éléments, précis et circonstanciés, et alors que le ministre fait valoir en défense que la menace que représente le requérant doit être appréciée en tenant compte des qualifications militaires qui sont les siennes, notamment pour le maniement des armes et en particulier des armes de précision telle que celle figurant sur les vidéos, il n'apparaît pas, en l'état de l'instruction, qu'en prononçant l'assignation à résidence de M. B... A...au motif qu'il existait de sérieuses raisons de penser que son comportement constitue une menace grave pour la sécurité et l'ordre publics et en fixant les modalités d'exécution de cette mesure, le ministre de l'intérieur ait porté une atteinte grave et manifestement illégale à une liberté fondamentale ;<br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que M. B...A...n'est pas fondé à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Lyon a rejeté sa demande ; que, par suite, son appel ne peut pas être accueilli ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B...A...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. H...B...A...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
