<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029986106</ID>
<ANCIEN_ID>JG_L_2014_12_000000382387</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/98/61/CETATEXT000029986106.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 29/12/2014, 382387</TITRE>
<DATE_DEC>2014-12-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382387</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Anne Egerszegi</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:382387.20141229</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C...D...a demandé au tribunal administratif de Toulon d'annuler les opérations électorales qui se sont déroulées le 23 mars 2014 dans la commune de Fayence en vue de l'élection des conseillers municipaux et communautaires. Par un jugement n° 1401212 du 26 juin 2014, le tribunal administratif de Toulon a rejeté cette protestation.  <br/>
<br/>
              Par une requête et deux mémoires, enregistrés les 8 juillet, 3 septembre et 4 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, M. C...D...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) d'annuler les opérations électorales contestées.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive 94/80/CE du Conseil du 19 décembre 1994 ; <br/>
              - le code électoral ;<br/>
              - la loi organique n° 98-404 du 25 mai 1988 ; <br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Egerszegi, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que le sous-préfet de Draguignan (Var) a refusé, le 6 mars 2014, de délivrer à M.D..., avant le premier tour des élections municipales et communautaires organisé le 23 mars 2014 à Fayence (83440), le récépissé de la déclaration de candidature qu'il avait déposée pour la liste " Préserver Fayence " au motif que l'un des candidats figurant sur cette liste, de nationalité allemande, n'avait pas fourni un bulletin n° 3 du casier judiciaire délivré depuis moins de trois mois, document requis en vertu du 3° de l'article R. 128-1 du code électoral ; que, par un premier jugement du 10 mars 2014, le tribunal administratif de Toulon a rejeté la demande de M. D...dirigée contre le refus de délivrance du récépissé ; que M. D...relève appel du second jugement du 26 juin 2014 par lequel le même tribunal a rejeté sa protestation contre les opérations électorales qui se sont déroulées dans cette commune le 23 mars 2014 ;<br/>
<br/>
              Sur les conclusions de M. D...tendant à l'annulation des opérations électorales : <br/>
<br/>
              En ce qui concerne le grief tiré de l'irrégularité du refus de délivrance du récépissé à la liste " Préserver Fayence " : <br/>
<br/>
              2. Considérant d'une part, qu'aux termes de l'article L. 228 du code électoral : " Nul ne peut être élu conseiller municipal s'il n'est âgé de dix-huit ans révolus./Sont éligibles au conseil municipal tous les électeurs de la commune et les citoyens inscrits au rôle des contributions directes ou justifiant qu'ils devaient y être inscrits au 1er janvier de l'année de l'élection (...) " ; que l'article L. 265 du même code subordonne l'enregistrement d'une liste à la production des documents officiels justifiant que les candidats satisfont aux conditions posées par les deux premiers alinéas de l'article L. 228 précité ; que l'article R. 128 du même code prévoit : "  A la déclaration de candidature en vue du premier tour, il est joint, pour chaque candidat visé à l'article L. 265 : / 1° Si le candidat est électeur dans la commune où il se présente, une attestation d'inscription sur la liste électorale de cette commune (...) ; / 2° Si le candidat est électeur dans une autre commune que celle où il se présente, une attestation d'inscription sur la liste électorale de cette commune (...) ; / 3° Dans les autres cas, un certificat de nationalité, le passeport ou la carte nationale d'identité en cours de validité et un bulletin n° 3 du casier judiciaire délivré depuis moins de trois mois (...) "; <br/>
<br/>
              3. Considérant d'autre part, qu'aux termes de l'article LO. 228-1 du code électoral : " Sont en outre éligibles au conseil municipal ou au conseil de Paris les ressortissants des Etats membres de l'Union européenne autres que la France qui : / a) Soit sont inscrits sur la liste électorale complémentaire de la commune ; / b) Soit remplissent les conditions légales autres que la nationalité française pour être électeurs et être inscrits sur une liste électorale complémentaire en France et sont inscrits au rôle d'une des contributions directes de la commune ou justifient qu'ils devaient y être inscrits au 1er janvier de l'année de l'élection. " ; qu'aux termes de l'article LO. 265-1 du même code : " Chaque fois qu'une liste comporte la candidature d'un ressortissant d'un Etat membre de l'Union européenne autre que la France, la nationalité de celui-ci est portée sur la liste en regard de l'indication de ses nom, prénoms, date et lieu de naissance./ En outre, est exigée de l'intéressé la production : / a) D'une déclaration certifiant qu'il n'est pas déchu du droit d'éligibilité dans l'Etat dont il a la nationalité ; / b) Des documents officiels qui justifient qu'il satisfait aux conditions d'éligibilité posées par l'article LO. 228-1. / En cas de doute sur le contenu de la déclaration visée au a, est exigée, avant ou après le scrutin, la présentation d'une attestation des autorités compétentes de l'Etat dont l'intéressé a la nationalité, certifiant qu'il n'est pas déchu du droit d'éligibilité dans cet Etat ou qu'une telle déchéance n'est pas connue desdites autorités ". ; que l'article R. 128-1 de ce code dispose : " A la déclaration de candidature en vue du premier tour, il est joint, pour chaque candidat visé à l'article LO. 265-1 : / 1° Si le candidat est électeur dans la commune où il se présente, une attestation d'inscription sur la liste électorale complémentaire de cette commune (...) ; / 2° Si le candidat est électeur dans une autre commune que celle où il se présente, une attestation d'inscription sur la liste électorale complémentaire de cette commune (...) ; / 3° Dans les autres cas, une copie de la carte de séjour, du passeport ou de la carte nationale d'identité du candidat, ainsi qu'un bulletin n° 3 du casier judiciaire délivré depuis moins de trois mois (...) " ; <br/>
<br/>
              4. Considérant, en premier lieu, que le Premier ministre avait compétence pour prendre, par décret, les mesures nécessaires à l'application des dispositions de la loi organique du 25 mai 1998 déterminant les conditions d'application de l'article 88-3 de la Constitution relatif à l'exercice par les citoyens de l'Union européenne résidant en France, autres que les ressortissants français, du droit de vote et d'éligibilité aux élections municipales, et portant transposition de la directive 94/80/CE du Conseil du 19 décembre 1994, afin notamment de préciser quels sont les documents officiels mentionnés à l'article LO. 265-1 introduit par cette loi organique dans le code électoral, alors même que cet article ne comporte pas de renvoi à un décret d'application ; qu'ainsi, il était compétent pour arrêter la liste des documents susceptibles d'être exigés du candidat ressortissant d'un Etat membre de l'Union européenne autre que la France pour justifier qu'il satisfait aux conditions d'éligibilité posées par l'article LO. 228-1 du même code ;<br/>
<br/>
              5. Considérant, en deuxième lieu, que l'article LO. 228-1 du code électoral réserve le droit d'être élu au conseil municipal, s'agissant des citoyens de l'Union européenne résidant en France, à ceux qui sont inscrits sur la liste électorale complémentaire de la commune ou au rôle des contributions directes de la commune, lorsqu'ils remplissent les conditions légales autres que la nationalité française pour être électeurs et être inscrits sur une liste électorale complémentaire en France ; qu'à cet égard, l'article L. 2 du même code subordonne la qualité d'électeur à la jouissance des droits civils et politiques ; que par suite, en demandant aux candidats ressortissants de l'Union européenne de produire, lorsque leur qualité d'électeur n'est pas établie par l'inscription sur la liste électorale complémentaire d'une commune, un bulletin nº 3 du casier judiciaire, destiné à prouver qu'ils jouissent en France de leur capacité électorale et d'éligibilité, le 3° de l'article R. 128-1 du code électoral n'a pas ajouté aux exigences prévues par l'article LO. 228-1 ; <br/>
<br/>
              6. Considérant, en troisième lieu, qu'en exigeant des ressortissants d'un Etat membre de l'Union européenne autre que la France la production de documents différents, selon qu'ils sont inscrits ou non sur la liste électorale complémentaire d'une commune française, le 3° de l'article R. 128-1 du code électoral n'a pas porté atteinte à l'égalité entre ces électeurs, qui sont placés dans des situations différentes ; <br/>
<br/>
              7. Considérant, en quatrième lieu, que le 3° de l'article R. 128 du code électoral exige des candidats de nationalité française, lorsque leur qualité de citoyen n'est pas établie par l'inscription sur la  liste électorale d'une commune, la production d'un bulletin nº 3 du casier judiciaire ; qu'ainsi, les dispositions contestées ne méconnaissent pas l'obligation d'égalité de traitement entre les ressortissants de l'Etat de résidence et les citoyens de l'Union résidant dans un État membre dont ils n'ont pas la nationalité, prévue par la directive du 19 décembre 1994 fixant les modalités de l'exercice du droit de vote et d'éligibilité aux élections municipales pour les citoyens de l'Union résidant dans un État membre dont ils n'ont pas la nationalité, dont le 1 de l'article 9 prévoit que " lors du dépôt de sa déclaration de candidature, chaque éligible visé à l'article 3 doit apporter les mêmes preuves qu'un candidat national " ; <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que le requérant n'est pas fondé à soutenir que le refus de délivrance du récépissé à la liste " Préserver Fayence " reposerait sur des dispositions réglementaires illégales ; <br/>
<br/>
              En ce qui concerne le grief tiré de l'atteinte à la confidentialité du vote : <br/>
<br/>
              9. Considérant que la circonstance que la taille des enveloppes ne serait pas adaptée au format et au grammage des bulletins, ce qui permettrait, selon le requérant, d'identifier les électeurs ayant déposé dans l'urne une enveloppe vide, est, en tout état de cause, sans incidence sur la régularité du scrutin, dès lors qu'il n'est pas contesté que ces enveloppes et  bulletins étaient conformes aux prescriptions du code électoral ; que le grief ne peut, par suite, qu'être écarté ; <br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède que M. D...n'est pas fondé à se plaindre de ce que, par le jugement du 26 juin 2014, le tribunal administratif de Toulon a rejeté sa protestation ; <br/>
<br/>
              Sur les conclusions présentées en défense par M.B... : <br/>
<br/>
              11. Considérant, d'une part, qu'aux termes de l'article R. 741-12 du code de justice administrative : " Le juge peut infliger à l'auteur d'une requête qu'il estime abusive une amende dont le montant ne peut excéder 3 000 euros " ; que la faculté ouverte par ces dispositions constituant un pouvoir propre du juge, les conclusions de M. B...tendant à ce que M. D...soit condamné sur le fondement de ces dispositions ne sont pas recevables ;<br/>
<br/>
              12. Considérant, d'autre part, qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. D...est rejetée. <br/>
Article 2 : Les conclusions de M. B...tendant à l'application des articles R. 741-12 et L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à M.D..., à M.B..., à M. A...et au ministre de l'intérieur. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-04-01 ÉLECTIONS ET RÉFÉRENDUM. ÉLECTIONS MUNICIPALES. OPÉRATIONS PRÉLIMINAIRES À L'ÉLECTION. - DÉCLARATION DE CANDIDATURE - DOCUMENTS SUSCEPTIBLES D'ÊTRE EXIGÉS D'UN CANDIDAT RESSORTISSANT D'UN ETAT MEMBRE DE L'UNION EUROPÉENNE AUTRE QUE LA FRANCE POUR JUSTIFIER QU'IL SATISFAIT AUX CONDITIONS D'ÉLIGIBILITÉ (3° DE L'ART. R. 128-1 DU CODE ÉLECTORAL) - 1) COMPÉTENCE DU PREMIER MINISTRE POUR ARRÊTER LA LISTE DE CES DOCUMENTS - EXISTENCE - 2) EXIGENCE QUE LES CANDIDATS RESSORTISSANTS DE L'UNION EUROPÉENNE PRODUISENT, LORSQUE LEUR QUALITÉ D'ÉLECTEUR N'EST PAS ÉTABLIE PAR L'INSCRIPTION SUR LA LISTE ÉLECTORALE COMPLÉMENTAIRE D'UNE COMMUNE, UN BULLETIN Nº 3 DU CASIER JUDICIAIRE - A) AJOUT AUX EXIGENCES PRÉVUES PAR LA LOI ORGANIQUE (ART. LO. 228-1 DU CODE ÉLECTORAL) - ABSENCE - B) DIFFÉRENCE DE TRAITEMENT ENTRE LES RESSORTISSANTS D'UN ETAT MEMBRE DE L'UNION EUROPÉENNE SELON QU'ILS SONT INSCRITS OU NON SUR LA LISTE ÉLECTORALE COMPLÉMENTAIRE D'UNE COMMUNE FRANÇAISE - VIOLATION DU PRINCIPE D'ÉGALITÉ - ABSENCE - C) DIFFÉRENCE DE TRAITEMENT ENTRE LES CANDIDATS RESSORTISSANTS D'UN ETAT MEMBRE DE L'UNION EUROPÉENNE ET LES CANDIDATS DE NATIONALITÉ FRANÇAISE - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 28-04-01 1) Le Premier ministre avait compétence pour prendre, par décret, les mesures nécessaires à l'application des dispositions de la loi organique du 25 mai 1998 déterminant les conditions d'application de l'article 88-3 de la Constitution relatif à l'exercice par les citoyens de l'Union européenne résidant en France, autres que les ressortissants français, du droit de vote et d'éligibilité aux élections municipales, et portant transposition de la directive 94/80/CE du Conseil du 19 décembre 1994, afin notamment de préciser quels sont les documents officiels mentionnés à l'article LO. 265-1 introduit par cette loi organique dans le code électoral, alors même que cet article ne comporte pas de renvoi à un décret d'application. Ainsi, il était compétent pour arrêter, au 3° de l'article R. 128-1 du code électoral, la liste des documents susceptibles d'être exigés du candidat ressortissant d'un Etat membre de l'Union européenne autre que la France pour justifier qu'il satisfait aux conditions d'éligibilité posées par l'article LO. 228-1 du même code.,,,2) a) L'article LO. 228-1 du code électoral réserve le droit d'être élu au conseil municipal, s'agissant des citoyens de l'Union européenne résidant en France, à ceux qui sont inscrits sur la liste électorale complémentaire de la commune ou au rôle des contributions directes de la commune, lorsqu'ils remplissent les conditions légales autres que la nationalité française pour être électeurs et être inscrits sur une liste électorale complémentaire en France. A cet égard, l'article L. 2 du même code subordonne la qualité d'électeur à la jouissance des droits civils et politiques. Par suite, en demandant aux candidats ressortissants de l'Union européenne de produire, lorsque leur qualité d'électeur n'est pas établie par l'inscription sur la liste électorale complémentaire d'une commune, un bulletin nº 3 du casier judiciaire, destiné à prouver qu'ils jouissent en France de leur capacité électorale et d'éligibilité, le 3° de l'article R. 128-1 du code électoral n'a pas ajouté aux exigences prévues par l'article LO. 228-1.,,,b) En exigeant des ressortissants d'un Etat membre de l'Union européenne autre que la France la production de documents différents, selon qu'ils sont inscrits ou non sur la liste électorale complémentaire d'une commune française, le 3° de l'article R. 128-1 du code électoral n'a pas porté atteinte à l'égalité entre ces électeurs, qui sont placés dans des situations différentes.,,,c) Le 3° de l'article R. 128 du code électoral exige des candidats de nationalité française, lorsque leur qualité de citoyen n'est pas établie par l'inscription sur la  liste électorale d'une commune, la production d'un bulletin nº 3 du casier judiciaire. Ainsi, les dispositions du 3° de l'article R. 128-1 du code électoral ne méconnaissent pas l'obligation d'égalité de traitement entre les ressortissants de l'Etat de résidence et les citoyens de l'Union résidant dans un État membre dont ils n'ont pas la nationalité, prévue par la directive du 19 décembre 1994 fixant les modalités de l'exercice du droit de vote et d'éligibilité aux élections municipales pour les citoyens de l'Union résidant dans un État membre dont ils n'ont pas la nationalité.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., en ce qui concerne l'article R. 128 du code électoral, CE, 29 juillet 2002, Elections municipales de Dunkerque (Nord), n°239142, aux Tables sur un autre point.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
