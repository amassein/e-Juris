<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026116819</ID>
<ANCIEN_ID>JG_L_2012_07_000000358262</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/11/68/CETATEXT000026116819.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 02/07/2012, 358262, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-07-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>358262</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Philippe Martin</PRESIDENT>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>M. Didier-Roland Tabuteau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Landais</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:358262.20120702</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 1008793 du 30 mars 2012, enregistrée le 4 avril 2012 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président de la 3ème section du tribunal administratif de Paris, avant qu'il soit statué sur la demande de la société Egilia tendant à l'annulation de la décision implicite par laquelle le préfet de la région Ile-de-France, préfet de Paris, a refusé de faire droit à sa demande tendant à obtenir l'annulation de la décision du 23 novembre 2009 en ce qu'elle conclut au rejet de diverses dépenses au titre de son activité de dispensateur de formation professionnelle et lui prescrit de verser au Trésor public une somme totale de 176 649,57 euros correspondant à l'ensemble de ces dépenses, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des dispositions des articles L. 6362-5, L. 6362-7 et L. 6362-10 du code du travail ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu les articles L. 6362-5, L. 6362-7 et L. 6362-10 du code du travail ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier-Roland Tabuteau, Conseiller d'Etat,  <br/>
<br/>
              - les observations de la SCP Célice, Blancpain, Soltner, avocat de la société Egilia,<br/>
<br/>
              - les conclusions de Mme Claire Landais, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Célice, Blancpain, Soltner, avocat de la société Egilia ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 6362-5 du code du travail : " Les organismes mentionnés à l'article L. 6361-2 sont tenus, à l'égard des agents de contrôle mentionnés à l'article L. 6361-5 : / 1° De présenter les documents et pièces établissant l'origine des produits et des fonds reçus ainsi que la nature et la réalité des dépenses exposées pour l'exercice des activités conduites en matière de formation professionnelle continue ; / 2° De justifier le rattachement et le bien-fondé de ces dépenses à leurs activités ainsi que la conformité de l'utilisation des fonds aux dispositions légales régissant ces activités. / A défaut de remplir ces conditions, les organismes font, pour les dépenses considérées, l'objet de la décision de rejet prévue à l'article L. 6362-10 " ; que lorsqu'une telle décision de rejet est prononcée par l'administration à l'encontre d'un organisme prestataire d'actions de formation dans les conditions prévues à l'article L. 6362-10 du code du travail, l'organisme doit verser au Trésor public, en application de l'article L. 6362-7 du même code, une somme égale au montant des dépenses ayant fait l'objet de la décision de rejet ; que ce versement, qui tend à punir les manquements aux obligations fixées à l'article L. 6362-5 du code du travail, revêt le caractère d'une sanction administrative ;<br/>
<br/>
              3. Considérant que les articles L. 6362-5, L. 6362-7 et L. 6362-10 du code du travail sont applicables au litige dont est saisi le tribunal administratif de Paris ; que ces dispositions n'ont pas déjà été déclarées conformes à la Constitution par le Conseil constitutionnel ; que le moyen tiré de ce qu'elles portent atteinte aux droits et libertés garantis par la Constitution, et notamment au principe de légalité des délits garanti par l'article 8 de la Déclaration des droits de l'homme et du citoyen de 1789, soulève une question présentant un caractère sérieux ; qu'ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité à la Constitution des articles L. 6362-5, L. 6362-7 et L. 6362-10 du code du travail est renvoyée au Conseil constitutionnel.<br/>
Article 2 : La présente décision sera notifiée à la société Egilia et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
Copie en sera adressée au Premier ministre et au tribunal administratif de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
