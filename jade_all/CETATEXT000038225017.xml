<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038225017</ID>
<ANCIEN_ID>JG_L_2019_03_000000419825</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/22/50/CETATEXT000038225017.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 12/03/2019, 419825, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-03-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419825</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE CHAISEMARTIN, DOUMIC-SEILLER</AVOCATS>
<RAPPORTEUR>Mme Cécile Isidoro</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:419825.20190312</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par deux mémoires, enregistrés les 14 décembre 2018 et 25 janvier 2019, M. B... A...demande au Conseil d'Etat, en application de l'article 23-5 de l'ordonnance du 7 novembre 1958 et à l'appui de son pourvoi tendant à l'annulation de l'arrêt n° 17MA01615 du 13 février 2018 par lequel la cour administrative d'appel de Marseille a rejeté l'appel qu'il avait formé contre le jugement n° 1505845 du 20 février 2017 du tribunal administratif de Montpellier ayant rejeté sa demande de décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre des années 2010 et 2011 et des pénalités correspondantes, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article 238 bis M du code général des impôts.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code civil ; <br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - la loi n° 79-1102 du 21 décembre 1979 ;  <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Isidoro, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Chaisemartin, Doumic-Seiller, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garanties par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. M.A..., qui est membre de la société de fait Armement A...Frères, demande au Conseil d'Etat, en application de l'article 23-5 de l'ordonnance du 7 novembre 1958 et à l'appui de son pourvoi, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article 238 bis M du code général des impôts. <br/>
<br/>
              3. Aux termes de l'article 238 bis M du code général des impôts, issu de la loi du 21 décembre 1979 de finances rectificative pour 1979 : " Les sociétés en participation doivent, pour l'application des articles 8 et 60, inscrire à leur actif les biens dont les associés ont convenu de mettre la propriété en commun ". L'article 238 bis L du même code prévoit que les bénéfices réalisés par les sociétés créées de fait sont imposés selon les règles prévues pour les sociétés en participation. <br/>
<br/>
              4. Le second alinéa de l'article 1871 du code civil, applicable aux sociétés de fait, dispose que " Les associés conviennent librement de l'objet du fonctionnement et des conditions de la société en participation (...) ". <br/>
<br/>
              5. L'article 238 bis M du code général des impôts, en imposant aux membres de sociétés de fait qui souhaitent mettre en commun la propriété d'un bien qu'ils détiennent d'inscrire ce bien à l'actif du bilan de la société de fait, ne détermine pas, par lui-même, les modalités selon lesquelles la plus-value résultant de la cession ultérieure de ce bien sera imposée entre les mains des associés. Par suite, il ne saurait être regardé comme faisant peser sur les contribuables des charges excessives au regard de leurs facultés contributives. Il ne méconnaît dès lors pas le principe d'égalité devant les charges publiques.<br/>
<br/>
              6. Les dispositions de l'article 238 bis M ne présentant aucune difficulté particulière d'interprétation, elles ne peuvent être regardées comme une source d'insécurité juridique à raison de leur ambiguïté ou de leur caractère contradictoire ou incompréhensible. Par suite, et en tout état de cause, elles ne méconnaissent pas l'objectif de valeur constitutionnelle d'intelligibilité et d'accessibilité de la loi, qui découle des articles 4, 5, 6 et 16 de la Déclaration des droits de l'homme et du citoyen.  <br/>
<br/>
              7. Il résulte de tout ce qui précède que la question prioritaire de constitutionnalité soulevée, qui n'est pas nouvelle, ne présente pas de caractère sérieux. Par suite, il n'y a pas lieu de la renvoyer au Conseil constitutionnel. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M.A....<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au ministre de l'action et des comptes publics.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
