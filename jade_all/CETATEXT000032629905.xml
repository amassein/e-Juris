<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032629905</ID>
<ANCIEN_ID>JG_L_2014_12_000000364954</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/62/99/CETATEXT000032629905.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème SSJS, 18/12/2014, 364954, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-12-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364954</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Jean-Marie Deligne</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:364954.20141218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
Procédure contentieuse antérieure :<br/>
<br/>
              M. A...a demandé au tribunal administratif de Lyon de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu, et des pénalités correspondantes, auxquelles il a été assujetti au titre des années 1998 et 1999. Par un jugement n° 0302908 du 5 juillet 2005, le tribunal administratif de Lyon a :<br/>
              - par son article 1er, réduit ses bases d'imposition, dans la catégorie des bénéfices industriels et commerciaux, pour chacune des années 1998 et 1999, de la différence entre le montant annuel des redevances retenu par l'administration et la somme annuelle de 342 700 francs ;<br/>
              - par son article 2, prononcé la décharge des impositions, en droits et pénalités, correspondant à cette réduction de base ;<br/>
              - par son article 3, rejeté le surplus des conclusions de sa demande.<br/>
<br/>
              Par un arrêt n° 05LY01544 du 20 avril 2009, la cour administrative d'appel de Lyon a annulé l'article 3 du jugement du tribunal administratif de Lyon et a déchargé M. A... de la totalité des cotisations supplémentaires d'impôt sur le revenu, et des pénalités correspondantes, mises à sa charge au titre des années 1998 et 1999.<br/>
<br/>
              Par une décision n° 329032 du 21 mai 2012 le Conseil d'Etat, statuant au contentieux, a annulé l'arrêt de la cour administrative d'appel de Lyon du 20 avril 2009 et a renvoyé l'affaire à cette cour.<br/>
<br/>
              Par un second arrêt n° 12LY01441 du 6 novembre 2012, la cour administrative d'appel de Lyon a rejeté l'appel formé par M. A...contre le jugement du tribunal administratif de Lyon du 5 juillet 2005. <br/>
<br/>
Procédure devant le Conseil d'Etat :<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 4 janvier et 2 avril 2013 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 12LY01441 du 6 novembre 2012 de la cour administrative d'appel de Lyon ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marie Deligne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M. A...;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'après avoir exploité un garage, à titre individuel, M. A...a, par un acte du 1er avril 1992, donné ce fonds de commerce en location-gérance à la SARLA..., moyennant le versement d'une redevance annuelle, initialement fixée à 360 000 F hors taxes (ht). Le montant de cette redevance a été porté à la somme de 456 000 F ht par un avenant du 31 juillet 1992, puis été successivement réduit à 300 000 F au premier semestre 1994, réévalué à 342 700 F à compter du 1er juillet 1994 et abaissé à 272 700 F en septembre 1996 et à 248 700 F le 27 mars 1997. M. A... a cédé son fonds de commerce à la SARLA..., le 23 décembre 1999, pour un montant de 1 700 000 F ; <br/>
<br/>
              2. A l'occasion de la vérification de comptabilité de son activité de loueur de fonds de commerce, l'administration a estimé que M. A...avait renoncé sans contrepartie à obtenir le paiement de la différence entre la somme de 456 000 F fixée dans l'avenant du 31 juillet 1992 et la redevance effectivement perçue au titre des exercices clos les 31 mars 1998, 31 mars 1999 et 31 décembre 1999 et que les diminutions successives de redevances n'avaient eu aucun autre but que celui de lui permettre de bénéficier de l'exonération de la plus-value réalisée lors de la cession de son fonds de commerce, en décembre 1999, en ramenant son chiffre d'affaires en deçà de la limite fixée par les articles 151 septies et 202 bis du code général des impôts. L'administration lui a, en conséquence, notifié des redressements portant, d'une part, sur la réintégration dans son résultat imposable, au titre des bénéfices industriels et commerciaux des exercices clos en 1998 et 1999, des redevances auxquelles il aurait indûment renoncé et, d'autre part, sur la remise en cause de l'exonération des plus-values constatées lors de la cession du fonds de commerce. La demande de décharge des impositions supplémentaires et des pénalités résultant de ces redressements formée par M. A...a été partiellement accueillie par le tribunal administratif de Lyon qui, par un jugement du 5 juillet 2005, a jugé que l'administration n'établissait l'existence d'une sous-évaluation anormale de la redevance de location-gérance qu'à hauteur de la différence entre le montant de 342 000 ht fixé à compter du 1er juillet 1994 et les sommes réellement perçues postérieurement et a, en conséquence, déchargé M. A...des impositions et pénalités correspondant à la réduction, à due concurrence, de ses bases d'imposition.<br/>
<br/>
              3. Il ressort des écritures présentées par M.A..., tant devant la cour administrative d'appel de Lyon que devant le Conseil d'Etat dans le cadre du pourvoi en cassation formé par le ministre du budget à l'encontre du premier arrêt rendu par la cour, que le requérant contestait, en appel, non seulement les impositions supplémentaires résultant de la remise en cause de l'exonération de la plus-value réalisée en 1999 à l'occasion de la cession du fonds de commerce, laissées à sa charge par le jugement du tribunal administratif, mais également la fraction des impositions résultant du redressement opéré, au titre des années 1998 et 1999, sur le fondement de l'acte anormal de gestion, maintenues à sa charge par ce jugement. Par suite, en jugeant que M. A...ne contestait pas, en appel, " le rejet du surplus de ses conclusions dirigées contre les rehaussements d'imposition établis du fait d'actes anormaux de gestion " et que le litige d'appel ne portait plus que sur l'imposition de la plus-value de cession de son fonds de commerce établie au titre de l'année 1999, la cour a dénaturé les écritures du requérant. Dès lors, son arrêt doit, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, être annulé.<br/>
<br/>
              4. Aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; qu'il incombe, par suite, au Conseil d'Etat, de régler l'affaire au fond.<br/>
<br/>
              Sur la régularité de la procédure :<br/>
<br/>
              5. D'une part, aux termes de l'article L. 64 du livre des procédures fiscales, dans sa rédaction applicable au litige : " Ne peuvent être opposés à l'administration des impôts les actes qui dissimulent la portée véritable d'un contrat ou d'une convention à l'aide de clauses : / (...) qui déguisent soit une réalisation, soit un transfert de bénéfices ou de revenus ; (...). / L'administration est en droit de restituer son véritable caractère à l'opération litigieuse. En cas de désaccord sur les redressements notifiés sur le fondement du présent article, le litige est soumis, à la demande du contribuable, à l'avis du comité consultatif pour la répression des abus de droit (...). ". Aux termes de l'article R. 64-2 du même livre, dans sa rédaction applicable à la présente procédure : " Lorsque l'administration se prévaut des dispositions du deuxième alinéa de l'article L. 64, le contribuable dispose d'un délai de trente jours à compter de la réception de la réponse de l'administration à ses observations pour demander que le litige soit soumis à l'avis du comité consultatif pour la répression des abus de droit. ".<br/>
<br/>
              6. Lorsque l'administration procède à une nouvelle notification de redressements qui se substitue intégralement à la notification initiale, il appartient au contribuable, s'il l'estime utile, de renouveler, dans le délai de trente jours à compter de la réception de la réponse de l'administration à ses observations prévu à l'article R. 64-2 précité du livre des procédures fiscales, sa demande de saisine du comité consultatif pour la répression des abus de droit.<br/>
<br/>
              7. En premier lieu, il résulte de l'instruction que, dans sa réponse du 26 février 2001 aux observations sur  la notification de redressements du 29 janvier 2001, M. A... a, d'une part, demandé la saisine du comité consultatif pour la répression des abus de droit dans l'hypothèse où l'administration maintiendrait les redressements sur le fondement des dispositions de l'article L. 64 du livre des procédures fiscales et, d'autre part, sollicité un entretien avec l'interlocuteur départemental. A la suite de cet entretien, qui s'est déroulé le 10 juillet 2001, l'administration a adressé le 23 juillet 2001 au contribuable une nouvelle notification de redressements se substituant en totalité à celle du 29 janvier 2001 et comportant un descriptif plus précis des faits et des termes de comparaison retenus pour fixer le montant des redevances de location-gérance que M. A...aurait, selon elle, dû percevoir au cours des années d'imposition en litige. Par courrier du 3 août 2001, M. A...a contesté cette seconde notification, et s'est borné à solliciter un entretien avec le directeur régional des impôts et la saisine de la commission départementale des impôts directs et des taxes sur le chiffre d'affaires sur le litige persistant avec l'administration, sans renouveler sa demande de saisine du comité consultatif pour la répression des abus de droit. Par suite, l'administration n'était pas tenue de soumettre le litige à ce comité. Si M. A...soutient que l'administration l'aurait, par son courrier du 23 juillet 2001, " égaré dans l'exercice de ses droits ", il ne ressort pas des termes de ce courrier, dans lequel il lui était indiqué " qu'en cas de désaccord, il avait la possibilité de soumettre ce litige à l'avis du comité consultatif pour la répression des abus de droit ", qu'il aurait été de nature à induire M. A...en erreur. Cette faculté de saisine du comité a d'ailleurs une nouvelle fois été rappelée à M. A...par l'administration dans son courrier du 12 octobre 2001. Par suite, le moyen tiré de ce que la procédure d'imposition aurait été entachée d'irrégularité doit être écarté.<br/>
<br/>
              8. En second lieu, lorsque l'administration entend fonder, au moins en partie, un redressement sur des éléments de comparaison issus de données chiffrées provenant d'autres entreprises, elle doit, pour assurer le caractère contradictoire de la procédure sans méconnaître le secret professionnel protégé par l'article L. 103 du livre des procédures fiscales, désigner nommément ces entreprises, mais ne fournir au contribuable que des moyennes ne lui permettant pas de connaître, fût-ce indirectement, les données propres à chacune d'elles. La notification de redressement du 23 juillet 2001 indiquait au contribuable les nom et adresse des deux entreprises retenues par l'administration comme termes de comparaison, en précisant qu'elles exerçaient leur activité de garage sous forme de location-gérance, les montants moyens de chiffres d'affaires et de bénéfice réalisés en 1998 et 1999, ainsi que le rapport moyen entre le montant de la redevance de location-gérance et le chiffre d'affaires au cours des mêmes années. Ces éléments étaient suffisants pour permettre au requérant de présenter utilement ses observations et la notification de redressement était ainsi, contrairement à ce que soutient celui-ci, suffisamment motivée.  Etait également suffisamment motivée la réponse aux observations du contribuable.<br/>
<br/>
              Sur le bien-fondé des impositions :<br/>
<br/>
              En ce qui concerne les impositions résultant du redressement opéré, en matière de bénéfices industriels et commerciaux, sur le fondement de l'acte anormal de gestion :<br/>
<br/>
              9. Comme il est indiqué au point 2, le tribunal administratif de Lyon a jugé que l'administration n'établissait l'existence d'une sous-évaluation anormale de la redevance de location-gérance qu'à hauteur de la différence entre le montant de 342 000 F ht. fixé à compter du 1er juillet 1994 et les sommes réellement perçues postérieurement. Si M. A...soutient que les diminutions du montant de la redevance résultant des avenants du 30 septembre 1996 et du 27 mars 1997 étaient économiquement justifiées au regard des éléments d'exploitation, il n'en apporte pas la preuve en se bornant à faire état de données comptables propres à l'année 1999. L'administration relève, en effet, d'une part, que les investissements réalisés par la SARL A...ont été réduits à 900 000 F au cours des années 1996 à 1998 alors qu'ils avaient atteint la somme de 2 400 000 F au cours des années 1994 et 1995, d'autre part, que le chiffre d'affaires de la société a progressé de manière notable au cours de la même période, enfin, que la redevance de location-gérance, qui représentait 5,3 % du chiffre d'affaires de la société A...en 1992 ne représentait plus que 1,5 % de ce même chiffre en 1999, alors que les redevances versées par deux entreprises analogues se situaient entre 2,86 % et 3,73 % de leur chiffre d'affaires.  Contrairement à ce que soutient le requérant, il n'y a pas lieu de déduire du chiffre d'affaires pris en compte la part représentée par la nouvelle clientèle développée par le preneur.  Dans ces conditions, eu égard à la communauté d'intérêts existant entre le contribuable et la SARLA..., l'administration doit être regardée comme établissant que la minoration de recettes résultant des avenants des 30 septembre 1996 et 27 mars 1997 a été constitutive d'un acte anormal de gestion. Elle a, dès lors, pu réintégrer les sommes correspondantes au bénéfice imposable de M.A....<br/>
<br/>
              En ce qui concerne les impositions résultant de la remise en cause de l'exonération de la plus-value de cession  :<br/>
<br/>
              10. Aux termes de l'article 151 septies du code général des impôts dans sa rédaction alors en vigueur : " Les plus-values réalisées dans le cadre d'une activité agricole, artisanale, commerciale ou libérale par des contribuables dont les recettes n'excèdent pas le double de la limite du forfait prévu aux articles 64 à 65 A ou des régimes définis aux articles 50-0 et 102 ter, appréciée toutes taxes comprises sont exonérées, à condition que l'activité ait été exercée pendant au moins cinq ans, et que le bien n'entre pas dans le champ d'application du A de l'article 1594-0 G (...) / Lorsque les conditions visées au premier alinéa ne sont pas remplies, il est fait application : - des règles prévues aux articles 150 A à 150 S pour les terrains à bâtir et les terres à usage agricole ou forestier ; - du régime fiscal des plus-values professionnelles prévu aux articles 39 duodecies à 39 quindecies et à 93 quater pour les autres éléments de l'actif immobilisé (...) ". Aux termes de l'article 202 bis du même code : " En cas de cession ou de cessation de l'entreprise, les plus-values mentionnées à l'article 151 septies du présent code ne sont exonérées que si les recettes de l'année de réalisation, ramenées le cas échéant à douze mois, et celles de l'année précédente ne dépassent pas le double des limites du forfait prévu aux articles 64 à 65 A ou des régimes définis aux articles 50-0 et 102 ter, appréciées toutes taxes comprises (...) ".<br/>
<br/>
              11. Ainsi qu'il a été dit au point 9, M. A...n'établit pas que la diminution de la redevance résultant des avenants du 30 septembre 1996 et du 27 mars 1997 aurait été justifiée par des raisons économiques propres à la consistance des éléments loués. L'administration relève, pour sa part, que l'avenant du 27 mars 1997 a permis de fixer la redevance à un montant de 248 700 F ht, soit 299 932 F toutes taxes comprises, un peu inférieur à la limite de 300 000 F prévue par les articles 151 septies et 202 bis du code général des impôts pour bénéficier de l'exonération qu'ils prévoient. Elle établit, ainsi, que les avenants en cause n'ont pu être inspirés par aucun motif autre que celui d'éluder l'impôt et elle a pu, dès lors, à bon droit, les écarter comme ne lui étant pas opposables et soumettre, en conséquence, à l'impôt sur le revenu la plus-value réalisée par M. A... à l'occasion de la cession de son fonds de commerce.<br/>
<br/>
              Sur les pénalités :<br/>
<br/>
              12. Aux termes de l'article 1729 du code général des impôts : " 1. Lorsque la déclaration ou l'acte mentionnés à l'article 1728 font apparaître une base d'imposition ou des éléments servant à la liquidation de l'impôt insuffisants, inexacts ou incomplets, le montant des droits mis à la charge du contribuable est assorti de l'intérêt de retard visé à l'article 1727 et d'une majoration de 40 p. 100 si la mauvaise foi de l'intéressé est établie ou de 80 p. 100 s'il s'est rendu coupable de manoeuvres frauduleuses ou d'abus de droits au sens de l'article L. 64 du livre des procédures fiscales. " ;<br/>
<br/>
              13. Il résulte de ce qui a été dit au point 11 que M. A...a commis un abus de droit, du fait des minorations de recettes résultant des avenants des 30 septembre 1996 et 27 mars 1997. Il n'est, dès lors, pas fondé à demander la décharge de la pénalité de 80 % dont ont été assorties les impositions supplémentaires mises à sa charge.<br/>
<br/>
              14. Il résulte de tout ce qui précède que M. A...n'est pas fondé à soutenir que c'est à tort que, par le jugement du 5 juillet 2005 qu'il attaque, le tribunal administratif de Lyon a rejeté sa demande. Les conclusions de sa requête d'appel tendant à ce qu'une somme soit mise à la charge de l'Etat au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées.<br/>
<br/>
              15. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. A...devant le Conseil d'Etat tendant à ce qu'une somme soit mise à la charge de l'Etat au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 6 novembre 2012 est annulé.<br/>
Article 2 : La requête d'appel de M. A...et le surplus des conclusions de son pourvoi sont rejetés.<br/>
Article 3 : La présente décision sera notifiée à M. B...A...et au ministre des finances et des comptes public.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
