<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027990515</ID>
<ANCIEN_ID>JG_L_2013_09_000000353093</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/99/05/CETATEXT000027990515.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 23/09/2013, 353093</TITRE>
<DATE_DEC>2013-09-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353093</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:353093.20130923</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 3 octobre 2011 et 3 janvier 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme A...B..., demeurant... ; Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0705185 du 29 juillet 2011 par lequel le tribunal administratif de Toulouse a rejeté sa demande tendant à l'annulation des arrêtés du directeur du centre hospitalier universitaire de Toulouse des 27 décembre 2005, 26 juillet 2007 et 25 septembre 2007 ainsi qu'à la reconnaissance de son droit à rémunération intégrale pour la période comprise entre le 2 mai 2005 et la reprise de ses activités professionnelles, au titre des conséquences de l'accident de trajet rattachable au service survenu le 16 avril 2004 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande et d'enjoindre au directeur du centre hospitalier universitaire de Toulouse de prendre une nouvelle décision reconnaissant l'imputabilité au service de son congé maladie pour la période comprise entre le 2 mai 2005 et le 22 août 2007 ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier universitaire de Toulouse le versement d'une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 86-33 du 9 janvier 1986 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Peignot, Garreau, Bauer-Violas, avocat de Mme B...et à la SCP Delaporte, Briard, Trichet, avocat du centre hospitalier universitaire de Toulouse ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B..., fonctionnaire hospitalier, est employée par le centre hospitalier universitaire de Toulouse en qualité d'aide-soignante ; qu'elle a présenté le 30 janvier 2004 un lumbago d'effort reconnu comme un accident de service puis a été victime, le 16 avril 2004, d'un accident de trajet qui a entraîné des contusions musculaires au niveau du mollet droit et une atteinte du rachis cervical impliquant le port d'une minerve pendant quinze jours ; qu'elle a présenté un état dépressif modéré à partir de l'automne 2004 ; qu'elle a dû interrompre ses activités professionnelles, qu'elle n'a reprises que le 23 août 2007 ; que, par un arrêté du 26 juillet 2007, le directeur du centre hospitalier universitaire de Toulouse a reconnu l'imputabilité au service de l'accident de trajet ainsi que de la période de congé de maladie intervenue du 2 mai 2004 au 12 avril 2005 ; que, par trois arrêtés en date des 27 décembre 2005, 27 juillet 2006 et 25 septembre 2007, il a placé l'intéressée en congé de maladie ordinaire du 2 mai 2005 au 1er mai 2006, puis en disponibilité d'office pour maladie du 2 mai 2006 au 22 août 2007 ; que Mme B...se pourvoit en cassation contre le jugement du 29 juillet 2011 par lequel le tribunal administratif de Toulouse a rejeté sa demande tendant à l'annulation de ces arrêtés et à ce que le droit à bénéficier de l'intégralité de son traitement lui soit reconnu pour la période comprise entre le 2 mai 2005 et le 22 août 2007 ; <br/>
<br/>
              2. Considérant qu'aux termes du deuxième alinéa du 2° de l'article 41 de la loi du 9 janvier 1986 portant dispositions statutaires relatives à la fonction publique hospitalière : " (...) si la maladie provient (...) d'un accident survenu dans l'exercice ou à l'occasion de ses fonctions, le fonctionnaire conserve l'intégralité de son traitement jusqu'à ce qu'il soit en état de reprendre son service ou jusqu'à sa mise à la retraite. Il a droit, en outre, au remboursement des honoraires médicaux et des frais directement entraînés par la maladie ou l'accident " ; que le droit, prévu par ces dispositions, de conserver l'intégralité du traitement est soumis à la condition que la maladie mettant l'intéressé dans l'impossibilité d'accomplir son service soit en lien direct, mais non nécessairement exclusif, avec un accident survenu dans l'exercice ou à l'occasion de ses fonctions ; <br/>
<br/>
              3. Considérant que, pour rejeter les conclusions de Mme B...dirigées contre les arrêtés du directeur du CHU de Toulouse lui refusant le bénéfice de ces dispositions pour la période comprise entre le 2 mai 2005 et le 22 août 2007, le tribunal administratif de Toulouse s'est fondé sur le motif tiré de ce que l'état pathologique de l'intéressée ne pouvait alors être regardé comme directement et exclusivement imputable à l'accident de service dont elle avait  été victime le 16 avril 2004 ; qu'en exigeant que soit établi un lien non seulement direct mais aussi exclusif entre l'état pathologique de l'agent et l'accident de trajet du 16 avril 2004, le tribunal administratif de Toulouse a commis une erreur de droit ; que, dès lors, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, Mme B...est fondée à demander l'annulation de son jugement ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier que l'état dépressif qu'a connu Mme B..., revêtant un caractère réactionnel, a été causé par les accidents des 30 janvier et 16 avril 2004 et par les difficultés administratives consécutives à ces accidents, tenant essentiellement au retard avec lequel un poste de travail adapté à son état de santé a pu lui être proposé, alors qu'elle avait à plusieurs reprises demandé à reprendre ses activités professionnelles ; que, dans ces conditions, l'interruption du service de l'intéressée entre le 2 mai 2005 et le 22 août 2007  doit être regardée comme étant en lien direct avec des accidents subis en service ; que, dès lors, le bénéfice des dispositions précitées du deuxième alinéa du 2° de l'article 41 de la loi du 9 janvier 1986 ne pouvait lui être légalement refusé au titre de cette période ; qu'il suit de là que Mme B...est fondée à demander l'annulation des arrêtés du directeur du centre hospitalier universitaire de Toulouse  des 27 décembre 2005, 27 juillet 2006 et 25 septembre 2007 ;<br/>
<br/>
              6. Considérant que l'exécution de la présente décision implique nécessairement que la demande de Mme B...tendant à ce qu'elle bénéficie des dispositions du deuxième alinéa du 2° de l'article 41 de la loi du 9 janvier 1986 pour la période du 2 mai 2005 au 22 août 2007 soit accueillie ; qu'il y a lieu, par suite, d'enjoindre au directeur du centre hospitalier universitaire de Toulouse de prendre, au titre de cette période, une décision reconnaissant l'imputabilité au service de l'invalidité de Mme B...et la plaçant en congé pour maladie imputable au service ;  <br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de Mme B... qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier universitaire de Toulouse la somme de 3 500 euros à verser à MmeB..., au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement n° 0705185 du tribunal administratif de Toulouse du 29 juillet 2011 est annulé.<br/>
<br/>
Article 2 : Les arrêtés du directeur du centre hospitalier universitaire de Toulouse des 27 décembre 2005, 27 juillet 2006 et 25 septembre 2007 sont annulés.<br/>
<br/>
Article 3 : Il est enjoint au directeur du centre hospitalier universitaire de Toulouse de prendre une décision reconnaissant l'imputabilité au service de l'invalidité de Mme B...pendant la période comprise entre le 2 mai 2005 et le 22 août 2007 et la plaçant, pour cette période, en congé pour maladie imputable au service. <br/>
<br/>
Article 4 : Le centre hospitalier universitaire de Toulouse versera à Mme B...une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : Les conclusions  du centre hospitalier universitaire de Toulouse présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 6 : La présente décision sera notifiée à Mme A... B...et au centre hospitalier universitaire de Toulouse.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-05-04-01-03 FONCTIONNAIRES ET AGENTS PUBLICS. POSITIONS. CONGÉS. CONGÉS DE MALADIE. ACCIDENTS DE SERVICE. - FONCTION PUBLIQUE HOSPITALIÈRE - DROIT À CONSERVER L'INTÉGRALITÉ DU TRAITEMENT EN CAS DE MALADIE PROVENANT D'UN ACCIDENT DE SERVICE - CONDITION - LIEN DIRECT DE LA MALADIE AVEC L'ACCIDENT - EXISTENCE - CARACTÈRE EXCLUSIF DE CE LIEN - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-11 FONCTIONNAIRES ET AGENTS PUBLICS. DISPOSITIONS PROPRES AUX PERSONNELS HOSPITALIERS. - CONGÉS DE MALADIE - DROIT À CONSERVER L'INTÉGRALITÉ DU TRAITEMENT EN CAS DE MALADIE PROVENANT D'UN ACCIDENT DE SERVICE - CONDITION - LIEN DIRECT DE LA MALADIE AVEC L'ACCIDENT - EXISTENCE - CARACTÈRE EXCLUSIF DE CE LIEN - ABSENCE.
</SCT>
<ANA ID="9A"> 36-05-04-01-03 Le droit, prévu par les dispositions du deuxième alinéa du 2° de l'article 41 de la loi n° 86-33 du 9 janvier 1986 portant dispositions statutaires relatives à la fonction publique hospitalière, d'un fonctionnaire hospitalier en congé de maladie à conserver l'intégralité de son traitement en cas de maladie provenant d'un accident survenu dans l'exercice ou à l'occasion de ses fonctions est soumis à la condition que la maladie mettant l'intéressé dans l'impossibilité d'accomplir son service soit en lien direct, mais non nécessairement exclusif, avec un accident survenu dans l'exercice ou à l'occasion de ses fonctions.</ANA>
<ANA ID="9B"> 36-11 Le droit, prévu par les dispositions du deuxième alinéa du 2° de l'article 41 de la loi n° 86-33 du 9 janvier 1986 portant dispositions statutaires relatives à la fonction publique hospitalière, d'un fonctionnaire hospitalier en congé de maladie à conserver l'intégralité de son traitement en cas de maladie provenant d'un accident survenu dans l'exercice ou à l'occasion de ses fonctions est soumis à la condition que la maladie mettant l'intéressé dans l'impossibilité d'accomplir son service soit en lien direct, mais non nécessairement exclusif, avec un accident survenu dans l'exercice ou à l'occasion de ses fonctions.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
