<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000018007241</ID>
<ANCIEN_ID>JG_L_2007_09_000000301145</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/18/00/72/CETATEXT000018007241.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 26/09/2007, 301145</TITRE>
<DATE_DEC>2007-09-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>301145</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Delarue</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>Mme Catherine  Meyer-Lereculeur</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Prada Bordenave</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 1er février 2007 au secrétariat du contentieux du Conseil d'Etat, présentée par M. Rachid A, demeurant ...; M. A demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le décret du 23 décembre 2006 prononçant sa déchéance de la nationalité française ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat les frais exposés par lui et non compris dans les dépens en application de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales du 4 novembre 1950 ;<br/>
<br/>
              Vu le code civil ;<br/>
<br/>
              Vu le code pénal ;<br/>
<br/>
              Vu le décret n° 93-1362 du 30 décembre 1993, modifié ; <br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Meyer-Lereculeur, chargée des fonctions de Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Prada Bordenave, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes de l'article 25 du code civil : « L'individu qui a acquis la qualité de Français peut, par décret pris après avis conforme du Conseil d'Etat, être déchu de la nationalité française, sauf si la déchéance a pour résultat de le rendre apatride : 1°) S'il est condamné pour un acte qualifié de crime ou délit constituant une atteinte aux intérêts fondamentaux de la Nation ou pour un crime ou un délit constituant un acte de terrorisme...(...) » ; qu'aux termes de l'article 25-1 du même code : « La déchéance n'est encourue que si les faits reprochés à l'intéressé et visés à l'article 25 se sont produits antérieurement à l'acquisition de la nationalité française ou dans le délai de dix ans à compter de la date de cette acquisition. / Elle ne peut être prononcée que dans le délai de dix ans à compter de la perpétration desdits faits. / Si les faits reprochés à l'intéressé sont visés au 1° de l'article 25, les délais mentionnés aux deux alinéas précédents sont portés à quinze ans » ; qu'aux termes de l'article 421-2-1 du code pénal : « Constitue également un acte de terrorisme le fait de participer à un groupement formé ou à une entente établie en vue de la préparation, caractérisée par un ou plusieurs faits matériels, d'un des actes de terrorisme mentionnés aux articles précédents » ; <br/>
<br/>
              Considérant que M. A demande l'annulation du décret du 23 décembre 2006 prononçant sa déchéance de la nationalité française ; <br/>
<br/>
              Sur la légalité externe : <br/>
<br/>
              Considérant que l'ampliation du décret attaqué, qui n'avait pas à comporter la signature manuscrite du Premier ministre, est revêtue de la certification du secrétariat général du gouvernement qui suffit à établir la régularité du décret ; que, par suite, le moyen tiré de l'illégalité externe doit être écarté ; <br/>
<br/>
              Sur la légalité interne : <br/>
<br/>
              Considérant qu'il ressort des pièces du dossier que M. A, né le 21 avril 1969 en Algérie, a acquis la nationalité française par mariage en 1998 ; que, par jugement du 15 mars 2005, devenu définitif, du tribunal de grande instance de Paris, M. A a été condamné à une peine de trois ans d'emprisonnement et à la privation, pour une durée de dix ans, de tous ses droits civiques, civils et de famille, pour des faits dont il s'était rendu coupable entre 1999 et 2001, de participation à une association de malfaiteurs en vue de la préparation d'un acte de terrorisme, faits réprimés notamment par l'article 421-2-1 précité du code pénal, et constituant un acte de terrorisme au sens des dispositions précitées du 1° de l'article 21 précité du code civil ; que M. A a été déchu de la nationalité française, par décret du 26 décembre 2006, sur le fondement des dispositions précitées des articles 25 et 25-1 du code civil ; que, par suite, le moyen tiré par M. A de ce que la condamnation pénale de trois ans d'emprisonnement, qui a été prononcée à son encontre, démontrerait le caractère relatif de son implication dans les faits de terrorisme qui lui étaient reprochés, doit être écarté ; <br/>
<br/>
              Considérant que M. A soutient qu'il est susceptible de faire l'objet, du fait du décret prononçant sa déchéance de la nationalité française, d'une mesure de reconduite à la frontière qui le priverait de tout lien avec son épouse et ses enfants, portant ainsi atteinte à leur droit au respect de leur vie familiale ; que, toutefois, un décret portant déchéance de la nationalité française est dépourvu d'effet sur la présence sur le territoire français de celui qu'il vise, comme sur les liens de ce dernier avec les membres de sa famille ; qu'ainsi, les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ne peuvent être utilement invoquées à l'appui de conclusions dirigées contre ce décret ; que, par suite, le moyen tiré de la violation de ces stipulations doit être écarté ; <br/>
<br/>
              Considérant qu'il résulte de l'ensemble de ce qui précède qu'en estimant, d'une part, que les conditions fixées par les articles 25 et 25-1 du code civil permettant de déchoir M. A de la nationalité française étaient réunies, d'autre part, qu'aucun élément d'opportunité n'était de nature à y faire obstacle, dans les circonstances de l'espèce, le gouvernement n'a pas fait une inexacte application des dispositions précitées du code civil ; que, par suite, M. A n'est pas fondé à demander l'annulation du décret du 23 décembre 2006 prononçant sa déchéance de la nationalité française ; que, dès lors, ses conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative doivent être rejetées ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. Rachid A, au ministre de l'immigration, de l'intégration, de l'identité nationale et du codéveloppement et au ministre du travail, des relations sociales et de la solidarité.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-01-01-02-02 DROITS CIVILS ET INDIVIDUELS. ÉTAT DES PERSONNES. NATIONALITÉ. EFFETS DE L'ACQUISITION ET DE LA PERTE DE LA NATIONALITÉ. EFFETS DE LA PERTE DE LA NATIONALITÉ. - EFFET SUR LA VIE FAMILIALE DE L'INTÉRESSÉ - ABSENCE - CONSÉQUENCE - CARACTÈRE INOPÉRANT DU MOYEN TIRÉ DE LA MÉCONNAISSANCE DE L'ARTICLE 8 DE LA CEDH À L'ENCONTRE D'UN DÉCRET PORTANT DÉCHÉANCE DE LA NATIONALITÉ FRANÇAISE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-055-01-08-01 DROITS CIVILS ET INDIVIDUELS. CONVENTION EUROPÉENNE DES DROITS DE L'HOMME. DROITS GARANTIS PAR LA CONVENTION. DROIT AU RESPECT DE LA VIE PRIVÉE ET FAMILIALE (ART. 8). CHAMP D'APPLICATION. - EXCLUSION - DÉCRET PORTANT DÉCHÉANCE DE LA NATIONALITÉ FRANÇAISE.
</SCT>
<ANA ID="9A"> 26-01-01-02-02 Le moyen tiré de la méconnaissance de l'article 8 de la Convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales est inopérant à l'encontre d'un décret portant déchéance de la nationalité française, qui n'a, par lui-même, aucun effet sur les liens entre la personne déchue de sa nationalité et sa famille.</ANA>
<ANA ID="9B"> 26-055-01-08-01 Le moyen tiré de la méconnaissance de l'article 8 de la Convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales est inopérant à l'encontre d'un décret portant déchéance de la nationalité française, qui n'a, par lui-même, aucun effet sur les liens entre la personne déchue de sa nationalité et sa famille.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
