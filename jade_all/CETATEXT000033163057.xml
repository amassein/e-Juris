<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033163057</ID>
<ANCIEN_ID>JG_L_2016_09_000000394176</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/16/30/CETATEXT000033163057.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 28/09/2016, 394176, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-09-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394176</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Luc Briand</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:394176.20160928</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Versailles d'annuler pour excès de pouvoir l'arrêté du 11 septembre 2013 par lequel le préfet de l'Essonne a rejeté sa demande de titre de séjour, lui a fait obligation de quitter le territoire français et a fixé le pays à destination duquel il pourrait être reconduit d'office à la frontière. Par un jugement n° 1306155 du 16 octobre 2014, le tribunal administratif a annulé cet arrêté et enjoint au préfet de délivrer le titre de séjour sollicité.<br/>
<br/>
              Par un arrêt n° 14VE03146 du 21 juillet 2015, la cour administrative d'appel de Versailles a, sur appel du préfet de l'Essonne, annulé ce jugement et rejeté la demande de M. A....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 21 octobre 2015 et 21 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Luc Briand, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. A...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que, selon les dispositions de l'article R. 811-13 du code de justice administrative, l'introduction de l'instance devant le juge d'appel suit les règles relatives à l'introduction de l'instance en premier ressort ; qu'aux  termes de l'article R. 411-1 du code de justice administrative : " La juridiction est saisie par requête. La requête (...) contient l'exposé des faits et moyens, ainsi que l'énoncé des conclusions soumises au juge. / L'auteur d'une requête ne contenant l'exposé d'aucun moyen ne peut la régulariser par le dépôt d'un mémoire exposant un ou plusieurs moyens que jusqu'à l'expiration du délai de recours " ; qu'aux termes de l'article R. 776-9 du même code, applicable au contentieux des obligations de quitter le territoire, " Le délai d'appel est d'un mois. Il court à compter du jour où le jugement a été notifié à la partie intéressée " ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier des juges du fond que le jugement du 16 octobre 2014 par lequel le tribunal administratif de Versailles a fait droit à la demande de M. A...a été notifié le 22 octobre 2014 au préfet de l'Essonne ; que la " déclaration d'appel " formée par ce dernier, enregistrée le 13 novembre 2014 au greffe de la cour administrative de Versailles, ne contenait l'exposé d'aucun moyen ; que le mémoire complémentaire produit par le préfet, comportant l'exposé de moyens, n'a été enregistré au greffe de la cour administrative d'appel que le 23 décembre 2014, soit après l'expiration du délai d'appel ; qu'ainsi la requête d'appel du préfet était irrecevable ; que, par suite, la cour administrative d'appel a commis une erreur de droit en y faisant droit ; que M. A...est, dès lors, et sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, fondé à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              4. Considérant que, ainsi qu'il vient d'être dit, la requête d'appel du préfet de l'Essonne, qui n'a pas été motivée avant l'expiration du délai d'appel, n'est pas recevable ; qu'elle ne peut, dès lors, qu'être rejetée ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M.A..., au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 21 juillet 2015 est annulé.<br/>
<br/>
Article 2 : La requête présentée par le préfet de l'Essonne devant la cour administrative d'appel de Versailles est rejetée. <br/>
<br/>
Article 3 : L'Etat versera à M. A...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. B...A...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
