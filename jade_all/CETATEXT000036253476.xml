<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036253476</ID>
<ANCIEN_ID>JG_L_2017_12_000000406382</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/25/34/CETATEXT000036253476.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème chambre jugeant seule, 22/12/2017, 406382, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>406382</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème chambre jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Jean-Yves Ollier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:406382.20171222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...F...C...et Mme B...C..., agissant en leur nom propre et au nom de leurs trois enfants mineurs, ont demandé au juge des référés du tribunal administratif de Nantes, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de la décision implicite de rejet résultant du silence conservé par la commission des recours contre les décisions de refus de visa d'entrée en France sur leur contestation de la décision de refus de visa opposée par les services de l'ambassade de France en Afghanistan, et d'enjoindre à l'administration de réexaminer leur demande de visa dans un délai de quinze jours à compter de la notification de l'ordonnance, sous astreinte journalière de 50 euros. <br/>
<br/>
              Par une ordonnance n° 1609200 du 22 novembre 2016, le juge des référés du tribunal administratif de Nantes a rejeté leur demande. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 28 décembre 2016, 12 janvier, 21 août et 29 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, M. et Mme C...demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à leur demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Yves Ollier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. et MmeC....<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que M. A...F...C..., ressortissant afghan, a été, entre mai 2002 et juillet 2012, interprète auprès des forces armées françaises alors déployées en Afghanistan ; que les autorités françaises ont annoncé au mois de mai 2012 le retrait des forces françaises d'Afghanistan à partir du mois de juillet ; que M. C...a déposé le 16 août 2015 une demande de visa pour lui-même, pour son épouse Mme B...C...et pour leurs enfants mineursD..., E...et A...auprès de l'ambassade de France en Afghanistan ; qu'un refus de visa lui ayant été opposé le 7 mai 2016, il a saisi le 29 août suivant la commission de recours contre les décisions de refus de visa d'entrée en France ; que leur demande ayant fait l'objet d'une décision implicite de rejet, M. et Mme C...ont formé un recours contre ce refus de visa devant le tribunal administratif de Nantes ; qu'ils ont parallèlement demandé au juge des référés de ce tribunal, sur le fondement de l'article L. 521-1 du code de justice administrative, d'en suspendre l'exécution ; que, par une ordonnance du 22 novembre 2016, le juge des référés a rejeté cette demande de suspension, en estimant qu'aucun des moyens soulevés par M. et Mme C...ne paraissait de nature, en l'état de l'instruction, à faire naître un doute sérieux quant à la légalité de la décision contestée ; que M. et Mme C...se pourvoient en cassation contre cette ordonnance ; <br/>
<br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              3. Considérant que, pour rejeter la demande de suspension que lui soumettaient M. et MmeC..., le juge des référés du tribunal administratif de Nantes a estimé qu'aucun des moyens qu'ils avaient soulevés ne paraissait de nature à faire naître un doute sérieux quant à la légalité de la décision de refus de visa, notamment celui tiré de l'erreur manifeste d'appréciation des conséquences de ce refus ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que M. C...a servi pendant plus de dix ans en qualité d'interprète auprès des forces françaises au sein de centres de formation et d'instruction des militaires afghans à Kaboul dans le cadre de l'opération Epidote ; qu'il a participé à des opérations sur le terrain avec les forces françaises dans la province de Kaboul ; qu'il fait valoir qu'il a fait l'objet de menaces de la part des talibans depuis la fin de son contrat et que, notamment, son véhicule a été attaqué ; que, dans les circonstances de l'espèce, alors que la situation en Afghanistan s'est dégradée avec une recrudescence des violences qui exposent à des risques élevés les ressortissants afghans qui ont accordé leur concours à des forces armées étrangères, le juge des référés du tribunal administratif a dénaturé les faits de l'espèce en estimant que le moyen tiré de ce que la commission de recours contre les décisions de refus de visa d'entrée en France aurait commis une erreur manifeste d'appréciation sur la réalité des risques courus par les requérants et leur famille n'était pas de nature à faire naître un doute sérieux quant à la légalité de la décision contestée ; que, dès lors, sans qu'il soit besoin d'examiner les autres moyens de leur pourvoi, M. et Mme C...sont fondés à demander l'annulation de l'ordonnance qu'ils attaquent ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée, en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Sur l'intervention :<br/>
<br/>
              6. Considérant que la Ligue des droits de l'homme justifie, eu égard à la nature et à l'objet du litige, d'un intérêt suffisant pour intervenir au soutien de la demande de suspension ; que son intervention est, par suite, recevable ;<br/>
<br/>
              Sur la demande de suspension :<br/>
<br/>
              7. Considérant, d'une part, que l'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'en l'espèce, il ressort des éléments versés au dossier que la condition d'urgence, eu égard aux risques dont fait état M. C...pour lui et sa famille, doit être regardée comme remplie ; <br/>
<br/>
              8. Considérant, d'autre part, que le moyen tiré de l'erreur manifeste dont serait entachée la décision de refus de visa contestée, eu égard aux risques courus par l'intéressé et sa famille du fait des missions accomplies, est, en l'état de l'instruction, propre à créer un doute sérieux sur sa légalité ; <br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que M. et Mme C...sont fondés à demander la suspension de l'exécution de la décision implicite née le 29 octobre 2016 de la commission de recours contre les décisions de refus de visa d'entrée en France ; qu'il y a lieu de prononcer l'injonction sollicitée par M. et Mme C...et de prescrire à l'autorité administrative de réexaminer la demande de visa de M. et Mme C...et de leurs enfants dans un délai d'un mois à compter de la notification de la présente décision, sans qu'il soit nécessaire de prononcer une astreinte ;<br/>
<br/>
              10. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 4 000 euros à verser à M. et Mme C...au titre, pour la première instance et la procédure devant le Conseil d'Etat, de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 22 novembre 2016 du juge des référés du tribunal administratif de Nantes est annulée. <br/>
Article 2 : L'intervention de la Ligue des droits de l'homme est admise.<br/>
Article 3 : L'exécution de la décision implicite née le 29 octobre 2016 de la commission de recours contre les décisions de refus de visa d'entrée en France est suspendue. <br/>
Article 4 : Il est enjoint au ministre d'Etat, ministre de l'intérieur, de réexaminer la demande de visa de M. et Mme C...et de leurs enfants dans le délai d'un mois à compter de la présente décision. <br/>
Article 5 : L'Etat versera à M. et Mme C...une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 6 : La présente décision sera notifiée à M. A...F...C..., à Mme B...C..., à la Ligue des droits de l'homme et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
