<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033308582</ID>
<ANCIEN_ID>JG_L_2016_10_000000392668</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/30/85/CETATEXT000033308582.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème chambre jugeant seule, 21/10/2016, 392668, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-10-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392668</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème chambre jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BOUTHORS ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Frédéric Dieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:392668.20161021</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Tempeol a saisi la cour administrative d'appel de Versailles, sur le fondement de l'article L. 911-4 du code de justice administrative, d'une demande tendant à l'exécution de l'arrêt n° 10VE03203 du 29 novembre 2012 par lequel la cour administrative d'appel de Versailles a, en premier lieu, ramené à 22 269,52 euros la somme que le tribunal administratif de Cergy-Pontoise a condamné l'Opérateur du patrimoine et des projets immobiliers de la culture (OPPIC) à payer à la société Tempeol, venant aux droits de la société Chauffage et Entretien, au titre des travaux supplémentaires exécutés dans le cadre de deux marchés publics de travaux conclus avec l'établissement les 18 et 25 février 2002, en deuxième lieu, porté à 173 052,82 euros la somme allouée à la société au titre de l'allongement du délai de chantier, somme augmentée des intérêts moratoires à compter du 14 septembre 2004 et de la capitalisation des intérêts à compter du 19 novembre 2007 et, en dernier lieu, déchargé la société Tempeol des pénalités de retard infligées par le maître de l'ouvrage à hauteur de 58 266 euros.<br/>
<br/>
              Par un arrêt n° 15VE00447 du 11 juin 2015, la cour administrative d'appel de Versailles a, d'une part, jugé que l'OPPIC devait verser à la société Tempeol, en exécution de l'arrêt du 29 novembre 2012, outre les sommes déjà attribuées, le montant des intérêts au taux légal portant sur la somme de 22 269,52 euros à compter du 13 décembre 2005 et la capitalisation de ces intérêts à compter du 19 novembre 2007 ainsi que d'une majoration de deux points des intérêts moratoires calculés par application des arrêtés des 17 janvier 1991, 17 décembre 1993 et 31 mai 1997, sur la somme de 62 074,81 euros, entre le 23 juillet 2013 et le 10 octobre 2013, et des intérêts moratoires complémentaires calculés sur les intérêts dus au titre des travaux supplémentaires versés en exécution du jugement et, d'autre part, rejeté le surplus des conclusions de la société Tempeol.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux mémoires en réplique, enregistrés les 14 août et 13 novembre 2015, 14 mars et 9 août 2016 au secrétariat du contentieux du Conseil d'Etat, la société Tempeol demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande d'exécution ;<br/>
<br/>
              3°) de mettre à la charge de l'OPPIC la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des marchés publics ;<br/>
              - le décret n° 2001-210 du 7 mars 2001 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Dieu, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Bouthors, avocat de la société Tempeol et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de l'Opérateur du patrimoine et des projets immobiliers de la culture ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un jugement du 27 juillet 2010, le tribunal administratif de Cergy-Pontoise a condamné l'opérateur du patrimoine et des projets immobiliers de la culture (OPPIC) à payer à la société Chauffage et Entretien, aux droits de laquelle est venue la société Tempeol, une somme de 85 101,38 euros, soit 23 322 euros au titre des travaux supplémentaires et 61 779,38 euros au titre de l'allongement de la durée du chantier, somme portant intérêt au taux légal à compter du 13 décembre 2005, avec capitalisation des intérêts échus à la date du 19 novembre 2007 ; que, par un arrêt du 29 novembre 2012, la cour administrative d'appel de Versailles a ramené à 22 269,52 euros le montant alloué par le jugement du tribunal administratif au titre des travaux supplémentaires, a porté à 173 052,82 euros le montant alloué au titre de l'allongement du délai de chantier en l'assortissant des intérêts moratoires à compter du 14 septembre 2004 et de la capitalisation des intérêts à compter du 19 novembre 2007 ; que, par l'arrêt attaqué, la cour administrative d'appel de Versailles, saisie sur le fondement de l'article L. 911-4 du code de justice administrative d'une demande d'exécution de l'arrêt du 29 novembre 2012, a, d'une part, condamné l'OPPIC à verser à la société Tempeol le montant des intérêts au taux légal portant sur la somme de 22 269,52 euros à compter du 13 décembre 2005 et la capitalisation de ces intérêts à compter du 19 novembre 2007 ainsi qu'une majoration de deux points des intérêts moratoires calculés par application des arrêtés des 17 janvier 1991, 17 décembre 1993 et 31 mai 1997, sur la somme de 62 074,81 euros, entre le 23 juillet 2013 et le 10 octobre 2013, et des intérêts moratoires complémentaires calculés sur les intérêts dus au titre des travaux supplémentaires versés en exécution du jugement et, d'autre part, rejeté le surplus des conclusions de la société Tempeol ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 911-1 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution " ; qu'aux termes de l'article L. 911-4 du même code : " En cas d'inexécution d'un jugement ou d'un arrêt, la partie intéressée peut demander au tribunal administratif ou à la cour administrative d'appel qui a rendu la décision d'en assurer l'exécution. / (...) Si le jugement ou l'arrêt dont l'exécution est demandée n'a pas défini les mesures d'exécution, la juridiction saisie procède à cette définition. Elle peut fixer un délai d'exécution et prononcer une astreinte. / Le tribunal administratif ou la cour administrative d'appel peut renvoyer la demande d'exécution au Conseil d'Etat " ; <br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions qu'en l'absence de définition, par le jugement ou l'arrêt dont l'exécution lui est demandée, des mesures qu'implique nécessairement cette décision, il appartient au juge saisi sur le fondement de l'article L. 911-4 du code de justice administrative d'y procéder lui-même en tenant compte des situations de droit et de fait existant à la date de sa décision ; que, si la décision faisant l'objet de la demande d'exécution prescrit déjà de telles mesures en application de l'article L. 911-1 du même code, il peut, dans l'hypothèse où elles seraient entachées d'une obscurité ou d'une ambigüité, en préciser la portée ; que, le cas échéant, il lui appartient aussi d'en édicter de nouvelles en se plaçant, de même, à la date de sa décision, sans toutefois pouvoir remettre en cause celles qui ont précédemment été prescrites ni méconnaître l'autorité qui s'attache aux motifs qui sont le soutien nécessaire du dispositif de la décision juridictionnelle dont l'exécution lui est demandée ; qu'en particulier, la rectification des erreurs de droit ou de fait dont serait entachée la décision en cause ne peut procéder que de l'exercice, dans les délais fixés par les dispositions applicables, des voies de recours ouvertes contre cette décision ;<br/>
<br/>
              4. Considérant qu'il ressort des motifs de l'arrêt du 29 novembre 2012 que la cour administrative d'appel de Versailles, en se référant à l'article 96 du code des marchés publics, dans sa rédaction issue du décret du 7 mars 2001, a seulement entendu affirmer le principe de l'application d'intérêts moratoires en cas de dépassement du délai imparti à la personne publique pour s'acquitter des sommes dues en exécution d'un marché public ; que, dans ce même arrêt, la cour n'a pas fondé le mode de calcul des intérêts moratoires sur cet article, que le décret du 21 février 2002 a rendu applicable aux marchés dont la procédure de consultation avait été engagée ou l'avis d'appel public à la concurrence avait été envoyé à la publication postérieurement au 1er mars 2002 ; que, par suite, en retenant, d'une part, que les dispositions de l'article 96 du code des marchés publics, dans sa rédaction issue du décret du 7 mars 2001, n'étaient pas applicables aux lots litigieux dès lors que ces lots avaient été attribués les 18 et 25 février 2002 et, d'autre part, que le calcul des intérêts moratoires dus par l'OPPIC en exécution de l'arrêt du 29 novembre 2012 devait être effectué sur le fondement de l'article 178 du code des marchés publics, dans sa rédaction antérieure au code issu du décret du 7 mars 2001, et des arrêtés des 17 janvier 1991, 17 décembre 1993 et 31 mai 1997, la cour administrative d'appel de Versailles n'a pas méconnu l'autorité de la chose jugée par l'arrêt du 29 novembre 2012 ni procédé à la rectification d'une erreur qui aurait entaché cet arrêt, mais a seulement précisé la portée de cet arrêt qui était, sur ce point, entaché d'une ambigüité ; que la cour, dont l'arrêt est suffisamment motivé, n'a ainsi commis aucune erreur de droit ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que le pourvoi de la société Tempeol doit être rejeté ;<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de l'Opérateur du patrimoine et des projets immobiliers de la culture, qui n'est pas, dans la présente instance, la partie perdante, le versement de la somme que demande à ce titre la société Tempeol ; qu'il y a lieu, en revanche, de mettre à la charge de cette dernière, au même titre, le versement à l'Opérateur du patrimoine et des projets immobiliers de la culture de la somme de 3 000 euros ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Tempeol est rejeté.<br/>
Article 2 : La société Tempeol versera à l'Opérateur du patrimoine et des projets immobiliers de la culture une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Tempeol et à l'Opérateur du patrimoine et des projets immobiliers de la culture.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
