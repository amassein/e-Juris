<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042520647</ID>
<ANCIEN_ID>JG_L_2020_11_000000438509</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/52/06/CETATEXT000042520647.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 13/11/2020, 438509, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438509</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT>M. Marc Pichon de Vendeuil</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEORD:2020:438509.20201113</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance n° 1803208 du 7 février 2020, enregistrée le 12 février 2020 au secrétariat du contentieux du Conseil d'Etat, la présidente du tribunal administratif de Rouen a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête de M. A... B..., enregistrée le 14 août 2018 au greffe de ce tribunal. Par cette requête, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 12 juin 2018 de la ministre des armées prononçant à son encontre la sanction disciplinaire du premier groupe de " blâme du ministre " ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la défense ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Marc Pichon de Vendeuil, rapporteur public ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. M. A... B..., chef d'escadron, affecté le 1er août 2014 au commandement de la compagnie de gendarmerie départementale de Soissons, s'est vu infliger le 12 juin 2018 un " blâme du ministre " par la ministre des armées, en raison de sa consommation d'alcool pendant son service le 10 juillet 2016 et d'un comportement inapproprié à l'égard d'un commerçant de la ville de Villers-Cotterêts. Par une ordonnance du 7 février 2020, la présidente du tribunal administratif de Rouen a transmis au Conseil d'Etat la requête de M. B... tendant à l'annulation de cette sanction.<br/>
<br/>
              2. En premier lieu, il ressort des pièces du dossier que l'intéressé a signé le 8 mars 2018 un bordereau attestant la remise de 59 pièces se référant à la procédure disciplinaire en cours, dont la pièce n° 50 relative à un enregistrement vidéo réalisé le 10 juillet 2016 par un client dans le magasin du commerçant cité au point 1. Par suite, le moyen tiré de ce que cet enregistrement ne lui aurait pas été communiqué manque en fait. <br/>
<br/>
              3. En deuxième lieu, le rapport de l'enquête administrative remis le 4 janvier 2017 se fonde sur les témoignages des principaux témoins des faits survenus le 10 juillet 2016 à Villers-Cotterêts. Aucune règle ni aucun principe n'exigent que les enquêteurs entendent toutes les personnes susceptibles de témoigner en faveur ou en défaveur de la personne susceptible d'être l'objet d'une sanction disciplinaire. En l'espèce, M. B... a communiqué spontanément à l'autorité investie du pouvoir disciplinaire des témoignages en sa faveur. Par ailleurs, M. B... ne démontre pas que son audition ni celles des deux témoins à charge auraient été réalisées dans des conditions partiales. Il s'ensuit que le moyen tiré du manque d'impartialité des enquêteurs doit être écarté. <br/>
<br/>
              4. En troisième lieu, aux termes de l'article R. 434-12 du code de la sécurité intérieure : " Le policier ou le gendarme ne se départ de sa dignité en aucune circonstance. / En tout temps, dans ou en dehors du service, (...) il s'abstient de tout acte, propos ou comportement de nature à nuire à la considération portée à la police nationale et à la gendarmerie nationale. Il veille à ne porter, par la nature de ses relations, aucune atteinte à leur crédit ou à leur réputation ". Aux termes de l'article R. 434-14 du même code : " Le policier ou le gendarme est au service de la population. / Sa relation avec celle-ci est empreinte de courtoisie et requiert l'usage du vouvoiement. / Respectueux de la dignité des personnes, il veille à se comporter en toute circonstance d'une manière exemplaire, propre à inspirer en retour respect et considération ".<br/>
<br/>
              5. Il ressort des témoignages du commerçant cité au point 1, du gendarme qui a accompagné en patrouille le requérant le 10 juillet 2016 au soir, et du commandant d'un peloton de gendarmerie présent le même soir dans la ville, que M. B... a consommé de l'alcool pendant son service et qu'il a eu un comportement inapproprié dans ses échanges avec ce commerçant en méconnaissance des dispositions citées au point 4. Par suite, le moyen tiré de ce que le comportement reproché au requérant ne serait ni établi ni fautif doit être écarté.<br/>
<br/>
              6. En dernier lieu, aux termes de l'article L. 4137-2 du code de la défense : " Les sanctions disciplinaires applicables aux militaires sont réparties en trois groupes : / 1° Les sanctions du premier groupe sont : / a) L'avertissement ; / b) La consigne ; / c) La réprimande ; / d) Le blâme ; / e) Les arrêts ; / f) Le blâme du ministre (...) ". En infligeant au requérant un " blâme du ministre ", la ministre des armées n'a pas pris une décision disproportionnée, eu égard à la nature des faits reprochés à l'intéressé, aux fonctions qu'il exerce et à l'atteinte à la réputation de la gendarmerie nationale. <br/>
<br/>
              7. Il résulte de tout ce qui précède que M. B... n'est pas fondé à demander l'annulation de la décision qu'il attaque.<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A... B... et à la ministre des armées.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
