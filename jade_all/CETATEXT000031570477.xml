<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031570477</ID>
<ANCIEN_ID>JG_L_2015_12_000000373948</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/57/04/CETATEXT000031570477.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 03/12/2015, 373948, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373948</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Yannick Faure</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:373948.20151203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° Sous le n° 373948, par une requête sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 12 décembre 2013, 13 mars 2014, 22 avril 2015 et 26 octobre 2015 au secrétariat du contentieux du Conseil d'Etat, la société anonyme Groupe Lépine demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du comité économique des produits de santé, publiée le 11 octobre 2013 au Journal officiel par avis du ministère des affaires sociales et de la santé, portant fixation des tarifs et prix limites de vente au public de certains implants articulaires inscrits à la section 5, chapitre 1er, titre III, de la liste prévue à l'article L. 165-1 du code de la sécurité sociale ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              2° Sous le n° 383513, par une requête et un mémoire en réplique, enregistrés les 6 août 2014 et 22 avril 2015 au secrétariat du contentieux du Conseil d'Etat, la société anonyme Groupe Lépine demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du comité économique des produits de santé, publiée le 6 juin 2014 au Journal officiel par avis du ministère des affaires sociales et de la santé, portant fixation des tarifs et des prix limites de vente au public de certains implants orthopédiques inscrits à la section 5, chapitre 1er, titre III, et à la sous-section 2, section 2, chapitre 2, titre III de la liste prévue à l'article L. 165-1 du code de la sécurité sociale ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code de la sécurité sociale ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yannick Faure, auditeur,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de la S.A. Groupe Lepine ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 4 novembre 2015, présentée dans les instances n° 373948 et n° 383513 par le comité économique des produits de santé ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que les requêtes de la société anonyme Groupe Lépine présentent à juger les mêmes questions ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant qu'aux termes du deuxième alinéa de l'article L. 165-2 du code de la sécurité sociale, dans sa rédaction applicable au litige : " Les tarifs de responsabilité des produits ou prestations mentionnés à l'article L. 165-1 inscrits par description générique sont établis par convention entre un ou plusieurs fabricants ou distributeurs des produits ou prestations répondant à la description générique ou, le cas échéant, une organisation regroupant ces fabricants ou distributeurs et le Comité économique des produits de santé dans les mêmes conditions que les conventions visées à l'article L. 162-17-4 ou, à défaut, par décision du Comité économique des produits de santé " ; qu'aux termes de l'article R.165-15 du même code : " I. - Le tarif ou le prix des produits ou des prestations mentionnés à l'article L. 165-1 peut être modifié par convention ou par décision du comité économique des produits de santé. / La modification du tarif ou du prix peut intervenir soit à la demande du fabricant ou du distributeur, soit à l'initiative du comité économique des produits de santé, soit à la demande des ministres chargés de la sécurité sociale, de la santé ou de l'économie ou de l'Union nationale des caisses d'assurance maladie. / (...) III. - Lorsque la modification du tarif ou du prix est effectuée à l'initiative du comité économique des produits de santé (...) les fabricants ou les distributeurs des produits ou des prestations en sont informés par une notification, adressée à chacun d'eux ou par un avis publié au Journal officiel. Ils peuvent présenter des observations écrites ou demander à être entendus par le comité dans les trente jours suivant la réception de la notification ou la publication de l'avis " ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier que par quatre avis publiés au Journal officiel du 31 mai 2013, le comité économique des produits de santé (CEPS) a fait connaître son intention de proposer une baisse des tarifs et prix limites de vente au public, dans leur description générique, de certains implants articulaires inscrits à la section 5, chapitre 1er, titre III, de la liste prévue à l'article L. 165-1 du code de la sécurité sociale ; que des discussions se sont alors engagées avec les deux organisations syndicales représentant les fabricants et distributeurs de ces dispositifs médicaux et le CEPS, qui ont donné lieu à divers échanges de courriers et réunions ; qu'à l'issue de cette période de concertation, le CEPS a adressé à ces deux organisations des propositions de conventions en leur demandant de lui faire connaître leur éventuel accord dans un délai de dix jours ; que l'organisation professionnelle à laquelle appartient la société requérante ayant manifesté son opposition à la proposition de convention qui lui avait été transmise, les nouveaux tarifs et prix limites de vente au public de différents types d'implants orthopédiques fabriqués ou distribués par elle ont été fixés par une décision unilatérale du CEPS, qui a donné lieu à un avis du ministère des affaires sociales et de la santé publiée le 11 octobre 2013 au Journal officiel ; qu'à la suite de l'absence de mise en oeuvre d'une baisse, initialement envisagée, du taux de la taxe sur la valeur ajoutée applicable aux implants en cause, le CEPS a pris l'initiative d'un allongement de l'échéancier de baisse des tarifs et prix limites de vente au public ; qu'en l'absence d'accord de l'une des organisations syndicales représentant les fabricants et distributeurs de ces dispositifs médicaux, les nouveaux tarifs et prix ont, de nouveau, fait l'objet d'une décision de fixation unilatérale qui a donné lieu à un nouvel avis du ministre, publié au Journal officiel le 6 juin 2014 ; que la société Groupe Lépine demande l'annulation pour excès de pouvoir de ces décisions ;<br/>
<br/>
              4. Considérant, en premier lieu, qu'aux termes de l'article D. 162-2-1 du code de la sécurité sociale : " Le comité économique des produits de santé institué par l'article L. 162-17-3 est composé des membres suivants : / 1° Un président et deux vice-présidents, l'un chargé du médicament, l'autre des produits et prestations mentionnés à l'article L. 165-1, nommés pour une durée de trois ans par arrêté conjoint des ministres chargés de la sécurité sociale, de la santé et de l'économie ; / 2° Le directeur de la sécurité sociale ou son représentant ; / 3° Le directeur général de la santé ou son représentant ; / 4° Le directeur général de la concurrence, de la consommation et de la répression des fraudes ou son représentant ; / 5° Le directeur général de la compétitivité, de l'industrie et des services ou son représentant ; / 6° Deux représentants des organismes nationaux d'assurance maladie désignés par le directeur général de la Caisse nationale d'assurance maladie des travailleurs salariés et un représentant désigné conjointement par le directeur de la Caisse nationale du régime social des indépendants et le directeur de la Caisse centrale de la mutualité sociale agricole ; / 7° Un représentant désigné par le conseil de l'Union nationale des organismes d'assurance maladie complémentaire. / (...) / Les directeurs d'administration centrale ne peuvent se faire représenter que par des membres de leur service occupant des fonctions au moins égales à celles de sous-directeur. Les représentants des organismes nationaux d'assurance maladie doivent occuper des fonctions au moins égales à celle de directeur adjoint " ; que le II de l'article D. 162-2-3 du même code précise que : " Lorsqu'il exerce les missions définies aux articles L. 165-2, L. 165-3 et L. 165-4, le comité économique des produits de santé se réunit en section des produits et prestations mentionnés à l'article L. 165-1 ; le vice-président qui siège est celui en charge des produits et prestations mentionnés à l'article L. 165-1 " ; qu'en l'absence d'un texte fixant  une règle de quorum ou exigeant la présence de certains membres, les décisions du comité économique des produits de santé sont régulières dès lors que la majorité de ses  membres a siégé ;<br/>
<br/>
              5. Considérant, d'une part, qu'il ressort des pièces du dossier qu'étaient présents à la réunion du 1er octobre 2013, au cours de laquelle le comité économique des produits de santé a décidé de diminuer de façon unilatérale les tarifs et prix limites de vente au public des implants orthopédiques en litige, le président du comité, le vice-président en charge des produits et prestations mentionnés à l'article L. 165-1 et le représentant de l'Union nationale des organismes d'assurance maladie complémentaire ; que si des membres de la direction de la sécurité sociale, de la direction générale de la santé, de la direction générale de la concurrence, de la consommation et de la répression des fraudes et de la direction générale de la compétitivité, de l'industrie et des services étaient présents, aucun d'entre eux n'occupait des fonctions au moins égales à celles de sous-directeur ; que, de même, si des membres des services des organismes nationaux d'assurance maladie étaient présents, aucun d'entre eux n'occupait des fonctions au moins égales à celle de directeur adjoint ; qu'ainsi, le comité économique des produits de santé a siégé sans qu'au moins la moitié de ses membres aient été présents ; <br/>
<br/>
              6. Considérant que, d'autre part, ni dans le cadre des échanges contradictoires entre les parties, ni au terme d'une mesure supplémentaire d'instruction ordonnée à cette fin par la première sous-section de la section du contentieux du Conseil d'Etat, le comité économique des produits de santé n'a versé au dossier de document de nature à établir la liste des personnes ayant siégé lors de sa séance du 11 février 2014, au cours de laquelle il a décidé de modifier l'échéancier de baisse des tarifs et prix limites de vente au public des mêmes implants ; que, par suite, le moyen tiré de ce que le comité économique des produits de santé a siégé sans que le quorum ne soit atteint ne peut qu'être accueilli ; <br/>
<br/>
              7. Considérant, en second lieu, qu'aux termes du troisième alinéa de l'article D. 162-2-5 du code de la sécurité sociale : " Le président [du comité économique des produits de santé] signe les conventions passées et les décisions prises en application des articles L. 162-16-4 à L. 162-16-6, L. 162-17-3, L. 162-17-4, L. 162-18 et L. 165-2 à L. 165-4 " ; que ni dans le cadre des échanges contradictoires entre les parties, ni au terme d'une mesure supplémentaire d'instruction ordonnée par la première sous-section de la section du contentieux du Conseil d'Etat, le comité économique des produits de santé n'a versé aux dossiers les décisions de fixation des tarifs et prix limites de vente au public des implants en litige, ni d'élément de nature à établir que ces décisions auraient été signées par son président ; que, dans ces conditions, le moyen tiré du défaut de signature de ces décisions doit également être accueilli ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens des requêtes, la société requérante est fondée à demander l'annulation de ces décisions ; <br/>
<br/>
              Sur les effets de l'annulation :<br/>
<br/>
              9. Considérant qu'il ne ressort pas des pièces des dossiers que l'annulation des décisions attaquées soit de nature à emporter des conséquences manifestement excessives en raison tant des effets que ces actes ont produits que des situations qui ont pu se constituer lorsqu'ils étaient en vigueur ; qu'ainsi, contrairement à ce que soutient le comité économique des produits de santé, il n'y a pas lieu, dans les circonstances de l'espèce, de limiter les effets dans le temps de l'annulation de ces actes ;<br/>
<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              10. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 2 000 euros, à verser à la société Groupe Lépine ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les décisions du comité économique des produits de santé publiées le 11 octobre 2013 et le 6 juin 2014 au Journal officiel de la République française par avis du ministère des affaires sociales et de la santé sont annulées.<br/>
Article 2 : L'Etat versera à la société Groupe Lépine une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société anonyme Groupe Lépine et au comité économique des produits de santé.<br/>
Copie en sera adressée à la ministre des affaires sociales, de la santé et des droits des femmes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
