<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035803964</ID>
<ANCIEN_ID>JG_L_2017_10_000000398823</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/80/39/CETATEXT000035803964.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 13/10/2017, 398823</TITRE>
<DATE_DEC>2017-10-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398823</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:398823.20171013</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Les communes de Saint-Sorlin-en-Valloire, d'Epinouze, de Manthes et de Moras-en-Valloire (Drôme) ont demandé au tribunal administratif de Grenoble d'annuler huit titres exécutoires émis à leur encontre les 15 mai 2012 et 6 juin 2013 par l'Office national des forêts (ONF) de Rhône-Alpes en paiement de frais de garderie pour la forêt indivise de Moras-Saint-Sorlin. Par un jugement n° 1204088, 1204089, 1204091, 1204092, 1304381, 1304382, 1304524 et 1304525 du 11 février 2014, le tribunal administratif a fait droit à cette demande.<br/>
<br/>
              Par un arrêt n°14LY01152 du 16 février 2016, la cour administrative d'appel de Lyon a rejeté l'appel de l'ONF contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique enregistrés les 18 avril et 18 juillet 2016 et le 10 mai 2017 au secrétariat du contentieux du Conseil d'Etat, l'ONF demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge des communes de Saint-Sorlin-en-Valloire, d'Epinouze, de Manthes et de Moras-en-Valloire une somme de 1 000 euros chacune au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - la loi n° 78-1239 du 29 décembre 1978 ;<br/>
<br/>
              - la loi n° 2011-1977 du 28 décembre 2011 ;<br/>
<br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
<br/>
              - le décret n° 79-333 du 19 avril 1979 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Nervo, Poupet, avocat de l'Office national des forêts et à la SCP Spinosi, Sureau, avocat de la commune de Saint-Sorlin-en-Valloire, de la commune d'Epinouze, de la commune de Manthes et de la commune de Moras-en-Valloire.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, les 15 mai 2012 et 6 juin 2013, l'Office national des forêts (ONF) de Rhône-Alpes a émis à l'encontre des communes de Saint-Sorlin-en-Valloire, d'Epinouze, de Manthes et de Moras-en-Valloire huit titres exécutoires en vue du recouvrement de " frais de garderie " pour la forêt indivise de Moras-Saint-Sorlin ; que, saisi par les quatre communes, le tribunal administratif de Grenoble a annulé ces titres exécutoires par un jugement du 11 février 2014 au motif que, ne comportant ni nom, ni prénom, ni signature, ils ne permettaient pas d'identifier leur auteur ; que l'ONF se pourvoit en cassation contre l'arrêt du 16 février 2016 par lequel la cour administrative d'appel de Lyon a rejeté son appel contre ce jugement en retenant, d'une part, que l'absence de signature sur les titres exécutoires les entachait d'illégalité et, d'autre part, que les produits du bail conclu entre les quatre communes et le syndicat de traitement des déchets Ardèche-Drôme portant sur l'implantation d'une installation de stockage de déchets non dangereux dans la forêt de Moras-Saint-Sorlin ne pouvaient être compris dans l'assiette des frais de garderie de bois et forêts soumis au régime forestier ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 92 de la loi du 29 décembre 1978 de finances pour 1979, dans sa rédaction issue de la loi du 28 décembre 2011 de finances pour 2012 : " A compter du 1er janvier 1996, les contributions des collectivités territoriales, sections de communes, établissements publics, établissements d'utilité publique, sociétés mutualistes et caisses d'épargne aux frais de garderie et d'administration de leurs forêts relevant du régime forestier, prévues à l'article L. 147-1 du code forestier, sont fixées à 12 p. 100 du montant hors taxe des produits de ces forêts (...) / Les produits des forêts mentionnés au premier alinéa sont tous les produits des forêts relevant du régime forestier, y compris ceux issus de la chasse, de la pêche et des conventions ou concessions de toute nature liées à l'utilisation ou à l'occupation de ces forêts, ainsi que tous les produits physiques ou financiers tirés du sol ou de l'exploitation du sous-sol. Pour les produits de ventes de bois, le montant est diminué des ristournes consenties aux acheteurs dans le cas de paiement comptant et, lorsqu'il s'agit de bois vendus façonnés, des frais d'abattage et de façonnage hors taxe " ; qu'il résulte de ces dispositions, éclairées par les travaux préparatoires de l'article 113 de la loi du 28 décembre 2011 duquel elles sont issues, qu'en mentionnant les produits physiques ou financiers tirés du sol ou de l'exploitation parmi les éléments de l'assiette de la contribution pour " frais de garderie ", le législateur a entendu y inclure l'ensemble des produits tirés des forêts relevant du régime forestier, y compris ceux qui résultent d'activités sans autre lien avec les bois et forêts que leur localisation géographique à l'intérieur d'une zone soumise à ce régime ; <br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède qu'en jugeant que les produits du bail conclu entre les quatre communes destinataires des titres exécutoires contestés et le syndicat de traitement des déchets Ardèche-Drôme portant sur l'implantation d'une installation de stockage de déchets non dangereux dans la forêt de Moras-Saint-Sorlin ne pouvaient être compris dans l'assiette de la contribution due au titre des dispositions précitées au motif qu'ils n'avaient d'autre lien avec les bois et forêts que leur localisation géographique à l'intérieur d'une zone soumise au régime forestier, la cour administrative d'appel de Lyon a commis une erreur de droit ; <br/>
<br/>
              4. Mais considérant que la cour a également fondé l'annulation des titres exécutoires sur le motif, dont le bien-fondé n'est pas discuté par le pourvoi, qu'ils n'étaient pas signés ; que son arrêt est suffisamment motivé sur ce point ; que la cour s'étant bornée, en retenant l'existence d'une obligation de signature applicable même sans texte, à reprendre le motif du jugement qui lui était déféré, elle n'a pas soulevé d'office un moyen sans recueillir les observations des parties ; que les moyens tirés d'une insuffisance de motivation et de la méconnaissance du caractère contradictoire de la procédure soulevés par l'ONF doivent, par suite, être écartés ;<br/>
<br/>
              5. Considérant que le motif de l'arrêt relatif au défaut de signature des états exécutoires justifiait à lui seul le rejet de l'appel de l'ONF ; qu'il suit de là que l'office n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ; <br/>
<br/>
              6. Considérant que les dispositions de L. 761-1 du code de justice administrative  font obstacle à ce qu'une somme soit mise à la charge des communes de Saint-Sorlin-en-Valloire, d'Epinouze, de Manthes et de Moras-en-Valloire qui ne sont pas, dans la présente instance, les parties perdantes  ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de l'ONF une somme à verser à ces communes au même titre ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de l'ONF est rejeté.<br/>
<br/>
Article 2 : Les conclusions présentées par les communes de Saint-Sorlin-en-Valloire, d'Epinouze, de Manthes et de Moras-en-Valloire au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à l'Office national des forêts et aux communes de Saint-Sorlin-en-Valloire, d'Epinouze, de Manthes et de Moras-en-Valloire.<br/>
		Copie en sera adressée au ministre de l'agriculture et de l'alimentation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">03-06-01 AGRICULTURE ET FORÊTS. BOIS ET FORÊTS. GESTION DES FORÊTS. - FRAIS DE GARDERIE ET D'ADMINISTRATION (ART. 92 DE LA LOI DU 29 DÉCEMBRE 1978, DANS SA RÉDACTION ISSUE DE LA LOI DU 28 DÉCEMBRE 2011) - ASSIETTE - INCLUSION - ENSEMBLE DES PRODUITS TIRÉS DES FORÊTS RELEVANT DU RÉGIME FORESTIER, Y COMPRIS CEUX RÉSULTANT D'ACTIVITÉS SANS AUTRE LIEN AVEC LES BOIS ET LES FORÊTS QUE LEUR LOCALISATION GÉOGRAPHIQUE À L'INTÉRIEUR D'UNE ZONE SOUMISE AU RÉGIME FORESTIER [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">135-02-04-02 COLLECTIVITÉS TERRITORIALES. COMMUNE. FINANCES COMMUNALES. DÉPENSES. - FRAIS DE GARDERIE ET D'ADMINISTRATION (ART. 92 DE LA LOI DU 29 DÉCEMBRE 1978, DANS SA RÉDACTION ISSUE DE LA LOI DU 28 DÉCEMBRE 2011) - ASSIETTE - INCLUSION - ENSEMBLE DES PRODUITS TIRÉS DES FORÊTS RELEVANT DU RÉGIME FORESTIER, Y COMPRIS CEUX RÉSULTANT D'ACTIVITÉS SANS AUTRE LIEN AVEC LES BOIS ET LES FORÊTS QUE LEUR LOCALISATION GÉOGRAPHIQUE À L'INTÉRIEUR D'UNE ZONE SOUMISE AU RÉGIME FORESTIER [RJ1].
</SCT>
<ANA ID="9A"> 03-06-01 Il résulte de l'article 92 de la loi n° 78-1239 du 29 décembre 1978, éclairées par les travaux préparatoires de l'article 113 de la loi n° 2011-1977 du 28 décembre 2011 duquel elles sont issues, qu'en mentionnant les produits physiques ou financiers tirés du sol ou de l'exploitation parmi les éléments de l'assiette de la contribution pour frais de garderie, le législateur a entendu y inclure l'ensemble des produits tirés des forêts relevant du régime forestier, y compris ceux qui résultent d'activités sans autre lien avec les bois et forêts que leur localisation géographique à l'intérieur d'une zone soumise à ce régime.</ANA>
<ANA ID="9B"> 135-02-04-02 Il résulte de l'article 92 de la loi n° 78-1239 du 29 décembre 1978, éclairées par les travaux préparatoires de l'article 113 de la loi n° 2011-1977 du 28 décembre 2011 duquel elles sont issues, qu'en mentionnant les produits physiques ou financiers tirés du sol ou de l'exploitation parmi les éléments de l'assiette de la contribution pour frais de garderie, le législateur a entendu y inclure l'ensemble des produits tirés des forêts relevant du régime forestier, y compris ceux qui résultent d'activités sans autre lien avec les bois et forêts que leur localisation géographique à l'intérieur d'une zone soumise à ce régime.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., en l'état antérieur des textes, CE, 1er avril 2005, Office national des forêts, n°257168, pp. 733 -761.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
