<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042520625</ID>
<ANCIEN_ID>JG_L_2020_11_000000429326</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/52/06/CETATEXT000042520625.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 13/11/2020, 429326</TITRE>
<DATE_DEC>2020-11-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429326</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SARL DIDIER, PINET ; SCP GHESTIN</AVOCATS>
<RAPPORTEUR>M. Thomas Janicot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:429326.20201113</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au tribunal administratif de Nantes d'annuler la décision portant interdiction totale et générale de postuler aux emplois du département de Loire-Atlantique, d'enjoindre au département d'une part de lui communiquer le dernier arrêté de situation administrative de l'agent la remplaçant durant son absence et, d'autre part, de lui proposer un poste correspondant réellement à son grade, et de condamner le département de Loire-Atlantique à lui verser une indemnité de 7 000 euros en réparation de ses préjudices. Par un jugement n° 1607861 du 19 décembre 2018, le tribunal administratif de Nantes a condamné le département de Loire-Atlantique à verser à Mme A... une indemnité de 1 000 euros et rejeté le surplus de ses conclusions. <br/>
<br/>
              Par une ordonnance n° 19NT00620 du 29 mars 2019, enregistrée le 1er avril 2019 au secrétariat du contentieux du Conseil d'Etat, la présidente de la cour administrative d'appel de Nantes a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 11 février 2019 au greffe de cette cour, présenté par le département de Loire-Atlantique. Par ce pourvoi et par un nouveau mémoire enregistré le 13 mai 2019 au secrétariat du contentieux du Conseil d'Etat, le département de Loire-Atlantique demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de Mme A... la somme de 2 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Janicot, auditeur,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat du département de Loire-Atlantique et à la SCP Ghestin, avocat de Mme B... A... ;<br/>
<br/>
              Vu les notes en délibéré, enregistrées le 23 octobre et le 2 novembre 2020, présentées par Mme A... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article R. 811-1 du code de justice administrative : " Toute partie présente dans une instance devant le tribunal administratif ou qui y a été régulièrement appelée, alors même qu'elle n'aurait produit aucune défense, peut interjeter appel contre toute décision juridictionnelle rendue dans cette instance./ Toutefois, le tribunal administratif statue en premier et dernier ressort : [...] 8° Sauf en matière de contrat de la commande publique sur toute action indemnitaire ne relevant pas des dispositions précédentes, lorsque le montant des indemnités demandées n'excède pas le montant déterminé par les articles R. 222-14 et R. 222-15 ; [...] Par dérogation aux dispositions qui précèdent, en cas de connexité avec un litige susceptible d'appel, les décisions portant sur les actions mentionnées au 8° peuvent elles-mêmes faire l'objet d'un appel ". Le montant visé par les dispositions précédemment citées est de 10 000 euros et s'apprécie au regard des sommes demandées dans la requête introductive d'instance, compte non tenu des demandes d'intérêts et des sommes demandées au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              2. Par son jugement du 19 décembre 2018, le tribunal administratif de Nantes a statué sur une demande de Mme A... qui tendait, d'une part, à l'annulation d'une décision regardée par elle comme portant interdiction totale et générale de postuler aux emplois du département de Loire-Atlantique et, d'autre part, à la condamnation du département à lui verser une indemnité de 7 000 euros en réparation de ses préjudices. Ce jugement, en tant qu'il statuait sur les conclusions de Mme A... tendant à l'annulation de la décision du département, portait sur un litige susceptible d'appel. Si le montant de l'indemnité demandée par Mme A... n'excédait pas celui déterminé par les articles R. 222-14 et R. 222-15 du code de justice administrative, la demande indemnitaire formée par cette dernière était cependant connexe avec le litige susceptible d'appel. Dès lors, en application des dispositions précitées, le jugement contesté est, dans son ensemble, susceptible d'appel, sans qu'ait d'incidence à cet égard la circonstance que le département de Loire-Atlantique ne l'a contesté qu'en tant qu'il statuait sur les conclusions indemnitaires de Mme A.... Par suite, les conclusions présentées devant la cour par le département de Loire-Atlantique, dirigées contre le jugement du tribunal administratif de Nantes du 19 décembre 2018 en tant qu'il l'a condamné à verser à Mme A... une indemnité de 1 000 euros en réparation de son préjudice moral, présentant le caractère d'un appel, il y a lieu de renvoyer leur jugement à la cour administrative d'appel de Nantes. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement de la requête du département de Loire-Atlantique contre le jugement du 19 décembre 2018 du tribunal administratif de Nantes en tant qu'il l'a condamné à verser une indemnité à Mme A... est attribué à la cour administrative d'appel de Nantes.<br/>
<br/>
Article 2 : La présente décision sera notifiée au département de Loire-Atlantique, à Mme B... A... et au président de la cour administrative d'appel de Nantes.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-012 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER ET DERNIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. - EXCLUSION - DEMANDE INDEMNITAIRE CONNEXE À UNE DEMANDE TENDANT À L'ANNULATION DE LA DÉCISION EN CAUSE, ALORS MÊME QUE LE MONTANT DEMANDÉ N'EXCÈDE PAS LE SEUIL DÉTERMINÉ PAR LES ARTICLES R. 222-14 ET R. 222-15 DU CJA [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-05-015 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE D'APPEL DES COURS ADMINISTRATIVES D'APPEL. - DEMANDE TENDANT, D'UNE PART, À L'ANNULATION D'UNE DÉCISION POUR EXCÈS DE POUVOIR ET, D'AUTRE PART, À LA RÉPARATION DES PRÉJUDICES CORRESPONDANTS POUR UN MONTANT N'EXCÉDANT PAS LE SEUIL DÉTERMINÉ PAR LES ARTICLES R. 222-14 ET R. 222-15 DU CJA - DEMANDES CONNEXES (ART. R. 811-1 DU CJA) - EXISTENCE [RJ1] - CONSÉQUENCE - JUGEMENT SUSCEPTIBLE D'APPEL DANS SON ENSEMBLE, MÊME SI SEUL SON DISPOSITIF INDEMNITAIRE EST CONTESTÉ.
</SCT>
<ANA ID="9A"> 17-05-012 Un tribunal administratif a statué sur la demande d'un requérant tendant, d'une part, à l'annulation de la décision d'un département et, d'autre part, à la condamnation de ce département à une indemnité de 7 000 euros. Le jugement, en tant qu'il statue sur les conclusions tendant à l'annulation de la décision du département, porte sur un litige susceptible d'appel. Si le montant de l'indemnité demandée n'excède pas celui déterminé par les articles R. 222-14 et R. 222-15 du code de justice administrative (CJA), la demande indemnitaire est cependant connexe avec le litige susceptible d'appel. Dès lors, en application de l'article R. 811-1 du CJA, le jugement contesté est, dans son ensemble, susceptible d'appel, sans qu'ait d'incidence à cet égard la circonstance que le département ne l'a contesté qu'en tant qu'il statuait sur les conclusions indemnitaires.</ANA>
<ANA ID="9B"> 17-05-015 Un tribunal administratif a statué sur la demande d'un requérant tendant, d'une part, à l'annulation de la décision d'un département et, d'autre part, à la condamnation de ce département à une indemnité de 7 000 euros. Le jugement, en tant qu'il statue sur les conclusions tendant à l'annulation de la décision du département, porte sur un litige susceptible d'appel. Si le montant de l'indemnité demandée n'excède pas celui déterminé par les articles R. 222-14 et R. 222-15 du code de justice administrative (CJA), la demande indemnitaire est cependant connexe avec le litige susceptible d'appel. Dès lors, en application de l'article R. 811-1 du CJA, le jugement contesté est, dans son ensemble, susceptible d'appel, sans qu'ait d'incidence à cet égard la circonstance que le département ne l'a contesté qu'en tant qu'il statuait sur les conclusions indemnitaires.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 29 décembre 2004, Mme,, n° 272318, inédite au Recueil ; CE, 22 juillet 2015, M.,, n° 374274, inédite au Recueil. Rappr., s'agissant de la connexité entre un recours en exécution et un recours indemnitaire, CE, 31 mars 2014, M.,, n° 363627, T. pp. 584-588 ; s'agissant de la connexité entre recours pécuniaire et indemnitaire, CE, 10 mai 2019, M.,, n° 423836, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
