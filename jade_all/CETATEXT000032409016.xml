<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032409016</ID>
<ANCIEN_ID>JG_L_2016_04_000000390185</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/40/90/CETATEXT000032409016.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 15/04/2016, 390185, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-04-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390185</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR>Mme Florence Chaltiel-Terral</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:390185.20160415</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 15 mai 2015 au secrétariat du contentieux du Conseil d'Etat, M. B...A...demande au Conseil d'Etat : <br/>
<br/>
              1°) à titre principal, d'annuler pour excès de pouvoir la décision du 3 mars 2015 par laquelle le Conseil national de l'ordre des médecins, statuant en formation restreinte, l'a suspendu pour une durée de dix huit mois du droit d'exercer la médecine et lui a imposé le suivi d'une formation ;<br/>
<br/>
              2°) à titre subsidiaire, de limiter la suspension d'exercice à la seule médecine générale à l'exception de l'homéopathie ; <br/>
<br/>
              3°) de mettre à la charge du Conseil national de l'ordre des médecins la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Chaltiel-Terral, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Vexliard, Poupot, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article R. 4124-3-5 du code de la santé publique : " I. - En cas d'insuffisance professionnelle rendant dangereux l'exercice de la profession, la suspension temporaire, totale ou partielle, du droit d'exercer est prononcée par le conseil régional ou interrégional pour une période déterminée, qui peut, s'il y a lieu, être renouvelée. (...)  II. - La suspension ne peut être ordonnée que sur un rapport motivé établi à la demande du conseil régional ou interrégional dans les conditions suivantes : 1° Pour les médecins, le rapport est établi par trois médecins qualifiés dans la même spécialité que celle du praticien concerné désignés comme experts, le premier par l'intéressé, le deuxième par le conseil régional ou interrégional et le troisième par les deux premiers experts. Ce dernier est choisi parmi les personnels enseignants et hospitaliers titulaires de la spécialité. Pour la médecine générale, le troisième expert est choisi parmi les personnels enseignants titulaires ou les professeurs associés ou maîtres de conférences associés des universités ; (...) IV. - Les experts procèdent ensemble, sauf impossibilité manifeste, à l'examen des connaissances théoriques et pratiques du praticien. Le rapport d'expertise est déposé au plus tard dans le délai de six semaines à compter de la saisine du conseil (...) " ; qu'enfin, aux termes du VI du même article, le conseil national de l'ordre statue sur les demandes pour lesquelles le conseil régional initialement saisi ne s'est pas prononcé dans les deux mois ; <br/>
<br/>
              2. Considérant qu'en application de ces dispositions, le Conseil national de l'ordre des médecins a, par la décision litigieuse du 3 mars 2015, décidé de suspendre M.A..., médecin généraliste, du droit d'exercer la médecine pendant une période de dix-huit mois et subordonné la reprise de son activité professionnelle à la justification de certaines obligations de formation ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier que la décision du 3 mars 2015 est intervenue après examen du praticien par trois médecins experts ; que, contrairement à ce que soutient M.A..., il ressort également des pièces du dossier que la désignation des trois médecins experts a été faite conformément aux dispositions de l'article R. 4124-3-5 du code de la santé publique ; <br/>
<br/>
              4. Considérant que si l'article R. 4124-3-5 du code de la santé publique cité ci-dessus prévoit que le rapport des experts doit être déposé dans le délai de six semaines à compter de la saisine de l'instance ordinale compétente, ce délai n'est pas prescrit à peine de nullité ; que M. A...ne saurait, par suite, utilement soutenir que la seule circonstance que ce rapport a été remis au terme d'un délai plus long entache d'irrégularité la décision qu'il attaque ; <br/>
<br/>
              5. Considérant que la décision attaquée énonce, au-delà de la seule reprise de citations du rapport des experts, les considérations de droit et de fait sur lesquelles elle se fonde ; qu'elle est, par suite, suffisamment motivée ;<br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier et, notamment, des faits non contestés relevés par le rapport des experts, que les connaissances de M. A...présentent des lacunes importantes pour un grand nombre de traitements courants dans l'exercice de la médecine générale ; qu'ainsi, à supposer même que certaines de ses prescriptions inadaptées de psychotropes s'expliqueraient par des menaces dont il aurait fait l'objet, le Conseil national de l'ordre des médecins n'a pas fait une inexacte application des dispositions de l'article R. 4124-3-5 du code de la santé publique en estimant que l'insuffisance professionnelle de ce praticien était, à la date de sa décision, incompatible avec l'exercice de l'art médical ; <br/>
<br/>
              7. Considérant qu'il ressort enfin des pièces du dossier qu'en prononçant une suspension temporaire du droit d'exercer tout acte médical et non, comme le demandait M. A...à titre subsidiaire, une suspension partielle l'autorisant à pratiquer l'homéopathie, le Conseil national de l'ordre des médecins n'a, eu égard à la nature de l'exercice de la médecine générale, pas fait une inexacte application des dispositions de l'article R. 4124-3-5 du code de la santé publique ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que M. A...n'est pas fondé à demander l'annulation de la décision qu'il attaque ; que les dispositions de l'article L. 761-1 du code de justice administrative font, par suite, obstacle à ce qu'une somme soit mise à ce titre à la charge du Conseil national de l'ordre des médecins qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au Conseil national de l'ordre des médecins.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
