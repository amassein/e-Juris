<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026738941</ID>
<ANCIEN_ID>JG_L_2012_12_000000348347</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/73/89/CETATEXT000026738941.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 07/12/2012, 348347, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>348347</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN</AVOCATS>
<RAPPORTEUR>M. Philippe Josse</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:348347.20121207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 11 avril et 11 juillet 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SARL DSE, dont le siège social est Pôle de la Viande, rue Salvador Allende, BP 402 à La Talaudière (42354), représentée par son gérant en exercice ; la SARL DSE demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 2 de l'arrêt n° 10LY02450 du 1er février 2011 par lequel la cour administrative d'appel de Lyon, après avoir annulé l'ordonnance n° 0803355 du 26 août 2010 du président de la IVe chambre du tribunal administratif de Lyon rejetant sa demande tendant à la restitution de la taxe sur les achats de viande qu'elle a acquittée au titre de la période allant du 1er janvier 2001 au 31 décembre 2003 et évoqué, a rejeté cette demande et le surplus des conclusions de la requête ;<br/>
<br/>
              2°) réglant l'affaire au fond, d'accorder la restitution demandée ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le traité instituant la Communauté européenne ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Josse, Conseiller d'Etat,  <br/>
<br/>
              - les observations de la SCP Monod, Colin, avocat de la SARL DSE,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Monod, Colin, avocat de la SARL DSE ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SARL DSE a sollicité la restitution des droits de taxe sur les achats de viande qu'elle avait acquittés pour les années 2001, 2002 et 2003 ; que l'administration fiscale a prononcé le dégrèvement de ces droits par des décisions en date des 5, 10 et 16 août 2004 ; que l'administration a, en conséquence, restitué les sommes perçues augmentées des intérêts moratoires ; que, toutefois, l'administration a invoqué une erreur dans la liquidation du dégrèvement et prétendu que le principe d'égalité devant les charges publiques commandait d'annuler ces décisions de dégrèvement ; qu'elle a alors informé le contribuable par trois lettres du 10 novembre 2004 qu'elle souhaitait persister dans sa volonté de l'imposer au titre des années 2001 à 2003 ; qu'elle a adressé à cette société trois propositions de rectification le 20 décembre 2004 ; que les droits de taxe sur les achats de viande au titre des années 2001 à 2003 restitués par l'administration ont été mis en recouvrement par deux avis de mise en recouvrement du 4 octobre 2007 ; que la société DSE se pourvoit en cassation contre l'article 2 de l'arrêt du 1er février 2011 de la cour administrative d'appel de Lyon qui, après avoir annulé l'ordonnance du 26 août 2010 du Président de la IVe chambre du tribunal administratif de Lyon rejetant sa demande et évoqué, a rejeté sa demande de décharge et le surplus de sa requête ;<br/>
<br/>
              Sur la régularité de l'arrêt :<br/>
<br/>
              2. Considérant qu'en jugeant que le moyen tiré de ce que les lettres d'information adressées en novembre 2004 à la société n'étaient pas de nature à permettre le retrait des décisions de dégrèvement dont les sociétés avaient bénéficié ne pouvait être accueilli qu'en relevant qu'aucune discrimination au principe d'égalité devant les charges publiques n'était constituée et en jugeant que la note du 6 janvier 2004 émanant du service juridique de la direction générale des impôts présentait le caractère d'un document interne à l'administration n'ayant pas fait l'objet de la part de celle-ci d'une diffusion destinée aux contribuables, pour écarter l'application des articles L. 80 A et L. 80 B du livre des procédures fiscales invoqués, la cour a suffisamment motivé son arrêt ;<br/>
<br/>
              Sur la prescription :<br/>
<br/>
              3. Considérant, d'une part, que l'article 302 bis ZD du code général des impôts alors en vigueur prévoit que la taxe sur les achats de viande est constatée, recouvrée et contrôlée selon les mêmes procédures que la taxe sur la valeur ajoutée ; que l'article L. 176 du livre des procédures fiscales dispose que : " pour les taxe sur le chiffre d'affaires, le droit de reprise de l'administration s'exerce jusqu'à la fin de la troisième année suivant celle au cours de laquelle la taxe est devenue exigible [...] " ; que cette disposition s'applique également dans le cas où des droits de taxe sur les achats de viande ont été dégrevés à tort ;<br/>
<br/>
              4. Considérant, d'autre part, qu'aux termes de l'article L. 189 du livre des procédures fiscales : " La prescription est interrompue par la notification d'une proposition de rectification, par la déclaration ou la notification d'un procès-verbal, de même que par tout acte comportant reconnaissance de la part des contribuables et par tous les autres actes interruptifs de droit commun " ; qu'ainsi, en jugeant que le délai de reprise avait été interrompu par la notification de la proposition de rectification le 20 décembre 2004, alors même que l'administration n'était pas tenue d'y recourir, de sorte qu'elle pouvait établir une nouvelle imposition pour une période identique, la cour, qui a suffisamment motivé sa décision, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              Sur la qualification d'aides d'Etat :<br/>
<br/>
              5. Considérant qu'après avoir rappelé qu'il n'existait, à compter du 1er janvier 2001, aucun lien d'affectation contraignant entre la taxe sur les achats de viande et le service public de l'équarrissage ni aucun rapport entre le produit de la taxe et le montant du financement public attribué à ce service, la cour a pu, sans commettre d'erreur de droit, juger que la taxe sur les achats de viande n'entrait plus, à compter du 1er janvier 2001, dans le champ d'application des stipulations des articles 87 et 88 du traité instituant la Communauté européenne concernant les aides d'Etat ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui a été dit ci-dessus que la SARL DSE n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soient mises à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante, les sommes demandées par la société requérante au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Le pourvoi de la SARL DSE est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la SARL DSE et au ministre de l'économie et des finances. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
