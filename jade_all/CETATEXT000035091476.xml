<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035091476</ID>
<ANCIEN_ID>JG_L_2017_06_000000395555</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/09/14/CETATEXT000035091476.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 30/06/2017, 395555, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395555</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR>M. Marc Lambron</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:395555.20170630</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Paris d'annuler l'arrêté du 30 octobre 2012 par lequel le ministre de l'intérieur l'a radié des cadres de la police nationale pour abandon de poste, d'ordonner sa réintégration et de condamner l'Etat à réparer le préjudice subi.<br/>
<br/>
              Par un jugement n° 1301071/5-1 du 16 octobre 2013, le tribunal administratif de Paris a annulé l'arrêté du 30 octobre 2012 du ministre de l'intérieur, lui a enjoint de réintégrer M. A...dans les cadres de la police nationale à compter de la date d'effet de l'arrêté annulé, dans le délai de deux mois à compter de la notification du jugement et, enfin, a rejeté les conclusions indemnitaires présentées par M.A....<br/>
<br/>
              Par un arrêt n° 13PA04758 du 29 octobre 2015, la cour administrative d'appel de Paris a rejeté le recours du ministre de l'intérieur ainsi que les conclusions indemnitaires présentées par M.A....<br/>
<br/>
              Par un pourvoi enregistré le 23 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) règlant l'affaire au fond, de faire droit à son appel.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
               - le décret n° 95-654 du 9 mai 1995 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Lambron, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gadiou, Chevallier, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.A..., gardien de la paix, n'a plus occupé son poste à compter du 20 juillet 2011 ; qu'un courrier le mettant en demeure de se présenter au service médical muni de tout document médical de nature à justifier son absence lui a été adressé, sous pli recommandé, le 1er février 2012, à l'adresse de son domicile familial situé 105 avenue du Belvédère, au Pré-Saint-Gervais (Seine-Saint-Denis) ; que ce courrier a fait l'objet d'une réexpédition par La Poste à l'adresse située 30 rue Louis Blanc, à Paris (75010), puis a été retourné à l'administration accompagné de la mention " anomalie adresse " ; que le ministre de l'intérieur a adressé les deux courriers suivants, en date du 15 mars et du 10 mai 2012, mettant en demeure M. A...de réintégrer le service sous peine de radiation des cadres, au 30 rue Louis Blanc à Paris ; que ces deux courriers sont également revenus à l'administration avec la mention " anomalie adresse " ; que le dernier courrier en date du 2 août 2012, enjoignant à M. A...de reprendre le service sous peine de radiation des cadres, a été adressé par l'administration à l'adresse de son domicile familial 105 avenue du Belvédère au Pré-Saint-Gervais ; que le ministre de l'intérieur a radié M. A... des cadres de la police nationale pour abandon de poste par un arrêté du 30 octobre 2012, notifié à l'intéressé à l'adresse du Pré-Saint-Gervais et retourné à l'administration avec la mention " destinataire non identifiable ", mais dont une copie a été envoyée à une adresse située à Nandy où elle a été reçue par l'intéressé ; que, par un jugement du 16 octobre 2013, le tribunal administratif de Paris a annulé l'arrêté litigieux et enjoint au ministre de réintégrer M. A...dans les cadres de la police nationale au motif que la décision de radiation avait été prise au terme d'une procédure irrégulière ; que, par l'arrêt du 29 octobre 2015, contre lequel le ministre de l'intérieur se pourvoit en cassation, la cour administrative d'appel de Paris a rejeté les conclusions du ministre tendant à l'annulation de ce jugement ;<br/>
<br/>
              Sur la régularité de l'arrêt attaqué : <br/>
<br/>
              2. Considérant qu'en vertu de l'article R. 731-3 du code de justice administrative, toute partie à l'instance peut, à l'issue de l'audience, adresser au président de la formation de jugement une note en délibéré ; que l'article R. 741-2 du même code prévoit que cette production est mentionnée dans la décision ; que, eu égard à l'objet de l'obligation ainsi prescrite, qui est de permettre à l'auteur de la note en délibéré de s'assurer que la formation de jugement en a pris connaissance, la circonstance qu'une note en délibéré n'a pas été mentionnée dans la décision, en méconnaissance de cette obligation, ne peut être utilement invoquée pour contester cette décision que par la partie qui a produit cette note ; que, par suite, le ministre ne peut utilement soulever le moyen tiré de ce qu'une note en délibéré produite par M. A...n'aurait pas été visée par l'arrêt attaqué ;<br/>
<br/>
              3. Considérant que le ministre fait valoir que la cour a omis de répondre à un moyen qu'il soulevait dans ses écritures d'appel, tiré de ce que le tribunal administratif avait commis une erreur de droit en inversant la charge de la preuve ; que, toutefois, eu égard à l'office du juge d'appel, un tel moyen était inopérant ; que, par suite, en n'y répondant pas, la cour n'a pas entaché son arrêt d'irrégularité ; que n'est pas davantage de nature à justifier l'annulation de l'arrêt la circonstance que ses visas ne mentionnent pas deux moyens d'appel tirés d'erreurs d'appréciation imputés aux premiers juges, dès lors que, dans ses motifs, la cour a pris parti sur les faits contestés ;  <br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué : <br/>
<br/>
              4. Considérant qu'une mesure de radiation des cadres pour abandon de poste ne peut être régulièrement prononcée que si l'agent concerné a, préalablement à cette décision, été mis en demeure de rejoindre son poste ou de reprendre son service dans un délai qu'il appartient à l'administration de fixer ; qu'une telle mise en demeure doit prendre la forme d'un document écrit, notifié à l'intéressé, l'informant du risque qu'il court d'une radiation des cadres sans procédure disciplinaire préalable ; que lorsque l'agent ne s'est pas présenté et n'a fait connaître à l'administration aucune intention avant l'expiration du délai fixé par la mise en demeure, et en l'absence de toute justification d'ordre matériel ou médical, présentée par l'agent, de nature à expliquer le retard qu'il aurait eu à manifester un lien avec le service, cette administration est en droit d'estimer que le lien avec le service a été rompu du fait de l'intéressé ;<br/>
<br/>
              5. Considérant que l'arrêt attaqué relève que l'administration reconnaît que M. A... a communiqué par téléphone à l'un des agents de la direction territoriale de la sécurité de proximité de Seine-Saint-Denis, qui était son service d'affectation, l'adresse de Nandy " pour le renvoi de son courrier " et que l'administration en a eu connaissance lorsqu'elle a entrepris des démarches pour identifier les nouvelles coordonnées de M. A...après que les plis contenant les mises en demeure ont été retournés par les services postaux ; qu'il relève également " qu'il ne ressort pas des pièces du dossier que M. A...se serait volontairement soustrait à la notification de ces mises en demeure en s'abstenant de communiquer son adresse à son administration " ; qu'en déduisant de ces éléments, constatés dans le cadre de son pouvoir souverain d'appréciation des faits de l'espèce, que, faute d'avoir procédé à une mise en demeure adressée à l'adresse de Nandy, qui lui avait été communiquée, fût-ce selon des modalités différentes de celles que prévoit l'article 24 du décret du 9 mai 1995 fixant les dispositions communes applicables aux fonctionnaires actifs des services de la police nationale, l'administration n'avait pu légalement prendre une mesure de radiation des cadres pour abandon de poste, la cour administrative d'appel n'a pas commis d'erreur de droit ;   <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que le ministre n'est pas fondé à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
 Article 1er : Le pourvoi du ministre de l'intérieur est rejeté.<br/>
<br/>
 Article 2 : La présente décision sera notifiée au ministre d'Etat, ministre de l'intérieur et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
