<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032722803</ID>
<ANCIEN_ID>JG_L_2016_06_000000384356</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/72/28/CETATEXT000032722803.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 13/06/2016, 384356, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-06-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384356</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR>M. Matias de Sainte Lorette</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:384356.20160613</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Auchan France a demandé au tribunal administratif de Lille de condamner l'Etat à lui verser une indemnité en réparation du préjudice financier qu'elle estime avoir subi en raison de la mise en oeuvre des modalités de remboursement des crédits sur le Trésor nés de la suppression de la règle dite du " décalage d'un mois " en matière de taxe sur la valeur ajoutée. Par un jugement n° 0507364 du 14 décembre 2011, le tribunal a fait partiellement droit à cette demande.<br/>
<br/>
              Par un arrêt n° 12DA00297 du 8 juillet 2014, la cour administrative d'appel de Douai a décidé d'inclure la créance n° 32 469 777 000 que détenait la société Auchan France dans la base de l'indemnité définie à l'article 1er de ce jugement, puis a rejeté le surplus des conclusions de la requête d'appel présentée par cette société. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 9 septembre et 9 décembre 2014 et le 22 février 2016 au secrétariat du contentieux du Conseil d'Etat, la société Auchan France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 3 de cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et le premier protocole additionnel à cette convention ;<br/>
              - la sixième directive 77/388/CEE du Conseil des Communautés européennes du 17 mai 1977 en matière d'harmonisation des législations des Etats membres relatives aux taxes sur le chiffre d'affaires ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code civil ;<br/>
              - la loi n° 68-1250 du 31 décembre 1968 ;<br/>
              - la loi n° 93-859 du 22 juin 1993 ;<br/>
              - la loi n° 2008-561 du 17 juin 2008 ;<br/>
              - le décret n° 93-1078 du 14 septembre 1993 ;<br/>
              - le décret n° 94-296 du 6 avril 1994 ;<br/>
              - le décret n° 2002-179 du 13 février 2002 ;<br/>
              - l'arrêté du 15 avril 1994 fixant les modalités de paiement des intérêts des créances résultant de la suppression du décalage d'un mois en matière de taxe sur la valeur ajoutée;<br/>
              - les arrêtés du 17 août 1995 et du 15 mars 1996 fixant les taux d'intérêt applicables à compter du 1er janvier 1994 et du 1er janvier 1995 aux créances résultant de la suppression du décalage d'un mois en matière de taxe sur la valeur ajoutée ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matias de Sainte Lorette, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bouzidi, Bouhanna, avocat de la société Auchan France ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par les dispositions de l'article 2 de la loi du 22 juin 1993 portant loi de finances rectificative pour 1993, le législateur a mis fin à la règle dite du " décalage d'un mois " selon laquelle les assujettis ne pouvaient déduire immédiatement de la taxe sur la valeur ajoutée dont ils étaient redevables la taxe payée sur les biens ne constituant pas des immobilisations et sur les services, la déduction ne pouvant être opérée que le mois suivant. Afin d'étaler sur plusieurs années l'incidence budgétaire de ce changement de règle, qui entraînait l'imputabilité sur la taxe due par les assujettis au titre du premier mois de sa prise d'effet, soit le mois de juillet 1993, de la taxe ayant grevé des biens et services acquis au cours de deux mois, soit les mois de juin et juillet 1993, les dispositions du II du même article 2 de la loi du 22 juin 1993, insérant dans le code général des impôts un article 271 A, ont prévu que, sous réserve d'exceptions et d'aménagements divers, les redevables devaient soustraire du montant de la taxe déductible ainsi déterminé celui d'une " déduction de référence (...) égale à la moyenne mensuelle des droits à déduction afférents aux biens ne constituant pas des immobilisations et aux services qui ont pris naissance au cours du mois de juillet 1993 et des onze mois qui précèdent ", que les droits à déduction de la sorte non exercés ouvriraient aux redevables " une créance (...) sur le Trésor (...) convertie en titres inscrits en compte d'un égal montant ", que des décrets en Conseil d'Etat détermineraient, notamment, les modalités de remboursement de ces titres, ce remboursement devant intervenir " à hauteur de 10 % au minimum pour l'année 1994 et pour les années suivantes de 5 % par an au minimum (...) et dans un délai maximal de vingt ans ", et, enfin, que les créances porteraient intérêt " à un taux fixé par arrêté du ministre du budget sans que ce taux puisse excéder 4,5 % ". Le décret du 14 septembre 1993 a prévu le remboursement dès 1993 de la totalité des créances qui n'excédaient pas 150 000 F et d'une fraction au moins égale à cette somme et au plus égale à 25 % du montant des créances qui l'excédaient, le taux d'intérêt applicable en 1993 étant fixé à 4,5 % par un arrêté du 15 avril 1994. Le décret du 6 avril 1994 a prévu le remboursement du solde des créances à concurrence de 10 % de leur montant initial en 1994 et de 5 % chaque année suivante, le taux d'intérêt étant fixé, par les arrêtés du 17 août 1995 et du 15 mars 1996, à 1 % pour 1994, puis à 0,1 % pour les années suivantes. Enfin, le décret du 13 février 2002 a prévu le remboursement anticipé immédiat des créances non encore soldées et celui des créances non encore portées en compte dès leur inscription.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que, par une réclamation adressée le 30 décembre 2003 au ministre de l'économie, des finances et de l'industrie, la société Auchan France a demandé la réparation du préjudice financier qu'elle estime avoir subi du fait des modalités de remboursement et de l'insuffisante rémunération, au titre des années 1993 à 2002, de la créance qu'elle détenait sur le Trésor public du fait de la suppression de la règle dite du " décalage d'un mois " en matière de taxe sur la valeur ajoutée. Par un jugement du 14 décembre 2011, le tribunal administratif de Lille n'a que partiellement fait droit à ses demandes en lui accordant, au titre des seules années 1999 à 2002, une indemnité d'un montant correspondant à la différence entre la rémunération de cette créance calculée sur la base d'un taux d'intérêt équivalent à la moitié du taux applicable aux obligations assimilables du Trésor et celle qui lui avait été allouée pour chacune de ces années, et a rejeté le surplus de ses demandes, en lui opposant notamment la prescription quadriennale pour les années 1993 à 1998 et en excluant certaines créances dont la société requérante n'établissait pas qu'elles lui avaient été régulièrement transférées. La société se pourvoit en cassation contre l'arrêt du 8 juillet 2014 en tant que la cour administrative d'appel de Douai, après avoir réformé ce jugement pour inclure une créance supplémentaire dans la base d'indemnisation définie par le tribunal, a rejeté le surplus des conclusions de sa requête d'appel.<br/>
<br/>
              3. En premier lieu, il ressort des pièces du dossier soumis aux juges du fond que les indemnités demandées par la société requérante ont pour origine le caractère insuffisant d'une rémunération, fixée par arrêtés du ministre du budget, d'une créance sur le Trésor se substituant à un remboursement d'impôt. De telles indemnités ont la nature d'un bien au sens des stipulations de l'article 1er du protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. La cour, qui a suffisamment motivé son arrêt, n'a pas commis d'erreur de droit en jugeant que le fait que les prétentions d'une société au versement de telles indemnités puissent être soumises, en vertu des dispositions de l'article 1er de la loi du 31 décembre 1968, à un délai de prescription de quatre ans, qui ne présente pas un caractère exagérément court, n'est pas en lui-même incompatible avec ces stipulations.<br/>
<br/>
              4. En deuxième lieu, il ressort des énonciations de l'arrêt attaqué que la société requérante a eu connaissance des taux d'intérêt appliqués au remboursement de la créance qu'elle détenait sur le Trésor public au plus tard lors de la publication des arrêtés les fixant, en date respectivement des 15 avril 1994, 17 août 1995 et 15 mars 1996, et a ainsi été mise en mesure de les contester dès leur publication. Par suite, la cour n'a pas commis d'erreur de droit en jugeant que la circonstance que la société requérante sollicite une indemnisation en se prévalant des stipulations de l'article 1er du protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et qu'elle soutienne n'avoir eu connaissance de ses droits en ce domaine qu'à compter de la publication du décret du 13 février 2002 était sans incidence sur le point de départ du délai de prescription quadriennale, qui a commencé à courir à compter du premier jour de chacune des années suivant celles au cours desquelles étaient nés les droits au paiement de la créance correspondant à la différence entre les intérêts versés en application de ces arrêtés et les intérêts qu'elle estimait lui être dus.<br/>
<br/>
              5. En troisième lieu, la cour n'a pas davantage commis d'erreur de droit en jugeant que le délai de prescription quadriennale n'a été interrompu ni par le versement annuel d'intérêts sur la créance que la société détenait sur le Trésor du fait de la suppression de la règle dite du " décalage d'un mois ", ni par le décret du 13 février 2002 prévoyant le remboursement anticipé immédiat de cette créance.<br/>
<br/>
              6. En quatrième lieu, il résulte des termes mêmes du premier alinéa de l'article L. 80 A du livre des procédures fiscales qu'une instruction fiscale ne peut être invoquée sur le fondement de cet alinéa que par un contribuable ayant supporté un rehaussement d'impositions antérieures. Dès lors que le présent litige est relatif à la réparation du préjudice financier né de la rémunération insuffisante de la créance que la société requérante détenait sur le Trésor, la cour n'était pas tenue de répondre au moyen inopérant par lequel la société invoquait, sur le fondement de ces dispositions, l'instruction 3D-1-226 du 2 novembre 1996.<br/>
<br/>
              7. En dernier lieu, il ne ressort pas des pièces du dossier soumis aux juges du fond que la cour aurait dénaturé celles-ci en jugeant que la société n'avait pas établi, par les documents qu'elle a produits, le montant des droits qu'elle indiquait détenir au titre de certaines créances.<br/>
<br/>
              8. Il résulte de tout ce qui précède que la société Auchan France n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. Par suite, son pourvoi ne peut qu'être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Le pourvoi de la société Auchan France est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la société Auchan France et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
