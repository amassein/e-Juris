<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033508874</ID>
<ANCIEN_ID>JG_L_2016_11_000000394974</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/50/88/CETATEXT000033508874.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 30/11/2016, 394974, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-11-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394974</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>M. Stéphane Decubber</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:394974.20161130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et deux mémoires en réplique, enregistrés le 2 décembre 2015 et les 24 et 28 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, le syndicat des pilotes de ligne France ALPA demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 29 septembre 2015 de la ministre de l'écologie, du développement durable et de l'énergie, relatif aux membres d'équipage technique des opérations d'hélitreuillage et des opérations du service médical d'urgence par hélicoptère ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le règlement (CE) n° 216/2008 du Parlement européen et du Conseil du 20 février 2008 ;<br/>
              - le règlement (UE) n° 965/2012 de la Commission du 5 octobre 2012 ; <br/>
              - le code de l'aviation civile ;<br/>
              - le code des transports ;<br/>
              - l'ordonnance n° 2010-1307 du 28 octobre 2010 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Stéphane Decubber, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouin-Palat, Boucard, avocat du syndicat des pilotes de ligne France ALPA et à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 6511-11 du code des transports : " Le personnel navigant est soumis au présent titre et aux dispositions du règlement (CE) n° 216/2008 du Parlement européen et du Conseil, du 20 février 2008, concernant les règles communes dans le domaine de l'aviation civile et instituant une Agence européenne de la sécurité aérienne (...) ainsi qu'aux dispositions des règlements pris pour son application par la Commission européenne. " ; qu'aux termes de l'article L. 6521-1 de ce code : " Est navigant professionnel de l'aéronautique civile toute personne exerçant de façon habituelle et principale (...) l'une des fonctions suivantes : / 1° Commandement et conduite des aéronefs ; / 2° Service à bord des moteurs, machines et instruments divers nécessaires à la navigation de l'aéronef ; / 3° Service à bord des autres appareils montés sur aéronefs... ; / 4° Services complémentaires de bord comprenant, notamment, le personnel navigant commercial du transport aérien. " ; qu'aux termes de l'article L. 6521-2 du même code : " Nul ne peut faire partie du personnel navigant professionnel de l'aéronautique civile s'il n'est : 1° Titulaire d'un titre aéronautique en état de validité (...). " ;<br/>
<br/>
              2. Considérant que l'arrêté contesté dispose, d'une part, que les membres d'équipage technique des opérations d'hélitreuillage, qui assurent des tâches liées à l'utilisation d'un treuil, et les membres d'équipage technique des opérations du service médical d'urgence par hélicoptère, qui assistent le pilote pendant un vol de service médical d'urgence par hélicoptère, font partie des personnels navigants mentionnés, respectivement, au 3° et au 4° de l'article L. 6521-1 du code des transports ; qu'il dispose, d'autre part, que les documents établis par les exploitants agréés selon les sous-parties I ou J de l'annexe V du règlement (UE) n° 965/2012 attestant que ces personnels ont suivi les formations applicables et subi avec succès les évaluations requises par ce règlement sont assimilés au titre mentionné à l'article L. 6521-2 du code des transports pour l'application de ce même code ;<br/>
<br/>
              Sur la légalité externe de l'arrêté attaqué :<br/>
<br/>
              3. Considérant qu'aux termes de l'article R. 410-1 du code de l'aviation civile : " Les conditions dans lesquelles les personnels visés à l'article L. 410-1 doivent être pourvus de titres aéronautiques et de qualifications sont fixées, après avis du conseil du personnel navigant professionnel de l'aéronautique civile, par arrêté du ministre chargé de l'aviation civile et, dans le domaine des essais et réceptions, par arrêté conjoint du ministre chargé de l'aviation civile et du ministre de la défense. Toutefois, l'avis du conseil du personnel navigant professionnel de l'aéronautique civile n'est pas requis pour les dispositions relatives aux conditions médicales d'aptitude et pour les dispositions relatives au personnel navigant non professionnel. " ; qu'aux termes du premier alinéa de l'article L. 6511-1 du code des transports, qui reprend la substance du premier alinéa de l'article L. 410-1 du code de l'aviation civile, abrogé par l'article 7 de l'ordonnance du 28 octobre 2010 relative à la partie législative du code des transports : " Le commandant, les pilotes, les mécaniciens et toute personne assurant la conduite d'un aéronef doivent être pourvus de titres aéronautiques et de qualifications dans des conditions déterminées par voie réglementaire. " ; <br/>
<br/>
              4. Considérant qu'il résulte de ces dispositions combinées que le ministre chargé de l'aviation civile n'était pas tenu de consulter le conseil du personnel navigant professionnel de l'aéronautique civile avant de fixer les conditions dans lesquelles les membres d'équipage technique des opérations d'hélitreuillage et des opérations du service médical d'urgence par hélicoptère doivent être pourvus de titres aéronautiques et de qualifications, dès lors que ces personnels, qui n'appartiennent pas à l'équipage de conduite mais à l'équipage technique et ne sauraient donc accomplir de tâches relevant du pilotage, ne sont pas au nombre de ceux visés à l'article L. 6511-1 du code des transports ; que le moyen tiré de ce que l'arrêté attaqué aurait été pris au terme d'une procédure irrégulière, faute pour le conseil du personnel navigant professionnel de l'aéronautique civile d'avoir été consulté, doit, par suite, être écarté comme inopérant ; <br/>
<br/>
              Sur la légalité interne de l'arrêté attaqué :<br/>
<br/>
              5. Considérant, d'une part, qu'ainsi qu'il a été dit ci-dessus, les membres d'équipage technique des opérations d'hélitreuillage et des opérations du service médical d'urgence par hélicoptère n'appartiennent pas à l'équipage de conduite mais à l'équipage technique et ne sauraient donc accomplir de tâches relevant du commandement et de la conduite des aéronefs ; que le syndicat requérant n'est donc pas fondé à soutenir que l'arrêté attaqué aurait méconnu les dispositions l'article L. 6521-2 du code de l'aviation civile en incluant ces membres d'équipage au sein des personnels navigants au titre des 3° et 4° de cet article et non au titre de son 1° ; que les moyens tirés de ce que l'arrêté attaqué permettrait aux personnels concernés de prendre part à la conduite de l'appareil tout en ne justifiant ni d'aptitudes physiques identiques à celles du personnel de pilotage ni d'une formation adéquate ne peuvent, par suite, qu'être écartés ;<br/>
<br/>
              6. Considérant, d'autre part, que si l'article L. 6521-2 du code de l'aviation civile impose que les membres du personnel navigant professionnel de l'aéronautique civile soient titulaires d'un titre aéronautique en état de validité, l'article 2 de l'arrêté attaqué prévoit que, pour les membres d'équipage technique des opérations d'hélitreuillage et des opérations du service médical d'urgence par hélicoptère, les documents établis par les exploitants agréés attestant que ces personnels ont suivi les formations applicables et subi avec succès les évaluations requises sont assimilés à un tel titre ; que le syndicat requérant n'est, par suite, pas fondé à soutenir que l'arrêté du 29 septembre 2015 méconnaîtrait les dispositions de l'article L. 6521-2 du code de l'aviation civile en tant qu'il dispense les personnels concernés de la détention d'un titre aéronautique ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que le syndicat des pilotes de ligne France ALPA n'est pas fondé à demander l'annulation de l'arrêté qu'il attaque ; que sa requête ne peut, par suite, qu'être rejetée, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le ministre chargé de l'environnement au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête du syndicat des pilotes de ligne France ALPA est rejetée. <br/>
<br/>
Article 2 : Les conclusions de la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat, présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée au syndicat des pilotes de ligne France ALPA et à la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
