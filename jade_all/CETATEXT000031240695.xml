<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031240695</ID>
<ANCIEN_ID>JG_L_2015_09_000000370687</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/24/06/CETATEXT000031240695.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème / 10ème SSR, 25/09/2015, 370687, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-09-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370687</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème / 10ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Julien Anfruns</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:370687.20150925</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Centrale des Artisans Coiffeurs a demandé au tribunal administratif de Strasbourg la décharge du supplément d'impôt sur les sociétés et de l'imposition forfaitaire annuelle auxquels elle a été assujettie au titre des années 2003 à 2006 ainsi que la restitution de l'impôt acquitté à tort sur les sommes portées en réserve. Par un jugement n° 0700754 du 30 juin 2011, le tribunal administratif de Strasbourg l'a déchargée du supplément d'impôt sur les sociétés auquel elle a été assujettie au titre des années 2003, 2004 et 2005 et de l'imposition forfaitaire annuelle à laquelle elle a été assujettie pour les années 2003 à 2006.<br/>
<br/>
              Par un arrêt n° 11NC01412, 11NC01421 du 30 mai 2013, la cour administrative d'appel de Nancy, saisie d'appels du ministre de l'économie et des finances et de la Centrale des Artisans Coiffeurs, a prononcé un non-lieu à statuer sur les conclusions de cette dernière tendant à la restitution des impositions versées spontanément au titre de l'année 2005 (article 1er), annulé le jugement du 30 juin 2011 du tribunal administratif de Strasbourg (article 2), remis à sa charge les cotisations supplémentaires d'impôt sur les sociétés et l'imposition forfaitaire annuelle auxquelles elle a été assujettie au titre des années 2003 à 2006 (article 3) et rejeté sa demande et sa requête (article 4).<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 30 juillet 2013, 30 octobre 2013 et 15 juin 2015 au secrétariat du contentieux du Conseil d'État, la Centrale des Artisans Coiffeurs demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du ministre et de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'État une somme de 5.000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 83-657 du 20 juillet 1983 ;<br/>
              - la loi locale du 1er mai 1889 sur les associations coopératives de production et de consommation ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Julien Anfruns, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la Centrale des Artisans Coiffeurs ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond, et notamment des statuts de la Centrale des Artisans Coiffeurs, que cette dernière est une société coopérative à responsabilité limitée régie par la loi locale du 1er mai 1889 sur les associations coopératives de production et de consommation, modifiée le 20 mai 1898 ; qu'elle a pour objet " l'achat des marchandises et petit outillage, en gros l'achat des appareils et mobiliers, pour les fournir à ses membres, notamment coiffeurs et esthéticiens légalement établis ", ainsi que " l'étude et la surveillance des agencements de magasins de coiffure et d'esthétique " ; que par un jugement du 30 juin 2011, le tribunal administratif de Strasbourg a déchargé la Centrale des Artisans Coiffeurs du supplément d'impôt sur les sociétés auquel elle avait été assujettie au titre des années 2003, 2004 et 2005 et de l'imposition forfaitaire annuelle à laquelle elle avait été assujettie pour les années 2003 à 2006 ; que par l'arrêt attaqué du 30 mai 2013, la cour administrative d'appel de Nancy a prononcé un non-lieu à statuer sur les conclusions de la Centrale des Artisans Coiffeurs tendant à la restitution des impositions versées spontanément au titre de l'année 2005 en raison du dégrèvement de 206 499 euros accordé par l'administration fiscale, annulé le jugement du tribunal administratif de Strasbourg du 30 juin 2011 et a remis à la charge de la Centrale des Artisans Coiffeurs les cotisations supplémentaires d'impôt sur les sociétés et d'imposition forfaitaire annuelle auxquelles cette dernière a été assujettie au titre des années 2003 à 2006 ;<br/>
<br/>
              Sur la régularité de l'arrêté attaqué<br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article R. 732-1 du code de justice administrative : " Après le rapport qui est fait sur chaque affaire par un membre de la formation de jugement ou par le magistrat mentionné à l'article R. 222-13, le rapporteur public prononce ses conclusions lorsque le présent code l'impose. Les parties peuvent ensuite présenter, soit en personne, soit par un avocat au Conseil d'Etat et à la Cour de cassation, soit par un avocat, des observations orales à l'appui de leurs conclusions écrites " ; qu'aux termes de l'article R. 741-2 du même code :  " La décision mentionne que l'audience a été publique (...) / Elle contient le nom des parties, l'analyse des conclusions et mémoires ainsi que les visas des dispositions législatives ou réglementaires dont elle fait application. / Mention y est faite que le rapporteur et le rapporteur public et, s'il y a lieu, les parties, leurs mandataires ou défenseurs ainsi que toute personne entendue sur décision du président en vertu du deuxième alinéa de l'article R. 731-3 ont été entendus. / (...) Mention est également faite de la production d'une note en délibéré. / La décision fait apparaître la date de l'audience et la date à laquelle elle a été prononcée " ; que si ces dispositions imposent que toute personne entendue soit mentionnée par la décision, elles ne font, en revanche, pas obligation à celle-ci de mentionner que les parties ou leurs mandataires ont présenté leurs observations après le prononcé des conclusions du rapporteur public ;<br/>
<br/>
              3. Considérant que l'arrêt attaqué mentionne que le directeur de la Centrale des Artisans Coiffeurs, M.A..., a été entendu au cours de l'audience publique du 21 février 2013 ; qu'il résulte de ce qui a été dit au point 2 que la circonstance que les visas de l'arrêt attaqué ne mentionnent pas que le directeur de la Centrale des Artisans Coiffeurs a été informé de la possibilité de présenter, s'il le souhaitait, des observations orales après les conclusions du rapporteur public, dans les conditions fixées par les dispositions de l'article R. 732-1 du code de justice administrative, n'est pas de nature à entacher cet arrêt d'irrégularité ;<br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il statue sur le bien-fondé de l'imposition<br/>
<br/>
              En ce qui concerne les motifs de l'arrêt attaqué relatifs à l'impôt sur les sociétés<br/>
<br/>
              4. Considérant, qu'aux termes du 1 de l'article 207 du code général des impôts : " Sont exonérés de l'impôt sur les sociétés:/ (...) 3° bis. Lorsqu'elles fonctionnent conformément aux dispositions de la loi n° 83-657 du 20 juillet 1983 relative au développement de certaines activités d'économie sociale, les coopératives artisanales et leurs unions (...), sauf pour les affaires effectuées avec des non-sociétaires " ; que le titre premier de la loi du 20 juillet 1983 fixe le statut des coopératives et de leurs unions ; qu'aux termes de l'article 32 de la même loi : " Les sociétés coopératives d'artisans et leurs unions, existant à la date de publication de la présente loi, disposent d'un délai de deux ans à partir de cette date pour mettre leurs statuts en conformité avec ses dispositions. (...) Les coopératives créées en application de la loi locale du 20 mai 1898 dont le siège est fixé dans les départements du Haut-Rhin, du Bas-Rhin et de la Moselle ont la faculté de conserver le bénéfice des dispositions de ladite loi. Cette option est également ouverte aux coopératives créées après l'entrée en vigueur de la présente loi. " ;<br/>
<br/>
              5. Considérant qu'il résulte des termes mêmes du 3° bis du 1 de l'article 207 du code général des impôts que le bénéfice de l'exonération qu'il prévoit est réservé aux coopératives artisanales fonctionnant conformément aux dispositions de la loi du 20 juillet 1983 ; que la Centrale des Artisans Coiffeurs, société coopérative à responsabilité limitée, a fait usage de la faculté offerte par l'article 32 de la loi du 20 juillet 1983 de rester régie par les dispositions de la loi locale du 1er mai 1889 modifiée le 20 mai 1898 sur les associations coopératives de production et de consommation ; que, dès lors, elle n'est pas au nombre des coopératives artisanales mentionnées au 3° bis du 1 de l'article 207 du code général des impôts et n'entre ainsi pas dans le champ d'application de cet article ; que ce motif, qui est d'ordre public et dont l'examen n'implique l'appréciation d'aucune circonstance de fait, doit être substitué au motif retenu par le jugement attaqué, dont il justifie le dispositif ; que, par suite, le moyen tiré de ce que la cour aurait commis une erreur de droit en jugeant que la Centrale des Artisans Coiffeurs ne répondait pas aux conditions prévues par l'article 10 de la loi du 20 juillet 1983 et ne pouvait ainsi bénéficier de l'exonération prévue au 3° bis du 1 de l'article 207 du code général des impôts ne peut qu'être écarté ;<br/>
<br/>
              En ce qui concerne les motifs de l'arrêt attaqué relatifs à l'imposition forfaitaire annuelle<br/>
<br/>
              6. Considérant qu'aux termes de l'article 223 septies du code général des impôts : " Les personnes morales passibles de l'impôt sur les sociétés sont assujetties à une imposition forfaitaire annuelle (...)/ Cette imposition n'est pas applicable aux organismes sans but lucratif visés au 5 de l'article 206 ainsi qu'aux personnes morales exonérées de l'impôt sur les sociétés en vertu des articles 207 et 208. (...) " ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui est dit au point 5 que la Centrale des Artisans Coiffeurs n'est pas exonérée de l'impôt sur les sociétés en vertu de l'article 207 du code général des impôts ; que, par suite, elle n'est pas fondée à soutenir que la cour administrative d'appel de Nancy a méconnu les dispositions de l'article 223 septies du code général des impôts en remettant à sa charge l'imposition forfaitaire annuelle au titre des années 2003 à 2006 ;<br/>
<br/>
              8. Considérant que dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat, qui n'est pas, dans la présente espèce, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de la Centrale des Artisans Coiffeur est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la Centrale des Artisans Coiffeurs et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
