<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041807002</ID>
<ANCIEN_ID>JG_L_2020_04_000000434531</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/80/70/CETATEXT000041807002.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 09/04/2020, 434531, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-04-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434531</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Cécile Chaduteau-Monplaisir</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:434531.20200409</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le syndicat des copropriétaires de la copropriété Brotteaux An 2000 - Immeuble A - 18, rue de la Gaîté, M. M... F..., M. et Mme K... et Sophie Hadjur, Mme G... L... épouse J..., M. et Mme E... et Michèle Lalechère, Mme H... D..., M. I... B..., M. M... C..., M. et Mme A... et Monique Savarin et la société civile immobilière Galyas ont demandé au tribunal administratif de Lyon d'annuler pour excès de pouvoir l'arrêté du 7 février 2018 par lequel le maire de Lyon a délivré à la société Eiffage Immobilier Centre-Est le permis de construire, après la démolition d'une construction existante, un ensemble immobilier comprenant 54 logements, des commerces, des bureaux et des emplacements de stationnement, sur un terrain situé 51, rue Bellecombe dans le 6ème arrondissement, à Lyon, ainsi que la décision du 16 mai 2018 rejetant leur recours gracieux. Par un jugement n° 1805554 du 11 juillet 2019, le tribunal administratif de Lyon a annulé l'arrêté du 7 février 2018 en tant qu'il ne prévoit pas une superficie suffisante des locaux dédiés aux deux roues et des espaces verts plantés en pleine terre et en tant qu'il ne comporte pas la création de 30 % de logements aidés, en fixant à trois mois le délai dans lequel la société Eiffage Immobilier Centre-Est pourrait demander la régularisation du permis de construire en application de l'article L. 600-5 du code de l'urbanisme.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 11 septembre et 11 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, le syndicat des copropriétaires de la copropriété Brotteaux An 2000 - Immeuble A - 18, rue de la Gaîté et les autres demandeurs de première instance demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il n'a fait que partiellement droit aux conclusions de leur demande ;<br/>
<br/>
              2°) réglant l'affaire au fond, d'annuler le permis de construire attaqué dans son entier ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Lyon et de la société Eiffage Immobilier Centre-Est la somme de 6 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative, au titre des frais exposés devant le tribunal administratif de Lyon et devant le Conseil d'Etat.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts ;<br/>
              - le code de l'urbanisme ;<br/>
              - le décret n° 2013-392 du 10 mai 2013 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Chaduteau-Monplaisir, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat du syndicat des copropriétaires de la copropriété Brotteaux An 2000 - Immeuble A - 18, rue de la Gaîté et autres ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. En vertu des dispositions de l'article R. 811-1-1 du code de justice administrative, les tribunaux administratifs statuent en dernier ressort sur les recours, introduits entre le 1er décembre 2013 et 31 décembre 2022, dirigés contre " les permis de construire ou de démolir un bâtiment à usage principal d'habitation (...) lorsque le bâtiment (...) est implanté en tout ou partie sur le territoire d'une des communes mentionnées à l'article 232 du code général des impôts et son décret d'application (...) ". Ces dispositions, qui ont pour objectif, dans les zones où la tension entre l'offre et la demande de logements est particulièrement vive, de réduire le délai de traitement des recours pouvant retarder la réalisation d'opérations de construction de logements, dérogent aux dispositions du premier alinéa de l'article R. 811-1 du même code, aux termes desquelles : " Toute partie présente dans une instance devant le tribunal administratif (...) peut interjeter appel contre toute décision juridictionnelle rendue dans cette instance ". Pour leur application dans le cas où la construction autorisée est destinée à différents usages, doit être regardé comme un bâtiment à usage principal d'habitation celui dont plus de la moitié de la surface de plancher est destinée à l'habitation.<br/>
<br/>
              2. En l'espèce, si la commune de Lyon figure sur la liste annexée au décret du 10 mai 2013 relatif au champ d'application de la taxe annuelle sur les logements vacants instituée par l'article 232 du code général des impôts, il ressort des pièces du dossier que le permis de construire délivré le 7 février 2018 par son maire à la société Eiffage Immobilier Centre-Est autorise une construction destinée à différents usages dont moins de la moitié de la surface de plancher est destinée à l'habitation. Par suite, ce permis ne peut être regardé comme relatif à un bâtiment à usage principal d'habitation, au sens de l'article R. 811-1-1 du code de justice administrative cité ci-dessus.<br/>
<br/>
              3. Il résulte de ce qui précède que le tribunal administratif de Lyon n'a pas statué en dernier ressort sur la demande formée par le syndicat de copropriétaires de la copropriété Brotteaux An 2000 - Immeuble A - 18, rue de la Gaîté et les autres demandeurs de première instance devant ce tribunal, enregistrée le 20 juin 2018, tendant à l'annulation de cet arrêté ainsi qu'à celle de la décision du 16 mai 2018 rejetant leur recours gracieux. Par suite, la requête des demandeurs de première instance tendant à l'annulation du jugement de ce tribunal a le caractère d'un appel, qui relève de la compétence de la cour administrative d'appel de Lyon.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement de la requête du syndicat des copropriétaires de la copropriété Brotteaux An 2000 - Immeuble A - 18, rue de la Gaîté et des autres requérants est transmis à la cour administrative d'appel de Lyon.<br/>
Article 2 : La présente décision sera notifiée au syndicat des copropriétaires de la copropriété Brotteaux An 2000 - Immeuble A - 18, rue de la Gaîté, premier dénommé, pour l'ensemble des requérants et au président de la cour administrative d'appel de Lyon.<br/>
Copie en sera adressée à la commune de Lyon et à la société Eiffage Immobilier Centre-Est.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
