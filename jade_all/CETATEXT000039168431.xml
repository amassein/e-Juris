<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039168431</ID>
<ANCIEN_ID>JG_L_2019_10_000000418930</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/16/84/CETATEXT000039168431.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 02/10/2019, 418930</TITRE>
<DATE_DEC>2019-10-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418930</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Nevache</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:418930.20191002</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Nîmes d'annuler la décision du 20 juillet 2016 par laquelle le président du conseil départemental du Gard a confirmé la fin de ses droits à l'allocation de revenu de solidarité active. Par un jugement n° 1602386 du 28 février 2018, le tribunal administratif de Nîmes rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 12 mars et 5 septembre 2018 et le 10 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) de mettre à la charge du département du Gard la somme de 3 000 euros, à verser à la SCP Monod, Colin, Stoclet, son avocat, au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - la loi n° 2008-1249 du 1er décembre 2008 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Nevache, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de M. A... et à la SCP Lyon-Caen, Thiriez, avocat du département du Gard ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que M. A..., bénéficiaire du revenu minimum d'insertion depuis le 21 janvier 1989, puis du revenu de solidarité active depuis le 1er juin 2009, a atteint l'âge de soixante-cinq ans le 9 janvier 2015. A la suite d'un premier recours gracieux de M. A... contre une décision du 26 mai 2015 par laquelle la caisse d'allocations familiales du Gard avait mis fin à son droit au revenu de solidarité active au motif qu'il n'avait pas accompli les démarches nécessaires pour faire valoir ses droits à la retraite, le département du Gard a décidé, le 5 octobre 2015, de reprendre le versement de cette prestation à compter du mois de février 2015, puis lui a proposé, par des courriers des 9 et 20 octobre et 24 novembre 2015, différents rendez-vous pour faire le point sur sa situation et l'aider dans ses démarches. Par un courrier du 14 octobre 2015, la caisse d'allocations familiales a confirmé la mise en place d'un accompagnement social et, par un courrier du 15 décembre 2015, le département a informé M. A... de ce que son droit au revenu de solidarité active avait été rétabli pour une durée de quatre mois pour lui permettre de faire valoir ses droits à prestations. En l'absence de démarche de l'intéressé, la caisse d'allocations familiales a suspendu son droit au revenu de solidarité active par une décision du 15 avril 2016, confirmée le 20 juillet 2016 par une décision du président du conseil départemental du Gard rejetant le recours gracieux de M. A.... Celui-ci se pourvoit en cassation contre le jugement du 28 février 2018 par lequel le tribunal administratif de Nîmes a rejeté sa demande tendant à l'annulation de cette décision. <br/>
<br/>
              2. En premier lieu, aux termes de l'article L. 262-2 du code de l'action sociale et des familles : " Toute personne résidant en France de manière stable et effective, dont le foyer dispose de ressources inférieures à un montant forfaitaire, a droit au revenu de solidarité active dans les conditions définies au présent chapitre. / Le revenu de solidarité active est une allocation qui porte les ressources du foyer au niveau du montant forfaitaire (...) ". Aux termes de l'article L. 262-10 du même code : " Le droit au revenu de solidarité active est subordonné à la condition que le foyer fasse valoir ses droits aux prestations sociales, législatives, réglementaires et conventionnelles, à l'exception des allocations mensuelles mentionnées à l'article L. 222-3 et, sauf pour les personnes reconnues inaptes au travail dont l'âge excède celui mentionné au premier alinéa de l'article L. 351-1 du code de la sécurité sociale, des pensions de vieillesse des régimes légalement obligatoires. (...) ". Ainsi qu'il résulte des travaux préparatoires de la loi du 1er décembre 2008 généralisant le revenu de solidarité active et réformant les politiques d'insertion dont elles sont issues, le législateur a entendu, par ces dispositions, permettre aux bénéficiaires du revenu de solidarité active ayant atteint l'âge d'ouverture du droit à pension de retraite, mais ne justifiant pas de la durée requise d'assurance pour bénéficier d'un taux plein, d'attendre, pour liquider leur pension, l'âge auquel ils bénéficieraient de ce taux.<br/>
<br/>
              3. Aux termes de l'article L. 815-1 du code de la sécurité sociale : " Toute personne justifiant d'une résidence stable et régulière sur le territoire métropolitain ou dans une collectivité mentionnée à l'article L. 751-1 et ayant atteint un âge minimum bénéficie d'une allocation de solidarité aux personnes âgées dans les conditions prévues par le présent chapitre. Cet âge minimum est abaissé en cas d'inaptitude au travail. (...) ". Cet âge est fixé par l'article R. 815-1 du même code à soixante-cinq ans et est abaissé à l'âge d'ouverture du droit à une pension de retraite prévu à l'article L. 161-17-2 de ce code pour les personnes qui, en vertu de son article L. 351-8, bénéficient dès cet âge du taux plein quelle que soit leur durée d'assurance. Aux termes de l'article L. 815-5 du même code : " La personne âgée et, le cas échéant, son conjoint ou concubin ou partenaire lié par un pacte civil de solidarité doivent faire valoir en priorité les droits en matière d'avantages de vieillesse auxquels ils peuvent prétendre au titre de dispositions législatives ou réglementaires françaises ou étrangères, des conventions internationales, ainsi que des régimes propres aux organisations internationales ". Enfin, en vertu de l'article L. 815-9 du même code, l'allocation de solidarité aux personnes âgées est une allocation différentielle qui porte le total de cette allocation et des ressources personnelles de l'intéressé et de son conjoint, concubin ou partenaire lié par un pacte civil de solidarité au niveau de plafonds fixés par décret. <br/>
<br/>
              4. Si le bénéfice de l'allocation de solidarité aux personnes âgées, qui revêt le caractère d'une prestation sociale au sens de l'article L. 262-10 du code de l'action sociale et des familles, est subordonné à la condition d'avoir fait valoir ses droits en matière d'avantages de vieillesse, elle ne peut toutefois être regardée comme une pension de vieillesse. Par suite, il résulte de la combinaison des dispositions mentionnées aux points 2 et 3 que le droit au revenu de solidarité active est subordonné, pour les personnes qui remplissent les conditions pour en bénéficier, à la condition de faire valoir leurs droits à cette allocation, sauf à ce qu'elles ne remplissent pas encore les conditions pour bénéficier de la liquidation d'une pension de retraite à taux plein. <br/>
<br/>
              5. Il suit de là que le tribunal administratif de Nîmes, qui a suffisamment motivé son jugement et n'a pas relevé d'office un moyen qui n'aurait pas été soulevé par les parties, n'a pas commis d'erreur de droit en jugeant que M. A..., qui, ainsi qu'il ressortait des pièces du dossier qui lui était soumis, avait atteint l'âge de soixante-cinq ans, correspondant, pour les assurés nés comme lui en 1950, à l'âge auquel ils bénéficient du taux plein même s'ils ne justifient pas de la durée requise d'assurance, et remplissait, eu égard au montant de l'allocation de revenu de solidarité active dont il bénéficiait jusque-là, la condition de ressources mentionnée à l'article L. 815-9 du code de la sécurité sociale, devait faire valoir ses droits à l'allocation de solidarité aux personnes âgées. <br/>
<br/>
              6. En second lieu, il résulte des articles L. 262-10 et L. 262-11 du code de l'action sociale et des familles que le droit au revenu de solidarité active est subordonné à la condition que le foyer fasse valoir ses droits, notamment, aux prestations sociales que ces dispositions mentionnent et que, lorsque les démarches nécessaires à cette fin sont engagées, l'organisme chargé du service du revenu de solidarité active, qui assiste le demandeur dans ces démarches, sert ce revenu à titre d'avance en étant subrogé, pour le compte du département, dans les droits du demandeur à l'égard des organismes sociaux. Il résulte également de ces dispositions et de celles des articles R. 262-46, R. 262-47 et R. 262-49 du même code que si le bénéficiaire qui acquiert des droits aux prestations sociales dont il ne disposait pas lors de l'ouverture du droit à l'allocation de revenu de solidarité active ne fait pas valoir ses droits à prestations dans un délai de deux mois à compter de l'injonction qui lui en est faite par le président du conseil départemental, ce dernier peut, dans les conditions prévues par ces articles, mettre fin au versement de l'allocation ou en réduire le montant.<br/>
<br/>
              7. Le tribunal administratif de Nîmes a retenu, sans dénaturer les pièces du dossier, que M. A... avait été invité, à plusieurs reprises, à prendre un rendez-vous avec un conseiller de la caisse d'allocations familiales et une assistante sociale des services du département du Gard afin de bénéficier d'un accompagnement social, en vue de lui permettre de faire valoir ses droits aux prestations sociales, et avait bénéficié de la prorogation de son droit au revenu de solidarité active afin de lui permettre d'accomplir les démarches nécessaires pour obtenir les avantages vieillesse auxquels il pouvait prétendre à compter de son soixante-cinquième anniversaire, sans avoir accompli ces démarches. Il n'a pas commis d'erreur de droit en en déduisant que M. A... n'avait pas satisfait à l'obligation prescrite à l'article L. 262-10 du code de l'action sociale et des familles et que le département du Gard avait pu mettre fin au versement de l'allocation de revenu de solidarité active.<br/>
<br/>
              8. Il résulte de ce tout qui précède que M. A... n'est pas fondé à demander l'annulation du jugement qu'il attaque.<br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du département du Gard, qui n'est pas, dans la présente instance, la partie perdante. Dans les circonstances de l'espèce, il n'y a pas lieu de mettre à la charge de M. A... la somme demandée par le département du Gard au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A... est rejeté. <br/>
Article 2 : Les conclusions du département du Gard présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. B... A... et au département du Gard.<br/>
Copie en sera adressée à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-06 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. REVENU MINIMUM D'INSERTION (RMI). - RSA - 1) POSSIBILITÉ POUR LES BÉNÉFICIAIRES AYANT ATTEINT L'ÂGE D'OUVERTURE DU DROIT À PENSION DE RETRAITE D'ATTENDRE, POUR LIQUIDER LEUR PENSION, L'ÂGE AUQUEL ILS BÉNÉFICIERAIENT D'UN TAUX PLEIN - 2) DROIT AU RSA SUBORDONNÉ À LA CONDITION DE FAIRE VALOIR SES DROITS À L'ASPA, SAUF À CE QUE L'INTÉRESSÉ NE REMPLISSE PAS ENCORE LES CONDITIONS POUR BÉNÉFICIER DE LA LIQUIDATION D'UNE PENSION DE RETRAITE À TAUX PLEIN.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">62-04-07-01 SÉCURITÉ SOCIALE. PRESTATIONS. ALLOCATIONS DE SÉCURITÉ SOCIALE DIVERSES. ALLOCATIONS AUX PERSONNES ÂGÉES. - ASPA - PRESTATION SOCIALE SUBORDONNÉE À LA CONDITON D'AVOIR FAIT VALOIR SES DROITS EN MATIÈRE D'AVANTAGES DE VIEILLESSE - CONSÉQUENCE - BÉNÉFICE DU RSA SUBORDONNÉ À LA CONDITION DE FAIRE VALOIR SES DROITS À L'ASPA, SAUF À CE QUE L'INTÉRESSÉ NE REMPLISSE PAS ENCORE LES CONDITIONS POUR BÉNÉFICIER DE LA LIQUIDATION D'UNE PENSION DE RETRAITE À TAUX PLEIN.
</SCT>
<ANA ID="9A"> 04-02-06 1) Ainsi qu'il résulte des travaux préparatoires de la loi n° 2008-1249 du 1er décembre 2008, le législateur a entendu, par les dispositions des articles L. 262-2 et L. 262-10 du code de l'action sociale et des familles (CASF), permettre aux bénéficiaires du revenu de solidarité active (RSA) ayant atteint l'âge d'ouverture du droit à pension de retraite, mais ne justifiant pas de la durée requise d'assurance pour bénéficier d'un taux plein, d'attendre, pour liquider leur pension, l'âge auquel ils bénéficieraient de ce taux.,,,2) Si le bénéfice de l'allocation de solidarité aux personnes âgées (ASPA), qui revêt le caractère d'une prestation sociale au sens de l'article L. 262-10 du CASF, est subordonné à la condition d'avoir fait valoir ses droits en matière d'avantages de vieillesse, elle ne peut toutefois être regardée comme une pension de vieillesse. Par suite, il résulte de la combinaison des articles L. 262-2, L. 262-10 du CASF, L. 815-1, L. 815-5 et L. 815-9 du code de la sécurité sociale (CSS) que le droit au RSA est subordonné, pour les personnes qui remplissent les conditions pour en bénéficier, à la condition de faire valoir leurs droits à cette allocation, sauf à ce qu'elles ne remplissent pas encore les conditions pour bénéficier de la liquidation d'une pension de retraite à taux plein.</ANA>
<ANA ID="9B"> 62-04-07-01 Ainsi qu'il résulte des travaux préparatoires de la loi n° 2008-1249 du 1er décembre 2008, le législateur a entendu, par les dispositions des articles L. 262-2 et L. 262-10 du code de l'action sociale et des familles (CASF), permettre aux bénéficiaires du revenu de solidarité active (RSA) ayant atteint l'âge d'ouverture du droit à pension de retraite, mais ne justifiant pas de la durée requise d'assurance pour bénéficier d'un taux plein, d'attendre, pour liquider leur pension, l'âge auquel ils bénéficieraient de ce taux.,,,Si le bénéfice de l'allocation de solidarité aux personnes âgées (ASPA), qui revêt le caractère d'une prestation sociale au sens de l'article L. 262-10 du CASF, est subordonné à la condition d'avoir fait valoir ses droits en matière d'avantages de vieillesse, elle ne peut toutefois être regardée comme une pension de vieillesse. Par suite, il résulte de la combinaison des articles L. 262-2, 2-10 du CASF,  815-5 et L. 815-9 du CSS que le droit au revenu de solidarité active (RSA) est subordonné, pour les personnes qui remplissent les conditions pour en bénéficier, à la condition de faire valoir leurs droits à cette allocation, sauf à ce qu'elles ne remplissent pas encore les conditions pour bénéficier de la liquidation d'une pension de retraite à taux plein.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
