<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030956739</ID>
<ANCIEN_ID>JG_L_2015_07_000000388931</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/95/67/CETATEXT000030956739.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème SSJS, 27/07/2015, 388931, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388931</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP GASCHIGNARD ; SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Vincent Villette</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:388931.20150727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              L'association Promouvoir a demandé au tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de la décision du 9 février 2015 par laquelle la ministre de la culture a accordé un visa d'exploitation au film " Cinquante nuances de Grey " avec interdiction aux mineurs de moins de douze ans.<br/>
<br/>
              Par une ordonnance n° 1502353/9 du 3 mars 2015, le juge des référés du tribunal administratif de Paris a rejeté sa demande. <br/>
<br/>
              Par un pourvoi et un nouveau mémoire, enregistrés les 23 mars et 29 mai 2015 au secrétariat du contentieux du Conseil d'Etat, l'association Promouvoir demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande de suspension ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Villette, auditeur,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de l'association Promouvoir  et à la SCP Piwnica, Molinié, avocat du ministre de la culture et de la communication ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1.	La SAS Universal pictures France, qui est le distributeur en France du film " Cinquante nuances de Grey ", a intérêt au maintien de l'ordonnance attaquée. Ainsi son intervention est recevable.<br/>
<br/>
              2.	Le chapitre 2 du titre IV du livre VII du code de justice administrative qui, ainsi que le prévoient les dispositions de l'article R. 522-11 de ce même code, définit les mentions que doit porter l'ordonnance du juge des référés, dispose en son article R. 742-2 que : " Les ordonnances mentionnent le nom des parties, l'analyse des conclusions ainsi que les visas des dispositions législatives ou réglementaires dont elles font application. / Elles font apparaître la date à laquelle elles ont été signées (...) ". Il s'en déduit que, contrairement aux dispositions de l'article R. 741-2 du même code relatives à la mention des notes en délibéré, celles de l'article R. 742-2, seules applicables aux mentions que doivent comporter les ordonnances de référé en ce qui concerne les productions des parties, ne prescrivent pas au juge des référés de viser celles de ces productions qui interviennent après la clôture de l'instruction.<br/>
<br/>
              3.	L'association Promouvoir soutient que l'ordonnance attaquée est irrégulière dans la mesure où, ayant été rendue le 3 mars 2015, elle ne pouvait valablement viser une note en délibéré qu'elle avait produite avant que l'ordonnance ne soit rendue mais qui n'a été enregistrée, dans l'application Télérecours, que le 4 mars 2015. Toutefois ainsi qu'il est rappelé au point 2, le juge des référés n'était pas tenu de viser cette note. Dès lors, la circonstance qu'elle ait été enregistrée le lendemain du jour où l'ordonnance a été rendue est, en tout état de cause, sans incidence sur la régularité de cette dernière. Par suite, le moyen ne peut qu'être écarté.<br/>
<br/>
              4.	Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". L'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés, d'une part, d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue, d'autre part, de se placer, pour apprécier si cette condition est remplie, non à la date d'introduction de la requête aux fins de suspension mais à celle à laquelle il est appelé à se prononcer .<br/>
<br/>
              5.	Il ressort des pièces du dossier que la ministre de la culture et de la communication a délivré, le 9 février 2015, après avis de la commission de classification des oeuvres cinématographiques, un visa d'exploitation avec interdiction aux mineurs de moins de douze ans au film " Cinquante nuances de Grey ". L'exploitation en salle de ce film a débuté le 11 février 2015.<br/>
<br/>
              6.	Contrairement à ce que soutient l'association requérante, c'est sans erreur de droit que le juge des référés a, en se fondant sur les circonstances de l'espèce, souverainement apprécié que la condition d'urgence, prévue par l'article L. 521-1 du code de justice administrative cité au point 5, n'était pas remplie à la date à laquelle l'ordonnance a été rendue.<br/>
<br/>
              7.	 Le juge des  référés qui rejette une demande de suspension de l'exécution d'une décision administrative au motif qu'il n'est pas justifié de l'urgence de l'affaire au sens des dispositions précitées au point 5 n'est pas tenu d'analyser les moyens relatifs à la légalité de la décision contestée. Il lui appartient seulement, afin, notamment, de mettre le juge de cassation en mesure d'exercer son contrôle, de faire apparaître les raisons de droit et de fait pour lesquelles il estime que l'urgence ne justifie pas la suspension de l'acte attaqué. <br/>
<br/>
              8.	Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, qui sont inopérants, il y a lieu, dès lors que le juge des référés a valablement justifié l'absence d'urgence, de rejeter le pourvoi présenté par l'association Promouvoir.<br/>
<br/>
              9.	Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante. Ces mêmes dispositions font obstacle à ce qu'une somme soit versée à ce titre à la SAS Universal pictures France qui n'a pas, dans la présente instance, la qualité de partie.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de la SAS Universal pictures France est admise.<br/>
Article 2 : Le pourvoi de l'association Promouvoir est rejeté. <br/>
Article 3 : Les conclusions présentées par la SAS Universal pictures France au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à l'association Promouvoir, à la ministre de la culture et de la communication et à la SAS Universal pictures France.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
