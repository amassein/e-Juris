<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031587367</ID>
<ANCIEN_ID>JG_L_2015_12_000000374743</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/58/73/CETATEXT000031587367.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème SSJS, 07/12/2015, 374743, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374743</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Paquita Morellet-Steiner</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:374743.20151207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Poitiers d'annuler la décision du 24 février 2012 du président de la Communauté d'agglomération de Royan-Atlantique (CARA) lui  refusant, d'une part, le paiement des congés annuels non pris au titre des années 2008 et 2009, d'autre part, le remboursement des frais de transport liés à ses déplacements pour des contrôles médicaux obligatoires effectués durant son congé maladie. Par un jugement n° 1201465 du 20 novembre 2013, ce tribunal a fait droit à sa demande, et renvoyé M. A...devant la Communauté d'agglomération de Royan-Atlantique pour qu'il soit procédé à la liquidation de ses droits au titre de ses congés annuels non pris en  2008 et 2009. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 20 janvier, 4 avril et 25 septembre 2014, la Communauté d'agglomération de Royan-Atlantique, représentée par la SCP Odent, Poulet, demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement  n° 1201465 du 20 novembre 2013 du tribunal administratif de Poitiers ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M. A...;<br/>
<br/>
              3°) de mettre à la charge de M. A...la somme de 3 000 euros, au titre des dispositions de l'article L. 761 1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 2003/88/CE du Parlement européen et du Conseil du 4 novembre 2003 concernant certains aspects de l'aménagement du temps de travail ; <br/>
              - la loi du 10 juillet 1991 ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Paquita Morellet-Steiner, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de la communauté d'agglomération de Royan-Atlantique et à la SCP Rocheteau, Uzan-Sarano, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. En premier lieu, dans son mémoire enregistré au greffe du tribunal le 30 octobre 2013, la Communauté d'agglomération de Royan-Atlantique présentait le moyen nouveau tiré de ce que M. A...ne se trouvait pas en fin de relation de travail au sens de la directive 2003/88/CE. Il ressort des énonciations du jugement attaqué que le tribunal y a explicitement répondu. Dès lors, la circonstance que le tribunal a, en méconnaissance des prescriptions de l'article R. 741-2 du code de justice administrative, omis de mentionner ce mémoire présenté avant la clôture de l'instruction, dans les visas de son jugement, n'est, par elle-même, pas de nature à avoir vicié la régularité de la procédure au terme de laquelle celui-ci a été rendu. Le moyen tiré d'une irrégularité entachant le jugement attaqué ne peut, par suite, qu'être écarté. <br/>
<br/>
              2. En deuxième lieu, il ressort des énonciations du jugement que le moyen tiré de ce que le tribunal aurait omis de répondre à la fin de non-recevoir opposée par la Communauté d'agglomération Royan-Atlantique à la requête de M. A...manque en fait. <br/>
<br/>
              3. En troisième lieu, il ressort des pièces du dossier soumis aux juges du fond que la décision attaquée, dont la collectivité requérante ne conteste pas qu'elle est la première à mentionner les voies et délais de recours, date du 24 février 2012. Dès lors, en l'absence de justification quant à la date de notification de cette décision à M.A..., ce dernier est réputé en avoir pris connaissance à la date du recours gracieux qu'il a formé, le 13 avril 2012, contre cette décision. En conséquence, et contrairement à ce que soutient la collectivité requérante, c'est sans erreur de droit que le tribunal a jugé recevable la requête introduite par M. A...le 14 juin 2012.<br/>
<br/>
              4. Enfin, c'est, contrairement à ce que soutient la collectivité requérante, sans commettre d'erreur de droit que le tribunal a jugé, pour faire application des dispositions de l'article 7 de la directive 2003/88/CE, que M.A..., qui avait été recruté par la ville de Royan, se trouvait dans une situation de fin de relation de travail par rapport à la Communauté d'agglomération de Royan-Atlantique. <br/>
<br/>
              5. Il résulte de tout ce qui précède que le pourvoi de la Communauté d'agglomération de Royan-Atlantique doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la Communauté d'agglomération de Royan-Atlantique la somme de 3 000 euros, demandée par M.A..., au titre du même article et de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la Communauté d'agglomération de Royan-Atlantique est rejeté. <br/>
Article 2 : Les conclusions présentées par la Communauté d'agglomération de Royan-Atlantique au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La Communauté d'agglomération de Royan-Atlantique versera à la SCP Rocheteau, Uzan-Sarano, une somme de 3 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991 sous réserve qu'elle renonce à percevoir la somme correspondant à la part contributive de l'Etat. <br/>
Article 4 : La présente décision sera notifiée à la Communauté d'agglomération de Royan-Atlantique et à M. B...A.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
