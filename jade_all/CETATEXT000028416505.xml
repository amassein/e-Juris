<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028416505</ID>
<ANCIEN_ID>JG_L_2013_12_000000361188</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/41/65/CETATEXT000028416505.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 30/12/2013, 361188</TITRE>
<DATE_DEC>2013-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361188</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROGER, SEVAUX, MATHONNET ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Charles Touboul</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:361188.20131230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu 1 °, sous le n° 361188, le pourvoi sommaire et le mémoire complémentaire, enregistrés les 19 juillet et 17 octobre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM), dont le siège est 36 avenue du Général de Gaulle à Bagnolet Cedex (93175) ; l'ONIAM demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10NT01794 du 24 mai 2012 par lequel  la cour administrative d'appel de Nantes a annulé, sur l'appel du centre hospitalier de La Roche-sur-Yon, le jugement n° 0700448 du tribunal administratif de Nantes du 10 juin 2010 condamnant cet établissement à verser à la caisse primaire d'assurance maladie de la Vendée la somme de 52 676, 92 euros correspondant aux débours exposés à raison des séquelles qu'a conservées M. A... B...à la suite d'une intervention chirurgicale pratiquée le 17 octobre 2003 et a rejeté les conclusions de l'ONIAM tendant à l'annulation de ce jugement pour irrégularité et à la confirmation de la responsabilité du centre hospitalier de La Roche-sur-Yon ; <br/>
<br/>
              2°) réglant l'affaire au fond, d'annuler le jugement du tribunal administratif de Nantes, de condamner le centre hospitalier de La Roche-sur-Yon à raison de la faute commise au cours de l'intervention subie par M. B...à lui rembourser la somme de 42 492,31 euros, au versement d'une somme égale à 15 % de cette somme en application de l'article L. 1142-15 du code de la santé publique et au versement de la somme de 600 euros en remboursement des frais d'expertise exposés devant la CRCI des Pays de la Loire et d'assortir ces sommes des intérêts et de leur capitalisation ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier de La Roche-sur-Yon le versement d'une somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu 2°,  sous le n° 361329, le pourvoi et le mémoire complémentaire enregistrés les 25 juillet 2012 et 8 octobre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la caisse primaire d'assurance maladie (CPAM) de la Vendée, dont le siège est rue Alain à La Roche-sur-Yon Cedex 09 (85931) ; la CPAM de la Vendée demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10NT01794 du 24 mai 2012 par lequel la cour administrative d'appel de Nantes a annulé, sur l'appel du centre hospitalier de La Roche-sur-Yon, le jugement n° 0700448 du tribunal administratif de Nantes du 10 juin 2010 condamnant cet établissement à verser à la caisse la somme de 52 676, 92 euros correspondant aux débours exposés à raison des séquelles qu'a conservées M. A...B...à la suite d'une intervention chirurgicale pratiquée le 17 octobre 2003 et a rejeté ses conclusions tendant au relèvement de cette indemnité ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du centre hospitalier de la Roche-sur-Yon et de faire droit à son appel incident ; <br/>
<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier de La Roche-sur-Yon le versement d'une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ainsi que les entiers dépens ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles Touboul, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Roger, Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales, à Me Le Prado, avocat du centre hospitalier de la Roche-sur-Yon, et à Me Foussard, avocat de la caisse primaire d'assurance maladie de la Vendée ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite de la biopsie d'un ganglion cervical, pratiquée le 17 octobre 2003 au centre hospitalier de La Roche-sur-Yon (Vendée), M. B...est demeuré atteint de séquelles liées à une atteinte du nerf spinal gauche ; que, dans un avis émis en février 2006, la commission régionale de conciliation et d'indemnisation des Pays de la Loire, estimant que le dommage était la conséquence d'une faute engageant la responsabilité du centre hospitalier, a invité l'assureur de cet établissement à faire à la victime une offre d'indemnisation ; qu'en l'absence de réponse de l'assureur, l'office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) a indemnisé M. B... en lieu et place de celui-ci puis a saisi le tribunal administratif de Nantes d'une demande tendant à ce que le centre hospitalier soit condamné à lui rembourser les sommes exposées par l'effet de cette substitution ; que la caisse primaire d'assurance maladie (CPAM) de la Vendée ayant parallèlement saisi le même tribunal pour obtenir le remboursement des dépenses exposées par elle à la suite de l'accident, le centre hospitalier de La Roche-sur-Yon a sollicité la jonction de l'instance introduite par la caisse avec celle introduite par l'ONIAM ; que, statuant sur la demande de la caisse par un jugement du 10 juin 2010 sans procéder à cette jonction, le tribunal administratif a condamné le centre hospitalier à lui rembourser ses dépenses ; que la cour administrative d'appel de Nantes a été saisie à l'encontre de ce jugement d'un appel du centre hospitalier contestant le principe de sa responsabilité, d'un appel incident de la CPAM de la Vendée sollicitant l'actualisation de ses débours et de conclusions de l'ONIAM contestant le jugement en tant qu'il lui déniait la qualité de partie à l'instance ; que, par un arrêt du 24 mai 2012, la cour administrative d'appel a fait droit à l'appel du centre hospitalier et rejeté l'appel incident de la CPAM de la Vendée ainsi que les conclusions de l'ONIAM ; que l'ONIAM et la CPAM de la Vendée se pourvoient en cassation contre cet arrêt ; que leurs pourvois étant dirigés contre le même arrêt, il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant, d'une part, qu'aux termes de l'article L. 376-1 du code de la sécurité sociale : " L'intéressé ou ses ayants droit doivent indiquer, en tout état de la procédure, la qualité d'assuré social de la victime de l'accident ainsi que les caisses de sécurité sociale auxquelles celle-ci est ou était affiliée pour les divers risques. Ils doivent appeler ces caisses en déclaration de jugement commun ou réciproquement " ; qu'il résulte de ces dispositions qu'il incombe au juge administratif, saisi d'un recours indemnitaire de la victime contre une personne publique regardée comme responsable de l'accident, de mettre en cause les caisses auxquelles la victime est ou était affiliée ; que, symétriquement, lorsque le juge est saisi d'un recours indemnitaire introduit contre la personne publique par une caisse agissant dans le cadre de la subrogation légale, il lui incombe de mettre en cause la victime ; que le défaut de mise en cause, selon le cas, de la caisse ou de la victime entache la procédure d'irrégularité ;<br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes de l'article L. 1142-15 du code de la santé publique : " En cas de silence ou de refus explicite de la part de l'assureur de faire une offre (...) l'office institué à l'article L. 1142-22 est substitué à l'assureur. / (...) / L'acceptation de l'offre de l'office vaut transaction au sens de l'article 2044 du code civil. La transaction est portée à la connaissance du responsable et, le cas échéant, de son assureur (...).  / L'office est subrogé, à concurrence des sommes versées, dans les droits de la victime contre la personne responsable du dommage ou, le cas échéant, son assureur. (...) " ; que, dès lors qu'en vertu de ces dispositions l'ONIAM, qui s'est substitué à l'assureur d'un hôpital pour indemniser la victime d'un dommage corporel, se trouve subrogé dans les droits de la victime, le juge administratif saisi d'un recours indemnitaire de la caisse dirigé contre l'hôpital doit, en application des dispositions précitées de l'article L. 376-1 du code de la sécurité sociale, appeler l'ONIAM en la cause ; <br/>
<br/>
              4. Considérant qu'ainsi qu'il a été dit, il ressort des pièces du dossier soumis aux juges du fond que l'ONIAM s'était substitué à l'assureur du centre hospitalier de La Roche-sur-Yon pour indemniser M. B...et se trouvait, par suite, subrogé dans les droits de la victime en application des dispositions de l'article L. 1142-15 du code de la santé publique ; que, dès lors, par application des dispositions de l'article L. 376-1 du code de la sécurité sociale, il incombait aux juges du fond, saisis d'un recours de la CPAM de la Vendée, d'appeler l'office dans la cause ;<br/>
<br/>
              5. Considérant que l'ONIAM, qui est intervenu en demande devant la cour administrative d'appel de Nantes, aurait eu, en l'absence d'intervention de sa part, qualité pour former tierce opposition contre l'arrêt du 24 mai 2012 ; que, par suite, contrairement à ce que soutient le centre hospitalier de La Roche-sur-Yon, l'office a qualité pour se pourvoir en cassation contre cet arrêt ;<br/>
<br/>
              6. Considérant qu'en jugeant que le tribunal administratif de Nantes n'avait pas entaché son jugement d'irrégularité en s'abstenant d'appeler l'ONIAM dans la cause, la cour administrative d'appel a commis une erreur de droit ; que l'ONIAM et la CPAM de la Vendée sont, dès lors, fondés, sans qu'il soit besoin d'examiner les autres moyens de leurs pourvois, à demander  l'annulation de l'arrêt du 24 mai 2012 ; <br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier de La Roche-sur-Yon une somme de 2 000 euros à verser à l'ONIAM au titre des dispositions de l'article L. 761-1 du code de justice administrative  et une somme de 2 000 euros à verser à la CPAM de la Vendée au titre des mêmes dispositions et de celles de l'article R. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 24 mai 2012 est annulé.  <br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
<br/>
Article 3 : Le centre hospitalier de La Roche-sur-Yon versera une somme de 2 000 euros à l'ONIAM au titre des dispositions de l'article L. 761-1 du code de justice administrative et une somme de 2 000 euros à la CPAM de la Vendée au titre des mêmes dispositions et de celles de l'article R. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à l'Office national des accidents médicaux, des affections iatrogènes et des infections nosocomiales, à la caisse primaire d'assurance maladie de la Vendée et au centre hospitalier de La Roche-sur-Yon.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-07-01-07 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. DEVOIRS DU JUGE. - RESPONSABILITÉ MÉDICALE - DOMMAGE CORPOREL IMPUTABLE À L'ACTIVITÉ DE PRÉVENTION, DE DIAGNOSTIC OU DE SOINS D'UN ÉTABLISSEMENT DE SANTÉ - INDEMNISATION DE LA VICTIME PAR L'ONIAM DANS LE CADRE D'UNE PROCÉDURE AMIABLE (ART. L. 1142-4 ET SUIVANTS DU CSP) - RECOURS INDEMNITAIRE INTRODUIT PAR UNE CAISSE AGISSANT DANS LE CADRE DE LA SUBROGATION LÉGALE (ART. L. 376-1 DU CSS) - OBLIGATION D'APPELER L'ONIAM EN LA CAUSE [RJ1] - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-02-01-01-02-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. ÉTABLISSEMENTS PUBLICS D'HOSPITALISATION. RESPONSABILITÉ POUR FAUTE MÉDICALE : ACTES MÉDICAUX. EXISTENCE D'UNE FAUTE MÉDICALE DE NATURE À ENGAGER LA RESPONSABILITÉ DU SERVICE PUBLIC. - INDEMNISATION DE LA VICTIME PAR L'ONIAM DANS LE CADRE D'UNE PROCÉDURE AMIABLE (ART. L. 1142-4 ET SUIVANTS DU CSP) - RECOURS INDEMNITAIRE INTRODUIT PAR UNE CAISSE AGISSANT DANS LE CADRE DE LA SUBROGATION LÉGALE (ART. L. 376-1 DU CSS) - OBLIGATION D'APPELER L'ONIAM EN LA CAUSE [RJ1] - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-03-02-02-04 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. PROBLÈMES D'IMPUTABILITÉ. PERSONNES RESPONSABLES. ÉTAT OU AUTRES COLLECTIVITÉS PUBLIQUES. ÉTAT OU ÉTABLISSEMENT PUBLIC. - DOMMAGE CORPOREL IMPUTABLE À L'ACTIVITÉ DE PRÉVENTION, DE DIAGNOSTIC OU DE SOINS D'UN ÉTABLISSEMENT DE SANTÉ - INDEMNISATION DE LA VICTIME PAR L'ONIAM DANS LE CADRE D'UNE PROCÉDURE AMIABLE (ART. L. 1142-4 ET SUIVANTS DU CSP) - RECOURS INDEMNITAIRE INTRODUIT PAR UNE CAISSE AGISSANT DANS LE CADRE DE LA SUBROGATION LÉGALE (ART. L. 376-1 DU CSS) - OBLIGATION D'APPELER L'ONIAM EN LA CAUSE [RJ1] - EXISTENCE.
</SCT>
<ANA ID="9A"> 54-07-01-07 Il résulte des dispositions combinées de l'article L. 376-1 du code de la sécurité sociale (CSS) et de l'article L. 1142-15 du code de la santé publique (CSP) que le juge administratif, saisi par une caisse agissant dans le cadre de la subrogation légale d'un recours indemnitaire introduit contre un hôpital regardé comme responsable d'un accident ayant causé un dommage corporel, doit, lorsque l'office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) s'est substitué à l'assureur de l'hôpital pour indemniser la victime et est ainsi lui-même subrogé dans les droits de la victime, appeler l'ONIAM en la cause.</ANA>
<ANA ID="9B"> 60-02-01-01-02-01 Il résulte des dispositions combinées de l'article L. 376-1 du code de la sécurité sociale (CSS) et de l'article L. 1142-15 du code de la santé publique (CSP) que le juge administratif, saisi par une caisse agissant dans le cadre de la subrogation légale d'un recours indemnitaire introduit contre un hôpital regardé comme responsable d'un accident ayant causé un dommage corporel, doit, lorsque l'office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) s'est substitué à l'assureur de l'hôpital pour indemniser la victime et est ainsi lui-même subrogé dans les droits de la victime, appeler l'ONIAM en la cause.</ANA>
<ANA ID="9C"> 60-03-02-02-04 Il résulte des dispositions combinées de l'article L. 376-1 du code de la sécurité sociale (CSS) et de l'article L. 1142-15 du code de la santé publique (CSP) que le juge administratif, saisi par une caisse agissant dans le cadre de la subrogation légale d'un recours indemnitaire introduit contre un hôpital regardé comme responsable d'un accident ayant causé un dommage corporel, doit, lorsque l'office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) s'est substitué à l'assureur de l'hôpital pour indemniser la victime et est ainsi lui-même subrogé dans les droits de la victime, appeler l'ONIAM en la cause.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., pour l'obligation d'appeler la victime en la cause, CE, avis, 7 octobre 2013, ONIAM, n° 369121, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
