<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042699880</ID>
<ANCIEN_ID>JG_L_2020_12_000000430314</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/69/98/CETATEXT000042699880.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 17/12/2020, 430314, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430314</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:430314.20201217</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 30 avril 2019 et 12 juin 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... D... demande au Conseil d'Etat d'annuler pour excès de pouvoir l'arrêté du 25 février 2019 modifiant l'arrêté du 18 mars 1982 relatif à l'exercice de la vènerie et visant à limiter les incidents en fin de chasse à proximité des lieux habités. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ;<br/>
              - le code de l'environnement ;<br/>
              - l'arrêté du 18 mars 1982 relatif à l'exercice de la vènerie ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme C... B..., maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 420-2 du code de l'environnement : " Le Gouvernement exerce la surveillance et la police de la chasse dans l'intérêt général ". Aux termes de l'article L. 424-1 du même code : " Sans qu'il soit ainsi dérogé au droit de destruction des bêtes fauves édicté à l'article L. 427-9, le ministre chargé de la chasse prend des arrêtés pour : / - prévenir la destruction ou favoriser le repeuplement des oiseaux ou de toutes espèces de gibier (...) ". <br/>
<br/>
              2. Sur le fondement de ces dispositions, l'arrêté du 25 février 2019 modifiant l'arrêté du 18 mars 1982 relatif à l'exercice de la vènerie et visant à limiter les incidents en fin de chasse à proximité des lieux habités complète l'article 5 de cet arrêté du 18 mars 1982, pour fixer à soixante l'effectif maximal de chiens courants en action de chasse, ainsi que son article 6, pour soumettre la délivrance et le renouvellement de l'attestation de conformité de meute à l'avis de la fédération départementale ou interdépartementale des chasseurs, et introduit un nouvel article 7 définissant la procédure applicable lorsqu'un animal " aux abois ou au ferme " se trouve à proximité de lieux habités ou fréquentés du public, afin de prévenir les risques d'atteinte aux personnes et aux biens. M. D... demande l'annulation pour excès de pouvoir de cet arrêté du 25 février 2019.<br/>
<br/>
              Sur la légalité externe de l'arrêté attaqué :<br/>
<br/>
              3. L'article L. 123-19-1 du code de l'environnement définit les conditions et limites dans lesquelles les décisions, autres que les décisions individuelles, des autorités publiques ayant une incidence directe ou significative sur l'environnement sont soumises au principe de participation du public énoncé à l'article 7 de la Charte de l'environnement. Aux termes du II de cet article : " Sous réserve des dispositions de l'article L. 123-19-6, le projet d'une décision mentionnée au I, accompagné d'une note de présentation précisant notamment le contexte et les objectifs de ce projet, est mis à disposition du public par voie électronique et, sur demande présentée dans des conditions prévues par décret, mis en consultation sur support papier dans les préfectures et les sous-préfectures en ce qui concerne les décisions des autorités de l'Etat, y compris les autorités administratives indépendantes, et des établissements publics de l'Etat, ou au siège de l'autorité en ce qui concerne les décisions des autres autorités. (...) / Le projet de décision ne peut être définitivement adopté avant l'expiration d'un délai permettant la prise en considération des observations et propositions déposées par le public et la rédaction d'une synthèse de ces observations et propositions. Sauf en cas d'absence d'observations et propositions, ce délai ne peut être inférieur à quatre jours à compter de la date de la clôture de la consultation. / (...) Au plus tard à la date de la publication de la décision et pendant une durée minimale de trois mois, l'autorité administrative qui a pris la décision rend publics, par voie électronique, la synthèse des observations et propositions du public avec l'indication de celles dont il a été tenu compte, les observations et propositions déposées par voie électronique ainsi que, dans un document séparé, les motifs de la décision ".<br/>
<br/>
              4. D'une part, la circonstance que la synthèse des observations et propositions du public et les motifs de la décision n'aient été rendus publics que le 11 avril 2019, près d'un mois et demi après la publication de l'arrêté litigieux, est, par elle-même, sans incidence sur la légalité de cet acte. Par ailleurs, ce délai ne saurait, par lui-même, établir que la rédaction de cette synthèse serait intervenue postérieurement à l'adoption de l'arrêté attaqué. <br/>
<br/>
              5. D'autre part, si, comme le soutient le requérant sans être contredit par le ministre chargé de la chasse, seule une partie des contributions du public déposées par voie électronique a été rendue accessible sur le site internet de la consultation, cette circonstance n'est pas de nature à entacher d'illégalité l'arrêté attaqué, alors qu'il ressort de la synthèse élaborée par l'autorité administrative qu'elle a pris en compte l'ensemble des avis formulés au cours de la consultation du public. <br/>
<br/>
              6. Enfin, s'il ressort des pièces du dossier que la notice explicative accompagnant le projet d'arrêté indique à tort que l'attestation de conformité de meute prévue à l'article 6 de l'arrêté du 18 mars 1982 " devra dorénavant recevoir l'avis de la Fédération départementale (FDC) ou interdépartementale (FIDC) des chasseurs et celui de la Société de vénerie ", alors que le projet soumis à la consultation ne prévoyait que l'avis de la fédération des chasseurs, cette seule circonstance n'a pas été de nature à induire le public en erreur quant à l'objet et à la portée de la consultation. <br/>
<br/>
              7. Il résulte de ce qui précède que le moyen tiré de ce que l'arrêté attaqué aurait été pris en méconnaissance des dispositions de l'article L. 123-19-1 du code de l'environnement doit être écarté.<br/>
<br/>
              Sur la légalité interne de l'arrêté attaqué :<br/>
<br/>
              8. En premier lieu, l'arrêté du 18 mars 1982 soumet tout équipage de vènerie à l'obligation de détenir une attestation de conformité de meute, laquelle est délivrée par le préfet pour une période de six ans, avec une période probatoire d'un an lors de la première délivrance, et peut être suspendue ou retirée en cas de manquement grave aux prescriptions de l'arrêté ou à la réglementation en vigueur en matière de chasse ou de protection de l'environnement. Il prévoit par ailleurs que chaque équipage de chasse à courre ou de chasse sous terre est, au cours de la chasse, dirigé par un responsable, qui doit être titulaire et porteur d'un permis de chasser visé et validé, comme doit l'être aussi " tout membre de l'équipage portant soit simultanément le fouet et la trompe de chasse (ou corne de chasse), soit une arme destinée à servir l'animal ". Eu égard à l'encadrement ainsi exigé de la pratique de la vènerie, il ne ressort pas des pièces du dossier que le ministre chargé de la chasse aurait commis une erreur manifeste d'appréciation en fixant à soixante, par l'article 1er de l'arrêté attaqué, le nombre maximal de chiens courants en action de chasse.<br/>
<br/>
              9. En deuxième lieu, l'article 1er de l'arrêté litigieux a introduit à l'article 6 de l'arrêté du 18 mars 1982 un nouvel alinéa soumettant la délivrance et le renouvellement de l'attestation de conformité de meute à l'avis préalable de la fédération départementale ou interdépartementale des chasseurs. La circonstance que ces dispositions ne prévoient pas, en outre, la consultation de l'association française des équipages de vènerie ou de l'association française des équipages de vènerie sous terre, ne saurait, en tout état de cause, entacher l'arrêté attaqué d'erreur manifeste d'appréciation.<br/>
<br/>
              10. En troisième lieu, l'arrêté attaqué insère dans l'arrêté du 18 mars 1982 un nouvel article 7 ainsi rédigé : " En grande vénerie, lorsque l'animal est aux abois ou au ferme (sur ses fins, pris, forcé ou hallali courant) et qu'il se trouve à proximité d'habitations, de jardins privés y attenant, de zones commerciales ou artisanales et de bureaux et d'établissements accueillant du public, il est gracié. / Le maître d'équipage ou son suppléant doit sans délai et par tout moyen veiller à ce que l'animal ne soit pas approché. Il s'assure de la sécurité des personnes et des biens. Il met tout en oeuvre pour retirer les chiens dans les meilleurs délais. Il facilite le déplacement de l'animal loin de la zone habitée. / Si ce résultat n'est pas atteint ou si les moyens requis ne permettent pas raisonnablement de contraindre l'animal, le responsable de l'équipage avise la gendarmerie, la police nationale, le maire de la commune ou le service en charge de la police de la chasse, qui décide de faire appel aux services d'un vétérinaire. L'autorité publique évalue la situation et décide de faire procéder à l'anesthésie de l'animal par le vétérinaire, aux frais de l'équipage, ou à défaut, de procéder à sa mise à mort. "<br/>
<br/>
              11. Aux termes de l'article L. 422-1 du même code : " Nul n'a la faculté de chasser sur la propriété d'autrui sans le consentement du propriétaire ou de ses ayants droit ". L'article L. 428-1 du même code punit de trois mois d'emprisonnement et de 3 750 euros d'amende le fait de chasser sur le terrain d'autrui sans son consentement, si ce terrain est attenant à une maison habitée ou servant à l'habitation, et s'il est entouré d'une clôture continue faisant obstacle à toute communication avec les héritages voisins, la peine d'emprisonnement encourue étant portée à deux ans si le délit est commis pendant la nuit. Par ailleurs, l'article R. 428-1 du même code punit de l'amende prévue pour les contraventions de la cinquième classe le fait de chasser sur le terrain d'autrui sans le consentement du propriétaire ou du détenteur du droit de chasse. Son II dispose toutefois que " peut ne pas être considéré comme une infraction le passage des chiens courants sur l'héritage d'autrui, lorsque ces chiens sont à la suite d'un gibier lancé sur la propriété de leur maître, sauf l'action civile, s'il y a lieu, en cas de dommages ". <br/>
<br/>
              12. Si les dispositions de l'article 7 de l'arrêté du 18 mars 1982 modifié par l'arrêté attaqué, citées précédemment, édictées dans le but de limiter les incidents en fin de chasse, prévoient les obligations mises à la charge du maître d'équipage pour assurer la sécurité des personnes et des biens et faciliter le déplacement de l'animal loin des zones habitées ou fréquentées du public énumérées, elles n'ont ni pour objet ni pour effet de soustraire le maître d'équipage à l'interdiction de chasser sur la propriété d'autrui sans le consentement du propriétaire ou de ses ayants droit, et de façon générale au respect du droit de propriété. Dans le cas où la maître d'équipage ne peut satisfaire aux obligations mentionnées à l'alinéa 2 de l'article 7 de l'arrêté du 18 mars 1982 modifié, notamment en cas d'opposition d'un propriétaire, il lui appartient, conformément aux dispositions de l'alinéa 3 de cet article, de faire appel aux autorités publiques que cet article mentionne. Dans ces conditions, le moyen tiré de l'atteinte au droit de propriété doit être écarté.<br/>
<br/>
              13 En outre, les dispositions de l'article 7 de l'arrêté du 18 mars 1982 modifié, en tant qu'elles prévoient d'épargner l'animal aux abois ou au ferme qui se trouve à proximité d'habitations, de jardins privés y attenant, de zones commerciales ou artisanales et de bureaux et d'établissements accueillant du public, prises au titre de la police de la chasse, répondent au motif d'intérêt général d'assurer la sécurité des personnes et des biens face au risque né de la présence d'un animal aux abois ou au ferme à proximité d'un lieu habité ou fréquenté par le public et ne portent pas d'atteinte excessive au droit de propriété susceptible d'être revendiqué par les chasseurs sur l'animal qu'ils chassent.<br/>
<br/>
              14. Par ailleurs, les dispositions de l'article 7 modifié déterminent précisément les obligations pesant sur l'équipage lorsque l'animal aux abois ou au ferme se trouve à proximité d'un lieu habité ou fréquenté par le public, les conditions de leur mise en oeuvre devant s'apprécier au cas par cas au regard du danger que la situation est susceptible de présenter pour les personnes ou pour les biens. Le moyen tiré de la méconnaissance du principe de sécurité juridique alléguée à l'encontre de ces dispositions ne peut, en tout état de cause, qu'être écarté.<br/>
<br/>
              15. Contrairement à ce qui est soutenu, les dispositions contestées, notamment celles mettant à la charge de l'équipage les éventuels frais d'anesthésie de l'animal, prise au titre de la police administrative de la chasse, ne constituent pas des sanctions ayant le caractère de punition. Par suite, les moyens tirés de la méconnaissance du principe de légalité des délits et des peines et des prérogatives de l'autorité judiciaire doivent être écartés comme inopérants. <br/>
<br/>
              16. Enfin, si les dispositions litigieuses imposent au responsable de l'équipage, dans le cas où l'animal ne peut être éloigné de la zone habitée, d'en aviser la gendarmerie, la police nationale, le maire de la commune ou le service en charge de la police de la chasse, il ressort des termes mêmes de l'arrêté que cette énumération est alternative. Le moyen tiré de ce que l'arrêté litigieux serait entaché d'erreur manifeste d'appréciation en raison des conflits de compétences entre autorités qu'il serait susceptible de provoquer ne saurait être retenu. <br/>
<br/>
              17. En dernier lieu, les moyens mettant en cause la légalité des dispositions de l'arrêté du 18 mars 1982 qui n'ont pas été modifiées par l'arrêté du 25 février 2019 sont inopérants à l'appui du recours pour excès de pouvoir formé contre ce dernier arrêté.<br/>
<br/>
              18. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur la recevabilité de sa requête, que M. D... n'est pas fondé à demander l'annulation pour excès de pouvoir de l'arrêté qu'il attaque.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. D... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A... D... et à la ministre de la transition écologique. <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
