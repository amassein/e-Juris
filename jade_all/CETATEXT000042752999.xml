<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042752999</ID>
<ANCIEN_ID>JG_L_2020_12_000000445078</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/75/29/CETATEXT000042752999.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 24/12/2020, 445078, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445078</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP BOULLOCHE</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:445078.20201224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Cuisine froid professionnel a demandé au juge des référés du tribunal administratif de Marseille, statuant sur le fondement de l'article L. 551-1 du code de justice administrative, à titre principal, d'annuler la décision d'attribuer le marché de fourniture et de maintenance de déshydrateurs thermiques et de collecte, transport et valorisation des biodéchets dans le cadre du projet européen " Life IP Smart Waste " à la société Diffusion solutions écologiques, à titre subsidiaire, d'annuler la procédure de passation, et d'enjoindre à la région Provence-Alpes-Côte d'Azur de reprendre la procédure au stade de l'examen des candidatures. <br/>
<br/>
              Par une ordonnance n° 2006184 du 18 septembre 2020, le juge des référés du tribunal administratif de Marseille a annulé cette procédure et enjoint à la région Provence-Alpes-Côte d'Azur, si elle entendait conclure un marché ayant le même objet, de lancer une nouvelle procédure.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 5 et 13 octobre et 23 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, la région Provence-Alpes-Côte d'Azur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société Cuisine froid professionnel ;<br/>
<br/>
              3°) mettre à la charge de la société Cuisine froid professionnel la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la commande publique ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la région Provence-Alpes-Côte d'Azur et à la SCP Boulloche, avocat de la société Cuisine froid professionnel ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du référé précontractuel que, par un avis d'appel public à la concurrence publié le 13 mars 2020, la région Provence-Alpes-Côte d'Azur a lancé un avis d'appel à candidature pour l'attribution, selon une procédure d'appel d'offres ouvert, d'un accord-cadre mono-attributaire portant sur la fourniture et la maintenance de déshydrateurs thermiques et la collecte, le transport et la valorisation des biodéchets dans le cadre du projet européen " Life IP Smart Waste "  pour un groupement de commande constitué de sept lycées membres. Par courrier du 4 août 2020, la région Provence-Alpes-Côte d'Azur a informé la société Cuisine froid professionnel du rejet de son offre classée en seconde position et de l'attribution du marché à la société Diffusion solutions écologiques. La société Cuisine froid professionnel a demandé au juge des référés du tribunal administratif de Marseille, sur le fondement de l'article L. 551-1 du code de justice administrative, d'annuler cette décision et la procédure de passation de ce marché. La région Provence-Alpes-Côte d'Azur se pourvoit en cassation contre l'ordonnance du 18 septembre 2020 par laquelle le juge des référés du tribunal administratif de Marseille a annulé la procédure et lui a enjoint, si elle entendait conclure un marché ayant le même objet, de lancer une nouvelle procédure.<br/>
<br/>
              2. Aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation, la délégation d'un service public (...) / Le juge est saisi avant la conclusion du contrat ".<br/>
<br/>
              3. Lorsque, pour fixer un critère ou un sous-critère d'attribution du marché, le pouvoir adjudicateur prévoit que la valeur des offres sera examinée au regard d'une caractéristique technique déterminée, il lui incombe d'exiger la production de justificatifs lui permettant de vérifier l'exactitude des informations données par les candidats. <br/>
<br/>
              4. En estimant, pour juger que la région avait manqué à ses obligations de publicité et de mise en concurrence, que le respect effectif des normes européennes constituait une exigence précise, impliquant la production de justificatifs, sanctionnée par le système d'évaluation des offres, alors que le règlement de la consultation se bornait à prévoir que l'ergonomie des équipements constituait un élément d'appréciation du critère de la valeur technique, sans que cette exigence, au demeurant générale, soit assortie de conséquences directes sur la notation des offres, le juge des référés du tribunal administratif de Marseille a dénaturé les pièces du dossier qui lui était soumis.<br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur l'autre moyen du pourvoi, que la région Provence-Alpes-Côte d'Azur est fondée à demander l'annulation de l'ordonnance qu'elle attaque.<br/>
<br/>
              6. Dans les circonstances de l'espèce, il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée.<br/>
<br/>
              7. En premier lieu, une offre ne saurait être regardée comme ne respectant pas les exigences du règlement de la consultation au seul motif que le prix qu'elle propose est inférieur au montant minimum de l'accord-cadre figurant dans le règlement de la consultation. La société Cuisine froid professionnel n'est, par suite, pas fondée à soutenir que l'offre de la société Diffusion solutions écologiques serait irrégulière au motif que le détail quantitatif estimatif qu'elle a soumis, aux fins de la notation de son offre sur le critère du prix, afficherait un total inférieur au montant minimum de l'accord-cadre. Elle ne peut davantage soutenir que cette circonstance révélerait une ambiguïté des documents de la consultation.<br/>
<br/>
              8. En deuxième lieu, il résulte de ce qui a été dit aux points 3 et 4 de la présente décision que la région Provence-Alpes-Côte d'Azur n'était pas tenue d'exiger des documents permettant d'assurer le respect des normes européennes de sécurité des personnes par les candidats et leurs équipements. Par ailleurs, le pouvoir adjudicateur, qui était libre de choisir les critères d'attribution du marché dès lors qu'ils lui permettaient de déterminer l'offre économiquement la plus avantageuse, n'a pas davantage commis de manquement à ses obligations de publicité et de mise en concurrence en ne prévoyant pas de critère de notation spécifique portant sur le respect de ces normes.<br/>
<br/>
              9. En troisième lieu, la société Cuisine froid professionnel, qui ne peut utilement soutenir que le refus de la région Provence-Alpes-Côte d'Azur, qui n'y était pas tenue, de lui communiquer le rapport d'analyse des offres révélerait à lui seul l'irrégularité de la méthode de notation des offres, n'apporte aucune précision au soutien de son allégation selon laquelle cette méthode de notation serait de nature à neutraliser les critères de notation. <br/>
<br/>
              10. Il résulte de tout ce qui précède que la société Cuisine froid professionnel n'est pas fondée à demander l'annulation de la décision d'attribuer le marché litigieux à la société Diffusion solutions écologiques et de la procédure de passation. <br/>
<br/>
              11. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de la région Provence-Alpes-Côte d'Azur, qui n'est pas la partie perdante, le versement d'une somme au titre de cet article. Il y a lieu, en revanche, de mettre à la charge de la société Cuisine froid professionnel une somme de 3 000 euros à verser à la région Provence-Alpes-Côte d'Azur sur le fondement des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 18 septembre 2020 du juge des référés du tribunal administratif de Marseille est annulée.<br/>
Article 2 : La demande présentée par la société Cuisine froid professionnel devant le juge des référés du tribunal administratif de Marseille est rejetée.<br/>
Article 3 : La société Cuisine froid professionnel versera la somme de 3 000 euros à la région Provence-Alpes-Côte d'Azur au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la société Cuisine froid professionnel au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la région Provence-Alpes-Côte d'Azur et à la société Cuisine froid professionnel.<br/>
Copie en sera adressée à la société Diffusion solutions écologiques.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
