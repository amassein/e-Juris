<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028241700</ID>
<ANCIEN_ID>JG_L_2013_11_000000368346</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/24/17/CETATEXT000028241700.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème sous-section jugeant seule, 25/11/2013, 368346, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-11-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368346</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BLANC, ROUSSEAU</AVOCATS>
<RAPPORTEUR>Mme Natacha Chicot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:368346.20131125</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 7 mai et 6 août 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A... B..., demeurant... ; M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1103142-1103688 du 5 mars 2013 par lequel le président du tribunal administratif de Bordeaux a, sur ses demandes tendant à l'annulation de la décision implicite du 15 mars 2011 et de la décision du 20 juillet 2011 du ministre de la défense lui refusant le bénéfice de l'indemnité pour charges militaires au taux particulier n° 1 à compter de la date de souscription de son pacte civil de solidarité, en premier lieu, annulé la décision du ministre de la défense en tant qu'elle rejette sa demande tendant à obtenir le bénéfice de l'indemnité pour charges militaires au taux particulier n° 1 pour la période allant du 3 décembre 2010 au 13 janvier 2011, en deuxième lieu, enjoint au ministre de la défense de réexaminer sa demande, et, en dernier lieu, rejeté le surplus de ses conclusions ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 99-944 du 15 novembre 1999 ;<br/>
<br/>
              Vu le décret n° 59-1193 du 13 octobre 1959 modifié notamment par le décret n° 2011-38 du 10 janvier 2011 ;<br/>
<br/>
              Vu le code de justice administrative et notamment son article R. 611-8 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Natacha Chicot, Auditeur,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Blanc, Rousseau, avocat de M. B...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que M.B..., militaire de carrière, a conclu un pacte civil de solidarité le 3 décembre 2010 et a sollicité l'attribution de l'indemnité pour charges militaires au taux particulier n° 1 à compter de cette date ; que le ministre de la défense a rejeté sa demande par décision du 20 juillet 2011 ; que par l'ordonnance attaquée, le président du tribunal administratif de Bordeaux a fait partiellement droit aux conclusions de M.B..., d'une part, en annulant la décision du ministre de la défense en tant seulement qu'elle rejetait sa demande tendant à obtenir le bénéfice de l'indemnité pour charges militaires au taux particulier n° 1 pour la période allant du 3 décembre 2010 au 13 janvier 2011 et, d'autre part, en enjoignant à ce dernier de réexaminer la demande du requérant ;<br/>
<br/>
              2. Considérant que, bien que M. B...conclut à l'annulation de l'ordonnance du président du tribunal administratif de Bordeaux, il ressort de l'argumentation de son pourvoi qu'il ne la conteste en réalité qu'en tant qu'elle rejette le surplus de ses conclusions tendant, d'une part, à l'annulation de la décision du ministre de la défense en tant qu'elle lui refuse le bénéfice de l'indemnité pour charges militaires au taux particulier n° 1 pour la période postérieure au 13 janvier 2011 et, d'autre part, à ce qu'il soit enjoint à l'Etat de lui verser ladite indemnité ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article R. 222-1 du code de justice administrative : " Les présidents de tribunal administratif et de cour administrative d'appel (...) et les présidents de formation de jugement des tribunaux et des cours peuvent, par ordonnance : / (...) / 6° Statuer sur les requêtes relevant d'une série, qui sans appeler de nouvelle appréciation ou qualification de faits, présentent à juger en droit, pour la juridiction saisie, des questions identiques à celles qu'elle a déjà tranchées ensemble par une même décision passée en force de chose jugée ou à celles tranchées ensemble par une même décision du Conseil d'Etat statuant au contentieux, ou examinées ensemble par un même avis par le Conseil d'Etat en application de l'article L. 113-1 " ; <br/>
<br/>
              4. Considérant que, pour statuer par ordonnance en application de l'article R. 222-1 du code de justice administrative précité, le président du tribunal administratif de Bordeaux a énoncé que, par une décision rendue le 29 octobre 2012 sous le n° 357822, le Conseil d'Etat avait jugé des questions identiques à celles soulevées par la demande qui lui était soumise ; qu'il ressort cependant des termes de son ordonnance qu'il s'est fondé non seulement sur la décision précitée mais également sur un avis rendu le 13 juin 2012 par le Conseil d'Etat sous les nos 357793 et 357794 ; qu'ainsi, les questions qui lui étaient soumises n'avaient pas été tranchées ou examinées ensemble par une même décision ou un même avis du Conseil d'Etat ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son ordonnance doit être annulée en tant qu'elle rejette le surplus des conclusions de M. B...tendant, d'une part, à l'annulation de la décision du ministre de la défense en tant qu'elle lui refuse le bénéfice de l'indemnité pour charges militaires au taux particulier n° 1 pour la période postérieure au 13 janvier 2011 et, d'autre part, à ce qu'il soit enjoint à l'Etat de lui verser ladite indemnité ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              6. Considérant qu'aux termes de l'article 1er du décret du 13 octobre 1959 fixant le régime de l'indemnité pour charges militaires : " L'indemnité représentative de frais dite indemnité pour charges militaires est attribuée aux officiers et militaires non officiers à solde mensuelle, ainsi qu'aux volontaires dans les armées, pour tenir compte des diverses sujétions spécifiquement militaires, et notamment de la fréquence des mutations d'office. (...) " ; qu'aux termes du deuxième alinéa de l'article 3 du même décret, dans sa rédaction antérieure à la publication du décret du 10 janvier 2011 relatif à la prise en compte du pacte civil de solidarité dans le régime indemnitaire des militaires et modifiant diverses dispositions relatives à la délégation de solde des militaires : " Sous réserve du quatrième alinéa du présent article, les militaires mariés ou ayant un ou deux enfants à charge (...) peuvent bénéficier en plus du taux de base d'un taux particulier correspondant à cette situation de famille " ; qu'aux termes de l'article 5 du même texte : " L'indemnité pour charges militaires est soumise aux règles d'allocation de la solde et perçue dans les mêmes conditions. / Elle est payée mensuellement et à terme échu. / L'indemnité se décompte par mois, à raison de la douzième partie de la fixation annuelle, et par jour, à raison de la trois cent soixantième partie de la même fixation " ; que le décret du 10 janvier 2011 a ouvert aux militaires liés par un pacte civil de solidarité, conclu depuis au moins deux ans, le bénéfice de l'indemnité pour charges militaires au taux particulier n° 1 applicable aux militaires mariés ; <br/>
<br/>
              7. Considérant, en premier lieu, que le principe d'égalité ne s'oppose pas à ce que l'autorité investie du pouvoir réglementaire règle de façon différente des situations différentes ni à ce qu'elle déroge à l'égalité pour des raisons d'intérêt général, pourvu que, dans l'un comme dans l'autre cas, la différence de traitement qui en résulte soit en rapport avec l'objet de la norme qui l'établit et ne soit pas manifestement disproportionnée au regard des différences de situation susceptibles de la justifier ; <br/>
<br/>
              8. Considérant que la différence de traitement instituée par le décret du 10 janvier 2011 entre militaires mariés et militaires liés par un pacte civil de solidarité n'apparaît pas manifestement disproportionnée au regard des différences existant entre le régime juridique du mariage et celui du pacte civil de solidarité ; que M. B...ne peut utilement invoquer une rupture d'égalité entre militaires et fonctionnaires civils, lesquels sont placés dans des situations différentes ; qu'il résulte de ce qui précède que le ministre de la défense pouvait légalement refuser à M. B...le bénéfice de l'indemnité pour charges militaires au taux particulier n° 1 applicable aux militaires mariés pour la période postérieure au 13 janvier 2011, date d'entrée en vigueur du décret du 10 janvier 2011 ;<br/>
<br/>
              9. Considérant cependant, en second lieu, que l'annulation par le président du tribunal administratif de Bordeaux dans la partie de son ordonnance devenue définitive, de la décision du ministre de la défense en tant qu'elle refuse à M. B...le bénéfice de l'indemnité pour charges militaires au taux particulier n° 1 pour la période allant du 3 décembre 2010 au 13 janvier 2011 implique nécessairement, sur cette période, l'octroi à M. B...de l'indemnité pour charges militaires au taux particulier n° 1, compte tenu de l'absence, d'une part, de toute différenciation légale entre les militaires mariés et les militaires liés par un pacte civil de solidarité durant la période litigieuse et, d'autre part, d'éléments produits par le ministre de la défense susceptibles de fonder le rejet de la demande de M.B... ; qu'il y a lieu, par suite, pour le Conseil d'Etat d'ordonner que l'Etat procède, au cas où il ne l'aurait pas déjà fait, au versement à M. B...des sommes correspondantes à l'indemnité pour charges militaires au taux particulier n° 1 à compter de la date de conclusion de son pacte civil de solidarité, soit le 3 décembre 2010, et jusqu'au 13 janvier 2011, dans un délai de deux mois à compter de la notification de la présente décision ; que, dans les circonstances de l'espèce, il n'y a pas lieu d'assortir cette injonction d'une astreinte ; <br/>
<br/>
              10. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement des sommes demandées par M. B...en application des dispositions de l'article L. 761-1 du code de justice administrative, au titre des frais exposés par lui et non compris dans les dépens, tant devant le tribunal administratif de Bordeaux que devant le conseil d'Etat ;   <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du président du tribunal administratif de Bordeaux du 5 mars 2013 est annulée en tant qu'elle rejette le surplus des conclusions de M. B...tendant, d'une part, à l'annulation de la décision du ministre de la défense en tant qu'elle lui refuse le bénéfice de l'indemnité pour charges militaires au taux particulier n° 1 pour la période postérieure au 13 janvier 2011 et, d'autre part, à ce qu'il soit enjoint à l'Etat de lui verser ladite indemnité.<br/>
Article 2 : Il est enjoint à l'Etat de verser à M.B..., au cas où il ne l'aurait pas déjà fait, les sommes correspondantes à l'indemnité pour charges militaires au taux particulier n° 1 à compter de la date de conclusion de son pacte civil de solidarité, soit le 3 décembre 2010, et jusqu'au 13 janvier 2011, dans un délai de deux mois à compter de la notification de la présente décision.<br/>
Article 3 : Le surplus des conclusions présentées devant le Conseil d'Etat et le tribunal administratif de Bordeaux par M. B...est rejeté. <br/>
Article 4 : La présente décision sera notifiée à M. A... B...et au ministre de la défense. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
