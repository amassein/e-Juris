<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037034087</ID>
<ANCIEN_ID>JG_L_2018_06_000000413271</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/03/40/CETATEXT000037034087.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 07/06/2018, 413271, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413271</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>M. Jean-Yves Ollier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:413271.20180607</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...Gonzalez a demandé au tribunal administratif de Limoges d'annuler pour excès de pouvoir la décision du 23 janvier 2013 par laquelle le ministre de l'économie et des finances a refusé de prendre en compte sa préinscription à l'examen professionnel pour l'accès au grade d'attaché d'administration au sein des ministères économiques et financiers. Par un jugement n° 1300511 du 10 juillet 2015, le tribunal administratif de Limoges a annulé la décision contestée.<br/>
<br/>
              Par un arrêt n° 15BX02989 du 3 juillet 2017, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par le ministre de l'économie et des finances contre ce jugement.  <br/>
<br/>
              Par un pourvoi, enregistré le 10 août 2017 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'économie et des finances demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - la loi n° 2001-2 du 3 janvier 2001 ;<br/>
              - le décret n° 67-290 du 28 mars 1967 ;<br/>
              - le décret n° 69-697 du 18 juin 1969 ;<br/>
              - le décret n° 85-986 du 16 septembre 1985 ;<br/>
              - le décret n° 2005-1215 du 26 septembre 2005 ;<br/>
              - le décret n° 2007-537 du 10 avril 2007 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Yves Ollier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de Mme A...Gonzalez.<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme Gonzalez a intégré le ministère de l'économie et des finances en 1995 en qualité d'agent contractuel, pour y occuper des emplois relevant du réseau des services économiques de l'Etat à l'étranger ; qu'elle a été nommée, à la suite de son succès à un concours réservé, secrétaire administrative de classe normale avec effet au 8 octobre 2004, puis titularisée dans ce grade à compter du 8 octobre 2005 ; qu'elle a été placée en position de détachement sur contrat du 8 octobre 2004 au 31 décembre 2012 pour continuer d'y occuper des emplois relevant de ce réseau, successivement à la mission économique à Istanbul en tant que rédacteur principal, puis au service économique de Lima en tant que rédacteur bilingue,  avant de réintégrer le corps des secrétaires administratifs le 1er janvier 2013 et d'être affectée à la direction régionale des entreprises, de la concurrence, de la consommation, du travail et de l'emploi du Limousin ; qu'elle a présenté, le 19 décembre 2012, sa candidature à l'examen professionnel pour l'accès au grade d'attaché d'administration au sein des ministères économiques et financiers, au titre de l'année 2013 ; que, par une décision du 23 janvier 2013, le ministre de l'économie et des finances a refusé de l'admettre à participer aux épreuves de cet examen professionnel, au motif que les services qu'elle avait accomplis dans le cadre de contrats à durée déterminée lors de son détachement ne pouvaient être pris en compte pour apprécier la condition de sept années de services effectifs permettant de se présenter à l'examen professionnel pour l'accès au grade d'attaché d'administration ; que, par un  jugement du 10 juillet 2015, le tribunal administratif de Limoges a annulé cette décision ; que, par un arrêt du 3 juillet 2017, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par le ministre de l'économie et des finances contre ce jugement ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 7 du décret du 26 septembre 2005 portant dispositions statutaires communes applicables aux corps des attachés d'administration et à certains corps analogues, dans sa rédaction applicable à la date de la décision attaquée : " Les nominations au choix sont prononcées par le ministre dont relève le corps d'attachés concerné après inscription sur une liste d'aptitude établie après avis de la commission administrative paritaire. Peuvent être inscrits sur cette liste d'aptitude les fonctionnaires de l'Etat appartenant à un corps classé dans la catégorie B ou de même niveau de l'administration concernée. Les intéressés doivent justifier d'au moins neuf années de services publics, dont cinq au moins de services civils effectifs dans un corps régi par les dispositions du décret n° 94-1017 du 18 novembre 1994 susvisé. Des décrets en Conseil d'Etat peuvent prévoir que les nominations au choix sont également prononcées après sélection par la voie d'un examen professionnel, ouvert à des fonctionnaires de catégorie B (...) " ; qu'aux termes de l'article 2 du décret du 10 avril 2007 portant dispositions statutaires relatives au corps des attachés d'administration du ministère de l'économie, des finances et de l'industrie : " Outre la voie de l'inscription sur la liste d'aptitude prévue à l'article 7 du décret du 26 septembre 2005 susvisé, le recrutement au choix dans le corps des attachés d'administration du ministère de l'économie, des finances et de l'industrie peut avoir lieu par la voie d'un examen professionnel ouvert aux membres du corps des secrétaires administratifs d'administration centrale du ministère de l'économie, des finances et de l'industrie régi par le décret n° 94-1017 du 18 novembre 1994 modifié fixant les dispositions statutaires communes applicables aux corps des secrétaires administratifs des administrations de l'Etat et à certains corps analogues. Pour se présenter à l'examen professionnel, les intéressés doivent justifier, au 1er janvier de l'année au titre de laquelle l'examen professionnel est organisé, d'au moins sept années de services effectifs dans un corps ou cadre d'emploi de catégorie B ou de niveau équivalent (...) " ; <br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que les membres du corps des secrétaires administratifs d'administration centrale du ministère de l'économie, des finances et de l'industrie doivent, pour que leur candidature à un recrutement au choix dans le corps des attachés d'administration soit recevable, justifier de sept années de services effectifs soit dans le corps des secrétaires administratifs, soit dans un autre corps ou cadre d'emploi de catégorie B, que ces services aient été effectués en position d'activité ou de détachement ; qu'en revanche, les services accomplis par les membres du corps dans la position de détachement ne peuvent, sous réserve de dispositions particulières, être pris en compte lorsqu'ils ne sont pas détachés dans un autre corps ou cadre d'emploi de catégorie B ; <br/>
<br/>
              4. Considérant, toutefois, qu'aux termes de l'article 1er du décret du 18 juin 1969 portant fixation du statut des agents contractuels de l'Etat et des établissements publics de l'Etat à caractère administratif, de nationalité française, en service à l'étranger, en application duquel ont été conclus les contrats proposés à Mme Gonzalez : " Les dispositions du présent décret sont applicables aux agents contractuels de nationalité française relevant de l'Etat et des établissements publics de l'Etat à caractère administratif en service à l'étranger. (...) / Des arrêtés conjoints du ministre de l'économie et des finances, du ministre des affaires étrangères et du secrétaire d'Etat auprès du Premier ministre, chargé de la fonction publique, pris sur proposition du ministre intéressé, définiront pour chaque ministère les emplois et préciseront en tant que de besoin les pays étrangers auxquels les dispositions du présent décret sont applicables. / Les emplois susvisés peuvent être confiés soit à des agents non titulaires, soit à des agents titulaires. Dans ce dernier cas, les agents sont détachés dans les conditions prévues par l'ordonnance n° 59-244 du 4 février 1959 relative au statut général des fonctionnaires " ; que l'arrêté interministériel du 24 avril 1972 a fixé les conditions d'application de ces dispositions aux agents contractuels du ministère de l'économie et des finances à l'étranger et défini les emplois concernés qui comprennent, au sein de la catégorie B, ceux de rédacteur bilingue et de rédacteur principal, successivement occupés par Mme Gonzalez ; que Mme Gonzalez, secrétaire administrative d'administration centrale du ministère de l'économie, des finances et de l'industrie affectée dans un  service de l'Etat à l'étranger, a ainsi fait l'objet, faute de pouvoir être maintenue en position d'activité ou détachée dans un corps d'accueil, d'un détachement sur contrat pour exercer des fonctions relevant de son corps d'origine ou d'un autre corps ou cadre d'emploi de catégorie B ; <br/>
<br/>
              5. Considérant que les dispositions de l'article 2 du décret du 10 avril 2007 citées au point 2 ne sauraient être interprétées comme excluant la prise en compte de services accomplis à l'étranger dans un emploi relevant d'un corps ou cadre d'emploi de catégorie B par un fonctionnaire du seul fait que le régime institué par le décret du 18 juin 1969 prévoit, dans une telle hypothèse, un détachement sur contrat ;<br/>
<br/>
              6. Considérant que, dès lors, le ministre de l'économie et des finances a commis une erreur de droit en estimant que les services accomplis par Mme Gonzalez dans le cadre de ces contrats ne pouvaient être pris en compte pour apprécier la condition de sept années de services effectifs  prévue par l'article 2 du décret du 10 avril 2007 ; que ce motif, qui répond au moyen invoqué devant les juges du fond et dont l'examen n'implique l'appréciation d'aucune circonstance de fait qui ne résulte de l'arrêt attaqué, doit être substitué au motif retenu par la cour administrative d'appel ; que, par suite, le pourvoi du ministre de l'économie et des finances doit être rejeté ;<br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 500 euros à verser à Mme Gonzalez au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre de l'économie et des finances est rejeté. <br/>
Article 2 : L'Etat versera à Mme Gonzalez une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'économie et des finances et à Mme A... Gonzalez.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
