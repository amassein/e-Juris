<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043486327</ID>
<ANCIEN_ID>JG_L_2021_05_000000428154</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/48/63/CETATEXT000043486327.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 06/05/2021, 428154, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-05-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428154</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:428154.20210506</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. D... E... et Mme C... B..., agissant en leur nom personnel et en qualité de représentants de leur fils A..., ont demandé au tribunal administratif de Lille de condamner le centre hospitalier de Valenciennes à leur verser une provision au titre des préjudices résultant de la paralysie dont A... E... est atteint depuis sa naissance dans cet établissement. La caisse primaire d'assurance maladie (CPAM) du Hainaut a demandé la condamnation du centre hospitalier à lui rembourser ses débours. Par un jugement n° 1400861 du 9 novembre 2016, le tribunal administratif a condamné, à titre provisionnel, le centre hospitalier à verser à Swann E... une somme de 18 400 euros, à ses parents une provision de 1 200 euros chacun et à la CPAM du Hainaut une somme de 8 260,74 euros.<br/>
<br/>
              Par un arrêt n° 16DA02538, 17DA00055 du 18 décembre 2018, la cour administrative d'appel de Douai a, sur appels de M. E..., de Mme B... et du centre hospitalier de Valenciennes, ramené aux sommes respectives de 6 900 euros, 450 euros chacun et 3 097,78 euros le montant des indemnités provisionnelles que le centre hospitalier de Valenciennes est condamné à verser à A... E..., à ses parents et à la CPAM du Hainaut.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 février et 20 mai 2019 au secrétariat du contentieux du Conseil d'Etat, M. E... et Mme B... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge du centre hospitalier de Valenciennes la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
			               Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de M. E... et de Mme B... et à Me Le Prado, avocat du centre hospitalier de Valenciennes.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme B... a accouché en avril 2012, au centre hospitalier de Valenciennes, d'un enfant dénommé A... qui présentait à sa naissance un poids de plus de 5 kg et pour lequel s'est produite, au moment de l'expulsion, une dystocie des épaules. A la suite des manoeuvres effectuées par l'équipe obstétricale pour extraire l'enfant, celui-ci est resté atteint d'une paralysie du plexus brachial. Par un jugement du 9 novembre 2016, le tribunal administratif de Lille a condamné le centre hospitalier à verser à Mme B... et à son conjoint, M. E..., tant en leur nom propre qu'au nom de leur fils mineur, des indemnités provisionnelles s'élevant globalement à 20 800 euros. M. E... et Mme B... se pourvoient en cassation contre l'arrêt du 18 décembre 2018 par lequel la cour administrative d'appel de Douai a, sur appel du centre hospitalier de Valenciennes, diminué ces indemnités en les ramenant à un montant global de 7 800 euros.<br/>
<br/>
              2. Dans le cas où la faute commise lors de la prise en charge ou le traitement d'un patient dans un établissement public de santé a compromis ses chances d'obtenir une amélioration de son état de santé ou d'échapper à son aggravation, le préjudice résultant directement de la faute commise par l'établissement et qui doit être intégralement réparé n'est pas le dommage corporel constaté, mais la perte de chance d'éviter que ce dommage advienne. La réparation qui incombe à l'hôpital doit alors être évaluée à une fraction du dommage corporel, déterminée en fonction de l'ampleur de la chance perdue.<br/>
<br/>
              3. Il résulte des termes mêmes de l'arrêt attaqué que, pour condamner le centre hospitalier de Valencienne à indemniser les requérants, la cour administrative d'appel a, par une appréciation souveraine qui n'est pas contestée en cassation, retenu que, lors de l'accouchement de Mme B... et après que la dystocie des épaules ait été diagnostiquée, une sage-femme a tenté une manoeuvre de " rotation paradoxale " de la tête de l'enfant qui était, en l'espèce, formellement contre-indiquée et susceptible de provoquer un arrachement des racines du plexus brachial, sans qu'il soit possible de savoir si la lésion dont souffre le jeune A... était liée à ce geste ou déjà présente au moment où il a été effectué. La cour en a déduit que l'acte fautif commis par l'équipe médicale était en lien direct, non avec le dommage subi par les victimes mais avec la perte de chance d'éviter que ce dommage advienne.<br/>
<br/>
              4. Il résulte également des termes de l'arrêt attaqué que, pour estimer le taux de cette perte de chance, la cour a, au vu notamment des expertises figurant dans le dossier qui lui était soumis, estimé à 50 % le taux de perte de chance d'éviter la paralysie du plexus brachial provoqué par ce geste de " rotation paradoxale ". Elle a ensuite ramené ce taux à 30 % en raison de la rapidité avec laquelle l'équipe obstétricale avait, après la manoeuvre fautive, obtenu l'expulsion du foetus et elle a, enfin, ramené ce même taux à 15% en raison de ce que Mme B..., faute d'observer les consignes d'hygiène alimentaire et d'activité physique qui lui avaient été prodiguées pendant sa grossesse, avait connu une importante prise de poids, augmentant ainsi le risque d'un poids élevé de son enfant à la naissance et donc celui d'une dystocie des épaules lors de l'accouchement.<br/>
<br/>
              5. Pour l'application de la règle rappelée au point 2, le taux de perte de chance d'éviter la paralysie du plexus brachial du jeune A... imputable à une faute de l'établissement de santé correspond à la probabilité que, en l'absence de tout geste fautif commis par l'équipe médicale et compte tenu de l'état de santé de la mère et de l'enfant, notamment du surpoids de ce dernier, la naissance se soit déroulée sans entraîner le dommage en litige.<br/>
<br/>
              6. Par suite, en premier lieu, en jugeant que l'exécution diligente des manoeuvres obstétricales, postérieurement au geste dont elle avait estimé qu'il avait fait perdre 50 % de chances d'une naissance sans dommage au jeune A..., justifiait qu'on révise ce taux à la baisse, alors que la bonne exécution des manoeuvres obstétricales après le geste erroné commis par la sage-femme était seulement de nature à ne pas aggraver la perte de chance d'une naissance sans dommage et donc à ne pas accroître ce taux, la cour a entaché son arrêt d'une erreur de droit. <br/>
<br/>
              7. En second lieu, en jugeant que le surpoids de Mme B..., parce qu'il aurait concouru au surpoids de son enfant et donc à un risque accru de dystocie des épaules lors de l'accouchement, devait se traduire par une réduction du taux de perte de chance imputable à la faute commise par l'établissement de santé, alors que ce taux incorporait nécessairement, ainsi qu'il a été dit au point 5 et en raison de son objet même, la probabilité d'un accouchement dystocique en l'absence de toute faute de l'établissement, la cour administrative d'appel a commis une autre erreur de droit.<br/>
<br/>
              8. Par suite, sans qu'il soit besoin de se prononcer sur les autres moyens de leur pourvoi, M. E... et Mme B... sont fondés à demander l'annulation de l'arrêt qu'ils attaquent.<br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier de Valenciennes le versement à M. E... et à Mme B... d'une somme de 1 500 euros chacun, au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				 --------------<br/>
<br/>
Article 1er : L'arrêt du 18 décembre 2018 de la cour administrative d'appel de Douai est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Douai.<br/>
<br/>
Article 3 : Le centre hospitalier de Valenciennes versera à M. E... et à Mme B... une somme de 1 500 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. D... E..., premier requérant désigné et au centre hospitalier de Valenciennes.<br/>
		Copie en sera adressée à la caisse primaire d'assurance maladie du Hainaut.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
