<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033508871</ID>
<ANCIEN_ID>JG_L_2016_11_000000393208</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/50/88/CETATEXT000033508871.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 30/11/2016, 393208, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-11-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393208</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:393208.20161130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et deux mémoires en réplique, enregistrés les 4 septembre 2015, 25 mai 2016 et 4 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, M. A... B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 6 juillet 2015 par laquelle la présidente de la Commission nationale de l'informatique et des libertés (CNIL) l'a informé de la clôture de la plainte qu'il a déposée pour obtenir la communication des données relatives à la demande de logement social qu'il a présentée auprès de la commune de Garges-lès-Gonesses ; <br/>
<br/>
              2°) d'ordonner une enquête afin de contrôler les pratiques de la direction du logement de la mairie de Garges-lès-Gonesses.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la construction et de l'habitation ;<br/>
              - le code du patrimoine ; <br/>
              - la loi n° 78-17 du 6 janvier 1978 ; <br/>
              - le décret n° 2015-522 du 12 mai 2015 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 6 de la loi du 6 janvier 1978 : " Un traitement ne peut porter que sur des données à caractère personnel qui satisfont aux conditions suivantes : (...) 4° Elles sont exactes, complètes et, si nécessaire, mises à jour ; les mesures appropriées doivent être prises pour que les données inexactes ou incomplètes au regard des finalités pour lesquelles elles sont collectées ou traitées soient effacées ou rectifiées ". Aux termes de l'article 39 de la même loi : " I.-Toute personne physique justifiant de son identité a le droit d'interroger le responsable d'un traitement de données à caractère personnel en vue d'obtenir : (...) 4° La communication, sous une forme accessible, des données à caractère personnel qui la concernent ainsi que de toute information disponible quant à l'origine de celles-ci ". Et aux termes de l'article 40 de cette loi : " Toute personne physique justifiant de son identité peut exiger du responsable d'un traitement que soient, selon les cas, rectifiées, complétées, mises à jour, verrouillées ou effacées les données à caractère personnel la concernant, qui sont inexactes, incomplètes, équivoques, périmées, ou dont la collecte, l'utilisation, la communication ou la conservation est interdite. /  Lorsque l'intéressé en fait la demande, le responsable du traitement doit justifier, sans frais pour le demandeur, qu'il a procédé aux opérations exigées en vertu de l'alinéa précédent ".<br/>
<br/>
              2. Il ressort des pièces du dossier que M. B...a saisi le 2 juin 2014 la Commission nationale de l'informatique et des libertés (CNIL) d'une plainte à l'encontre de la direction du logement de la mairie de Garges-lès-Gonesses afin d'obtenir communication des données relatives à sa demande de logement social. A la suite de la transmission à laquelle la commune a procédé le 7 août 2014 ainsi qu'aux informations ultérieurement fournies sur le mode d'actualisation des demandes de logement social, la présidente de la CNIL a décidé de la clôture de cette plainte.<br/>
<br/>
              3. En premier lieu, en mentionnant l'ensemble des diligences opérées par ses services ainsi que les résultats obtenus auprès de la commune de Garges-les-Gonesses, la présidente de la CNIL a fourni les éléments de droit et de fait sur lesquels elle a fondé sa décision de clôture de la plainte du requérant. Le moyen tiré de l'insuffisante motivation de sa décision doit dès lors être écarté. <br/>
<br/>
              4. En deuxième lieu, l'absence de mention des recours administratifs ouverts contre la décision attaquée est sans incidence sur sa légalité et ne peut dès lors être utilement invoquée par le requérant. <br/>
<br/>
              5. En troisième lieu, aux termes du troisième alinéa de l'article L. 441-2-1 du code de la construction et de l'habitation : " Les informations fournies par le demandeur lors de sa demande ou à l'occasion des modifications éventuelles de celle-ci sont enregistrées dans le système national d'enregistrement dans les mêmes conditions. Il en est de même des informations permettant d'apprécier la situation du demandeur au regard des dispositions de la présente section ". Et aux termes de l'article R. 441-2-7 du même code, modifié par le décret n° 2015-522 du 12 mai 2015 : " La demande de logement social a une durée de validité d'un an à compter de sa présentation initiale ou, le cas échéant, de son dernier renouvellement. / Un mois au moins avant la date d'expiration de validité de la demande, le demandeur reçoit notification, par lettre recommandée avec accusé de réception ou tout autre moyen permettant d'attester la remise ou par voie électronique lorsque le demandeur a enregistré ou renouvelé au moins une fois sa demande par cette voie, de la date à laquelle sa demande cessera d'être valide si la demande n'est pas renouvelée (...). / Lors du renouvellement de la demande, le demandeur actualise les informations contenues dans sa demande initiale ou fournies lors du dernier renouvellement (...) /  Toute mise à jour ou correction des informations contenues dans la demande est effectuée sous le numéro d'enregistrement délivré lors de la présentation initiale de la demande, en conservant la date de cette présentation initiale ".<br/>
<br/>
              6. Aux termes de l'article L. 212-2 du code du patrimoine : " A l'expiration de leur période d'utilisation courante, les archives publiques autres que celles mentionnées à l'article L. 212-3 font l'objet d'une sélection pour séparer les documents à conserver des documents dépourvus d'utilité administrative ou d'intérêt historique ou scientifique, destinés à l'élimination ".<br/>
<br/>
              7. Pour soutenir que la commune ne lui aurait pas transmis, le 7 août 2014, l'intégralité des données personnelles relatives à sa demande de logement social, le requérant invoque l'absence d'un formulaire administratif remis par les services de la commune le 25 mars 2013 ainsi que deux courriers qu'il a adressés au maire les 6 décembre 2012 et 15 novembre 2013. Toutefois, dès lors que la présentation d'une nouvelle demande de logement portant sur une localisation différente conduit à la suppression du dossier relatif à la précédente demande, devenu inutile, la CNIL a pu légalement estimer que les données personnelles que contenait la demande initiale n'avaient pas à être conservées, en application des dispositions précitées de l'article L. 212-2 du code du patrimoine, et en déduire que cette suppression n'avait pas fait obstacle au respect du droit d'accès de M. B...aux données relatives à sa demande de logement social.<br/>
<br/>
              8. Si le requérant soutient, en outre, que la CNIL n'aurait pas respecté son droit à rectification de ses données personnelles auprès de la direction du logement de la mairie, il ne ressort pas des pièces du dossier que les deux formulaires Cerfa des 5 février et 1er avril 2014 qui contiendraient des informations erronées, et que l'intéressé a lui-même remplis, sont issus du système national d'enregistrement des demandes de logement social mentionné à l'article L. 441-2-1 du code de la construction et de l'habitation cité au point 5. Le moyen tiré de ce que la CNIL ne se serait pas acquittée, avant de prendre la décision de clôture attaquée, des diligences requises en matière de rectification de ses données personnelles ne peut dès lors qu'être écarté. <br/>
<br/>
              9. Il résulte de tout ce qui précède que M. B...n'est pas fondé à demander l'annulation de la décision du 6 juillet 2015 par laquelle la CNIL l'a informé de la clôture de sa plainte relative à la communication de données personnelles le concernant, détenues par la direction du logement de la mairie de Garges-lès-Gonesses. Ses conclusions tendant à ce que soit ordonnée une enquête afin de contrôler les pratiques de cette direction doivent être, par voie de conséquence, rejetées.  <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et à la Commission nationale de l'informatique et des libertés.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
