<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000045072750</ID>
<ANCIEN_ID>JG_L_2022_01_000000433993</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/45/07/27/CETATEXT000045072750.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 24/01/2022, 433993, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2022-01-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433993</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SARL DIDIER-PINET ; SCP MELKA-PRIGENT-DRUSCH</AVOCATS>
<RAPPORTEUR>Mme Thalia Breton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2022:433993.20220124</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. C... L... a demandé au tribunal administratif de Lyon d'annuler pour excès de pouvoir les délibérations du 13 décembre 2013 par lesquelles le conseil d'administration de l'Ecole normale supérieure de Lyon a approuvé les statuts de l'établissement public de coopération culturelle Musée des confluences, le versement d'une subvention de 86 063 euros à l'université de Lyon, ainsi que la convention constitutive du groupement de commandes LyRES en tant qu'elle concerne l'Université catholique de Lyon et l'Institut polytechnique de Lyon. Par un jugement n° 1308662 du 2 février 2017, le tribunal administratif a annulé la délibération du 13 décembre 2013 en tant qu'elle autorise le versement d'une subvention d'un montant de 86 063 euros à l'Université de Lyon et la délibération du même jour approuvant la convention constitutive du groupement de commandes LyRES, et rejeté le surplus des conclusions de la demande. <br/>
<br/>
              Par un arrêt n° 17LY01350 du 27 juin 2019, la cour administrative d'appel de Lyon a, sur appel de l'Ecole normale supérieure de Lyon, partiellement annulé ce jugement et rejeté le surplus des conclusions de la demande de M. L.... <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 27 août et 21 novembre 2019 et le 24 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. L... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Ecole normale supérieure de Lyon la somme de 3500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'éducation ; <br/>
              - le décret n° 2009-1533 du 10 décembre 2009 ; <br/>
              - le décret n° 2012-715 du 7 mai 2012 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Thalia Breton, auditrice,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SARL Didier-Pinet, avocat de M. L... et à la SCP Melka-Prigent-Drusch, avocat de l'Ecole normale supérieure de Lyon ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que par trois délibérations du 13 décembre 2013, le conseil d'administration de l'Ecole normale supérieure (ENS) de Lyon a approuvé les statuts de l'établissement public de coopération culturelle " Musée des confluences ", le versement d'une subvention au pôle de recherche et d'enseignement supérieur (PRES) " Université de Lyon " d'un montant de 86 063 euros pour l'année 2014 ainsi que la convention constitutive du groupement de commandes en vue de la passation de marchés de services dans le cadre du réseau métropolitain universitaire LyRES. Par un jugement du 2 février 2017, le tribunal administratif de Lyon a annulé, sur demande de M. L..., la délibération approuvant le versement d'une subvention au PRES " Université de Lyon " et la délibération approuvant la convention constitutive du groupement de commandes LyRES, estimant que le conseil d'administration avait été irrégulièrement convoqué par M. J..., et rejeté pour irrecevabilité le surplus des conclusions de M. L.... Toutefois, par un arrêt du 27 juin 2019, la cour administrative d'appel de Lyon, saisie par l'ENS de Lyon, a annulé ce jugement et rejeté la demande de M. L.... Ce dernier se pourvoit en cassation contre cet arrêt. <br/>
<br/>
              2. D'une part, aux termes de l'article 5 du décret du 10 décembre 2009 portant création de l'Ecole normale supérieure de Lyon : " Le président de l'école et le directeur général sont nommés pour un mandat d'une durée de cinq ans renouvelable une fois, par décret du Président de la République, pris sur le rapport du ministre chargé de l'enseignement supérieur, sans considération de nationalité, après appel de candidatures. (...) Dans le cas où le président ou le directeur général cessent leurs fonctions pour quelque cause que ce soit, un nouveau président ou un nouveau directeur général est nommé pour la durée du mandat de son prédécesseur restant à courir. Si la vacance intervient au cours du dernier tiers du mandat, le nouveau président ou le nouveau directeur général peut exercer deux autres mandats. " Aux termes de l'article 20 du même décret : " Par dérogation à l'article 5, le directeur de l'Ecole normale supérieure de Lyon assure, à la date d'entrée en vigueur du présent décret, pour une durée de trois ans, les fonctions de président de l'Ecole normale supérieure de Lyon créée par le présent décret. Il préside le conseil d'administration provisoire, en fixe l'ordre du jour et s'assure de la mise en œuvre de ses délibérations. (...) Par dérogation à l'article 5, le directeur de l'Ecole normale supérieure de Fontenay - Saint-Cloud assure, à la date d'entrée en vigueur du présent décret, pour une durée de trois ans, les fonctions de directeur général de l'Ecole normale supérieure de Lyon créée par le présent décret. Il élabore le règlement intérieur de l'école et organise les élections au conseil d'administration et au conseil scientifique, dans un délai de trois mois après l'adoption du règlement intérieur. Dans le cas où le président de l'école ou le directeur général cessent leurs fonctions pour quelque cause que ce soit, un nouveau président et un nouveau directeur général sont désignés dans les conditions fixées à l'article 5. " En outre, aux termes de l'article 20 du décret du 7 mai 2012 fixant les règles d'organisation et de fonctionnement de l'Ecole normale supérieure de Lyon, applicable au litige : " (...) le président et le directeur général de l'Ecole normale supérieure de Lyon en fonction à la date de publication du présent décret, exercent leurs attributions respectives, telles que définies par le présent décret, jusqu'au 31 décembre 2013. ".<br/>
<br/>
              3. D'autre part, aux termes de l'article 6 du décret du 7 mai 2012 : " Le président de l'école préside le conseil d'administration, en fixe l'ordre du jour (...) ". Et aux termes de l'article 7 du même décret, dans sa rédaction applicable au litige : " Le directeur général (...) assiste aux séances du conseil d'administration (...) avec voix consultative. (...) ". <br/>
<br/>
              4. En premier lieu, il ressort des pièces du dossier soumis aux juges du fond que M. J... a été nommé directeur de l'ENS de Lyon par décret du 15 mai 2008. En application des dispositions transitoires de l'article 20 du décret du 10 décembre 2009, citées au point 2, M. J... a assuré les fonctions de président de l'ENS de Lyon à compter de l'entrée en vigueur de ce décret. Si la cessation de fonctions du directeur général de cet établissement en juillet 2011 devait conduire, en application des dispositions de l'article 5 du décret du 10 décembre 2009 alors en vigueur, à la nomination d'un nouveau président et d'un nouveau directeur général, la circonstance que ces dispositions n'avaient pas été mises en œuvre n'avait pas mis fin, par elle-même, à l'exercice par M. J... des fonctions de président de l'ENS de Lyon. Il en résulte que M. J... ayant toujours la qualité de président de l'ENS de Lyon lors de l'entrée en vigueur du décret du 7 mai 2012, son mandat de président a été prorogé jusqu'au 31 décembre 2013, par application des dispositions de l'article 20  de ce décret, contrairement à ce qu'a jugé la cour administrative d'appel de Lyon. Ce motif, dont l'examen n'implique l'appréciation d'aucune circonstance de fait, doit être substitué au motif erroné retenu par l'arrêt attaqué, dont il justifie le dispositif.<br/>
<br/>
              5. En deuxième lieu, si la cour administrative d'appel de Lyon a retenu que M. J... ne s'était pas maintenu abusivement en fonctions après le 19 avril 2012, que son maintien n'avait fait l'objet d'aucune contestation et que le ministre chargé des universités avait reconnu implicitement cette qualité, il résulte de la substitution de motif effectuée au point 4 que M. L... ne peut utilement soutenir que ces constatations procèderaient d'une dénaturation des pièces du dossier. <br/>
<br/>
              6. En troisième lieu, il ressort des termes mêmes de l'arrêt attaqué que le moyen tiré de ce que la cour administrative d'appel de Lyon aurait entaché son arrêt d'insuffisance de motivation et d'erreur de droit en omettant de rechercher la base légale autorisant le versement d'une subvention de 86 063 euros au PRES " Université de Lyon " manque en fait.<br/>
<br/>
              7. Il résulte de tout ce qui précède que M. L... n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque. Son pourvoi doit, dès lors, être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions de l'ENS de Lyon présentées au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. L... est rejeté.<br/>
Article 2 : Les conclusions de l'Ecole nationale supérieure de Lyon présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. C... L... et à l'Ecole normale supérieure de Lyon. <br/>
Copie en sera adressée à la ministre de l'enseignement supérieur, de la recherche et de l'innovation. <br/>
              Délibéré à l'issue de la séance du 5 janvier 2022 où siégeaient : M. Rémy Schwartz, président adjoint de la section du contentieux, présidant ; Mme A... Q..., Mme F... P..., présidentes de chambre ; M. N... I..., Mme M... O..., Mme B... H..., M. D... K... et Mme Carine Chevrier conseillers d'Etat et Mme Thalia Breton, auditrice-rapporteure.<br/>
<br/>
              Rendu le 24 janvier 2022.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Rémy Schwartz<br/>
 		La rapporteure : <br/>
      Signé : Mme Thalia Breton<br/>
                 La secrétaire :<br/>
                 Signé : Mme E... G...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
