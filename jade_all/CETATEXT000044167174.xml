<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044167174</ID>
<ANCIEN_ID>JG_L_2021_10_000000440428</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/16/71/CETATEXT000044167174.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 04/10/2021, 440428, Publié au recueil Lebon</TITRE>
<DATE_DEC>2021-10-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440428</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Jonathan  Bosredon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:440428.20211004</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société sportive professionnelle Olympique de Marseille a demandé au tribunal administratif de Marseille de condamner la commune de Marseille à lui verser la somme de 1 003 325 euros, majorée des intérêts au taux légal ainsi que la capitalisation de ceux-ci, en réparation du préjudice qu'elle estime avoir subi du fait de l'indisponibilité du stade Vélodrome le 16 août 2009. Par un jugement n° 1304062 du 23 mai 2017, ce tribunal a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 17MA03262 du 23 mai 2018, la cour administrative d'appel de Marseille a rejeté l'appel formé par la société Olympique de Marseille contre ce jugement. <br/>
<br/>
              Par une décision n° 421909 du 24 avril 2019, le Conseil d'Etat a annulé cet arrêt et renvoyé l'affaire devant la cour administrative d'appel de Marseille.<br/>
<br/>
              Par un arrêt n° 19MA02108 du 6 mars 2020, la cour administrative d'appel de Marseille a rejeté à nouveau l'appel formé par la société Olympique de Marseille.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 6 mai 2020, 3 août 2020, 22 mars 2021 et 16 avril 2021 au secrétariat du contentieux du Conseil d'Etat, la société Olympique de Marseille demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Marseille la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code civil ;<br/>
              - le code de commerce ;<br/>
              - le code du sport ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jonathan Bosredon, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la société sportive professionnelle Olympique de Marseille et à Me Haas, avocat de la ville de Marseille ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la commune de Marseille a conclu le 1er juillet 2009 avec la société sportive professionnelle Olympique de Marseille une convention de mise à disposition du stade Vélodrome, valable pour la période du 1er juillet 2009 au 30 juin 2011, en vue de l'organisation des rencontres de football programmées du club de l'Olympique de Marseille. La commune de Marseille a également conclu avec la société Live Nation France une convention de mise à disposition de ce même stade pour la période du 15 au 21 juillet 2009 en vue de l'organisation d'un concert. Le 16 juillet 2009, au cours des opérations de montage de la scène de spectacle édifiée en vue de cette manifestation, la structure métallique de la scène s'est effondrée, occasionnant le décès de deux personnes. A la suite de cet accident, le match de football devant opposer, le 16 août 2009, l'Olympique Marseille et le Lille Olympique Sporting Club n'a pu avoir lieu au stade Vélodrome, mais s'est tenu au stade de la Mosson à Montpellier. La société Olympique de Marseille a demandé à la commune de Marseille de lui verser la somme de 1 003 325 euros en réparation du préjudice qu'elle estime avoir subi du fait de l'indisponibilité du stade Vélodrome le 16 août 2009. Par un jugement du 23 mai 2017, le tribunal administratif de Marseille a rejeté sa demande. La société Olympique de Marseille se pourvoit en cassation contre l'arrêt du 6 mars 2020 par lequel la cour administrative d'appel de Marseille, statuant sur renvoi après annulation d'un premier arrêt par une décision du Conseil d'Etat du 24 avril 2019, a rejeté l'appel qu'elle avait formé contre ce jugement.<br/>
<br/>
              2. Pour juger que l'effondrement de la structure scénique prévue pour le concert et l'accident mortel qui s'en est suivi constituaient un cas de force majeure de nature à exonérer la commune de Marseille de toute responsabilité au regard du manquement aux obligations contractuelles résultant des stipulations de l'article 4.1 de la convention que celle-ci avait conclue le 1er juillet 2009 avec la société Olympique de Marseille, qu'elle a regardé comme établi dès lors que la commune n'a pas été à même de mettre le stade Vélodrome à disposition de cette société pour la rencontre sportive prévue le 16 août 2009, la cour administrative d'appel de Marseille s'est fondée sur la circonstance que l'effondrement de la structure scénique et l'accident mortel qui s'en est suivi n'avaient pas pour origine une faute de la commune de Marseille, laquelle était étrangère à l'opération de montage de cette structure, et résultaient de faits qui étaient extérieurs à cette commune et avaient le caractère d'un événement indépendant de sa volonté, qu'elle était impuissante à prévenir et empêcher. En statuant ainsi, alors que l'indisponibilité du stade, bien qu'elle résulte de fautes commises par la société Live Nation France et les sous-traitants de cette dernière dans le montage de la structure scénique, n'aurait pu survenir sans la décision initiale de la commune de Marseille de mettre le stade Vélodrome à disposition de cette société pour l'organisation d'un concert, la cour a inexactement qualifié les faits soumis à son appréciation.<br/>
<br/>
              3. Il s'ensuit, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que la société Olympique de Marseille est fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              4. Aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'État statue définitivement sur cette affaire ". Le Conseil d'État étant saisi, en l'espèce, d'un second pourvoi en cassation, il lui incombe de régler l'affaire au fond.<br/>
<br/>
              Sur la recevabilité de l'appel de la société Olympique de Marseille :<br/>
<br/>
              5. En premier lieu, la présentation d'une action par un avocat ou un avocat au Conseil d'Etat et à la Cour de cassation ne dispense pas le juge administratif de s'assurer, le cas échéant, lorsque la partie en cause est une personne morale, que le représentant de cette personne morale justifie de sa qualité pour engager cette action. Une telle vérification n'est toutefois pas normalement nécessaire lorsque la personne morale requérante est dotée, par des dispositions législatives ou réglementaires, de représentants légaux ayant de plein droit qualité pour agir en justice en son nom. Il résulte des dispositions combinées des articles L. 225-64 et L. 225-66 du code de commerce applicables aux sociétés anonymes disposant d'un directoire et d'un conseil de surveillance, ainsi que, par renvoi, de celles mentionnées au 3° de l'article L. 122-2 du code du sport applicables aux sociétés disposant de tels organes, que le président du directoire ou le directeur général unique ainsi que, le cas échéant, le directeur général, sont investis des pouvoirs les plus étendus pour agir en toute circonstance au nom de la société et représentent la société dans ses rapports avec les tiers. Ils ont ainsi, de plein droit, qualité pour agir en justice au nom de la société.<br/>
<br/>
              6. Il résulte de l'instruction que M. Jacques-Henri Eyraud, président du directoire de la société Olympique de Marseille à la date du pourvoi qu'il a formé devant le Conseil d'Etat au nom de cette société, a conclu à ce que le Conseil d'Etat, réglant l'affaire au fond, fasse droit à la requête d'appel de cette société. Par suite, l'intéressé doit, en tout état de cause, être regardé comme s'étant approprié dans ce cadre les conclusions de cette requête. Il s'ensuit que le moyen tiré de ce que la société Olympique de Marseille n'aurait pas été représentée par une personne ayant qualité pour former l'appel objet de la présente instance doit être écarté.<br/>
<br/>
              7. En deuxième lieu, aux termes de l'article R. 411-1 du code de justice administrative : " La juridiction est saisie par requête. La requête (...) contient l'exposé des faits et moyens, ainsi que l'énoncé des conclusions soumises au juge ". La requête d'appel présentée par la société Olympique de Marseille satisfaisant à ces exigences, la fin de non-recevoir soulevée par la commune de Marseille doit être écartée.<br/>
<br/>
              Sur la recevabilité de la demande formée devant le tribunal administratif par la société Olympique de Marseille :<br/>
<br/>
              8. En premier lieu, il résulte de ce qui est dit au point 5 que M. A..., alors président du directoire de la société Olympique de Marseille, avait qualité pour introduire la demande soumise au tribunal administratif le 20 juin 2013.<br/>
<br/>
              9. En deuxième lieu, aucune fin de non-recevoir tirée du défaut de décision préalable ne peut être opposée à un requérant ayant introduit devant le juge administratif un contentieux indemnitaire à une date où il n'avait présenté aucune demande en ce sens devant l'administration lorsqu'il a formé, postérieurement à l'introduction de son recours juridictionnel, une demande auprès de l'administration sur laquelle le silence gardé par celle-ci a fait naître une décision implicite de rejet avant que le juge de première instance ne statue. Il résulte de l'instruction que la société Olympique de Marseille a demandé le 14 mars 2017 à la commune de Marseille de l'indemniser du préjudice qu'elle estimait avoir subi du fait de l'indisponibilité du stade Vélodrome pour la rencontre du 16 août 2009. Cette demande ayant été rejetée par le silence gardé par la commune de Marseille avant que le tribunal administratif statue, le 23 mai 2017, celle-ci n'est ainsi pas fondée à soutenir que la demande indemnitaire de la société Olympique de Marseille serait irrecevable faute de liaison du contentieux.<br/>
<br/>
              Sur la responsabilité contractuelle de la commune de Marseille :<br/>
<br/>
              10. Aux termes de l'article 2 de la convention conclue le 1er juillet 2009 entre la commune de Marseille et la société Olympique de Marseille : " La convention a pour objet de déterminer les conditions dans lesquelles le stade Vélodrome est mis à disposition de l'OM pour l'organisation de ses rencontres programmées de football. La ville de Marseille conserve la disposition du stade Vélodrome et en assure l'entretien gros et menu, la gestion ainsi que l'exploitation des différentes activités susceptibles de s'y dérouler en dehors des périodes de mise à disposition prévues par la convention. ". Aux termes de l'article 4.1 de cette même convention : " La ville de Marseille met le stade Vélodrome à la disposition de l'OM pour l'organisation des rencontres programmées dans les conditions définies par la convention. L'OM est le " club résident " de football du stade Vélodrome, prioritaire en cette qualité pour l'utilisation du stade Vélodrome dans le cadre des rencontres officielles (...). La ville de Marseille prend les mesures appropriées pour que le stade Vélodrome puisse permettre l'organisation des rencontres officielles dans des conditions compatibles avec les normes impératives ". Selon l'article 1er de cette même convention, les " normes impératives " s'entendent de " toute règlementation ou norme impérative applicable aux enceintes sportives recevant du public émanant des autorités administratives compétentes ainsi que toute prescription ou décision en matière de sécurité des spectateurs et des sportifs édictées par les autorités nationales, européennes et internationales du football dont la mise en œuvre est nécessaire pour permettre l'organisation des rencontres de l'OM dans le stade ".<br/>
<br/>
              11. Il résulte de l'instruction que le match devant opposer l'Olympique de Marseille au Lille Olympique Sporting Club le 16 août 2009, joué au titre de la deuxième journée du championnat de France de ligue 1, constituait une rencontre programmée au sens des stipulations de la convention citées au point 10. La commune de Marseille n'ayant pas été en mesure de mettre le stade Vélodrome à disposition de la société Olympique de Marseille en vue de cette rencontre sportive, cette dernière, qui s'est trouvée contrainte d'organiser la rencontre dans un autre lieu sans que cela résulte, contrairement à ce qui est soutenu en défense, de sa seule initiative, est fondée à soutenir que la commune de Marseille a manqué aux obligations mises à sa charge par les stipulations de l'article 4.1 de la convention conclue avec ce club sportif. En l'absence de stipulation ou, comme il a été dit au point 2 et contrairement à ce qu'a jugé le tribunal administratif, de circonstances relevant de la force majeure de nature à exonérer la commune de sa responsabilité contractuelle, la société Olympique de Marseille est fondée à demander la réparation du préjudice qui en est résulté pour elle.<br/>
<br/>
              Sur l'évaluation du préjudice : <br/>
<br/>
              12. En premier lieu, il résulte de l'instruction qu'en raison des capacités d'accueil du stade de la Mosson, limitées à 32 900 places, la rencontre sportive organisée à Montpellier le 16 août 2009 s'est déroulée en présence d'un nombre de spectateurs notablement inférieur à celui qui aurait pu être accueilli dans le cadre d'un match organisé au stade Vélodrome à une telle période de l'année. Il y a lieu de déterminer le montant du préjudice qui en est résulté en se référant à la rencontre ayant opposé l'Olympique de Marseille et l'Association de la Jeunesse Auxerroise le 17 août 2008, laquelle est représentative, en termes de nombre de spectateurs, des matchs de l'Olympique de Marseille qui se sont déroulés au stade Vélodrome au cours des mois d'août des années 2006 à 2010. La perte de recettes qui en résulte peut être évaluée sur cette base à 308 780 euros hors taxes au titre de la vente de places aux spectateurs, somme à laquelle il convient d'ajouter un manque à gagner pouvant être estimé à 21 111 euros hors taxes au titre des prestations de restauration qui leur sont délivrées sur place et dont la société Olympique de Marseille bénéfice par convention à hauteur de 20 % et à 9 930 euros hors taxes au titre des pertes sur les ventes de places " VIP ". En revanche, les justificatifs produits par cette société sont insuffisants pour établir d'éventuelles pertes liées à l'exploitation de la boutique vendant sur place des produits à l'effigie du club.<br/>
<br/>
              13. En deuxième lieu, il résulte de l'instruction qu'en raison de la délocalisation à Montpellier de la rencontre sportive qui s'est déroulée le 16 août 2009, la société Olympique de Marseille a exposé des frais de déplacement et d'hébergement pour ses salariés et les stadiers auxquels elle a recours. Elle justifie de ces frais à concurrence de la somme de 2 819 euros hors taxes, les autres dépenses de déplacement et d'hébergement dont la société demande l'indemnisation à ce titre n'étant assorties d'aucun justificatif ou concernant des personnels non répertoriés dans la liste des salariés du club versée au dossier par la société Olympique de Marseille et ne pouvant, par suite, être retenues. Il y a également lieu de l'indemniser des dépenses qu'elle a exposées pour une somme de 87 509 euros hors taxes aux fins d'assurer le transport de supporters à Montpellier.<br/>
<br/>
              14. En troisième lieu, la société Olympique de Marseille expose avoir engagé une somme totale de 56 670 euros hors taxes pour l'achat d'encarts publicitaires destinés à informer le public de la délocalisation du match, pour le transport et l'entreposage au stade de la Mosson de matériels et de panneaux d'affichage publicitaires et pour l'achat de banderoles. Il résulte de l'instruction que ces dépenses ont directement été supportées pour l'organisation de cette rencontre sportive et doivent ainsi donner lieu à indemnisation.<br/>
<br/>
              15. En quatrième lieu, il résulte de l'instruction qu'en raison de l'indisponibilité du stade Vélodrome le 16 août 2009, la société Olympique de Marseille a été contrainte de prendre à sa charge les frais inhérents à la location du stade de la Mosson à Montpellier pour une somme de 29 290 euros hors taxes. Elle est par suite fondée à demander à en être indemnisée.<br/>
<br/>
              16. En cinquième lieu, la société Olympique de Marseille justifie avoir exposé des dépenses liées au recrutement de personnels intérimaires à Montpellier, pour la tenue du guichet ou des opérations de manutention, pour un montant dépassant de 18 860 euros hors taxes les charges habituellement supportées au titre de ces mêmes postes de dépenses lors des rencontres comparables organisées dans l'enceinte du stade Vélodrome. Cette somme doit par suite être prise en compte pour le calcul du montant du préjudice indemnisable.<br/>
<br/>
              17. En sixième lieu, la société Olympique de Marseille n'établit pas que la délivrance gratuite aux abonnés de places pour un match contre le FC Copenhague et la parution gratuite, dans les publications du club, de publicité pour des partenaires qui ont pu être affectés par la délocalisation du match procèderait d'obligations contractuelles à l'égard de ces personnes et non de choix commerciaux de sa part. Il n'y a, par suite, pas lieu d'en tenir compte pour déterminer le montant du préjudice ouvrant droit à indemnisation.<br/>
<br/>
              18. En septième lieu, l'indemnisation à accorder à la société Olympique de Marseille doit être réduite à hauteur des dépenses dont elle a été dispensée pour la location du stade Vélodrome du fait de cette délocalisation. Celles-ci doivent être évaluées, sur la base de la même hypothèse de fréquentation que celle retenue au point 12, à 73 082 euros hors taxes.<br/>
<br/>
              19. Il résulte de tout ce qui précède que la société Olympique de Marseille est fondée à soutenir que la commune de Marseille doit être condamnée à lui verser, à titre d'indemnité, une somme qu'il y a lieu de fixer à 461 887 euros. La société Olympique de Marseille a droit, ainsi qu'elle le demande, aux intérêts au taux légal afférents à cette indemnité à compter du 20 juin 2013, date à laquelle elle a saisi le tribunal administratif de Marseille. La capitalisation des intérêts ayant été demandée par cette société à la même date, il y a lieu de faire droit à cette demande à compter du 20 juin 2014, date à laquelle était due pour la première fois une année d'intérêts ainsi qu'à chaque échéance annuelle à compter de cette date.<br/>
<br/>
              Sur l'appel en garantie de la société Live Nation France :<br/>
<br/>
              20. En premier lieu, la commune de Marseille n'ayant pas expressément abandonné, dans l'instance ayant donné lieu à l'arrêt attaqué, rendu par la cour administrative d'appel sur renvoi après annulation d'un premier arrêt par une décision du Conseil d'Etat du 24 avril 2019, les conclusions qu'elle a formées devant le tribunal administratif tendant à ce que la société Live Nation France soit condamnée à la garantir des indemnisations mises à sa charge, le Conseil d'Etat, réglant l'affaire au fond après annulation de l'arrêt de la cour administrative d'appel, demeure, contrairement à ce que soutient cette société, saisi de ces conclusions, la circonstance que celle-ci n'ait pas été partie à l'instance ayant donné lieu à la décision précitée du Conseil d'Etat étant dépourvue d'incidence à cet égard.<br/>
<br/>
              21. En deuxième lieu, la circonstance que la société Olympique de Marseille n'a formé aucune demande tendant à la condamnation de la société Live Nation France à l'indemniser du préjudice qu'elle a subi à raison de l'impossibilité d'organiser au stade Vélodrome la rencontre programmée le 16 août 2019 est sans incidence sur la recevabilité de l'appel en garantie formé par la commune de Marseille à l'égard de cette société. Par ailleurs, ayant été formé le 18 octobre 2016, soit moins de cinq ans après la requête par laquelle la société Olympique de Marseille a sollicité la mise à la charge de la commune de Marseille de l'indemnisation contre laquelle cette commune demandait à être garantie, la société Live Nation France n'est pas fondée à soutenir que cet appel en garantie serait atteint par la prescription prévue par l'article 2224 du code civil.<br/>
<br/>
              22. En troisième lieu, aux termes des stipulations de l'article 6 de la convention de mise à disposition du stade Vélodrome pour le concert devant se tenir le 19 juillet 2009, " La société Live Nation France organisatrice de la manifestation est responsable des dommages de toute nature pouvant survenir du fait ou à l'occasion de l'exécution de la présente convention ". Par suite, nonobstant les manquements qui auraient pu être commis par les sous-traitants auxquels a recouru la société Live Nation France pour le montage du dispositif scénique prévu pour la tenue du concert, il y a lieu d'accueillir les conclusions de la commune de Marseille tendant à ce que cette société soit appelée en garantie. En l'absence, au vu de l'instruction, de négligences de la commune de Marseille de nature à atténuer les responsabilités incombant à la société Live Nation France en application des stipulations précitées, cette société garantira la commune du montant total des sommes mises à sa charge par la présente décision.<br/>
<br/>
              23. Il résulte de tout ce qui précède que la société Olympique de Marseille est fondée à soutenir que c'est à tort que le tribunal administratif de Marseille a rejeté sa requête et à demander la condamnation de la commune de Marseille à lui verser, en réparation du préjudice qu'elle a subi, une somme de 461 887 euros assortis des intérêts au taux légal à compter du 20 juin 2013, ces intérêts étant capitalisés à compter du 20 juin 2014 ainsi qu'à chaque échéance annuelle à compter de cette date, la commune étant elle-même fondée à demander à être garantie par la société Live Nation France de la totalité des sommes mises à sa charge.<br/>
<br/>
              Sur les frais de l'instance :<br/>
<br/>
              24. Il y a lieu dans les circonstances de l'espèce de mettre à la charge de la commune de Marseille la somme de 6 000 euros, à verser à la société Olympique de Marseille, pour l'ensemble de la procédure, au titre de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce que des sommes soient mise à ce titre à la charge de cette société, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 6 mars 2020 de la cour administrative d'appel de Marseille et le jugement du 23 mai 2017 du tribunal administratif de Marseille sont annulés.<br/>
Article 2 : La commune de Marseille est condamnée à verser à la société Olympique de Marseille une indemnité de 461 887 euros qui portera intérêts au taux légal à compter du 20 juin 2013. Les intérêts échus à la date du 20 juin 2014, puis à chaque échéance annuelle à compter de cette date seront capitalisés à chacune de ces dates pour produire eux-mêmes intérêts.<br/>
Article 3 : La société Live Nation France garantira la commune de Marseille du montant total des condamnations prononcées à son encontre.<br/>
Article 4 : Le surplus des conclusions de la requête de la société Olympique de Marseille est rejeté.<br/>
Article 5 : La commune de Marseille versera à la société Olympique de Marseille une somme de 6 000 euros au titre de l'article L. 761-1 du code justice administrative.<br/>
Article 6 : Les conclusions présentées par la commune de Marseille et la société Live Nation France au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 7 : La présente décision sera notifiée à la société sportive professionnelle Olympique de Marseille, à la commune de Marseille et à la société par actions simplifiée Live Nation France.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">24-01-02-01-01-03 DOMAINE. - DOMAINE PUBLIC. - RÉGIME. - OCCUPATION. - UTILISATIONS PRIVATIVES DU DOMAINE. - DROITS À INDEMNISATION DE L'OCCUPANT. - OCCUPANT TITULAIRE D'UN CONTRAT DE MISE À DISPOSITION - FORCE MAJEURE EXONÉRANT LA PERSONNE PUBLIQUE CONCÉDANTE DE SA RESPONSABILITÉ CONTRACTUELLE - EXCLUSION, FAUTE D'EXTÉRIORITÉ [RJ1] - CAS D'UN MANQUEMENT D'UN AUTRE COCONTRACTANT DE LA PERSONNE PUBLIQUE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-03-03-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. - EXÉCUTION TECHNIQUE DU CONTRAT. - ALÉAS DU CONTRAT. - FORCE MAJEURE. - EXCLUSION, FAUTE D'EXTÉRIORITÉ [RJ1] - CAS D'INEXÉCUTION PAR UNE PERSONNE PUBLIQUE D'UN CONTRAT DU FAIT DES MANQUEMENTS D'UN AUTRE DE SES COCONTRACTANTS.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-04-02-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. - RÉPARATION. - CAUSES EXONÉRATOIRES DE RESPONSABILITÉ. - FORCE MAJEURE. - EXCLUSION, FAUTE D'EXTÉRIORITÉ [RJ1] - CAS D'INEXÉCUTION PAR UNE PERSONNE PUBLIQUE D'UN CONTRAT DU FAIT DES MANQUEMENTS D'UN AUTRE DE SES COCONTRACTANTS.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">60-05-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. - RECOURS OUVERTS AUX DÉBITEURS DE L'INDEMNITÉ, AUX ASSUREURS DE LA VICTIME ET AUX CAISSES DE SÉCURITÉ SOCIALE. - ACTION EN GARANTIE. - APPEL EN GARANTIE CONTRACTUELLE DIRIGÉ PAR UNE PERSONNE PUBLIQUE CONTRE UN OCCUPANT DU DOMAINE PUBLIC - 1) PRESCRIPTION QUINQUENNALE (ART. 2224 DU CODE CIVIL) - POINT DE DÉPART [RJ2] - 2) BIEN-FONDÉ [RJ3] - 3) EVALUATION.
</SCT>
<ANA ID="9A"> 24-01-02-01-01-03 Stade municipal mis à la disposition, dans la durée, d'un club sportif en vue de l'organisation de rencontres de football programmées et, exceptionnellement, d'un autre cocontractant en vue de l'organisation d'un concert. Accident mortel ayant été causé, au cours des opérations de montage de la scène de ce spectacle, par l'effondrement d'une structure métallique. Stade rendu par suite indisponible pour accueillir, un mois plus tard, une rencontre sportive programmée.......L'indisponibilité du stade, bien que résultant de fautes commises par le cocontractant de la commune et les sous-traitants de celui-ci dans le montage de la structure scénique, n'aurait pu survenir sans la décision initiale de la commune de mettre le stade à disposition de ce cocontractant pour l'organisation d'un concert.......Par suite, l'effondrement de la structure scénique et l'accident mortel qui s'en est suivi ne résultent pas de faits extérieurs à cette commune et, dès lors, ne constituent pas un cas de force majeure de nature à l'exonérer de toute responsabilité contractuelle vis-à-vis du club sportif.</ANA>
<ANA ID="9B"> 39-03-03-01 Stade municipal mis à la disposition, dans la durée, d'un club sportif en vue de l'organisation de rencontres de football programmées et, exceptionnellement, d'un autre cocontractant en vue de l'organisation d'un concert. Accident mortel ayant été causé, au cours des opérations de montage de la scène de ce spectacle, par l'effondrement d'une structure métallique. Stade rendu par suite indisponible pour accueillir, un mois plus tard, une rencontre sportive programmée.......L'indisponibilité du stade, bien que résultant de fautes commises par le cocontractant de la commune et les sous-traitants de celui-ci dans le montage de la structure scénique, n'aurait pu survenir sans la décision initiale de la commune de mettre le stade à disposition de ce cocontractant pour l'organisation d'un concert.......Par suite, l'effondrement de la structure scénique et l'accident mortel qui s'en est suivi ne résultent pas de faits extérieurs à cette commune et, dès lors, ne constituent pas un cas de force majeure de nature à l'exonérer de toute responsabilité contractuelle vis-à-vis du club sportif.</ANA>
<ANA ID="9C"> 60-04-02-03 Stade municipal mis à la disposition, dans la durée, d'un club sportif en vue de l'organisation de rencontres de football programmées et, exceptionnellement, d'un autre cocontractant en vue de l'organisation d'un concert. Accident mortel ayant été causé, au cours des opérations de montage de la scène de ce spectacle, par l'effondrement d'une structure métallique. Stade rendu par suite indisponible pour accueillir, un mois plus tard, une rencontre sportive programmée.......L'indisponibilité du stade, bien que résultant de fautes commises par le cocontractant de la commune et les sous-traitants de celui-ci dans le montage de la structure scénique, n'aurait pu survenir sans la décision initiale de la commune de mettre le stade à disposition de ce cocontractant pour l'organisation d'un concert.......Par suite, l'effondrement de la structure scénique et l'accident mortel qui s'en est suivi ne résultent pas de faits extérieurs à cette commune et, dès lors, ne constituent pas un cas de force majeure de nature à l'exonérer de toute responsabilité contractuelle vis-à-vis du club sportif.</ANA>
<ANA ID="9D"> 60-05-01 Stade municipal mis à la disposition, dans la durée, d'un club sportif en vue de l'organisation de rencontres de football programmées et, exceptionnellement, d'un autre cocontractant en vue de l'organisation d'un concert. Accident mortel ayant été causé, au cours des opérations de montage de la scène de ce spectacle, par l'effondrement d'une structure métallique. Stade rendu par suite indisponible pour accueillir, un mois plus tard, une rencontre sportive programmée. Appel contractuel en garantie de la commune, condamnée à indemniser le club sportif, contre l'organisateur du spectacle.......1) Un appel en garantie formé par une personne publique moins de cinq ans après la requête par laquelle la victime a sollicité la mise à sa charge de l'indemnisation contre laquelle cette personne publique demande à être garantie n'est pas atteint par la prescription prévue par l'article 2224 du code civil.......2) Article 6 de la convention de mise à disposition du stade à l'organisateur de spectacle prévoyant la responsabilité de celui-ci pour les dommages de toute nature pouvant survenir du fait ou à l'occasion de l'exécution de cette convention. Par suite, nonobstant les manquements qui auraient pu être commis par les sous-traitants auxquels ce cocontractant a recouru pour le montage du dispositif scénique prévu pour la tenue du concert, il y a lieu d'accueillir les conclusions de la commune tendant à ce que ce cocontractant soit appelé en garantie.......3) En l'absence, au vu de l'instruction, de négligences de la commune de nature à atténuer les responsabilités incombant à son cocontractant en application de ces stipulations, ce cocontractant doit garantir la commune du montant total des sommes mises à sa charge.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur les conditions d'extériorité et d'irrésistibilité, CE, 29 janvier 1909, Compagnie des messageries maritimes de l'Etat, n° 17614, p. 111....[RJ2] Rappr., s'agissant de la prescription décennale, CE, 10 février 2017, Société Campenon Bernard Côte d'Azur et société Fayat Bâtiment, n° 391722, T. pp. 805-840....[RJ3] Comp., s'agissant de l'absence de garantie contractuelle par les constructeurs après la fin des rapports contractuels nés d'un marché de travaux, CE, Section, 4 juillet 1980, SA Forrer et Cie, n° 03433, p. 307 ; CE, Section, 15 juillet 2004, Syndicat intercommunal d'alimentation en eau des communes de la Seyne et de la région Est de Toulon, n° 235053, p. 345 ; CE, Section, 6 avril 2007, Centre hospitalier général de Boulogne-sur-Mer, n°s 264490 264491, p. 163.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
