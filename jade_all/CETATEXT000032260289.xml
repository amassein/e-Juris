<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032260289</ID>
<ANCIEN_ID>JG_L_2016_03_000000374909</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/26/02/CETATEXT000032260289.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème SSR, 16/03/2016, 374909, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-03-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374909</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE CHAISEMARTIN, COURJON</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2016:374909.20160316</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 22 juillet 2015, le Conseil d'Etat, statuant au contentieux, a prononcé l'admission du pourvoi de la SAS Cannes Evolution contre l'arrêt n°s 11PA04091, 11PA04721 de la cour administrative d'appel de Paris en tant seulement que cet arrêt a statué sur les cotisations supplémentaires d'impôt sur les sociétés et de contributions additionnelles et sur les pénalités correspondantes relatives à la réintégration dans sa base d'imposition de la somme prélevée sur la réserve spéciale des plus-values à long terme.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 65-566 du 12 juillet 1965 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Chaisemartin, Courjon, avocat de la SAS Cannes Evolution ;<br/>
<br/>
<br/>
<br/>Sur les motifs de l'arrêt attaqué relatifs à la régularité de la procédure d'imposition :<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le rehaussement en litige a été notifié par une proposition de rectification en date du 15 décembre 2005 ; que cette proposition a été précédée d'une vérification de comptabilité qui s'est achevée le 22 novembre 2005, date à laquelle s'est tenue une réunion de synthèse en présence des mandataires de la société ; que, par ailleurs, l'administration a, postérieurement à l'achèvement de la vérification de comptabilité, procédé, le 25 novembre 2005, à une visite sur le fondement de l'article L. 16 B du livre des procédures fiscales, lors de laquelle diverses pièces ont été saisies ;<br/>
<br/>
              2. Considérant que l'indépendance de la procédure de visite et de saisie instituée par l'article L. 16 B du livre des procédures fiscales par rapport à la procédure de vérification de comptabilité concernant le même contribuable, qui constituent deux étapes distinctes de la procédure d'imposition, ne saurait avoir pour effet de réduire dans chacune d'entre elles les garanties accordées au contribuable ; que l'administration, lorsqu'elle saisit des pièces et documents, notamment comptables, dans le cadre d'une opération de visite domiciliaire, est tenue de restituer ces documents au contribuable en principe avant l'engagement de la vérification de comptabilité, en tout état de cause dans les six mois de la visite, et, avant ce terme, dans un délai permettant au contribuable d'avoir, sur place, un débat oral et contradictoire avec le vérificateur, eu égard à la teneur de ces documents, à leur portée et à l'usage que l'administration pourrait en faire à l'issue de la vérification de comptabilité ; que, s'il est constant qu'aucun débat oral et contradictoire n'a pu avoir lieu sur les documents saisis le 25 novembre 2005, puisque cette date est postérieure à celle de l'achèvement de la vérification de comptabilité,  la cour a pu, sans commettre d'erreur de droit, rejeter le moyen soulevé devant elle par la SAS Cannes Evolution tiré de ce que la SAS Financière Giraudoux Kléber, aux droits de laquelle elle vient, aurait été privée d'un débat oral et contradictoire sur ces pièces, en se fondant sur la circonstance qu'elle ne contestait pas que le redressement en litige n'était pas fondé sur des éléments recueillis au cours de cette visite ;<br/>
<br/>
              Sur les motifs de l'arrêt attaqué relatifs au bien-fondé des impositions :<br/>
<br/>
              3. Considérant, d'une part, qu'aux termes de l'article L. 64 du livre des procédures fiscales, dans sa rédaction alors applicable : " Ne peuvent être opposés à l'administration des impôts les actes qui dissimulent la portée véritable d'un contrat ou d'une convention à l'aide de clauses : / (...) b) (...) qui déguisent soit une réalisation, soit un transfert de bénéfices ou de revenus ; (...) L'administration est en droit de restituer son véritable caractère à l'opération litigieuse. En cas de désaccord sur les redressements notifiés sur le fondement du présent article, le litige est soumis, à la demande du contribuable, à l'avis du comité consultatif pour la répression des abus de droit. L'administration peut également soumettre le litige à l'avis du comité dont les avis rendus feront l'objet d'un rapport annuel. Si l'administration ne s'est pas conformée à l'avis du comité, elle doit apporter la preuve du bien-fondé du redressement " ; qu'il résulte de ces dispositions que, lorsque l'administration use de la faculté qu'elles lui confèrent dans des conditions telles que la charge de la preuve lui incombe, elle est fondée à écarter comme ne lui étant pas opposables certains actes passés par le contribuable, dès lors que ces actes ont un caractère fictif, ou que, recherchant le bénéfice d'une application littérale des textes à l'encontre des objectifs poursuivis par leurs auteurs, ils n'ont pu être inspirés par aucun autre motif que celui d'éluder ou d'atténuer les charges fiscales que l'intéressé, s'il n'avait pas passé ces actes, aurait normalement supportées, eu égard à sa situation ou à ses activités réelles ;<br/>
<br/>
              4. Considérant, d'autre part, qu'en vertu de l'article 209 quater du code général des impôts, dans sa rédaction alors applicable : " 1. Les plus-values soumises à l'impôt au taux réduit prévu au a du I de l'article 219 diminuées du montant de cet impôt, sont portées à une réserve spéciale. / 2. Les sommes prélevées sur cette réserve sont rapportées aux résultats de l'exercice en cours lors de ce prélèvement, sous déduction de l'impôt perçu lors de la réalisation des plus-values correspondantes. / 3. La disposition du 2 n'est pas applicable : / (...) b. En cas d'incorporation au capital (...) " ;<br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que figurait au bilan d'ouverture du premier exercice soumis à vérification de la SAS Financière Giraudoux Kléber une somme de 25 428 814 euros inscrite à la réserve spéciale prévue par les dispositions précitées de l'article 209 quater du code général des impôts, à laquelle étaient portées des plus-values ayant supporté l'impôt au taux réduit de 19 % ; que, réunis en assemblée générale le 24 juin 2002, les associés de cette société ont décidé de prélever sur cette réserve spéciale, en vue de son incorporation au capital, une somme de 25 428 784 euros ; qu'en application des dispositions précitées du 2 et du b du 3 du même article 209 quater, les sommes ainsi prélevées sur la réserve pour être incorporées au capital n'ont pas été rapportées par la société à son résultat imposable ; que, postérieurement à l'entrée, le 2 décembre 2002, des sociétés Tampico et Bunbury au capital de la SAS Financière Giraudoux Kléber, l'assemblée générale de cette dernière a, le 23 décembre 2002, décidé de procéder à une réduction de capital d'un montant de 22 356 000 euros et de procéder à la répartition de la somme correspondante entre ses associés ;<br/>
<br/>
              6. Considérant que la cour a retenu, d'une part, qu'il résultait des travaux préparatoires de la loi du 12 juillet 1965 modifiant l'imposition des entreprises et des revenus de capitaux mobiliers que le législateur a eu pour objectif, en instituant le régime spécial d'imposition des plus-values à long terme, de faire bénéficier du taux réduit d'imposition de ces plus-values celles qui sont réinvesties dans l'entreprise et qu'ainsi, l'opération par laquelle des sommes figurant à la réserve spéciale des plus-values à long terme étaient, dans un premier temps, incorporées au capital social puis, au terme d'un bref délai, réparties entre les associés à la suite d'une réduction du capital, allait à l'encontre de cet objectif ; qu'elle a retenu, d'autre part, que le ministre rapportait la preuve que l'incorporation au capital de la somme prélevée sur la réserve spéciale des plus-values à long terme de la SAS Financière Giraudoux Kléber, suivie de la réduction de capital opérée moins de six mois après et du désinvestissement des sommes correspondantes, était constitutive d'un montage artificiel qui n'avait pas d'autre motif que d'éluder ou d'atténuer les charges fiscales que la société aurait supportées si elle n'avait pas effectué ces opérations ; que la cour en a déduit que l'administration était fondée à regarder comme ne lui étant pas opposable la décision par laquelle la SAS Financière Giraudoux Kléber avait décidé d'incorporer à son capital social la somme de 25 428 784 euros et, par suite, à rapporter à son résultat imposable la somme de 22 356 000 euros comme ayant été prélevée sur la réserve spéciale en application des dispositions précitées du 2 de l'article 209 quater du code général des impôts, quand bien même elles n'y figureraient plus à la date à laquelle elles ont été réparties entre les associés ;<br/>
<br/>
              7. Considérant, en premier lieu, que c'est au niveau de la société soumise à l'impôt et non de ses associés qu'il y a lieu de déterminer si l'objectif qui a été poursuivi était, ou non, exclusivement d'éluder les charges fiscales ou de les atténuer ; qu'il suit de là que la cour n'a pas commis d'erreur de droit en regardant comme indifférente, pour l'application des dispositions précitées de l'article L. 64 du livre des procédures fiscales, la circonstance que les associés de la SAS Financière Giraudoux Kléber ayant décidé l'incorporation au capital de sommes prélevées sur la réserve spéciale des plus-values à long terme ne soient pas, compte tenu de la cession de leurs parts sociales, les mêmes que ceux qui ont ultérieurement décidé de la réduction de capital et de la répartition des sommes correspondantes ;<br/>
<br/>
              8. Considérant, en deuxième lieu, que lorsque l'administration entend écarter comme ne lui étant pas opposables certains actes passés par le contribuable, après avoir établi que ces actes ont eu un caractère fictif ou tendaient à obtenir le bénéfice d'une application littérale des textes à l'encontre des objectifs poursuivis par leurs auteurs, dans le seul but d'éluder ou d'atténuer les charges fiscales que l'intéressé, s'il ne les avait pas passés, aurait normalement supportées eu égard à sa situation et à ses activités réelles, elle doit, pour établir l'impôt qui aurait été dû en leur absence, se placer non pas à la date de l'acte qu'elle a écarté, mais à celle de l'opération dont elle entend tirer les conséquences et qui constitue le fait générateur de l'imposition ; qu'ainsi, la cour, après avoir constaté, d'une part, que la SAS Financière Giraudoux Kléber clôturait ses comptes au 30 juin, d'autre part, que l'administration avait entendu écarter comme ne lui étant pas opposable la décision du 24 juin 2002 par laquelle l'assemblée générale de cette même société avait incorporé au capital social des sommes figurant sur la réserve spéciale des plus-values à long terme et, enfin, que la répartition des mêmes sommes entre les associés avait été décidée le 22 décembre 2002, a pu, sans commettre d'erreur de droit, déduire de ce que le fait générateur de l'imposition en litige était intervenu à cette dernière date que cette imposition devait être établie au titre de l'exercice clôturé le 30 juin 2003 ;<br/>
<br/>
              9. Considérant, enfin, que le moyen tiré de ce qu'il aurait été fait application, en l'espèce, d'une interprétation de l'article L. 64 du livre des procédures fiscales établie par le Conseil d'Etat postérieurement aux années d'imposition en litige et, qu'ainsi, la pénalité dont est assortie la mise en oeuvre de cet article aurait été infligée en méconnaissance du principe constitutionnel de non-rétroactivité des délits et des peines est nouveau en cassation ; qu'il est, dès lors, inopérant ;<br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède que les conclusions admises du pourvoi de la SAS Cannes Evolution doivent être rejetées, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Les conclusions admises du pourvoi de la SAS Cannes Evolution sont rejetées.<br/>
Article 2 : La présente décision sera notifiée à la SAS Cannes Evolution et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
