<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028589059</ID>
<ANCIEN_ID>JG_L_2014_02_000000360382</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/58/90/CETATEXT000028589059.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 10/02/2014, 360382, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-02-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360382</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:360382.20140210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi, enregistré le 20 juin 2012 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre des affaires sociales et de la santé ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° AD3305 du 20 mars 2012 par laquelle la chambre de discipline du Conseil national de l'ordre des pharmaciens a rejeté l'appel formé par le directeur général de l'Agence régionale de santé de Bretagne tendant à l'annulation de la décision du 12 avril 2011 de la chambre de discipline du conseil régional de l'ordre des pharmaciens de Basse-Normandie rejetant la plainte qu'il avait déposée à l'encontre de M. et MmeA... ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à l'appel du directeur général de l'Agence régionale de santé de Bretagne ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 27 janvier 2013, présentée pour le Conseil national de l'ordre des pharmaciens ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, avocat du Conseil national de l'ordre des pharmaciens ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 5143-5 du code de la santé publique : " Est subordonnée à la rédaction par un vétérinaire d'une ordonnance, qui est obligatoirement remise à l'utilisateur, la délivrance au détail, à titre gratuit ou onéreux, des médicaments suivants : 1° Les médicaments vétérinaires contenant des substances prévues à l'article L. 5144-1, à l'exception des substances vénéneuses à doses ou concentrations trop faibles pour justifier de la soumission au régime de ces substances (...) " ; qu'aux termes de l'article R. 5141-112 du même code : " Lors de la délivrance des médicaments vétérinaires prescrits conformément aux dispositions de l'article R. 5141-111, le pharmacien ou le vétérinaire transcrit aussitôt à l'encre, sans blanc ni surcharge, cette délivrance sur un registre ou l'enregistre par tout système approprié ne permettant aucune modification des données qu'il contient après validation de leur enregistrement. Les systèmes d'enregistrement permettent une édition immédiate à la demande de toute autorité de contrôle des mentions prévues au présent article (...) " ;<br/>
<br/>
              2. Considérant qu'il résulte des pièces du dossier soumis aux juges du fond que M. et MmeA..., pharmaciens à Fougères (Ille-et-Vilaine), se sont vu reprocher, à la suite d'une enquête de la direction régionale de l'action sanitaire et sociale de Bretagne, d'avoir méconnu les dispositions précitées en délivrant des médicaments vétérinaires soumis à la réglementation des substances vénéneuses pour des animaux destinés à la consommation humaine en l'absence d'ordonnance établie par un vétérinaire ou au vu d'ordonnances qui n'étaient plus valables, et en ne procédant pas régulièrement à l'enregistrement des délivrances ; que, pour estimer que ces manquements ne justifiaient pas une sanction disciplinaire, la chambre de discipline du Conseil national de l'ordre des pharmaciens, confirmant la décision rendue en première instance par la chambre de discipline du conseil régional de Bretagne, s'est fondée, d'une part, sur la circonstance que les intéressés s'étaient heurtés aux pratiques de nombreux vétérinaires consistant à refuser de remettre une ordonnance à l'utilisateur ou à y porter la mention " non renouvelable " afin d'empêcher la délivrance des médicaments par des pharmaciens et, d'autre part, sur ce que les irrégularités constatées portaient sur de faibles quantités de médicaments et que les délivrances ne présentaient pas de caractère dangereux dans la mesure où la traçabilité des médicaments était assurée ; que si la juridiction disciplinaire peut, même si elle retient l'existence d'une faute, tenir compte de certaines circonstances ou certains faits pour décider de ne pas infliger de sanction, les juges du fond ont en l'espèce, eu égard à l'objet des dispositions méconnues, inexactement qualifié les faits en estimant que ces éléments justifiaient de dispenser M. et Mme A...de toute sanction  ;<br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède que la ministre des affaires sociales et de la santé est fondée à demander l'annulation de la décision de la chambre de discipline du Conseil national de l'ordre des pharmaciens du 20 mars 2012 ;<br/>
<br/>
              4. Considérant que le Conseil national de l'ordre des pharmaciens n'étant pas partie à la présente instance, ses conclusions tendant à ce que les frais qu'il a exposés pour produire des observations soit mis à la charge de l'Etat en application des dispositions de l'article L. 761-1 du code de justice administrative doivent être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision de la chambre de discipline du Conseil national de l'ordre des pharmaciens du 20 mars 2012 est annulée.<br/>
<br/>
Article 2 : Les conclusions du Conseil national de l'ordre des pharmaciens tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 3 : L'affaire est renvoyée à la chambre de discipline du Conseil national de l'ordre des pharmaciens.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la ministre des affaires sociales et de la santé, à Mme B...A...et à M. C...A....<br/>
Copie en sera adressée au Conseil national de l'ordre des pharmaciens.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-08-02-02-01-02 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. QUALIFICATION JURIDIQUE DES FAITS. - CHOIX DE LA JURIDICTION DISCIPLINAIRE DE PRONONCER OU NON UNE SANCTION EN PRÉSENCE D'UNE FAUTE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">55-04-01-03 PROFESSIONS, CHARGES ET OFFICES. DISCIPLINE PROFESSIONNELLE. PROCÉDURE DEVANT LES JURIDICTIONS ORDINALES. POUVOIRS DU JUGE DISCIPLINAIRE. - FACULTÉ POUR LA JURIDICTION DISCIPLINAIRE DE DISPENSER DE SANCTION LE PROFESSIONNEL MALGRÉ L'EXISTENCE D'UNE FAUTE 1) EXISTENCE - 2) CONTRÔLE DU JUGE DE CASSATION SUR L'EXERCICE DE CETTE FACULTÉ - QUALIFICATION JURIDIQUE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">59-02-02 RÉPRESSION. DOMAINE DE LA RÉPRESSION ADMINISTRATIVE RÉGIME DE LA SANCTION ADMINISTRATIVE. - FACULTÉ POUR LA JURIDICTION DISCIPLINAIRE DE DISPENSER DE SANCTION UN PROFESSIONNEL MALGRÉ L'EXISTENCE D'UNE FAUTE 1) EXISTENCE - 2) CONTRÔLE DU JUGE DE CASSATION SUR L'EXERCICE DE CETTE FACULTÉ - QUALIFICATION JURIDIQUE.
</SCT>
<ANA ID="9A"> 54-08-02-02-01-02 Le juge de cassation exerce un contrôle de qualification juridique sur l'appréciation portée par la juridiction disciplinaire sur le point de savoir si, alors même qu'est retenue l'existence d'une faute, il est justifié de dispenser de sanction le professionnel poursuivi compte tenu des circonstances et faits de l'espèce.</ANA>
<ANA ID="9B"> 55-04-01-03 1) La juridiction disciplinaire peut, même si elle retient l'existence d'une faute, tenir compte de certaines circonstances ou certains faits pour décider de ne pas infliger de sanction au professionnel poursuivi.,,,2) Le juge de cassation exerce un contrôle de qualification juridique sur l'appréciation portée par la juridiction disciplinaire sur le point de savoir si, alors même qu'est retenue l'existence d'une faute, il est justifié de dispenser de sanction le professionnel poursuivi compte tenu des circonstances et faits de l'espèce.</ANA>
<ANA ID="9C"> 59-02-02 1) La juridiction disciplinaire peut, même si elle retient l'existence d'une faute, tenir compte de certaines circonstances ou certains faits pour décider de ne pas infliger de sanction au professionnel poursuivi.,,,2) Le juge de cassation exerce un contrôle de qualification juridique sur l'appréciation portée par la juridiction disciplinaire sur le point de savoir si, alors même qu'est retenue l'existence d'une faute, il est justifié de dispenser de sanction le professionnel poursuivi compte tenu des circonstances et faits de l'espèce.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
