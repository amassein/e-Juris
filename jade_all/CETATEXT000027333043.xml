<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027333043</ID>
<ANCIEN_ID>JG_L_2013_04_000000365340</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/33/30/CETATEXT000027333043.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 19/04/2013, 365340, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-04-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365340</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE ; SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Nuttens</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:365340.20130419</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 21 janvier 2013 au secrétariat du contentieux du Conseil d'Etat, présenté pour la ville de Marseille, représentée par son maire ; la ville de Marseille demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1208200 du 4 janvier 2013 du juge des référés du tribunal administratif de Marseille en tant que, statuant en application de l'article L. 551-1 du code de justice administrative, il a fait droit à la demande de la société Purfer tendant à l'annulation de la procédure de passation du marché portant sur la destruction de véhicules hors d'usage ; <br/>
<br/>
              2°) statuant en référé, de rejeter la demande présentée sur ce point par la société Purfer ; <br/>
<br/>
              3°) de mettre à la charge de la société Purfer le versement d'une somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Nuttens, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Coutard, Munier-Apaire, avocat de la ville de Marseille, et de la SCP Célice, Blancpain, Soltner, avocat de la société Purfer,<br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Coutard, Munier-Apaire, avocat de la ville de Marseille, et à la SCP Célice, Blancpain, Soltner, avocat de la société Purfer ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation, ou la délégation d'un service public (...) " ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Marseille que, par un avis d'appel public à la concurrence publié le 10 juillet 2012, la ville de Marseille a lancé une procédure d'appel d'offres ouvert en vue de l'attribution d'un marché de destruction de véhicules hors d'usage ; que le règlement de la consultation précisait que les offres seraient appréciées au regard des critères du prix, de la valeur technique et des délais d'exécution, respectivement pondérés à hauteur de 50 %, 40 % et 10 % ; que, par l'article 1er de l'ordonnance attaquée du 4 janvier 2013, le juge des référés, saisi par la société Purfer sur le fondement de l'article L. 551-1 du code de justice administrative, a annulé la procédure de passation du marché ;<br/>
<br/>
              Sur la régularité de l'ordonnance attaquée :<br/>
<br/>
              3. Considérant, en premier lieu, que l'article R. 742-2 du code de justice administrative, applicable en matière de référé, en vertu duquel " Les ordonnances mentionnent le nom des parties, l'analyse des conclusions ainsi que les visas des dispositions législatives ou réglementaires dont elles font application ", n'a pas pour effet d'imposer au juge des référés d'analyser ou de viser, dans sa décision, les moyens développés par les parties à l'appui de leurs conclusions, auxquels le juge devra toutefois répondre, en tant que de besoin, au titre de la motivation de son ordonnance ; qu'il suit de là que la ville de Marseille ne peut utilement soutenir qu'en ne visant pas avec suffisamment de précision les moyens de son mémoire en défense le juge des référés du tribunal administratif de Marseille aurait méconnu l'article R. 742-2 du code de justice administrative ; <br/>
<br/>
              4. Considérant, en second lieu, que la minute de l'ordonnance attaquée comporte la signature du magistrat qui l'a rendue, conformément aux dispositions de l'article R. 742-3 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Sur le bien fondé de l'ordonnance attaquée :<br/>
<br/>
              5. Considérant que le juge des référés a constaté, par une appréciation souveraine, que la méthode de notation retenue, consistant en la détermination d'une note globale par la somme des notes attribuées, d'une part, à la proposition de prix payable par la ville à son cocontractant pour l'enlèvement des véhicules et, d'autre part, à la proposition de prix de reprise du véhicule payable, à l'inverse, par le cocontractant à la ville, ne pouvait être mise en oeuvre dans l'hypothèse d'une offre de prix pour l'enlèvement des véhicules d'un montant égal à zéro euro, dès lors qu'elle reposait sur le calcul du quotient entre le montant de l'offre la moins disante et le montant de l'offre du candidat, et que cette difficulté avait suscité une incertitude au moment du dépôt des offres, conduisant la ville à modifier la formule au moment de l'analyse des offres ; qu'en se prononçant ainsi sur la méthode de notation retenue - laquelle, au demeurant, compte tenu de l'examen indépendant des propositions relatives au prix d'enlèvement des véhicules payable par la ville et au prix de reprise de ceux-ci payable à la ville, ne permettait pas d'attribuer de manière certaine la meilleure note au candidat proposant l'offre la plus avantageuse pour la ville - le juge des référés ne s'est pas fondé, contrairement à ce que soutient la ville de Marseille, sur ce que les détails de la méthode de notation auraient dû être rendus publics ;<br/>
<br/>
              6. Considérant qu'après avoir estimé que les difficultés suscitées par la méthode de notation des offres avaient incité la société Purfer à transmettre deux bordereaux de prix unitaires, l'un d'entre eux ayant été écarté par les auteurs du rapport d'analyse et des candidatures des offres qui ont, par ailleurs, remplacé le prix égal à zéro euro par un prix égal à un euro pour pouvoir noter la société Guy Dauphin environnement, déclarée attributaire du marché, le juge des référés a pu en déduire, par une appréciation souveraine exempte de dénaturation et en motivant suffisamment son ordonnance, que l'incertitude résultant de cette méthode au moment du dépôt des offres, et son adaptation lors de l'analyse des offres, étaient de nature à porter atteinte, dès le lancement de la procédure d'appel public à la concurrence, au principe d'égalité entre les candidats ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que le pourvoi de la ville de Marseille doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la ville de Marseille, au titre des mêmes dispositions, le versement à la société Purfer d'une somme de 3 000 euros ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la ville de Marseille est rejeté. <br/>
Article 2 : La ville de Marseille versera à la société Purfer une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à la ville de Marseille, à la société Purfer et à la société Guy Dauphin Environnement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
