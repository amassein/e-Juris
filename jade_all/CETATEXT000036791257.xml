<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036791257</ID>
<ANCIEN_ID>JG_L_2018_04_000000417471</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/79/12/CETATEXT000036791257.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 11/04/2018, 417471</TITRE>
<DATE_DEC>2018-04-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417471</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Richard Senghor</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:417471.20180411</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire et un mémoire en réplique, enregistrés les 19 janvier et 28 mars 2018 au secrétariat du contentieux du Conseil d'État, la Section française de l'Observatoire international des prisons demande au Conseil d'État, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de sa requête tendant à l'annulation pour excès de pouvoir de la décision implicite de rejet résultant du silence gardé par le Premier ministre et le garde des sceaux, ministre de la justice sur sa demande tendant à l'abrogation de l'annexe de l'article R. 57-6-18 et des articles R. 57-8-16 à R. 57-8-20, D. 262 à D. 264 et A. 40-2 du code de procédure pénale, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de l'article 434-35 du code pénal.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code pénal ;<br/>
              - le code de procédure pénale ;<br/>
              - la loi n° 2009-1436 du 24 novembre 2009 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Richard Senghor, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la Section française de l'Observatoire international des prisons ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes de l'article 434-35 du code pénal : " Est puni d'un an d'emprisonnement et de 15 000 euros d'amende le fait, en quelque lieu qu'il se produise, de remettre ou de faire parvenir à un détenu, ou de recevoir de lui et de transmettre des sommes d'argent, correspondances, objets ou substances quelconques en dehors des cas autorisés par les règlements. / Est puni des mêmes peines le fait, pour une personne se trouvant à l'extérieur d'un établissement pénitentiaire ou d'un établissement de santé habilité à recevoir des détenus, de communiquer avec une personne détenue à l'intérieur de l'un de ces établissements, y compris par la voie des communications électroniques, hors les cas où cette communication est autorisée en application de l'article 145-4 du code de procédure pénale ou des articles 39 et 40 de la loi n° 2009-1436 du 24 novembre 2009 pénitentiaire et réalisée par les moyens autorisés par l'administration pénitentiaire ".<br/>
<br/>
              3. La Section française de l'Observatoire international des prisons soutient que cet article, en tant qu'il renvoie, d'une part, au pouvoir règlementaire le soin de fixer la portée du délit d'échange avec une personne détenue et, d'autre part, à l'administration pénitentiaire le soin de fixer, en définissant les moyens de communication autorisés, la portée du délit de communication irrégulière avec une personne détenue, méconnaît le principe de légalité des délits et des peines, est entaché d'incompétence négative en méconnaissance de l'article 34 de la Constitution et porte atteinte au droit au respect de la vie privée, au droit de mener une vie familiale normale ainsi qu'à la liberté d'expression et de communication.<br/>
<br/>
              4. Les dispositions contestées de l'article 434-35 du code pénal, qui incriminent un certain nombre de comportements s'agissant des échanges et des communications avec une personne détenue renvoient, pour déterminer la portée des délits qu'elles définissent, d'une part, aux cas autorisés par les règlements et, d'autre part, aux moyens de communication autorisés par l'administration pénitentiaire. Toutefois l'annexe à l'article R. 57-6-18 du code de procédure pénale ainsi que les articles R. 57-8-16 à R. 57-8-20, D. 262 à D. 264 et A. 40-2 du même code, qui sont au nombre des dispositions réglementaires auxquelles il est ainsi renvoyé, n'ont pas pour objet de préciser la définition de cette infraction pénale mais de définir, dans le cadre du service public pénitentiaire, la nature des biens et objets qui peuvent être échangés entre une personne détenue et l'extérieur ainsi que les modalités autorisées de communication avec une telle personne. Ainsi, la question de la conformité  aux droits et libertés garantis par Constitution de l'article 434-35 du code pénal est sans incidence sur la légalité des dispositions réglementaires du code de procédure pénale dont la Section française de l'Observatoire international des prisons a demandé l'abrogation. Il en résulte que l'article 434-35 du code pénal ne peut être regardé comme applicable au litige, au sens de l'article 23-5 de l'ordonnance du 7 novembre 1958. Par suite, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la Section française de l'Observatoire international des prisons.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la Section française de l'Observatoire international des prisons. <br/>
Article 2 : La présente décision sera notifiée à la Section française de l'Observatoire international des prisons, au Premier ministre, à la garde des sceaux, ministre de la justice et au Secrétariat général du gouvernement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-10-05-01-03 PROCÉDURE. - QPC PORTANT SUR L'ARTICLE 434-35 DU CODE PÉNAL RELATIF AUX INFRACTIONS PÉNALES EN MATIÈRE D'ÉCHANGES ET DE COMMUNICATION AVEC UNE PERSONNE DÉTENUE À L'OCCASION D'UN RECOURS CONTRE LES ARTICLES R. 57-6-18, R. 57-8-16 À R. 57-8-20, D. 262 À D. 264 ET A. 40-2 DU CPP - CONTESTATION SANS INCIDENCE SUR LA LÉGALITÉ DE CES DISPOSITIONS RÉGLEMENTAIRES QUI N'ONT PAS POUR OBJET DE PRÉCISER LA DÉFINITION DE CETTE INFRACTION PÉNALE - CONSÉQUENCE - INAPPLICABILITÉ AU LITIGE [RJ1].
</SCT>
<ANA ID="9A"> 54-10-05-01-03 Les dispositions contestées de l'article 434-35 du code pénal, qui incriminent un certain nombre de comportements s'agissant des échanges et des communications avec une personne détenue renvoient, pour déterminer la portée des délits qu'elles définissent, d'une part, aux cas autorisés par les règlements et, d'autre part, aux moyens de communication autorisés par l'administration pénitentiaire. Toutefois, l'annexe à l'article R. 57-6-18 du code de procédure pénale (CPP) ainsi que les articles R. 57-8-16 à R. 57-8-20, D. 262 à D. 264 et A. 40-2 du même code, qui sont au nombre des dispositions réglementaires auxquelles il est ainsi renvoyé, n'ont pas pour objet de préciser la définition de cette infraction pénale mais de préciser, dans le cadre du service public pénitentiaire, la nature des biens et objets qui peuvent être échangés entre une personne détenue et l'extérieur ainsi que les modalités autorisées de communication avec une telle personne. Ainsi, la question de la conformité  aux droits et libertés garantis par Constitution de l'article 434-35 du code pénal est sans incidence sur la légalité des dispositions réglementaires du CPP dont l'association requérante a demandé l'abrogation. Il en résulte que l'article 434-35 du code pénal ne peut être regardé comme applicable au litige, au sens de l'article 23-5 de l'ordonnance du 7 novembre 1958.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, 9 mars 2018, M. et Mme Agnellet, n° 416492, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
