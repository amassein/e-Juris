<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023564073</ID>
<ANCIEN_ID>JG_L_2011_01_000000345351</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/56/40/CETATEXT000023564073.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 28/01/2011, 345351, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2011-01-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>345351</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Christnacht</PRESIDENT>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Alain  Christnacht</RAPPORTEUR>
<COMMISSAIRE_GVT/>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu, 1° sous le n° 345351, la requête, enregistrée le 28 décembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentée par l'association FRANCE NATURE ENVIRONNEMENT, dont le siège est 10, rue Barbier, au Mans (72000) ; FRANCE  NATURE ENVIRONNEMENT demande au juge des référés du Conseil d'Etat : <br/>
<br/>
              1°) de suspendre l'arrêté de la ministre de l'écologie, du développement durable, des transports et du logement du 22 novembre 2010 modifiant l'arrêté du 19 janvier 2009 relatif aux dates de fermeture de la chasse aux oiseaux de passage et au gibier d'eau, en tant qu'il fixe au 10 février la date de fermeture de la chasse aux oies cendrées, rieuses et des moissons ;<br/>
<br/>
              2°) d'enjoindre à la ministre de prendre sans délai, sous astreinte, un arrêté fixant pour ces trois espèces d'oies une date de fermeture qui ne soit pas postérieure au 20 janvier  et, subsidiairement, au 31 janvier ;  <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              elle soutient qu'elle a un intérêt lui donnant qualité pour agir contre cet arrêté qui lui fait grief ; que la condition d'urgence est satisfaite dès lors que l'arrêté, en prolongeant de dix jours la période de la chasse aux oies, permet la destruction d'un grand nombre de ces oiseaux, dont la migration prénuptiale commence à la mi-janvier, ce qui préjudicie de manière grave et immédiate aux intérêts qu'elle défend ; que la fixation au 10 février de date de fermeture de la chasse aux oies est contraire à la jurisprudence communautaire et nationale en ce qu'elle ne permet pas une protection complète de ces espèces en période prénuptiale ; qu'en l'état des connaissances scientifiques, l'oie cendrée commence sa migration prénuptiale pendant la première décade de février, l'oie rieuse et l'oie des moissons pendant la deuxième décade ; que le Groupe d'experts sur les oiseaux et leur chasse (GEOC), créé par le décret n°2009-401 du 14 avril 2009, a indiqué, dans un rapport publié en 2009, qu'une part significative de la migration des oies avait lieu avant le 31 janvier, portant en moyenne sur 10% des effectifs des oiseaux dénombrés les quatre dernières années ; qu'un article publié dans la revue Wildfowl en 2009 confirme la tendance à l'avancement de la date de migration des oies en France ; que ces données scientifiques devraient conduire à avancer au 20 janvier la date de fermeture de la chasse pour ces espèces ; que l'échelonnement des dates de chasse créerait un risque de confusion entre les trois espèces d'oies d'autant plus important que la chasse peut être pratiquée à des heures pendant lesquelles la luminosité est faible ; que la fermeture de la chasse aux oies le 10 février va provoquer un dérangement important pour les oiseaux pour lesquels la fermeture de la chasse intervient au 31 janvier ; qu'ainsi, l'arrêté attaqué méconnaît l'objectif de protection complète fixé par la directive 79/409/CEE du 2 avril 1979 et repris au 2 de l'article L. 424-2 du code de l'environnement ; <br/>
<br/>
              Vu l'arrêté du 22 novembre 2010 dont la suspension est demandée ;<br/>
<br/>
              Vu la copie de la requête en annulation  présentée par l'association FRANCE NATURE ENVIRONNEMENT ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 17 janvier 2011, présenté par la ministre de l'écologie, du développement durable, des transports et du logement qui conclut au rejet de la requête ; elle soutient que la requête en suspension est irrecevable par suite de l'irrecevabilité de la requête au fond dès lors que cette dernière est dirigée contre des dispositions de l'arrêté du 22 novembre 2010 concernant les oies, qui sont divisibles du reste de l'arrêté et n'ont fait que confirmer celles d'un arrêté du 18 janvier 2010 qui fixaient au 10 février la date de fermeture de la chasse aux oies, lesquelles n'ont pas été contestées dans le délai de recours contentieux ; que l'étude publiée dans la revue Wildfowl et reprise dans un avis du GEOC de 2009 ne fait qu'observer que dans trois départements de l'ouest de la France une faible proportion d'oies cendrées a débuté sa migration prénuptiale avant la fin de la première décade de février, sans conclure de manière générale que cette migration commencerait à des dates plus précoces qu'antérieurement ; que la migration prénuptiale est non significative au mois de janvier et un peu supérieure à 10% pendant la première décade de février ; que le comité Ornis a maintenu la date de début de la migration prénuptiale à la première décade de février ; qu'ainsi la demande de fixer la date de fermeture de la chasse au 20 janvier ne peut qu'être écartée ; que la révision des dates de fermeture à la suite de toute étude scientifique ponctuelle serait source d'insécurité juridique ; que la jurisprudence du Conseil d'Etat a établi que la date de fermeture de la chasse pouvait être fixée à toute date à l'intérieur de la première décade de février, et notamment à la fin de celle-ci comme il a été jugé pour le canard colvert et le canard chipeau ; que l'estimation haute des prélèvements d'oies cendrées par la chasse au cours de la première décade de février serait de 4 000 oies sur un total de 600 000 individus, d'ailleurs en augmentation,  et de 60 000 entamant leur migration au cours de la première décade de février ; qu'ainsi l'objectif de protection complète doit être regardé comme atteint ; que le risque de confusion entre les oies est écarté par la fixation de la même date de fermeture de la chasse pour les trois espèces ; qu'aucune confusion ne peut être faite entre les oies et d'autres oiseaux de passage ; que le risque de dérangement d'autres espèces n'est pas établi ; que la condition d'urgence n'est pas remplie, dès lors que l'arrêté modifiant la date de fermeture de la chasse aux oies est celui du 18 janvier 2010 ; qu'aucun élément nouveau n'est survenu depuis cette dernière date qui soit de nature à créer une situation d'urgence ; que la migration des oies rieuses et des oies des moissons ne commençant que pendant la deuxième décade de février, la condition d'urgence n'est pas remplie pour ces deux espèces ; qu'en raison de la faiblesse des prélèvements réalisés par la chasse au cours de la première décade de février sur la population des oies cendrées, la fixation de la date de fermeture au 10 février ne préjudicie pas de manière grave et immédiate aux intérêts défendus par l'association ;    <br/>
<br/>
<br/>
              Vu le mémoire en réplique, enregistré le 19 janvier 2011, présenté par l'ASSOCIATION FRANCE NATURE ENVIRONNEMENT, qui maintient ses conclusions  par les mêmes moyens ; elle soutient, en outre, que les dispositions de l'arrêté du 22 novembre 2010 sont indivisibles en ce qu'elles introduisent un échelonnement des dates de fermeture à l'intérieur d'un groupe d'espèces qui n'en comportait pas ; qu'en l'absence d'éléments établissant que cet échelonnement ne fait pas obstacle à une protection complète, celui-ci n'est pas conforme à la directive du 30 novembre 2009 ; en raison de la primauté du droit communautaire sur le droit national, l'obligation de suspension d'une disposition contraire à la directive prévaut ; que, contrairement à ce que soutient la ministre, l'étude publiée par la revue Wildfowl conclut bien que la migration des oies se produit plus tôt, ainsi que le mentionne son titre même  ; que la méthodologie scientifique de cette étude est cohérente ; que la proportion des oies cendrées ayant commencé leur migration a atteint 10% non à la fin de la décade mais en cours de décade pour les années 2006, 2007 et 2009 et même le 27 janvier en 2008 ; qu'en tout état de cause un Etat ne peut organiser la protection en fonction d'un pourcentage accepté d'oiseaux y échappant ; que le rapport du GEOC étant postérieur à la parution du rapport du comité Ornis, ce dernier ne pouvait le prendre en compte ; que la décision du Conseil d'Etat du 2 février 2007, relative à la chasse aux turdidés dans 17 départements, ne constitue pas une application d'un principe permettant de fixer la date de fermeture à l'intérieur de la décade retenue comme première période de vulnérabilité ; que la décision du Conseil d'Etat du 5 juillet 2004 a jugé que l'arrêté du 31 décembre 2003 était entaché d'illégalité en ce qu'il autorisait la chasse aux oies au-delà du 31 janvier ; que la ministre n'apporte aucune donnée scientifique nouvelle qui établirait que la date de fermeture de la chasse aux oies pourrait désormais être légalement fixée au-delà du 31 janvier, alors qu'au contraire les études les plus récentes établissent que cette date doit être avancée au 20 janvier ; que la ministre n'apporte aucun élément précis tendant à établir l'absence de risque de dérangement et de perturbation pour les autres espèces ; que le prélèvement de 4000 oiseaux ne peut être regardé comme négligeable ; que le délai mis à saisir la juridiction administrative s'explique par l'accord signé à l'issue de la table ronde sur la chasse, le 14 janvier 2010, par lequel elle s'engageait à ne pas former de recours contre les dates de fermeture de la chasse en 2010, alors même qu'elle les désapprouvait, dans l'attente de la décision  du Conseil d'Etat à intervenir sur les recours formés contre les arrêtés du 19 janvier 2009 fixant des dates échelonnées et postérieures au 31 janvier pour la fermeture de la chasse aux oiseaux d'eau, après laquelle les dates de fermeture de la chasse devaient être revues pour tenir compte de cette décision ; que l'annulation par le Conseil d'Etat des dispositions contestées l'a conduite à réitérer son opposition aux dates de fermeture prévues par l'arrêté du 18 janvier 2010 et à demander à la ministre, par un courrier du 22 octobre 2010, auquel il n'a pas été répondu, de fixer de nouvelles dates de fermeture de la chasse ; que la confirmation de la date du 10 février pour les oies et la suspension de la table ronde sur la chasse, à la demande de la Fédération nationale des chasseurs, l'ont convaincue de demander l'annulation et la suspension de l'arrêté confirmant cette date ; que,  ces circonstances nouvelles justifient que la condition d'urgence soit satisfaite malgré l'absence de recours contre l'arrêté du 18 janvier 2010 ;<br/>
<br/>
              Vu, enregistré le 17 janvier 2011, le mémoire en intervention présenté pour la Fédération nationale des chasseurs, représentée par son président en exercice, domicilié en cette qualité au siège de la fédération, 13, rue du Général Leclerc, à Issy-les-Moulineaux (92136), qui conclut au rejet de la requête ; elle soutient que son intervention est recevable ; que l'arrêté attaqué n'est entaché d'aucune irrégularité de procédure ; que la condition d'urgence ne peut être satisfaite du seul fait que la prolongation de la durée de la chasse aux oies, pour une durée limitée, conduit à la destruction de certains individus des espèces concernées, dès lors que la chasse est une activité licite ; que l'association requérante n'apporte aucun élément démontrant la réalité du préjudice irréversible qu'elle invoque ; que l'association n'a pas formé de recours contre l'arrêté du 18 janvier 2010 qui portait la date de fermeture de la chasse aux oies du 31 janvier au 10 février ; que les dates de fermeture de la chasse aux oiseaux sauvages peuvent être échelonnées, ainsi que l'admet la jurisprudence du Conseil d'Etat, si la protection complète des espèces n'est pas menacée et si cet échelonnement ne crée pas de risques de confusion entre les espèces et de perturbation des autres espèces ; que la date de fermeture peut être fixée au dernier jour de la première décade de vulnérabilité si le choix est fondé sur des données scientifiques circonstanciées, lesquelles évoluent constamment ; que les conclusions de l'étude réalisée par le GEOC ne s'opposent pas au maintien de la date du 10 février ; que l'article publié en anglais dans la revue Wildfowl et non traduit en langue française ne peut être pris en considération ; que les indications de cet article sur une plus grande précocité des vols de retour des oies ne sont pas de nature à remettre en cause la date du 10 février ; que compte tenu de la progression des effectifs des espèces concernées, la fermeture de la chasse au 10 février ne compromet pas leur conservation ; que les risques de confusion évoqués, entre espèces d'oie et, a fortiori, entre les oies et d'autres espèces d'oiseaux, sont inexistants, même en chasse de nuit ; que le risque de dérangement pour d'autres espèces n'est pas démontré par l'association requérante ; <br/>
<br/>
              Vu, 2° sous le n° 345372, la requête, enregistrée le 28 décembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentée par la LIGUE POUR LA PROTECTION DES OISEAUX, dont le siège est 8-10, rue du docteur Pujos - BP 90263, à Rochefort cedex (17305) ; la LIGUE POUR LA PROTECTION DES OISEAUX demande au juge des référés du Conseil d'Etat : <br/>
<br/>
              1°) de suspendre l'arrêté de la ministre de l'écologie, du développement durable, des transports et du logement du 22 novembre 2010 modifiant l'arrêté du 19 janvier 2009 relatif aux dates de fermeture de la chasse aux oiseaux de passage et au gibier d'eau, en tant qu'il fixe au 10 février la date de fermeture de la chasse aux oies cendrées, rieuses et des moissons ;<br/>
<br/>
              2°) d'enjoindre à la ministre de prendre dans les meilleurs délais, sous astreinte, un arrêté fixant au 20 janvier 2011 la fermeture de la chasse aux oies cendrées, rieuses et des moissons ;<br/>
<br/>
              3°) d'enjoindre à la ministre, à titre subsidiaire, de prendre dans les meilleurs délais sous astreinte, un nouvel arrêté fixant au 31 janvier 2011 la fermeture de la chasse pour ces trois espèces d'oies ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 10 000 euros au titre de l'article  L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              elle soutient qu'elle a un intérêt lui donnant qualité pour agir contre cet arrêté qui lui fait grief ; que la condition d'urgence est satisfaite dès lors que l'arrêté, en prolongeant jusqu'au 10 février la période de la chasse aux oies, alors qu'il est établi que la migration prénuptiale des oies cendrées commence désormais toujours avant le 31 janvier, permet la destruction d'un grand nombre de ces oiseaux, ce qui préjudicie de manière grave et immédiate aux intérêts qu'elle défend ; que, dans sa décision du 10 février 2003  Ligue pour la préservation de la faune sauvage et la défense des non-chasseurs , le Conseil d'Etat a estimé que la condition d'urgence était présumée en matière de fixation des dates de la chasse ; que le Groupe d'experts sur les oiseaux et leur chasse (GEOC), créé par le décret n°2009-401 du 14 avril 2009, a indiqué, dans un rapport publié en 2009, qu'une part significative de la migration des oies avait lieu avant le 31 janvier, portant en moyenne sur 10% des effectifs des oiseaux dénombrés les quatre dernières années ; qu'un article publié dans la revue Wildfowl en 2009 confirme la tendance à l'avancement de la date de migration des oies en France ; que le GEOC a également conclu que les oies qui provoquent des dégâts aux cultures aux Pays-Bas ne transitent pas par la France ; qu'en l'état des connaissances scientifiques, l'oie cendrée commence sa migration prénuptiale pendant la première décade de février ; que ces données scientifiques devraient conduire à avancer au 20 janvier la date de fermeture de la chasse pour ces trois espèces, compte tenu des risques de confusion entre elles ; qu'ainsi, l'arrêté attaqué méconnaît l'objectif de protection complète fixé par la directive 79/409/CEE du 2 avril 1979 et repris au 2 de l'article L. 424-2 du code de l'environnement ;<br/>
<br/>
              Vu l'arrêté du 22 novembre 2010, dont la suspension est demandée ;<br/>
<br/>
              Vu la copie de la requête en annulation  présentée par la LIGUE POUR LA PROTECTION DES OISEAUX ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 17 janvier 2011, présenté par la ministre de l'écologie, du développement durable, des transports et du logement qui conclut au rejet de la requête pour les mêmes motifs que ceux exposés sous le n° 345351 ;<br/>
<br/>
              Vu le mémoire en intervention, enregistré le 17 janvier 2011, présenté pour la Fédération nationale des chasseurs qui demande au Conseil d'Etat de rejeter les conclusions tendant à la suspension de l'arrêté du ministre de l'écologie, du développement durable, des transports et du logement du 22 novembre 2010 pour les mêmes motifs que ceux exposés sous le n°345351 ;<br/>
<br/>
              Vu, 3° sous le n° 345383, la requête, enregistrée le 29 décembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentée par l'association LIGUE ROC, dont le siège est 110, boulevard Saint-Germain à Paris (75006), représentée par sa vice-présidente ; l'association LIGUE ROC demande au juge des référés du Conseil d'Etat : <br/>
<br/>
              1°) de suspendre l'arrêté de la ministre de l'écologie, du développement durable, des transports et du logement du 22 novembre 2010 modifiant l'arrêté du 19 janvier 2009 relatif aux dates de fermeture de la chasse aux oiseaux de passage et au gibier d'eau, en tant qu'il fixe au 10 février la date de fermeture de la chasse aux oies cendrées, rieuses et des moissons ;<br/>
<br/>
              2°) d'enjoindre à la ministre de l'écologie, du développement durable, des transports et du logement de prendre, sans délai, un nouvel arrêté fixant au 20 janvier 2011 et, à titre subsidiaire, au plus tard au 31 janvier la fermeture de la chasse pour ces trois espèces d'oies ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              elle soutient qu'elle a un intérêt lui donnant qualité pour agir contre cet arrêté qui lui fait grief ; que la condition d'urgence est satisfaite dès lors que l'arrêté, en prolongeant jusqu'au 10 février la période de la chasse aux oies, alors qu'il est établi que la migration prénuptiale des oies cendrées commence désormais toujours avant le 31 janvier, permet la destruction d'un grand nombre de ces oiseaux, ce qui préjudicie de manière grave et immédiate aux intérêts qu'elle défend ; que le Groupe d'experts sur les oiseaux et leur chasse (GEOC), créé par le décret n°2009-401 du 14 avril 2009, a indiqué, dans un rapport publié en 2009, qu'une part significative de la migration des oies avait lieu avant le 31 janvier, portant en moyenne sur 10% des effectifs des oiseaux dénombrés les quatre dernières années ; qu'un article publié dans la revue Wildfowl en 2009 confirme la tendance à l'avancement de la date de migration des oies en France ; qu'en l'état des connaissances scientifiques, l'oie cendrée commence sa migration prénuptiale pendant la première décade de février ; que ces données scientifiques devraient conduire à avancer au 20 janvier la date de fermeture de la chasse pour ces trois espèces, compte tenu des risques de confusion entre elles ; qu'ainsi, l'arrêté attaqué méconnaît l'objectif de protection complète fixé par la directive 79/409/CEE du 2 avril 1979 et repris au 2 de l'article L. 424-2 du code de l'environnement ;<br/>
<br/>
              Vu l'arrêté du 22 novembre 2010, dont la suspension est demandée ;<br/>
<br/>
              Vu la copie de la requête en annulation  présentée par la LIGUE ROC ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 17 janvier 2011, présenté par la ministre de l'écologie, du développement durable, des transports et du logement qui conclut au rejet de la requête pour les mêmes motifs que ceux exposés sous le n° 345351 ;<br/>
<br/>
              Vu le mémoire en intervention, enregistré le 17 janvier 2011, présenté pour la Fédération nationale des chasseurs qui demande au Conseil d'Etat de rejeter les conclusions tendant à la suspension de l'arrêté de la ministre de l'écologie, du développement durable, des transports et du logement du 22 novembre 2010 pour les mêmes motifs que ceux exposés sous le n°345351 ;<br/>
<br/>
              Vu le mémoire en réplique, enregistré le 19 janvier 2011, présenté par l'association LIGUE ROC qui maintient ses conclusions  par les mêmes moyens ; elle soutient, en outre, que l'arrêté est susceptible de recours du seul fait qu'il est contraire à une directive ; que l'avancée du début de la migration prénuptiale est indéniable ; que, contrairement à ce que soutient la ministre, l'étude publiée par la revue Wildfowl conclut bien que la migration des oies se produit plus tôt, ainsi que le mentionne son titre même  ; que la méthodologie scientifique de cette étude est cohérente ; que les risques de dérangement des autres espèces d'oiseaux sont réels ; que la ministre n'apporte aucun élément permettant de remettre en cause les conclusions des deux rapports scientifiques produits ; qu'en fixant la fermeture de la chasse aux oies au 10 février, l'arrêté du 22 novembre 2010 méconnaît l'objectif de protection complète tel qu'énoncé par la directive européenne ; que la condition d'urgence est satisfaite dès lors qu'autoriser la chasse aux oies équivaut à priver ces espèces du pourcentage de population la plus apte à les pérenniser ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 19 janvier 2011, présenté par la LIGUE ROC, qui maintient ses conclusions  par les mêmes moyens ;<br/>
<br/>
              Vu, 4° sous le n° 345744, la requête, enregistrée le 13 janvier 2011 au secrétariat du contentieux du Conseil d'Etat, présentée par l'ASSOCIATION POUR LA PROTECTION DES ANIMAUX SAUVAGES, dont le siège est 10, rue Haguenau à Strasbourg (67000) ; l'ASSOCIATION POUR LA PROTECTION DES ANIMAUX SAUVAGES demande au juge des référés du Conseil d'Etat : <br/>
<br/>
              1°) de suspendre l'arrêté de la ministre de l'écologie, du développement durable, des transports et du logement du 22 novembre 2010 modifiant l'arrêté du 19 janvier 2009 relatif aux dates de fermeture de la chasse aux oiseaux de passage et au gibier d'eau, en tant qu'il fixe au 10 février la date de fermeture de la chasse aux oies cendrées, rieuses et des moissons  ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre de l'article  L. 761-1 du code de justice administrative ;<br/>
<br/>
              elle soutient qu'elle a un intérêt lui donnant qualité pour agir contre cet arrêté qui lui fait grief ; que la condition d'urgence est satisfaite dès lors que l'arrêté, en prolongeant jusqu'au 10 février la période de la chasse aux oies, préjudicie de manière grave et immédiate aux intérêts qu'elle défend ; que la ministre n'établit pas que le Conseil national de la chasse et de la faune sauvage ait été régulièrement consulté ; que la fixation de la date de fermeture de la chasse au 10 février pour les oies est contraire à la jurisprudence du Conseil d'Etat ; que les travaux scientifiques et notamment le rapport du groupe présidé par le Professeur Lefeuvre, du Muséum national d'histoire naturelle, attestent que l'oie cendrée commence sa migration dès la troisième décade de janvier, ce qui doit conduire à fixer la date de fermeture au 20 janvier  ; que l'existence de deux dates de fermeture crée un risque de confusion entre espèces, compte tenu notamment de la pratique de la chasse de nuit ; qu'ainsi, l'arrêté attaqué méconnaît l'objectif de protection complète fixé par la directive 79/409/CEE du 2 avril 1979 et repris au 2 de l'article L. 424-2 du code de l'environnement ;<br/>
<br/>
              Vu l'arrêté du 22 novembre 2010, dont la suspension est demandée ;<br/>
<br/>
              Vu la copie de la requête en annulation présentée par l'ASSOCIATION POUR LA PROTECTION DES ANIMAUX SAUVAGES ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 17 janvier 2011, présenté par la ministre de l'écologie, du développement durable, des transports et du logement qui conclut au rejet de la requête pour les mêmes motifs que ceux exposés sous le n° 345351 ; elle soutient en outre que le moyen tiré de l'irrégularité de la réunion du Conseil national de la chasse et de la faune sauvage n'est pas assorti des précisions permettant d'en apprécier la portée et, en tout état de cause, manque en fait ;<br/>
<br/>
              Vu le mémoire en intervention, enregistré le 17 janvier 2011, présenté pour la Fédération nationale des chasseurs qui demande au Conseil d'Etat de rejeter les conclusions tendant à la suspension de l'arrêté de la ministre de l'écologie, du développement durable, des transports et du logement du 22 novembre 2010 pour les mêmes motifs que ceux exposés sous le n°345351 ;<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu la directive 79/409/CEE du Conseil du 2 avril 1979, notamment le 4 de son article 7 ;<br/>
<br/>
              Vu la directive 2009/147/CE du Parlement européen et du Conseil du 30 novembre 2009, notamment le 4 de son article 7 ;<br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, l'ASSOCIATION FRANCE NATURE ENVIRONNEMENT, la LIGUE POUR LA PROTECTION DES OISEAUX, la LIGUE ROC, l'ASSOCIATION POUR LA PROTECTION DES ANIMAUX SAUVAGES et, d'autre part, la ministre de l'écologie, du développement durable, des transports et du logement ainsi que la Fédération nationale des chasseurs ;<br/>
<br/>
              Vu le procès-verbal de l'audience du 20 janvier 2011 à 9 heures 30, au cours de laquelle ont été entendus : <br/>
<br/>
              - la représentante de l'ASSOCIATION FRANCE NATURE ENVIRONNEMENT ;<br/>
<br/>
              - le représentant de la LIGUE POUR LA PROTECTION DES OISEAUX ;<br/>
<br/>
              - la représentante de la LIGUE ROC ;<br/>
<br/>
              - Me Farge, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la Fédération nationale des chasseurs ; <br/>
<br/>
              - les représentants de la ministre de l'écologie, du développement durable, des transports et du logement ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clôturé l'instruction ;<br/>
<br/>
              Considérant que les requêtes sont dirigées contre le même arrêté du 22 novembre 2010 de la ministre de l'écologie, du développement durable, des transports et du logement ; qu'il y a lieu de les joindre pour statuer par une seule décision ; <br/>
<br/>
              Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative :  Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision  ;<br/>
<br/>
              Considérant que la Fédération nationale des chasseurs a intérêt au maintien de l'arrêté attaqué par les associations requérantes ; qu'ainsi, ses interventions en défense sont recevables ; <br/>
<br/>
              Considérant qu'il résulte de l'instruction que, par une décision en date du 23 juillet 2010, le Conseil d'Etat, statuant au contentieux, a, d'une part, annulé l'article 1er de l'arrêté du 19 janvier 2009 relatif aux dates de fermeture de la chasse aux oiseaux de passage et au gibier d'eau, hormis les limicoles et les oies, en tant qu'il fixait des dates de fermeture de la chasse distinctes pour les canards de surface, les canards plongeurs fréquentant les plans d'eau et cours d'eau intérieurs et les rallidés, alors qu'existait un risque de confusion pour la chasse entre ces espèces et, d'autre part, a enjoint au ministre de prendre un nouvel arrêté, dans un délai de quatre mois, fixant la date de fermeture de la chasse au plus tard le 31 janvier, date du dernier jour de la période de vulnérabilité de deux de ces espèces ; que, par un arrêté du 18 janvier 2010, le ministre avait ajouté à la deuxième ligne du tableau de l'article 1er de l'arrêté du 19 janvier 2009, correspondant à des espèces pour lesquelles la date de fermeture de la chasse était fixée au 10 février, de manière pérenne, les oies cendrées, rieuses et des moissons, espèces pour lesquelles l'arrêté du 19 janvier 2009 ne fixait pas jusqu'alors la date de fermeture de la chasse, qui avait été fixée pour ces espèces au 31 janvier par l'arrêté du 17 janvier 2005 relatif aux dates de fermeture de la chasse aux oiseaux de passage et au gibier d'eau et, pour la seule année 2008, au 10 février par l'arrêté du 28 janvier 2008 relatif aux dates de fermeture de la chasse aux oiseaux de passage et au gibier d'eau pour 2008  ; <br/>
<br/>
              Considérant que, pour l'exécution de la décision du Conseil d'Etat du 23 juillet 2010, la ministre de l'écologie, du développement durable, des transports et du logement a, par l'arrêté attaqué en date du 22 novembre 2010, à nouveau modifié l'arrêté du 19 janvier 2009, pour les espèces mentionnées par cette décision ; que cet arrêté modificatif a maintenu le 10 février comme date de fermeture de la chasse aux oies, sur laquelle la décision du Conseil d'Etat ne porte pas et est sans incidence, le risque de confusion pendant la chasse entre les oies et d'autres espèces d'oiseaux étant inexistant ; <br/>
<br/>
              Considérant que l'association FRANCE NATURE ENVIRONNEMENT fait valoir que les associations requérantes n'ont pas contesté dans le délai du recours contentieux les dispositions de l'arrêté du 18 janvier 2010 relatives à la date de la chasse aux oies au motif qu'elles s'étaient engagées, lors d'une table ronde sur la chasse réunie par le ministre en janvier 2010, à ne pas former de recours contre cet arrêté dans l'attente de la décision du Conseil d'Etat sur les recours qu'elles avaient formés contre l'arrêté du 19 janvier 2009 ; que, le Conseil d'Etat ayant, par sa décision du 23 juillet 2010, annulé certaines dispositions de cet arrêté et la ministre n'ayant pas répondu à la lettre du 22 octobre par laquelle elles lui demandaient de modifier l'arrêté du 19 janvier 2009 pour fixer au 31 janvier la date de fermeture de la chasse aux oies, les travaux de la table ronde ayant, en outre, été suspendus, elles ont alors contesté l'arrêté du 22 novembre 2010, lequel, en confirmant la date du 10 février pour la fermeture de la chasse aux oies, traduisait le refus de la ministre de répondre favorablement à la demande qu'elles lui avaient adressée le 22 octobre d'avancer cette date ; que, toutefois, ces circonstances, pour particulières qu'elles soient, ne permettent pas de regarder la décision attaquée comme une décision intervenue dans des circonstances nouvelles et qui, dès lors, ne serait pas purement confirmative ; que, par suite, les requêtes tendant à l'annulation de l'arrêté du 22 novembre 2010, en tant qu'il maintient au 10 février la date de fermeture de la chasse aux oies, sont irrecevables ; <br/>
<br/>
              Considérant que si la requête tendant à l'annulation du ou des actes administratifs dont la suspension est demandée est irrecevable, aucun des moyens présentés au soutien d'une requête formée sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative n'est susceptible de créer un doute sérieux quant à la légalité du ou des actes administratifs contestés ; que, dès lors, les requêtes des associations requérantes tendant à la suspension de l'arrêté du 22 novembre 2010 ne peuvent qu'être rejetées, y compris leurs conclusions à fin d'injonction et celles tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : Les interventions de la Fédération nationale des chasseurs sont admises.<br/>
<br/>
Article 2 : Les requêtes de l'association FRANCE NATURE ENVIRONNEMENT, de la LIGUE POUR LA PROTECTION DES OISEAUX, de la LIGUE ROC et de l'ASSOCIATION POUR LA PROTECTION DES ANIMAUX SAUVAGES sont rejetées.<br/>
<br/>
Article 3 : La présente ordonnance sera notifiée à l'association FRANCE NATURE ENVIRONNEMENT, à la LIGUE POUR LA PROTECTION DES OISEAUX, à la LIGUE ROC, à l'ASSOCIATION POUR LA PROTECTION DES ANIMAUX SAUVAGES, à la ministre de l'écologie, du développement durable, des transports et du logement et à la Fédération nationale des chasseurs.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
