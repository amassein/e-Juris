<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038778968</ID>
<ANCIEN_ID>JG_L_2019_07_000000424998</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/77/89/CETATEXT000038778968.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 17/07/2019, 424998, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424998</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>Mme Liza Bellulo</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:424998.20190717</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Montreuil, d'une part, d'annuler l'avis de saisie administrative à tiers détenteur émis le 6 mars 2018 par la direction départementale des finances publiques de Seine-Saint-Denis et notifié à la caisse nationale d'assurance vieillesse des travailleurs salariés (CNAVTS) en vue du recouvrement de sommes dues par lui, ainsi que la décision du 13 avril 2018 par laquelle la direction départementale des finances publiques de Seine-Saint-Denis a rejeté le recours qu'il avait formé contre cet avis, d'autre part, d'en prononcer la mainlevée.<br/>
<br/>
               Par une ordonnance n° 1805386 du 28 août 2018, prise sur le fondement de l'article R. 222-1 du code de justice administrative, le président de la 5ème chambre de ce tribunal a rejeté son recours.<br/>
<br/>
              Par un pourvoi, enregistré le 23 octobre 2018 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
               2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative, notamment son article R. 611-8 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Liza Bellulo, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que <br/>
M. A...s'est vu réclamer par l'association pour l'emploi dans l'industrie et le commerce (Assedic), le 14 décembre 2006, un trop-perçu d'allocation spécifique de solidarité d'un montant de 18 083,19 euros. Le 6 mars 2018, la direction départementale des finances publiques de Seine-Saint-Denis lui a notifié à un avis de saisie administrative à tiers détenteur portant sur une somme de 4 173 euros, le tiers détenteur saisi étant la caisse nationale d'assurance vieillesse des travailleurs salariés (CNAVTS). Par une décision du 13 avril 2018, la direction départementale des finances publiques de Seine-Saint-Denis a rejeté la réclamation formée par M. A...contre cet avis de saisie administrative à tiers détenteur. L'intéressé a demandé au tribunal administratif de Montreuil, d'une part, d'annuler cet avis ainsi que la décision du 13 avril 2018 et, d'autre part, de prononcer la mainlevée de cet acte de poursuite. Il se pourvoit en cassation contre l'ordonnance du 28 août 2018, prise sur le fondement de l'article R. 222-1 du code de justice administrative, par laquelle le président de la 5ème chambre de ce tribunal a rejeté ses demandes.  <br/>
<br/>
              2. Aux termes de l'article R. 222-1 du code de justice administrative : " (...) les présidents de formation de jugement des tribunaux (...) peuvent, par ordonnance : (...) 4° Rejeter les requêtes manifestement irrecevables, lorsque la juridiction n'est pas tenue d'inviter leur auteur à les régulariser ou qu'elles n'ont pas été régularisées à l'expiration du délai imparti par une demande en ce sens ; (...) ". Aux termes de l'article R. 414-1 du même code : " Lorsqu'elle est présentée par un avocat (...) la requête doit, à peine d'irrecevabilité, être adressée à la juridiction par voie électronique au moyen d'une application informatique dédiée accessible par le réseau internet. La même obligation est applicable aux autres mémoires du requérant. (...) ". <br/>
<br/>
              3.  Il ressort des pièces du dossier soumis aux juges du fond que l'avocat de M. A... a reçu, le 15 juin 2018, une invitation à régulariser dans un délai de quinze jours la requête qu'il avait introduite devant le tribunal administratif en l'adressant à la juridiction par voie électronique au moyen de l'application Télérecours. Il ressort également des pièces du dossier que l'avocat du requérant a procédé à cette régularisation le 28 juin 2018, soit avant l'expiration du délai imparti. Il en résulte qu'en jugeant que la requête de M. A...était manifestement irrecevable au motif que son conseil s'était abstenu de procéder, dans le délai imparti, à la régularisation à laquelle il avait été invité, le président de la 5ème chambre du tribunal administratif de Montreuil a entaché son ordonnance d'inexactitude matérielle et, par voie de conséquence, méconnu les dispositions de l'article R. 414-1 du code de justice administrative. Par suite et sans qu'il soit besoin d'examiner l'autre moyen de son pourvoi, M. A... est fondé à demander l'annulation de l'ordonnance qu'il attaque.<br/>
<br/>
              4.  Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 000 euros à verser à M. A...au titre de <br/>
l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance n° 1805386 du 28 août 2018 du président de la <br/>
5ème chambre du tribunal administratif de Montreuil est annulée.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Montreuil.<br/>
Article 3 : L'Etat versera une somme de 1 000 euros à M. A...au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. B...A...et au ministre de l'action et des comptes publics.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
