<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028782003</ID>
<ANCIEN_ID>JG_L_2014_03_000000360204</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/78/20/CETATEXT000028782003.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 26/03/2014, 360204, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-03-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360204</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Agnès Martinel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:360204.20140326</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 13 juin et 27 août 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B... A..., demeurant... ; M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 1016856-1017007/5-1 du 12 avril 2012 par lequel le tribunal administratif de Paris a rejeté ses demandes tendant, d'une part, à l'annulation de la décision implicite de rejet née du silence gardé par le ministre de l'intérieur, de l'outre-mer et des collectivités territoriales sur sa demande tendant à être inscrit sur la liste d'aptitude à l'avancement au grade d'attaché principal d'administration de l'intérieur et de l'outre-mer et, d'autre part, à l'annulation de l'arrêté du ministre de l'intérieur, de l'outre-mer et des collectivités territoriales du 4 mars 2010 portant inscription sur le tableau d'avancement au grade d'attaché principal d'administration de l'intérieur et de l'outre-mer au titre de l'année 2010 ainsi que la décision implicite de rejet née du silence gardé par le ministre sur son recours tendant à l'annulation de cet arrêté ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu le décret n° 82-451 du 28 mai 1982 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Agnès Martinel, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de M. A...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 14 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat : " Dans chaque corps de fonctionnaires existent une ou plusieurs commissions administratives paritaires (...). / Ces commissions sont consultées sur les décisions individuelles intéressant les membres du ou des corps qui en relèvent " ; qu'aux termes de l'article 25 du décret du 28 mai 1982 relatif aux commissions administratives paritaires : " Les commissions administratives paritaires (...) connaissent des questions d'ordre individuel résultant de l'application (...) des articles 45, 48, 51, 52, 55, 58, 60, 67, 70 et 72 de la loi du 11 janvier 1984 précitée. (...) " ;  qu'en vertu de l'article 58 de la loi du 11 janvier 1984 précitée, l'avancement de grade a lieu soit au choix par voie d'inscription à un tableau annuel d'avancement, établi après avis de la commission administrative paritaire, par appréciation de la valeur professionnelle et des acquis de l'expérience professionnelle, soit par voie d'inscription à un tableau annuel d'avancement, établi après avis de la commission administrative paritaire, après une sélection par voie d'examen professionnel, soit par sélection opérée par voie de concours professionnel ; qu'aux termes de l'article 59 de la même loi : " L'avancement des fonctionnaires bénéficiant d'une décharge totale de service pour l'exercice de mandats syndicaux a lieu sur la base de l'avancement moyen des fonctionnaires du corps auquel ils appartiennen. " ;<br/>
<br/>
              2. Considérant que, s'il résulte des dispositions de l'article 59 de la loi du 11 janvier 1984 que les décisions relatives à l'avancement de grade des fonctionnaires bénéficiant d'une décharge totale de service obéissent à des modalités dérogatoires, fondées sur la référence à l'avancement moyen des fonctionnaires du corps auxquels ils appartiennent, les fonctionnaires bénéficiant d'une telle décharge sont promus au grade supérieur par voie d'inscription à un tableau annuel d'avancement, qui, en application des dispositions des articles 58 de la loi du 11 janvier 1984 et 25 du décret du 28 mai 1982, est arrêté après avis de la commission administrative paritaire compétente ; que, par suite, en jugeant, pour rejeter la demande de M. A..., attaché de préfecture bénéficiant d'une décharge totale de service tendant à l'annulation de décisions implicites du ministre de l'intérieur, de l'outre-mer et des collectivités territoriales rejetant sa demande d'inscription sur la liste d'aptitude de l'année 2010 à l'avancement au grade d'attaché principal d'administration de l'intérieur et de l'outre-mer et de l'arrêté du même ministre approuvant le tableau d'avancement à ce grade pour 2010, qu'il ne ressortait d'aucune disposition législative ou réglementaire, ni d'aucun principe général du droit, que la commission administrative paritaire devait se prononcer sur le droit à l'avancement des agents bénéficiant d'une décharge totale de service, le tribunal administratif a commis une erreur de droit ; que M. A...est fondé, pour ce motif et sans qu'il soit besoin d'examiner l'autre moyen de son pourvoi, à demander l'annulation du jugement attaqué ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros, à verser à M. A..., au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Paris du 12 avril 2012 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Paris.<br/>
Article 3 : L'Etat versera à M. A... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. B... A...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
