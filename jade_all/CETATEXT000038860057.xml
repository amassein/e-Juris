<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038860057</ID>
<ANCIEN_ID>JG_L_2019_07_000000417109</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/86/00/CETATEXT000038860057.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Formation spécialisée, 31/07/2019, 417109</TITRE>
<DATE_DEC>2019-07-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417109</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Formation spécialisée</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Nathalie Escaut</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEFSP:2019:417109.20190731</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 417109, la procédure suivante :<br/>
<br/>
              Par une requête et un nouveau mémoire, enregistrés les 5 janvier 2018 et 11 décembre 2018 au secrétariat du contentieux du Conseil d'Etat, M. A...B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision, révélée par le courrier de la présidente de la Commission nationale de l'informatique et des libertés (CNIL) du 14 novembre 2017, par laquelle le ministre de l'intérieur lui a refusé l'accès aux données susceptibles de le concerner figurant dans le traitement automatisé de données à caractère personnel mis en oeuvre par la direction générale de la sécurité intérieure (DGSI), dénommé CRISTINA ;<br/>
<br/>
              2°) d'enjoindre, en tant que de besoin, d'effacer les données le concernant dans ce fichier.<br/>
<br/>
<br/>
              Vu 2°, sous le n° 420925, la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 22 mai 2018 au secrétariat du contentieux du Conseil d'Etat, M. A...B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision, révélée par le courrier de la présidente de la Commission nationale de l'informatique et des libertés (CNIL) du 22 mars 2018, par laquelle la ministre des armées lui a refusé l'accès aux données susceptibles de le concerner figurant dans le traitement automatisé de données à caractère personnel mis en oeuvre par la direction générale de la sécurité extérieure (DGSE) ;<br/>
<br/>
              2°) d'enjoindre, en tant que de besoin, d'effacer les données le concernant dans ce fichier.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code de la sécurité intérieure ;<br/>
              - la loi n° 78-17 du 6 janvier 1978 ; <br/>
              - le décret n° 2005-1309 du 20 octobre 2005 ;<br/>
              -le décret n° 2019-536 du 29 mai 2019 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une séance à huis-clos, d'une part, M.B..., et d'autre part, la ministre des armées, le ministre de l'intérieur et la Commission nationale de l'informatique et des libertés, qui ont été mis à même de prendre la parole avant les conclusions ;<br/>
<br/>
              Et après avoir entendu en séance :<br/>
<br/>
              - le rapport de Mme Nathalie Escaut, conseiller d'Etat,<br/>
              - et, hors la présence des parties, les conclusions de Cécile Barrois de Sarigny, rapporteur public ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. En vertu des dispositions de l'article 41 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, applicables à la procédure en cause et désormais reprises à l'article 118 de la même loi, et des dispositions de l'article 88 du décret susvisé du 20 octobre 2005, applicables à la procédure en cause et reprises désormais à l'article 143 du décret susvisé du 29 mai 2019, lorsqu'un traitement intéresse la sûreté de l'Etat ou la défense, les demandes tendant à l'exercice du droit d'accès, de rectification ou d'effacement sont adressées à la Commission nationale de l'informatique et des libertés (CNIL). Celle-ci désigne l'un de ses membres appartenant ou ayant appartenu au Conseil d'Etat, à la Cour de cassation ou à la Cour des comptes pour mener les investigations utiles et faire procéder aux modifications nécessaires. Lorsque la Commission constate, en accord avec le responsable du traitement, que la communication de données qui y sont contenues, ou l'information selon laquelle le traitement ne contient aucune donnée concernant le demandeur, ne met pas en cause ses finalités, la sûreté de l'Etat, la défense ou la sécurité publique, ces données ou cette information peuvent être communiquées au requérant. Dans le cas contraire, la Commission se borne à notifier au requérant qu'il a été procédé aux vérifications nécessaires, sans autre précision, avec la mention des voies et délais de recours.<br/>
<br/>
              2. En vertu de l'article 31 de la loi du 6 janvier 1978, les traitements de données à caractère personnel mis en oeuvre pour le compte de l'Etat et intéressant la sûreté de l'Etat, la défense ou la sécurité publique sont autorisés par arrêté du ou des ministres compétents, pris après avis motivé de la CNIL, publié avec l'arrêté autorisant le traitement. Ceux de ces traitements qui portent sur des données mentionnées au I de l'article 6 de la même loi doivent être autorisés par décret en Conseil d'Etat pris après avis motivé la Commission, publié avec ce décret. Un décret en Conseil d'Etat peut dispenser de publication l'acte réglementaire autorisant la mise en oeuvre de ces traitements ; le sens de l'avis émis par la CNIL est alors publié avec ce décret.<br/>
<br/>
              3. L'article L. 841-2 du code de la sécurité intérieure prévoit que le Conseil d'Etat est compétent pour connaître, dans les conditions prévues au chapitre III bis du titre VII du livre VII du code de justice administrative, des requêtes concernant la mise en oeuvre de l'article 41 devenu l'article 118 de la loi du 6 janvier 1978, pour les traitements ou parties de traitements intéressant la sûreté de l'Etat dont la liste est fixée par décret en Conseil d'Etat. En vertu de l'article R. 841-2 du même code, figurent notamment au nombre de ces traitements le fichier CRISTINA et le fichier de la direction générale de la sécurité extérieure (DGSE).<br/>
<br/>
              4. L'article L. 773-8 du code de justice administrative dispose que, lorsqu'elle traite des requêtes relatives à la mise en oeuvre de l'article 41 devenu l'article 118 de la loi du 6 janvier 1978, " la formation de jugement se fonde sur les éléments contenus, le cas échéant, dans le traitement sans les révéler ni révéler si le requérant figure ou non dans le traitement. Toutefois, lorsqu'elle constate que le traitement ou la partie de traitement faisant l'objet du litige comporte des données à caractère personnel le concernant qui sont inexactes, incomplètes, équivoques ou périmées, ou dont la collecte, l'utilisation, la communication ou la conservation est interdite, elle en informe le requérant, sans faire état d'aucun élément protégé par le secret de la défense nationale. Elle peut ordonner que ces données soient, selon les cas, rectifiées, mises à jour ou effacées. Saisie de conclusions en ce sens, elle peut indemniser le requérant ". L'article R. 773-20 du même code précise que : " Le défendeur indique au Conseil d'Etat, au moment du dépôt de ses mémoires et pièces, les passages de ses productions et, le cas échéant, de celles de la Commission nationale de contrôle des techniques de renseignement, qui sont protégés par le secret de la défense nationale. /Les mémoires et les pièces jointes produits par le défendeur et, le cas échéant, par la Commission nationale de contrôle des techniques de renseignement sont communiqués au requérant, à l'exception des passages des mémoires et des pièces qui, soit comportent des informations protégées par le secret de la défense nationale, soit confirment ou infirment la mise en oeuvre d'une technique de renseignement à l'égard du requérant, soit divulguent des éléments contenus dans le traitement de données, soit révèlent que le requérant figure ou ne figure pas dans le traitement. /Lorsqu'une intervention est formée, le président de la formation spécialisée ordonne, s'il y a lieu, que le mémoire soit communiqué aux parties, et à la Commission nationale de contrôle des techniques de renseignement, dans les mêmes conditions et sous les mêmes réserves que celles mentionnées à l'alinéa précédent ".<br/>
<br/>
              5. Il ressort des pièces du dossier que M. B...a saisi la CNIL afin de pouvoir accéder aux données susceptibles de le concerner figurant dans le fichier CRISTINA et le fichier de la DGSE. La Commission a désigné, en application de l'article 41 de la loi du 6 janvier 1978, alors applicable, un membre pour mener toutes investigations utiles et faire procéder, le cas échéant, aux modifications nécessaires. Par des lettres des 14 novembre 2017 et 22 mars 2018, la présidente de la Commission a informé M. B...qu'il avait été procédé à l'ensemble des vérifications demandées et que la procédure était terminée, sans lui apporter d'autres informations. M. B...demande l'annulation des refus, révélés par ces courriers, de lui donner accès aux données susceptibles de le concerner et figurant dans les fichiers litigieux et d'enjoindre à ces ministres de procéder à leur effacement.<br/>
<br/>
              6. Le ministre de l'intérieur, la ministre des armées et la CNIL ont communiqué au Conseil d'Etat, dans les conditions prévues à l'article R. 773-20 du code de justice administrative, les éléments relatifs à la situation de l'intéressé. Les ministres ont, en outre, communiqué les actes réglementaires, non publiés, créant les fichiers litigieux.<br/>
<br/>
              7. Il appartient à la formation spécialisée, créée par l'article L. 773-2 du code de justice administrative précité, saisie de conclusions dirigées contre le refus de communiquer les données relatives à une personne qui allègue être mentionnée dans un fichier figurant à l'article R. 841-2 du code de la sécurité intérieure, de vérifier, au vu des éléments qui lui ont été communiqués hors la procédure contradictoire, si le requérant figure ou non dans le fichier litigieux. Dans l'affirmative, il lui appartient d'apprécier si les données y figurant sont pertinentes au regard des finalités poursuivies par ce fichier, adéquates et proportionnées. Pour ce faire, elle peut relever d'office tout moyen ainsi que le prévoit l'article L. 773-5 du code de justice administrative. Lorsqu'il apparaît soit que le requérant n'est pas mentionné dans le fichier litigieux soit que les données à caractère personnel le concernant qui y figurent ne sont entachées d'aucune illégalité, la formation de jugement rejette les conclusions du requérant sans autre précision. Dans le cas où des informations relatives au requérant figurent dans le fichier litigieux et apparaissent entachées d'illégalité soit que les données à caractère personnel le concernant sont inexactes, incomplètes, équivoques ou périmées soit que leur collecte, leur utilisation, leur communication ou leur consultation est interdite, elle en informe le requérant sans faire état d'aucun élément protégé par le secret de la défense nationale. Cette circonstance, le cas échéant relevée d'office par le juge dans les conditions prévues à l'article R. 773-21 du code de justice administrative, implique nécessairement que l'autorité gestionnaire du fichier rétablisse la légalité en effaçant ou en rectifiant, dans la mesure du nécessaire, les données illégales. Dans pareil cas, doit être annulée la décision implicite refusant de procéder à un tel effacement ou à une telle rectification.<br/>
<br/>
              8. La formation spécialisée a procédé à l'examen des actes réglementaires autorisant la création des fichiers litigieux ainsi que des éléments fournis par les ministres et par la CNIL, laquelle a effectué les diligences qui lui incombent dans le respect des règles de compétence et de procédure applicables.<br/>
<br/>
              9. Il résulte, en premier lieu, de cet examen, qui s'est déroulé selon les modalités décrites au point précédent, qui n'ont révélé aucune illégalité s'agissant du fichier de la DGSE, que les conclusions de M. B...tendant à l'annulation de la décision de la ministre des armées lui refusant l'accès aux données susceptibles de le concerner dans ce traitement doivent être rejetées. <br/>
<br/>
              10. Il résulte, en second lieu, de cet examen ainsi que des vérifications opérées par la formation spécialisée que le fichier CRISTINA contient, eu égard aux finalités qu'il poursuit, des données périmées concernant M.B.... Par suite, il y a lieu d'en ordonner l'effacement. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Il est enjoint au ministre de l'intérieur (Direction générale de la sécurité intérieure) de procéder à l'effacement des données concernant M. A...B...contenues dans le traitement de données personnelles dénommé CRISTINA. <br/>
Article 2 : Le surplus des conclusions des requêtes de M. A...B...est rejeté.<br/>
Article 3 : La présente décision sera notifiée à M. A...B..., à la ministre des armées et au ministre de la défense.<br/>
Copie en sera adressée à la Commission nationale de l'informatique et des libertés.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-07-06 DROITS CIVILS ET INDIVIDUELS. - TRAITEMENTS AUTOMATISÉS INTÉRESSANT LA SÛRETÉ DE L'ETAT - LOI N° 2015-912 DU 24 JUILLET 2015 - CONTRÔLE JURIDICTIONNEL - OFFICE DE LA FORMATION SPÉCIALISÉE (ART. L. 773-2 DU CJA) - EXAMEN RÉVÉLANT L'EXISTENCE DE DONNÉES PÉRIMÉES CONCERNANT L'AUTEUR DE LA DEMANDE - INJONCTION DE LES EFFACER [RJ1].
</SCT>
<ANA ID="9A"> 26-07-06 Formation spécialisée constatant, à l'issue de l'examen d'un fichier figurant à l'article R. 841-2 du code de la sécurité intérieure (CSI) dans lequel une personne allègue être mentionnée, que celui-ci contient, eu égard aux finalités qu'il poursuit, des données périmées concernant l'intéressé.,,,Cette circonstance implique nécessairement que l'autorité gestionnaire du fichier rétablisse la légalité en effaçant ou en rectifiant, dans la mesure du nécessaire, les données illégales.,,,La formation spécialisée ordonne donc l'effacement des données en cause.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur l'office de la formation spécialisée en présence d'une illégalité, CE, formation spécialisée, 19 octobre 2016,,, n° 400688, p. 430 ; pour un précédent en cas de données périmées, CE, formation spécialisée, 28 décembre 2018,,, n° 396542, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
