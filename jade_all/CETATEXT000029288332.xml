<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029288332</ID>
<ANCIEN_ID>JG_L_2014_07_000000380474</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/28/83/CETATEXT000029288332.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 23/07/2014, 380474, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380474</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP HEMERY, THOMAS-RAQUIN ; SCP THOUIN-PALAT, BOUCARD ; HAAS</AVOCATS>
<RAPPORTEUR>Mme Laurence Marion</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:380474.20140723</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 20 mai et 3 juin 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. F...B..., demeurant..., MmeI..., demeurant..., M. J...A..., demeurant..., Mme E...C..., demeurant..., Mme D...G..., demeurant ... et la société Téléimagerie médicale temps réel de Bourgogne, dont le siège est 1 rue d'Assas à Dijon (21000) ; M. B...et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1401105 du 5 mai 2014 par laquelle le juge des référés du tribunal administratif de Dijon, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a rejeté leur demande tendant à ce que soit ordonnée la suspension de l'exécution du marché de téléconsultation attribué par le centre hospitalier intercommunal de Châtillon-sur-Seine - Montbard à la société Group Emaging Technology ;<br/>
<br/>
              2°) de mettre à la charge du centre hospitalier intercommunal de Châtillon-sur-Seine - Montbard et de la société Group Emaging Technology le versement d'une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de la santé publique ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Nuttens, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Hémery, Thomas-Raquin, avocat de M.B..., MmeH..., M.A..., MmeC..., Mme G...et de la société Téléimagerie médicale temps réel de Bourgogne, à Me Haas, avocat du centre hospitalier intercommunal de Chatillon-sur-Seine - Montbard et à la SCP Thouin-Palat, Boucard, avocat de la société Group Emaging Technology ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'ordonnance attaquée que le centre hospitalier intercommunal de Châtillon-sur-Seine - Montbard a publié au mois de septembre 2013 un avis d'appel à la concurrence pour un appel d'offres ouvert en vue de passer un marché de " prestations de téléconsultation de radiologie " ; que l'offre de la société Téléimagerie médicale temps réel de Bourgogne a été rejetée et le marché attribué à la société Group Emaging Technology ; que M. B...et les autres médecins radiologues associés au sein de la société Téléimagerie médicale temps réel de Bourgogne ont contesté, avec cette société, la validité de ce contrat devant le tribunal administratif de Dijon et ont assorti leur recours d'une demande tendant, sur le fondement de l'article L. 521-1 du code de justice administrative, à la suspension de son exécution ; qu'ils se pourvoient en cassation contre l'ordonnance du 5 mai 2014 par laquelle le juge des référés du tribunal administratif de Dijon a rejeté cette demande de suspension ; <br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; que l'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celui-ci porte atteinte de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ; que l'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire ; que, contrairement à ce que soutiennent M. B... et autres, l'urgence justifiant la suspension d'un contrat sur le fondement des dispositions précitées ne saurait être présumée, alors même que sont invoquées des atteintes à différents intérêts publics  ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que M. B...et autres soutenaient, à l'appui de leur demande de suspension du contrat litigieux, que son exécution conduirait à méconnaître plusieurs règles s'imposant aux professions de santé,  notamment aux médecins, et qu'elle était, par suite, de nature à porter une atteinte grave et immédiate à un intérêt public ; <br/>
<br/>
              4. Considérant, toutefois, qu'après avoir souverainement relevé, sans dénaturer les faits qui lui étaient soumis, que la société attributaire du contrat avait fourni les données relatives au réseau des radiologues auxquels elle était en mesure de faire appel et joint leurs diplômes universitaires ainsi que leurs attestations d'inscription à l'ordre des médecins et qu'elle ne pratiquait pas elle-même la lecture et l'interprétation des radios, le juge des référés a pu, sans donner aux faits une qualification inexacte ni commettre d'erreur de droit, juger que l'exécution du contrat en litige ne conduisait pas à un exercice illégal de la médecine et qu'aucune atteinte à un intérêt public sur ce point ne caractérisait une situation d'urgence ; <br/>
<br/>
              5. Considérant, par ailleurs, qu'après avoir relevé que l'interdiction pour toute personne ne remplissant pas les conditions requises pour l'exercice de la profession de médecin de recevoir, en vertu d'une convention, la totalité ou une quote-part des honoraires ou des bénéfices provenant de l'activité professionnelle d'un médecin, prévue par le premier alinéa de l'article L. 4113-5 du code de la santé publique, n'était pas applicable à l'activité de télémédecine, en vertu du second alinéa du même article, le juge des référés n'a commis aucune erreur de droit et a suffisamment motivé son ordonnance en jugeant qu'aucune atteinte à un intérêt public qui résulterait d'une méconnaissance de l'interdiction du compérage ne caractérisait une situation d'urgence ;<br/>
<br/>
              6. Considérant, enfin, que le juge des référés a suffisamment motivé son ordonnance et n'a commis aucune erreur de droit en jugeant que l'exécution du contrat ne portait aucune atteinte grave et immédiate à un intérêt public qui résulterait de la méconnaissance de l'indépendance professionnelle des médecins ou de l'interdiction de fixer des honoraires médicaux dans un but de commerce ; <br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que M. B... et autres ne sont pas fondés à demander l'annulation de l'ordonnance qu'ils attaquent ;<br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge du centre hospitalier intercommunal de Châtillon-sur-Seine - Montbard et de la société Group Emaging Technology, qui ne sont pas, dans la présente instance, les parties perdantes, le versement de la somme que les requérants demandent à ce titre ; qu'il y a lieu en revanche, au titre des mêmes dispositions, de mettre à la charge de M.B..., de Mme H..., de M. A..., de Mme C..., de Mme G... et de la société Téléimagerie médicale temps réel de Bourgogne la somme de 500 euros chacun à verser au centre hospitalier intercommunal de Châtillon-sur-Seine - Montbard et la somme de 500 euros chacun à verser à la société Group Emaging Technology ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B...et autres est rejeté.<br/>
<br/>
Article 2 : M.B..., Mme H..., M. A..., Mme C..., Mme G... et la société Téléimagerie médicale temps réel de Bourgogne verseront chacun une somme de 500 euros au centre hospitalier intercommunal de Châtillon-sur-Seine - Montbard et chacun une somme de 500 euros à la société Group Emaging Technology  au titre  des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à M. F...B..., premier requérant dénommé, au centre hospitalier intercommunal de Châtillon-sur-Seine - Montbard et à la société Group Emaging Technology. Les autres requérants seront informés de la présente décision par la SCP Hémery, Thomas-Raquin, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
