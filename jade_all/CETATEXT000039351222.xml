<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039351222</ID>
<ANCIEN_ID>JG_L_2019_11_000000420456</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/35/12/CETATEXT000039351222.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 07/11/2019, 420456, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-11-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420456</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELVOLVE ET TRICHET ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:420456.20191107</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Paris de condamner la Banque de France à lui verser une indemnité d'un montant de 106 699,27 euros, assortie des intérêts au taux légal, correspondant à la différence entre l'indemnité de licenciement prévue par l'articles L. 1234-9 du code du travail et l'indemnité qu'il a perçue à l'occasion de son admission à la retraite le 31 juillet 2009. Par un jugement n° 1603182 du 10 novembre 2016, le tribunal administratif de Paris a rejeté sa demande. Par un arrêt n° 17PA00005 du 8 mars 2018, la cour administrative d'appel de Paris, sur appel de M. A..., a annulé ce jugement et fait droit à sa demande en fixant le départ des intérêts à la date de réception de sa réclamation préalable d'indemnité en date du 2 décembre 2015.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 9 mai et 8 août 2018 et le 25 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, la Banque de France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel formé par M. A... ;<br/>
<br/>
              3°) de mettre à la charge de M. A... la somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code monétaire et financier ;<br/>
              - le code du travail ;<br/>
              - la loi n° 53-611 du 11 juillet 1953 ;<br/>
              - le décret n° 53-711 du 9 août 1953 ;<br/>
              - le décret n° 68-299 du 29 mars 1968 ; <br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delvolvé et Trichet, avocat de la Banque de France et à la SCP Waquet, Farge, Hazan, avocat de M. A... ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 22 octobre 2019, présentée par M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A..., qui a été recruté par la Banque de France le 1er février 1973 et y a exercé, en dernier lieu, les fonctions de directeur de service à la direction générale des activités fiduciaires et de place, a été admis à faire valoir ses droits à la retraite à l'âge de 63 ans à compter du 1er août 2009, par une décision du gouverneur de la Banque de France du 18 août 2009. Il a bénéficié à cette occasion d'une allocation de départ à la retraite d'un montant de 35 485,07 euros, calculé sur le fondement de la décision règlementaire n° 718 du 20 mars 1961 du gouverneur de la Banque de France. <br/>
<br/>
              2. Par une première demande, adressée le 15 juin 2011 à la Banque de France, M. A... a sollicité, d'une part, que le montant de cette indemnité soit porté à celui de l'indemnité de licenciement prévue par l'article L. 1234-9 du code du travail en se prévalant de l'article L. 1237-7 du même code et, d'autre part, la réparation des préjudices subis à raison de l'illégalité de sa mise à la retraite. Par un jugement du 25 avril 2013, le tribunal administratif de Paris a totalement fait droit à la demande indemnitaire sur le premier point et partiellement sur le second. Par un arrêt du 30 juin 2015 devenu définitif faute d'avoir été contesté, la cour administrative d'appel de Paris a fait droit aux conclusions de l'appel principal formé par la Banque de France en tant qu'il portait sur le premier point, au motif que l'indemnité de départ à la retraite de M. A... devait être calculée en application de la décision règlementaire n° 178 du 20 mars 1961, mais non sur le second point, en confirmant que la mise à la retraite de M. A... était illégale. Statuant sur l'appel incident présenté par ce dernier, elle a, en outre, rejeté comme irrecevables faute d'avoir été présentées en première instance et de relever de la même cause juridique que ses autres conclusions, les conclusions tendant à la condamnation de la Banque de France à lui verser une indemnité de licenciement en application des articles L. 1237-8 et L. 1234-9 du code du travail.<br/>
<br/>
              3. Par une seconde demande, adressée le 2 décembre 2015 à la Banque de France, M. A... a sollicité le versement d'une indemnité d'un montant de 106 699,27 euros, assortie des intérêts au taux légal, correspondant à la différence entre l'indemnité de licenciement d'un montant de 142 184,34 euros, résultant de l'application combinée des articles L. 1237-8 et L. 1234-9 du code du travail et l'indemnité perçue lors de l'admission à la retraite. Après rejet implicite de cette demande, il a saisi le tribunal administratif de Paris qui a rejeté sa demande indemnitaire par un jugement du 10 novembre 2016. La Banque de France se pourvoit en cassation contre l'arrêt du 8 mars 2018 par lequel la cour administrative d'appel de Paris, après avoir annulé ce jugement, a fait droit à cette demande.<br/>
<br/>
              4. Aux termes, de l'article L. 142-1 du code monétaire et financier : " La Banque de France est une institution dont le capital appartient à l'Etat ". Aux termes de l'article L. 142-9 du même code : " (...) Le conseil général de la Banque de France détermine, dans les conditions prévues par le troisième alinéa de l'article L. 142-2, les règles applicables aux agents de la Banque de France dans les domaines où les dispositions du code du travail sont incompatibles avec le statut ou avec les missions de service public dont elle est chargée. (...) ". Il résulte de ces dispositions que la Banque de France constitue une personne publique chargée par la loi de missions de service public, qui n'a pas cependant le caractère d'un établissement public, mais revêt une nature particulière et présente des caractéristiques propres. Au nombre de ces caractéristiques figure l'application à son personnel des dispositions du code du travail qui ne sont incompatibles ni avec son statut, ni avec les missions de service public dont elle est chargée.<br/>
<br/>
              5. Aux termes de l'article L. 1237-5 du code du travail : " La mise à la retraite s'entend de la possibilité donnée à l'employeur de rompre le contrat de travail d'un salarié ayant atteint l'âge mentionné au 1° de l'article L. 351-8 du code de la sécurité sociale sous réserve des septièmes à neuvième alinéas (...) ". Aux termes de l'article L. 1237-7 du même code : " La mise à la retraite d'un salarié lui ouvre droit à une indemnité de mise à la retraite au moins égale à l'indemnité de licenciement prévue à l'article L. 1234-9 ". Aux termes de l'article L. 1237-8 du même code : " Si les conditions de mise à la retraite ne sont pas réunies, la rupture du contrat de travail par l'employeur constitue un licenciement. ". Aux termes, enfin, de l'article L. 1234-9 du code : " Le salarié titulaire d'un contrat de travail à durée indéterminée, licencié alors qu'il compte 8 mois d'ancienneté ininterrompus au service du même employeur, a droit, sauf en cas de faute grave, à une indemnité de licenciement. / Les modalités de calcul de cette indemnité sont fonction de la rémunération brute dont le salarié bénéficiait antérieurement à la rupture du contrat de travail. Ce taux et ces modalités sont déterminés par voie réglementaire ".<br/>
<br/>
              6. Enfin, en vertu de l'article 5 de la loi du 11 juillet 1953 portant redressement économique et financier, la fixation des limites d'âge des personnels de la Banque de France relève du pouvoir réglementaire. Le décret du 9 août 1953 relatif au régime de retraite des personnels de l'Etat et des services publics, pris pour l'application de cette loi, a fixé les conditions générales de mise à la retraite des agents de la Banque de France et prévu que les mesures d'adaptation nécessaires seraient prises par des règlements d'administration publique. Aux termes de l'article 1er du décret du 29 mars 1968 portant modification des limites d'âge du personnel de la Banque de France, dans sa rédaction applicable au litige : " Sont arrêtées, pour chaque grade ou emploi, entre soixante ans minimum et soixante-cinq ans maximum, par décision du conseil général de la Banque de France approuvée par le ministre de l'économie et des finances, les limites d'âge des agents de ladite banque recrutés, après la date de publication du présent décret, dans les cadres du personnel titulaire, stagiaire ou auxiliaire, sauf s'ils ont été admis à l'un des concours dont les résultats ont été publiés avant cette date ". Ce texte, comme l'ensemble des dispositions législatives et réglementaires régissant l'institution, est partie intégrante du statut de la Banque de France. En application de ce statut, la Banque de France est tenue de mettre un terme aux fonctions des agents atteints par la limite d'âge. En revanche, l'article L. 1237-5 du code du travail prévoit seulement la faculté pour l'employeur de rompre le contrat de travail d'un salarié ayant atteint l'âge mentionné au 1° de l'article L. 351-8 du code de la sécurité sociale. Dès lors, les dispositions de l'article L. 1237-5 du code du travail sont incompatibles avec le statut de la Banque de France. Il en va de même des dispositions qui précisent les règles applicables à la mise à la retraite, notamment de celles de l'article L. 1237-8, les agents de la Banque de France ayant droit, comme les autres agents publics, lorsqu'ils sont irrégulièrement évincés au regard des règles statutaires qui leur sont applicables en matière de retraite, à la réparation intégrale du préjudice qu'ils ont effectivement subi du fait de la mesure illégalement prise à leur encontre.<br/>
<br/>
              7. Il résulte de ce qui précède qu'en jugeant que les dispositions de l'article L. 1237-8 du code du travail ne sont pas incompatibles avec le statut de la Banque de France et sont applicables à M. A..., la cour administrative d'appel de Paris a commis une erreur de droit. Dès lors, la Banque de France est fondée, sans qu'il soit besoin d'examiner les moyens du pourvoi, à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              9. Il résulte de ce qui a été dit au point 6 que M. A... ne peut utilement invoquer les dispositions de l'article L. 1237-8 du code du travail, qui sont incompatibles avec le statut de la Banque de France, pour demander à celle-ci de lui verser l'indemnité de licenciement prévue par l'article L. 1234-9 du code du travail. Par suite, il n'est pas fondé à se plaindre de ce que, par le jugement attaqué, le tribunal administratif de Paris a rejeté sa demande de versement de cette indemnité de licenciement ainsi que, par voie de conséquence, ses conclusions à fin d'injonction.<br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A... la somme de 1 500 euros à verser à la Banque de France au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font en revanche obstacle à ce qu'une somme soit mise à ce titre à la charge de la Banque de France, qui n'est pas la partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 8 mars 2018 de la cour administrative d'appel de Paris est annulé.<br/>
Article 2 : La requête de M. A... devant la cour administrative d'appel de Paris est rejetée.<br/>
Article 3 : M. A... versera une somme de 1 500 euros à la Banque de France au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par M. A... au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la Banque de France et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
