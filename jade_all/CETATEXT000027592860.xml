<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027592860</ID>
<ANCIEN_ID>JG_L_2013_06_000000361043</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/59/28/CETATEXT000027592860.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  2ème sous-section jugeant seule, 21/06/2013, 361043, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-06-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361043</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 2ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Marc Perrin de Brichambaut</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:361043.20130621</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 13 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par la société Bricorama France, dont le siège est rue du Moulin Paillasson à Roanne (42300), représentée par son président directeur général ; la société Bricorama France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 1262 T-1264 T du 4 avril 2012 par laquelle la Commission nationale d'aménagement commercial a confirmé l'autorisation qui avait été accordée par la commission départementale d'aménagement commercial de la Côte d'Or à la société l'Immobilière européenne des Mousquetaires et à la société Immo Mousquetaires Centre Est de procéder à la création d'un magasin de bricolage, à l'enseigne Bricomarché, d'une surface de vente de 3 500 m2, au sein de la zone d'activité commerciale des Renardières, à Nuits-Saint-Georges (Côte d'Or) ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat et des sociétés l'Immobilière européenne des Mousquetaires et Immo Mousquetaires Centre Est le versement de la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu la loi n° 73-1193 du 27 décembre 1973 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Perrin de Brichambaut, Conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, Rapporteur public,<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant, d'une part, qu'aux termes du troisième alinéa de l'article 1er de la loi du 27 décembre 1973 : " Les pouvoirs publics veillent à ce que l'essor du commerce et de l'artisanat permette l'expansion de toutes les formes d'entreprises, indépendantes, groupées ou intégrées, en évitant qu'une croissance désordonnée des formes nouvelles de distribution ne provoque l'écrasement de la petite entreprise et le gaspillage des équipements commerciaux et ne soit préjudiciable à l'emploi " ; qu'aux termes de l'article L. 750-1 du code de commerce, dans sa rédaction issue de la loi du 4 août 2008 de modernisation de l'économie : " Les implantations, extensions, transferts d'activités existantes et changements de secteur d'activité d'entreprises commerciales et artisanales doivent répondre aux exigences d'aménagement du territoire, de la protection de l'environnement et de la qualité de l'urbanisme. Ils doivent en particulier contribuer au maintien des activités dans les zones rurales et de montagne ainsi qu'au rééquilibrage des agglomérations par le développement des activités en centre-ville et dans les zones de dynamisation urbaine. / Dans le cadre d'une concurrence loyale, ils doivent également contribuer à la modernisation des équipements commerciaux, à leur adaptation à l'évolution des modes de consommation et des techniques de commercialisation, au confort d'achat du consommateur et à l'amélioration des conditions de travail des salariés " ; <br/>
<br/>
              Considérant, d'autre part, qu'aux termes de l'article L. 752-6 du même code, issu de la même loi du 4 août 2008 : " Lorsqu'elle statue sur l'autorisation d'exploitation commerciale visée à l'article L. 752-1, la commission départementale d'aménagement commercial se prononce sur les effets du projet en matière d'aménagement du territoire, de développement durable et de protection des consommateurs. Les critères d'évaluation sont : / 1° En matière d'aménagement du territoire : / a) L'effet sur l'animation de la vie urbaine, rurale et de montagne ; / b) L'effet du projet sur les flux de transport ; / c) Les effets découlant des procédures prévues aux articles L. 303-1 du code de la construction et de l'habitation et L. 123-11 du code de l'urbanisme ; / 2° En matière de développement durable : / a) La qualité environnementale du projet ; / b) Son insertion dans les réseaux de transports collectifs " ; <br/>
<br/>
              Considérant qu'il résulte de ces dispositions combinées que l'autorisation d'aménagement commercial ne peut être refusée que si, eu égard à ses effets, le projet contesté compromet la réalisation des objectifs énoncés par la loi ; qu'il appartient aux commissions d'aménagement commercial, lorsqu'elles statuent sur les dossiers de demande d'autorisation, d'apprécier la conformité du projet à ces objectifs, au vu des critères d'évaluation mentionnés à l'article L. 752-6 du code de commerce ; <br/>
<br/>
              Considérant que, pour statuer sur l'autorisation du projet litigieux, consistant à créer un magasin de bricolage avec jardinerie à Nuits-Saint-Georges d'une surface de 3 500 m², la Commission nationale d'aménagement commercial a relevé que la population de la zone de chalandise définie par le demandeur avait augmenté de 7,48 % depuis 1999, que le projet contribuera à renforcer l'attractivité de la commune et affirmera son rôle d'appui aux communes rurales environnantes, qu'il participera à l'animation de la vie urbaine et rurale en apportant une offre nouvelle de bricolage à destination des particuliers et contribuera au confort d'achat des consommateurs ; qu'en outre, la commission nationale a relevé, d'une part, que la desserte routière du site d'implantation de ce projet est assurée par une route départementale et, d'autre part, que la réalisation de ce projet s'inscrira dans une démarche conforme à la réglementation thermique 2012, avec la mise en oeuvre de dispositifs de réduction des consommations d'énergie et de mesures relatives à la gestion de l'eau et des déchets ;<br/>
<br/>
              Considérant, en premier lieu, que si, eu égard à la nature, à la composition et aux attributions de la Commission nationale d'aménagement commercial, les décisions qu'elle prend doivent être motivées, cette obligation n'implique pas que la commission soit tenue de prendre explicitement parti sur le respect, par le projet qui lui est soumis, de chacun des objectifs et critères d'appréciation fixés par les dispositions législatives applicables ; qu'en l'espèce, la commission nationale a satisfait à cette obligation ; que, par suite, le moyen tiré d'une motivation insuffisante doit être écarté ;<br/>
<br/>
              Considérant, en deuxième lieu, que si la société requérante soutient que la société Immo Mousquetaires Centre Est n'aurait pas justifié, devant la Commission Nationale d'aménagement commercial, de la maitrise foncière du terrain d'assiette de son projet, il ressort des pièces du dossier que la promesse de vente dont disposait la société Immo Mousquetaires Centre Est avait été prorogée et que cette dernière justifiait ainsi d'un titre, au sens de l'article R. 752-7 du code du commerce, lui permettant de présenter un dossier de demande d'aménagement commercial ; <br/>
<br/>
              Considérant, en troisième lieu, qu'il ne ressort en tout état de cause pas des éléments versés au dossier que le projet litigieux aurait été incompatible avec les orientations générales et les objectifs définis par un schéma de cohérence territoriale qui lui serait opposable ; <br/>
<br/>
              Considérant, en quatrième lieu, que si la société requérante soutient que la décision attaquée aurait méconnu les objectifs fixés par le législateur en matière d'aménagement du territoire et de développement durable, il ressort des pièces du dossier que le projet litigieux sera implanté à proximité immédiate d'une zone qui accueille de nombreux commerces que ce projet aura, par lui-même, un faible impact sur le trafic routier ; que la circonstance que la zone d'implantation n'est que peu desservie par des transports en commun n'est pas, à elle seule et s'agissant en particulier d'une implantation qui n'est pas située en zone urbaine, de nature à entacher ce projet d'illégalité ; qu'il ressort des pièces du dossier que le projet s'inscrit dans une démarche d'insertion environnementale en termes de réduction des consommations d'énergie, d'utilisation de l'énergie solaire, de gestion de l'eau et des déchets ; que, dès lors, la Commission nationale d'aménagement commercial n'a pas fait une inexacte application des dispositions précédemment citées en confirmant l'autorisation que la commission départementale avait accordée ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par la société Immobilière européenne des Mousquetaires, que la société Bricorama France n'est pas fondée à demander l'annulation pour excès de pouvoir de la décision attaquée ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ; qu'il y a lieu, en revanche, de mettre à sa charge le versement à la société l'Immobilière européenne des Mousquetaires d'une somme de 3 000 euros au même titre ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de la société Bricorama France est rejetée.<br/>
<br/>
Article 2 : La société Bricorama France versera à la société l'Immobilière européenne des Mousquetaires une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société Bricorama France, à la société l'Immobilière européenne des Mousquetaires, à la société Immo Mousquetaires Centre Est et à la commission nationale d'aménagement commercial. Copie en sera adressé pour information au ministre de l'économie et des finances.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
