<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039080641</ID>
<ANCIEN_ID>JG_L_2002_10_0000235904</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/08/06/CETATEXT000039080641.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 18/10/2002, 235904, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2002-10-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>235904</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Pierre-Alain Jeanneney</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane             Austry             </COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2002:235904.20021018</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée au secrétariat du contentieux du Conseil d'Etat le 11 juillet 2001, présentée par la COMMUNE DE SAINT LAURENT DU MARONI (Guyane), représentée par son maire en exercice, habilité par une délibération du conseil municipal en date du 28 juin 2001 ; la COMMUNE DE SAINT-LAURENT DU MARONI demande au Conseil d'Etat d'annuler pour excès de pouvoir la décision en date du 10 avril 2001 de la commission nationale chargée de régler la situation des personnels et des biens transférés aux services départementaux d'incendie et de secours en tant : <br/>
<br/>
              1°) qu'elle institue divers remboursements dus par la commune au service départemental de secours et d'incendie de la Guyane ; <br/>
<br/>
              2°) qu'elle prévoit l'actualisation de la participation annuelle due par la commune ; <br/>
<br/>
              3°) qu'elle dispose que la mise à disposition des immeubles transférés cessera de plein droit si le service départemental décide leur désaffectation ;<br/>
<br/>
              4°) qu'elle indique que les modalités de financement des travaux de grosses réparations, de construction neuve ou d'agrandissement sont arrêtées par le conseil d'administration du service départemental ;<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu la Constitution, notamment son article 73 ;<br/>
	Vu le code général des collectivités territoriales, notamment ses articles L. 1424-17, L. 1424-19, L. 1424-22, L. 1424-23, L. 1424-35 et L. 1424-36 ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jeanneney, Conseiller d'Etat,  <br/>
<br/>
<br/>
              - les conclusions de M. Austry, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              En ce qui concerne l'ensemble de la décision attaquée :<br/>
<br/>
              Considérant que la loi du 3 mai 1996 relative  aux services d'incendie et de secours, dont sont issues les dispositions du chapitre IV du titre II du livre IV de la première partie du code général des collectivités territoriales relatives aux conditions financières du  transfert aux services départementaux d'incendie et de secours des biens et des personnels des collectivités locales ou des établissements publics antérieurement compétents en matière de service d'incendie et de secours, n'a pas prévu de régime particulier pour les départements d'outre-mer ; que, par suite, la COMMUNE DE SAINT-LAURENT-DU-MARONI ne peut pas faire valoir utilement, à l'encontre de la décision en date du 10 avril 2001 par laquelle la commission nationale chargée de régler la situation des personnels et des biens transférés aux services départementaux d'incendie et de secours a déterminé les modalités du transfert des personnes, des biens meubles et immeubles et des charges des centres d'incendie et de secours au service départemental d'incendie et de secours de la Guyane, que cette décision aurait été prise en méconnaissance de l'article 73 de la Constitution,  en vertu duquel le régime législatif et l'organisation administrative des départements d'outre-mer peuvent faire l'objet de mesures d'adaptation nécessitées par leur situation particulière ; que la commune requérante ne peut davantage reprocher utilement à la décision attaquée, qui a fait application  des règles déterminées par la loi, de méconnaître un principe selon lequel tout transfert de compétences devrait être accompagné du transfert des ressources financières correspondantes ; <br/>
<br/>
              En ce qui concerne les articles 4, 5, 6 et 7 de l'annexe à la décision attaquée :<br/>
<br/>
              Considérant qu'en vertu des articles L. 1424-13, L. 1424-14 et L. 1424-16 du code général des collectivités territoriales les modalités du transfert des sapeurs pompiers et des personnels sont déterminées par une convention signée entre la commune et le service départemental d'incendie et de secours ; que, contrairement à ce que soutient la requérante, la commission nationale a pu, sans méconnaître ces dispositions et sans commettre d'erreur manifeste d'appréciation, décider par les articles 4, 5, 6 et 7 de l'annexe de sa décision, que la commune rembourserait au service départemental de secours et d'incendie de la Guyane diverses charges résultant d'avantages acquis dont bénéficiaient les agents transférés ;<br/>
<br/>
              En ce qui concerne l'article 29 de l'annexe à la décision attaquée :<br/>
<br/>
              Considérant qu'en vertu de l'article L. 1424-35 du code général des collectivités territoriales les modalités de calcul et de répartition des contributions  dues par les communes, les établissements publics de coopération intercommunale compétents pour la gestion des services d'incendie et de secours et le département pour le financement du service départemental d'incendie et de secours sont fixées par le conseil d'administration de celui-ci ; qu'en vertu de l'article L. 1424-22 du même code, la commission nationale, à défaut de signature des conventions prévues par les articles L. 1424-13, L. 1424-14 et L. 1424-17 et fixant les conditions du transfert des personnels et des biens, règle la situation des personnels et des biens transférés au service départemental d'incendie et de secours ; que ces dispositions ne confèrent en revanche à la commission nationale  aucune compétence pour déterminer le montant des contributions annuelles dues aux services départementaux d'incendie et de secours par les communes, les établissements publics de coopération intercommunale et les départements ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin d'examiner les moyens invoqués par la COMMUNE DE SAINT-LAURENT DU MARONI, que la décision en date du 10 avril 2001 de la commission nationale  est entachée d'incompétence en tant qu'elle prévoit, par l'article 29 de son annexe, que la participation financière due par la commune au service départemental d'incendie et de secours de la Guyane sera actualisée annuellement ; que cette décision doit, dans cette mesure, être annulée;<br/>
<br/>
<br/>
              En ce qui concerne les articles 15, 20 et 21 de l'annexe à la décision attaquée : 	<br/>
              Considérant qu'aux termes du premier alinéa de l'article L. 1424-17 du code général des collectivités territoriales : "Les biens affectés, à la date de promulgation de la loi n° 96-369 du 3 mai 1996 relative aux services d'incendie et de secours, par les communes, les établissements publics de coopération intercommunale et le département au fonctionnement des services d'incendie et de secours et nécessaires au fonctionnement du service départemental d'incendie et de secours sont mis, à titre gratuit, à compter de la date fixée par une convention, à la disposition de celui-ci, sous réserve des dispositions de l'article L. 1424-19", selon lesquelles le transfert des biens peut, à toute époque, avoir lieu en pleine propriété ; qu'aux termes du quatrième alinéa du même article : "Lorsque les biens cessent d'être affectés au fonctionnement des services d'incendie et de secours, leur mise à disposition prend fin" ; qu'aux termes du premier alinéa de l'article L. 1424-35 du même code : "Les modalités de calcul et de répartition des contributions des communes, des établissements publics de coopération intercommunale compétents pour la gestion des services d'incendie et de secours et du département au financement du service départemental d'incendie et de secours sont fixées par le conseil d'administration de celui-ci" ; <br/>
<br/>
              Considérant qu'en vertu de l'article 15 de l'annexe de la décision attaquée, la commune demeure propriétaire des biens mis à disposition du service départemental ; qu'en vertu de son article 21 la mise à disposition prend fin si le service départemental décide de désaffecter le bien et qu'en vertu de son article 23 les modalités de financement des travaux de construction neuve et d'agrandissement sont arrêtées par le conseil d'administration du service départemental ; que, contrairement à ce que soutient la COMMUNE DE SAINT LAURENT DU MARONI, la commission nationale, en permettant au conseil d'administration du service départemental de lui faire éventuellement supporter le coût des travaux de grosses réparations, de construction neuve ou d'agrandissement des immeubles transférés, n'a pas méconnu les dispositions législatives précitées ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la COMMUNE DE SAINT-LAURENT DU MARONI est fondée à demander l'annulation de la décision attaquée en tant seulement que, par l'article 29 de son annexe, elle prévoit que le montant de sa participation annuelle au service départemental d'incendie et de secours de la Guyane sera actualisé annuellement ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
                                   D E C I D E :<br/>
                                    --------------<br/>
<br/>
Article 1er : La décision de la commission nationale chargée de régler la situation des personnels et des biens transférés aux services départementaux d'incendie et de secours en date du  10 avril 2001 est annulée en tant qu'elle prévoit, par l'article 29 de son annexe, que le montant de la participation due par la COMMUNE DE SAINT-LAURENT-DU-MARONI au service départemental d'incendie et de secours de la Guyane sera actualisé annuellement.<br/>
<br/>
Article 2 : Le surplus des conclusions de la requête de la COMMUNE DE SAINT-LAURENT DU MARONI est rejeté.<br/>
Article 3 : La présente décision sera notifiée à la COMMUNE DE SAINT-LAURENT-DU-MARONI,  au service départemental de secours et d'incendie de la Guyane et au secrétaire d'Etat à l'Outre-Mer.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
