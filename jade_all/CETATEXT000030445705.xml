<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030445705</ID>
<ANCIEN_ID>JG_L_2015_03_000000382144</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/44/57/CETATEXT000030445705.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère SSR, 27/03/2015, 382144, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-03-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382144</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD - COLIN - STOCLET</AVOCATS>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2015:382144.20150327</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 3 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, présentée pour M. B...A..., demeurant ... ; M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 5 mai 2014 par laquelle le procureur général près la cour d'appel de Saint-Denis de la Réunion a prononcé à son encontre un avertissement ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 11 mars 2015, présentée pour M. A...;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              Vu l'ordonnance n° 58-1270 du 22 décembre 1958 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 44 de l'ordonnance du 22 décembre 1958 portant loi organique relative au statut de la magistrature : " En dehors de toute action disciplinaire, l'inspecteur général des services judiciaires, les premiers présidents, les procureurs généraux et les directeurs ou chefs de service à l'administration centrale ont le pouvoir de donner un avertissement aux magistrats placés sous leur autorité. / L'avertissement est effacé automatiquement du dossier au bout de trois ans si aucun nouvel avertissement ou aucune sanction disciplinaire n'est intervenu pendant cette période " ; que, par la décision attaquée, le procureur général près la cour d'appel de Saint-Denis de La Réunion a, sur le fondement de ces dispositions, prononcé à l'encontre de M.A..., qui était alors procureur de la République près le tribunal de grande instance de Saint-Denis de la Réunion, un avertissement ; <br/>
<br/>
              2. Considérant qu'il ressort des motifs de la décision attaquée que l'avertissement litigieux a été prononcé à l'encontre de M. A...à raison, d'une part, de la " rupture de la relation de confiance avec ses collaborateurs dans un climat de souffrance émaillé de plusieurs incidents " résultant notamment des conditions inappropriées dans lesquelles il avait procédé à la réorganisation du parquet après son arrivée et du niveau élevé de contrôle de l'activité qu'il avait imposé, d'autre part, d'un " déficit dans la remontée d'information au parquet général ", certaines demandes du parquet général n'ayant pas été suivies de réponses ou ayant fait l'objet de réponses insuffisantes, et de diverses " atteintes à la confiance ", tenant au caractère inexact de propos qu'il avait tenus, notamment au cours de l'audience solennelle du 17 février 2014 ; que, toutefois, il ressort, d'une part, des pièces du dossier, notamment du compte-rendu établi par M. A...le 4 septembre 2013 à l'intention du procureur général près la cour d'appel de Saint-Denis de La Réunion sur la procédure d'alerte initiée par certains des magistrats du parquet, que les difficultés relationnelles relevées au sein du parquet du tribunal de grande instance de Saint-Denis de La Réunion et, en particulier, le climat de défiance régnant entre les magistrats sont la conséquence d'un mauvais fonctionnement du service, auquel M. A... a tenté de remédier, et ne sont pas exclusivement imputables à l'intéressé ; que, par ailleurs, à la date à laquelle l'avertissement a été prononcé, une partie de ces difficultés avait été résolue ; qu'il ressort, d'autre part, des pièces du dossier que le déficit dans la remontée d'informations au parquet général est dû, en partie, au retard accumulé pendant la période durant laquelle, avant l'arrivée de M.A..., le poste de procureur général de la République près la cour d'appel de Saint-Denis de La Réunion avait été vacant ainsi qu'à l'absence, pendant plusieurs semaines, de secrétariat ; qu'enfin, le caractère inexact des propos tenus par M.A..., en particulier lors l'audience solennelle du 17 février 2014, ne saurait justifier, à lui seul, l'avertissement prononcé ; qu'ainsi, le procureur général près la cour d'appel de Saint-Denis de La Réunion a fait une inexacte application de l'article 44 de l'ordonnance du 22 décembre 1958 en estimant que les faits reprochés à M. A...justifiaient que soit prononcé à son encontre un avertissement ; <br/>
<br/>
              3. Considérant, par suite et sans qu'il soit besoin d'examiner les autres moyens de la requête et sans qu'il y ait lieu d'ordonner la mesure d'instruction sollicitée, que M. A...est fondé à demander l'annulation de la décision qu'il attaque ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du procureur général près la cour d'appel de Saint-Denis de La Réunion du 5 mai 2014 est annulée. <br/>
<br/>
Article 2 : L'Etat versera à M. A...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 3 : La présente décision sera notifiée à M. B...A..., au Premier ministre et à la garde des sceaux, ministre de la justice. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
