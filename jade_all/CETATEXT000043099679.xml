<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043099679</ID>
<ANCIEN_ID>JG_L_2021_02_000000441593</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/09/96/CETATEXT000043099679.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 04/02/2021, 441593</TITRE>
<DATE_DEC>2021-02-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441593</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GADIOU, CHEVALLIER ; SCP BOULLOCHE ; SCP BOUTET-HOURDEAUX ; SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:441593.20210204</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La communauté de communes du Haut-Jura a demandé au juge des référés du tribunal administratif de Besançon de prescrire une expertise aux fins de se prononcer sur l'origine et l'étendue des désordres affectant la toiture terrasse en forme de dôme de la médiathèque communautaire de Saint-Claude, de déterminer les travaux pour y remédier et de fournir les éléments permettant d'établir les responsabilités encourues. Par une ordonnance n° 1802244 du 11 juin 2019, le juge des référés a fait droit à sa demande.<br/>
<br/>
              La société mutuelle d'assurance du bâtiment et des travaux publics (SMABTP) a demandé au juge du référé d'étendre les opérations d'expertise prescrites par son ordonnance du 11 juin 2019 à d'autres personnes que les parties initialement désignées et de compléter les missions de l'expert. Par une ordonnance n° 1901307 du 24 octobre 2019, rectifiée par une ordonnance du 3 décembre 2019, le juge des référés du tribunal administratif de Besançon a fait partiellement droit à sa demande.<br/>
<br/>
              Par une ordonnance n° 19NC03247 du 26 mai 2020, la présidente de la cour administrative d'appel de Nancy a rejeté la requête d'appel de la SMABTP.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 2 et 16 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, la SMABTP demande au Conseil d'Etat :<br/>
<br/>
<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de prononcer l'extension des opérations d'expertise à la mutuelle des architectes français (MAF) et à la société Axa Iard ;<br/>
<br/>
              3°) de mettre à la charge solidaire des défendeurs la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des assurances ; <br/>
              - le code civil ;<br/>
              - la loi n°2008-661 du 17 juin 2008 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme G... F..., rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Gadiou, Chevallier, avocat de la SMABTP, à la SCP Boutet-Hourdeaux, avocat de la société AXA France Iard et de la société Broissiat Dequerer, à la SCP Thouvenin, Coudray, Grevy, avocat de la compagnie L'Auxiliaire et de la société CVF Structures et à la SCP Boulloche, avocat de la MAF.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que la communauté de communes du Haut-Jura Saint-Claude a fait procéder à compter de l'année 2014 à des travaux en vue de l'aménagement d'une médiathèque. Des désordres étant apparus, elle a saisi, le 18 décembre 2018, le juge des référés du tribunal administratif de Besançon d'une demande d'expertise aux fins de se prononcer sur l'origine et l'étendue des désordres affectant la toiture terrasse en forme de dôme de la médiathèque, de déterminer les travaux à effectuer pour y remédier et de fournir les éléments permettant d'établir les responsabilités encourues. Par une ordonnance du 11 juin 2019, le juge des référés a fait droit à sa demande. La SMABTP, appelée en la cause en qualité d'assureur dommages-ouvrage du maître d'ouvrage, a demandé au juge du référé d'étendre les opérations d'expertise à d'autres personnes que les parties initialement désignées et de compléter les missions de l'expert. Par une ordonnance du 24 octobre 2019, rectifiée par une ordonnance du 3 décembre 2019, le juge des référés du tribunal administratif de Besançon a fait partiellement droit à sa demande mais a rejeté le surplus de ses conclusions, tendant à la mise en cause, d'une part, de la société Axa France Iard, en qualité d'assureur des sociétés ECB, société anonyme de transactions et courtage et Socotec construction et, d'autre part, de la MAF en qualité d'assureur de la société Etamine. La SMABTP se pourvoit en cassation contre l'ordonnance du 26 mai 2020 par laquelle la présidente de la cour administrative d'appel de Nancy a rejeté son appel.<br/>
<br/>
              Sur le cadre juridique :<br/>
<br/>
              2. En premier lieu, aux termes du premier alinéa de l'article R. 532-1 du code de justice administrative : " Le juge des référés peut, sur simple requête et même en l'absence de décision administrative préalable, prescrire toute mesure utile d'expertise ou d'instruction ". Aux termes de l'article R. 532-3 du même code : " Le juge des référés peut, à la demande de l'une des parties formée dans le délai de deux mois qui suit la première réunion d'expertise, ou à la demande de l'expert formée à tout moment, étendre l'expertise à des personnes autres que les parties initialement désignées par l'ordonnance, ou mettre hors de cause une ou plusieurs des parties ainsi désignées. / Il peut, dans les mêmes conditions, étendre la mission de l'expertise à l'examen de questions techniques qui se révélerait indispensable à la bonne exécution de cette mission, ou, à l'inverse, réduire l'étendue de la mission si certaines des recherches envisagées apparaissent inutiles ". Il résulte de ces dispositions que, lorsqu'il est saisi d'une demande d'une partie ou de l'expert tendant à l'extension de la mission de l'expertise à des personnes autres que les parties initialement désignées par l'ordonnance ou à l'examen de questions techniques qui se révélerait indispensable à la bonne exécution de cette mission, le juge des référés ne peut ordonner cette extension qu'à la condition qu'elle présente un caractère utile. Cette utilité doit être appréciée, d'une part, au regard des éléments dont le demandeur dispose ou peut disposer par d'autres moyens et, d'autre part, bien que ce juge ne soit pas saisi du principal, au regard de l'intérêt que la mesure présente dans la perspective d'un litige principal, actuel ou éventuel, auquel elle est susceptible de se rattacher. <br/>
<br/>
              3. En deuxième lieu, aux termes de l'article 2241 du code civil : " La demande en justice, même en référé, interrompt le délai de prescription (...) ". Alors même que l'article 2244 du code civil dans sa rédaction antérieure à la loi du 17 juin 2008 réservait un effet interruptif aux actes "signifiés à celui qu'on veut empêcher de prescrire", termes qui n'ont pas été repris par le législateur aux nouveaux articles 2239 et 2241 de ce code, il ne résulte ni des dispositions de la loi du 17 juin 2008 ni de ses travaux préparatoires que la réforme des règles de prescription résultant de cette loi aurait eu pour effet d'étendre le bénéfice de la suspension ou de l'interruption du délai de prescription à d'autres personnes que le demandeur à l'action. Il en résulte qu'une citation en justice, au fond ou en référé, n'interrompt la prescription qu'à la double condition d'émaner de celui qui a la qualité pour exercer le droit menacé par la prescription et de viser celui-là même qui en bénéficierait. <br/>
<br/>
              4. S'agissant en particulier de la responsabilité décennale des constructeurs, il en résulte que, lorsqu'une demande est dirigée contre un constructeur, la prescription n'est pas interrompue à l'égard de son assureur s'il n'a pas été également cité en justice. Lorsqu'une demande est dirigée contre un assureur au titre de la garantie décennale souscrite par un constructeur, la prescription n'est interrompue qu'à la condition que cette demande précise en quelle qualité il est mis en cause, en mentionnant l'identité du constructeur qu'il assure. A cet égard n'a pas d'effet interruptif de la prescription au profit d'une partie la circonstance que les opérations d'expertise ont déjà été étendues à cet assureur par le juge, d'office ou à la demande d'une autre partie. De son côté, l'assureur du maître de l'ouvrage, susceptible d'être subrogé dans ses droits, bénéficie de l'effet interruptif d'une citation en justice à laquelle le maître d'ouvrage a procédé dans le délai de garantie décennale.<br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              5. Il ressort des pièces du dossier soumis au juge des référés de la cour administrative d'appel de Nancy que la SMABTP lui demandait d'étendre et de rendre communes et opposables les opérations d'expertise prescrites par l'ordonnance du juge des référés du 11 juin 2019 à la MAF, en sa qualité d'assureur de la société Etamine, et à la société AXA France Iard, en sa qualité d'assureur des sociétés ECB, société analyse de transactions et de courtage et Socotec construction. La SMABTP soutenait devant la cour que sa demande tendant à la mise en cause aux opérations d'expertise de ces deux assureurs en ces qualités était utile dans la perspective d'un litige éventuel, dès lors que seule cette demande permettait d'interrompre à son bénéfice le délai de son action contre ces assureurs, son assuré, la communauté de communes du Haut-Jura Saint-Claude, n'ayant pas demandé leur mise en cause, en ces qualités, à l'expertise.<br/>
<br/>
              6. La présidente de la cour administrative d'appel de Nancy a rejeté les conclusions de la SMABTP au motif que, par application des dispositions de l'article 2241 du code civil, la demande en référé présentée par la communauté de communes du Haut-Jura Saint-Claude, maître d'ouvrage public, avait aussi pour effet d'interrompre la prescription au bénéfice de son assureur, la SMABTP, à l'encontre de tous les assureurs des constructeurs. En statuant ainsi, alors que ces assureurs n'avaient pas été mis en cause au titre des constructeurs concernés, la présidente de la cour a commis une erreur de droit. Il suit de là que la SMABTP est fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de l'ordonnance qu'elle attaque.<br/>
<br/>
              7. Dans les circonstances de l'espèce, il y a lieu, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée.<br/>
<br/>
              Sur la mise en cause de la société Axa France Iard :<br/>
<br/>
              8. Il résulte de l'instruction que, dans son ordonnance du 11 juin 2019, le juge des référés du tribunal administratif de Besançon a étendu les opérations d'expertise, à la demande de l'expert qu'il avait désigné, à la société Axa France Iard. Toutefois, les conclusions de la SMABTP, assureur de la communauté de communes, tendant à ce que les opérations d'expertise soient déclarées communes et opposables à la société Axa France Iard en qualité d'assureur de la société ECB, de la société anonyme de transactions et courtage et de la société Socotec construction conservent une utilité, dans la perspective d'un litige éventuel, dès lors que la commune n'avait pas demandé la mise en cause de la société Axa France Iard en sa qualité d'assureur de ces trois constructeurs, si bien que, ainsi qu'il a été dit au point 4, la prescription n'était pas interrompue au bénéfice de la SMABTP, la circonstance que la société Axa France Iard avait été spontanément mise en cause à l'expertise par le tribunal étant sans incidence.<br/>
<br/>
<br/>
<br/>
              Sur la mise en cause de la MAF :<br/>
<br/>
              9. Aux termes de l'annexe I de l'article A 243-1 du code des assurances : " Le contrat couvre, pour la durée de la responsabilité pesant sur l'assuré en vertu des articles 1792 et 2270 du code civil, les travaux ayant fait l'objet d'une ouverture de chantier pendant la période de validité fixée aux conditions particulières ". Il résulte de ces dispositions que l'assurance couvre les travaux confiés à l'assuré qui ont effectivement commencé pendant sa période de validité.<br/>
<br/>
              10. Il résulte de l'instruction que la société Etamine a résilié le contrat d'assurance qu'elle avait conclu avec la MAF à compter du 31 décembre 2013 et que les travaux litigieux n'ont effectivement commencé qu'en 2014. Par suite, alors même que l'acte d'engagement de la société Etamine date de 2011, la responsabilité de la MAF n'est pas susceptible d'être engagée pour des désordres consécutifs aux travaux auxquels cette société a participé. L'extension de la mission de l'expertise à la MAF, en sa qualité d'assureur de la société Etamine, n'est donc pas utile dans la perspective d'un litige auquel elle est susceptible de se rattacher.<br/>
<br/>
              11. Il résulte de ce qui précède que la société SMABTP n'est pas fondée à se plaindre de ce que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Besançon n'a pas fait droit à sa demande d'extension de l'expertise en ce qui concerne la MAF. Elle est en revanche fondée à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés a refusé d'étendre, à sa demande, à la société Axa France Iard, en sa qualité d'assureur de la société ECB, de la société anonyme de transactions et courtage et de la société Socotec construction, les opérations d'expertise prescrites par l'ordonnance du 11 juin 2019. <br/>
<br/>
              Sur les frais du litige :<br/>
<br/>
              12. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que la SMABTP, qui n'est pas la partie perdante dans le litige l'opposant à la société Axa France Iard, lui verse la somme qu'elle demande. Il y a lieu, dans les circonstances de l'espèce, d'une part, de mettre à la charge de la société Axa France Iard la somme de 1 500 euros à verser à la SMABTP au même titre et, d'autre part, de mettre à la charge de la SMABTP la somme de 1 500 euros à verser à la MAF. Il n'y a en revanche pas lieu, dans les circonstances de l'espèce, de mettre à la charge de la communauté de communes du Haut-Jura Saint-Claude la somme que demande la SMABTP.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 26 mai 2020 de la présidente de la cour administrative d'appel de Nancy est annulée.<br/>
Article 2 : L'ordonnance du 24 octobre 2019 du juge des référés du tribunal administratif de Besançon est annulée en tant qu'elle rejette les conclusions de la SMABTP tendant à la mise en cause à l'expertise de la société Axa France Iard.<br/>
Article 3 : Les opérations d'expertise sont étendues, à la demande de la SMABTP, à la société Axa France Iard en sa qualité d'assureur de la société ECB, de la société anonyme de transactions et courtage et de la société Socotec construction.<br/>
Article 4 : La société Axa France Iard versera la somme de 1 500 euros à la SMABTP au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 5 : La SMABTP versera la somme de 1 500 euros à la MAF au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 6 : Le surplus des conclusions de la SMABTP et les conclusions des autres parties au titre de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
Article 7 : La présente décision sera notifiée à la société mutuelle d'assurance du bâtiment et des travaux publics (SMABTP), à la Mutuelle des architectes français (MAF), à la société Axa France Iard, à la compagnie L'Auxiliaire et à la société CVF Structures.<br/>
Copie en sera adressée à la communauté de communes du Haut-Jura Saint-Claude, à la société Eda éclairement désenfumage aération, à la société Souchier Boullet, à Me A... I..., mandataire liquidateur de la société ECB, à la société anonyme de transactions et courtage, au cabinet Architecture Patrick Mauger, à M. B... J..., à la société Etamine, à la société Broissiat Dequerer, à la société Synacoustique, à la société Socotec construction, à Me D... E..., mandataire liquidateur de la société Puget, à la société Pateu et Robert, à la société Bejean, à la société SMA et à M. H... C..., expert.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-06-01-04-02-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. RAPPORTS ENTRE L'ARCHITECTE, L'ENTREPRENEUR ET LE MAÎTRE DE L'OUVRAGE. RESPONSABILITÉ DES CONSTRUCTEURS À L'ÉGARD DU MAÎTRE DE L'OUVRAGE. RESPONSABILITÉ DÉCENNALE. DÉLAI DE MISE EN JEU. INTERRUPTION DU DÉLAI. - CITATION EN JUSTICE - 1) CONDITIONS DE L'INTERRUPTION [RJ1] - 2) DEMANDE DIRIGÉE CONTRE UN CONSTRUCTEUR - PRESCRIPTION INTERROMPUE À L'ÉGARD DE SON ASSUREUR - ABSENCE - 3) DEMANDE DIRIGÉE CONTRE UN ASSUREUR - A) PRESCRIPTION INTERROMPUE À SON ÉGARD - EXISTENCE, À CONDITION QUE LA DEMANDE MENTIONNE L'IDENTITÉ DU CONSTRUCTEUR - B) CIRCONSTANCE QUE LES OPÉRATIONS D'EXPERTISE ONT DÉJÀ ÉTÉ ÉTENDUES À CET ASSUREUR PAR LE JUGE - CIRCONSTANCE SANS INCIDENCE [RJ2] - 4) DEMANDE FORMULÉE PAR LE MAÎTRE D'OUVRAGE - PRESCRIPTION INTERROMPUE AU BÉNÉFICE DE SON ASSUREUR - EXISTENCE.
</SCT>
<ANA ID="9A"> 39-06-01-04-02-02 Alors même que l'article 2244 du code civil dans sa rédaction antérieure à la loi n° 2008-561 du 17 juin 2008 réservait un effet interruptif aux actes signifiés à celui qu'on veut empêcher de prescrire, termes qui n'ont pas été repris par le législateur aux nouveaux articles 2239 et 2241 de ce code, il ne résulte ni de la loi du 17 juin 2008 ni de ses travaux préparatoires que la réforme des règles de prescription résultant de cette loi aurait eu pour effet d'étendre le bénéfice de la suspension ou de l'interruption du délai de prescription à d'autres personnes que le demandeur à l'action.,,,1) Il en résulte qu'une citation en justice, au fond ou en référé, n'interrompt la prescription qu'à la double condition d'émaner de celui qui a la qualité pour exercer le droit menacé par la prescription et de viser celui-là même qui en bénéficierait.... ,,2) S'agissant en particulier de la responsabilité décennale des constructeurs, il en résulte que, lorsqu'une demande est dirigée contre un constructeur, la prescription n'est pas interrompue à l'égard de son assureur s'il n'a pas été également cité en justice.... ,,3) a) Lorsqu'une demande est dirigée contre un assureur au titre de la garantie décennale souscrite par un constructeur, la prescription n'est interrompue qu'à la condition que cette demande précise en quelle qualité il est mis en cause, en mentionnant l'identité du constructeur qu'il assure.... ,,b) A cet égard n'a pas d'effet interruptif de la prescription au profit d'une partie la circonstance que les opérations d'expertise ont déjà été étendues à cet assureur par le juge, d'office ou à la demande d'une autre partie.... ,,4) De son côté, l'assureur du maître de l'ouvrage, susceptible d'être subrogé dans ses droits, bénéficie de l'effet interruptif d'une citation en justice à laquelle le maître d'ouvrage a procédé dans le délai de garantie décennale.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., dans l'état du droit antérieur à la loi du 17 juin 2008, CE, 7 octobre 2009, Société atelier des maîtres d'oeuvre Atmo et compagnie les souscripteurs du Lloyd's de Londres, n° 308163, T. p. 837 ; CE, 19 avril 2017, Communauté urbaine de Dunkerque, 395328, T. p. 680 ; depuis l'entrée en vigueur de cette loi, CE, 20 novembre 2020, Société Véolia Eau - Compagnie générale des Eaux, n° 432678, à mentionner aux Tables.,,[RJ2] Ab. jur. CE, 14 mai 2003, Société Thales engineering et consulting, n° 250585, T. p. 908.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
