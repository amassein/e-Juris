<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035831290</ID>
<ANCIEN_ID>JG_L_2017_10_000000410193</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/83/12/CETATEXT000035831290.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 18/10/2017, 410193</TITRE>
<DATE_DEC>2017-10-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410193</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sandrine Vérité</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:410193.20171018</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. J...C...a demandé au tribunal administratif de Nantes, qui a transmis sa protestation au tribunal administratif de Caen, d'annuler les opérations électorales qui se sont déroulées le 30 janvier 2017 en vue de l'élection des conseillers communautaires de la commune de Villeneuve-en-Perseigne (Sarthe) à la communauté urbaine d'Alençon, à l'issue desquelles ont été proclamés élus, outre lui-même, M. A...H..., Mme G...K..., M. F...I..., Mme J...B...et M. D...E.... Par un jugement n° 1700250 du 4 avril 2017, le tribunal administratif de Caen a annulé ces élections.<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 28 avril et 24 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, M.H..., MmeK..., M. I..., Mme B...et M. E...demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de rejeter la protestation de M.C... ;<br/>
<br/>
              3°) de mettre à la charge de M. C...la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 2016-1500 du 8 novembre 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sandrine Vérité, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par un arrêté du 22 septembre 2014, prenant effet le 1er janvier 2015, le préfet de la Sarthe a créé la commune nouvelle de Villeneuve-en-Perseigne, constituée des six communes de La Fresnaye-sur-Chédouet, Chassé, Lignières-la-Carelle, Montigny, Roullée et Saint-Rigomer-des-Bois, emportant la suppression de la communauté de communes du Massif-de-Perseigne composée de ces mêmes communes. Par un arrêté du 12 octobre 2016, les préfets de l'Orne et de la Sarthe ont étendu le périmètre de la communauté urbaine d'Alençon à la commune de Villeneuve-en-Perseigne à compter du 1er janvier 2017, puis, par un arrêté du 16 décembre 2016, ils ont fixé le nombre et la répartition des sièges au sein du conseil communautaire de cette communauté urbaine, en attribuant six sièges à la commune nouvelle. Le 30 janvier 2017, le conseil municipal de Villeneuve-en-Perseigne a élu conseillers communautaires, d'une part, cinq des six candidats de la liste conduite par M. H...et, d'autre part, M.C.... M. H...et ses colistiers relèvent appel du jugement du 4 avril 2017 par lequel le tribunal administratif de Caen, accueillant la protestation de M.C..., a annulé ces opérations électorales.<br/>
<br/>
              2. Aux termes du II de l'article L. 5211-6-1 du code général des collectivités territoriales : " Dans les métropoles et les communautés urbaines (...) la composition de l'organe délibérant est établie par les III à VI selon les principes suivants : / 1° L'attribution des sièges à la représentation proportionnelle à la plus forte moyenne aux communes membres de l'établissement public de coopération intercommunale, en fonction du tableau fixé au III, garantit une représentation essentiellement démographique ; /  2° L'attribution d'un siège à chaque commune membre de l'établissement public de coopération intercommunale assure la représentation de l'ensemble des communes ". Aux termes de l'article L. 5211-6-2 du même code, dans sa rédaction applicable au litige : " Par dérogation aux articles L. 5211-6 et L. 5211-6-1, entre deux renouvellements généraux des conseils municipaux : / 1° En cas de création d'un établissement public de coopération intercommunale à fiscalité propre, (...) d'extension du périmètre d'un tel établissement par l'intégration d'une ou de plusieurs communes (...), il est procédé à la détermination du nombre et à la répartition des sièges de conseiller communautaire dans les conditions prévues à l'article L. 5211-6-1. (...) Dans les communes dont le conseil municipal est élu selon les modalités prévues au chapitre III du titre IV dudit livre Ier : / (...) c) Si le nombre de sièges attribués à la commune est inférieur au nombre de conseillers communautaires élus à l'occasion du précédent renouvellement général du conseil municipal, les membres du nouvel organe délibérant sont élus par le conseil municipal parmi les conseillers communautaires sortants au scrutin de liste à un tour, sans adjonction ni suppression de noms et sans modification de l'ordre de présentation. La répartition des sièges entre les listes est opérée à la représentation proportionnelle à la plus forte moyenne. (...) / 1° bis En cas de fusion d'établissements publics de coopération intercommunale à fiscalité propre ou d'extension du périmètre d'un établissement public de coopération intercommunale à fiscalité propre, lorsque le périmètre issu de la fusion ou de l'extension de périmètre comprend une commune nouvelle qui a été créée après le dernier renouvellement général des conseils municipaux et que le nombre de sièges de conseillers communautaires qui lui sont attribués en application de l'article L. 5211-6-1 est inférieur au nombre des anciennes communes qui ont constitué la commune nouvelle, il est procédé, jusqu'au prochain renouvellement du conseil municipal, à l'attribution au bénéfice de la commune nouvelle d'un nombre de sièges supplémentaires lui permettant d'assurer la représentation de chacune des anciennes communes. (...) Les conseillers communautaires représentant la commune nouvelle sont désignés dans les conditions prévues au 1° du présent article ".  <br/>
<br/>
              3. Il résulte de l'instruction que la commune de Villeneuve-en-Perseigne, commune nouvelle créée après le dernier renouvellement général des conseils municipaux, s'est d'abord vu attribuer, en application de l'article L. 5211-6-1 du code général des collectivités territoriales, compte tenu de sa population, un nombre de deux sièges au conseil communautaire de la communauté urbaine d'Alençon, inférieur au nombre des anciennes communes l'ayant constituée. Ainsi, contrairement à ce que soutiennent les requérants, c'est à bon droit que le tribunal administratif de Caen a jugé que les dispositions du 1° bis de l'article L. 5211-6-2 du même code étaient applicables à la détermination du nombre de sièges à attribuer à cette commune et que les préfets de l'Orne et de la Sarthe, par leur arrêté du 16 décembre 2016, en avaient fait application pour lui attribuer, comme ils le devaient, quatre sièges supplémentaires.<br/>
<br/>
              4. Cependant, si, par les dispositions du 1° bis qu'elle a insérées à l'article L. 5211-6-2 du code général des collectivités territoriales, la loi du 8 novembre 2016 tendant à permettre le maintien des communes associées, sous forme de communes déléguées, en cas de création d'une commune nouvelle, a entendu favoriser la représentation de chacune des anciennes communes, en prévoyant l'attribution, au profit de la commune nouvelle, d'un nombre de sièges au moins égal à celui de ces anciennes communes, elle n'a prévu aucune règle relative à la constitution des listes ou au mode de scrutin imposant que chacune des anciennes communes soit représentée par un conseiller communautaire au sein de l'établissement public de coopération intercommunale. En particulier, aucune disposition n'interdit qu'une liste de candidats comporte plus d'un représentant d'une même ancienne commune. Par suite, c'est à tort que le tribunal administratif de Caen s'est fondé sur la circonstance que la liste conduite par M. H... comportait plusieurs représentants d'une même ancienne commune pour annuler les opérations électorales qui se sont déroulées le 30 janvier 2017. <br/>
<br/>
              5. Il appartient toutefois au Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner l'autre grief soulevé par M. C...devant le tribunal administratif.<br/>
<br/>
              6. Si M. C...soutient que le procès-verbal de l'élection des conseillers communautaires ne comporte pas de cachet ni de mention de l'heure, n'a pas été signé par le doyen de séance et ne fait pas mention de sa protestation, il ne conteste pas que les résultats inscrits à ce procès-verbal sont conformes au décompte des suffrages opéré lors du dépouillement.<br/>
<br/>
              7. Il résulte de ce qui précède que les requérants sont fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Caen a annulé l'élection des conseillers communautaires de la commune de Villeneuve-en-Perseigne à la communauté urbaine d'Alençon.  <br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge des requérants, qui ne sont pas les parties perdantes dans la présente instance. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. C...la somme demandée par les requérants au titre des mêmes dispositions. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Caen du 4 avril 2017 est annulé.<br/>
Article 2 : L'élection de M. A...H..., Mme G...K..., M. F...I..., Mme J...B..., M. D...E...et M. J...C...en qualité de conseillers communautaires de la commune de Villeneuve-en-Perseigne à la communauté urbaine d'Alençon est validée. <br/>
Article 3 : Les conclusions présentées par les parties au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à M. A...H..., premier dénommé, pour l'ensemble des requérants, et à M. J...C.... <br/>
Copie en sera adressée au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-05-01-01 COLLECTIVITÉS TERRITORIALES. COOPÉRATION. ÉTABLISSEMENTS PUBLICS DE COOPÉRATION INTERCOMMUNALE - QUESTIONS GÉNÉRALES. DISPOSITIONS GÉNÉRALES ET QUESTIONS COMMUNES. - ELECTIONS AU CONSEIL COMMUNAUTAIRE D'UN EPCI - MODALITÉS D'ATTRIBUTION DES SIÈGES EN CAS D'ÉLARGISSEMENT DU PÉRIMÈTRE DE L'EPCI ENTRE DEUX RENOUVELLEMENTS GÉNÉRAUX DES CONSEILS MUNICIPAUX (ART. L. 5211-6-2, 1° DU CGCT) - CAS DE L'INTÉGRATION À UN EPCI D'UNE COMMUNE NOUVELLE CRÉÉE APRÈS LE DERNIER RENOUVELLEMENT DES CONSEILS MUNICIPAUX - OBLIGATION DE PRÉVOIR UN NOMBRE DE SIÈGES AU MOINS ÉGAL À CELUI DES ANCIENNES COMMUNES - EXISTENCE - OBLIGATION TENANT À CE QUE LA CONSTITUTION DES LISTES OU LE MODE DE SCRUTIN PERMETTE LA REPRÉSENTATION DE CHACUNE DES ANCIENNES COMMUNES PAR UN CONSEILLER COMMUNAUTAIRE - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">28 ÉLECTIONS ET RÉFÉRENDUM. - ELECTIONS AU CONSEIL COMMUNAUTAIRE D'UN EPCI - MODALITÉS D'ATTRIBUTION DES SIÈGES EN CAS D'ÉLARGISSEMENT DU PÉRIMÈTRE DE L'EPCI ENTRE DEUX RENOUVELLEMENTS GÉNÉRAUX DES CONSEILS MUNICIPAUX (ART. L. 5211-6-2, 1° DU CGCT) - CAS DE L'INTÉGRATION À UN EPCI D'UNE COMMUNE NOUVELLE CRÉÉE APRÈS LE DERNIER RENOUVELLEMENT DES CONSEILS MUNICIPAUX - OBLIGATION DE PRÉVOIR UN NOMBRE DE SIÈGES AU MOINS ÉGAL À CELUI DES ANCIENNES COMMUNES - EXISTENCE - OBLIGATION TENANT À CE QUE LA CONSTITUTION DES LISTES OU LE MODE DE SCRUTIN PERMETTE LA REPRÉSENTATION DE CHACUNE DES ANCIENNES COMMUNES PAR UN CONSEILLER COMMUNAUTAIRE - ABSENCE.
</SCT>
<ANA ID="9A"> 135-05-01-01 Si, par le 1° bis qu'elle a inséré à l'article L. 5211-6-2 du code général des collectivités territoriales (CGCT), la loi n° 2016-1500 du 8 novembre 2016 tendant à permettre le maintien des communes associées, sous forme de communes déléguées, en cas de création d'une commune nouvelle, a entendu favoriser la représentation de chacune des anciennes communes, en prévoyant l'attribution, au profit de la commune nouvelle, d'un nombre de sièges au moins égal à celui de ces anciennes communes, elle n'a prévu aucune règle relative à la constitution des listes ou au mode de scrutin imposant que chacune des anciennes communes soit représentée par un conseiller communautaire au sein de l'établissement public de coopération intercommunale. En particulier, aucune disposition n'interdit qu'une liste de candidats comporte plus d'un représentant d'une même ancienne commune.</ANA>
<ANA ID="9B"> 28 Si, par le 1° bis qu'elle a inséré à l'article L. 5211-6-2 du code général des collectivités territoriales (CGCT), la loi n° 2016-1500 du 8 novembre 2016 tendant à permettre le maintien des communes associées, sous forme de communes déléguées, en cas de création d'une commune nouvelle, a entendu favoriser la représentation de chacune des anciennes communes, en prévoyant l'attribution, au profit de la commune nouvelle, d'un nombre de sièges au moins égal à celui de ces anciennes communes, elle n'a prévu aucune règle relative à la constitution des listes ou au mode de scrutin imposant que chacune des anciennes communes soit représentée par un conseiller communautaire au sein de l'établissement public de coopération intercommunale. En particulier, aucune disposition n'interdit qu'une liste de candidats comporte plus d'un représentant d'une même ancienne commune.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
