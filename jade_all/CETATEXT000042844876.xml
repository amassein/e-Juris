<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042844876</ID>
<ANCIEN_ID>JG_L_2020_12_000000432421</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/84/48/CETATEXT000042844876.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 30/12/2020, 432421, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432421</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH ; SCP MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>M. Thibaut Félix</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:432421.20201230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société à responsabilité limitée Le Klubbing a demandé au tribunal administratif de Nice d'annuler pour excès de pouvoir l'arrêté du 1er juillet 2014 par lequel le maire de Villeneuve-Loubet a refusé de lui délivrer le permis de construire une terrasse démontable de 78,18 mètres carrés sur le domaine public maritime. Par un jugement n° 1403709 du 28 septembre 2017, le tribunal administratif de Nice a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 17MA04675 du 9 mai 2019, la cour administrative d'appel de Marseille a, sur l'appel de la société Le Klubbing, annulé ce jugement ainsi que l'arrêté du maire de Villeneuve-Loubet du 1er juillet 2014.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 9 juillet et 9 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, la commune de Villeneuve-Loubet demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société Le Klubbing ;<br/>
<br/>
              3°) de mettre à la charge de la société Le Klubbing la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thibaut Félix, auditeur,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de la commune de Villeneuve-Loubet, et à la SCP Marlange, de la Burgade, avocat de la SAR Le Klubbing ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Le Klubbing, qui exploite un restaurant à Villeneuve-Loubet, a déposé une demande de permis de construire en vue de la réalisation d'une terrasse démontable sur le domaine public maritime. Par un arrêté du 1er juillet 2014, le maire de Villeneuve-Loubet a refusé le permis de construire sollicité au motif, notamment, que cette terrasse prenait appui sur une construction non autorisée et que le projet ne pouvait ainsi être autorisé sans que la construction illégale soit elle-même régularisée. Par un jugement du 28 septembre 2017, le tribunal administratif de Nice a rejeté la demande de la société Le Klubbing tendant à l'annulation pour excès de pouvoir de l'arrêté du 1er juillet 2014. La commune de Villeneuve-Loubet se pourvoit en cassation contre l'arrêt du 9 mai 2019 par lequel la cour administrative d'appel de Marseille a, sur l'appel de la société Le Klubbing, annulé le jugement du tribunal administratif de Nice et l'arrêté du 1er juillet 2014.<br/>
<br/>
              2. Lorsqu'une construction a été édifiée sans autorisation en méconnaissance des prescriptions légales alors applicables, il appartient au propriétaire qui envisage d'y faire de nouveaux travaux de présenter une demande d'autorisation d'urbanisme portant sur l'ensemble du bâtiment. De même, lorsqu'une construction a été édifiée sans respecter la déclaration préalable déposée ou le permis de construire obtenu ou a fait l'objet de transformations sans les autorisations d'urbanisme requises, il appartient au propriétaire qui envisage d'y faire de nouveaux travaux de présenter une demande d'autorisation d'urbanisme portant sur l'ensemble des éléments de la construction qui ont eu ou auront pour effet de modifier le bâtiment tel qu'il avait été initialement approuvé. Dans l'hypothèse où l'autorité administrative est saisie d'une demande qui ne satisfait pas à cette exigence, elle est tenue de la rejeter et d'inviter son auteur à présenter une demande portant sur l'ensemble des éléments qui doivent être soumis à son autorisation. Cette invitation, qui a pour seul objet d'informer le pétitionnaire de la procédure à suivre s'il entend poursuivre son projet, n'a pas à précéder le refus que l'administration a compétence liée pour opposer à une demande portant sur les seuls nouveaux travaux envisagés.  <br/>
<br/>
              3. Il résulte de ce qui précède que la cour administrative d'appel a entaché son arrêt d'erreur de droit en se fondant, pour annuler l'arrêté par lequel le maire de Villeneuve-Loubet avait refusé de délivrer le permis de construire sollicité au motif que la terrasse objet de la demande prenait appui sur une construction réalisée sans l'autorisation requise et que la demande ne portait pas sur l'ensemble du bâtiment, sur la circonstance que l'administration aurait privé la société pétitionnaire d'une garantie en ne l'invitant pas à présenter une demande de permis de construire portant sur l'ensemble du bâtiment. La commune de Villeneuve-Loubet est, par suite, fondée à demander l'annulation pour ce motif de l'arrêt qu'elle attaque, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              5. Il ressort des pièces du dossier que la demande de permis de construire déposée par la société Le Klubbing avait pour objet la construction d'une terrasse longeant le bâtiment principal qu'elle exploite sur le domaine public maritime. Il est constant que ce bâtiment ne correspond pas aux prescriptions de la décision du 29 mai 1987 par laquelle le maire de Villeneuve-Loubet n'a autorisé " à titre précaire pendant la période estivale et renouvelable tous les ans sur demande explicite du pétitionnaire " que l'installation d'un établissement de plage, comprenant une construction modulaire, un muret de délimitation en béton et une couverture sur dalle, et a ainsi été érigé sans permis de construire. Il s'ensuit que toute nouvelle demande d'autorisation de construction portant sur ce bâtiment doit non seulement porter sur la nouvelle construction mais aussi sur la construction érigée irrégulièrement. <br/>
<br/>
              6. Contrairement à ce que soutient la société Le Klubbing, tout d'abord, il en va ainsi même dans le cas où les éléments de construction résultant des nouveaux travaux envisagés ne prennent pas directement appui sur une partie de l'édifice réalisée sans autorisation. Ensuite, s'il appartient à l'administration de tenir compte, le cas échéant, de l'application des dispositions de l'article L. 111-12 du code de l'urbanisme alors applicables, désormais reprises à l'article L. 421-9 de ce code, emportant, sous certaines réserves relatives notamment aux constructions situées sur le domaine public, régularisation des travaux réalisés depuis plus de dix ans, c'est, en tout état de cause, à la condition qu'elle soit saisie d'une demande d'autorisation d'urbanisme portant sur l'ensemble du bâtiment ou des éléments de celui-ci qui n'ont pas déjà été autorisés. Enfin, il résulte de ce qui a été dit au point 2 que s'il appartient à l'administration d'inviter le pétitionnaire à présenter une demande portant sur l'ensemble des éléments qui doivent être soumis à son autorisation, cette invitation n'a pas à précéder le refus qu'elle a compétence liée pour opposer à une demande portant sur les seuls nouveaux travaux envisagés. <br/>
<br/>
              7. Dès lors, constatant que la construction sur laquelle devait s'adosser la terrasse démontable était irrégulière et que la demande de permis présentée ne portait que sur la nouvelle construction, le maire de Villeneuve-Loubet était tenu, ainsi qu'il l'a fait par son arrêté du 1er juillet 2014, de refuser de délivrer le permis de construire sollicité. Par suite, les moyens soulevés par la société Le Klubbing contre cette décision sont inopérants et la société n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Nice a rejeté sa demande. <br/>
<br/>
              8. L'appel formé par la société Le Klubbing doit ainsi être rejeté, sans qu'il soit besoin de se prononcer sur les fins de non-recevoir présentées par la commune de Villeneuve-Loubet. <br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Villeneuve-Loubet qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Le Klubbing la somme que la commune de Villeneuve-Loubet demande au même titre.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 9 mai 2019 est annulé. <br/>
Article 2 : La requête présentée par la société Le Klubbing devant la cour administrative d'appel de Marseille est rejeté. <br/>
Article 3 : Les conclusions des parties présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la commune de Villeneuve-Loubet et à la société à responsabilité limitée Le Klubbing.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
