<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039168444</ID>
<ANCIEN_ID>JG_L_2019_10_000000432036</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/16/84/CETATEXT000039168444.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 02/10/2019, 432036, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432036</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Cécile Renault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:432036.20191002</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. D... B... a demandé au juge des référés du tribunal administratif de Paris, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de la décision implicite par laquelle la ministre des armées a refusé de lui accorder la protection fonctionnelle, et de lui enjoindre, d'une part, de lui délivrer un visa ainsi qu'à son épouse et à ses enfants, d'autre part, de mettre en oeuvre les mesures propres à assurer sa sécurité et celle de sa famille dans l'attente de cette délivrance ou de prendre en charge matériellement le voyage de la famille au Pakistan ou en France, en assurant sa sécurité et son accueil.<br/>
<br/>
              Par une ordonnance n° 1911277 du 12 juin 2019, le juge des référés du tribunal administratif de Paris a suspendu l'exécution de la décision en litige et enjoint, d'une part, à l'Etat de délivrer à M. B..., à son épouse et ses enfants, des visas dans un délai d'un mois à compter de la notification de son ordonnance et, d'autre part, à la ministre des armées de prendre toute mesure de nature à assurer la sécurité de l'intéressé et de sa famille jusqu'à ce que des visas leur soient effectivement délivrés.<br/>
<br/>
              1° Sous le n° 432036, par un pourvoi, enregistré le 27 juin 2019 au secrétariat du contentieux du Conseil d'Etat, la ministre des armées demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de M. B....<br/>
<br/>
              2° Sous le n° 432037, par une requête, enregistrée le 27 juin 2019 au secrétariat du contentieux du Conseil d'Etat, la ministre des armées demande au Conseil d'Etat d'ordonner le sursis à exécution de l'ordonnance du juge des référés du tribunal administratif de Paris du 12 juin 2019 jusqu'à ce qu'il ait été statué sur son pourvoi en cassation.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... C..., auditrice,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public. <br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de M. B.... <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le pourvoi et la requête de la ministre des armées sont dirigés contre la même ordonnance. Il y a lieu de les joindre pour statuer par une seule décision. <br/>
<br/>
              2. Il ressort des pièces du dossier soumis au juge des référés que M. B..., ressortissant afghan, a exercé, entre avril 2011 et novembre 2012, les fonctions d'agent d'entretien au sein de la " Task force Lafayette " installée au camp de Mahmood e Raqi, en Afghanistan. Les autorités françaises ont annoncé au mois de mai 2012 le retrait des forces françaises de ce pays à partir du mois de juillet. M. B..., qui vit à Kaboul avec son épouse et ses enfants mineurs, a sollicité en février 2018, de la ministre des armées qu'elle lui accorde, dans le cadre d'un dispositif de réexamen des demandes de relocalisation des personnels civils à recrutement local, le bénéfice de la protection fonctionnelle pour lui-même et sa famille compte tenu des menaces dont ils feraient l'objet en Afghanistan en raison de sa collaboration avec les forces armées françaises. Cette demande a fait l'objet d'une décision implicite de rejet dont M. B... a demandé au juge des référés du tribunal administratif de Paris, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension assortie d'une injonction tendant à l'octroi d'un visa pour lui-même et sa famille et, dans l'attente de cette délivrance, diverses mesures de mise en sécurité de lui-même et de sa famille au Pakistan ou en France. La ministre des armées se pourvoit en cassation contre l'ordonnance du 12 juin 2019 par laquelle le juge des référés de ce tribunal a suspendu sa décision et, d'une part, a enjoint à l'Etat de délivrer les visas sollicités et, d'autre part, lui a enjoint de prendre toute mesure de nature à assurer la sécurité de l'intéressé et de sa famille jusqu'à la délivrance effective de ces visas. Elle demande en outre qu'il soit sursis à l'exécution de cette ordonnance sur le fondement de l'article R. 821-5 du code de justice administrative.<br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              3. L'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre.<br/>
<br/>
              4. Il ressort des énonciations de l'ordonnance attaquée que, pour considérer que la condition d'urgence était satisfaite, le juge des référés du tribunal administratif de Paris a retenu, en particulier, qu'en raison de son service auprès de l'armée française en tant que personnel civil de recrutement local, M. B... subissait des menaces de mort de la part des groupes insurgés en Afghanistan et avait dû déménager à plusieurs reprises à Kaboul afin d'assurer sa sécurité et celle de sa famille. Toutefois, il ressort des pièces du dossier soumis au juge des référés qu'aucune de ces affirmations, dont la validité était sérieusement contestée par la ministre des armées, n'est étayée par des éléments probants. Par suite, en estimant qu'était établie l'existence de menaces présentant un caractère personnel, actuel et réel en raison des anciennes fonctions exercées par M. B..., le juge des référés a entaché son ordonnance de dénaturation des pièces du dossier. La ministre des armées est, dès lors, fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à en demander l'annulation.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée, en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              Sur la demande de suspension :<br/>
<br/>
              6. Aux termes du IV de l'article 11 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires : " La collectivité publique est tenue de protéger le fonctionnaire contre les atteintes volontaires à l'intégrité de la personne, les violences, (...) les menaces, les injures, les diffamations ou les outrages dont il pourrait être victime sans qu'une faute personnelle puisse lui être imputée. Elle est tenue de réparer, le cas échéant, le préjudice qui en est résulté ".<br/>
<br/>
              7. Il résulte d'un principe général du droit que, lorsqu'un agent public est mis en cause par un tiers en raison de ses fonctions, il incombe à la collectivité dont il dépend de le couvrir des condamnations civiles prononcées contre lui, dans la mesure où une faute personnelle détachable du service ne lui est pas imputable, de lui accorder sa protection dans le cas où il fait l'objet de poursuites pénales, sauf s'il a commis une faute personnelle, et, à moins qu'un motif d'intérêt général ne s'y oppose, de le protéger contre les menaces, violences, voies de fait, injures, diffamations ou outrages dont il est l'objet. Ce principe général du droit s'étend aux agents non titulaires de l'Etat recrutés à l'étranger, alors même que leur contrat est soumis au droit local. La juridiction administrative est compétente pour connaître des recours contre les décisions des autorités de l'Etat refusant aux intéressés le bénéfice de cette protection.<br/>
<br/>
              8. Ainsi qu'il a été dit au point 4, il ne ressort pas des pièces du dossier que M. B... faisait, à la date de la décision dont la suspension est demandée, l'objet de menaces personnelles, actuelles et réelles en raison de ses anciennes fonctions auprès des forces armées françaises. Dans ces conditions, le moyen tiré de la violation de l'article 11 de la loi du 13 juillet 1983 ou du principe général du droit à la protection fonctionnelle qui l'inspire, n'est pas, en l'état de l'instruction et en tout état de cause, de nature à faire naître un doute sérieux sur la légalité du refus opposé par la ministre des armées à la demande de protection fonctionnelle présentée en 2018 par M. B.... <br/>
<br/>
              9. Il en va de même des moyens tirés, d'une part, de l'absence de motivation de la décision implicite de rejet et, d'autre part, d'un défaut d'examen individuel de sa demande.<br/>
<br/>
              10. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur la condition d'urgence, que M. B... n'est pas fondé à demander la suspension de la décision de refus de protection fonctionnelle qui lui a été opposée par la ministre des armées. Ses conclusions à fins d'injonction et celles présentées sur le fondement des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ne peuvent, par suite, qu'être rejetées.<br/>
<br/>
              Sur les conclusions à fin de sursis à exécution :<br/>
<br/>
              11. Compte tenu de ce qui précède, il n'y a plus lieu de statuer sur les conclusions à fin de sursis à l'exécution de l'ordonnance du 12 juin 2019.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 12 juin 2019 du juge des référés du tribunal administratif de Paris est annulée.<br/>
Article 2 : La demande présentée par M. B... devant le juge des référés du tribunal administratif de Paris et ses conclusions présentées devant le Conseil d'Etat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 3 : Il n'y a pas lieu de statuer sur les conclusions de la requête n° 432037.<br/>
Article 4 : La présente décision sera notifiée à la ministre des armées et à M. D... B....<br/>
Copie en sera adressée au ministre de l'intérieur et au ministre de l'Europe et des affaires étrangères. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
