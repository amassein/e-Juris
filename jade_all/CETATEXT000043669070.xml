<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043669070</ID>
<ANCIEN_ID>JG_L_2021_06_000000446630</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/66/90/CETATEXT000043669070.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 15/06/2021, 446630, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>446630</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>M. Bertrand Mathieu</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:446630.20210615</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              M. B... a demandé à la Cour nationale du droit d'asile d'annuler la décision du 12 août 2019 par laquelle le directeur général de l'Office français de protection des réfugiés et apatrides (OFPRA) a rejeté sa demande d'asile et de lui reconnaître la qualité de réfugié ou, à défaut, de lui accorder le bénéfice de la protection subsidiaire.<br/>
<br/>
              Par une décision n° 19041236 du 16 juin 2020, la Cour nationale du droit d'asile a rejeté sa demande.<br/>
<br/>
              1° Sous le n° 446630, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 novembre 2020 et 18 février 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros à verser à la SCP Thouin-Palat, Boucard, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
              2° Sous le n° 449876, par une requête, enregistrée le 18 février 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'ordonner le sursis à exécution de la même décision ;  <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros à verser à la SCP Thouin-Palat, Boucard, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New York le 31 janvier 1967 relatif au statut des réfugiés ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - Le rapport de M. Bertrand Mathieu, conseiller d'Etat en service extraordinaire,<br/>
<br/>
              - Les conclusions de Mme Sophie Roussel, rapporteure publique,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de M. A... ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Le pourvoi et la requête présentés par M. A... tendent respectivement à l'annulation et au sursis à l'exécution de la même décision. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
<br/>
              Sur le pourvoi dirigé contre la décision attaquée :<br/>
<br/>
              2.	Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              3.	Pour demander l'annulation de la décision qu'il attaque, M. A... soutient que la Cour nationale du droit d'asile a : <br/>
              -	commis une erreur de droit en subordonnant la reconnaissance de la protection subsidiaire à un lien entre le risque de persécution et une appartenance à un groupe social ;<br/>
              -	insuffisamment motivé sa décision et dénaturé les pièces du dossier faute d'avoir examiné la réalité des faits allégués et en estimant que les craintes de persécution n'étaient pas établies ;<br/>
              -	dénaturé les faits de l'espèce en jugeant qu'il ne pouvait se prévaloir des dispositions du c) de l'article L. 712-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, alors que la situation en Somalie ne cesse de se dégrader, et commis une erreur de droit en se fondant sur les seuls décès de civils sans prendre en compte les risques de blessure.<br/>
<br/>
              4.	Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
              Sur la requête à fin de sursis à exécution :<br/>
<br/>
              5.	Il résulte de ce qui précède que le pourvoi formé par M. A... contre la décision du 16 juin 2020 de la Cour nationale du droit d'asile n'est pas admis. Par suite, les conclusions à fin de sursis à exécution de cette décision sont devenues sans objet. <br/>
<br/>
              6.	Les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce que soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante, la somme demandée à ce titre.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi n° 446630 de M. A... n'est pas admis.<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions à fin de sursis à exécution de la requête n° 449876 de M. B....<br/>
Article 3 : Les conclusions de la requête n° 449876 présentées au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. B....<br/>
Copie en sera adressée à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
