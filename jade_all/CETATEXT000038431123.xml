<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038431123</ID>
<ANCIEN_ID>JG_L_2019_03_000000418634</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/43/11/CETATEXT000038431123.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 29/03/2019, 418634, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-03-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418634</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY ; SCP BARADUC, DUHAMEL, RAMEIX ; SCP L. POULET, ODENT</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:418634.20190329</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<p>Vu la procédure suivante :<br clear="none"/>
<br clear="none"/>
Procédure contentieuse antérieure<br clear="none"/>
<br clear="none"/>
La société API, M. E...F..., la société Fadi Color, M. G...B..., la société Librairie Sud-Ouest, l'association Point Com, M. C...H...A..., la Sarl Wagon Sud-Ouest, Mme I...D...et leur assureur, la compagnie d'assurance Alliance IARD, ont demandé au tribunal administratif de la Guyane de condamner la commune de Cayenne à réparer les préjudices qu'ils ont subis en conséquence de dégâts causés par une inondation survenue dans la nuit du 1er au 2 décembre 2007. Par un jugement n° 1300809 du 19 février 2015, le tribunal administratif de la Guyane a rejeté cette demande.<br clear="none"/>
<br clear="none"/>
Par un arrêt n° 15BX01191 du 28 décembre 2017, la cour administrative d'appel de Bordeaux a, sur appel de la compagnie Allianz Iard et autres, annulé ce jugement et a condamné la commune de Cayenne à verser la somme de 152 504,71 euros à la compagnie Allianz Iard, et les sommes de 231 euros à la société API, de 230 euros à M.F..., de 242 euros à la société Fadi Color, de 782 euros à M.B..., de 248 euros à la société Librairie du Sud-Ouest, de 255 euros à l'association Point Com, de 262 euros à M.A..., de 231 euros à la société Wagon Sud-Ouest et de 231 euros à MmeD....<br clear="none"/>
<br clear="none"/>
Procédure devant le Conseil d'Etat<br clear="none"/>
<br clear="none"/>
Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 28 février, 28 mai et 12 décembre 2018 au secrétariat du contentieux du Conseil d'Etat, la commune de Cayenne demande au Conseil d'Etat :<br clear="none"/>
<br clear="none"/>
1°) d'annuler cet arrêt ;<br clear="none"/>
<br clear="none"/>
2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br clear="none"/>
<br clear="none"/>
3°) de mettre solidairement à la charge de la compagnie Allianz Iard, de la société API, de M.F..., de la société Fadi Color, de M.B..., de la société Librairie Sud-Ouest, de l'association Point Com, de M.A..., de la SARL Wagon Sud-Ouest et de Mme D... une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
Vu les autres pièces du dossier ;<br clear="none"/>
<br clear="none"/>
Vu :<br clear="none"/>
- le code général des impôts ;<br clear="none"/>
- le code de justice administrative ;<br clear="none"/>
<br clear="none"/>
Après avoir entendu en séance publique :<br clear="none"/>
<br clear="none"/>
- le rapport de M. Jean Sirinelli, maître des requêtes en service extraordinaire,<br clear="none"/>
<br clear="none"/>
- les conclusions de M. Gilles Pellissier, rapporteur public.<br clear="none"/>
<br clear="none"/>
La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de la commune de Cayenne et à la SCP Baraduc, Duhamel, Rameix, avocat de la compagnie d'assurance Allianz Iard et autres.<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
Considérant ce qui suit :<br clear="none"/>
<br clear="none"/>
1. Les dispositions de l'article 1635 bis Q du code général des impôts, applicables aux instances introduites entre le 1er octobre 2011 et le 31 décembre 2013, ont institué, au I, une contribution pour l'aide juridique de 35 euros, perçue par instance introduite devant une juridiction administrative. En vertu du II de cet article, cette contribution est exigible lors de l'introduction de l'instance et elle est due par la partie qui introduit une instance. Et selon le V du même article, lorsque l'instance est introduite par un auxiliaire de justice, ce dernier acquitte pour le compte de son client la contribution par voie électronique.<br clear="none"/>
<br clear="none"/>
2. L'article R. 411-2 du code de justice administrative, dans sa rédaction alors applicable, disposait que : " Lorsque la contribution pour l'aide juridique prévue à l'article 1635 bis Q du code général des impôts est due et n'a pas été acquittée, la requête est irrecevable. / Cette irrecevabilité est susceptible d'être couverte après l'expiration du délai de recours. Lorsque le requérant justifie avoir demandé le bénéfice de l'aide juridictionnelle, la régularisation de sa requête est différée jusqu'à la décision définitive statuant sur sa demande ". Le second alinéa de l'article R. 411-2-1 du même code, alors applicable, prévoyait que : " La contribution n'est due qu'une seule fois lorsqu'un même requérant introduit une demande au fond portant sur les mêmes faits qu'une demande de référé présentée accessoirement et fondée sur le titre III du livre V du présent code ". Une demande tendant à ce que soit prescrite une mesure utile d'expertise par le juge des référés, sur le fondement de l'article R. 532-1 du code de justice administrative, est au nombre de celles visées par le second alinéa de l'article R. 411-2-1 précité.<br clear="none"/>
<br clear="none"/>
3. Il ressort des pièces soumises au juge du fond que la société API, six autres commerçants de quartiers de la partie basse de la ville de Cayenne riverains du canal Laussat qui traverse ses quartiers, et leur assureur, la compagnie d'assurance Alliance IARD, ont, le 11 août 2010, introduit une demande au tribunal administratif de Cayenne, depuis dénommé tribunal administratif de la Guyane, tendant à ce que, par voie de référé, soit diligentée une mesure d'expertise portant sur la recherche des causes et des conséquences des inondations dont ils ont été victimes du fait d'un débordement des eaux du canal. Il est constant, eu égard à ce qui a été rappelé au point 1, que les dispositions de l'article 1635 bis Q du code général des impôts n'étaient pas applicables à cette date. Par suite, les demandeurs n'ont pas eu à acquitter la contribution pour l'aide juridique lors de l'introduction de cette première instance. En revanche, l'article 1635 bis Q était entré en vigueur et n'avait pas encore été abrogé lorsqu'ils ont, le 12 août 2013, introduit devant le même tribunal une demande indemnitaire portant sur les mêmes faits que la demande d'expertise. Par suite, la contribution pour l'aide juridique était due pour la première fois à l'occasion de l'introduction de cette instance.<br clear="none"/>
<br clear="none"/>
4. Il ressort également des pièces soumises au juge du fond que la contribution pour l'aide juridique n'a pas été acquittée par les demandeurs à l'occasion de cette instance devant le tribunal administratif. Il résulte des dispositions de l'article R. 411-2 du code de justice administrative alors applicables que le tribunal administratif pouvait la rejeter d'office sans demande de régularisation préalable, dès lors que cette demande était introduite par un avocat. Dans ces conditions, en application de l'article R. 411-2 du code de justice administrative, la requête enregistrée le 12 août 2013 était irrecevable.<br clear="none"/>
<br clear="none"/>
5. Statuant sur la requête d'appel introduite par les demandeurs de première instance contre le jugement du 19 février 2015 par lequel le tribunal administratif de Cayenne avait rejeté leur demande, la cour administrative d'appel de Bordeaux y a fait droit partiellement, alors qu'il lui appartenait de soulever d'office l'irrecevabilité de la demande de première instance pour défaut d'acquittement de la contribution pour l'aide juridique qui ressortait des pièces du dossier, n'était pas susceptible d'être régularisée devant la cour et faisait obstacle à ce qu'elle puisse accueillir la requête d'appel. Du fait de cette omission, elle a, ainsi que le soutient la commune de Cayenne dans son pourvoi, commis une erreur de droit. La commune de Cayenne est par suite fondée, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, à demander l'annulation de l'arrêt attaqué de la cour administrative d'appel de Bordeaux.<br clear="none"/>
<br clear="none"/>
6. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br clear="none"/>
<br clear="none"/>
7. Ainsi qu'il a été dit au point 4, la demande introduite par la compagnie Allianz IARD et autres tendant à la condamnation de la commune de Cayenne à réparer les conséquences dommageables subis par ces derniers résultant de l'inondation de certains quartiers de la basse ville, était irrecevable pour défaut d'acquittement spontané de la contribution pour l'aide juridique par leur conseil. Les demandeurs de première instance ne sont par suite pas fondés à se plaindre de ce que, par le jugement attaqué, le tribunal administratif de la Guyane a rejeté leur demande.<br clear="none"/>
<br clear="none"/>
8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la compagnie Allianz IARD et autres la somme de 3 000 euros à verser à la commune de Cayenne sur le fondement de l'article L. 761-1 du code de justice administrative. En revanche, ces mêmes dispositions font obstacle à ce qu'il soit fait droit aux conclusions présentées sur le même fondement par la société Allianz et autres.<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
D E C I D E :<br clear="none"/>
--------------<br clear="none"/>
Article 1er : L'arrêt du 28 décembre 2017 de la cour administrative d'appel de Bordeaux est annulé.<br clear="none"/>
Article 2 : La requête de la compagnie Allianz Iard et autres est rejetée.<br clear="none"/>
Article 3 : La compagnie Allianz IARD et autres verseront la somme de 3 000 euros à la commune de Cayenne sur le fondement de l'article L. 761-1 du code de justice administrative.<br clear="none"/>
Article 4 : La présente décision sera notifiée à la commune de Cayenne, à la compagnie d'assurance Allianz Iard, représentant unique pour l'ensemble des défendeurs et au GFA Caraïbes.<br clear="none"/>
Copie en sera adressée à la société guyanaise des eaux, à la communauté d'agglomération du centre littoral de Guyane, au ministre de l'intérieur et à la ministre des outre-mer.<br clear="none"/>
</p>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP>
<CONTENU>



</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
