<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044167168</ID>
<ANCIEN_ID>JG_L_2021_09_000000456571</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/16/71/CETATEXT000044167168.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 27/09/2021, 456571, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-09-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>456571</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:456571.20210927</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés le 10 et le 20 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, Mme AD... W..., épouse AE..., Mme Y... J... et Mme D... T... demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution du 10° de l'article 1er du décret n° 2021-1059 du 7 août 2021 modifiant le décret 2021-699 du 1er juin 2021 prescrivant les mesures générales nécessaires à la gestion de la sortie de la crise sanitaire, en ce qu'il fixe également limitativement, et de façon générale et absolue, la liste des contre-indications médicales faisant obstacle à la vaccination contre la Covid-19 pour les personnes soumises à cette vaccination obligatoire sans possibilité pour ces derniers de faire valoir une contre-indication médicale personnelle ; <br/>
<br/>
              2°) d'enjoindre au Premier ministre d'abroger cet article en tant qu'il ne permet pas aux personnes soumises à cette vaccination obligatoire de faire valoir une contre-indication médicale personnelle ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elles soutiennent que :<br/>
              - la condition d'urgence est satisfaite dès lors, d'une part, que l'entrée en vigueur de l'obligation vaccinale pour les professionnels de santé est fixée au 15 septembre 2021, ce qui implique qu'après cette date elles ne pourront plus exercer leur profession sans avoir satisfait à cette obligation vaccinale et, d'autre part, qu'elles ne peuvent se soumettre à cette obligation vaccinale eu égard aux contre-indications médicales qu'elles présentent ;  <br/>
              - il est porté une atteinte grave et manifestement illégale à la liberté fondamentale du patient de donner son consentement libre et éclairé aux soins médicaux qui lui sont prodigués et au droit au respect de la vie ;<br/>
              - les dispositions contestées méconnaissent l'interdiction de prononcer des mesures à caractère général et absolu dès lors que, d'une part, elles fixent une liste limitative des cas de contre-indication à la vaccination, empêchant ainsi de se prévaloir d'autres contre-indications à la vaccination obligatoire contre la Covid-19 que celles qu'elles prévoient et ne permettent pas de faire valoir de contre-indication médicale personnelle à cette vaccination et, d'autre part, une partie des contre-indications à recevoir une deuxième dose de vaccin résulte de la survenance de certains effets indésirables à la suite, précisément, de l'injection de la première dose ;<br/>
              -  le vaccin Pfizer ne peut plus être administré aux requérantes dès lors que son autorisation conditionnelle de mise sur le marché est caduque, en l'absence d'éléments établissant qu'une demande de renouvellement de cette autorisation a été présentée dans les délais prévus par les règlements européens applicables ;<br/>
              - les dispositions contestées méconnaissent le droit à la vie rappelé par l'article 2 de la convention européenne des droits de l'homme et de sauvegarde des libertés fondamentales, en ce qu'elles interdisent la prise en compte de toutes autres contre-indications médicales que celles qu'elles recensent, et exposent ainsi les personnes concernées au risque d'une réaction allergique grave à la première dose ;<br/>
<br/>
              Par un mémoire en intervention, enregistré le 20 septembre 2021, M. F... U..., M. P... N..., Mme Z... C..., M. B... AC..., M. AA... E..., Mme L... AB..., M. K... S..., M. X... R..., M. M... V..., M. AF... AG..., Mme O... A..., Mme H... Q... et M. I... G..., demandent au juge des référés du Conseil d'Etat de faire droit aux conclusions de la requête n° 456571. Ils soutiennent que leur intervention est recevable et s'associent aux moyens de la requête.<br/>
<br/>
              Par un mémoire en défense, enregistré le 17 septembre 2021, le ministre des solidarités et de la santé conclut au rejet de la requête. Il soutient qu'aucune atteinte grave et manifestement illégale n'a été portée à une liberté fondamentale.<br/>
<br/>
              La requête a été communiquée au Premier ministre qui n'a pas produit d'observations. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne des droits de l'homme et de sauvegarde des libertés fondamentales ;<br/>
              - la loi n° 2021-689 du 31 mai 2021 ;<br/>
              - la loi n° 2021-1040 du 5 août 2021 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, Mme W..., Mme J... et Mme T... et, d'autre part, le Premier ministre et le ministre des solidarités et de la santé ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 21 septembre 2021, à 10 heures 30 : <br/>
<br/>
              - Me Sebagh, avocat au Conseil d'Etat, au titre de la permanence, avocat de Mme W... et autres ;<br/>
<br/>
              - les représentants de Mme W..., Mme J..., et Mme T... ;<br/>
<br/>
              -les représentants du ministre des solidarités et de la santé ;<br/>
<br/>
              à l'issue de laquelle le juge des référés a prononcé la clôture de l'instruction ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". <br/>
<br/>
              2. L'intervention de M. U... et autres est admise.<br/>
<br/>
              3. Le I de l'article 12 de la loi du 5 août 2021 relative à la gestion de la crise sanitaire instaure une obligation de vaccination pour les personnes qu'il énumère, notamment, au 1°, les personnes exerçant leur activité les établissements de santé, les établissements sociaux et médicaux sociaux et les autres établissements et services dont il fixe la liste, " 2° Les professionnels de santé mentionnés à la quatrième partie du code de la santé publique, lorsqu'ils ne relèvent pas du 1° du présent I ; / 3° Les personnes, lorsqu'elles ne relèvent pas des 1° ou 2° du présent I, faisant usage : / a) Du titre de psychologue(...) ; / b) Du titre d'ostéopathe ou de chiropracteur(...) ; / c) Du titre de psychothérapeute(...) ; / 4° Les étudiants ou élèves des établissements préparant à l'exercice des professions mentionnées aux 2° et 3° du présent I ainsi que les personnes travaillant dans les mêmes locaux que les professionnels mentionnés au 2° ou que les personnes mentionnées au 3° (...) /  6° Les sapeurs-pompiers et les marins-pompiers des services d'incendie et de secours, les pilotes et personnels navigants de la sécurité civile assurant la prise en charge de victimes, les militaires des unités investies à titre permanent de missions de sécurité civile mentionnés au premier alinéa de l'article L. 721-2 du code de la sécurité intérieure ainsi que les membres des associations agréées de sécurité civile mentionnées à l'article L. 725-3 du même code participant, à la demande de l'autorité de police compétente ou lors du déclenchement du plan Orsec, aux opérations de secours et à l'encadrement des bénévoles dans le cadre des actions de soutien aux populations ou qui contribuent à la mise en place des dispositifs de sécurité civile dans le cadre de rassemblements de personnes (...) ". Aux termes du II du même article : " Un décret, pris après avis de la Haute Autorité de santé, détermine les conditions de vaccination contre la Covid-19 des personnes mentionnées au I du présent article. Il précise les différents schémas vaccinaux et, pour chacun d'entre eux, le nombre de doses requises. / Ce décret fixe les éléments permettant d'établir un certificat de statut vaccinal pour les personnes mentionnées au même I et les modalités de présentation de ce certificat sous une forme ne permettant d'identifier que la nature de celui-ci et la satisfaction aux critères requis. Il détermine également les éléments permettant d'établir le résultat d'un examen de dépistage virologique ne concluant pas à une contamination par la Covid-19 et le certificat de rétablissement à la suite d'une contamination par la Covid-19 ". Aux termes de l'article 13 de cette loi : " I. - Les personnes mentionnées au I de l'article 12 établissent : / 1° Satisfaire à l'obligation de vaccination en présentant le certificat de statut vaccinal prévu au second alinéa du II du même article 12. / Par dérogation au premier alinéa du présent 1°, peut être présenté, pour sa durée de validité, le certificat de rétablissement prévu au second alinéa du II de l'article 12. Avant la fin de validité de ce certificat, les personnes concernées présentent le justificatif prévu au premier alinéa du présent 1°. / Un décret détermine les conditions d'acceptation de justificatifs de vaccination, établis par des organismes étrangers, attestant de la satisfaction aux critères requis pour le certificat mentionné au même premier alinéa ; / 2° Ne pas être soumises à cette obligation en présentant un certificat médical de contre-indication. Ce certificat peut, le cas échéant, comprendre une date de validité. "<br/>
<br/>
              4. Aux termes du J du II de l'article 1er de la loi du 31 mai 2021 relative à la gestion de la sortie de crise sanitaire, issu de la loi du 5 août 2021 : " Un décret, pris après avis de la Haute Autorité de santé, détermine les cas de contre-indication médicale faisant obstacle à la vaccination et permettant la délivrance d'un document pouvant être présenté dans les cas prévus au 2° du A du présent II. "<br/>
<br/>
              5. Aux termes de l'article 2-4 du décret du 1er juin 2021 prescrivant les mesures générales nécessaires à la gestion de la sortie de crise sanitaire, issu du 3° de l'article 1er du décret du 7 août 2021 : " Les cas de contre-indication médicale faisant obstacle à la vaccination contre la covid-19 et permettant la délivrance du document pouvant être présenté dans les cas prévus au 2° du A du II de l'article 1er de la loi du 31 mai 2021 susvisée sont mentionnés à l'annexe 2 du présent décret. / L'attestation de contre-indication médicale est remise à la personne concernée par un médecin. "<br/>
<br/>
              6. Aux termes de l'annexe 2 du décret du 1er juin 2021, issue du 10° de l'article 1er du décret du 7 août 2021, et complétée par le décret du 11 août 2021 : " I. - Les cas de contre-indication médicale faisant obstacle à la vaccination contre la covid-19 mentionnés à l'article 2-4 sont : / 1° Les contre-indications inscrites dans le résumé des caractéristiques du produit (RCP) : / - antécédent d'allergie documentée (avis allergologue) à un des composants du vaccin en particulier polyéthylène-glycols et par risque d'allergie croisée aux polysorbates ; / - réaction anaphylaxique au moins de grade 2 (atteinte au moins de 2 organes) à une première injection d'un vaccin contre le COVID posée après expertise allergologique ; / - personnes ayant déjà présenté des épisodes de syndrome de fuite capillaire (contre-indication commune au vaccin Vaxzevria et au vaccin Janssen) ; / - personnes ayant présenté un syndrome thrombotique et thrombocytopénique (STT) suite à la vaccination par Vaxzevria. / 2° Une recommandation médicale de ne pas initier une vaccination (première dose) : / - syndrome inflammatoire multi systémique pédiatrique (PIMS) post-covid-19. / 3° Une recommandation établie après concertation médicale pluridisciplinaire de ne pas effectuer la seconde dose de vaccin suite à la survenue d'un effet indésirable d'intensité sévère ou grave attribué à la première dose de vaccin signalé au système de pharmacovigilance (par exemple : la survenue de myocardite, de syndrome de Guillain-Barré ...). / II. - Les cas de contre-indication médicale temporaire faisant obstacle à la vaccination contre la covid-19 mentionnés à l'article 2-4 sont :  / 1° Traitement par anticorps monoclonaux anti-SARS-CoV-2. / 2° Myocardites ou péricardites survenues antérieurement à la vaccination et toujours évolutives. "<br/>
<br/>
              7. Mme W... et les autres requérantes doivent être regardées comme demandant, sur le fondement des dispositions citées au point 1, d'une part, la suspension de l'exécution des dispositions de l'annexe 2 du décret du 1er juin 2021, en ce qu'elles fixent limitativement, et de façon générale, la liste des cas de contre-indication médicale faisant obstacle à la vaccination contre la Covid-19, sans possibilité pour les personnes soumises à l'obligation de vaccination de faire valoir une contre-indication médicale individuelle et, d'autre part, qu'il soit enjoint au Premier ministre d'abroger ces dispositions dans cette mesure.<br/>
<br/>
              8. En premier lieu, les requérantes ne sauraient utilement se prévaloir, à l'encontre des dispositions attaquées, des conditions de renouvellement de l'autorisation de mise sur le marché conditionnelle délivrée pour l'un des vaccins contre la Covid-19.<br/>
<br/>
              9. En deuxième lieu, le droit du patient de donner son consentement libre et éclairé aux soins médicaux qui lui sont prodigués ne saurait être utilement invoqué à l'encontre des dispositions prises par le pouvoir réglementaire pour mettre en œuvre une obligation de vaccination établie par la loi pour lutter contre l'épidémie de Covid-19, dont le principe même écarte l'application de ce droit. <br/>
<br/>
              10. En dernier lieu, le droit au respect de la vie, rappelé notamment par l'article 2 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, constitue une liberté fondamentale au sens des dispositions de l'article L. 521-2 du code de justice administrative. Lorsque l'action ou la carence de l'autorité publique crée un danger caractérisé et imminent pour la vie des personnes, portant ainsi une atteinte grave et manifestement illégale à cette liberté fondamentale, et que la situation permet de prendre utilement des mesures de sauvegarde dans un délai de quarante-huit heures, le juge des référés peut,  au titre de la procédure particulière prévue par cet article, prescrire toutes les mesures de nature à faire cesser le danger résultant de cette action ou de cette carence. Si les requérantes font valoir que la limitation des possibilités de contre-indications individuelles qui résulte des dispositions contestées porterait une atteinte potentielle à ce droit, compte tenu des risques révélés par les données de pharmacovigilance, de tels éléments ne sont pas de nature à caractériser un danger de cette nature. Par suite, elles ne sont pas fondées à soutenir que les dispositions litigieuses porteraient une atteinte grave et manifestement illégale au droit au respect de la vie.<br/>
<br/>
              11. Il résulte de ce qui précède qu'aucun des moyens soulevés par les requérantes n'est de nature à caractériser une atteinte grave et manifestement illégale portée aux libertés fondamentales. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de Mme W..., Mme J... et Mme T... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme AD... W..., épouse AE..., Mme Y... J... et Mme D... T..., à M. F... U..., premier intervenant dénommé, au ministre des solidarités et de la santé et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
