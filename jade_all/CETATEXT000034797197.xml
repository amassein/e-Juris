<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034797197</ID>
<ANCIEN_ID>JG_L_2017_05_000000386746</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/79/71/CETATEXT000034797197.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 22/05/2017, 386746</TITRE>
<DATE_DEC>2017-05-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386746</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Christophe Pourreau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:386746.20170522</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat les 26 décembre 2014, 26 mars 2015 et 21 mars 2017, l'Union des maisons et marques de vin (UMVIN) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2014-709 du 25 juin 2014 relatif à l'appellation d'origine contrôlée " Côtes de Bergerac " et la décision implicite de rejet née du silence gardé par le Premier ministre sur sa demande du 26 août 2014 de retrait de ce décret ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement (CE) n° 607/2009 du 14 juillet 2009 ;<br/>
              - le règlement (UE) n° 1308/2013 du 17 décembre 2013 ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de l'Union des maisons et marques de vin ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Le décret du 25 juin 2014 relatif à l'appellation d'origine contrôlée (AOC) " Côtes de Bergerac " a homologué le cahier des charges modifié qui lui est annexé et abrogé le précédent cahier des charges de cette appellation. L'Union des maisons et marques de vin (UMVIN) demande au Conseil d'Etat d'annuler pour excès de pouvoir ce décret, ainsi que la décision implicite de rejet née du silence gardé par le Premier ministre sur sa demande du 27 août 2014 de retrait de ce décret.<br/>
<br/>
              Sur l'intervention de la Fédération des vins de Bergerac et Duras :<br/>
<br/>
              2. La Fédération des vins de Bergerac et Duras justifie d'un intérêt au maintien du décret attaqué. Par suite, son intervention est recevable.<br/>
<br/>
              Sur la légalité externe du décret attaqué :<br/>
<br/>
              3. En premier lieu, si l'UMVIN soutient que la composition du comité national des appellations d'origine relatives aux vins et aux boissons alcoolisées, et des eaux-de-vie de l'INAO, qui a examiné la demande de modification du cahier des charges de l'AOC " Côtes de Bergerac ", n'assure pas une " représentation équilibrée des différents secteurs et signes en cause ", en méconnaissance de l'article L. 642-9 du code rural et de la pêche maritime, elle n'assortit pas ce moyen des précisions permettant d'en établir le bien-fondé.<br/>
<br/>
              4. En deuxième lieu, si l'UMVIN soutient que l'organisme de défense et de gestion de l'AOC " Côtes de Bergerac " n'aurait pas été consulté sur la proposition émanant du comité national des appellations d'origine relatives aux vins et aux boissons alcoolisées, et des eaux-de-vie de l'INAO, il ressort des pièces du dossier, notamment d'une lettre de la Fédération des vins du Bergeracois du 28 janvier 2014 produite en défense par le ministre de l'agriculture, de l'agroalimentaire et de la forêt, qu'un tel moyen manque également en fait.<br/>
<br/>
              5. En troisième lieu, aux termes de l'article L. 644-4 du code rural et de la pêche maritime, en vigueur à la date d'édiction du décret attaqué : " Le ministre chargé de l'agriculture peut décider, après avis de l'organisme de défense et de gestion et de l'organisation professionnelle compétents, que la mise en bouteille et le conditionnement des produits d'origine vitivinicole bénéficiant d'une appellation d'origine s'effectue dans les régions de production ". La procédure spéciale prévue à cet article n'était pas exclusive de la procédure de droit commun prévue à l'article L. 641-7 du même code, selon laquelle la modification des conditions de production d'un vin, qui peuvent inclure son conditionnement, implique une modification du cahier des charges de l'AOC, homologuée par décret. Par suite, le moyen tiré de ce qu'en vertu de l'article L. 644-4 du code rural et de la pêche maritime, le Premier ministre n'était pas compétent pour prendre le décret attaqué doit être écarté.<br/>
<br/>
              6. En quatrième lieu, si l'UMVIN fait valoir que le décret litigieux a été pris en méconnaissance de l'article 96 du règlement (UE) n° 1308/2013 du 20 décembre 2013, faute d'avoir été notifié à la Commission européenne, il ressort, en tout état de cause, des pièces du dossier, notamment de l'accusé de réception produit par le ministre de l'agriculture, de l'agroalimentaire et de la forêt, que le décret attaqué lui a été notifié le 19 septembre 2014.<br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              7. Aux termes de l'article 94 du règlement (UE) n° 1308/2013 mentionné ci-dessus : " 1. Les demandes de protection de dénominations en tant qu'appellations d'origine (...) sont accompagnées d'un dossier technique comportant : / (...) c) le cahier des charges visé au paragraphe 2 (...) / 2. (...) / Le cahier des charges comporte au minimum les éléments suivants : / (...) h) les exigences (...) prévues par les Etats membres ou une organisation responsable de la gestion de l'appellation d'origine protégée (...), étant entendu que ces exigences doivent être objectives, non discriminatoires et compatibles avec la législation de l'Union (...) ". Il résulte de la jurisprudence de la Cour de justice de l'Union européenne, notamment de l'arrêt rendu le 16 mai 2000 dans l'affaire C-388/95, Royaume de Belgique c/ Royaume d'Espagne, qu'une réglementation nationale relative aux vins d'appellation d'origine et limitant la quantité de vin pouvant être exportée en vrac et conditionnée en dehors de la région de production est compatible avec le droit de l'Union européenne à la double condition qu'elle soit nécessaire pour que l'appellation d'origine remplisse sa fonction spécifique, qui est de garantir que le produit qui en bénéficie provient d'une zone géographique déterminée et présente des qualités particulières, et qu'elle ne soit pas disproportionnée. En particulier, une telle réglementation est justifiée lorsque l'embouteillage dans la région de production imprime au vin des caractères particuliers, de nature à l'individualiser, ou s'il est indispensable à la conservation de ces caractères particuliers.<br/>
<br/>
              8. D'une part, en vertu des 1° et 3° du IV du chapitre Ier du cahier des charges de l'AOC " Côtes de Bergerac " homologué par le décret attaqué, les vins rouges bénéficiant de cette appellation doivent être conditionnés dans l'aire géographique de production.<br/>
<br/>
              9. D'autre part, aux termes du 2° du IX du même chapitre : " Les vins rouges sont élevés durant une période qui s'achève au plus tôt le 30 novembre de l'année qui suit celle de la récolte, dont au moins deux mois en bouteilles ". Aux termes du a du 5° du même IX : " A l'issue de la période d'élevage, les vins rouges sont mis en marché à destination du consommateur à partir du 15 décembre de l'année qui suit celle de la récolte ". Aux termes du b du même 5° : " Les vins rouges peuvent circuler entre entrepositaires agréés à partir du 1er décembre de l'année qui suit celle de la récolte ". Il résulte de ces dispositions que des vins rouges bénéficiant de l'AOC " Côtes de Bergerac ", qui ne peuvent être mis à la vente avant le 15 décembre de l'année qui suit l'année de la récolte, ne peuvent être transportés dans les installations de négociants et élevés par des négociants avant le 30 novembre de cette même année, y compris lorsque ces derniers disposent d'installations situées à l'intérieur de l'aire géographique de production ou de l'aire de proximité immédiate de cette appellation.<br/>
<br/>
              10. Il ressort des pièces du dossier que les vins rouges bénéficiant de l'AOC " Côtes de Bergerac ", issus des cépages cabernet, cabernet sauvignon, cot et merlot, sont des vins tranquilles, provenant de vignes à faible production, dont la maturation est poussée à son paroxysme, afin qu'ils acquièrent une couleur marquée et de riches propriétés organoleptiques. La longue maturation des vignes rend cependant ces vins sensibles à l'oxygène et au développement des micro-organismes, tels que les bactéries acétiques. Le parti pris de limiter l'emploi du dioxyde de soufre, dont l'usage est limité pour des motifs de santé publique, implique de prêter une attention particulière à l'évolution des vins au cours de l'élevage.<br/>
<br/>
              11. Ces caractéristiques justifient que l'élevage et l'embouteillage, qui intervient au cours de la période d'élevage, dès lors que les vins doivent être élevés en bouteilles pendant au moins deux mois, et constitue la dernière occasion de vérifier que les vins ont des caractéristiques conformes à celles prévues dans le cahier des charges de l'AOC, soient conduits par des entreprises qui disposent d'une connaissance particulière de ces vins, qui acceptent de faire l'objet de contrôles réguliers et qui puissent, le cas échéant, recourir au savoir-faire des producteurs pour rétablir les caractéristiques initiales des vins. Les entreprises établies dans l'aire géographique de production sont plus susceptibles de présenter de telles garanties. En revanche, il ne ressort pas des pièces du dossier que l'élevage et l'embouteillage de ces vins ne pourraient pas être réalisés, dans des conditions présentant les mêmes garanties, par des opérateurs autres que les récoltants, tels que des négociants, disposant d'installations situées dans l'aire géographique de production. Ainsi, si les dispositions du 3° du IV du chapitre Ier du cahier des charges exigeant que les vins rouges bénéficiant de l'AOC " Côtes de Bergerac " soient élevés et conditionnés dans l'aire géographique de production apparaissent nécessaires et proportionnées à l'objectif de préservation des caractères distinctifs de ces vins, il n'en va pas de même des dispositions du b du 4° du IX du même chapitre, qui ont pour effet d'interdire l'élevage de ces vins par des négociants avant le 30 novembre de l'année qui suit l'année de la récolte.<br/>
<br/>
              12. Il résulte de tout ce qui précède que l'UMVIN est fondée à demander l'annulation pour excès de pouvoir du décret attaqué et de la décision implicite de rejet née du silence gardé par le Premier ministre sur sa demande de retrait de ce décret en tant seulement que le b du 4° du IX du chapitre Ier du cahier des charges qu'il homologue prévoit que les vins rouges bénéficiant de l'AOC " Côtes de Bergerac " ne peuvent circuler entre entrepositaires agréés avant le 1er décembre de l'année qui suit l'année de la récolte.<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              13. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à l'UMVIN au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de la Fédération des vins de Bergerac et Duras est admise.<br/>
Article 2 : Le décret du 25 juin 2014 relatif à l'AOC " Côtes de Bergerac " et la décision implicite de rejet née du silence gardé par le Premier ministre sur la demande d'abrogation de ce décret sont annulés en tant que le b du 4° du IX du chapitre Ier du cahier des charges qu'il homologue prévoit que les vins rouges bénéficiant de cette appellation ne peuvent circuler entre entrepositaires agréés avant le 1er décembre de l'année qui suit l'année de la récolte.<br/>
Article 3 : L'Etat versera à l'UMVIN la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions de la requête de l'UMVIN est rejeté.<br/>
Article 5 : La présente décision sera notifiée à l'Union des maisons et marques de vin, au Premier ministre, au ministre de l'économie, au ministre de l'agriculture et de l'alimentation et à la Fédération des vins de Bergerac et Duras.<br/>
Copie en sera adressée pour information à l'Institut national de l'origine et de la qualité.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">03-05-06-02 AGRICULTURE ET FORÊTS. PRODUITS AGRICOLES. VINS. CONTENTIEUX DES APPELLATIONS. - DÉCRET HOMOLOGUANT LE CAHIER DES CHARGES DE L'AOC CÔTES DE BERGERAC - CARACTÈRE NÉCESSAIRE ET PROPORTIONNÉ, AU REGARD DES OBJECTIFS POURSUIVIS, DES CONDITIONS TENANT À L'ÉLEVAGE ET L'EMBOUTEILLAGE - LIMITATION AUX SEULES ENTREPRISES ÉTABLIES DANS L'AIRE GÉOGRAPHIQUE DE PRODUCTION [RJ1] [RJ2] - EXISTENCE - EXCLUSION DES NÉGOCIANTS SITUÉS DANS L'AIRE GÉOGRAPHIQUE DE PRODUCTION - ABSENCE [RJ2].
</SCT>
<ANA ID="9A"> 03-05-06-02 Les caractéristiques des vins rouges bénéficiant de l'AOC  Côtes de Bergerac justifient que leur élevage et leur embouteillage soient conduits par des entreprises qui disposent d'une connaissance particulière de ces vins, qui acceptent de faire l'objet de contrôles réguliers et qui puissent, le cas échéant, recourir au savoir-faire des producteurs pour rétablir les caractéristiques initiales des vins.,,,Les entreprises établies dans l'aire géographique de production sont plus susceptibles de présenter de telles garanties. En revanche, il ne ressort pas des pièces du dossier que l'élevage et l'embouteillage de ces vins ne pourraient pas être réalisés, dans des conditions présentant les mêmes garanties, par des opérateurs autres que les récoltants, tels que des négociants, disposant d'installations situées dans l'aire géographique de production.,,,Les dispositions du cahier des charges exigeant que les vins rouges bénéficiant de l'AOC Côtes de Bergerac soient élevés et conditionnés dans l'aire géographique de production apparaissent par suite nécessaires et proportionnées à l'objectif de préservation des caractères distinctifs de ces vins. Tel n'est pas le cas de celles qui ont pour effet d'interdire l'élevage de ces vins par des négociants avant le 30 novembre de l'année qui suit l'année de la récolte.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 3 octobre 2016, Société Compagnie Vinicole de Bourgogne, n° 388585, inédite au Recueil., ,[RJ2] Cf. décision du même jour, Société M. Chapoutier SA, n° 397570, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
