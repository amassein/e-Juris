<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034081873</ID>
<ANCIEN_ID>JG_L_2017_02_000000397569</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/08/18/CETATEXT000034081873.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 24/02/2017, 397569, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-02-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397569</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Anton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:397569.20170224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Marseille la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre des années 2006 et 2007. Par un jugement n° 1201682 du 26 novembre 2013, ce tribunal a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 14MA00505 du 14 janvier 2016, la cour administrative d'appel de Marseille a, sur appel de M.B..., annulé ce jugement et déchargé ce dernier de ces impositions supplémentaires.  <br/>
<br/>
              Par un pourvoi, enregistré le 2 mars 2016 au secrétariat du contentieux du Conseil d'Etat, le ministre des finances et des comptes publics demande au Conseil d'Etat d'annuler les articles 1er et 2 de cet arrêt.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code des postes et des communications électroniques ;<br/>
              - l'arrêté du 7 février 2007 pris en application de l'article R. 2-1 du code des postes et des communications électroniques et fixant les modalités relatives au dépôt et à la distribution des envois postaux ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Anton, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de M.B....<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite de la vérification de comptabilité dont la société Brasserie de l'Europe a fait l'objet, M.B..., gérant et associé de cette société, a été assujetti à des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales au titre des années 2006 et 2007. Le tribunal administratif de Marseille a rejeté sa demande tendant à la décharge de ces impositions par un jugement du 26 novembre 2013. Par les articles 1er et 2 de l'arrêt du 14 janvier 2016 contre lesquels le ministre des finances et des comptes publics se pourvoit en cassation, la cour administrative d'appel de Marseille a annulé ce jugement et déchargé M. B...de ces impositions.<br/>
<br/>
              2. Aux termes de l'article L. 57 du livre des procédures fiscales : " L'administration adresse au contribuable une proposition de rectification qui doit être motivée de manière à lui permettre de formuler ses observations ou de faire connaître son acceptation (...) ". Les rectifications doivent être notifiées au contribuable. En cas de contestation sur ce point, il incombe à l'administration fiscale d'établir qu'une telle notification a été régulièrement adressée au contribuable et, lorsque le pli contenant cette notification a été renvoyé par le service postal au service expéditeur, de justifier de la régularité des opérations de présentation à l'adresse du destinataire. La preuve qui lui incombe ainsi peut résulter soit des mentions précises, claires et concordantes figurant sur les documents, le cas échéant électroniques, remis à l'expéditeur conformément à la règlementation postale soit, à défaut, d'une attestation de l'administration postale ou d'autres éléments de preuve établissant la délivrance par le préposé du service postal d'un avis de passage prévenant le destinataire de ce que le pli est à sa disposition au bureau de poste.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond qu'une proposition de rectification a été envoyée au domicile de M. B...le 30 septembre 2009 par un pli recommandé qui n'a pas été retiré par son destinataire, alors qu'un avis de passage mentionnant la vaine présentation avait été déposé dans sa boîte aux lettres. L'administration fiscale a produit devant les juges du fond un avis de réception sur lequel a été apposée par voie de duplication une date de vaine présentation figurant sur l'avis de passage indiquant à tort le 1er septembre 2009 et non le 1er octobre 2009, ainsi que l'établit la fiche de suivi informatique du courrier faisant état du dépôt de la lettre recommandée le 30 septembre 2009. <br/>
<br/>
              4. En premier lieu, alors qu'il ressortait des pièces de la procédure qui lui étaient soumises et qu'il n'était pas contesté qu'aucun autre élément ne permettait à M. B...de savoir que le pli en litige n'avait pas été expédié avant la date du 1er septembre 2009 porté sur l'avis de passage qui lui avait été laissé, la cour n'a pas commis d'erreur de droit en jugeant que, dès lors que la date erronée du 1er septembre 2009 portée sur cet avis suffisait à l'avoir induit en erreur, le délai de quinze jours qui lui était imparti pour retirer un pli recommandé qui aurait été présenté le 1er septembre étant expiré à la date du 1er octobre, M. B...avait été privé d'une garantie et était par suite fondé à soutenir que la procédure d'imposition était entachée d'irrégularité. <br/>
<br/>
              5. En second lieu, la cour, qui s'est bornée à relever qu'à supposer même qu'elle ait été le résultat d'une erreur de plume, la date du 1er septembre mentionnée sur l'avis de réception était de nature à avoir induit M. B...en erreur, n'a pas  dénaturé les pièces du dossier qui lui était soumis sur l'existence d'une telle erreur de plume.<br/>
<br/>
              6. Il résulte de ce qui précède que le pourvoi du ministre des finances et des comptes publics doit être rejeté.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à M. B...de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>                     D E C I D E :<br/>
                                   --------------<br/>
<br/>
Article 1er : Le pourvoi du ministre des finances et des comptes publics est rejeté.<br/>
<br/>
Article 2 : L'Etat versera à M. B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de l'économie et des finances et à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
