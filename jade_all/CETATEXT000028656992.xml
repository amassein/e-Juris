<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028656992</ID>
<ANCIEN_ID>JG_L_2014_02_000000357111</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/65/69/CETATEXT000028656992.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 19/02/2014, 357111, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-02-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357111</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Olivier Japiot</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:357111.20140219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête et le mémoire complémentaire, enregistrés les 24 février et 8 mars 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société d'importation Leclerc (SIPLEC), dont le siège est situé 26, quai Marcel Boyer à Ivry-sur-Seine (94200) ; la société SIPLEC demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision prise au nom de la ministre de l'écologie, du développement durable, des transports et du logement et du ministre de l'économie, des finances et de l'industrie, révélée par la lettre d'information " Certificats d'économies d'énergie " de décembre 2011, d'instaurer une période de tolérance portant sur la justification de l'antériorité du rôle actif et incitatif des acteurs du dispositif dans le cadre des demandes de certificats d'économies d'énergie ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 6 février 2014, présentée par la société d'importation Leclerc ;<br/>
<br/>
              Vu le code de l'énergie ;<br/>
<br/>
              Vu le décret n° 2005-850 du 27 juillet 2005 ; <br/>
<br/>
              Vu le décret n° 2010-1443 du 25 novembre 2010 ;<br/>
<br/>
              Vu le décret n° 2010-1447 du 25 novembre 2010 ; <br/>
<br/>
              Vu le décret n° 2010-1664 du 29 décembre 2010 ;<br/>
<br/>
              Vu l'arrêté du 29 décembre 2010 de la ministre de l'écologie, du développement durable, des transports et du logement et de la ministre de l'économie, des finances et de l'industrie fixant la liste des éléments d'une demande de certificats d'économies d'énergie et la composition d'une demande d'agrément d'un plan d'actions d'économies d'énergie ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Japiot, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de la société d'importation Leclerc (SIPLEC) ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article 6 du décret du 29 décembre 2010 relatif aux certificats d'économies d'énergie, pris en application de l'article L. 221-6 du code de l'énergie : " La demande de certificats d'économies d'énergie (...) est accompagnée d'un dossier comportant les pièces dont la liste est arrêtée par le ministre chargé de l'énergie (...) Le demandeur de certificats d'économies d'énergie doit à l'appui de sa demande justifier son rôle actif et incitatif dans la réalisation de l'opération. Est considérée comme un rôle actif et incitatif toute contribution directe, quelle qu'en soit la nature, apportée, par le demandeur ou par l'intermédiaire d'une personne qui lui est liée contractuellement, à la personne bénéficiant de l'opération d'économies d'énergie et permettant la réalisation de cette dernière. Cette contribution doit être intervenue antérieurement au déclenchement de l'opération (...) " ; qu'en vertu du 3.1.2 de l'annexe 1 de l'arrêté du 29 décembre 2010 fixant la liste des éléments d'une demande de certificats d'économies d'énergie, pris sur le fondement de ce décret, le demandeur de certificats doit produire : " la description de la contribution du demandeur ; / la justification que cette contribution est directe et intervenue antérieurement au déclenchement de l'opération ; / une attestation sur l'honneur signée par le bénéficiaire de l'opération d'économies d'énergie du rôle actif et incitatif du demandeur dans la réalisation de cette opération " ; <br/>
<br/>
              2. Considérant qu'a été publiée, sur le site internet du ministère de l'écologie, du développement durable, des transports et du logement, une lettre d'information intitulée " Certificats d'économies d'énergie " datée du mois de décembre 2011, qui énonce notamment que " de façon à tenir compte des délais de mise en oeuvre de ces évolutions [réglementaires], l'administration, lors de l'instruction des dossiers de demande de certificats d'économies d'énergie, appliquera une période de tolérance portant sur la preuve de l'antériorité du rôle actif et incitatif par le demandeur. / Seront notamment acceptées les demandes de certificats d'économies d'énergie respectant l'ensemble des conditions suivantes : / - l'établissement, antérieur aux travaux, d'une convention de partenariat avec le professionnel ayant réalisé les travaux. Cette convention doit préciser le rôle de conseil ou d'information (...) du professionnel auprès du client final. / - Le client atteste sur l'honneur du rôle actif et incitatif du demandeur, et de son antériorité, dans la réalisation des travaux. Cette attestation peut être signée et remontée après les travaux./ - Le demandeur effectue sa demande avant le 31 mars 2012 (...)./ La période de tolérance concerne les opérations engagées du 1er janvier 2011 au 30 septembre 2011. Sous réserve du respect du reste du cadre réglementaire du dispositif, les opérations engagées avant le 30 septembre seront acceptées sous les conditions précitées. (...) " ; <br/>
<br/>
              Sur la fin de non-recevoir opposée par le ministre de l'écologie, du développement durable et de l'énergie :<br/>
<br/>
              3. Considérant que les termes de cette lettre d'information qu'attaque la société d'importation Leclerc, qui admettent, à titre temporaire, certains types de pièces pour justifier de l'antériorité de la contribution du demandeur de certificat d'économies d'énergie à la réalisation de l'opération justifiant la délivrance de celui-ci, présentent le caractère de dispositions impératives à caractère général ; que les dispositions attaquées constituent donc une décision susceptible d'être contestée par la voie du recours pour excès de pouvoir ; que la société d'importation Leclerc, qui est soumise, en sa qualité de distributeur de carburant, aux dispositions rappelées ci-dessus, justifie d'un intérêt lui donnant qualité pour les attaquer ; que, dès lors, la fin de non-recevoir opposée par le ministre doit être écartée ;<br/>
<br/>
              Sur la légalité des dispositions attaquées :<br/>
<br/>
              4. Considérant, en premier lieu, qu'il résulte des dispositions précitées de l'article 6 du décret du 29 décembre 2010 que la liste des pièces à fournir par les demandeurs de certificats d'économie d'énergie est fixée par le ministre chargé de l'énergie ; qu'il résulte des dispositions combinées des décrets du 25 novembre 2010 relatifs aux attributions respectives du ministre de l'économie, des finances et de l'industrie, et du ministre de l'écologie, du développement durable, des transports et du logement, que ces deux ministres sont compétents conjointement pour préparer et mettre en oeuvre la politique du Gouvernement en matière d'énergie et, par conséquent, pour définir la liste des pièces mentionnée ci-dessus ; que la décision attaquée a été signée par MmeA..., chargée de la sous-direction du climat et de la qualité de l'air, qui bénéficiait d'une délégation à l'effet de signer, au nom du ministre chargé de l'énergie, tous actes, arrêtés et décisions, à l'exclusion des décrets, dans la limite des attributions de sa sous-direction, par une décision en date du 26 juillet 2011 du directeur général de l'énergie et du climat, qui lui-même bénéficiait de plein droit de la délégation de signature des deux ministres en charge de l'énergie en vertu des dispositions de l'article 1er du décret du 27 juillet 2005 ; qu'il suit de là que le moyen tiré de ce que les dispositions attaquées seraient entachées d'incompétence doit être écarté ;<br/>
<br/>
              5. Considérant, en deuxième lieu, que les dispositions en cause, qui explicitent les moyens de justifier de l'antériorité de la contribution requise par l'article 6 du décret du 29 décembre 2010 précité, admettent notamment, à ce titre, la production d'une convention de partenariat avec un installateur - préalable à l'opération d'économie d'énergie - assortie d'une attestation individuelle du bénéficiaire de cette opération certifiant le rôle actif et incitatif du demandeur et son antériorité, sans exclure aucun autre mode de justification ; qu'elles ne modifient donc pas les règles fixées par le décret et l'arrêté du 29 décembre 2010 précités ; <br/>
<br/>
              6. Considérant, en troisième lieu, que les dispositions contestées ne visent que les demandes de certificats d'économies d'énergie présentées après leur publication sur le site internet du ministère, sans remettre en cause aucune situation juridiquement constituée ; qu'ainsi, et contrairement à ce que soutient la société requérante, elles ne présentent pas un caractère rétroactif  ;<br/>
<br/>
              7. Considérant, enfin, que la différence de traitement entre opérateurs susceptible de résulter de ces dispositions, qui est en rapport direct avec l'objet de la réglementation relative aux certificats d'économies d'énergie, n'est pas manifestement disproportionnée au regard de l'objectif d'intérêt général poursuivi ; que le moyen tiré de la méconnaissance du principe d'égalité doit, par suite, être écarté ; <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que la société requérante n'est pas fondée à demander l'annulation des dispositions attaquées ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de la société d'importation Leclerc est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la société d'importation Leclerc (SIPLEC), au ministre de l'écologie, du développement durable et de l'énergie.<br/>
Copie en sera adressée au ministre de l'économie et des finances.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
