<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028200591</ID>
<ANCIEN_ID>JG_L_2013_11_000000368636</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/20/05/CETATEXT000028200591.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  8ème sous-section jugeant seule, 04/11/2013, 368636, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-11-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368636</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 8ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; FOUSSARD</AVOCATS>
<RAPPORTEUR>Mme Marie-Astrid Nicolazo de Barmon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:368636.20131104</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 17 mai et 3 juin 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la Ville de Paris, représentée par son maire ; elle demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1305501 du 3 mai 2013 par laquelle le juge des référés du tribunal administratif de Paris, statuant sur la demande de la société Serfil, sur le fondement de l'article L. 521-1 du code de justice administrative, a suspendu l'exécution de l'arrêté du maire de Paris du 12 décembre 2012 abrogeant l'autorisation accordée à cette société pour l'installation de deux contre-terrasses ouvertes au droit de l'établissement qu'elle exploite au 48, rue d'Argout à Paris et a mis à la charge de la Ville le versement à cette société de la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société Serfil ; <br/>
<br/>
              3°) de mettre à la charge de la société Serfil le versement d'une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Astrid Nicolazo de Barmon, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Foussard, avocat de la Ville de Paris et à la SCP Waquet, Farge, Hazan, avocat de la société Serfil ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; <br/>
<br/>
              2. Considérant, en premier lieu, qu'il résulte de ces dispositions que la condition d'urgence à laquelle est subordonné le prononcé d'une mesure de suspension doit être regardée comme remplie lorsque la décision contestée préjudicie de manière suffisamment grave et immédiate à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il peut en aller ainsi, alors même que cette décision n'aurait un objet ou des répercussions que purement financiers ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ; que l'urgence doit être appréciée objectivement compte tenu de l'ensemble des circonstances de l'affaire ; <br/>
<br/>
              3. Considérant, d'une part, que pour estimer qu'il y avait urgence à suspendre l'arrêté du 12 décembre 2012 par lequel le maire de Paris a abrogé l'autorisation accordée le  29 juin 2009 à la société Serfil pour l'installation de deux contre-terrasses ouvertes en face du restaurant qu'elle exploite au 48, rue d'Argout à Paris, le juge des référés du tribunal administratif de Paris a relevé, par une appréciation souveraine exempte de dénaturation, qu'il résultait de l'instruction, notamment des documents d'expertise comptable que la société Serfil avait produits à l'appui de sa demande, que l'exécution de la décision litigieuse aurait pour effet de priver cette société d'une part substantielle de sa clientèle et de son chiffre d'affaires et risquait, par suite, de porter gravement atteinte à son équilibre économique ; qu'en statuant ainsi, le juge des référés n'a pas commis d'erreur de droit, dès lors que les conséquences financières d'un retrait d'autorisation d'occupation du domaine public sont au nombre des éléments susceptibles d'être pris en compte pour apprécier si la condition d'urgence, à laquelle est subordonné le prononcé d'une mesure de suspension, est satisfaite et que, pour caractériser une telle urgence, la société pouvait utilement faire valoir au soutien de sa demande que son activité ne serait pas rentable en l'absence des contre-terrasses, sans qu'y fassent obstacle ni le caractère précaire et révocable par nature de l'autorisation d'occupation du domaine public qui lui avait été accordée ni la circonstance que cette situation lui serait imputable au motif qu'elle aurait fait dépendre sa rentabilité de l'activité déployée sur le domaine public ; que le moyen tiré de la méconnaissance de l'article DG5 du règlement des étalages et terrasses de la ville de Paris est nouveau en cassation et, par suite, sans incidence sur le bien-fondé de l'ordonnance attaquée ; <br/>
<br/>
              4. Considérant, d'autre part, que le juge des référés a estimé, par une appréciation souveraine exempte de dénaturation, que l'occupation par les contre-terrasses de la société Serfil d'une superficie excédant celle qui avait été autorisée, telle qu'elle avait pu être ponctuellement constatée par les services de la Ville de Paris, ne suffisait pas à établir qu'il y avait urgence à exécuter l'arrêté mettant fin à l'autorisation d'installation de ces contre-terrasses, afin de préserver l'affectation du domaine public et la libre circulation sur la voie publique ; qu'en jugeant, dans ces conditions, et eu égard à l'importance économique particulière que revêtait l'utilisation des contre-terrasses litigieuses pour la société Serfil, notamment pendant la période estivale avant laquelle le tribunal administratif ne serait pas à même de juger la requête au fond, qu'il y avait urgence à suspendre la décision attaquée, le juge des référés n'a pas commis d'erreur de droit ; <br/>
<br/>
              5. Considérant, en deuxième lieu, que le juge des référés n'a pas entaché son ordonnance de dénaturation en estimant qu'il ressortait des pièces du dossier, confortées par les explications présentées à l'audience par la société Serfil et non contredites par la Ville de Paris, que la jardinière dont la Ville envisage la création ne serait pas installée précisément à l'emplacement des contre-terrasses de la société Serfil ; <br/>
<br/>
              6. Considérant, en troisième lieu, que pour estimer qu'il ne ressortait pas des pièces du dossier que la Ville de Paris aurait pris la même décision au seul motif de l'occupation excédentaire des contre-terrasses reprochée à la société Serfil, le juge des référés s'est livré à une appréciation souveraine des données de l'affaire, exempte de dénaturation, qui n'est pas susceptible d'être discutée devant le juge de cassation, et n'a pas commis d'erreur de droit ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que la Ville de Paris n'est pas fondée à demander l'annulation de l'ordonnance qu'elle attaque ; <br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la société Serfil, qui n'est pas, dans la présente instance, la partie perdante, la somme que demande la ville de Paris au titre des frais exposés par elle et non compris dans les dépens ; que, dans les circonstances de l'espèce, il y a lieu de mettre à la charge de la ville de Paris, au titre des mêmes dispositions, le versement à la société Serfil d'une somme de 3 000 euros ; <br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
 Article 1er : Le pourvoi de la Ville de Paris est rejeté.<br/>
<br/>
 Article 2 : La Ville de Paris versera à la société Serfil une somme de 3 000 euros au titre de dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
 Article 3 : La présente décision sera notifiée à la Ville de Paris et à la société Serfil.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
