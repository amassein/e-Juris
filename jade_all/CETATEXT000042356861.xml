<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042356861</ID>
<ANCIEN_ID>JG_L_2020_09_000000444514</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/35/68/CETATEXT000042356861.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 21/09/2020, 444514, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-09-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>444514</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:444514.20200921</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 16 septembre 2020 au secrétariat du contentieux du Conseil d'Etat, l'association le Conseil national du logiciel libre (CNLL), l'association Ploss Rhônes-Alpes, l'association Solibre, la société Nexedi, l'association Interhop, les hôpitaux français pour l'interopérabilité et le partage libre des algorithmes, Mme B... I..., M. C... A..., le Syndicat national des journalistes (SNJ), le Syndicat de la médecine générale (SMG), l'Union française pour une médecine libre (UFML), M. H... J..., M. D... G..., l'Union générale des ingénieurs, cadres et techniciens CGT (UGICT-CGT), l'Union fédérale médecins, ingénieurs, cadres, techniciens CGT santé et action sociale (UFMICT - CGT santé et action sociale), Mme L... K..., M. E... F..., l'association Constances, l'association les Actupiennes et l'Association française des hémophiles (AFH) demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-4 du code de justice administrative : <br/>
<br/>
              1°) à titre principal, d'ordonner la suspension de la centralisation et du traitement des données en lien avec l'épidémie de covid-19 sur la Plateforme des données de santé, ainsi que toutes mesures utiles pour garantir l'absence d'atteinte grave et manifestement illégale aux libertés fondamentales du fait de l'utilisation de cette plateforme pour le traitement des données en lien avec la covid-19 ;<br/>
<br/>
              2°) à titre subsidiaire, de renvoyer à la Commission nationale de l'informatique et des libertés la question de l'appréciation de la protection adéquate des données liées à la covid-19 centralisées et traitées au sein du Health Data Hub au regard du transfert de ces données vers les Etats-Unis ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Ils soutiennent que : <br/>
              - leur requête est recevable dès lors qu'ils justifient d'un intérêt leur donnant qualité pour agir, que l'ordonnance n° 440916 du 19 juin 2020 du juge des référés du Conseil d'Etat ne saurait être regardée comme une décision de rejet pur et simple et, enfin, que l'arrêt de la Cour de justice de l'Union européenne du 16 juillet 2020 constitue un élément de droit nouveau au sens de l'article L. 521-4 du code de justice administrative ;<br/>
              - l'arrêt de la Cour de justice, qui juge la décision de la Commission 2016/1250 du 12 juillet 2016 incompatible avec l'article 45 du règlement général sur la protection des données, justifie que de nouvelles mesures soient ordonnées ;<br/>
              - il résulte de ce même arrêt que les clauses contractuelles types ne suffisent pas à assurer la protection des données personnelles ;<br/>
              - il en résulte également une présomption d'ingérences des autorités américaines à l'égard des données à caractère personnel en vertu du CLOUD Act et du FISA.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". Aux termes de l'article L. 521-4 du même code : " Saisi par toute personne intéressée, le juge des référés peut, à tout moment, au vu d'un élément nouveau, modifier les mesures qu'il avait ordonnées ou y mettre fin ". <br/>
<br/>
              2.  En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              3. L'association le Conseil national du logiciel libre et plusieurs autres requérants ont demandé au juge des référés du Conseil d'Etat, sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner toutes mesures utiles pour faire cesser les atteintes graves et manifestement illégales qu'ils estimaient être portées au droit au respect de la vie privée par l'arrêté du ministre des solidarités et de la santé du 21 avril 2020 complétant l'arrêté du 23 mars 2020 prescrivant les mesures d'organisation et de fonctionnement du système de santé nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire, en ce qu'il confiait la collecte et le traitement de données de santé au groupement d'intérêt public " Plateforme des données de santé " ou " Health Data Hub ". Par une ordonnance n° 440916 du 19 juin 2020, le juge des référés du Conseil d'Etat a ordonné que la Plateforme des données de santé, d'une part, fournisse à la Commission nationale de l'informatique et des libertés, dans un délai de cinq jours, tous éléments relatifs aux procédés de pseudonymisation utilisés, propres à permettre à celle-ci de vérifier que les mesures prises assurent une protection suffisante des données de santé traitées sur le fondement de l'arrêté du 23 mars 2020 modifié et, d'autre part, complète, dans le même délai, les informations figurant sur son site internet relatives au projet portant sur l'exploitation des données de passages aux urgences pour l'analyse du recours aux soins et le suivi de la crise sanitaire de la covid-19, conformément aux motifs de sa décision. Il a rejeté le surplus des conclusions de la requête.<br/>
<br/>
              4. Par la présente requête, présentée sur le fondement de l'article L. 521-4 du code de justice administrative, le Conseil national du logiciel libre et les autres requérants demandent au juge des référés du Conseil d'Etat, à titre principal, d'ordonner la suspension de la centralisation et du traitement des données en lien avec l'épidémie de covid-19 sur la Plateforme des données de santé, ainsi que toutes mesures utiles pour garantir l'absence d'atteinte grave et manifestement illégale aux droits et libertés fondamentaux du fait de l'utilisation de cette plateforme pour le traitement des données en lien avec la covid-19, et, à titre subsidiaire, de renvoyer à la Commission nationale de l'informatique et des libertés la question de l'appréciation de la protection adéquate des données liées à la covid-19 centralisées et traitées au sein de la Plateforme des données de santé, au regard du transfert de ces données vers les Etats-Unis. <br/>
<br/>
              5.  Une telle requête ne tend pas à ce que les mesures prescrites par l'ordonnance du 19 juin 2020 soient modifiées, qu'il y soit mis fin ou que leur exécution soit assurée, mais à ce que de nouvelles mesures soient prises, consistant notamment dans la suspension de la centralisation et du traitement, par le biais de la Plateforme des données de santé, des données de santé utiles à la gestion de l'urgence sanitaire et à l'amélioration des connaissances sur le SARS-CoV-2, au demeurant désormais prévus non plus par l'arrêté du 23 mars 2020 modifié, abrogé par l'arrêté du 10 juillet 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans les territoires sortis de l'état d'urgence sanitaire et dans ceux où il a été prorogé, mais par l'article 30 de cet arrêté. Les mesures ainsi sollicitées excèdent celles que le juge des référés, saisi sur le fondement de l'article L. 521-4 du code de justice administrative, pourrait adopter. En outre, en l'absence de toute justification de l'urgence de l'affaire, la requête ne saurait, en tout état de cause, s'analyser utilement comme une nouvelle demande présentée au titre de l'article L. 521-2 du code de justice administrative.<br/>
<br/>
              6. Dans ces conditions, la requête du Conseil national du logiciel libre et des autres requérants, y compris leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, doit être rejetée, selon la procédure prévue par l'article L. 522-3 du même code.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête du Conseil national du logiciel libre et des autres requérants est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à l'association le Conseil national du logiciel libre, premier requérant dénommé, pour l'ensemble des requérants.<br/>
Copie en sera adressée au ministre des solidarités et de la santé, à la Plateforme des données de santé et à la Commission nationale de l'informatique et des libertés.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
