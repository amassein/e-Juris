<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028721827</ID>
<ANCIEN_ID>JG_L_2014_03_000000375279</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/72/18/CETATEXT000028721827.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 10/03/2014, 375279, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-03-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375279</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2014:375279.20140310</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 7 février 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par le département de l'Aveyron, dont le siège est hôtel du département, BP 724 à Rodez cedex (12007), représenté par le président du conseil général, le département de la Corse du sud, dont le siège est hôtel du département, palais Lantivy, BP 414 à Ajaccio cedex (20183), représenté par le président du conseil général, le département de la Côte d'Or, dont le siège est hôtel du département, 53 bis rue de la préfecture, BP 1601 à Dijon cedex (21035), représenté par le président du conseil général, le département du Loir-et-Cher, dont le siège est hôtel du département, place de la République à Blois (41020), représenté par le président du conseil général, et le département du Loiret, dont le siège est hôtel du département, 15 rue Vignat, BP 2019 à Orléans cedex 1 (45010), représenté par le président du conseil général ; les requérants demandent au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de la circulaire du 31 mai 2013 de la garde des sceaux, ministre de la justice, relative aux modalités de prise en charge des jeunes isolés étrangers, dispositif national de mise à l'abri, d'évaluation et d'orientation ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat, au profit de chaque requérant, la somme de 2 000 euros au titre de l'article L.761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              ils soutiennent que : <br/>
              - la condition d'urgence est remplie dès lors que, d'une part, la circulaire contestée préjudicie à l'intérêt public d'accueil des jeunes isolés étrangers en ce qu'elle les prive du droit d'être consultés sur le choix de leur département d'accueil et que, d'autre part, les départements se trouvent dans l'impossibilité de s'opposer à la prise en charge matérielle et financière de ces jeunes alors que l'obligation de cette prise en charge repose sur une circulaire illégale ; <br/>
              - la tardiveté de leur recours ne saurait justifier un rejet pour défaut d'urgence dans la mesure où l'urgence à suspendre s'est consolidée postérieurement à l'introduction de leur requête au fond ; <br/>
              - il existe un doute sérieux quant à la légalité de la circulaire contestée ;<br/>
              - la circulaire litigieuse, qui comporte des dispositions impératives, est entachée d'une incompétence ; <br/>
              - elle est entachée d'erreurs de droit en ce que, d'une part, elle définit un critère de placement des jeunes isolés étrangers autre que celui posé par la loi et, d'autre part, elle prive les jeunes isolés étrangers du droit d'être consultés sur le choix du département vers lequel ils seront orientés ; <br/>
              - elle est entachée d'une erreur d'appréciation dans la mesure où l'intérêt de l'enfant, critère posé par la loi, conduit en règle générale à maintenir les jeunes isolés étrangers dans le département où ils ont été identifiés ou à proximité de celui-ci ; <br/>
              - elle méconnaît les dispositions des articles 72 et 72-2 de la Constitution ; <br/>
<br/>
<br/>
              Vu la circulaire dont la suspension de l'exécution est demandée ;<br/>
              Vu la copie de la requête à fin d'annulation de cette circulaire ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 27 février 2014, présenté par la garde des sceaux, ministre de la justice, qui conclut, à titre principal, à l'irrecevabilité de la requête et, à titre subsidiaire, au rejet de la requête comme non fondée ;<br/>
<br/>
              elle soutient que : <br/>
              - la requête est irrecevable dès lors que la circulaire contestée ne présente aucun caractère impératif ;<br/>
              - la condition d'urgence n'est pas remplie dès lors, d'une part, que les requérants n'établissent pas concrètement que la circulaire contestée serait de nature à préjudicier de manière suffisamment grave et immédiate à l'intérêt public dont ils se prévalent et, d'autre part, que les requérants ont présenté leur recours plus de cinq mois après leur requête au fond ; <br/>
              - il n'existe pas de doute sérieux quant à la légalité de la circulaire litigieuse ;<br/>
              - la garde des sceaux, en prenant la circulaire contestée, n'a pas outrepassé les limites de sa compétence dès lors qu'elle se borne à émettre des préconisations en matière de prise en charge des mineurs isolés étrangers qui sont conformes aux dispositions en vigueur, sans interférer dans l'exercice de l'office des magistrats du parquet en la matière ;<br/>
              - la circulaire contestée n'est pas entachée d'erreur de droit ; <br/>
              - elle n'est pas entachée d'erreur manifeste d'appréciation ;<br/>
              - elle ne méconnaît pas les dispositions des articles 72 et 72-2 de la Constitution dès lors qu'elle ne transfère aucune compétence nouvelle ni aucune charge supplémentaire aux départements ; <br/>
<br/>
              Vu les observations, enregistrées le 27 février 2014, présentées par le ministre de l'intérieur, qui conclut aux mêmes fins que la garde des sceaux, ministre de la justice, par les mêmes moyens ;<br/>
<br/>
              il soutient en outre que : <br/>
              - la circulaire contestée ne méconnaît pas les principes constitutionnels de libre administration des collectivités et du droit à compensation financière pour toute extension de compétence dès lors que la protection de l'enfance est déjà une compétence obligatoire des départements ;<br/>
               - elle ne contrevient pas à l'intérêt supérieur de l'enfant, qui réside au contraire dans une orientation rapide pour un placement et une prise en charge effective ;<br/>
<br/>
              Vu les observations, enregistrées le 5 mars 2014, présentées par la ministre des affaires sociales et de la santé, qui conclut aux mêmes fins que la garde des sceaux, ministre de la justice, par les mêmes moyens ; <br/>
                          Vu les autres pièces du dossier ;<br/>
<br/>
                          Vu la Constitution ; <br/>
<br/>
              Vu le code civil ;<br/>
<br/>
              Vu le code de l'action sociale et des familles ;<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part le département de l'Aveyron et autres et, d'autre part, la garde des sceaux, ministre de la justice ainsi que le ministre de l'intérieur et la ministre des affaires sociales et de la santé ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 6 mars 2014 à 15 heures au cours de laquelle ont été entendus : <br/>
<br/>
               - le représentant du département de l'Aveyron et autres ; <br/>
<br/>
              - les représentants de la garde des sceaux, ministre de la justice ; <br/>
              et à l'issue de laquelle le juge des référés a clôturé l'instruction ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; <br/>
<br/>
              2. Considérant que, par la circulaire contestée, la garde des sceaux, ministre de la justice, a adressé aux procureurs généraux près les cours d'appel des instructions concernant les modalités de prise en charge des jeunes isolés étrangers en posant le principe d'une orientation nationale dans le choix du département définitif d'accueil de ces jeunes d'après une clé de répartition correspondant à la part de population de moins de 19 ans dans chaque département ; <br/>
<br/>
              Sur la fin de non-recevoir soulevée par la garde des sceaux, ministre de la justice :<br/>
<br/>
              3. Considérant que la circulaire contestée, en ce qu'elle contient des dispositions impératives, constitue une décision faisant grief susceptible de faire l'objet d'un recours pour excès de pouvoir et, par suite, d'une demande de suspension ; qu'en conséquence, la fin de non-recevoir soulevée par la garde des sceaux, ministre de la justice, doit être rejetée ;<br/>
<br/>
              Sur la requête :<br/>
<br/>
              4. Considérant que l'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ; que l'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire ;<br/>
<br/>
              5. Considérant, d'une part, que, si les requérants soutiennent que la circulaire contestée préjudicie à l'intérêt public d'accueil des jeunes isolés étrangers en ce qu'elle les prive du droit d'être consultés sur le choix de leur département d'accueil, il ne ressort pas des termes mêmes de cette circulaire qu'elle aurait pour effet d'exclure la possibilité pour le mineur isolé étranger d'être entendu par le juge ou la personne désignée par le juge à cet effet ; qu'elle ne saurait ainsi faire obstacle à la mise en oeuvre des dispositions de l'article 388-1 du code civil ;<br/>
<br/>
              6. Considérant, d'autre part, que les requérants, qui ont présenté leurs conclusions à fin de suspension plus de cinq mois après l'introduction de leur requête au fond, n'apportent ni dans leurs écritures ni dans leurs déclarations à l'audience d'éléments précis et suffisants de nature à établir que la circulaire contestée serait de nature à préjudicier de manière grave et immédiate à leur situation ;<br/>
<br/>
              7. Considérant, enfin, que la condition d'urgence exigée par l'article L. 521-1 du code de justice administrative étant distincte du point de savoir s'il existe des doutes sérieux sur la légalité des dispositions litigieuses, les départements requérants ne sauraient tirer parti de la circonstance qu'ils sont contraints d'accueillir et de prendre en charge les mineurs isolés étrangers qui leur sont affectés par la cellule nationale de la direction de la protection judiciaire de la jeunesse au besoin sur injonction des autorités préfectorales dans le cadre du contrôle de légalité, en application de dispositions qui seraient illégales, pour établir l'existence d'une situation d'urgence ; <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin de statuer sur l'existence d'un doute sérieux quant à la légalité de la circulaire contestée, que la condition d'urgence requise par l'article L. 521-1 du code de justice administrative ne peut être regardée comme remplie ; qu'il y a lieu, par suite, de rejeter la requête du département de l'Averyon et autres y compris leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête du département de l'Aveyron, du département de la Corse du sud, du département de la Côte d'Or, du département du Loir-et-Cher et du département du Loiret est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée au département de l'Aveyron, au département de la Corse du sud, au département de la Côte d'Or, au département du Loir-et-Cher, au département du Loiret, à la garde des sceaux, ministre de la justice, au ministre de l'intérieur et à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
