<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039198197</ID>
<ANCIEN_ID>JG_L_2019_10_000000416020</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/19/81/CETATEXT000039198197.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 09/10/2019, 416020, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416020</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Appréciation de la légalité</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>OCCHIPINTI ; SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>Mme Tiphaine Pinault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:416020.20191009</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un arrêt du 10 septembre 2015, la cour d'appel de Versailles statuant avant dire droit sur un litige opposant M. A... B... à la société Entreprise générale de nettoyage Arcade, a renvoyé les parties à saisir la juridiction administrative aux fins d'appréciation de la légalité du diplôme de licence en droit délivré le 24 mai 2011 à M. B... par le président de l'université de Reims au titre de l'année universitaire 1976-1977. Par un jugement n°1600526 du 27 septembre 2017, le tribunal administratif de Châlons-en-Champagne a déclaré ce diplôme illégal. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire enregistrés les 27 novembre et 12 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de la société Entreprise générale de nettoyage Arcade ;<br/>
<br/>
              3°) de mettre à la charge de la société Entreprise générale de nettoyage Arcade la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'éducation ;<br/>
              - la loi n°68-978 du 12 novembre 1968 ;<br/>
              - le décret n°62-768 du 10 juillet 1962 ;<br/>
              - le décret n°73-326 du 27 février 1973 ;<br/>
              - l'arrêté du 16 janvier 1976 portant dispositions relatives au deuxième cycle des études universitaires ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Tiphaine Pinault, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Occhipinti, avocat de M. B... et à la SCP Gatineau, Fattaccini, avocat de la société Entreprise générale de nettoyage Arcade.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêt du 10 septembre 2015, la cour d'appel de Versailles, estimant que le règlement du litige dont elle était saisie, opposant M. B... à la société Entreprise générale de nettoyage Arcade sur la validité d'une convention de prestation de service, était subordonné à l'appréciation de la légalité du diplôme de licence en droit de M. B..., a sursis à statuer et renvoyé les parties à saisir la juridiction administrative aux fins d'appréciation de la légalité de ce diplôme. Saisi de cette question préjudicielle par la société Entreprise générale de nettoyage Arcade, le tribunal administratif de Châlons-en-Champagne a déclaré, par un jugement du 27 septembre 2017, illégal le diplôme de licence en droit délivré le 24 mai 2011 à M. B... par le président de l'université de Reims au titre de l'année 1976-1977. M. B... se pourvoit en cassation contre ce jugement.<br/>
<br/>
              2. Par un arrêté du 16 janvier 1976, le secrétaire d'Etat aux universités a, en application des articles 20 et 20 bis de la loi du 12 novembre 1968 d'orientation de l'enseignement supérieur et du décret du 27 février 1973 relatif aux diplômes nationaux d'enseignement supérieur, réformé l'organisation des études du deuxième cycle universitaire en prévoyant que deux diplômes nationaux - la licence et la maîtrise - sanctionnent les études de ce deuxième cycle. Selon les dispositions des articles 7 et 8 de l'arrêté du 16 janvier 1976, la licence, " conçue comme un diplôme terminal ", est " délivrée aux candidats qui ont satisfait au contrôle des aptitudes et des connaissances correspondant à un enseignement d'une année (...) " à l'issue du diplôme d'études universitaires générales (DEUG) qui sanctionne les deux années du premier cycle. Selon les dispositions de l'article 18 de ce même arrêté, la maîtrise est " délivrée aux candidats titulaires de la licence qui ont satisfait au contrôle des aptitudes et des connaissances portant sur un enseignement d'une année ". <br/>
<br/>
              3. Pour la mise en oeuvre de cette réforme, l'article 25 du même arrêté a fixé respectivement au 1er octobre 1977 et au 1er octobre 1978 l'ouverture aux étudiants des formations de licence et de maîtrise habilitées suivant les nouvelles modalités. Par ailleurs, l'article 28 de cet arrêté a prévu que, dans les disciplines où la licence sanctionnait jusqu'alors quatre années d'études, ce qui était le cas de la licence en droit en vertu du décret du 10 juillet 1962, " les diplômes de licence délivrés sont, en vertu du présent arrêté, homologués en qualité de diplôme de maîtrise assortie de la mention correspondante " et que " dans les mêmes disciplines, à compter du 1er juin 1977, l'attestation de succès aux examens sanctionnant la troisième année d'études délivrée en application des dispositions actuellement en vigueur est, en vertu du présent arrêté, homologuée en qualité de diplôme de licence assortie de la mention correspondante ". Il résulte de la combinaison de ces dispositions transitoires que seuls les étudiants ayant subi, à compter de l'année universitaire 1976-1977, avec succès les examens sanctionnant la troisième année d'études peuvent prétendre à l'homologation en qualité de diplôme de licence d'une attestation de succès aux examens de troisième année d'études de droit.<br/>
<br/>
              4. Il ressort des pièces du dossier que M. B... a validé sa troisième année d'études de droit à l'université de Reims Champagne-Ardenne en juin 1971 et qu'il s'est ensuite inscrit en quatrième année d'études de droit en 1974, 1975, 1976, 1977, 1978, 1980, 1981 et 1983, sans toutefois réussir les épreuves lui permettant de valider cette quatrième année. En conséquence, en relevant que M. B..., qui avait été admis en juin 1971 aux examens validant sa troisième année d'études de droit, ne remplissait pas les conditions pour bénéficier de l'application des dispositions transitoires de l'article 28 de l'arrêté du 16 janvier 1976, le tribunal administratif n'a pas commis d'erreur de droit. Par suite, M. B..., qui ne peut utilement se prévaloir d'un droit à bénéficier d'une homologation de licence à raison de son inscription en quatrième année d'études de droit pendant plusieurs années consécutives entre 1974 et 1983, n'est pas fondé à demander l'annulation du jugement du 27 septembre 2017. <br/>
<br/>
              5. Il résulte de tout ce qui précède que le pourvoi de M. B... doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. Dans les circonstances de l'espèce, il y a lieu, sur le fondement de ces mêmes dispositions, de mettre à la charge de M. B... la somme de 1 500 euros à verser à la société Entreprise générale de nettoyage Arcade.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B... est rejeté. <br/>
Article 2 : M. B... versera à la société Entreprise générale de nettoyage Arcade une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3: La présente décision sera notifiée à M. A... B... et à la société Entreprise générale de nettoyage Arcade. <br/>
Copie en sera adressée à la ministre de l'enseignement supérieur, de la recherche et de l'innovation. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
