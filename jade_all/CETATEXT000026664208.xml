<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026664208</ID>
<ANCIEN_ID>JG_L_2012_11_000000329345</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/66/42/CETATEXT000026664208.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 21/11/2012, 329345, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-11-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>329345</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Bernard Stirn</PRESIDENT>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Jean-Claude Hassan</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:329345.20121121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 1er juillet 2009 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 1er, 2 et 4 de l'arrêt n° 08NT00882 du 25 mai 2009 par lequel la cour administrative d'appel de Nantes, faisant partiellement droit à la requête de Mme Jacqueline A et réformant le jugement n° 05-4303 du 5 février 2008 du tribunal administratif d'Orléans, a déchargé l'intéressée de la cotisation supplémentaire d'impôt sur le revenu à laquelle elle a été assujettie au titre de l'année 2003 à raison de l'imposition d'une somme de 106 800 euros et a mis à la charge de l'Etat la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la requête d'appel de Mme A ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Claude Hassan, Conseiller d'Etat,<br/>
<br/>
              - les observations de la SCP Delaporte, Briard, Trichet, avocat de Mme A,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Delaporte, Briard, Trichet, avocat de Mme A ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 29 du code général des impôts dans sa rédaction applicable à l'imposition en litige : " (...) le revenu brut des immeubles ou parties d'immeubles donnés en location, est constitué par le montant des recettes brutes perçues par le propriétaire, augmenté du montant des dépenses incombant normalement à ce dernier et mises par les conventions à la charge des locataires et diminué du montant des dépenses supportées par le propriétaire pour le compte des locataires. (...) / Dans les recettes brutes de la propriété sont comprises notamment celles qui proviennent de la location du droit d'affichage ou du droit de chasse, de la concession du droit d'exploitation des carrières, de redevances tréfoncières ou autres redevances analogues ayant leur origine dans le droit de propriété ou d'usufruit " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un acte notarié en date du 18 juin 2003, il a été mis fin au bail par lequel Mme A donnait en location à la SAS Euro-Textile des locaux commerciaux situés dans un immeuble dans lequel elle possédait en outre des locaux d'habitation, et que la société a acquis le même jour les locaux ayant fait l'objet du bail résilié, afin d'y poursuivre son activité en continuant à bénéficier des aménagements qu'elle y avait apportés ; que la société jusque là locataire a versé à Mme A une somme de 106 800 euros en contrepartie de la renonciation de cette dernière à exiger la remise en l'état des locaux prévue par le bail et en réparation du préjudice, et notamment du préjudice visuel, que lui causaient, en sa qualité de propriétaire des locaux d'habitation mentionnés plus haut, les aménagements dont il s'agit ;<br/>
<br/>
              3. Considérant, en premier lieu, que la cour administrative d'appel de Nantes a porté sur les faits soumis à son examen une appréciation souveraine exempte de dénaturation en estimant que l'indemnité versée par son locataire à Mme A, en raison de l'absence de travaux de remise en état, compensait la dépréciation que cette dernière faisait subir à son bien ; que c'est sans commettre d'erreur de droit qu'elle a par suite jugé que la somme ainsi versée ne constitue pas un revenu brut de l'immeuble au sens et pour l'application du premier alinéa de l'article 29 du code général des impôts cité plus haut ;<br/>
<br/>
              4. Considérant, en second lieu, qu'il résulte des termes mêmes du second alinéa de cet article 29 que ne sont assimilées au revenu brut de l'immeuble que les redevances versées en rémunération d'un droit attaché à la propriété et concédé à des tiers ; que les dispositions de cet alinéa n'ont ni pour objet ni pour effet de permettre de qualifier comme revenu de l'immeuble toutes les sommes versées à un propriétaire à raison de son droit de propriété ;<br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède qu'en jugeant que l'indemnité versée à Mme A revêtait un caractère non imposable, la cour administrative d'appel de Nantes n'a commis ni erreur de qualification des faits ni erreur de droit ; que le pourvoi du ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat doit par suite être rejeté ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 2 500 euros demandée par Mme A au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi du ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat est rejeté.<br/>
<br/>
Article 2 : L'Etat versera à Mme A la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de l'économie et des finances et à Mme Jacqueline A.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
