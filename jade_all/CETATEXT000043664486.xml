<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043664486</ID>
<ANCIEN_ID>JG_L_2021_06_000000438874</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/66/44/CETATEXT000043664486.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 14/06/2021, 438874, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438874</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Sylvain Monteillet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:438874.20210614</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un autre mémoire, enregistrés le 19 février 2020 et les 15 janvier et 18 mai 2021 au secrétariat du contentieux du Conseil d'Etat, le syndicat Solidaires Finances Publiques, le syndicat CFTC 21 Finances Publiques, section de la Côte d'Or, et le syndicat Finances Bourgogne-Franche-Comté CFDT demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du ministre de l'action et des comptes publics du 17 décembre 2019 portant ajustement de périmètre des services déconcentrés de la direction générale des finances publiques en tant qu'il crée dans le département de la Côte d'Or, à compter du 1er janvier 2020, le service des impôts des particuliers (SIP) de Dijon et Amendes, par fusion du SIP de Dijon Sud et Amendes et du SIP de Dijon Nord ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 83-634 du 13 juillet 1983, notamment son article 9;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ; <br/>
              - la loi n° 2010-751 du 5 juillet 2010 ;<br/>
              - la loi n° 2013-1168 du 18 décembre 2013 ;<br/>
              - le décret n° 82-453 du 28 mai 1982 ; <br/>
              - le décret n° 2011-184 du 15 février 2011 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sylvain Monteillet, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Par un arrêté du 17 décembre 2019 portant ajustement de périmètre des services déconcentrés de la direction générale des finances publiques, le ministre de l'action et des comptes publics a décidé d'évolutions du périmètre relatif à la fiscalité des particuliers au sein des postes comptables énumérés à l'annexe I de cet arrêté, parmi lesquelles la fusion du service des impôts des particuliers (SIP) de Dijon Sud et Amendes et du SIP de Dijon Nord afin de créer le SIP de Dijon et Amendes. Les syndicats Solidaires Finances Publiques, CFTC 21 Finances Publiques, section de la Côte d'Or, et Finances Bourgogne-Franche-Comté CFDT demandent l'annulation de cet arrêté en tant qu'il concerne cette fusion.<br/>
<br/>
              Sur le cadre juridique :<br/>
<br/>
              2. Les articles 15 et 16 de la loi du 11 janvier 1984 prévoient, respectivement et dans leur rédaction issue des lois n° 2013-1168 du 18 décembre 2013 et n° 2010-751 du 5 juillet 2010, que, dans toutes les administrations de l'Etat et dans les établissements publics ne présentant pas un caractère industriel et commercial, les comités techniques " connaissent des questions relatives à l'organisation et au fonctionnement des services " et les comités d'hygiène de sécurité et des conditions de travail ont " pour mission de contribuer à la protection de la santé physique et mentale et de la sécurité des agents dans leur travail, à l'amélioration des conditions de travail et de veiller à l'observation des prescriptions légales prises en ces matières ". L'article 34 du décret du 15 février 2011 relatif aux comités techniques dans les administrations et les établissements publics de l'Etat, pris pour l'application de l'article 15 de la loi du 11 janvier 1984 et en vigueur à la date de l'arrêté attaqué, énumère les questions et projets de textes sur lesquels les comités techniques sont obligatoirement consultés, qui incluent ceux relatifs à l'organisation et au fonctionnement des administrations, établissements ou services. L'article 47 du décret du 28 mai 1982, pris pour l'application de l'article 16 de la loi du 11 janvier 1984 et dans sa rédaction en vigueur à la date de l'arrêté attaqué, précise que les comités d'hygiène, de sécurité et des conditions de travail exercent leurs missions " sous réserve des compétences des comités techniques ". Le 1° de l'article 57 du même décret prévoit que le comité d'hygiène, de sécurité et des conditions de travail est notamment consulté " sur les projets d'aménagement importants modifiant les conditions de santé et de sécurité ou les conditions de travail (...) ". <br/>
<br/>
              3. Il résulte de ces dispositions qu'une question ou un projet de disposition ne doit être soumis à la consultation du comité d'hygiène, de sécurité et des conditions de travail que si le comité technique ne doit pas lui-même être consulté sur la question ou le projet de disposition en cause. Le comité d'hygiène, de sécurité et des conditions de travail ne doit ainsi être saisi que d'une question ou projet de disposition concernant exclusivement la santé, la sécurité ou les conditions de travail. En revanche, lorsqu'une question ou un projet de disposition concerne ces matières et l'une des matières énumérées à l'article 34 du décret du 15 février 2011, seul le comité technique doit être obligatoirement consulté. Ce comité peut, le cas échéant, saisir le comité d'hygiène, de sécurité et des conditions de travail de toute question qu'il juge utile de lui soumettre. En outre, l'administration a toujours la faculté de consulter le comité d'hygiène, de sécurité et des conditions de travail. Dans le cas où, sans y être légalement tenue, elle sollicite l'avis d'un organisme consultatif au sujet, notamment, d'un projet de réorganisation des services, l'administration doit procéder à cette consultation dans des conditions régulières.<br/>
<br/>
              4. Aux termes de l'article 55 du décret du 28 mai 1982 relatif à l'hygiène et à la sécurité du travail ainsi qu'à la prévention médicale dans la fonction publique : " Le comité d'hygiène, de sécurité et des conditions de travail peut demander au président de faire appel à un expert agréé conformément aux articles R. 4614-6 et suivants du code du travail : / (...) 2° En cas de projet important modifiant les conditions de santé et de sécurité ou les conditions de travail, prévu à l'article 57. (...) / La décision de l'administration refusant de faire appel à un expert doit être substantiellement motivée. Cette décision est communiquée au comité d'hygiène, de sécurité et des conditions de travail ministériel. / En cas de désaccord sérieux et persistant entre le comité et l'autorité administrative sur le recours à l'expert agréé, la procédure prévue à l'article 5-5 peut être mise en oeuvre ". <br/>
<br/>
              5.  Il résulte de l'ensemble de ces dispositions que le " projet important " s'entend de tout projet qui affecte de manière déterminante les conditions de santé, de sécurité ou de travail d'un nombre significatif d'agents, le critère du nombre de salariés ne déterminant toutefois pas, à lui seul, l'importance du projet.<br/>
<br/>
              Sur la régularité de la procédure d'élaboration de l'arrêté litigieux : <br/>
<br/>
              6. Il ressort des pièces du dossier que le projet de fusion du SIP de Dijon Sud et Amendes et du SIP de Dijon Nord a été mis à l'ordre du jour des séances du comité technique local de la Côte d'Or des 18 novembre et 29 novembre 2019, ainsi qu'à l'ordre du jour des séances du comité d'hygiène, de sécurité et des conditions de travail (CHSCT) de la Côte d'Or des 4 novembre et 13 novembre 2019 et que, au cours de la séance du CHSCT du 13 novembre 2019, les représentants du personnel ont sollicité le recours à une expertise agréée, en application de l'article 57 du décret du 28 mai 1982 cité au point 2, au motif que, selon eux, une telle expertise était justifiée par le caractère important du projet modifiant les conditions de santé et de sécurité ou les conditions de travail. Toutefois, il ressort également des pièces du dossier que cette fusion n'a pas modifié la résidence administrative des agents relevant désormais du SIP de Dijon et Amendes, dont au demeurant le lieu de travail est maintenu dans le même immeuble que celui dans lequel étaient installés le SIP de Dijon Sud et Amendes et le SIP de Dijon Nord, qu'elle n'a pas entraîné, à la date de l'arrêté attaqué, de changement important dans les missions exercées par les agents et la sectorisation géographique des services et qu'elle a affecté la situation d'un agent, conformément à ses souhaits. Enfin, si les syndicats requérants font valoir que cinq emplois ont été supprimés au sein du nouveau SIP de Dijon et Amendes au titre des années 2020 et 2021, il n'est pas établi que ces suppressions étaient la conséquence du projet de fusion des SIP décidé par l'arrêté attaqué. Il s'ensuit que ce projet ne peut être regardé, dans les circonstances de l'espèce, comme un projet important au sens des dispositions citées au point 4. <br/>
<br/>
              7. Dès lors, les syndicats requérants ne sont pas fondés à soutenir que l'arrêté du 17 décembre 2019, en tant qu'il concerne la fusion du SIP de Dijon Sud et Amendes et du SIP de Dijon Nord, aurait été pris au terme d'une procédure irrégulière au motif qu'il est intervenu sans attendre que l'administration ait répondu à la demande d'expertise formulée lors de la réunion du CHSCT du 13 novembre 2019, le directeur régional des finances publiques ayant refusé de faire droit à cette demande par courrier du 18 décembre 2019.<br/>
<br/>
              8. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les fins de non-recevoir soulevées par le ministre de l'économie, des finances et de la relance, que les syndicats Solidaires Finances Publiques, CFTC 21 Finances Publiques, section de la Côte d'Or, et Finances Bourgogne-Franche-Comté CFDT ne sont pas fondés à demander l'annulation de l'arrêté qu'ils attaquent.<br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'État qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête des syndicats Solidaires Finances Publiques, CFTC 21 Finances Publiques, section de la Côte d'Or, et Finances Bourgogne-Franche-Comté CFDT est rejetée.<br/>
Article 2 : La présente décision sera notifiée aux syndicats Solidaires Finances Publiques, CFTC 21 Finances Publiques, section de la Côte d'Or, et Finances Bourgogne-Franche-Comté CFDT et au ministre de l'économie, des finances et de la relance.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
