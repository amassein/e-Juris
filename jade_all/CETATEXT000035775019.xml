<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035775019</ID>
<ANCIEN_ID>JG_L_2017_10_000000412620</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/77/50/CETATEXT000035775019.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 11/10/2017, 412620, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-10-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412620</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET ; HAAS</AVOCATS>
<RAPPORTEUR>M. Vincent Villette</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:412620.20171011</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. C...A...et autres ont demandé au tribunal administratif de Caen d'annuler la décision du 11 janvier 2017 par laquelle la maire de Granville a fixé la liste des conseillers municipaux en exercice de ladite commune et d'enjoindre au préfet de la Manche de procéder aux élections d'un nouveau conseil municipal sans délai. Par un jugement n° 1700069 du 9 février 2017, le tribunal a, d'une part, annulé la décision par laquelle la maire de Granville a écarté le renoncement à siéger au conseil municipal de Mme E...et l'a appelée à siéger en remplacement d'un conseiller démissionnaire, d'autre part, enjoint à la maire de transmettre au préfet de la Manche la démission de MmeE..., enfin mis à la charge de la commune de Granville la somme de 1 500 euros à verser aux requérants au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par une décision n° 408295 du 19 juillet 2017, le Conseil d'Etat statuant au contentieux a, sur appel formé par la commune de Granville, annulé ce jugement en tant qu'il avait mis à la charge de cette commune une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative, rejeté les conclusions présentées à ce titre par M. A...et autres, et rejeté le surplus des conclusions d'appel de la commune.  <br/>
<br/>
Recours en tierce opposition<br/>
<br/>
              Par une requête en tierce opposition et un mémoire en réplique, enregistrés le 19 juillet et le 9 août 2017 au secrétariat du contentieux du Conseil d'Etat, Mme B...E...et autres demandent au Conseil d'Etat : <br/>
<br/>
              1°) de déclarer non avenu le jugement n° 1700069 du 9 février 2017 ; <br/>
<br/>
              2°) de rejeter la requête de M. A...et autres.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Villette, auditeur,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de Mme B...E..., et autres et à Me Haas, avocat de M. C...A...et autres ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 18 septembre 2017, présentée par M. A... et autres ; <br/>
<br/>
              Vu la note en délibéré, enregistrée le 19 septembre 2017, présentée par Mme E... et autres ; <br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Mme E...et autres, par une requête en tierce opposition, demandent au Conseil d'Etat de déclarer non avenu le jugement du 9 février 2017 par lequel le tribunal administratif de Caen a, d'une part, annulé la décision par laquelle la maire de Granville a écarté le renoncement à siéger au conseil municipal de Mme E...et l'a appelée à siéger en remplacement d'un conseiller démissionnaire, d'autre part, enjoint à la maire de transmettre au préfet de la Manche la démission de MmeE....<br/>
<br/>
              2. Aux termes de l'article R. 832-1 du code de justice administrative : " Toute personne peut former tierce opposition à une décision juridictionnelle qui préjudicie à ses droits, dès lors que ni elle ni ceux qu'elles représentent n'ont été présents ou régulièrement appelés dans l'instance ayant abouti à cette décision ".<br/>
<br/>
              Sur la compétence du Conseil d'Etat :<br/>
<br/>
              3. Si la tierce opposition à un jugement doit, en principe, être portée devant la juridiction qui a prononcé ce jugement, il peut toutefois en aller différemment en matière électorale, compte tenu notamment des articles R. 120 et R. 121 du code électoral qui impartissent au tribunal administratif un délai de deux mois, à compter de l'enregistrement de la requête au greffe, pour statuer sur les opérations électorales municipales, à peine de dessaisissement. <br/>
<br/>
              4. Il ressort des pièces de la procédure que la demande sur laquelle le tribunal administratif de Caen a statué par son jugement du 9 février 2017 avait été enregistrée le 16 janvier 2017 au greffe de ce tribunal. Il suit de là que la tierce-opposition formée par les requérants contre ce jugement, introduit plus de deux mois après cette date, ressortit à la compétence du Conseil d'Etat, et non à celle du tribunal administratif de Caen. <br/>
<br/>
              Sur la recevabilité de la tierce-opposition :<br/>
<br/>
              5. Une tierce-opposition contre le jugement rendu par un tribunal administratif formée après qu'une partie a frappé ce jugement d'appel est irrecevable. La personne qui aurait eu qualité pour former tierce-opposition est dans ce cas recevable à intervenir dans la procédure d'appel ou, si elle n'a été ni présente ni représentée devant la juridiction d'appel, à former tierce-opposition contre l'arrêt rendu par celle-ci, s'il préjudicie à ses droits. La personne recevable à intervenir dans la procédure d'appel acquiert la qualité de partie dans cette instance. <br/>
<br/>
              6. Il ressort des pièces de la procédure que les requérants ont introduit leur requête en tierce opposition, devant le Conseil d'Etat, le 19 juillet à 18h07, alors que le Conseil d'Etat statuant au contentieux avait auparavant, par une décision lue ce même jour à 14h, statué sur l'appel formé par la commune de Granville contre le jugement du tribunal administratif de Caen du 19 juillet 2017. Par suite, s'il reste loisible aux requérants de former tierce-opposition contre cette décision du Conseil d'Etat, la tierce-opposition qu'ils ont formée contre le jugement du tribunal administratif de Caen du 9 février 2017 est en revanche irrecevable, et doit par suite être rejetée. <br/>
<br/>
              7. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. A... et autres au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de Mme E...et autres est rejetée.<br/>
Article 2 : Les conclusions présentées par M. A...et autres au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à Mme B...E..., première dénommée et à M. C... A..., premier dénommé. Les autres requérants seront informés de la présente décision respectivement par la SCP Monod-Colin-Stoclet et par Maître D...Haas, avocats au Conseil d'Etat et à la Cour de cassation, qui les représentent devant le Conseil d'Etat. <br/>
Copie en sera transmise au ministre d'Etat, ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
