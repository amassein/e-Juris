<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030618757</ID>
<ANCIEN_ID>JG_L_2015_05_000000369373</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/61/87/CETATEXT000030618757.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 20/05/2015, 369373, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-05-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369373</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LESOURD</AVOCATS>
<RAPPORTEUR>M. Julien Anfruns</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:369373.20150520</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              La société Universal Aviation France (UAF) a demandé au tribunal administratif de Cergy-Pontoise la décharge des cotisations supplémentaires de retenue à la source, d'impôt sur les sociétés et de contribution à l'impôt sur les sociétés portant sur les exercices clos les 30 juin 2003, 2004 et 2005. Par deux jugements nos 0711296 et 0711297 du 21 octobre 2010, le tribunal administratif de Cergy-Pontoise a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 10VE03950 du 1er mars 2013, la cour administrative d'appel de Versailles a rejeté l'appel formé contre ce jugement par la société Universal Aviation France.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés respectivement les 14 juin et 10 septembre 2013 et le 5 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, la société Universal Aviation France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'État la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son article 55 ;<br/>
              - la convention signée le 31 août 1994 entre les Etats-Unis d'Amérique et la République française, destinée à éviter les doubles impositions en matière d'impôts sur le revenu et sur la fortune ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Julien Anfruns, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lesourd, avocat de la société Universal Aviation France (UAF) ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Universal Aviation France (UAF), dont le siège social est situé au Bourget, exerce une activité de fournitures de biens et de services pour l'assistance au sol d'aéronefs privés ; qu'elle est détenue à 96,4 % par la société de droit américain Universal Weather and Aviation Inc. (UWA) ; que la société UAF a fait l'objet d'une vérification de comptabilité au titre des exercices clos les 30 juin 2003, 2004 et 2005, à l'issue de laquelle l'administration fiscale a réintégré au bénéfice imposable de la société des intérêts non réclamés au titre d'avances consenties à la société mère UWA, des renonciations à produits résultant d'une insuffisante facturation de frais administratifs à certains clients et une fraction des commissions de gestion (" managements fees ") déduites par la société ; que, par deux jugements du 21 octobre 2010, le tribunal administratif de Cergy-Pontoise a rejeté la demande de décharge des cotisations supplémentaires de retenue à la source, d'impôt sur les sociétés et de contribution sur l'impôt sur les sociétés au titre des trois exercices ; que, par un arrêt du 1er mars 2013, la cour administrative d'appel de Versailles a rejeté l'appel de la société contre ce jugement ;<br/>
<br/>
              Sur l'arrêt en tant qu'il statue sur le chef de redressement relatif aux frais administratifs facturés par la société UAF :<br/>
<br/>
              2. Considérant que la facturation de frais administratifs, avec, le cas échéant, certaines réductions décidées dans l'intérêt de l'entreprise, constitue un acte de gestion courant pour une entreprise ; qu'une telle pratique ne saurait être regardée comme un des cas de renonciation à recettes relevant, en principe, d'une gestion commerciale anormale ; qu'il appartient, dès lors, à l'administration d'apporter la preuve de ce qu'elle relève d'une gestion commerciale anormale ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, durant les exercices clos en 2004 et 2005, la SARL UAF a facturé à ses clients des frais administratifs à des taux variant entre 0 % et 15 % du montant des services effectués ; que l'administration a estimé que ces facturations constituaient un acte anormal de gestion, en ce que la SARL UAF avait ainsi renoncé à percevoir une fraction des recettes qui lui étaient dues, sans justifier d'un intérêt commercial à ne pas facturer des frais à hauteur de 15 % à l'ensemble de ses clients ; qu'elle a procédé, sur le fondement de l'acte anormal de gestion, à un rehaussement du bénéfice imposable, correspondant à l'application du taux de 15 % à l'ensemble des clients sur chacune des opérations réalisées ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui est dit au point 2 qu'il revenait à l'administration d'apporter la preuve du caractère anormal de la facturation en litige ; que, par suite, en admettant le bien-fondé des rehaussements en litige sans rechercher si l'administration apportait une telle preuve, la cour a méconnu les règles gouvernant la charge de la preuve et commis une erreur de droit ; qu'il en résulte, sans qu'il soit besoin d'examiner les autres moyens du pourvoi sur ce chef de redressement, que son arrêt doit être annulé en tant qu'il statue sur le chef de redressement relatif aux frais administratifs facturés par la société UAF;<br/>
<br/>
              Sur l'arrêt en tant qu'il statue sur le chef de redressement relatif à la facturation des commissions de gestion :<br/>
<br/>
              5. Considérant que si une convention bilatérale conclue en vue d'éviter les doubles impositions peut, en vertu de l'article 55 de la Constitution, conduire à écarter, sur tel ou tel point, la loi fiscale nationale, elle ne peut pas, par elle-même, directement servir de base légale à une décision relative à l'imposition ; que, par suite, il incombe au juge de l'impôt, lorsqu'il est saisi d'une contestation relative à une telle convention, de se placer d'abord au regard de la loi fiscale nationale pour rechercher si, à ce titre, l'imposition contestée a été valablement établie et, dans l'affirmative, sur le fondement de quelle qualification ; qu'il lui appartient ensuite, le cas échéant, en rapprochant cette qualification des stipulations de la convention, de déterminer, en fonction des moyens invoqués devant lui ou même, s'agissant de déterminer le champ d'application de la loi, d'office, si cette convention fait ou non obstacle à l'application de la loi fiscale ; <br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'administration fiscale s'est fondée sur les articles 39-1 et 57 du code général des impôts pour réintégrer des charges qu'elle a jugées non déductibles, puis sur les articles 109 et 119 bis de ce même code pour appliquer à ces sommes, qu'elle a regardées comme des revenus distribués, la retenue à la source prévue par ces articles, le taux de cette dernière se trouvant limité par la convention fiscale franco-américaine du 31 août 1994 ; que, dans sa proposition de rectification du 25 septembre 2006, l'administration fiscale s'est fondée, non pas sur l'article 9 de cette convention, relatif aux prix de transfert, mais sur le paragraphe 7 de l'article 10, relatif à l'imposition des dividendes ; qu'il suit de là qu'en jugeant que le moyen tiré de la contestation par la société de l'applicabilité de l'article 9 de la convention franco-américaine du 31 août 1994 était inopérant, la cour a, sans méconnaître son office ni entacher son arrêt de contradiction de motifs, porté sur les faits de l'espèce une appréciation exempte de dénaturation ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que la société UAF est seulement fondée à demander l'annulation de l'arrêt qu'elle attaque en tant qu'il rejette ses conclusions tendant à la décharge des cotisations supplémentaires de retenue à la source, d'impôt sur les sociétés et de contribution sur l'impôt sur les sociétés, ainsi que des pénalités correspondantes, qui ont été mises à sa charge au titre des exercices clos les 30 juin 2003, 2004 et 2005, en raison de la remise en cause des frais administratifs facturés à ses clients ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'État une somme de 2 500 euros à verser à la société UAF au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles est annulé en tant qu'il rejette les conclusions de la société Universal Aviation France (UAF) tendant à la décharge des cotisations supplémentaires de retenue à la source, d'impôt sur les sociétés et de contribution sur l'impôt sur les sociétés, ainsi que des pénalités correspondantes, auxquelles elle a été assujettie au titre des exercices clos les 30 juin 2003, 2004 et 2005 en raison de la remise en cause des frais administratifs facturés à ses clients.<br/>
Article 2 : L'affaire est renvoyée, dans la mesure de la cassation prononcée, à la cour administrative d'appel de Versailles.<br/>
Article 3 : L'État versera à la société Universal Aviation France (UAF) une somme de 2 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions du pourvoi est rejeté.<br/>
Article 5 : La présente décision sera notifiée à la société Universal Aviation France (UAF) et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
