<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034808227</ID>
<ANCIEN_ID>JG_L_2017_05_000000396062</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/80/82/CETATEXT000034808227.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 24/05/2017, 396062</TITRE>
<DATE_DEC>2017-05-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396062</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BRIARD</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:396062.20170524</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Nice d'annuler la décision du 5 février 2013 par laquelle la commission de médiation des Alpes-Maritimes a refusé de le reconnaître comme prioritaire et devant être relogé en urgence sur le fondement de l'article L. 441-2-3 du code de la construction et de l'habitation. Par un jugement n° 1300642 du 5 décembre 2013, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14MA00360 du 9 novembre 2015, la cour administrative d'appel de Marseille, statuant sur l'appel de M.A..., a annulé ce jugement et la décision attaquée. <br/>
<br/>
              Par un pourvoi, enregistré le 12 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, le ministre du logement, de l'égalité des territoires et de la ruralité demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la construction et de l'habitation ;<br/>
<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Briard, avocat de M.A....<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A..., qui habite à Montpellier dans un logement social et est handicapé à 80 %, a constamment sollicité le bénéfice d'un logement social à Nice à compter d'avril 2002 ; qu'il a présenté devant la commission de médiation des Alpes-Maritimes une demande, enregistrée le 20 août 2012, tendant à se voir reconnaître comme prioritaire et devant être relogé en urgence en invoquant l'ancienneté de sa demande d'attribution d'un logement social et son handicap ; que la commission de médiation a rejeté sa demande par une décision du 5 février 2013, au motif qu'il était locataire d'un logement social adapté à ses besoins et que le seul fait d'avoir déposé une demande depuis 125 mois ne suffisait pas à conférer un caractère prioritaire à sa demande ; que, par un jugement du 5 décembre 2013, le tribunal administratif de Nice a rejeté son recours pour excès de pouvoir contre cette décision ; que le ministre du logement, de l'égalité des territoires et de la ruralité se pourvoit en cassation contre l'arrêt du 9 novembre 2015 par lequel la cour administrative d'appel de Marseille, statuant sur la requête de M.A..., a annulé ce jugement et la décision du 5 février 2013 au motif que l'intéressé avait établi devant elle que le logement qu'il occupait ne présentait pas un caractère décent ; <br/>
<br/>
              2. Considérant qu'aux termes des deux premiers alinéas du II de l'article L. 441-2-3 du code de la construction et de l'habitation : " La commission de médiation peut être saisie par toute personne qui, satisfaisant aux conditions réglementaires d'accès à un logement locatif social, n'a reçu aucune proposition adaptée en réponse à sa demande de logement dans le délai fixé en application de l'article L. 441-1-4. / Elle peut être saisie sans condition de délai lorsque le demandeur, de bonne foi, est dépourvu de logement, menacé d'expulsion sans relogement, hébergé ou logé temporairement dans un établissement ou un logement de transition, un logement-foyer ou une résidence hôtelière à vocation sociale, logé dans des locaux impropres à l'habitation ou présentant un caractère insalubre ou dangereux. Elle peut également être saisie, sans condition de délai, lorsque le demandeur est logé dans des locaux manifestement suroccupés ou ne présentant pas le caractère d'un logement décent, s'il a au moins un enfant mineur, s'il présente un handicap au sens de l'article L. 114 du code de l'action sociale et des familles ou s'il a au moins une personne à charge présentant un tel handicap " ; qu'aux termes de l'article R. 441-14-1 du même code : " La commission, saisie sur le fondement du II (...) de l'article L. 441-2-3, se prononce sur le caractère prioritaire de la demande et sur l'urgence qu'il y a à attribuer au demandeur un logement (...), en tenant compte notamment des démarches précédemment effectuées dans le département ou en Ile-de-France dans la région. / Peuvent être désignées par la commission comme prioritaires et devant être logées d'urgence en application du II de l'article L. 441-2-3 les personnes de bonne foi qui satisfont aux conditions réglementaires d'accès au logement social et qui répondent aux caractéristiques suivantes : - ne pas avoir reçu de proposition adaptée à leur demande dans le délai fixé en application de l'article L. 441-1-4 (...) / - être logées dans des locaux impropres à l'habitation, ou présentant un caractère insalubre ou dangereux.(...)/ - être handicapées (...), et occuper un logement soit présentant au moins un des risques pour la sécurité ou la santé énumérés à l'article 2 du décret du 30 janvier 2002 ou auquel font défaut au moins deux des éléments d'équipement et de confort mentionnés à l'article 3 du même décret, soit d'une surface habitable inférieure aux surfaces mentionnées au 2° de l'article D. 542-14 du code de la sécurité sociale, ou, pour une personne seule, d'une surface inférieure à celle mentionnée au premier alinéa de l'article 4 du même décret " ; qu'enfin, aux termes du quatrième alinéa du II de l'article L. 441-2-3 de ce code : " La commission reçoit notamment du ou des bailleurs chargés de la demande ou ayant eu à connaître de la situation locative antérieure du demandeur tous les éléments d'information sur la qualité du demandeur et les motifs invoqués pour expliquer l'absence de proposition. Elle reçoit également des services sociaux qui sont en contact avec le demandeur et des instances du plan départemental d'action pour le logement et l'hébergement des personnes défavorisées ayant eu à connaître de sa situation toutes informations utiles sur ses besoins et ses capacités et sur les obstacles à son accès à un logement décent et indépendant ou à son maintien dans un tel logement. " ;<br/>
<br/>
              3. Considérant, en premier lieu, qu'il résulte de ces dispositions qu'il appartient à la commission de médiation, qui, pour instruire les demandes qui lui sont présentées en application du II de l'article L. 441-2-3 du code de la construction et de l'habitation, peut obtenir des professionnels de l'action sociale et médico-sociale, au besoin sur sa demande, les informations propres à l'éclairer sur la situation des demandeurs, de procéder, sous le contrôle du juge de l'excès de pouvoir, à un examen global de la situation de ces derniers au regard des informations dont elle dispose, sans être limitée par le motif invoqué dans la demande, afin de vérifier s'ils se trouvent dans l'une des situations envisagées à l'article R. 441-14-1 de ce code pour être reconnus prioritaires et devant être relogés en urgence au titre du premier ou du deuxième alinéa du II de l'article L. 441-2-3 ; que le demandeur qui forme un recours pour excès de pouvoir contre la décision par laquelle la commission de médiation a refusé de le déclarer prioritaire et devant être relogé en urgence peut utilement faire valoir qu'à la date de cette décision, il remplissait les conditions pour être déclaré prioritaire sur le fondement d'un autre alinéa du II de l'article L. 441-2-3 que celui qu'il avait invoqué devant la commission de médiation ; qu'il peut également présenter pour la première fois devant le juge de l'excès de pouvoir des éléments de fait ou des justificatifs qu'il n'avait pas soumis à la commission, sous réserve que ces éléments tendent à établir qu'à la date de la décision attaquée, il se trouvait dans l'une des situations lui permettant d'être reconnu comme prioritaire et devant être relogé en urgence ; <br/>
<br/>
              4. Considérant, en second lieu, que, lorsqu'un demandeur bénéficiant d'un logement dans le parc social invoque le premier alinéa du II de l'article L. 441-2-3 du code de la construction et de l'habitation pour être reconnu prioritaire en vue d'être relogé en urgence dans un autre logement social, en se bornant à faire valoir qu'il n'a reçu aucune proposition adaptée en réponse à sa demande de logement social locatif dans le délai fixé en application de l'article L. 441-1-4, la commission de médiation peut se fonder, pour refuser de le déclarer prioritaire, sur la circonstance qu'il ne justifie pas de motifs sérieux de vouloir quitter le logement social qu'il occupe ; <br/>
<br/>
              5. Considérant qu'il ressort des motifs de l'arrêt attaqué qu'après avoir constaté que M. A... avait fait valoir devant la commission de médiation des Alpes-Maritimes qu'il remplissait la condition, prévue au premier alinéa du II de l'article L. 441-2-3 du code de la construction et de l'habitation, de n'avoir pas reçu de proposition adaptée à sa demande de logement social dans le délai fixé en application de l'article L. 441-1-4, la cour administrative d'appel s'est fondée, pour censurer la décision de la commission, sur la circonstance que l'intéressé justifiait en outre que son logement ne présentait pas un caractère décent à la date à laquelle elle s'était prononcée ; qu'il résulte de ce qui a été dit au point 3 que la cour n'a pas commis d'erreur de droit en prenant ainsi en compte un motif qui n'avait pas été soumis à la commission de médiation et qui ne relevait pas du même alinéa du II de l'article L. 441-2-3 du code de la construction et de l'habitation que le motif présenté devant la commission ; que, si elle s'est fondée, pour regarder comme établi l'état dégradé du logement social de M.A..., sur un rapport d'expertise produit par lui qui avait été réalisé le 25 novembre 2013, quelques mois après la décision de la commission, elle s'est néanmoins replacée à la date de cette décision pour en apprécier le bien-fondé ; qu'ainsi, le moyen tiré de ce qu'elle aurait commis une erreur de droit en se fondant sur un rapport postérieur à la décision attaquée pour prononcer son annulation sans rechercher si ce rapport révélait une situation existante à la date de la décision manque en fait ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que le pourvoi du ministre du logement, de l'égalité des territoires et de la ruralité doit être rejeté ; que M. A...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Briard, avocat de M. A..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Briard ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre du logement, de l'égalité des territoires et de la ruralité est rejeté.<br/>
<br/>
Article 2 : L'Etat versera à la SCP Briard, avocat de M.A..., une somme de 3 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de la cohésion des territoires et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">38-07-01 LOGEMENT. - 1) OFFICE DE LA COMMISSION DE MÉDIATION - EXAMEN GLOBAL DE LA SITUATION DU DEMANDEUR, NON LIMITÉ PAR LE MOTIF INVOQUÉ DANS LA DEMANDE - 2) A) CONSÉQUENCE - FACULTÉ POUR LE DEMANDEUR DE FAIRE VALOIR DEVANT LE JUGE DE L'EXCÈS DE POUVOIR QU'IL REMPLISSAIT LES CONDITIONS POUR ÊTRE DÉCLARÉ PRIORITAIRE SUR LE FONDEMENT D'UN AUTRE ALINÉA DU II DE L'ART. L. 441-2-3 DU CCH - EXISTENCE - B) FACULTÉ DE PRÉSENTER DE NOUVEAUX ÉLÉMENTS DE FAIT OU DES JUSTIFICATIFS NON SOUMIS À LA COMMISSION - EXISTENCE - CONDITION.
</SCT>
<ANA ID="9A"> 38-07-01 1) Il appartient à la commission de médiation, qui, pour instruire les demandes qui lui sont présentées en application du II de l'article L. 441-2-3 du code de la construction et de l'habitation (CCH), peut obtenir des professionnels de l'action sociale et médico-sociale, au besoin sur sa demande, les informations propres à l'éclairer sur la situation des demandeurs [RJ1], de procéder, sous le contrôle du juge de l'excès de pouvoir, à un examen global de la situation de ces derniers au regard des informations dont elle dispose, sans être limitée par le motif invoqué dans la demande, afin de vérifier s'ils se trouvent dans l'une des situations envisagées à l'article R. 441-14-1 de ce code pour être reconnus prioritaires et devant être relogés en urgence au titre du premier ou du deuxième alinéa du II de l'article L. 441-2-3.... ,,2) a) Le demandeur qui forme un recours pour excès de pouvoir contre la décision par laquelle la commission de médiation a refusé de le déclarer prioritaire et devant être relogé en urgence peut utilement faire valoir qu'à la date de cette décision, il remplissait les conditions pour être déclaré prioritaire sur le fondement d'un autre alinéa du II de l'article L. 441-2-3 que celui qu'il avait invoqué devant la commission de médiation.... ,,b) Il peut également présenter pour la première fois devant le juge de l'excès de pouvoir des éléments de fait ou des justificatifs qu'il n'avait pas soumis à la commission, sous réserve que ces éléments tendent à établir qu'à la date de la décision attaquée, il se trouvait dans l'une des situations lui permettant d'être reconnu comme prioritaire et devant être relogé en urgence.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, 27 juillet 2016, M.,, n° 388029, T. p. 819.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
