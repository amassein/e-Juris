<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041819162</ID>
<ANCIEN_ID>JG_L_2020_03_000000422999</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/81/91/CETATEXT000041819162.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 18/03/2020, 422999, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-03-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422999</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:422999.20200318</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) a demandé au tribunal administratif de Melun de condamner le centre hospitalier de Lagny-sur-Marne à lui verser la somme de 192 474,03 euros en application de l'article L. 1142-15 du code de la santé publique et de 2 385 euros au titre des dépens. La caisse primaire d'assurance maladie (CPAM) de Seine-et-Marne a demandé le remboursement par le même centre hospitalier de la somme de 102 446,16 euros. Par un jugement n° 1306168/1 du 3 octobre 2014, le tribunal administratif a condamné le centre hospitalier de Lagny-sur-Marne à verser à l'ONIAM la somme de 179 478,18 euros et à verser à la CPAM de Seine-et-Marne la somme de 51 223,08 euros.<br/>
<br/>
              Par un arrêt n° 14PA04869 du 12 juin 2018, la cour administrative d'appel de Paris a, sur appels du centre hospitalier de Lagny-sur-Marne, de l'ONIAM et de la CPAM de Seine-et-Marne, ramené à 33 858,32 euros et 10 244,61 euros les sommes que le centre hospitalier a été condamné à verser, respectivement, à l'ONIAM et à la CPAM de Seine-et-Marne, a condamné le centre hospitalier à verser à l'ONIAM une somme de 5 078,75 euros au titre de la pénalité civile instituée à l'article L. 1142-15 du code de la santé publique et rejeté le surplus des conclusions des parties.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 7 août et 7 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, l'ONIAM demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il rejette le surplus de ses conclusions ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier de Lagny-sur-Marne la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales et à Me Le Prado, avocat du centre hospitalier de Lagny-Sur-Marne.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis à la cour administrative d'appel que, par un jugement du 3 octobre 2014, le tribunal administratif de Melun a condamné le centre hospitalier de Lagny-sur-Marne à verser à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) et à la caisse primaire d'assurance maladie (CPAM) de Seine et Marne les sommes, respectivement, de 179 478,18 euros et 51 223,08 euros en raison des fautes commises par l'établissement hospitalier lors de la prise en charge d'une patiente, Mme A..., hospitalisée à la suite d'un accident vasculaire cérébral. Sur appel du centre hospitalier, la cour administrative d'appel de Paris a, par l'arrêt du 12 juin 2018 contre lequel l'ONIAM se pourvoit en cassation, ramené à 38 937,07 euros et 10 244,61 euros les sommes dues, respectivement, à l'ONIAM et à la CPAM de Seine et Marne. <br/>
<br/>
              2. Il ressort des termes mêmes de l'arrêt attaqué que, pour juger que l'erreur de diagnostic commise lorsque Mme A... s'est présentée, victime d'un accident vasculaire cérébral, au centre hospitalier de Lagny-sur-Marne le 18 décembre 2004 et, par suite, l'omission de lui prescrire de l'aspirine, lui avait causé un préjudice de perte de chance de 10% d'éviter la récidive, quelques jours plus tard, de cet accident vasculaire cérébral, la cour administrative d'appel s'est fondée sur ce que la prescription d'aspirine permet d'éviter en moyenne 65 % des récidives d'accident vasculaire cérébral et a appliqué ce taux au risque général de récidive de ce type d'accident, évalué à 15 %. En statuant ainsi, alors qu'il résultait de ses propres constatations que le dommage subi par Mme A... à raison de la récidive de son accident avait 65 % de chances de ne pas se produire si la faute reprochée au centre hospitalier n'avait pas été commise, la cour a entaché son arrêt d'erreur de droit.<br/>
<br/>
              3. Compte tenu du lien qu'établissent les dispositions de l'article L. 376-1 du code de la sécurité sociale entre la détermination des droits de la victime, à laquelle est en l'espèce subrogé l'ONIAM, et celle des droits de la caisse d'assurance maladie à laquelle la victime est affiliée, l'erreur de droit commise par la cour doit entraîner l'annulation de son arrêt, non seulement en tant qu'il rejette le surplus des conclusions de l'ONIAM, mais également en tant qu'il rejette le surplus des conclusions de la CPAM de Seine et Marne.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier de Lagny-sur-Marne le versement à l'ONIAM d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 12 juin 2018 est annulé en tant qu'il rejette le surplus des conclusions de l'ONIAM et de la CPAM de Seine-et-Marne.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris dans la mesure de la cassation prononcée.<br/>
Article 3 : Le centre hospitalier de Lagny-sur-Marne versera à l'ONIAM une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales, au centre hospitalier de Lagny-sur-Marne et à la caisse primaire d'assurance maladie de Seine-et-Marne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
