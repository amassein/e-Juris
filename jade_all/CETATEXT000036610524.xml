<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036610524</ID>
<ANCIEN_ID>JG_L_2018_02_000000409496</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/61/05/CETATEXT000036610524.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 16/02/2018, 409496, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-02-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409496</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Vincent Uher</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:409496.20180216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...C...a demandé au tribunal administratif de Lille d'annuler la décision par laquelle le directeur départemental des finances publiques du Pas-de-Calais a rejeté sa demande de décharge de son obligation de paiement de la cotisation supplémentaire d'impôt sur le revenu ainsi que des majorations correspondantes auxquelles elle avait été assujettie solidairement avec son ancien époux, M. D...A..., au titre de l'année 2005. Par un jugement n° 1201270 du 3 février 2015, le tribunal administratif de Lille a fait droit à sa demande. <br/>
<br/>
              Par un arrêt n° 15DA00534 du 7 février 2017, la cour administrative d'appel de Douai, statuant sur le recours du ministre des finances et des comptes publics, a réformé le jugement du tribunal administratif en tant qu'il a prononcé cette décharge pour un montant en droits supérieur à 72 426 euros, remis à la charge de Mme C...un montant de 4 707 euros, et rejeté le surplus des conclusions qui lui avaient été présentées par le ministre et MmeC.... <br/>
<br/>
              Par un pourvoi enregistré le 3 avril 2017 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'économie et des finances demande au Conseil d'Etat d'annuler l'article 3 de cet arrêt.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Uher, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de MmeC....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un jugement du 6 décembre 2006, prenant effet le 9 janvier 2007, le tribunal de grande instance d'Arras a prononcé le divorce de Mme B...C...et M. D...A.... A l'issue d'une procédure d'évaluation d'office, l'administration a, au cours de l'année 2011, assujetti Mme C...et M. A...à des cotisations supplémentaires d'impôt sur le revenu au titre de l'année 2005 à raison de bénéfices industriels et commerciaux tirés de l'exercice d'une activité occulte d'agent immobilier indépendant par M. A...ainsi qu'aux majorations correspondantes, incluant la pénalité de 80 % prévue par le quatrième alinéa du 1 de l'article 1728 du code général des impôts. Par une décision du 16 décembre 2011, le directeur départemental des finances publiques du Pas-de-Calais a opposé un refus à la demande de Mme C...tendant à la décharge de son obligation solidaire de paiement de ces impositions au motif qu'il n'existait pas de disproportion marquée entre le montant de sa dette fiscale et sa situation financière et patrimoniale nette de charges à la date de cette demande. Par un jugement du 3 février 2015, le tribunal administratif de Lille a annulé cette décision et déchargé Mme C...de son obligation solidaire de paiement. Par un arrêt du 7 février 2017, la cour administrative d'appel de Douai, statuant sur le recours du ministre des finances et des comptes publics, a réformé le jugement du tribunal administratif en tant qu'il a prononcé cette décharge pour un montant en droits supérieur à 72 426 euros, remis à la charge de Mme C...un montant de 4 707 euros, évoqué l'affaire et rejeté le surplus des conclusions que lui avaient présentées le ministre et Mme C.... <br/>
<br/>
               2. Aux termes de l'article 1691 bis du code général des impôts : " I. Les époux et les partenaires liés par un pacte civil de solidarité sont tenus solidairement au paiement : / 1° De l'impôt sur le revenu lorsqu'ils font l'objet d'une imposition commune ; / 2° De la taxe d'habitation lorsqu'ils vivent sous le même toit. / II. - 1. Les personnes divorcées ou séparées peuvent demander à être déchargées des obligations de paiement prévues au I ainsi qu'à l'article 1723 ter-00 B lorsque, à la date de la demande : / a) Le jugement de divorce ou de séparation de corps a été prononcé (...) ; / 2. La décharge de l'obligation de paiement est accordée en cas de disproportion marquée entre le montant de la dette fiscale et, à la date de la demande, la situation financière et patrimoniale, nette de charges, du demandeur. ". En l'absence de dispositions réglementaires précisant l'application du critère fixé au 2 du II de cet article, il appartient aux juges du fond, saisis d'un recours concernant une demande de décharge de l'obligation solidaire de paiement de l'impôt sur le revenu ou de la taxe d'habitation, d'apprécier souverainement l'existence, à la date de la demande, d'une disproportion marquée entre le montant de la dette fiscale des conjoints, anciens conjoints ou partenaires, et la situation financière et patrimoniale, nette de charges, du demandeur, sans qu'ait d'influence à cet égard la durée maximale des plans de rééchelonnement ou d'apurement des dettes non professionnelles des personnes physiques. <br/>
<br/>
              3. Il résulte de ce qui a été dit au 2 qu'en écartant le moyen du ministre tiré de ce que la dette de Mme C...était susceptible d'être éteinte dans un délai de cinq ans, bien inférieur à celui retenu par les dispositions du code de la consommation relatives au surendettement des particuliers, la cour administrative d'appel n'a pas commis d'erreur de droit. <br/>
<br/>
              4. La cour administrative d'appel a relevé que, à supposer même que le patrimoine mobilier de MmeC..., qui s'élevait à 44 457,86 euros à la date de sa demande, puisse être regardé comme une réserve patrimoniale mobilisable pouvant être affectée au paiement de sa dette fiscale et non comme l'unique source future de ses revenus, le montant de sa dette fiscale s'établissait, après soustraction de la valeur de ce patrimoine, à 32 788,14 euros, alors que son revenu annuel net de charges que lui procure son emploi d'auxiliaire de vie-employée de maison, s'établissait au plus à la somme de 8 891,04 euros. Elle en a déduit qu'en dépit de la circonstance, invoquée par le ministre, que sa dette serait susceptible d'être éteinte dans un délai bien inférieur à cinq ans après la cession de l'ensemble de son patrimoine mobilier, il existait une disproportion marquée entre le montant de la dette fiscale de Mme C...et sa situation financière et patrimoniale, nette de charges. Par ces motifs, la cour a porté sur les faits qui lui étaient soumis une appréciation souveraine exempte de dénaturation.<br/>
<br/>
              5. Par suite, le ministre de l'économie et des finances n'est pas fondé à demander l'annulation de l'article 3 de l'arrêt qu'il attaque.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros  à verser à Mme C...au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>                 D E C I D E :<br/>
                              --------------<br/>
<br/>
Article 1er : Le pourvoi du ministre de l'économie et des finances est rejeté.<br/>
<br/>
Article 2 : L'Etat versera à Mme C...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de l'action et des comptes publics à Mme B...C.... <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
