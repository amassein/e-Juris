<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034971138</ID>
<ANCIEN_ID>JG_L_2017_06_000000395456</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/97/11/CETATEXT000034971138.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 19/06/2017, 395456, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395456</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>Mme Cécile Isidoro</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:395456.20170619</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. et Mme A...C...ont demandé au tribunal administratif de Melun la décharge de cotisations supplémentaires d'impôt sur le revenu au titre de l'année 2007.<br/>
<br/>
              Par un jugement n° 1306517/3 du 4 décembre 2014 le tribunal administratif de Melun a rejeté leur demande. <br/>
<br/>
              Par un arrêt n° 15PA00623 du 20 octobre 2015, la cour administrative d'appel de Paris a rejeté leur appel contre ce jugement.<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 21décembre 2015 et 21 mars 2016 au secrétariat du contentieux du Conseil d'Etat, M. et Mme B... C...demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 15PA00623 du 20 octobre 2015 de la cour administrative d'appel de Paris ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leurs conclusions d'appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Isidoro, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de M. et Mme C...;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par acte du 13 décembre 1995, le district de Bénévent Grand-Bourg a, pour pallier la carence de l'initiative privée et développer une activité d'hôtel-restaurant sur son territoire, décidé de confier un immeuble à la SARL Le Cèdre, dont M. A...C...est porteur de parts à hauteur de 10 %. Pour ce faire, le district a passé un contrat de crédit-bail d'une durée de 15 ans à compter du 1er juin 1996 avec cette société. Par acte du 12 décembre 2007, la société a levé par anticipation l'option d'achat avec effet rétroactif au 1er janvier 2006. A la suite de la vérification de comptabilité de la SARL, l'administration a considéré que la société avait omis, lors de la levée d'option d'achat, de réintégrer la totalité de la valeur de l'immeuble à l'actif de son bilan et a assujetti à des cotisations supplémentaires d'impôt sur le revenu au titre de 2007 les associés de la SARL à due proportion de leur parts respectives. M. et Mme C...ont alors saisi le tribunal administratif de Melun d'une demande tendant à la décharge des cotisations ainsi mises à leurs charges. Ils ont fait appel du jugement du tribunal administratif en date du 4 décembre 2014 rejetant leur demande devant la cour administrative d'appel de Paris qui, par un arrêt en date du 20 octobre 2015, a rejeté leur requête. Ils se pourvoient en cassation contre cet arrêt. <br/>
<br/>
              2. D'une part, aux termes du I de l'article 239 sexies du code général des impôts, dans sa rédaction applicable au cas d'espèce en application de l'article 239 sexies B du même code : " Lorsque le prix d'acquisition, par le locataire, de l'immeuble pris en location par un contrat de crédit-bail (...) est inférieur à la différence existant entre la valeur de l'immeuble lors de la signature du contrat et le montant total des amortissements que le locataire aurait pu pratiquer s'il avait été propriétaire du bien depuis cette date, le locataire acquéreur est tenu de réintégrer, dans les résultats de son entreprise afférents à l'exercice en cours au moment de la cession, la fraction des loyers versés pendant la période au cours de laquelle l'intéressé a été titulaire du contrat et correspondant à ladite différence diminuée du prix de cession de l'immeuble. Le montant ainsi déterminé est diminué des quotes-parts de loyers non déductibles en application des dispositions du 10 de l'article 39 (...) ". D'autre part, aux termes de l'article 239 sexies C du même code, dans sa rédaction applicable aux contrats conclus jusqu'au 31 décembre 1995 : " (...) Le prix de revient du bien acquis à l'échéance d'un contrat de crédit-bail est majoré des sommes réintégrées en application des articles 239 sexies et 239 sexies B. La fraction du prix qui excède, le cas échéant, le prix d'achat du terrain par le bailleur, regardée comme le prix de revient des constructions est amorti sur la durée normale d'utilisation du bien restant à courir à cette date depuis son acquisition par le bailleur (...) ". Pour l'application de ces dispositions, la valeur de l'immeuble lors de la signature du contrat correspond, en principe, à celle qui y est mentionnée, sans qu'il y ait lieu de la diminuer d'une subvention consentie par une collectivité publique à un bailleur, alors même que cette subvention a permis de réduire le montant de la somme à verser par l'acheteur.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond et en particulier du contrat de crédit-bail du 13 décembre 1995 que si SARL Le Cèdre pouvait acquérir l'immeuble litigieux en s'acquittant seulement d'une somme de 440 273 euros, à travers les loyers annuels successifs et le prix de cession dû lors de la levée d'option, la valeur de l'immeuble était néanmoins évaluée par le contrat lui-même à 987 473 euros, compte tenu des coûts liés à son acquisition et à sa rénovation par le district, la différence entre les deux montants correspondant aux subventions perçues par ce dernier au titre de l'opération. Par suite, la cour n'a pas commis d'erreur de droit et n'a pas dénaturé les stipulations du contrat précité, en jugeant que l'administration avait pu prendre en compte, au titre de la valeur de l'immeuble prévue par les dispositions citées au point précédent, le second des deux montants. <br/>
<br/>
              4. Il résulte de ce qui précède que M. et Mme C...ne sont pas fondés à demander l'annulation de l'arrêt qu'ils attaquent. Leurs conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative doivent être rejetées par voie de conséquence.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. et Mme A...C...est rejeté. <br/>
Article 2 : La présente décision sera notifiée à M. et Mme A...C...et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
