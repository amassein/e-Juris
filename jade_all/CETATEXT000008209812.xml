<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000008209812</ID>
<ANCIEN_ID>JGXLX2003X07X000000252817</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/08/20/98/CETATEXT000008209812.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'Etat, Président de la Section du Contentieux, du 30 juillet 2003, 252817, inédit au recueil Lebon</TITRE>
<DATE_DEC>2003-07-30</DATE_DEC>
<JURIDICTION>Conseil d'Etat</JURIDICTION>
<NUMERO>252817</NUMERO>
<SOLUTION>Rejet</SOLUTION>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>PRESIDENT DE LA SECTION DU CONTENTIEUX</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>Mme Imbert-Quaretta</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>M. XX</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Lamy</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
     
                Vu la requête enregistrée le 24 décembre 2002 au secrétariat du contentieux du Conseil d'Etat, présentée par Mme Zahia Y... épouse Y, demeurant ...  ; Mme Y..., épouse Y, demande au président de la section du contentieux du Conseil d'Etat  :
<br/>
<br/>
     
                1°) d'annuler le jugement du 1er octobre 2002 par lequel le magistrat délégué par le président du tribunal administratif de Paris a rejeté sa demande tendant à l'annulation de l'arrêté du préfet de police en date du 7 mars 2002 ordonnant sa reconduite à la frontière et fixant l'Algérie comme pays de renvoi  ;
<br/>
<br/>
     
                2°) d'annuler cet arrêté pour excès de pouvoir  ;
<br/>
<br/>
<br/>
<br/>
     
                Vu les autres pièces du dossier  ;
<br/>
<br/>
     
                Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales  ;
<br/>
<br/>
     
                Vu l'accord franco-algérien du 27 décembre 1968 modifié  ;
<br/>
<br/>
     
                Vu l'ordonnance n° 45-2658 du 2 novembre 1945 modifiée  ;	
<br/>
<br/>
     
                Vu le code de justice administrative  ;
<br/>
<br/>
<br/>
     
                Après avoir entendu en séance publique  :
<br/>
<br/>
<br/>
     
                - les conclusions de M. Lamy, Commissaire du gouvernement  ;
<br/>
<br/>
<br/>
<br/>
<br/>
     
                Considérant qu'aux termes du I de l'article 22 de l'ordonnance du 2 novembre 1945 modifiée relative aux conditions d'entrée et de séjour des étrangers en France  : Le représentant de l'Etat dans le département et, à Paris, le préfet de police peuvent, par arrêté motivé, décider qu'un étranger sera reconduit à la frontière dans les cas suivants  : (...) 3° Si l'étranger auquel la délivrance ou le renouvellement d'un titre de séjour a été refusé ou dont le titre de séjour a été retiré, s'est maintenu sur le territoire au-delà du délai d'un mois à compter de la date de notification du refus ou du retrait (...)  ;
<br/>
<br/>
     
                Considérant qu'il ressort des pièces du dossier que Mme Y..., épouse Y, de nationalité algérienne, s'est maintenue sur le territoire français plus d'un mois après la notification, le 9 janvier 2002, de la décision du préfet de police du même jour lui refusant la délivrance d'un titre de séjour et l'invitant à quitter le territoire français  ; qu'elle entrait ainsi dans le champ d'application de la disposition précitée  ;
<br/>
<br/>
     
                Considérant que si, à l'appui de sa demande d'annulation de l'arrêté prononçant sa reconduite à la frontière, Mme Y..., épouse Y, invoque l'illégalité de la décision du 23 février 2001 du ministre de l'intérieur lui refusant le bénéfice de l'asile territorial, elle doit être regardée comme excipant également de l'illégalité de la décision préfectorale lui refusant un titre de séjour fondée notamment sur le refus d'asile territorial qui n'est pas devenue définitive  ;
<br/>
<br/>
     
                Considérant que si Mme Y..., épouse Y, soutient que son époux a fait l'objet de menaces de mort de la part d'intégristes islamistes pour avoir refusé de leur apporter sa collaboration et qu'elle même, en raison de son mode de vie occidental et de ses liens avec la France, est menacée, il ne ressort pas des pièces du dossier, qui sont insuffisantes pour établir les risques personnels qu'elle allègue en cas de retour dans son pays d'origine, que le ministre de l'intérieur ait commis une erreur manifeste d'appréciation en lui refusant l'asile territorial, ni méconnu les stipulations de l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales  ; que, dès lors, la requérante n'est pas fondée à soutenir que le refus de titre de séjour qui lui a été opposé serait illégal en raison de l'illégalité de la décision du ministre de l'intérieur refusant de lui accorder l'asile territorial  ;
<br/>
<br/>
     
                Considérant qu'aux termes de l'article 2 de l'ordonnance du 2 novembre 1945 modifiée  : Les étrangers sont, en ce qui concerne leur séjour en France, soumis aux dispositions de la présente ordonnance, sous réserve des conventions internationales ; que l'accord franco-algérien du 27 décembre 1968 et ses avenants régissent d'une manière complète les conditions dans lesquelles les ressortissants algériens peuvent être admis à séjourner en France et y exercer une activité professionnelle, ainsi que les règles concernant la nature et la durée de validité des titres de séjour qui peuvent leur être délivrés  ; que dès lors, Mme Y..., épouse Y, ne peut utilement se prévaloir des dispositions du 7° de l'article 12 bis de l'ordonnance du 2 novembre 1945 modifiée qui ne s'appliquent pas aux ressortissants algériens  ;
<br/>
<br/>
     
                	Considérant que si Mme Y..., épouse Y, entrée en France le 13 juin 1999 après un premier séjour de 1995 à 1998, fait valoir qu'elle a un enfant régulièrement scolarisé en France depuis 1995, il ne ressort pas des pièces du dossier que, compte tenu de l'ensemble des circonstances de l'espèce, et notamment de la durée et des conditions de séjour de Mme Y..., épouse Y, en France, dont l'époux fait également l'objet d'un arrêté de reconduite à la frontière, l'arrêté du préfet de police en date du 7 mars 2002 ait porté au droit de celle-ci au respect de sa vie privée et familiale une atteinte disproportionnée aux buts en vue desquels il a été pris  ; qu'il n'a ainsi pas méconnu les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales  ;
<br/>
<br/>
     
                	Considérant enfin que si Mme Y..., épouse Y, a vécu précédemment en France, de 1993 à 1998, cette circonstance ne suffit pas à établir que le préfet de police aurait commis une erreur manifeste dans l'appréciation qu'il a faite des conséquences de la mesure contestée sur sa situation personnelle  ;
<br/>
<br/>
     
                Considérant qu'il résulte de ce qui précède que Mme Y..., épouse X..., n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le magistrat délégué par le président du tribunal administratif de Paris a rejeté sa demande  ;
<br/>
<br/>
<br/>
<br/>
     
                			D E C I D E  :
<br/>
     
                			--------------
<br/>
<br/>
<br/>
<br/>
     
		Article 1er  : La requête de Mme Y..., épouse Y, est rejetée.
<br/>
<br/>
     
Article 2  : La présente décision sera notifiée à Mme Zahia Y..., épouse Y, au préfet de police et au ministre de l'intérieur, de la sécurité intérieure et des libertés locales.
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP>
<CONTENU>&lt;br/&gt;</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
