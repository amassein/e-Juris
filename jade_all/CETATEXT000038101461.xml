<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038101461</ID>
<ANCIEN_ID>JG_L_2019_02_000000418122</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/10/14/CETATEXT000038101461.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 06/02/2019, 418122, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418122</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LESOURD</AVOCATS>
<RAPPORTEUR>M. Alexandre  Koutchouk</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:418122.20190206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C...B...(épouseA...) a demandé au tribunal administratif de Nice de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles elle a été assujettie au titre des années 2008 et 2009 et de la période du 1er janvier au 2 septembre 2010, ainsi que des pénalités correspondantes. Par un jugement n° 1303756 du 30 octobre 2015, ce tribunal, après avoir constaté qu'il n'y avait pas lieu à statuer sur les conclusions de la requête à concurrence des dégrèvements prononcés en cours d'instance, a rejeté le surplus des conclusions présentées par MmeB....  <br/>
<br/>
              Par un arrêt n° 16MA00035 du 5 décembre 2017, la cour administrative d'appel de Marseille  a rejeté l'appel formé  par Mme B...contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 13 février et 7 mai 2018 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 800 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;		<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alexandre Koutchouk, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lesourd, avocat de MmeB....<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que MmeB..., de nationalité allemande, a fait l'objet d'un examen contradictoire de l'ensemble de sa situation fiscale personnelle portant sur les années 2008 à 2010, à l'issue duquel l'administration fiscale a estimé que son domicile fiscal était, au cours de ces années, situé en France. Après avoir mis sans succès l'intéressée en demeure de déposer des déclarations de revenu global pour l'année 2008, l'année 2009 et la période du 1er janvier au 2 septembre 2010, veille de son mariage, l'administration l'a assujettie, au titre de ces années et de cette période, à des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales selon la procédure de taxation d'office. Mme B...se pourvoit en cassation contre l'arrêt du 5 décembre 2017 par lequel la cour administrative d'appel de Marseille a rejeté l'appel qu'elle avait formé contre le jugement du tribunal administratif de Nice du 30 octobre 2015 rejetant, après avoir constaté un non-lieu à statuer à concurrence de dégrèvements accordés en cours d'instance, le surplus de sa demande tendant à la décharge de ces impositions. <br/>
<br/>
              2. Aux termes de l'article L. 66 du livre des procédures fiscales : " Sont taxés d'office : 1º à l'impôt sur le revenu les contribuables qui n'ont pas déposé dans le délai légal la déclaration d'ensemble de leurs revenus (...) sous réserve de la procédure de régularisation prévue à l'article L. 67. ". Aux termes de l'article L. 67 du même livre : " La procédure de taxation d'office prévue aux 1º et 4º de l'article L. 66 n'est applicable que si le contribuable n'a pas régularisé sa situation dans les trente jours de la notification d'une première mise en demeure. (...) ". Pour être régulière, la mise en demeure adressée au contribuable en application de ces dispositions doit être notifiée à la dernière adresse qu'il a officiellement communiquée à l'administration fiscale. En cas de changement de domicile, il appartient au contribuable d'établir qu'il a fait les diligences nécessaires pour informer l'administration de sa nouvelle adresse. <br/>
<br/>
              3. Pour juger que Mme B... n'était pas fondée à soutenir que les mises en demeure de souscrire des déclarations d'ensemble de son revenu global lui avaient été irrégulièrement notifiées à l'adresse de l'appartement dont elle est propriétaire boulevard de Strasbourg à Cannes (Alpes-Maritimes), la cour administrative d'appel s'est fondée, après avoir exposé les motifs pour lesquels elle estimait que Mme B...était résidente fiscale française au cours des années et de la période visées par ces mises en demeure, d'une part, sur ce qu'elle ne contestait pas avoir mentionné l'adresse de son domicile cannois dans la déclaration d'ensemble de ses revenus de l'année 2011 et dans la copie de son acte de mariage de 2010 et, d'autre part, sur ce qu'elle ne justifiait pas avoir informé l'administration d'une autre adresse, située en Allemagne, à laquelle auraient dû lui être expédiés par l'administration fiscale les courriers la concernant. En se fondant, pour estimer que l'adresse de l'appartement dont Mme B...est propriétaire à Cannes était la dernière adresse indiquée à l'administration fiscale comme celle à laquelle les courriers la concernant devaient lui être envoyés, sur les mentions de son acte de mariage, dont l'objet est étranger aux relations entre les contribuables et l'administration fiscale et dont il ressort au demeurant des pièces du dossier soumis aux juges du fond qu'il n'a en l'espèce été obtenu par cette dernière, par l'exercice de son droit de communication, que postérieurement aux mises en demeure litigieuses, et sur l'adresse qu'elle avait mentionnée dans la déclaration de ses revenus de l'année 2011, souscrite postérieurement à ces mises en demeure, et en écartant, par ailleurs, comme sans incidence sur la détermination de la dernière adresse indiquée à l'administration fiscale la circonstance que celle-ci avait notifié à l'adresse de Mme B...en Allemagne, à l'automne 2010, des avis d'imposition à la taxe foncière et à la taxe d'habitation relatives à son appartement cannois, la cour administrative d'appel a entaché son arrêt d'erreur de droit. <br/>
<br/>
              4. Il résulte de ce qui précède que Mme B...est fondée à demander l'annulation de l'arrêt qu'elle attaque. <br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à Mme B...d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>                      D E C I D E :<br/>
                                    --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 5 décembre 2017 est annulé. <br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : L'Etat versera à Mme B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme C... B...et au ministre de l'action et des comptes publics. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
