<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041735775</ID>
<ANCIEN_ID>JG_L_2020_03_000000425990</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/73/57/CETATEXT000041735775.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 18/03/2020, 425990</TITRE>
<DATE_DEC>2020-03-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425990</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP CELICE, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Pierre Boussaroque</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:425990.20200318</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le centre hospitalier universitaire de Nice a demandé au tribunal administratif de Nice d'annuler pour excès de pouvoir l'arrêté du 26 septembre 2014 du président du service départemental d'incendie et de secours des Alpes-Maritimes portant fixation du montant de la participation aux frais d'intervention du centre hospitalier universitaire de Nice lorsque le " centre 15 " sollicite le service départemental d'incendie et de secours pour réaliser une intervention n'entrant pas dans ses missions propres. Par un jugement n° 1405130 du 2 novembre 2016, le tribunal administratif de Nice a fait droit à cette demande.<br/>
<br/>
              Par un arrêt n° 17MA00014 du 4 octobre 2018, la cour administrative d'appel de Marseille a rejeté l'appel formé par le service départemental d'incendie et de secours des Alpes-Maritimes contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés le 5 décembre 2018 et les 28 février et 2 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, le service départemental d'incendie et de secours des Alpes-Maritimes demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier universitaire de Nice la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité intérieure ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Boussaroque, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat du service départemental d'incendie et de secours des Alpes-Maritimes et à la SCP Célice, Texidor, Perier, avocat du centre hospitalier universitaire de Nice ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le conseil d'administration du service départemental d'incendie et de secours des Alpes-Maritimes a décidé, par une délibération du 11 juillet 2014, de facturer au centre hospitalier universitaire de Nice chaque intervention réalisée à la demande du centre de réception et de régulation des appels (" centre 15 ") de son service d'aide médicale urgente pour des missions ne se rattachant pas aux missions de service public des services d'incendie et de secours définies à l'article L. 1424-2 du code général des collectivités territoriales. Le président du conseil d'administration du service départemental d'incendie et de secours des Alpes-Maritimes, autorisé par la délibération du 11 juillet 2014 à signer tout document nécessaire à la mise en oeuvre de cette décision, a, par un arrêté du 26 septembre 2014, fixé à 1 022,17 euros le montant unitaire de cette participation. Saisi par le centre hospitalier universitaire de Nice, le tribunal administratif de Nice a annulé cet arrêté pour excès de pouvoir par un jugement du 2 novembre 2016. Le service départemental d'incendie et de secours des Alpes-Maritimes se pourvoit en cassation contre l'arrêt du 4 octobre 2018 par lequel la cour administrative d'appel de Marseille a rejeté l'appel qu'il avait formé contre ce jugement.<br/>
<br/>
              2. D'une part, aux termes de l'article L. 1424-2 du code général des collectivités territoriales : " Les services d'incendie et de secours (...) concourent, avec les autres services et professionnels concernés, (...) aux secours d'urgence. / Dans le cadre de leurs compétences, ils exercent les missions suivantes : (...) / 4° Les secours d'urgence aux personnes victimes d'accidents, de sinistres ou de catastrophes ainsi que leur évacuation ". L'article L. 742-11 du code de la sécurité intérieure prévoit que : " Les dépenses directement imputables aux opérations de secours au sens des dispositions de l'article L. 1424-2 du code général des collectivités territoriales sont prises en charge par le service départemental d'incendie et de secours. (...) ". L'article L. 1424-42 du code général des collectivités territoriales dispose que : " Le service départemental d'incendie et de secours n'est tenu de procéder qu'aux seules interventions qui se rattachent directement à ses missions de service public définies à l'article L. 1424-2. / S'il a procédé à des interventions ne se rattachant pas directement à l'exercice de ses missions, il peut demander aux personnes bénéficiaires une participation aux frais, dans les conditions déterminées par délibération du conseil d'administration. / Les interventions effectuées par les services d'incendie et de secours à la demande de la régulation médicale du centre 15, lorsque celle-ci constate le défaut de disponibilité des transporteurs sanitaires privés, et qui ne relèvent pas de l'article L. 1424-2, font l'objet d'une prise en charge financière par les établissements de santé, sièges des services d'aide médicale d'urgence. / Les conditions de cette prise en charge sont fixées par une convention entre le service départemental d'incendie et de secours et l'hôpital siège du service d'aide médicale d'urgence, selon des modalités fixées par arrêté conjoint du ministre de l'intérieur et du ministre chargé de la sécurité sociale (...) ".<br/>
<br/>
              3. D'autre part, aux termes de l'article L. 6311-1 du code de la santé publique : " L'aide médicale urgente a pour objet, en relation notamment avec les dispositifs communaux et départementaux d'organisation des secours, de faire assurer aux malades, blessés et parturientes, en quelque endroit qu'ils se trouvent, les soins d'urgence appropriés à leur état " et l'article L. 6311-2 du même code prévoit qu' : " (...) un centre de réception et de régulation des appels est installé dans les services d'aide médicale urgente (...) ". L'article R. 6311-1 de ce code précise que : " Les services d'aide médicale urgente ont pour mission de répondre par des moyens exclusivement médicaux aux situations d'urgence. / Lorsqu'une situation d'urgence nécessite la mise en oeuvre conjointe de moyens médicaux et de moyens de sauvetage, les services d'aide médicale urgente joignent leurs moyens à ceux qui sont mis en oeuvre par les services d'incendie et de secours " et l'article R. 6311-2 que : " Pour l'application de l'article R. 6311-1, les services d'aide médicale urgente : / (...) 2° Déterminent et déclenchent, dans le délai le plus rapide, la réponse la mieux adaptée à la nature des appels ; / (...) 4° Organisent, le cas échéant, le transport dans un établissement public ou privé en faisant appel à un service public ou à une entreprise privée de transports sanitaires (...) ". L'article D. 6124-12 de ce code permet aux services d'incendie et de secours de mettre des équipages et véhicules à disposition d'une structure mobile d'urgence et de réanimation dans le cadre, qui régit alors cette mise à disposition, d'une convention avec l'établissement de santé autorisé à disposer d'une telle structure. Il résulte enfin de l'article R. 6312-15 du même code que ces services, indépendamment de la conclusion d'une telle convention, peuvent être amenés à intervenir pour effectuer des transports sanitaires d'urgence faute de moyens de transport sanitaire.<br/>
<br/>
              4. Il résulte de l'ensemble de ces dispositions que les services départementaux d'incendie et de secours ne doivent supporter la charge que des interventions qui se rattachent directement aux missions de service public définies à l'article L. 1424-2 du code général des collectivités territoriales, au nombre desquelles figurent celles qui relèvent des secours d'urgence aux personnes victimes d'accidents, de sinistres ou de catastrophes, y compris l'évacuation de ces personnes. Les interventions ne relevant pas directement de l'exercice de leurs missions de service public effectuées par les services départementaux d'incendie et de secours peuvent donner lieu à une participation aux frais des personnes qui en sont bénéficiaires, dont ces services déterminent eux-mêmes les conditions.<br/>
<br/>
              5. Il résulte également  de ces dispositions qu'il incombe aux services d'aide médicale urgente de faire assurer aux malades, blessés et parturientes, en quelque endroit qu'ils se trouvent, les soins d'urgence appropriés à leur état et, à cette fin, au centre de réception et de régulation des appels, dit " centre 15 ", installé dans ces services, de déterminer et déclencher, dans le délai le plus rapide, la réponse la mieux adaptée à la nature des appels, le cas échéant en organisant un transport sanitaire d'urgence faisant appel à une entreprise privée de transport sanitaire ou, au besoin, aux services d'incendie et de secours. Les interventions ne relevant pas de l'article L. 1424-2 du code général des collectivités territoriales qui sont effectuées par les services départementaux d'incendie et de secours à la demande du centre 15, lorsque celui-ci constate le défaut de disponibilité des transporteurs sanitaires privés, sont décidées, sous sa responsabilité, par le médecin régulateur du service d'aide médicale urgente, qui les a estimées médicalement justifiées compte tenu des informations dont il disposait sur l'état du patient. Elles font l'objet d'une prise en charge financière par l'établissement de santé siège des services d'aide médicale d'urgence, dans des conditions fixées par une convention - distincte de celle que prévoit l'article D. 6124-12 du code de la santé publique en cas de mise à disposition de certains moyens - conclue entre le service départemental d'incendie et de secours et l'établissement de santé et selon des modalités fixées par arrêté conjoint du ministre de l'intérieur et du ministre chargé de la sécurité sociale. <br/>
<br/>
              6. En jugeant que les dispositions des troisième et quatrième alinéas de l'article L. 1424-42 du code général des collectivités territoriales doivent dans ces conditions être regardées comme régissant l'ensemble des conditions de prise en charge financière par les établissements de santé d'interventions effectuées par les services départementaux d'incendie et de secours à la demande du centre de réception et de régulation des appels lorsque ces interventions ne sont pas au nombre des missions de service public définies à l'article L. 1424-2 de ce code auxquelles ces établissements publics sont tenus de procéder et dont ils supportent la charge, la cour administrative d'appel n'a pas commis d'erreur de droit. Elle n'a pas davantage commis d'erreur de droit en en déduisant que les services départementaux d'incendie et de secours ne peuvent demander, sur le fondement du deuxième alinéa de l'article L. 1424-42 du même code, une participation aux frais, dans les conditions déterminées par délibération de leur seul conseil d'administration, aux établissements de santé, sièges des services d'aide médicale d'urgence.<br/>
<br/>
              7. Il résulte de tout ce qui précède que le service départemental d'incendie et de secours des Alpes-Maritimes n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge du centre hospitalier universitaire de Nice, qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du service départemental d'incendie et de secours des Alpes-Maritimes une somme de 3 000 euros à verser au centre hospitalier universitaire de Nice au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi du service départemental d'incendie et de secours des Alpes-Maritimes est rejeté. <br/>
<br/>
Article 2 : Le service départemental d'incendie et de secours des Alpes-Maritimes versera au centre hospitalier universitaire de Nice une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée au service départemental d'incendie et de secours des Alpes-Maritimes et au centre hospitalier universitaire de Nice.<br/>
Copie en sera adressée au ministre des solidarités et de la santé et à l'agence régionale de santé Provence-Alpes-Côte d'Azur.<br/>
<br/>
.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
