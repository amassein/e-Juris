<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038077319</ID>
<ANCIEN_ID>JG_L_2019_01_000000411132</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/07/73/CETATEXT000038077319.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 30/01/2019, 411132</TITRE>
<DATE_DEC>2019-01-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411132</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:411132.20190130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Exane a demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir la décision du 2 décembre 2013 par laquelle le Défenseur des droits a décidé de présenter des observations dans un litige opposant, devant la cour d'appel de Paris, cette société à une ancienne salariée. Par un jugement n° 1401627 du 4 juin 2015, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15PA03145 du 6 avril 2017, la cour administrative d'appel de Paris a rejeté l'appel formé par la société Exane contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 1er juin et 1er septembre 2017 au secrétariat du contentieux du Conseil d'Etat, la société Exane demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi organique n° 2011-333 du 29 mars 2011 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat de la société Exane et à la SCP Foussard, Froger, avocat du Défenseur des droits ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. En vertu du deuxième alinéa de l'article 33 de la loi organique du 29 mars 2011 relative au Défenseur des droits, celui-ci peut présenter des observations devant une juridiction civile, administrative ou pénale, soit d'office, soit à l'invitation de cette juridiction. La décision par laquelle le Défenseur des droits décide, sur ce fondement, de présenter de telles observations, n'est pas détachable de la procédure juridictionnelle à laquelle elle se rapporte. <br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que la société Exane a contesté, par la voie d'un recours pour excès de pouvoir, la décision du 2 décembre 2013 par laquelle le Défenseur des droits a décidé de présenter des observations dans un litige opposant, devant la cour d'appel de Paris, cette société à une ancienne salariée. Une telle décision, qui n'a pas d'autre objet que celui qui a été exposé au point précédent, est indissociable de la procédure juridictionnelle à laquelle elle se rapporte. Cette procédure juridictionnelle étant suivie devant une juridiction judiciaire, le litige soulevé par la demande de la société Exane n'est pas au nombre de ceux dont il appartient à la juridiction administrative de connaître. <br/>
<br/>
              3. Il résulte de ce qui précède qu'en rejetant comme irrecevable la requête de la société Exane, sans relever d'office l'incompétence de la juridiction administrative, la cour administrative d'appel de Paris a entaché son arrêt d'une erreur de droit. Par conséquent, sans qu'il soit besoin d'examiner les moyens de son pourvoi, la société Exane est fondée à en demander l'annulation.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative et de statuer sur l'appel formé par la société Exane contre le jugement du 4 juin 2015 du tribunal administratif de Paris.<br/>
<br/>
              5. Par ce jugement, le tribunal administratif a rejeté comme irrecevable la demande d'annulation présentée par la société Exane. Pour les motifs qui ont été exposés aux points 2 et 3, il y a lieu d'annuler ce jugement et, statuant par la voie de l'évocation, de rejeter la demande de la société Exane comme portée devant une juridiction incompétente pour en connaître.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance. Par ailleurs, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Exane une somme de 3 000 euros à verser à l'Etat au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 6 avril 2017 et le jugement du tribunal administratif de Paris du 4 juin 2015 sont annulés.<br/>
<br/>
Article 2 : La demande de la société Exane est rejetée comme portée devant une juridiction incompétente pour en connaître.<br/>
<br/>
Article 3 : La société Exane versera à l'Etat une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société Exane et au Défenseur des droits.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03-02-07-05-02 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. PROBLÈMES PARTICULIERS POSÉS PAR CERTAINES CATÉGORIES DE SERVICES PUBLICS. SERVICE PUBLIC JUDICIAIRE. FONCTIONNEMENT. - DÉCISION DU DÉFENSEUR DES DROITS DE PRÉSENTER DES OBSERVATIONS DANS UNE PROCÉDURE JURIDICTIONNELLE DEVANT UNE JURIDICTION JUDICIAIRE - ACTE DÉTACHABLE D'UNE PROCÉDURE JUDICIAIRE - ABSENCE [RJ1] - CONSÉQUENCE - INCOMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">52-06 POUVOIRS PUBLICS ET AUTORITÉS INDÉPENDANTES. - DÉCISION DU DÉFENSEUR DES DROITS DE PRÉSENTER DES OBSERVATIONS DANS UNE PROCÉDURE JURIDICTIONNELLE DEVANT UNE JURIDICTION JUDICIAIRE - ACTE DÉTACHABLE D'UNE PROCÉDURE JUDICIAIRE - ABSENCE - CONSÉQUENCE - INCOMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE.
</SCT>
<ANA ID="9A"> 17-03-02-07-05-02 La décision par laquelle le Défenseur des droits a décidé de présenter des observations dans un litige opposant, devant une juridiction judiciaire, une société à une ancienne salariée, est indissociable  de la procédure juridictionnelle à laquelle elle se rapporte. Cette procédure juridictionnelle étant suivie devant une juridiction judiciaire, le litige soulevé par la décision du Défenseur des droits n'est pas au nombre de ceux dont il appartient à la juridiction administrative de connaître.</ANA>
<ANA ID="9B"> 52-06 La décision par laquelle le Défenseur des droits a décidé de présenter des observations dans un litige opposant, devant une juridiction judiciaire, une société à une ancienne salariée, est indissociable  de la procédure juridictionnelle à laquelle elle se rapporte [RJ1]. Cette procédure juridictionnelle étant suivie devant une juridiction judiciaire, le litige soulevé par la décision du Défenseur des droits n'est pas au nombre de ceux dont il appartient à la juridiction administrative de connaître.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. s'agissant des décisions de l'administration de transmettre ou de ne pas transmettre un document à l'autorité judiciaire, CE, Section, 4 février 1944, Sieur Lemoine, p 47 ; CE, 18 novembre 1977, Albrecht, n° 2330, T. pp. 743-920 ; TC, 6 juillet 2015, Ministre de l'intérieur c/ M. Taïs, n° 4017, T. p. 602 ; s'agissant des décisions de l'administration d'intervenir ou de ne pas intervenir dans un litige judiciaire, TC, 2 juillet 1979, Agelasto, n° 2134, p. 573 ; CE, 20 avril 2005, Régie départementale des transports de l'Ain et autres, n° 255417, p. 150 ;.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
