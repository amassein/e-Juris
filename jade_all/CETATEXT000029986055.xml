<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029986055</ID>
<ANCIEN_ID>JG_L_2014_12_000000373657</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/98/60/CETATEXT000029986055.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 29/12/2014, 373657, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-12-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373657</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Benjamin de Maillard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:373657.20141229</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête enregistrée le 2 décembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentée par la société Olmes Distribution, dont le siège est rue des Pyrénées, à Lavelanet (09300) représentée par son gérant en exercice, la société CSF France, dont le siège est zone industrielle, route de Paris, à Mondeville (14120), représentée par son gérant en exercice, la SARL Lavenalet Distribution, dont le siège est 1 place de la Résistance, à Lavelanet (09300), représentée par son gérant en exercice ; la société Olmes Distribution et autres demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 1913 T du 11 septembre 2013 par laquelle la Commission nationale d'aménagement commercial a accordé à la société l'Immobilière Européenne des Mousquetaires l'autorisation de créer un supermarché Intermarché de 2 000 m² de surface de vente, à Laroque d'Olmes (Ariège) ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu la loi n° 73-1193 du 27 décembre 1973 ;<br/>
<br/>
              Vu la loi n° 2008-776 du 4 août 2008 ;<br/>
<br/>
              Vu le décret n° 2008-1212 du 24 novembre 2008 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Benjamin de Maillard, auditeur,  <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Sur la légalité de la décision attaquée : <br/>
<br/>
              En ce qui concerne la composition de la Commission nationale d'aménagement commercial :<br/>
<br/>
              1. Considérant qu'il ne résulte d'aucune disposition législative ou réglementaire ni d'aucun principe que les membres siégeant à la Commission nationale d'aménagement commercial doivent obligatoirement être en activité ; que, dès lors, le moyen tiré de ce que la commission nationale serait irrégulièrement composée dès lors que son président a été nommé alors qu'il était retraité ne peut qu'être écarté ; <br/>
<br/>
              En ce qui concerne la composition du dossier :<br/>
<br/>
              2. Considérant que, si les requérants soutiennent que le dossier de demande d'autorisation était incomplet en ce qui concerne la maîtrise du foncier, la définition de la zone de chalandise, l'impact du projet en matière d'aménagement du territoire, de développement durable et de protection des consommateurs, il ressort des pièces du dossier que la commission nationale disposait des éléments suffisants lui permettant d'apprécier la conformité du projet à l'article L. 752-6 du code de commerce ; <br/>
<br/>
              En ce qui concerne le moyen tiré de la méconnaissance de l'article L. 122-2 du code de l'urbanisme : <br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 122-2 du code de l'urbanisme : " Dans les communes qui ne sont pas couvertes par un schéma de cohérence territoriale applicable, le plan local d'urbanisme ne peut être modifié ou révisé en vue d'ouvrir à l'urbanisation une zone à urbaniser délimitée après le 1er juillet 2002 ou une zone naturelle. / (...) A compter du 1er janvier 2013 et jusqu'au 31 décembre 2016, le premier alinéa s'applique dans les communes situées à moins de quinze kilomètres du rivage de la mer ou à moins de quinze kilomètres de la périphérie d'une agglomération de plus de 15 000 habitants au sens du recensement général de la population (...) / Dans les communes où s'applique le premier alinéa et à l'intérieur des zones à urbaniser ouvertes à l'urbanisation après l'entrée en vigueur de la loi n° 2003-590 du 2 juillet 2003 urbanisme et habitat, il ne peut être délivré d'autorisation d'exploitation commerciale en application de l'article L. 752-1 du code de commerce " ; qu'il ressort des pièces du dossier que la commune de Laroque d'Olmes n'est pas située à moins de quinze kilomètres d'une agglomération de plus de 15 000 habitants, ni à moins de quinze kilomètres du rivage ; qu'il s'ensuit que les sociétés requérantes ne sauraient utilement se prévaloir de la méconnaissance des dispositions précitées, qui ne sont pas applicables à cette commune ;<br/>
<br/>
              En ce qui concerne l'appréciation de la Commission nationale d'aménagement commercial :<br/>
<br/>
              4. Considérant qu'il appartient aux commissions d'aménagement commercial, lorsqu'elles se prononcent sur un projet d'exploitation commerciale soumis à autorisation en application de l'article L. 752-1 du code de commerce, d'apprécier la conformité de ce projet aux objectifs prévus à l'article 1er de la loi du 27 décembre 1973 et à l'article L. 750-1 du code de commerce, au vu des critères d'évaluation mentionnés à l'article L. 752-6 du même code ; que l'autorisation ne peut être refusée que si, eu égard à ses effets, le projet compromet la réalisation de ces objectifs ;<br/>
<br/>
              5. Considérant que, si les requérantes soutiennent que la décision attaquée méconnaît l'objectif fixé par le législateur en matière d'aménagement du territoire en raison des conséquences de l'ouverture du magasin sur les commerces du centre-ville et les conditions de circulation, il ressort des pièces du dossier que le projet, qui se situe en entrée de ville, à un kilomètre du centre-bourg de la commune de Laroque d'Olmes, dans une zone d'activités et à proximité d'un espace d'habitation, contribue à l'animation de cette commune ; que, compte tenu des capacités des infrastructures routières qui desservent le site sur lequel le projet est implanté, les flux de véhicules supplémentaires engendrés par sa réalisation ne sont pas susceptibles de provoquer des difficultés de circulation ; <br/>
<br/>
              6. Considérant que, si les requérantes soutiennent que la décision attaquée méconnaît l'objectif fixé par le législateur en matière de développement durable en raison des lacunes du projet en matière de qualité environnementale, il ressort des pièces du dossier que le pétitionnaire a prévu la mise en place de dispositifs permettant de limiter les consommations énergétiques et d'améliorer le traitement des déchets ; que l'insuffisance de la desserte du projet par les transports en commun et les pistes cyclables n'est pas établie ; que cette parcelle ne présente pas de caractéristique naturelle remarquable ; que, par ailleurs, le pétitionnaire a prévu la réalisation d'espaces verts en vue d'améliorer l'insertion du projet dans les paysages ;<br/>
<br/>
              7. Considérant que, si les requérantes soutiennent que la décision attaquée méconnaît l'objectif fixé par le législateur en matière de protection du consommateur, il ne ressort pas des pièces du dossier que la proximité du projet avec une usine de textile compromettrait la sécurité des consommateurs ;<br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que les requérantes ne sont pas fondés à demander l'annulation de la décision attaquée ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              9. Considérant que ces dispositions font obstacle à ce que la somme que demandent la société Olmes Distribution, la société CSF France et la SARL Lavelanet Distribution soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ; qu'en revanche, il y a lieu de mettre à la charge de la société Olmes Distribution, la société CSF France et la SARL Lavelanet Distribution la somme de 1 500 euros chacune à verser à la société l'Immobilière Européenne des Mousquetaires ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de la société Olmes Distribution, la société CSF France et la SARL Lavelanet Distribution est rejetée.<br/>
Article 2 : La société Olmes Distribution, la société CSF France et la SARL Lavelanet Distribution verseront la somme de 1 500 euros chacune à la société l'Immobilière Européenne des Mousquetaires au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Olmes Distribution, à la société CSF France, à la SARL Lavelanet Distribution et à la société l'Immobilière Européenne des Mousquetaires.<br/>
Copie en sera adressée pour information à la Commission nationale d'aménagement commercial.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
