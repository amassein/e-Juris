<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036715364</ID>
<ANCIEN_ID>JG_L_2018_03_000000389176</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/71/53/CETATEXT000036715364.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 16/03/2018, 389176</TITRE>
<DATE_DEC>2018-03-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389176</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:389176.20180316</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° La commune de Bonneuil-sur-Marne a demandé au tribunal administratif de Melun, à titre principal, d'annuler pour excès de pouvoir l'arrêté du 13 décembre 2010 du ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration, du ministre de l'économie et des finances et du ministre du budget et des comptes publics en tant qu'il a refusé de reconnaître l'état de catastrophe naturelle sur son territoire pour des mouvements de terrain différentiels consécutifs à la sécheresse et à la réhydratation des sols survenus entre le 1er janvier et le 31 décembre 2009 et, à titre subsidiaire, de désigner un expert chargé de rechercher si les phénomènes qu'elle a invoqués relèvent des critères de l'état de catastrophe naturelle définis par la loi. Par un jugement nos 1102101/9, 1104200/9 du 18 décembre 2013, le tribunal administratif de Melun a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14PA00618 du 2 février 2015, la cour administrative d'appel de Paris a rejeté l'appel formé par la commune de Bonneuil-sur-Marne.<br/>
<br/>
              Sous le n° 389176, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 2 avril et 2 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, la commune de Bonneuil-sur-Marne demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'État la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Sous le n° 389177, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 2 avril et 2 juillet 2015 au secrétariat du contentieux du Conseil d'État, la commune de Bonneuil-sur-Marne demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'État la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code des assurances ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la commune de Bonneuil-sur-Marne.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 125-1 du code des assurances : " Les contrats d'assurance, souscrits par toute personne physique ou morale autre que l'Etat et garantissant les dommages d'incendie ou tous autres dommages à des biens situés en France, ainsi que les dommages aux corps de véhicules terrestres à moteur, ouvrent droit à la garantie de l'assuré contre les effets des catastrophes naturelles (...) ; / Sont considérés comme les effets des catastrophes naturelles, au sens du présent chapitre, les dommages matériels directs non assurables ayant eu pour cause déterminante l'intensité anormale d'un agent naturel, lorsque les mesures habituelles à prendre pour prévenir ces dommages n'ont pu empêcher leur survenance ou n'ont pu être prises. / L'état de catastrophe naturelle est constaté par arrêté interministériel qui détermine les zones et les périodes où s'est située la catastrophe ainsi que la nature des dommages résultant de celle-ci couverts par la garantie visée au premier alinéa du présent article. Cet arrêté précise, pour chaque commune ayant demandé la reconnaissance de l'état de catastrophe naturelle, la décision des ministres. Cette décision est ensuite notifiée à chaque commune concernée par le représentant de l'État dans le département, assortie d'une motivation. L'arrêté doit être publié au Journal officiel dans un délai de trois mois à compter du dépôt des demandes à la préfecture. De manière exceptionnelle, si la durée des enquêtes diligentées par le représentant de l'État dans le département est supérieure à deux mois, l'arrêté est publié au plus tard deux mois après la réception du dossier par le ministre chargé de la sécurité civile. " ; <br/>
<br/>
              2. Considérant qu'il ressort des énonciations des arrêts attaqués qu'à la suite de la sécheresse des années 2009 et 2010, la commune de Bonneuil-sur-Marne a adressé au préfet du Val-de-Marne, sur le fondement des dispositions de l'article L. 125-1 du code des assurances citées au point 1, des demandes de reconnaissance de l'état de catastrophe naturelle ; que, par des arrêtés du 13 décembre 2010 et du 30 janvier 2012, les ministres chargés de l'intérieur, de l'économie et des finances et du budget ont fixé la liste des communes pour lesquelles a été constaté l'état de catastrophe naturelle au titre des mouvements de terrain différentiels consécutifs à la sécheresse et à la réhydratation des sols durant les années 2009 et 2010, au nombre desquelles ne figure pas la commune de Bonneuil-sur-Marne ; que par deux jugements des 1er février et 18 décembre 2013, le tribunal administratif de Melun a rejeté les demandes de la commune de Bonneuil-sur-Marne tendant à l'annulation pour excès de pouvoir des arrêtés des 13 décembre 2010 et 30 janvier 2012 en tant qu'ils ne la mentionnent pas parmi les communes où a été déclaré l'état de catastrophe naturelle ; que, par deux arrêts du 2 février 2015 rédigés en termes identiques, contre lesquels la commune de Bonneuil-sur-Marne se pourvoit en cassation, la cour administrative d'appel de Paris a rejeté ses appels contre ces jugements ; que les pourvois de la commune présentent à juger les mêmes questions ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              3. Considérant, en premier lieu, qu'il ressort des énonciations des arrêts attaqués que la cour, après avoir rappelé de manière détaillée la méthodologie et les paramètres scientifiques utilisés par les ministres compétents pour apprécier la survenance, sur le territoire d'une commune, d'une sécheresse d'une intensité anormale à l'origine de mouvements différentiels de terrain, a jugé qu'ils étaient de nature à permettre de caractériser une sécheresse d'une intensité anormale et répondaient ainsi aux objectifs prévus à l'article L. 125-1 du code des assurances ; que, dès lors, la commune requérante n'est pas fondée à soutenir que la cour aurait omis de répondre au moyen tiré du caractère inintelligible de la méthodologie et des paramètres mis en oeuvre pour apprécier l'intensité anormale du phénomène naturel ;<br/>
<br/>
              4. Considérant, en deuxième lieu, qu'il ressort des énonciations des jugements attaqués devant la cour que celle-ci n'a entaché son arrêt ni d'insuffisance de motivation, ni de dénaturation des pièces du dossier, en estimant que le tribunal administratif n'avait pas omis de statuer sur le moyen tiré de ce que les ministres signataires n'étaient pas compétents pour déterminer les paramètres permettant de définir l'état de catastrophe naturelle ;<br/>
<br/>
              5. Considérant, en troisième lieu, qu'aux termes du premier alinéa de l'article R. 621-1 du code de justice administrative, dans sa rédaction applicable au litige : " La juridiction peut, soit d'office, soit sur la demande des parties ou de l'une d'elles, ordonner, avant dire droit, qu'il soit procédé à une expertise sur les points déterminés par sa décision. " ; qu'il incombe, en principe, au juge administratif de statuer au vu des pièces du dossier, le cas échéant après avoir demandé aux parties les éléments complémentaires qu'il juge nécessaires à son appréciation ; qu'il ne lui appartient d'ordonner une expertise que lorsqu'il n'est pas en mesure de se prononcer au vu des pièces et éléments qu'il a recueillis et que l'expertise présente ainsi un caractère utile ;<br/>
<br/>
              6. Considérant qu'en estimant que l'expertise sollicitée par la commune requérante ne présentait pas un caractère utile, la cour s'est livrée à une appréciation souveraine des circonstances de l'espèce qui n'est entachée ni d'une dénaturation des pièces du dossier qui lui était soumis, ni d'une erreur de droit ;<br/>
<br/>
              7. Considérant, en quatrième lieu, qu'il résulte des dispositions de l'article L. 125-1 du code des assurances que le législateur a entendu confier aux ministres concernés la compétence pour se prononcer sur les demandes des communes tendant à la reconnaissance sur leur territoire de l'état de catastrophe naturelle ; qu'il leur appartient, à cet effet, d'apprécier l'intensité et l'anormalité des agents naturels en cause sur le territoire des communes concernées ; qu'ils peuvent légalement, même en l'absence de dispositions législatives ou réglementaires le prévoyant, s'entourer, avant de prendre les décisions relevant de leurs attributions, des avis qu'ils estiment utiles de recueillir et s'appuyer sur des méthodologies et paramètres scientifiques, sous réserve que ceux-ci apparaissent appropriés, en l'état des connaissances, pour caractériser l'intensité des phénomènes en cause et leur localisation, qu'ils ne constituent pas une condition nouvelle à laquelle la reconnaissance de l'état de catastrophe naturelle serait subordonnée ni ne dispensent les ministres d'un examen particulier des circonstances propres à chaque commune ; qu'il incombe enfin aux ministres concernés de tenir compte de l'ensemble des éléments d'information ou d'analyse dont ils disposent, le cas échéant à l'initiative des communes concernées ; <br/>
<br/>
              8. Considérant que la commission interministérielle prévue par la circulaire interministérielle du 27 mars 1984 n'a pour mission que d'éclairer les ministres sur l'application à chaque commune des méthodologies et paramètres scientifiques permettant de caractériser les phénomènes naturels en cause, notamment ceux issus des travaux de Météo France, les avis qu'elle émet ne liant pas les autorités dont relève la décision  ; qu'en jugeant qu'en s'appuyant sur les résultats issus de la méthodologie élaborée par cet organisme ainsi que sur l'avis de cette commission pour apprécier l'existence, dans les communes concernées, d'un état de catastrophe naturelle, les ministres n'avaient pas méconnu l'étendue de leur compétence, la cour n'a pas entaché son arrêt d'erreur de droit ;<br/>
<br/>
              9. Considérant, en cinquième lieu, qu'il résulte de ce qui a été dit précédemment qu'en se fondant, pour juger que le rejet des demandes de reconnaissance de l'état de catastrophe naturelle présentées par la commune de Bonneuil-sur-Marne n'était pas entaché d'une erreur d'appréciation, sur les paramètres météorologiques et géologiques constatés sur le territoire de cette commune en application de la méthodologie scientifique élaborée par Météo France, la cour n'a pas non plus commis d'erreur de droit ; <br/>
<br/>
              10. Considérant, en dernier lieu, que, pour juger que le refus de reconnaitre l'état de catastrophe naturelle pour la commune de Bonneuil-sur-Marne, alors que cet état avait été reconnu pour des communes voisines, n'avait pas été pris en méconnaissance du principe d'égalité, la cour s'est fondée sur ce que les ministres s'étaient appuyés sur les mêmes paramètres scientifiques pour apprécier l'existence ou non d'un état de catastrophe naturelle pour ces différentes communes ; que, ce faisant, la cour n'a pas commis d'erreur de droit ;  <br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui précède que la commune de Bonneuil-sur-Marne n'est pas fondée à demander l'annulation des arrêts qu'elle attaque ; que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : Les pourvois nos 389176 et 389177 sont rejetés.<br/>
Article 2 : La présente décision sera notifiée à la commune de Bonneuil-sur-Marne et au ministre d'État, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">12-02 ASSURANCE ET PRÉVOYANCE. CONTRATS D'ASSURANCE. - ETAT DE CATASTROPHE NATURELLE (ART. L. 125-1 DU CODE DES ASSURANCES) - DÉCISION DE RECONNAISSANCE PRISE PAR LES MINISTRES CONCERNÉS - FACULTÉ DE S'APPUYER SUR DES MÉTHODOLOGIES ET PARAMÈTRES SCIENTIFIQUES - 1) PRINCIPE - EXISTENCE, MÊME EN L'ABSENCE DE TEXTE - CONDITIONS - 2) ESPÈCE - MÉTHODOLOGIE ÉLABORÉE PAR MÉTÉO FRANCE [RJ1].
</SCT>
<ANA ID="9A"> 12-02 Il résulte de l'article L. 125-1 du code des assurances que le législateur a entendu confier aux ministres concernés la compétence pour se prononcer sur les demandes des communes tendant à la reconnaissance sur leur territoire de l'état de catastrophe naturelle. Il leur appartient, à cet effet, d'apprécier l'intensité et l'anormalité des agents naturels en cause sur le territoire des communes concernées.... ,,1) Ils peuvent légalement, même en l'absence de dispositions législatives ou réglementaires le prévoyant, s'entourer, avant de prendre les décisions relevant de leurs attributions, des avis qu'ils estiment utiles de recueillir et s'appuyer sur des méthodologies et paramètres scientifiques, sous réserve que ceux-ci apparaissent appropriés, en l'état des connaissances, pour caractériser l'intensité des phénomènes en cause et leur localisation, qu'ils ne constituent pas une condition nouvelle à laquelle la reconnaissance de l'état de catastrophe naturelle serait subordonnée ni ne dispensent les ministres d'un examen particulier des circonstances propres à chaque commune.... ,,2) Espèce - Légalité de la décision ministérielle fondée notamment sur les résultats issus de la méthodologie élaborée par Météo France.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de la possibilité pour le préfet de se fonder sur les travaux de Météo France pour apprécier le potentiel éolien d'une zone, CE, 30 janvier 2013, Société Eole les Patoures, n°s 355370 355732, T. pp. 619-809.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
