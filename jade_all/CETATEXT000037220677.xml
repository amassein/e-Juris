<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037220677</ID>
<ANCIEN_ID>JG_L_2018_07_000000406288</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/22/06/CETATEXT000037220677.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 18/07/2018, 406288</TITRE>
<DATE_DEC>2018-07-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>406288</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>Mme Dorothée Pradines</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:406288.20180718</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C...B...a demandé au tribunal administratif de Paris d'annuler la décision du 26 novembre 2015 par laquelle la présidente du conseil de Paris a confirmé le calcul de son allocation de revenu de solidarité active établi par la caisse d'allocations familiales de Paris et d'enjoindre au département de Paris de procéder à un nouvel examen de ses droits à compter du 1er octobre 2013. Par un jugement n° 1520159 du 20 octobre 2016, le tribunal administratif de Paris a annulé cette décision et enjoint à la présidente du conseil de Paris de réexaminer sa situation et de recalculer le montant de son revenu de solidarité active dans un délai de deux mois.<br/>
<br/>
              Par un pourvoi, enregistré le 24 décembre 2016 au secrétariat du contentieux du Conseil d'Etat, le département de Paris demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de MmeB....<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dorothée Pradines, auditeur,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat du département de Paris et à la SCP Monod, Colin, Stoclet, avocat de Mme B....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que, par une décision du 7 octobre 2013, la caisse d'allocations familiales de Paris a ouvert à Mme B...le droit au revenu de solidarité active. Le 23 septembre 2015, celle-ci a contesté les modalités de calcul de son allocation auprès de la présidente du conseil de Paris, qui a rejeté son recours par une décision du 26 novembre 2015. Le département de Paris se pourvoit en cassation contre le jugement du 20 octobre 2016 par lequel le tribunal administratif de Paris a annulé cette décision et enjoint à la présidente du conseil de Paris de réexaminer la situation de Mme B...et de recalculer le montant de son allocation de revenu de solidarité active.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 262-2 du code de l'action sociale et des familles, dans sa rédaction applicable jusqu'au 31 décembre 2015 : " Toute personne résidant en France de manière stable et effective, dont le foyer dispose de ressources inférieures à un revenu garanti, a droit au revenu de solidarité active dans les conditions définies au présent chapitre ". Pour être pris en compte au titre des droits du bénéficiaire, les autres membres du foyer doivent nécessairement remplir également cette condition de résidence stable et effective en France. <br/>
<br/>
              3. Dès lors, le département de Paris est fondé à soutenir qu'en jugeant que la circonstance que le conjoint de Mme B...vivait à l'étranger ne faisait pas obstacle au calcul des droits de Mme B...au revenu de solidarité active sur la base du montant dû à un couple avec trois enfants, le tribunal administratif a commis une erreur de droit.<br/>
<br/>
              4. Il résulte de ce qui précède que le département de Paris est fondé à demander l'annulation du jugement qu'il attaque. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens du pourvoi.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              Sur la recevabilité de la demande de MmeB... :<br/>
<br/>
              6. Aux termes de l'article L. 262-21 du code de l'action sociale et des familles, dans sa rédaction applicable au litige : " Il est procédé au réexamen périodique du montant de l'allocation définie à l'article L. 262-2. Les décisions qui en déterminent le montant sont révisées dès lors que des éléments nouveaux modifient la situation au vu de laquelle celles-ci sont intervenues. Les conditions d'application du présent article sont fixées par décret ". Aux termes de l'article R. 262-37 du même code : " Le bénéficiaire de l'allocation de revenu de solidarité active est tenu de faire connaître à l'organisme chargé du service de la prestation toutes informations relatives à sa résidence, à sa situation de famille, aux activités, aux ressources et aux biens des membres du foyer ; il doit faire connaître à cet organisme tout changement intervenu dans l'un ou l'autre de ces éléments ". En outre, il résulte de l'article R. 262-38 de ce code qu'il est procédé au calcul de l'allocation au vu de la déclaration de ressources que le bénéficiaire de l'allocation doit remplir chaque trimestre. Il suit de là que, lorsqu'un bénéficiaire a déposé une demande de réexamen de ses droits qui a été rejetée, le refus opposé à une nouvelle demande ayant le même objet n'a le caractère d'une décision confirmative qu'en tant qu'elle concerne la même période.<br/>
<br/>
              7. Il résulte de l'instruction que, par un courrier du 15 janvier 2015, qui doit être regardé comme un premier recours administratif préalable, obligatoire en vertu de l'article L. 262-47 du code de l'action sociale et des familles, A...B...a demandé à la présidente du conseil de Paris de réviser le montant de l'allocation de revenu de solidarité active qui lui était servie par la caisse d'allocations familiales de Paris depuis l'ouverture de son droit, le 1er octobre 2013, au motif que son foyer devait être regardé comme composé d'un couple et de trois enfants. Par une décision du 22 avril 2015, qui comportait la mention des voies et délais de recours, la présidente du conseil de Paris a rejeté sa demande. Par un courrier du 23 septembre 2015, Mme B...a de nouveau demandé la révision du montant de son allocation de revenu de solidarité active et le versement des sommes qu'elle estimait lui être dues depuis le 1er octobre 2013. La décision du 26 novembre 2015 par laquelle la présidente du conseil de Paris a rejeté sa demande est ainsi purement confirmative de la décision du 22 avril 2015, devenue définitive, en tant qu'elle concerne la période comprise entre le 1er octobre 2013 et le 30 juin 2015 et le département de Paris est fondé à soutenir que la requête de Mme B...est, dans cette mesure, tardive et par suite irrecevable. En revanche, il n'en va pas de même de sa requête en tant qu'elle concerne sa demande de réexamen de ses droits à compter du 1er juillet 2015. <br/>
<br/>
              Sur les droits de MmeB... :<br/>
<br/>
              8. Aux termes de l'article L. 262-2 du code de l'action sociale et des familles, dans sa rédaction en vigueur jusqu'au 31 décembre 2015 : " Toute personne résidant en France de manière stable et effective, dont le foyer dispose de ressources inférieures à un revenu garanti, a droit au revenu de solidarité active dans les conditions définies au présent chapitre. / Le revenu garanti est calculé, pour chaque foyer, en faisant la somme : / 1° D'une fraction des revenus professionnels des membres du foyer ; / 2° D'un montant forfaitaire, dont le niveau varie en fonction de la composition du foyer et du nombre d'enfants à charge. / Le revenu de solidarité active est une allocation qui porte les ressources du foyer au niveau du revenu garanti (...) ". Aux termes du deuxième alinéa de l'article L. 262-3 du même code : " (...) L'ensemble des ressources du foyer, y compris celles qui sont mentionnées à l'article L. 132-1, est pris en compte pour le calcul du revenu de solidarité active (...) ". En vertu de l'article L. 262-10 du même code, dans sa rédaction applicable au litige, le droit au revenu de solidarité active dit " socle " permettant de porter les ressources du foyer au niveau du montant forfaitaire mentionné au 2° de l'article L. 262-2 de ce code, " est subordonné à la condition que le foyer fasse valoir ses droits : / 1° Aux créances d'aliments qui lui sont dues au titre des obligations instituées par les articles 203, 212, 214, 255, 342 et 371-2 du code civil ainsi qu'à la prestation compensatoire due au titre de l'article 270 du même code ; (...) ". Enfin, aux termes de l'article L. 262-12 de ce code : " Le foyer peut demander à être dispensé de satisfaire aux obligations mentionnées aux deuxième à dernier alinéas de l'article L. 262-10. Le président du conseil départemental statue sur cette demande compte tenu de la situation du débiteur défaillant et après que le demandeur, assisté le cas échéant de la personne de son choix, a été mis en mesure de faire connaître ses observations. Il peut mettre fin au versement du revenu de solidarité active ou le réduire d'un montant au plus égal à celui de la créance alimentaire, lorsqu'elle est fixée, ou à celui de l'allocation de soutien familial. "<br/>
<br/>
              9. Il résulte de ces dispositions, qui ne méconnaissent pas les articles 8 et 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, que l'ensemble des ressources du foyer doit en principe être pris en compte pour le calcul de l'allocation de revenu de solidarité active. Toutefois, lorsque l'un des membres du foyer ne peut être pris en compte pour le calcul du revenu garanti du fait de sa résidence à l'étranger, il convient de prendre en considération non l'ensemble de ses ressources, mais les sommes qu'il verse au bénéficiaire du revenu de solidarité active ou les prestations en nature qu'il lui sert, au titre, notamment, de ses obligations alimentaires.<br/>
<br/>
              10. Il résulte de l'instruction que M.B..., qui réside et travaille en Tunisie, ne peut être regardé comme résidant en France de manière stable et effective. Par suite, Mme B... est fondée à soutenir que, alors même qu'elle n'est pas séparée de fait de son conjoint, les revenus de ce dernier devaient seulement être pris en compte dans le calcul des ressources du foyer dans les conditions définies au point 9. <br/>
<br/>
              11. Il résulte de ce qui précède que Mme B...est fondée à demander l'annulation de la décision du 26 novembre 2015 de la présidente du conseil de Paris en tant que celle-ci a refusé de réviser ses droits pour la période courant à compter du 1er juillet 2015.<br/>
<br/>
              12. La présente décision implique nécessairement qu'il soit procédé à un nouveau calcul des droits de Mme B...au revenu de solidarité active à compter du 1er juillet 2015, conformément aux motifs de la présente décision. Par suite, il y a lieu de renvoyer l'intéressée devant la présidente du conseil de Paris afin qu'il soit procédé à ce calcul et au versement des sommes correspondantes dans un délai de deux mois à compter de la notification de cette décision.<br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              13. Les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce qu'une somme soit mise à ce titre à la charge du département de Paris, qui n'est pas, pour l'essentiel, la partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Paris du 20 octobre 2016 est annulé.<br/>
Article 2 : La décision de la présidente du conseil de Paris du 26 novembre 2015 est annulée en tant qu'elle refuse de réviser les droits de Mme B...au revenu de solidarité active, pour la période courant à compter du 1er juillet 2015. <br/>
Article 3 : Il est enjoint à la présidente du conseil de Paris de recalculer le montant de l'allocation de revenu de solidarité active à laquelle Mme B...a droit et de lui verser les sommes correspondantes, conformément aux motifs de la présente décision, dans un délai de deux mois à compter de sa notification.<br/>
Article 4 : Les conclusions de Mme B...présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 et le surplus des conclusions de la demande présentée par Mme B...devant le tribunal administratif de Paris sont rejetés.<br/>
Article 5 : La présente décision sera notifiée à Mme C...B...et au département de Paris.<br/>
Copie en sera adressée à la ministre des solidarités et de la santé. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-06 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. REVENU MINIMUM D'INSERTION (RMI). - RSA - 1) REFUS OPPOSÉ À UNE DEMANDE DE RÉEXAMEN DES DROITS SUITE AU REJET D'UNE PREMIÈRE DEMANDE - DÉCISION CONFIRMATIVE - EXISTENCE, SEULEMENT EN TANT QU'ELLE CONCERNE LA MÊME PÉRIODE - 2) CALCUL DU MONTANT DE L'ALLOCATION - CAS DANS LEQUEL L'UN DES MEMBRES DU FOYER RÉSIDE À L'ÉTRANGER - PRISE EN COMPTE DES SOMMES VERSÉES OU DES PRESTATIONS EN NATURE SERVIES AU BÉNÉFICIAIRE DU RSA, NOTAMMENT AU TITRE DES OBLIGATIONS ALIMENTAIRES [RJ1].
</SCT>
<ANA ID="9A"> 04-02-06 1) Lorsqu'un bénéficiaire a déposé une demande de réexamen de ses droits qui a été rejetée, le refus opposé à une nouvelle demande ayant le même objet n'a le caractère d'une décision confirmative qu'en tant qu'elle concerne la même période.,,,2) Il résulte des articles L. 262-2, L. 262-3, L. 262-10 et L. 262-12 du code de l'action sociale et des familles (CASF), qui ne méconnaissent pas les articles 8 et 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, que l'ensemble des ressources du foyer doit en principe être pris en compte pour le calcul de l'allocation de revenu de solidarité active. Toutefois, lorsque l'un des membres du foyer ne peut être pris en compte pour le calcul du revenu garanti du fait de sa résidence à l'étranger, il convient de prendre en considération non l'ensemble de ses ressources, mais les sommes qu'il verse au bénéficiaire du revenu de solidarité active ou les prestations en nature qu'il lui sert, au titre, notamment, de ses obligations alimentaires.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 9 novembre 2016, Département de la Haute-Garonne, n° 392482, T. p. 638.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
