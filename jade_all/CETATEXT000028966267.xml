<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028966267</ID>
<ANCIEN_ID>JG_L_2014_05_000000365173</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/96/62/CETATEXT000028966267.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème SSR, 21/05/2014, 365173, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-05-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365173</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>Mme Maryline Saleix</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2014:365173.20140521</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 14 janvier et 15 avril 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Brit Air, dont le siège est Aéroport de Morlaix à Morlaix (29600), représentée par son président-directeur général en exercice ; elle demande au Conseil d'Etat : <br/>
<br/>
               1°) d'annuler l'arrêt n° 10VE02993 du 13 novembre 2012 par lequel la cour administrative d'appel de Versailles a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0900839 du 24 juin 2010 par lequel le tribunal administratif de Montreuil a rejeté sa demande en décharge des rappels de taxe sur la valeur ajoutée mis à sa charge au titre de la période allant du 1er avril 2001 au 31 août 2005, d'autre part, à la décharge de ces rappels de taxe ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 5 mai 2014, présentée pour la société Brit Air ;<br/>
<br/>
              Vu le traité sur le fonctionnement de l'Union européenne ; <br/>
<br/>
              Vu la sixième directive 77/388/CEE du Conseil des Communautés européennes du 17 mai 1977, en matière d'harmonisation des législations des Etats membres relatives aux taxes sur le chiffre d'affaires reprise par la directive 2006/112/CE du Conseil du 26 novembre 2006 relative au système commun de taxe sur la valeur ajoutée ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;	<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Maryline Saleix, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, avocat de la société Brit Air ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Brit Air exploitait des lignes commerciales dans le cadre d'un contrat de franchise avec la société Air France à laquelle elle avait confié la commercialisation et la gestion de la billetterie sur ces lignes ; qu'à ce titre, la société Air France percevait le prix des billets dus par les clients et le reversait à la société Brit Air pour chaque passager transporté par cette dernière ; que lorsque le billet vendu par la société Air France n'était pas utilisé par le client, soit en raison de la défaillance de celui-ci au moment de l'embarquement soit parce que la durée de validité du billet était expirée, la société Air France versait à la société Brit Air une compensation forfaitaire annuelle calculée en un pourcentage (2 %) du chiffre d'affaires annuel (TVA incluse) réalisé sur les lignes exploitées en franchise ; que celle-ci n'a pas soumis cette somme à la taxe sur la valeur ajoutée ; qu'à l'issue d'une vérification de comptabilité de la société Brit Air, l'administration fiscale lui a notifié, au titre de la période allant du 1er avril 2001 au 31 août 2005, des rappels de la taxe sur la valeur ajoutée portant sur des sommes reçues de la société Air France ; que la société Brit Air se pourvoit en cassation contre l'arrêt du 13 novembre 2012 par lequel la cour administrative d'appel de Versailles a confirmé le jugement du 9 juin 2011 du tribunal administratif de Montreuil qui a rejeté sa demande tendant à la décharge de ces  rappels de taxe sur la valeur ajoutée ;  <br/>
<br/>
              2. Considérant que l'article 2, paragraphe 1, de la sixième directive 77/388/CEE du Conseil du 17 mai 1977, applicable aux rappels de taxe contestés, repris à l'article 2 de la directive 2006/112/CE du Conseil du 28 novembre 2006 relative au système commun de taxe sur la valeur ajoutée, dispose que " Sont soumises à la taxe sur la valeur ajoutée, (...) les prestations de services effectuées à titre onéreux à l'intérieur du pays par un assujetti agissant en tant que tel " ; que l'article 10, paragraphe 2, de la directive 77/388/CEE, repris à l'article 63 de la directive 2006/112/CE, dispose que " le fait générateur de la taxe intervient et la taxe devient exigible au moment où la livraison de biens ou la prestation de services est effectuée " ; qu'aux termes du I de l'article 256 du code général des impôts, " Sont soumises à la taxe sur la valeur ajoutée les livraisons de biens et les prestations de services effectuées à titre onéreux par un assujetti agissant en tant que tel " ; qu'aux termes de l'article 269 du même code faisant application des facultés laissées aux Etats membres par le 2 de l'article 10 de la directive en ce qui concerne sa transposition: " 1. Le fait générateur de la taxe se produit : a) au moment où la livraison, l'acquisition intra communautaire du bien ou la prestation de services est réalisée (...) / 2. La taxe est exigible : (...) pour les prestations de services, lors de l'encaissement des acomptes, du prix, de la rémunération " ; <br/>
<br/>
              3. Considérant qu'ainsi que l'a jugé la Cour de justice des Communautés européennes dans son arrêt du 9 juillet 2009, rendu dans l'affaire C-204/08, Rehder c/ Air Baltic, en réponse à une demande de décision préjudicielle portant sur l'interprétation de l'article 5, point 1, sous b), second tiret, du règlement (CE) n° 44/2001 du Conseil du 22 décembre 2000 concernant la compétence judiciaire, la reconnaissance et l'exécution des décisions en matière civile et commerciale, les services dont la fourniture correspond à l'exécution des obligations découlant d'un contrat de transport aérien de personnes sont l'enregistrement ainsi que l'embarquement des passagers et l'accueil de ces derniers à bord de l'avion au lieu de décollage convenu dans le contrat de transport, le départ de l'appareil à l'heure prévue, le transport des passagers et de leurs bagages du lieu de départ au lieu d'arrivée, la prise en charge des passagers pendant le vol, et, enfin, le débarquement de ceux-ci, dans des conditions de sécurité, au lieu d'atterrissage et à l'heure convenus ; <br/>
<br/>
              4. Considérant que, pour l'application des dispositions fiscales citées au point 2, la société Brit Air soutient que la somme acquittée par le client qui renonce à utiliser son billet d'avion doit être regardée comme correspondant à une indemnité non soumise à la taxe sur la valeur ajoutée réparant le préjudice qui lui serait causé par la renonciation du client à exécuter le voyage ; qu'elle soutient également que cette somme ne peut être regardée comme la contrepartie d'une prestation de transport dès lors que le client n'a pas effectué le voyage correspondant au billet qu'il avait acheté et qu'en conséquence, il n'y a pas eu de fait générateur de la taxe sur la valeur ajoutée, en l'absence de réalisation de la prestation de transport ;<br/>
<br/>
              5. Considérant qu'elle soutient également que la somme qu'elle perçoit de la société Air France au titre des billets périmés ne peut être regardée comme correspondant à la contrepartie d'une prestation de transport accomplie au bénéfice de clients, dès lors que cette somme, par son caractère forfaitaire, ne correspond pas à la valeur de ces billets et constitue une indemnité compensatrice versée par Air France pour les recettes de billets émis et non utilisés ; <br/>
<br/>
              6. Considérant que le ministre soutient que les dispositions précitées, selon lesquelles le fait générateur de la taxe intervient au moment où la prestation de services est effectuée, n'impliquent pas l'exécution effective et matérielle de cette prestation au bénéfice du client mais doivent être interprétées en ce sens qu'elles permettent de regarder une prestation de transport comme effectuée dès le moment où le client acquiert, moyennant le paiement du prix convenu, un droit à son transport, par l'engagement souscrit par le prestataire lors de la conclusion du contrat de mettre à sa disposition, à travers le titre de transport, les moyens nécessaires à l'exécution du service, indépendamment de leur utilisation par l'acheteur ; qu'il invoque notamment à l'appui de son argumentation une interprétation commune des Etats membres, formulée dans le compte rendu de la 99ème réunion qu'a tenue le 3 juillet 2013 le comité de la taxe sur la valeur ajoutée, selon laquelle, lorsqu'un client paie le prix du billet mais ne prend pas son vol sans annuler la réservation, le prix payé par le client et retenu par la compagnie aérienne ne peut être qualifié de garantie retenue en compensation d'une perte, mais doit être regardé comme la contrepartie d'une prestation de services de la compagnie aérienne qui est, à ce titre, soumise à la taxe sur la valeur ajoutée ; qu'il soutient également que la somme forfaitaire que la société Brit Air perçoit de la société Air France au titre des billets émis et non utilisés constitue la contrepartie de prestations de services rendues aux clients qui en ont acquitté le prix, indépendamment de l'utilisation de ces prestations, et par conséquent, ne présente pas de caractère indemnitaire ;<br/>
<br/>
              7. Considérant qu'il y a lieu de rechercher une application uniforme au sein de l'Union européenne des règles d'assujettissement à la taxe sur la valeur ajoutée des prestations de services ; qu'il convient donc, avant de statuer sur le pourvoi formé par la société Brit Air, en premier lieu, de déterminer si les dispositions précitées de la sixième directive doivent être interprétées en ce sens qu'une somme forfaitaire calculée en pourcentage du chiffre d'affaires annuel (TVA incluse) réalisé sur les lignes exploitées en franchise, comme celle évaluée en l'espèce à 2 %, et reversée par une compagnie aérienne qui a émis pour le compte d'une autre des billets qui deviennent périmés constitue une indemnité non imposable versée à cette dernière, réparant le préjudice subi du fait de la vaine mobilisation par celle-ci de ses moyens de transport, ou une somme correspondant au prix des billets émis et périmés ; que, dans ce dernier cas, il convient en second lieu de déterminer si les dispositions précitées doivent être interprétées en ce sens que la délivrance du billet peut être assimilée à l'exécution effective de la prestation de transport et si ces dispositions impliquent que les sommes conservées par une compagnie aérienne lorsque le titulaire du billet d'avion n'a pas utilisé son billet et que celui-ci est devenu périmé sont soumises à la taxe sur la valeur ajoutée ; qu'il convient également de déterminer si, dans le cas où la réponse à cette dernière question est affirmative, la taxe collectée doit être reversée au Trésor dès l'encaissement du prix, alors même que le voyage peut ne pas avoir lieu du fait du client ; <br/>
<br/>
              8. Considérant que ces questions sont déterminantes pour la solution du litige que doit trancher le Conseil d'Etat ; qu'elles présentent une difficulté sérieuse ; qu'il y a lieu, par suite, d'en saisir la Cour de justice de l'Union européenne en application de l'article 267 du traité sur le fonctionnement de l'Union européenne et, jusqu'à ce que celle-ci se soit prononcée, de surseoir à statuer sur le pourvoi de la société Brit Air ;   <br/>
<br/>
<br/>
<br/>                   D E C I D E :<br/>
                                    --------------<br/>
<br/>
 Article 1er : Il est sursis à statuer sur le pourvoi de la société Brit Air jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur les questions suivantes :<br/>
              - les dispositions des articles 2§1 et 10§2 de la directive 77/388/CEE du Conseil du 17 mai 1977 doivent-elles être interprétées en ce sens que la somme forfaitaire calculée en pourcentage du chiffre d'affaires annuel réalisé sur les lignes exploitées en franchise et reversée par une compagnie aérienne qui a émis pour le compte d'une autre des billets qui deviennent périmés constitue une indemnité non imposable versée à cette dernière, réparant le préjudice indemnisable subi du fait de la vaine mobilisation par celle-ci de ses moyens de transport ou une somme correspondant aux recettes des billets émis et périmés '<br/>
              - dans le cas où cette somme serait réputée correspondre au prix des billets émis et périmés, ces dispositions doivent-elles être interprétées en ce sens que la délivrance du billet peut être assimilée à l'exécution effective de la prestation de transport et que les sommes conservées par une compagnie aérienne lorsque le titulaire du billet d'avion n'a pas utilisé son billet et que celui-ci est devenu périmé sont soumises à la taxe sur la valeur ajoutée '<br/>
              - dans cette hypothèse, la taxe collectée doit-elle être reversée au Trésor par la société Air France ou par la société Brit Air dès l'encaissement du prix, alors même que le voyage peut ne pas avoir lieu du fait du client ' <br/>
<br/>
 Article 2 : La présente décision sera notifiée à la société Brit Air, devenue SAS Hop!- Brit Air, au ministre des finances et des comptes publics et au président de la Cour de justice de l'Union européenne.<br/>
 Copie en sera adressée, pour information, au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
