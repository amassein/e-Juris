<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043926986</ID>
<ANCIEN_ID>JG_L_2021_08_000000439050</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/92/69/CETATEXT000043926986.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 06/08/2021, 439050, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-08-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439050</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. François Charmont</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:439050.20210806</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Paris de condamner l'Etat à lui verser une somme de 126 750,55 euros en réparation des préjudices qu'il estime avoir subis en raison de fautes commises dans sa prise en charge au centre hospitalier des armées Bouffard. Par un jugement n° 1719423/6-1 du 29 mars 2019, le tribunal administratif a condamné l'Etat, d'une part, à lui verser la somme de 140 083 euros ainsi que, à compter du 16 novembre 2016, une rente annuelle d'un montant de 652,17 euros.<br/>
<br/>
              Par un arrêt n° 19PA01778, 19PA01779 du 26 septembre 2019, la cour administrative d'appel de Paris a, sur appel de la ministre des armées, ramené à 10 091 euros la somme que l'Etat est condamné à verser à M. A....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés le 24 février 2020 et le 30 avril 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la ministre des armées ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761 1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Charmont, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Piwnica, Molinié, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'une opération subie par M. A... le 21 février 2002 au centre hospitalier des armées Bouffard à Djibouti, le ministre des armées a reconnu la responsabilité de l'Etat pour les préjudices subis par l'intéressé. Celui-ci se pourvoit en cassation contre l'arrêt du 26 septembre 2019 par lequel la cour administrative d'appel de Paris a, sur appel de la ministre des armées, ramené à 10 091 euros la somme de 140 083 euros que, par son jugement du 29 mars 2019, le tribunal administratif de Paris avait condamné l'Etat à lui verser.<br/>
<br/>
              2. En premier lieu, il ressort des termes mêmes de l'arrêt attaqué qu'il adopte les motifs du jugement du tribunal administratif de Paris en tant qu'il juge que M. A... doit être indemnisé à hauteur de 4 500 euros pour l'aggravation de son déficit fonctionnel permanent. Toutefois, l'article 1er de ce même arrêt omet de prendre en compte cette somme dans le montant qu'il condamne l'Etat à verser à M. A.... Il y a lieu, par suite, de rectifier cette erreur matérielle en portant la somme due à M. A... de 10 091 euros à 14 591 euros.<br/>
<br/>
              3. En second lieu, aux termes de l'article L. 341-1 du code de la sécurité sociale : " L'assuré a droit à une pension d'invalidité lorsqu'il présente une invalidité réduisant dans des proportions déterminées sa capacité de travail ou de gain, c'est-à-dire le mettant hors d'état de se procurer un salaire supérieur à une fraction de la rémunération soumise à cotisations et contributions sociales qu'il percevait dans la profession qu'il exerçait avant la date de l'interruption de travail suivie d'invalidité ou la date de la constatation médicale de l'invalidité ". Eu égard à la finalité de réparation d'une incapacité permanente de travail qui lui est assignée par ces dispositions législatives et à son mode de calcul, en fonction du salaire, fixé par l'article R. 341-4 du code de la sécurité sociale, la pension d'invalidité doit être regardée comme ayant pour objet exclusif de réparer, sur une base forfaitaire, les préjudices subis par la victime dans sa vie professionnelle en conséquence de l'accident, c'est-à-dire ses pertes de revenus professionnels et l'incidence professionnelle de l'incapacité. <br/>
<br/>
              4. Il résulte des termes de l'arrêt attaqué que, faisant application des principes rappelés ci-dessus, la cour administrative d'appel, après avoir relevé que l'aggravation de l'état de santé de M. A... était à l'origine d'un préjudice d'incidence professionnelle, a soustrait de l'indemnisation qui lui était due au titre de ce préjudice le montant de la pension d'invalidité qui lui était versée. Si M. A... soutient pour la première fois en cassation que cette pension d'invalidité a également pour objet la réparation d'autres préjudices que ceux résultant de l'accident médical fautif, cette circonstance, qui ne ressort pas des pièces du dossier soumis à la cour administrative d'appel, soulève un moyen nouveau, qui n'est pas d'ordre public et est, par suite, inopérant.<br/>
<br/>
              5. Il résulte de tout ce qui précède que M. A... n'est fondé qu'à demander la rectification de l'arrêt qu'il attaque en tant qu'il omet de réparer, dans la somme que l'Etat est condamné à lui verser, les souffrances endurées au titre de l'aggravation de son déficit fonctionnel permanent. <br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à M. A... au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 1er de l'arrêt de la cour administrative de Paris du 26 septembre 2019 est rectifié conformément aux motifs du point 2 de la présente décision.<br/>
<br/>
		Article 2 : Le surplus des conclusions du pourvoi est rejeté.<br/>
<br/>
Article 3 : L'Etat versera à M. A... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. B... A... et à la ministre des armées.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
