<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029471751</ID>
<ANCIEN_ID>JG_L_2014_08_000000383247</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/47/17/CETATEXT000029471751.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 01/08/2014, 383247, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-08-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383247</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MEIER-BOURDEAU, LECUYER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2014:383247.20140801</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 29 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par M. B...A...et Mme C...A..., élisant domicile chez FranceTerre d'Asile, 7, rue du Docteur Roux à Caen (14000) ; les requérants demandent au juge des référés du Conseil d'Etat : <br/>
              1°) d'annuler l'ordonnance n° 1401457 du 17 juillet 2014 par laquelle le juge des référés du tribunal administratif de Caen, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté leur demande tendant, d'une part, à l'annulation de la décision du 10 juillet 2014 du directeur départemental de la cohésion sociale du Calvados refusant de leur accorder ainsi qu'à leurs enfants un hébergement d'urgence, d'autre part, à ce qu'il soit enjoint au préfet du Calvados de les prendre en charge et de leur fournir sans délai un hébergement d'urgence en application du dispositif général de veille sociale prévu par l'article L. 345-2-2 du code de l'action sociale et des familles ;<br/>
              2°) de faire droit à leur demande de première instance ;<br/>
              3°) de les admettre au bénéfice de l'aide juridictionnelle à titre provisoire et de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              ils soutiennent que : <br/>
              - la condition d'urgence est remplie, dès lors qu'ils se trouvent, ainsi que leurs enfants, sans logement et dorment dans la rue ; <br/>
              - le préfet du Calvados a porté une atteinte grave et manifestement illégale au droit à l'hébergement d'urgence ;<br/>
              - le préfet du Calvados a méconnu les dispositions de l'article L. 345-2-2 du code de l'action sociale et des familles en ce qu'aucune diligence n'a été accomplie par l'administration en vue de la recherche d'une solution adaptée à leur situation ;<br/>
              - le fait, pour leurs enfants mineurs, de ne pas disposer d'un lieu sécurisé où dormir est constitutif d'une violation de l'article 3 de la convention de New-York relative aux droits de l'enfant, de l'article 24 de la Charte des droits fondamentaux de l'Union européenne et du principe de dignité humaine reconnu par l'article 3 de la Convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et de l'article 16 du code civil ;<br/>
<br/>
<br/>
              Vu l'ordonnance attaquée ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 1er août 2014, présenté par la ministre des affaires sociales et de la santé, qui conclut, à titre principal, au non-lieu à statuer, dès lors qu'une solution d'hébergement a été proposée aux requérants et à leurs enfants et, à titre subsidiaire, au rejet de la requête dès lors qu'il n'existe pas de carence caractérisée des autorités de l'Etat dans la mise en oeuvre du droit à l'hébergement d'urgence eu égard aux moyens dont dispose l'administration et à la situation personnelle des intéressés ; <br/>
<br/>
              Vu le mémoire en réplique, enregistré le 1er août 2014, présenté par les requérants qui acquiescent aux conclusions à fin de non-lieu, tout en maintenant leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
              Vu les observations, enregistrées le 1er août 2014, produites par le Défenseur des droits ; <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'action sociale et des familles ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ; <br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. et Mme A... et, d'autre part, le ministre des affaires sociales et de la santé ainsi que le ministre de l'intérieur ;<br/>
<br/>
              Vu la lettre informant les parties de la radiation de l'affaire du rôle de l'audience publique du 4 août 2014 ; <br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il résulte de l'instruction que M. et MmeA..., ressortissants azerbaïdjanais, sont entrés en France, avec leurs enfants, le 10 juillet 2014 en vue d'y demander le statut de réfugié ; qu'ils ont sollicité un rendez-vous à la préfecture du Calvados afin d'y déposer une demande d'asile ; qu'un rendez-vous leur a été fixé le 13 août 2014 ; qu'ils bénéficient d'une domiciliation postale auprès de l'association France Terre d'Asile ; que, par décision du 10 juillet 2014, le directeur départemental de la cohésion sociale du Calvados a refusé de leur accorder un hébergement d'urgence ; que, depuis, ils se sont trouvés sans abri, dormant dans la rue ; que les intéressés ont saisi le juge des référés du tribunal administratif de Caen, sur le fondement de l'article L. 521-2 du code de justice administrative, d'une demande tendant à ce qu'il soit enjoint au préfet du Calvados de leur fournir sans délai un hébergement d'urgence pouvant les accueillir ainsi que leurs enfants ; qu'ils relèvent appel de l'ordonnance du 17 juillet 2014 par laquelle le juge des référés du tribunal administratif de Caen a rejeté leur demande ;<br/>
<br/>
              2. Considérant, toutefois, que, le 24 juillet 2014, postérieurement à l'introduction de la requête, la famille de M. et de Mme A...a obtenu, à compter du 1er août 2014, un hébergement d'urgence qui lui a été garanti jusqu'au 13 août 2014 inclus ; que, dans ces conditions, leurs conclusions aux fins d'injonction sont devenues sans objet ; qu'il n'y a, dès lors, plus lieu d'y statuer ; <br/>
<br/>
              3. Considérant que si, dans leur mémoire en réplique, les requérants déclarent maintenir leurs conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative, il n'y a pas lieu, dans les circonstances de l'espèce, d'y faire droit ni de les admettre au bénéfice de l'aide juridictionnelle à titre provisoire ;<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
 Article 1er : Il n'y a pas lieu de statuer sur les conclusions de M. et Mme A...dirigées contre l'ordonnance du juge des référés du tribunal administratif de Caen du 17 juillet 2014 et tendant à ce que le juge des référés fasse usage des pouvoirs qu'il tient de l'article L. 521-2 du code de justice administrative.<br/>
<br/>
 Article 2  : Le surplus des conclusions de la requête est rejeté.<br/>
<br/>
 Article 3 : La présente ordonnance sera notifiée à M. B...A...et Mme C...A...ainsi qu'à la ministre des affaires sociales et de la santé et au ministre de l'intérieur.<br/>
<br/>
 Copie en sera adressée pour information au Défenseur des droits.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
