<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044806239</ID>
<ANCIEN_ID>JG_L_2021_12_000000450823</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/80/62/CETATEXT000044806239.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 30/12/2021, 450823, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450823</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Edouard Solier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:450823.20211230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Commission nationale des comptes de campagne et des financements politiques a, sur le fondement de l'article L. 52-15 du code électoral, saisi le tribunal administratif de Nîmes de sa décision du 7 décembre 2020 rejetant le compte de campagne de M. A... D..., candidat tête de liste aux élections municipales du 15 mars 2020 dans la commune d'Avignon (Vaucluse). Par un jugement n° 2003769 du 19 février 2021, le tribunal administratif a jugé que la Commission nationale des comptes de campagne et des financements politiques avait rejeté à bon droit le compte de campagne de M. D... et qu'il n'y avait pas lieu de déclarer l'intéressé inéligible.<br/>
<br/>
              Par une requête, enregistrée le 18 mars 2021 au secrétariat du contentieux du Conseil d'Etat, M. D... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il a confirmé la décision de la Commission nationale des comptes de campagne et des financements politiques du 7 décembre 2020 rejetant son compte de campagne ;<br/>
<br/>
              2°) de constater que c'est à tort que la Commission nationale des comptes de campagne et des financements politiques a rejeté son compte de campagne.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Edouard Solier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte de l'instruction que M. A... D... a été candidat tête de liste aux élections qui se sont déroulées le 15 mars 2020 pour l'élection des conseillers municipaux et communautaires de la commune d'Avignon (Vaucluse). Par une décision du 7 décembre 2020, la Commission nationale des comptes de campagne et des financements politiques a rejeté son compte de campagne au motif qu'il a réglé directement des dépenses d'un montant de 8 708 euros, en méconnaissance des dispositions de l'article L. 52-4 du code électoral et saisi le tribunal administratif de Nîmes de cette décision, en application de l'article L. 52-15 du même code. M. D... relève appel du jugement du 19 février 2021 en tant que le tribunal administratif de Nîmes, statuant sur cette saisine, a jugé que son compte de campagne avait été rejeté à bon droit par la Commission nationale des comptes de campagne et des financements politiques.<br/>
<br/>
              2. Aux termes de l'article L. 52-4 du code électoral : " Tout candidat à une élection déclare un mandataire conformément aux articles L. 52-5 et L. 52-6 au plus tard à la date à laquelle sa candidature est enregistrée. Ce mandataire peut être une association de financement électoral, ou une personne physique dénommée " le mandataire financier ". Un même mandataire ne peut être commun à plusieurs candidats (...) / Il règle les dépenses engagées en vue de l'élection et antérieures à la date du tour de scrutin où elle a été acquise, à l'exception des dépenses prises en charge par un parti ou groupement politique. Les dépenses antérieures à sa désignation, payées directement par le candidat ou à son profit, ou par l'un des membres d'un binôme de candidats ou au profit de ce membre, font l'objet d'un remboursement par le mandataire et figurent dans son compte de dépôt / (...) ".<br/>
<br/>
              3. En raison de la finalité poursuivie par les dispositions précitées de l'article L. 52-4 du code électoral, l'obligation de recourir à un mandataire pour toute dépense effectuée en vue de la campagne constitue une formalité substantielle à laquelle il ne peut, en principe, être dérogé. Si, par dérogation à la formalité substantielle que constitue l'obligation de recourir à un mandataire pour toute dépense effectuée en vue de sa campagne, le règlement direct de menues dépenses par le candidat peut être admis, ce n'est qu'à la double condition que leur montant, en vertu des dispositions précitées, c'est-à-dire prenant en compte non seulement les dépenses intervenues après la désignation du mandataire financier mais aussi celles réglées avant cette désignation et qui n'auraient pas fait l'objet d'un remboursement par le mandataire, soit faible par rapport au total des dépenses du compte de campagne et négligeable au regard du plafond de dépenses autorisées fixé par l'article L. 52-11 du code électoral.<br/>
<br/>
              4. Il résulte de l'instruction et il n'est pas contesté que M. D... a réglé directement ou par l'intermédiaire d'une de ses colistières une somme de 8 708 euros, soit 59% du montant total des dépenses déclarées dans son compte de campagne, lesquelles s'élèvent à 14 739 euros, et 8% du plafond des dépenses applicables à cette élection, sans que cette somme ait fait l'objet d'un remboursement de la part de son mandataire financier. Ce montant, qui n'est ni faible au regard du total des dépenses de campagne de M. D..., ni négligeable au regard du plafond des dépenses autorisées, justifie le rejet de son compte de campagne par la Commission nationale des comptes de campagne et des financements politiques, sans que M. D... puisse utilement se prévaloir des difficultés qu'il aurait rencontrées dans la désignation de son mandataire financier et de l'ouverture de son compte de campagne ou de la circonstance que certaines de ses dépenses auraient été réglées à son insu ainsi qu'à celle de son mandataire financier, par une de ses colistières.<br/>
<br/>
              5. Il résulte de tout ce qui précède que M. D... n'est pas fondé à soutenir que c'est à tort que par le jugement attaqué, le tribunal administratif de Nîmes a jugé que son compte de campagne avait été rejeté à bon droit par la Commission nationale des comptes de campagnes et des financements politiques et qu'il n'avait pas droit au remboursement forfaitaire de l'Etat en vertu de l'article L. 52-11-1 du code électoral.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. D... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A... D... et à la Commission nationale des comptes de campagne et des financements politiques.<br/>
Copie en sera adressée au ministre de l'intérieur.<br/>
              Délibéré à l'issue de la séance du 2 décembre 2021 où siégeaient : Mme Maud Vialettes, présidente de chambre, présidant ; Mme Fabienne Lambolez, conseillère d'Etat et M. Edouard Solier, maître des requêtes-rapporteur. <br/>
<br/>
              Rendu le 30 décembre 2021.<br/>
<br/>
                 La présidente : <br/>
                 Signé : Mme Maud Vialettes<br/>
 		Le rapporteur : <br/>
      Signé : M. Edouard Solier<br/>
                 La secrétaire :<br/>
                 Signé : Mme C... B...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
