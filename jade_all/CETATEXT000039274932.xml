<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039274932</ID>
<ANCIEN_ID>JG_L_2019_10_000000420485</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/27/49/CETATEXT000039274932.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 23/10/2019, 420485</TITRE>
<DATE_DEC>2019-10-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420485</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP SEVAUX, MATHONNET ; SCP LEDUC, VIGAND</AVOCATS>
<RAPPORTEUR>Mme Louise Cadin</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2019:420485.20191023</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. D... C... a demandé au tribunal administratif de Nice de condamner le centre hospitalier (CH) de Cannes, ou, à titre subsidiaire, l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM), à lui verser la somme de 240 213,62 euros en réparation des préjudices qu'il estime avoir subis du fait de sa prise en charge à compter du 9 septembre 2009 dans cet établissement. La caisse primaire d'assurance maladie (CPAM) des Alpes-Maritimes a demandé au tribunal de condamner le CH de Cannes à lui verser la somme de 166 756,79 euros au titre des débours exposés. Par un jugement n° 1202063 du 11 mai 2015, le tribunal administratif de Nice a condamné le CH de Cannes à verser à M. C... la somme de 75 187 euros et à la CPAM des Alpes-Maritimes la somme de 83 378,38 euros au titre des débours et la somme de 1 028 euros au titre de l'indemnité forfaitaire de gestion.<br/>
<br/>
              Par un arrêt n° 15MA03017 du 8 mars 2018, la cour administrative d'appel de Marseille a, sur appel du CH de Cannes, de M. C..., de l'ONIAM et de la CPAM des Alpes-Maritimes, annulé ce jugement en tant qu'il statue sur l'indemnisation des préjudices patrimoniaux et personnels de M. C... ainsi que sur les demandes de la CPAM des Alpes-Maritimes et a, d'une part, condamné le CH de Cannes à indemniser M. C... des conséquences de l'infection nosocomiale subie à hauteur de 3 500 euros et, d'autre part, s'agissant des préjudices consécutifs au défaut d'information et aux fautes médicales, ordonné une expertise avant dire droit et condamné le centre hospitalier à verser à M. C... la somme de 72 000 euros à titre provisionnel.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 9 mai et 27 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, le CH de Cannes demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... A..., auditrice,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat du centre hospitalier de Cannes et à la SCP Leduc, Vigand, avocat de M. C... et à la SCP Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que M. C... été hospitalisé en urgence au centre hospitalier de Cannes le 9 septembre 2009 et qu'une thrombose des artères fémorale et poplitée du membre inférieur droit a été diagnostiquée. Le 23 septembre 2009, il a subi dans cet établissement un pontage fémoro-poplité, avec pose d'une prothèse vasculaire. Toutefois, le 28 octobre 2009, l'occlusion du pontage a été constatée et M. C... a subi une seconde intervention en urgence pour l'ablation de la prothèse et la réalisation d'un nouveau pontage, selon une technique différente. A la suite de ces opérations, M. C... a présenté un syndrome des loges et une paralysie du nerf sciatique poplité externe et a été placé en coma artificiel au service de réanimation du centre hospitalier de Cannes du 5 au 12 novembre 2009. Le 13 avril 2010, un scanner a révélé que M. C... souffrait d'une ostéite du talon du pied droit, qui a nécessité plusieurs opérations et finalement l'amputation partielle de la jambe droite. Par ailleurs, des prélèvements peropératoires ont révélé la présence de germes, résultant d'une endocardite sur sonde, qui a nécessité l'explantation, le 29 avril 2010, du défibrillateur qui avait été posé sur lui en 2008.<br/>
<br/>
              2. Par un jugement du 11 mai 2015, le tribunal administratif de Nice a condamné le centre hospitalier à verser à M. C... la somme de 75 187 euros et à la caisse primaire d'assurance maladie des Alpes-Maritimes la somme de 83 378, 38 euros. Sur appels du centre hospitalier et de M. C..., la cour administrative d'appel de Marseille a, le 8 mars 2018, d'une part, condamné le centre hospitalier de Cannes à indemniser M. C... pour l'infection nosocomiale subie à hauteur de 3 500 euros et, d'autre part, s'agissant des préjudices consécutifs au défaut d'information et aux fautes médicales, ordonné une expertise avant dire droit et condamné le centre hospitalier à verser à M. C... la somme de 72 000 euros à titre provisionnel. Le centre hospitalier de Cannes se pourvoit en cassation contre cet arrêt. Par la voie du pourvoi incident, M. C... en demande l'annulation en tant qu'il ne fait pas intégralement droit à son appel.<br/>
<br/>
              Sur le pourvoi du centre hospitalier de Cannes :<br/>
<br/>
              En ce qui concerne la responsabilité du centre hospitalier de Cannes :<br/>
<br/>
              3. En premier lieu, en estimant que le choix, lors de l'intervention du 23 septembre 2009, d'un pontage prothétique et non d'un pontage veineux était inapproprié et que la réalisation, en l'espèce, d'un pontage long plutôt que d'un pontage court n'était pas conforme aux bonnes pratiques médicales, la cour a porté sur les pièces du dossier qui lui était soumis une appréciation souveraine, qui n'est pas entachée de dénaturation. En jugeant, par suite, que l'exécution de cet acte médical était entachée d'une faute de nature à engager la responsabilité du service public hospitalier, elle exactement qualifié les faits qui lui étaient soumis, par un arrêt suffisamment motivé sur ce point, alors même que celui-ci ne fait pas expressément mention du rapport d'expertise produit par le centre hospitalier.<br/>
<br/>
              4. En deuxième lieu, aux termes de l'article L. 1111-2 du code de la santé publique : " Toute personne a le droit d'être informée sur son état de santé. Cette information porte sur les différentes investigations, traitements ou actions de prévention qui sont proposés, leur utilité, leur urgence éventuelle, leurs conséquences, les risques fréquents ou graves normalement prévisibles qu'ils comportent ainsi que sur les autres solutions possibles et sur les conséquences prévisibles en cas de refus. (...) Cette information est délivrée au cours d'un entretien individuel. (...) En cas de litige, il appartient au professionnel ou à l'établissement de santé d'apporter la preuve que l'information a été délivrée à l'intéressé dans les conditions prévues au présent article. Cette preuve peut être apportée par tout moyen ". Il résulte de ces dispositions qu'un manquement des médecins à leur obligation d'information engage la responsabilité de l'hôpital dans la mesure où il a privé le patient d'une chance de se soustraire au risque lié à l'intervention en refusant qu'elle soit pratiquée. Ce n'est que dans le cas où l'intervention était impérieusement requise, en sorte que le patient ne disposait d'aucune possibilité raisonnable de refus, que les juges du fond peuvent écarter l'existence d'une perte de chance.<br/>
<br/>
              5. Il ressort des termes mêmes de l'arrêt attaqué que la cour s'est fondée, pour juger que le centre hospitalier avait méconnu son obligation d'information, sur ce que M. C... n'avait pas été informé des alternatives, des risques d'échec et des risques de complications liés à la technique du pontage prothétique. Le manquement à l'obligation d'information retenu par la cour portant ainsi, non sur le principe même de la réalisation d'un pontage mais sur le choix du pontage prothétique plutôt que du pontage veineux, la circonstance que la réalisation d'un pontage aurait été impérieusement requise était sans incidence sur la perte de chance résultant spécifiquement du choix de la technique utilisée. Par suite, la cour n'a pas commis d'erreur de droit ni entaché son arrêt d'insuffisance de motivation en s'abstenant de rechercher si, ainsi que cela était soutenu devant elle par le centre hospitalier requérant, la réalisation d'un pontage était, le 23 septembre 2009, impérieusement requise. <br/>
<br/>
              6. En troisième lieu, en jugeant que les fautes médicales commises pendant l'intervention du 23 septembre 2009 avaient représenté, pour M. C..., une perte de chance d'obtenir un pontage avec un bon taux de perméabilité qu'il convenait de fixer à 90%, la cour, qui a suffisamment motivé son arrêt, a porté sur les pièces du dossier qui lui étaient soumis une appréciation souveraine, exempte de dénaturation. <br/>
<br/>
              7. Il résulte de ce qui précède que le centre hospitalier de Cannes n'est pas fondé à demander l'annulation de l'arrêt en tant qu'il se prononce sur sa responsabilité.<br/>
<br/>
              En ce qui concerne la provision :<br/>
<br/>
              8. Le juge du fond peut accorder une provision au créancier qui l'a saisi d'une demande indemnitaire lorsqu'il constate qu'un agissement de l'administration a été à l'origine d'un préjudice et que, dans l'attente des résultats d'une expertise permettant de déterminer l'ampleur de celui-ci, il est en mesure de fixer un montant provisionnel dont il peut anticiper qu'il restera inférieur au montant total qui sera ultérieurement défini.<br/>
<br/>
              9. Il ressort des termes mêmes de l'arrêt attaqué que la cour a jugé que le centre hospitalier avait commis un manquement à son obligation d'information et pris deux décisions fautives dans le choix de la technique de pontage et que ces fautes avaient été, au moins partiellement, à l'origine d'une obstruction de l'artère provoquant l'ischémie aiguë des tissus et des muscles dévascularisés ainsi qu'une paralysie du nerf poplité, elles-mêmes à l'origine des soins successifs puis de l'amputation. <br/>
<br/>
              10. En se prononçant ainsi sur l'existence d'un lien de causalité entre les agissements du service public hospitalier et les préjudices subis par la victime, la cour, qui n'était pas tenue, pour fixer le montant d'une indemnité provisionnelle, de préciser les liens entre les faits fautifs et chacun des postes du préjudice invoqué par la victime, a suffisamment motivé son arrêt.<br/>
<br/>
              11. Il résulte de ce qui précède que le centre hospitalier de Cannes n'est pas fondé à demander l'annulation de l'arrêt attaqué en tant qu'il statue sur les conclusions de M. C... tendant à l'octroi d'une provision.<br/>
<br/>
              Sur le pourvoi incident de M. C... :<br/>
<br/>
              12. En fixant à 72 000 euros la provision mise à la charge du centre hospitalier de Cannes au titre des préjudices en lien avec le pontage du 23 septembre 2009, la cour a porté sur les faits de l'espèce une appréciation souveraine, exempte de dénaturation.<br/>
<br/>
              13. Par ailleurs, en condamnant le centre hospitalier à verser à M. C... une somme de 3 500 euros en réparation des préjudices résultant de l'infection nosocomiale contractée dans cet établissement, la cour a, sans commettre l'erreur de droit qui aurait consisté à ne pas appliquer le principe de réparation intégrale du préjudice subi par l'intéressé, porté sur le montant de ce préjudice une appréciation souveraine, qui n'est pas entachée de dénaturation.<br/>
<br/>
              14. Il résulte de ce qui est dit aux deux points précédents que le pourvoi incident de M. C... doit être rejeté. <br/>
<br/>
              Sur les conclusions présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 :<br/>
<br/>
              15. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier de Cannes une somme de 2 000 euros à verser à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales au titre de l'article L.761-1 du code de justice administrative et une somme de 2000 euros à verser à la SCP Leduc et Vigand, avocat de M. C..., au titre du même article et de l'article 37 de la loi du 10 juillet 1991, sous réserve que celle-ci renonce à la part contributive de l'Etat.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du centre hospitalier de Cannes et le pourvoi incident de M. C... sont rejetés. <br/>
Article 2 : Le centre hospitalier de Cannes versera une somme de 2 000 euros à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales au titre de l'article L. 761-1 du code de justice administrative et une somme de 2 000 euros à la SCP Leduc et Vigand au titre du même article, et de l'article 37 de la loi du 10 juillet 1991 sous réserve que celle-ci renonce à la part contributive de l'Etat.<br/>
Article 3 : La présente décision sera notifiée au centre hospitalier de Cannes, à M. D... C... et à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales.<br/>
Copie en sera adressée à la caisse primaire d'assurance maladie des Alpes-Maritimes et à la caisse primaire d'assurance maladie du Var.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-04-04-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. MODALITÉS DE LA RÉPARATION. ALLOCATION D'UNE PROVISION. - JUGE DU FOND ACCORDANT UNE PROVISION DANS L'ATTENTE DU RÉSULTAT D'UNE EXPERTISE SUR L'AMPLEUR DU PRÉJUDICE - 1) CONDITIONS - EXISTENCE D'UN LIEN DE CAUSALITÉ ENTRE LA FAUTE DE L'ADMINISTRATION ET LE PRÉJUDICE SUBI - PROVISION INFÉRIEURE AU MONTANT TOTAL DE L'INDEMNITÉ TEL QU'IL PEUT ÊTRE ANTICIPÉ - 2) APPLICATION - FAUTES D'UN CENTRE HOSPITALIER ÉTANT AU MOINS PARTIELLEMENT À L'ORIGINE DE PLUSIEURS DOMMAGES MÉDICAUX - OBLIGATION, POUR LE JUGE, DE PRÉCISER LES LIENS ENTRE LES FAITS FAUTIFS ET CHACUN DES POSTES DE PRÉJUDICE INVOQUÉS - ABSENCE.
</SCT>
<ANA ID="9A"> 60-04-04-03 1) Le juge du fond peut accorder une provision au créancier qui l'a saisi d'une demande indemnitaire lorsqu'il constate qu'un agissement de l'administration a été à l'origine d'un préjudice et que, dans l'attente des résultats d'une expertise permettant de déterminer l'ampleur de celui-ci, il est en mesure de fixer un montant provisionnel dont il peut anticiper qu'il restera inférieur au montant total qui sera ultérieurement défini.,,,2) Cour administrative d'appel ayant accordé une provision à la victime d'une infection nosocomiale qui l'a saisie d'une demande indemnitaire, dans l'attente du résultat d'une expertise permettant de déterminer l'ampleur du préjudice.... ,,Il ressort des termes mêmes de l'arrêt attaqué que la cour a jugé que le centre hospitalier avait commis un manquement à son obligation d'information et pris deux décisions fautives dans le choix de la technique de pontage et que ces fautes avaient été, au moins partiellement, à l'origine d'une obstruction de l'artère provoquant l'ischémie aiguë des tissus et des muscles dévascularisés ainsi qu'une paralysie du nerf poplité, elles-mêmes à l'origine des soins successifs puis de l'amputation.... ,,En se prononçant ainsi sur l'existence d'un lien de causalité entre les agissements du service public hospitalier et les préjudices subis par la victime, la cour, qui n'était pas tenue, pour fixer le montant d'une indemnité provisionnelle, de préciser les liens entre les faits fautifs et chacun des postes du préjudice invoqué par la victime, a suffisamment motivé son arrêt.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
