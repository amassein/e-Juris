<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031281226</ID>
<ANCIEN_ID>JG_L_2015_09_000000393476</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/28/12/CETATEXT000031281226.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 16/09/2015, 393476, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-09-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393476</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2015:393476.20150916</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au préfet de la région Ile-de-France, préfet de Paris, de lui fournir un hébergement d'urgence, sous astreinte de 50 euros par jour de retard à compter de la notification de l'ordonnance. Par une ordonnance n° 1514281 du 27 août 2015, le juge des référés a rejeté sa demande. <br/>
<br/>
              Par une requête enregistrée le 11 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) de l'admettre à titre provisoire au bénéfice de l'aide juridictionnelle ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - la condition d'urgence est remplie dès lors qu'elle se trouve actuellement sans-abri ; <br/>
              - l'absence de proposition d'hébergement porte une atteinte grave et manifestement illégale à son droit à un hébergement d'urgence ;<br/>
              - le fait que plusieurs demandes d'hébergement n'aient pu être satisfaites ne permet pas de justifier de l'absence de carence des services de l'Etat. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de la construction et de l'habitation ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
	- le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ; qu'en vertu de l'article L. 522-3 du même code, le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée ; qu'à cet égard, il appartient au juge d'appel de prendre en considération les éléments recueillis par le juge du premier degré dans le cadre de la procédure écrite et orale qu'il a diligentée ;<br/>
<br/>
              2. Considérant qu'il appartient aux autorités de l'Etat de mettre en oeuvre le droit à l'hébergement d'urgence reconnu par la loi à toute personne sans abri qui se trouve en situation de détresse médicale, psychique et sociale ; que seule une carence caractérisée des autorités de l'Etat dans la mise en oeuvre du droit à l'hébergement d'urgence peut faire apparaître, pour l'application de l'article L. 521-2 du code de justice administrative, une atteinte manifestement illégale à une liberté fondamentale permettant au juge des référés de faire usage des pouvoirs qu'il tient de ce texte, en ordonnant à l'administration de faire droit à une demande d'hébergement d'urgence ; qu'il lui incombe d'apprécier, dans chaque cas, les diligences accomplies par l'administration, en tenant compte des moyens dont elle dispose, ainsi que l'âge, de l'état de santé et de la situation de famille de la personne intéressée ;<br/>
<br/>
              3. Considérant qu'il résulte des pièces du dossier que la requérante, reconnue prioritaire pour l'attribution d'un logement par la commission de médiation du département de Paris, se trouve sans abri dans l'attente d'une offre de logement dans le cadre du droit au logement opposable ; que le juge des référés du tribunal administratif de Paris a rejeté sa demande tendant à enjoindre au préfet de lui fournir un hébergement d'urgence, par ordonnance du 27 août 2015, dont Mme A...relève appel ;<br/>
<br/>
              4. Considérant que le juge des référés du tribunal administratif de Paris, après avoir rappelé les règles au regard desquelles il lui appartient d'exercer son office lorsqu'il est saisi d'une requête relative à un hébergement d'urgence, a constaté à bon droit que ni les circonstances particulières de l'espèce ni le comportement de l'administration, au regard des moyens dont elle dispose, ne font apparaître de méconnaissance grave et manifeste des obligations qu'impose la mise en oeuvre du droit à l'hébergement d'urgence ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que, sans qu'il y ait lieu de l'admettre à titre provisoire au bénéfice de l'aide juridictionnelle, la requête de MmeA..., y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, doit être rejetée selon la procédure prévue par l'article L. 522-3 du même code ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de Mme A...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme B...A....<br/>
Copie en sera adressée, pour information, au préfet de la région Ile-de-France. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
