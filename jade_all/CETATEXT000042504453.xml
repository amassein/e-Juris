<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042504453</ID>
<ANCIEN_ID>JG_L_2020_11_000000423804</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/50/44/CETATEXT000042504453.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 05/11/2020, 423804, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423804</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI, REBEYROL ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:423804.20201105</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au tribunal administratif de Marseille d'annuler pour excès de pouvoir la décision du 18 mars 2015 par laquelle le directeur du centre hospitalier Édouard Toulouse a refusé de renouveler son contrat de " praticien hospitalier attaché " à temps partiel à son échéance le 1er mai 2015, d'enjoindre au centre hospitalier Édouard Toulouse de la réintégrer dans ses droits sous astreinte de 10 000 euros par jour de retard dans le délai de deux mois suivant la notification du jugement, de condamner le centre hospitalier Édouard Toulouse à lui verser la somme de 4 000 euros en réparation du préjudice qu'elle estime avoir subi et d'ordonner la notification du jugement aux parties et l'envoi de la copie de ce jugement à l'ordre national des médecins, au conseil départemental de l'ordre des médecins des Bouches-du-Rhône, au préfet, à l'agence régionale de santé et au procureur de la République. Par un jugement n° 1507519 du 15 mai 2017, le tribunal administratif a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 17MA03018 du 3 juillet 2018, la cour administrative d'appel de Marseille a rejeté l'appel formé par Mme A... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat les 3 septembre et 15 novembre 2018 et le 17 mars 2020, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier Edouard Toulouse une somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code du travail ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 86-33 du 9 janvier 1986 ;<br/>
              - le décret n° 91-155 du 6 février 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, Rebeyrol, avocat de Mme A... et à la SCP Waquet, Farge, Hazan, avocat du centre hospitalier Edouard Toulouse.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme A..., médecin généraliste, a été recrutée par le centre hospitalier Edouard Toulouse en qualité de praticien hospitalier contractuel à temps partiel à compter du 1er août 2013 sous couvert de contrats d'une durée de trois mois, renouvelés à six reprises. Le directeur du centre hospitalier ayant, le 18 mars 2015, décidé de ne pas renouveler le contrat de Mme A... à son échéance du 1er mai 2015, l'intéressée a demandé l'annulation pour excès de pouvoir de cette décision, et a assorti ces conclusions d'une demande de réintégration sous astreinte et d'une demande de condamnation du centre hospitalier à lui verser la somme de 4 000 euros en réparation du préjudice subi du fait de son éviction du service. Par un jugement du 15 mai 2017, le tribunal administratif de Marseille a rejeté sa demande. Mme A... se pourvoit en cassation contre l'arrêt du 3 juillet 2018 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant à l'annulation de ce jugement ainsi qu'à la condamnation du centre hospitalier Edouard Toulouse à lui verser la somme de 4 700 euros au titre de l'indemnité de précarité non perçue.<br/>
<br/>
              Sur le pourvoi : <br/>
<br/>
              En ce qui concerne le rejet des conclusions d'excès de pouvoir :<br/>
<br/>
              2. Un agent public qui a été recruté par un contrat à durée déterminée ne bénéficie pas d'un droit au renouvellement de son contrat. Toutefois, l'administration ne peut légalement décider, au terme de son contrat, de ne pas le renouveler que pour un motif tiré de l'intérêt du service. Un tel motif s'apprécie au regard des besoins du service ou de considérations tenant à la personne de l'agent. Dès lors qu'elles sont de nature à caractériser un intérêt du service justifiant le non renouvellement du contrat, la circonstance que des considérations relatives à la personne de l'agent soient par ailleurs susceptibles de justifier une sanction disciplinaire ne fait pas obstacle, par elle-même, à ce qu'une décision de non renouvellement du contrat soit légalement prise, pourvu que l'intéressé ait alors été mis à même de faire valoir ses observations.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que la décision litigieuse a été prise pour un motif tiré de l'intérêt du service apprécié au regard notamment de considérations susceptibles de justifier une sanction disciplinaire, à savoir des manquements commis par l'intéressée à ses obligations professionnelles tels qu'ils avaient été mis en évidence par une enquête administrative diligentée à la suite de la plainte d'un patient qui lui reprochait un comportement contraire à la déontologie médicale, et pour lequel la juridiction ordinale, saisie par le centre hospitalier, l'a d'ailleurs condamnée à une peine de 3 mois de suspension dont 2 mois avec sursis par une décision de la chambre nationale de discipline de l'ordre des médecins du 14 novembre 2018, devenue définitive. Dès lors, il résulte de ce qui été dit ci-dessus que la cour administrative d'appel a commis une erreur de droit en écartant comme inopérant le moyen tiré de ce que Mme A... n'avait pas été mise à même de faire valoir ses observations avant l'intervention de la mesure contestée. Par suite, Mme A..., sans qu'il soit besoin de se prononcer sur les autres moyens de son pourvoi, est fondée à demander, pour ce motif, l'annulation de l'arrêt en tant qu'il se prononce sur ses conclusions tendant à l'annulation du refus de renouveler son contrat.<br/>
<br/>
              En ce qui concerne le rejet des conclusions indemnitaires présentées au titre de la faute commise par le centre hospitalier :<br/>
<br/>
              4. La cour a rejeté, par adoption des motifs du tribunal administratif, les conclusions indemnitaires de Mme A... tendant à la condamnation du centre hospitalier Edouard Toulouse à lui verser la somme de 4 000 euros en réparation des préjudices résultant de la faute commise par le centre hospitalier pour avoir illégalement refusé de renouveler son contrat, par voie de conséquence du rejet des conclusions d'excès de pouvoir dirigées contre ce refus. Mme A... est dès lors également fondée à demander, par voie de conséquence, l'annulation de l'arrêt sur ce point.<br/>
<br/>
              En ce qui concerne les conclusions tendant à la condamnation du centre hospitalier à verser l'indemnité de précarité :<br/>
<br/>
              5. Il ressort des termes mêmes de l'arrêt attaqué qu'il n'a pas statué sur les conclusions présentées dans le mémoire en réplique, qu'il a pourtant visées, tendant à la condamnation du centre hospitalier Édouard Toulouse à verser à la requérante la somme de 4 700 euros au titre d'une indemnité de précarité. Mme A... est par suite fondée à demander l'annulation de cet arrêt en tant qu'il n'a pas statué sur ces conclusions.<br/>
<br/>
              Sur le règlement au fond :<br/>
<br/>
              6. Il y a lieu, pour le Conseil d'Etat, de faire application de l'article L. 821-2 du code de justice administrative et de statuer directement sur les conclusions mentionnées au 5.<br/>
<br/>
              7. Les conclusions présentées pour la première fois par Mme A... dans le mémoire en réplique qu'elle a déposé devant la cour administrative d'appel tendent au versement d'une indemnité de précarité de 10 % qui est due en cas de non renouvellement d'un contrat à durée déterminée en application de l'article R. 6152-418 du code de la santé publique, lequel rend applicables aux praticiens contractuels les dispositions de l'article L. 1243-8 du code du travail. Ces conclusions se rattachent à une cause juridique distincte de celle soulevée devant les premiers juges à l'appui des conclusions indemnitaires, exclusivement fondée sur la faute commise par le centre hospitalier pour avoir refusé illégalement de renouveler le contrat de l'intéressée. Elles sont par suite nouvelles en cause d'appel et doivent dès lors être rejetées.<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de Mme A... qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier Edouard Toulouse la somme de 3 000 euros à verser, à ce titre, à Mme A... au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 3 juillet 2018 est annulé.<br/>
<br/>
Article 2 : Les conclusions de Mme A... tendant à la condamnation du centre hospitalier Édouard Toulouse à lui verser la somme de 4 700 euros au titre de l'indemnité de précarité sont rejetées.<br/>
<br/>
Article 3 : L'affaire est renvoyée devant la cour administrative d'appel de Marseille sauf en ce qui concerne les conclusions sur lesquelles il est statué à l'article 2.<br/>
<br/>
Article 4 : Le centre hospitalier Edouard Toulouse versera à Mme A... une somme de 3 000 euros au titre des frais exposés par elle et non compris dans les dépens.<br/>
<br/>
Article 5 : Les conclusions du centre hospitalier Edouard Toulouse présentées sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 6 : La présente décision sera notifiée à Mme B... A... et au centre hospitalier Edouard Toulouse.<br/>
		Copie en sera adressée au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
