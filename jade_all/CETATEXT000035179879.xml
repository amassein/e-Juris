<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035179879</ID>
<ANCIEN_ID>JG_L_2017_07_000000401718</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/17/98/CETATEXT000035179879.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 12/07/2017, 401718, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-07-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401718</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS</AVOCATS>
<RAPPORTEUR>Mme Florence Marguerite</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:401718.20170712</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B...a demandé au tribunal administratif de Montpellier de condamner la maison départementale des personnes handicapées de l'Hérault à lui verser la somme de 18 488 euros en réparation du préjudice qu'il estime avoir subi en raison d'un défaut d'information sur son droit au complément à l'allocation d'éducation de l'enfant handicapé. Par un jugement n° 1301882 du 3 juin 2014, le tribunal administratif de Montpellier a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14MA03391 du 8 février 2016, la cour administrative d'appel de Marseille, sur appel de M.B..., a annulé ce jugement et rejeté la demande présentée par M. B...devant le tribunal administratif de Montpellier.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 21 juillet et 24 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt de la cour administrative d'appel de Marseille ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la maison départementale des personnes handicapées de l'Hérault la somme de 3 000 euros à verser à son avocat, Me Haas, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - la loi n° 2005-102 du 11 février 2005 ;<br/>
              - le décret n° 2015-233 du 27 février 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Marguerite, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de M. A...B....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B...a bénéficié à compter du 1er décembre 2004 de l'allocation d'éducation spéciale, devenue l'allocation d'éducation de l'enfant handicapé, pour sa fille Hanan, dont le taux d'incapacité a été évalué à 80 %. La commission des droits et de l'autonomie des personnes handicapées de l'Hérault lui ayant reconnu le droit au bénéfice du complément d'allocation de deuxième catégorie par une décision du 30 juin 2010, prenant effet le 1er septembre 2010, M. B...a estimé que la maison départementale des personnes handicapées de l'Hérault avait commis une faute en ne l'informant pas dès 2004 de l'étendue de ses droits et en instruisant incomplètement sa demande, qui portait tant sur l'allocation que sur son complément. Il a, pour ce motif, formé une demande indemnitaire pour un montant correspondant aux sommes qui auraient dû, selon lui, lui être versées au titre du complément de deuxième catégorie du 1er décembre 2004 au 31 août 2010. Par un jugement du 3 juin 2014, le tribunal administratif de Montpellier a rejeté la demande de M. B...comme portée devant une juridiction incompétente pour en connaître. Par un arrêt du 8 février 2016, contre lequel M. B...se pourvoit en cassation, la cour administrative d'appel de Marseille a annulé ce jugement mais rejeté, après évocation, la demande de M. B...comme non fondée.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article 35 du décret du 27 février 2015 : " Lorsqu'une juridiction est saisie d'un litige qui présente à juger, soit sur l'action introduite, soit sur une exception, une question de compétence soulevant une difficulté sérieuse et mettant en jeu la séparation des ordres de juridiction, elle peut, par une décision motivée qui n'est susceptible d'aucun recours, renvoyer au Tribunal des conflits le soin de décider sur cette question de compétence ".<br/>
<br/>
              3. En premier lieu, aux termes de l'article L. 541-1 du code de la sécurité sociale : " Toute personne qui assume la charge d'un enfant handicapé a droit à une allocation d'éducation de l'enfant handicapé, si l'incapacité permanente de l'enfant est au moins égale à un taux déterminé. / Un complément d'allocation est accordé pour l'enfant atteint d'un handicap dont la nature ou la gravité exige des dépenses particulièrement coûteuses ou nécessite le recours fréquent à l'aide d'une tierce personne. Son montant varie suivant l'importance des dépenses supplémentaires engagées ou la permanence de l'aide nécessaire (...) ". L'article L. 541-2 du même code précise que : " L'allocation et son complément éventuel sont attribués au vu de la décision de la commission mentionnée à l'article L. 146-9 du code de l'action sociale et des familles appréciant si l'état de l'enfant ou de l'adolescent justifie cette attribution (...) ". Aux termes du I de l'article L. 241-6 du code de l'action sociale et des familles : " La commission des droits et de l'autonomie des personnes handicapées est compétente pour : (...) / 3° Apprécier : / a) Si l'état ou le taux d'incapacité de la personne handicapée justifie l'attribution, pour l'enfant ou l'adolescent, de l'allocation et, éventuellement, de son complément mentionnés à l'article L. 541-1 du code de la sécurité sociale (...) ". Enfin, l'article L. 241-9 du même code prévoit que les décisions relevant du 3° de l'article L. 241-6 peuvent faire l'objet de recours devant la juridiction du contentieux technique de la sécurité sociale.<br/>
<br/>
              4. En deuxième lieu, il résulte de l'article L. 511-1 du code de la sécurité sociale que l'allocation d'éducation de l'enfant handicapé est une prestation familiale, dont le contentieux relève par suite, en vertu de l'article L. 142-1 du même code et sous réserve de la compétence de la juridiction du contentieux technique de la sécurité sociale, du contentieux général de la sécurité sociale. <br/>
<br/>
              5. En dernier lieu, la loi du 11 février 2005 pour l'égalité des droits et des chances, la participation et la citoyenneté des personnes handicapées a créé les maisons départementales des personnes handicapées, notamment pour leur offrir un accès unique aux droits et prestations dont elles peuvent bénéficier. Aux termes de l'article R. 541-3 du code de la sécurité sociale : " La demande d'allocation d'éducation de l'enfant handicapé, de son complément et de la majoration mentionnés aux articles L. 541-1 et L. 541-4, est adressée à la maison départementale des personnes handicapées du lieu de résidence de l'intéressé. / Cette demande est accompagnée de toutes les pièces justificatives utiles à l'appréciation des droits de l'intéressé (...) La maison départementale des personnes handicapées transmet, sans délai, un exemplaire du dossier de demande à l'organisme débiteur en vue de l'examen des conditions relevant de la compétence de celui-ci ". L'article L. 146-3 du code de l'action sociale et des familles charge la maison départementale des personnes handicapées de l'évaluation des demandes et de l'attribution de diverses prestations, dont l'allocation d'éducation de l'enfant handicapé et de son complément. L'article R. 241-31 de ce code précise que les décisions de la commission des droits et de l'autonomie des personnes handicapées sont prises au nom de la maison départementale des personnes handicapées. Enfin, aux termes de l'article L. 146-4 du même code : " La maison départementale des personnes handicapées est un groupement d'intérêt public constitué pour une durée indéterminée, dont le département assure la tutelle administrative et financière. / Le département, l'Etat et les organismes locaux d'assurance maladie et d'allocations familiales du régime général de sécurité sociale définis aux articles L. 211-1 et L. 212-1 du code de la sécurité sociale sont membres de droit de ce groupement. [...] ".<br/>
<br/>
              6. Les litiges relatifs à la responsabilité qui peut incomber à l'Etat ou aux autres personnes morales de droit public en raison des dommages imputés à leurs services publics administratifs relèvent en principe de la compétence de la juridiction administrative. Toutefois, il résulte de la combinaison des dispositions mentionnées aux points 3 et 4 que les contestations relatives à l'attribution de l'allocation d'éducation de l'enfant handicapé et de son complément relèvent de la compétence des juridictions du contentieux technique et du contentieux général de la sécurité sociale, qui sont des juridictions de l'ordre judiciaire. <br/>
<br/>
              7. Par suite, le litige né de l'action de M.B..., qui poursuit la responsabilité de la maison départementale des personnes handicapées de l'Hérault pour une faute commise dans le cadre de l'instruction de sa demande d'allocation d'éducation de l'enfant handicapé et de son complément, présente à juger une question de compétence soulevant une difficulté sérieuse et de nature à justifier le recours à la procédure prévue par l'article 35 du décret du 27 février 2015. Dès lors, il y a lieu de renvoyer au Tribunal des conflits la question de savoir si l'action introduite par M. B...relève ou non de la compétence de la juridiction administrative et de surseoir à toute procédure jusqu'à la décision de ce tribunal.<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'affaire est renvoyée au Tribunal des conflits.<br/>
Article 2 : Il est sursis à statuer sur le pourvoi de M. B...jusqu'à ce que le Tribunal des conflits ait tranché la question de savoir si le litige né de son action poursuivant la responsabilité de la maison départementale des personnes handicapées de l'Hérault pour une faute commise dans le cadre du versement de l'allocation d'éducation de l'enfant handicapé et de son complément relève ou non de la compétence de la juridiction administrative.<br/>
Article 3 : La présente décision sera notifiée à M. A...B...et à la maison départementale des personnes handicapées de l'Hérault.<br/>
Copie en sera adressée à la ministre des solidarités et de la santé.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
