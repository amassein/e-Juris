<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036927163</ID>
<ANCIEN_ID>JG_L_2018_05_000000414583</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/92/71/CETATEXT000036927163.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Assemblée, 18/05/2018, 414583, Publié au recueil Lebon</TITRE>
<DATE_DEC>2018-05-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414583</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Assemblée</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>M. Pierre Ramain</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEASS:2018:414583.20180518</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés le 25 septembre 2017 et le 18 décembre 2017, la Fédération des finances et affaires économiques de la CFDT (CFDT Finances) demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite du Premier ministre et la décision du ministre de l'action et des comptes publics en date du 31 août 2017 rejetant la demande qu'elle a présentée le 25 mai 2017 tendant à l'abrogation du décret n° 2017-436 du 29 mars 2017 fixant la liste des emplois et des types d'emplois dérogatoires à l'emploi permanent des établissements publics administratifs en tant qu'il fixe ces emplois en ce qui concerne l'Institut national de la propriété industrielle (INPI) ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              3°) d'enjoindre au Premier ministre d'abroger, dans la mesure de sa demande, le décret du 29 mars 2017. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la propriété intellectuelle ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ; <br/>
              - la loi n° 84-16 du 11 janvier 1984 ; <br/>
              - le décret n° 88-507 du 29 avril 1988 ; <br/>
              - le décret n° 2012-225 du 16 février 2012<br/>
              - le décret n° 2012-984 du 22 août 2012 ; <br/>
              - le décret n° 2017-41 du 17 janvier 2017 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Ramain, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de la fédération des finances et affaires économiques de la CFDT ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. La Fédération des finances et affaires économiques de la CFDT a saisi le Premier ministre d'une demande tendant à l'abrogation du décret du 29 mars 2017 fixant la liste des emplois et types d'emplois des établissements publics administratifs de l'Etat prévue au 2° de l'article 3 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat en tant qu'il détermine la liste des emplois pour lesquels l'Institut national de la propriété industrielle (INPI) peut recruter des agents contractuels par dérogation à la règle selon laquelle les emplois permanents des établissements publics administratifs de l'Etat sont occupés par des fonctionnaires. Elle demande l'annulation pour excès de pouvoir du refus opposé à cette demande. <br/>
<br/>
              2. Le contrôle exercé par le juge administratif sur un acte qui présente un caractère réglementaire porte sur la compétence de son auteur, les conditions de forme et de procédure dans lesquelles il a été édicté, l'existence d'un détournement de pouvoir et la légalité des règles générales et impersonnelles qu'il énonce, lesquelles ont vocation à s'appliquer de façon permanente à toutes les situations entrant dans son champ d'application tant qu'il n'a pas été décidé de les modifier ou de les abroger.<br/>
<br/>
              3. Le juge administratif exerce un tel contrôle lorsqu'il est saisi, par la voie de l'action, dans le délai de recours contentieux. En outre, en raison de la permanence de l'acte réglementaire, la légalité des règles qu'il fixe, comme la compétence de son auteur et l'existence d'un détournement de pouvoir doivent pouvoir être mises en cause à tout moment, de telle sorte que puissent toujours être sanctionnées les atteintes illégales que cet acte est susceptible de porter à l'ordre juridique.<br/>
<br/>
              4. Après l'expiration du délai de recours contentieux, une telle contestation peut être formée par voie d'exception à l'appui de conclusions dirigées contre une décision administrative ultérieure prise pour l'application de l'acte réglementaire ou dont ce dernier constitue la base légale. Elle peut aussi prendre la forme d'un recours pour excès de pouvoir dirigé contre la décision refusant d'abroger l'acte réglementaire, comme l'exprime l'article L. 243-2 du code des relations entre le public et l'administration aux termes duquel : " L'administration est tenue d'abroger expressément un acte réglementaire illégal ou dépourvu d'objet, que cette situation existe depuis son édiction ou qu'elle résulte de circonstances de droit ou de fait postérieures, sauf à ce que l'illégalité ait cessé [...] ". Si, dans le cadre de ces deux contestations, la légalité des règles fixées par l'acte réglementaire, la compétence de son auteur et l'existence d'un détournement de pouvoir peuvent être utilement critiquées, il n'en va pas de même des conditions d'édiction de cet acte, les vices de forme et de procédure dont il serait entaché ne pouvant être utilement invoqués que dans le cadre du recours pour excès de pouvoir dirigé contre l'acte réglementaire lui-même et introduit avant l'expiration du délai de recours contentieux.<br/>
<br/>
              5. Il résulte de ce qui précède que la fédération requérante ne peut utilement invoquer, à l'appui de ses conclusions tendant à l'annulation pour excès de pouvoir du refus d'abroger le décret du 29 mars 2017, les moyens tirés respectivement de l'irrégularité de la consultation du conseil supérieur de la fonction publique de l'Etat et de ce que ce décret différerait à la fois du projet qui avait été soumis par le Gouvernement au Conseil d'Etat et de celui adopté par ce dernier. <br/>
<br/>
              6. Aux termes de l'article 3 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires :  " Sauf dérogation prévue par une disposition législative, les emplois civils permanents de l'Etat, des régions, des départements, des communes et de leurs établissements publics à caractère administratif sont, à l'exception de ceux réservés aux magistrats de l'ordre judiciaire et aux fonctionnaires des assemblées parlementaires, occupés soit par des fonctionnaires régis par le présent titre, soit par des fonctionnaires des assemblées parlementaires, des magistrats de l'ordre judiciaire ou des militaires dans les conditions prévues par leur statut ". L'article 3 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat dispose, dans sa rédaction issue de la loi du 20 avril 2016 relative à la déontologie et aux droits et obligations des fonctionnaires, que : " les emplois permanents de l'Etat et des établissements public de l'Etat énumérés ci-après ne sont pas soumis à la règle énoncée à l'article 3 du titre Ier du statut général (...) 2) Les emplois des établissements publics qui requièrent des qualifications professionnelles particulières indispensables à l'exercice de leur missions spécifiques et non dévolues à des corps de fonctionnaires, inscrits pour une durée déterminée sur une liste établie par décret en Conseil d'Etat (...) Les agents occupant ces emplois sont recrutés par contrat à durée indéterminée (...) ". L'article 4 de la loi du 11 janvier 1984 dispose que " par dérogation au principe énoncé à l'article 3 du titre Ier du statut général, des agents contractuels peuvent être recrutés dans les cas suivants : / 1° Lorsqu'il n'existe pas de corps de fonctionnaires susceptibles d'assurer les fonctions correspondantes (...) ". Il résulte des dispositions précitées de l'article 3 de la loi du 11 janvier 1984 que la possibilité pour un établissement public administratif de l'Etat de pourvoir, sur leur fondement, à des emplois permanents en recourant à des agents contractuels recrutés par contrat à durée indéterminée, par dérogation à la règle selon lequelle ces emplois sont occupés par des fonctionnaires, est subordonnée à l'absence de corps de fonctionnaires possédant les qualifications professionnelles particulières requises pour occuper ces emplois afin d'exercer les missions spécifiques de cet établissement public.<br/>
<br/>
              7. Le décret du 29 mars 2017 fixe la liste des établissements publics administratifs et les types d'emploi concernés par la dérogation prévue à l'article 3 de la loi du 11 janvier 1984. Il prévoit que l'INPI bénéficie de cette dérogation pour huit types d'emplois. <br/>
<br/>
              8. Aux termes de l'article L. 411-1 du code de la propriété intellectuelle, l'Institut national de la propriété industrielle " a pour mission : / 1° De centraliser et diffuser toute information nécessaire pour la protection des innovations et pour l'enregistrement des entreprises, ainsi que d'engager toute action de sensibilisation et de formation dans ces domaines ; / 2° D'appliquer les lois et règlements en matière de propriété industrielle et de registre du commerce et des sociétés ; à cet effet, l'Institut pourvoit, notamment, à la réception des dépôts de demandes des titres de propriété industrielle (...), à leur examen et à leur délivrance ou enregistrement et à la surveillance de leur maintien ; il centralise le registre du commerce et des sociétés (...) ; il assure la diffusion des informations techniques, commerciales et financières contenues dans les titres de propriété industrielle ; il assure la diffusion et la mise à la disposition gratuite du public, à des fins de réutilisation, des informations techniques, commerciales et financières qui sont contenues dans le registre national du commerce et des sociétés et dans les instruments centralisés de publicité légale (...) / 3° De prendre toute initiative en vue d'une adaptation permanente du droit national et international aux besoins des innovateurs et des entreprises (...) ".<br/>
<br/>
              9. Il ressort des pièces du dossier que les spécificités des missions confiées à l'INPI requièrent, eu égard aux compétences techniques et juridiques dont elles supposent la maîtrise, des qualifications professionnelles particulières dans le domaine de la propriété industrielle. Il ressort également des pièces du dossier que l'ensemble des huit types d'emplois retenus par le décret du 29 mars 2017 requièrent une expertise dans le domaine de la propriété industrielle et, en particulier, dans le maniement des titres et des données ainsi que du registre national du commerce et des sociétés. <br/>
<br/>
              10. Contrairement à ce qui est soutenu, ni le corps des ingénieurs de l'industrie et des mines qui, en vertu du décret du 29 avril 1988 portant création et statut particulier de ce corps ont " vocation à servir en position d'activité (...) dans les établissement publics de l'Etat " et " sont chargés de fonctions de direction d'encadrement, d'expertise, d'étude, d'administration, de recherche ou d'enseignement dans les domaines scientifique, technique, environnemental, économique ou social " ni le corps des techniciens supérieurs de l'économie et de l'industrie qui, en vertu du décret du 22 août 2012 portant statut particulier de ce corps, " effectuent des études, des enquêtes, des expertises et des contrôles dans les domaines de la sécurité, de la protection de l'environnement, de l'exploitation des ressources minières, de la métrologie et de l'économie " ne donnent à leurs membres vocation à détenir, eu égard à la spécificité des missions de l'INPI, les qualifications professionnelles particulières requises pour occuper les huit types d'emplois mentionnés dans le décret du 29 mars 2017 pour lesquels l'INPI peut recruter des agents contractuels sur le fondement de l'article 3 de la loi du 11 janvier 1984. <br/>
<br/>
              11. Il ne ressort pas des pièces du dossier que d'autres corps de fonctionnaires donneraient à leurs membres vocation à détenir les qualifications professionnelles particulières requises, compte tenu de la spécificité des missions de l'INPI, pour occuper les huit types d'emplois mentionnés dans le décret du 29 mars 2017. Il s'ensuit que le décret litigieux a pu légalement ranger ces types d'emplois au nombre de ceux pour lesquels il peut être dérogé, sur le fondement de l'article 3 de la loi du 11 janvier 1984, à la règle selon laquelle les emplois permanents des établissements publics administratifs sont occupés par des fonctionnaires. <br/>
<br/>
              12. Il résulte de tout ce qui précède que la Fédération des finances et affaires économiques de la CFDT n'est pas fondée à demander l'annulation pour excès de pouvoir du refus d'abroger le décret du 29 mars 2017 en tant qu'il a ouvert à l'INPI la faculté, pour huit types d'emplois, de déroger à la règle selon laquelle les emplois permanents des établissements publics administratifs sont occupés par des fonctionnaires. Ses conclusions à fin d'injonction ainsi que celles présentées au titre de l'article L. 761-1 du code de justice administrative doivent, par voie de conséquence, être rejetées. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la Fédération des finances et affaires économiques de la CFDT est rejetée. <br/>
Article 2 : La présente décision sera notifiée à la Fédération des finances et affaires économiques de la CFDT, au Premier ministre et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-09-02-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DISPARITION DE L'ACTE. ABROGATION. ABROGATION DES ACTES RÉGLEMENTAIRES. - RECOURS CONTRE LE REFUS D'ABROGER UN ACTE RÉGLEMENTAIRE [RJ1] - MOYENS OPÉRANTS - ILLÉGALITÉ DES RÈGLES FIXÉES PAR L'ACTE RÉGLEMENTAIRE, COMPÉTENCE DE SON AUTEUR ET DÉTOURNEMENT DE POUVOIR - EXISTENCE - VICES DE FORME ET DE PROCÉDURE - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-04 DROITS CIVILS ET INDIVIDUELS. DROIT DE PROPRIÉTÉ. - PROPRIÉTÉ INDUSTRIELLE - RECOURS À DES AGENTS CONTRACTUELS AU SEIN DE L'INPI SUR DES EMPLOIS PERMANENTS EN PRINCIPE OCCUPÉS PAR DES FONCTIONNAIRES - 1) CONDITION - ABSENCE DE CORPS DE FONCTIONNAIRES POSSÉDANT LES QUALIFICATIONS PROFESSIONNELLES PARTICULIÈRES REQUISES POUR OCCUPER CES EMPLOIS AFIN D'EXERCER LES MISSIONS SPÉCIFIQUES DE L'INPI - 2) DÉCRET DU 29 MARS 2017 RECENSANT LES EMPLOIS DE L'INPI POUVANT ÊTRE POURVUS PAR DES CONTRACTUELS - CONDITION REMPLIE [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">36-12 FONCTIONNAIRES ET AGENTS PUBLICS. AGENTS CONTRACTUELS ET TEMPORAIRES. - RECOURS À DES AGENTS CONTRACTUELS AU SEIN D'UN ÉTABLISSEMENT PUBLIC ADMINISTRATIF DE L'ETAT SUR DES EMPLOIS PERMANENTS EN PRINCIPE OCCUPÉS PAR DES FONCTIONNAIRES - 1) CONDITION - ABSENCE DE CORPS DE FONCTIONNAIRES POSSÉDANT LES QUALIFICATIONS PROFESSIONNELLES PARTICULIÈRES REQUISES POUR OCCUPER CES EMPLOIS AFIN D'EXERCER LES MISSIONS SPÉCIFIQUES DE CET ÉTABLISSEMENT PUBLIC - 2) DÉCRET DU 29 MARS 2017 RECENSANT LES EMPLOIS DE L'INPI POUVANT ÊTRE POURVUS PAR DES CONTRACTUELS - CONDITION REMPLIE [RJ2].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-07-01-04-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS INOPÉRANTS. - CONTESTATION D'UN ACTE RÉGLEMENTAIRE - MOYENS OPÉRANTS - 1) AVANT L'EXPIRATION DU RECOURS CONTENTIEUX, DANS LE CADRE D'UN RECOURS PAR VOIE D'ACTION - 2) APRÈS L'EXPIRATION DU RECOURS CONTENTIEUX, À L'APPUI DE LA CONTESTATION D'UN REFUS D'ABROGER [RJ1] OU D'UNE EXCEPTION D'ILLÉGALITÉ.
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">54-07-01-04-04 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. EXCEPTION D'ILLÉGALITÉ. - EXCEPTION D'ILLÉGALITÉ DIRIGÉE CONTRE UN ACTE RÉGLEMENTAIRE - MOYENS OPÉRANTS - ILLÉGALITÉ DES RÈGLES FIXÉES PAR L'ACTE RÉGLEMENTAIRE, COMPÉTENCE DE SON AUTEUR ET DÉTOURNEMENT DE POUVOIR - EXISTENCE - VICES DE FORME ET DE PROCÉDURE - ABSENCE.
</SCT>
<ANA ID="9A"> 01-09-02-01 Si, dans le cadre d'un recours pour excès de pouvoir dirigé contre la décision refusant d'abroger un acte réglementaire, la légalité des règles fixées par celui-ci, la compétence de son auteur et l'existence d'un détournement de pouvoir peuvent être utilement critiquées, il n'en va pas de même des conditions d'édiction de cet acte, les vices de forme et de procédure dont il serait entaché ne pouvant être utilement invoqués que dans le cadre du recours pour excès de pouvoir dirigé contre l'acte réglementaire lui-même et introduit avant l'expiration du délai de recours contentieux.</ANA>
<ANA ID="9B"> 26-04 1) Il résulte de l'article 3 de la loi n° 84-16 du 11 janvier 1984, dans sa rédaction issue de la loi n° 2016-483 du 20 avril 2016, que la possibilité pour un établissement public administratif de l'Etat de pourvoir, sur son fondement, à des emplois permanents en recourant à des agents contractuels recrutés par contrat à durée indéterminée, par dérogation à la règle selon laquelle ces emplois sont occupés par des fonctionnaires, est subordonnée à l'absence de corps de fonctionnaires possédant les qualifications professionnelles particulières requises pour occuper ces emplois afin d'exercer les missions spécifiques de cet établissement public.... ,,2) Les spécificités des missions confiées à l'Institut national de la propriété industrielle (INPI) requièrent, eu égard aux compétences techniques et juridiques dont elles supposent la maîtrise, des qualifications professionnelles particulières dans le domaine de la propriété industrielle. L'ensemble des huit types d'emplois retenus par le décret n° 2017-436 du 29 mars 2017 requièrent une expertise dans le domaine de la propriété industrielle et, en particulier, dans le maniement des titres et des données ainsi que du registre national du commerce et des sociétés. Ni le corps des ingénieurs de l'industrie et des mines, ni le corps des techniciens supérieurs de l'économie et de l'industrie ni d'autres corps de fonctionnaires ne donnent à leurs membres vocation à détenir, eu égard à la spécificité des missions de l'INPI, les qualifications professionnelles particulières requises pour occuper les huit types d'emplois mentionnés dans le décret du 29 mars 2017 pour lesquels l'INPI peut recruter des agents contractuels sur le fondement de l'article 3 de la loi du 11 janvier 1984.</ANA>
<ANA ID="9C"> 36-12 1) Il résulte de l'article 3 de la loi n° 84-16 du 11 janvier 1984, dans sa rédaction issue de la loi n° 2016-483 du 20 avril 2016, que la possibilité pour un établissement public administratif de l'Etat de pourvoir, sur son fondement, à des emplois permanents en recourant à des agents contractuels recrutés par contrat à durée indéterminée, par dérogation à la règle selon laquelle ces emplois sont occupés par des fonctionnaires, est subordonnée à l'absence de corps de fonctionnaires possédant les qualifications professionnelles particulières requises pour occuper ces emplois afin d'exercer les missions spécifiques de cet établissement public.... ,,2) Les spécificités des missions confiées à l'Institut national de la propriété intellectuelle (INPI) requièrent, eu égard aux compétences techniques et juridiques dont elles supposent la maîtrise, des qualifications professionnelles particulières dans le domaine de la propriété industrielle. L'ensemble des huit types d'emplois retenus par le décret n° 2017-436 du 29 mars 2017 requièrent une expertise dans le domaine de la propriété industrielle et, en particulier, dans le maniement des titres et des données ainsi que du registre national du commerce et des sociétés. Ni le corps des ingénieurs de l'industrie et des mines, ni le corps des techniciens supérieurs de l'économie et de l'industrie ni d'autres corps de fonctionnaires ne donnent à leurs membres vocation à détenir, eu égard à la spécificité des missions de l'INPI, les qualifications professionnelles particulières requises pour occuper les huit types d'emplois mentionnés dans le décret du 29 mars 2017 pour lesquels l'INPI peut recruter des agents contractuels sur le fondement de l'article 3 de la loi du 11 janvier 1984.</ANA>
<ANA ID="9D"> 54-07-01-04-03 Le contrôle exercé par le juge administratif sur un acte qui présente un caractère réglementaire porte sur la compétence de son auteur, les conditions de forme et de procédure dans lesquelles il a été édicté, l'existence d'un détournement de pouvoir et la légalité des règles générales et impersonnelles qu'il énonce, lesquelles ont vocation à s'appliquer de façon permanente à toutes les situations entrant dans son champ d'application tant qu'il n'a pas été décidé de les modifier ou de les abroger.,,,1) Le juge administratif exerce un tel contrôle lorsqu'il est saisi, par la voie de l'action, dans le délai de recours contentieux. En outre, en raison de la permanence de l'acte réglementaire, la légalité des règles qu'il fixe, comme la compétence de son auteur et l'existence d'un détournement de pouvoir doivent pouvoir être mises en cause à tout moment, de telle sorte que puissent toujours être sanctionnées les atteintes illégales que cet acte est susceptible de porter à l'ordre juridique.,,,2) Après l'expiration du délai de recours contentieux, une telle contestation peut être formée par voie d'exception à l'appui de conclusions dirigées contre une décision administrative ultérieure prise pour l'application de l'acte réglementaire ou dont ce dernier constitue la base légale. Elle peut aussi prendre la forme d'un recours pour excès de pouvoir dirigé contre la décision refusant d'abroger l'acte réglementaire, comme l'exprime l'article L. 243-2 du code des relations entre le public et l'administration (CRPA). Si, dans le cadre de ces deux contestations, la légalité des règles fixées par l'acte réglementaire, la compétence de son auteur et l'existence d'un détournement de pouvoir peuvent être utilement critiquées, il n'en va pas de même des conditions d'édiction de cet acte, les vices de forme et de procédure dont il serait entaché ne pouvant être utilement invoqués que dans le cadre du recours pour excès de pouvoir dirigé contre l'acte réglementaire lui-même et introduit avant l'expiration du délai de recours contentieux.</ANA>
<ANA ID="9E"> 54-07-01-04-04 Si, dans le cadre de la contestation d'un acte réglementaire par voie d'exception à l'appui de conclusions dirigées contre une décision administrative ultérieure prise pour son application ou dont il constitue la base légale, la légalité des règles fixées par cet acte réglementaire, la compétence de son auteur et l'existence d'un détournement de pouvoir peuvent être utilement critiquées, il n'en va pas de même des conditions d'édiction de cet acte, les vices de forme et de procédure dont il serait entaché ne pouvant être utilement invoqués que dans le cadre du recours pour excès de pouvoir dirigé contre l'acte réglementaire lui-même et introduit avant l'expiration du délai de recours contentieux.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., en précisant, CE, Assemblée, 3 février 1989, Compagnie Alitalia, n° 74052, p. 44 ; CE, 10 octobre 2013, Fédération française de gymnastique, n° 359219, p. 251.,,[RJ2] Cf. décision du même jour, Syndicat CGT de l'administration centrale et des services des ministères économiques et financiers et du Premier ministre, n° 411045, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
