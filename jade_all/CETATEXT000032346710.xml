<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032346710</ID>
<ANCIEN_ID>JG_L_2016_03_000000395119</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/34/67/CETATEXT000032346710.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème SSR, 30/03/2016, 395119, Publié au recueil Lebon</TITRE>
<DATE_DEC>2016-03-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395119</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème SSR</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Pauline Jolivet</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:XX:2016:395119.20160330</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement du 30 novembre 2015, enregistré le 8 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, le tribunal administratif de Montreuil, avant de statuer sur la demande de M. B...A...tendant à l'annulation de la décision du 4 novembre 2014 par laquelle le procureur de la République près le tribunal de grande instance de Bobigny a refusé l'effacement de la mention le concernant dans le système de traitement des infractions constatées, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen les questions suivantes : <br/>
<br/>
              1°) L'autorité judiciaire, saisie d'une demande d'effacement de données relatives à une condamnation pénale et dont l'inscription est conforme aux dispositions sus-rappelées, est-elle en situation de compétence liée pour refuser cette demande ou peut-elle, au contraire, apprécier le bien-fondé du maintien dans le temps de ces données dans le fichier, en mettant par exemple en balance, d'une part, l'intérêt général poursuivi par le maintien de ces données dans le fichier de traitement des antécédents judiciaires et, d'autre part, tant les caractéristiques particulières des affaires en cause et du parcours du demandeur, comme, notamment, la gravité des faits reprochés et le quantum de la condamnation prononcée, que le nombre de condamnations éventuellement intervenues, leur ancienneté, l'âge de l'auteur de l'infraction lorsqu'elle a été commise, ou encore l'absence de récidive et les efforts de réinsertion de la personne condamnée ' Les stipulations de l'article 8 de la convention européenne des droits de l'homme peuvent-elles être prises en compte par l'autorité judiciaire dans cette analyse '<br/>
<br/>
              2°) En l'absence de condamnation pénale, la décision de maintien dans le fichier de traitement des antécédents judiciaires implique-t-elle un examen des intérêts respectifs en présence ' Les stipulations de l'article 8 de la convention européenne des droits de l'homme peuvent-elles être prises en compte dans cette analyse '<br/>
<br/>
              3°) Quel est le degré de contrôle du juge administratif dans la mise en oeuvre de ces critères '<br/>
<br/>
<br/>
              Des observations, enregistrées le 25 janvier 2016, ont été présentées par le garde des sceaux, ministre de la justice. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu : <br/>
              - la Convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code de procédure pénale ;<br/>
              - l'arrêt de la Cour européenne des droits de l'homme Brunet c/ France (n° 21010/10) ;<br/>
              - le code de justice administrative, notamment son article L. 113-1 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Jolivet, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
           REND L'AVIS SUIVANT<br/>
<br/>
<br/>
<br/>
<br/>1. Aux termes de l'article 230-6 du code de procédure pénale qui autorise sa mise en oeuvre, le traitement des antécédents judiciaires a pour finalité de " faciliter la constatation des infractions à la loi pénale, le rassemblement de preuves de ces infractions et la recherche de leurs auteurs ". Il a également pour finalité, en application des articles L. 234-1 à L. 234-4 du code de la sécurité intérieure, " dans la stricte mesure exigée par la protection de la sécurité des personnes et la défense des intérêts fondamentaux de la Nation ", de contribuer à mettre en oeuvre des mesures de protection ou recueillir des renseignements pour la prise de décisions administratives relatives à des emplois ou activités mentionnés à l'article L. 114-1 du même code, par l'intermédiaire de consultations autorisées, ainsi que, en application de l'article 17-1 de la loi du 21 janvier 1995, de faciliter l'instruction des demandes en matière de nationalité, de titres de séjour et ordres nationaux.<br/>
<br/>
              2. Aux termes de l'article 230-7 du code de procédure pénale qui détermine les données qui peuvent être collectées par ce traitement, il peut contenir des informations sur les personnes à l'encontre desquelles il existe des indices graves ou concordants rendant vraisemblable qu'elles aient pu participer, comme auteurs ou complices, à la commission de certaines infractions mentionnées au 1° de l'article 230-6, sur les victimes de ces infractions et sur les personnes faisant l'objet d'une enquête ou d'une instruction pour recherche des causes de la mort. Aux termes du premier alinéa de l'article 230-8 du code de procédure pénale : " Le traitement des données à caractère personnel est opéré sous le contrôle du procureur de la République territorialement compétent qui demande qu'elles soient effacées, complétées ou rectifiées, notamment en cas de requalification judiciaire. La rectification pour requalification judiciaire est de droit. Le procureur de la République se prononce sur les suites qu'il convient de donner aux demandes d'effacement ou de rectification dans un délai d'un mois. En cas de décision de relaxe ou d'acquittement devenue définitive, les données personnelles concernant les personnes mises en cause sont effacées, sauf si le procureur de la République en prescrit le maintien pour des raisons liées à la finalité du fichier, auquel cas elle fait l'objet d'une mention. Lorsque le procureur de la République prescrit le maintien des données personnelles relatives à une personne ayant bénéficié d'une décision d'acquittement ou de relaxe devenue définitive, il en avise la personne concernée. Les décisions de non-lieu et, lorsqu'elles sont motivées par une insuffisance de charges, de classement sans suite font l'objet d'une mention, sauf si le procureur de la République ordonne l'effacement des données personnelles. Les autres décisions de classement sans suite font l'objet d'une mention. Lorsqu'une décision fait l'objet d'une mention, les données relatives à la personne concernée ne peuvent faire l'objet d'une consultation dans le cadre des enquêtes administratives prévues aux articles L. 114-1, L. 234-1 à L. 234-3 du code de la sécurité intérieure et à l'article 17-1 de la loi n° 95-73 du 21 janvier 1995 d'orientation et de programmation relative à la sécurité. ". Le décret n° 2012-652 du 4 mai 2012, pris sur le fondement de ces dispositions, encadre la mise en oeuvre du traitement des antécédents judiciaires. L'article R. 40-27 du code de procédure pénale fixe la durée de conservation des données concernant les personnes mises en cause dans le cadre des procédures établies par les services chargés des opérations de police judiciaire. Cette durée est modulée en fonction de l'âge de la personne mise en cause, de la gravité des infractions et de l'inscription de nouveaux faits dans le fichier. <br/>
<br/>
              3. Il résulte de ces dispositions que le législateur a entendu décrire entièrement les possibilités de radiation, correction ou maintien de données dans le fichier " traitement des antécédents judiciaires ", offertes à l'autorité à laquelle il a confié la responsabilité de contrôler sa mise en oeuvre.<br/>
<br/>
              4. Il en découle, en premier lieu, que saisis d'une demande d'effacement de données qui ne sont pas au nombre de celles que l'article 230-7 du code de procédure pénale autorise à collecter dans le traitement des antécédents judiciaires, le procureur de la République ou le magistrat référent mentionné à l'article 230-9 du même code, désignés par la loi pour contrôler le fichier, sont tenus d'en ordonner l'effacement. <br/>
<br/>
              5. En deuxième lieu, les dispositions de l'article 230-8 du code de procédure pénale citées au point 2 ne prévoyant de règles particulières relatives au maintien ou à l'effacement des données du traitement des antécédents judiciaires qu'en cas de décisions de relaxe, d'acquittement, de non-lieu ou de classement sans suite, le législateur doit être regardé comme n'ayant entendu ouvrir la possibilité d'effacement que dans les cas où les poursuites pénales sont, pour quelque motif que ce soit, demeurées sans suite. Hors cette hypothèse, les données ne peuvent être effacées qu'à l'issue de la durée de conservation fixée par voie réglementaire et le procureur de la République ne peut alors que refuser une demande d'effacement avant ce terme.<br/>
<br/>
              6. En troisième lieu, si la procédure a abouti à une décision de relaxe ou d'acquittement, le principe est l'effacement des données et l'exception, le maintien pour des raisons tenant à la finalité du fichier. Lorsque les faits à l'origine de l'enregistrement des données dont l'effacement est demandé ont fait l'objet d'une ordonnance de non-lieu rendue par le juge d'instruction en application de l'article 177 du code de procédure pénale ou d'un classement sans suite pour insuffisance de charges par le procureur de la République, les données sont conservées dans le fichier mais sont assorties d'une mention qui fait obstacle à la consultation dans le cadre des enquêtes administratives. Le procureur de la République a toutefois la possibilité d'ordonner leur effacement. Lorsque les faits à l'origine de l'enregistrement des données dont l'effacement est demandé ont fait l'objet d'un classement sans suite pour un autre motif que l'insuffisance de charges, les données sont assorties d'une mention et les dispositions précitées de l'article 230-8 du code de procédure pénale, si elles ne le prévoient pas expressément, ne font pas obstacle à ce que le procureur de la République ou le magistrat référent décide d'accueillir une demande d'effacement. <br/>
<br/>
              7. Dans les hypothèses mentionnées au point 6, les magistrats compétents pour décider de l'effacement des données prennent en considération la nature et la gravité des faits constatés, les motifs de la relaxe, de l'acquittement, du non-lieu ou du classement sans suite, le temps écoulé depuis les faits et la durée légale de conservation restant à courir, au regard de la situation personnelle de l'intéressé, protégée par les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. Ils peuvent prendre ainsi en considération l'âge auquel l'intéressé a commis les faits, son comportement depuis et son attitude vis-à-vis des éventuelles victimes ou son insertion sociale. L'application des dispositions de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales impose également au juge de l'excès de pouvoir d'exercer un entier contrôle sur la décision prise par les autorités désignées par la loi sur les demandes d'effacement des données.<br/>
<br/>
              8. Enfin, ces règles en matière d'effacement s'exercent sans préjudice de l'obligation pour l'autorité compétente de faire droit aux demandes fondées de rectification ou de mise à jour.<br/>
<br/>
<br/>
<br/>
<br/>Le présent avis sera notifié au tribunal administratif de Montreuil, à M. B...A...et au garde des sceaux, ministre de la justice.<br/>
<br/>
Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-07-05 DROITS CIVILS ET INDIVIDUELS. - FICHIER TAJ - EFFACEMENT DES DONNÉES - 1) DEMANDE D'EFFACEMENT DE DONNÉES NE POUVANT ÊTRE COLLECTÉES - OBLIGATION D'ORDONNER L'EFFACEMENT - EXISTENCE - 2) CAS OÙ LES POURSUITES PÉNALES NE SONT PAS RESTÉES SANS SUITE - DEMANDE D'EFFACEMENT DE DONNÉES PRÉSENTÉE AVANT LE TERME DE LA DURÉE DE CONSERVATION - OBLIGATION DE REFUSER L'EFFACEMENT - EXISTENCE - 3) CAS OÙ LES POURSUITES PÉNALES SONT RESTÉES SANS SUITE - A) FACULTÉ D'ACCUEILLIR UNE DEMANDE D'EFFACEMENT - EXISTENCE, DANS TOUS LES CAS - B) ELÉMENTS À PRENDRE EN CONSIDÉRATION - C) CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR - CONTRÔLE ENTIER - 4) ARTICULATION AVEC LES RÈGLES DE RECTIFICATION OU DE MISE À JOUR.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-02-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE NORMAL. - DÉCISIONS PRISES SUR LES DEMANDES D'EFFACEMENT DE DONNÉES DU FICHIER TAJ, DANS LE CAS OÙ LES POURSUITES PÉNALES SONT RESTÉES SANS SUITE.
</SCT>
<ANA ID="9A"> 26-07-05 Il résulte des articles 230-6, 230-7 et 230-8 du code de procédure pénale (CPP) que le législateur a entendu décrire entièrement les possibilités de radiation, correction ou maintien de données dans le fichier traitement des antécédents judiciaires (TAJ), offertes à l'autorité à laquelle il a confié la responsabilité de contrôler sa mise en oeuvre.,,,1) Saisis d'une demande d'effacement de données qui ne sont pas au nombre de celles que l'article 230-7 du CPP autorise à collecter dans le traitement des antécédents judiciaires, le procureur de la République ou le magistrat référent mentionné à l'article 230-9 du même code, désignés par la loi pour contrôler le fichier, sont tenus d'en ordonner l'effacement.... ,,2) Les dispositions de l'article 230-8 du CPP ne prévoyant de règles particulières relatives au maintien ou à l'effacement des données du traitement des antécédents judiciaires qu'en cas de décisions de relaxe, d'acquittement, de non-lieu ou de classement sans suite, le législateur doit être regardé comme n'ayant entendu ouvrir la possibilité d'effacement que dans les cas où les poursuites pénales sont, pour quelque motif que ce soit, demeurées sans suite. Hors cette hypothèse, les données ne peuvent être effacées qu'à l'issue de la durée de conservation fixée par voie réglementaire et le procureur de la République ne peut alors que refuser une demande d'effacement avant ce terme.,,,3) a) Si la procédure a abouti à une décision de relaxe ou d'acquittement, le principe est l'effacement des données et l'exception, le maintien pour des raisons tenant à la finalité du fichier. Lorsque les faits à l'origine de l'enregistrement des données dont l'effacement est demandé ont fait l'objet d'une ordonnance de non-lieu rendue par le juge d'instruction en application de l'article 177 du CPP ou d'un classement sans suite pour insuffisance de charges par le procureur de la République, les données sont conservées dans le fichier mais sont assorties d'une mention qui fait obstacle à la consultation dans le cadre des enquêtes administratives. Le procureur de la République a toutefois la possibilité d'ordonner leur effacement. Lorsque les faits à l'origine de l'enregistrement des données dont l'effacement est demandé ont fait l'objet d'un classement sans suite pour un autre motif que l'insuffisance de charges, les données sont assorties d'une mention et les dispositions de l'article 230-8 du CPP, si elles ne le prévoient pas expressément, ne font pas obstacle à ce que le procureur de la République ou le magistrat référent décide d'accueillir une demande d'effacement.... ,,b) Dans les hypothèses mentionnées au a), les magistrats compétents pour décider de l'effacement des données prennent en considération la nature et la gravité des faits constatés, les motifs de la relaxe, de l'acquittement, du non-lieu ou du classement sans suite, le temps écoulé depuis les faits et la durée légale de conservation restant à courir, au regard de la situation personnelle de l'intéressé, protégée par les stipulations de l'article 8 de la convention européenne de sauvegarde  des droits de l'homme et des libertés fondamentales (convention EDH). Ils peuvent prendre ainsi en considération l'âge auquel l'intéressé a commis les faits, son comportement depuis et son attitude vis-à-vis des éventuelles victimes ou son insertion sociale.... ,,c) L'application de l'article 8 de la convention EDH impose au juge de l'excès de pouvoir d'exercer un entier contrôle sur la décision prise par les autorités désignées par la loi sur les demandes d'effacement des données.,,,4) Ces règles en matière d'effacement s'exercent sans préjudice de l'obligation pour l'autorité compétente de faire droit aux demandes fondées de rectification ou de mise à jour.</ANA>
<ANA ID="9B"> 54-07-02-03 L'application de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (CEDH) impose au juge de l'excès de pouvoir d'exercer un entier contrôle sur la décision prise par les autorités désignées par la loi sur les demandes d'effacement des données du fichier traitement des antécédents judiciaires (TAJ) dans le cas où les poursuites pénales sont restées sans suite.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
