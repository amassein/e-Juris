<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039728704</ID>
<ANCIEN_ID>JG_L_2019_12_000000421675</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/72/87/CETATEXT000039728704.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 31/12/2019, 421675, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-12-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421675</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:421675.20191231</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 21 juin et 16 août 2018 et le 3 avril 2019 au secrétariat du contentieux du Conseil d'Etat, la Fédération des établissements hospitaliers, et d'aide à la personne, privés à but non lucratif (FEHAP) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler, pour excès de pouvoir, l'arrêté de la ministre des solidarités et de la santé et du ministre de l'action et des comptes publics du 17 avril 2018 fixant pour l'année 2018 l'objectif de dépenses d'assurance maladie afférent aux activités de soins de suite et de réadaptation ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts ;<br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 2012-1510 du 29 décembre 2012 ;<br/>
              - la loi n° 2015-1702 du 21 décembre 2015 ;<br/>
              - la loi n° 2016-1917 du 29 décembre 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la Fédération des établissements hospitaliers, et d'aide à la personne, privés à but non lucratif et de l'association Groupe SOS santé ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte des dispositions du 2° de l'article L. 162-22 du code de la sécurité sociale, dans sa rédaction antérieure à la loi du 21 décembre 2015 de financement de la sécurité sociale pour 2016, et des articles R. 162-29-1 et R. 162-29-2 du même code, alors applicables, que les activités de soins de suite et de réadaptation étaient, avant l'intervention de cette loi, financées selon deux modalités différentes. Dans les établissements publics de santé et les établissements de santé privés à but non lucratif mentionnés aux b et c de l'article L. 162-22-6 du code de la sécurité sociale, la part des frais d'hospitalisation, au titre des soins dispensés dans le cadre de ces activités, prise en charge par les régimes obligatoires d'assurance maladie était financée par une dotation annuelle de financement, fixée dans les conditions prévues aux articles L. 174-1 et L. 174-1-1 du même code. Dans les établissements de santé privés mentionnés aux d et e de l'article L. 162-22-6, les prestations d'hospitalisation prises en charge par les régimes obligatoires de sécurité sociale l'étaient sur la base de tarifs journaliers, fixés pour chaque établissement par le directeur général de l'agence régionale de santé, en vertu des articles L. 162 -22-1 et R. 162-29-1 du même code. La loi du 21 décembre 2015 de financement de la sécurité sociale pour 2016 a substitué à ces modalités un financement reposant principalement, en application du 1° de l'article L. 162-23-2 du code de la sécurité sociale et dans les conditions prévues par l'article L. 162-23-3 du même code, d'une part, sur une dotation calculée chaque année sur la base de l'activité antérieure et, d'autre part, pour chaque séjour, sur une fraction des tarifs nationaux des prestations d'hospitalisation mentionnés par l'article L. 162-23-4 du même code. A titre transitoire, le 2° du E du III de l'article 78 de la loi du 21 décembre 2015 a prévu que, du 1er mars 2017 au 31 décembre 2019, les prestations d'hospitalisation relevant des activités de soins de suite et de réadaptation sont financées par le cumul de deux montants correspondant, pour chaque établissement, à une fraction des recettes issues de l'application des modalités de financement antérieures à la loi et à une fraction des recettes issues de l'application des nouvelles modalités de financement prévues au 1° de l'article L. 162-23-2 du code de la sécurité sociale.<br/>
<br/>
              2. Aux termes de l'article L. 162-23 du code de la sécurité sociale, dans sa rédaction issue de la loi du 21 décembre 2015 de financement de la sécurité sociale pour 2016 : " I.- Chaque année, est défini un objectif de dépenses d'assurance maladie afférent aux activités mentionnées au 4° de l'article L. 162-22 qui sont exercées par les établissements de santé (...). Cet objectif est constitué du montant annuel des charges supportées par les régimes obligatoires d'assurance maladie afférentes aux frais d'hospitalisation au titre des soins dispensés au cours de l'année dans le cadre de ces activités. Le contenu de cet objectif est défini par décret. / Le montant de cet objectif est arrêté par l'Etat en fonction de l'objectif national de dépenses d'assurance maladie. / Il prend en compte les évolutions de toute nature à la suite desquelles des établissements, des services ou des activités sanitaires ou médico-sociaux se trouvent placés, pour tout ou partie, sous un régime juridique ou de financement différent de celui sous lequel ils étaient placés auparavant, notamment les conversions d'activité. Il peut être corrigé en fin d'année pour prendre en compte les évolutions intervenues en cours d'année. / (...) / II.- Un décret en Conseil d'Etat précise les éléments pris en compte pour la détermination de cet objectif ainsi que les modalités selon lesquelles, chaque année, sont déterminés les éléments mentionnés aux 1° à 6° de l'article L. 162-23-4 compatibles avec le respect de l'objectif, en prenant en compte à cet effet, notamment, les prévisions d'évolution de l'activité des établissements pour l'année en cours (...). Les tarifs nationaux des prestations mentionnées au 2° de l'article L. 162-23-4 du présent code peuvent être déterminés, en tout ou partie, à partir des données afférentes au coût relatif des prestations, issues notamment de l'étude nationale de coûts définie à l'article L. 6113-11 du code de la santé publique ". Aux termes de l'article R. 162-34-4 du même code : " I. - Chaque année, dans un délai de quinze jours suivant la promulgation de la loi de financement de la sécurité sociale, les ministres chargés de la santé, de la sécurité sociale et du budget arrêtent : / 1° Le montant de l'objectif de dépenses mentionné au I de l'article L. 162-23 ; / (...) II. - Le montant de l'objectif et des parts affectées mentionnés au I sont déterminés notamment en fonction de : / 1° L'état provisoire et l'évolution des charges d'assurance maladie au titre des soins dispensés l'année précédente ; / 2° L'évaluation des charges des établissements ; / 3° L'évaluation des gains de productivité réalisés et envisageables dans le secteur ; / 4° Les changements de régime juridique ou de financement de certains établissements, ou services ou activités des établissements concernés ". Le 6° du E du III de l'article 78 de la loi du 21 décembre 2015 dispose cependant que, du 1er janvier 2018 au 31 décembre 2019, l'objectif de dépenses mentionné au I de l'article L. 162-23 du code de la sécurité sociale est constitué des dépenses afférentes aux activités de soins de suite et de réadaptation financées dans les conditions prévues par les dispositions transitoires de la loi et en particulier par le 2° de ce même E.<br/>
<br/>
              3. Sur le fondement des dispositions mentionnées au point 2, la ministre des solidarités et de la santé et le ministre de l'action et des comptes publics ont, par un arrêté du 17 avril 2018 dont la Fédération des établissements hospitaliers, et d'aide à la personne, privés à but non lucratif demande l'annulation pour excès de pouvoir, fixé l'objectif de dépenses d'assurance maladie afférent aux activités de soins de suite et de réadaptation pour 2018.<br/>
<br/>
              Sur l'intervention de l'association Groupe SOS santé :<br/>
<br/>
              4. L'association Groupe SOS santé justifie d'un intérêt suffisant à l'annulation de l'arrêté attaqué. Ainsi, son intervention est recevable.<br/>
<br/>
              Sur la légalité de l'arrêté attaqué :<br/>
<br/>
              5. Par l'arrêté attaqué, les ministres ont fixé à 8 543,8 millions d'euros l'objectif de dépenses d'assurance maladie afférent aux activités de soins de suite et de réadaptation pour 2018. Il ressort des pièces du dossier que ce montant a été déterminé en tenant compte des allègements de charge que représentent, pour les établissements privés à but lucratif, le crédit d'impôt pour la compétitivité et l'emploi prévu par l'article 244 quater C du code général des impôts issu de l'article 66 de la loi du 29 décembre 2012 de finances rectificative pour 2012 et, pour les établissements privés à but non lucratif, le crédit d'impôt sur la taxe sur les salaires prévu par l'article 231 A du code général des impôts issu de l'article 88 de la loi du 29 décembre 2016 de finances pour 2017.     <br/>
<br/>
              6. En premier lieu, si l'arrêté attaqué prend en considération, dans la fixation de l'objectif de dépenses d'assurance maladie afférent aux activités de soins de suite et de réadaptation, l'incidence du crédit d'impôt pour la compétitivité et l'emploi et du crédit d'impôt sur la taxe sur les salaires sur la moyenne des charges des établissements de santé privés, il n'en résulte pas pour autant qu'il modifierait les règles relatives à ces dispositifs d'allégements de charges. Par suite, le moyen tiré de ce que cet arrêté méconnaîtrait les dispositions législatives instituant les allégements en cause doit être écarté.<br/>
<br/>
              7. En deuxième lieu, sur le fondement des dispositions précitées des articles L. 162-23 et R. 162-34-4 du code de la sécurité sociale, les ministres chargés de la santé, de la sécurité sociale et du budget peuvent légalement tenir compte du niveau respectif des charges exposées par les établissements des différentes catégories mentionnées à l'article L. 162-22-6 du même code ainsi que des produits susceptibles de venir en atténuation des charges que les dotations et tarifs ont vocation à financer. Ces dispositions ne font pas obstacle, à ce titre, à ce qu'ils prennent en considération des charges de nature fiscale, ainsi que des atténuations de charge participant du régime fiscal auquel les établissements sont soumis. Ils ont ainsi pu légalement tenir compte, pour fixer l'objectif de dépenses d'assurance maladie afférent à ces activités, de l'incidence positive du crédit d'impôt pour la compétitivité et l'emploi prévu par l'article 244 quater C du code général des impôts et du crédit d'impôt sur la taxe sur les salaires prévu par l'article 231 A du même code. Par suite, le moyen tiré de ce que l'arrêté attaqué méconnaîtrait les dispositions de l'article R. 162-34-4 du code de la sécurité sociale doit être écarté.<br/>
<br/>
              8. En dernier lieu, il ne ressort pas des pièces du dossier qu'en prenant en considération à hauteur de 30 % l'incidence positive du crédit d'impôt sur la taxe sur les salaires pour les établissements de santé privés à but non lucratif et en fixant le montant de l'objectif de dépenses d'assurance maladie afférent aux activités de soins de suite et de réadaptation à 8 543,8 millions d'euros pour l'année 2018, les ministres chargés de la santé, de la sécurité sociale et du budget auraient commis, au regard notamment des charges de personnel des établissements de santé ayant une activité de soins de suite et de réadaptation, une erreur manifeste dans l'application des dispositions des articles L. 162-23 et R. 162-34-4 du code de la sécurité sociale.<br/>
<br/>
              9. Il résulte de tout ce qui précède que la Fédération des établissements hospitaliers, et d'aide à la personne, privés à but non lucratif n'est pas fondée à demander l'annulation de l'arrêté qu'elle attaque.<br/>
<br/>
              Sur les frais liés au litige : <br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : L'intervention de l'association Groupe SOS santé est admise.<br/>
Article 2 : La requête de la Fédération des établissements hospitaliers, et d'aide à la personne, privés à but non lucratif est rejetée.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la Fédération des établissements hospitaliers, et d'aide à la personne, privés à but non lucratif, à l'association Groupe SOS santé, à la ministre des solidarités et de la santé et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
