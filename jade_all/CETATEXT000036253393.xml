<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036253393</ID>
<ANCIEN_ID>JG_L_2017_12_000000401116</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/25/33/CETATEXT000036253393.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 18/12/2017, 401116, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401116</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Cyrille Beaufils</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Julie Burguburu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:401116.20171218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Mme CJ...DB...et autres ont demandé au tribunal administratif de Clermont-Ferrand d'annuler pour excès de pouvoir, d'une part, l'arrêté n° 2012/113 du 20 juillet 2012 par lequel le préfet de la région Auvergne a approuvé le schéma régional du climat, de l'air et de l'énergie et son annexe le schéma régional éolien et, d'autre part, la décision implicite rejetant leur recours gracieux contre cet arrêté. Par un jugement n° 1300098 du 17 décembre 2013, le tribunal administratif de Clermont-Ferrand a rejeté cette demande. <br/>
<br/>
              Par un arrêt 14LY00473 du 3 mai 2016, la cour administrative d'appel de Lyon a, sur appel de Mme DB...et autres, partiellement annulé le jugement du tribunal administratif du 17 décembre 2013, et annulé l'arrêté du 20 juillet 2012 du préfet de la région Auvergne et la décision implicite de rejet du recours gracieux formé contre cet arrêté. <br/>
<br/>
              Par un pourvoi, enregistré le 30 juin 2016 au secrétariat du contentieux du Conseil d'Etat, la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ;<br/>
              - la directive 2001/42/CE du Parlement européen et du Conseil du 27 juin 2001 relative à l'évaluation des incidences de certains plans et programmes sur l'environnement ;<br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Cyrille Beaufils, auditeur,  <br/>
<br/>
              - les conclusions de Mme Julie Burguburu, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte des pièces du dossier soumis aux juges du fond que le préfet de la région Auvergne a, par un arrêté du 20 juillet 2012, approuvé le schéma régional du climat, de l'air et de l'énergie d'Auvergne et son annexe, le schéma régional éolien ; que, par un jugement du 17 décembre 2013, le tribunal administratif de Clermont-Ferrand a rejeté la demande de Mme DB...et autres tendant à l'annulation pour excès de pouvoir de cet arrêté ainsi que de la décision de rejet de leur recours gracieux ; que, par un arrêt du 3 mai 2016, contre lequel la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat, se pourvoit en cassation, la cour administrative d'appel de Lyon a, après avoir partiellement annulé ce jugement, annulé ces arrêtés au motif qu'ils avaient été adoptés à l'issue d'une procédure irrégulière, faute d'avoir été précédés de l'évaluation environnementale mentionnée à l'article L. 122-4 du code de l'environnement ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 3 de la directive du Parlement européen et du Conseil du 27 juin 2001 relative à l'évaluation des incidences de certains plans et programmes sur l'environnement : " 1. Une évaluation environnementale est effectuée, conformément aux articles 4 à 9, pour les plans et programmes visés aux paragraphes 2, 3 et 4 susceptibles d'avoir des incidences notables sur l'environnement. / 2. Sous réserve du paragraphe 3, une évaluation environnementale est effectuée pour tous les plans et programmes : / a) qui sont élaborés pour les secteurs de l'agriculture, de la sylviculture, de la pêche, de l'énergie, de l'industrie, des transports, de la gestion des déchets, de la gestion de l'eau, des télécommunications, du tourisme, de l'aménagement du territoire urbain et rural ou de l'affectation des sols et qui définissent le cadre dans lequel la mise en oeuvre des projets énumérés aux annexes I et II de la directive 85/337/CEE pourra être autorisée à l'avenir ; / ou b) pour lesquels, étant donné les incidences qu'ils sont susceptibles d'avoir sur des sites, une évaluation est requise en vertu des articles 6 et 7 de la directive 92/43/CEE. (...) " ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 122-4 du code de l'environnement, pris pour la transposition de ces dispositions : " I.- Font l'objet d'une évaluation environnementale au regard des critères mentionnés à l'annexe II à la directive 2001/42/ CE du Parlement européen et du Conseil du 27 juin 2001, relative à l'évaluation des incidences de certains plans et programmes sur l'environnement, les plans, schémas, programmes et autres documents de planification susceptibles d'avoir des incidences sur l'environnement qui, sans autoriser par eux-mêmes la réalisation de travaux ou prescrire des projets d'aménagement, sont applicables à la réalisation de tels travaux ou projets : / 1° Les plans, schémas, programmes et autres documents de planification adoptés par l'Etat, les collectivités territoriales ou leurs groupements et les établissements publics en dépendant, relatifs à l'agriculture, à la sylviculture, à la pêche, à l'énergie ou à l'industrie, aux transports, à la gestion des déchets ou à la gestion de l'eau, aux télécommunications, au tourisme ou à l'aménagement du territoire qui ont pour objet de définir le cadre de mise en oeuvre les travaux et projets d'aménagement entrant dans le champ d'application de l'étude d'impact en application de l'article L. 122-1 ; (...) / IV.- Un décret en Conseil d'Etat définit les plans, schémas, programmes et documents visés aux I et III qui font l'objet d'une évaluation environnementale après un examen au cas par cas effectué par l'autorité administrative de l'Etat compétente en matière d'environnement. " ; qu'aux termes de l'article L. 122-1 du même code : " Les conditions d'application de la présente section pour chaque catégorie de plans ou de documents sont précisées, en tant que de besoin, par décret en Conseil d'Etat. " ; que l'article R. 122-17 du même code fixe une liste de plans, schémas, programmes et autres documents de planification devant être systématiquement soumis à évaluation environnementale au titre du I de l'article L. 122-4 ; <br/>
<br/>
              4. Considérant qu'aux termes de l'article L. 222-1 du code de l'environnement, dans sa rédaction en vigueur : " I. Le préfet de région et le président du conseil régional élaborent conjointement le projet de schéma régional du climat, de l'air et de l'énergie, après consultation des collectivités territoriales concernées et de leurs groupements. Ce schéma fixe, à l'échelon du territoire régional et à l'horizon 2020 et 2050 : / 1° Les orientations permettant d'atténuer les effets du changement climatique et de s'y adapter (...). A ce titre, il définit notamment les objectifs régionaux en matière de maîtrise de l'énergie ; / 2° Les orientations permettant, pour atteindre les normes de qualité de l'air mentionnées à l'article L. 221-1, de prévenir ou de réduire la pollution atmosphérique ou d'en atténuer les effets. A ce titre, il définit des normes de qualité de l'air propres à certaines zones lorsque les nécessités de leur protection le justifient ; / 3° Par zones géographiques, les objectifs qualitatifs et quantitatifs à atteindre en matière de valorisation du potentiel énergétique terrestre, renouvelable et de récupération et en matière de mise en oeuvre de techniques performantes d'efficacité énergétique (...). A ce titre, le schéma régional du climat, de l'air et de l'énergie vaut schéma régional des énergies renouvelables au sens du III de l'article 19 de la loi n° 2009-967 du 3 août 2009 de programmation relative à la mise en oeuvre du Grenelle de l'environnement. (...) " ; que cet article L. 222-1 précise en outre que : " Un schéma régional éolien qui constitue un volet annexé à ce document définit, en cohérence avec les objectifs issus de la législation européenne relative à l'énergie et au climat, les parties du territoire favorables au développement de l'énergie éolienne. " ; que l'article R. 222-2 du même code dispose que : " I.- Le rapport du schéma régional présente et analyse, dans la région, et en tant que de besoin dans des parties de son territoire, la situation et les politiques dans les domaines du climat, de l'air et de l'énergie et les perspectives de leur évolution aux horizons 2020 et 2050 (...). / II. - Sur la base de ce rapport, un document d'orientations définit (...) : / 1° Des orientations ayant pour objet la réduction des émissions de gaz à effet de serre portant sur l'amélioration de l'efficacité énergétique et la maîtrise de la demande énergétique dans les secteurs résidentiel, tertiaire, industriel, agricole, du transport et des déchets ainsi que des orientations visant à adapter les territoires et les activités socio-économiques aux effets du changement climatique ; / 2° Des orientations destinées à prévenir ou à réduire la pollution atmosphérique (...). Ces orientations sont renforcées dans les zones où les valeurs limites de la qualité de l'air sont ou risquent d'être dépassées et dites sensibles en raison de l'existence de circonstances particulières locales liées à la protection des intérêts définis à l'article L. 220-2, pour lesquelles il définit des normes de qualité de l'air lorsque les nécessités de cette protection le justifient ; / 3° Des objectifs quantitatifs de développement de la production d'énergie renouvelable (...). / IV. - Le volet annexé au schéma régional du climat, de l'air et de l'énergie, intitulé " schéma régional éolien ", identifie les parties du territoire régional favorables au développement de l'énergie éolienne compte tenu d'une part du potentiel éolien et d'autre part des servitudes, des règles de protection des espaces naturels ainsi que du patrimoine naturel et culturel, des ensembles paysagers, des contraintes techniques et des orientations régionales. Il établit la liste des communes dans lesquelles sont situées ces zones. Les territoires de ces communes constituent les délimitations territoriales du schéma régional éolien au sens de l'article L. 314-9 du code de l'énergie. " ; que le schéma régional du climat, de l'air et de l'énergie et le schéma régional éolien n'étaient pas au nombre des schémas, programmes et autres documents de planification devant être précédés d'une évaluation environnementale selon la rédaction de l'article R. 122-17 du même code rappelée ci-dessus alors en vigueur ; <br/>
<br/>
              5. Considérant qu'il résulte des dispositions rappelées au point précédent que le schéma régional du climat, de l'air et de l'énergie définit les orientations permettant d'atténuer les effets du changement climatique et de s'y adapter et de prévenir ou de réduire la pollution atmosphérique ou d'en atténuer les effets ; qu'il définit également, par zones géographiques, les objectifs qualitatifs et quantitatifs à atteindre en matière de valorisation du potentiel énergétique terrestre, renouvelable et de récupération et en matière de mise en oeuvre de techniques performantes d'efficacité énergétique telles que les unités de cogénération, notamment alimentées à partir de biomasse, conformément aux objectifs issus de la législation européenne relative à l'énergie et au climat ; que le schéma régional éolien qui lui est annexé détermine les parties du territoire favorables au développement de l'énergie éolienne, conformément à ces derniers objectifs ; qu'en outre, en vertu du I de l'article L. 222-4 précité du code de l'environnement, les orientations définies par le schéma régional du climat de l'air et de l'énergie s'imposent dans un rapport de compatibilité aux plans de protection de l'atmosphère pour l'exécution desquels les autorités compétentes en matière de police arrêtent, en application de l'article L. 222-6, les mesures destinées à réduire les émissions des sources de pollution atmosphérique, ainsi qu'aux plans de déplacement urbains et aux plans locaux d'urbanisme pour leurs dispositions tenant lieu de plans de déplacement urbain, en vertu de l'article L. 1214-7 du code des transports, dans sa rédaction applicable ; qu'il résulte de l'ensemble de ces éléments que les schémas ainsi définis doivent être regardés comme définissant, au sens des dispositions du I de l'article L. 122-4 du code de l'environnement, le cadre de mise en oeuvre de travaux et projets d'aménagement entrant dans le champ d'application de l'étude d'impact dans les domaines, notamment, de l'industrie, de l'énergie et des transports ; que ces schémas doivent en conséquence faire l'objet d'une évaluation environnementale ; qu'ainsi les dispositions de l'article L. 122-4 imposaient, à la date des décisions attaquées, par des dispositions suffisamment précises, la réalisation d'une telle évaluation, sans qu'il fût nécessaire qu'un texte réglementaire le prescrivît ; que l'article L. 122-4 ne prévoit, d'ailleurs, l'intervention d'un décret d'application que pour définir les plans, schémas, programmes et documents qui font l'objet d'une évaluation environnementale après un examen au cas par cas ; que la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat ne peut, par suite, utilement exciper de ce que, selon les dispositions de l'article R. 122-17 du code de l'environnement alors en vigueur, les schémas régionaux du climat, de l'air et de l'énergie et leur volet schéma régional éolien n'étaient pas au nombre des plans, schémas, programmes et documents devant être obligatoirement soumis à évaluation environnementale ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la cour administrative d'appel de Lyon, qui a suffisamment motivé son arrêt sur ce point, n'a pas commis d'erreur de droit en se fondant, pour annuler le schéma régional du climat, de l'air et de l'énergie et le schéma régional de l'éolien de la région Auvergne, sur le motif tiré de l'absence de l'évaluation environnementale prescrite par l'article L. 122-4 du code de l'environnement ; <br/>
<br/>
              7. Considérant que, par suite, la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée au ministre d'Etat, ministre de la transition écologique et solidaire.<br/>
Copie en sera adressée à Mme CJ...DB..., à Mme X...BN..., à Mme CR...CB..., à M. K...U..., à Mme CT...B..., à Mme BS...V..., à Mme S...DA..., à Mme AN...BP..., à Mme CP...AX..., à M. AF...CU..., à M. G...CX..., à Mme AJ...CX..., à Mme M...AZ..., à Mme BH...AZ..., à M. G...W..., à M. F...CV..., à M. K...BA..., à M. J...L..., à Mme AY...Y..., à M. T...BR..., à Mme BQ...CE..., à M. F...DE...CY..., à Mme AV...CF..., à M. AK...CG..., à M. I...CZ...DC..., à Mme A...BB..., à M. O...BB..., à Mme AG...BB...-DF..., à M. F...-DD...BB..., à M. AW...CH..., à M. AC...CH..., à M. C... AB..., à M. AK...AB..., à Mme CC...AB..., à M. F...AQ...N..., à M. BI... D..., à M. O...CK..., à Mme CD...BT..., à M. AW...AE..., à M. Q... CL..., à M. AA...E..., à M.F...  AQ...AH..., à M. I...BD..., à M. CM...CW..., à M. AK...BE..., à M. X...P..., à Mme BO...P..., à M. AQ... AI..., à Mme AO...BF..., à M. J...CN..., à Mme BW...CO..., à Mme AP...BG..., à Mme BY...AL..., à M. BC...AM..., à M. F...-DD...CQ..., à M. F...-Q...R..., à M. Z...BX..., à M. AD...CS..., à Mme DG... -CZ...BJ..., à M. BK...AR..., à M. X...AS..., à Mme BV...BL..., à M. AQ...BL..., à M. Z...AT..., à M. O...BZ..., à Mme CZ...-Q...AU..., à M. X...AU..., à Mme BU...H..., à Mme CZ...CD...CA..., à M. CI... BM..., à Mme BU...BM..., à la fédération "Environnement Durable", à l'association "Vieille maisons françaises", à l'association "Stop Eole-Collectif Auvergne", à l'association "Vigies du Montfouat à Espalem", à l'association "Les amis de Montcelet", à l'association "Eolienne s'en naît trop", à l'association "Protection des paysages des Mazeaux de Riotord", à l'association "Sauvegarde patrimoniale de Châtel Montagne", à l'association "Le vent qui souffle à travers la montagne", à l'association "Hurlevent", à l'association "Oustaou Vellavi", à l'association "Vent libre", à l'association "AuTant en Emporte le VEnt", à l'association "Vent de raison", à l'association "Pour la protection des sites naturels entre Jordanne et Goul", à l'association "Vent de la châtaigne", à l'association "Brisevent/Forterre", à l'association "Défense de l'environnement des monts du Forez", à l'association "Le vent de la chaux", à l'association de défense de l'environnement du Lembron Val d'Allier et des alentours, à l'association pour la sauvegarde et la promotion du patrimoine du canton de Saint-Cernin, à l'association "Ally Mercoeur, Vivre en paix" et à l'association "Du vent les éoliennes".<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
