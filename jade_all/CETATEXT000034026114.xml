<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034026114</ID>
<ANCIEN_ID>JG_L_2017_02_000000405811</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/02/61/CETATEXT000034026114.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 10/02/2017, 405811, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-02-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>405811</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Sylvain Monteillet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:405811.20170210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1°) Sous le n° 405811, M. et Mme B...A..., à l'appui de leur demande tendant à la décharge des suppléments d'impositions sur le revenu et de cotisations sociales mis à leur charge au titre de l'année 2012, ont produit deux mémoires, enregistrés les 31 août et 22 novembre 2016 au greffe du tribunal administratif de Rouen, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lesquels ils soulèvent une question prioritaire de constitutionnalité.<br/>
<br/>
              Par une ordonnance n° 1502042 du 6 décembre 2016, enregistrée le 9 décembre 2016 au secrétariat du contentieux du Conseil d'Etat, le président de la 2ème chambre du tribunal administratif de Rouen a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 80 D du livre des procédures fiscales.<br/>
<br/>
<br/>
              2°) Sous le n° 405812, M. et MmeA..., à l'appui de leur demande tendant à la décharge des suppléments d'impositions sur le revenu et de cotisations sociales mis à leur charge au titre des années 2010 et 2011, ont produit deux mémoires, enregistrés les 31 août et 22 novembre 2016 au greffe du tribunal administratif de Rouen, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lesquels ils soulèvent une question prioritaire de constitutionnalité.<br/>
<br/>
              Par une ordonnance n° 1502043 du 6 décembre 2016, enregistrée le 9 décembre 2016 au secrétariat du contentieux du Conseil d'Etat, le président de la 2ème chambre du tribunal administratif de Rouen a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 80 D du livre des procédures fiscales.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et ses articles 61-1 et 72 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sylvain Monteillet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les questions prioritaires de constitutionnalité transmises au Conseil d'Etat sous les n°s 405811 et 405812 ont pour objet les dispositions de l'article L. 80 D du livre des procédures fiscales. Il y a lieu de les joindre pour statuer par une seule décision. <br/>
<br/>
              2. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              3. L'article L. 80 D du livre des procédures fiscales dispose : " Les décisions mettant à la charge des contribuables des sanctions fiscales sont motivées au sens de la loi n° 79-587 du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public, quand un document ou une décision adressés au plus tard lors de la notification du titre exécutoire ou de son extrait en a porté la motivation à la connaissance du contribuable. / Les sanctions fiscales ne peuvent être prononcées avant l'expiration d'un délai de trente jours à compter de la notification du document par lequel l'administration a fait connaître au contribuable ou redevable concerné la sanction qu'elle se propose d'appliquer, les motifs de celle-ci et la possibilité dont dispose l'intéressé de présenter dans ce délai ses observations ".<br/>
<br/>
              4. M. et Mme B...A...soutiennent que ces dispositions, combinées au principe jurisprudentiel d'indépendance des procédures fiscales, sont contraires au principe du respect des droits de la défense, protégé par l'article 8 de la Déclaration des droits de l'homme et du citoyen.<br/>
<br/>
              5. Les dispositions précitées de l'article L. 80 D du livre des procédures fiscales ne comportent toutefois par elles-mêmes aucune atteinte au principe constitutionnel du respect des droits de la défense qu'elles visent, au contraire, à garantir. Par ailleurs, si un contribuable ne peut tirer argument d'une simple contradiction de motifs entre les pénalités infligées dans deux procédures d'imposition distinctes, en se bornant à soutenir qu'elles correspondent à des majorations différentes alors que les faits en cause étaient identiques, il peut, en revanche, contester chaque pénalité en faisant valoir que n'étaient pas réunies les conditions requises pour que soit appliquée la majoration correspondant à chacune d'entre elles.<br/>
<br/>
              6. Il résulte de ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité ainsi soulevée, le moyen tiré de ce que les dispositions de l'article L. 80 D du livre des procédures fiscales portent atteinte aux droits et libertés garantis par la Constitution doit être écarté.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le tribunal administratif de Rouen.<br/>
Article 2 : La présente décision sera notifiée M. et Mme B...A....<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre, au ministre de l'économie et des finances et au tribunal administratif de Rouen.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
