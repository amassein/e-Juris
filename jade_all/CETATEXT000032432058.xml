<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032432058</ID>
<ANCIEN_ID>JG_L_2016_04_000000389063</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/43/20/CETATEXT000032432058.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 15/04/2016, 389063, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-04-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389063</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Tristan Aureau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:389063.20160415</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 389063, la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 27 mars et 12 juin 2015 et le 14 mars 2016 au secrétariat du contentieux du Conseil d'Etat, les sociétés Groupe Eurotunnel, France Manche et Channel Tunnel Group demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2015-84 du 28 janvier 2015 fixant la liste des réseaux ferroviaires présentant des caractéristiques d'exploitation comparables à celles du réseau ferré national ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu 2°, sous le n° 390985, la procédure suivante : <br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 12 juin 2015 et le 18 mars 2016, les sociétés Groupe Eurotunnel, France Manche et Channel Tunnel Group demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté de la ministre de l'écologie, du développement durable et de l'énergie du 13 avril 2015 fixant les modalités particulières d'application aux réseaux présentant des caractéristiques d'exploitation comparables à celles du réseau ferré national des dispositions des titres II et V du décret n° 2006-1279 du 19 octobre 2006 relatif à la sécurité des circulations ferroviaires et à l'interopérabilité du système ferroviaire, fixant les caractéristiques de l'inscription d'identification prévue à l'article 57 du décret du 19 octobre 2006 précité et fixant les conditions et modalités d'application des arrêtés prévus par le décret du 19 octobre 2006 précité ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des transports ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Tristan Aureau, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat du Groupe Eurotunnel et autres ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que les requêtes visées ci-dessus présentent à juger les mêmes questions ; qu'il y a lieu de les joindre pour statuer par une seule décision ; <br/>
<br/>
              2.	Considérant que les sociétés requérantes ont demandé, dans leur requête enregistrée le 27 mars 2015, l'annulation pour excès de pouvoir du décret du 28 janvier 2015 visé ci-dessus ; que, toutefois, leur requête doit être regardée comme tendant, en réalité, uniquement à l'annulation pour excès de pouvoir des dispositions du 2° de l'article 1er de ce décret, lesquelles portent sur la liaison trans-Manche et sont divisibles des autres dispositions figurant dans ce décret ; que, de même, si les sociétés requérantes ont demandé, dans leur requête enregistrée le 12 juin 2015, l'annulation pour excès de pouvoir de l'arrêté ministériel du 13 avril 2015 visé ci-dessus, cette requête doit être regardée comme tendant, en réalité, uniquement à l'annulation pour excès de pouvoir des dispositions de cet arrêté en tant qu'elles ont été prises pour application du 2° de l'article 1er du décret du 28 janvier 2015 et concernent, à ce titre, la liaison trans-Manche ; <br/>
<br/>
              3.	Considérant qu'aux termes de l'article L. 2221-1 du code des transports : " L'établissement public de l'Etat dénommé "Etablissement public de sécurité ferroviaire" veille au respect des règles relatives à la sécurité et à l'interopérabilité des transports ferroviaires sur le réseau ferré national et sur les autres réseaux ferroviaires présentant des caractéristiques d'exploitation comparables dont la liste est fixée par voie réglementaire. (...) " ; qu'aux termes de l'article L. 2221-7 du même code : " Un décret en Conseil d'Etat fixe les modalités d'application de la présente section (...) " ;<br/>
<br/>
              4.	Considérant qu'il résulte des dispositions précitées de l'article L. 2221-7 du code des transports que les dispositions réglementaires qui déterminent les modalités d'applications de la section 1 du chapitre Ier du titre II du livre II de la deuxième partie de la partie législative de ce code sont prises par décret en Conseil d'Etat ; que les dispositions précitées de l'article L. 2221-1 du même code figurent dans cette section ; que le renvoi fait par les dispositions de ce dernier article à des dispositions prises " par voie réglementaire " n'implique pas, par lui-même, qu'il puisse être dérogé à la nécessité d'un décret en Conseil d'Etat ; que les dispositions du 2° de l'article 1er du décret du 28 janvier 2015, prises sur le fondement de l'article L. 2221-1, auraient ainsi dû être préalablement soumises au Conseil d'Etat ; que, faute de l'avoir été, ces dispositions doivent être annulées ; que doivent, par voie de conséquence, être également annulées les dispositions de l'arrêté ministériel du 13 avril 2015, en tant qu'elles ont été prises pour application du 2° de l'article 1er du décret du 28 janvier 2015 ; <br/>
<br/>
              5.	Considérant qu'il résulte de ce qui précède que les sociétés requérantes sont fondées, sans qu'il soit besoin d'examiner les autres moyens des requêtes, à demander l'annulation pour excès de pouvoir des dispositions du 2° de l'article 1er du décret du 28 janvier 2015 ainsi que des dispositions de l'arrêté du 13 avril 2015 en tant qu'elles ont été prises pour leur application ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser aux sociétés Groupe Eurotunnel, France Manche et Channel Tunnel Group au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le 2° de l'article 1er du décret du 28 janvier 2015 fixant la liste des réseaux ferroviaires présentant des caractéristiques d'exploitation comparables à celles du réseau ferré national, ainsi que les dispositions de l'arrêté du ministre de l'écologie, du développement durable et de l'énergie du 13 avril 2015, en tant qu'elles ont été prises pour application du 2° de l'article 1er du décret du 28 janvier 2015, sont annulées.<br/>
<br/>
Article 2 : L'Etat versera aux sociétés Groupe Eurotunnel, France Manche et Channel Tunnel Group une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée aux sociétés Groupe Eurotunnel, France Manche et Channel Tunnel Group ainsi qu'à la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
