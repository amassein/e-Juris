<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027979352</ID>
<ANCIEN_ID>JG_L_2013_09_000000367424</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/97/93/CETATEXT000027979352.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 20/09/2013, 367424, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-09-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367424</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CARBONNIER ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Jean-Baptiste de Froment</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:367424.20130920</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 5 avril et 5 juillet 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B...C..., demeurant au ... à Marseille (13001) ; M. C...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 12MA00409 du 5 février 2013 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant, d'une part, à l'annulation du jugement n°1002569 du 29 novembre 2011 par lequel le tribunal administratif de Marseille a rejeté sa demande tendant à la constatation de l'inexistence matérielle d'un acte de délégation de compétence au profit de MaîtreA..., à ce que soient déclarées nulles et de nul effet la décision du 6 mai 2009, par laquelle MaîtreA..., agissant au nom du bâtonnier de l'ordre des avocats du barreau de Marseille, a prorogé de quatre mois le délai imparti par l'article 175 du décret           n° 91-1197 du 27 novembre 1991 pour statuer sur une contestation d'honoraires l'opposant à ses clients, et la décision du 14 septembre 2009, par laquelle Maître A...a statué sur la contestation d'honoraires et subsidiairement, à l'annulation de ces deux dernières décisions, d'autre part, à ce qu'il soit fait droit à sa demande de première instance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'ordre des avocats au barreau de Marseille la somme de 4 000 euros, en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu la loi n° 71-1130 du 31 décembre 1971 ;<br/>
<br/>
              Vu le décret du 26 octobre 1849 portant règlement d'administration publique déterminant les formes de procédure du tribunal des conflits ;<br/>
<br/>
              Vu le décret n° 91-1197 du 27 novembre 1991 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Baptiste de Froment, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Carbonnier, avocat de M. C...;<br/>
<br/>
<br/>
<br/>Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'État (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant, d'une part, que les dispositions de l'article L. 822-1 du code de justice administrative instituent, dans l'intérêt d'une bonne administration de la justice, une procédure d'admission des pourvois en cassation devant le Conseil d'Etat, en prévoyant que l'admission est refusée si ces pourvois sont irrecevables ou ne sont fondés sur aucun moyen sérieux et sont ainsi dépourvus de toute chance de succès ; que, par suite, les dispositions contestées ne méconnaissent pas le droit à un recours juridictionnel effectif ou les droits de la défense ; qu'elles n'ont, en tout état de cause, pas eu pour objet ou pour effet de porter atteinte au droit de propriété, à la liberté d'expression ou à celle d'entreprendre, au principe d'égalité ou à un " droit au développement personnel " ; <br/>
<br/>
              3. Considérant, d'autre part, que la méconnaissance par le législateur de sa propre compétence ne peut être invoquée à l'appui d'une question prioritaire de constitutionnalité que dans le cas où cette méconnaissance affecte, par elle-même, un droit ou une liberté que la Constitution garantit ; qu'il résulte des articles 34 et 37 de la Constitution que les dispositions de la procédure à suivre devant les juridictions relèvent de la compétence réglementaire, dès lors qu'elles ne concernent pas la procédure pénale et qu'elles ne mettent pas en cause les règles ou les principes fondamentaux placés par la Constitution dans le domaine de la loi ; que les modalités d'instruction des pourvois en cassation ne mettent pas en cause ces règles ou principes fondamentaux ; qu'il n'appartient pas davantage au législateur de déterminer les modalités de rédaction des motifs des décisions juridictionnelles ; que le moyen tiré de ce que le législateur aurait méconnu sa compétence ne présente donc pas un caractère sérieux ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; qu'ainsi, et sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que les dispositions contestées porteraient atteinte aux droits et libertés garantis par la Constitution doit être écarté ;<br/>
<br/>
              Sur les autres moyens :<br/>
<br/>
              5. Considérant qu'aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux. " ;<br/>
<br/>
              6. Considérant que, pour demander l'annulation de l'arrêt qu'il attaque, M. C... soutient que l'arrêt est intervenu au terme d'une procédure irrégulière, et en méconnaissance du principe du contradictoire, dès lors que l'arrêt vise sans autre précision " les autres pièces du dossier " et que les conclusions du rapporteur public ne lui ont pas été communiquées ; que  l'arrêt est insuffisamment motivé en ce qu'il ne répond pas à certains de ses moyens et arguments, relatifs notamment à l'existence d'une voie de fait, tendant à démontrer la compétence de la juridiction administrative ; que l'arrêt est entaché de défaut de réponse à moyen et d'erreur de droit pour avoir écarté le moyen tiré de l'irrecevabilité du mémoire en défense du 20 juillet 2010 de l'ordre des avocats au barreau de Marseille ; que l'arrêt est entaché d'une erreur de droit pour avoir jugé la juridiction administrative incompétente ; que l'arrêt est entaché d'une autre erreur de droit en ce que, en méconnaissance des dispositions de l'article 34 du décret du 26 octobre 1849 et alors que la juridiction judiciaire avait décliné sa compétence, la cour administrative d'appel n'a pas renvoyé le litige au Tribunal des conflits pour qu'il statue sur la compétence ;<br/>
<br/>
              7. Considérant qu'aucun de ces moyens n'est de nature à permettre l'admission du pourvoi ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M.C....<br/>
<br/>
      Article 2 : Le pourvoi de M.C...  n'est pas admis.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M.C..., à la garde des sceaux, ministre de la justice et à l'ordre des avocats au barreau de Marseille.<br/>
      Copie en sera adressée Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
