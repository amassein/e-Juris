<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038823643</ID>
<ANCIEN_ID>JG_L_2019_07_000000428111</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/82/36/CETATEXT000038823643.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 24/07/2019, 428111, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428111</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Yohann Bouquerel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:428111.20190724</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Par un jugement n°s 1600632, 1606006 du 14 février 2019, enregistré les 18 et 19 février 2019 au secrétariat du contentieux du Conseil d'Etat, le tribunal administratif de Cergy-Pontoise a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, les deux requêtes présentées à ce tribunal par M. A...B...et enregistrées respectivement sous les numéros 428111 et 428193. <br/>
<br/>
              1° Sous le n° 428111, par une requête et un mémoire en réplique, enregistrés au greffe du tribunal administratif de Cergy-Pontoise les 22 janvier 2016 et 19 décembre 2018, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le décret du 9 juillet 2015 portant nomination et affectation dans la 1ère section, nomination dans la 2ème section d'officiers généraux de la marine (corps des officiers de la marine nationale administrés par le ministère de l'écologie, du développement durable et de l'énergie), en tant qu'il ne prévoit pas sa nomination au grade de professeur général de 2ème classe de l'enseignement maritime ;<br/>
<br/>
              2°) d'annuler la décision implicite de rejet de son recours formé le 29 juillet 2015 auprès de la commission des recours des militaires et tendant à l'annulation dans cette mesure du décret du 9 juillet 2015.<br/>
<br/>
              2° Sous le n° 428193, par une requête et un mémoire en réplique, enregistrés au greffe du tribunal administratif de Cergy-Pontoise les 22 juin 2016 et 30 décembre 2018, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le décret du 27 octobre 2015 portant affectation d'un officier général (corps d'officiers de la marine nationale administrés par le ministère de l'écologie, du développement durable et de l'énergie) ;<br/>
<br/>
              2°) d'annuler la décision implicite de rejet de son recours formé le 23 décembre 2015 auprès de la commission des recours des militaires et tendant à l'annulation de ce décret du 27 octobre 2015.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code de la défense ;<br/>
              - le décret n° 77-33 du 4 janvier 1977 ;<br/>
              - le décret n° 2010-1129 du 28 septembre 2010 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yohann Bouquerel, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Les requêtes de M. B...présentant à juger les mêmes questions, il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Aux termes de l'article R. 311-1 du code de justice administrative : " Le Conseil d'Etat est compétent pour connaître en premier et dernier ressort : 1° Des recours dirigés contre les ordonnances du Président de la République et les décrets (...) " ; aux termes de l'article R. 351-1 du même code: " Lorsque le Conseil d'Etat est saisi de conclusions relevant de sa compétence de premier ressort, il est également compétent pour connaître de conclusions connexes relevant normalement de la compétence de premier ressort d'un tribunal administratif ".<br/>
<br/>
              3. M.B..., professeur en chef de 1ère classe de l'enseignement maritime, a formé un recours, le 29 juillet 2015, auprès de la commission des recours des militaires contre le décret du 9 juillet 2015 portant nomination et affectation dans la 1ère section, nomination dans la 2ème section d'officiers généraux de la marine, en tant qu'il ne le nomme pas au grade de professeur général de 2ème classe de l'enseignement maritime. La commission des recours des militaires n'a pas répondu à cette demande. Par un recours formé le 23 décembre 2015 auprès de la commission des recours des militaires, M. B...a également demandé l'annulation du décret du 27 octobre 2015 portant nomination d'un inspecteur général de l'enseignement maritime en tant qu'il ne le nomme pas dans ces fonctions. La commission des recours des militaires n'a pas répondu à cette seconde demande. M. B...a introduit deux requêtes auprès du tribunal administratif de Cergy-Pontoise, respectivement les 22 janvier et 22 juin 2016, tendant, d'une part, à l'annulation pour excès de pouvoir du décret du 9 juillet 2015, en tant qu'il ne prévoit pas sa nomination au grade de professeur général de 2ème classe de l'enseignement maritime, ainsi que du refus implicite de la commission des recours des militaires de faire droit à sa demande, et, d'autre part, à l'annulation pour excès de pouvoir du décret du 27 octobre 2015 ainsi que du refus implicite de la commission de faire droit à sa demande. Par un jugement du 14 février 2019, le tribunal administratif de Cergy-Pontoise, après avoir joint les deux affaires, les a transmises au Conseil d'Etat sur le fondement de l'article R. 351-2 du code de justice administrative.<br/>
<br/>
              4. Le Conseil d'Etat est compétent, en application des dispositions du 1° de l'article R. 311-1 du code de justice administrative, pour connaître des conclusions de M. B..., tendant à l'annulation des décrets du Président de la République des 9 juillet et 27 octobre 2015, en tant qu'il ne figure pas parmi les personnes nommées ou affectées par ces décrets. Le Conseil d'Etat est, par suite, également compétent pour connaître des conclusions dirigées contre les décisions de rejet des recours formés par M. B...devant la commission des recours des militaires.<br/>
<br/>
              Sur les conclusions dirigées contre le décret du 9 juillet 2015 en tant qu'il ne prévoit pas sa nomination au grade de professeur général de 2ème classe de l'enseignement maritime et contre le décret du 27 octobre 2015 en tant qu'il ne le nomme pas inspecteur général de l'enseignement maritime :<br/>
<br/>
              5. M. B...ayant saisi la commission des recours des militaires respectivement de deux recours dirigés contre les décrets contestés, les décisions implicites de rejet nées du silence gardé par la commission sur ces recours se sont entièrement substituées à ces décrets en tant qu'ils ne comportent pas la nomination ou l'affectation de M.B.... Ainsi, les conclusions de M. B...dirigées contre ces décrets sont irrecevables et ne peuvent être accueillies.<br/>
<br/>
              Sur les conclusions dirigées contre les décisions de rejet des recours formés devant la commission de recours des militaires :<br/>
<br/>
              En ce qui concerne la décision du rejet du recours dirigé contre le décret du 9 juillet 2015 en tant qu'il ne le nomme pas au grade de professeur général de 2ème classe de l'enseignement maritime :<br/>
<br/>
              6. En premier lieu, aux termes de l'article 1er du décret du 25 août 2005 relatif à la gestion et à l'administration des corps militaires relevant du ministre chargé de la mer : " Les officiers du corps des administrateurs des affaires maritimes, du corps des professeurs de l'enseignement maritime et du corps technique et administratif des affaires maritimes relèvent du ministre chargé de la mer qui exerce, conjointement avec le ministre de la défense, les pouvoirs dévolus à celui-ci ". Aux termes de l'article 12 du décret du 4 janvier 1977 portant statut particulier du corps des professeurs de l'enseignement maritime : " Les promotions dans le corps des professeurs de l'enseignement maritime ont lieu exclusivement au choix ". 	<br/>
<br/>
              7. Si M. B...allègue être le plus ancien fonctionnaire dans le grade de professeur en chef de 1ère classe de l'enseignement maritime et avoir bénéficié, tout au long de sa carrière, de très bonnes appréciations de la part de sa hiérarchie, cette double circonstance, à la supposer établie, ne confère aucun droit à ce dernier à un avancement de grade, compte tenu du pouvoir d'appréciation reconnu au ministre chargé de la mer et au ministre de la défense par les dispositions citées au point 6. Il résulte en particulier des éléments fournis par le ministre chargé de la mer à la suite d'une mesure supplémentaire d'instruction, que ce dernier n'a pas commis d'erreur de fait ni d'erreur manifeste dans l'appréciation des mérites comparés du requérant et des deux fonctionnaires nommés professeurs généraux de 2ème classe de l'enseignement maritime par le décret du 9 juillet 2015, compte tenu des évaluations et des notations de ces deux fonctionnaires et du requérant pour la période comprise entre 2010 et 2015. Par ailleurs, il ne ressort pas des pièces du dossier que la commission d'avancement aurait méconnu le principe d'impartialité à son encontre.<br/>
<br/>
              8. En deuxième lieu, si le requérant soutient que le décret du 9 juillet 2015 a été pris afin de l'empêcher d'exercer des responsabilités au sein de l'Ecole nationale supérieure maritime, le détournement de pouvoir allégué n'est pas établi.<br/>
<br/>
              9. En troisième lieu, le requérant soutient que la décision de ne pas le nommer professeur général de 2ème classe de l'enseignement maritime est injustifiée car elle est la conséquence de sa notation en 2010, qui lui était défavorable et qui reposait sur des griefs infondés. Il ressort toutefois des pièces du dossier que, par un arrêt du 11 mai 2017 devenu définitif, la cour administrative d'appel de Versailles a rejeté le recours de M. B...tendant à annuler sa notation de 2010 au motif qu'elle serait entachée d'une erreur manifeste d'appréciation de la part de son notateur et d'un détournement de pouvoir. Ce moyen ne peut, dès lors, qu'être écarté.<br/>
<br/>
              En ce qui concerne la décision du rejet du recours dirigé contre le décret du 27 octobre 2015 en tant qu'il ne le nomme pas inspecteur général de l'enseignement maritime :<br/>
<br/>
              10. Il résulte en particulier des éléments fournis par le ministre chargé de la mer à la suite d'une mesure supplémentaire d'instruction que la décision contestée n'est entachée ni d'erreur de fait ni d'erreur manifeste dans l'appréciation des mérites comparés du requérant et du professeur général de l'enseignement maritime nommé inspecteur général, compte tenu des évaluations et des notations de ce fonctionnaire et du requérant pour la période comprise entre 2010 et 2015.<br/>
<br/>
              11. Le détournement de pouvoir allégué n'est pas établi.<br/>
<br/>
              12. Il résulte de tout ce qui précède que les deux requêtes de M. B...ne peuvent qu'être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes de M. B...sont rejetées.<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et à la ministre de la transition écologique et solidaire.<br/>
Copie en sera adressée à la ministre des armées.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
