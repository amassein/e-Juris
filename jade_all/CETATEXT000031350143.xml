<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031350143</ID>
<ANCIEN_ID>JG_L_2015_10_000000381754</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/35/01/CETATEXT000031350143.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 21/10/2015, 381754, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-10-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>381754</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR>M. Benjamin de Maillard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:381754.20151021</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 24 juin et 6 août 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme B...C..., demeurant au... ; Mme C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 11659 du 5 mai 2014 par laquelle la chambre disciplinaire nationale de l'ordre des médecins a, d'une part, rejeté sa requête tendant à l'annulation de la décision n° DG 778 du 24 avril 2012 par laquelle la chambre disciplinaire de première instance de Champagne-Ardenne de l'ordre des médecins, statuant sur la plainte de M. D... A...transmise par le conseil départemental de l'Aube de l'ordre des médecins, lui avait infligé la sanction d'interdiction d'exercer la médecine durant six mois et, d'autre part, a porté la sanction d'interdiction à un an, à compter du 1er septembre 2014 ; <br/>
<br/>
              2°) de mettre à la charge du Conseil national de l'ordre des médecins la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Benjamin de Maillard, auditeur,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat de Mme C...et à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que MmeC..., médecin spécialiste en gastro-entérologie et hépatologie, a suivi Mme A..., qui lui avait été adressée par son médecin traitant, comme patiente à compter du 1er décembre 2009 ; que le 9 juillet 2010, Mme A...a consulté un autre médecin qui, pour déterminer l'origine de différentes affections dont elle souffrait, a fait réaliser des examens qui ont conduit à diagnostiquer qu'elle était atteinte d'un cancer du pancréas, dont elle est décédée peu après ; que le 9 décembre 2010, M.A..., fils de la patiente décédée, a adressé au conseil départemental de l'Aube de l'ordre des médecins une plainte contre MmeC..., que ce conseil départemental a transmise à la chambre disciplinaire de première instance de Champagne-Ardenne sans s'y associer ; que, par une décision du 24 avril 2012, la chambre disciplinaire de première instance a infligé à Mme C...la sanction d'interdiction du droit d'exercer la médecine pendant six mois ; que, sur appels de l'intéressée et du Conseil national de l'ordre des médecins, la chambre disciplinaire nationale de l'ordre des médecins a, par une décision du 5 mai 2014, porté la sanction d'interdiction d'exercice à un an ; que Mme C... se pourvoit en cassation contre cette décision ;<br/>
<br/>
              2. Considérant que la procédure devant les chambres disciplinaires de l'ordre des médecins est essentiellement écrite ; que, toutefois, il résulte de l'article R. 4126-29 du code de la santé publique que toute personne convoquée à l'audience y est entendue, même lorsqu'elle n'a pas produit d'observations écrites avant la clôture de l'instruction ; qu'il ressort des pièces de la procédure devant la chambre disciplinaire nationale de l'ordre des médecins que le représentant du conseil départemental de l'Aube, qui n'avait pas produit au cours de l'instruction, a présenté des observations orales au cours de l'audience publique ; que le respect, d'une part, du caractère contradictoire de la procédure et des droits du praticien poursuivi, d'autre part, du caractère essentiellement écrit de la procédure, imposait non seulement que la chambre disciplinaire nationale ne tienne pas compte de circonstances de fait ou d'éléments de droit exposés par la personne entendue dont il n'aurait pas été fait état par écrit avant la clôture de l'instruction sans rouvrir celle-ci et les soumettre au débat contradictoire écrit, mais aussi que, si les propos du représentant du conseil départemental  étaient d'une nature telle qu'ils étaient susceptibles d'exercer une influence sur la décision de la juridiction disciplinaire et qu'il ne pouvait  utilement y être répondu pendant l'audience, l'affaire soit rayée du rôle et l'instruction rouverte ; <br/>
<br/>
              3. Considérant qu'il ressort toutefois des pièces de la procédure devant la chambre disciplinaire nationale de l'ordre des médecins que le représentant du conseil départemental de l'Aube s'est borné à indiquer que si le conseil départemental n'avait rien, à l'époque de la plainte, à reprocher à MmeC..., celle-ci faisait l'objet d'une attention particulière parce qu'elle avait la réputation de recourir à la médecine non conventionnelle ; que ces propos, d'une part, n'apportaient pas de circonstances de fait ou d'éléments de droit nouveaux dont la chambre disciplinaire nationale aurait tenu compte, d'autre part, ne peuvent être regardés comme d'une nature telle qu'ils étaient susceptibles d'exercer une influence sur la décision de la juridiction disciplinaire et que Mme C...n'était pas en mesure d'y répondre utilement pendant l'audience ; que, par suite, la requérante n'est pas fondée à soutenir que la décision est intervenue à l'issue d'une procédure irrégulière ;<br/>
<br/>
              4. Considérant qu'en relevant, pour écarter le moyen tiré de ce que certains symptômes de la patiente avaient masqué l'affection dont elle était atteinte, que Mme C... n'avait pas conduit les investigations approfondies qui auraient permis de poser dans un délai raisonnable le diagnostic pertinent, la chambre disciplinaire nationale a suffisamment motivé sa décision ; <br/>
<br/>
              5. Considérant qu'en reprochant à Mme C...d'avoir prescrit à sa patiente de suivre un " prétendu régime d'amaigrissement dit Seignalet ", la chambre disciplinaire nationale a, par une motivation suffisante, porté sur les faits qui lui étaient soumis une appréciation souveraine exempte de dénaturation ;<br/>
<br/>
              6. Considérant qu'aux termes de l'article R. 4127-13 du code de la santé publique : " Lorsque le médecin participe à une action d'information du public de caractère éducatif et sanitaire, quel qu'en soit le moyen de diffusion, il doit ne faire état que de données confirmées, faire preuve de prudence et avoir le souci des répercussions de ses propos auprès du public. Il doit se garder à cette occasion de toute attitude publicitaire, soit personnelle, soit en faveur des organismes où il exerce ou auxquels il prête son concours, soit en faveur d'une cause qui ne soit pas d'intérêt général " ; qu'en jugeant, après avoir relevé que Mme C...faisait état sur des sites internet d'un traitement par " biorésonance " qu'elle pratiquait à Londres ainsi que des mérites supposés de cette méthode non éprouvée dont elle se portait garante, qu'elle s'était ainsi rendue coupable d'user de procédés à caractère publicitaire contraires aux dispositions du code de la santé publique citées ci-dessus, la chambre disciplinaire nationale, qui a suffisamment motivé sa décision, n'a pas donné aux faits ainsi énoncés une qualification juridique erronée ;<br/>
<br/>
              7. Considérant que si le choix de la sanction relève de l'appréciation des juges du fond au vu de l'ensemble des circonstances de l'espèce, il appartient au juge de cassation de vérifier que la sanction retenue n'est pas hors de proportion avec la faute commise et qu'elle a pu dès lors être légalement prise ; que la chambre disciplinaire nationale a pu légalement estimer que les manquements aux exigences déontologiques qu'elle a retenus justifiaient, eu égard à leur gravité, la sanction de l'interdiction d'exercer la médecine pendant une durée d'un an ; <br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que Mme C...n'est pas fondée à demander l'annulation de la décision attaquée ;<br/>
<br/>
              9. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du Conseil national de l'ordre des médecins qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
Article 1er : Le pourvoi de Mme C...est rejeté. <br/>
Article 2 : La présente décision sera notifiée à Mme B... C...et au Conseil national de l'ordre des médecins.<br/>
Copie en sera adressée à M. D... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-06-02 PROCÉDURE. JUGEMENTS. TENUE DES AUDIENCES. - JURIDICTIONS DISCIPLINAIRES DE L'ORDRE DES MÉDECINS - PRISE DE PAROLE D'UNE PERSONNE QUI N'A PAS PRODUIT PAR ÉCRIT - LÉGALITÉ - CONDITIONS [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">55-04-01-02 PROFESSIONS, CHARGES ET OFFICES. DISCIPLINE PROFESSIONNELLE. PROCÉDURE DEVANT LES JURIDICTIONS ORDINALES. JUGEMENTS. - AUDIENCE - PRISE DE PAROLE D'UNE PERSONNE QUI N'A PAS PRODUIT PAR ÉCRIT - LÉGALITÉ - CONDITIONS [RJ1].
</SCT>
<ANA ID="9A"> 54-06-02 La procédure devant les chambres disciplinaires de l'ordre des médecins est essentiellement écrite. Toutefois, il résulte de l'article R. 4126-29 du code de la santé publique que toute personne convoquée à l'audience y est entendue, même lorsqu'elle n'a pas produit d'observations écrites avant la clôture de l'instruction. Lorsque tel est le cas, le respect, d'une part, du caractère contradictoire de la procédure et des droits du praticien poursuivi, d'autre part, du caractère essentiellement écrit de la procédure, impose non seulement que la chambre disciplinaire nationale ne tienne pas compte de circonstances de fait ou d'éléments de droit exposés par la personne entendue dont il n'aurait pas été fait état par écrit avant la clôture de l'instruction sans rouvrir celle-ci et les soumettre au débat contradictoire écrit, mais aussi que, si les propos tenus par la personne n'ayant pas produit d'observations écrites sont d'une nature telle qu'ils sont susceptibles d'exercer une influence sur la décision de la juridiction disciplinaire et qu'il ne peut utilement y être répondu pendant l'audience, l'affaire soit rayée du rôle et l'instruction rouverte.</ANA>
<ANA ID="9B"> 55-04-01-02 La procédure devant les chambres disciplinaires de l'ordre des médecins est essentiellement écrite. Toutefois, il résulte de l'article R. 4126-29 du code de la santé publique que toute personne convoquée à l'audience y est entendue, même lorsqu'elle n'a pas produit d'observations écrites avant la clôture de l'instruction. Lorsque tel est le cas, le respect, d'une part, du caractère contradictoire de la procédure et des droits du praticien poursuivi, d'autre part, du caractère essentiellement écrit de la procédure, impose non seulement que la chambre disciplinaire nationale ne tienne pas compte de circonstances de fait ou d'éléments de droit exposés par la personne entendue dont il n'aurait pas été fait état par écrit avant la clôture de l'instruction sans rouvrir celle-ci et les soumettre au débat contradictoire écrit, mais aussi que, si les propos tenus par la personne n'ayant pas produit d'observations écrites sont d'une nature telle qu'ils sont susceptibles d'exercer une influence sur la décision de la juridiction disciplinaire et qu'il ne peut utilement y être répondu pendant l'audience, l'affaire soit rayée du rôle et l'instruction rouverte.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf., sur le caractère écrit de la procédure devant ces juridictions, CE, 27 avril 1966, Sieur Dionnet, n° 64489, p. 290 ; CE, 25 janvier 1980, M. Gras, n° 07646, p. 50. Comp., en matière électorale, CE, 21 août 1996, Elections municipales de Noyon, n° 176927, T. p. 1100.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
