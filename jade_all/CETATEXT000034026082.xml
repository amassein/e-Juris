<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034026082</ID>
<ANCIEN_ID>JG_L_2017_02_000000393720</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/02/60/CETATEXT000034026082.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 10/02/2017, 393720</TITRE>
<DATE_DEC>2017-02-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393720</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Thomas Odinot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:393720.20170210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une décision du 24 février 2016, la Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi de la société Bancel dirigées contre l'arrêt n° 14NC02239 du 23 juillet 2015 de la cour administrative d'appel de Nancy en tant seulement qu'il a rejeté ses conclusions tendant à l'indemnisation des frais de présentation de son offre.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Odinot, auditeur,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la société Bancel et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de l'Etablissement public d'hébergement pour personnes agées dépendantes d'Audincourt.<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que l'EHPAD d'Audincourt a lancé en 2009 une procédure d'appel d'offres ouvert pour la passation d'un marché de construction d'une maison de retraite ; que la société Bancel a présenté une offre pour l'attribution du lot n° 2 " gros oeuvre " de ce marché et a été informée, le 3 août 2009, du rejet de son offre ; que par un jugement du 13 novembre 2011, le tribunal administratif de Besançon a condamné l'EHPAD d'Audincourt à verser à la société Bancel une somme de 3 000 euros au titre de l'indemnisation des frais qu'elle a engagés pour la présentation de son offre et a rejeté le surplus des conclusions de la société aux fins d'annulation du marché et d'indemnisation du préjudice subi ; que, par un arrêt du 17 décembre 2012, la cour administrative d'appel de Nancy a rejeté les conclusions d'appel principal de la société Bancel demandant l'annulation du jugement en tant qu'il n'a que partiellement fait droit à sa demande ainsi que les conclusions d'appel incident de l'EHPAD demandant l'annulation du jugement en tant qu'il l'a condamné à verser la somme de 3 000 euros susmentionnée ; que le Conseil d'Etat a, par une décision du 3 décembre 2014, annulé l'arrêt en tant qu'il a statué sur les conclusions indemnitaires de la société Bancel ; que, par l'arrêt attaqué, la cour administrative d'appel de Nancy, saisie comme juge du renvoi, a annulé l'article 1er du jugement du tribunal administratif de Besançon du 13 janvier 2011 portant condamnation de l'EHPAD d'Audincourt à indemniser la société Bancel des frais qu'elle a engagés pour la présentation de son offre à hauteur de 3 000 euros et a rejeté la demande présentée par la société Bancel devant le tribunal administratif de Besançon tendant à la condamnation de l'EHPAD à l'indemniser du préjudice subi du fait de son éviction ainsi que les conclusions de sa requête d'appel ayant le même objet ; que, par une décision du 24 février 2016, le Conseil d'Etat statuant au contentieux a admis les conclusions du pourvoi de la société Bancel dirigées contre cet arrêt en tant seulement qu'il a rejeté ses conclusions tendant à l'indemnisation des frais de présentation de son offre ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 50 du code des marchés publics : " Lorsque le pouvoir adjudicateur se fonde sur plusieurs critères pour attribuer le marché, il peut autoriser les candidats à présenter des variantes. / Le pouvoir adjudicateur indique dans l'avis d'appel public à la concurrence ou dans les documents de la consultation s'il autorise ou non les variantes ; à défaut d'indication, les variantes ne sont pas admises. / Les documents de la consultation mentionnent les exigences minimales que les variantes doivent respecter ainsi que les modalités de leur présentation. Seules les variantes répondant à ces exigences minimales peuvent être prises en considération. / Les variantes sont proposées avec l'offre de base. / Pour les marchés de fournitures ou de services, une variante ne peut être rejetée au seul motif qu'elle aboutirait, si elle était retenue, respectivement soit à un marché de services au lieu d'un marché de fournitures, soit à un marché de fournitures au lieu d'un marché de services " ;<br/>
<br/>
              3. Considérant, en premier lieu, que la cour administrative d'appel de Nancy, après avoir  relevé que l'absence d'encadrement des modalités de présentation des variantes dans les documents de la consultation a constitué une méconnaissance des dispositions de l'article 50 du code des marchés publics, a estimé, par une appréciation souveraine exempte de dénaturation, que cette irrégularité n'a affecté ni la sélection des candidatures, ni le choix de l'offre économiquement la plus avantageuse, dès lors que les entreprises candidates n'ont pas présenté de variante ; <br/>
<br/>
              4. Considérant, en second lieu, que lorsqu'un candidat à l'attribution d'un contrat public demande la réparation du préjudice qu'il estime avoir subi du fait de l'irrégularité ayant, selon lui, affecté la procédure ayant conduit à son éviction, il appartient au juge, si cette irrégularité est établie, de vérifier qu'il existe un lien direct de causalité entre la faute en résultant et les préjudices dont le candidat demande l'indemnisation ; qu'il s'en suit que lorsque l'irrégularité ayant affectée la procédure de passation n'a pas été la cause directe de l'éviction du candidat, il n'y a pas de lien direct de causalité entre la faute résultant de l'irrégularité et les préjudices invoqués par le requérant à raison de son éviction ; que sa demande de réparation des préjudices allégués ne peut alors qu'être rejetée ; <br/>
<br/>
              5. Considérant qu'il ressort des énonciations de l'arrêt que la cour a rejeté les conclusions de la requérante tendant à la condamnation de l'EHPAD à lui verser une indemnité au titre des frais engagés pour la présentation de son offre au motif, alors même qu'elle n'était pas dépourvue de toute chance de remporter le marché, que la méconnaissance des dispositions de l'article 50 du code des marchés publics n'avait affecté ni la sélection des candidatures, ni le choix de l'offre économiquement la plus avantageuse ; que, ce faisant, il résulte de ce qui a été dit au point 4 qu'elle n'a pas commis d'erreur de droit dès lors que l'irrégularité ayant affectée la procédure n'a pas été la cause de l'éviction de la société Bancel et, qu'ainsi, il n'y avait pas de lien direct de causalité entre l'irrégularité de la procédure et le préjudice dont la société demandait réparation ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que les conclusions du pourvoi de la société Bancel dirigées contre l'arrêt attaqué en tant qu'il a rejeté ses conclusions tendant à l'indemnisation des frais de présentation de son offre doivent être rejetées ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'EHPAD d'Audincourt qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Bancel la somme de 3 000 euros à verser à l'EHPAD d'Audincourt au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions du pourvoi de la société Bancel dirigées contre l'arrêt attaqué en tant qu'il a rejeté ses conclusions tendant à l'indemnisation des frais de présentation de son offre sont rejetées.<br/>
Article 2 : La société Bancel versera à l'Etablissement public d'hébergement pour personnes âgées dépendantes d'Audincourt une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Bancel et à l'Etablissement public d'hébergement pour personnes âgées dépendantes d'Audincourt.<br/>
Copie en sera adressée à la société Demathieu et Bard.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-08-03 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS ET OBLIGATIONS DU JUGE. - DEMANDE INDEMNITAIRE DU CANDIDAT ÉVINCÉ À L'ISSUE D'UNE PROCÉDURE IRRÉGULIÈRE [RJ1] - CAS OÙ L'IRRÉGULARITÉ N'A PAS ÉTÉ LA CAUSE DIRECTE DE L'ÉVICTION DU CANDIDAT  - ABSENCE DE DROIT À INDEMNITÉ - CAS D'ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-04-01-03-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. PRÉJUDICE. CARACTÈRE DIRECT DU PRÉJUDICE. ABSENCE. - DEMANDE INDEMNITAIRE DU CANDIDAT ÉVINCÉ À L'ISSUE D'UNE PROCÉDURE IRRÉGULIÈRE [RJ1] - CAS OÙ L'IRRÉGULARITÉ N'A PAS ÉTÉ LA CAUSE DIRECTE DE L'ÉVICTION DU CANDIDAT - ABSENCE DE DROIT À INDEMNITÉ - CAS D'ESPÈCE.
</SCT>
<ANA ID="9A"> 39-08-03 Candidat évincé demandant à être indemnisé des frais de présentation de son offre du fait de l'irrégularité de la procédure d'attribution.... ,,Lorsqu'un candidat à l'attribution d'un contrat public demande la réparation du préjudice qu'il estime avoir subi du fait de l'irrégularité ayant, selon lui, affecté la procédure ayant conduit à son éviction, il appartient au juge, si cette irrégularité est établie, de vérifier qu'il existe un lien direct de causalité entre la faute en résultant et les préjudices dont le candidat demande l'indemnisation. Il s'en suit que lorsque l'irrégularité ayant affecté la procédure de passation n'a pas été la cause directe de l'éviction du candidat, il n'y a pas de lien direct de causalité entre la faute résultant de l'irrégularité et les préjudices invoqués par le requérant à raison de son éviction. Sa demande de réparation des préjudices allégués ne peut alors qu'être rejetée.,,,En l'espèce, alors même que la société n'était pas dépourvue de toute chance de remporter le marché, l'absence d'encadrement des modalités de présentation des variantes dans les documents de la consultation, qui méconnaissait l'article 50 du code des marchés publics, n'a affecté ni la sélection des candidatures, ni le choix de l'offre économiquement la plus avantageuse, dès lors que les entreprises candidates n'ont pas présenté de variante.</ANA>
<ANA ID="9B"> 60-04-01-03-01 Candidat évincé demandant à être indemnisé des frais de présentation de son offre du fait de l'irrégularité de la procédure d'attribution.... ,,Lorsqu'un candidat à l'attribution d'un contrat public demande la réparation du préjudice qu'il estime avoir subi du fait de l'irrégularité ayant, selon lui, affecté la procédure ayant conduit à son éviction, il appartient au juge, si cette irrégularité est établie, de vérifier qu'il existe un lien direct de causalité entre la faute en résultant et les préjudices dont le candidat demande l'indemnisation. Il s'en suit que lorsque l'irrégularité ayant affecté la procédure de passation n'a pas été la cause directe de l'éviction du candidat, il n'y a pas de lien direct de causalité entre la faute résultant de l'irrégularité et les préjudices invoqués par le requérant à raison de son éviction. Sa demande de réparation des préjudices allégués ne peut alors qu'être rejetée.,,,En l'espèce, alors même que la société n'était pas dépourvue de toute chance de remporter le marché,  l'absence d'encadrement des modalités de présentation des variantes dans les documents de la consultation, qui méconnaissait l'article 50 du code des marchés publics, n'a affecté ni la sélection des candidatures, ni le choix de l'offre économiquement la plus avantageuse, dès lors que les entreprises candidates n'ont pas présenté de variante.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., en précisant, CE, 18 juin 2003, Groupement d'entreprises solidaires ETPO Guadeloupe, Société Biwater et Société Aqua TP, n° 249630, T. pp. 865-909 ; CE, 10 juillet 2013, Compagnie martiniquaise de transports, n° 362777, T. pp. 699-837.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
