<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041549025</ID>
<ANCIEN_ID>JG_L_2020_02_000000428919</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/54/90/CETATEXT000041549025.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 07/02/2020, 428919</TITRE>
<DATE_DEC>2020-02-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428919</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Thomas Janicot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:428919.20200207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La communauté de communes Coeur d'Ostrevent a demandé au juge des référés du tribunal administratif de Lille de suspendre l'exécution de l'arrêté du 22 décembre 2018 par lequel le préfet du Nord a autorisé le retrait de la commune d'Emerchicourt de la communauté de communes Coeur d'Ostrevent en vue de son adhésion à la communauté d'agglomération de la Porte du Hainaut. <br/>
<br/>
              Par une ordonnance n° 1900927 du 28 février 2019, le juge des référés du tribunal administratif de Lille a rejeté sa demande.  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux nouveaux mémoires, enregistrés les 18 mars, 3 avril, 13 mai et 9 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, la communauté de communes Coeur d'Ostrevent demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande de suspension ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Janicot, auditeur,<br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de la communauté de communes Coeur d'Ostrevent ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que par une délibération de son conseil municipal du 16 janvier 2015, la commune d'Emerchicourt a décidé de se retirer de la communauté de communes Coeur d'Ostrevent (CCCO) et d'adhérer à la communauté d'agglomération de la Porte du Hainaut (CAPH). Par une délibération du 9 février 2015, le conseil communautaire de cette communauté d'agglomération a accepté la demande d'adhésion de la commune. Par un avis du 23 octobre 2015, la commission départementale de coopération intercommunale s'est prononcée en faveur du retrait de la commune d'Emerchicourt de la communauté de communes et de son adhésion à la communauté d'agglomération. Par un arrêté du 22 décembre 2018 pris sur le fondement de l'article L. 5214-26 du code général des collectivités territoriales, le préfet du Nord a autorisé le retrait de la commune d'Emerchicourt de la communauté de communes Coeur d'Ostrevent à compter du 1er janvier 2019, en vue de son adhésion à la communauté d'agglomération de la Porte du Hainaut. La communauté de communes a demandé au juge des référés du tribunal administratif de Lille, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de cet arrêté. Par une ordonnance du 28 février 2019, le juge des référés a rejeté cette demande de suspension. La communauté de communes Coeur d'Ostrevent se pourvoit en cassation contre cette ordonnance. <br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              3. Aux termes de l'article L. 5211-19 du code général des collectivités territoriales : " Une commune peut se retirer de l'établissement public de coopération intercommunale, sauf s'il s'agit d'une communauté urbaine ou d'une métropole, dans les conditions prévues à l'article L. 5211-25-1, avec le consentement de l'organe délibérant de l'établissement. A défaut d'accord entre l'organe délibérant de l'établissement public de coopération intercommunale et le conseil municipal concerné sur la répartition des biens ou du produit de leur réalisation et du solde de l'encours de la dette visés au 2° de l'article L. 5211-25-1, cette répartition est fixée par arrêté du ou des représentants de l'Etat dans le ou les départements concerné (...) ". Aux termes de l'article L. 5214-26 du même code : " Par dérogation à l'article L. 5211-19, une commune peut être autorisée, par le représentant de l'Etat dans le département après avis de la commission départementale de la coopération intercommunale réunie dans la formation prévue au second alinéa de l'article L. 5211-45, à se retirer d'une communauté de communes pour adhérer à un autre établissement public de coopération intercommunale à fiscalité propre dont le conseil communautaire a accepté la demande d'adhésion (...) ".<br/>
<br/>
              4. L'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier objectivement et concrètement, compte tenu des justifications fournies par le requérant et de l'ensemble des circonstances de chaque espèce, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. <br/>
<br/>
              5. En premier lieu, si la condition d'urgence à laquelle est subordonné le prononcé d'une mesure de suspension en application de l'article L. 521-1 du code de justice administrative doit être regardée comme étant, en principe, remplie lorsqu'un arrêté préfectoral a pour objet de modifier la répartition des compétences entre une collectivité territoriale et un groupement de collectivités territoriales ou entre deux groupements de collectivités territoriales, tel n'est pas le cas s'agissant de l'exécution d'un arrêté préfectoral pris sur le fondement de l'article L. 5214-26 du code général des collectivités territoriales, autorisant une commune à se retirer d'une communauté de communes pour adhérer à un autre établissement public de coopération intercommunale, lequel emporte seulement modification du périmètre géographique de la communauté de communes. Dès lors, en jugeant que la seule modification des limites territoriales de la communauté de communes Coeur d'Ostrevent découlant du retrait de la commune d'Emerchicourt ne saurait conduire à présumer satisfaite la condition d'urgence requise par l'article L. 521-1 du code de justice administrative et en appréciant concrètement, au vu de l'ensemble des circonstances de l'affaire, si cette condition était remplie, le juge des référés du tribunal administratif de Lille n'a ni dénaturé les faits qui lui étaient soumis, ni commis d'erreur de droit.<br/>
<br/>
              6. En second lieu, pour juger que la condition d'urgence n'était pas remplie, le juge des référés a relevé, d'une part, que le retrait de la commune d'Emerchicourt de la communauté de communes Coeur d'Ostrevent induisait, pour cette dernière, une perte de ressources financières de l'ordre de 1,5 million d'euros par an, soit 7,89 % de ses recettes de fonctionnement, mais aussi qu'elle impliquait une diminution annuelle des charges de la communauté d'environ 445 000 euros et qu'un protocole d'accord destiné à compenser une partie des pertes induites par le retrait de la commune d'Emerchicourt était en cours de discussion entre la communauté de communes Coeur d'Ostrevent et la communauté d'agglomération de la Porte du Hainaut. C'est sans dénaturer les faits de l'espèce que le juge des référés a jugé crédible la perspective d'une compensation financière apportée par la communauté d'agglomération, et sans erreur de droit, dans ces conditions, qu'il a tenu compte de l'existence d'un projet de convention en ce sens pour apprécier la gravité de l'atteinte portée à la situation de la communauté de communes.<br/>
<br/>
              7. D'autre part, c'est sans contradiction de motifs et sans dénaturation des faits de l'espèce que le juge des référés a relevé que si la communauté de communes requérante soutenait avoir investi environ 2,5 millions d'euros dans le projet de lotissement " Chemin d'Azincourt " sur des terrains situés dans la commune d'Emerchicourt, de sorte que le retrait de celle-ci la priverait des recettes issues de la future vente de ce lotissement, les éléments versés au dossier ne permettaient pas d'évaluer l'ampleur du préjudice financier allégué, et qu'il a relevé, par un motif en tout état de cause surabondant, que le préfet soutenait devant lui sans être contredit que ces terrains étaient la propriété de la seule communauté de communes. <br/>
<br/>
              8. Ainsi, en jugeant, au vu des éléments débattus devant lui, que la communauté de communes Coeur d'Ostrevent ne justifiait pas d'une atteinte suffisamment grave et immédiate à ses intérêts, constitutive d'une situation d'urgence justifiant l'usage, par le juge des référés, des pouvoirs qu'il tient de l'article L. 521-1 du code de justice administrative, le juge des référés a porté sur les faits de l'espèce une appréciation souveraine qui est exempte de dénaturation.<br/>
<br/>
              9. Il résulte de tout ce qui précède que le pourvoi de la communauté de communes Coeur d'Ostrevent doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de la communauté de communes Coeur d'Ostrevent est rejeté.  <br/>
<br/>
Article 2 : La présente décision sera notifiée à la communauté de communes Coeur d'Ostrevent et à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-05-01-01 COLLECTIVITÉS TERRITORIALES. COOPÉRATION. ÉTABLISSEMENTS PUBLICS DE COOPÉRATION INTERCOMMUNALE - QUESTIONS GÉNÉRALES. DISPOSITIONS GÉNÉRALES ET QUESTIONS COMMUNES. - ARRÊTÉ PRÉFECTORAL AUTORISANT UNE COMMUNE À SE RETIRER D'UN EPCI (ART. L. 5214-26 DU CGCT) - RÉFÉRÉ-SUSPENSION (ART. L. 521-1 DU CJA) - CONDITION D'URGENCE - PRÉSOMPTION - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-035-02-03-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CODE DE JUSTICE ADMINISTRATIVE). CONDITIONS D'OCTROI DE LA SUSPENSION DEMANDÉE. URGENCE. - PRÉSOMPTION - ABSENCE - DEMANDE DE SUSPENSION D'UN ARRÊTÉ PRÉFECTORAL AUTORISANT UNE COMMUNE À SE RETIRER D'UN EPCI (ART. L. 5214-26 DU CGCT) [RJ1].
</SCT>
<ANA ID="9A"> 135-05-01-01 Si la condition d'urgence à laquelle est subordonné le prononcé d'une mesure de suspension en application de l'article L. 521-1 du code de justice administrative (CJA) doit être regardée comme étant, en principe, remplie lorsqu'un arrêté préfectoral a pour objet de modifier la répartition des compétences entre une collectivité territoriale et un groupement de collectivités territoriales ou entre deux groupements de collectivités territoriales, tel n'est pas le cas s'agissant de l'exécution d'un arrêté préfectoral pris sur le fondement de l'article L. 5214-26 du code général des collectivités territoriales (CGCT), autorisant une commune à se retirer d'une communauté de communes pour adhérer à un autre établissement public de coopération intercommunale (EPCI), lequel emporte seulement modification du périmètre géographique de la communauté de communes.</ANA>
<ANA ID="9B"> 54-035-02-03-02 Si la condition d'urgence à laquelle est subordonné le prononcé d'une mesure de suspension en application de l'article L. 521-1 du code de justice administrative (CJA) doit être regardée comme étant, en principe, remplie lorsqu'un arrêté préfectoral a pour objet de modifier la répartition des compétences entre une collectivité territoriale et un groupement de collectivités territoriales ou entre deux groupements de collectivités territoriales, tel n'est pas le cas s'agissant de l'exécution d'un arrêté préfectoral pris sur le fondement de l'article L. 5214-26 du code général des collectivités territoriales (CGCT), autorisant une commune à se retirer d'une communauté de communes pour adhérer à un autre établissement public de coopération intercommunale (EPCI), lequel emporte seulement modification du périmètre géographique de la communauté de communes.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp. s'agissant d'un arrêté ayant pour objet de modifier la répartition des compétences entre une collectivité territoriale et un EPCI ou entre deux EPCI, CE, 30 décembre 2009, Syndicat intercommunal à vocation unique de gestion du centre social intercommunal rural, n° 328184, T. p. 893 ; CE, 17 mars 2017, Ministre de l'intérieur c/ Communauté de communes du Cordais et du Causse, n° 404891, T. pp. 489-735.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
