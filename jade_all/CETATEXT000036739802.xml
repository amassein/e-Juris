<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036739802</ID>
<ANCIEN_ID>JG_L_2018_03_000000416600</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/73/98/CETATEXT000036739802.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 23/03/2018, 416600, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-03-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416600</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Rectif. d'erreur matérielle</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Louise Bréhier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:416600.20180323</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              La société Hochtief Solutions AG a demandé au tribunal administratif de Strasbourg d'annuler la décision de résiliation du marché passé avec les Hôpitaux universitaires de Strasbourg pour la construction du nouvel hôpital civil de Strasbourg, de prononcer la nullité du contrat et de l'indemniser des dépenses utiles, ou, à titre subsidiaire, de condamner les Hôpitaux universitaires de Strasbourg à lui verser une indemnité de 57 012 556,78 euros (HT) au titre du solde du marché. <br/>
<br/>
              Par un jugement n° 0605349, 0800854 du 30 juin 2011, le tribunal administratif de Strasbourg a, d'une part, arrêté le décompte général du marché dont la société était titulaire à la somme de 12 602 475,70 euros à la charge de la société et, d'autre part, rejeté les conclusions de celle-ci.<br/>
<br/>
              Par un arrêt n° 11NC01445 du 18 novembre 2013, rectifié par un arrêt du 9 octobre 2014, la cour administrative d'appel de Nancy a arrêté le décompte du marché dont la société Hochtief Solutions AG était titulaire à la somme de 375 969,50 euros à la charge de la société.<br/>
<br/>
              Par une décision n° 376465 du 9 décembre 2015, le Conseil d'Etat statuant au contentieux, saisi d'un pourvoi par la société Hochtief Solutions AG, a annulé cet arrêt en tant qu'il a statué, d'une part, sur le montant des travaux supplémentaires portant sur la réalisation du bâtiment énergie et ayant fait l'objet du projet d'avenant n° 5, d'autre part, sur les surcoûts liés aux prolongations des délais par ordre de service du 15 juillet 2005, et renvoyé l'affaire dans cette mesure à la cour administrative d'appel de Nancy.<br/>
<br/>
              Par un nouvel arrêt n° 15NC02571 du 24 novembre 2016, la cour administrative d'appel de Nancy a arrêté le décompte du marché dont la société Hochtief Solutions AG était titulaire à la somme de 340 089,50 euros à la charge de la société.<br/>
<br/>
              Par une décision n° 409219 du 16 octobre 2017, le Conseil d'Etat statuant au contentieux, saisi d'un nouveau pourvoi par la société Hochtief Solutions AG a admis partiellement les conclusions dirigées contre ce dernier arrêt de la cour administrative d'appel de Nancy, en tant qu'il s'est prononcé sur les conclusions tendant au versement d'une somme de 57 000 euros au titre du poste n° 1 (" fondations spéciales ") du devis n° DTS 031 A du 6 avril 2004, et n'a pas admis le surplus des conclusions de ce pourvoi.<br/>
<br/>
              Recours en rectification d'erreur matérielle<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 18 décembre 2017 et 5 mars 2018 au secrétariat du contentieux du Conseil d'Etat, la société Hochtief Solutions AG demande au Conseil d'État : <br/>
<br/>
              1°) de rectifier pour erreur matérielle sa décision du 16 octobre 2017 en tant qu'elle n'a pas admis les conclusions de son pourvoi ; <br/>
<br/>
              2°) d'admettre les conclusions de son pourvoi qui n'ont pas été admises. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Louise Bréhier, auditrice,<br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de la société Hochtief solutions AG, et à la SCP Waquet, Farge, Hazan, avocat des Hôpitaux universitaires de Strasbourg ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes des dispositions de l'article R. 833-1 du code de justice administrative : " Lorsqu'une décision d'une cour administrative d'appel ou du Conseil d'Etat est entachée d'une erreur matérielle susceptible d'avoir exercé une influence sur le jugement de l'affaire, la partie intéressée peut introduire devant la juridiction qui a rendu la décision un recours en rectification " ; <br/>
<br/>
              2.	Considérant que le recours en rectification d'erreur matérielle n'est ainsi ouvert qu'en vue de corriger des erreurs de caractère matériel qui ne sont pas imputables aux parties et qui ont pu avoir une influence sur le sens de la décision ; que si l'omission de répondre à un moyen invoqué de manière distincte des autres moyens constitue une erreur matérielle susceptible d'être rectifiée par la voie du recours prévu à l'article R. 833-1 du code de justice administrative, les appréciations d'ordre juridique auxquelles se livre le Conseil d'Etat pour interpréter les moyens soulevés devant lui et pour décider de la façon d'y répondre ne sont pas susceptibles d'être remises en cause par la voie du recours en rectification d'erreur matérielle ;<br/>
<br/>
              3.	Considérant que, pour demander la rectification pour erreur matérielle de la décision du 16 octobre 2017 visée au dossier en tant qu'elle n'a pas admis toutes les conclusions de son pourvoi, la société Hochtief Solutions AG soutient que la décision aurait omis de se prononcer sur les moyens tirés de ce que l'arrêt attaqué était entaché de dénaturation dès lors que le calendrier " indice F " notifié en 2002 était différent du " calendrier F " notifié le 15 juillet 2005, de ce qu'il était entaché de dénaturation, d'erreur de droit et d'insuffisance de motivation en jugeant que la décision du 6 juin 2002 retenant le calendrier " indice D " n'avait fait l'objet d'aucun ordre de service et de ce que la cour administrative d'appel de Nancy avait entaché son arrêt de dénaturation en retenant qu'il n'était pas démontré que la décision du 6 juin 2002 était en lien avec des dépenses supplémentaires ; que, toutefois, la décision du 16 octobre 2017 se prononce sur les moyens tirés de ce que la cour administrative d'appel de Nancy avait dénaturé les faits de l'espèce et les pièces du dossier, commis une erreur de droit et insuffisamment motivé son arrêt en retenant que le calendrier des travaux était le calendrier indice " F " figurant dans l'ordre de service notifié le 6 juin 2002 ; que la contestation, présentée par la voie du recours en rectification d'erreur matérielle, ne conduit pas à réparer une omission matérielle mais revient à mettre en cause les appréciations d'ordre juridique auxquelles s'est livré le Conseil d'Etat en interprétant l'argumentation dont il était saisi et en décidant de la façon d'y répondre ;<br/>
<br/>
              4.	Considérant qu'il résulte de ce qui précède que le recours en rectification d'erreur matérielle présenté par la société Hochtief Solutions AG, qui ne satisfait pas aux conditions posées par l'article R. 833-1 du code de justice administrative, n'est pas recevable et ne peut qu'être rejeté ;<br/>
<br/>
              5.	Considérant qu'il y a lieu de mettre à la charge de la société Hochtief Solutions AG une somme de 3 000 euros à verser aux Hôpitaux universitaires de Strasbourg au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le recours en rectification d'erreur matérielle présenté par la société Hochtief Solutions AG est rejeté.<br/>
<br/>
Article 2 : La société Hochtief Solutions AG versera la somme de 3 000 euros aux Hôpitaux universitaires de Strasbourg au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 3 : La présente décision sera notifiée à la société Hochtief Solutions AG, aux Hôpitaux universitaires de Strasbourg et à la société Meyer Isolation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
