<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032750864</ID>
<ANCIEN_ID>JG_L_2016_06_000000393234</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/75/08/CETATEXT000032750864.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 22/06/2016, 393234, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-06-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393234</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:393234.20160622</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B... A...a demandé au tribunal administratif de Melun d'annuler plusieurs décisions du ministre de l'intérieur portant retrait de points de son permis de conduire ainsi que la décision 48 SI du 7 février 2014 constatant la perte de validité de ce permis de conduire. Par un jugement n° 1402733 du 2 juillet 2015, le tribunal administratif a fait partiellement droit à ces conclusions.<br/>
<br/>
              Par un pourvoi, enregistré le 7 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les conclusions de M. A... devant le tribunal administratif.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de procédure pénale ;<br/>
<br/>
              - le code de la route ; <br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A..., à la suite d'infractions au code de la route commises entre le 16 février 2007 et le 30 octobre 2013, s'est vu notifier une décision 48 SI du 7 février 2014 par laquelle le ministre de l'intérieur a constaté l'invalidité de son permis de conduire et lui a enjoint de le restituer ; que M. A... a demandé au tribunal administratif de Melun d'annuler l'ensemble de ces décisions ; que le ministre de l'intérieur se pourvoit en cassation contre le jugement du 2 juillet 2015 par lequel ce tribunal a annulé les décisions retirant des points sur le permis de conduire de l'intéressé à la suite des infractions commises les 16 février 2007, 9 mai 2010 et 18 septembre 2011 ainsi que la décision 48 SI précitée du 7 février 2014, et rejeté le surplus des conclusions de sa demande ;<br/>
<br/>
              En ce qui concerne les décisions de retraits de points consécutives aux infractions commises les 16 février 2007 et 9 mai 2010 :<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 223-1 du code de la route : " Le permis de conduire est affecté d'un nombre de points. Celui-ci est réduit de plein droit si le titulaire du permis a commis une infraction pour laquelle cette réduction est prévue. / (...) La réalité d'une infraction entraînant retrait de points est établie par le paiement d'une amende forfaitaire ou l'émission du titre exécutoire de l'amende forfaitaire majorée, l'exécution d'une composition pénale ou par une condamnation définitive (...) " ; que la délivrance, au titulaire du permis de conduire à l'encontre duquel est relevée une infraction donnant lieu à retrait de points, de l'information prévue aux articles L. 223-3 et R. 223-3 du code de la route constitue une garantie essentielle donnée à l'auteur de l'infraction pour lui permettre, avant d'en reconnaître la réalité par le paiement d'une amende forfaitaire ou l'exécution d'une composition pénale, d'en mesurer les conséquences sur la validité de son permis et éventuellement d'en contester la réalité devant le juge pénal ; qu'elle revêt le caractère d'une formalité substantielle et conditionne la régularité de la procédure au terme de laquelle le retrait de points est décidé ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que les infractions commises les 16 février 2007 et 9 mai 2010, qui ont été constatées avec interception du véhicule, ont donné lieu chacune à l'établissement d'un procès-verbal de police mentionnant, d'une part, la nature de l'infraction et les dispositions du code de la route la réprimant, et, d'autre part, le fait que cette infraction entraînait retrait de points ; que M. A... a apposé sa signature sur chacun de ces procès-verbaux, sous la mention  selon laquelle il reconnaissait avoir reçu une carte de paiement et un avis de contravention, lesquels sont réputés en application des articles 37 à 37-4 du code de procédure pénale comporter les informations prévues aux articles L. 223-3 et R. 223-3 du code de la route ; que la production des procès-verbaux ainsi signés établit suffisamment que l'intéressé a bénéficié de ces informations ; que s'il résulte des dispositions de l'article R. 49-2 du code de procédure pénale qu'en cas de paiement de l'amende forfaitaire entre les mains de l'agent verbalisateur le contrevenant se voit remettre une quittance de paiement, dont le modèle comporte les informations exigées par la loi, la circonstance que le relevé d'information intégral mentionne la même date pour la constatation de l'infraction et le paiement de l'amende n'est pas, à elle seule, de nature à priver de sa valeur probante un procès-verbal revêtu de la signature du contrevenant attestant qu'il s'est vu remettre un avis de contravention et une carte de paiement comportant les informations en cause ; que dès lors, en jugeant que les retraits de points consécutifs aux infractions commises les 16 février 2007 et 9 mai 2010 étaient irréguliers dès lors que le ministre de l'intérieur ne produisait aucun document probant établissant la délivrance de ces informations avant le paiement de l'amende infligée en répression de ces contraventions, le tribunal administratif a entaché son jugement d'une erreur de droit ;<br/>
<br/>
              En ce qui concerne la décision de retrait de points consécutive à l'infraction commise le 18 septembre 2011 :<br/>
<br/>
              4. Considérant que les dispositions portant application des articles R. 49-1 et R. 49-10 du code de procédure pénale en vigueur à la date de l'infraction litigieuse, notamment celles des articles A. 37 à A. 37-4 de ce code, issues de l'arrêté du 5 octobre 1999 relatif aux formulaires utilisés pour la constatation et le paiement des contraventions soumises à la procédure de l'amende forfaitaire, prévoient que, lorsqu'une amende soumise à cette procédure est relevée avec interception du véhicule mais sans que l'amende soit payée immédiatement entre les mains de l'agent verbalisateur, ce dernier utilise un formulaire réunissant, en une même liasse autocopiante, le procès-verbal conservé par le service verbalisateur, une carte de paiement matériellement indispensable pour procéder au règlement de l'amende et l'avis de contravention, également remis au contrevenant pour servir de justificatif du paiement ultérieur, qui comporte une information suffisante au regard des exigences résultant des articles L. 223-3 et R. 223-3 du code de la route ; <br/>
<br/>
              5. Considérant que, dès lors, le titulaire d'un permis de conduire à l'encontre duquel une infraction au code de la route est relevée au moyen d'un formulaire conforme à ce modèle et dont il est établi, notamment par la mention qui en est faite au système national des permis de conduire, qu'il a payé l'amende forfaitaire correspondant à cette infraction a nécessairement reçu l'avis de contravention ; qu'eu égard aux mentions dont cet avis est réputé être revêtu, l'administration doit être regardée comme s'étant acquittée envers le titulaire du permis de son obligation de lui délivrer les informations requises préalablement au paiement de l'amende, à moins que l'intéressé, à qui il appartient à cette fin de produire l'avis qu'il a nécessairement reçu, ne démontre s'être vu remettre un avis inexact ou incomplet ;<br/>
<br/>
              6. Considérant qu'après avoir relevé que l'infraction commise par M. A... le 18 septembre 2011, constatée avec interception du véhicule, avait donné lieu au paiement ultérieur d'une amende forfaitaire, le tribunal administratif a relevé qu'il n'était pas établi que cette infraction ait été relevée au moyen d'un formulaire conforme aux dispositions mentionnées ci-dessus, de sorte que le paiement de l'amende n'était pas de nature à établir que l'intéressé avait bénéficié des informations requises ; qu'en statuant ainsi, alors que l'avis de contravention était réputé être revêtu des mentions relatives à ces informations, et que M. A... ne démontrait pas s'être vu remettre un formulaire inexact ou incomplet, le tribunal a entaché son jugement d'une erreur de droit ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que le ministre de l'intérieur est fondé à demander l'annulation du jugement attaqué ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 7 février 2014 du tribunal administratif de Melun est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée au tribunal administratif de Melun.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à M. B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
