<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042417983</ID>
<ANCIEN_ID>JG_L_2020_10_000000432966</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/41/79/CETATEXT000042417983.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 08/10/2020, 432966, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432966</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD</AVOCATS>
<RAPPORTEUR>M. Joachim Bendavid</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:432966.20201008</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 25 juillet, 12 août et 27 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, Mme B... A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 12 juin 2019 par laquelle le Conseil national de l'ordre des infirmiers a, sur son recours dirigé contre la décision du 29 mars 2019 du conseil régional de l'ordre des infirmiers du Grand Est, refusé de l'inscrire au tableau de cet ordre ;<br/>
<br/>
              2°) de mettre à la charge du Conseil national de l'ordre des infirmiers la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Joachim Bendavid, auditeur,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat de Mme A....<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier que Mme A..., infirmière diplômée d'Etat depuis 1985, a demandé son inscription au tableau de l'ordre des infirmiers le 27 novembre 2018. Par une décision du 25 février 2019, le conseil interdépartemental de l'ordre des infirmiers de l'Aube et de la Haute-Marne a refusé son inscription, au motif qu'elle ne remplissait pas la condition de moralité requise pour l'exercice de la profession. Mme A... demande l'annulation de la décision du 12 juin 2019 par laquelle la formation restreinte du Conseil national de l'ordre des infirmiers a, sur son recours exercé contre la décision du 29 mars 2019 du conseil régional du Grand Est ayant confirmé le rejet du conseil interdépartemental, refusé de l'inscrire au tableau de l'ordre.<br/>
<br/>
              2. Aux termes de l'article L. 4311-16 du code de la santé publique, applicable aux décisions par lesquelles, en application du II de l'article L. 4312-5 du même code, le Conseil national de l'ordre des infirmiers statue, sur recours hiérarchique, sur les demandes relatives à l'inscription au tableau de l'ordre : " Le conseil départemental de l'ordre des infirmiers refuse l'inscription au tableau de l'ordre si le demandeur ne remplit pas les conditions de compétence, de moralité et d'indépendance exigées pour l'exercice de la profession (...) ". <br/>
<br/>
              3. Il ressort des pièces du dossier que, pour refuser, par la décision attaquée du 12 juin 2019, l'inscription de Mme A... au tableau de l'ordre des infirmiers, la formation restreinte du Conseil national de l'ordre s'est fondée sur ce que l'intéressée s'était rendue coupable, avec son mari, en août 2010, d'une agression sexuelle sur la nièce de ce dernier, qui était alors une mineure de plus de 15 ans, que ces faits avaient donné lieu à une peine d'emprisonnement de deux ans et que, faute d'apporter des " garanties de nature à exclure que les faits pour lesquels elle a été condamnée ne sont pas susceptibles de se reproduire en d'autres circonstances ", elle ne remplissait pas, alors même que ces faits avaient eu lieu de manière isolée et en dehors de tout exercice professionnel, la condition de moralité requise par les dispositions citées ci-dessus de l'article L. 4311-16 du code de la santé publique.<br/>
<br/>
              4. Il ressort toutefois également des pièces du dossier que, pendant la dizaine d'années qui a suivi ces faits, l'intéressée a spontanément engagé une psychothérapie et suivi un processus de réinsertion professionnelle consistant à reprendre dès 2011 et à poursuivre ensuite sans interruption, grâce à une mesure d'aménagement de sa peine d'emprisonnement décidée en 2014 par le juge de l'application des peines du tribunal de grande instance de Troyes, une activité d'infirmière dans le cadre d'un contrat à durée indéterminée, au sein du service d'hospitalisation à domicile du centre hospitalier de Troyes, en se spécialisant dans le traitement de la douleur et l'accompagnement des patients en fin de vie. <br/>
<br/>
              5. Ainsi, malgré la gravité des faits reprochés à Mme A..., compte tenu de son attitude pendant la période  qui s'est écoulée entre leur commission et la date de la décision attaquée, la formation restreinte du Conseil national de l'ordre des infirmiers a, en estimant que l'intéressée ne remplissait pas la condition de moralité requise pour l'exercice de la profession d'infirmier, fait, dans les circonstances de l'espèce, une inexacte application des dispositions de l'article L. 4311-16 du code de la santé publique.<br/>
<br/>
              6. Par suite, sans qu'il soit besoin de se prononcer sur l'autre moyen de sa requête, Mme A... est fondée à demander l'annulation de la décision qu'elle attaque. <br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du Conseil national de l'ordre des infirmiers une somme de 3 000 euros à verser à Mme A... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du 12 juin 2019 de la formation restreinte du Conseil national de l'ordre des infirmiers est annulée.<br/>
<br/>
Article 2 : Le conseil national de l'ordre des infirmiers versera à Mme A... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à Mme B... A... et au Conseil national de l'ordre des infirmiers. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
