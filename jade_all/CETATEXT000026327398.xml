<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026327398</ID>
<ANCIEN_ID>JG_L_2012_08_000000361404</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/32/73/CETATEXT000026327398.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 27/08/2012, 361404, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-08-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361404</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Denis Piveteau</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2012:361404.20120827</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 27 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par le Groupe d'information et de soutien des immigrés (GISTI), dont le siège est 3, Villa Marcès à Paris (75011), représenté par son président en exercice, l'association Avocats pour la défense des droits des étrangers (ADDE), dont le siège est au bureau des associations de l'Ordre des avocats à la cour d'appel, 2-4, rue de Harley à Paris (75001), le Comité médical pour les exilés (COMEDE) dont le siège est 78, rue du Général Leclerc, au Kremlin-Bicêtre (94272), représenté par son président, la Ligue des droits de l'homme (LDH), dont le siège est 138, rue Marcadet à Paris (75018), représentée par son président en exercice, le Mouvement contre le racisme et pour l'amitié entre les peuples (MRAP), dont le siège est 43, boulevard Magenta à Paris (75010), représenté par sa co-présidente et représentante légale, et le Syndicat des avocats de France (SAF) dont le siège est 34, rue Saint Lazare à Paris (75009), représenté par sa présidente en exercice ; les requérants demandent au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de la décision du ministre de l'intérieur prescrivant au préfet de Mayotte de ne pas appliquer les dispositions de la circulaire NOR INT/K/12/07283/C du 6 juillet 2012 aux familles avec enfants en instance d'éloignement à Mayotte et, par suite, de les placer systématiquement en rétention administrative ;<br/>
<br/>
              2°) d'enjoindre au ministre de l'intérieur d'adopter des instructions prohibant la rétention des familles avec enfants ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 000 euros pour l'ensemble des requérants au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              ils soutiennent que :<br/>
              - l'urgence est établie en raison de ce que de jeunes enfants subissent quotidiennement la sur-fréquentation du centre de rétention administrative de Pamandzi ; <br/>
              - il existe des doutes sérieux quant à la légalité de la décision ; <br/>
              - celle-ci porte atteinte à la dignité de la personne humaine ; <br/>
              - elle méconnaît l'intérêt supérieur de l'enfant consacré à l'article 3-1 de la convention internationale des droits de l'enfant ; <br/>
              - elle méconnaît l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - la faculté de placer des enfants en rétention est dépourvue de base légale ; <br/>
              - elle méconnaît le droit au recours effectif garanti par les article 5.1 et 5.4 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
<br/>
              Vu la copie de la requête à fin d'annulation de la même décision ;<br/>
              Vu le mémoire en défense, enregistré le 14 août 2012, présenté par le ministre de l'intérieur ; il soutient que :<br/>
              - la requête est irrecevable, faute d'existence matérielle de la décision attaquée ; <br/>
              - à supposer que cette décision existe, elle n'a pas modifié l'état du droit et n'est donc pas susceptible de recours ; <br/>
              - l'urgence n'est pas établie, en raison de ce que la rétention n'est pas une pratique illégale et qu'il y a un intérêt général au maintien du régime dérogatoire applicable à Mayotte ; <br/>
              - les moyens des requérants mettent nécessairement en cause la constitutionnalité ou la conventionalité de l'ordonnance du 26 avril 2000, seule applicable à Mayotte ; <br/>
              - il n'est pas dans l'office du juge des référés de contrôler la conformité à la Constitution ou la compatibilité à une convention internationale d'une disposition de valeur législative ;<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu l'ordonnance n°2000-373 du 26 avril 2000 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, le Groupe d'information et de soutien des immigrés (GISTI), d'autre part, le ministre de  l'intérieur ;  <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 22 août 2012 à 10 heures, au cours de laquelle ont été entendus :<br/>
<br/>
              - les représentants du GISTI ;<br/>
              - la représentante de l'association Avocats pour la défense des droits des étrangers et de la Ligue des droits de l'homme ;<br/>
              - les représentantes du Syndicat des avocats de France ;<br/>
- les représentants du ministre de l'intérieur ; <br/>
              et à l'issue de laquelle l'instruction a été close ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              2. Considérant que si, dans les conclusions de leur mémoire enregistré le 27 juillet 2012, les requérants demandent au juge des référés du Conseil d'Etat de suspendre l'exécution de la " décision du ministre de l'intérieur prescrivant au préfet de Mayotte de ne pas appliquer les dispositions de la circulaire NOR INT/K/12/07283/C du 6 juillet 2012 aux familles avec enfants en instance d'éloignement à Mayotte et, par suite, de les placer systématiquement en rétention administrative ", il ne résulte pas de l'instruction, et notamment des éléments recueillis au cours de l'audience publique, que le ministre aurait effectivement pris, s'agissant d'une circulaire qui n'est de toutes façons pas applicable à Mayotte, une telle décision ; <br/>
<br/>
              3. Considérant qu'il résulte toutefois de l'ensemble des termes du mémoire du 27 juillet 2012, éclairés par les explications fournies lors de l'audience publique, que le GISTI et les autres requérants demandent également la suspension de la décision du ministre de l'intérieur, révélée notamment par un communiqué de presse et une déclaration radiophonique, de ne pas donner, dans l'immédiat, au préfet de Mayotte des instructions analogues à celles adressées aux préfets de région et de département par la circulaire du 6 juillet 2012 ; <br/>
<br/>
              4. Considérant que la circulaire du 6 juillet 2012 entend préciser, à l'intention des préfets de région et de département, les conditions à respecter pour que l'application des dispositions de l'article L. 561-2 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) à des ressortissants étrangers accompagnés d'enfants mineurs ne viole pas les stipulations de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; que toutefois, ces dispositions du CESEDA, qui prévoient la faculté de prendre une décision administrative d'assignation à résidence pour un étranger pour lequel l'exécution de l'obligation de quitter le territoire demeure une perspective raisonnable, ne sont pas applicables à Mayotte et n'ont pas d'équivalent dans les dispositions de l'ordonnance du 26 avril 2000 relative aux conditions d'entrée et de séjour des étrangers à Mayotte ; qu'en effet, dans un tel cas, sous réserve des dispositions spécifiques des articles 39-1 et 39-2, l'article 48 de cette ordonnance prévoit seulement la faculté de prononcer, à certaines conditions, une décision de placement en rétention ; que la décision contestée doit dès lors s'analyser comme l'abstention du ministre de l'intérieur de prendre une instruction indiquant, pour Mayotte, les conditions propres à garantir une application de l'article 48 de l'ordonnance du 26 avril 2000 compatible avec les stipulations de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              5. Considérant que si le ministre de l'intérieur peut compétemment rappeler, par circulaire, l'interprétation de l'état du droit qui s'impose au préfet de Mayotte pour que les décisions de placement en rétention prises par ce dernier sur le fondement de l'article 48 de l'ordonnance du 26 avril 2000 respectent notamment les stipulations des articles 3, 5 et 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, l'abstention du ministre à prendre un tel acte ne constitue pas, alors même que cette circulaire pourrait comporter des dispositions à caractère impératif, une décision susceptible d'être déférée au juge de l'excès de pouvoir ; qu'elle ne saurait, par suite, faire l'objet d'une demande de suspension présentée sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative ; <br/>
<br/>
              6. Considérant, par ailleurs et au surplus, qu'il ne résulte pas de l'instruction que le ministre de l'intérieur aurait, par une décision susceptible de faire l'objet d'une demande de suspension fondée sur les mêmes dispositions, décidé de renoncer à l'obligation qui incombe au gouvernement de garantir, à Mayotte, des conditions de rétention administrative qui assurent le respect des principes rappelés par les articles 3, 5 et 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; qu'à cet égard, il a notamment été indiqué à l'audience publique qu'une mission avait été confiée à une personnalité indépendante, visant à faire, dans des délais rapprochés, et en vue d'une amélioration effective de la situation des personnes concernées, des préconisations portant notamment sur le régime juridique applicable en matière d'entrée et de séjour des ressortissants étrangers à Mayotte et sur les conditions de rétention administrative qui s'y rattachent ; <br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que le GISTI et les autres requérants ne soumettent au juge des référés aucune décision administrative susceptible, en l'état de l'instruction, de faire l'objet d'une demande de suspension sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative ; que leur requête est, par suite, irrecevable ; qu'elle doit être rejetée, y compris en ses conclusions tendant à ce qu'il soit enjoint au ministre de l'intérieur d'adresser de nouvelles injonctions, et en ses conclusions présentées sur le fondement de l'article L. 761-1 du même code ; <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : la requête du GISTI et autres est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée au Groupe d'information et de soutien des immigrés, à l'association Avocats pour la défense des droits des étrangers, au Comité médical pour les exilés, à la Fédération des associations de solidarité avec les travailleur-euse-s immigré-e-s, à la Ligue des droits de l'homme, au Mouvement contre le racisme et pour l'amitié entre les peuples, au Syndicat des avocats de France et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
