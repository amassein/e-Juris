<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029626704</ID>
<ANCIEN_ID>JG_L_2014_10_000000362723</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/62/67/CETATEXT000029626704.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 24/10/2014, 362723</TITRE>
<DATE_DEC>2014-10-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362723</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:362723.20141024</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et les mémoires complémentaires, enregistrés les 13 septembre et 12 décembre 2012 et le 7 janvier 2013 au secrétariat du contentieux du Conseil d'État, présentés pour le Syndicat intercommunal d'équipements publics de Moirans, dont le siège est à l'Hôtel de ville, place de l'assemblée départementale à Moirans (38430), représenté par son président ; il demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler le jugement nos 0905722, 1001027 du 13 juillet 2012 par lequel le tribunal administratif de Grenoble a, d'une part, annulé l'arrêté du 28 août 2009 de son président plaçant Mme B...A...en congé ordinaire, ainsi que l'arrêté du 23 octobre 2009 la plaçant en disponibilité d'office et la décision du 8 janvier 2010 rejetant son recours gracieux et, d'autre part, lui a enjoint de prendre, dans un délai de deux mois, de nouvelles décisions pour placer rétroactivement Mme A...dans une position statutaire régulière à compter du 27 octobre 2009 et de procéder à la reconstitution de sa carrière ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de Mme A...;<br/>
<br/>
              3°) de mettre à la charge de Mme A...le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des pensions civiles et militaires de retraite ; <br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat du Syndicat intercommunal d'équipements publics de Moirans, et à la SCP Piwnica, Molinié, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 57 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, dans sa rédaction applicable à la date du litige : " Le fonctionnaire en activité a droit : (...) 2° A des congés de maladie dont la durée totale peut atteindre un an pendant une période de douze mois consécutifs en cas de maladie dûment constatée mettant l'intéressé dans l'impossibilité d'exercer ses fonctions. Celui-ci conserve alors l'intégralité de son traitement pendant une durée de trois mois ; ce traitement est réduit de moitié pendant les neuf mois suivants. Le fonctionnaire conserve, en outre, ses droits à la totalité du supplément familial de traitement et de l'indemnité de résidence. / Toutefois, si la maladie provient de l'une des causes exceptionnelles prévues à l'article L. 27 du code des pensions civiles et militaires de retraite ou d'un accident survenu dans l'exercice ou à l'occasion de l'exercice de ses fonctions, le fonctionnaire conserve l'intégralité de son traitement jusqu'à ce qu'il soit en état de reprendre son service ou jusqu'à sa mise à la retraite. Il a droit, en outre, au remboursement des honoraires médicaux et des frais directement entraînés par la maladie ou l'accident (...) Dans le cas visé à l'alinéa précédent, l'imputation au service de l'accident ou de la maladie est appréciée par la commission de réforme instituée par le régime des pensions des agents des collectivités locales (...) " ;<br/>
<br/>
              2. Considérant qu'il ressort des énonciations du jugement attaqué que Mme B...A..., alors fonctionnaire de la commune de Moirans, a tenté, le 6 mai 2008, de mettre fin à ses jours par ingestion médicamenteuse sur son lieu de travail ; qu'après six semaines d'arrêt de travail, elle a repris son service au sein du Syndicat intercommunal d'équipements publics (SIEP) de Moirans auprès duquel elle avait été détachée à compter du 1er juillet 2008 ; qu'elle a de nouveau été placée en arrêt de travail à compter du 8 octobre 2008 en raison de difficultés psychologiques sévères ; que, par un arrêté du 28 août 2009, le président du syndicat intercommunal a placé Mme A...en congé de maladie ordinaire à compter du 8 octobre 2008 ; qu'ayant été déclarée inapte à reprendre son poste au terme de son arrêt de travail, Mme A...a été placée en disponibilité d'office par arrêté du 23 octobre 2009, dans l'attente de sa réintégration dans les services de la commune de Moirans ; que, par un jugement du 13 juillet 2012, contre lequel le SIEP de Moirans se pourvoit en cassation, le tribunal administratif de Grenoble a annulé ces deux arrêtés ainsi que la décision du 8 janvier 2010 rejetant le recours gracieux de Mme A...contre le second et a enjoint au syndicat intercommunal de la placer rétroactivement dans une position statutaire régulière à compter du 27 octobre 2009 et de procéder à la reconstitution de sa carrière ; <br/>
<br/>
              3. Considérant que, pour statuer ainsi, le tribunal administratif a retenu que la tentative de suicide de Mme A...s'était produite sur le lieu de travail, après qu'un retard à prendre son service lui eut été reproché, et qu'elle avait travaillé sous les ordres d'une responsable avec laquelle existait une incompatibilité d'humeur, avant de faire l'objet d'un détachement auprès du SIEP, son changement d'affectation ayant été ressenti par elle comme une profonde dévalorisation professionnelle ; qu'il ressort toutefois des rapports d'expertise médicale établis à la demande de la commission de réforme et versés au dossier du tribunal que la pathologie dépressive de l'intéressée, si elle a pu être favorisée par certaines conditions de son activité professionnelle, s'était déjà manifestée précédemment et trouvait son origine dans sa personnalité ; qu'en jugeant néanmoins que la pathologie dont souffrait Mme A...devait être regardée comme étant imputable au service, alors que le dossier qui lui était soumis ne faisait apparaître aucune circonstance particulière, tenant à ses conditions de travail, susceptible de l'avoir occasionnée, le tribunal administratif a inexactement qualifié les faits de la cause ; que, par suite et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, le SIEP de Moirans est fondé, pour ce motif, à demander l'annulation du jugement qu'il attaque ; <br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du SIEP qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions qu'il présente au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Grenoble du 13 juillet 2012 est annulé.<br/>
<br/>
      Article 2 : L'affaire est renvoyée au tribunal administratif de Grenoble.<br/>
<br/>
Article 3 : Les conclusions présentées par le Syndicat intercommunal d'équipements publics de Moirans et par Mme A...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée au Syndicat intercommunal d'équipements publics de Moirans et à Mme B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-08-02-02-01-02 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. QUALIFICATION JURIDIQUE DES FAITS. - NOTION DE MALADIE IMPUTABLE AU SERVICE [RJ1].
</SCT>
<ANA ID="9A"> 54-08-02-02-01-02 Le juge de cassation contrôle la qualification juridique que les juges du fond donnent aux faits lorsqu'ils reconnaissent à une maladie le caractère de maladie imputable au service pour l'application des dispositions sur les congés de l'article 57 de la loi n° 84-53 du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Rappr., sur le contrôle de qualification juridique de la notion d'accident de service, CE, 6 octobre 1999, Roces-Fernandez, n° 180275, T. pp. 856-862-987.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
