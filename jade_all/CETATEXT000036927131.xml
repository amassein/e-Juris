<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036927131</ID>
<ANCIEN_ID>JG_L_2018_05_000000408511</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/92/71/CETATEXT000036927131.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  5ème chambre jugeant seule, 18/05/2018, 408511, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-05-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408511</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 5ème chambre jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:408511.20180518</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...C...et Mme A...C...ont demandé au tribunal administratif de Paris d'annuler l'arrêté du 8 novembre 2012 par lequel le préfet de la région d'Ile-de-France, préfet de Paris, a déclaré insalubre le logement dont ils sont propriétaires 13, rue Lacroix, à Paris et leur a ordonné de prendre toutes mesures nécessaires pour remédier à cet état dans un délai de deux mois, d'annuler la décision du 17 janvier 2013 par laquelle l'agence régionale de santé d'Ile-de-France a rejeté leurs recours gracieux des 28 décembre 2012 et 7 janvier 2013 et d'annuler l'arrêté du 9 avril 2013 par lequel le maire de Paris les a mis en demeure d'exécuter les mesures prescrites par l'arrêté d'insalubrité du 8 novembre 2012 dans le délai d'un mois. Par un jugement n°1303797/6-3 du 6 novembre 2014, le tribunal administratif a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 15PA00680 du 30 décembre 2016, la cour administrative d'appel de Paris a rejeté l'appel formé contre ce jugement par M. et MmeC.... <br/>
<br/>
              Par un pourvoi sommaire et deux mémoires complémentaire, enregistrés les 28 février, 29 mai et 21 juin 2017 au secrétariat du contentieux du Conseil d'Etat, M. et Mme C...demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 600 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la santé publique ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Zribi, Texier, avocat de M. et MmeC....<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il résulte des pièces du dossier soumis aux juges du fond que, par un arrêté du 8 novembre 2012, le préfet de la région d'Ile-de-France, préfet de Paris a déclaré insalubre à titre remédiable un logement appartenant à M. et Mme C...et prescrit la réalisation des travaux nécessaires pour remédier à l'insalubrité constatée dans un délai de deux mois ; que, par une décision du 17 janvier 2013, le directeur de l'agence régionale de santé d'Ile-de-France a rejeté les recours gracieux formés par M. et Mme C...contre cet arrêté ; que, par un arrêté du 9 avril 2013, le maire de Paris a mis les consorts C...en demeure de réaliser les travaux en cause ; que M. et Mme C...ont présenté devant le tribunal administratif de Paris des conclusions tendant, notamment, à l'annulation des décisions des 8 novembre 2012, 17 janvier 2013 et 9 avril 2013 ; que, par un jugement du 6 novembre 2014, le tribunal administratif de Paris a constaté que, eu égard à l'exécution des travaux prescrits, le préfet avait prononcé en cours d'instance la mainlevée de l'arrêté du 8 novembre 2012 par un arrêté du 5 septembre 2014 et en a déduit qu'il n'y avait plus lieu de statuer sur les conclusions aux fins d'annulation présentées par M. et MmeC... ; que, par un arrêt du 30 décembre 2016, la cour administrative d'appel de Paris, après avoir annulé ce jugement, a jugé qu'il n'y avait  pas lieu de statuer sur la requête de M. et Mme C...tendant à l'annulation des arrêtés du préfet de la région d'Ile-de-France, préfet de Paris des 8 novembre 2012 et 9 avril 2013 et de la décision du 17 janvier 2013 du directeur de l'agence régionale de santé d'Ile-de-France rejetant leurs recours gracieux et a  rejeté leurs conclusions indemnitaires comme étant nouvelles en appel ; que M. et MmeC...  demandent l'annulation de cet arrêt ;<br/>
<br/>
              2. Considérant, en premier lieu, que, lorsque le propriétaire ou locataire d'un immeuble a formé un recours contre l'acte par lequel l'autorité préfectorale a, en application des articles L. 1331-17 à L. 1331-22 du code de la santé publique, déclaré un immeuble insalubre et prescrit la réalisation de travaux, l'exécution par le propriétaire ou le locataire en cours d'instance des travaux prescrits et la mainlevée par le préfet de l'arrêté d'insalubrité, ne privent pas d'objet le recours engagé par le propriétaire ou le locataire ; qu'en jugeant que l'intervention de l'arrêté du 5 septembre 2014 par lequel le préfet de la région d'Ile-de-France, préfet de Paris, a constaté l'exécution des travaux prescrits par son arrêté du 8 novembre 2012 et prononcé la mainlevée de cet arrêté privait d'objet les conclusions aux fins d'annulation de M. et Mme C..., la cour administrative d'appel de Paris a commis une erreur de droit qui justifie l'annulation de son arrêt en tant qu'il se prononce sur les conclusions aux fins d'annulation présentées par les intéressés ; <br/>
<br/>
              3. Considérant, en deuxième lieu, que, dans un mémoire enregistré le 17 juillet 2013, M. et Mme C...ont demandé au tribunal administratif de Paris à titre subsidiaire, de condamner l'Etat à les indemniser du préjudice ayant résulté pour eux d'une perte de loyers, dont ils ont soutenu qu'elle était imputable à la faute ayant consisté, de la part du préfet, à ne pas avoir fait usage en cours de procédure de la faculté de saisir en référé le tribunal de grande instance pour assigner les occupants du logement opposés à l'exécution des travaux, qui lui était ouverte par l'article L. 1334-4 du code de la santé publique ; qu'ils ont maintenu ces conclusions, chiffrées par référence au montant mensuel du loyer du logement en cause, dans leurs productions ultérieures des 4 septembre 2013, 4 octobre 2013, 9 janvier 2014 et 20 janvier 2014 ; qu'ils ont repris ces conclusions dans leur requête d'appel enregistrée le 20 février 2015 ; qu'en jugeant qu'à supposer qu'ils aient entendu présenter des conclusions indemnitaires, celles-ci devaient être regardées comme présentées pour la première fois en appel et, de ce fait, irrecevables, la cour administrative d'appel s'est méprise sur la portée des écritures qui lui étaient soumises ; que les requérants sont, par suite, également fondés à demander l'annulation de l'arrêt attaqué en tant qu'il se prononce sur leurs conclusions indemnitaires ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. et Mme C...en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 30 décembre 2016 de la cour administrative d'appel de Paris est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : L'Etat versera la somme de 3 000 euros à M. et Mme C...au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à M. B...C..., à Mme A...C...et à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
