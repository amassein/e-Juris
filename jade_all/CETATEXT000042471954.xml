<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042471954</ID>
<ANCIEN_ID>JG_L_2020_10_000000437791</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/47/19/CETATEXT000042471954.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 23/10/2020, 437791, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437791</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Mélanie Villiers</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:437791.20201023</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              La société Vendasi a demandé au tribunal administratif de Bastia d'annuler le marché conclu le 18 juillet 2014 entre la collectivité territoriale de Corse et le groupement des entreprises Raffalli et Pompeani en vue de l'aménagement du carrefour de la route nationale n° 193 situé à Furiani et de condamner la collectivité territoriale de Corse à lui verser la somme de 3 046 327 euros HT, majorée des intérêts moratoires et de leur capitalisation, en réparation des préjudices résultant de son éviction de la procédure de passation du marché, ainsi que le marché en litige. Par un jugement n° 1400700 du 4 octobre 2016, le tribunal administratif de Bastia a rejeté cette demande.<br/>
<br/>
              Par un premier arrêt n° 16MA04379 du 30 mars 2018, la cour administrative d'appel de Marseille a, sur appel de la société Vendasi, annulé ce jugement, estimé que le groupement d'entreprises composé des sociétés Vendasi, Antoniotti, Via Corsa et Raffali Paul Mathieu disposait d'une chance sérieuse de remporter ce marché et ordonné avant-dire droit une expertise afin d'évaluer le préjudice subi par ce groupement d'entreprises.<br/>
<br/>
              Par une décision n° 421075 du 20 septembre 2019, le Conseil d'Etat a rejeté le pourvoi de la collectivité territoriale de Corse contre cet arrêt. <br/>
<br/>
              Par un second arrêt n° 16MA04379 du 18 novembre 2019, la cour administrative d'appel de Marseille a condamné la collectivité de Corse à verser à la société Vendasi, en sa qualité de mandataire du groupement d'entreprises comprenant par ailleurs les sociétés Antoniotti, Via Corsa et Raffalli Paul Mathieu, la somme de 1 747 818,15 euros, somme portant intérêts au taux légal à compter du 18 août 2014 et a mis à sa charge l'ensemble des frais d'expertise.<br/>
<br/>
              1° Sous le n° 437791, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 janvier et 11 juin 2020 au secrétariat du contentieux du Conseil d'Etat, la collectivité de Corse demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 18 novembre 2019 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la société Vendasi la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              2° Sous le n° 441131, par une requête enregistrée le 11 juin 2020 au secrétariat du contentieux du Conseil d'Etat, la même collectivité demande au Conseil d'Etat : <br/>
<br/>
              1°) d'ordonner le sursis à exécution de ce même arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la société Vendasi la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... B..., maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la collectivité de Corse ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              Sur le pourvoi dirigé contre l'arrêt attaqué :<br/>
<br/>
              1. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              2. Pour demander l'annulation de l'arrêt de la cour administrative de Marseille qu'elle attaque, la collectivité de Corse soutient que la cour a : <br/>
              - dénaturé son argumentation en retenant qu'elle ne soutenait pas que la régularité ou le bien-fondé des conclusions de l'expert judiciaire était remis en cause par la prise en compte des attestations comptables communiquées par les sociétés appelantes dans le cadre des opérations d'expertise, en raison de la non-conformité formelle de ces documents et des contradictions dont ils étaient entachés ;<br/>
              - dénaturé les pièces versées aux débats en retenant que le taux de marge nette de la société Vendasi s'établissait à 28,82 %, alors que ce taux ne pouvait en réalité être retenu pour évaluer le manque à gagner subi par la société.<br/>
<br/>
              3. Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
              Sur la requête à fin de sursis à exécution : <br/>
<br/>
              4. Il résulte de ce qui précède que le pourvoi formé par la collectivité de Corse contre l'arrêt du 18 novembre 2019 de la cour administrative d'appel de Marseille n'est pas admis. Par suite, les conclusions à fin de sursis à exécution de cet arrêt sont devenues sans objet. Les conclusions présentées par la collectivité au titre de l'article L. 761-1 du code de justice administrative à l'appui de sa demande de sursis à exécution ne peuvent, par conséquent, qu'être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la collectivité de Corse n'est pas admis.<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions de la requête n° 441131.<br/>
Article 3 : Les conclusions présentées sous le n° 441131 par la collectivité de Corse au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la collectivité de Corse.<br/>
Copie en sera adressée aux sociétés Vendasi, Raffalli Paul Mathieu, Via Corsa, Raffali TP et Antoniotti. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
