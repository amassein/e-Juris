<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038860084</ID>
<ANCIEN_ID>JG_L_2019_07_000000424486</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/86/00/CETATEXT000038860084.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 31/07/2019, 424486, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424486</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:424486.20190731</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Marseille l'annulation de l'arrêté du 12 juin 2015 par lequel le président de la communauté d'agglomération du pays d'Aubagne et de l'Etoile a prononcé sa révocation à compter du 1er juillet 2015 et sa radiation des cadres à compter de cette même date. Par un jugement n° 1505879 du 3 mai 2017, le tribunal administratif de Marseille a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17MA02778 du 17 juillet 2018, la cour administrative d'appel de Marseille a annulé ce jugement et enjoint à la métropole Aix-Marseille Provence, venue aux droits de la communauté d'agglomération du pays d'Aubagne et de l'Etoile, de réintégrer M. A... dans le poste qu'il occupait ou, à défaut, dans un poste équivalent, et de procéder à la reconstitution de sa carrière et de ses droits sociaux.<br/>
<br/>
              Par un pourvoi enregistré le 26 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, la métropole Aix-Marseille Provence demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la requête de M.A... ;<br/>
<br/>
              3°) de mettre à la charge de M. A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de Métropole Aix-marseille Provence et à la SCP Didier, Pinet, avocat de M. B...A...;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A..., adjoint technique territorial de 2ème classe depuis 2010 de la communauté d'agglomération du Pays d'Aubagne et de l'Etoile, aux droits de laquelle vient la métropole Aix-Marseille Provence, exerçait les fonctions d'ambassadeur du tri au sein du service de la collecte des déchets. Il a été déclaré coupable, par un arrêt du 26 septembre 2014 de la cour d'appel d'Aix-en-Provence devenu définitif, pour des faits de recel survenus en 2011 et portant sur cinq santons de collection, volés dans des circonstances non élucidées à son employeur. Se trouvant en état de récidive légale compte tenu d'une condamnation définitive en date du 28 mars 2007 liée notamment à l'obtention frauduleuse d'une allocation d'aide aux travailleurs privés d'emploi avec vol, il a été condamné à huit mois d'emprisonnement assorti d'un sursis et d'une mise à l'épreuve ainsi qu'au paiement à la collectivité de la somme de 9 000 euros en réparation du préjudice subi. Le président de la collectivité a, par arrêté du 12 juin 2015, prononcé la révocation de M. A...et sa radiation des cadres à compter du 1er juillet 2015. Par un jugement du 3 mai 2017, le tribunal administratif de Marseille a rejeté la demande de M. A... tendant à l'annulation de cet arrêté. La métropole Aix-Marseille Provence se pourvoit en cassation contre l'arrêt du 17 juillet 2018 par lequel la cour administrative d'appel de Marseille a fait droit à l'appel de M. A...contre ce jugement, a annulé l'arrêté précité et a enjoint à la métropole de le réintégrer dans le poste qu'il occupait ou, à défaut, dans un poste équivalent.  <br/>
<br/>
              2. En premier lieu, il ressort des pièces du dossier que l'arrêt attaqué comporte les signatures exigées par l'article R. 741-7 du code de justice administrative. Par suite, le moyen tiré de ce qu'il serait entaché d'irrégularité à défaut d'être revêtu de ces signatures doit être écarté.<br/>
<br/>
              3. En second lieu, aux termes de l'article 29 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires : " Toute faute commise par un fonctionnaire dans l'exercice ou à l'occasion de l'exercice de ses fonctions l'expose à une sanction disciplinaire sans préjudice, le cas échéant, des peines prévues par la loi pénale ". Aux termes de l'article 89 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " Les sanctions disciplinaires sont réparties en quatre groupes : (...) / troisième groupe : / la rétrogradation ; / l'exclusion temporaire de fonctions pour une durée de seize jours à deux ans ; / Quatrième groupe : / la mise à la retraite d'office ; / la révocation ".<br/>
<br/>
              4. Il appartient au juge de l'excès de pouvoir, saisi de moyens en ce sens, de rechercher si les faits reprochés à un agent public ayant fait l'objet d'une sanction disciplinaire constituent des fautes de nature à justifier une sanction et si la sanction retenue est proportionnée à la gravité de ces fautes. Si le caractère fautif des faits reprochés est susceptible de faire l'objet d'un contrôle de qualification juridique de la part du juge de cassation, l'appréciation du caractère proportionné de la sanction au regard de la gravité des fautes commises relève de l'appréciation des juges du fond et n'est susceptible d'être remise en cause par le juge de cassation que dans le cas où la solution qu'ils ont retenue quant au choix, par l'administration, de la sanction est hors de proportion avec les fautes commises.<br/>
<br/>
              5. Par ailleurs, aux termes du premier alinéa de l'article 321-1 du code pénal : " Le recel est le fait de dissimuler, de détenir ou de transmettre une chose, ou de faire office d'intermédiaire afin de la transmettre, en sachant que cette chose provient d'un crime ou d'un délit. ". Selon une jurisprudence établie du juge judiciaire, l'élément intentionnel du délit de recel consiste dans la connaissance de l'origine frauduleuse des objets recelés, quand bien même le receleur aurait ignoré la personne au préjudice de laquelle cette infraction a été commise.<br/>
<br/>
              6. Par l'arrêt attaqué, qui est suffisamment motivé, la cour a estimé, d'une part, que les faits de recel et l'état de récidive étaient établis et, d'autre part, que les manquements aux obligations de loyauté et de probité reprochés à M.A..., par ailleurs suspendu à titre conservatoire, étaient d'une particulière gravité. Toutefois, relevant qu'au regard notamment de la manière satisfaisante de servir de l'intéressé le conseil de discipline avait été d'avis à une large majorité de prononcer à son encontre une exclusion temporaire de fonctions pour une durée de vingt-quatre mois assortie d'un sursis partiel de vingt-deux mois, et que la décision litigieuse était exclusivement justifiée par le délit de recel pour lequel M. A... avait été pénalement condamné, la cour a jugé que la sanction de révocation était, dans les circonstances de l'espèce, disproportionnée par rapport aux fautes commises. Dès lors qu'il ne ressort pas des pièces du dossier soumis aux juges du fond que les sanctions susceptibles d'être infligées à M. A...par la métropole, sans méconnaître l'autorité de la chose jugée, seraient toutes, en raison de leur caractère insuffisant, hors de proportion avec les fautes commises, la cour, qui n'a pas dénaturé les faits de l'espèce, n'a pas commis d'erreur de droit. <br/>
<br/>
              7. Il résulte de ce qui précède que la métropole Aix-Marseille Provence n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la charge de M. A..., qui n'est pas la partie perdante dans la présente instance. M. A... a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Hélène Didier et François Pinet, avocat de M.A..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Hélène Didier et François Pinet. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de la métropole Aix-Marseille Provence est rejetée.<br/>
Article 2 : La métropole Aix-Marseille Provence versera à la SCP Hélène Didier et François Pinet une somme de 3 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 3 : La présente décision sera notifiée à la métropole Aix-Marseille Provence et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
