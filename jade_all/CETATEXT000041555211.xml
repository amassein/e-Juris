<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041555211</ID>
<ANCIEN_ID>JG_L_2020_02_000000426545</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/55/52/CETATEXT000041555211.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 10/02/2020, 426545, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-02-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426545</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY ; SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:426545.20200210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Lyon de condamner l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) à lui verser la somme de 343 707,52 euros en réparation des préjudices qu'il estime avoir subis, ainsi qu'une somme de 10 000 euros au titre de dommages et intérêts. Par un jugement n° 1305586 du 7 avril 2016, le tribunal administratif a condamné l'ONIAM à verser à M. B... la somme de 54 242,92 euros au titre des préjudices imputables à l'aléa thérapeutique dont il a été victime et a rejeté le surplus de sa demande.<br/>
<br/>
              Par un arrêt n° 16LY01977 du 25 octobre 2018, la cour administrative d'appel de Lyon a, sur appel de M. B..., porté à 71 917,41 euros la somme que l'ONIAM est condamné à lui verser et rejeté le surplus de son appel.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 24 décembre 2018 et 25 mars 2019 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il rejette le surplus de son appel ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'ONIAM la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de M. B....<br/>
<br/>
<br/>
<br/>
<br/>1. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              2. Pour demander l'annulation de l'arrêt de la cour administrative d'appel de Lyon qu'il attaque, M. B... soutient qu'il est entaché :<br/>
              - de dénaturation des pièces du dossier en ce qu'il estime qu'il n'y a pas de lien de causalité entre l'infection nosocomiale et les dommages nés de la nécessité d'aménager son logement ;<br/>
              - d'insuffisance de motivation, d'erreur de droit et de dénaturation des pièces du dossier en ce qu'il juge que l'indemnité représentative des indemnités pour heures supplémentaires et primes exceptionnelles qu'il aurait dû percevoir durant la période pendant laquelle il a été placé en arrêt de travail s'élève à 9 066 euros ;<br/>
              - de dénaturation des pièces du dossier en ce qu'il estime qu'il n'est pas établi que, sans la survenance de l'algodystrophie dont il souffre, il serait resté en fonctions jusqu'à l'âge de 65 ans.<br/>
<br/>
              3. Eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi qui sont dirigées contre l'arrêt attaqué en tant qu'il se prononce, d'une part sur les dommages nés de la nécessité d'aménager le logement et, d'autre part, sur l'indemnisation des pertes de gains professionnels entre septembre 2009 et septembre 2011. En revanche, s'agissant du surplus des conclusions de M. B..., aucun des moyens soulevés n'est de nature à en permettre l'admission.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Les conclusions du pourvoi de M. B... qui sont dirigées contre l'arrêt attaqué en tant qu'il se prononce sur l'indemnisation des pertes de gains professionnels entre septembre 2009 et septembre 2011 ainsi que sur l'indemnisation du préjudice d'aménagement du logement sont admises.<br/>
Article 2 : Le surplus des conclusions du pourvoi n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à M. A... B.... <br/>
Copie en sera adressée à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales et à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
