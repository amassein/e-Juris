<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033442773</ID>
<ANCIEN_ID>JG_L_2016_11_000000392560</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/44/27/CETATEXT000033442773.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 21/11/2016, 392560</TITRE>
<DATE_DEC>2016-11-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392560</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:392560.20161121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. B...D...a demandé au tribunal administratif d'Orléans d'annuler pour excès de pouvoir la décision du 4 mars 2013 par laquelle le ministre de l'intérieur a autorisé l'inhumation de M. A... C..., archevêque émérite de Tours, dans la cathédrale de cette ville ainsi que la décision de rejet implicite née du silence gardé par le ministre de l'intérieur sur son recours gracieux. Par un jugement n° 1302244 du 13 février 2014, le tribunal administratif a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 14NT00868 du 18 juin 2015, la cour administrative d'appel de Nantes a rejeté l'appel formé contre ce jugement par M.D.... <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux nouveaux mémoires, enregistrés les 11 août et 11 novembre 2015 et les 12  et 14 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, M. D...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              - le code général des collectivités territoriales ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de M.D....<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 4 mars 2013, prise sur la demande du préfet d'Indre-et-Loire, le ministre de l'intérieur a autorisé l'inhumation de M. A...C..., archevêque émérite de Tours, dans la cathédrale de cette ville ; que M. D...a formé un recours pour excès de pouvoir contre cette décision ainsi que contre la décision implicite par laquelle le ministre de l'intérieur a rejeté son recours gracieux ; que le tribunal administratif a rejeté sa demande par un jugement du 13 février 2014 au motif que M. D...ne justifiait pas d'un intérêt à agir contre la décision attaquée ; que la cour administrative d'appel de Nantes a rejeté l'appel qu'il a formé contre ce jugement au motif que la décision du 4 mars 2013 devait être regardée comme une décision purement gracieuse et, comme telle, insusceptible de faire l'objet d'un recours en excès de pouvoir ; <br/>
<br/>
              2. Considérant que, si le refus d'accorder une mesure purement gracieuse n'est pas susceptible de recours, la décision par laquelle une autorité administrative octroie une telle mesure peut être attaquée par un tiers justifiant, eu égard à l'atteinte que cette décision porte à sa situation, d'un intérêt lui donnant qualité pour agir ; qu'en déduisant l'irrecevabilité du recours de M. D...du seul caractère purement gracieux de la mesure attaquée, la cour administrative d'appel a commis une erreur de droit qui justifie, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'annulation de son arrêt ; <br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              4. Considérant que l'intérêt à agir s'apprécie par rapport à l'objet de la décision attaquée et à la qualité dont se prévaut le requérant ; qu'en l'espèce, les qualités de membre de la communauté chrétienne appartenant au diocèse de Tours, de résident du ressort de ce diocèse et d'usager du service public des monuments historiques dont se prévaut M. D...ne suffisent pas à lui donner intérêt pour agir contre la décision par laquelle le ministre de l'intérieur a autorisé l'inhumation de M. C...dans la cathédrale de Tours ; qu'il n'est, dès lors, pas fondé à soutenir que c'est à tort que le tribunal administratif d'Orléans a rejeté sa demande pour ce motif ;<br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que la somme demandée au titre de ces dispositions par M. D... soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ; qu'il résulte des dispositions de l'article L. 761-1 du code de justice administrative que, si une personne publique qui n'a pas eu recours au ministère d'avocat peut néanmoins demander au juge l'application de cet article au titre des frais spécifiques exposés par elle à l'occasion de l'instance, elle ne saurait se borner à faire état d'un surcroît de travail de ses services sans indiquer précisément les frais que l'Etat a exposés pour assurer sa défense ; que, par suite, il y a lieu en l'espèce de rejeter les conclusions présentées en appel par le ministre de l'intérieur et tendant à ce que la somme de 3 000 euros soit mise à la charge de M.D... ; <br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 18 juin 2015 de la cour administrative d'appel de Nantes est annulé. <br/>
Article 2 : Les conclusions présentées par M. D...devant la cour administrative d'appel de Nantes et le surplus des conclusions de son pourvoi sont rejetées. <br/>
<br/>
Article 3 : Les conclusions présentées en appel par le ministre de l'intérieur au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à M. B...D...et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-01-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES CONSTITUANT DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - OCTROI D'UNE MESURE PUREMENT GRACIEUSE - CONDITIONS.
</SCT>
<ANA ID="9A"> 54-01-01-01 Si le refus d'accorder une mesure purement gracieuse n'est pas susceptible de recours, la décision par laquelle une autorité administrative octroie une telle mesure peut être attaquée par un tiers justifiant, eu égard à l'atteinte que cette décision porte à sa situation, d'un intérêt lui donnant qualité pour agir.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
