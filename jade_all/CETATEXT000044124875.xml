<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044124875</ID>
<ANCIEN_ID>JG_L_2021_09_000000438042</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/12/48/CETATEXT000044124875.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 28/09/2021, 438042, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-09-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438042</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SARL MEIER-BOURDEAU, LECUYER ET ASSOCIES ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Martin Guesdon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:438042.20210928</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante : <br/>
<br/>
              La société à responsabilité limitée (SARL) Banchereau a demandé au tribunal administratif de Poitiers d'annuler la décision du 6 janvier 2015 par laquelle le directeur général de l'établissement national des produits de l'agriculture et de la mer (FranceAgriMer) lui a demandé le versement de la somme de 89 451,38 euros, correspondant au remboursement partiel de l'aide à l'enrichissement de la récolte par adjonction de moût de raisin concentré ou de moût concentré rectifié dont elle a bénéficié pour la campagne viticole 2010/2011. Par un jugement n° 1500717 du 24 mai 2017, le tribunal administratif de Poitiers l'a déchargée de l'obligation de payer la somme de 87 146,11 euros et rejeté le surplus de la demande.<br/>
<br/>
              Par un arrêt n° 17BX002447 du 28 novembre 2019, la cour administrative d'appel de Bordeaux a, d'une part, sur appel formé par FranceAgriMer, annulé l'article 1er de ce jugement et remis à la charge de la société Banchereau la somme de 87 146,11 euros et, d'autre part, rejeté les conclusions incidentes de la société Banchereau tendant à l'annulation du jugement en tant qu'il avait rejeté le surplus de sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 28 janvier et 23 avril 2020 et 11 août 2021 au secrétariat du contentieux du Conseil d'Etat, la société Banchereau demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de FranceAgriMer et de faire droit à son appel incident ;<br/>
<br/>
              3°) de mettre à la charge de FranceAgriMer la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le règlement (CE) n° 555/2008 de la Commission du 27 juin 2008 ; <br/>
              - le règlement (CE) n° 491/2009 du Conseil du 25 mai 2009 ; <br/>
              - le règlement (CE) n° 436/2009 de la Commission du 26 mai 2009 ;<br/>
              - le code rural et de la pêche maritime ; <br/>
              - le décret n° 2009-178 du 16 février 2009 ;<br/>
              - l'arrêté du 16 février 2009 relatif aux opérations d'enrichissement des produits vinicoles par addition de moût concentré ou de moût concentré rectifié pour le paiement des aides communautaires prévues à l'article 19 du règlement (CE) n° 479/2008 et à leur contrôle ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Martin Guesdon, auditeur,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la société Banchereau et à la SARL Meier-Bourdeau, Lecuyer et associés, avocat de l'établissement national des produits de l'agriculture et de la mer ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que la société Banchereau a bénéficié, pour la campagne viticole 2010/2011, d'une aide à l'enrichissement de la récolte par adjonction de moût de raisin concentré ou de moût concentré rectifié d'un montant de 145 850,96 euros. A la suite de contrôles opérés par les agents de la direction des douanes et des droits indirects, le directeur général de FranceAgriMer a informé la société de ce qu'il était susceptible de procéder à un retrait partiel des aides accordées à hauteur de 94 745,76 euros. Le 6 janvier 2015, le directeur général de FranceAgriMer a émis un titre exécutoire d'un montant de 89 451,38 euros à l'encontre de la société Banchereau, correspondant au remboursement des aides restant en litige. Par un jugement du 24 mai 2017, le tribunal administratif de Poitiers a déchargé la société de l'obligation de payer la somme de 87 146,11 euros. La société Banchereau se pourvoit en cassation contre l'arrêt du 28 novembre 2019 par lequel la cour administrative d'appel de Bordeaux, d'une part, sur appel formé par FranceAgriMer, a annulé le jugement en tant qu'il l'avait déchargée de l'obligation de payer et remis à sa charge la somme de 87 146,11 euros et, d'autre part, a rejeté ses conclusions incidentes tendant à l'annulation du jugement en tant qu'il avait rejeté le surplus de sa demande. <br/>
<br/>
              2. D'une part, aux termes de l'article 103 sexvicies du règlement n° 1234/2007 du Conseil du 22 octobre 2007 portant organisation commune des marchés dans le secteur agricole et dispositions spécifiques en ce qui concerne certains produits de ce secteur : " Un soutien peut être accordé jusqu'au 31 juillet 2012 aux producteurs de vin qui utilisent le moût de raisin concentré, y compris le moût de raisin concentré rectifié, pour accroître le titre alcoométrique naturel des produits conformément aux conditions fixées à l'annexe XV bis ". En vertu du point D de l'annexe XV bis de ce même règlement, les opérations d'enrichissement doivent faire l'objet d'une déclaration aux autorités compétentes. Aux termes de l'article 41 du règlement n° 436/2009 de la Commission du 26 mai 2009 : " 1. Les manipulations suivantes sont indiquées dans les registres: / a) l'augmentation du titre alcoométrique (...) 2. Pour chacune des manipulations visées au paragraphe 1, sont mentionnés : / a) la manipulation effectuée et la date de celle-ci ; / b) la nature et les quantités de produits mis en œuvre ; / (...) d) la quantité de produit utilisé pour augmenter le titre alcoométrique (...) ". Aux termes de l'article 45 de ce même règlement : " 1. Les écritures sur les registres ou comptes particuliers: (...) / b) visées à l'article 41 sont passées au plus tard le premier jour ouvrable suivant celui de la manipulation et pour celles relatives à l'enrichissement, le jour même (...) / Toutefois, les États membres peuvent autoriser des délais plus longs, ne dépassant pas trente jours, notamment lorsqu'il est utilisé une comptabilité matières informatisée, à condition qu'un contrôle des entrées et des sorties ainsi que des manipulations visées à l'article 41 reste possible à tout moment sur la base d'autres pièces justificatives, pour autant qu'elles sont considérées comme dignes de foi par l'instance compétente, un service ou organisme habilité par celle-ci ". Aux termes de l'article 34 du règlement n° 555/2008 de la Commission du 27 juin 2008, relatif au contrôle de l'aide à l'utilisation du moût de raison concentré : " Les autorités compétentes des États membres prennent toutes les mesures qui s'imposent pour veiller à ce que soient effectués les contrôles nécessaires en vue de vérifier, notamment, l'identité et le volume du produit utilisé aux fins de l'augmentation du titre alcoométrique ". <br/>
<br/>
              3. D'autre part, en vertu de l'article 8 de l'arrêté du 16 février 2009 relatif aux opérations d'enrichissement des produits vinicoles par addition de moût concentré ou de moût concentré rectifié pour le paiement des aides communautaires prévues à l'article 19 du règlement (CE) n° 479/2008 et à leur contrôle, les " irrégularités constatées " lors des contrôles des opérations d'enrichissement " entraînent le rejet ou la diminution de l'aide à verser, selon les modalités prévues " par cet arrêté. Aux termes du 4 de l'article 11 du même arrêté : " Le volume total d'un type de vin déclaré obtenu au registre de manipulation après enrichissement ne peut pas être supérieur au volume de ce type de vin déclaré produit sur la déclaration de production. Toutefois le dépassement du volume de vin figurant dans la déclaration de production peut faire l'objet d'informations complémentaires, au plus tard le 31 mai de la campagne ou, au-delà de cette date, dans un délai d'un mois suivant la notification par établissement créé en application de l'article L. 621-1 du code rural et de la pêche maritime, compétent en matière viticole. Dans ce cas l'aide sera versée sans minoration ". L'article 17 du même arrêté dispose : " (...) Dans le cas d'application de l'article 11, paragraphe 4, du présent arrêté, sauf informations complémentaires apportées, l'aide est limitée proportionnellement au volume de vin du type concerné déclaré sur la déclaration de production (...) ". <br/>
<br/>
              4. En premier lieu, en jugeant que les fiches de chai et les tableaux produits par la société ne permettaient pas de justifier l'écart entre le volume de vin déclaré obtenu au registre de manipulation après enrichissement et le volume de vin déclaré produit sur la déclaration de production, au motif que ces documents ne présentaient pas un caractère probant permettant de les regarder comme des " informations complémentaires " au sens des dispositions de l'article 11 de l'arrêté du 16 février 2009 précité, la cour, qui a souverainement apprécié le caractère probant des documents en cause, n'a pas commis d'erreur de droit.<br/>
<br/>
              5. En second lieu, pour écarter l'argumentation de la société qui expliquait la différence entre le volume total de moûts rosés acquis et les quantités de moûts rosés mises en œuvre lors des opérations d'enrichissement par l'assemblage préalable de moûts blancs et de moûts rosés, la cour a jugé qu'elle ne justifiait pas de l'inscription de cette manipulation sur le registre dédié et que les fiches de chai et les tableaux produits étaient dénués de valeur probante. Si la société soutient qu'aucune disposition de l'arrêté du 16 février 2009 ne permettait à FranceAgriMer de prononcer la restitution de l'aide à hauteur de 2 305,27 euros, un tel moyen est nouveau en cassation et ne peut, par suite, qu'être écarté comme inopérant.<br/>
<br/>
              6. Il résulte de tout ce qui précède que la société Banchereau n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. <br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Banchereau la somme de 3 000 euros à verser à FranceAgriMer au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font, en revanche, obstacle à ce qu'une somme soit mise à ce titre à la charge de FranceAgriMer qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Banchereau est rejeté. <br/>
Article 2 : La société Banchereau versera à FranceAgriMer la somme de 3 000 euros au titre au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La décision sera notifiée à la société Banchereau et à l'Etablissement national des produits de l'agriculture et de la mer (FranceAgriMer). <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
