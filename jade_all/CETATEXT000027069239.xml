<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027069239</ID>
<ANCIEN_ID>JG_L_2013_02_000000351124</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/06/92/CETATEXT000027069239.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 15/02/2013, 351124</TITRE>
<DATE_DEC>2013-02-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>351124</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Guillaume Odinet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:351124.20130215</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 22 juillet 2011 au secrétariat du contentieux du Conseil d'Etat, présentée par la Fédération nationale des syndicats professionnels de l'enseignement libre catholique ; la Fédération nationale des syndicats professionnels de l'enseignement libre catholique demande au Conseil d'Etat d'annuler la circulaire du 24 mai 2011 du ministre de l'éducation nationale, de la jeunesse et de la vie associative en tant qu'elle prévoit que les dispositions de l'article L. 921-4 du code de l'éducation relatives au maintien en activité des personnels enseignants appartenant aux corps des instituteurs et des professeurs des écoles sont également applicables aux maîtres contractuels ou agréés des établissements d'enseignement privés sous contrat ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'éducation ;<br/>
<br/>
              Vu la loi n° 1330-2010 du 9 novembre 2010 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Odinet, Auditeur,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier qu'à la suite de la promulgation de la loi du 9 novembre 2010 portant réforme des retraites, le ministre de l'éducation nationale, de la jeunesse et de la vie associative a adressé aux recteurs et inspecteurs d'académie une circulaire du 24 mai 2011, qui précise notamment les modalités de cessation d'activité pour départ à la retraite des maîtres contractuels ou agréés des établissements d'enseignement privés sous contrat ; que la Fédération nationale des syndicats professionnels de l'enseignement libre catholique demande l'annulation de cette circulaire en tant qu'elle prévoit que sont applicables à ces agents les dispositions de l'article L. 921-4 du code de l'éducation relatives à la cessation d'activité des personnels enseignants appartenant aux corps des instituteurs et des professeurs des écoles ; <br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article L. 921-4 du code de l'éducation : " Les personnels enseignants appartenant aux corps des instituteurs et des professeurs des écoles qui remplissent, en cours d'année scolaire, les conditions d'âge pour obtenir la jouissance immédiate de leur pension sont maintenus en activité jusqu'au 31 août, sauf s'ils sont atteints par la limite d'âge. Ce maintien en activité ne s'applique pas aux personnels visés aux 2° et 3° du I de l'article L. 24 du code des pensions civiles et militaires de retraite. " ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 914-1 du code de l'éducation : " Les règles générales qui déterminent les conditions de service et de cessation d'activité des maîtres titulaires de l'enseignement public, ainsi que les mesures sociales et les possibilités de formation dont ils bénéficient, sont applicables également et simultanément aux maîtres justifiant du même niveau de formation, habilités par agrément ou par contrat à exercer leur fonction dans des établissements d'enseignement privés liés à l'Etat par contrat. Ces maîtres bénéficient également des mesures de promotion et d'avancement prises en faveur des maîtres de l'enseignement public. / (...) " ;<br/>
<br/>
              4. Considérant qu'il résulte de ces dispositions que la règle de maintien en activité des personnels enseignants énoncée par l'article L. 921-4 du code de l'éducation, qui concerne les conditions de service et de cessation d'activité des enseignants, est applicable de plein droit aux maîtres contractuels ou agréés des établissements d'enseignement privés sous contrat ; que le moyen tiré de ce que la circulaire attaquée, qui prescrit à ses destinataires d'adopter une telle interprétation des dispositions législatives, serait entachée d'incompétence, ne peut, par suite, qu'être écarté ; <br/>
<br/>
              5. Considérant, en deuxième lieu, que, si la fédération requérante soutient que la circulaire attaquée créerait une différence de traitement entre les maîtres contractuels ou agréés des établissements d'enseignement privés sous contrat et les salariés du secteur privé, qui relèvent du même régime de retraite, cette différence de traitement ne résulte pas, en tout état de cause, de la circulaire attaquée mais des dispositions législatives précitées de l'article L. 921-4 du code de l'éducation  ; que, par suite, le moyen tiré de ce que la circulaire attaquée porterait atteinte au principe d'égalité ne peut qu'être écarté ;<br/>
<br/>
              6. Considérant, en troisième lieu, que le détournement de pouvoir allégué n'est pas établi ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que la Fédération nationale des syndicats professionnels de l'enseignement libre catholique n'est pas fondée à demander l'annulation de la circulaire du 24 mai 2011 en tant qu'elle prévoit que les dispositions de l'article L. 921-4 du code de l'éducation sont applicables aux maîtres contractuels ou agréés des établissements d'enseignement privés sous contrat ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la  Fédération nationale des syndicats professionnels de l'enseignement libre catholique est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la Fédération nationale des syndicats professionnels de l'enseignement libre catholique et au ministre de l'éducation nationale, de la jeunesse et de la vie associative.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">30-01-02-01 ENSEIGNEMENT ET RECHERCHE. QUESTIONS GÉNÉRALES. QUESTIONS GÉNÉRALES RELATIVES AU PERSONNEL. QUESTIONS GÉNÉRALES RELATIVES AU PERSONNEL ENSEIGNANT. - DISPOSITIONS DE L'ARTICLE L. 914-1 DU CODE DE L'ÉDUCATION RENDANT APPLICABLES AUX MAÎTRES DE L'ENSEIGNEMENT PRIVÉ LES RÈGLES GÉNÉRALES ET MESURES SOCIALES APPLICABLES AUX MAÎTRES DE L'ENSEIGNEMENT PUBLIC - PORTÉE [RJ1] - RÈGLES CONCERNANT LES CONDITIONS DE SERVICE ET DE CESSATION D'ACTIVITÉ DES ENSEIGNANTS - RÈGLE DE MAINTIEN EN ACTIVITÉ DES PERSONNELS ENSEIGNANTS PRÉVUE PAR L'ARTICLE L. 921-4 DU MÊME CODE - INCLUSION - CONSÉQUENCE - RÈGLE APPLICABLE DE PLEIN DROIT AUX MAÎTRES CONTRACTUELS OU AGRÉÉS DES ÉTABLISSEMENTS D'ENSEIGNEMENT PRIVÉS SOUS CONTRAT.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">30-02-01-03 ENSEIGNEMENT ET RECHERCHE. QUESTIONS PROPRES AUX DIFFÉRENTES CATÉGORIES D'ENSEIGNEMENT. ENSEIGNEMENT DU PREMIER DEGRÉ. INSTITUTEURS ET PROFESSEURS DES ÉCOLES. - DISPOSITIONS DE L'ARTICLE L. 914-1 DU CODE DE L'ÉDUCATION RENDANT APPLICABLES AUX MAÎTRES DE L'ENSEIGNEMENT PRIVÉ LES RÈGLES GÉNÉRALES ET MESURES SOCIALES APPLICABLES AUX MAÎTRES DE L'ENSEIGNEMENT PUBLIC - PORTÉE [RJ1] - RÈGLES CONCERNANT LES CONDITIONS DE SERVICE ET DE CESSATION D'ACTIVITÉ DES ENSEIGNANTS - RÈGLE DE MAINTIEN EN ACTIVITÉ DES PERSONNELS ENSEIGNANTS PRÉVUE PAR L'ARTICLE L. 921-4 DU MÊME CODE - INCLUSION - CONSÉQUENCE - RÈGLE APPLICABLE DE PLEIN DROIT AUX MAÎTRES CONTRACTUELS OU AGRÉÉS DES ÉTABLISSEMENTS D'ENSEIGNEMENT PRIVÉS SOUS CONTRAT.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">30-02-07-01 ENSEIGNEMENT ET RECHERCHE. QUESTIONS PROPRES AUX DIFFÉRENTES CATÉGORIES D'ENSEIGNEMENT. ÉTABLISSEMENTS D'ENSEIGNEMENT PRIVÉS. PERSONNEL. - DISPOSITIONS DE L'ARTICLE L. 914-1 DU CODE DE L'ÉDUCATION RENDANT APPLICABLES AUX MAÎTRES DE L'ENSEIGNEMENT PRIVÉ LES RÈGLES GÉNÉRALES ET MESURES SOCIALES APPLICABLES AUX MAÎTRES DE L'ENSEIGNEMENT PUBLIC - PORTÉE [RJ1] - RÈGLES CONCERNANT LES CONDITIONS DE SERVICE ET DE CESSATION D'ACTIVITÉ DES ENSEIGNANTS - RÈGLE DE MAINTIEN EN ACTIVITÉ DES PERSONNELS ENSEIGNANTS PRÉVUE PAR L'ARTICLE L. 921-4 DU MÊME CODE - INCLUSION - CONSÉQUENCE - RÈGLE APPLICABLE DE PLEIN DROIT AUX MAÎTRES CONTRACTUELS OU AGRÉÉS DES ÉTABLISSEMENTS D'ENSEIGNEMENT PRIVÉS SOUS CONTRAT.
</SCT>
<ANA ID="9A"> 30-01-02-01 Il résulte des dispositions de l'article L. 914-1 du code de l'éducation que la règle de maintien en activité, jusqu'au 31 août, des personnels enseignants qui remplissent en cours d'année scolaire les conditions d'âge pour obtenir la jouissance immédiate de leur pension énoncée par l'article L. 921-4 du code de l'éducation, qui concerne les conditions de service et de cessation d'activité des enseignants qu'elles visent, est applicable de plein droit aux maîtres contractuels ou agréés des établissements d'enseignement privés sous contrat.</ANA>
<ANA ID="9B"> 30-02-01-03 Il résulte des dispositions de l'article L. 914-1 du code de l'éducation que la règle de maintien en activité, jusqu'au 31 août, des personnels enseignants qui remplissent en cours d'année scolaire les conditions d'âge pour obtenir la jouissance immédiate de leur pension énoncée par l'article L. 921-4 du code de l'éducation, qui concerne les conditions de service et de cessation d'activité des enseignants qu'elles visent, est applicable de plein droit aux maîtres contractuels ou agréés des établissements d'enseignement privés sous contrat.</ANA>
<ANA ID="9C"> 30-02-07-01 Il résulte des dispositions de l'article L. 914-1 du code de l'éducation que la règle de maintien en activité, jusqu'au 31 août, des personnels enseignants qui remplissent en cours d'année scolaire les conditions d'âge pour obtenir la jouissance immédiate de leur pension énoncée par l'article L. 921-4 du code de l'éducation, qui concerne les conditions de service et de cessation d'activité des enseignants qu'elles visent, est applicable de plein droit aux maîtres contractuels ou agréés des établissements d'enseignement privés sous contrat.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de la portée de l'article L. 914-1 du code de l'éducation, CE, 9 mai 2012, Ministre de l'éducation nationale, de la jeunesse et de la vie associative c/ Jazet, n° 354473, à mentionner aux Tables ; CE, 16 février 2007, Mme Le Nouvel, n° 270497, T. pp. 878-901 ; s'agissant de la jurisprudence rendue sous l'empire de l'article 15 de la loi n° 59-1557 du 31 décembre 1959, CE, 30 mars 2001, Mme Bremond, n° 213830, T. pp. 980-983 ; CE, Assemblée, 5 décembre 1997, Union régionale des organismes de gestion des établissements d'enseignement catholique des Pays-de-Loire et autres, n° 174185, p. 478 ; CE, 30 novembre 1990, Ministre de l'éducation nationale c/ Mme Condat, n° 77701, T. p. 808 ; CE, 15 avril 1988, Mlle Chereau, n° 67327, T. pp. 823-924.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
