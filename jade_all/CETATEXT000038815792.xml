<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038815792</ID>
<ANCIEN_ID>JG_L_2019_07_000000417529</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/81/57/CETATEXT000038815792.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 24/07/2019, 417529, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417529</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP HEMERY, THOMAS-RAQUIN, LE GUERER ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2019:417529.20190724</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision n° 2015/31-003 du 27 janvier 2016, la chambre disciplinaire de première instance de l'ordre des masseurs-kinésithérapeutes de Midi-Pyrénées, statuant sur la plainte du conseil départemental de l'ordre des masseurs-kinésithérapeutes de la Haute-Garonne, a infligé à M. D...A...et à Mme C...E...la sanction du blâme, pour avoir, par contrat, mis une partie de leur cabinet à disposition de M. B...afin qu'il y pratique l'endermologie et accompagne l'usage de plateformes Huber et PowerPlate, alors qu'il ne possédait pas la qualité de masseur-kinésithérapeute. <br/>
<br/>
              Par une décision n° 005-2016 du 13 octobre 2017, la chambre disciplinaire nationale de l'ordre des masseurs-kinésithérapeutes a rejeté l'appel formé contre cette décision par le Conseil national de l'ordre des masseurs-kinésithérapeutes et l'appel incident formé par M. A...et MmeE.... <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 22 janvier et 20 avril 2018 au secrétariat du contentieux du Conseil d'Etat, le Conseil national de l'ordre des masseurs-kinésithérapeutes demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la décision de la chambre disciplinaire nationale de l'ordre des masseurs-kinésithérapeutes du 13 octobre 2017 ;<br/>
<br/>
              2°) de mettre à la charge de M. A...et de Mme E...la somme de 2 000 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Hémery, Thomas-Raquin, Le Guerer, avocat du Conseil national de l'ordre des masseurs-kinésithérapeutes et à la SCP Foussard, Froger, avocat de Mme E...et de M.A....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis à la chambre disciplinaire nationale de l'ordre des masseurs-kinésithérapeutes que, par un contrat prenant effet le 6 mai 2013, Mme E... et M.A..., masseurs-kinésithérapeutes, ont mis une partie de leur cabinet à la disposition de M.B..., qui n'avait pas la qualité de masseur-kinésithérapeute, afin qu'il y exerce " les missions relatives à la salle exploitée par les bénéficiaires : / - Endermologie / - Coach sur plateforme Huber et Power Plate ". Par une décision du 27 janvier 2016, la chambre disciplinaire de première instance de l'ordre des masseurs-kinésithérapeutes de Midi-Pyrénées, saisie d'une plainte du conseil départemental de l'ordre des masseurs-kinésithérapeutes de Haute-Garonne, a retenu que les prestations en cause ne pouvaient être assurées que par un masseur-kinésithérapeute, et que Mme E...et M. A...avaient commis une faute en permettant à M. B...de les assurer dans leur cabinet et a infligé à ces derniers la sanction du blâme. Le Conseil national de l'ordre des masseurs-kinésithérapeutes se pourvoit en cassation contre la décision du 28 novembre 2017 par laquelle la chambre disciplinaire nationale de l'ordre des masseurs-kinésithérapeutes, après avoir jugé que les missions confiées à M. B...ne relevaient pas d'un exercice illégal de la masso-kinésithérapie, s'est fondée sur d'autres griefs, tirés d'une violation du secret professionnel, d'une publicité indirecte et de la non-communication du contrat aux instances de l'ordre, pour prononcer la même sanction contre Mme E...et M.A....<br/>
<br/>
              2. Aux termes des deux premiers alinéas de l'article L. 4321-1 du code de la santé publique, dans leur rédaction antérieure à la loi du 26 janvier 2016 de modernisation de notre système de santé, applicable à la date des faits reprochés aux intéressés : " La profession de masseur-kinésithérapeute consiste à pratiquer habituellement le massage et la gymnastique médicale. / La définition du massage et de la gymnastique médicale est précisée par un décret en Conseil d'Etat, après avis de l'Académie nationale de médecine ". Aux termes de l'article L. 4321-2 : " Peuvent exercer la profession de masseur-kinésithérapeute les personnes titulaires d'un diplôme, certificat ou titre mentionné aux articles L. 4321-3 et L. 4321-4 ou titulaires des autorisations mentionnées aux articles L. 4321-5 à L. 4321-7 ". Aux termes de l'article R. 4321-3 : " On entend par massage toute manoeuvre externe, réalisée sur les tissus, dans un but thérapeutique ou non, de façon manuelle ou par l'intermédiaire d'appareils autres que les appareils d'électrothérapie, avec ou sans l'aide de produits, qui comporte une mobilisation ou une stimulation méthodique, mécanique ou réflexe de ces tissus ". Aux termes de l'article R. 4321-4 : " On entend par gymnastique médicale la réalisation et la surveillance des actes à visée de rééducation neuromusculaire, corrective ou compensatrice, effectués dans un but thérapeutique ou préventif afin d'éviter la survenue ou l'aggravation d'une affection. Le masseur-kinésithérapeute utilise à cette fin des postures et des actes de mobilisation articulaire passive, active, active aidée ou contre résistance, à l'exception des techniques ergothérapiques ".<br/>
<br/>
              3. Il ressort des pièces du dossier soumis à la chambre disciplinaire nationale que la marque déposée " endermologie " désigne une technique de stimulation mécanique de la peau au moyen de rouleaux mobiles couplés à un système d'aspiration et que les marques déposées " Huber " et " Power Plate " désignent des dispositifs destinés à améliorer la mobilité et le tonus musculaire de l'usager au moyen de plateformes vibrantes ou oscillantes. Si la chambre disciplinaire nationale a pu sans erreur de qualification juridique retenir que ces derniers dispositifs, dont il ne ressortait pas des pièces du dossier qui lui était soumis et n'était pas même allégué qu'ils soient utilisés dans un but thérapeutique ou préventif afin d'éviter la survenue ou l'aggravation d'une affection, ne répondaient pas à la définition réglementaire de la gymnastique médicale et en déduire que Mme E...et M. A...avaient pu autoriser M. B... à les mettre en oeuvre dans leur cabinet sans se rendre coupables de complicité d'exercice illégal de la masso-kinésithérapie, elle a en revanche inexactement qualifié les faits de l'espèce en écartant ce même grief pour les appareils d'endermologie, dont la mise en oeuvre répond à la définition réglementaire du massage et relève, par suite, du monopole professionnel institué par la combinaison des articles L. 4321-1 et L. 4321-2 du code de la santé publique, dans leur rédaction alors en vigueur. Il y a lieu, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, d'annuler sa décision.<br/>
<br/>
              4. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme E...et de M. A...la somme demandée par le Conseil national de l'ordre des masseurs-kinésithérapeutes au titre de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à la charge du Conseil national de l'ordre des masseurs-kinésithérapeutes, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La décision n° 005-2016 du 13 octobre 2017 de la chambre disciplinaire nationale de l'ordre des masseurs-kinésithérapeutes est annulée.<br/>
Article 2 : L'affaire est renvoyée devant la chambre disciplinaire nationale de l'ordre des masseurs-kinésithérapeutes. <br/>
Article 3 : Les conclusions présentées par le Conseil national de l'ordre des masseurs-kinésithérapeutes et par M. A...et Mme E...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au Conseil national de l'ordre des masseurs-kinésithérapeutes, à M. D...A..., à Mme C...E..., au conseil départemental de l'ordre des masseurs-kinésithérapeutes de la Haute-Garonne et à la chambre disciplinaire de première instance du conseil de l'ordre des masseurs-kinésithérapeutes de Midi-Pyrénées.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
