<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032175836</ID>
<ANCIEN_ID>JG_L_2016_03_000000396726</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/17/58/CETATEXT000032175836.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 03/03/2016, 396726, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-03-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396726</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP TIFFREAU, MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2016:396726.20160303</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 3 et 24 février 2016 au secrétariat du contentieux du Conseil d'Etat, le Conseil national des professions de l'automobile (CNPA), représenté par son président, demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution du décret n° 2015-1571 du 1er décembre 2015 relatif aux conditions d'application de l'article L. 213-2 du code de la route ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence est remplie, dès lors que le décret litigieux préjudicie de manière grave et immédiate à ses intérêts, notamment financiers ; <br/>
              - il porte atteinte à la liberté du commerce et de l'industrie ;<br/>
              - il existe un doute sérieux quant à la légalité du décret contesté ;<br/>
              - le décret contesté est entaché d'irrégularités dès lors que l'existence de l'avis de l'Autorité de la concurrence n'est pas établie et que sont absents les contreseings du ministre de l'intérieur et du secrétaire d'Etat aux transports, pourtant chargés de son exécution ;<br/>
              - il est entaché d'une erreur de droit dès lors qu'il méconnaît les dispositions de l'article L. 213-2 du code de la route dont il dénature la portée ;<br/>
              - le décret contesté méconnaît le principe de liberté du commerce et de l'industrie.<br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 18 février 2016, le ministre de l'économie et des finances conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par le requérant ne sont pas fondés.<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, le CNPA, d'autre part, le Premier ministre et le ministre de l'économie et des finances ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 25 février 2016 à 10 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me de la Burgade, avocat au Conseil d'Etat et à la Cour de cassation, avocat du Conseil national des professions de l'automobile ;<br/>
<br/>
              - les représentants du Conseil national des professions de l'automobile ;<br/>
<br/>
              - les représentants du ministre de l'économie et des finances ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a différé la clôture de l'instruction jusqu'au 1er mars à 12 heures ;<br/>
<br/>
              Vu le mémoire, enregistré le 29 février 2016, présenté par le CNPA, qui conclut aux mêmes fins que la requête, par les mêmes moyens ;<br/>
<br/>
              Vu le mémoire, enregistré le 1er mars 2016, présenté par le ministre de l'économie et des finances qui conclut aux mêmes fins que son précédent mémoire, par les mêmes moyens. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la route ;<br/>
              - le code de commerce ;<br/>
              - la loi n° 2015-990 du 6 août 2015 ;<br/>
              - le décret n° 2015-1571 du 1er décembre 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 213-1 du code de la route dans sa rédaction résultant de la loi du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques " L'enseignement, à titre onéreux, de la conduite des véhicules à moteur d'une catégorie donnée et de la sécurité routière ainsi que l'animation des stages de sensibilisation à la sécurité routière (...) ne peuvent être organisés que dans le cadre d'un établissement dont l'exploitation est subordonnée à un agrément délivré par l'autorité administrative, après avis d'une commission. " ; que, selon le troisième alinéa de l'article L. 213-2 du même code, dans sa rédaction résultant de la même loi, " La présentation du candidat aux épreuves du permis de conduire ne peut donner lieu à l'application d'aucuns frais. Les frais facturés au titre de l'accompagnement du candidat à l'épreuve sont réglementés dans les conditions prévues au deuxième alinéa de l'article L. 410-2 du code de commerce. " ; qu'aux termes du 2ème alinéa de l'article L. 410-2 du code de commerce " (...) Dans les secteurs ou les zones où la concurrence par les prix est limitée en raison (...)  de dispositions législatives ou réglementaires, un décret en Conseil d'Etat peut réglementer les prix après consultation de l'Autorité de la concurrence " ;<br/>
<br/>
              3. Considérant que pour l'application des dispositions citées ci-dessus de l'article L. 213-2 du code de la route, le décret du 1er décembre 2015 susvisé, a créé dans la partie règlementaire de ce code un article R. 213-3-3, dont le II dispose que " II.-Les frais appliqués au titre de l'accompagnement du candidat à l'épreuve sont déterminés préalablement à cette prestation./Pour la partie pratique, ils couvrent forfaitairement l'ensemble de la charge de l'accompagnement, tant à l'épreuve en circulation que, le cas échéant, à celle hors circulation. Ils ne peuvent excéder les prix appliqués par l'établissement pour les durées de formation suivantes :-pour le permis des catégories A1, A2, A et BE : une heure et demie ;-pour le permis des catégories B1 et B : une heure ;-pour les permis des catégories C1, C, D1 et D : deux heures ;-pour les permis des catégories C1E, CE, D1E et DE : deux heures et demie. " ; que le conseil national des professions de l'automobile demande la suspension de ces dispositions réglementaires ;<br/>
<br/>
              4. Considérant en premier lieu que le moyen tiré de ce que l'Autorité de la concurrence n'aurait pas été consultée sur le décret litigieux préalablement à son édiction manque en fait ; qu'en deuxième lieu, aucune des dispositions règlementaires citées ci-dessus n'appelle nécessairement l'intervention de mesures que le ministre de l'intérieur serait compétent pour signer ou contresigner, le requérant ne pouvant, par ailleurs, utilement soutenir que ce décret devait être contresigné par le secrétaire d'Etat chargé des transports ; qu'il n'apparaît pas plus, en troisième lieu, en l'état de l'instruction, qu'est de nature à créer un doute sérieux sur la légalité de ces dispositions le moyen tiré de ce qu'en plafonnant ainsi qu'il l'a fait les sommes susceptibles d'être réclamées aux candidats à l'épreuve du permis de conduire au titre des frais d'accompagnement à cette épreuve, le Premier ministre aurait dénaturé les termes de l'article L. 213-2 selon lesquels ces frais sont " réglementés " ; que n'est pas davantage de nature à créer un tel doute, eu égard notamment à la réglementation du secteur de l'enseignement de la conduite automobile, en particulier en vertu de l'article L. 213-1 du code de la route, le moyen tiré de ce que les dispositions litigieuses méconnaitraient la liberté du commerce et de l'industrie ; <br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur la condition tenant à l'existence d'une situation d'urgence, qu'aucun moyen invoqué par le Conseil national des professions de l'automobile à l'appui de sa demande de suspension n'est de nature à créer un doute sérieux quant à la légalité du décret contesté ; que, par suite, sa requête ne peut qu'être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête du Conseil national des professions de l'automobile est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée au Conseil national des professions de l'automobile et au ministre de l'économie et des finances.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
