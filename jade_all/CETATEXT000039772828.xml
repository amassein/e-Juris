<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039772828</ID>
<ANCIEN_ID>JG_L_2019_12_000000417160</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/77/28/CETATEXT000039772828.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 27/12/2019, 417160, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-12-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417160</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Thomas Janicot</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:417160.20191227</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire, un mémoire en réplique et un autre mémoire, enregistrés le 9 janvier 2018, le 9 avril 2018, le 22 octobre 2018 et le 16 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, l'Association des producteurs de Bourgogne en Beaujolais demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêté du 30 octobre 2017 modifiant le cahier des charges de l'appellation d'origine contrôlée " Bourgogne ", homologué par le décret n° 2011-1615 du 22 novembre 2011 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le décret n° 2011-1615 du 22 novembre 2011 ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Janicot, auditeur,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de l'Association des producteurs de Bourgogne en Beaujolais et à la SCP Didier, Pinet, avocat de l'Institut national de l'origine et de la qualité ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par arrêté du 30 octobre 2017, le ministre de l'agriculture et de l'alimentation, le ministre de l'économie et des finances et le ministre de l'action et des comptes ont homologué le cahier des charges de l'appellation d'origine contrôlée " Bourgogne ", modifiant celui qui avait été homologué par le décret n° 2011-1615 du 22 novembre 2011. L'Association des producteurs de Bourgogne en Beaujolais demande au Conseil d'Etat l'annulation pour excès de pouvoir de cet arrêté. Eu égard aux termes dans lesquels elle est formulée, sa requête doit être regardée comme tendant à l'annulation de l'arrêté attaqué en tant que le cahier des charges qu'il homologue impose de faire suivre le nom de l'appellation d'origine contrôlée " Bourgogne " de l'indication " gamay " pour les vins issus des aires parcellaires délimitées relevant des appellations d'origine contrôlée " Brouilly ", " Chénas ", " Chiroubles ", " Côtes de Brouilly ", " Fleurie ", " Juliénas ", " Morgon ", " Moulin-à-Vent ", " Régnié " et " Saint-Amour ", impose de maintenir la proportion du cépage gamay N à un niveau inférieur ou égal à 30 % dans l'encépagement et dans l'assemblage des vins et impose d'inscrire l'indication " gamay " immédiatement au-dessous du nom de l'appellation d'origine contrôlée avec des caractères dont les dimensions, aussi bien en hauteur qu'en largeur, sont égales à celles des caractères du nom de l'appellation d'origine contrôlée. <br/>
<br/>
              2. Pour intervenir au soutien des conclusions de l'Association des producteurs de Bourgogne en Beaujolais, la commune de Lachassagne et les autres intervenants justifient d'un intérêt suffisant à l'annulation de l'arrêté attaqué. Par suite, leur intervention est recevable. <br/>
<br/>
              3. Dans sa décision du 6 mars 2014 dans l'affaire n° 356101, l'Association des producteurs de Bourgogne en Beaujolais, le Conseil d'Etat statuant au contentieux a annulé le décret du 22 novembre 2011 relatif à l'appellation d'origine contrôlée " Bourgogne ", d'une part, en tant qu'il homologue les dispositions du cahier des charges relatives à l'aire géographique de l'appellation " Bourgogne " excluant les communes du département du Rhône qui figuraient dans l'aire géographique délimitée par le cahier des charges homologué par le décret du 16 octobre 2009 relatif aux appellations d'origine contrôlée " Bourgogne ", " Bourgogne grand ordinaire ", " Bourgogne ordinaire ", " Bourgogne Passe-tout-grains " et " Bourgogne aligoté " et les dispositions qui imposent des contraintes particulières aux exploitants dont les parcelles sont situées sur le territoire de communes du département du Rhône maintenu dans l'aire et, d'autre part, en tant qu'il abroge les dispositions du cahier des charges annexé à ce dernier décret relatives à cette même aire géographique de production. <br/>
<br/>
              4. Les " contraintes particulières " dont l'homologation a été annulée par la décision visée au point précédent sont, à la lumière des conclusions sur lesquelles elle a statué, celles qui résultaient des dispositions figurant au 2° du IV du chapitre Ier du cahier des charges de l'appellation d'origine contrôlée " Bourgogne ", imposant, pour le vin rouge et le vin rosé, l'obligation de déclaration d'affectation des parcelles, et au point 1.2 du I du chapitre II du même cahier des charges, imposant, pour le vin blanc, l'obligation d'identification des parcelles dont les raisins sont issus. Ces dispositions sont sans rapport avec celles dont l'Association des producteurs de Bourgogne en Beaujolais conteste l'homologation, par la présente requête, et qui figuraient au 3° du II, au 2° du V, au 1° a) du IX et au 2° c) du XII du chapitre Ier du cahier des charges homologué en 2011. La décision du 6 mars 2014 n'imposait donc pas de supprimer ces dispositions du cahier des charges homologué en 2011. Par ailleurs, il ressort des pièces du dossier que le cahier des charges homologué par l'arrêté contesté s'est borné à reprendre ces dispositions, sans les modifier. Par suite, les moyens tirés de ce que cet arrêté est irrégulier, faute pour la procédure nationale d'opposition ayant précédé son adoption d'avoir porté sur ces dispositions, de ce qu'il méconnaît l'autorité de la chose jugée par la décision du 6 mars 2014 et de ce qu'il méconnaît les principes de sécurité juridique et de confiance légitime en n'assortissant pas ces dispositions de mesures transitoires en faveur des exploitants situés dans les communes du Beaujolais ne peuvent qu'être écartés.<br/>
<br/>
              5. Si l'Association des producteurs de Bourgogne en Beaujolais soutient que la procédure nationale d'opposition a été également irrégulière en ce que l'INAO ne justifierait pas d'une publication de l'annonce d'ouverture de cette procédure au Bulletin officiel de la propriété industrielle, en méconnaissance de l'article R. 641-13 du code rural et de la pêche maritime dans sa rédaction applicable lors de l'ouverture de ladite procédure, ce moyen manque en fait.<br/>
<br/>
              6. Il résulte de tout ce qui précède que l'Association des producteurs de Bourgogne en Beaujolais n'est pas fondée à demander l'annulation de l'arrêté qu'elle attaque. Sans qu'il soit besoin de se prononcer sur la fin de non-recevoir du ministre de l'action et des comptes publics, la requête de l'Association des producteurs de Bourgogne en Beaujolais doit donc être rejetée. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soient mises à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante, les sommes que demandent l'Association des producteurs de Bourgogne en Beaujolais et, en tout état de cause, les intervenants. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'intervention de la commune de Lachassagne et autres est admise.<br/>
<br/>
Article 2 : La requête de l'Association des producteurs de Bourgogne en Beaujolais est rejetée.<br/>
<br/>
Article 3 : Les conclusions présentées par la commune de Lachassagne et les autres intervenants au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à l'Association des producteurs de Bourgogne en Beaujolais, au ministre de l'agriculture et de l'alimentation, au ministre de l'économie et des finances, au ministre de l'action et des comptes publics, à l'Institut national de l'origine et de la qualité et au Syndicat des bourgognes, ainsi qu'à la commune de Lachassagne, première dénommée, pour l'ensemble des intervenants.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
