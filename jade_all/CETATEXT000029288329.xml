<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029288329</ID>
<ANCIEN_ID>JG_L_2014_07_000000379875</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/28/83/CETATEXT000029288329.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème sous-section jugeant seule, 23/07/2014, 379875, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>379875</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:379875.20140723</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
       L'association " Section française de l'Observatoire international des prisons " (OIP-SF) a demandé au juge des référés du tribunal administratif de Rennes de prescrire :<br/>
<br/>
       - sur le fondement des dispositions de l'article L. 521-3 du code de justice administrative, toute mesure utile pour permettre aux personnes détenues au centre pénitentiaire de Rennes-Vezin d'utiliser les téléphones mis à leur disposition dans des conditions garantissant la confidentialité des conversations et, plus précisément, de faire procéder au cloisonnement des " points-phones " par un dispositif d'isolation phonique de type cabine téléphonique ou tout autre dispositif équivalent permettant de garantir la confidentialité des échanges, dans un délai de trois mois ;<br/>
<br/>
       - à titre subsidiaire et dans l'attente du cloisonnement des " points-phones ", la création de plusieurs locaux disposant d'un téléphone susceptibles d'être accessibles sur demande afin de s'entretenir confidentiellement avec leurs avocats, ainsi que d'informer par voie d'affiches les détenus de leur droit d'en disposer pour téléphoner à leurs avocats.<br/>
<br/>
       Par une ordonnance n° 1401157 du 23 avril 2014, le juge des référés du tribunal administratif de Rennes a enjoint au directeur du centre pénitentiaire de Rennes-Vezin, sous trois mois :<br/>
<br/>
       - de mettre en oeuvre toute mesure permettant aux personnes détenues, dès qu'elles entendent avoir une communication téléphonique avec leur avocat, de mener celle-ci confidentiellement à l'égard tant de leurs codétenus que de l'administration pénitentiaire ;<br/>
<br/>
       - de mettre en oeuvre toute mesure permettant, dans les cas justifiés, aux personnes détenues de mener des conversations avec leurs familles en préservant la confidentialité de celles-ci à l'égard de leurs codétenus.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
       Par un pourvoi et un mémoire en réplique, enregistrés les 9 mai et 3 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, la garde des sceaux, ministre de la justice, demande au Conseil d'Etat d'annuler cette ordonnance n°1401157 du juge des référés du tribunal administratif de Rennes du 23 avril 2014.<br/>
<br/>
<br/>
       Vu :<br/>
       - les autres pièces du dossier ;<br/>
       - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, notamment son article 8 ;<br/>
       - le code de procédure pénale ;<br/>
       - la loi n°2009-1436 du 24 novembre 2009 ;<br/>
       - l'avis du 10 janvier 2011 du contrôleur général des lieux de privation de liberté relatif à l'usage du téléphone par les personnes privées de liberté ;<br/>
       - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
       Après avoir entendu en séance publique :<br/>
<br/>
       - le rapport de Mme Isabelle Lemesle, Maître des Requêtes,<br/>
<br/>
       - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
       La parole ayant été donnée, avant et après les conclusions, à Me Spinosi, avocat de l'association " Section française de l'Observatoire international des prisons " (OIP-SF) et à la SCP Masse-Dessen, Thouvenin, Coudray, avocat du Syndicat des avocats de France.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
       1. Aux termes de l'article L. 521-3 du code de justice administrative : " En cas d'urgence et sur simple requête qui sera recevable même en l'absence de décision administrative préalable, le juge des référés peut ordonner toutes autres mesures utiles sans faire obstacle à l'exécution d'aucune décision administrative ". <br/>
<br/>
Sur la condition d'urgence :<br/>
<br/>
       2. Par l'ordonnance attaquée, le juge des référés du tribunal administratif de Rennes a estimé qu'il ressortait des pièces du dossier qui lui était soumis, notamment du rapport établi par un expert désigné par le tribunal dans une instance impliquant un détenu au.... Il a ensuite relevé qu'il y avait urgence à ce que les personnes détenues au centre pénitentiaire de Rennes-Vezin puissent bénéficier des droits que leur reconnaît la loi en ce qui concerne les conversations téléphoniques et a estimé que la circonstance que ces droits soient méconnus depuis plusieurs années ne saurait faire disparaître l'urgence. Il a également relevé que l'administration ne saurait se fonder sur la présence de téléphones cellulaires au sein des centres de détention, situation de fait illégale, pour s'abstenir de respecter les droits reconnus aux personnes détenues par la loi et pour contester l'urgence de permettre à ces personnes d'être remplies de leurs droits. En s'appuyant sur ces éléments pour juger que la condition d'urgence exigée par l'article L. 521-3 du code de justice était satisfaite, le juge des référés du tribunal administratif, qui n'était pas tenu de répondre à chacun des arguments avancés en défense par la garde des sceaux, a suffisamment motivé son ordonnance et porté sur les faits de l'espèce une appréciation souveraine exempte de dénaturation.<br/>
<br/>
Sur l'utilité de la mesure ordonnée :<br/>
<br/>
       3. A l'appui de son moyen tiré de ce que le juge des référés du tribunal administratif a dénaturé les faits de l'espèce en estimant que la mesure demandée était utile, la garde des sceaux, ministre de la justice, soutient, d'une part, qu'il ne ressort pas de l'expertise réalisée que la confidentialité des conversations téléphoniques soit totalement impossible au sein du centre pénitentiaire de Rennes-Vezin et, d'autre part, que les particularités du régime " maison d'arrêt " permettent la plupart du temps la confidentialité des conversations téléphoniques dans les quartiers " maison d'arrêt " du centre pénitentiaire. Elle ne conteste toutefois pas que la confidentialité des conversations téléphoniques ne soit pas, en règle générale, garantie, que ce soit dans les quartiers " maison d'arrêt " du centre pénitentiaire ou, à plus forte raison, dans les autres quartiers. De plus, contrairement à ce que soutient la ministre, le juge des référés du tribunal administratif n'a pas considéré que le téléphone fût le seul moyen à la disposition des détenus pour communiquer confidentiellement avec leurs avocats. Dans ces conditions, la ministre n'est pas fondée à soutenir que le juge des référés du tribunal administratif aurait dénaturé les faits de l'espèce en estimant que la mesure qui lui demandée était utile.<br/>
<br/>
       4. En outre, lorsqu'il se prononce sur le caractère utile de la mesure sollicitée par un requérant sur le fondement de l'article L. 521-3 du code de justice administrative, le juge des référés se livre à une appréciation souveraine qui ne peut, en l'absence de dénaturation, être discutée devant le juge de cassation. Par suite, le moyen d'erreur de qualification juridique des faits soulevé par la ministre ne peut être accueilli.<br/>
<br/>
Sur le bien-fondé de la mesure ordonnée :<br/>
<br/>
       5. Aux termes du 1 de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " Toute personne a droit au respect de sa vie privée et familiale, de son domicile et de sa correspondance ". Aux termes de l'article 22 de la loi du 24 novembre 2009 : " L'administration pénitentiaire garantit à toute personne détenue le respect de sa dignité et de ses droits. L'exercice de ceux-ci ne peut faire l'objet d'autres restrictions que celles résultant des contraintes inhérentes à la détention, du maintien de la sécurité et du bon ordre des établissements, de la prévention de la récidive et de la protection de l'intérêt des victimes. ". Aux termes de l'article 25 de la même loi : " Les personnes détenues communiquent librement avec leurs avocats ". Aux termes de l'article R. 57-6-6 du code de procédure pénale : " La communication se fait verbalement ou par écrit. Aucune sanction ni mesure ne peut supprimer ou restreindre la libre communication de la personne détenue avec son conseil. ". Enfin, les dispositions de l'article 727-1 du code de procédure pénale prévoient que, par dérogation au droit commun, les conversations téléphoniques des détenus avec leurs avocats ne peuvent être écoutées, enregistrées ou interrompues par l'administration pénitentiaire. Il résulte de l'ensemble de ces stipulations et dispositions que les personnes détenues ont le droit de s'entretenir par téléphone avec leurs avocats, de façon confidentielle tant vis-à-vis de l'administration pénitentiaire que des autres personnes détenues. Par suite, en jugeant que la confidentialité des conversations téléphoniques entre les détenus et leurs avocats devait être assurée vis-à-vis des autres personnes détenues, le juge des référés du tribunal administratif n'a pas commis d'erreur de droit.<br/>
<br/>
       6. Il ressort des termes mêmes du dispositif de l'ordonnance attaquée que le juge des référés a enjoint au directeur du centre pénitentiaire de Rennes-Vezin, de mettre en oeuvre toute mesure permettant aux personnes détenues, dès qu'elles entendent avoir une communication téléphonique avec leur avocat, de mener celle-ci confidentiellement à l'égard tant de leurs codétenus que des membres de l'administration pénitentiaire et toute mesure permettant, dans les cas justifiés, aux personnes détenues de mener des conversations avec leurs familles en préservant la confidentialité de celles-ci à l'égard de leurs codétenus. S'il est indiqué dans les motifs de l'ordonnance que la mise en place de cabines téléphoniques constitue un exemple de mesure envisageable, le juge des référés a laissé à l'administration le soin de mettre en oeuvre les mesures adéquates de son choix, compte tenu de ses contraintes. Il ne saurait donc être utilement soutenu qu'il a dénaturé les faits de l'espèce en ordonnant le remplacement des " points phones " par des cabines téléphonique, sans tenir compte des difficultés économiques ou du risque sécuritaire. <br/>
<br/>
       7. Il résulte de ce qui précède que la garde des sceaux, ministre de la justice, n'est pas fondée à demander l'annulation de l'ordonnance qu'elle attaque.<br/>
<br/>
Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
       8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 2 000 euros à verser à l'association " Section française de l'Observatoire international des prisons " (OIP-SF), d'une part, et au syndicat des avocats de France, d'autre part, au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la garde des sceaux, ministre de la justice, est rejeté.<br/>
Article 2 : L'Etat versera à l'association " Section française de l'Observatoire international des prisons " (OIP-SF), d'une part, et au Syndicat des avocats de France, d'autre part, une somme de 2 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la garde des sceaux, ministre de la justice, à l'association " Section française de l'Observatoire international des prisons " (OIP-SF) et au Syndicat des avocats de France.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
