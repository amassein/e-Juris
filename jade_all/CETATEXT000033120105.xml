<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033120105</ID>
<ANCIEN_ID>JG_L_2016_09_000000400684</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/12/01/CETATEXT000033120105.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 14/09/2016, 400684, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-09-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400684</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Frédéric Puigserver</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:400684.20160914</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La commune de Coti-Chiavari, à l'appui de sa demande tendant à l'annulation pour excès de pouvoir de la délibération du 2 octobre 2015 par laquelle l'assemblée de Corse a approuvé le plan d'aménagement et de développement durable de la Corse, a produit un mémoire, enregistré le 14 mai 2016 au greffe du tribunal administratif de Bastia, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lequel elle soulève une question prioritaire de constitutionnalité.<br/>
<br/>
              Par une ordonnance n° 1600645 du 13 juin 2016, enregistrée le 15 juin 2016 au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de Bastia, avant qu'il soit statué sur sa demande de la commune de Coti-Chiavari, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du I de l'article L. 4424-9, du II de l'article L. 4424-11 et du I de l'article L. 4424-12 du code général des collectivités territoriales.<br/>
<br/>
              Dans la question prioritaire de constitutionnalité transmise et dans deux mémoires, enregistrés les 27 juillet et 1er septembre 2016 au secrétariat du contentieux du Conseil d'Etat, la commune de Coti-Chiavari soutient que les dispositions, applicables au litige, du I de l'article L. 4424-9, du II de l'article L. 4424-11 et du I de l'article L. 4424-12 du code général des collectivités territoriales, qui n'encadrent pas la précision des documents cartographiques du plan d'aménagement et de développement durable de la Corse, sont entachées d'incompétence négative et portent atteinte aux principes de libre administration des collectivités territoriales et de non-tutelle d'une collectivité territoriale sur une autre, protégés par l'article 72 de la Constitution.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des collectivités territoriales ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Puigserver, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la commune de Coti-Chiavari ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes du I de l'article L. 4424-9 du code général des collectivités territoriales : " La collectivité territoriale de Corse élabore le plan d'aménagement et de développement durable de Corse. / Le plan (...) fixe les orientations fondamentales en matière de protection et de mise en valeur du territoire, de développement agricole, rural et forestier, de pêche et d'aquaculture, d'habitat, de transports, d'intermodalité d'infrastructures et de réseaux de communication et de développement touristique. / Il définit les principes de l'aménagement de l'espace qui en résultent et il détermine notamment les espaces naturels, agricoles et forestiers ainsi que les sites et paysages à protéger ou à préserver, l'implantation des grandes infrastructures de transport et des grands équipements, la localisation préférentielle ou les principes de localisation des extensions urbaines, des activités industrielles, artisanales, commerciales, agricoles, forestières, touristiques, culturelles et sportives. / La destination générale des différentes parties du territoire de l'île fait l'objet d'une carte, dont l'échelle est déterminée par délibération de l'Assemblée de Corse dans le respect de la libre administration des communes et du principe de non-tutelle d'une collectivité sur une autre, et que précisent, le cas échéant, les documents cartographiques prévus à l'article L. 4424-10 et au II de l'article L. 4424-11. (...) ". Aux termes du II de l'article L. 4424-11 du même code : " Le plan d'aménagement et de développement durable de Corse peut, compte tenu du caractère stratégique au regard des enjeux de préservation ou de développement présentés par certains espaces géographiques limités, définir leur périmètre, fixer leur vocation et comporter des dispositions relatives à l'occupation du sol propres auxdits espaces, assorties, le cas échéant, de documents cartographiques dont l'objet et l'échelle sont déterminés par délibération de l'Assemblée de Corse. (...) ". Aux termes du I de l'article L. 4424-12 de ce code, dans sa rédaction applicable au litige : " Le plan d'aménagement et de développement durable peut, par une délibération particulière et motivée de l'Assemblée de Corse, fixer, pour l'application du premier alinéa de l'article L. 146-6 du code de l'urbanisme, une liste complémentaire à la liste des espaces terrestres et marins, sites et paysages remarquables ou caractéristiques du patrimoine naturel et culturel du littoral et des milieux nécessaires au maintien des équilibres biologiques à préserver. Cette délibération tient lieu du décret prévu au premier alinéa du même article L. 146-6. Elle définit également leur localisation ".<br/>
<br/>
              3. Les dispositions du I de l'article L. 4424-9, du II de l'article L. 4424-11 et du I de l'article L. 4424-12 du code général des collectivités territoriales sont applicables au litige dont est saisi le tribunal administratif de Bastia. Elles n'ont pas déjà été déclarées conformes à la Constitution par le Conseil constitutionnel. Le moyen tiré de ce qu'elles portent atteinte aux droits et libertés garantis par la Constitution soulève une question présentant un caractère sérieux. Ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée par la commune de Coti-Chiavari.<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : La question de la conformité à la Constitution des dispositions du I de l'article L. 4424-9, du II de l'article L. 4424-11 et du I de l'article L. 4424-12 du code général des collectivités territoriales est renvoyée au Conseil constitutionnel. <br/>
Article 2 : La présente décision sera notifiée à la commune de Coti-Chiavari et à la collectivité territoriale de Corse.<br/>
Copie en sera adressée au Premier ministre, à la ministre du logement et de l'habitat durable ainsi qu'au tribunal administratif de Bastia.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
