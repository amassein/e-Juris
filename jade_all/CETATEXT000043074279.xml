<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043074279</ID>
<ANCIEN_ID>JG_L_2021_01_000000437728</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/07/42/CETATEXT000043074279.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 26/01/2021, 437728, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-01-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437728</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SARL MEIER-BOURDEAU, LECUYER ET ASSOCIES</AVOCATS>
<RAPPORTEUR>Mme Rose-Marie Abel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:437728.20210126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              L'EARL la Cave du Maître A... a demandé au tribunal administratif de Dijon, à titre principal, d'annuler le titre de recettes émis le 24 juin 2016 par le directeur général de l'établissement public national des produits de l'agriculture et de la mer (FranceAgriMer) en vue du remboursement d'une aide à l'investissement viticole, ainsi que la décision implicite de cette autorité ayant rejeté ses recours gracieux, et, à titre subsidiaire, de réduire la somme à restituer à un euro symbolique.<br/>
<br/>
              Par un jugement n° 1603158 du 22 mai 2017, le tribunal administratif de Dijon a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17LY03457 du 18 novembre 2019, la cour administrative d'appel de Lyon a rejeté l'appel formé l'EARL la Cave du Maître A... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique enregistrés les 17 janvier, 17 avril 2020 et 12 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, l'EARL la Cave du Maître A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de FranceAgriMer la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement (CE, Euratom) n° 2988/95 du Conseil du 18 décembre 1995 ;<br/>
              - le règlement (CE) n° 1234/2007 du Conseil du 22 octobre 2007 ;<br/>
              - le règlement (CE) n° 555/2008 de la Commission du 27 juin 2008 ;<br/>
              - le code civil ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - le décret n° 2009178 du 16 février 2009 ;<br/>
              - l'arrêté du 17 avril 2009 définissant les conditions de mise en oeuvre de la mesure de soutien aux investissements éligibles au financement par les enveloppes nationales en application du règlement (CE) n° 479/2008 du Conseil du 29 avril 2008 portant organisation commune du marché vitivinicole ;<br/>
              - l'arrêt C-59/14 du 6 octobre 2015 de la Cour de justice de l'Union européenne ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Rose-Marie Abel, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à Me Le Prado, avocat de l'EARL la Cave du Maître A... et à la SARL Meier-Bourdeau, Lecuyer et associés, avocat de l'établissement public national des produits de l'agriculture et de la mer (FranceAgriMer) ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que l'EARL la Cave du Maître A... a bénéficié d'une aide à l'investissement agricole accordée par décision de l'établissement public national des produits de l'agriculture et de la mer (FranceAgriMer) du 11 mai 2010 pour un montant maximum de 76 123,43 euros. Après une avance de 38 061,72 euros versée le 31 août 2010, un solde de 29 820,35 euros a été versé le 19 septembre 2013. A la suite d'un contrôle sur place réalisé le 11 décembre 2014, FranceAgriMer a émis, le 20 juin 2016, un titre de recettes pour le remboursement de cette aide au motif de diverses irrégularités. Par un jugement du 22 mai 2017, le tribunal administratif de Dijon a rejeté la demande de l'EARL la Cave du Maître A... tendant à l'annulation de ce titre de recettes ou, à titre subsidiaire, à la réduction de la somme à restituer à un euro symbolique. L'EARL la Cave du Maître A... se pourvoit en cassation contre l'arrêt du 18 novembre 2019 par lequel la cour administrative d'appel de Lyon a rejeté l'appel qu'elle avait formé contre ce jugement.<br/>
<br/>
              2. Aux termes de l'article 1er du règlement (CE, Euratom) n° 2988/95 du 18 décembre 1995 relatif à la protection des intérêts financiers des Communautés européennes : " 1. Aux fins de la protection des intérêts financiers des Communautés européennes, est adoptée une réglementation générale relative à des contrôles homogènes et à des mesures et des sanctions administratives portant sur des irrégularités au regard du droit communautaire. / 2. Est constitutive d'une irrégularité toute violation d'une disposition du droit communautaire résultant d'un acte ou d'une omission d'un opérateur économique qui a ou aurait pour effet de porter préjudice au budget général des Communautés ou à des budgets gérés par celles-ci, soit par la diminution ou la suppression de recettes provenant des ressources propres perçues directement pour le compte des Communautés, soit par une dépense indue ". Aux termes de l'article 3 du même règlement : " 1. Le délai de prescription des poursuites est de quatre ans à partir de la réalisation de l'irrégularité visée à l'article 1er paragraphe 1. Toutefois, les réglementations sectorielles peuvent prévoir un délai inférieur qui ne saurait aller en deçà de trois ans. / Pour les irrégularités continues ou répétées, le délai de prescription court à compter du jour où l'irrégularité a pris fin. Pour les programmes pluriannuels, le délai de prescription s'étend en tout cas jusqu'à la clôture définitive du programme. / La prescription des poursuites est interrompue par tout acte, porté à la connaissance de la personne en cause, émanant de l'autorité compétente et visant à l'instruction ou à la poursuite de l'irrégularité. Le délai de prescription court à nouveau à partir de chaque acte interruptif. / (...) / 3. Les États membres conservent la possibilité d'appliquer un délai plus long que celui prévu respectivement au paragraphe 1 et au paragraphe 2 ". <br/>
<br/>
              3. En l'absence d'un texte spécial fixant, dans le respect du principe de proportionnalité, un délai de prescription plus long pour le reversement des aides à l'investissement accordées dans le cadre de l'organisation commune du marché vitivinicole, seul le délai de prescription de quatre années prévu au premier alinéa du paragraphe 1 de l'article 3 du règlement n° 2988/95 cité ci-dessus est applicable.<br/>
<br/>
              4. Par suite, la cour administrative d'appel de Lyon a commis une erreur de droit en jugeant que le délai de prescription de cinq années, prévu, depuis l'entrée en vigueur de la loi du 17 juin 2008 portant réforme de la prescription en matière civile, par les dispositions à caractère général de l'article 2224 du code civil, était applicable en lieu et place du délai de prescription de quatre années précité.<br/>
<br/>
              5. Toutefois, par un arrêt du 6 octobre 2015 Firma Ernst Kollmer Fleischimport und-export c/ Hauptzollamt Hamburg-Jonas (C-59/14), la Cour de justice de l'Union européenne, statuant sur renvoi préjudiciel, a dit pour droit que les articles 1er, paragraphe 2, et 3, paragraphe 1, premier alinéa, du règlement n° 2988/95, cités au point 2, doivent être interprétés en ce sens que, dans des circonstances où la violation d'une disposition du droit de l'Union n'a été détectée qu'après la réalisation d'un préjudice, le délai de prescription commence à courir à partir du moment où tant l'acte ou l'omission d'un opérateur économique constituant une violation du droit de l'Union que le préjudice porté au budget de l'Union ou aux budgets gérés par celle-ci sont survenus, et donc à compter de la plus tardive de ces deux dates. Par le même arrêt, la Cour de Justice a dit pour droit que l'article 1er, paragraphe 2, de ce règlement doit être interprété en ce sens que le préjudice est réalisé à la date à laquelle la décision d'octroyer définitivement l'avantage concerné est prise. <br/>
<br/>
              6. Aux termes de l'article 19 du règlement n° 555/2008 de la Commission du 27 juin 2008 fixant les modalités d'application du règlement (CE) n° 479/2008 du Conseil portant organisation commune du marché vitivinicole, en ce qui concerne les programmes d'aide, les échanges avec les pays tiers, le potentiel de production et les contrôles dans le secteur vitivinicole : " 1.  L'aide est versée une fois qu'il a été établi qu'une action individuelle ou la totalité des actions couvertes par la demande de soutien, selon le mode de gestion de la mesure choisi par l'État membre, ont été mises en oeuvre et contrôlées sur place. / (...) / 2.  Les bénéficiaires de l'aide à l'investissement peuvent demander le versement d'une avance aux organismes payeurs compétents pourvu que cette possibilité soit prévue dans le programme d'aide national. / Le montant de l'avance ne peut dépasser 20 % de l'aide publique à l'investissement et sa liquidation doit être subordonnée à la constitution d'une garantie bancaire ou d'une garantie équivalente correspondant à 110 % du montant avancé. Toutefois, dans le cas d'un investissement pour lequel la décision d'accorder un soutien est rendue en 2009 ou 2010, le montant des avances peut être augmenté à hauteur de 50 % au plus de l'aide publique liée à cet investissement. / La garantie est libérée lorsque l'organisme payeur compétent constate que le montant des dépenses réelles correspondant à l'aide publique liée à l'investissement dépasse le montant de l'avance ".<br/>
<br/>
              7. En outre, aux termes de l'article 6 de l'arrêté du 17 avril 2009 définissant les conditions de mise en oeuvre de la mesure de soutien aux investissements éligibles au financement par les enveloppes nationales en application du règlement (CE) n° 479/2008 du Conseil du 29 avril 2008 portant organisation commune du marché vitivinicole : " 2. L'aide est attribuée sur décision du directeur de l'établissement désigné à l'article 2, après avis de la commission composée d'experts. / (...) / 4. L'aide est accordée sous forme de subvention. Le bénéficiaire de l'aide aux investissements peut demander le versement d'une avance, conformément à l'article 19 du règlement (CE) n° 555/2008, s'il constitue une caution égale à 110 % du montant de l'avance. / (...) / L'ensemble des dispositions du présent article sont précisées par la circulaire du directeur de l'établissement désigné à l'article 2 ". Le 7° du V de la circulaire du directeur général de FranceAgriMer du 26 mai 2009 pris sur le fondement de ces dispositions, applicable au litige, précise que " le montant de l'aide (ou le solde) est versé au bénéficiaire après réalisation complète des travaux programmés et vérification sur place de la réalisation des investissements, et des dépenses éligibles effectuées sur la base des factures acquittées. Dans le cas où une avance a été versée, la caution est libérée dès que le montant des dépenses réelles correspondant à l'aide publique dépasse le montant de l'avance ".<br/>
<br/>
              8. Il résulte des dispositions mentionnées aux points 6 et 7 que, si des avances peuvent être accordées aux bénéficiaires de l'aide sous réserve qu'ils constituent une garantie, le montant définitif de l'aide n'est établi et versé qu'après réalisation des investissements contrôlés sur place. Ainsi, la cour administrative d'appel de Lyon n'a pas commis d'erreur de droit ou dénaturé les pièces du dossier en jugeant que la date de la décision d'octroyer définitivement l'avantage concerné, pour calculer le point de départ du délai de prescription, correspondait à la date du 19 septembre 2013 à laquelle est intervenu le versement du solde de l'aide à l'EARL la Cave du Maître A.... Le motif tiré de ce que la prescription quadriennale prévue au premier alinéa du paragraphe 1 de l'article 3 du règlement n° 2988/95, applicable en l'espèce ainsi qu'il a été dit au point 3, n'était pas acquise au 24 juin 2016, date d'émission du titre de recettes contesté, justifie légalement que la cour ait écarté la prescription invoquée par la société à l'encontre de la créance de FranceAgriMer. <br/>
<br/>
              9. Il résulte de tout ce qui précède que la société requérante n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'EARL la Cave du Maître A... la somme de 1 500 euros à verser à FranceAgriMer, au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à la charge de FranceAgriMer, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de l'EARL la Cave du Maître A... est rejeté.<br/>
Article 2 : L'EARL la Cave du Maître A... versera à l'établissement public national des produits de l'agriculture et de la mer une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
Article 3 : La présente décision sera notifiée à l'EARL la Cave du Maître A... et à l'établissement public national des produits de l'agriculture et de la mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
