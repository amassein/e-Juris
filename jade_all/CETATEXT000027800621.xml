<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027800621</ID>
<ANCIEN_ID>JG_L_2013_08_000000345133</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/80/06/CETATEXT000027800621.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 01/08/2013, 345133, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-08-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>345133</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR>M. Jérôme Marchand-Arvier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:345133.20130801</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi, enregistré le 20 décembre 2010 au secrétariat du contentieux du Conseil d'Etat, présenté pour M. B..., ayant élu domicile ......) ; M. A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement n°1013688 du 12 novembre 2010 par lequel le tribunal administratif de Paris a rejeté sa requête tendant à ordonner à l'Etat de lui attribuer un hébergement, sous astreinte de 50 euros par jour de retard en application de l'article L. 441-2-3-1-II du code de la construction et de l'habitation, à compter de la notification du jugement à intervenir ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la construction et de l'habitation ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jérôme Marchand-Arvier, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bouzidi, Bouhanna, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes des dispositions de l'article L. 441-2-3 du même code : " I.-Dans chaque département, une ou plusieurs commissions de médiation sont créées auprès du représentant de l'Etat dans le département. Chaque commission est présidée par une personnalité qualifiée désignée par le représentant de l'Etat dans le département. (...) II.-La commission de médiation peut être saisie par toute personne qui, satisfaisant aux conditions réglementaires d'accès à un logement locatif social, n'a reçu aucune proposition adaptée en réponse à sa demande de logement dans le délai fixé en application de l'article L. 441-1-4. (...) III.-La commission de médiation peut également être saisie, sans condition de délai, par toute personne qui, sollicitant l'accueil dans une structure d'hébergement, un établissement ou logement de transition, un logement-foyer ou une résidence hôtelière à vocation sociale, n'a reçu aucune proposition adaptée en réponse à sa demande. La commission de médiation transmet au représentant de l'Etat dans le département la liste des demandeurs pour lesquels doit être prévu un tel accueil et précise, le cas échéant, les mesures de diagnostic ou d'accompagnement social nécessaires " ; qu'aux termes des dispositions de l'article L. 441-2-3-1 du même code : " I.-Le demandeur qui a été reconnu par la commission de médiation comme prioritaire et comme devant être logé d'urgence et qui n'a pas reçu, dans un délai fixé par décret, une offre de logement tenant compte de ses besoins et de ses capacités peut introduire un recours devant la juridiction administrative tendant à ce que soit ordonné son logement ou son relogement. (...)II.-Le demandeur qui a été reconnu par la commission de médiation comme prioritaire et comme devant être accueilli dans une structure d'hébergement, un établissement ou logement de transition, un logement-foyer ou une résidence hôtelière à vocation sociale et qui n'a pas été accueilli, dans un délai fixé par décret, dans l'une de ces structures peut introduire un recours devant la juridiction administrative tendant à ce que soit ordonné son accueil dans une structure d'hébergement, un établissement ou logement de transition, un logement-foyer ou une résidence hôtelière à vocation sociale. (...). " ; <br/>
<br/>
              2. Considérant que le juge administratif, saisi sur le fondement de ces dispositions d'une demande tendant à ce qu'il ordonne l'hébergement d'une personne dont la commission de médiation a estimé qu'elle est prioritaire, doit y faire droit s'il constate qu'il n'a pas été proposée à cette personne une place dans une structure d'hébergement, sauf lorsque l'administration apporte la preuve que l'urgence a complètement disparu ; qu'eu égard à la nature de son office, il n'appartient pas au juge saisi en vertu des dispositions de l'article L. 441-2-3-1 du code de la construction et de l'habitation d'apprécier la légalité des décisions des commissions de médiation ;<br/>
<br/>
              3. Considérant qu'après avoir constaté que M. A...n'établissait ni n'alléguait résider régulièrement en France, le tribunal administratif de Paris en a déduit que sa demande d'hébergement, qui avait été reconnue comme prioritaire par la commission de médiation, n'était pas au nombre de celles qui devaient être satisfaites d'urgence ; qu'en statuant ainsi, il a en réalité porté une appréciation sur la légalité de la décision de la commission de médiation et a, par suite, commis une erreur de droit ; que, dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son jugement doit être annulé ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Sur la demande d'injonction :<br/>
<br/>
              5. Considérant qu'il résulte de l'instruction qu'aucune offre d'accueil dans une structure d'hébergement n'a été faite à M.A... ; que l'administration ne soutient pas qu'elle ne soit plus tenue d'accueillir M. A...dans une structure d'hébergement du fait de circonstances postérieures à la décision de la commission de médiation ; que, par suite, il y a lieu d'enjoindre au préfet de la région Ile-de-France, préfet de Paris de faire à l'intéressé une offre d'hébergement ; <br/>
<br/>
              Sur l'astreinte :<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, en application des dispositions de l'article L. 441-2-3-1 du code de la construction et de l'habitation issues de la loi du 25 mars 2009, d'assortir l'injonction prononcée d'une astreinte destinée au fonds national d'accompagnement vers et dans le logement, et d'en fixer le montant  à 50 euros par jour de retard ;<br/>
<br/>
              Sur les conclusions de M. A...présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 2 000 euros à verser à M. A...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Le jugement du 12 novembre 2010 du tribunal administratif de Paris est annulé.<br/>
Article 2 : Il est enjoint au préfet de la région Ile-de-France, préfet de Paris, d'assurer l'accueil de M. A...dans une structure d'hébergement, un établissement ou logement de transition, un logement-foyer ou une résidence hôtelière à vocation sociale, sous une astreinte, destinée au fonds national d'accompagnement vers et dans le logement, de 50 euros par jour de retard à compter du premier jour du deuxième mois suivant celui au cours duquel la présente décision lui sera notifiée. <br/>
Article 3 : L'Etat versera à M. A...une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. B...et à la ministre de l'égalité des territoires et du logement.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
