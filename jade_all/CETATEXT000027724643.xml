<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027724643</ID>
<ANCIEN_ID>JG_L_2013_07_000000362335</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/72/46/CETATEXT000027724643.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  2ème sous-section jugeant seule, 17/07/2013, 362335, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-07-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362335</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 2ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BLANC, ROUSSEAU</AVOCATS>
<RAPPORTEUR>Mme Bénédicte Vassallo-Pasquet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:362335.20130717</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 30 août et 22 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B...A..., demeurant ...à Pirae (98716) ; M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 1 RG 4/CP/11 du 12 avril 2012 par lequel la cour régionale des pensions de Papeete a, sur appel du ministre de la défense, d'une part, annulé le jugement n° 11/03 du 20 juin 2011 du tribunal des pensions de Papeete lui accordant la revalorisation de sa pension militaire d'invalidité attribuée à l'indice du grade de major de l'armée de l'air en fonction de l'indice du grade équivalent pratiqué pour les personnels de la marine nationale à compter du 1er janvier 2007 et, d'autre part, rejeté sa demande ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du ministre de la défense ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Blanc, Rousseau, son avocat, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des pensions militaires d'invalidité et des victimes de la guerre ; <br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ; <br/>
<br/>
              Vu le décret n° 56-913 du 5 septembre 1956 ;<br/>
<br/>
              Vu le décret n° 59-327 du 20 février 1959 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Vassallo-Pasquet, Maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, Rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Blanc, Rousseau, avocat de M. A...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.A..., ancien major de l'armée de l'air rayé des contrôles de l'armée active le 15 janvier 1998, a demandé le 16 mai 2009 au ministre de la défense de recalculer la pension militaire d'invalidité qui lui avait été concédée à titre définitif par un arrêté du 27 juillet 1993 en fonction de l'indice du grade équivalent, plus favorable, pratiqué pour les personnels de la marine nationale ; que, par un courrier du 7 juillet 2009, le ministre lui a indiqué qu'il ne pouvait donner une réponse définitive à sa demande et qu'il serait tenu informé des suites qui lui seraient réservées ; que M. A...a renouvelé sa demande par lettre du 8 novembre 2010 ; que, par lettre du 10 décembre 2010, le ministre de la défense a rejeté sa demande ; que M. A...a saisi le 14 janvier 2011 le tribunal des pensions de Papeete d'un recours contre le rejet qui avait été opposé à sa demande ; qu'il se pourvoit en cassation contre l'arrêt du 12 avril 2012 par lequel la cour régionale des pensions de Papeete, faisant droit à l'appel du ministre de la défense, a annulé le jugement du tribunal des pensions de Papeete et rejeté sa demande ; <br/>
<br/>
              2.	Considérant que le principe d'égalité ne s'oppose pas à ce que l'autorité investie du pouvoir réglementaire règle de façon différente des situations différentes ni à ce qu'elle déroge à l'égalité pour des raisons d'intérêt général, pourvu que dans l'un comme dans l'autre cas, la différence de traitement qui en résulte soit en rapport avec l'objet de la norme qui l'établit et ne soit pas manifestement disproportionnée ; que ces modalités de mise en oeuvre du principe d'égalité sont applicables à l'édiction de normes régissant la situation des militaires qui, en raison de leur contenu, ne sont pas limitées à un même corps d'appartenance ; <br/>
<br/>
              3.	Considérant qu'aux termes de l'article L. 1 du code des pensions militaires d'invalidité et des victimes de la guerre : " La République française, reconnaissante envers les anciens combattants et victimes de la guerre qui ont assuré le salut de la patrie, s'incline devant eux et devant leurs familles. Elle proclame et détermine, conformément aux dispositions du présent code, le droit à réparation due : / 1° Aux militaires des armées de terre, de mer et de l'air, aux membres des forces françaises de l'intérieur, aux membres de la Résistance, aux déportés et internés politiques et aux réfractaires affectés d'infirmités résultant de la guerre (...) " ; que les dispositions du code prévoient l'octroi d'une pension militaire d'invalidité aux militaires, quel que soit leur corps d'appartenance, aux fins d'assurer une réparation des conséquences d'une infirmité résultant de blessures reçues par suite d'évènements de guerre ou d'accidents dont ils ont été victimes à l'occasion du service ou de maladies contractées par le fait ou à l'occasion du service ; que le décret du 5 septembre 1956 relatif à la détermination des indices des pensions et accessoires de pensions alloués aux invalides au titre du code des pensions militaires d'invalidité et des victimes de la guerre a fixé les indices de la pension d'invalidité afférents aux grades des sous-officiers de l'armée de terre, de l'armée de l'air et de la gendarmerie à un niveau inférieur aux indices attachés aux grades équivalents dans la marine nationale ; que le ministre de la défense et des anciens combattants n'invoque pas de considérations d'intérêt général de nature à justifier que le montant de la pension militaire d'invalidité concédée diffère, à grades équivalents, selon les corps d'appartenance des bénéficiaires des pensions ; qu'ainsi, en estimant que le décret du 5 septembre 1956 n'était pas contraire, sur ce point, au principe d'égalité, la cour régionale des pensions de Papeete a commis une erreur de droit ; que, par suite, M. A...est fondé à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              4.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5.	Considérant qu'aux termes de l'article L. 24 du code des pensions militaires d'invalidité et des victimes de la guerre, alors en vigueur : " Les pensions militaires prévues par le présent code sont liquidées et concédées, sous réserve de la confirmation ou modification prévues à l'alinéa ci-après, par le ministre des anciens combattants et des victimes de guerre ou par les fonctionnaires qu'il délègue à cet effet (...). / Les concessions ainsi établies sont confirmées ou modifiées par un arrêté conjoint du ministre des anciens combattants et victimes de guerre et du ministre de l'économie et des finances. La concession ne devient définitive qu'après intervention dudit arrêté. (...) / Les dispositions qui précèdent ne sont pas applicables aux militaires et marins de carrière (...), pour lesquels la pension est liquidée (...) par le ministre d'Etat chargé de la défense nationale (...), la constatation de leurs droits incombant au ministre des anciens combattants et victimes de la guerre. Ces pensions sont concédées par arrêté signé du ministre de l'économie et des finances " ; que, d'une part, en vertu de l'article 5 du décret du 20 février 1959 relatif aux juridictions des pensions, dans sa rédaction alors en vigueur, l'intéressé dispose d'un délai de six mois pour contester, devant le tribunal des pensions, les décisions prises en vertu du premier ou du dernier alinéa de l'article L. 24 ainsi que la décision prise en vertu du deuxième alinéa du même article, sauf si celle-ci a simplement confirmé la décision primitive prise en vertu du premier alinéa ; que, d'autre part, aux termes de l'article L. 78 du même code : " Les pensions définitives ou temporaires attribuées au titre du présent code peuvent être révisées dans les cas suivants :/  1° Lorsqu'une erreur matérielle de liquidation a été commise. / 2° Lorsque les énonciations des actes ou des pièces sur le vu desquels l'arrêté de concession a été rendu sont reconnues inexactes soit en ce qui concerne le grade, le décès ou le genre de mort, soit en ce qui concerne l'état des services, soit en ce qui concerne l'état civil ou la situation de famille, soit en ce qui concerne le droit au bénéfice d'un statut légal générateur de droits. / Dans tous les cas, la révision a lieu sans condition de délai (...) " ;<br/>
<br/>
              6.	Considérant que le décalage défavorable entre l'indice de la pension servie à un ancien sous-officier de l'armée de terre, de l'armée de l'air ou de la gendarmerie et l'indice afférent au grade équivalent dans la marine nationale, lequel ne résulte ni d'une erreur matérielle dans la liquidation de la pension, ni d'une inexactitude entachant les informations relatives à la personne du pensionné, notamment quant au grade qu'il détenait ou au statut générateur de droit auquel il pouvait légalement prétendre, ne figure pas au nombre des cas permettant la révision, sans condition de délai, d'une pension militaire d'invalidité sur le fondement de l'article L. 78 du code des pensions militaires d'invalidité et des victimes de la guerre ; qu'ainsi, la demande présentée par le titulaire d'une pension militaire d'invalidité, concédée à titre temporaire ou définitif sur la base du grade que l'intéressé détenait dans l'armée de terre, l'armée de l'air ou la gendarmerie, tendant à la revalorisation de cette pension en fonction de l'indice afférent au grade équivalent dans la marine nationale doit être formée dans le délai de six mois fixé par l'article 5 du décret du 20 février 1959 ; que passé ce délai de six mois ouvert au pensionné pour contester l'arrêté lui concédant sa pension, l'intéressé ne peut demander sa révision que pour l'un des motifs limitativement énumérés aux 1° et 2° de cet article L. 78 ;<br/>
<br/>
              7.	Considérant, cependant, qu'aux termes du dernier alinéa de l'article L. 25 du code des pensions militaires d'invalidité et des victimes de la guerre, en vigueur à la date des notifications litigieuses : " La notification des décisions prises en vertu de l'article L. 24, premier alinéa, du présent code, doit mentionner que le délai de recours contentieux court à partir de cette notification et que les décisions confirmatives à intervenir n'ouvrent pas de nouveau délai de recours " ; qu'aux termes de l'article 1er du décret du 11 janvier 1965, issu du décret n° 83-1025 du 28 novembre 1983 : " Les délais de recours ne sont opposables qu'à la condition d'avoir été mentionnés, ainsi que les voies de recours, dans la notification de la décision " ; qu'il appartient à l'administration, lorsqu'elle oppose à l'intéressé la tardiveté de son recours, de justifier devant le juge de la date à laquelle elle a notifié la décision contestée et du respect des formes prescrites pour cette notification par les dispositions en vigueur ;<br/>
<br/>
              8.	Considérant qu'il ne résulte pas de l'instruction que la notification de l'arrêté de concession de pension du 27 juillet 1993 ait comporté l'indication des voies de recours ; qu'ainsi, en l'absence de déclenchement du délai de recours contentieux, M. A...était recevable, le 14 janvier 2011, à saisir le tribunal des pensions d'un recours devant être regardé comme dirigé contre la décision initialement prise sur sa demande de pension ; que, par suite, la fin de non-recevoir opposée par le ministre de la défense doit être écartée ;<br/>
<br/>
              9.	Considérant, ainsi qu'il a été dit au point 3, que M. A...est fondé à soutenir que les dispositions du décret du 5 septembre 1956 sont contraires au principe d'égalité et à demander, pour ce motif, l'annulation du refus opposé à sa demande contestant l'indice de sa pension ;<br/>
<br/>
              10.	Considérant qu'aux termes de l'article L. 108 du code des pensions militaires d'invalidité et des victimes de la guerre : " Lorsque, par suite du fait personnel du pensionné, la demande de liquidation ou de révision de la pension est déposée postérieurement à l'expiration de la troisième année qui suit celle de l'entrée en jouissance normale de la pension, le titulaire ne peut prétendre qu'aux arrérages, afférents à l'année au cours de laquelle la demande a été déposée et aux trois années antérieures. " ;<br/>
<br/>
              11.	Considérant qu'il ne résulte pas de l'instruction qu'une circonstance particulière ait empêché M. A...de se prévaloir, avant l'expiration de la troisième année suivant celle de l'entrée en jouissance normale de sa pension d'invalidité, de ce que l'indice qui lui était appliqué était inférieur à celui fixé, à grade équivalent, pour les personnels de la marine nationale et qu'une telle différence de traitement était contraire au principe d'égalité ; que, par suite, il ne peut prétendre, en application des dispositions de l'article L. 108 du code des pensions militaires d'invalidité et des victimes de la guerre, qu'aux arrérages afférents à l'année au cours de laquelle il a présenté sa demande de revalorisation ainsi qu'aux trois années antérieures ; que cette demande ayant été présentée à l'administration au mois de novembre 2010, M. A...est fondé à solliciter la revalorisation de sa pension à compter du 1er janvier 2007 ; que le ministre de la défense n'est, par suite, pas fondé à soutenir que c'est à tort que le tribunal des pensions de Papeete a fait droit à la demande de M. A... ; <br/>
<br/>
              12.	Considérant que M. A...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Blanc, Rousseau, avocat de M.A..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat, le versement à la SCP Blanc, Rousseau de la somme de 2 000 euros au titre des frais exposés et non compris dans les dépens que l'intéressé aurait dû engager s'il n'avait obtenu l'aide juridictionnelle ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour régionale des pensions de Papeete du 12 avril 2012 est annulé.<br/>
<br/>
Article 2 : L'appel formé par le ministre de la défense devant la cour régionale des pensions de Papeete est rejeté.<br/>
<br/>
Article 3 : L'Etat versera à la SCP Blanc, Rousseau la somme de 2 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. B...A...et au ministre de la défense.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
