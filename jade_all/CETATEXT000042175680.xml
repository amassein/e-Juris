<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042175680</ID>
<ANCIEN_ID>JG_L_2020_07_000000429114</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/17/56/CETATEXT000042175680.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 29/07/2020, 429114, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429114</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE ; SCP MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>M. Laurent-Xavier Simonel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:429114.20200729</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Montreuil d'annuler la décision implicite par laquelle le président du conseil général de la Seine-Saint-Denis a rejeté sa demande du 30 septembre 2014 tendant à obtenir le bénéfice de la protection fonctionnelle, d'enjoindre au département de la Seine-Saint-Denis de reconstituer sa carrière, au besoin sous astreinte, à compter de sa réussite au concours d'ingénieur territorial, d'ordonner au département, au titre de la protection fonctionnelle, de prendre en charge ses frais de procédure et de le réhabiliter au sein du service, enfin, de condamner le département à lui verser la somme de 10 000 euros en réparation de son préjudice moral. Par un jugement n° 1501227 du 1er juillet 2016, le tribunal administratif de Montreuil a annulé la décision implicite du président du conseil général de la Seine-Saint-Denis refusant le bénéfice de la protection fonctionnelle et rejeté le surplus des conclusions de la demande de M. A....<br/>
<br/>
              Par un arrêt n° 16VE02863 du 24 janvier 2019, la cour administrative d'appel de Versailles a rejeté l'appel formé par M. A... contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés le 25 mars 2019, le 25 juin 2019 et le 15 janvier 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du département de la Seine-Saint-Denis la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - la loi n° 84-594 du 12 juillet 1984 ;<br/>
              - le décret n° 88-145 du 15 février 1988 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent-Xavier Simonel, conseiller d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M. A... et à la SCP Marlange, de la Burgade, avocat du département de la Seine-Saint-Denis ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A... a été recruté par le département de la Seine-Saint-Denis par contrat à durée déterminée du 15 juillet 2010 au 30 juin 2011 pour être affecté en qualité d'analyste des systèmes applicatifs au sein de la direction des systèmes d'information. Son contrat a été renouvelé pour une durée d'un an, du 1er juillet 2011 au 30 juin 2012, afin qu'il exerce les fonctions d'ingénieur informatique " projets " au sein de la direction des bâtiments et de la logistique. Son engagement contractuel a ensuite été prolongé pour trois années, du 1er juillet 2012 au 30 juin 2015. Après avoir été admis au concours d'ingénieur territorial en 2013, il a demandé à être nommé fonctionnaire stagiaire en vue d'être titularisé dans le poste qu'il occupait mais s'est heurté à un refus de la part du département. M. A... a alors demandé au président du conseil général le bénéfice de la protection fonctionnelle au titre du harcèlement moral dont il s'estimait victime et l'indemnisation de son préjudice moral, demandes qui ont été tacitement rejetées. Par jugement du 1er juillet 2016, le tribunal administratif de Montreuil a fait droit à ses conclusions tendant à l'annulation de la décision implicite rejetant sa demande de protection fonctionnelle, pour un motif tiré de son insuffisante motivation, mais a rejeté le surplus de ses conclusions, notamment celles qui tendaient à la condamnation du département à lui verser la somme de 10 000 euros en réparation du préjudice moral qu'il estimait avoir subi. M. A... se pourvoit en cassation contre l'arrêt du 24 janvier 2019 par lequel la cour administrative d'appel de Versailles a rejeté son appel contre ce jugement.<br/>
<br/>
              2. En premier lieu, aux termes de l'article R. 612-6 du code de justice administrative : " Si, malgré une mise en demeure, la partie défenderesse n'a produit aucun mémoire, elle est réputée avoir acquiescé aux faits exposés dans les mémoires du requérant ". Il ressort des pièces du dossier que, par lettre du 10 février 2017, la cour administrative d'appel de Versailles a mis en demeure le département de la Seine-Saint-Denis de produire sa défense dans un délai d'un mois. Si le département n'a produit son mémoire en défense que le 8 novembre 2017, soit après l'expiration du délai d'un mois imparti dans la mise en demeure, ce mémoire a été produit avant la clôture de l'instruction, fixée au 11 décembre 2017. Par suite, le département ne pouvait être regardé comme ayant acquiescé aux faits exposés dans la requête d'appel de M. A.... Le moyen tiré de ce que la cour administrative d'appel aurait méconnu les dispositions de l'article R. 612-6 du code de justice administrative en ne tirant pas les conséquences de l'acquiescement aux faits du département ne peut, dès lors, qu'être écarté. <br/>
<br/>
              3. En deuxième lieu, il ressort des pièces du dossier soumis aux juges du fond, notamment de la lettre du 28 juin 2011 adressée par le département de la Seine-Saint-Denis à M. A... et du compte-rendu de la réunion du 1er décembre 2011 consacrée à un point d'étape à la suite de son recrutement au sein de la direction des bâtiments et de la logistique, que le département envisageait alors sa nomination comme fonctionnaire stagiaire sur le poste qu'il occupait en cas de réussite au concours d'ingénieur territorial organisé en 2011. Dès lors, la cour n'a pas dénaturé les faits et n'a pas commis d'erreur de droit en retenant, pour caractériser l'engagement pris par le département, qu'aucune promesse formelle, inconditionnelle et illimitée dans le temps de le nommer stagiaire n'avait été formulée lors de cette réunion au bénéfice de M. A.... Il ressort, par ailleurs, des pièces du dossier soumis aux juges du fond que M. A... n'a pas été admis au concours d'ingénieur territorial au titre de la session organisée en 2011 mais, comme il a été dit au point 1, au titre de la session organisée en 2013. Dès lors, la cour administrative d'appel n'a pas inexactement qualifié les faits en jugeant que le département, en refusant en 2014 de nommer M. A... fonctionnaire stagiaire dans le poste qu'il occupait, n'avait commis aucune faute tirée d'une méconnaissance des engagements pris à son égard. <br/>
<br/>
              4. En troisième lieu, aux termes de l'article 44 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, dans sa rédaction applicable au litige : " Chaque concours donne lieu à l'établissement d'une liste d'aptitude classant par ordre alphabétique les candidats déclarés aptes par le jury. (...) / L'inscription sur une liste d'aptitude ne vaut pas recrutement. / (...) / Toute personne déclarée apte depuis moins de trois ans ou, si celui-ci est intervenu au-delà de ce délai, depuis le dernier concours, peut être nommée dans un des emplois auxquels le concours correspondant donne accès ; la personne déclarée apte ne bénéficie de ce droit la deuxième et la troisième année que sous réserve d'avoir fait connaître son intention d'être maintenue sur ces listes au terme de l'année suivant son inscription initiale et au terme de la deuxième année (...) ". Il résulte de ces dispositions que les candidats déclarés admis à l'issue d'un concours de recrutement au sein de la fonction publique territoriale ne disposent d'aucun droit à être nommés fonctionnaires dans un emploi donné. Un tel droit ne résulte pas davantage, contrairement à ce que soutient M. A..., des dispositions des articles 1er et 2 de la loi du 12 juillet 1984 relative à la formation professionnelle des agents de la fonction publique territoriale, qui portent sur l'exercice du droit à la formation. Dès lors, la cour administrative d'appel n'a commis aucune erreur de droit en s'abstenant de rechercher si la réussite de M. A... au concours d'ingénieur territorial lui conférait un droit à être nommé fonctionnaire stagiaire sur le poste qu'il occupait. <br/>
<br/>
              5. En quatrième lieu, il appartient à l'agent public qui soutient avoir été victime de faits constitutifs de harcèlement moral, lorsqu'il entend contester le refus opposé par l'administration dont il relève à une demande de protection fonctionnelle fondée sur des faits de harcèlement, de soumettre au juge des éléments de fait susceptibles d'en faire présumer l'existence. Il incombe à l'administration de produire, en sens contraire, une argumentation de nature à démontrer que les agissements en cause sont justifiés par des considérations étrangères à tout harcèlement. La conviction du juge, à qui il revient d'apprécier si les agissements de harcèlement sont ou non établis, se détermine au vu de ces échanges contradictoires, qu'il peut compléter, en cas de doute, en ordonnant toute mesure d'instruction utile. Pour apprécier si des agissements dont il est allégué qu'ils sont constitutifs d'un harcèlement moral revêtent un tel caractère, le juge administratif doit tenir compte des comportements respectifs de l'agent auquel il est reproché d'avoir exercé de tels agissements et de l'agent qui estime avoir été victime d'un harcèlement moral. <br/>
<br/>
              6. D'une part, en estimant que les missions confiées à M. A... par son employeur n'avaient pas excédé celles qui étaient mentionnées sur les versions successives de sa fiche de poste, qu'il n'était pas établi que les appréciations critiques sur sa manière de servir dans ses évaluations annuelles à compter de l'année 2012 fussent entachées d'inexactitudes ou d'erreur manifeste d'appréciation, que le refus de le nommer fonctionnaire stagiaire dans le poste qu'il occupait au sein de la direction des bâtiments et de la logistique, à la suite de sa réussite au concours d'ingénieur territorial en novembre 2013, était justifié non seulement par ses difficultés relationnelles avec ses collègues et sa hiérarchie mais aussi par l'achèvement des projets de développement mis en place par cette direction et par la disparition du besoin ayant justifié son affectation dans ce service, enfin que la diminution, à l'approche du terme de son contrat, des tâches qu'il effectuait au titre de ses missions de chef de projets informatiques était également liée à la disparition, au sein de la direction des bâtiments et de la logistique, du besoin ayant justifié son affectation dans ce service, la cour administrative d'appel a porté sur les faits de l'espèce une appréciation qui, au vu des pièces du dossier qui lui était soumis, n'est pas entachée de dénaturation. D'autre part, en jugeant, au vu des motifs qui précèdent et des autres motifs figurant aux points 7 à 17 de l'arrêt attaqué, que les faits de harcèlement moral allégués par M. A... n'étaient pas établis, la cour administrative d'appel, qui n'a pas renversé la charge de la preuve, n'a pas inexactement qualifié ces faits. <br/>
<br/>
              7. Il résulte de tout ce qui précède que M. A... n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du département de la Seine-Saint-Denis, qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions que le département présente au titre de ces mêmes dispositions.  <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de M. A... est rejeté.<br/>
<br/>
Article 2 : Les conclusions du département de la Seine-Saint-Denis présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 3 : La présente décision sera notifiée à Monsieur A... et au département de la Seine-Saint-Denis. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
