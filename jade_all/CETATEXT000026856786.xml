<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026856786</ID>
<ANCIEN_ID>JG_L_2012_12_000000338349</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/85/67/CETATEXT000026856786.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème sous-section jugeant seule, 28/12/2012, 338349, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>338349</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. Michel Bart</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:338349.20121228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 2 avril et 2 juillet 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune d'Acoua, représentée par son maire ; la commune d'Acoua demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09BX00618 du 5 janvier 2010 par lequel la cour administrative d'appel de Bordeaux a rejeté sa requête tendant à l'annulation du jugement du 16 décembre 2008 par lequel le tribunal administratif de Mayotte, suite à la demande du préfet de Mayotte, a annulé la délibération du 13 avril 2008 par laquelle le conseil municipal de ladite commune a procédé à la fixation des indemnités des élus ainsi que la décision implicite de rejet de son recours gracieux ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi organique n° 2007-223 du 21 février 2007 ;<br/>
<br/>
              Vu la loi n° 2001-616 du 11 juillet 2001 ;<br/>
<br/>
              Vu la loi n° 2003-660 du 21 juillet 2003 ;<br/>
<br/>
              Vu l'ordonnance n° 2002-1450 du 12 décembre 2002 ;<br/>
<br/>
              Vu l'ordonnance n° 2007-1434 du 5 octobre 2007 ;<br/>
<br/>
              Vu la loi n° 2010-1487 du 7 décembre 2010 ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Michel Bart, Conseiller d'Etat,<br/>
<br/>
              - les observations de la SCP Barthélemy, Matuchansky, Vexliard, avocat de la commune d'Acoua,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Barthélemy, Matuchansky, Vexliard, avocat de la commune d'Acoua ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par délibération du 13 avril 2008, le conseil municipal de la commune d'Acoua a procédé à la fixation des indemnités des élus, maire et adjoints, en faisant application des dispositions de l'article L. 2123-20 du code général des collectivités territoriales ; que la commune se pourvoit en cassation contre l'arrêt du 5 janvier 2010 par lequel la cour administrative d'appel de Bordeaux a rejeté sa requête tendant à l'annulation du jugement du 16 décembre 2008 par lequel le tribunal administratif de Mayotte a, à la demande du préfet de Mayotte, annulé la délibération du 13 avril 2008 ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 64-1 de la loi du 11 juillet 2001 relative à Mayotte, dans sa rédaction issue de l'article 64 de la loi n° 2003-660 du 21 juillet 2003 : " Sont applicables aux agents de la collectivité départementale, des communes et des établissements publics administratifs de Mayotte, selon les modalités définies ci-après, les dispositions de la loi n° 83-634 du 13 juillet 1983 portant droits et obligations des fonctionnaires ainsi que celles de la loi n° 84-16 du 11 janvier 1984 (...) de la loi n° 84-53 du 26 janvier 1984 (...) de la loi n° 86-33 du 9 janvier 1986 (...) " ; qu'aux termes de l'article L. 2123-20 du code général des collectivités territoriales " les indemnités maximales pour l'exercice des fonctions de maires et adjoints aux maire des communes, de conseillers municipaux des communes de 100 000 habitants et plus (...) sont fixées par référence au montant correspondant à l'indice brut terminal de l'échelle indiciaire de la fonction publique " ; qu'il résulte toutefois de l'article L. 2572-6 du code général des collectivités territoriales dans sa rédaction issue du III de l'article 6 de l'ordonnance n° 2002-1450 du 12 décembre 2002 relative à la modernisation du régime communal, à la coopération intercommunale, aux conditions d'exercice des mandats locaux à Mayotte et modifiant le code général des collectivités territoriales, devenu article L. 2572-8 dans sa numérotation issue de l'ordonnance n° 2007-1434 du 5 octobre 2007, que " pour l'application de l'article L. 2123-20, après les mots : "de la fonction publique" sont ajoutés les mots : "de Mayotte" " ;<br/>
<br/>
              3. Considérant qu'il résulte de l'ensemble de ces dispositions que la fixation des indemnités des conseillers était déterminée à la date des délibérations litigieuses par application des dispositions législatives propres à Mayotte issues de l'article L. 2572-6 du code général des collectivités territoriales précitées ; que pour juger illégale la délibération attaquée, fixant les indemnités des maires et adjoints par référence à l'indice terminal de la fonction publique d'Etat, la cour administrative d'appel a relevé que l'article 64 de la loi de 2003 introduisant l'article 64 - I et abrogeant le statut de la fonction publique mahoraise n'avait pas implicitement abrogé ces dispositions spécifiques ; qu'il ressort en effet du texte même de ces dispositions que l'abrogation du statut général de la fonction publique mahoraise ne mettait pas fin à l'application de la grille indiciaire qui leur était spécifique jusqu'au 31 décembre 2010 pour ceux des fonctionnaires qui continuaient d'en relever de par les dispositions mêmes de cet article 64 ; que les dispositions spécifiques issues de l'article L. 2572-6 du code général des collectivités territoriales, loin d'être abrogées implicitement par  la loi n°2003-660 du 21 juillet 2003, ont été expressément ratifiées par le 1° du V de l'article 65 de cette même loi de 2003, et n'ont été abrogées que par la loi n° 2010-1487 du 7 décembre 2010 ; qu'ainsi, en regardant les dispositions se référant à l'indice terminal de la fonction publique mahoraise comme applicables au litige, et déterminant l'illégalité des délibérations attaquées, qu'il appartenait bien au préfet sur ce fondement de déférer au juge administratif, la cour administrative d'appel de Bordeaux n'a, par une décision suffisamment motivée, commis aucune erreur de droit ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que la commune d'Acoua n'est pas fondée à demander l'annulation de l'arrêt de la cour administrative d'appel de Bordeaux ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de la commune d'Acoua est rejeté.<br/>
Article 2 : Les conclusions de la commune d'Acoua tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la commune d'Acoua et au ministre de l'intérieur.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
