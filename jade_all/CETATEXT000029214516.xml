<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029214516</ID>
<ANCIEN_ID>JG_L_2014_07_000000365037</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/21/45/CETATEXT000029214516.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 02/07/2014, 365037, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365037</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR>M. Renaud Jaune</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:365037.20140702</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 8 janvier et 8 avril 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Roux Poissonnier, dont le siège est 2, place du marché Saint-Pierre à Clermont-Ferrand (63000), représentée par son gérant ; elle demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 12LY00636 du 6 novembre 2012 par lequel la cour administrative d'appel de Lyon a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 1001470-1001472 du 20 décembre 2011 du tribunal administratif de Clermont-Ferrand rejetant ses demandes tendant à la restitution des sommes qu'elle a acquittées au titre de la contribution pour une pêche durable instituée par l'article 302 bis KF du code général des impôts pour les périodes du 1er janvier 2008 au 31 décembre 2008 et du 1er janvier au 31 mai 2009, soit 16 513 euros et 8 230 euros respectivement et, d'autre part, à la restitution de ces sommes ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;  <br/>
<br/>
              3°) le cas échéant, de renvoyer à la Cour de justice de l'Union européenne une question préjudicielle relative à la validité de cette imposition au regard des aides d'Etat et de surseoir à statuer jusqu'à ce qu'elle se soit prononcée sur la question ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 1 250 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le traité instituant la Communauté européenne ;<br/>
<br/>
              Vu le traité sur le fonctionnement de l'Union européenne; <br/>
<br/>
              Vu la loi organique n° 2001-692 du 1er août 2001 modifiée ; <br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu la loi n° 2007-1824 du 25 décembre 2007 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Renaud Jaune, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gadiou, Chevallier, avocat de la société Roux Poissonier  ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Roux Poissonnier a demandé au tribunal administratif de Clermont-Ferrand de prononcer la restitution de la contribution mise à sa charge en application des dispositions de l'article 302 bis KF du code général des impôts au motif que cette contribution n'avait pas fait l'objet d'une notification préalable à la Commission européenne ; qu'elle se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Lyon du 6 novembre 2012 qui a confirmé le jugement du tribunal administratif rejetant ses demandes ; <br/>
<br/>
              2. Considérant qu'aux termes du paragraphe 1 de l'article 87 du traité instituant la Communauté européenne, alors applicable, devenu l'article 107 du traité sur le Fonctionnement de l'Union européenne (TFUE) : " Sauf dérogations prévues par le présent traité, sont incompatibles avec le marché commun, dans la mesure où elles affectent les échanges entre Etats membres, les aides accordées par les Etats ou au moyen de ressources d'Etat sous quelque forme que ce soit qui faussent ou qui menacent de fausser la concurrence en favorisant certaines entreprises ou certaines productions " ; qu'aux termes de l'article 88 du même traité, alors applicable, devenu l'article 108 TFUE : "  1. La Commission procède avec les Etats membres à l'examen permanent des régimes d'aides existant dans ces Etats. (...) / 2. Si (...) la Commission constate qu'une aide accordée par un Etat ou au moyen de ressources d'Etat n'est pas compatible avec le marché commun aux termes de l'article 87 (...) elle décide que l'Etat intéressé doit la supprimer ou la modifier (...) / 3. La Commission est informée, en temps utile pour présenter ses observations, des projets tendant à instituer ou à modifier des aides. Si elle estime qu'un projet n'est pas compatible avec le marché commun, aux termes de l'article 87, elle ouvre sans délai la procédure prévue au paragraphe précédent. L'Etat membre intéressé ne peut mettre à exécution les mesures projetées, avant que cette procédure ait abouti à une décision finale. " ; qu'il résulte de ces stipulations que, s'il  relève de la compétence exclusive de la Commission de décider, sous le contrôle de la Cour de justice de l'Union européenne, si une aide de la nature de celles visées par l'article 87 du traité est ou non, compte tenu des dérogations prévues par le traité, compatible avec le marché commun, il incombe, en revanche, aux juridictions nationales de sanctionner, le cas échéant, l'invalidité des dispositions de droit national qui auraient institué ou modifié une telle aide en méconnaissance de l'obligation d'en notifier à la Commission, préalablement à toute mise à exécution, le projet ; <br/>
<br/>
              3. Considérant qu'il résulte de la jurisprudence de la Cour de justice de l'Union européenne, d'une part, que les taxes n'entrent pas dans le champ d'application des stipulations précitées du traité concernant les aides d'Etat, à moins qu'elles ne constituent le mode de financement d'une mesure d'aide, de sorte qu'elles font partie intégrante de cette mesure, d'autre part, que, pour que l'on puisse juger qu'une taxe, ou une partie d'une taxe, fait partie intégrante d'une mesure d'aide, il doit exister un lien d'affectation contraignant entre la taxe et l'aide en vertu de la réglementation nationale pertinente, en ce sens que le produit de la taxe est nécessairement affecté au financement de l'aide ;<br/>
<br/>
<br/>
              4. Considérant qu'aux termes de 302 bis KF du code général des impôts, applicable du 1er janvier 2008 au 31 décembre 2011, instituant une contribution pour une pêche durable: "Les ventes en France métropolitaine à des personnes autres que des personnes assujetties à la taxe sur la valeur ajoutée agissant en tant que telles, de poissons, crustacés, mollusques et autres invertébrés marins, ainsi que de produits alimentaires dont le poids comporte pour plus de 30 % de tels produits de la mer sont soumises à une taxe. / La taxe ne s'applique pas aux huîtres et aux moules. / La liste des poissons, crustacés, mollusques ou invertébrés marins visés au premier alinéa est fixée par arrêté. / La taxe est calculée au taux de 2 % sur le montant hors taxe des ventes des produits visés au premier alinéa. / La taxe est due par les personnes dont le chiffre d'affaires de l'année précédente a excédé le premier des seuils mentionnés au I de l'article 302 septies A. / Le fait générateur et l'exigibilité de la taxe interviennent dans les mêmes conditions que celles applicables en matière de taxe sur la valeur ajoutée. La taxe est constatée, liquidée, recouvrée et contrôlée selon les mêmes procédures et sous les mêmes sanctions, garanties, sûretés et privilèges. Les réclamations sont présentées, instruites et jugées selon les règles applicables à cette même taxe. " / II. - Le I entre en vigueur le 1er janvier 2008 " ;<br/>
<br/>
              5. Considérant qu'en vertu du principe à valeur constitutionnelle d'universalité budgétaire résultant des dispositions de l'article 2 de la loi organique du 1er août 2001 relative aux lois de finances, les recettes et les dépenses doivent figurer au budget de l'Etat pour leur montant brut, sans être contractées, et l'affectation d'une recette déterminée à la couverture d'une dépense déterminée est interdite, sous réserve des exceptions prévues par le deuxième alinéa de l'article 2 et le quatrième alinéa de l'article 6 ;<br/>
<br/>
              6. Considérant qu'eu égard à ce principe et aux dispositions précitées de l'article 302 bis KF du code général des impôts, la cour n'a pas commis d'erreur de droit en jugeant qu'il n'existait aucun lien d'affectation contraignant entre la contribution et le plan d'action en quinze mesures pour une pêche durable et responsable ou une des mesures de ce plan ; qu'elle a relevé que cette contribution constituait une recette du budget général dépourvue de tout lien avec le budget du ministère en charge de la pêche et la dotation inscrite à son budget servant à financer le plan d'action pour une pêche durable, et qu'il n'y avait pas de rapport entre le produit de la contribution et le montant des financements publics consacrés au plan ; qu'elle a ainsi exactement qualifié les faits qui lui étaient soumis sans les dénaturer ; <br/>
<br/>
              7. Considérant, par suite, que, sans qu'il y ait lieu de saisir la Cour de justice de l'Union européenne d'une question préjudicielle, le pourvoi de la société doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>                  D E C I D E :<br/>
                                --------------<br/>
<br/>
 Article 1er : Le pourvoi de la société Roux Poissonnier est rejeté.<br/>
<br/>
 Article 2 : La présente décision sera notifiée à la société Roux Poissonnier et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
