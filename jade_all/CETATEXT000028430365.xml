<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028430365</ID>
<ANCIEN_ID>JG_L_2013_12_000000374139</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/43/03/CETATEXT000028430365.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 26/12/2013, 374139, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-12-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374139</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2013:374139.20131226</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 20 décembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentée par M. C...A...et Mme B...épouseA..., élisant domicile ...à Saint Etienne (42000) ; M. et Mme A...demandent au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance nos 1308461,1308462 du 17 décembre 2013 par laquelle le juge des référés du tribunal administratif de Lyon, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté leur demande tendant à ce qu'il soit enjoint à la préfète de la Loire de procéder à l'enregistrement de leurs demandes d'asile, de leur remettre un dossier de saisine de l'Office français de protection des réfugiés et des apatrides et de les admettre provisoirement au séjour au titre de l'asile, dans un délai de quarante-huit heures à compter le la notification de l'ordonnance à intervenir et sous astreinte de 100 euros par jour de retard ;<br/>
<br/>
              2°) de faire droit à leur demande de première instance ;<br/>
<br/>
              3°) de leur accorder le bénéfice de l'aide juridictionnelle à titre provisoire et de mettre à la charge de l'Etat le versement de la somme de 1 500 euros à leur avocat en application des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, ainsi que le paiement des dépens ;<br/>
<br/>
<br/>
              ils soutiennent que : <br/>
              - la condition d'urgence est remplie dès lors qu'ils font l'objet de décisions de réadmission vers la Hongrie qui sont susceptibles d'être exécutées d'office ; <br/>
              - un risque sérieux existe que leurs demandes d'asile ne soient pas traitées par les autorités hongroises dans des conditions conformes à l'ensemble des garanties exigées par le respect du droit d'asile ; <br/>
              - ils n'ont pas eu un comportement caractérisant la fuite et ont répondu à toutes les convocations de la préfecture à l'exception de la dernière ; <br/>
              - les décisions leur refusant l'admission provisoire au séjour au titre de l'asile et prononçant leur réadmission vers la Hongrie méconnaissent leur droit d'asile ainsi que les dispositions de l'article 9-3 du règlement (CE) n° 1560/2003 dès lors que les autorités hongroises n'ont pas été informées de la prolongation du délai de six mois prévu par le règlement Dublin II ; <br/>
              - c'est à tort que le juge des référés du tribunal administratif de Lyon a estimé que les décisions contestées ne méconnaissaient pas l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le refus de faire application de la " clause humanitaire " prévue par l'article 15, ou des dispositions du paragraphe 2 de l'article 3 du règlement 343/2003 constitue une erreur manifeste d'appréciation ;<br/>
<br/>
<br/>
              Vu l'ordonnance attaquée ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 24 décembre 2013, présenté par le ministre de l'intérieur, qui conclut au rejet de la requête ; il soutient que les moyens des requérants ne sont pas fondés ; <br/>
                          Vu les autres pièces du dossier ;<br/>
<br/>
                          Vu la Constitution ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le règlement (CE) n° 343/2003 du Conseil du 18 février 2003 ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. et MmeA..., d'autre part, le ministre de l'intérieur ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 26 décembre 2013 à 11 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Bouzidi, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. et MmeA... ; <br/>
<br/>
              - M.A..., requérant, ainsi que les représentants d'une association de soutien ;<br/>
<br/>
              - la représentante du ministre de l'intérieur ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clôturé l'instruction ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ;<br/>
<br/>
              2. Considérant que le droit constitutionnel d'asile, qui a pour corollaire le droit de solliciter la qualité de réfugié, constitue une liberté fondamentale au sens des dispositions de l'article L. 521-2 du code de justice administrative ; qu'il implique que l'étranger qui sollicite la qualité de réfugié soit en principe autorisé à demeurer sur le territoire jusqu'à ce qu'il ait été statué sur sa demande, dans les conditions définies par l'article L. 741-4 du code de l'entrée et du séjour des étrangers et du droit d'asile ; que le 1° de cet article permet de refuser l'admission au séjour en France d'un demandeur d'asile lorsque la demande relève de la compétence d'un autre Etat membre de l'Union européenne en application des dispositions du règlement (CE) n° 343/2003 du Conseil du 18 février 2003 établissant les critères et mécanismes de détermination de l'Etat membre responsable de l'examen d'une demande d'asile présentée dans l'un des Etats membres par un ressortissant d'un pays tiers ; que, toutefois, le dernier alinéa du même article prévoit que " les dispositions du présent article ne font pas obstacle au droit souverain de l'Etat d'accorder l'asile à toute personne qui se trouverait néanmoins dans l'un des cas mentionnés aux 1° à 4° " ; que le paragraphe 2 de l'article 3 du règlement du 18 février 2003 prévoit que " chaque État membre peut examiner une demande d'asile qui lui est présentée par un ressortissant d'un pays tiers, même si cet examen ne lui incombe pas en vertu des critères fixés dans le présent règlement. Dans ce cas, cet État devient l'État membre responsable au sens du présent règlement et assume les obligations qui sont liées à cette responsabilité " ;<br/>
<br/>
              3. Considérant qu'il résulte de l'instruction que les requérants, de nationalité kosovare, qui ont quitté leur pays d'origine le 7 avril 2013 avec leurs trois enfants mineurs, ont été arrêtés à la frontière hongroise par les services de police et ont été placés en détention avant d'être conduits dans le centre de Debrecen ; qu'après avoir quitté ce centre, ils sont, selon leurs dires, retournés au Kosovo, avant de quitter à nouveau leur pays pour rejoindre la France, où ils sont arrivés le 18 avril 2013 : qu'après s'être présentés à la préfecture de la Loire, ils ont déposé une demande d'asile à la préfecture du Rhône le 30 avril suivant ; qu'après avoir constaté, par la consultation du fichier " Eurodac " qu'une telle demande avait été enregistrée le 10 avril 2013 en Hongrie, le préfet du Rhône, estimant que la France n'était pas responsable de leurs demandes d'asile, a saisi les autorités hongroises ; que, celles-ci ayant accepté de reprendre en charge les demandes d'asile de M. et Mme A...le 16 mai, le préfet du Rhône a refusé, par décisions du 21 mai, leur admission au séjour au titre de l'asile en vue d'une réadmission dans ce pays ; que le 1er août, la préfète de la Loire a pris deux arrêtés portant remise de M. et Mme A...aux autorités hongroises ; que M. et Mme A...ont contesté ces arrêtés ; que le 18 novembre 2013, ils ont sollicité à nouveau leur admission au séjour en qualité de demandeur d'asile ; que le 9 décembre, la préfète de la Loire a indiqué aux intéressés qu'ils étaient considérés comme étant en fuite et que le délai de transfert était porté à dix-huit mois ; que le 11 décembre, les requérants ont saisi le juge des référés du tribunal administratif de Lyon d'une demande, présentée sur le fondement de l'article L. 521-2 du code de justice administrative, tendant à ce qu'il soit enjoint à l'administration de procéder à l'enregistrement de leurs demandes d'asile, de transmettre leurs demandes à l'Office français de protection des réfugiés et des apatrides, et de les admettre au séjour à titre provisoire ; que, par l'ordonnance attaquée, le juge des référés a rejeté leurs demandes ; <br/>
<br/>
              4. Considérant, d'une part, que si la Hongrie est un Etat membre de l'Union européenne et partie tant à la convention de Genève du 28 juillet 1951 sur le statut des réfugiés, complétée par le protocole de New York, qu'à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, il appartient néanmoins à l'administration d'apprécier dans chaque cas, au vu des pièces qui lui sont soumises et sous le contrôle du juge, si les conditions dans lesquelles un dossier particulier est traité par les autorités de ce pays répondent à l'ensemble des garanties exigées par le respect du droit d'asile ; qu'en l'espèce, il résulte de l'instruction, notamment des explications données par les requérants tant dans leurs écritures qu'au cours de l'audience devant le Conseil d'Etat, en particulier de témoignages et de certificats médicaux circonstanciés relatifs aux conditions dans lesquelles M. et MmeA... et leurs enfants ont été traités par les autorités hongroises lors de leur transit par ce pays, que l'existence d'un risque sérieux que leurs demandes d'asile ne soient pas examinées par ces autorités dans des conditions conformes à l'ensemble des garanties exigées par le respect du droit d'asile doit être tenue pour établie ; qu'ainsi, dans les circonstances particulières de l'espèce, leur réadmission vers la Hongrie serait de nature à porter une atteinte grave et manifestement illégale à la liberté fondamentale que constitue le droit d'asile ; <br/>
<br/>
              5. Considérant, d'autre part, qu'une décision de remise à un Etat étranger, susceptible à tout moment d'être exécutée d'office, crée pour son destinataire une situation d'urgence au sens de l'article L. 521-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin d'examiner leurs autres moyens, les requérants sont fondés à soutenir que c'est à tort que le juge des référés du tribunal administratif de Lyon a rejeté leurs demandes ; qu'il y a lieu, dès lors que n'est n'invoqué aucun des autres cas prévus à l'article  L. 741-4 du code de l'entrée et du séjour des étrangers et du droit d'asile, d'enjoindre à l'administration de leur délivrer, dans un délai de quinze jours à compter de la notification de la présente ordonnance, une autorisation provisoire de séjour leur permettant de saisir l'Office français de protection des réfugiés et des apatrides ; qu'il n'y a pas lieu, en l'état, d'assortir cette injonction d'une astreinte ;<br/>
<br/>
              7. Considérant, enfin, que dans le dernier état des conclusions de M. et Mme A..., telles qu'elles ont été précisées à l'audience par l'avocat au Conseil d'Etat et à la Cour de cassation qui les représente, leur demande d'admission provisoire à l'aide juridictionnelle, ainsi que celle que l'avocat au barreau qu'ils avaient initialement mandaté a présentée sur le fondement de l'article 37 de la loi du 10 juillet 1991, doivent être regardées comme abandonnées ; que par ailleurs, s'agissant d'une requête présentée au titre de l'article L. 521-2 du code de justice administrative, leur demande de remboursement des dépens est sans objet ; <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'ordonnance du 17 décembre 2013 du juge des référés du tribunal administratif de Lyon est annulée.<br/>
Article 2 : Il est enjoint au ministre de l'intérieur de faire délivrer à M. et MmeA..., dans un délai de quinze jours à compter de la notification de la présente ordonnance, une autorisation provisoire de séjour leur permettant de présenter en France leur demande d'asile. <br/>
Article 3 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 4 : La présente ordonnance sera notifiée à M. et Mme A...et au ministre de l'intérieur.<br/>
.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
