<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039127706</ID>
<ANCIEN_ID>JG_L_2019_03_000000420274</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/12/77/CETATEXT000039127706.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 25/03/2019, 420274, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-03-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420274</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Coralie Albumazard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:420274.20190325</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 30 avril 2018 et 8 février 2019 au secrétariat du contentieux du Conseil d'Etat, Mme B...A...demande au Conseil d'Etat d'annuler pour excès de pouvoir la décision implicite du Premier ministre, née du silence gardé sur sa demande présentée le 9 février 2018 et tendant à l'abrogation de l'article 1187 du code de procédure civile, dans sa rédaction issue des décrets n° 2002-361 du 15 mars 2002 et n° 2013-429 du 24 mai 2013.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
Vu : <br/>
- la Constitution, notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
- le code civil ;<br/>
- le code de procédure civile ;<br/>
		- le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Coralie Albumazard, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1.	MmeA..., qui se prévaut de sa qualité de mère de trois enfants mineurs ayant fait l'objet d'une mesure de placement dans le cadre d'une procédure d'assistance éducative ouverte devant le juge des enfants, a, le 9 février 2018, saisi le Premier ministre d'une demande d'abrogation de l'article 1187 du code de procédure civile, lequel prévoit que les parents peuvent consulter le dossier d'assistance éducative au greffe de la juridiction et aux heures et jours fixés par le juge des enfants sans pouvoir en prendre copie. Cette demande a fait l'objet d'une décision implicite de rejet acquise le 9 avril 2018, dont Mme A...demande l'annulation pour excès de pouvoir.<br/>
<br/>
<br/>
              Sur l'intervention de l'association Droit au logement Paris et environs :<br/>
<br/>
              2. L'association Droit au logement Paris et environs, qui a pour objet la défense du logement des individus et des familles mal logés ou sans logis, ne justifie pas d'un intérêt donnant qualité pour intervenir en demande devant le Conseil d'Etat à l'appui des conclusions tendant à l'annulation de la décision implicite de rejet de la demande d'abrogation de l'article 1187 du code de procédure civile relatif aux modalités d'accès au dossier d'assistance éducative, formée par MmeA.... Par suite, son intervention n'est pas recevable.<br/>
<br/>
<br/>
              Sur la légalité de la décision attaquée :<br/>
<br/>
              3. Aux termes de l'article 375 du code civil : " Si la santé, la sécurité ou la moralité d'un mineur non émancipé sont en danger, ou si les conditions de son éducation ou de son développement physique, affectif, intellectuel et social sont gravement compromises, des mesures d'assistance éducative peuvent être ordonnées par justice (...) ". Selon l'article 375-1 du même code, le juge des enfants doit toujours se prononcer en stricte considération de l'intérêt de l'enfant. Aux termes de l'article 1187 du code de procédure civile : " Dès l'avis d'ouverture de la procédure, le dossier peut être consulté au greffe, jusqu'à la veille de l'audition ou de l'audience, par l'avocat du mineur et celui de ses parents ou de l'un d'eux, de son tuteur, de la personne ou du service à qui l'enfant a été confié. L'avocat peut se faire délivrer copie de tout ou partie des pièces du dossier pour l'usage exclusif de la procédure d'assistance éducative. Il ne peut transmettre les copies ainsi obtenues ou la reproduction de ces pièces à son client. / Le dossier peut également être consulté, sur leur demande et aux jours et heures fixés par le juge, par les parents, le tuteur, la personne ou le représentant du service à qui l'enfant a été confié et par le mineur capable de discernement, jusqu'à la veille de l'audition ou de l'audience. / La consultation du dossier le concernant par le mineur capable de discernement ne peut se faire qu'en présence de ses parents ou de l'un d'eux ou de son avocat. En cas de refus des parents et si l'intéressé n'a pas d'avocat, le juge saisit le bâtonnier d'une demande de désignation d'un avocat pour assister le mineur ou autorise le service éducatif chargé de la mesure à l'accompagner pour cette consultation. / Par décision motivée, le juge peut, en l'absence d'avocat, exclure tout ou partie des pièces de la consultation par l'un ou l'autre des parents, le tuteur, la personne ou le représentant du service à qui l'enfant a été confié ou le mineur lorsque cette consultation ferait courir un danger physique ou moral grave au mineur, à une partie ou à un tiers. / Le dossier peut également être consulté, dans les mêmes conditions, par les services en charge des mesures prévues à l'article 1183 du présent code et aux articles 375-2 et 375-4 du code civil. / L'instruction terminée, le dossier est transmis au procureur de la République qui le renvoie dans les quinze jours au juge, accompagné de son avis écrit sur la suite à donner ou de l'indication qu'il entend formuler cet avis à l'audience. ".<br/>
<br/>
              4. En premier lieu, les dispositions contestées de l'article 1187 du code de procédure civile, qui autorisent les parents d'un mineur non émancipé faisant l'objet d'une procédure d'assistance éducative à consulter le dossier d'assistance éducative au greffe de la juridiction, aux heures et jours fixées par le juge, sans pouvoir en prendre copie, aménagent à leur profit un accès au dossier dans des conditions permettant d'assurer à la fois la connaissance suffisante des éléments le composant et la nécessaire protection due à l'enfant faisant l'objet d'une telle procédure. Elles ne portent, dès lors, atteinte ni aux droits de la défense garantis par l'article 16 de la Déclaration des droits de l'homme et du citoyen, ni au droit à un procès équitable garanti par l'article 6§1 de la Convention européenne des droits de l'homme et de sauvegarde des libertés fondamentales.<br/>
<br/>
              5. En second lieu, le principe d'égalité ne s'oppose pas à ce que l'autorité investie du pouvoir réglementaire règle de façon différente des situations différentes ni à ce qu'elle déroge à l'égalité pour des raisons d'intérêt général pourvu que, dans l'un comme l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la norme qui l'établit et ne soit pas manifestement disproportionnée au regard des motifs susceptibles de la justifier.<br/>
<br/>
              6. Si l'article 1187 du code de procédure civile limite le droit d'accès des parents au dossier d'assistance éducative à sa seule consultation au greffe de la juridiction et aux heures et jours fixés par le juge, il prévoit que l'avocat d'une partie à la procédure peut se voir délivrer copie de tout ou partie des pièces du dossier, avec l'interdiction de les transmettre à son client, et que le dossier d'assistance éducative est transmis au procureur de la République qui le renvoie dans les quinze jours au juge, accompagné de son avis écrit. L'avocat est, de par sa profession, soumis à des règles déontologiques particulières relatives notamment à l'usage qu'il peut être fait des pièces obtenues dans le cadre d'une procédure en justice. Le procureur de la République doit, en sa qualité de magistrat représentant du ministère public, donner un avis écrit sur les mesures à prendre dans le cadre de la procédure d'assistance éducative dans les quinze jours suivant la transmission du dossier. Par suite, la requérante n'est pas fondée à soutenir qu'en organisant des règles différentes en matière d'accès au dossier d'assistance éducative, les dispositions de l'article 1187 du code de procédure civile méconnaitraient le principe d'égalité.<br/>
<br/>
              7. Il résulte ce qui précède que la requérante n'est pas fondée à demander l'annulation pour excès de pouvoir de la décision implicite de refus d'abroger l'article 1187 du code de procédure civile.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de l'association Droit au logement Paris et environs n'est pas admise.<br/>
Article 2 : La requête de Mme A...est rejetée.<br/>
Article 3 : La présente décision sera notifiée à Madame B...A..., à l'association Droit au logement Paris et environs, au Premier ministre et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
