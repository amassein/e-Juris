<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037220727</ID>
<ANCIEN_ID>JG_L_2018_07_000000414569</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/22/07/CETATEXT000037220727.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 18/07/2018, 414569, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414569</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GHESTIN</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:414569.20180718</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Paris de condamner l'Etat à lui verser la somme de 15 000 euros en réparation des préjudices résultant de son absence de relogement et d'enjoindre à l'Etat d'assurer son relogement dans un délai d'un mois à compter de la décision à intervenir sous astreinte de 60 euros par jour de retard. Par un jugement n° 1612062/6-1 du 14 avril 2017, le magistrat désigné par le président du tribunal administratif a rejeté cette demande.<br/>
<br/>
              Par un pourvoi enregistré au secrétariat du contentieux du Conseil d'Etat le 25 septembre 2017, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat une somme de 3 000 euros à verser à son avocat, la SCP Gestin, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi n° 91-647 du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la construction et de l'habitation ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 91-1266 du 19 décembre 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Ghestin, avocat de M.B....<br/>
<br/>
<br/>
<br/>
<br/>
              Sur la fin de non-recevoir opposée par le ministre de la cohésion des territoires :<br/>
              1. Considérant qu'aux termes de l'article R. 821-1 du code de justice administrative : " Sauf disposition contraire, le délai de recours en cassation est de deux mois " ; qu'il ressort des pièces du dossier que le jugement attaqué a été notifié à M. B...le 22 avril 2017 ; qu'il a présenté le 15 mai 2017, dans le délai de recours contentieux, une demande d'aide juridictionnelle, adressée au bureau d'aide juridictionnelle près le tribunal de grande instance de Paris qui l'a transmise au bureau d'aide juridictionnelle près le Conseil d'Etat, conformément aux dispositions de l'avant-dernier alinéa de l'article 26 du décret n° 91-1266 du 19 décembre 1991 aux termes desquelles : " Dans tous les cas, la demande d'aide juridictionnelle peut être portée devant le bureau établi au siège du tribunal de grande instance du domicile du demandeur. Le bureau ainsi saisi transmet, s'il y a lieu, le dossier, après avoir vérifié qu'il contenait les pièces exigées par la loi, au bureau compétent pour statuer sur la demande " ; que le pourvoi enregistré au secrétariat du contentieux du Conseil d'Etat le 25 septembre 2017, soit, conformément à l'article 38 du même décret, dans le nouveau délai de deux mois ouvert par la décision du bureau du 28 juillet 2017 accordant l'aide juridictionnelle totale à la requérante, n'est donc pas tardif ; que la fin de non-recevoir doit être écartée ;<br/>
<br/>
              Sur le pourvoi de M.B... :<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que M. B...a été reconnu comme prioritaire et devant être relogé en urgence par une décision de la commission de médiation de Paris du 19 novembre 2015, en raison de l'absence de réponse adaptée à sa demande de logement social pendante depuis 1998 ; qu'en l'absence de relogement, le requérant a saisi le tribunal administratif de Paris d'une demande tendant à la condamnation de l'Etat à lui verser une somme de 15 000 euros en réparation des préjudices qu'il estime avoir subis et à ce qu'il soit enjoint au préfet de la région d'Ile-de-France, préfet de Paris, d'exécuter la décision de la commission de médiation ; qu'il se pourvoit en cassation contre le jugement du 14 avril 2017 par lequel le magistrat désigné par le tribunal administratif a rejeté sa demande ;<br/>
<br/>
              3. Considérant, d'une part,  qu'aux termes des deux premiers alinéas du II de l'article L. 441-2-3 du code de la construction et de l'habitation : " La commission de médiation peut être saisie par toute personne qui, satisfaisant aux conditions réglementaires d'accès à un logement locatif social, n'a reçu aucune proposition adaptée en réponse à sa demande de logement dans le délai fixé en application de l'article L. 441-1-4. / Elle peut être saisie sans condition de délai lorsque le demandeur, de bonne foi, est dépourvu de logement, menacé d'expulsion sans relogement, hébergé ou logé temporairement dans un établissement ou un logement de transition, un logement-foyer ou une résidence hôtelière à vocation sociale, logé dans des locaux impropres à l'habitation ou présentant un caractère insalubre ou dangereux. Elle peut également être saisie, sans condition de délai, lorsque le demandeur est logé dans des locaux manifestement suroccupés ou ne présentant pas le caractère d'un logement décent, s'il a au moins un enfant mineur, s'il présente un handicap au sens de l'article L. 114 du code de l'action sociale et des familles ou s'il a au moins une personne à charge présentant un tel handicap (...) " ;<br/>
<br/>
              4. Considérant, d'autre part, qu'aux termes du septième alinéa du II de l'article L. 441-2-3 du code de la construction et de l'habitation : " Après avis des maires des communes concernées et en tenant compte des objectifs de mixité sociale définis par les orientations mentionnées à l'article L. 441-1-5 et la convention mentionnée à l'article L. 441-1-6 ou par l'accord collectif intercommunal ou départemental, le représentant de l'Etat dans le département ou, en Ile-de-France, le représentant de l'Etat dans la région définit le périmètre au sein duquel ces logements doivent être situés et qui, en Ile-de-France, peut porter sur des territoires situés dans d'autres départements de la région après consultation du représentant de l'Etat territorialement compétent. Le représentant de l'Etat dans le département ou, en Ile-de-France, le représentant de l'Etat dans la région tient compte, dans des conditions fixées par décret, de la situation des quartiers prioritaires de la politique de la ville pour la définition de ce périmètre. Il fixe le délai dans lequel le demandeur doit être logé (...) " ; qu'aux termes de l'article R. 441-16-2 du même code : " La commission de médiation, lorsqu'elle détermine en application du II de l'article L. 441-2-3 les caractéristiques du logement devant être attribué en urgence à toute personne reconnue prioritaire, puis le préfet, lorsqu'il définit le périmètre au sein duquel ce logement doit être situé et fixe le délai dans lequel le bailleur auquel le demandeur a été désigné est tenu de le loger dans un logement tenant compte de ses besoins et capacités, apprécient ces derniers en fonction de la taille et de la composition du foyer au sens de l'article L. 442-12, de l'état de santé, des aptitudes physiques ou des handicaps des personnes qui vivront au foyer, de la localisation des lieux de travail ou d'activité et de la disponibilité des moyens de transport, de la proximité des équipements et services nécessaires à ces personnes. Ils peuvent également tenir compte de tout autre élément pertinent propre à la situation personnelle du demandeur ou des personnes composant le foyer. / (...) / Le préfet, lorsqu'il définit le périmètre au sein duquel le logement à attribuer doit être situé, et le bailleur, lorsqu'il propose une offre de logement tenant compte des besoins et capacités du demandeur, apprécient ces derniers à la date à laquelle ils lui proposent un logement, en prenant en considération les changements dans la taille ou la composition du foyer portés à leur connaissance ou survenus postérieurement à la décision de la commission " ; qu'aux termes de l'article R. 441-16-3 : " Le bailleur auquel le demandeur est désigné informe ce dernier ainsi que, le cas échéant, la personne assurant l'assistance prévue au troisième alinéa du II de l'article L. 441-2-3, dans la proposition de logement qu'il lui adresse, que cette offre lui est faite au titre du droit au logement opposable et attire son attention sur le fait qu'en cas de refus d'une offre de logement tenant compte de ses besoins et capacités il risque de perdre le bénéfice de la décision de la commission de médiation en application de laquelle l'offre lui est faite " ; qu'aux termes de l'article R. 441-2-2 : " (...) La demande de logement social comporte les rubriques suivantes : (...) h) Type de logement recherché et localisation souhaitée (...) " ; <br/>
<br/>
              5. Considérant qu'il résulte de ces dispositions que, lorsqu'un demandeur a été reconnu comme prioritaire et devant être relogé en urgence par une commission de médiation, il incombe au représentant de l'Etat dans le département de définir le périmètre au sein duquel le logement à attribuer doit être situé, sans être tenu par les souhaits de localisation formulés par l'intéressé dans sa demande de logement social ; que le refus, sans motif impérieux, d'une proposition de logement adaptée est de nature à faire perdre à l'intéressé le bénéfice de la décision de la commission de médiation, pour autant qu'il ait été préalablement informé de cette éventualité conformément à l'article R. 441-16-3 du code de la construction et de l'habitation ; que, par suite, en jugeant que la responsabilité de l'Etat à raison de la non exécution de la décision de la commission de médiation ne pouvait être engagée au seul motif que M. B...avait limité sa demande de logement social à trois arrondissements parisiens, alors que le préfet de la région d'Ile-de-France, préfet de Paris n'était pas tenu par ce souhait et qu'il devait proposer à l'intéressé un logement social dans le périmètre qu'il lui revenait de déterminer et qui pouvait même inclure d'autres départements de la région, le magistrat désigné par le président du tribunal administratif de Paris a commis une erreur de droit ; que, par conséquent, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. B...est fondé à demander l'annulation du jugement qu'il attaque ;<br/>
<br/>
              6. Considérant que, devant le Conseil d'Etat, M. B...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Ghestin, avocat de M.B..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 2 000 euros à verser à la SCP Ghestin ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 14 avril 2017 du magistrat désigné par le président du tribunal administratif de Paris est annulé.<br/>
Article 2 : L'affaire est renvoyée devant le tribunal administratif de Paris.<br/>
Article 3 : L'Etat versera à la SCP Ghestin une somme de 2 000 euros en application de l'article L. 761-1 du code de justice administrative et du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cet avocat renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et au ministre de la cohésion des territoires. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
