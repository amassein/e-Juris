<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032916608</ID>
<ANCIEN_ID>JG_L_2016_07_000000396026</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/91/66/CETATEXT000032916608.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 19/07/2016, 396026, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396026</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:396026.20160719</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Lille d'annuler la décision référencée 48SI du 13 février 2015 par laquelle le ministre de l'intérieur a constaté la perte de validité de son permis de conduire pour solde de points nul et lui a enjoint de restituer ce titre, ainsi que les décisions de retrait de points consécutives à six infractions commises entre le 25 août 2012 et le 10 août 2014. Par une ordonnance n° 1504384 du 9 novembre 2015, le président de la 2e chambre du tribunal administratif a rejeté sa demande.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 11 janvier et 11 avril 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la route ; <br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de M.B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B... a saisi, le 26 mai 2015, le tribunal administratif de Lille d'une demande tendant à l'annulation pour excès de pouvoir, d'une part, de six décisions de retrait de points consécutives à six infractions au code de la route commises entre le 25 août 2012 et le 20 août 2014 et, d'autre part, de la décision du 13 février 2015 par laquelle le ministre a constaté la perte de validité de son permis de conduire pour solde de points nul et lui a enjoint de restituer ce titre ; qu'il demande l'annulation de l'ordonnance du 9 novembre 2015 par laquelle le président de la 2e chambre du tribunal administratif de Lille a, sur le fondement des dispositions de l'article R. 222-1 du code de justice administrative, rejeté sa demande comme tardive et, par suite, entachée d'une irrecevabilité manifeste insusceptible d'être couverte en cours d'instance ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 611-1 du code de justice administrative : " La requête, le mémoire complémentaire annoncé dans la requête et le premier mémoire de chaque défendeur sont communiqués aux parties avec les pièces jointes dans les conditions prévues aux articles R. 611-3, R. 611-5 et R. 611-6 " ; qu'aux termes de l'article R. 613-3 : " Les mémoires produits après la clôture de l'instruction ne donnent pas lieu à communication et ne sont pas examinés par la juridiction (...) " ; que lorsque, postérieurement à la clôture de l'instruction, le juge est saisi d'une production, d'un mémoire ou d'une pièce émanant d'une partie à l'instance, il lui appartient de prendre connaissance de cette production pour déterminer s'il y a lieu de rouvrir l'instruction afin de la soumettre au débat contradictoire et de pouvoir en tenir compte dans le jugement de l'affaire ; que, s'il s'abstient de rouvrir l'instruction, le juge doit se borner à viser la production sans l'analyser et ne peut la prendre en compte sans entacher sa décision d'irrégularité ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'en l'espèce, le président de la 2e chambre du tribunal administratif de Lille a fixé la clôture de l'instruction au 4 septembre 2015 ; que le ministre de l'intérieur a produit le 11 septembre 2015 un premier mémoire en défense dans lequel il a soulevé une fin de non-recevoir tirée de la tardiveté de la demande de M.B..., en soutenant que ce dernier n'apportait pas la preuve de la réception effective de son recours gracieux par l'administration ; qu'en analysant ce mémoire et en se fondant sur l'argumentation qui y était développée pour rejeter la demande de M. B... comme manifestement irrecevable, sans avoir rouvert l'instruction ni soumis le mémoire du ministre de l'intérieur au débat contradictoire, le président de la 2e chambre du tribunal administratif de Lille a entaché son ordonnance d'une irrégularité qui en justifie l'annulation ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 9 novembre 2015 du président de la 2e chambre du tribunal administratif de Lille est annulée. <br/>
<br/>
		Article 2 : L'affaire est renvoyée au tribunal administratif de Lille. <br/>
<br/>
Article 3 : L'Etat versera la somme de 3 000 euros à M. B...au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
