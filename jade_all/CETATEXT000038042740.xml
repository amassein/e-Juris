<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038042740</ID>
<ANCIEN_ID>JG_L_2019_01_000000426479</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/04/27/CETATEXT000038042740.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 18/01/2019, 426479, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-01-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426479</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MEIER-BOURDEAU, LECUYER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:426479.20190118</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 21 décembre 2018 au secrétariat du contentieux du Conseil d'Etat, M. A...B...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, en premier lieu, de suspendre l'exécution de la circulaire n° 218-8 du 18 décembre 2018 du Conseil supérieur du notariat relative aux conséquences de la suppression des compétences notariales des consuls en tant qu'il ne prévoit pas la possibilité pour les notaires français de recevoir leurs actes à l'étranger dans les mêmes limites de compétence que les consuls avant la suspension de leurs compétences notariales et, en second lieu, de déclarer, par voie d'exception, l'illégalité des articles 8 et 9 du décret n° 71-942 du 26 novembre 1971 en conséquence de la suppression des compétences notariales des consuls. <br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence est remplie dès lors qu'en application de l'arrêté du 28 septembre 2018, portant abrogation de l'arrêté du 18 décembre 2017 fixant la liste des postes diplomatiques et consulaires dans lesquels sont exercées des attributions notariales, les compétences notariales des consuls de France à l'étranger seront supprimées dans la totalité des postes consulaires à partir du 1er janvier 2019 ;<br/>
              - il existe un doute sérieux quant à la légalité de la circulaire contestée ;<br/>
              - elle est entachée d'une erreur de droit dès lors qu'elle prive les notaires de la possibilité de recevoir des actes en matière notariale à l'étranger ;<br/>
              - l'impossibilité de recevoir des actes notariaux à l'étranger méconnaît le principe d'égalité ;<br/>
              - l'impossibilité de recevoir des actes notariaux à l'étranger n'est assortie d'aucun motif ;<br/>
              - la disparition des attributions notariales des consuls constitue pour les notaires un cas de force majeure qui les autorise à établir leurs actes à l'étranger ;<br/>
              - les articles 8 et 9 du décret du 26 novembre 1971 sont, par voie d'exception, entachés d'illégalité en conséquence de la suppression des compétences notariales des consuls.<br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 10 janvier 2019, le Conseil supérieur du notariat conclut à l'irrecevabilité de la requête. Il soutient que la circulaire n'ajoute rien au cadre législatif et règlementaire, que le requérant ne démontre pas son intérêt à agir et que la condition d'urgence n'est pas remplie.<br/>
<br/>
              Par un mémoire en défense et un nouveau mémoire, enregistrés les 10 et 14 janvier 2019, la garde des sceaux, ministre de la justice conclut au rejet de la requête. Elle soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par le requérant ne sont pas fondés.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le décret n° 71-942 du 26 novembre 1971 ;<br/>
              - le décret n° 91-152 du 7 février 1991 ;<br/>
              - l'arrêté du 28 septembre 2018 portant abrogation de l'arrêté du 18 décembre 2017 fixant la liste des postes diplomatiques et consulaires dans lesquels sont exercées des attributions notariales ; <br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M.B..., d'autre part, le Conseil supérieur du notariat et la garde de sceaux, ministre de la justice ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du lundi 14 janvier 2019 à 16 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - M.B... ;<br/>
<br/>
              - Me Meier, avocat au Conseil d'Etat et à la Cour de cassation, avocat du Conseil supérieur du notariat ;<br/>
<br/>
              - le représentant du Conseil supérieur du notariat ;<br/>
              - les représentants de la garde des sceaux, ministre de la justice ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Toutefois lorsque l'acte objet du litige n'est pas susceptible de recours, cette irrecevabilité affecte tant la demande d'annulation de cet acte que la demande tendant à sa suspension.<br/>
<br/>
              2. Par un arrêté du 28 septembre 2018, le ministre de l'Europe et des affaires étrangères a abrogé à partir du 1er janvier 2019 l'arrêté du 18 décembre 2017, fixant la liste des postes diplomatiques et consulaires de quarante-trois pays dans lesquels étaient exercées des attributions notariales et, par conséquent, supprimé les compétences notariales des consuls dans les pays étrangers. M. B...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, en premier lieu, de suspendre l'exécution de la circulaire n° 218-8 du 18 décembre 2018 du Conseil supérieur du notariat relative aux conséquences de la suppression des compétences notariales des consuls en tant qu'il ne prévoit pas la possibilité pour les notaires français de recevoir leurs actes à l'étranger dans les mêmes limites de compétence que les consuls avant la suspension de leurs compétences notariales et, en second lieu, de déclarer l'illégalité des articles 8 et 9 du décret du 26 novembre 1971 en conséquence de la suppression des compétences notariales des consuls.<br/>
<br/>
              3. Par la circulaire attaquée, le président du Conseil supérieur du notariat se borne à tirer les conséquences des dispositions réglementaires contenues dans l'arrêté du ministre des affaires étrangères du 28 septembre 2018. Cet arrêté prévoit que, à compter du 1er janvier 2019, les attributions notariales ne sont plus exercées au sein des postes diplomatiques et consulaires français. La circulaire comporte des préconisations adressées aux notaires définissant, eu égard à la situation de droit nouvelle créée par l'arrêté ministériel, les conduites qu'il est conseillé de tenir en fonction des conditions, parfois très différentes, dans lesquelles la fonction notariale ou assimilée est assurée dans les pays étrangers. Elle ne comporte, en elle-même, aucune disposition ayant un objet ou un effet impératif, autre que celui qui s'attache à la règlementation ou aux arrêts de la Cour de cassation qu'elle cite. <br/>
<br/>
              4. Il en résulte que la demande de suspension de cette circulaire, présentée par M.B..., dirigée contre un acte ne faisant pas grief, est irrecevable. <br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B...est rejetée.  <br/>
Article 2 : La présente ordonnance sera notifiée à M. A...B..., au Conseil supérieur du notariat et à la garde de sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
