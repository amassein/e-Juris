<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044462347</ID>
<ANCIEN_ID>JG_L_2021_12_000000450571</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/46/23/CETATEXT000044462347.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Formation spécialisée, 08/12/2021, 450571, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450571</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Formation spécialisée</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>Mme Nathalie Escaut</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois De Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEFSP:2021:450571.20211208</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 10 mars et 28 juin 2021, Mme D... C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du ministre de l'intérieur, révélée par le courrier de la présidente de la Commission nationale de l'informatique et des libertés en date du 21 janvier 2021, lui refusant l'accès aux données susceptibles de la concerner figurant dans le fichier des personnes recherchées et intéressant la sûreté de l'Etat ; <br/>
<br/>
              2°) d'enjoindre au ministre de l'intérieur d'effacer les informations la concernant figurant dans ce fichier, ou subsidiairement de les lui communiquer.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code de la sécurité intérieure ;<br/>
              - la loi n° 78-17 du 6 janvier 1978, modifiée notamment par la loi n° 2018-693 du 20 juin 2018 et l'ordonnance n° 2018-1125 du 12 décembre 2018 ;<br/>
              - le décret n° 2010-569 du 28 mai 2010 ;<br/>
              - le décret n° 2018-687 du 1er août 2018 ; <br/>
              - le décret n° 2019-536 du 29 mai 2019 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une séance à huis-clos, d'une part, Mme C... et la SCP Célice, Texidor, Perier, son avocat, et, d'autre part, le ministre de l'intérieur et la Commission nationale de l'informatique et des libertés, qui ont été mis à même de prendre la parole avant les conclusions ;<br/>
<br/>
              Et après avoir entendu en séance :<br/>
<br/>
              - le rapport de Mme Nathalie Escaut, conseillère d'Etat, <br/>
<br/>
              - et, hors la présence des parties, les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. En vertu de l'article 31 de la loi du 6 janvier 1978, les traitements de données à caractère personnel mis en œuvre pour le compte de l'Etat et intéressant la sûreté de l'Etat, la défense ou la sécurité publique sont autorisés par arrêté du ou des ministres compétents, pris après avis motivé de la Commission nationale de l'informatique et des libertés (CNIL), publié avec l'arrêté autorisant le traitement. Ceux de ces traitements qui portent sur des données mentionnées au I de l'article 6 de la même loi doivent être autorisés par décret en Conseil d'Etat pris après avis motivé de la Commission, publié avec ce décret. Un décret en Conseil d'Etat peut dispenser de publication l'acte réglementaire autorisant la mise en œuvre de ces traitements. Le sens de l'avis émis par la CNIL est alors publié avec ce décret.<br/>
<br/>
              2. L'article L. 841-2 du code de la sécurité intérieure prévoit que le Conseil d'Etat est compétent pour connaître, dans les conditions prévues au chapitre III bis du titre VII du livre VII du code de justice administrative, des requêtes concernant la mise en œuvre du droit d'accès aux données à caractère personnel et intéressant la sûreté de l'Etat qui sont contenues dans les traitements mis en œuvre pour le compte de l'Etat, dont la liste est fixée par décret en Conseil d'Etat. En vertu de l'article R. 841-2 du même code, figure notamment au nombre de ces traitements le fichier des personnes recherchées pour les seules données intéressant la sûreté de l'Etat mentionnées au 8° du III de l'article 2 du décret du 28 mai 2010 susvisé. <br/>
<br/>
              3. L'article L. 773-8 du code de justice administrative dispose que, lorsqu'elle traite des requêtes mentionnées au point 2 : " la formation de jugement se fonde sur les éléments contenus, le cas échéant, dans le traitement sans les révéler ni révéler si le requérant figure ou non dans le traitement. Toutefois, lorsqu'elle constate que le traitement ou la partie de traitement faisant l'objet du litige comporte des données à caractère personnel le concernant qui sont inexactes, incomplètes, équivoques ou périmées, ou dont la collecte, l'utilisation, la communication ou la conservation est interdite, elle en informe le requérant, sans faire état d'aucun élément protégé par le secret de la défense nationale. Elle peut ordonner que ces données soient, selon les cas, rectifiées, mises à jour ou effacées. Saisie de conclusions en ce sens, elle peut indemniser le requérant ". L'article R. 773-20 du même code précise que : " Le défendeur indique au Conseil d'Etat, au moment du dépôt de ses mémoires et pièces, les passages de ses productions et, le cas échéant, de celles de la Commission nationale de contrôle des techniques de renseignement, qui sont protégés par le secret de la défense nationale. /Les mémoires et les pièces jointes produits par le défendeur et, le cas échéant, par la Commission nationale de contrôle des techniques de renseignement sont communiqués au requérant, à l'exception des passages des mémoires et des pièces qui, soit comportent des informations protégées par le secret de la défense nationale, soit confirment ou infirment la mise en œuvre d'une technique de renseignement à l'égard du requérant, soit divulguent des éléments contenus dans le traitement de données, soit révèlent que le requérant figure ou ne figure pas dans le traitement. /Lorsqu'une intervention est formée, le président de la formation spécialisée ordonne, s'il y a lieu, que le mémoire soit communiqué aux parties, et à la Commission nationale de contrôle des techniques de renseignement, dans les mêmes conditions et sous les mêmes réserves que celles mentionnées à l'alinéa précédent ".<br/>
<br/>
              4. Il ressort des pièces du dossier que Mme C... a saisi la commission nationale de l'informatique et des libertés d'une demande d'accès aux données susceptibles de la concerner figurant dans le fichier des personnes recherchées et intéressant la sûreté de l'Etat ou la défense. Par une lettre du 21 janvier 2021, la présidente de la Commission nationale de l'informatique et des libertés a informé Mme C... qu'il avait été procédé à l'ensemble des vérifications demandées et que la procédure était terminée, sans lui apporter d'autres informations. Mme C... demande l'annulation du refus, révélé par ce courrier, du ministre de l'intérieur de lui donner accès aux données susceptibles de la concerner et figurant dans le fichier litigieux et d'enjoindre au ministre d'effacer les informations la concernant ou de les lui communiquer. <br/>
<br/>
              5. Le ministre de l'intérieur a produit devant le Conseil d'Etat, dans les conditions prévues à l'article R. 773-20 du code de justice administrative, un mémoire en défense portant sur les éléments susceptibles d'être relatifs à la situation de l'intéressée.<br/>
<br/>
              6. Il appartient à la formation spécialisée, créée par l'article L. 773-2 du code de justice administrative précité, saisie de conclusions dirigées contre le refus de communiquer les données relatives à une personne qui allègue être mentionnée dans un fichier figurant à l'article R. 841-2 du code de la sécurité intérieure, de vérifier, au vu des éléments qui lui ont été communiqués hors la procédure contradictoire, si le requérant figure ou non dans le fichier litigieux. Dans l'affirmative, il lui appartient d'apprécier si les données y figurant sont pertinentes au regard des finalités poursuivies par ce fichier, adéquates et proportionnées. Pour ce faire, elle peut relever d'office tout moyen ainsi que le prévoit l'article L. 773-5 du code de justice administrative. Lorsqu'il apparaît soit que le requérant n'est pas mentionné dans le fichier litigieux soit que les données à caractère personnel le concernant qui y figurent ne sont entachées d'aucune illégalité, la formation de jugement rejette les conclusions du requérant sans autre précision. Dans le cas où des informations relatives au requérant figurent dans le fichier litigieux et apparaissent entachées d'illégalité, soit que les données à caractère personnel le concernant sont inexactes, incomplètes, équivoques ou périmées, soit que leur collecte, leur utilisation, leur communication ou leur consultation est interdite, elle en informe le requérant sans faire état d'aucun élément protégé par le secret de la défense nationale. Cette circonstance, le cas échéant relevée d'office par le juge dans les conditions prévues à l'article R. 773-21 du code de justice administrative, implique nécessairement que l'autorité gestionnaire du fichier rétablisse la légalité en effaçant ou en rectifiant, dans la mesure du nécessaire, les données illégales. Dans pareil cas, doit être annulée la décision implicite refusant de procéder à un tel effacement ou à une telle rectification.<br/>
<br/>
              7. La formation spécialisée a procédé à l'examen des éléments fournis par le ministre et la CNIL. Il résulte de cet examen, qui s'est déroulé selon les modalités décrites au point précédent, qui n'a révélé aucune illégalité, notamment au regard de l'article 8 de la convention européenne de sauvegarde des droits et des libertés fondamentales, que les conclusions de Mme C..., qui ne peut utilement se prévaloir de l'absence de motivation de la décision du ministre, doivent être rejetées, y compris ses conclusions à fin d'injonction. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de Mme C... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à Mme D... C... et au ministre de l'intérieur.<br/>
Copie en sera adressée à la Commission nationale de l'informatique et des libertés. <br/>
              Délibéré à l'issue de la séance du 22 novembre 2021 où siégeaient : M. Rémy Schwartz, président de la formation spécialisée, présidant ; M. Thomas Andrieu, conseiller d'Etat et Mme Nathalie Escaut, conseillère d'Etat-rapporteure.<br/>
                 Le président : <br/>
                 Signé : M. Rémy Schwartz<br/>
<br/>
<br/>
 		La rapporteure : <br/>
      Signé : Mme Nathalie Escaut<br/>
<br/>
                 La secrétaire :<br/>
                 Signé : Mme B... A...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
