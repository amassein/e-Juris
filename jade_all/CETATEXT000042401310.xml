<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042401310</ID>
<ANCIEN_ID>JG_L_2020_10_000000427552</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/40/13/CETATEXT000042401310.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 05/10/2020, 427552, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427552</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Pierre Ramain</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:427552.20201005</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et deux mémoires en réplique, enregistrés le 1er février 2019, le 12 août 2020 et le 16 septembre 2020 au secrétariat du contentieux du Conseil d'Etat, la société Mobil International Petroleum Corporation demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite du Premier ministre rejetant la demande qu'elle a présentée le 3 octobre 2018 tendant à l'abrogation de l'article 1er du décret du 6 mai 1995 relatif à l'obligation de constituer et de conserver des stocks stratégiques de produits pétroliers dans les territoires d'outre-mer et les collectivités d'outre-mer de Mayotte et de Saint-Pierre-et-Miquelon en tant qu'il s'applique en Nouvelle Calédonie ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre d'édicter dans un délai de 3 mois à compter de la décision à intervenir, un décret ramenant à 8 % ou 10 % le niveau de stocks stratégiques de carburant devant être constitués et maintenus par les compagnies pétrolières exerçant une activité en Nouvelle-Calédonie ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'énergie ; <br/>
              - le code de la défense ; <br/>
              - la loi n° 93-1 du 4 janvier 1993 ; <br/>
              - le décret n° 95-597 du 6 mai 1995 ; <br/>
              - le décret n° 2007-585 du 23 avril 2007 ; <br/>
              - le décret n° 2016-55 du 29 janvier 2016 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Ramain, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. La société Mobil International Petroleum Corporation a saisi le Premier ministre d'une demande tendant à l'abrogation de l'article 1er du décret du 6 mai 1995 en tant qu'il fixe, pour la Nouvelle-Calédonie, le volume des stocks stratégiques de produits pétroliers que chaque opérateur est tenu de constituer en application de l'article 57 de la loi du 4 janvier 1993 portant dispositions diverses relatives aux départements d'outre-mer, aux territoires d'outre-mer et aux collectivités territoriales de Mayotte et de Saint-Pierre-et-Miquelon. Elle demande l'annulation pour excès de pouvoir de la décision implicite de rejet résultant du silence gardé par le Premier ministre sur cette demande. <br/>
<br/>
              2. Si les dispositions de l'article 1er du décret du 6 mai 1995 relatif à l'obligation de constituer et de conserver des stocks stratégiques de produits pétroliers dans les territoires d'outre-mer et les collectivités territoriales de Mayotte et de Saint-Pierre-et-Miquelon ont été abrogées par le décret du 23 avril 2007 relatif à certaines dispositions réglementaires de la première partie du code de la défense (Décrets en Conseil d'Etat), elles ont été reprises et codifiées à l'article R. 1682-10 du code de la défense. Par suite, la requête de la société Mobil International Petroleum Corporation doit être regardée comme dirigée contre le refus du Premier ministre d'abroger les dispositions de cet article en tant qu'elles sont applicables en Nouvelle-Calédonie. <br/>
<br/>
              3. L'article L. 671-1 du code de l'énergie, qui reprend les dispositions de l'article 57 de la loi du 4 janvier 1993, dispose  que : " I. _ Toute personne physique ou morale autre que l'Etat qui met à la consommation ou livre à l'avitaillement des aéronefs civils des produits pétroliers, en Nouvelle-Calédonie, en Polynésie française, aux îles Wallis-et-Futuna ou à Saint-Pierre-et-Miquelon, est tenue de constituer et de conserver en permanence un stock de réserve de ces produits de cette collectivité territoriale./ II. _ Ce stock doit être au moins égal à une proportion fixée par voie réglementaire des quantités qu'elle a mises à la consommation ou livrées à l'avitaillement en franchise des aéronefs civils au cours des douze mois précédents dans chacune de collectivités mentionnées à l'alinéa précédent. / (...) / Un décret en Conseil d'Etat fixe les conditions d'application du présent article ". Aux termes de l'article R. 1682-10 du code de la défense, " Le volume des stocks stratégiques de produits pétroliers que chaque opérateur est tenu de constituer et de conserver en proportion des quantités de produits ayant fait l'objet des opérations mentionnées à l'article 57 de la loi n° 93-1 du 4 janvier 1993 portant dispositions diverses relatives aux départements d'outre-mer, aux territoires d'outre-mer et aux collectivités territoriales de Mayotte et de Saint-Pierre-et-Miquelon est fixé à 20 % de ces quantités dans chaque collectivité mentionnée au premier alinéa de l'article 57 de ladite loi ". <br/>
<br/>
              4. Le mécanisme de stocks stratégiques de produits pétroliers institué par l'article L. 671-1 du code de l'énergie, cité au point 3, tend à prémunir l'Etat du risque de rupture d'approvisionnement dans ces produits à la fois en cas de crise internationale et de crise locale. La société requérante relève que l'arrêté du 25 mars 2016 relatif à la constitution de stocks stratégiques en France métropolitaine, en Martinique, en Guadeloupe, en Guyane, à La Réunion et à Mayotte a fixé un volume de stocks stratégiques compris, selon les produits pétroliers, entre 7 % et 13 % pour la zone constituée par la Martinique, la Guadeloupe et la Guyane, d'une part, et entre 8 % et 13 % pour La Réunion, d'autre part. Il ressort de la réponse de la ministre de la transition écologique à la mesure d'instruction diligentée par la 10ème chambre, d'une part, que ces taux ont été déterminés selon une méthodologie fondée sur quatre critères tenant à la dépendance énergétique du territoire, à l'état de la logistique pétrolière, à l'analyse des risques d'indisponibilité immédiate des stocks stratégiques et à la situation concurrentielle du marché et, d'autre part, que cette méthodologie n'a été appliquée qu'aux départements d'outre-mer et non aux collectivités régies par l'article 74 de la Constitution, ni à la Nouvelle-Calédonie, qui ont gardé " leur taux historique de 20 % ". Pour expliquer les raisons pour lesquelles un taux de 20 % a été maintenu pour la Nouvelle- Calédonie, la ministre s'est bornée à se prévaloir, de manière générale, de l'incertitude sur l'avenir institutionnel de ce territoire et du niveau de son taux de dépendance énergétique, qui correspond au rapport entre ses importations nettes d'énergie et sa consommation brute et qui intègre l'ensemble des sources d'énergie. Dans ces conditions, faute d'élément rationnel et objectif de nature à justifier le taux de 20 % retenu en Nouvelle-Calédonie, la ministre ne peut être regardée comme ayant donné un motif légal à la décision de refus qu'elle a opposée à la société requérante. Par suite, celle-ci est fondée à demander l'annulation de la décision qu'elle attaque.<br/>
<br/>
              5. L'exécution de la présente décision implique seulement que le Premier ministre réexamine la demande de la société requérante dans un délai de six mois à compter de la notification de la présente décision. <br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à la société Mobil International Petroleum Corporation au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le refus du Premier ministre d'abroger les dispositions de l'article R. 1682-10 du code de la défense en tant qu'elles sont applicables à la Nouvelle-Calédonie est annulé. <br/>
Article 2 : Il est enjoint au Premier ministre, dans un délai de six mois à compter de la notification de la présente décision, de réexaminer la demande d'abrogation de la société Mobil International Petroleum Corporation. Le surplus des conclusions de la société aux fins d'injonction est rejeté.<br/>
Article 3 : L'Etat versera à la société Mobil International Petroleum Corporation une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
Article 4 : La présente décision sera notifiée à la société Mobil International Petroleum Corporation, au Premier ministre et à la ministre de la transition écologique. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
