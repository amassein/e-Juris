<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032491590</ID>
<ANCIEN_ID>JG_L_2016_05_000000381121</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/49/15/CETATEXT000032491590.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 04/05/2016, 381121, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>381121</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR</AVOCATS>
<RAPPORTEUR>Mme Esther de Moustier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:381121.20160504</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société anonyme Bastide Le Confort Médical a demandé au tribunal administratif de Nîmes de la décharger des rappels de taxe sur la valeur ajoutée mis à sa charge au titre de la période du 1er juillet 2003 au 31 décembre 2005 et des pénalités pour manquement délibéré dont ils ont été assortis. Par un jugement n° 1100760 du 24 février 2012, le tribunal administratif a déchargé la société de la pénalité pour manquement délibéré correspondant aux rappels de taxe sur la valeur ajoutée relatifs aux fauteuils releveurs et a rejeté le surplus de sa demande.<br/>
<br/>
              Par un arrêt n° 12LY21771 du 3 avril 2014, la cour administrative d'appel de Lyon a rejeté l'appel formé par la société Bastide Le Confort Médical contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 10 juin et 10 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, la société Bastide Le Confort Médical demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt :<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Esther de Moustier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, Texidor, avocat de la société Bastide Le Confort Médical ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Bastide Le Confort Médical a fait l'objet d'une vérification de comptabilité à l'issue de laquelle l'administration a remis en cause l'application des taux réduit de taxe sur la valeur ajoutée de 5,5 % sur les fauteuils releveurs et de 2,1 % sur les forfaits d'oxygénothérapie qu'elle commercialisait et lui a notifié des rappels de taxe sur la valeur ajoutée au titre de la période du 1er juillet 2003 au 31 décembre 2005 ; que ces rappels ont été assortis d'une pénalité de 40 % pour manquement délibéré ; qu'après avoir vainement contesté les rappels et les pénalités relatifs aux fauteuils releveurs ainsi que les seules pénalités s'agissant des forfaits d'oxygénothérapie, la société en a demandé la décharge au tribunal administratif de Nîmes ; que celui-ci a accordé la décharge de la pénalité pour manquement délibéré relative aux fauteuils releveurs et a rejeté le surplus de sa demande ; que la cour administrative d'appel de Lyon a rejeté l'appel formé par la société contribuable contre ce jugement en tant qu'il rejetait le surplus de sa demande ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 278 quinquies du code général des impôts en vigueur pendant la période des impositions en litige : " La taxe sur la valeur ajoutée est perçue au taux de 5,50 % en ce qui concerne les opérations d'achat, d'importation, d'acquisition intracommunautaire, de vente, de livraison, de commission, de courtage ou de façon, portant sur : / (...) c. Les équipements spéciaux, dénommés aides techniques et autres appareillages, dont la liste est fixée par arrêté du ministre chargé du budget et qui sont conçus exclusivement pour les personnes handicapées en vue de la compensation d'incapacités graves (...) " ; que l'article 30-0 B de l'annexe IV à ce code établit cette liste qui comprend notamment les " matériels de transfert : élévateurs et releveurs hydrauliques ou électriques, lève-personnes " ; qu'en estimant que les fauteuils releveurs correspondent à des produits de relaxation et de confort destinés au grand public et ne constituent pas un matériel exclusivement conçu pour des personnes handicapées, la cour a porté sur les faits de l'espèce qui lui étaient soumis une appréciation souveraine exempte de dénaturation ; que, dès lors, en jugeant que les fauteuils releveurs ne peuvent être regardés comme des matériels de transfert au sens de l'article 30-0 B de l'annexe IV au code général des impôts pouvant bénéficier de l'application d'un taux réduit de taxe sur la valeur ajoutée sur le fondement de l'article 278 quinquies du code général des impôts, la cour n'a commis ni erreur de droit ni erreur de qualification juridique des faits ;<br/>
<br/>
              3. Considérant que, dans ses écritures devant les juges du fond, la société requérante a mentionné des énonciations de la doctrine administrative indiquant que l'application de l'article 278 quinquies du code général des impôts ne doit pas être fondée sur un critère tenant à l'acquéreur du matériel, dit critère " de destination " ; qu'en jugeant que les fauteuils releveurs ne constituent pas un matériel exclusivement conçu pour des personnes handicapées, la cour a écarté l'application de ce critère ; qu'elle a ainsi suffisamment motivé son arrêt ;<br/>
<br/>
              4. Considérant qu'aux termes de l'article 1729 du code général des impôts dans sa rédaction applicable au litige : " Les inexactitudes ou les omissions relevées dans une déclaration ou un acte comportant l'indication d'éléments à retenir pour l'assiette ou la liquidation de l'impôt ainsi que la restitution d'une créance de nature fiscale dont le versement a été indûment obtenu de l'État entraînent l'application d'une majoration de : / a. 40 % en cas de manquement délibéré (...) " ; que la cour administrative d'appel a relevé que lors d'un précédent contrôle, l'administration fiscale avait précisé, à la demande de la société Bastide Le Confort Médical, dans une réponse écrite datée du 26 novembre 2003, les conditions d'application du taux réduit de 2,1 % aux forfaits d'oxygénothérapie ; que si la société a, devant la cour, invoqué une jurisprudence au demeurant sans rapport direct avec le litige, qui aurait pu la conduire à appliquer ce taux réduit, elle n'a jamais contesté le redressement portant sur le taux de taxe appliqué aux forfaits d'oxygénothérapie qu'elle commercialisait ni tenté de démontrer devant le tribunal administratif qu'un contribuable de bonne foi pouvait hésiter sur le taux de taxe à appliquer pour ces produits ; qu'ainsi, en jugeant que l'administration fiscale avait établi la réalité du caractère délibéré des manquements et le bien-fondé des pénalités correspondantes, la cour a donné aux faits qu'elle a souverainement appréciés une qualification juridique exacte ; qu'elle n'a, par suite, ni commis d'erreur de droit, ni insuffisamment motivé son arrêt ;<br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que la SA Bastide Le Confort Médical n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>                D E C I D E :<br/>
                               --------------<br/>
<br/>
Article 1er : Le pourvoi de la SA Bastide Le Confort Médical est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société anonyme Bastide Le Confort Médical et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
