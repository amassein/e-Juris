<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032699028</ID>
<ANCIEN_ID>JG_L_2016_06_000000394350</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/69/90/CETATEXT000032699028.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 08/06/2016, 394350, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-06-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394350</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Clément Malverti</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:394350.20160608</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 2 novembre 2015 et 20 avril 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 7 octobre 2015 l'ayant déchu de la nationalité française ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code civil ;<br/>
              - le code pénal ;<br/>
              - la loi n° 2006-64 du 23 janvier 2006 ;<br/>
              - le décret n° 93-1362 du 30 décembre 1993 ;<br/>
              - l'arrêt de la Cour de justice de l'Union européenne C-135/08 du 2 mars 2010 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Clément Malverti, auditeur, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M. A...; <br/>
<br/>
              Vu la note en délibéré, enregistrée le 23 mai 2016, présentée par M. A...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes de l'article 25 du code civil : " L'individu qui a acquis la qualité de Français peut, par décret pris après avis conforme du Conseil d'Etat, être déchu de la nationalité française, sauf si la déchéance a pour résultat de le rendre apatride : / 1° S'il est condamné pour un acte qualifié de crime ou délit constituant une atteinte aux intérêts fondamentaux de la Nation ou pour un crime ou un délit constituant un acte de terrorisme (...) " ; que l'article 25-1 de ce code, tel que résultant de la loi du 23 janvier 2006, ne permet la déchéance de la nationalité dans ce cas qu'à la condition que les faits aient été commis moins de quinze ans auparavant et qu'ils aient été commis soit avant l'acquisition de la nationalité française, soit dans un délai de quinze ans à compter de cette acquisition ; <br/>
<br/>
              2.	Considérant que l'article 421-2-1 du code pénal qualifie d'acte de terrorisme " le fait de participer à un groupement formé ou à une entente établie en vue de la préparation, caractérisée par un ou plusieurs faits matériels, d'un des actes de terrorisme " mentionnés aux articles 421-1 et 421-2 du code pénal ;<br/>
<br/>
              3.	Considérant que M. B...A...a été déchu de la nationalité française par un décret du 7 octobre 2015 pris sur le fondement des articles 25 et 25-1 du code civil, au motif qu'il a été condamné par un arrêt de la cour d'appel de Paris en date du 1er juillet 2008 pour avoir participé à une association de malfaiteurs en vue de la préparation d'un acte de terrorisme, faits prévus par l'article 421-2-1 du code pénal ;<br/>
<br/>
              Sur la régularité de la procédure juridictionnelle devant le Conseil d'Etat :<br/>
<br/>
              4.	Considérant que si M. A...fait valoir que le Conseil d'Etat ne pourrait, sans méconnaître le principe d'impartialité, statuer au contentieux sur sa requête qui est dirigée contre un décret pris, conformément à l'article 25 du code civil, sur avis conforme du Conseil d'Etat, il résulte des termes mêmes de la Constitution, et notamment de ses articles 37, 38, 39 et 61-1 tels qu'interprétés par le Conseil constitutionnel, que le Conseil d'Etat est simultanément chargé par la Constitution de l'exercice de fonctions administratives et placé au sommet de l'un des deux ordres de juridiction qu'elle reconnaît ; que ces dispositions n'ont ni pour objet ni pour effet de porter les avis rendus par les formations administratives du Conseil d'Etat à la connaissance de ses membres siégeant au contentieux ; qu'au demeurant, ainsi qu'il résulte des dispositions de l'article R. 122-21-1 du code de justice administrative, les membres du Conseil d'Etat qui ont participé à un avis rendu sur un projet d'acte soumis par le Gouvernement ne participent pas au jugement des recours mettant en cause ce même acte ; qu'enfin, en vertu de l'article R. 122-21-2 du même code, lorsque le Conseil d'Etat est saisi d'un recours contre un acte pris après avis d'une de ses formations consultatives, il est loisible au requérant de demander la liste des membres ayant pris part à la délibération de cet avis ; que, dans ces conditions, le moyen tiré de la méconnaissance des stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ne peut qu'être écarté ;<br/>
<br/>
              Sur la légalité externe du décret attaqué :<br/>
<br/>
              5.	Considérant qu'aux termes de l'article 61 du décret du 30 décembre 1993 : " Lorsque le Gouvernement décide de faire application des articles 25 et 25-1 du code civil, il notifie les motifs de droit et de fait justifiant la déchéance de la nationalité française, en la forme administrative ou par lettre recommandée avec demande d'avis de réception (...). L'intéressé dispose d'un délai d'un mois à dater de la notification ou de la publication de l'avis au Journal officiel pour faire parvenir au ministre chargé des naturalisations ses observations en défense. A l'expiration de ce délai, le Gouvernement peut déclarer, par décret motivé pris sur avis conforme du Conseil d'Etat, que l'intéressé est déchu de la nationalité française " ;<br/>
<br/>
              6.	Considérant, en premier lieu, qu'après avoir cité les textes applicables et rappelé que M.A..., qui a acquis la nationalité française en 2001, a été condamné par un arrêt de la cour d'appel de Paris du 1er juillet 2008 à une peine de six ans d'emprisonnement assortie d'une période de sûreté d'une durée de quarante-huit mois pour des faits commis entre 2000 et 2004 et qualifiés de participation à une association de malfaiteurs en vue de la préparation d'un acte de terrorisme, le décret attaqué énonce que les conditions légales permettant de prononcer la déchéance de la nationalité française doivent être regardées comme réunies sans qu'aucun élément relatif à la situation personnelle du requérant et aux circonstances de l'espèce ne justifie qu'il y soit fait obstacle ; que, dans ces conditions, le décret attaqué satisfait à l'exigence de motivation posée par l'article 61 du décret du 30 décembre 1993 ;<br/>
<br/>
              7.	Considérant, en second lieu, que M. A...a été informé qu'une procédure de déchéance de la nationalité française était engagée à son encontre et a été mis en mesure de présenter ses observations en défense en temps utile avant l'intervention du décret attaqué ; qu'aucun texte ni aucun principe ne faisait obligation à l'administration de répondre aux observations qu'il a produites ; qu'il ressort ainsi des pièces du dossier que le décret attaqué a été précédé de la procédure contradictoire prévue par l'article 61 du décret du 30 décembre 1993 ; que le moyen tiré d'une méconnaissance des droits de la défense ne peut, dès lors, qu'être écarté ;<br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              8.	Considérant, en premier lieu, qu'il ne ressort pas des pièces du dossier que, pour prononcer la déchéance de la nationalité française de M.A..., le Premier ministre se soit exclusivement fondé sur la condamnation prononcée par la cour d'appel de Paris, sans procéder à un examen de l'ensemble des circonstances propres à sa situation ; qu'ainsi, le moyen tiré de ce que le décret attaqué serait entaché d'erreur de droit faute d'avoir procédé à un examen particulier des circonstances de l'espèce doit être écarté ;<br/>
<br/>
              9.	Considérant, en deuxième lieu, que si, en matière d'édiction de sanction administrative, sont seuls punissables les faits constitutifs de manquement à des obligations définies par des dispositions législatives ou réglementaires en vigueur à la date où ces faits ont été commis, en revanche, et réserve faite du cas où il en serait disposé autrement, s'appliquent immédiatement les textes fixant les modalités des poursuites et les formes de la procédure, alors même qu'ils conduisent à réprimer des manquements commis avant leur entrée en vigueur ; qu'il en va ainsi des textes fixant les délais dans lesquels une sanction administrative peut être prononcée sauf si les délais antérieurement applicables étaient expirés avant leur entrée en vigueur ;<br/>
<br/>
              10.	Considérant que la déchéance de la nationalité française constitue une sanction administrative ; qu'en l'espèce, les derniers faits pour lesquels M. A...a été condamné ont été commis en 2004 ; que la loi du 23 janvier 2006 a porté de dix à quinze ans le délai fixé à l'article 25-1 du code civil dans lequel la déchéance de la nationalité peut être prononcée à compter de la perpétration des faits à l'origine de la condamnation pour un crime ou un délit constituant un acte de terrorisme ; qu'à la date de l'entrée en vigueur de cette loi, le délai de dix ans antérieurement applicable dans lequel la sanction de déchéance de la nationalité pouvait être prononcée à l'encontre de M. A...n'était pas expiré ; que, par suite, le moyen tiré de ce qu'en faisant application du délai prévu par l'article 25-1 du code civil dans sa rédaction issue de la loi du 23 janvier 2006, le décret attaqué se serait fondé sur des dispositions législatives qui n'auraient pas été applicables ne peut qu'être écarté ;<br/>
<br/>
              11.	Considérant, en troisième lieu, que, ainsi qu'il a été dit, le législateur a prévu la possibilité de déchoir de la nationalité française des personnes ayant fait l'objet d'une condamnation pénale pour un crime ou un délit constituant un acte de terrorisme ; que la conformité des dispositions adoptées sur ce point par le législateur au principe de nécessité des délits et des peines qui découle de l'article 8 de la Déclaration des droits de l'homme et du citoyen de 1789 ne saurait être contestée devant le Conseil d'Etat statuant au contentieux en dehors de la procédure prévue à l'article 61-1 de la Constitution ;<br/>
<br/>
              12.	Considérant, en quatrième lieu, qu'aux termes de l'article 4 du protocole n° 7 à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " Nul ne peut être poursuivi ou puni pénalement par des juridictions du même Etat en raison d'une infraction pour laquelle il a déjà été acquitté ou condamné par un jugement définitif conformément à la loi et à la procédure pénale de cet Etat " ; que ces stipulations ne trouvent à s'appliquer que pour les poursuites en matière pénale ; que la déchéance de la nationalité constitue une sanction de nature administrative ; que, par suite, le requérant ne saurait utilement soutenir que le décret attaqué méconnaîtrait ces stipulations ;<br/>
<br/>
              13.	Considérant, en cinquième lieu, qu'il ressort des pièces du dossier que M. A... a été condamné à une peine de six ans d'emprisonnement assortie d'une période de sûreté d'une durée de quarante-huit mois pour avoir apporté un soutien financier et logistique à une organisation dite " groupe islamiste combattant marocain " (GICM), proche de l'organisation " Salafiya Jihadia " à laquelle sont liés les auteurs des attentats qui ont été commis à Casablanca au Maroc le 16 mai 2003, faits qualifiés par le juge pénal de participation à une association de malfaiteurs en vue de la préparation d'un acte de terrorisme ; qu'il ressort des constatations de fait auxquelles a procédé le juge pénal qu'il a, notamment, travaillé dans des sociétés commerciales soutenant l'activité du GICM et obtenu des passeports destinés à permettre, après falsification, la circulation des membres du GICM ; qu'eu égard à la nature et à la gravité des faits commis par le requérant qui ont conduit à sa condamnation pénale, la sanction de déchéance de la nationalité française n'a pas revêtu, dans les circonstances de l'espèce, un caractère disproportionné ; que le comportement ultérieur de l'intéressé ne permet pas de remettre en cause cette appréciation ;<br/>
<br/>
              14.	Considérant, en sixième lieu, qu'aux termes de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " Toute personne a droit au respect de sa vie privée et familiale, de son domicile et de sa correspondance. / 2. Il ne peut y avoir ingérence d'une autorité publique dans l'exercice de ce droit que pour autant que cette ingérence est prévue par la loi et qu'elle constitue une mesure qui, dans une société démocratique, est nécessaire à la sécurité nationale, à l'intégrité territoriale ou à la sûreté publique, à la défense de l'ordre et à la prévention du crime, à la protection de la santé ou de la morale, à la protection de la réputation ou des droits d'autrui (...) " ; <br/>
<br/>
              15.	Considérant que la sanction de déchéance de la nationalité, prévue par les articles 25 et 25-1 du code civil, a pour objectif de renforcer la lutte contre le terrorisme ; qu'un décret portant déchéance de la nationalité française est par lui-même dépourvu d'effet sur la présence sur le territoire français de celui qu'il vise, comme sur ses liens avec les membres de sa famille, et n'affecte pas, dès lors, le droit au respect de sa vie familiale ; qu'en revanche, un tel décret affecte un élément constitutif de l'identité de la personne concernée et est ainsi susceptible de porter atteinte au droit au respect de sa vie privée ; qu'en l'espèce, eu égard à la gravité des faits commis par le requérant, le décret attaqué n'a pas porté une atteinte disproportionnée au droit au respect de sa vie privée garanti par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              16.	Considérant, en dernier lieu, que le détournement de pouvoir allégué n'est pas établi ;<br/>
<br/>
              17.	Considérant qu'il résulte de tout ce qui précède que M. A...n'est pas fondé à demander l'annulation pour excès de pouvoir du décret qu'il attaque ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B...A..., au Premier ministre et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
