<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029702476</ID>
<ANCIEN_ID>JG_L_2014_10_000000382747</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/70/24/CETATEXT000029702476.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère SSJS, 24/10/2014, 382747, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-10-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382747</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Philippe Combettes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:382747.20141024</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              Mme L...S...épouseF..., Mme J...C...et M Q...A...ont demandé au tribunal administratif de Toulouse d'annuler les opérations électorales qui se sont déroulées le 23 mars 2014 pour l'élection des conseillers municipaux de la commune de Montmaurin (Haute-Garonne) et, à titre subsidiaire, d'annuler  l'élection de M. M...H..., M. G...I..., M. P...D..., Mme R...K..., M. N...B...et M. E...O.... Par un jugement n° 1401443 du 17 juin 2014, le tribunal administratif de Toulouse a annulé l'élection de M.H..., M.I..., M.D..., MmeK..., M. B...et M. O...et a rejeté le surplus des conclusions de la protestation. <br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par une requête, enregistrée le 16 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, M.B..., MmeK..., M.H..., M.I..., M. O...et M. D...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement n° 1401443 du tribunal administratif de Toulouse du 17 juin 2014 ; <br/>
<br/>
              2°) de mettre à la charge de MmeS..., Mme C...et M. A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu : <br/>
              - les autres pièces du dossier ;<br/>
              - le code électoral ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Combettes, maître des requêtes en service extraordinaire,  <br/>
<br/>
- les conclusions de M. Alexandre Lallet, rapporteur public.<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              Sur la régularité du jugement attaqué :<br/>
<br/>
              1. D'une part, il résulte des visas du jugement attaqué que le tribunal administratif de Toulouse a, conformément aux dispositions de l'article R. 741-2 du code de justice administrative, analysé le mémoire en défense produit devant lui par les requérants. D'autre part, le tribunal ayant accueilli l'un des griefs des protestataires, tenant à l'absence de passage d'une électrice par l'isoloir, il n'était pas tenu d'examiner leur autre grief relatif à l'irrégularité du vote de la même électrice ni, par suite, l'argumentation soulevée en défense sur ce point par les requérants. Par suite, les requérants ne sont pas fondés à soutenir que le jugement serait entaché d'irrégularité.<br/>
<br/>
              Sur le bien-fondé du jugement attaqué :<br/>
<br/>
              2. L'article L. 62 du code électoral dispose que : " (...) Sans quitter la salle du scrutin, [l'électeur] doit se rendre isolément dans la partie de la salle aménagée pour le soustraire aux regards pendant qu'il met son bulletin dans l'enveloppe (...) ". Aux termes du premier alinéa de l'article L. 64 du même code : " Tout électeur atteint d'infirmité certaine et le mettant dans l'impossibilité d'introduire son bulletin dans l'enveloppe et de glisser celle-ci dans l'urne ou de faire fonctionner la machine à voter est autorisé à se faire assister par un électeur de son choix ". Les dispositions de l'article L. 64 permettent à l'électeur atteint d'une infirmité certaine de se faire assister par un électeur de son choix, qui peut, le cas échéant, entrer dans l'isoloir avec lui, mais ne le dispensent pas du respect de l'obligation prévue par l'article L. 62. Or l'utilisation de l'isoloir fait partie de l'ensemble des mesures voulues par le législateur pour assurer le secret du vote et la sincérité des opérations électorales. Un nombre de votes correspondant au nombre d'électeurs qui n'ont pas respecté cette obligation doit ainsi être annulé, même en l'absence de fraude.<br/>
<br/>
              3. Il n'est pas contesté que le 23 mars 2014, à l'occasion du premier tour de l'élection des conseillers municipaux de la commune de Montmaurin, une électrice a voté sans être préalablement passée par l'isoloir, en méconnaissance des dispositions de l'article L. 62 du code électoral. Il ne résulte pas de l'instruction que cette électrice, qui présentait un bras en écharpe, ait été dans l'impossibilité de passer par l'isoloir avec l'électrice qui l'assistait avant d'introduire dans l'urne l'enveloppe contenant son bulletin de vote. Par suite, et alors même que l'intéressée n'aurait pas subi de pression ou de contrainte et qu'elle aurait pris soin de ne pas faire apparaître le sens de son vote, c'est à bon droit que le tribunal a retranché un vote, irrégulièrement émis, tant du nombre des suffrages exprimés que du nombre des voix obtenues par les candidats proclamés élus lors de ce scrutin et que, constatant que le nombre des suffrages obtenus par M.H..., M. I..., M.D..., MmeK..., M. B...et M. O...devenait inférieur à la majorité absolue, il a rectifié en conséquence les résultats de l'élection.<br/>
<br/>
              4. Il résulte de ce qui précède que les requérants ne sont pas fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Toulouse a annulé l'élection de M.H..., M.I..., M.D..., MmeK..., M. B...et M.O.... <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de MmeS..., Mme C...et M. A..., qui ne sont pas, dans la présente instance, les parties perdantes.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M.B..., MmeK..., M.H..., M.I..., M. O...et M. D... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. N...B..., Mme R...K..., M. M... H..., M. G...I..., M. E...O..., M. P...D..., Mme L...S...épouseF..., Mme J...C...et M. Q...A....<br/>
            Copie en sera adressée pour information au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
