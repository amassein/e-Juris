<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029955358</ID>
<ANCIEN_ID>JG_L_2014_12_000000362053</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/95/53/CETATEXT000029955358.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 23/12/2014, 362053, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362053</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Julia Beurton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:362053.20141223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 20 août 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par l'Association lacanienne internationale, dont le siège est 25, rue de Lille à Paris (75007), représentée par son président ; l'association requérante demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir, premièrement, la décision n° 2012.0015/DC/SBP du collège de la Haute Autorité de santé (HAS) du 7 mars 2012 portant adoption de la recommandation de bonne pratique " Autisme et autres troubles envahissants du développement : interventions éducatives et thérapeutiques coordonnées chez l'enfant et l'adolescent ", deuxièmement, cette recommandation, troisièmement, la décision du 18 juin 2012 par laquelle le président de la HAS a rejeté son recours gracieux tendant au retrait de cette recommandation et, quatrièmement, si elle existe, la décision de l'Agence nationale de l'évaluation et de la qualité des établissements et services sociaux et médico-sociaux (ANESM) portant adoption de cette recommandation ; <br/>
<br/>
              2°) de mettre à la charge de la Haute Autorité de santé et de l'Agence nationale de l'évaluation et de la qualité des établissements et services sociaux et médico-sociaux la somme de 4 000 euros chacune au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 9 décembre 2014, présentée par la Haute autorité de santé ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 12 décembre 2014, présentée par le ministre des affaires sociales, de la santé et des droits des femmes ;<br/>
<br/>
              Vu le code de l'action sociale et des familles ; <br/>
<br/>
              Vu le code de la sécurité sociale ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Julia Beurton, auditeur,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 161-37 du code de la sécurité sociale : " La Haute Autorité de santé, autorité publique indépendante à caractère scientifique dotée de la personnalité morale, est chargée de : (...) / 2° Elaborer les guides de bon usage des soins ou les recommandations de bonne pratique, procéder à leur diffusion et contribuer à l'information des professionnels de santé et du public dans ces domaines (...) " ; qu'aux termes du premier alinéa de l'article L. 312-8 du code de l'action sociale et des familles : " Les établissements et services mentionnés à l'article L. 312-1 procèdent à des évaluations de leurs activités et de la qualité des prestations qu'ils délivrent, au regard notamment de procédures, de références et de recommandations de bonnes pratiques professionnelles validées ou, en cas de carence, élaborées, selon les catégories d'établissements ou de services, par l'Agence nationale de l'évaluation et de la qualité des établissements et services sociaux et médico-sociaux (...) " ; que sur le fondement de ces dispositions, la Haute Autorité de santé et l'Agence nationale de l'évaluation et de la qualité des établissements et services sociaux et médico-sociaux ont décidé d'élaborer conjointement des recommandations de bonne pratique et des recommandations de bonnes pratiques professionnelles sur l'autisme et les troubles envahissants du développement ; qu'au terme d'une procédure commune associant notamment des professionnels de santé et des professionnels exerçant dans des établissements sociaux et médico-sociaux ainsi que des associations de familles concernées, le comité d'orientation stratégique de l'Agence nationale de l'évaluation et de la qualité des établissements et services sociaux et médico-sociaux a émis un avis favorable à la recommandation " Autisme et autres troubles envahissants du développement : interventions éducatives et thérapeutiques coordonnées chez l'enfant et l'adolescent " lors de sa séance du 6 décembre 2011 et le collège de la Haute Autorité de santé a adopté cette recommandation lors de sa séance du 7 mars 2012 ; que l'Association lacanienne internationale demande l'annulation pour excès de pouvoir de cette recommandation et de la décision du 18 juin 2012 par laquelle le président du collège de la Haute Autorité de santé a rejeté son recours gracieux tendant à son retrait, ainsi que de la délibération du collège de la Haute Autorité de santé du 7 mars 2012 et, le cas échéant, de la décision de l'Agence nationale de l'évaluation et de la qualité des établissements et services sociaux et médico-sociaux adoptant cette recommandation ; que les conclusions dirigées contre cette délibération et cette décision doivent être regardées comme tendant à l'annulation de la recommandation en tant qu'elle concerne, d'une part, les professionnels de santé, d'autre part, les établissements et services sociaux et médico-sociaux ; <br/>
<br/>
              Sur la fin de non-recevoir opposée par la Haute Autorité de santé et l'Agence nationale de l'évaluation et de la qualité des établissements et services sociaux et médico-sociaux :<br/>
<br/>
              2. Considérant que la publicité donnée à la recommandation attaquée sur les sites internet de la Haute Autorité de santé et de l'Agence nationale de l'évaluation et de la qualité des établissements et services sociaux et médico-sociaux n'a pas été de nature à faire courir le délai de recours contentieux à l'égard des tiers ; que, par suite, la Haute Autorité de santé et l'Agence nationale de l'évaluation et de la qualité des établissements et services sociaux et médico-sociaux ne sont pas fondées à soutenir, en tout état de cause, que la requête de l'Association lacanienne internationale serait tardive ; <br/>
<br/>
              Sur la recommandation attaquée, en tant qu'elle concerne les établissements et services sociaux et médico-sociaux :<br/>
<br/>
              3. Considérant qu'aux termes du neuvième alinéa de l'article L. 312-8 du code de l'action sociale et des familles : " L'Agence nationale de l'évaluation et de la qualité des établissements et services sociaux et médico-sociaux prend ses décisions après avis d'un conseil scientifique indépendant dont la composition est fixée par décret. Elle est un groupement d'intérêt public constitué entre l'Etat, la Caisse nationale de solidarité pour l'autonomie et d'autres personnes morales (...) " ; qu'aux termes de l'article D. 312-195 du même code : " Le conseil scientifique de l'Agence nationale de l'évaluation et de la qualité des établissements et services sociaux et médico-sociaux comprend quinze personnes choisies en raison de leurs compétences scientifiques dans le domaine des sciences sociales, de l'évaluation, de la qualité et de l'action sociale et médico-sociale. / Le président et les membres du conseil scientifique sont nommés par arrêté du ministre chargé de l'action sociale pour une durée de trois ans " ; <br/>
<br/>
              4. Considérant qu'il est constant que le conseil scientifique de l'Agence nationale de l'évaluation et de la qualité des établissements et services sociaux et médico-sociaux n'a pas été consulté préalablement à l'adoption de la recommandation attaquée ; que l'Agence fait certes valoir l'impossibilité de réunir le conseil scientifique entre la date d'expiration du mandat de ses membres, nommés pour trois ans par un arrêté du 5 octobre 2007, et les nouvelles nominations auxquelles il a été procédé par arrêté du 28 mars 2012 ; que, toutefois, elle ne peut utilement se prévaloir de cette circonstance pour soutenir que sa consultation présentait le caractère d'une formalité impossible, dès lors qu'il appartenait à l'Etat, membre du groupement d'intérêt public, de procéder en temps utile à la nomination des membres du conseil et de permettre ainsi sa consultation conformément aux dispositions de l'article L. 312-8 du code de l'action sociale et des familles ; que, dès lors, l'Association lacanienne internationale est fondée à soutenir que l'Agence nationale de l'évaluation et de la qualité des établissements et services sociaux et médico-sociaux a adopté la recommandation attaquée au terme d'une procédure irrégulière ; que ce défaut de consultation, qui a privé d'une garantie les établissements et services auxquelles la recommandation peut être opposée, a constitué une irrégularité de nature à entacher sa légalité, en tant qu'elle concerne les établissements et services sociaux et médico-sociaux ; qu'en revanche, il ne ressort pas des pièces du dossier qu'il ait été susceptible d'exercer, en l'espèce, une influence sur le sens de l'acte attaqué en tant qu'il comporte des recommandations à l'attention des professionnels de santé ou qu'il ait privé ces professionnels d'une garantie ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de la requête relatifs à la procédure suivie devant l'Agence nationale de l'évaluation et de la qualité des établissements et services sociaux et médico-sociaux, la recommandation attaquée doit être annulée en tant qu'elle concerne les établissements et services sociaux et médico-sociaux ; <br/>
<br/>
              Sur la recommandation attaquée, en tant qu'elle concerne les professionnels de santé :<br/>
<br/>
              6. Considérant, en premier lieu, que la Haute Autorité de santé tenait des dispositions de l'article L. 161-37 du code de la sécurité sociale, citées au point 1, la compétence pour adopter une recommandation de bonne pratique relative au traitement de l'autisme et des troubles envahissants du développement ; que la méconnaissance de l'accord-cadre conclu le 14 juin 2010 entre la Haute Autorité de santé et l'Agence nationale de l'évaluation et de la qualité des établissements et services sociaux et médico-sociaux, prévoyant une signature conjointe des travaux par les deux institutions, ne peut être utilement invoquée au soutien du présent recours pour excès de pouvoir ; que, par suite, la circonstance que la recommandation attaquée soit illégale en tant qu'elle émane de l'Agence nationale de l'évaluation et de la qualité des établissements et services sociaux et médico-sociaux et, dès lors, insusceptible de produire des effets juridiques à l'égard des établissements et services sociaux et médico-sociaux, n'entraîne pas nécessairement son illégalité en tant qu'elle émane de la Haute Autorité de santé ; <br/>
<br/>
              7. Considérant, en deuxième lieu, qu'en vertu de l'article R. 161-77 du code de la sécurité sociale, le collège de la Haute Autorité de santé " arrête son règlement intérieur, qui fixe : / 1° Ses modalités de délibération, notamment les règles de convocation, de quorum et de suppléance du président ainsi que les modalités selon lesquelles il traite les demandes qui lui sont adressées (...) " ; qu'aux termes de l'article I-4 de ce règlement intérieur, dans sa rédaction alors  applicable : " (...) Le collège se réunit sur convocation de son président ou à la demande de la moitié de ses membres (...) " ; qu'aux termes de l'article I-5 du même règlement, dans sa rédaction alors applicable : " (...) Sauf cas d'urgence, l'ordre du jour des réunions est transmis aux participants au plus tard sept jours avant la séance, et six jours au plus tard pour les dossiers (...) " ; qu'aux termes de l'article I-6 de ce règlement : " Le collège ne peut valablement délibérer que si cinq membres au moins sont présents (...) " ;<br/>
<br/>
              8. Considérant qu'il ressort des pièces du dossier que les membres du collège de la Haute Autorité de santé ont été régulièrement convoqués le 27 février 2012 à la séance du 7 mars 2012 au cours de laquelle la recommandation a été adoptée, qu'ils ont eu communication dans les délais requis du texte et des annexes de la recommandation soumis à leur délibération et que le quorum lors de cette séance a été atteint ; que, par suite, le moyen tiré de l'irrégularité de la délibération du collège de la Haute Autorité de santé manque en fait ; <br/>
<br/>
              9. Considérant, en troisième lieu, qu'il est loisible à la Haute Autorité de santé de choisir la procédure d'évaluation qui lui semble la plus pertinente lors de l'élaboration d'une recommandation de bonne pratique ; qu'elle a pu, en l'espèce, sans commettre d'erreur manifeste d'appréciation, opter pour la méthode des " recommandations par consensus formalisé ", qui prévoit notamment, après une analyse critique de la littérature scientifique existante, l'intervention d'un groupe de pilotage, chargé de rédiger l'argumentaire scientifique et des propositions de recommandations, et d'un groupe de cotation, responsable de l'identification, par vote, des points d'accord et de désaccord entre ses membres, de façon à aboutir à la sélection des propositions faisant l'objet d'un consensus ; qu'il ne ressort pas des pièces du dossier que la composition des groupes de travail constitués en vue de l'élaboration de la recommandation attaquée, qui comprenaient des partisans des diverses approches existant dans le traitement de l'autisme, y compris psychanalytique, aurait été manifestement déséquilibrée ; <br/>
<br/>
              10. Considérant, en quatrième lieu, qu'il ressort des pièces du dossier que les approches thérapeutiques faisant intervenir la psychanalyse et la psychothérapie institutionnelle dans le traitement de l'autisme et des troubles envahissants du comportement n'ont pas réuni, lors de l'élaboration de la recommandation attaquée, un accord suffisant des membres du groupe de cotation pour qu'elles soient qualifiées d'interventions recommandées, ni d'ailleurs de méthodes non recommandées ; qu'ainsi, l'association requérante n'est pas fondée à soutenir qu'au regard de la position d'une majorité des professionnels de santé, et notamment des pédopsychiatres, la recommandation attaquée serait entachée d'inexactitude matérielle en ce qu'elle qualifie ces approches d'" interventions globales non consensuelles " ; <br/>
<br/>
              11. Considérant, en cinquième lieu, qu'il ressort des pièces du dossier que la revue de littérature scientifique sur laquelle s'appuie la recommandation attaquée comprend des travaux de littérature scientifique anglo-saxonne postérieurs à 2009 ainsi que des travaux de littérature scientifique française ; qu'il ressort des pièces du dossier que la recommandation attaquée recourt aux définitions utilisées par la classification internationale des maladies, conformément aux préconisations faites par la Fédération française de psychiatrie pour homogénéiser la formulation des diagnostics et faciliter les comparaisons en recherche, et rappelle la diversité des situations cliniques regroupées sous l'expression de troubles envahissants du développement ; que si elle préconise les interventions précoces fondées sur une approche éducative, comportementale et développementale, elle ne valorise pas exclusivement ces méthodes mais cite notamment la psychothérapie parmi les interventions thérapeutiques à associer selon les besoins ; qu'il ne ressort pas des pièces du dossier que, par l'appréhension de l'état des connaissances scientifiques sur laquelle elle s'appuie, par le choix de son champ, incluant les interventions éducatives et thérapeutiques chez les enfants et les adolescents atteints d'un trouble envahissant du développement mais non le dépistage et le diagnostic précoces de l'autisme, par la présentation des interventions proposées ou par la façon dont les commentaires des représentants des approches psychanalytiques ont été pris en compte, la recommandation attaquée serait entachée d'erreur manifeste d'appréciation ;<br/>
<br/>
              12. Considérant, enfin, que l'association requérante n'est pas fondée à soutenir que la recommandation attaquée serait inintelligible et équivoque ;<br/>
<br/>
              13. Considérant qu'il résulte de tout ce qui précède que l'Association lacanienne internationale n'est fondée à demander l'annulation de la recommandation " Autisme et autres troubles envahissants du développement : interventions éducatives et thérapeutiques coordonnées chez l'enfant et l'adolescent " qu'en tant qu'elle concerne les établissements et services sociaux et médico-sociaux ; qu'en revanche, le surplus de ses conclusions à fin d'annulation doit être rejeté ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              14. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative par l'Association lacanienne internationale et par la Haute Autorité de santé ; que les dispositions de cet article font obstacle à ce qu'il soit fait droit aux conclusions de l'Agence nationale de l'évaluation et de la qualité des établissements et services sociaux et médico-sociaux présentées au même titre ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La recommandation " Autisme et autres troubles envahissants du développement : interventions éducatives et thérapeutiques coordonnées chez l'enfant et l'adolescent " est annulée en tant qu'elle concerne les établissements et services sociaux et médico-sociaux. <br/>
Article 2 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 3 : Les conclusions de la Haute Autorité de santé et de l'Agence nationale de l'évaluation et de la qualité des établissements et services sociaux et médico-sociaux présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à l'Association lacanienne internationale, à la Haute Autorité de santé et à l'Agence nationale de l'évaluation et de la qualité des établissements et services sociaux et médico-sociaux.<br/>
Copie en sera adressée pour information à la ministre des affaires sociales, de la santé et des droits des femmes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
