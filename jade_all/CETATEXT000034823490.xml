<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034823490</ID>
<ANCIEN_ID>JG_L_2017_05_000000400761</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/82/34/CETATEXT000034823490.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 29/05/2017, 400761, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-05-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400761</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CAPRON</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:400761.20170529</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 17 juin 2016 au secrétariat du contentieux du Conseil d'Etat, la Fédération des établissements hospitaliers et d'aide à la personne, privés à but non lucratif demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet né du silence gardé par le Premier ministre sur sa demande d'abrogation du décret n° 2015-1732 du 22 décembre 2015 relatif à l'obligation de mise à jour et de publication par les chambres régionales de l'économie sociale et solidaire de la liste des entreprises régies par l'article 1er de la loi n° 2014-856 du 31 juillet 2014 relative à l'économie sociale et solidaire ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 2014-856 du 31 juillet 2014 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Capron, avocat de la Fédération des établissements hospitaliers et d'aide à la personne, privés à but non lucratif ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du II de l'article 1er de la loi du 31 juillet 2014 relative à l'économie sociale et solidaire : " (...) II. - L'économie sociale et solidaire est composée des activités de production, de transformation, de distribution, d'échange et de consommation de biens ou de services mises en oeuvre : / 1° Par les personnes morales de droit privé constituées sous la forme de coopératives, de mutuelles ou d'unions relevant du code de la mutualité ou de sociétés d'assurance mutuelles relevant du code des assurances, de fondations ou d'associations régies par la loi du 1er juillet 1901 relative au contrat d'association ou, le cas échéant, par le code civil local applicable aux départements du Bas-Rhin, du Haut-Rhin et de la Moselle ; / 2° Par les sociétés commerciales qui, aux termes de leurs statuts, remplissent les conditions suivantes : / a) Elles respectent les conditions fixées au I du présent article ; / b) Elles recherchent une utilité sociale au sens de l'article 2 de la présente loi ; / c) Elles appliquent les principes de gestion suivants : / - le prélèvement d'une fraction définie par arrêté du ministre chargé de l'économie sociale et solidaire et au moins égale à 20 % des bénéfices de l'exercice, affecté à la constitution d'une réserve statutaire obligatoire, dite " fonds de développement ", tant que le montant total des diverses réserves n'atteint pas une fraction, définie par arrêté du ministre chargé de l'économie sociale et solidaire, du montant du capital social. Cette fraction ne peut excéder le montant du capital social. Les bénéfices sont diminués, le cas échéant, des pertes antérieures ; / - le prélèvement d'une fraction définie par arrêté du ministre chargé de l'économie sociale et solidaire et au moins égale à 50 % des bénéfices de l'exercice, affecté au report bénéficiaire ainsi qu'aux réserves obligatoires. Les bénéfices sont diminués, le cas échéant, des pertes antérieures ; / - l'interdiction pour la société d'amortir le capital et de procéder à une réduction du capital non motivée par des pertes, sauf lorsque cette opération assure la continuité de son activité, dans des conditions prévues par décret. Le rachat de ses actions ou parts sociales est subordonné au respect des exigences applicables aux sociétés commerciales, dont celles prévues à l'article L. 225-209-2 du code de commerce. ". Par ailleurs, aux termes du onzième alinéa de l'article 6 de cette loi : " Dans des conditions définies par décret, les chambres régionales de l'économie sociale et solidaire tiennent à jour et assurent la publication de la liste des entreprises de l'économie sociale et solidaire, au sens des 1° et 2° du II de l'article 1er, qui sont situées dans leur ressort. ".<br/>
<br/>
              2. Pour l'application de l'article 6 de la loi du 31 juillet 2014 cité ci-dessus, le pouvoir réglementaire a prévu, à l'article 1er du décret attaqué, que " conformément aux dispositions du onzième alinéa de l'article 6 de la loi du 31 juillet 2014 susvisée, chaque chambre régionale de l'économie sociale et solidaire met à jour et publie, selon une fréquence au moins annuelle, la liste des entreprises de l'économie sociale et solidaire au sens des 1° et 2° du II de l'article 1er de la loi du 31 juillet 2014 susvisée, dont le siège social ou l'un des établissements est situé dans le ressort territorial de cette chambre régionale. ". Pour demander l'annulation de la décision implicite de rejet de sa demande tendant à l'abrogation de ce décret, la fédération requérante soutient que le pouvoir réglementaire a méconnu les dispositions de l'article 6 de la loi du 31 juillet 2014 qui, selon elle, imposeraient, en vertu de l'intention manifestée par le législateur au cours des travaux préparatoires de cette loi, l'établissement et la publication de deux listes distinctes des entreprises de l'économie sociale et solidaire relevant respectivement des 1° et 2° du II de cet article.<br/>
<br/>
              3. Il ne résulte néanmoins pas des termes de l'article 6 de la loi du 31 juillet 2014 que le pouvoir réglementaire aurait dû imposer aux chambres régionales de l'économie sociale et solidaire l'établissement et la publication de deux listes distinctes des entreprises relevant respectivement des 1° et 2° de l'article 1er de la loi du 31 juillet 2014. En particulier, et contrairement à ce que soutient la fédération requérante, il ne résulte pas de ces dispositions que la mention " au sens des 1° et 2° du II de l'article 1er " ait pour objet d'imposer une telle obligation au pouvoir réglementaire, alors que le législateur n'a prescrit, dans le ressort de chaque chambre régionale de l'économie sociale et solidaire, que la tenue et la publication d'une seule " liste des entreprises de l'économie sociale et solidaire ".<br/>
<br/>
              4. Il résulte de ce qui précède, sans qu'il soit besoin de recourir aux travaux préparatoires de l'article 6 de la loi du 31 janvier 2014, que la fédération requérante n'est pas fondée à demander l'annulation de la décision implicite par laquelle le Premier ministre a rejeté sa demande tendant à l'abrogation du décret n° 2015-1732 du 22 décembre 2015 relatif à l'obligation de mise à jour et de publication par les chambres régionales de l'économie sociale et solidaire de la liste des entreprises régies par l'article 1er de la loi n° 2014-856 du 31 juillet 2014 relative à l'économie sociale et solidaire. Par suite, sa requête doit être rejetée, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la Fédération des établissements hospitaliers et d'aide à la personne, privés à but non lucratif est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la Fédération des établissements hospitaliers et d'aide à la personne privés à but non lucratif et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
