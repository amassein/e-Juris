<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035990965</ID>
<ANCIEN_ID>JG_L_2017_10_000000412827</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/99/09/CETATEXT000035990965.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 06/10/2017, 412827, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-10-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412827</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>Mme Marie-Gabrielle Merloz</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Yohann Bénard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:412827.20171006</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 27 juillet 2017, la Fédération bancaire française a demandé au Conseil d'Etat d'annuler l'arrêté du 14 juin 2017 modifiant l'arrêté du 29 avril 2015 précisant le format et le contenu de la fiche standardisée d'information relative à l'assurance ayant pour objet le remboursement d'un prêt, en tant qu'il modifie le dernier alinéa de la partie 8 du modèle de fiche standardisée d'information annexée à l'arrêté du 29 avril 2015. <br/>
<br/>
              A l'appui de cette requête, la Fédération bancaire française a, en application de l'article 23-5 de l'ordonnance n°58-1067 du 7 novembre 1958, demandé au Conseil d'Etat, par un mémoire distinct enregistré le 27 juillet 2017, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution, d'une part, des dispositions du a du 1° du I de l'article 10 de la loi n° 2017-203 du 21 février 2017 ratifiant les ordonnances n° 2016-301 du 14 mars 2016 relative à la partie législative du code de la consommation et n° 2016-351 du 25 mars 2016 sur les contrats de crédits aux consommateurs relatifs aux biens immobiliers à usage d'habitation et simplifiant le dispositif de mise en oeuvre des obligations en matière de conformité et de sécurité des produits et services, en tant qu'elles modifient l'article L. 313-30 du code de la consommation ainsi que, d'autre part, des dispositions du V de l'article 10 de cette même loi, qui commandent l'application des premières.  <br/>
<br/>
              Elle soutient que ces dispositions, qui sont applicables au litige et n'ont pas déjà été déclarées conformes à la Constitution, portent atteinte aux principes d'égalité devant la loi et d'égalité devant les charges publiques garantis par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen, à la garantie des droits protégée par l'article 16 de la même Déclaration et au droit au maintien de l'économie des conventions légalement conclues résultant des articles 4 et 16 de la même Déclaration.  <br/>
<br/>
              Par une intervention, enregistrée le 28 juillet 2017, les sociétés Assurances du Crédit mutuel - Vie SA (ACM Vie SA), BPCE Vie SA, Cardif Assurance Vie, CNP Assurances, HSBC Assurances Vie (France), Prédica - Prévoyance dialogue du Crédit agricole, Sogecap et Suravenir demandent au Conseil d'Etat de faire droit à la demande de renvoi au Conseil constitutionnel de la question prioritaire de constitutionnalité soulevée par la Fédération bancaire française.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code des assurances ; <br/>
              - le code de la consommation ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Gabrielle Merloz, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Yohann Bénard, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la Fédération Bancaire Française ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. D'une part, aux termes du premier alinéa de l'article L. 313-30 du code de la consommation, dans sa version antérieure à l'article 10 contesté de la loi du 21 février 2017 ratifiant les ordonnances n° 2016-301 du 14 mars 2016 relative à la partie législative du code de la consommation et n° 2016-351 du 25 mars 2016 sur les contrats de crédits aux consommateurs relatifs aux biens immobiliers à usage d'habitation et simplifiant le dispositif de mise en oeuvre des obligations en matière de conformité et de sécurité des produits et services : " Jusqu'à la signature par l'emprunteur de l'offre mentionnée à l'article L. 313-24, le prêteur ne peut pas refuser en garantie un autre contrat d'assurance dès lors que ce contrat présente un niveau de garantie équivalent au contrat d'assurance de groupe qu'il propose. Il en est de même lorsque l'emprunteur fait usage du droit de résiliation mentionné au premier alinéa de l'article L. 113-12-2 du code des assurances ou au deuxième alinéa de l'article L. 221-10 du code de la mutualité dans un délai de douze mois à compter de la signature de l'offre de prêt mentionnée à l'article L. 313-24 ". Le a du 1° du I de l'article 10 de la loi du 21 février 2017 a notamment complété la seconde phrase de cet alinéa par les mots " ou qu'il fait usage du droit de résiliation annuel mentionné au deuxième alinéa de l'article L. 113-12 du code des assurances ou au premier alinéa de l'article L. 221-10 du code de la mutualité ". En vertu du IV et du V de ce même article, ce droit de résiliation annuel est applicable, respectivement, aux offres de prêts émises à compter du 22 février 2017 et, à compter du 1er janvier 2018, aux contrats d'assurance en cours d'exécution à cette date. <br/>
<br/>
              3. D'autre part, aux termes du deuxième alinéa de l'article L. 113-12 du code des assurances : " (...) l'assuré a le droit de résilier le contrat à l'expiration d'un délai d'un an, en envoyant une lettre recommandée à l'assureur au moins deux mois avant la date d'échéance. Ce droit appartient, dans les mêmes conditions, à l'assureur. Il peut être dérogé à cette règle pour les contrats individuels d'assurance maladie et pour la couverture des risques autres que ceux des particuliers. Le droit de résilier le contrat tous les ans doit être rappelé dans chaque police. Le délai de résiliation court à partir de la date figurant sur le cachet de la poste. / (...) ". <br/>
<br/>
              4. La Fédération bancaire française soutient que les dispositions précitées du a du 1° du I de l'article 10 de la loi précitée du 21 février 2017 ainsi que celles du V de ce même article portent atteinte aux principes d'égalité devant la loi et d'égalité devant les charges publiques garantis par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen, à la garantie des droits protégée par l'article 16 de cette même déclaration et au droit au maintien de l'économie des conventions légalement conclues résultant des articles 4 et 16 de cette même déclaration. <br/>
<br/>
              Sur l'intervention : <br/>
<br/>
              5. Les sociétés Assurances du Crédit mutuel - Vie SA (ACM Vie SA), BPCE Vie SA, Cardif Assurance Vie, CNP Assurances, HSBC Assurances Vie (France), Prédica - Prévoyance dialogue du Crédit agricole, Sogecap et Suravenir, qui sont intervenues au soutien de la requête de la Fédération bancaire française tendant à l'annulation de l'arrêté du 14 juin 2017 modifiant l'arrêté du 29 avril 2015 précisant le format et le contenu de la fiche standardisée d'information relative à l'assurance ayant pour objet le remboursement d'un prêt, en tant qu'il modifie le dernier alinéa de la partie 8 du modèle de fiche standardisée d'information annexée à l'arrêté du 29 avril 2015, interviennent, par mémoire distinct, au soutien de la question prioritaire de constitutionnalité soulevée par celle-ci. Elles justifient d'un intérêt suffisant à l'annulation de l'arrêté attaqué par la Fédération bancaire française. Par suite, leur intervention est recevable. <br/>
<br/>
              Sur la question prioritaire de constitutionnalité : <br/>
<br/>
              6. Les dispositions contestées sont applicables au présent litige et n'ont pas déjà été déclarées conformes à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel. Le moyen tiré de ce qu'elles portent atteinte aux droits et libertés garantis par la Constitution, notamment à la garantie des droits qui résulte de l'article 16 de la Déclaration des droits de l'homme et du citoyen, soulève une question présentant un caractère sérieux. Il y a donc lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité à la Constitution du a du 1° du I de l'article 10 de la loi n° 2017-203 du 21 février 2017 ratifiant les ordonnances n° 2016-301 du 14 mars 2016 relative à la partie législative du code de la consommation et n° 2016-351 du 25 mars 2016 sur les contrats de crédit aux consommateurs relatifs aux biens immobiliers à usage d'habitation et simplifiant le dispositif de mise en oeuvre des obligations en matière de conformité et de sécurité des produits et services, en tant qu'elles modifient le premier alinéa de l'article L. 313-30 du code de la consommation, ainsi que celles du V de ce même article 10, qui les rend applicables, à compter du 1er janvier 2018, aux contrats d'assurance en cours d'exécution à cette date, est renvoyée au Conseil constitutionnel.<br/>
Article 2 : La présente décision sera notifiée à la Fédération bancaire française, pour l'ensemble des intervenants, à la Société Assurances du Credit Mutuel-Vie SA (ACM VIE SA), première dénommée et au ministre de l'économie et des finances.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
