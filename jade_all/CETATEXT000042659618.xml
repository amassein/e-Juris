<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042659618</ID>
<ANCIEN_ID>JG_L_2020_12_000000427622</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/65/96/CETATEXT000042659618.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 10/12/2020, 427622, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427622</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT>M. Marc Pichon de Vendeuil</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEORD:2020:427622.20201210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Nogent Musée a demandé au tribunal administratif de Châlons-en-Champagne d'annuler les titres exécutoires émis le 16 octobre 2015 à son encontre par la commune de Nogent-sur-Seine et de la décharger de l'obligation de payer les sommes correspondantes. Par un jugement n°s 1502620, 1600212 du 24 mai 2017, le tribunal administratif de Châlons-en-Champagne a, d'une part, annulé ces titres exécutoires et déchargé la société Nogent Musée du paiement de la somme de 114 000 euros au titre des pénalités qui lui ont été infligées en application du contrat de partenariat conclu entre elle et la commune de Nogent-sur-Seine et, pour un montant total de 139 646,43 euros, des sommes prélevées par compensation par la commune au titre des loyers R3, d'autre part, enjoint à la commune de Nogent-sur-Seine de restituer les sommes prélevées par compensation sur les loyers pour des montants respectifs de 91 662,47 euros et de 139 646, 43 euros dans un délai de deux mois à compter de la date de notification du jugement et, enfin, rejeté le surplus des conclusions de la société Nogent Musée.<br/>
<br/>
              Par un arrêt n°s 17NC01814, 17NC01815 du 4 décembre 2018, la cour administrative d'appel de Nancy a, sur appel de la commune de Nogent-sur-Seine, en premier lieu, annulé ce jugement en tant qu'il se prononce sur les conclusions de la demande n° 1600212 de la société Nogent Musée, en deuxième lieu, annulé les titres exécutoires émis le 16 octobre 2015 pour un montant total de 70 551,60 euros, en troisième lieu, enjoint à la commune de Nogent-sur-Seine de restituer les sommes prélevées par compensation sur les loyers en recouvrement des titres exécutoires ainsi annulés, dans un délai de deux mois à compter de la date de notification de l'arrêt, en quatrième lieu, rejeté le surplus des conclusions des parties et, en dernier lieu, décidé qu'il n'y a pas lieu de se prononcer sur les conclusions de la commune de Nogent-sur-Seine aux fins de sursis à exécution du jugement du tribunal administratif.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 4 février, 11 avril et 29 novembre 2019 et le 9 mars 2020 au secrétariat du contentieux du Conseil d'Etat, la commune de Nogent-sur-Seine demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il a rejeté le surplus de ses conclusions d'appel ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit au surplus de ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de la société Nogent Musée la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Pez-Lavergne, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Marc Pichon de Vendeuil, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la commune de Nogent-sur-seine et à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la société Nogent Musée ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 23 novembre 2020 présentée par la société Nogent Musée ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la commune de Nogent-sur-Seine et la société Nogent Musée ont conclu, le 8 mars 2012, un contrat de partenariat portant sur le transfert, la restructuration et l'agrandissement du musée Dubois-Boucher sur le site de l'îlot Saint-Epoing, la réhabilitation et la restructuration de la maison Camille Claudel et l'aménagement complet du site. Les bâtiments ont été mis à la disposition de la commune de Nogent-sur-Seine à la date, prévue par avenant, du 2 janvier 2015 et la commune a commencé à verser à la société une partie des loyers prévus par les stipulations du contrat de partenariat. S'agissant, en revanche, des équipements muséographiques, des installations techniques et de la maison Camille Claudel, cette mise à disposition n'était pas intervenue à l'échéance contractuelle du 20 mai 2015 et la commune a, dans ces conditions, émis, le 16 octobre 2015, un titre exécutoire pour un montant de 114 000 euros correspondant à des pénalités de retard pour la période courant du 21 mai au 16 juillet 2015 ainsi que trois mandats d'annulation-réduction valant titres exécutoires en vue de la récupération d'une partie des loyers dits " R3 " (maintenance courante des installations). La commune a procédé au recouvrement d'une partie de ces créances par compensation sur les loyers et les rémunérations qu'elle devait à la société Nogent Musée. La commune a relevé appel du jugement par lequel le tribunal administratif de Châlons-en-Champagne a, d'une part, annulé les titres exécutoires du 16 octobre 2015 et, d'autre part, déchargé la société Nogent Musée de l'obligation de payer la somme de 114 000 euros au titre des pénalités mises à sa charge et les sommes correspondant à la restitution des loyers R3. Par un arrêt du 4 décembre 2018, la cour administrative d'appel de Nancy a, d'une part, annulé le jugement en tant qu'il se prononce sur les conclusions de la société Nogent Musée tendant à l'annulation des titres exécutoires correspondant à la restitution des loyers R3, d'autre part, annulé les titres exécutoires émis le 16 octobre 2015 pour un montant total de 70 551,60 euros, enfin, enjoint à la commune de Nogent-sur-Seine de restituer les sommes prélevées par compensation sur les loyers en recouvrement des titres exécutoires ainsi annulés. La commune de Nogent-sur-Seine se pourvoit en cassation contre cet arrêt en tant qu'il a rejeté le surplus de ses conclusions d'appel.<br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il statue sur les conclusions tendant à l'annulation des mandats d'annulation-réduction valant titres exécutoires correspondant à la restitution des loyers R3 :<br/>
<br/>
              2. Il appartient au juge d'appel, lorsqu'il statue sur le litige qui lui est soumis par la voie de l'évocation après avoir annulé pour irrégularité le jugement de première instance, d'examiner l'ensemble des conclusions, moyens et fins de non-recevoir opposés en première instance, même s'ils n'ont pas été repris en appel, à la seule exception de ceux qui ont été expressément abandonnés ou réduits en appel.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que la commune de Nogent-sur-Seine a opposé devant le tribunal administratif de Châlons-en-Champagne une exception tirée de l'incompétence de la juridiction administrative, que le tribunal a expressément écartée avant d'annuler les titres exécutoires en litige. La commune de Nogent-sur-Seine n'a pas expressément abandonné en appel cette exception. Dès lors, la requérante est fondée à soutenir qu'en s'abstenant de statuer à nouveau sur l'exception d'incompétence de la juridiction administrative soulevée en première instance lorsqu'elle a statué par la voie de l'évocation sur la demande de la société Nogent Musée enregistrée au greffe du tribunal sous le n° 1600212, la cour administrative d'appel de Nancy a insuffisamment motivé son arrêt sur ce point.<br/>
<br/>
              4. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi dirigés contre cette partie de l'arrêt, que la commune de Nogent-sur-Seine est fondée à demander l'annulation de l'arrêt qu'elle attaque qu'en tant qu'il a, d'une part, annulé le jugement du tribunal administratif de Châlons-en-Champagne en tant que celui-ci a statué sur la demande de la société Nogent Musée tendant à l'annulation des titres exécutoires correspondant à la restitution des loyers R3 et, d'autre part, statué sur cette demande par la voie de l'évocation.<br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il statue sur les conclusions tendant à l'annulation du titre exécutoire d'un montant de 114 000 euros au titre des pénalités mises à la charge de la société Nogent Musée :<br/>
<br/>
              5. Il ressort des pièces du dossier soumis aux juges du fond que selon l'article II.2.1.2.1 " Stipulation générales " relatives au " non-respect du délai de mise à disposition " du contrat de partenariat : " Sans préjudice des autres droits de la Commune de Nogent-sur-Seine aux termes du contrat de partenariat, en cas de non-respect du délai de mise à disposition de chacun des ouvrages et équipements, la Commune de Nogent-sur-Seine peut exiger une indemnité, payable à partir de l'expiration du délai de mise à disposition de chacun desdits ouvrages et équipements, d'un montant égal à deux mille (2000) euros par jour de retard. / Le montant de ladite indemnité ne peut cependant excéder par ouvrage et équipement le plafond défini à l'article IV.2.1 ci-après. / (...) ". Aux termes de l'article III.1.1.4 " Pénalités " du même contrat : " Les pénalités dont le Partenaire est redevable au titre du présent contrat font l'objet d'un calcul contradictoire entre les Parties. Le montant de la (des) pénalité(s) doit être calculé quinze (15) jours au moins avant la fin du trimestre. / Dans le cas où ce montant fait l'objet d'un paiement postérieurement à l'envoi de la facture par le Partenaire, ledit montant est alors imputé sur la facture du trimestre suivant, à l'exception du Loyer R1i qui ne peut faire l'objet de compensation. / Après accord des Parties sur le montant des pénalités, la commune de Nogent-sur-Seine procède au paiement du Partenaire, en tenant compte du montant desdites pénalités. Il est procédé par compensation, sous réserve des dispositions de la clause relative à la cession de créance. / En cas de désaccord, les Parties devront faire application des mesures de règlement des litiges faisant l'objet du TITRE VII du présent contrat ". Selon l'article IV.2.1. " Pénalités de retard dans la réalisation des travaux " de ce contrat : " (...) Dans l'hypothèse où la date de mise à disposition n'est pas respectée par le Partenaire, la Commune de Nogent-sur-Seine impose au Partenaire, sans mise en demeure préalable, le versement d'une pénalité d'un montant égal à 2 000 (deux mille) &#128; par jour de retard. / Le Partenaire doit s'acquitter du montant des pénalités au titre du présent article dans un délai de trente (30) jours à compter de la notification qui lui en est faite par la Commune de Nogent-sur-Seine. (...) / L'ensemble des pénalités liées à la réalisation des travaux objet du présent Contrat que la Commune de Nogent-sur-Seine est en droit d'appliquer au Partenaire est libératoire et ne saurait dépasser le plafond global de pénalités de retard fixé à 7% du montant de l'investissement initial tel que défini à l'annexe 6 " Coûts d'investissements initiaux " ". Aux termes de l'article VII.1.2 " Règlement amiable des litiges " du même contrat : " En cas d'apparition d'un litige relatif à l'application du présent contrat, les parties s'engagent à se rapprocher pour régler celui-ci dans les meilleurs délais. / Dans l'hypothèse où les parties ne parviennent pas à trouver un accord dans un délai de trente (30) jours, elles procèdent, d'un commun accord, à la désignation d'un expert. / À défaut d'accord, les parties réunissent un comité composé de trois membres : (...) En cas de désaccord persistant au-delà de quinze (15) jours, sur la composition du comité, un expert sera désigné par le tribunal administratif (...) à la requête de la partie la plus diligente. / La survenance d'un différend ne saurait, en aucun cas, soustraire le partenaire de ses obligations contractuelles. / Si le désaccord persiste au-delà d'un délai de trente (30) jours à compter de la remise du rapport du comité, la partie qui le souhaite saisit le tribunal administratif compétent ".<br/>
<br/>
              6. En premier lieu, en interprétant les stipulations du contrat de partenariat citées au point 5 comme rendant obligatoire la procédure de conciliation avant la saisine du juge du contrat en cas de désaccord sur les pénalités prévues à l'article II.2.1.2.1 du contrat pour sanctionner le non-respect du délai de mise à disposition de chacun des ouvrages et équipements, la cour a porté une appréciation qui échappe au contrôle du juge de cassation en l'absence de leur dénaturation.<br/>
<br/>
              7. En deuxième lieu, une stipulation contractuelle subordonnant la saisine du juge, pour le règlement des contestations sur l'interprétation ou l'exécution du contrat, à la mise en oeuvre préalable d'une procédure de conciliation, fait également obstacle à ce que la collectivité publique contractante émette directement des titres exécutoires pour le règlement des sommes correspondant à une contestation relative à l'exécution du contrat, sans mettre en oeuvre la procédure de conciliation préalable. La collectivité ne peut pas davantage procéder directement au recouvrement de ces sommes par la voie de la compensation avec ses dettes à l'égard de son cocontractant. Par suite, la cour administrative d'appel de Nancy n'a pas commis d'erreur de droit en jugeant que les titres exécutoires ayant pour objet le recouvrement de ces pénalités étaient irréguliers faute pour la commune d'avoir mis en oeuvre, avant leur émission, la procédure de règlement amiable des litiges.<br/>
<br/>
              8. En troisième lieu, la cour administrative d'appel de Nancy n'a pas davantage commis d'erreur de droit en estimant que l'obligation d'engager la procédure de règlement amiable des litiges prévue par ces stipulations faisait également obstacle à ce que la commune procède par compensation entre les pénalités dues par la société Nogent Musée et la part des loyers que la commune devait à cette dernière, alors que la procédure de concertation avait précisément pour objet la détermination de la créance relative à ces pénalités de retard, notamment de son montant, et donc l'établissement de son caractère certain, liquide et exigible.<br/>
<br/>
              9. En quatrième lieu, la cour ayant estimé par appréciation souveraine exempte de dénaturation que la procédure de règlement amiable des litiges était, en cas de désaccord entre les parties contractantes, un préalable obligatoire à la détermination de la créance relative aux pénalités contractuelles, notamment de son montant, et donc à l'établissement de son caractère certain, liquide et exigible, l'absence de sa mise en oeuvre affecte le bien-fondé de cette créance. Ainsi, le motif d'annulation des titres exécutoires litigieux ne tenant pas à une irrégularité en la forme de ces derniers, mais à leur bien-fondé, la cour administrative d'appel de Nancy n'a pas commis d'erreur de droit en estimant que leur annulation impliquait nécessairement la décharge des sommes correspondantes et, par conséquent, la restitution des sommes indûment prélevées par compensation sur les loyers dus par la commune à la société Nogent Musée.<br/>
<br/>
              10. En dernier lieu, la commune de Nogent-sur-Seine ne peut utilement soutenir que la cour administrative d'appel de Nancy aurait omis de statuer sur les conclusions chiffrées à fin d'injonction présentées par la société Nogent Musée.<br/>
<br/>
              11. Il résulte de ce qui précède que la commune de Nogent-sur-Seine n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque en tant qu'il statue sur les conclusions tendant à l'annulation du titre exécutoire d'un montant de 114 000 euros au titre des pénalités mises à la charge de la société Nogent Musée.<br/>
<br/>
              12. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions des parties au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er à 3 de l'arrêt du 4 décembre 2018 de la cour administrative d'appel de Nancy sont annulés.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Nancy.<br/>
Article 3 : Le surplus des conclusions du pourvoi de la commune de Nogent-sur-Seine est rejeté.<br/>
Article 4 : Les conclusions de la société Nogent Musée au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la commune de Nogent-sur-Seine et à la société Nogent Musée.<br/>
Copie en sera adressée au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
