<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025562645</ID>
<ANCIEN_ID>JG_L_2012_03_000000349705</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/56/26/CETATEXT000025562645.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème sous-section jugeant seule, 23/03/2012, 349705, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-03-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349705</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Rémy Schwartz</PRESIDENT>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. Francis Girault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 30 mai et 30 août 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Jean-Pierre A, demeurant au ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0802015 du 30 mars 2011 par lequel le tribunal administratif de Grenoble a rejeté sa demande tendant, d'une part, à l'annulation de l'arrêté du 18 février 2008 portant titre de pension et de la décision du 21 mars 2008 par laquelle le service des pensions de La Poste et de France Telecom a refusé de prendre en compte ses 24 mois de service national effectué en qualité d'objecteur de conscience pour le calcul de sa pension de retraite et, d'autre part, à ce qu'il soit enjoint au service des pensions de La Poste et de France Telecom de réintégrer ces 24 mois dans le calcul de sa pension et de lui verser le rappel de pension consécutif à ce nouveau calcul, assorti des intérêts au taux légal à compter du 1er janvier 2008, sous astreinte en cas de retard ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment ses articles 61-1 et 62 ;<br/>
<br/>
              Vu le code du service national ;<br/>
<br/>
              Vu la décision n° 2011-181 QPC du 13 octobre 2011 du Conseil constitutionnel ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Francis Girault, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Barthélemy, Matuchansky, Vexliard, avocat de M. A, <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Barthélemy, Matuchansky, Vexliard, avocat de M. A ; <br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis au juge du fond que M. A, ancien fonctionnaire de France Télécom, a effectué son service national en qualité d'objecteur de conscience du 1er décembre 1975 au 30 novembre 1977 ; qu'il s'est vu délivrer un premier titre de pension le 14 janvier 2008, intégrant ses vingt-quatre mois de service national dans le calcul de ses droits ; que le ministre du budget a toutefois retiré ce titre initial par un second arrêté de pension du 18 février 2008, excluant les deux années de service national de l'intéressé du calcul de ses droits à pension ; que par jugement du 30 mars 2011, contre lequel M. A se pourvoit en cassation, le tribunal administratif de Grenoble a rejeté sa demande tendant à l'annulation de ce second titre de pension et de la décision du 21 mars 2008 par laquelle le service des pensions de La Poste et France Télécom a rejeté son recours gracieux, au motif que les objecteurs de conscience ne pouvaient bénéficier des dispositions de l'article L. 63 du code du service national, dans sa rédaction issue de la loi du 10 juin 1971, organisant la comptabilisation du temps de service national actif dans l'avancement et la retraite des fonctionnaires ;<br/>
<br/>
              Considérant qu'aux termes du premier alinéa de l'article 61-1 de la Constitution : " Lorsque, à l'occasion d'une instance en cours devant une juridiction, il est soutenu qu'une disposition législative porte atteinte aux droits et libertés que la Constitution garantit, le Conseil constitutionnel peut être saisi de cette question sur renvoi du Conseil d'Etat ou de la Cour de cassation. " ; qu'aux termes du deuxième alinéa de son article 62 : " Une disposition déclarée inconstitutionnelle sur le fondement de l'article 61-1 est abrogée à compter de la publication de la décision du Conseil constitutionnel ou d'une date ultérieure fixée par cette décision. Le Conseil constitutionnel détermine les conditions et limites dans lesquelles les effets que la disposition a produits sont susceptibles d'être remis en cause " ; qu'aux termes du troisième alinéa du même article : " Les décisions du Conseil constitutionnel ne sont susceptibles d'aucun recours. Elles s'imposent aux pouvoirs publics et à toutes les autorités administratives et juridictionnelles. " ;<br/>
<br/>
              Considérant, d'une part, que par sa décision n° 2011-181 QPC du 13 octobre 2011, le Conseil constitutionnel a déclaré contraire à la Constitution les dispositions de l'article L. 63 du code du service national, dans sa rédaction issue de la loi du 10 juin 1971, réservant la comptabilisation des années de service national au titre de l'ancienneté et de la retraite aux fonctionnaires ayant accompli leur service dans " l'une des formes du III " du même code, au nombre desquelles ne figurait pas le service des objecteurs de conscience jusqu'à l'entrée en vigueur de la loi du 8 juillet 1983 réformant ce titre III ; que, d'autre part, le Conseil constitutionnel a expressément prévu, par la même décision du 13 octobre 2011, que cette déclaration d'inconstitutionnalité serait invocable dans les litiges en cours à cette date ; que M. A est par suite fondé à demander l'annulation du jugement litigieux, par lequel le tribunal administratif de Grenoble a fait application des dispositions de l'article L. 63 déclarées inconstitutionnelles pour rejeter sa demande ; <br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Sur les conclusions à fin d'annulation et d'injonction :<br/>
<br/>
              Considérant qu'il résulte de l'instruction que, par arrêté du 28 novembre 2011, le ministre du budget a modifié la pension de M. A, avec effet rétroactif au 1er janvier 2008, pour prendre en compte ses mois de service national accompli en qualité d'objecteur de conscience, et a procédé au versement des arrérages résultant de cette modification ; que l'émission de ce nouveau titre de pension a eu pour effet de rapporter l'arrêté litigieux du 18 février 2008 ; que, par suite, les conclusions de M. A tendant à l'annulation de cet arrêté et de la décision du 21 mars 2008 rejetant son recours gracieux, ainsi que ses conclusions tendant à ce qu'il soit enjoint à l'administration de comptabiliser ses années de service national dans ses droits à pension et de lui verser les arrérages correspondants depuis le 1er janvier 2008, ont perdu leur objet ; qu'il n'y a dès lors pas lieu d'y statuer ;<br/>
<br/>
              Sur les intérêts et la capitalisation des intérêts :<br/>
<br/>
              Considérant en revanche que M. A a droit aux intérêts sur les arrérages de pension qui résultent de la révision de ses droits à compter du 29 février 2008, date de son recours gracieux ; qu'à la date du 4 janvier 2012, à laquelle M. A a présenté des conclusions à fin de capitalisation de ces intérêts, il était dû plus d'une année d'intérêts ; que, dès lors, conformément aux dispositions de l'article 1154 du code civil, il y a lieu de faire droit à cette demande à cette date ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge de l'Etat le versement à M. A de la somme de 4 500 euros ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article  1er : Le jugement du 30 mars 2011 du tribunal administratif de Grenoble est annulé.<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions à fin d'annulation présentées par M. A devant le tribunal administratif de Grenoble, ni sur ses conclusions tendant à ce qu'il soit enjoint à l'administration de réviser rétroactivement sa pension à compter du 1er janvier 2008 et à lui verser les arrérages de pension correspondant.<br/>
Article 3 : L'Etat versera à M. A les intérêts au taux légal sur ses arrérages de pension à compter du 29 février 2008, avec capitalisation à compter du 4 janvier 2012.<br/>
Article 4 : L'Etat versera à M. A la somme de 4 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à M. Jean-Pierre A, au service des pensions de La Poste et France Télécom et à la ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du Gouvernement.<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
