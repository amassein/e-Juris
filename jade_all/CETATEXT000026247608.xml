<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026247608</ID>
<ANCIEN_ID>JG_L_2012_08_000000348115</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/24/76/CETATEXT000026247608.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  5ème sous-section jugeant seule, 01/08/2012, 348115, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-08-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>348115</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 5ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Didier Chauvaux</PRESIDENT>
<AVOCATS>SCP DEFRENOIS, LEVIS ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Leïla Derouich</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:348115.20120801</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 4 avril et 30 juin 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Strasbourg, représentée par son maire ; la commune de Strasbourg demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10NC00861 du 10 février 2011 par lequel la cour administrative d'appel de Nancy, après avoir annulé le jugement n° 0601358 du 6 avril 2010 du tribunal administratif de Strasbourg en tant qu'il statuait sur ses conclusions tendant à ce que l'association Alligator le garantisse à hauteur de 80 % des sommes mises à sa charge à raison de l'accident subi le 6 juillet 2001 par Mme Anja A, a rejeté ces mêmes conclusions comme portées devant un ordre de juridiction incompétent pour en connaître ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel en garantie ;<br/>
<br/>
              3°) de mettre à la charge de l'association Alligator la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général de la propriété des personnes publiques, notamment son article L. 2331-1 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Leïla Derouich, Auditeur,  <br/>
<br/>
              - les observations de la SCP Defrenois, Levis avocat de la commune de Strasbourg et de la SCP Rocheteau, Uzan-Sarano avocat de l'association Alligator.<br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Defrenois, Levis avocat de la commune de Strasbourg et à la SCP Rocheteau, Uzan-Sarano avocat de l'association Alligator ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, le 6 juillet 2001, dans le parc de Pourtalès à Strasbourg, lors d'un concert produit par l'association Alligator dans le cadre du festival " L'Eté culturel " organisé par la commune, la chute d'un arbre a provoqué la mort de treize spectateurs, plusieurs dizaines d'autres étant blessés ; que, par un jugement du 6 avril 2010, le tribunal administratif de Strasbourg a, d'une part, condamné la commune de Strasbourg à indemniser le Fonds de garantie des victimes d'actes de terrorisme et d'autres infractions et la caisse primaire d'assurance maladie de Strasbourg des dépenses exposées à raison des dommages subis par Mme Anja A et, d'autre part, rejeté l'appel en garantie de la commune contre l'association Alligator ; que la commune a fait appel de ce jugement en tant qu'il rejetait ses conclusions dirigées contre l'association ; qu'elle se pourvoit en cassation contre l'arrêt du 10 février 2011 par lequel la cour administrative d'appel de Nancy, après avoir annulé le jugement en tant qu'il se prononçait sur ces conclusions, les a rejetées au motif qu'elles ne ressortissaient pas à la compétence de la juridiction administrative dès lors que l'association Alligator n'avait pas été investie de prérogatives de puissance publique ; <br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 2331-1 du code général de la propriété des personnes publiques : " Sont portés devant la juridiction administrative les litiges relatifs : 1° Aux autorisations ou contrats comportant occupation du domaine public, quelle que soit leur forme ou leur dénomination, accordées ou conclus par les personnes publiques ou leurs concessionnaires " ; <br/>
<br/>
              3. Considérant qu'à l'appui de ses conclusions tendant à ce que la réparation des dommages subis par Mme A soit mise à la charge de l'association Alligator, la commune de Strasbourg invoquait la violation par cette association des clauses d'un contrat intitulé " convention de mise à disposition d'équipements publics ", qui comportait l'autorisation d'occuper une dépendance du domaine public en vue de la tenue d'un concert ; que le litige ainsi soulevé relevait de la compétence de la juridiction administrative par application des dispositions précitées de l'article L. 2331-1 du code général de la propriété des personnes publiques ; qu'en rejetant l'appel en garantie de la commune comme porté devant un ordre de juridiction incompétent pour en connaître, la cour administrative d'appel de Nancy a commis une erreur de droit ; que son arrêt doit dès lors être annulé ;<br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Strasbourg qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la commune de Strasbourg au titre de ces dispositions ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 10 février 2011 est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy.<br/>
<br/>
Article 3 : Les conclusions de la commune de Strasbourg et de l'association Alligator au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 4 : La présente décision sera notifiée à la commune de Strasbourg et à l'association Alligator.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
