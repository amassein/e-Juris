<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042854698</ID>
<ANCIEN_ID>JG_L_2020_12_000000420445</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/85/46/CETATEXT000042854698.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 31/12/2020, 420445, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420445</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:420445.20201231</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. C... A... et sa fille, Mme D... A..., ont demandé au tribunal administratif de Marseille de condamner l'Assistance publique - Hôpitaux de Marseille (AP-HM) à les indemniser de préjudices qu'ils estiment avoir subis du fait de la perte du dossier obstétrical de Mme B..., épouse A.... Par un jugement n° 1301606 du 2 novembre 2015, le tribunal administratif a condamné l'AP-HM à leur verser, respectivement, les sommes de 1 000 et 5 000 euros au titre de leur préjudice moral et a rejeté le surplus de leurs conclusions.<br/>
<br/>
              Par un arrêt n° 15MA04963 du 11 janvier 2018, la cour administrative d'appel de Marseille a, sur appel de M. et Mme A... et appel incident de l'AP-HM, annulé ce jugement et rejeté la demande de M. et Mme A....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 7 mai et 7 août 2018 au secrétariat du contentieux du Conseil d'Etat, M. et Mme A... demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'AP-HM la somme de 3 000 euros à verser à la SCP Richard, leur avocat, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Richard, avocat de M. A... et Mme A... et à Me Le Prado, avocat de l'Assistance publique - Hôpitaux de Marseille.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme A..., fille de M. A... et de Mme B..., a conservé des séquelles de lésions de son plexus brachial droit survenues lors de sa naissance, en 1992, dans un établissement de l'Assistance publique - Hôpitaux de Marseille (AP-HM). Par un arrêt irrévocable du 11 juin 2012, la cour administrative d'appel de Marseille a jugé que ces dommages ne résultaient d'aucune faute commise par l'établissement de santé lors du suivi de la grossesse de Mme B... ou de la naissance de sa fille et a, en conséquence, rejeté les conclusions par lesquelles M. A... et Mme A... demandaient que l'AP-HM soit condamnée à les indemniser.<br/>
<br/>
              2. M A... et Mme A..., sa fille, ont alors à nouveau recherché la responsabilité de l'AP-HM en soutenant que la perte, par l'établissement de santé, du dossier obstétrical de Mme B... les avait empêchés d'établir la responsabilité de cet établissement. Ils se pourvoient en cassation contre l'arrêt du 11 avril 2018 par lequel la cour administrative d'appel de Marseille a, sur appel d'un jugement du 2 novembre 2015 du tribunal administratif de Marseille, rejeté leur demande.<br/>
<br/>
              3. En premier lieu, M. et Mme A... ne sont pas fondés à soutenir que la cour administrative d'appel a dénaturé les pièces du dossier qui lui était soumis en estimant, alors même que la consultation du dossier obstétrical de Mme B... était impossible en raison de sa perte par l'établissement hospitalier et ainsi qu'il résultait, au demeurant, de la chose jugée par l'arrêt de la même cour du 11 juin 2012, qu'il n'y avait, avant la naissance de Mme A..., aucune indication à la réalisation d'une césarienne prophylactique, que le choix de l'accouchement par voie basse était conforme aux bonnes pratiques médicales et qu'aucune faute technique de nature à créer des lésions du plexus brachial ne pouvait être retenue à l'encontre du médecin accoucheur.<br/>
<br/>
              4. En deuxième lieu, ayant, ainsi qu'il a été dit ci-dessus, jugé que les dommages subis par Mme A... n'étaient pas dus à une faute commise dans la prise en charge de la grossesse de sa mère, la cour a pu, sans erreur de droit, juger que la perte du dossier obstétrical de Mme B... était dépourvue de tout lien avec le préjudice moral invoqué par M. et Mme A....<br/>
<br/>
              5. Enfin, si la cour a jugé que la consultation par Mme A... du dossier obstétrical de sa mère était soumise à l'accord de cette dernière, il résulte des termes de l'arrêt attaqué que ce motif a un caractère surabondant. Le moyen tiré de ce que la cour aurait commis sur ce point une erreur de droit est, par suite, inopérant.<br/>
<br/>
              6. Il résulte de tout ce qui précède que le pourvoi de M. et Mme A... doit être rejeté, y compris, par voie de conséquence, leurs conclusions présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. et Mme A... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme D... A..., à M. C... A..., à l'Assistance publique - Hôpitaux de Marseille et à la caisse primaire d'assurance maladie des Bouches-du-Rhône.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
