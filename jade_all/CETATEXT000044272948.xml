<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044272948</ID>
<ANCIEN_ID>JG_L_2021_10_000000424065</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/27/29/CETATEXT000044272948.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 29/10/2021, 424065, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424065</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:424065.20211029</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le numéro 424065, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 11 septembre 2018, 11 décembre 2018 et 24 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, la société RMC Découverte demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 11 juillet 2018 par laquelle le Conseil supérieur de l'audiovisuel (CSA) a refusé de qualifier de documentaires les programmes " Billy l'exterminateur : le B... ", " Cabanes perchées ", " Vintage mecanic ", " Alaska : De l'or sous la mer ", " Aquamen ", " Americars ", " Lagoon master ", " Traqueur de bolides ", " Boat masters ", " Ultime garage ", " Casse-cash ", " B... à l'instinct primaire ", " Bamazon ", " Mecanic brothers ", " La fièvre du ginseng " et " The grand tour " ; <br/>
<br/>
              2°) de mettre à la charge du CSA le versement d'une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              2° Sous le numéro 433701, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 19 août 2019, 18 novembre 2019 et 29 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, la société RMC Découverte demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 17 juin 2019 par laquelle le CSA a refusé de qualifier de documentaires les programmes " Americars, pièces détachées ", " Avions à la casse ", " Voitures à la casse ", " 60 jours en prison ", " Flipping bangers, voitures à tout prix ", " Pawnshop : une histoire de famille ", " B... à l'instinct primaire ", " Classic car rescue ", " Chat sauvage avec Rich ", " Tueurs nés avec Tom ", " Maraudeur avec Marty ", " Sang sur la neige avec Rich ", " Pris sur le fait avec Tom ", " Piège fatal avec Eustache " et " Intrus mortel avec Tom " ; <br/>
<br/>
              2°) de mettre à la charge du Conseil supérieur de l'audiovisuel le versement d'une somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              3° Sous le numéro 442205, par une requête et un mémoire en réplique, enregistrés les 27 juillet 2020 et 8 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, la société RMC Découverte demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 20 mai 2020 par laquelle le CSA a refusé de qualifier de documentaires les programmes : " Last stop garage ", " B... à la civilisation ", " Cabane fever ", " Off road garage ", " Troc en stock " et " Les Brown, génération Alaska " ;<br/>
<br/>
              2°) de mettre à la charge du Conseil supérieur de l'audiovisuel le versement d'une somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code des relations entre le public et l'administration ;<br/>
              - la loi n° 86-1067 du 30 septembre 1986 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Piwnica-Molinié, avocat de la société RMC Découverte.<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Les requêtes de la société RMC Découverte visées ci-dessus présentent à juger des questions semblables. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Aux termes de l'article 28 de la loi du 30 septembre 1986 relative à la liberté de communication : " La délivrance des autorisations d'usage de la ressource radioélectrique pour chaque nouveau service diffusé par voie hertzienne terrestre autre que ceux exploités par les sociétés nationales de programme est subordonnée à la conclusion d'une convention passée entre le Conseil supérieur de l'audiovisuel au nom de l'Etat et la personne qui en demande l'autorisation. / Dans le respect de l'honnêteté et du pluralisme de l'information et des programmes et des règles générales fixées en application de la présente loi (...), cette convention fixe les règles générales applicables au service compte tenu (...) du respect de l'égalité de traitement entre les différents services et des conditions de concurrence propres à chacun d'eux (...). / La convention porte notamment sur un ou plusieurs des points suivants : 1° La durée et les caractéristiques générales du programme propre (...) ". <br/>
<br/>
              3. Il ressort des pièces du dossier que la société RMC Découverte, éditrice du service de télévision du même nom, a conclu le 3 juillet 2012 avec le Conseil supérieur de l'audiovisuel (CSA), sur le fondement de l'article 28 de la loi du 30 septembre 1986 relative à la liberté de communication, une convention dont l'article 3-1-1 stipule que " les documentaires représentent annuellement au moins 75 % du temps total de diffusion et portent sur une grande variété de sujets ". Par plusieurs décisions des 11 juillet 2018, 17 juin 2019 et 20 mai 2020, le CSA a refusé de qualifier respectivement seize, quinze et six programmes de " documentaires " au sens de ces dispositions. Par ses requêtes, la société RMC Découverte demande au Conseil d'Etat d'annuler ces trente-sept décisions de refus de qualification. <br/>
<br/>
              Sur la nature des actes attaqués : <br/>
<br/>
              4. Les décisions, telles que celles visées par les présentes requêtes, par lesquelles le CSA refuse de classer un programme au sein d'un certain genre d'œuvres audiovisuelles, qui fondent notamment l'appréciation du respect des quotas de diffusion prévus par les conventions signées avec les éditeurs de service, ont, contrairement à ce que soutient le CSA, le caractère d'actes faisant grief, susceptibles de faire l'objet d'un recours pour excès de pouvoir devant le juge administratif.<br/>
<br/>
              Sur la légalité des actes attaqués :<br/>
<br/>
              5. En premier lieu, ces décisions n'entrant dans aucune des catégories de décisions devant être motivées en application de l'article L. 211-2 du code des relations entre le public et l'administration ou en application d'un autre texte ou principe, la société RMC Découverte ne saurait utilement soutenir qu'elles sont, faute d'être suffisamment motivées, entachées d'illégalité.<br/>
<br/>
              6. En deuxième lieu, il ressort des pièces du dossier que, pour déterminer si les différents programmes litigieux pouvaient être qualifiés de " documentaires " au sens de l'article 3-1-1 de la convention du 3 juillet 2012 cité au point 3, le CSA s'est livré, pour chacun d'entre eux, à une appréciation prenant en compte l'existence d'un point de vue d'auteur, la présence d'un apport de connaissances pour le spectateur, la présentation de faits ou de situations qui préexistent à la réalisation de l'émission, l'absence - sans interdire toute reconstitution -  de mises en scène artificielles et, le cas échéant, l'obtention du soutien du Centre national du cinéma et de l'image animée au titre des œuvres documentaires. En se fondant sur l'ensemble de ces critères, dans le cadre d'une appréciation globale de chaque programme soumis à son examen, le CSA n'a pas commis d'erreur de droit.<br/>
<br/>
              7. En troisième lieu, il ressort des pièces du dossier que les programmes litigieux, soit invitent à suivre une ou plusieurs personnes dans l'exercice de leur profession ou de leur passion tels que l'orpaillage, la réparation de véhicules de collection, la réalisation de cabanes ou de piscines ou l'extermination d'animaux nuisibles, soit s'attachent à des protagonistes en situation d'aventure, de voyage ou de survie. En se fondant, pour refuser à ces programmes la qualification de " documentaires ", sur la circonstance qu'ils utilisent des procédés narratifs et de mise en image caractéristiques du divertissement, qu'ils ne procèdent pas d'une intention d'enrichir les connaissances du téléspectateur, faute d'apporter, de manière substantielle et continue, des données informatives sur leurs sujets et qu'ils mettent en scène la réalité de manière artificielle, en recherchant la dramatisation, les effets de surprise et l'exacerbation des émotions, le CSA a fait une exacte application de l'article 3-1-1 de la convention du 3 juillet 2012 citée ci-dessus.<br/>
<br/>
              8. Il résulte de tout ce qui précède que, sans qu'il soit besoin de prescrire une enquête sur le fondement de l'article R. 623-1 du code de justice administrative, la société RMC Découverte n'est pas fondée à demander l'annulation des décisions qu'elle attaque. Ses requêtes doivent par suite être rejetées, y compris, par voie de conséquence, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              				D E C I D E :<br/>
              				--------------<br/>
<br/>
		Article 1er : Les requêtes de la société RMC Découverte sont rejetées.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société RMC Découverte et au Conseil supérieur de l'audiovisuel.<br/>
		Copie en sera adressée à la ministre de la culture.<br/>
<br/>
              Délibéré à l'issue de la séance du 23 septembre 2021 où siégeaient : M. Denis Piveteau, président de chambre, présidant ; M. Olivier Yeznikian, conseiller d'Etat et M. Alain Seban, conseiller d'Etat-rapporteur. <br/>
<br/>
              Rendu le 29 octobre 2021.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Denis Piveteau<br/>
 		Le rapporteur : <br/>
      Signé : M. Alain Seban<br/>
                 La secrétaire :<br/>
      Signé : Mme C... A...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
