<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030459160</ID>
<ANCIEN_ID>JG_L_2015_04_000000364539</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/45/91/CETATEXT000030459160.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 02/04/2015, 364539, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-04-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364539</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>FOUSSARD ; SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR</AVOCATS>
<RAPPORTEUR>M. Romain Victor</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:364539.20150402</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Cap Caraïbes a demandé au tribunal administratif de Fort-de-France d'annuler la délibération du 8 septembre 2008 par laquelle le conseil municipal de Case-Pilote a décidé de rétracter la promesse unilatérale de vente d'un terrain que la commune lui avait consentie par acte authentique du 18 décembre 2007.<br/>
<br/>
              La société Gaïa W, venue aux droits de la société Cap Caraïbes, a demandé au même tribunal d'annuler la délibération du 26 janvier 2010 par laquelle le conseil municipal de cette commune a mis à la disposition du service départemental d'incendie et de secours (SDIS) de la Martinique une partie du terrain ayant fait l'objet de cette promesse de vente.<br/>
<br/>
              Par un jugement n° 0800723-1000205 du 29 avril 2011, le tribunal administratif de Fort-de-France a rejeté les demandes de ces deux sociétés.<br/>
<br/>
              Par un arrêt n° 11BX01601 du 16 octobre 2012, la cour administrative d'appel de Bordeaux, faisant droit à l'appel formé contre ce jugement par les sociétés Cap Caraïbes et Gaïa W, l'a annulé ainsi que les délibérations de la commune de Case-Pilote des 8 septembre 2008 et 26 janvier 2010.<br/>
<br/>
              1° Par un pourvoi et un mémoire en réplique, enregistrés les 14 décembre 2012 et 5 mars 2015 au secrétariat du contentieux du Conseil d'Etat sous le n° 364539, la commune de Case-Pilote demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel des sociétés Gaïa W et Cap Caraïbes ;<br/>
<br/>
              3°) de mettre à la charge de ces sociétés la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Par une requête, enregistrée sous le n° 364540 le 14 décembre 2012 au secrétariat du contentieux, la commune de Case-Pilote demande au Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner qu'il soit sursis à l'exécution de l'arrêt attaqué sous le <br/>
n° 364539 ;<br/>
<br/>
              2°) de mettre à la charge des sociétés Cap Caraïbes et Gaïa W la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Romain Victor, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Foussard, avocat de la commune de Case Pilote, et à la SCP Célice, Blancpain, Soltner, Texidor, avocat de la société Gaïa W et de la société Cap Caraïbes ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que, par une délibération du 30 août 2007, le conseil municipal de Case-Pilote a décidé, dans le cadre du projet d'aménagement d'une zone d'activités, de fixer le prix de cession d'un terrain, situé au lieudit Plateforme, à la somme de 606 985 euros ; que, par une délibération du 14 novembre 2007, le conseil municipal a autorisé le maire à signer au nom de la commune la promesse unilatérale de vente de ce terrain à la société Cap Caraïbes ; que, par acte authentique du 18 décembre 2007, la commune a consenti à cette société la faculté d'acquérir le bien au prix de 606 985 euros en levant à cette fin, dans le délai de deux ans,  l'option qui lui était offerte ; que, par une délibération du 8 septembre 2008, adoptée avant la réalisation de la cession, le conseil municipal de Case-Pilote a décidé de " dénoncer la promesse de vente " et de ne pas donner au maire l'autorisation nécessaire pour signer l'acte authentique de vente ; que, par acte du 4 décembre 2009, la société Gaïa W a été conventionnellement substituée à la société Cap Caraïbes dans le bénéfice de la promesse de vente et qu'elle a, en cette qualité, levé l'option le 15 décembre 2009 ; que, par une délibération du 26 janvier 2010, le conseil municipal de Case-Pilote a adopté le principe de la reconstruction de la caserne de sapeurs-pompiers communale sur une parcelle de 3 100 m² située au lieudit Plateforme et autorisé la mise à disposition de ce terrain au service départemental d'incendie et de secours (SDIS) de la Martinique ; que, par un jugement du 24 avril 2011, le tribunal administratif de Fort-de-France a rejeté les demandes des sociétés Cap Caraïbes et Gaïa W tendant à l'annulation des délibérations des 8 septembre 2008 et 26 janvier 2010 ; que la commune de Case-Pilote se pourvoit en cassation contre l'arrêt du 16 octobre 2012 par lequel la cour administrative d'appel de Bordeaux, faisant droit à l'appel formé par les sociétés Cap Caraïbes et Gaïa W, a annulé ce jugement et les délibérations litigieuses ; qu'elle présente par ailleurs une requête tendant à ce qu'il soit sursis à l'exécution de cet arrêt ; qu'il y a lieu de joindre ce pourvoi et cette requête pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant qu'il résulte de la combinaison des articles 1101, 1134 et 1589 du code civil que, ainsi que le juge la Cour de cassation, la rétractation par le promettant d'une promesse unilatérale de vente, lorsqu'elle intervient avant que le bénéficiaire ait levé l'option dans le délai stipulé dans le contrat, se résout, conformément aux dispositions de l'article 1142 du code civil, en dommages et intérêts, à moins que les parties aient contractuellement décidé d'écarter l'application des dispositions de cet article ;<br/>
<br/>
              3. Considérant que, pour annuler la délibération du 8 septembre 2008 dénonçant la promesse de vente, la cour administrative d'appel de Bordeaux a jugé que le conseil municipal de Case-Pilote avait retiré illégalement, plus de quatre mois après son adoption, la délibération du 14 novembre 2007 ; qu'en statuant ainsi, alors d'une part, que cette délibération, qui se bornait à autoriser le maire à signer la promesse de vente, n'avait créé par elle-même aucun droit au profit de la société bénéficiaire, d'autre part, que celle-ci ne pouvait tenir de la décision du maire de signer la promesse unilatérale de vente le 18 décembre 2007 d'autres droits que ceux résultant de l'application des dispositions du code civil régissant les rapports entre les parties à un tel contrat de droit privé et enfin que, dès lors qu'il résultait du dossier soumis à la cour que la société bénéficiaire n'avait pas encore levé l'option, elle ne pouvait prétendre à la réalisation forcée de la vente, mais seulement à des dommages et intérêts, en saisissant le cas échéant le juge du contrat, la cour a inexactement qualifié la délibération du 8 septembre 2008 et entaché son arrêt d'erreurs de droit ; qu'en annulant, par voie de conséquence, la délibération du 26 janvier 2010, la cour a commis une autre erreur de droit ; que, pour ces motifs et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la commune de Case-Pilote est fondée à demander l'annulation de l'arrêt qu'elle attaque ; qu'il s'ensuit que ses conclusions tendant à ce qu'il soit sursis à l'exécution de cet arrêt deviennent sans objet et qu'il n'y a, dès lors, plus lieu d'y statuer ;<br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Case-Pilote qui n'est pas, dans la présente instance, la partie perdante ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge des sociétés Cap Caraïbes et Gaïa W la somme de 3 000 euros à verser solidairement à la commune de Case-Pilote au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 16 octobre 2012 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux.<br/>
Article 3 : Il n'y a pas lieu de statuer sur la requête n° 364540.<br/>
Article 4 : Les sociétés Cap Caraïbes et Gaïa W verseront ensemble à la commune de Case-Pilote une somme globale de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Les conclusions des sociétés Cap Caraïbes et Gaïa W présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à la commune de Case-Pilote et aux sociétés Gaïa W et Cap Caraïbes.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-06-02-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - CLASSIFICATION. ACTES INDIVIDUELS OU COLLECTIFS. ACTES CRÉATEURS DE DROITS. - PROMESSE UNILATÉRALE DE VENTE - ETENDUE DES DROITS CRÉÉS - DROITS RÉSULTANTS DE L'APPLICATION DES DISPOSITIONS PERTINENTES DU CODE CIVIL - CONSÉQUENCE EN CAS DE RÉTRACTATION PAR LE PROMETTANT AVANT QUE LE BÉNÉFICIAIRE AIT LEVÉ L'OPTION - RÉALISATION FORCÉE DE LA VENTE - ABSENCE - DOMMAGES ET INTÉRÊTS - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-01-06-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - CLASSIFICATION. ACTES INDIVIDUELS OU COLLECTIFS. ACTES NON CRÉATEURS DE DROITS. - DÉLIBÉRATION MUNICIPALE AUTORISANT LE MAIRE À SIGNER UNE PROMESSE DE VENTE [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">135-01-03-02 COLLECTIVITÉS TERRITORIALES. DISPOSITIONS GÉNÉRALES. BIENS DES COLLECTIVITÉS TERRITORIALES. RÉGIME JURIDIQUE DES BIENS. - PROMESSE UNILATÉRALE DE VENTE - CRÉATION DE DROITS AU PROFIT DU BÉNÉFICIAIRE DE LA PROMESSE - 1) ABSENCE - DÉLIBÉRATION SE BORNANT À AUTORISER LE MAIRE À SIGNER LA PROMESSE [RJ2] - 2) EXISTENCE - DÉCISION DE SIGNER LA PROMESSE - ETENDUE DES DROITS CRÉÉS - DROITS RÉSULTANTS DE L'APPLICATION DES DISPOSITIONS PERTINENTES DU CODE CIVIL - CONSÉQUENCE EN CAS DE RÉTRACTATION PAR LE PROMETTANT AVANT QUE LE BÉNÉFICIAIRE AIT LEVÉ L'OPTION - RÉALISATION FORCÉE DE LA VENTE - ABSENCE - DOMMAGES ET INTÉRÊTS - EXISTENCE [RJ1].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">24-02-02-01 DOMAINE. DOMAINE PRIVÉ. RÉGIME. ALIÉNATION. - PROMESSE UNILATÉRALE DE VENTE - CRÉATION DE DROITS AU PROFIT DU BÉNÉFICIAIRE DE LA PROMESSE - 1) ABSENCE - DÉLIBÉRATION SE BORNANT À AUTORISER LE MAIRE À SIGNER LA PROMESSE [RJ2] - 2) EXISTENCE - DÉCISION DE SIGNER LA PROMESSE - ETENDUE DES DROITS CRÉÉS - DROITS RÉSULTANTS DE L'APPLICATION DES DISPOSITIONS PERTINENTES DU CODE CIVIL - CONSÉQUENCE EN CAS DE RÉTRACTATION PAR LE PROMETTANT AVANT QUE LE BÉNÉFICIAIRE AIT LEVÉ L'OPTION - RÉALISATION FORCÉE DE LA VENTE - ABSENCE - DOMMAGES ET INTÉRÊTS - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 01-01-06-02-01 Il résulte de la combinaison des articles 1101, 1134 et 1589 du code civil que, ainsi que le juge la Cour de cassation, la rétractation par le promettant d'une promesse unilatérale de vente, lorsqu'elle intervient avant que le bénéficiaire ait levé l'option dans le délai stipulé dans le contrat, se résout, conformément aux dispositions de l'article 1142 du code civil, en dommages et intérêts, à moins que les parties aient contractuellement décidé d'écarter l'application des dispositions de cet article.,,,Le bénéficiaire d'une promesse unilatérale de vente consentie par une commune ne peut tenir de la décision du maire de signer cette promesse d'autres droits que ceux résultant de l'application des dispositions du code civil régissant les rapports entre les parties à un tel contrat de droit privé. Dès lors que le bénéficiaire n'a pas encore levé l'option, il ne peut prétendre à la réalisation forcée de la vente, mais seulement à des dommages et intérêts, en saisissant le cas échéant le juge du contrat.</ANA>
<ANA ID="9B"> 01-01-06-02-02 Une délibération d'un conseil municipal qui se borne à autoriser le maire à signer une promesse de vente, ne crée par elle-même aucun droit au profit du bénéficiaire de cette promesse.</ANA>
<ANA ID="9C"> 135-01-03-02 1) Une délibération d'un conseil municipal qui se borne à autoriser le maire à signer une promesse de vente, ne crée par elle-même aucun droit au profit du bénéficiaire de cette promesse.,,,2) Il résulte de la combinaison des articles 1101, 1134 et 1589 du code civil que, ainsi que le juge la Cour de cassation, la rétractation par le promettant d'une promesse unilatérale de vente, lorsqu'elle intervient avant que le bénéficiaire ait levé l'option dans le délai stipulé dans le contrat, se résout, conformément aux dispositions de l'article 1142 du code civil, en dommages et intérêts, à moins que les parties aient contractuellement décidé d'écarter l'application des dispositions de cet article.,,,Le bénéficiaire d'une promesse unilatérale de vente consentie par une commune ne peut tenir de la décision du maire de signer cette promesse d'autres droits que ceux résultant de l'application des dispositions du code civil régissant les rapports entre les parties à un tel contrat de droit privé. Dès lors que le bénéficiaire n'a pas encore levé l'option, il ne peut prétendre à la réalisation forcée de la vente, mais seulement à des dommages et intérêts, en saisissant le cas échéant le juge du contrat.</ANA>
<ANA ID="9D"> 24-02-02-01 1) Une délibération d'un conseil municipal qui se borne à autoriser le maire à signer une promesse de vente, ne crée par elle-même aucun droit au profit du bénéficiaire de cette promesse.,,,2) Il résulte de la combinaison des articles 1101, 1134 et 1589 du code civil que, ainsi que le juge la Cour de cassation, la rétractation par le promettant d'une promesse unilatérale de vente, lorsqu'elle intervient avant que le bénéficiaire ait levé l'option dans le délai stipulé dans le contrat, se résout, conformément aux dispositions de l'article 1142 du code civil, en dommages et intérêts, à moins que les parties aient contractuellement décidé d'écarter l'application des dispositions de cet article.,,,Le bénéficiaire d'une promesse unilatérale de vente consentie par une commune ne peut tenir de la décision du maire de signer cette promesse d'autres droits que ceux résultant de l'application des dispositions du code civil régissant les rapports entre les parties à un tel contrat de droit privé. Dès lors que le bénéficiaire n'a pas encore levé l'option, il ne peut prétendre à la réalisation forcée de la vente, mais seulement à des dommages et intérêts, en saisissant le cas échéant le juge du contrat.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. Cass. civ.3e, 15 décembre 1993, n°91-10199, Bull. III n°174, p. 115.,,[RJ2] Ab. Jur., en ce qui concerne le caractère créateur de droits de la délibération autorisant le maire à signer une promesse de vente, CE, 8 janvier 1982, Epoux Hostetter, n°21510, T. p. 619. Cf. CE, 13 février 2015, Société Le Patio Lafayette, n°381412, inédit au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
