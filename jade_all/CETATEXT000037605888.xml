<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037605888</ID>
<ANCIEN_ID>JG_L_2018_11_000000413882</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/60/58/CETATEXT000037605888.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 12/11/2018, 413882, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-11-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413882</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD</AVOCATS>
<RAPPORTEUR>M. Vincent Uher</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:413882.20181112</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 20 juin 2018, le Conseil d'État statuant au contentieux a prononcé l'admission des conclusions du pourvoi de la société par actions simplifiée (SAS) Arkéa crédit bail dirigées contre le jugement n° 1500900 du 30 juin 2017 du tribunal administratif de Nantes en tant seulement que ce jugement s'est prononcé sur l'inclusion dans les bases des cotisations de taxe foncière sur les propriétés bâties auxquelles cette société a été assujettie au titre des années 2012 et 2013 dans les rôles de la commune de Brissac-Quincé (Maine-et-Loire) d'un réservoir de fioul, d'une passerelle métallique reposant sur un châssis à roulettes et d'un réseau de vapeur. <br/>
<br/>
              Par un mémoire, enregistré le 19 octobre 2018, le ministre de l'action et des comptes publics conclut à l'annulation du jugement attaqué dans la mesure des conclusions du pourvoi dont l'admission a été prononcée et au renvoi de l'affaire, dans cette mesure, devant le tribunal administratif de Nantes. Il s'en remet à la sagesse du Conseil d'État s'agissant des conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Uher, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, au cabinet Briard, avocat de la SAS Arkea Credit Bail.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 1380 du code général des impôts : " La taxe foncière est établie annuellement sur les propriétés bâties sises en France à l'exception de celles qui en sont expressément exonérées par les dispositions du présent code. " Aux termes de l'article 1381 du même code : " Sont également soumis à la taxe foncière sur les propriétés bâties : / 1° Les installations destinées à abriter des personnes ou des biens ou à stocker des produits ainsi que les ouvrages en maçonnerie présentant le caractère de véritables constructions tels que, notamment, les cheminées d'usine, les réfrigérants atmosphériques, les formes de radoub, les ouvrages servant de support aux moyens matériels d'exploitation ". Aux termes de l'article 1382 du même code : " Sont exonérés de taxe foncière sur les propriétés bâties : / (...) 11° Les outillages et autres installations et moyens matériels d'exploitation des établissements industriels à l'exclusion de ceux visés au 1° et 2° de l'article 1381 ". <br/>
<br/>
              2. Il résulte de ces dispositions, d'une part, que les bâtiments mentionnés au 1° de l'article 1381 cité ci-dessus comprennent également les aménagements faisant corps avec eux, et d'autre part, que les outillages, autres installations et moyens matériels d'exploitation des établissements industriels mentionnés au 11° de l'article 1382 s'entendent de ceux qui participent directement à l'activité industrielle de l'établissement et sont dissociables des immeubles.<br/>
<br/>
              3. En premier lieu, pour juger que l'administration avait à bon droit pris en compte un réservoir de fioul dans le calcul de la valeur locative servant de base aux impositions litigieuses, le tribunal s'est borné à mentionner la thèse de l'administration selon laquelle le devis produit au dossier correspondant à cette installation indiquait une fixation aux murs, planches et plafonds, alors, au demeurant, qu'il ressortait manifestement de cette pièce qu'elle ne pouvait donner lieu à pareille lecture, ainsi que l'avait d'ailleurs fait valoir la requérante dans son mémoire en réplique. En statuant ainsi, le tribunal, qui a insuffisamment motivé son jugement sur ce point, a dénaturé les faits de l'espèce et entaché ce jugement d'erreur de qualification juridique des faits.<br/>
<br/>
              4. En deuxième lieu, le tribunal a également entaché son jugement d'insuffisance de motivation et a dénaturé les pièces du dossier en se bornant à juger, à propos d'une passerelle métallique, qu'elle pouvait être incluse dans le calcul de la valeur locative au seul motif de la " réserve sérieuse " émise par l'administration quant à la valeur probante de photographies produites par la requérante, alors que cette dernière avait également produit une facture qui, en précisant que cette passerelle était montée sur un châssis à roulettes, était de nature à la faire regarder comme dissociable de l'immeuble passible de taxe foncière et à justifier en conséquence son exonération de cette imposition au regard du 11° précité de l'article 1382 du code général des impôts.<br/>
<br/>
              5. En troisième et dernier lieu, il ressort des pièces du dossier soumis au tribunal que celui-ci a omis de statuer sur les conclusions de la requête tendant à la réduction des cotisations de taxe foncière sur les propriétés bâties en litige résultant de la prise en compte, pour leur calcul, du réseau de vapeur, à propos duquel la requérante avait produit des factures distinctes au soutien de sa demande.<br/>
<br/>
              6. Il résulte de tout ce qui précède que la société requérante est fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation du jugement attaqué, en tant seulement que celui-ci s'est prononcé sur l'inclusion dans les bases des cotisations de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie, au titre des années 2012 et 2013, d'un réservoir de fioul, d'une passerelle métallique reposant sur un châssis à roulettes et d'un réseau de vapeur.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'État la somme de 1 500 euros à verser à la SAS Arkéa crédit bail au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article  1er : Le jugement du 30 juin 2017 du tribunal administratif de Nantes est annulé en tant qu'il s'est prononcé sur l'inclusion dans les bases des cotisations de taxe foncière sur les propriétés bâties auxquelles la SAS Arkéa crédit bail a été assujettie, au titre des années 2012 et 2013 dans les rôles de la commune de Brissac-Quincé, d'un réservoir de fioul, d'une passerelle métallique reposant sur un châssis à roulettes et d'un réseau de vapeur.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, au tribunal administratif de Nantes.<br/>
Article 3 : L'État versera à la SAS Arkéa crédit bail une somme de 1 500 euros au titre de l'article  L.761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions du pourvoi est rejeté.<br/>
Article 5 : La présente décision sera notifiée à la société par actions simplifiée Arkéa crédit bail et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
