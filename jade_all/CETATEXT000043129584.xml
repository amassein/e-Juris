<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043129584</ID>
<ANCIEN_ID>JG_L_2021_02_000000441395</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/12/95/CETATEXT000043129584.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 11/02/2021, 441395, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441395</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>M. Bertrand Mathieu</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:441395.20210211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés les 24 juin et 24 septembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... D... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 28 novembre 2019 lui refusant l'acquisition de la nationalité française ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Il soutient que le décret repose sur une application inexacte de l'article 21-4 du code civil en estimant qu'il était indigne d'acquérir la nationalité française.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le code justice administrative et le décret n° 2020-1406 du 18 novembre 2020<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bertrand Mathieu, conseiller d'Etat en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme B... C..., rapporteure publique,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Fabiani,                      Luc-Thaler, Pinatel, avocat de M. D...,<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 21-2 du code civil : " L'étranger ou apatride qui contracte mariage avec un conjoint de nationalité française peut, après un délai de quatre ans à compter du mariage, acquérir la nationalité française par déclaration à condition qu'à la date de cette déclaration la communauté de vie tant affective que matérielle n'ait pas cessé entre les époux depuis le mariage et que le conjoint français ait conservé sa nationalité ". L'article 21-4 du même code prévoit toutefois que : " Le Gouvernement peut s'opposer par décret en Conseil d'Etat, pour indignité ou défaut d'assimilation, à l'acquisition de la nationalité française par le conjoint étranger dans un délai d'un an à compter de la date du récépissé prévu au deuxième alinéa de l'article 26 ou, si l'enregistrement a été refusé, à compter du jour où la décision judiciaire admettant la régularité de la déclaration est passée en force de chose jugée. En cas d'opposition du Gouvernement, l'intéressé est réputé n'avoir jamais acquis la nationalité française. Toutefois, la validité des actes passés entre la déclaration et le décret d'opposition ne pourra être contestée pour le motif que l'auteur n'a pu acquérir la nationalité française ". <br/>
<br/>
              2. M. D..., de nationalité tunisienne, a souscrit le 27 juin 2017 une déclaration d'acquisition de la nationalité française à raison de son mariage avec une ressortissante française. Par le décret attaqué, le Premier ministre s'est opposé à l'acquisition de la nationalité française au motif que M. D... ne pouvait être regardé comme digne de l'acquérir.<br/>
<br/>
              3. Il ressort des pièces du dossier que M. D... a été reconnu coupable, par un arrêt de la cour d'appel de Poitiers du 6 septembre 2013, de délit de fuite après un accident en tant que conducteur de véhicule terrestre le 11 octobre 2012. Pour ces faits, il a été condamné à une peine de deux mois d'emprisonnement avec sursis et six mois de suspension de son permis de conduire. M. D... a également été reconnu coupable, par le tribunal de grande instance de La Rochelle le 16 novembre 2015, de rébellion le 7 juillet 2015. Pour ces faits, il a été condamné à une peine de trois mois d'emprisonnement avec sursis et deux ans de mise à l'épreuve. Enfin, M. D... a été reconnu coupable, par un arrêt de la cour d'appel de Poitiers le 19 décembre 2018, de violence dans un accès à un moyen de transport collectif de voyageurs suivie d'incapacité d'excédant pas huit jours le 25 juillet 2014. Pour ces faits, il a été condamné à une peine de quatre mois d'emprisonnement avec sursis assortis d'une mise à l'épreuve de deux ans. Par suite, en estimant qu'en raison de la gravité, de leur caractère récent des faits qui lui sont reprochés, M. D... devait être regardé, à la date du décret attaqué, comme indigne d'acquérir la nationalité française, le Premier ministre n'a pas fait une inexacte application de l'article 21-4 du code civil. <br/>
              4. Il résulte de ce qui précède que M. D... n'est pas fondé à demander l'annulation pour excès de pouvoir du décret du 29 novembre 2019 lui refusant l'acquisition de la nationalité française. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			  --------------<br/>
<br/>
Article 1er : La requête de M. D... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A... D... et au ministre de l'intérieur.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
