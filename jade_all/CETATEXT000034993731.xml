<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034993731</ID>
<ANCIEN_ID>JG_L_2017_06_000000411587</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/99/37/CETATEXT000034993731.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, formation collégiale, 19/06/2017, 411587, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411587</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés, formation collégiale</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Caroline Martin</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:411587.20170619</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. E...D...a demandé au juge des référés du tribunal administratif de Lille, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner la suspension de l'exécution de l'arrêté du 20 mars 2017 modifié ultérieurement par lequel le ministre de l'intérieur a prolongé son assignation à résidence sur le territoire de la commune de Maubeuge pour une durée de trois mois, avec obligation de se présenter deux fois par jour au commissariat de police de Maubeuge, tous les jours de la semaine, y compris les jours fériés et chômés, de demeurer tous les jours de 20 heures à 6 heures à son domicile, avec interdiction de se déplacer de son lieu d'assignation à résidence sans avoir préalablement obtenu un sauf-conduit du préfet du Nord. <br/>
<br/>
              Par une ordonnance du 16 juin 2017, le juge des référés du tribunal administratif a suspendu l'exécution de l'arrêté du 20 mars 2017.  <br/>
<br/>
              Par un recours, enregistré au secrétariat du contentieux le 16 juin, le ministre de l'intérieur demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de rejeter la demande de M.D... ;<br/>
<br/>
<br/>
              Il soutient que le juge des référés a méconnu les pièces du dossier et s'est mépris sur ses écritures en jugeant qu'il ne faisait pas état d'éléments nouveaux ou complémentaires justifiant le renouvellement de l'assignation à résidence, alors que le Conseil d'Etat statuant au contentieux le 17 mars 2017 a confirmé en formation collégiale que des éléments nouveaux et complémentaires intervenus dans un délai de douze mois antérieurement à l'arrêté attaqué justifiaient le renouvellement de l'assignation à résidence.  <br/>
<br/>
              Par un mémoire en défense, enregistré le 18 juin 2017,  M. D...conclut au rejet du recours et à ce que l'Etat lui verse une somme de 2 000 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il soutient :<br/>
              - que son orientation ainsi que celle de son épouse vers un fondamentalisme religieux n'est pas établie ; <br/>
              - qu'il n'a plus de contact avec M. G...et les frèresB... ;  <br/>
              - que la prolongation de son assignation à résidence ne saurait se fonder sur la consultation de journaux et de sites de propagande djihadiste datant de plus d'un an et demi et qu'au demeurant, les poursuites pour consultation de sites faisant l'apologie du terrorisme ont été abandonnées ; <br/>
              - qu'il ne souhaite pas quitter la France pour la Syrie et, au demeurant, fait l'objet d'une mesure d'interdiction de sortie du territoire ;  <br/>
              - qu'il ne saurait lui être opposé un comportement provoquant à l'égard des fonctionnaires de police ; <br/>
              - qu'il n'a pas eu l'intention de déménager clandestinement ; <br/>
              - qu'il a été en retard une seule fois en janvier 2017 pour pointer au commissariat ; <br/>
              - que sa fréquentation du restaurant dont le propriétaire a été mis en examen pour financement d'une entreprise terroriste était uniquement due à des raisons professionnelles. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ;<br/>
              - la loi n° 55-385 du 3 avril 1955 ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - la loi n° 2015-1501 du 20 novembre 2015 ;<br/>
              - la loi n° 2016-162 du 19 février 2016 ;<br/>
              - la loi n° 2016-629 du 20 mai 2016 ;<br/>
              - la loi n° 2016-987 du 21 juillet 2016 ;<br/>
              - la loi n° 2016-1767 du 19 décembre 2016 ;<br/>
              - le décret n° 2015-1475 du 14 novembre 2015 ;<br/>
              - le décret n° 2015-1476 du 14 novembre 2015 ;<br/>
              - le décret n° 2015-1478 du 14 novembre 2015 ;<br/>
              Vu la décision du Conseil constitutionnel n° 2017-624 QPC du 16 mars 2017 ;<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. D..., d'autre part, le ministre d'Etat, ministre de l'intérieur ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 19 juin 2017 à 11 heures 30 au cours de laquelle ont été entendus : <br/>
<br/>
              - la représentante du ministre d'Etat, ministre de l'intérieur ;<br/>
<br/>
              - M. et MmeD... ;<br/>
<br/>
              - les représentants de M. et MmeD... ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              Sur le cadre juridique du litige :<br/>
<br/>
              1. L'article L. 521-2 du code de justice administrative prévoit que le juge des référés, saisi d'une demande en ce sens justifiée par l'urgence, peut ordonner toute mesure nécessaire à la sauvegarde d'une liberté fondamentale à laquelle une autorité administrative aurait porté une atteinte grave et manifestement illégale.<br/>
<br/>
              2. En application de la loi du 3 avril 1955, l'état d'urgence a été déclaré sur le territoire métropolitain et en Corse par le décret n° 2015-1475 du 14 novembre 2015, à compter du même jour à zéro heure. Il a ensuite été prorogé par la loi à cinq reprises, pour une durée de trois mois par l'article 1er de la loi du 20 novembre 2015, à nouveau pour une durée de trois mois à compter du 26 février 2016 par l'article unique de la loi du 19 février 2016, pour une durée de deux mois par la loi du 20 mai 2016, pour une durée de six mois à compter du 21 juillet 2016 par la loi du même jour, et, en dernier lieu, jusqu'au 15 juillet 2017 par l'article 1er de la loi du 19 décembre 2016.<br/>
<br/>
              3. En vertu de l'article 6 de la loi du 3 avril 1955, dans la rédaction que lui a donnée la loi du 20 novembre 2015, l'état d'urgence permet au ministre de l'intérieur de prononcer l'assignation à résidence, dans un lieu qu'il fixe, d'une personne " à l'égard de laquelle il existe des raisons sérieuses de penser que son comportement constitue une menace pour l'ordre et la sécurité publics ". Cet article précise que la personne assignée à résidence " peut également être astreinte à demeurer dans le lieu d'habitation déterminé par le ministre de l'intérieur, pendant la plage horaire qu'il fixe, dans la limite de douze heures par vingt-quatre heures " et que le ministre peut prescrire à cette personne " l'obligation de se présenter périodiquement aux services de police ou aux unités de gendarmerie, selon une fréquence qu'il détermine, et dans la limite de trois présentations par jour ". Il ajoute que la personne assignée à résidence " peut se voir interdire par le ministre de l'intérieur de se trouver en relation, directement ou indirectement, avec certaines personnes, nommément désignées, dont il existe des raisons sérieuses de penser que leur comportement constitue une menace pour la sécurité et l'ordre publics ". Ainsi que le Conseil constitutionnel l'a constaté dans sa décision 2015-527 QPC du 22 décembre 2015, M. C...D., il revient au juge administratif de s'assurer que les mesures de police administrative prescrites sur le fondement de ces dispositions sont adaptées, nécessaires et proportionnées à la finalité qu'elles poursuivent.<br/>
              4. La loi du 19 décembre 2016 a introduit à l'article 6 de la loi du 3 avril 1955 des dispositions relatives à la durée maximale de l'assignation à résidence. Elle prévoit ainsi qu'" à compter de la déclaration de l'état d'urgence et pour toute sa durée, une même personne ne peut être assignée à résidence pour une durée totale équivalent à plus de douze mois ". Elle autorise toutefois le ministre de l'intérieur à prolonger une assignation à résidence au-delà de cette durée, en précisant que la prolongation ne peut excéder une durée de trois mois. Par sa décision 2017-624 QPC du 16 mars 2017, M. F... I., le Conseil constitutionnel a censuré les dispositions de cette loi qui, au-delà d'une première période transitoire de trois mois, subordonnaient la possibilité pour le ministre de l'intérieur de décider une telle prolongation à une autorisation du juge des référés du Conseil d'Etat. Il a en outre jugé qu' " au-delà de douze mois, une mesure d'assignation à résidence ne saurait, sans porter une atteinte excessive à la liberté d'aller et de venir, être renouvelée que sous réserve, d'une part, que le comportement de la personne en cause constitue une menace d'une particulière gravité pour la sécurité et l'ordre publics, d'autre part, que l'autorité administrative produise des éléments nouveaux ou complémentaires, et enfin que soient prises en compte dans l'examen de la situation de l'intéressé la durée totale de son placement sous assignation à résidence, les conditions de celle-ci et les obligations complémentaires dont cette mesure a été assortie ". Il revient au juge administratif de s'assurer qu'une décision par laquelle le ministre de l'intérieur prolonge, au-delà de douze mois, une assignation à résidence respecte les réserves ainsi formulées par le Conseil constitutionnel. Il lui appartient en conséquence de vérifier que le comportement de la personne concernée constitue une menace d'une particulière gravité pour l'ordre et la sécurité publics. Il lui incombe aussi de s'assurer que l'administration fait état d'éléments nouveaux ou complémentaires, qui résultent de faits qui sont survenus ou qui ont été révélés postérieurement à la décision initiale d'assignation à résidence ou aux précédents renouvellements, au cours des douze mois précédents. De tels faits peuvent résulter d'agissements de la personne concernée, de procédures judiciaires et même, si elles sont fondées sur des éléments nouveaux par rapport à ceux qui ont justifié la première mesure d'assignation, de décisions administratives. Le juge administratif contrôle enfin que l'administration a pris en compte la durée totale de l'assignation et l'ensemble des contraintes qui s'y attachent.<br/>
              5. Il résulte de l'instruction que, sur le fondement des dispositions mentionnées au point 2, M. et Mme D...ont fait l'objet d'arrêtés d'assignation à résidence le 23 décembre 2015, les astreignant à résider sur le territoire de la commune de Maubeuge (Nord), à se présenter trois fois par jour, respectivement à 8 heures, 15 heures et 19 heures pour M.D..., et à 8 heures, 12 heures et 19 heures pour Mme D...au commissariat de police de cette commune tous les jours de la semaine, y compris les jours fériés ou chômés et à demeurer à leur domicile tous les jours, de 20 heures à 6 heures, avec interdiction de se déplacer de leur lieu d'assignation à résidence sans avoir obtenu préalablement l'autorisation écrite du préfet du Nord. Les mesures d'assignation à résidence de M. et Mme D...ont été renouvelées dans les mêmes conditions par des arrêtés du 24 février et du 24 mai 2016. Par un arrêté du 26 mai 2016, le ministre de l'intérieur a modifié les horaires de pointage de Mme D...à 8 heures, 15 heures et 19 heures. Les mesures d'assignation à résidence de M. et Mme D...ont été renouvelées, dans les mêmes conditions, par des arrêtés du 22 juillet 2016. Par un arrêté du 25 août 2016, afin de tenir compte de la grossesse de MmeD..., le ministre de l'intérieur a réduit son obligation de passage au commissariat à une présentation quotidienne à 15 heures. Les mesures d'assignation à résidence de M. et Mme D...ont été renouvelées, dans les mêmes conditions, par des arrêtés du 20 décembre 2016, pour une durée de 90 jours, en application des dispositions précitées de l'article 2 de la loi du 19 décembre 2016. Par un nouvel arrêté du 20 mars 2017, le ministre de l'intérieur a renouvelé l'assignation à résidence de M. D...pour une durée de trois mois. M. D...en a demandé le 9 juin 2017 au juge des référés du tribunal administratif de Lille la suspension sur le fondement de l'article L. 521-2 du code de justice administrative. Le 12 juin 2017, l'obligation de se présenter au commissariat à 15 heures a été supprimée.<br/>
<br/>
              6. Par une ordonnance du 16 juin 2017, dont le ministre de l'intérieur demande l'annulation, le juge des référés du tribunal administratif de Lille a suspendu l'exécution de l'arrêté du 20 mars 2017.<br/>
<br/>
              7. Il résulte de l'instruction, en particulier de plusieurs " notes blanches " des services de renseignement soumis au débat contradictoire, que M. et Mme D...ont été proches de la mouvance islamiste, ont fréquenté des personnes en lien avec l'organisation dite " Etat islamique ", ont consulté régulièrement des sites relayant les activités et mots d'ordre de cette organisation terroriste, ainsi que la publication " Dar Al Islam " prônant la guerre sainte et publiant des consignes à l'usage des individus projetant de commettre des attentats entre autres sur le territoire de la République. M. D...a notamment publié sur son compte Facebook des images et textes favorables au djihad mené en Syrie, et s'est livré avec des personnes partageant la même idéologie à des entrainements paramilitaires dans le bois de Marpent (Nord). Une perquisition administrative réalisée à leur domicile, le 21 novembre 2015, a conduit à la saisie, sur l'ordinateur familial, de supports informatiques comportant plusieurs centaines de fichiers relatifs à l'organisation mentionnée ci-dessus. Il résulte également de l'instruction qu'une autre perquisition du domicile de M. et MmeD..., diligentée le 20 septembre 2016, a donné lieu à une exploitation de l'ordinateur familial ayant permis de trouver trace de multiples consultations de sites djihadistes. <br/>
<br/>
              8. Compte-tenu de l'ensemble de ces éléments, M. D...a pu être regardé comme représentant une menace d'une particulière gravité pour la sécurité et l'ordre publics laquelle justifie qu'ils fassent l'objet d'une surveillance renforcée impliquant que soient prises à leur endroit des mesures d'assignation à résidence. <br/>
<br/>
              9. M. et Mme D...ont en novembre 2016 changé de domicile sans solliciter au préalable, ainsi qu'ils y étaient tenus, l'autorisation de l'autorité administrative. Ils ont en conséquence été condamnés par le tribunal correctionnel en février 2017 à une amende de 500 euros avec sursis.<br/>
<br/>
              10. Par une décision du 3 janvier 2017, le ministre de l'intérieur a refusé d'aménager les conditions d'assignation à résidence de M.D..., celui-ci ayant obtenu une promesse d'embauche émanant de la société " K One ", exploitant un établissement de restauration rapide, et a, par un arrêté du 24 janvier 2017, assorti la mesure d'assignation à résidence de M. D...de l'obligation pour l'intéressé de ne plus se trouver en relation, directement ou indirectement, avec M. A...B... ; M.B..., qui exploite cet établissement, lequel est fréquenté par des personnes relevant de la mouvance salafiste, est sous contrôle judiciaire du chef de financement d'une entreprise terroriste pour avoir fait parvenir des fonds à son frère, qui se trouve dans une zone de combat en lien avec le djihad. <br/>
<br/>
              11. Le 25 janvier 2017, M. D...ne s'est pas présenté au pointage de 19 heures au commissariat de Maubeuge et était absent de son domicile à l'arrivée des policiers. Il a été condamné, à ce titre, le 27 janvier 2017, à un mois de prison ferme par le tribunal de grande instance d'Avesnes-sur-Helpe. <br/>
<br/>
              12. Si le juge des référés du Conseil d'Etat, saisi par les épouxD..., a, par une ordonnance du 17 mars 2017, confirmé l'ordonnance du juge des référés du tribunal administratif de Lille du 13 février 2017 rejetant leur demande tendant à la suspension des arrêtés du 20 décembre 2017 prolongeant leur assignation à résidence à Maubeuge pour une durée de trois mois, au motif que des éléments nouveaux et complémentaires justifiaient le renouvellement de l'assignation à résidence de M.D..., il ne résulte pas de l'instruction que ce dernier soit actuellement en contact avec les trois personnes impliquées dans la mouvance islamiste radicale avec lesquelles il avait eu des relations les années précédentes ; au demeurant, l'une est à l'étranger et ainsi qu'il a été dit, il lui est interdit de voir l'autre. Ainsi, alors même que des éléments nouveaux et complémentaires ont été fournis pour la période précédant l'édiction de l'arrêté du 20 mars 2017, lesquels justifiaient l'arrêté, le ministre ne fait état dans son recours d'aucun élément nouveau ou complémentaire intervenu depuis cette date de nature à établir la persistance de la menace. Par suite, alors que l'intéressé est assigné à résidence depuis le 23 décembre 2015 et compte tenu de ce qu'il n'est pas établi qu'il soit impliqué à ce jour dans des mouvements radicaux islamistes, il ne résulte pas de l'instruction qu'à la date de la présente ordonnance, le comportement de M. D...constitue une menace d'une particulière gravité pour l'ordre public. Par suite, le ministre de l'intérieur n'est pas fondé à soutenir que c'est à tort que, par son ordonnance, le juge des référés a suspendu le 16 juin 2017 l'exécution de l'arrêté du 20 mars 2017. <br/>
<br/>
              13. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 1 000 euros à verser à M. D...au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.  <br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : Le recours du ministre de l'intérieur est rejeté.<br/>
Article 2 : L'Etat versera une somme de 1 000 euros à M. D...au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
Article 3 : La présente ordonnance sera notifiée à M. E...D...et au ministre d'Etat, ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
