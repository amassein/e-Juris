<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028158631</ID>
<ANCIEN_ID>JG_L_2013_10_000000329420</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/15/86/CETATEXT000028158631.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 16/10/2013, 329420, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-10-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>329420</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN</AVOCATS>
<RAPPORTEUR>M. François Loloum</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:329420.20131016</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<p>Vu le recours, enregistré le 3 juillet 2009 au secrétariat du contentieux du Conseil d'Etat, du Ministre De L'économie Et Des Finances ;<br clear="none"/>
<br clear="none"/>
le ministre du budget, des comptes publics et de la fonction publique demande au Conseil d'Etat :<br clear="none"/>
<br clear="none"/>
1°) d'annuler l'arrêt n° 08NC00905 du 13 mai 2009 par lequel la cour administrative d'appel de Nancy, après avoir prononcé un non-lieu partiel, a accordé à M. et Mme B...A...la décharge des suppléments d'impôt sur le revenu et de contributions sociales auxquels ils restaient assujettis au titre de 1999, ainsi que des pénalités correspondantes ;<br clear="none"/>
<br clear="none"/>
2°) réglant l'affaire au fond, de faire droit à ses précédentes conclusions de rejet de la requête ou, à titre subsidiaire, de procéder à une substitution de base légale et d'imposer la somme litigieuse dans la catégorie des traitements et salaires en tant qu'avantage en nature consenti à un dirigeant ;<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
Vu les autres pièces du dossier ;<br clear="none"/>
<br clear="none"/>
Vu le code général des impôts et le livre des procédures fiscales ;<br clear="none"/>
<br clear="none"/>
Vu le code de justice administrative ;<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
Après avoir entendu en séance publique :<br clear="none"/>
<br clear="none"/>
- le rapport de M. François Loloum, Conseiller d'Etat,<br clear="none"/>
<br clear="none"/>
- les conclusions de Mme Delphine Hedary, rapporteur public ;<br clear="none"/>
<br clear="none"/>
La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, avocat de M. ou Mme B...A...;<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société par actions simplifiée (SAS) Adélaïde, société holding détenant la quasi-totalité du capital de la société Verlingue, a cédé le 2 août 1999 à M.A..., cadre dirigeant de cette dernière société, cinquante actions de la société Verlingue au prix unitaire de 1 200 F ; qu'à la suite de la vérification de la comptabilité de la société Adélaïde, l'administration fiscale a estimé que le prix consenti à M. A...avait été anormalement minoré par rapport à la valeur vénale de ces titres et que l'écart entre la valeur réelle des titres et celle dont l'acquéreur avait bénéficié lors de cette vente devait être regardé comme une libéralité accordée par la société Adélaïde ; qu'elle a en conséquence notifié à M. A... un redressement des bases de l'impôt sur le revenu et de cotisations sociales au titre de l'année 1999 sur le fondement du c de l'article 111 du code général des impôts ; qu'au cours de l'instance devant la cour administrative d'appel de Nancy, l'administration a admis que la valeur des titres devait être ramenée à 5 900 F et a prononcé le dégrèvement correspondant ; que le ministre du budget, des comptes publics et de la fonction publique se pourvoit en cassation contre l'arrêt du 13 mai 2009 en tant que la cour, infirmant le jugement du tribunal administratif de Strasbourg du 22 avril 2008, a accordé à M. et Mme B... A...la décharge des suppléments d'impôt sur le revenu et de contributions sociales auxquels ils restaient assujettis au titre de 1999, ainsi que des pénalités correspondantes ;<br clear="none"/>
<br clear="none"/>
2. Considérant qu'aux termes de l'article 111 du code général des impôts : "Sont notamment considérés comme revenus distribués ... c) les rémunérations et avantages occultes" ; qu'en cas d'acquisition par une société à un prix que les parties ont délibérément majoré par rapport à la valeur vénale de l'objet de la transaction, ou, s'il s'agit d'une vente, délibérément minoré, sans que cet écart de prix comporte de contrepartie, l'avantage ainsi octroyé doit être requalifié comme une libéralité représentant un avantage occulte constitutif d'une distribution de bénéfices au sens des dispositions précitées du c de l'article 111 du code général des impôts, alors même que l'opération est portée en comptabilité et y est assortie de toutes les justifications concernant son objet et l'identité du cocontractant, dès lors que cette comptabilisation ne révèle pas, par elle-même, la libéralité en cause ; que la preuve d'une telle distribution occulte doit être regardée comme apportée par l'administration lorsqu'elle établit l'existence, d'une part, d'un écart significatif entre le prix convenu et la valeur vénale du bien cédé et, d'autre part, d'une intention, pour la société, d'octroyer, et, pour le cocontractant, de recevoir, une libéralité du fait des conditions de la cession, sans que cet avantage soit assorti d'une contrepartie ; que la cour a dénaturé les pièces du dossier en jugeant que le ministre se bornait à déduire de l'anormalité du prix l'intention de la société Adelaïde d'accorder une libéralité et ne contredisait pas les allégations du contribuable relatives à l'existence d'une contrepartie de la minoration du prix de cession, alors que, pour établir l'intention libérale des parties à la cession de titres, le ministre invoquait non seulement l'écart significatif du prix convenu et de la valeur réelle de l'action mais aussi l'absence d'intérêt propre de la société Adelaïde à consentir un prix de vente minoré en l'absence de contrepartie pour elle et les relations d'intérêt particulières entre les parties à la transaction en raison des importantes responsabilités de M. A...au sein de la filiale et de son appartenance à l'équipe de direction du groupe Verlingue ;<br clear="none"/>
<br clear="none"/>
3. Considérant qu'il résulte de ce qui précède et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que le ministre est fondé à demander l'annulation de l'article 2 de l'arrêt attaqué ;<br clear="none"/>
<br clear="none"/>
4. Considérant qu'il y a lieu, en application de l'article L 821-2 du code de justice administrative, de statuer sur les conclusions d'appel de M. et Mme A...qui demeurent pendantes;<br clear="none"/>
<br clear="none"/>
5. Considérant que pour établir que le prix de cession dont a bénéficié M. A... était minoré, l'administration a utilisé quatre méthodes de valorisation ; que la seule circonstance que chacune donnait un prix différent est sans incidence sur la pertinence de leur utilisation combinée ; que comme le reconnaît le requérant, l'administration n'était pas tenue d'utiliser les pondérations purement indicatives du guide d'évaluation ; que, s'agissant de la cession de titres minoritaires pour une fraction très mineure du capital, l'administration pouvait à bon droit prendre en compte, pour déterminer la valeur des titres, les revenus tirés par l'entreprise d'un contrat d'un an renouvelable par tacite reconduction entre la société et une autre appartenant au même groupe dont la cession litigieuse n'affectait en rien la pérennité ; que la valeur ainsi établie par plusieurs méthodes constituait une évaluation raisonnable de la valeur du titre, à laquelle le recours à une seule méthode, ne prenant pas en compte le contrat dont la société retirait une large part de ses revenus, n'aurait pas permis de procéder ; qu'il résulte toutefois de l'instruction que le coefficient à appliquer à la marge brute d'autofinancement pour déterminer la valeur vénale des actions doit être ramené de 9 à 5 ; que, sous réserve de cette réduction de l'écart de prix, M. et Mme A... ne sont pas fondés à soutenir que c'est à tort que le jugement attaqué a estimé que l'administration établissait que la cession dont ils ont été les bénéficiaires avait été opérée à un prix minoré ;<br clear="none"/>
<br clear="none"/>
6. Considérant que si M. et Mme A... soutiennent que la société mère trouvait la contrepartie de l'avantage dont ils avaient bénéficié dans le maintien de la collaboration de cadres supérieurs employés par ses filiales, ils se bornent à exposer des considérations générales, sans faire état de circonstances particulières propres aux relations entre la SAS Adélaïde et sa filiale et sans apporter aucun élément précis de nature à corroborer une politique de l'entreprise en direction du personnel d'encadrement de la filiale ni aucune pièce mentionnant le rôle assigné aux cadres dirigeants bénéficiaires de cette politique ; qu'ainsi, en jugeant que la cession n'était assortie d'aucune contrepartie qui aurait justifié l'avantage consenti, le tribunal administratif de Strasbourg n'a pas inexactement qualifié les faits de l'espèce ;<br clear="none"/>
<br clear="none"/>
7. Considérant que l'administration fait valoir que M. A...faisait partie de l'équipe de direction du groupe Verlingue et avait été, en qualité de chargé du développement de la société Verlingue, responsable d'une opération récente de fusion entre une société soeur et la société Verlingue, de sorte qu'il ne pouvait ignorer la valeur réelle des titres de cette société ; qu'en invoquant les relations d'intérêt particulières entre le SAS Adélaïde et M.A..., l'administration apporte la preuve qui lui incombe de l'intention de la société d'octroyer et de M. A... de recevoir une libéralité sous la forme d'une minoration du prix de la cession de titres en cause ;<br clear="none"/>
<br clear="none"/>
8. Considérant qu'il résulte de ce qui précède que M. et Mme A...ne sont fondés à demander la décharge des impositions restant en litige que dans la mesure de la réduction de leur base d'imposition résultant de l'abaissement du coefficient à appliquer à la marge brute d'autofinancement ;<br clear="none"/>
<br clear="none"/>
9. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 500 euros en application de l'article L. 761-1 du code de justice administrative ;<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
D E C I D E :<br clear="none"/>
--------------<br clear="none"/>
Article 1er : L'article 2 de l'arrêt de la cour administrative d'appel de Nancy en date du 13 mai 2009 est annulé.<br clear="none"/>
Article 2 : La base d'imposition au titre de l'année 1999 sera calculée en appliquant à la marge brute d'autofinancement pour déterminer la valeur vénale des actions un coefficient ramené de 9 à 5.<br clear="none"/>
Article 3 : M. et Mme A...sont déchargés des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales et des pénalités correspondantes auxquels ils ont été assujettis au titre de 1999 à proportion de la réduction de base d'imposition définie à l'article 2.<br clear="none"/>
Article 4 : Le jugement du tribunal administratif de Strasbourg est réformé en ce sens.<br clear="none"/>
Article 5 : L'Etat versera à M. et Mme A...la somme de 1 500 euros en l'application de l'article L. 761-1 du code de justice administrative.<br clear="none"/>
Article 6 : Le surplus des conclusions de M. et Mme A... devant la cour administrative d'appel de Nancy est rejeté.<br clear="none"/>
Article 7 : La présente décision sera notifiée à M. et Mme B... A...et au ministre de l'économie et des finances.<br clear="none"/>
</p>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP>
<CONTENU>



</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
