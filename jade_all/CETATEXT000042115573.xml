<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042115573</ID>
<ANCIEN_ID>JG_L_2020_07_000000414099</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/11/55/CETATEXT000042115573.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 13/07/2020, 414099, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414099</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>Mme Marie Grosset</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:414099.20200713</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance n° 1506890 du 5 novembre 2015, le président du tribunal administratif de Versailles a transmis au tribunal administratif de Châlons-en-Champagne, en application de l'article R. 351-3 du code de justice administrative, la requête présentée à ce tribunal par Mme D... A...-E.... <br/>
<br/>
              Par une ordonnance n° 1502391 du 29 août 2017, enregistrée le 5 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de Châlons-en-Champagne a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée au tribunal administratif de Versailles par Mme A...-E.... <br/>
<br/>
              Par cette requête, enregistrée au greffe du tribunal administratif de Versailles le 19 octobre 2015, un mémoire complémentaire et un nouveau mémoire enregistrés au greffe du tribunal administratif de Châlons-en-Champagne le 19 novembre 2015 et le 5 février 2016, et un mémoire en réplique enregistré au secrétariat du contentieux du Conseil d'Etat le 23 juillet 2019, Mme A...-E... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la délibération du 7 avril 2015 par laquelle le conseil académique restreint de l'université Paris-Ouest Nanterre La Défense a refusé de transmettre au conseil d'administration sa demande de mutation pour rapprochement de conjoint sur le poste n° 4284 de professeur des universités (psychologie empirique et cognitivo-comportementale), ainsi que la décision implicite rejetant son recours gracieux contre la décision du président de l'université du 17 avril 2015 l'informant de la décision du conseil académique et cette délibération ;<br/>
<br/>
              2°) d'annuler pour excès de pouvoir la délibération du 21 avril 2015 par laquelle le comité de sélection a rejeté sa candidature au poste n° 4284 ; <br/>
<br/>
              3°) d'annuler pour excès de pouvoir, par voie de conséquence, le décret du 1er février 2016 en tant qu'il nomme M. C... B... professeur des universités à l'université de Paris X ;<br/>
<br/>
              4°) d'enjoindre à l'université de Paris-Ouest Nanterre La Défense de statuer à nouveau sur sa candidature dans le cadre de la procédure prévue par l'article 9-3 du décret n° 84-431 du 6 juin 1984 et de la procédure prévue par l'article 9-2 du même décret dans le délai d'un mois ; <br/>
<br/>
              5°) de mettre à la charge de l'université de Paris-Ouest Nanterre La Défense la somme de 3 800 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ; <br/>
              - le code de procédure civile ;<br/>
              - la loi n° 84-16 du 11 janvier 1984, notamment son article 60 ;<br/>
              - le décret n° 84-431 du 6 juin 1984 ; <br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Grosset, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de Mme A...-E... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 952-6-1 du code de l'éducation : " (...) lorsqu'un emploi d'enseignant-chercheur est créé ou déclaré vacant, les candidatures des personnes dont la qualification est reconnue par l'instance nationale prévue à l'article L. 952-6 sont soumises à l'examen d'un comité de sélection créé par délibération du conseil académique (...)./ Le comité est composé d'enseignants-chercheurs et de personnels assimilés, pour moitié au moins extérieurs à l'établissement, d'un rang au moins égal à celui postulé par l'intéressé. Ses membres sont proposés par le président et nommés par le conseil académique (...) siégeant en formation restreinte aux enseignants-chercheurs et personnels assimilés. (...) / Au vu de son avis motivé, le conseil académique (...) siégeant en formation restreinte aux enseignants-chercheurs et personnels assimilés de rang au moins égal à celui postulé, transmet au ministre compétent le nom du candidat dont il propose la nomination ou une liste de candidats classés par ordre de préférence (...) ". Aux termes de l'article 9-2 du décret du 6 juin 1984 fixant les dispositions statutaires communes applicables aux enseignants-chercheurs et portant statut particulier du corps des professeurs des universités et du corps des maîtres de conférences : " Le comité de sélection examine les dossiers des maîtres de conférences ou professeurs postulant à la nomination dans l'emploi par mutation et des candidats à cette nomination par détachement et par recrutement au concours parmi les personnes inscrites sur la liste de qualification aux fonctions, selon le cas, de maître de conférences ou de professeur des universités. Au vu de rapports pour chaque candidat présenté par deux de ses membres, le comité établit la liste des candidats qu'il souhaite entendre. Les motifs pour lesquels leur candidature n'a pas été retenue sont communiqués aux candidats qui en font la demande. (...) / Après avoir procédé aux auditions, le comité de sélection délibère sur les candidatures et, par un avis motivé unique portant sur l'ensemble des candidats, arrête la liste, classée par ordre de préférence, de ceux qu'il retient. Le comité de sélection se prononce à la majorité des voix des membres présents. En cas de partage des voix, le président du comité a voix prépondérante. / Le comité de sélection émet un avis motivé unique portant sur l'ensemble des candidats, ainsi qu'un avis motivé sur chaque candidature (...) ". Aux termes de l'article 9-3 du même décret : " Par dérogation à l'article 9-2, le conseil académique (...), en formation restreinte, examine les candidatures à la mutation et au détachement des personnes qui remplissent les conditions prévues aux articles 60 et 62 de la loi du 11 janvier 1984 susvisée, sans examen par le comité de sélection. Si le conseil académique retient une candidature, il transmet le nom du candidat sélectionné au conseil d'administration. Lorsque l'examen de la candidature ainsi transmise conduit le conseil d'administration à émettre un avis favorable sur cette candidature, le nom du candidat retenu est communiqué au ministre chargé de l'enseignement supérieur. L'avis défavorable du conseil d'administration est motivé. / Lorsque la procédure prévue au premier alinéa n'a pas permis de communiquer un nom au ministre chargé de l'enseignement supérieur, les candidatures qui n'ont pas été retenues par le conseil académique ou qui ont fait l'objet d'un avis défavorable du conseil d'administration sont examinées avec les autres candidatures par le comité de sélection selon la procédure prévue à l'article 9-2. ". <br/>
<br/>
              2. Il ressort des pièces du dossier que Mme A...-E..., professeur des universités en psychologie clinique à l'université de Reims-Champagne-Ardennes, a fait acte de candidature sur le poste de professeur des universités intitulé " psychologie empirique et cognito-comportementale" ouvert sous le n° 4284 à l'université Paris Ouest Nanterre La Défense en demandant à bénéficier des dispositions, citées au point 1, relatives aux personnes dont la candidature est dispensée de l'examen par le comité de sélection, au titre de la muation pour rapprochement de conjoints. Par une délibération du 7 avril 2015, le conseil académique de l'université Paris Ouest Nanterre La Défense a toutefois refusé de transmettre sa candidature au conseil d'administration et l'a transmise au comité de sélection, pour qu'elle soit examinée avec l'ensemble des autres candidatures. Par lettre du 17 avril 2015, le président de l'université a informé Mme A...-E... de la délibération du conseil académique. Par une délibération du 21 avril 2015, le comité de sélection a émis un avis défavorable à la candidature de Mme A...-E.... Par une décision du 16 août 2015, le président de l'université a rejeté le recours de Mme A...-E... tendant au retrait de la délibération du conseil académique et de la lettre du 17 avril 2015. Enfin, à l'issue de la procédure de recrutement, le Président de la République a, par un décret du 1er février 2016, nommé M. C... B... sur le poste ouvert à ce concours. Mme A...-E... demande l'annulation pour excès de pouvoir de ces décisions et du décret du 1er février 2016. <br/>
<br/>
              Sur les conclusions dirigées contre la délibération du conseil académique en date du 7 avril 2015 :<br/>
<br/>
              3. En premier lieu, contrairement à ce qui est soutenu en défense, la lettre en date du 12 juin 2015 adressée par la requérante au président de l'université présentait, compte tenu de ses termes, le caractère d'un recours gracieux contre cette délibération et la lettre du 17 avril l'informant de cette délibération. En conséquence ce recours gracieux, présenté dans le délai de deux mois, a eu pour effet d'interrompre le délai de recours contentieux. Ce délai, qui a recommencé à courir le 16 août 2015 et qui expirait le samedi 17 octobre 2015, s'est trouvé, en application de l'article 642 du code de procédure civile, prorogé jusqu'au premier jour ouvrable suivant. Par suite, la requête de Mme A...-E..., enregistrée au greffe du tribunal administratif de Versailles le lundi 19 octobre 2015, n'était pas tardive. <br/>
<br/>
              4. En deuxième lieu, aux termes de l'article R. 412-1 du code de justice administrative, dans sa rédaction alors applicable : " La requête doit, à peine d'irrecevabilité, être accompagnée, sauf impossibilité justifiée, de la décision attaquée [...]. Il ressort des pièces du dossier que Mme A...-E... a accompli auprès de l'université Paris Ouest Nanterre La Défense toutes les diligences qu'elle pouvait effectuer afin de se procurer la délibération du conseil académique du 7 avril 2015. En refusant de lui communiquer cette délibération, l'université n'a pas mis Mme A...-E... à même de satisfaire à l'exigence de production résultant de l'article R. 412-1 du code de justice administrative. Dès lors, la fin de non-recevoir qu'elle oppose à ce titre ne peut qu'être écartée.<br/>
<br/>
              5. L'avis défavorable émis par le conseil académique de l'université Paris Ouest Nanterre La Défense sur la candidature de Mme A...-E..., produit par l'université en réponse à la mesure d'instruction ordonnée par la quatrième chambre de la section du contentieux,  se borne à indiquer que le conseil académique siégeant en formation restreinte n'a pas retenu la candidature de l'intéressée et que son dossier a été transmis au comité de sélection. Cette délibération, insuffisamment motivée, est ainsi entachée d'illégalité. <br/>
<br/>
              6. Il résulte de ce qui précède que Mme A...-E... est recevable et fondée à demander l'annulation de la délibération du conseil académique de l'université Paris Ouest Naterre La Défense du 7 avril 2015 ainsi que la décision du président de l'université rejetant son recours gracieux, sans qu'il soit besoin d'examiner l'autre moyen soulevé à l'encontre de ces décisions. <br/>
<br/>
              Sur les conclusions dirigées contre la délibération du comité de sélection en date du 21 avril 2015 :<br/>
<br/>
              7. Il résulte de l'annulation prononcée au point 6 que les décisions prises, pour le recrutement sur le poste litigieux, à la suite de la transmission illégale de la candidature de Mme A...-E... au comité de sélection, sont, par voie de conséquence, également entachées d'illégalité. Mme A...-E... est, par suite, fondée à demander l'annulation de la délibération du comité de sélection du 21 avril 2015 relative à ce recrutement, sans qu'il soit besoin d'examiner les autres moyens soulevés à l'encontre de cette décision.<br/>
<br/>
              Sur les conclusions dirigées contre le décret du 1er février 2016 :<br/>
<br/>
              8. Si, dans un mémoire enregistré le 23 juillet 2019, Mme A...-E... conclut à l'annulation du décret du 1er février 2016 du Président de la République en tant qu'il nomme M. C... B... sur le poste litigieux, ces conclusions, dirigées contre un décret publié au Journal officiel de la République française le 3 février 2016, sont tardives et, par suite, irrecevables.<br/>
<br/>
              Sur les conclusions à fins d'injonction :<br/>
<br/>
              9. Il résulte de l'instruction et de ce qui est dit au point 8 que la procédure de recrutement sur le poste de professeur des universités en " psychologie empirique et cognitivo-comportementale " à l'université Paris Ouest Nanterre La Défense a fait l'objet d'une décision de nomination devenue définitive. Les conclusions de Mme A...-E... tendant à ce qu'il soit enjoint à cette université de reprendre la procédure de recrutement au stade de l'examen des demandes de mutation ne peuvent, par suite, qu'être rejetées.<br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'université Paris Ouest Nanterre La Défense une somme de 3000 euros à verser à Mme A...-E... au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à la charge de Mme A...-E... qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La délibération du 7 avril 2015 du conseil académique de l'université Paris Ouest Nanterre La Défense, la décision du président de l'université rejetant le recours gracieux de Mme A...-E... tendant au retrait de cette délibération et la délibération du 21 avril 2015 du comité de sélection sont annulées.<br/>
Article 2 : L'université Paris-Ouest Nanterre La Défense versera à Mme A...-E... une somme de 3000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Le surplus des conclusions de la requête de Mme A...-E... est rejeté. <br/>
Article 4 : Les conclusions de l'université Paris-Ouest Nanterre La Défense présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à Mme D... A...-E... et à l'université Paris-Ouest Nanterre La Défense.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
