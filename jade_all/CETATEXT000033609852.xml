<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033609852</ID>
<ANCIEN_ID>JG_L_2016_12_000000396662</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/60/98/CETATEXT000033609852.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 14/12/2016, 396662, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396662</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:396662.20161214</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 2 février et 2 mai 2016 au secrétariat du contentieux du Conseil d'Etat, la Fédération nationale des producteurs et élaborateurs de Crémant demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté interministériel du 26 novembre 2015 relatif à l'indication géographique protégée " Côtes de la Charité " ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement (UE) n° 1308/2013 du Parlement européen et du Conseil du 17 décembre 2013 portant organisation commune des marchés des produits agricoles ;<br/>
              - le règlement (CE) n° 607/2009 de la Commission du 14 juillet 2009 fixant certaines modalités d'application du règlement (CE) n° 479/2008 en ce qui concerne les appellations d'origine protégées et les indications géographiques protégées, les mentions traditionnelles, l'étiquetage et la présentation de certains produits du secteur vitivinicole ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la Fédération nationale des producteurs et élaborateurs de Crémant ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Pour le secteur vitivinicole, le b) du paragraphe 1 de l'article 93 du règlement (UE) n° 1308/2013 du Parlement européen et du Conseil du 17 décembre 2013 portant organisation commune des marchés des produits agricoles, qui reprend sur ce point les dispositions du b) du paragraphe 1 de l'article 118 ter du règlement (CE) n° 1234/2007 du Conseil du 22 octobre 2007, définit l'indication géographique protégée comme : " une indication renvoyant à une région, à un lieu déterminé ou, dans des cas exceptionnels, à un pays, qui sert à désigner un produit (...) : / i) possédant une qualité, une réputation ou d'autres caractéristiques particulières attribuables à cette origine géographique (...) ". Selon le 2 de l'article 94 du même règlement : " Le cahier des charges permet aux parties intéressées de vérifier le respect des conditions de production associées à l'appellation d'origine ou à l'indication géographique. / Il comporte au minimum les éléments suivants :/ (...) g) les éléments qui corroborent le lien visé (...) à l'article 93, paragraphe 1, point b) i) ". Enfin, l'article 7 du règlement (CE) n° 607/2009 de la Commission du 14 juillet 2009 fixant certaines modalités d'application du règlement (CE) n° 479/2008 du Conseil en ce qui concerne les appellations d'origine protégées et les indications géographiques protégées, les mentions traditionnelles, l'étiquetage et la présentation de certains produits du secteur vitivinicole dispose que : " 1. Les éléments qui corroborent le lien géographique (...) expliquent dans quelle mesure les caractéristiques de la zone géographique délimitée influent sur le produit final (...) 3. Pour une indication géographique, le cahier des charges contient : a) des informations détaillées sur la zone géographique contribuant au lien ; / b) des informations détaillées sur la qualité, la réputation ou d'autres caractéristiques spécifiques du produit découlant de son origine géographique ; / c) une description de l'interaction causale entre les éléments visés au point a) et ceux visés au point b). / 4. Pour une indication géographique, le cahier des charges précise si l'indication se fonde sur une qualité ou une réputation spécifique ou sur d'autres caractéristiques liées à l'origine géographique ".<br/>
<br/>
              2. Il résulte clairement de ces dispositions que l'homologation d'un cahier des charges d'une indication géographique protégée, qui n'est pas une simple indication de provenance géographique, ne peut légalement intervenir que si ce cahier précise les éléments qui permettent d'attribuer à une origine géographique déterminée une qualité, une réputation ou d'autres caractéristiques particulières du produit qui fait l'objet de l'indication et met en lumière de manière circonstanciée le lien géographique et l'interaction causale entre la zone géographique et la qualité, la réputation ou d'autres caractéristiques du produit. Il découle en outre nécessairement de ces mêmes dispositions qu'elles ne permettent de reconnaître un lien avec une origine géographique que pour une production existante, attestée dans la zone géographique à la date de l'homologation et depuis un temps suffisant pour établir ce lien. Enfin, celui-ci doit être établi pour un produit déterminé et ne peut donc procéder d'une analogie avec un autre produit, même voisin.<br/>
<br/>
              3. Par l'arrêté attaqué du 26 novembre 2015, les ministres compétents ont homologué le nouveau cahier des charges de l'indication géographique protégée " Côtes de la Charité ", proposé par l'Institut national de l'origine et de la qualité. Si la Fédération nationale des producteurs et élaborateurs de Crémant demande au Conseil d'Etat l'annulation pour excès de pouvoir de cet arrêté, il ressort des termes de sa requête et notamment des moyens développés qu'elle entend attaquer cet arrêté en tant seulement qu'il élargit aux vins mousseux de qualité rosés, gris et blancs la possibilité de se prévaloir de l'indication géographique protégée " Côtes de la Charité ", auparavant réservée aux seuls vins tranquilles par la précédente dénomination "Vin de pays des coteaux charitois " à laquelle s'est substituée l'indication litigieuse.<br/>
<br/>
              4. Il ressort des pièces du dossier que l'antériorité de la production de vins mousseux de qualité dans la zone géographique délimitée par le cahier des charges litigieux n'était pas établie à la date de l'arrêté attaqué. Il résulte de ce qui a été dit au point 2 que, dès lors qu'une production existante de vins mousseux de qualité n'est pas attestée dans la zone géographique à la date de l'homologation et depuis un temps suffisant pour établir le lien entre l'origine géographique et des vins mousseux de qualité, qui doit être établi pour ces vins et ne peut donc procéder d'une analogie avec des vins tranquilles de qualité, les ministres ont entaché leur arrêté d'une erreur manifeste d'appréciation en estimant que l'existence d'un lien géographique pouvait être établie entre l'aire géographique de l'indication " Côtes de la Charité " et la production de vins mousseux de qualité rosés, gris et blancs.<br/>
<br/>
              5. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de la requête, la Fédération nationale des producteurs et élaborateurs de Crémant est fondée à demander l'annulation de l'arrêté attaqué en tant qu'il homologue les dispositions du cahier des charges litigieux qui autorisent les vins mousseux de qualité issus des zones qu'il définit à se prévaloir de l'indication géographique protégée " Côtes de la Charité ". <br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 000 euros à verser à la Fédération nationale des producteurs et élaborateurs de Crémant, au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêté interministériel du 26 novembre 2015 relatif à l'indication géographique protégée " Côtes de la Charité " est annulé en tant qu'il homologue celles des dispositions du cahier des charges de cette indication géographique protégée relatives aux vins mousseux de qualité rosés, gris et blancs.<br/>
Article 2 : L'Etat versera une somme de 1 000 euros à la Fédération nationale des producteurs et élaborateurs de Crémant au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à la Fédération nationale des producteurs et élaborateurs de Crémant, au ministre de l'économie et des finances, au ministre de l'agriculture, de l'agroalimentaire et de la forêt et à l'Institut national de l'origine et de la qualité.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
