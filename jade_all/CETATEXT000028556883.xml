<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028556883</ID>
<ANCIEN_ID>JG_L_2014_01_000000359582</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/55/68/CETATEXT000028556883.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 27/01/2014, 359582, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-01-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359582</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BLANC, ROUSSEAU</AVOCATS>
<RAPPORTEUR>M. Rémi Decout-Paolini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:359582.20140127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>VU LA PROCEDURE SUIVANTE :<br/>
<br/>
 Procédure contentieuse antérieure<br/>
<br/>
              Mme D...B...a demandé au tribunal administratif de Paris de prononcer la décharge de l'obligation de payer la somme de 17 045,36 euros réclamée par un avis à tiers détenteur émis le 1er février 2010 par le trésorier-payeur général de l'Assistance publique - Hôpitaux de Paris (AP-HP), en règlement des frais d'hospitalisation à l'hôpital Charles-Foix de Mme C...A..., sa mère, entre le 1er avril 2003 et le 26 mai 2004. Par un jugement n° 1008306 du 20 janvier 2012, le tribunal administratif de Paris a prononcé la décharge des sommes réclamées à MmeB....<br/>
<br/>
              Par une ordonnance n° 12PA01112 du 30 mars 2012, le président de la troisième chambre de la cour administrative d'appel de Paris, sur la demande du trésorier-payeur général de l'Assistance publique - Hôpitaux de Paris, a annulé le jugement du tribunal administratif de Paris du 20 janvier 2012 et a rejeté, comme portée devant une juridiction incompétente pour en connaître, la demande présentée devant ce tribunal par MmeB....<br/>
<br/>
 Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 22 mai, 21 août et 30 octobre 2012 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance n° 12PA01112 de la cour administrative d'appel de Paris du 30 mars 2012 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du trésorier-payeur général de l'Assistance publique - Hôpitaux de Paris et de lui enjoindre de restituer les sommes qui auraient pu être saisies sur ses comptes personnels en exécution de l'avis d'opposition à tiers détenteur du 1er février 2010 ;<br/>
<br/>
              3°) de mettre à la charge de l'Assistance publique - Hôpitaux de Paris la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par un mémoire en défense, enregistré le 16 octobre 2012, le ministre de l'économie et des finances s'en remet à la sagesse du Conseil d'Etat sur la juridiction compétente et conclut au rejet du surplus des conclusions du pourvoi.<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Rémi Decout-Paolini, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Blanc, Rousseau, avocat de MmeB... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>CONSIDERANT CE QUI SUIT :<br/>
<br/>
              1. La personne hospitalisée dans un établissement public de santé est un usager d'un service public administratif et le rapport né de cette situation est un rapport de droit public. Par suite, les litiges susceptibles de s'élever entre l'établissement et la personne hospitalisée au sujet du paiement des frais de son hospitalisation relèvent de la juridiction administrative. Il en est de même, sauf si la loi en dispose autrement, des litiges relatifs au paiement des frais d'hospitalisation opposant l'établissement public de santé aux personnes et organismes tenus à ce paiement pour le compte de la personne hospitalisée. Si, aux termes de l'article L. 6145-11 du code de la santé publique : " Les établissements publics de santé peuvent toujours exercer leurs recours, s'il y a lieu, contre les hospitalisés, contre leurs débiteurs et contre les personnes désignées par les articles 205, 206, 207 et 212 du code civil. / Ces recours relèvent de la compétence du juge aux affaires familiales ", ces dispositions ont eu pour seul effet de transférer à la juridiction judiciaire compétence pour connaître des litiges relatifs au paiement des frais d'hospitalisation opposant les établissements publics de santé aux personnes ayant la qualité d'obligés alimentaires en vertu des articles 205, 206 et 207 du code civil ainsi qu'au conjoint, mentionné à l'article 212 du même code. Elles n'ont eu ni pour objet ni pour effet d'édicter de nouvelles règles de compétence relatives aux autres litiges pouvant naître de l'hospitalisation dans des établissements publics de santé. Dès lors, les actions engagées par ces établissements, à la suite du décès de la personne hospitalisée redevable de frais d'hospitalisation, à l'encontre de ses héritiers relèvent de la compétence de la juridiction administrative.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis au juge du fond que le trésorier-payeur général de l'Assistance publique - Hôpitaux de Paris a émis, le 25 août 2005, à l'encontre de MmeA..., un commandement de payer une somme de 17 045,36 euros au titre des frais exposés pour son hospitalisation entre le 1er avril 2003 et le 26 mai 2004 dans l'un de ses établissements. A la suite du décès de MmeA..., intervenu en 2006, le recouvrement de cette somme a été poursuivi auprès de sa fille, MmeB.... En estimant que le litige opposant le trésorier-payeur général de l'Assistance publique - Hôpitaux de Paris à Mme B...se fondait sur sa qualité de débiteur d'aliments de sa mère au titre de l'article 205 du code civil, alors que le recouvrement de la créance de Mme A...avait été engagé, de son vivant, uniquement à l'encontre de cette dernière et que les poursuites engagées par la suite contre Mme B... ne pouvaient l'avoir été qu'en sa qualité d'héritière de la défunte, et en en déduisant la compétence de la juridiction judiciaire, la cour administrative d'appel de Paris a dénaturé les pièces du dossier et commis une erreur de droit. Il s'ensuit que Mme B...est fondée, pour ces motifs, à demander l'annulation de l'ordonnance qu'elle attaque, sans qu'il soit besoin de statuer sur sa demande tendant à ce que les écritures en défense du ministre de l'économie et des finances soient écartées des débats.<br/>
<br/>
              3. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat (direction spécialisée des finances publiques), pour l'Assistance publique - Hôpitaux de Paris, le versement à Mme B...de la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du président de la troisième chambre de la cour administrative d'appel de Paris du 30 mars 2012 est annulée.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : L'Etat (direction spécialisée des finances publiques) versera à MmeB..., pour l'Assistance publique - Hôpitaux de Paris, la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à Mme D...B...et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
