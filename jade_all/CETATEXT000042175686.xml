<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042175686</ID>
<ANCIEN_ID>JG_L_2020_07_000000430891</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/17/56/CETATEXT000042175686.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 29/07/2020, 430891, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430891</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Arno Klarsfeld</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:430891.20200729</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé à la Cour nationale du droit d'asile d'annuler la décision de l'Office français de protection des réfugiés et apatrides (OFPRA) en date du 29 décembre 2017 refusant de lui reconnaître la qualité de réfugié ou à défaut de lui accorder le bénéfice de la protection subsidiaire. <br/>
<br/>
              Par une décision n° 18004037 du 31 janvier 2019, la Cour nationale du droit d'asile a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 mai et 24 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de lui reconnaître la qualité de réfugié ou, à défaut, de lui accorder le bénéfice de la protection subsidiaire ;<br/>
<br/>
              3°) de mettre à la charge de l'OFPRA une somme de 3 000 euros à verser à la SCP Rocheteau, Uzan-Sarano, son avocat, au titre de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New York le 31 janvier 1967 relatif au statut des réfugiés ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arno Klarsfeld, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M. A... B... et à la SCP Foussard, Froger, avocat de l'Office français de protection des réfugiés et apatrides ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B..., de nationalité turque et d'origine kurde, a été condamné, en 2009, dans son pays à sept ans et six mois de prison à raison de son appartenance au parti des travailleurs du Kurdistan (PKK). Lors d'une libération conditionnelle en 2014, il a rejoint la France et y a demandé l'asile en faisant état des risques de persécutions résultant de ses liens avec le PKK et de sa soustraction à son obligation de service militaire. M. B... se pourvoit en cassation contre la décision de la Cour nationale du droit d'asile en date du 31 janvier 2019 rejetant sa demande d'annulation de la décision de l'Office français de protection des réfugiés et apatrides du 29 décembre 2017 lui refusant le statut de réfugié ou le bénéfice de la protection subsidiaire. <br/>
<br/>
              2. Aux termes du 2 du A de l'article 1er de la convention de Genève du 28 juillet 1951, doit être considérée comme réfugié toute personne qui " craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays ". Aux termes du F de l'article 1er de la même convention : " Les dispositions de cette Convention ne seront pas applicables aux personnes dont on aura des raisons sérieuses de penser : (...) b) qu'elles ont commis un crime grave de droit commun en dehors du pays d'accueil avant d'y être admises comme réfugiées ; c) qu'elles se sont rendues coupables d'agissements contraires aux buts et aux principes des Nations Unies ". Si les actes à caractère terroriste peuvent relever du b) du F de l'article 1er précité de la convention de Genève, les actes terroristes ayant une ampleur internationale en termes de gravité, d'impact international et d'implications pour la paix et la sécurité internationales peuvent aussi être assimilés à des agissements contraires aux buts et aux principes des Nations Unies au sens du c) du F du même article. <br/>
<br/>
              3. Il ressort des énonciations de la décision attaquée que la Cour nationale du droit d'asile a jugé qu'il existait des raisons sérieuses de penser que M. B... a eu une part de responsabilité personnelle dans la commission d'actes susceptibles de justifier de son exclusion du bénéfice de la protection tant sur le fondement des stipulations du b) que du c) du F de l'article 1er de la convention de Genève. Après avoir relevé que le PKK était regardé comme une organisation terroriste par l'Union européenne et qu'il avait mené entre 2002 et 2006 des actions terroristes contre les populations civiles du sud-est de la Turquie, la Cour a retenu le fait que M. B... avait été envoyé en Syrie entre 2002 et 2009 par le PKK pour participer aux collectes de fonds auprès des familles kurdes ainsi qu'au recrutement de combattants kurdes tout en indiquant qu'il n'était pas établi que M. B... aurait utilisé des moyens coercitifs dans la collecte de fonds, ni qu'il aurait recruté des mineurs. En jugeant que les agissements de M. B... en Syrie avaient le caractère à la fois de crimes graves de droit commun, au sens du b) du F de l'article 1er de la convention de Genève, et d'actes contraires aux buts et principes des Nations Unies, au sens du c) du F du même article, sans caractériser d'une part, les crimes en cause, et d'autre part, la gravité de ces agissements au regard de leurs effets sur le plan international, la cour a commis une erreur de droit. M. B... est, dès lors, fondé, sans qu'il soit besoin de se prononcer sur les autres moyens de son pourvoi, à demander l'annulation de la décision qu'il attaque.<br/>
<br/>
              4. M. B... a obtenu le bénéfice de l'aide juridictionnelle. Son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Rocheteau, Uzan-Sarano, avocat de M. B..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'OFPRA une somme de 3 000 euros à verser à cette société.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                            --------------<br/>
<br/>
Article 1er : La décision du 29 décembre 2017 de la Cour nationale du droit d'asile est annulée.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la Cour nationale du droit d'asile.<br/>
<br/>
Article 3 : L'Office français de protection des réfugiés et apatrides versera à la SCP Rocheteau, Uzan-Sarano une somme de 3 000 euros au titre des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat. <br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A... B... et à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
