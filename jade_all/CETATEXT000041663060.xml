<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041663060</ID>
<ANCIEN_ID>JG_L_2020_02_000000428441</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/66/30/CETATEXT000041663060.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 28/02/2020, 428441</TITRE>
<DATE_DEC>2020-02-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428441</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LE BRET-DESACHE</AVOCATS>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:428441.20200228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C... A... a demandé au tribunal administratif de Strasbourg d'annuler la décision du 12 juin 2015 par laquelle l'évêque de Metz l'a licencié pour faute ainsi que les décisions implicites de rejet de ses recours gracieux et hiérarchique, d'enjoindre à la mense épiscopale de le réintégrer dans ses fonctions et de supprimer un passage qu'il estimait diffamatoire dans les écritures en défense. Par un jugement n° 1506561 du 15 novembre 2017, le tribunal administratif de Strasbourg a annulé la décision du 12 juin 2015 et les décisions rejetant les recours gracieux et hiérarchique formés par M. A..., enjoint à l'évêque de Metz de procéder à la réintégration et à la reconstitution des droits de M. A... et rejeté le surplus de la demande de M. A....<br/>
<br/>
              Par un arrêt n°s 17NC03112, 17NC03113 du 27 décembre 2018, la cour administrative d'appel de Nancy a rejeté l'appel formé par l'évêque de Metz contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et deux mémoires complémentaires, enregistrés les 26 février, 21 mai et 3 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, M. D... B..., évêque de Metz, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de M. A... la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention du 26 messidor an IX et ses articles organiques ;<br/>
              - la loi du 18 germinal an X ;<br/>
              - la loi du 1er juin 1924 mettant en vigueur la législation civile française dans les départements du Bas-Rhin, du Haut-Rhin et de la Moselle ;<br/>
              - le décret n° 86-83 du 17 janvier 1986 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Le Bret-Desaché, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A... a été recruté le 7 juin 2010 par la mense épiscopale du diocèse de Metz, dans le cadre d'un contrat à durée indéterminée, en qualité de responsable de la mission informatique et nouvelles technologies de l'évêché de Metz puis qu'il a exercé les mêmes fonctions en étant affecté à compter du 1er novembre 2010 sur un poste de secrétaire des cultes et rémunéré par l'Etat. Par une décision du 12 juin 2015, l'évêque de Metz l'a licencié pour faute. Par un jugement du 15 novembre 2017, le tribunal administratif de Strasbourg a annulé cette décision, ainsi que les décisions implicites de rejet des recours gracieux et hiérarchique formés par M. A... et a enjoint à la mense épiscopale de procéder à la réintégration et à la reconstitution des droits de M. A.... L'évêque de Metz se pourvoit en cassation contre l'arrêt du 27 décembre 2018 par lequel la cour administrative d'appel de Nancy a rejeté l'appel qu'il avait formé contre ce jugement.<br/>
<br/>
              2. Le décret du 17 janvier 1986 relatif aux dispositions applicables aux agents non titulaires de l'Etat pris pour l'application de l'article 7 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat pose les règles applicables aux agents non titulaires de l'Etat et de ses établissements publics à caractère administratif. Ainsi que l'a jugé le Conseil d'Etat statuant au contentieux par sa décision n° 383412 du 22 juillet 2016, la mense épiscopale de Metz, qui a le statut d'établissement public du culte, doit être regardée, pour l'application de ce décret, comme un établissement public de l'Etat à caractère administratif. Il en résulte que les agents publics de la mense épiscopale sont régis par ses dispositions et que le pouvoir disciplinaire de l'évêque s'exerce dans le cadre qu'elles définissent.<br/>
<br/>
              3. Aux termes de l'article 1-2 du décret du 17 janvier 1986 : " Dans toutes les administrations de l'Etat et dans tous les établissements publics de l'Etat, il est institué, par arrêté du ministre intéressé ou par décision de l'autorité compétente de l'établissement public, une ou plusieurs commissions consultatives paritaires comprenant en nombre égal des représentants de l'administration et des représentants des personnels mentionnés à l'article 1er. / Lorsque les effectifs d'agents contractuels d'un établissement sont insuffisants pour permettre la constitution d'une commission consultative paritaire en son sein, la situation des personnels concernés est examinée par une commission consultative paritaire du département ministériel correspondant désignée par arrêté du ministre intéressé. / Ces commissions sont obligatoirement consultées sur les décisions individuelles relatives aux licenciements intervenant postérieurement à la période d'essai au non-renouvellement du contrat des personnes investies d'un mandat syndical et aux sanctions disciplinaires autres que l'avertissement et le blâme (...) ".<br/>
<br/>
              4. Il ressort des pièces du dossier qu'à la date du licenciement de M. A..., intervenu le 12 juin 2015, les personnels des menses épiscopales n'étaient pas, en l'absence de décision du Conseil d'Etat ayant clarifié les règles juridiques applicables aux personnels administratifs des cultes dans les départements du Haut-Rhin, du Bas-Rhin et de la Moselle, alors que les juridictions du fond avaient pris sur ce point des positions différentes, considérés comme étant soumis au décret du 17 janvier 1986 relatif aux agents non titulaires de l'Etat, qui ne vise d'établissements publics que ceux de l'Etat. En conséquence, aucune commission consultative paritaire compétente pour ces établissements n'était alors constituée. Eu égard à ces circonstances particulières, qui, en l'espèce, rendaient alors impossible la mise en oeuvre de la procédure prévue à l'article 1-2 du décret du 17 janvier 1986 cité au point 3, la cour administrative d'appel de Nancy a, en estimant que la consultation de la commission consultative paritaire prévue par ces dispositions ne constituait pas une formalité impossible, dénaturé les faits qui lui étaient soumis. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que son arrêt doit être annulé.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A... la somme de 3 000 euros à verser à l'évêque de Metz, au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 27 décembre 2018 de la cour administrative d'appel de Nancy est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy.<br/>
Article 3 : M. A... versera la somme de 3 000 euros à l'évêque de Metz, au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. D... B..., évêque de Metz, et à M. C... A.... <br/>
Copie en sera adressée au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-02-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. PROCÉDURE CONSULTATIVE. QUESTIONS GÉNÉRALES. - AGENTS DES MENSES ÉPISCOPALES - AGENTS PUBLICS RÉGIS PAR LE DÉCRET N° 86-83 DU 17 JANVIER 1986, AINSI QUE L'A JUGÉ LE CONSEIL D'ETAT PAR UNE DÉCISION DU 22 JUILLET 2016 [RJ1] - CONSÉQUENCES - 1) CONSULTATION OBLIGATOIRE DE LA COMMISSION CONSULTATIVE PARITAIRE SUR LES DÉCISIONS DE LICENCIEMENT (ART. 1-2 DU DÉCRET) - 2) TEMPÉRAMENT, EN L'ESPÈCE - LICENCIEMENT D'UN AGENT INTERVENU AVANT CETTE DÉCISION DU CONSEIL D'ETAT - CONSULTATION DE LA COMMISSION - FORMALITÉ IMPOSSIBLE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">06-04 ALSACE-MOSELLE. ENSEIGNEMENT ET CULTES. - AGENTS DES MENSES ÉPISCOPALES - AGENTS PUBLICS RÉGIS PAR LE DÉCRET N° 86-83 DU 17 JANVIER 1986, AINSI QUE L'A JUGÉ LE CONSEIL D'ETAT PAR UNE DÉCISION DU 22 JUILLET 2016 [RJ1] - CONSÉQUENCES - 1) CONSULTATION OBLIGATOIRE DE LA COMMISSION CONSULTATIVE PARITAIRE SUR LES DÉCISIONS DE LICENCIEMENT (ART. 1-2 DU DÉCRET) - 2) TEMPÉRAMENT, EN L'ESPÈCE - LICENCIEMENT D'UN AGENT INTERVENU AVANT CETTE DÉCISION DU CONSEIL D'ETAT - CONSULTATION DE LA COMMISSION - FORMALITÉ IMPOSSIBLE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">21-04 CULTES. RÉGIME CONCORDATAIRE D'ALSACE-MOSELLE. - AGENTS DES MENSES ÉPISCOPALES - AGENTS PUBLICS RÉGIS PAR LE DÉCRET N° 86-83 DU 17 JANVIER 1986, AINSI QUE L'A JUGÉ LE CONSEIL D'ETAT PAR UNE DÉCISION DU 22 JUILLET 2016 [RJ1] - CONSÉQUENCES - 1) CONSULTATION OBLIGATOIRE DE LA COMMISSION CONSULTATIVE PARITAIRE SUR LES DÉCISIONS DE LICENCIEMENT (ART. 1-2 DU DÉCRET) - 2) TEMPÉRAMENT, EN L'ESPÈCE - LICENCIEMENT D'UN AGENT INTERVENU AVANT CETTE DÉCISION DU CONSEIL D'ETAT - CONSULTATION DE LA COMMISSION - FORMALITÉ IMPOSSIBLE.
</SCT>
<ANA ID="9A"> 01-03-02-01 1) Le décret n° 86-83 du 17 janvier 1986 pose les règles applicables aux agents non titulaires de l'Etat et de ses établissements publics à caractère administratif. Ainsi que l'a jugé le Conseil d'Etat statuant au contentieux par sa décision n° 383412 du 22 juillet 2016, la mense épiscopale de Metz, qui a le statut d'établissement public du culte, doit être regardée, pour l'application de ce décret, comme un établissement public de l'Etat à caractère administratif. Il en résulte que les agents publics de la mense épiscopale sont régis par ses dispositions et que le pouvoir disciplinaire de l'évêque s'exerce dans le cadre qu'elles définissent.,,,2) Agent de la mense épiscopale du diocèse de Metz, licencié pour faute par une décision du 12 juin 2015 de l'évêque de Metz.,,,A la date de ce licenciement, les personnels des menses épiscopales n'étaient pas, en l'absence de décision du Conseil d'Etat ayant clarifié les règles juridiques applicables aux personnels administratifs des cultes dans les départements du Haut-Rhin, du Bas-Rhin et de la Moselle, et alors que les juridictions du fond avaient pris sur ce point des positions différentes, considérés comme étant soumis au décret du 17 janvier 1986, qui ne vise d'établissements publics que ceux de l'Etat. En conséquence, aucune commission consultative paritaire compétente pour ces établissements n'était alors constituée. Eu égard à ces circonstances particulières, qui, en l'espèce, rendaient alors impossible la mise en oeuvre de la procédure prévue à l'article 1-2 du décret du 17 janvier 1986, la consultation de la commission consultative paritaire prévue par ces dispositions constituait une formalité impossible.</ANA>
<ANA ID="9B"> 06-04 1) Le décret n° 86-83 du 17 janvier 1986 pose les règles applicables aux agents non titulaires de l'Etat et de ses établissements publics à caractère administratif. Ainsi que l'a jugé le Conseil d'Etat statuant au contentieux par sa décision n° 383412 du 22 juillet 2016, la mense épiscopale de Metz, qui a le statut d'établissement public du culte, doit être regardée, pour l'application de ce décret, comme un établissement public de l'Etat à caractère administratif. Il en résulte que les agents publics de la mense épiscopale sont régis par ses dispositions et que le pouvoir disciplinaire de l'évêque s'exerce dans le cadre qu'elles définissent.,,,2) Agent de la mense épiscopale du diocèse de Metz, licencié pour faute par une décision du 12 juin 2015 de l'évêque de Metz.,,,A la date de ce licenciement, les personnels des menses épiscopales n'étaient pas, en l'absence de décision du Conseil d'Etat ayant clarifié les règles juridiques applicables aux personnels administratifs des cultes dans les départements du Haut-Rhin, du Bas-Rhin et de la Moselle, et alors que les juridictions du fond avaient pris sur ce point des positions différentes, considérés comme étant soumis au décret du 17 janvier 1986, qui ne vise d'établissements publics que ceux de l'Etat. En conséquence, aucune commission consultative paritaire compétente pour ces établissements n'était alors constituée. Eu égard à ces circonstances particulières, qui, en l'espèce, rendaient alors impossible la mise en oeuvre de la procédure prévue à l'article 1-2 du décret du 17 janvier 1986, la consultation de la commission consultative paritaire prévue par ces dispositions constituait une formalité impossible.</ANA>
<ANA ID="9C"> 21-04 1) Le décret n° 86-83 du 17 janvier 1986 pose les règles applicables aux agents non titulaires de l'Etat et de ses établissements publics à caractère administratif. Ainsi que l'a jugé le Conseil d'Etat statuant au contentieux par sa décision n° 383412 du 22 juillet 2016, la mense épiscopale de Metz, qui a le statut d'établissement public du culte, doit être regardée, pour l'application de ce décret, comme un établissement public de l'Etat à caractère administratif. Il en résulte que les agents publics de la mense épiscopale sont régis par ses dispositions et que le pouvoir disciplinaire de l'évêque s'exerce dans le cadre qu'elles définissent.,,,2) Agent de la mense épiscopale du diocèse de Metz, licencié pour faute par une décision du 12 juin 2015 de l'évêque de Metz.,,,A la date de ce licenciement, les personnels des menses épiscopales n'étaient pas, en l'absence de décision du Conseil d'Etat ayant clarifié les règles juridiques applicables aux personnels administratifs des cultes dans les départements du Haut-Rhin, du Bas-Rhin et de la Moselle, et alors que les juridictions du fond avaient pris sur ce point des positions différentes, considérés comme étant soumis au décret du 17 janvier 1986, qui ne vise d'établissements publics que ceux de l'Etat. En conséquence, aucune commission consultative paritaire compétente pour ces établissements n'était alors constituée. Eu égard à ces circonstances particulières, qui, en l'espèce, rendaient alors impossible la mise en oeuvre de la procédure prévue à l'article 1-2 du décret du 17 janvier 1986, la consultation de la commission consultative paritaire prévue par ces dispositions constituait une formalité impossible.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 22 juillet 2016, M.,, n° 383412, T. pp. 641-749-809.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
