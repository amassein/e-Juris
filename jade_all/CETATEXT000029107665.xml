<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029107665</ID>
<ANCIEN_ID>JG_L_2014_06_000000367725</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/10/76/CETATEXT000029107665.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème / 7ème SSR, 18/06/2014, 367725</TITRE>
<DATE_DEC>2014-06-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367725</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème / 7ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>Mme Bénédicte Vassallo-Pasquet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:367725.20140618</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 12 avril et 11 juillet 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme A...D..., domiciliée..., et M. B...C..., domicilié ...; Mme D...et M. C...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 11013588 et 11013741 du 30 août 2012 par laquelle la Cour nationale du droit d'asile a rejeté la requête de Mme D...tendant à l'annulation de la décision du 6 mai 2011 par laquelle le directeur général de l'Office français de protection des réfugiés et apatrides a rejeté sa demande d'asile et lui a refusé le bénéfice de la protection subsidiaire ainsi que la requête de M. C...tendant à l'annulation de la décision du 9 mai 2011 par laquelle le directeur général de l'Office français de protection des réfugiés et apatrides a rejeté sa demande d'asile et lui a refusé le bénéfice de la protection subsidiaire ;<br/>
<br/>
              2°) de renvoyer l'affaire devant la Cour nationale du droit d'asile ;<br/>
<br/>
              3°) de mettre à la charge de l'Office français de protection des réfugiés et apatrides la somme de 2 500 euros à verser à la SCP Hélène Dider et François Pinet, leur avocat, en application des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention de Genève du 28 juillet 1951 et le protocole signé à New-York le 31 janvier 1967 ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le décret n° 91-1266 du 19 décembre 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Bénédicte Vassallo-Pasquet, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de Mme D...et de M. C...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant, d'une part, qu'aux termes du premier alinéa de l'article L. 733-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Les intéressés peuvent présenter leurs explications à la Cour nationale du droit d'asile et s'y faire assister d'un conseil et d'un interprète " ; que ces dispositions imposent à la Cour nationale du droit d'asile de mettre les intéressés à même d'exercer la faculté qui leur est reconnue ;<br/>
<br/>
              2. Considérant, d'autre part, que le juge, auquel il incombe de veiller à la bonne administration de la justice, n'a aucune obligation, hormis le cas où des motifs exceptionnels tirés des exigences du débat contradictoire l'imposeraient, de faire droit à une demande de report de l'audience formulée par une partie et qu'il n'a pas à motiver le refus qu'il oppose à une telle demande ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la Cour nationale du droit d'asile a mis en place, à la fin de l'année 2011, un dispositif permettant notamment aux avocats " d'indiquer, s'ils le souhaitent, pour toute la durée de l'année civile, un jour fixe hebdomadaire pendant lequel ils ne seront pas convoqués devant les formations de jugement " ; que ce dispositif a été élaboré en concertation avec les représentants du Conseil national des barreaux et des barreaux concernés, dans le but d'améliorer l'inscription au rôle des affaires et de mieux organiser la convocation des avocats à l'audience ; que le 30 décembre 2011, Maître Tassev, avocat désigné par les requérants pour les assister, a précisé qu'il souhaitait que le jour fixe hebdomadaire sans audience soit fixé, en ce qui le concerne, au vendredi durant toute la journée ; que, dès qu'il a eu connaissance, le 4 juillet 2012, de ce que la date d'audience concernant les requérants avait été fixée au vendredi 13 juillet 2012, Maître Tassev a fait parvenir une télécopie à la présidente de la Cour nationale du droit d'asile lui demandant de renvoyer l'affaire à une date ultérieure, l'audience ayant été fixée un vendredi, jour de son indisponibilité hebdomadaire ; que, par télécopie du 12 juillet 2012, il a réitéré sa demande de renvoi et précisé que ses clients, résidant dans l'Isère, ne pourraient pas se déplacer ;<br/>
<br/>
              4. Considérant que les requérants, qui ont été privés de la possibilité d'être assistés ou représentés par leur avocat lors de l'audience du fait de la méconnaissance par la Cour, sans aucun motif tiré notamment d'une bonne administration de la justice, des règles qu'elle avait elle-même fixées en ce qui concerne la détermination du jour des audiences, et alors que leur avocat avait formulé une demande de report d'audience qui n'avait pas de caractère dilatoire et qui avait été présentée en temps utile, font, en l'espèce, état de motifs tirés des exigences du débat contradictoire qui imposaient, à titre exceptionnel, qu'il soit fait droit à cette demande de report ; qu'ils sont par suite, fondés à soutenir que la procédure suivie devant la Cour a été irrégulière ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la décision attaquée doit être annulée ;<br/>
<br/>
              6. Considérant que M. C...et Mme D...ont obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, leur avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Didier-Pinet renonce à percevoir la part contributive de l'Etat, de mettre à la charge de l'Office français de protection des réfugiés et apatrides la somme de 2 500 euros à verser à la SCP Hélène Didier et François Pinet ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La décision de la Cour nationale du droit d'asile du 30 août 2012 est annulée.<br/>
<br/>
Article 2 : L'affaire est renvoyée devant la Cour nationale du droit d'asile.<br/>
<br/>
Article 3 : L'Office français de protection des réfugiés et apatrides versera la somme de 2 500 euros à la SCP Didier-Pinet, en application des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve qu'elle renonce à percevoir la somme correspondant à la part contributive de l'Etat. <br/>
<br/>
Article 4 : La présente décision sera notifiée à M. B...C..., à Mme A...D...et au directeur général de l'Office français de protection des réfugiés et apatrides.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-08-04-03 - DEMANDE DE REPORT D'AUDIENCE - 1) OBLIGATION POUR LE JUGE D'Y FAIRE DROIT - ABSENCE - EXCEPTION, EN CAS DE MOTIF EXCEPTIONNEL TIRÉ DES EXIGENCES DU DÉBAT CONTRADICTOIRE [RJ1] - 2) ESPÈCE - MOTIF EXCEPTIONNEL - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-02 PROCÉDURE. JUGEMENTS. TENUE DES AUDIENCES. - DEMANDE DE REPORT D'AUDIENCE DEVANT LA CNDA - 1) OBLIGATION POUR LE JUGE D'Y FAIRE DROIT - ABSENCE - EXCEPTION, EN CAS DE MOTIF EXCEPTIONNEL TIRÉ DES EXIGENCES DU DÉBAT CONTRADICTOIRE [RJ1] - 2) ESPÈCE - MOTIF EXCEPTIONNEL - EXISTENCE.
</SCT>
<ANA ID="9A"> 095-08-04-03 1) La Cour nationale du droit d'asile (CNDA), à laquelle il incombe de veiller à la bonne administration de la justice, n'a aucune obligation, hormis le cas où des motifs exceptionnels tirés des exigences du débat contradictoire l'imposeraient, de faire droit à une demande de report de l'audience formulée par une partie.,,	...2) En l'espèce, le refus de report d'audience devant la CNDA est intervenu dans un contexte marqué par la mise en place, fin 2011, d'un dispositif permettant notamment aux avocats  d'indiquer, s'ils le souhaitent, pour toute la durée de l'année civile, un jour fixe hebdomadaire pendant lequel ils ne seront pas convoqués devant les formations de jugement . Ce dispositif a été élaboré en concertation avec les représentants du Conseil national des barreaux et des barreaux concernés, dans le but d'améliorer l'inscription au rôle des affaires et de mieux organiser la convocation des avocats à l'audience. Dans ce cadre, l'avocat désigné par les requérants pour les assister avait indiqué qu'il souhaitait que le jour hebdomadaire sans audience soit fixé le vendredi durant toute la journée. Dès qu'il eut connaissance, dix jours avant l'audience, de ce que cette dernière était fixée un vendredi, l'avocat a saisi par télécopie la présidente de la Cour d'une demande de report d'audience. Par une télécopie envoyée la veille de la date prévue pour l'audience, l'avocat a réitéré sa demande de renvoi et précisé que ses clients ne pouvaient pas se déplacer.,,,Les intéressés ont été privés de la possibilité d'être assistés ou représentés par leur avocat lors de l'audience du fait de la méconnaissance par la Cour, sans aucun motif tiré notamment d'une bonne administration de la justice, des règles qu'elle avait elle-même fixées en ce qui concerne la détermination du jour des audiences, et alors que leur avocat avait formulé une demande de report d'audience qui n'avait pas de caractère dilatoire et qui avait été présentée en temps utile. Ces circonstances sont constitutives de motifs tirés des exigences du débat contradictoire qui imposaient, à titre exceptionnel, qu'il soit fait droit à cette demande de report. La procédure suivie devant la Cour a, par suite, été irrégulière.</ANA>
<ANA ID="9B"> 54-06-02 1) La Cour nationale du droit d'asile (CNDA), à laquelle il incombe de veiller à la bonne administration de la justice, n'a aucune obligation, hormis le cas où des motifs exceptionnels tirés des exigences du débat contradictoire l'imposeraient, de faire droit à une demande de report de l'audience formulée par une partie.,,	...2) En l'espèce, le refus de report d'audience devant la CNDA est intervenu dans un contexte marqué par la mise en place, fin 2011, d'un dispositif permettant notamment aux avocats  d'indiquer, s'ils le souhaitent, pour toute la durée de l'année civile, un jour fixe hebdomadaire pendant lequel ils ne seront pas convoqués devant les formations de jugement . Ce dispositif a été élaboré en concertation avec les représentants du Conseil national des barreaux et des barreaux concernés, dans le but d'améliorer l'inscription au rôle des affaires et de mieux organiser la convocation des avocats à l'audience. Dans ce cadre, l'avocat désigné par les requérants pour les assister avait indiqué qu'il souhaitait que le jour fixe hebdomadaire sans audience soit fixé le vendredi durant toute la journée. Dès qu'il eut connaissance, dix jours avant l'audience, de ce que cette dernière était fixée un vendredi, l'avocat a saisi par télécopie la présidente de la Cour d'une demande de report d'audience. Par une télécopie envoyée la veille de la date prévue pour l'audience, l'avocat a réitéré sa demande de renvoi et précisé que ses clients ne pouvaient pas se déplacer.,,,Les intéressés ont été privés de la possibilité d'être assistés ou représentés par leur avocat lors de l'audience du fait de la méconnaissance par la Cour, sans aucun motif tiré notamment d'une bonne administration de la justice, des règles qu'elle avait elle-même fixées en ce qui concerne la détermination du jour des audiences, et alors que leur avocat avait formulé une demande de report d'audience qui n'avait pas de caractère dilatoire et qui avait été présentée en temps utile. Ces circonstances sont constitutives de motifs tirés des exigences du débat contradictoire qui imposaient, à titre exceptionnel, qu'il soit fait droit à cette demande de report. La procédure suivie devant la Cour a, par suite, été irrégulière.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 16 juillet 2010, Colomb, n° 294239, p. 298. Ab. jur. CE, 28 octobre 1987, Heng Ho, n° 76539, p. 331 (obligation de ne faire droit à une demande de report d'audience qu'en cas de force majeure).</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
