<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043486331</ID>
<ANCIEN_ID>JG_L_2021_05_000000432620</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/48/63/CETATEXT000043486331.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 06/05/2021, 432620</TITRE>
<DATE_DEC>2021-05-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432620</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>Mme Pearl Nguyên Duy</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:432620.20210506</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir la décision du 28 avril 2016 par laquelle la ministre des affaires sociales et de la santé a subordonné la délivrance d'une autorisation d'exercer la profession de pharmacien à la réalisation de mesures compensatoires. Par un jugement n° 1610297 du 18 janvier 2018, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 18PA00962 du 13 mai 2019, la cour administrative d'appel de Paris a rejeté l'appel formé par Mme A... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 15 juillet et 15 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu : <br/>
<br/>
              - la directive 2005/36/CE du 7 septembre 2005 ;<br/>
              - le code de la santé publique ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pearl Nguyên Duy, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de Mme A....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'après avoir obtenu en 1996 un titre de docteur en biochimie et pharmacie délivré en Equateur, puis l'avoir fait reconnaître en 2013 par les autorités espagnoles comme équivalent au diplôme universitaire espagnol en pharmacie, Mme A..., ressortissante de nationalité française, a demandé à être autorisée, sur le fondement des dispositions de l'article L. 4221-14-2 du code de la santé publique, à exercer en France la profession de pharmacien. Par une décision du 28 avril 2016, la ministre des affaires sociales et de la santé a subordonné cette autorisation à la réalisation par Mme A... d'une " mesure de compensation " consistant, au choix, en une épreuve d'aptitude portant sur la totalité du cursus de formation de la pharmacie clinique en France ou en un stage d'adaptation de 24 mois de fonctions hospitalières rémunérées dans le cadre d'un statut d'associé au sein d'un service agréé pour la formation des internes en pharmacie. Mme A... se pourvoit en cassation contre l'arrêt du 13 mai 2019 par lequel la cour administrative d'appel de Paris a rejeté son appel formé contre le jugement du 18 janvier 2018 du tribunal administratif de Paris rejetant sa demande d'annulation de cette décision.<br/>
<br/>
              2. Aux termes de l'article L. 4221-1 du code de la santé publique : " Nul ne peut exercer la profession de pharmacien s'il n'offre toutes garanties de moralité professionnelle et s'il ne réunit les conditions suivantes : / 1° Etre titulaire d'un diplôme, certificat ou autre titre mentionnés aux articles L. 4221-2 à L. 4221-5 (...) ". Aux termes de l'article L. 4221-2 du même code : " Sous réserve des dispositions des articles L. 4221-4 et L. 4221-5, les diplômes, certificats ou autres titres mentionnés au 1° de l'article L. 4221-1 sont le diplôme français d'Etat de docteur en pharmacie ou de pharmacien ". Aux termes de l'article L. 4221-4 de ce code : " Ouvre droit à l'exercice de la profession de pharmacien aux ressortissants d'un Etat membre de l'Union européenne ou d'un autre Etat partie à l'accord sur l'Espace économique européen :  / 1° Un titre de formation de pharmacien délivré par l'un de ces Etats conformément aux obligations communautaires et figurant sur une liste établie par arrêté des ministres chargés de l'enseignement supérieur et de la santé ; (...) ". Enfin, aux termes de l'article L. 4221-14-2 du même code, dans sa rédaction alors applicable : " L'autorité compétente peut également, après avis d'une commission, composée notamment de professionnels, autoriser individuellement, le cas échéant, dans la spécialité à exercer la profession de pharmacien les ressortissants d'un Etat membre de l'Union européenne ou d'un autre Etat partie à l'accord sur l'Espace économique européen, titulaires d'un titre de formation délivré par un Etat tiers, et reconnu dans un Etat, membre ou partie, autre que la France, permettant d'y exercer légalement la profession. Dans le cas où l'examen des qualifications professionnelles attestées par l'ensemble des titres de formation et de l'expérience professionnelle pertinente fait apparaître des différences substantielles au regard des qualifications requises pour l'accès à la profession et son exercice en France, l'autorité compétente exige que l'intéressé se soumette à une mesure de compensation qui consiste, au choix du demandeur, en une épreuve d'aptitude ou en un stage d'adaptation ". En vertu de l'article R. 4221-13-5 du même code dans sa rédaction alors applicable, l'autorité compétente au sens de ces dernières dispositions est le ministre chargé de la santé. <br/>
<br/>
              3. En premier lieu, en jugeant, ainsi qu'il résulte des termes de son arrêt, que le titre équatorien de docteur en biochimie et pharmacie de Mme A... ne pouvait, alors même qu'elle avait obtenu en Espagne une équivalence lui autorisant l'exercice de la profession de pharmacien dans cet Etat, être regardé comme un titre délivré par l'un des Etats membres de l'Union européenne au sens des dispositions du 1° de l'article L. 4221-4 du code de la santé publique, pour en déduire que sa demande devait être examinée sur le fondement des dispositions de l'article L. 4221-14-2 du même code, la cour administrative d'appel a exactement qualifié les faits qui lui étaient soumis et n'a pas commis d'erreur de droit. <br/>
<br/>
              4. En deuxième lieu, l'autorité compétente, saisie d'une demande d'autorisation d'exercice présentée sur le fondement des dispositions de l'article L. 4221-14-2 du code de la santé publique, dispose d'un large pouvoir d'appréciation lorsqu'elle estime, au vu de l'avis de la commission d'autorisation d'exercice mentionnée à l'article D. 4221-2 du même code, que les qualifications professionnelles d'un ressortissant d'un Etat membre de l'Union européenne ou d'un Etat partie à l'accord sur l'Espace économique européen, titulaire d'un titre de pharmacien délivré par un Etat tiers et reconnu dans un Etat, membre ou partie, autre que la France, comme permettant d'y exercer la profession de pharmacien, présentent, au regard des qualifications requises pour l'accès à la profession de pharmacien et son exercice en France, des différences substantielles justifiant que l'intéressé soit soumis à une mesure de compensation. La décision par laquelle cette autorité exige, le cas échéant, que l'intéressé se soumette à une mesure de compensation peut être censurée par le juge de l'excès de pouvoir en cas d'erreur de fait ou de droit, d'erreur manifeste d'appréciation ou de détournement de pouvoir.<br/>
<br/>
              5. En jugeant, ainsi qu'il ressort des termes de son arrêt, que la ministre des affaires sociales et de la santé n'avait pas commis d'erreur manifeste d'appréciation en estimant que les qualifications professionnelles de Mme A... présentaient des différences substantielles avec les qualifications requises pour l'accès à la profession de pharmacien en France, en raison de ce que l'écart entre sa formation initiale et le contenu des enseignements exigés pour obtenir le diplôme français de docteur en pharmacie n'était pas compensé par son expérience professionnelle essentiellement acquise en biologie médicale, la cour, qui n'a pas méconnu son office, a porté sur les pièces du dossier qui lui était soumis une appréciation souveraine, exempte de dénaturation et n'a pas commis d'erreur de droit.<br/>
<br/>
              6. Enfin, la cour ne s'est pas méprise sur la portée des écritures de Mme A... en estimant que le moyen tiré de ce que la décision attaquée méconnaissait les articles 49 et 50 du traité sur le fonctionnement de l'Union européenne et la directive 2005/36/CE du 7 septembre 2005 était dépourvu des précisions nécessaires pour en apprécier le bien-fondé.<br/>
<br/>
              7. Il résulte de tout ce qui précède que Mme A... n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. Son pourvoi doit, par suite, être rejeté, y compris, par voie de conséquence, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			-------------<br/>
<br/>
		Article 1er : Le pourvoi de Mme A... est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme B... A... et au ministre des solidarités et de la santé. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">15-05-01-01-05 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. LIBERTÉS DE CIRCULATION. LIBRE CIRCULATION DES PERSONNES. - AUTORISATION D'EXERCICE DES PHARMACIENS DIPLÔMÉS D'UN ETAT TIERS (ART. L. 4221-14-2 DU CSP) - 1) CHAMP D'APPLICATION - INCLUSION - DIPLÔMÉ D'UN TEL ETAT, NONOBSTANT LA CIRCONSTANCE QU'IL AURAIT OBTENU UNE ÉQUIVALENCE DANS UN ETAT MEMBRE DE L'U.E. [RJ1] - 2) CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR SUR LA DÉCISION DE SOUMETTRE CETTE AUTORISATION À L'ACCOMPLISSEMENT D'UNE MESURE DE COMPENSATION - CONTRÔLE RESTREINT [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-02-04 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE RESTREINT. - DÉCISION DE SOUMETTRE L'AUTORISATION D'EXERCICE ACCORDÉE À UN PHARMACIEN DIPLÔMÉ D'UN ETAT TIERS À L'ACCOMPLISSEMENT D'UNE MESURE DE COMPENSATION (ART. L. 4221-14-2 DU CSP) [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">55-02-04 PROFESSIONS, CHARGES ET OFFICES. ACCÈS AUX PROFESSIONS. PHARMACIENS. - AUTORISATION D'EXERCICE DES PHARMACIENS DIPLÔMÉS D'UN ETAT TIERS (ART. L. 4221-14-2 DU CSP) - 1) CHAMP D'APPLICATION - INCLUSION - DIPLÔMÉ D'UN TEL ETAT, NONOBSTANT LA CIRCONSTANCE QU'IL AURAIT OBTENU UNE ÉQUIVALENCE DANS UN ETAT MEMBRE DE L'U.E. [RJ1] - 2) CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR SUR LA DÉCISION DE SOUMETTRE CETTE AUTORISATION À L'ACCOMPLISSEMENT D'UNE MESURE DE COMPENSATION - CONTRÔLE RESTREINT [RJ2].
</SCT>
<ANA ID="9A"> 15-05-01-01-05 1) Le titre de docteur en biochimie et pharmacie obtenu dans un Etat tiers ne peut, alors même que l'intéressé a obtenu sur son fondement, dans un Etat membre de l'Union européenne (U.E.), une équivalence lui autorisant l'exercice de la profession de pharmacien dans cet Etat, être regardé comme un titre délivré par l'un des Etats membres de l'U.E. au sens du 1° de l'article L. 4221-4 du code de la santé publique (CSP).,,,Sa demande d'être autorisé à exercer en France la profession de pharmacien doit donc être examinée par l'autorité compétente sur le fondement de l'article L. 4221-14-2 du même code, le cas échéant en soumettant l'autorisation à la condition que l'intéressé se soumette à une mesure de compensation.,,,2) Le juge de l'excès de pouvoir exerce un contrôle restreint à l'erreur manifeste d'appréciation sur la décision par laquelle cette autorité exige, le cas échéant, que l'intéressé se soumette à une mesure de compensation.</ANA>
<ANA ID="9B"> 54-07-02-04 Le juge de l'excès de pouvoir exerce un contrôle restreint à l'erreur manifeste d'appréciation sur la décision par laquelle l'autorité compétente pour autoriser le diplômé d'un Etat tiers à l'Union européenne à exercer en France la profession de pharmacien, sur le fondement de l'article L. 4221-14-2 du code de santé publique (CSP) exige, le cas échéant, que l'intéressé se soumette à une mesure de compensation.</ANA>
<ANA ID="9C"> 55-02-04 1) Le titre de docteur en biochimie et pharmacie obtenu dans un Etat tiers ne peut, alors même que l'intéressé a obtenu sur son fondement, dans un Etat membre de l'Union européenne (U.E.), une équivalence lui autorisant l'exercice de la profession de pharmacien dans cet Etat, être regardé comme un titre délivré par l'un des Etats membres de l'U.E. au sens du 1° de l'article L. 4221-4 du code de la santé publique (CSP).,,,Sa demande d'être autorisé à exercer en France la profession de pharmacien doit donc être examinée par l'autorité compétente sur le fondement de l'article L. 4221-14-2 du même code, le cas échéant en soumettant l'autorisation à la condition que l'intéressé se soumette à une mesure de compensation.,,,2) Le juge de l'excès de pouvoir exerce un contrôle restreint à l'erreur manifeste d'appréciation sur la décision par laquelle cette autorité exige, le cas échéant, que l'intéressé se soumette à une mesure de compensation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 7 avril 1995, Mlle,, n° 151912, T. pp. 705-707-708-1012. Rappr., s'agissant d'un médecin, CE, 25 février 1991, Mme,, n° 106799, p. 66.,,[RJ2] Rappr., s'agissant du degré de contrôle sur un refus d'autorisation, CE, 28 janvier 1991, Ministre de la solidarité, de la santé et de la protection sociale c/,, n° 108812, p. 32 ; s'agissant de l'autorisation d'exercice de la médecine, CE, 25 avril 1994, Mme,, n° 137918, T. pp. 1142-1157.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
