<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034940733</ID>
<ANCIEN_ID>JG_L_2017_06_000000400010</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/94/07/CETATEXT000034940733.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 14/06/2017, 400010, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400010</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Emmanuelle Petitdemange</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:400010.20170614</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société coopérative agricole (SCA) Cave Les vins de Saint-Saturnin a demandé au tribunal administratif de Montpellier de la décharger de la cotisation de taxe foncière sur les propriétés bâties à laquelle elle a été assujettie au titre de l'année 2014 à raison d'un bâtiment dont elle est propriétaire dans la commune de Saint-Saturnin-de-Lucian (Hérault). Par un jugement n° 1405725 du 21 mars 2016, ce tribunal a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 mai et 3 août  2016 au secrétariat du contentieux du Conseil d'Etat, la société Cave Les vins de Saint-Saturnin demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Emmanuelle Petitdemange, auditeur,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'une vérification de comptabilité, l'administration fiscale a remis en cause l'exonération de taxe foncière sur les propriétés bâties dont bénéficiait, sur le fondement des dispositions du 6° de l'article 1382 du code général des impôts, la société coopérative agricole Cave les vins de Saint-Saturnin à raison de l'établissement qu'elle exploite sur le territoire de la commune de Saint-Saturnin-de-Lucian (Hérault) pour son activité d'embouteillage, de conditionnement et de stockage de vins. La société a demandé au tribunal administratif de Montpellier de prononcer la décharge de la cotisation de taxe foncière sur les propriétés bâties à laquelle elle a été assujettie au titre de l'année 2014. Elle se pourvoit en cassation contre le jugement par lequel le tribunal administratif a rejeté sa demande.<br/>
<br/>
              2. Aux termes de l'article 1382 du code général des impôts : " Sont exonérés de la taxe foncière sur les propriétés bâties : / (...) 6° a. Les bâtiments qui servent aux exploitations rurales tels que granges, écuries, greniers, caves, celliers, pressoirs et autres, destinés, soit à loger les bestiaux des fermes et métairies ainsi que le gardien de ces bestiaux, soit à serrer les récoltes. / (...) b. Dans les mêmes conditions qu'au premier alinéa du a, les bâtiments affectés à un usage agricole par les sociétés coopératives agricoles (...) ".<br/>
<br/>
              3. En faisant expressément référence aux conditions de l'exonération de taxe foncière prévue au a du 6° de l'article 1382 précité du code général des impôts, laquelle concerne les bâtiments servant aux exploitations rurales, les dispositions du b du même article ont entendu donner à la notion d'usage agricole qu'elles mentionnent une signification visant les opérations qui sont réalisées habituellement par les agriculteurs eux-mêmes et qui ne présentent pas un caractère industriel. Pour l'application de ces dispositions, ne présentent pas un caractère industriel les opérations réalisées par une société coopérative agricole avec des moyens techniques qui n'excèdent pas les besoins collectifs de ses adhérents, quelle que soit l'importance de ces moyens.<br/>
<br/>
              4. Pour déterminer si le bâtiment de la société requérante était affecté à un usage agricole lui ouvrant droit au bénéfice de l'exonération de taxe foncière, le tribunal administratif a comparé la capacité maximale de traitement de la chaîne d'embouteillage avec les cadences d'embouteillage, résultant de la production des adhérents, observées au cours des années en litige. Il en a déduit que les moyens techniques excédaient d'au minimum un tiers les besoins collectifs des adhérents et, implicitement mais nécessairement, que les opérations de la coopérative présentaient, dès lors, un caractère industriel. En déduisant de ces seules constatations que le bâtiment litigieux n'était pas affecté à un usage agricole, alors que la chaîne d'embouteillage ne constituait qu'une partie des moyens techniques utilisés par la société coopérative, y compris d'ailleurs au regard de l'activité d'embouteillage, de conditionnement et de stockage des vins opérée dans le bâtiment, le tribunal administratif a commis une erreur de droit. Par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société coopérative agricole Cave Les vins de Saint-Saturnin est fondée à demander l'annulation du jugement qu'elle attaque.  <br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              6. Il résulte des dispositions précitées de l'article 1382 du code général des impôts que l'exonération qu'elles prévoient s'applique aux bâtiments affectés à un usage agricole, c'est-à-dire à la réalisation d'opérations qui sont réalisées habituellement par les agriculteurs eux-mêmes. Une activité conduite pour le compte de tiers non coopérateurs, dans un cadre commercial, ne peut être regardée comme une opération habituellement réalisée par les agriculteurs eux-mêmes, sauf si l'activité conduite pour le compte de tiers a pour seul objet de  compenser, à activité globale inchangée et dans des conditions normales de fonctionnement des équipements, une réduction temporaire des besoins des coopérateurs.<br/>
<br/>
              7. Il est constant que la société Cave Les vins de Saint-Saturnin a une activité d'embouteillage, de conditionnement et de stockage pour le compte de ses associés coopérateurs et pour le compte de tiers non coopérateurs. Il ne résulte pas de l'instruction que les opérations conduites avec les tiers ont pour effet de compenser une réduction temporaire de l'activité conduite pour le compte de ses associés coopérateurs. Dès lors, le bâtiment dans lequel la société exerce cette activité n'est pas affecté à un usage agricole et n'ouvre pas droit au bénéfice de l'exonération prévue à l'article 1382 du code général des impôts. <br/>
<br/>
              8. Par suite, la demande de la société Cave Les vins de Saint-Saturnin doit être rejetée. Les dispositions de  l'article L. 761-1 du code de justice administrative font obstacle à ce que l'Etat, qui n'est pas la partie perdante, lui verse la somme qu'elle demande à ce titre. <br/>
<br/>
<br/>
<br/>                    D E C I D E :<br/>
                                  --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Montpellier du 21 mars 2016 est annulé.<br/>
<br/>
Article 2 : La demande de la SCA Cave Les vins de Saint-Saturnin devant le tribunal administratif et ses conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société coopérative agricole Cave Les vins de Saint-Saturnin et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
