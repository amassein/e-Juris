<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037461578</ID>
<ANCIEN_ID>JG_L_2018_10_000000413989</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/46/15/CETATEXT000037461578.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 03/10/2018, 413989, Publié au recueil Lebon</TITRE>
<DATE_DEC>2018-10-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413989</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Richard Senghor</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:413989.20181003</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Section française de l'Observatoire international des prisons a demandé au tribunal administratif de Lille d'annuler pour excès de pouvoir la décision par laquelle le directeur du centre pénitentiaire de Maubeuge a institué un régime de fouilles corporelles intégrales et systématiques des détenus au retour des parloirs. Par un jugement n° 1304184 du 2 juillet 2015, le tribunal administratif de Lille a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15DA01459 du 4 juillet 2017, la cour administrative d'appel de Douai a rejeté l'appel formé contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 4 septembre et 4 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, la Section française de l'observatoire international des prisons demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Richard Senghor, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la Section française de l'Observatoire international des prisons ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, le 28 juin 2013, la section française de l'observatoire international des prisons (SFOIP) a demandé au tribunal administratif de Lille l'annulation pour excès de pouvoir de la décision définissant le régime des fouilles corporelles pratiquées à l'issue des parloirs au sein  du centre pénitentiaire de Maubeuge qu'elle estimait être révélée par les fouilles pratiquées sur trois détenus de cet établissement. Invitée à produire la décision attaquée par le tribunal administratif, la SFOIP a saisi, le 12 juillet 2013, le directeur du centre pénitentiaire de Maubeuge d'une demande de communication des notes de service relatives aux fouilles à l'issue des parloirs ou de tout document ayant le même objet, comme le règlement intérieur de l'établissement. Cette demande a été réitérée, le 29 août 2013. Il n'y a jamais été donné suite. En défense, le ministre de la justice a conclu à l'irrecevabilité de la requête, en l'absence de production de la décision attaquée et d'élément permettant de démontrer son existence. Parallèlement à ces démarches, la SFOIP a adressé à plusieurs détenus du centre pénitentiaire un questionnaire sur les fouilles pratiquées à l'issue des parloirs. Ces courriers ont été interceptés par le chef d'établissement au motif qu'ils " pourraient amener une partie de la population pénale à s'opposer aux mesures de sécurité et de contrôle auxquelles elles sont soumises ". Par un jugement du 2 juillet 2015, le tribunal administratif de Lille a fait droit à la fin de non-recevoir opposée par le ministre de la justice et rejeté la requête de la SFOIP comme irrecevable. L'association requérante a produit, à l'appui de l'appel qu'elle a présenté contre ce jugement, une note du chef d'établissement, en date du 3 janvier 2014, indiquant qu'" à compter du 6 janvier 2014, de nouvelles modalités de contrôle vont entrer en application à l'issue des parloirs ". Par un arrêt du 4 juillet 2017, contre lequel la SFOIP se pourvoit en cassation, la cour administrative d'appel de Douai a rejeté l'appel dont elle était saisie en confirmant l'irrecevabilité des conclusions, après avoir relevé que la requérante n'établissait pas l'existence d'une décision administrative susceptible de recours pour excès de pouvoir.<br/>
<br/>
              2. D'une part, aux termes de l'article R. 412-1 du code de justice administrative, dans sa rédaction alors applicable : " La requête doit, à peine d'irrecevabilité, être accompagnée, sauf impossibilité justifiée, de la décision attaquée [...] ". Il résulte de ces dispositions qu'une requête est irrecevable et doit être rejetée comme telle lorsque son auteur n'a pas, en dépit d'une invitation à régulariser, produit la décision attaquée ou, en cas d'impossibilité, tout document apportant la preuve des diligences qu'il a accomplies pour en obtenir la communication.<br/>
<br/>
              3. D'autre part, il revient au juge de l'excès de pouvoir, avant de se prononcer sur une requête assortie d'allégations sérieuses non démenties par les éléments produits par l'administration en défense, de mettre en oeuvre ses pouvoirs généraux d'instruction des requêtes et de prendre toutes mesures propres à lui procurer, par les voies de droit, les éléments de nature à lui permettre de former sa conviction, en particulier en exigeant de l'administration compétente la production de tout document susceptible de permettre de vérifier les allégations du demandeur.<br/>
<br/>
              4. Il ressort des éléments mentionnés au point 1 que l'association requérante a accompli toutes les diligences qu'elle pouvait effectuer afin de se procurer la décision fixant le régime des fouilles des détenus à l'issue des parloirs du centre pénitentiaire de Maubeuge et que, en gardant le silence sur les demandes dont elle était saisie ou en interceptant les courriers adressés aux détenus de l'établissement pénitentiaire, l'administration n'a pas mis la SFOIP à même de satisfaire à l'exigence de production de la décision qu'elle attaquait. <br/>
<br/>
              5. Dès lors, eu égard aux éléments produits devant elle par l'association requérante et aux diligences que celle-ci a effectuées pour se procurer la décision qu'elle attaquait, la cour administrative d'appel de Douai a méconnu son office et commis une erreur de droit en confirmant l'irrecevabilité des conclusions dont elle était saisie, sans avoir préalablement fait usage de ses pouvoirs inquisitoriaux en demandant à l'administration pénitentiaire de produire la note de service définissant le régime des fouilles des détenus à la sortie des parloirs au centre pénitentiaire de Maubeuge ou, à défaut de l'existence d'une telle note, tous éléments de nature à révéler le régime de fouilles contesté, notamment le registre de consignation des fouilles mises en oeuvre sur les détenus. <br/>
<br/>
              6. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la SFOIP est fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Douai du 4 juillet 2017 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Douai.<br/>
Article 3 : La présente décision sera notifiée à la Section française de l'observatoire international des prisons et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">37-05-02-01 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. EXÉCUTION DES JUGEMENTS. EXÉCUTION DES PEINES. SERVICE PUBLIC PÉNITENTIAIRE. - MISE EN OEUVRE DES POUVOIRS D'INSTRUCTION DU JUGE DE L'EXCÈS DE POUVOIR - PRODUCTION DE TOUT DOCUMENT SUSCEPTIBLE DE PERMETTRE DE VÉRIFIER LES ALLÉGATIONS DU DEMANDEUR [RJ1] - DEMANDE D'ANNULATION DE LA DÉCISION DÉFINISSANT LE RÉGIME DE FOUILLES CORPORELLES PRATIQUÉES À L'ISSUE DES PARLOIRS AU SEIN D'UN CENTRE PÉNITENTIAIRE - ABSENCE DE PRODUCTION DE LA DÉCISION PAR L'ADMINISTRATION MALGRÉ LES DILIGENCES DU REQUÉRANT - OBLIGATION POUR LE JUGE DE FAIRE USAGE DE SES POUVOIRS INQUISITORIAUX EN DEMANDANT LA DÉCISION ATTAQUÉE, OU À DÉFAUT TOUT ÉLÉMENT DE NATURE À RÉVÉLER LE RÉGIME DE FOUILLES CONTESTÉ, NOTAMMENT LE REGISTRE DE CONSIGNATIONS DES FOUILLES - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-04-01-03 PROCÉDURE. INSTRUCTION. POUVOIRS GÉNÉRAUX D'INSTRUCTION DU JUGE. PRODUCTION ORDONNÉE. - MISE EN OEUVRE DES POUVOIRS D'INSTRUCTION DU JUGE DE L'EXCÈS DE POUVOIR - PRODUCTION DE TOUT DOCUMENT SUSCEPTIBLE DE PERMETTRE DE VÉRIFIER LES ALLÉGATIONS DU DEMANDEUR [RJ1] - DEMANDE D'ANNULATION DE LA DÉCISION DÉFINISSANT LE RÉGIME DE FOUILLES CORPORELLES PRATIQUÉES À L'ISSUE DES PARLOIRS AU SEIN D'UN CENTRE PÉNITENTIAIRE - ABSENCE DE PRODUCTION DE LA DÉCISION PAR L'ADMINISTRATION MALGRÉ LES DILIGENCES DU REQUÉRANT - OBLIGATION POUR LE JUGE DE FAIRE USAGE DE SES POUVOIRS INQUISITORIAUX EN DEMANDANT LA DÉCISION ATTAQUÉE, OU À DÉFAUT TOUT ÉLÉMENT DE NATURE À RÉVÉLER LE RÉGIME DE FOUILLES CONTESTÉ, NOTAMMENT LE REGISTRE DE CONSIGNATIONS DES FOUILLES - EXISTENCE.
</SCT>
<ANA ID="9A"> 37-05-02-01 Association ayant demandé l'annulation de la décision définissant le régime de fouilles corporelles pratiquées à l'issue des parloirs au sein d'un centre pénitentiaire qu'elle estimait révélée par les fouilles pratiquées sur des détenus. Administration ayant refusé de communiquer au juge la décision attaquée et à la requérante les notes de service s'y rapportant ou tout document ayant le même objet et ayant intercepté les questionnaires adressés aux détenus par l'association sur les fouilles pratiquées à l'issue des parloirs.... ,,L'association requérante a accompli toutes les diligences qu'elle pouvait effectuer afin de se procurer la décision fixant le régime des fouilles des détenus à l'issue des parloirs du centre pénitentiaire concerné. En gardant le silence sur les demandes dont elle était saisie ou en interceptant les courriers adressés aux détenus de l'établissement pénitentiaire, l'administration ne l'a pas mise à même de satisfaire à l'exigence de production de la décision qu'elle attaquait. Dès lors, eu égard aux éléments produits devant elle par l'association requérante et aux diligences que celle-ci a effectuées pour se procurer la décision qu'elle attaquait, méconnaît son office et commet une erreur de droit une cour qui confirme l'irrecevabilité des conclusions dont elle était saisie, sans avoir préalablement fait usage de ses pouvoirs inquisitoriaux en demandant à l'administration pénitentiaire de produire la note de service définissant le régime des fouilles des détenus à la sortie des parloirs au centre pénitentiaire concerné ou, à défaut de l'existence d'une telle note, tous éléments de nature à révéler le régime de fouilles contesté, notamment le registre de consignation des fouilles mises en oeuvre sur les détenus.</ANA>
<ANA ID="9B"> 54-04-01-03 Association ayant demandé l'annulation de la décision définissant le régime de fouilles corporelles pratiquées à l'issue des parloirs au sein d'un centre pénitentiaire qu'elle estimait révélée par les fouilles pratiquées sur des détenus. Administration ayant refusé de communiquer au juge la décision attaquée et à la requérante les notes de service s'y rapportant ou de tout document ayant le même objet. Administration ayant intercepté les questionnaires adressés aux détenus par l'association sur les fouilles pratiquées à l'issue des parloirs.... ,,L'association requérante a accompli toutes les diligences qu'elle pouvait effectuer afin de se procurer la décision fixant le régime des fouilles des détenus à l'issue des parloirs du centre pénitentiaire concerné. En gardant le silence sur les demandes dont elle était saisie ou en interceptant les courriers adressés aux détenus de l'établissement pénitentiaire, l'administration ne l'a pas mise à même de satisfaire à l'exigence de production de la décision qu'elle attaquait. Dès lors, eu égard aux éléments produits devant elle par l'association requérante et aux diligences que celle-ci a effectuées pour se procurer la décision qu'elle attaquait, méconnaît son office et commet une erreur de droit une cour qui confirme l'irrecevabilité des conclusions dont elle était saisie, sans avoir préalablement fait usage de ses pouvoirs inquisitoriaux en demandant à l'administration pénitentiaire de produire la note de service définissant le régime des fouilles des détenus à la sortie des parloirs au centre pénitentiaire concerné ou, à défaut de l'existence d'une telle note, tous éléments de nature à révéler le régime de fouilles contesté, notamment le registre de consignation des fouilles mises en oeuvre sur les détenus.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 26 novembre 2012, Mme Cordière, n° 354108, p. 394.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
