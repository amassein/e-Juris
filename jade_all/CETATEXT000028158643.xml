<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028158643</ID>
<ANCIEN_ID>JG_L_2013_10_000000372760</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/15/86/CETATEXT000028158643.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 15/10/2013, 372760, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-10-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372760</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2013:372760.20131015</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu, la requête, enregistrée le 11 octobre 2013 au secrétariat du contentieux du Conseil d'Etat, présentée par Mme C...A..., élisant domicile ...; Mme A...demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1307495 du 30 septembre 2013 par laquelle le juge des référés du tribunal administratif de Nantes, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté sa demande tendant à ce qu'il soit enjoint au préfet de la Loire-Atlantique d'enregistrer sa demande d'asile et de l'admettre au séjour en qualité de demandeur d'asile dans un délai d'une semaine à compter de la notification de l'ordonnance sous astreinte de 50 euros par jour ; <br/>
              2°) de faire droit à sa demande de première instance ;<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              elle soutient que :<br/>
              - le juge des référés du tribunal administratif de Nantes a commis une erreur manifeste d'appréciation en rejetant sa demande pour défaut d'urgence et une erreur de droit en considérant que le préfet n'avait pas commis d'atteinte grave et manifestement illégale à une liberté fondamentale ; <br/>
              - la condition d'urgence est remplie en raison du refus d'admission au séjour au titre de l'asile et en raison de la décision de remise à un Etat membre dans le cadre du règlement n° 343/2003 du Conseil du 18 février 2003 ; <br/>
              - l'autorité administrative a porté une atteinte grave et manifestement illégale au droit d'asile ; <br/>
<br/>
<br/>
	Vu l'ordonnance attaquée ;<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le règlement (CE) n° 343/2003 du Conseil du 18 février 2003 ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ; qu'en vertu de l'article L. 522-3 du même code, le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée ; qu'à cet égard, il appartient au juge d'appel de prendre en considération les éléments recueillis par le juge du premier degré dans le cadre de la procédure écrite et orale qu'il a diligentée ;<br/>
<br/>
              2. Considérant que le droit constitutionnel d'asile, qui a le caractère d'une liberté fondamentale, a pour corollaire le droit de solliciter le statut de réfugié ; que, s'il implique que l'étranger qui sollicite la reconnaissance de la qualité de réfugié soit en principe autorisé à demeurer sur le territoire jusqu'à ce qu'il ait été statué sur sa demande, ce droit s'exerce dans les conditions définies par l'article L. 741-4 du code de l'entrée et du séjour des étrangers et du droit d'asile ; qu'en vertu du 1° de cet article, l'admission en France d'un étranger qui demande à être admis au bénéfice de l'asile peut être refusée si l'examen de sa demande relève de la compétence d'un autre Etat en application des dispositions du règlement (CE) n° 343/2003 du 18 février 2003 ; que l'article 19 de ce règlement prévoit que le transfert du demandeur d'asile vers le pays de réadmission doit se faire dans les six mois à compter de l'acceptation de la demande de prise en charge, le demandeur d'asile étant, si nécessaire, muni par l'Etat membre requérant d'un laissez-passer conforme à un modèle, et que ce délai peut être porté à dix-huit mois si l'intéressé " prend la fuite " ;<br/>
<br/>
              3. Considérant qu'il résulte de l'instruction conduite par le juge des référés du tribunal administratif de Nantes que MmeB..., de nationalité russe, qui est entrée en France le 5 décembre 2012, a sollicité l'asile en France le 1er février 2013 ; que ses empreintes digitales ont été relevées en Pologne par le système " Eurodac " ; que le préfet de la Loire-Atlantique a, par une décision en date du 12 février 2013, refusé de l'admettre provisoirement au séjour en qualité de demandeur d'asile ; que, le 21 février 2013, la Pologne a accepté la demande de la France visant à la réadmission de l'intéressée conformément aux dispositions du règlement CE 343/2003 du Conseil du 18 février 2013 ; que, le 19 mars 2013, le préfet a notifié à Mme A... sa remise aux autorités polonaises et l'a informée de sa convocation, le 27 mars 2013, à l'aéroport de Roissy en vue de son départ ; que toutes les informations nécessaires et traduites ont été communiquées à l'intéressée, qui a été dûment informée qu'elle pouvait bénéficier d'une escorte de police depuis son lieu d'hébergement jusqu'à l'aéroport et qu'à défaut de déférer à cette convocation, elle serait considérée comme en fuite et qu'il serait alors mis fin aux prestations qui lui sont offertes ; que les services de la police de l'air et des frontières ont constaté et fait savoir au préfet de la Loire-Atlantique que Mme A...ne s'est pas présentée à l'aéroport  ; qu'en conséquence, en portant, dans ces circonstances, à 18 mois le délai de réadmission de l'intéressée vers la Pologne, le préfet de la Loire-Atlantique n'a pas commis de méconnaissance grave et manifeste des obligations qu'impose le respect du droit d'asile, ainsi que l'a jugé à bon droit le juge des référés du tribunal administratif de Nantes ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède qu'il est ainsi manifeste que la requête de Mme A...ne peut être accueillie ; qu'il y a lieu de rejeter cette requête, y compris les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative, selon la procédure prévue par l'article L. 522-3 de ce code ; <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de Mme A...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme C...A....<br/>
Copie de la présente ordonnance sera transmise pour information au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
