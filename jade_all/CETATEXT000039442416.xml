<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039442416</ID>
<ANCIEN_ID>JG_L_2019_12_000000423544</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/44/24/CETATEXT000039442416.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 02/12/2019, 423544</TITRE>
<DATE_DEC>2019-12-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423544</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLOCHE ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:423544.20191202</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société SM Entreprise a demandé au tribunal administratif de Montpellier de condamner, à titre principal, le centre hospitalier Francis Vals à lui verser la somme de 1 439 117,19 euros toutes taxes comprises (TTC), majorée des intérêts moratoires au taux légal en règlement du marché de construction du nouvel hôpital de Port-la-Nouvelle et, à titre subsidiaire, de condamner solidairement les sociétés Guervilly, Puig Pujol Architecture et Bâti structure à lui payer les sommes de 822 830,65 euros hors taxes (HT) et de 68 444,56 euros HT. Par un jugement n° 1005788 du 27 avril 2012, le tribunal administratif de Montpellier a condamné le centre hospitalier Francis Vals à verser la somme de 30 439,92 euros HT à la société SM Entreprise, majorée des intérêts au taux légal et de la capitalisation de ces intérêts.<br/>
<br/>
              Par un arrêt n° 12MA02540 du 21 décembre 2017, rectifié par un arrêt du 9 avril 2018, la cour administrative d'appel de Marseille a condamné le centre hospitalier Francis Vals à verser à la société SM Entreprise la somme de 619 889,79 euros TTC, augmentée des intérêts et de leur capitalisation et a ordonné, avant de statuer sur l'appel en garantie du centre hospitalier Francis Vals formé à l'encontre des sociétés Guervilly, Puig Pujol Architecture et Bâti Structure Ouest, un supplément d'instruction. Par un nouvel arrêt du 2 juillet 2018, la cour a, d'une part, condamné in solidum les sociétés Guervilly, Puig Pujol Architecture et Bâti Structure Ouest à garantir le centre hospitalier Francis Vals de la condamnation de 518 372,11 euros TTC prononcée à son encontre au titre du surcoût de construction par l'article 1er de l'arrêt n° 12MA02540 du 21 décembre 2017 tel que rectifié par l'article 2 de l'arrêt n° 18MA00021 du 9 avril 2018 et, d'autre part, mis à leur charge les frais d'expertise.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 24 août et 26 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, les sociétés Guervilly, Puig Pujol Architecture et Bâti Structure Ouest demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 2 juillet 2018 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'action dirigée à leur encontre par le centre hospitalier Francis Vals ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier Francis Vals la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des marchés publics ;<br/>
              - le décret n° 76-87 du 21 janvier 1976 ;<br/>
              - le décret n° 78-1306 du 26 décembre 1978 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Sirinelli, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boulloche, avocat de la société Guervilly, de la société Puig-Pujol Architecture et de la société Bati Structure Ouest et à la SCP Foussard, Froger, avocat du centre hospitalier Francis Vals ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juge du fond que, pour la construction de son nouvel hôpital, le centre hospitalier Francis Vals, situé à Port-la-Nouvelle a confié la maîtrise d'oeuvre de l'opération à un groupement solidaire formé entre les sociétés Guervilly, Puig Pujol et Bâti Structure Ouest et la réalisation des travaux de fondations et de gros oeuvre du bâtiment à la société SM Entreprise. La cour administrative d'appel de Marseille, après avoir condamné, par un arrêt du 21 décembre 2017 ayant fait l'objet d'une rectification le 9 avril 2018, le centre hospitalier à verser à cette entreprise, dans le cadre du règlement financier de son marché, la somme de 619 889,79 euros TTC au titre des travaux supplémentaires qu'elle avait dû effectuer, a, par un nouvel arrêt du 2 juillet 2018 intervenu à l'issue d'une mesure d'instruction, condamné solidairement les sociétés Guervilly, Puig Pujol Architecture et Bâti Structure Ouest à garantir le centre hospitalier du montant de 518 372,11 euros TTC au titre du surcoût de construction. Les sociétés Guervilly, Puig Pujol Architecture et Bâti Structure Ouest se pourvoient en cassation contre ce dernier arrêt.<br/>
<br/>
              2. En premier lieu, il ressort des pièces du dossier que, contrairement à ce qui est soutenu, la minute porte les signatures requises par les dispositions de l'article R. 741-7 du code de justice administrative. <br/>
<br/>
              3. En second lieu, il ressort des énonciations de l'arrêt attaqué que, pour estimer que le centre hospitalier Francis Vals était fondé à demander la condamnation solidaire de la société Guervilly, de la société Pujol et de la société Bati Structure Ouest à le garantir de la condamnation prononcée à son encontre au titre du surcoût de construction, la cour administrative d'appel de Marseille a, d'une part, relevé que ce surcoût était directement imputable à faute de conception de l'ouvrage des maîtres d'oeuvre et, d'autre part, estimé que ces derniers ne pouvaient se prévaloir de la réception de l'ouvrage ni du caractère définitif du décompte du marché de travaux. <br/>
<br/>
              4. D'une part, l'entrepreneur a le droit d'être indemnisé du coût des travaux supplémentaires indispensables à la réalisation d'un ouvrage dans les règles de l'art. La charge définitive de l'indemnisation incombe, en principe, au maître de l'ouvrage. Toutefois, le maître d'ouvrage est fondé, en cas de faute du maître d'oeuvre, à l'appeler en garantie, sans qu'y fasse obstacle la réception de l'ouvrage. Il en va ainsi lorsque la nécessité de procéder à ces travaux n'est apparue que postérieurement à la passation du marché, en raison d'une mauvaise évaluation initiale par le maître d'oeuvre, et qu'il établit qu'il aurait renoncé à son projet de construction ou modifié celui-ci s'il en avait été avisé en temps utile. Il en va de même lorsque, en raison d'une faute du maître d'oeuvre dans la conception de l'ouvrage ou dans le suivi de travaux, le montant de l'ensemble des travaux qui ont été indispensables à la réalisation de l'ouvrage dans les règles de l'art est supérieur au coût qui aurait dû être celui de l'ouvrage si le maître d'oeuvre n'avait commis aucune faute, à hauteur de la différence entre ces deux montants.<br/>
<br/>
              5. Par l'arrêt attaqué, la cour a retenu que le renchérissement de la construction résultait non des seules nécessités constructives du site, mais de l'existence d'une faute de conception des maîtres d'oeuvre résultant d'une mauvaise évaluation initiale dont les conséquences en termes de travaux supplémentaires ne sont apparues que postérieurement à la passation des marchés de maîtrise d'oeuvre et de travaux et que le centre hospitalier Francis Vals a établi qu'il aurait modifié le projet de construction s'il avait été avisé en temps utile de la nécessité de procéder à ces travaux supplémentaires. La cour a pu légalement déduire des constatations et appréciations qu'elle a portées dans le cadre de son pouvoir souverain, lesquelles sont exemptes de dénaturation, que les maîtres d'oeuvre devaient être solidairement condamnés à garantir le maître d'ouvrage de la condamnation prononcée à son encontre au titre du surcoût de construction directement imputable à cette erreur.<br/>
<br/>
              6. D'autre part, la réception d'un ouvrage est l'acte par lequel le maître de l'ouvrage déclare accepter l'ouvrage avec ou sans réserve. Elle vaut pour tous les participants à l'opération de travaux, même si elle n'est prononcée qu'à l'égard de l'entrepreneur, et met fin aux rapports contractuels entre le maître de l'ouvrage et les constructeurs en ce qui concerne la réalisation de l'ouvrage. Si elle interdit, par conséquent, au maître de l'ouvrage d'invoquer, après qu'elle a été prononcée, et sous réserve de la garantie de parfait achèvement, des désordres apparents causés à l'ouvrage ou des désordres causés aux tiers, dont il est alors réputé avoir renoncé à demander la réparation, elle ne met fin aux obligations contractuelles des constructeurs que dans cette seule mesure. Ainsi la réception demeure, par elle-même, sans effet sur les droits et obligations financiers nés de l'exécution du marché, à raison notamment de retards ou de travaux supplémentaires, dont la détermination intervient définitivement lors de l'établissement du solde du décompte définitif. Seule l'intervention du décompte général et définitif du marché a pour conséquence d'interdire au maître de l'ouvrage toute réclamation à cet égard.<br/>
<br/>
              7. Si, aux termes des stipulations de l'article 32 du cahier des clauses administratives générales applicables aux marchés de prestations intellectuelles, applicable au marché de maîtrise d'oeuvre en cause : " Les prestations faisant l'objet du marché sont soumises à des vérifications destinées à constater qu'elles répondent aux stipulations prévues dans le marché (...) ", et aux termes des stipulations de l'article 33.2 du même cahier : " La personne responsable du marché prononce la réception des prestations si elles répondent aux stipulations du marché. La date de prise d'effet de la réception est précisée dans la décision de réception ; à défaut, c'est la date de notification de cette décision (...) ", il résulte de ce qui a été dit au point précédent qu'indépendamment de la décision du maître d'ouvrage de réceptionner les prestations de maîtrise d'oeuvre prévue par les stipulations précitées de l'article 32 du cahier des clauses administratives générales applicables aux marchés de prestations intellectuelles, la réception de l'ouvrage met fin aux rapports contractuels entre le maître d'ouvrage et le maître d'oeuvre en ce qui concerne les prestations indissociables de la réalisation de l'ouvrage, au nombre desquelles figurent, notamment, les missions de conception de cet ouvrage. <br/>
<br/>
              8. Il suit de là qu'en justifiant par le fait que la réception de l'ouvrage n'a pas pour objet de constater les éventuelles fautes de conception imputables au maître d'oeuvre de l'opération, lesquelles ont vocation à être constatées et réservées, le cas échéant, à l'occasion de la réception des prestations du marché de maîtrise d'oeuvre, le constat que cette réception ne fait pas obstacle à ce que la responsabilité contractuelle des maîtres d'oeuvre soit recherchée à raison des fautes de conception qu'ils ont éventuellement commises, la cour administrative d'appel de Marseille a commis une erreur de droit. Toutefois, ce motif, qui ne justifie pas la solution retenue par la cour compte tenu de ce qui a été dit au point 4, présente un caractère surabondant. Par suite, le moyen tiré de l'erreur de droit affectant cette partie de l'arrêt peut être écarté comme inopérant.<br/>
<br/>
              9. Il résulte de tout ce qui précède que les sociétés Guervilly, Puig Pujol Architecture et Bâti Structure Ouest ne sont pas fondées à demander l'annulation de l'arrêt qu'elles attaquent.<br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées à ce titre par les sociétés Guervilly, Puig Pujol Architecture et Bâti Structure Ouest. En revanche, il y a lieu de mettre à la charge de ces sociétés la somme de 3 000 euros présentées au même titre par le centre hospitalier Francis Valls.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi des sociétés Guervilly, Puig Pujol Architecture et Bâti Structure Ouest est rejeté.<br/>
Article 2 : Il est mis à la charge des sociétés Guervilly, Puig Pujol Architecture et Bâti Structure Ouest la somme globale de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Guervilly, représentant unique ainsi qu'au centre hospitalier Francis Valls.<br/>
Copie en sera adressée à la société Sogea.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-06-01-01-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RAPPORTS ENTRE L'ARCHITECTE, L'ENTREPRENEUR ET LE MAÎTRE DE L'OUVRAGE. RESPONSABILITÉ DES CONSTRUCTEURS À L'ÉGARD DU MAÎTRE DE L'OUVRAGE. QUESTIONS GÉNÉRALES. RÉCEPTION DES TRAVAUX. - PORTÉE - FIN DES RAPPORTS CONTRACTUELS ENTRE LE MAÎTRE DE L'OUVRAGE ET LE MAÎTRE D'&#140;UVRE EN CE QUI CONCERNE LES PRESTATIONS INDISSOCIABLES DE LA RÉALISATION DE L'OUVRAGE [RJ1] - CONSÉQUENCE - IMPOSSIBILITÉ POUR LE MAÎTRE DE L'OUVRAGE DE RECHERCHER LA RESPONSABILITÉ CONTRACTUELLE DU MAÎTRE D'&#140;UVRE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-06-01-02-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. RAPPORTS ENTRE L'ARCHITECTE, L'ENTREPRENEUR ET LE MAÎTRE DE L'OUVRAGE. RESPONSABILITÉ DES CONSTRUCTEURS À L'ÉGARD DU MAÎTRE DE L'OUVRAGE. RESPONSABILITÉ CONTRACTUELLE. CHAMP D'APPLICATION. - RESPONSABILITÉ CONTRACTUELLE DU MAÎTRE D'&#140;UVRE EN CE QUI CONCERNE LES PRESTATIONS INDISSOCIABLES DE LA RÉALISATION DE L'OUVRAGE - RESPONSABILITÉ NE POUVANT ÊTRE RECHERCHÉE APRÈS LA RÉCEPTION DE L'OUVRAGE [RJ1].
</SCT>
<ANA ID="9A"> 39-06-01-01-01 Indépendamment de la décision du maître d'ouvrage de réceptionner les prestations de maîtrise d'oeuvre prévue par les stipulations de l'article 32 du cahier des clauses administratives générales (CCAG) applicables aux marchés de prestations intellectuelles, la réception de l'ouvrage met fin aux rapports contractuels entre le maître d'ouvrage et le maître d'oeuvre en ce qui concerne les prestations indissociables de la réalisation de l'ouvrage, au nombre desquelles figurent, notamment, les missions de conception de cet ouvrage.,,,Par suite, commet une erreur de droit la cour qui juge que le constat que la réception de l'ouvrage ne fait pas obstacle à ce que la responsabilité contractuelle des maîtres d'oeuvre soit recherchée à raison des fautes de conception qu'ils ont éventuellement commises.</ANA>
<ANA ID="9B"> 39-06-01-02-005 Indépendamment de la décision du maître d'ouvrage de réceptionner les prestations de maîtrise d'oeuvre prévue par les stipulations de l'article 32 du cahier des clauses administratives générales (CCAG) applicables aux marchés de prestations intellectuelles, la réception de l'ouvrage met fin aux rapports contractuels entre le maître d'ouvrage et le maître d'oeuvre en ce qui concerne les prestations indissociables de la réalisation de l'ouvrage, au nombre desquelles figurent, notamment, les missions de conception de cet ouvrage.,,,Par suite, commet une erreur de droit la cour qui juge que le constat que la réception de l'ouvrage ne fait pas obstacle à ce que la responsabilité contractuelle des maîtres d'oeuvre soit recherchée à raison des fautes de conception qu'ils ont éventuellement commises.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., en précisant s'agissant du maître d'oeuvre, CE, Section, 6 avril 2007, Centre hospitalier général de Boulogne-sur-Mer, n° 264490 264491, p. 163.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
