<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038411785</ID>
<ANCIEN_ID>JG_L_2019_04_000000420965</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/41/17/CETATEXT000038411785.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 24/04/2019, 420965, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-04-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420965</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE ; SCP MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>Mme Laure Durand-Viel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:420965.20190424</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Lyon d'annuler l'arrêté du 28 juillet 2014 par lequel le maire de la commune de Colombier-Saugnieu a délivré un permis de construire à la société Rhône Saône Habitat en vue de l'édification d'un ensemble de logements et de commerces sur un terrain situé au lieudit Champ Vallet ;<br/>
<br/>
              Par un jugement n° 1407885 du 12 mai 2016, le tribunal administratif de Lyon a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 16LY02606 du 27 mars 2018, sur appel de M.B..., la cour administrative d'appel de Lyon a annulé ce jugement ainsi que l'arrêté du 28 juillet 2014.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 28 mai et 28 août 2018 au secrétariat du contentieux du Conseil d'Etat, la commune de Colombier-Saugnieu demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de M. B...la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laure Durand-Viel, auditeur,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat de la commune de Colombier-Saugnieu et à la SCP Marlange, de la Burgade, avocat de M.B....<br/>
<br/>
              Vu la note en délibéré, enregistrée le 3 avril 2019, présentée par M.B....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que par un arrêté du 28 juillet 2014, le maire de Colombier-Saugnieu a délivré à la société Rhône Saône Habitat un permis de construire en vue de l'édification de dix-neuf maisons individuelles et d'un immeuble collectif sur deux niveaux comprenant des commerces. Par un jugement du 12 mai 2016, le tribunal administratif de Lyon a rejeté la demande de M.B..., voisin du projet litigieux, tendant à l'annulation pour excès de pouvoir de ce permis de construire. Par un arrêt du 27 mars 2018, contre lequel la commune de Colombier-Saugnieu se pourvoit en cassation, la cour administrative d'appel de Lyon, sur l'appel de M.B..., a annulé ce jugement ainsi que l'arrêté du 28 juillet 2014. Il ressort des énonciations de l'arrêt attaqué que la cour a jugé que le permis litigieux était illégal pour deux motifs, tirés de la méconnaissance, d'une part, des exigences relatives au permis de démolir et, d'autre part, des règles de distance par rapport aux limites séparatives. Elle a par ailleurs décidé de ne pas faire application des dispositions des articles L. 600-5 et L. 600-5-1 du code de l'urbanisme. <br/>
<br/>
              Sur le respect des exigences relatives au permis de démolir :<br/>
<br/>
              2. En premier lieu, l'article L. 421-3 du code de l'urbanisme dispose que : " Les démolitions de constructions existantes doivent être précédées de la délivrance d'un permis de démolir lorsque la construction relève d'une protection particulière définie par décret en Conseil d'Etat ou est située dans une commune ou partie de commune où le conseil municipal a décidé d'instaurer le permis de démolir. " Aux termes du deuxième alinéa de l'article L. 421-6 du même code, " Le permis de démolir peut être refusé ou n'être accordé que sous réserve de l'observation de prescriptions spéciales si les travaux envisagés sont de nature à compromettre la protection ou la mise en valeur du patrimoine bâti, des quartiers, des monuments et des sites. ". L'article L. 451-1 du même code dispose que : " Lorsque la démolition est nécessaire à une opération de construction ou d'aménagement, la demande de permis de construire ou d'aménager peut porter à la fois sur la démolition et sur la construction ou l'aménagement. Dans ce cas, le permis de construire ou le permis d'aménager autorise la démolition. " Aux termes de l'article R. 421-27 du même code : " Doivent être précédés d'un permis de démolir les travaux ayant pour objet de démolir ou de rendre inutilisable tout ou partie d'une construction située dans une commune ou une partie de commune où le conseil municipal a décidé d'instituer le permis de démolir. " Aux termes de l'article R. 431-21 du même code : " Lorsque les travaux projetés nécessitent la démolition de bâtiments soumis au régime du permis de démolir, la demande de permis de construire ou d'aménager doit : / a) Soit être accompagnée de la justification du dépôt de la demande de permis de démolir ; / b) Soit porter à la fois sur la démolition et sur la construction ou l'aménagement ". L'article A. 424-16 du même code dispose que si le projet prévoit des démolitions, le panneau d'affichage de l'autorisation doit indiquer la surface du ou des bâtiments à démolir. <br/>
<br/>
              3. Il résulte de ces dispositions, d'une part, que lorsqu'un permis de construire autorise un projet qui implique la démolition totale ou partielle d'un bâtiment soumis au régime du permis de démolir, la demande de permis de construire doit, soit être accompagnée de la justification du dépôt de la demande de permis de démolir, soit porter à la fois sur la démolition et sur la construction. D'autre part, si le permis de construire et le permis de démolir peuvent être accordés par une même décision, au terme d'une instruction commune, ils constituent des actes distincts ayant des effets propres. Eu égard à l'objet et à la portée du permis de démolir, la décision statuant sur la demande de permis de construire ne peut valoir autorisation de démolir que si le dossier de demande mentionne explicitement que le pétitionnaire entend solliciter cette autorisation. Est par elle-même sans incidence la circonstance que les plans joints à la demande de permis de construire montrent que la réalisation de la construction implique la démolition de bâtiments existants.<br/>
<br/>
              4. Il ressort des énonciations souveraines de l'arrêt attaqué, qui ne sont pas arguées de dénaturation, qu'en vertu des dispositions de l'article UD 2 du règlement du plan local d'urbanisme de la commune de Colombier-Saugnieu, les démolitions sont soumises à permis de démolir, et que le terrain d'assiette du projet litigieux supporte un bâtiment destiné à la démolition. En jugeant qu'en se bornant à verser au dossier de permis de construire un plan masse et un plan de situation où est mentionnée une construction dont l'emprise coïncide avec la future voirie de l'ensemble immobilier projeté, la société Rhône Saône Habitat, qui n'a pas joint au dossier la justification du dépôt d'une demande de permis de démolir et qui n'a pas précisé que sa demande portait également sur un permis de démolir, ne pouvait être regardée comme ayant présenté un dossier répondant aux exigences des dispositions régissant le permis de démolir, la cour, dont l'arrêt est sur ce point suffisamment motivé, n'a commis aucune erreur de droit au regard des règles rappelées au point 4.<br/>
<br/>
              Sur le respect des règles d'implantation : <br/>
<br/>
              5. En vertu des dispositions du premier alinéa de l'article L. 123-1-9 du code de l'urbanisme alors en vigueur, reprises en substance à l'article L. 152-3, les règles et servitudes définies par un plan local d'urbanisme ne peuvent faire l'objet d'aucune dérogation, à l'exception des adaptations mineures rendues nécessaires par la nature du sol, la configuration des parcelles ou le caractère des constructions avoisinantes. Aux termes de l'article R. 123-21 du même code, dans sa rédaction alors applicable, le règlement du plan doit : " b) Edicter, en fonction des situations locales, les prescriptions relatives à l'implantation des constructions par rapport aux voies, aux limites séparatives et autres constructions (...) ". <br/>
<br/>
              6. En vertu des dispositions rappelées au point précédent, le règlement d'un plan local d'urbanisme doit fixer des règles précises d'implantation. Le règlement peut contenir des dispositions permettant de faire exception aux règles générales d'implantation qu'il fixe, notamment afin de permettre une intégration plus harmonieuse des projets dans le milieu urbain environnant. Ces règles d'exception doivent alors être suffisamment encadrées, en particulier par la définition des catégories de constructions susceptibles d'en bénéficier, sans préjudice de la possibilité d'autoriser des adaptations mineures en vertu de l'article L. 123-19-1 du code de l'urbanisme.<br/>
<br/>
              7. Il ressort des pièces du dossier soumis aux juges du fond que l'article UD 7 du règlement du plan local d'urbanisme, après avoir fixé des règles d'implantation des constructions par rapport aux limites parcellaires, en imposant le respect de distances minimales de retrait, dispose que : " Une implantation différente de celle mentionnée ci-dessus peut être acceptée pour : (...) / des lotissements ou opérations groupées comportant au moins cinq logements (...) ". Ces dispositions, fondement du permis de construire contesté, définissent précisément les catégories de constructions susceptibles de bénéficier de cette règle d'exception. Dès lors, en jugeant que la règle d'exception critiquée était illégale faute d'être suffisamment encadrée, la cour a commis une erreur de qualification juridique.<br/>
<br/>
              Sur l'application des dispositions des articles L. 600-5 ou L. 600-5-1 du code de l'urbanisme :<br/>
<br/>
              8. Il ressort des énonciations de l'arrêt attaqué que la cour a annulé l'arrêté du 28 juillet 2014, après avoir décidé de ne pas faire application des dispositions des articles L. 600-5 et L. 600-5-1 du code de l'urbanisme. Cependant, il résulte de ce qui a été dit au point 7 que la cour a commis une erreur de qualification juridique en retenant le vice relatif à la méconnaissance des règles d'implantation des constructions par rapport aux limites parcellaires. Si la présente décision juge fondé le motif retenu par la cour relatif à la méconnaissance des exigences relatives au permis de démolir, un tel vice apparaît susceptible de faire l'objet d'une mesure de régularisation en application des articles L. 600-5 ou L. 600-5-1 du code de l'urbanisme et n'est, par suite, pas de nature à justifier à lui seul un refus de faire application de ces dispositions. Dès lors, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, il y a lieu de faire droit aux conclusions à fin d'annulation de l'arrêt attaqué.<br/>
<br/>
              Sur les frais de l'instance :<br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la commune de Colombier-Saugnieu, qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de M. B...une somme de 3 000 euros à verser à la commune de Colombier-Saugnieu au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 27 mars 2018 de la cour administrative d'appel de Lyon est annulé.<br/>
<br/>
      Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
<br/>
Article 3 : M. B...versera à la commune de Colombier-Saugnieu une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées par M. B...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la commune de Colombier-Saugnieu et à M. A... B....<br/>
      Copie en sera adressée à la société Rhône Saône Habitat. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
