<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033581197</ID>
<ANCIEN_ID>JG_L_2016_12_000000396352</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/58/11/CETATEXT000033581197.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 09/12/2016, 396352, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396352</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:396352.20161209</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Marseille d'annuler six titres exécutoires émis à son encontre par la commune de Fontvieille en application d'une convention qu'elle avait conclue avec celle-ci le 1er février 2010 et de la décharger de l'obligation de payer les sommes correspondantes. Par un jugement n° 1105215 du 18 mars 2013, le tribunal administratif de Marseille a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 13MA02242 du 13 février 2015, la cour administrative d'appel de Marseille a, sur appel de MmeB..., annulé ce jugement et les titres exécutoires émis par la commune de Fontvieille. <br/>
<br/>
              Par un arrêt n° 15MA01558 du 23 novembre 2015, la cour administrative d'appel de Marseille a rejeté l'opposition formée à l'encontre de l'arrêt du 13 février 2015 par la commune de Fontvieille.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 25 janvier, 25 avril et 3 novembre 2016 au secrétariat du contentieux du Conseil d'Etat, la commune de Fontvieille demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 15MA01558 du 23 novembre 2015 de la cour administrative d'appel de Marseille ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son opposition ;<br/>
<br/>
              3°) de mettre à la charge de Mme B...la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la commune de Fontvieille, et à la SCP Waquet, Farge, Hazan, avocat de MmeB....<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 1411-1 du code général des collectivités territoriales, dans sa version alors applicable : " Une délégation de service public est un contrat par lequel une personne morale de droit public confie la gestion d'un service public dont elle a la responsabilité à un délégataire public ou privé, dont la rémunération est substantiellement liée aux résultats de l'exploitation du service. Le délégataire peut être chargé de construire des ouvrages ou d'acquérir des biens nécessaires au service " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une convention du 1er février 2010, conclue, en vertu de son article 1er, " à titre précaire et révocable ", la commune de Fontvieille a confié à Mme B..." l'exploitation touristique " des sites " Moulin de Daudet " et " Château de Montauban " pour la période du 1er février au 31 décembre 2010, le contrat pouvant être prolongé par décision expresse " pour l'année 2011 " ; qu'aux termes de l'article 3, Mme B...assure l'ouverture au public du " Moulin de Daudet 7 jours sur 7 et du Château de Montauban à concurrence au moins de la durée des vacances scolaires " ; qu'en vertu de l'article 4, en contrepartie du versement d'une redevance de 7 500 euros mensuels, Mme B...est rémunérée par les droits d'entrée perçus du public et la vente de souvenirs, cartes postales, livres, dont elle fixe les prix librement ; que l'article 5 stipule que les produits vendus sur les sites ne peuvent être alimentaires ou de " nature dévalorisante ou anachronique pour l'image et la qualité des lieux " ; et qu'aux termes de l'article 8 : " la convention est résiliable à tout moment par la preneuse sous préavis de 3 mois " ;<br/>
<br/>
              3. Considérant qu'eu égard à l'absence d'implication dans l'organisation de l'exploitation touristique des sites en cause de la commune, qui, ainsi qu'il ressort des pièces du dossier soumis à la cour, s'est bornée à fixer les jours d'ouverture et à imposer à l'intéressée de respecter le caractère historique et culturel des sites dont elle devait assurer l'exploitation mais n'a exercé de contrôle ni sur le montant des droits d'entrée, ni sur les prix de vente des produits vendus sur les sites, ni sur les horaires d'ouverture des sites et n'a prescrit à la preneuse aucune obligation relative, notamment, à l'organisation de visites guidées ou d'activités culturelles ou à l'accueil de publics particuliers, qu'eu égard, au surplus, à la faculté donnée à la preneuse de révoquer la convention à tout moment et à la brièveté du préavis applicable, la cour administrative d'appel de Marseille a entaché son arrêt d'une erreur de qualification juridique en jugeant que ce contrat avait pour objet de faire participer directement Mme B...à l'exécution du service public culturel en raison de la dimension historique et littéraire des lieux et constituait une délégation de service public ; qu'il suit de là que la commune de Fontvieille est fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de la commune de Fontvieille, qui n'est pas la partie perdante, le versement des sommes que demande, à ce titre, Mme B...; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme B...le versement d'une somme de 1 500 euros à la commune de Fontvieille au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 23 novembre 2015 de la cour administrative d'appel de Marseille est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : Mme B...versera la somme de 1 500 euros à la commune de Fontvieille au titre des dispositions de l'article L. 761-1 du code de justice administrative. Les conclusions présentées au même titre par Mme B...sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la commune de Fontvieille et à Mme A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
