<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036521812</ID>
<ANCIEN_ID>JG_L_2018_01_000000389523</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/52/18/CETATEXT000036521812.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 19/01/2018, 389523</TITRE>
<DATE_DEC>2018-01-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389523</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP PIWNICA, MOLINIE ; BALAT ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:389523.20180119</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association Diderot Transparence a demandé au tribunal administratif de Paris d'annuler les arrêtés en date du 6 septembre 2012 par lesquels le préfet de police a autorisé l'ouverture au public des bâtiments M5B2 et M6A1 de la zone d'aménagement concerté Paris Rive Gauche. Par un jugement n° 1219653 du 28 février 2014, le tribunal administratif a fait droit à cette demande.<br/>
<br/>
              Par un arrêt n° 14PA01895, 14PA01906, 14PA01921 du 16 février 2015, la cour administrative d'appel de Paris a rejeté les appels formés par la société Udicité, l'Université Paris-Diderot-Paris 7 et le ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche contre ce jugement.  <br/>
<br/>
              1° Sous le numéro 389523, par un pourvoi sommaire et des mémoires complémentaires, enregistrés les 16 avril, 17 juillet et 15 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, la société Udicité demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'association Diderot Transparence la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° Sous le numéro 389654, par un pourvoi sommaire et des mémoires complémentaires, enregistrés les 21 avril et 21 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, l'Université Paris Diderot - Paris 7 demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce même arrêt de la cour administrative d'appel de Paris ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ; <br/>
<br/>
              3°) de mettre à la charge de l'association Diderot Transparence la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              - le code de la construction et de l'habitation ;<br/>
<br/>
              - le code de l'urbanisme ;<br/>
<br/>
              - l'arrêté du 25 juin 1980 modifié portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les établissements recevant du public ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de la société Udicité, à la SCP Piwnica, Molinié, avocat de l'Université Paris-Diderot-Paris 7, à Me Balat, avocat de l'association Diderot Transparence, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la Préfecture de police.<br/>
<br/>
<br/>
              Vu les notes en délibéré, enregistrées le 26 décembre 2017, présentées par l'association Diderot Transparence ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par des arrêtés en date du 6 septembre 2012, le préfet de police a autorisé l'ouverture au public des bâtiments universitaires M5B2 et M6A1 de la zone d'aménagement concerté Paris Rive Gauche, dans le 13ème arrondissement de Paris ; que par un jugement du 28 février 2014, le tribunal administratif de Paris, saisi par l'association Diderot Transparence, a annulé ces arrêtés ; que par un arrêt du 16 février 2015, la cour administrative d'appel de Paris a rejeté les requêtes introduites par la société Udicité, l'Université Paris Diderot - Paris 7 et le ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche contre ce jugement ; que la société Udicité et l'Université Paris Diderot - Paris 7 se pourvoient en cassation contre cet arrêt ; qu'il y a lieu de joindre leurs pourvois pour statuer par une seule décision ;<br/>
<br/>
              Sur la régularité de l'arrêt :<br/>
<br/>
              2. Considérant, d'une part, que si la société Udicité soutient que l'association Diderot Transparence se serait bornée, dans ses écritures d'appel, à soutenir que la mention, figurant dans les dossiers de demande et reprise par le préfet de police, selon laquelle les niveaux supérieurs des bâtiments M5B2 et M6A1 étaient inaccessibles au public était contredite par leur utilisation effective, postérieurement à leur ouverture au public, il ressort des pièces du dossier soumis aux juges du fond que l'association faisait également valoir que certains des locaux dont la réalisation était prévue à ces étages avaient par nature vocation à accueillir des personnes autres que le personnel de l'université ; que, par suite, le moyen tiré de ce que la cour aurait méconnu l'article R. 611-7 du code de justice administrative en relevant d'office un moyen qui n'aurait pas été préalablement communiqué aux parties, ne peut qu'être écarté ;<br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes de l'article R. 611-1 du code de justice administrative : " (...) La requête, le mémoire complémentaire annoncé dans la requête et le premier mémoire de chaque défendeur sont communiqués aux parties avec les pièces jointes (...). / Les répliques, autres mémoires et pièces sont communiqués s'ils contiennent des éléments nouveaux " ; qu'il ressort des pièces du dossier soumis aux juges du fond que le troisième mémoire en défense de l'association Diderot transparence ne comportait aucun élément nouveau ; que, par suite, en s'abstenant de communiquer ce mémoire à la société requérante, la cour n'a pas violé les dispositions précitées ; qu'elle n'a pas davantage méconnu les stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, qui, contrairement à ce que soutient la société Udicité, n'imposent pas aux juridictions de communiquer toutes les productions des parties, y compris celles qui sont insusceptibles d'avoir une influence sur la solution du litige ;<br/>
<br/>
              Sur le bien-fondé de l'arrêt :<br/>
<br/>
              4. Considérant qu'aux termes de l'article L. 111-18-3 du code de la construction et de l'habitation : " L'ouverture d'un établissement recevant du public est subordonnée à une autorisation délivrée par l'autorité administrative après contrôle du respect des dispositions de l'article L. 111-7 " ; qu'aux termes de l'article R. 111-19-29 de ce code : " L'autorisation d'ouverture prévue à l'article L. 111-8-3 est délivrée au nom de l'Etat par l'autorité définie à l'article R. 111-19-13 : a) Au vu de l'attestation établie en application de l'article R. 111-19-27, lorsque les travaux ont fait l'objet d'un permis de construire ;... c) Après avis de la commission de sécurité compétente, en application des articles R. 123-45 et R. 123-46 " ; qu'aux termes de l'article R. 123-45 de ce code, dans sa version alors en vigueur : " Avant toute ouverture des établissements au public ainsi qu'avant la réouverture des établissements fermés pendant plus de dix mois, il est procédé à une visite de réception par la commission. Celle-ci propose les modifications de détail qu'elle tient pour nécessaires " ;<br/>
<br/>
              5. Considérant qu'aux termes de l'article R. 123-2 du code de la construction et de l'habitation : " Pour l'application du présent chapitre, constituent des établissements recevant du public tous bâtiments, locaux et enceintes dans lesquels des personnes sont admises, soit librement, soit moyennant une rétribution ou une participation quelconque, ou dans lesquels sont tenues des réunions ouvertes à tout venant ou sur invitation, payantes ou non. / Sont considérées comme faisant partie du public toutes les personnes admises dans l'établissement à quelque titre que ce soit en plus du personnel " ; qu'aux termes de l'article GE1 du règlement de sécurité du 25 juin 1980 : " ... Sauf indications contraires, les dispositions du présent livre, relatives aux aménagements et installations techniques, ne s'appliquent qu'aux locaux ouverts au public. Les locaux et dégagements non accessibles au public doivent faire l'objet d'un examen spécial de la commission de sécurité. Selon leur importance, leur destination et leur disposition par rapport aux parties de l'établissement accessibles au public, la commission détermine les dangers qu'ils présentent pour le public et propose éventuellement les mesures de sécurité jugées nécessaires " ;<br/>
<br/>
              6. Considérant que la cour administrative d'appel a retenu qu'en dépit de la mention contraire figurant dans les avis de la commission de sécurité du 29 août 2012 favorables à l'ouverture au public des deux bâtiments, il ressortait des pièces des dossiers de demande d'autorisation d'ouverture au public que chacun des niveaux du troisième au huitième étage du bâtiment M6A1 et du quatrième au huitième étage du bâtiment M5B2 comportait des locaux devant être regardés comme ouverts au public au sens de l'article R 123-2 du code de la construction dès lors qu'ils étaient destinés à accueillir des personnes admises dans l'établissement en plus du personnel de l'université ou assimilé, tels que des étudiants ; qu'elle a en conséquence jugé que la commission avait fait reposer sur des faits inexacts l'appréciation des mesures de sécurité qu'il lui appartenait de recommander en application des dispositions précitées de l'article GE1 du règlement de sécurité du 25 juin 1980 ; qu'elle a pour ce motif confirmé l'annulation des arrêtés par lesquels le préfet de police, au vu de l'avis de la commission, avait autorisé l'ouverture au public des bâtiments universitaires M5B2 et M6A1 ;   <br/>
<br/>
              7. Considérant que, pour juger que certains des locaux des niveaux supérieurs des deux bâtiments étaient accessibles au public, la cour s'est exclusivement référée à leur destination, telle que mentionnée dans les dossiers de demande d'ouverture au public ; qu'en mentionnant " que les secrétariats pédagogiques des unités de formation et de recherche sont régulièrement visités par des étudiants ", la cour a entendu faire référence non à l'utilisation effective des locaux une fois l'autorisation délivrée mais à la destination normale de tels bureaux ; que par suite, elle n'a pas entaché son arrêt d'erreur de droit ;<br/>
<br/>
              8. Considérant qu'en se référant aux dispositions de l'article R. 123-2 du code de l'urbanisme pour déterminer si les locaux étaient accessibles au public au sens de l'article GE1 du règlement de sécurité, la cour n'a pas commis d'erreur de droit ; qu'en retenant qu'au sens de ces dispositions, les étudiants ne faisaient pas partie du personnel de l'université et ne pouvaient lui être assimilés, la cour en a fait une exacte application ; qu'elle n'a pas dénaturé les faits de l'espèce en estimant qu'eu égard à la destination normale des secrétariats pédagogiques et bibliothèques de recherche, et alors même qu'un badge serait exigé pour y accéder, les locaux correspondants avaient vocation à être fréquentés habituellement par des personnes ne faisant pas partie du personnel de l'université ;<br/>
<br/>
              9. Considérant qu'après avoir constaté que l'appréciation portée par la commission sur les mesures nécessaires pour assurer la sécurité du public reposait sur des faits inexacts en ce qui concernait l'accessibilité au public de plusieurs des étages des bâtiments en cause, la cour a pu, sans commettre d'erreur de droit, regarder les arrêtés préfectoraux comme étant, de ce seul fait, entachés d'illégalité ; <br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède que les requêtes de la société Udicité et de l'Université Paris Diderot - Paris 7 doivent être rejetées ; <br/>
<br/>
              11. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'association Diderot Transparence une somme au titre des frais exposés par la société Udicité et de l'Université Paris Diderot - Paris 7 et non compris dans les dépens ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Udicité et de l'Université Paris Diderot - Paris 7 une somme de 3 000 euros chacune au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les pourvois de la société Udicité et de l'Université Paris Diderot - Paris 7 sont rejetés.<br/>
<br/>
Article 2 : La société Udicité et l'Université Paris Diderot - Paris 7 verseront à l'association Diderot Transparence une somme de 3 000 euros chacune au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société Udicité, à l'Université Paris Diderot - Paris 7, à l'association Diderot Transparence et au préfet de police de Paris.<br/>
Copie en sera adressée à la FNATH - association des accidentés de la vie, à l'association Treize Ecolo, à la Fédération des syndicats Sud Etudiants, à M. C...B..., à M. A...E..., au Comité anti-amiante Jussieu, à Mme F...D..., au ministre de l'éducation nationale, à la ministre de l'enseignement supérieur, de la recherche et de l'innovation, au ministre de la cohésion des territoires et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. PROCÉDURE CONSULTATIVE. CONSULTATION OBLIGATOIRE. - AVIS DE LA COMMISSION DE SÉCURITÉ SUR LE PROJET D'AUTORISATION D'OUVERTURE D'UN ÉTABLISSEMENT RECEVANT DU PUBLIC (ART. R. 111-19-29 DU CCH) - CARACTÈRE DE GARANTIE AU SENS DE LA JURISPRUDENCE DANTHONY [RJ1] - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">49-05-003 POLICE. POLICES SPÉCIALES. - AVIS DE LA COMMISSION DE SÉCURITÉ (ART. R. 111-19-29 DU CCH) - CARACTÈRE DE GARANTIE AU SENS DE LA JURISPRUDENCE DANTHONY [RJ1] - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-03-02-02 L'avis de la commission de sécurité mentionnée à l'article R. 119-19-29 du code de la construction et de l'habitation (CCH), préalable à l'autorisation d'ouverture d'un établissement recevant du public, constitue une garantie au sens de la jurisprudence Danthony.</ANA>
<ANA ID="9B"> 49-05-003 L'avis de la commission de sécurité mentionnée à l'article R. 119-19-29 du code de la construction et de l'habitation, préalable à l'autorisation d'ouverture d'un établissement recevant du public, constitue une garantie au sens de la jurisprudence Danthony.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 23 décembre 2011, M.,et autres, n° 335033, p. 649.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
