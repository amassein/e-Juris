<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027626002</ID>
<ANCIEN_ID>JG_L_2013_06_000000342994</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/62/60/CETATEXT000027626002.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 28/06/2013, 342994, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-06-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>342994</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLEZ</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:342994.20130628</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 7 septembre et 7 décembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SA Cabinet Roux, dont le siège est parc de l'Angevinière, 15, boulevard Marcel-Paul, allée du Bagatelle, bâtiment C à Saint-Herblain (44800) ; la société demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08PA04431 du 18 juin 2010 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant à l'annulation du jugement du 23 juin 2008 du tribunal administratif de Paris rejetant sa demande en décharge de la cotisation de taxe professionnelle mise à sa charge au titre de l'année 1998 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boullez, avocat de la SA Cabinet Roux ;<br/>
<br/>
<br/>
<br/>1. Considérant, en premier lieu, qu'aux termes de l'article 1658 du code général des impôts : " Les impôts directs (...) sont recouvrés en vertu de rôles rendus exécutoires par arrêté du préfet. " ; que ces rôles doivent comporter l'identification du contribuable, ainsi que le total par nature d'impôt et par année des sommes à acquitter ;<br/>
<br/>
              2. Considérant qu'en jugeant que l'article 1658 du code général des impôts n'imposait pas à l'administration de préciser, dans le rôle, les bases d'imposition, la date d'exigibilité de l'impôt ainsi que la date limite de paiement, la cour n'a pas commis d'erreur de droit ; que les paragraphes de l'instruction codificatrice CP n° 95-027-A du 1er mars 1995, modifiée par l'instruction codificatrice CP n° 98-010-A1 du 12 janvier 1998, relatifs aux mentions que doit comporter le rôle, en tant qu'ils concernent la procédure d'établissement de l'impôt, ne sont pas, en tout état de cause, susceptibles d'être invoqués sur le fondement de l'article L. 80 A du livre des procédures fiscales ; que ces mêmes paragraphes, en tant qu'ils concernent la procédure de recouvrement de l'impôt, ne peuvent en outre être invoqués dans le cadre du présent litige d'assiette ; qu'il convient par suite d'écarter, par ces motifs qui doivent être substitués à ceux retenus par la cour, le moyen soulevé devant elle et tiré de l'invocation de la doctrine administrative ;<br/>
<br/>
              3. Considérant, en second lieu, qu'aux termes du dernier alinéa du I de l'article 1478 du code général des impôts : " Lorsqu'au titre d'une année une cotisation de taxe professionnelle a été émise au nom d'une personne autre que le redevable légal de l'impôt, l'imposition de ce dernier, au titre de la même année, est établie au profit de l'État dans la limite du dégrèvement accordé au contribuable imposé à tort " ; que ces dispositions ont pour seul objet de déterminer les modalités d'attribution du produit fiscal issu d'une cotisation de taxe professionnelle établie par voie de rôle au nom du contribuable après un dégrèvement accordé à une personne autre que le redevable légal, imposée à tort ; qu'elles sont sans incidence sur la cotisation exigible du redevable légal et ne sont, dès lors, pas utilement invocables par celui-ci à l'appui d'une contestation d'assiette relative à sa propre cotisation ; que la cour a par suite pu, sans entacher son arrêt d'irrégularité, s'abstenir de répondre au moyen inopérant tiré de l'invocation de ces dispositions ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que le pourvoi de la SA Cabinet Roux ne peut qu'être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de la SA Cabinet Roux est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la SA Cabinet Roux et au ministre délégué auprès du ministre de l'économie et des finances, chargé du budget.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
