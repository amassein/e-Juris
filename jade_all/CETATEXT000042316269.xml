<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042316269</ID>
<ANCIEN_ID>JG_L_2020_03_000000439553</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/31/62/CETATEXT000042316269.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 18/03/2020, 439553, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-03-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439553</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:439553.20200318</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
              Par une requête, enregistrée le 13 mars 2020 au secrétariat du contentieux du Conseil d'Etat, l'Association française de la cystite interstitielle et Mme B... A... demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative : <br/>
              1°) de suspendre l'exécution de la décision du 2 août 2019 du ministre des solidarités et de la santé et du ministre de l'action et des comptes publics refusant l'inscription de la spécialité Elmiron sur la liste des médicaments remboursables ;<br/>
<br/>
              2°) d'enjoindre au ministre des solidarités et de la santé et au ministre de l'action et des comptes publics de réexaminer la demande d'inscription dans un délai d'un mois ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elles soutiennent que :<br/>
              - la condition d'urgence est remplie du fait de l'atteinte portée aux intérêts des personnes atteintes du syndrome de la vessie douloureuse et à un intérêt de santé publique ;<br/>
              - il existe un doute sérieux quant à la légalité de la décision contestée ; en effet :<br/>
              - elle est entachée d'une erreur de droit en ce qu'elle applique les dispositions du I de l'article R. 163-5 du code de la sécurité sociale à une hypothèse dans laquelle le médicament apporte un service médical rendu et qu'il n'existe aucun autre médicament indiqué pour l'affection qu'il a vocation à traiter ;<br/>
              - elle est entachée de griefs d'erreurs de droit et d'erreur manifeste d'appréciation, en ce qu'elle a exclu toute amélioration du service médical rendu ;<br/>
              - les ministres ont entaché leur décision d'erreurs de droit, en ne respectant pas la méthodologie qui aurait dû être la leur, pour se déterminer sur l'amélioration du service médical rendu ;<br/>
              - les ministres ont commis une erreur manifeste d'appréciation en excluant toute amélioration du service médical rendu ;<br/>
              - la décision est entachée d'une erreur de droit, et à tout le moins d'une erreur manifeste d'appréciation, en ce qu'elle a considéré que l'inscription de l'Elmiron sur la liste mentionnée au premier alinéa de l'article L. 162-17 du code de la sécurité sociale n'engendrerait pas d'économie dans le coût du traitement médicamenteux ;<br/>
              - la décision est entachée d'une erreur de droit en ce qu'elle a considéré que le prix proposé par les sociétés n'est pas justifié ;<br/>
              - la décision méconnaît le droit fondamental à la protection de la santé et l'objectif d'égal accès à la santé prévu par l'article 1110-1 du code de la santé publique.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". L'article L. 522-3 de ce code prévoit que le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Il résulte des dispositions de l'article L. 521-1 du code de justice administrative que le prononcé de la suspension d'un acte administratif est subordonné notamment à une condition d'urgence. L'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celui-ci porte atteinte de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. L'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire.<br/>
<br/>
              3. L'association requérante et Mme A... demandent la suspension de l'exécution de la décision du 2 août 2019 du ministre des solidarités et de la santé et du ministre de l'action et des comptes publics refusant l'inscription de la spécialité Elmiron sur la liste des médicaments remboursables et d'enjoindre au ministre des solidarités et de la santé et au ministre de l'action et des comptes publics de réexaminer la demande d'inscription dans un délai d'un mois. Pour caractériser l'urgence qu'il y aurait à suspendre l'exécution de cette décision, elles font valoir l'intérêt que présenterait ce traitement pour soulager le syndrome de la vessie douloureuse. Toutefois, il n'est pas établi par les pièces du dossier que le traitement en cause présenterait un intérêt médical suffisant, de sorte que la décision attaquée serait susceptible de porter à la situation des personnes représentées une atteinte suffisamment immédiate pour caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, une mesure de suspension soit prononcée. <br/>
<br/>
              4. Il résulte de ce qui précède que la condition d'urgence ne peut pas être regardée comme remplie. Par suite, il y a lieu de rejeter la requête, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, selon la procédure prévue par l'article L. 522-3 du même code.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de l'Association française de la cystite interstitielle et de Mme A... est rejetée.<br/>
<br/>
Article 2 : La présente ordonnance sera notifiée à l'Association française de la cystite interstitielle et à Mme B... A....<br/>
Copie en sera adressée au ministre des solidarités et de la santé et au ministre de l'action et des comptes publics. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
