<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034099144</ID>
<ANCIEN_ID>JG_L_2017_02_000000394832</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/09/91/CETATEXT000034099144.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 27/02/2017, 394832, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-02-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394832</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLEZ ; SCP DE NERVO, POUPET</AVOCATS>
<RAPPORTEUR>Mme Marie Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:394832.20170227</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a formé opposition devant le tribunal administratif de Nice à la contrainte délivrée à son encontre le 15 février 2013 par le directeur de Pôle emploi Provence-Alpes-Côte d'Azur pour la récupération de la somme de 10 652,29 euros qui lui avait été versée au titre de l'allocation de solidarité spécifique. Par un jugement n° 1300749 du 6 octobre 2015, le tribunal administratif de Nice a rejeté son opposition.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 25 novembre 2015, 22 février 2016 et 14 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Nice du 6 octobre 2015 ;<br/>
<br/>
              2°) de mettre à la charge de Pôle emploi la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la sécurité sociale ;<br/>
              - le code du travail ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Sirinelli, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Nervo, Poupet, avocat de MmeB..., et à la SCP Boullez, avocat de Pôle emploi.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite de la création de son entreprise, Mme B...s'est vu accorder, à compter du 1er octobre 2008, l'aide à la création ou à la reprise d'une entreprise, tout en bénéficiant de l'allocation de solidarité spécifique jusqu'au 31 août 2011. Le 15 février 2013, le directeur de Pôle emploi Provence-Alpes-Côte d'Azur a délivré une contrainte à son encontre, afin d'obtenir le remboursement d'une somme de 10 642,15 euros qu'il estimait lui avoir été indûment versée au titre de l'allocation de solidarité spécifique. Mme B...a formé opposition devant le tribunal administratif de Nice. Elle se pourvoit en cassation contre le jugement du 6 octobre 2015 par lequel ce tribunal a rejeté son opposition.  <br/>
<br/>
              2. Aux termes de l'article L. 5141-1 du code du travail, dans sa rédaction applicable au litige : " Peuvent bénéficier des exonérations de charges sociales prévues à l'article L. 161-1-1 du code de la sécurité sociale, lorsqu'elles créent ou reprennent une activité économique, industrielle, commerciale, artisanale, agricole ou libérale, soit à titre individuel, soit sous la forme d'une société, à condition d'en exercer effectivement le contrôle, ou entreprennent l'exercice d'une autre profession non salariée : (...) 3° Les bénéficiaires de l'allocation de solidarité spécifique ou du revenu de solidarité active (...) ". L'article L. 5141-3 du même code, dans sa rédaction alors applicable, prévoit au titre du " maintien d'allocations " : " Les personnes admises au bénéfice de l'article L. 5141-1 et qui perçoivent l'allocation de solidarité spécifique (...) reçoivent une aide de l'Etat, attribuée pour une durée courant à compter de la date de création ou de reprise d'une entreprise ". En outre, selon l'article R. 5141-1 du même code, dans sa rédaction alors applicable : " Les aides destinées aux personnes qui créent ou reprennent une entreprise, ou qui entreprennent l'exercice d'une autre profession non salariée, prévues au présent chapitre, comprennent : / 1° L'exonération de cotisations sociales prévue à l'article L. 161-1-1 du code de la sécurité sociale (...) / 3° Le versement par l'Etat, aux bénéficiaires des exonérations prévues au 1°, effectué conformément aux dispositions de l'article L. 5141-3. Pour les personnes admises au bénéfice de ces exonérations au cours de leur période d'indemnisation au titre de l'allocation d'assurance, le bénéfice de l'allocation de solidarité spécifique prévue à l'article L. 5423-1 est maintenu jusqu'au terme du bénéfice de ces exonérations (...) ". L'aide de l'Etat prévue à l'article L. 5141-3, qui prend la forme d'un maintien du versement de l'allocation de solidarité spécifique au taux plein, est, aux termes de l'article R. 5141-28 du même code, " attribuée pour une durée d'un an à compter de la date de création ou de reprise d'une entreprise ". <br/>
<br/>
              3. Le tribunal administratif de Nice a relevé, d'une part, que, par la contrainte délivrée le 15 février 2013, objet de l'opposition formée devant lui, Pôle emploi réclamait à Mme B...le remboursement d'un trop-perçu d'allocation de solidarité spécifique correspondant à la période allant du 16 janvier 2009 au 31 août 2011. Il a jugé, d'autre part, eu égard à la date de création de sa société par MmeB..., que Pôle emploi était seulement fondé à lui demander le remboursement de l'allocation de solidarité spécifique qui lui avait été versée à compter du 1er octobre 2009. Dès lors, en en déduisant que la demande de Mme B... devait être rejetée dans sa totalité, le tribunal a entaché son jugement d'une contradiction de motifs.<br/>
<br/>
              4. Il suit de là que Mme B...est fondée à demander l'annulation du jugement qu'elle attaque. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens de son pourvoi.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de Pôle emploi le versement à Mme B...de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. En revanche, les dispositions de cet article font obstacle à ce qu'il soit fait droit aux conclusions de Pôle emploi présentées au même titre.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Nice du 6 octobre 2015 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Nice.<br/>
Article 3 : Pôle emploi versera à Mme B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de Pôle emploi présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme A...B...et à Pôle emploi.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
