<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027345134</ID>
<ANCIEN_ID>JG_L_2013_04_000000343073</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/34/51/CETATEXT000027345134.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 22/04/2013, 343073, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-04-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>343073</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:343073.20130422</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée les 6 et 8 septembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentée par l'Association des producteurs indépendants (API), dont le siège est 15, rue de Berri à Paris (75008), représentée par son président ; l'Association des producteurs indépendants demande au Conseil d'Etat d'annuler pour excès de pouvoir la délibération n° 2010-4 du 16 février 2010 du Conseil supérieur de l'audiovisuel (CSA) relative au placement de produit dans les programmes des services de télévision, ainsi que sa décision rejetant son recours gracieux contre cette délibération ; <br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la directive 89/552/CEE du Conseil, du 3 octobre 1989 visant à la coordination de certaines dispositions législatives, réglementaires et administratives des Etats membres relatives à l'exercice d'activités de radiodiffusion télévisuelle ;<br/>
<br/>
              Vu la directive 2007/65/CE du Parlement européen et du Conseil du 11 décembre 2007 modifiant la directive 89/552/CEE du Conseil visant à la coordination de certaines dispositions législatives, réglementaires et administratives des Etats membres relatives à l'exercice d'activités de radiodiffusion télévisuelle ; <br/>
<br/>
              Vu la directive 2010/13/UE du Parlement européen et du Conseil du 10 mars 2010 visant à la coordination de certaines dispositions législatives, réglementaires et administratives des Etats membres relatives à la fourniture de services de médias audiovisuels ;<br/>
<br/>
              Vu la loi n° 86-1067 du 30 septembre 1986 ; <br/>
<br/>
              Vu la loi n° 2009-258 du 5 mars 2009 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, Maître des Requêtes, <br/>
<br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article 3 octies de la directive 2007/65/CE du Parlement européen et du Conseil du 11 décembre 2007, issu de la directive 2007/65/CE du 11 décembre 2007, et dont les dispositions sont aujourd'hui reprises à l'article 11 de la directive 2010/13/UE du 10 mars 2010 : " 1. Le placement de produit est interdit. / 2. Par dérogation au paragraphe 1, le placement de produit est admissible à moins qu'un Etat membre n'en décide autrement : / - dans les oeuvres cinématographiques, films et séries réalisés pour des services de médias audiovisuels... " ; que la loi du 5 mars 2009 relative à la communication audiovisuelle et au nouveau service public de la télévision a inséré dans la loi du 30 septembre 1986 relative à la liberté de communication un article 14-1 aux termes duquel : " Le Conseil supérieur de l'audiovisuel fixe les conditions dans lesquelles les programmes des services de communication audiovisuelle, et notamment les vidéomusiques, peuvent comporter du placement de produit. / Le Conseil supérieur de l'audiovisuel veille à ce que les programmes comportant du placement de produit respectent les exigences suivantes : / 1° Leur contenu et, dans le cas de la radiodiffusion télévisuelle, leur programmation ne doivent en aucun cas être influencés de manière à porter atteinte à la responsabilité et à l'indépendance éditoriale de l'éditeur de services de médias ; / 2° Ils n'incitent pas directement à l'achat ou à la location des produits ou services d'un tiers et ne peuvent en particulier comporter des références promotionnelles spécifiques à ces produits ou services ; / 3° Ils ne mettent pas en avant de manière injustifiée le produit en question ; / 4° Les téléspectateurs sont clairement informés de l'existence d'un placement de produit. Les programmes comportant du placement de produit sont identifiés de manière appropriée au début et à la fin de leur diffusion, ainsi que lorsqu'un programme reprend après une interruption publicitaire, afin d'éviter toute confusion de la part du téléspectateur " ; <br/>
<br/>
              2. Considérant que, sur le fondement de ces dispositions législatives, le Conseil supérieur de l'audiovisuel a adopté le 16 février 2010 une délibération relative au placement de produit dans les programmes des services de télévision, publiée au Journal officiel de la République française du 5 mars 2010 ; que le IV de cette délibération autorise le placement de produit dans les oeuvres cinématographiques diffusées par les services de télévision, les fictions audiovisuelles et les vidéomusiques, sauf lorsqu'ils sont destinés aux enfants, et l'interdit dans les autres programmes de télévision ; que le V interdit le placement du tabac, de l'alcool, des médicaments et des préparations pour nourrissons, ainsi que celui des armes à feu et des munitions ; que le VI rappelle les prescriptions des 1°, 2° et 3° de l'article 14-1 de la loi du 30 septembre 1986 ; que le VII fixe les modalités de l'information du public mentionnée au 4° du même article, en prévoyant que les programmes " comportant du placement de produit " sont identifiés par un pictogramme apparaissant " pendant une minute au début du programme, pendant une minute après chaque interruption publicitaire et, à la fin du programme, pendant toute la durée du générique " ; que, dans sa rédaction antérieure à une modification résultant d'une délibération du 24 juillet 2012, le VIII disposait que : " Un contrat définit les relations économiques entre l'annonceur, le producteur du programme et l'éditeur du service de télévision lorsque le placement de produit est effectué dans un programme produit, coproduit ou préacheté par l'éditeur " ; que l'Association des producteurs indépendants demande l'annulation de la délibération, dont elle soutient qu'elle est illégale en tant qu'elle s'applique aux oeuvres cinématographiques diffusées par les services de télévision ;<br/>
<br/>
              En ce qui concerne la compétence du Conseil supérieur de l'audiovisuel : <br/>
<br/>
              3. Considérant, en premier lieu, que les dispositions précitées de l'article 14-1 de la loi du 30 septembre 1986 autorisent le Conseil supérieur de l'audiovisuel à réglementer la pratique du placement de produit dans l'ensemble des programmes des services de communication audiovisuelle ; que l'association requérante n'est dès lors pas fondée à soutenir que cette autorité était incompétente pour prendre la délibération attaquée, en tant qu'elle s'applique aux oeuvres cinématographiques diffusées par ces services ; <br/>
<br/>
              4. Considérant, en deuxième lieu, que, par le paragraphe VIII de la délibération attaquée, le Conseil supérieur de l'audiovisuel a prévu que, lorsque le placement de produit serait effectué dans un programme produit, coproduit ou préacheté par l'éditeur d'un service de télévision, les relations économiques entre cet éditeur, l'annonceur et le producteur du programme seraient définies par contrat ; que, dans la mesure où la conclusion d'un tel contrat est de nature à garantir l'information de l'éditeur du service sur l'existence d'un placement de produit dans le programme qu'il coproduit ou préachète et à le mettre en mesure de vérifier que ce programme pourra être diffusé dans le respect des dispositions en vigueur, elle figure au nombre des conditions que la loi du 5 mars 2009 autorisait le Conseil supérieur de l'audiovisuel à édicter ; que, par ailleurs, la délibération attaquée n'impose aucun contenu au contrat, dont il appartient aux seules parties de convenir ; que, dans ces conditions, l'association requérante n'est pas fondée à soutenir qu'en faisant figurer cette disposition dans sa délibération, le Conseil supérieur de l'audiovisuel aurait excédé la compétence qu'il tient de l'article 14-1 de la loi du 30 septembre 1986 ;<br/>
<br/>
              5 Considérant, en troisième lieu, qu'en habilitant le Conseil supérieur de l'audiovisuel à fixer les conditions dans lesquelles les programmes des services de communication audiovisuels pouvaient, à titre dérogatoire, comporter du placement de produit, la loi du 5 mars 2009 lui a conféré la faculté de l'interdire pour certains produits, à l'attention de certains publics ou dans certains programmes ;  <br/>
<br/>
              En ce qui concerne la légalité interne de la délibération attaquée :<br/>
<br/>
              6. Considérant que, en prévoyant que l'information du public sur l'existence d'un placement de produit serait assurée par l'apparition d'un pictogramme au cours de la diffusion, pendant une durée limitée à quelques minutes, la délibération attaquée qui, selon les termes mêmes du paragraphe VII, s'applique " quelles que soient l'origine et les conditions de production du programme ", n'introduit aucune discrimination entre les oeuvres cinématographiques françaises et étrangères ; que l'apparition brève et intermittente d'un pictogramme n'est pas de nature à porter atteinte à l'intégrité des oeuvres ; qu'en retenant cette modalité d'information du public pour les oeuvres cinématographiques, comme pour les autres programmes pour lesquels le placement de produit est autorisé, le Conseil supérieur de l'audiovisuel n'a pas commis d'erreur manifeste d'appréciation ; que ne révèle pas davantage l'existence d'une erreur manifeste la circonstance que les autres dispositions de la délibération sont rendues applicables aux oeuvres cinématographiques et que leur application n'est pas écartée au terme d'un certain délai à compter de la sortie de l'oeuvre ; <br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin de statuer sur les fins de non-recevoir opposées par le Conseil supérieur de l'audiovisuel, l'association requérante n'est pas fondée à demander l'annulation de la délibération qu'elle attaque ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'Association des producteurs indépendants est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à l'Association des producteurs indépendants et au Conseil supérieur de l'audiovisuel. <br/>
Copie pour information en sera adressée à la ministre de la culture et de la communication. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
