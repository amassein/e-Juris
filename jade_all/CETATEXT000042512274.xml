<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042512274</ID>
<ANCIEN_ID>JG_L_2020_10_000000445059</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/51/22/CETATEXT000042512274.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 23/10/2020, 445059, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445059</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:445059.20201023</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 3 octobre 2020 au secrétariat du contentieux du Conseil d'Etat, Mme A... B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution du décret n° 2020-1098 du 29 août 2020 pris pour l'application de l'article 20 de la loi n° 2020-473 du 25 avril 2020 de finances rectificatives pour 2020 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - sa requête est recevable ;<br/>
              - la condition d'urgence est remplie eu égard au risque de développement d'une forme grave de covid-19 par les personnes vulnérables au sens du décret du 5 mai 2020 qui ont dû reprendre leur activité le 31 août 2020 ;<br/>
              - il est porté une atteinte grave et manifestement illégale au droit à la santé et au droit à la vie ;<br/>
              - la légalité du décret litigieux doit être appréciée à la date de la décision du juge et non à la date de son adoption ;<br/>
              - le décret litigieux méconnaît l'article 20 de la loi du 25 avril 2020 en ce qu'il limite indûment la liste des personnes vulnérables présentant un risque de développer une forme grave d'infection au virus SARS-CoV-2 et est entaché d'une erreur manifeste d'appréciation en ce qu'il ne qualifie pas certaines catégories de personnes comme vulnérables ; <br/>
              - il est entaché d'une erreur manifeste d'appréciation en ce qu'il met fin dès le 31 août 2020 au chômage partiel des salariés du secteur privé qui partagent le domicile d'une personne vulnérable ;<br/>
              - il méconnaît les articles 221-6 et 222-19 du code pénal réprimant respectivement l'homicide involontaire et les blessures involontaires graves ;<br/>
              - il méconnaît le principe de sécurité juridique en ce qu'il impose un retour au travail à de nombreux salariés vulnérables ou partageant le domicile de personnes vulnérables dès le 31 août 2020, sans prévoir de délai d'adaptation suffisant.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code pénal ;<br/>
              - la loi n° 2020-473 du 25 avril 2020 ;<br/>
              - le décret n° 2020-521 du 5 mai 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures. ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
              Sur le cadre juridique du litige :<br/>
<br/>
<br/>
              2. Le I de l'article 20 de la loi du 25 avril 2020 de finances rectificative pour 2020 dispose que : " Sont placés en position d'activité partielle les salariés de droit privé se trouvant dans l'impossibilité de continuer à travailler pour l'un des motifs suivants :/ - le salarié est une personne vulnérable présentant un risque de développer une forme grave d'infection au virus SARS-CoV-2, selon des critères définis par voie réglementaire ;/ - le salarié partage le même domicile qu'une personne vulnérable au sens du deuxième alinéa du présent I ; (...) ", le III de cet article précisant que : " (...) / Pour les salariés mentionnés aux deuxième et troisième alinéas du (...) I, celui-ci s'applique jusqu'à une date fixée par décret et au plus tard le 31 décembre 2020./(...) Les modalités d'application du présent article sont définies par voie réglementaire. " Pour l'application de ces dispositions, le décret du 5 mai 2020 ci-dessus visé a défini les critères permettant d'identifier les salariés vulnérables présentant un risque de développer une forme grave d'infection au virus SARS-CoV-2 et pouvant être placés en activité partielle au titre des dispositions précitées de l'article 20 de la loi du 25 avril 2020. Puis, par le décret du 29 août 2020 dont la requérante demande la suspension, le Premier ministre a modifié ces critères à compter du 1er septembre 2020, fixé au 31 août 2020 la date jusqu'à laquelle le I de l'article 20 de la loi du 25 avril 2020 s'applique aux salariés partageant le même domicile qu'une personne vulnérable, et abrogé en conséquence le décret du 5 mai 2020 à compter du 1er septembre 2020, ces dates étant différées dans les départements de Guyane et de Mayotte jusqu'à la fin de l'état d'urgence sanitaire.<br/>
<br/>
<br/>
              Sur les conclusions de la requête :<br/>
<br/>
              3. En premier lieu, postérieurement à l'introduction de la requête, par une ordonnance du 15 octobre 2020, le juge des référés du Conseil d'Etat a suspendu l'exécution des articles 2, 3 et 4 du décret du 29 août 2020. Il n'y a dès lors plus lieu de statuer sur les conclusions à fin de suspension de l'exécution des mêmes dispositions présentées par la requérante, qui ont perdu leur objet.<br/>
<br/>
              4. En deuxième lieu, il résulte des dispositions de l'article 20 de la loi du 25 avril 2020 citées au point 2 qu'il était loisible au Premier ministre, s'il estimait, avant même l'échéance du 31 décembre 2020, que la situation ne justifiait plus que les salariés vulnérables ou ceux cohabitant avec une personne vulnérable fussent placés en position d'activité partielle, y compris au motif que la prescription d'arrêts de travail de droit commun par un médecin au terme d'une appréciation de la situation de chaque salarié apparaîtrait désormais plus adéquate, de mettre fin à cette mesure. A ce titre, les moyens tirés de ce que le III de cet article aurait imposé qu'il ne soit mis fin à cette mesure qu'à une date identique pour les salariés vulnérables eux-mêmes et pour les salariés cohabitant avec une personne vulnérable ou de ce que le décret serait entaché d'erreur manifeste d'appréciation en ce qu'il a entièrement mis fin à la mesure pour les salariés cohabitant avec une personne vulnérable alors qu'il n'y a pas entièrement mis fin pour les salariés vulnérables ne sont pas de nature à caractériser une atteinte grave et manifestement illégale à une liberté fondamentale, eu égard notamment à la différence de situation qui existe, entre les salariés eux-mêmes vulnérables et ceux cohabitant seulement avec une personne vulnérable, quant au risque créé pour les personnes vulnérables, direct pour les uns, indirect pour les autres, du fait de l'exposition des salariés concernés à une contamination par le virus à l'occasion du travail. Les conclusions de la requérante tendant à la suspension de l'exécution de l'article 1er du même décret ne peuvent par suite, manifestement qu'être rejetées, selon la procédure prévue à l'article L. 522-3 du code de justice administrative.<br/>
<br/>
              5. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions de la requête à fin de suspension de l'exécution des articles 2, 3 et 4 du décret du 29 août 2020.<br/>
Article 2 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 3 : La présente ordonnance sera notifiée à Mme A... B....<br/>
Copie en sera adressée au Premier ministre, au ministre des solidarités et de la santé et à la ministre du travail, de l'emploi et de l'insertion.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
