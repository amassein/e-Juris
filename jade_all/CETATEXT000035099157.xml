<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035099157</ID>
<ANCIEN_ID>JG_L_2017_06_000000407711</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/09/91/CETATEXT000035099157.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 30/06/2017, 407711, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407711</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER ; SCP LEVIS</AVOCATS>
<RAPPORTEUR>Mme Dorothée Pradines</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:407711.20170630</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La SARL New Coiffure a demandé au tribunal administratif de Paris, d'une part, de réduire la contribution spéciale, mise à sa charge par la décision du 2 juin 2015 du directeur général de l'Office français de l'immigration et de l'intégration (OFII), à une somme de 7 020 euros en appliquant la minoration de 1 000 fois le taux horaire du minimum garanti ou, subsidiairement, à une somme de 14 040 euros correspondant à une minoration de 2 000 fois le taux horaire du minimum garanti et, d'autre part, de la décharger de la contribution forfaitaire représentative des frais de réacheminement à hauteur de 4 248 euros, mise à sa charge à la suite de la même décision. Par un jugement n° 1512652 du 17 février 2016, le tribunal administratif de Paris a déchargé la SARL New Coiffure de la contribution spéciale à concurrence de la somme de 10 530 euros.<br/>
              Par un arrêt n°s 16PA01152, 16PA01464 du 6 décembre 2016, la cour administrative d'appel de Paris a rejeté l'appel formé par la SARL New Coiffure et l'appel incident formé par l'OFII contre le jugement du tribunal administratif de Paris.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 7 février et 3 avril 2017 au secrétariat du contentieux du Conseil d'Etat, la SARL New Coiffure demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il rejette son appel ; <br/>
<br/>
              2°) de mettre à la charge de l'État la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment ses articles 88-1 et 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la directive 2009/52/CE du Parlement européen et du Conseil du 18 juin 2009 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile, notamment son article L. 626-1 ;<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dorothée Pradines, auditeur, <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Zribi et Texier, avocat de la Sarl New Coiffure et à la SCP Lévis, avocat de l'Office français de l'immigration et de l'intégration.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Eu égard à la teneur des écritures de la société requérante, la question doit être regardée comme dirigée contre le premier alinéa de l'article L. 626-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, qui dispose que : " Sans préjudice des poursuites judiciaires qui pourront être engagées à son encontre et de la contribution spéciale prévue à l'article L. 8253-1 du code du travail, l'employeur qui aura occupé un travailleur étranger en situation de séjour irrégulier acquittera une contribution forfaitaire représentative des frais de réacheminement de l'étranger dans son pays d'origine ".<br/>
<br/>
              3. Aux termes de l'article 88-1 de la Constitution : " La République participe à l'Union européenne constituée d'États qui ont choisi librement d'exercer en commun certaines de leurs compétences en vertu du traité sur l'Union européenne et du traité sur le fonctionnement de l'Union européenne, tels qu'ils résultent du traité signé à Lisbonne le 13 décembre 2007 ". En l'absence de mise en cause d'une règle ou d'un principe inhérent à l'identité constitutionnelle de la France, le Conseil constitutionnel juge qu'il n'est pas compétent pour contrôler la conformité aux droits et libertés que la Constitution garantit de dispositions législatives qui se bornent à tirer les conséquences nécessaires de dispositions inconditionnelles et précises d'une directive de l'Union européenne et qu'en ce cas, il n'appartient qu'au juge de l'Union européenne, saisi le cas échéant à titre préjudiciel, de contrôler le respect par cette directive des droits fondamentaux garantis par l'article 6 du Traité sur l'Union européenne.<br/>
<br/>
              4. En l'absence de mise en cause, à l'occasion d'une question prioritaire de constitutionnalité soulevée sur des dispositions législatives se bornant à tirer les conséquences nécessaires de dispositions précises et inconditionnelles d'une directive de l'Union européenne, d'une règle ou d'un principe inhérent à l'identité constitutionnelle de la France, une telle question n'est pas au nombre de celles qu'il appartient au Conseil d'Etat de transmettre au Conseil constitutionnel sur le fondement de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel.<br/>
<br/>
              5. D'une part, aux termes de l'article 3 de la directive 2009/52/CE du 18 juin 2009 prévoyant des normes minimales concernant les sanctions et les mesures à l'encontre des employeurs de ressortissants de pays tiers en séjour irrégulier : " 1. Les États membres interdisent l'emploi de ressortissants de pays tiers en séjour irrégulier. / 2. Les infractions à cette interdiction sont passibles des sanctions et des mesures fixées dans la présente directive. (...) " et aux termes de l'article 5 de cette directive " 1. Les États membres prennent les mesures nécessaires pour s'assurer que les violations de l'interdiction visée à l'article 3 sont passibles de sanctions effectives, proportionnées et dissuasives à l'encontre de l'employeur concerné. /  2. Les sanctions infligées en cas de violation de l'interdiction visée à l'article 3 comportent : / a) des sanctions financières dont le montant augmente en fonction du nombre de ressortissants de pays tiers employés illégalement ; et / b) le paiement des frais de retour des ressortissants de pays tiers employés illégalement dans les cas où une procédure de retour est engagée. Les États membres peuvent alternativement décider de refléter au moins les coûts moyens du retour dans les sanctions financières prises conformément au point a) (...) ". Ainsi, si les Etats membres ont toujours la faculté, en vertu de cette directive, de prévoir que le paiement des frais de retour des ressortissants de pays tiers employés illégalement fera l'objet d'une sanction distincte dans les cas où une procédure de retour est engagée ou sera intégré aux autres sanctions financières infligées en cas de violation de l'interdiction de l'emploi de ressortissants de pays tiers en séjour irrégulier, les dispositions du b) du 2. de l'article 5 de la directive sont inconditionnelles et précises en tant qu'elles imposent aux Etats membres que le paiement de ces frais de retour soit au nombre des sanctions prévues.<br/>
<br/>
              6. D'autre part, en l'espèce, les dispositions du premier alinéa de l'article L. 626-1 du code de l'entrée et du séjour des étrangers et du droit d'asile se bornent, dans les éléments qu'en conteste la société requérante, à assurer la transposition en droit interne des dispositions inconditionnelles et précises du b) du 2. de l'article 5 de la directive.<br/>
<br/>
              7. Enfin, la question prioritaire de constitutionnalité soulevée par la société requérante, laquelle se borne à invoquer les principes de proportionnalité et d'individualisation des peines qui découlent du principe de nécessité des peines garanti par l'article 8 de la Déclaration de 1789, ne met en cause aucune règle ni aucun principe inhérent à l'identité constitutionnelle de la France. <br/>
<br/>
              8. Ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que le premier alinéa de l'article L. 626-1 du code de l'entrée et du séjour des étrangers et du droit d'asile porte atteinte aux droits et libertés garantis par la Constitution doit être regardé comme n'étant pas de nature à permettre l'admission du pourvoi.<br/>
<br/>
              Sur les autres moyens du pourvoi :<br/>
<br/>
              9. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              10. Pour demander l'annulation de l'arrêt de la cour administrative d'appel de Paris qu'elle attaque, la société New Coiffure soutient, en outre, que :<br/>
              - la cour a commis une erreur de droit en faisant application de l'article L. 8253-1 du code du travail alors que celui-ci est contraire au principe non bis in idem garanti par l'article 4 du protocole additionnel n° 7 à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la cour a commis une erreur de droit en faisant application des dispositions de cet article et de celles de l'article L. 626-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, alors qu'il en résulte un cumul de sanctions pour un même fait, en méconnaissance du même principe ;<br/>
              - la cour a commis une erreur de droit en s'abstenant de rechercher si elle était de bonne foi et si cette bonne foi ne devait pas la conduire à écarter l'application des articles L. 8263-1 du code du travail et L. 626-1 du code de l'entrée et du séjour des étrangers et du droit d'asile ou à moduler le montant de la contribution spéciale.<br/>
<br/>
              11. Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la SARL New Coiffure.<br/>
Article 2 : Le pourvoi de la société New Coiffure n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à la SARL New Coiffure et à l'Office français de l'immigration et de l'intégration.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
