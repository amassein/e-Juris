<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042393022</ID>
<ANCIEN_ID>JG_L_2020_10_000000436190</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/39/30/CETATEXT000042393022.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 02/10/2020, 436190, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436190</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICARD, BENDEL-VASSEUR, GHNASSIA ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Sébastien Gauthier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:436190.20201002</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 25 novembre 2019 au secrétariat du contentieux du Conseil d'Etat, M. B... A... demande au Conseil d'Etat : <br/>
<br/>
              1°) de procéder à l'inscription en faux de l'avis de réception postal de la notification de la décision n° D. 2017-73 du 5 octobre 2017 par laquelle l'Agence française de lutte contre le dopage (AFLD) a, d'une part, prononcé à son encontre la sanction de l'interdiction de participer pendant quatre ans aux compétitions et manifestations sportives organisées ou autorisées par la Fédération française de rugby à XIII, d'autre part, étendu cette sanction, pour la période restant à courir, aux fédérations sportives françaises agréées, autres que la Fédération française de rugby à XIII, et, enfin, a reformé la décision du 4 avril 2017 de l'organe disciplinaire d'appel de lutte contre le dopage de la Fédération de rugby à XIII ;<br/>
<br/>
              2°) d'annuler cette décision de l'AFLD ;<br/>
<br/>
              3°) d'ordonner la publication de l'annulation de cette décision sur le site Internet de l'AFLD et dans les bulletins officiels des fédérations concernées ;<br/>
<br/>
              4°) de mettre à la charge de l'AFLD la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du sport ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. B... Gauthier, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Ricard, Bendel-Vasseur, Ghnassia, avocat de M. A..., et à la SCP Matuchansky, Poupot, Valdelièvre, avocat de l'Agence française de lutte contre le dopage ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	Aux termes de l'article L. 232-21 du code du sport, dans sa rédaction applicable au litige : " Toute personne qui a contrevenu aux dispositions des articles L. 232-9, L. 232-9-1, L. 232-10, L. 232-14-5, L. 232-15, L. 232-15-1 ou L. 232-17 encourt des sanctions disciplinaires de la part de la fédération dont elle est licenciée. (...) Ces sanctions sont prononcées par les fédérations sportives mentionnées à l'article L. 131-8. (...). Les fédérations agréées informent sans délai l'Agence française de lutte contre le dopage des décisions prises en application du présent article ". Aux termes de l'article L. 232-22 du même code, dans sa rédaction alors en vigueur : " En cas d'infraction aux dispositions des articles L. 232-9, L. 232-9-1, L. 232-10, L. 232-14-5, L. 232-15, L. 232-15-1 ou L. 232-17, l'Agence française de lutte contre le dopage exerce un pouvoir de sanction dans les conditions suivantes : / (...) 3° Elle peut réformer les décisions prises en application de l'article L. 232-21. Dans ces cas, l'agence se saisit, dans un délai de deux mois à compter de la réception du dossier complet, des décisions prises par les fédérations agréées ".<br/>
<br/>
              2.	M. A..., joueur titulaire d'une licence délivrée par la Fédération française de rugby à XIII (FFR XIII), a fait l'objet d'un contrôle antidopage le 30 octobre 2016, à l'issue d'une rencontre du championnat Elite 1 de rugby à XIII. Par un courrier du 4 janvier 2017, le représentant de la FFR XIII a informé M. A... des griefs retenus contre lui. Par un second courrier du 10 janvier 2017, M. A... a été informé qu'une décision de suspension provisoire avait été prononcée à son encontre le 28 décembre 2016 par le président de l'organe disciplinaire de première instance de lutte contre le dopage de la FFR XIII. Par une décision du 8 février 2017, cet organe disciplinaire a prononcé une première sanction de suspension. A la suite de l'appel formé par M. A..., l'organe disciplinaire d'appel de lutte contre le dopage de la FFR XIII a, par une décision du 4 avril 2017, d'une part, prononcé à son encontre la sanction de l'interdiction de participer pendant deux ans, dont vingt-et-un mois avec sursis, aux manifestations sportives organisées ou autorisées par la FFR XIII et, d'autre part, demandé à l'Agence française de lutte contre le dopage (AFLD) que cette sanction soit étendue aux activités de l'intéressé relevant d'autres fédérations, en particulier la FFR XV. L'AFLD s'est alors saisie d'office de cette décision de sanction afin de réexaminer le dossier de M. A.... Par une décision n° D. 2017-73 du 5 octobre 2017, l'Agence a, d'une part, prononcé à son encontre la sanction de l'interdiction de participer pendant quatre ans aux compétitions et manifestations sportives organisées ou autorisées par la FFR XIII, d'autre part, étendu cette sanction, pour la période restant à courir, aux fédérations sportives françaises agréées, autres que la FFR XIII et, enfin, a réformé la décision du 4 avril 2017 de l'organe disciplinaire d'appel en ce qu'elle avait de contraire à sa décision. Par une requête enregistrée le 25 novembre 2019, M. A... demande l'annulation de cette décision.<br/>
<br/>
              3.	D'une part, il résulte de l'instruction que la décision contestée de l'AFLD du 5 octobre 2017 a été notifiée par lettre recommandée en date du 8 décembre 2017, avec demande d'avis de réception, à l'adresse indiquée en dernier lieu par M. A..., à laquelle lui avait été notifiés préalablement l'ensemble des actes de la procédure ainsi que les décisions de première instance et d'appel prononcées par la commission disciplinaire de la FFR XIII et auxquels il avait donné suite. Si M. A... fait valoir que l'adresse en cause est celle de sa mère et produit une attestation de celle-ci selon laquelle elle avait cessé de l'héberger depuis 2012, l'intéressé doit être regardé comme ayant élu domicile à cette adresse au titre de la procédure disciplinaire engagée à son encontre, et alors qu'il n'avait pas fait connaître à l'Agence une autre adresse. Eu égard au lien qui l'unit avec sa mère et alors qu'il ressort de l'instruction que celle-ci lui avait remis plusieurs des courriers qui lui avaient été adressés, le courrier dont il s'agit a valablement pu être remis à celle-ci, qui a d'ailleurs signé, le 9 décembre 2017, l'avis de réception postal de la notification de la décision litigieuse. Si M. A... se prévaut d'une attestation de sa mère, non datée, affirmant notamment avoir jeté trois lettres destinées à son fils, au nombre desquelles figure la notification de la décision litigieuse, au motif qu'elle entretenait alors de mauvaises relations avec lui, cette attestation, qui n'est pas cohérente avec d'autres pièces du dossier, ne peut être regardée comme suffisamment probante. Il suit de là que M. A... doit être réputé avoir reçu notification de la décision de l'AFLD à la date de remise du pli recommandé, soit le 9 décembre 2017. Cette notification, qui était accompagnée de l'indication des voies et délais de recours, a fait courir le délai de deux mois imparti par les dispositions de l'article L. 232-24 du code du sport pour former un recours de pleine juridiction devant le Conseil d'Etat. Les conclusions aux fins d'annulation de M. A..., qui n'ont été introduites que le 25 novembre 2019, sont par suite tardives et doivent être rejetées comme irrecevables. <br/>
<br/>
              4.	D'autre part, si M. A... estime que l'avis de réception postal de la notification de la décision du 5 octobre 2017 de l'AFLD constitue un faux en ce qu'il comporte une signature qui n'est pas la sienne, il résulte de ce qui a été dit au point précédent que la solution du litige n'est pas fondée sur la signature par lui de cet avis et qu'il n'est pas contesté que sa mère en est la signataire. Par suite, les conclusions qu'il a présentées sur le fondement de l'article R. 633-1 du code de justice administrative ne sauraient, en tout état de cause, être accueillies.<br/>
<br/>
              5.	Il résulte de tout ce qui précède que la requête de M. A... doit être rejetée, y compris les conclusions aux fins d'injonction et celles présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B... A... et à l'Agence française de lutte contre le dopage.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
