<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029626757</ID>
<ANCIEN_ID>JG_L_2014_10_000000380819</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/62/67/CETATEXT000029626757.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère SSJS, 24/10/2014, 380819, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-10-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380819</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Pascal Trouilly</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:380819.20141024</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête, enregistrée le 2 juin 2014 au secrétariat du contentieux du Conseil d'Etat, le département de Loir-et-Cher demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2014-213 du 21 février 2014 portant délimitation des cantons dans le département de Loir-et-Cher ainsi que la décision du 28 mars 2014 rejetant son recours gracieux contre ce décret ou, subsidiairement, l'annuler en tant qu'il ne fixe pas les sièges des nouveaux chefs-lieux de canton du département ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ; <br/>
              - la Constitution ; <br/>
              - le code électoral ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 90-1103 du 11 décembre 1990 ;<br/>
              - la loi n° 2013-403 du 17 mai 2013 ;<br/>
              - le décret n° 2013-938 du 18 octobre 2013, modifié par le décret n° 2014-112 du 6 février 2014 ;<br/>
              - la décision du 29 juillet 2014 par laquelle le Conseil d'Etat statuant au contentieux n'a pas renvoyé au Conseil constitutionnel les questions prioritaires de constitutionnalité soulevée par le département de Loir-et-Cher ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pascal Trouilly, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 191-1 du code électoral, résultant de la loi du 17 mai 2013 relative à l'élection des conseillers départementaux, des conseillers municipaux et des conseillers communautaires et modifiant le calendrier électoral : " Le nombre de cantons dans lesquels sont élus les conseillers départementaux est égal, pour chaque département, à la moitié du nombre de cantons existant au 1er janvier 2013, arrondi à l'unité impaire supérieure si ce nombre n'est pas entier impair. / Le nombre de cantons dans chaque département comptant plus de 500 000 habitants ne peut être inférieur à dix-sept. Il ne peut être inférieur à treize dans chaque département comptant entre 150 000 et 500 000 habitants ". Aux termes de l'article L. 3113-2 du code général des collectivités territoriales, dans sa rédaction issue de la même loi, applicable à la date du décret attaqué : " I. - Les modifications des limites territoriales des cantons, les créations et suppressions de cantons et le transfert du siège de leur chef-lieu sont décidés par décret en Conseil d'Etat après consultation du conseil général qui se prononce dans un délai de six semaines à compter de sa saisine. A l'expiration de ce délai, son avis est réputé rendu. (...) / III. - La modification des limites territoriales des cantons effectuée en application du I est conforme aux règles suivantes : / a) Le territoire de chaque canton est défini sur des bases essentiellement démographiques ; / b) Le territoire de chaque canton est continu ; / c) Est entièrement comprise dans le même canton toute commune de moins de 3 500 habitants ; / IV. - Il n'est apporté aux règles énoncées au III que des exceptions de portée limitée, spécialement justifiées, au cas par cas, par des considérations géographiques (...) ou par d'autres impératifs d'intérêt général ".<br/>
<br/>
              2. Le décret attaqué a, sur le fondement de l'article L. 3113-2 du code général des collectivités territoriales, procédé à une nouvelle délimitation des cantons du département de Loir-et-Cher, compte tenu de l'exigence de réduction du nombre des cantons de ce département de trente à quinze résultant de l'article L. 191-1 du code électoral.<br/>
<br/>
              I. - Sur la légalité externe du décret attaqué :<br/>
<br/>
              3. En premier lieu, il résulte des termes mêmes des dispositions législatives précitées qu'il appartenait au Premier ministre de procéder, par décret en Conseil d'Etat, à une nouvelle délimitation territoriale de l'ensemble des cantons. A cet égard, le département requérant ne peut utilement soutenir que le Premier ministre n'aurait pas compétence faute pour le législateur d'avoir adopté des dispositions garantissant que le Conseil d'Etat ne puisse se retrouver en situation d'être à la fois " juge et partie " en cas de recours contentieux contre la nouvelle délimitation. <br/>
<br/>
              4. En deuxième lieu, aux termes de l'article 22 de la Constitution : " Les actes du Premier ministre sont contresignés, le cas échéant, par les ministres chargés de leur exécution ". Les ministres chargés de son exécution sont ceux qui ont compétence pour signer ou contresigner les mesures réglementaires ou individuelles que comporte nécessairement son exécution. Le décret attaqué du 21 février 2014, qui se limite à modifier les circonscriptions électorales du département de Loir-et-Cher, n'appelle aucune mesure d'exécution que le garde des sceaux, ministre de la justice, serait compétent pour signer ou contresigner. Il suit de là que ce décret n'avait pas à être contresigné par ce ministre.<br/>
<br/>
              5. En troisième lieu, les dispositions de l'article L. 3113-2 du code général des collectivités territoriales se bornent à prévoir la consultation du conseil général du département concerné à l'occasion de l'opération de création et suppression de cantons. Ni le principe constitutionnel de libre administration des collectivités territoriales, ni aucune autre disposition législative ou réglementaire n'imposent une consultation des communes du département faisant l'objet d'une nouvelle délimitation des cantons, ou des établissements publics de coopération intercommunale.<br/>
<br/>
              6. En dernier lieu, le département requérant ne peut, en tout état de cause, utilement se prévaloir, pour soutenir que les principaux élus du département auraient dû être consultés, des termes de la circulaire du ministre de l'intérieur du 12 avril 2013 relative à la méthodologie du redécoupage cantonal en vue de la mise en oeuvre du scrutin binominal majoritaire aux élections départementales, laquelle est dépourvue de caractère réglementaire.<br/>
<br/>
              II. - Sur la légalité interne du décret attaqué :<br/>
<br/>
              En ce qui concerne le nombre de cantons :<br/>
<br/>
              7. Il résulte des dispositions de l'article L. 191-1 du code électoral, citées au point 1, que le législateur a déterminé lui-même le nombre de cantons par département. Par suite, il n'appartenait pas au Premier ministre de modifier ce nombre pour rééquilibrer le nombre d'habitants par canton sur l'ensemble du territoire national. Le moyen tiré de ce que le décret serait illégal faute d'un tel rééquilibrage ne peut, dès lors, qu'être écarté.<br/>
<br/>
              En ce qui concerne les données démographiques prises en considération :<br/>
<br/>
              8. En premier lieu, l'article L. 3113-2 du code général des collectivités territoriales prévoit que la délimitation des cantons doit être faite sur des bases essentiellement démographiques et l'article 71 du décret n° 2013-938 du 18 octobre 2013, dans sa rédaction issue de l'article 8 du décret n° 2014-112 du 6 février 2014, dispose que : " (...) Pour la première délimitation générale des cantons opérée en application de l'article L. 3113-2  du code général des collectivités territoriales, dans sa rédaction résultant de l'article 46 de la loi n° 2013-403 du 17 mai 2013 relative à l'élection des conseillers départementaux, (...), le chiffre de la population municipale auquel il convient de se référer est celui authentifié par le décret n° 2012-1479 du 27 décembre 2012 authentifiant les chiffres des populations (...) ". Par suite, le département de Loir-et-Cher, qui ne conteste pas la légalité du décret du 18 octobre 2013 sur ce point, n'est pas fondé à soutenir que le décret attaqué aurait également dû prendre en considération le nombre d'électeurs par canton.<br/>
<br/>
              9. En deuxième lieu, le département requérant soutient, par la voie de l'exception, que l'article 8 du décret n° 2014-112 du 6 février 2014 portant différentes mesures d'ordre électoral serait illégal en ce qu'il renvoie aux chiffres authentifiés par le décret n° 2012-1479 du 27 décembre 2012. Toutefois, d'abord, ce décret n'appelle aucune mesure d'exécution que le ministre chargé de l'économie ou le ministre chargé de l'outre-mer seraient compétents pour signer ou contresigner. Il n'avait donc pas à être contresigné par ces ministres. Ensuite, la délimitation des nouvelles circonscriptions cantonales devait, conformément aux dispositions de l'article 7 de la loi du 11 décembre 1990, être effectuée au plus tard un an avant le prochain renouvellement général des conseils généraux. Dans les circonstances de l'espèce, eu égard, d'une part, aux délais inhérents à l'élaboration de l'ensemble des projets de décrets de délimitation des circonscriptions cantonales, à la consultation des conseils généraux et à la saisine pour avis du Conseil d'Etat, d'autre part, à la circonstance que la déclinaison à l'échelon infra-communal des chiffres de population applicables à compter du 1er janvier 2014, nécessaire à la délimitation de certains cantons, n'était pas disponible à la date à laquelle devait être entreprise la délimitation des nouvelles circonscriptions cantonales, le décret du 6 février 2014 a pu légalement prévoir que le chiffre de population municipale auquel il convenait de se référer était le chiffre authentifié par le décret n° 2012-1479 du 27 décembre 2012 et non celui arrêté par le décret n° 2013-1289 du 27 décembre 2013, qui authentifie les chiffres de population auxquels il convient, en principe, de se référer pour l'application des lois et règlements à compter du 1er janvier 2014. Enfin, les dispositions dont l'illégalité est invoquée ne concernant que la délimitation des nouvelles circonscriptions cantonales, le moyen tiré de ce qu'elles auraient des conséquences sur l'application des règles relatives aux dépenses de campagne ne peut qu'être écarté.<br/>
<br/>
              10. Par suite, le département de Loir-et-Cher n'est pas fondé à soutenir que le décret du 6 février 2014 serait illégal en ce qu'il renvoie aux chiffres authentifiés par le décret du 27 décembre 2012 ni que le décret attaqué serait illégal au motif qu'il a délimité les nouveaux cantons sur la base de ces données et non de celles authentifiées par le décret du 27 décembre 2013.<br/>
<br/>
              11. En troisième lieu, il ressort des pièces du dossier que, contrairement à ce qui est soutenu, les limites des cantons Blois-1, Blois-2, Blois-3 et Vineuil coïncident avec celles des " îlots regroupés pour l'information statistique " (IRIS), unités de base de calcul de la population utilisées par l'Institut national de la statistique et des études économiques. Par suite, le département de Loir-et-Cher ne peut soutenir que, faute d'une telle concordance, le décompte de la population de ces cantons serait nécessairement erroné.<br/>
<br/>
              En ce qui concerne la délimitation des cantons :<br/>
<br/>
              12. En premier lieu, la circonstance que les tableaux annexés à l'article L. 125 du code électoral définissent les circonscriptions législatives par référence à l'ancienne carte cantonale ne faisait pas obstacle à ce que le Premier ministre, compétent sur le fondement de l'article L. 3113-2 du code général des collectivités territoriales, procède à une nouvelle délimitation des cantons, différente de celle à laquelle le législateur s'était référé.<br/>
<br/>
              13. En deuxième lieu, il résulte des dispositions de l'article L. 3113-2 du code général des collectivités territoriales citées au point 1 que le territoire de chaque canton doit être établi sur des bases essentiellement démographiques, qu'il doit être continu et que toute commune de moins de 3 500 habitants doit être entièrement comprise dans un même canton, seules des exceptions de portée limitée et spécialement justifiées pouvant être apportées à ces règles. Ni ces dispositions, ni aucun autre texte non plus qu'aucun principe n'imposent au Premier ministre de prévoir que les limites des cantons, qui sont des circonscriptions électorales, coïncident avec les limites des circonscriptions législatives, des ressorts des juridictions judiciaires, des arrondissements, des périmètres des établissements publics de coopération intercommunale, des schémas de cohérence territoriale ou des " bassins de vie " définis par l'Institut national de la statistique et des études économiques. De même, ni les dispositions de l'article L. 3113-2 du code général des collectivités territoriales, ni aucun autre texte non plus qu'aucun principe n'imposent au Premier ministre de prendre comme critères de délimitation de ces circonscriptions électorales les limites des anciens cantons, la proximité géographique des communes ou l'absence de disparité de superficie entre cantons.  <br/>
<br/>
              14. En troisième lieu, pour mettre en oeuvre les critères définis au III de l'article L. 3113-2 du code général des collectivités territoriales, le décret attaqué a procédé à la délimitation des quinze nouveaux cantons du département de Loir-et-Cher en se fondant sur une population moyenne et en rapprochant la population de chaque canton de cette moyenne. Le département requérant ne conteste pas que cette délimitation respecte les critères définis par les dispositions précitées du code général des collectivités territoriales et se borne à soutenir que les limites des cantons sont arbitraires au regard des limites des différentes structures ou circonscriptions existantes, notamment celles des établissements publics de coopération intercommunale, et conduisent à une surreprésentation de la population urbaine. Or, d'une part, il ressort des pièces du dossier que pour neuf des quatorze établissements publics de coopération intercommunale dont la population est inférieure à la population maximale par canton, toutes les communes sont regroupées au sein d'un même canton, tandis que le rattachement à différents cantons des communes des autres structures de coopération, en particulier de la communauté d'agglomération de Blois, de la communauté de communes du Val-de-Cher, de celle de la Sologne des Etangs et de celle du Romorantinais et Monestois, est justifié par le respect de l'exigence tenant aux bases essentiellement démographiques de la délimitation des cantons. D'autre part, la population des cantons de Blois-1, Blois-2, Blois-3 et Vineuil ne s'écartant pas de plus de 4,5 % de la population moyenne par canton, la nouvelle délimitation ne peut être regardée comme assurant une surreprésentation générale de la population urbaine du département au détriment de la population rurale. Le département de Loir-et-Cher n'est, ainsi, pas fondé à soutenir que les choix auxquels il a été procédé reposeraient sur une erreur manifeste d'appréciation.<br/>
<br/>
              15. En dernier lieu, d'une part, les dispositions précitées du c) du III de l'article L. 3113-2 imposent seulement au pouvoir réglementaire de comprendre entièrement dans le même canton les communes de moins de 3 500 habitants. Le choix de diviser la commune de Blois, dont la population municipale s'élève à 46 492 habitants, entre quatre cantons, dont la population ne s'écarte pas de plus de 4,5 % de la population moyenne par canton, est justifié par le souci de rattacher aux mêmes cantons la ville de Blois et les communes limitrophes membres de la communauté d'agglomération. D'autre part, il ne ressort pas des pièces du dossier que la délimitation des cantons de Romorantin-Lanthenay et de Vendôme, qui au demeurant permet de regrouper au sein d'un seul canton l'intégralité de la population de chacune des deux communes éponymes, reposerait sur des considérations étrangères à l'objet du décret attaqué. Par suite, le détournement de pouvoir allégué n'est pas établi.<br/>
<br/>
              En ce qui concerne les chefs-lieux de cantons et bureaux centralisateurs :<br/>
<br/>
              16. La circonstance que le décret attaqué se borne à identifier, pour chaque canton, un bureau centralisateur sans mentionner les chefs-lieux de canton est sans influence sur la légalité de ce décret, qui porte sur la délimitation des circonscriptions électorales dans le département de Loir-et-Cher.<br/>
<br/>
              17. Il résulte de tout ce qui précède que le département de Loir-et-Cher n'est pas fondé à demander l'annulation totale du décret qu'il attaque ou son annulation en tant qu'il ne désigne pas de chefs-lieux de canton, ni celle de la décision du ministre de l'intérieur du 28 mars 2014 rejetant le recours gracieux formé contre ce décret. Ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être également rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête du département de Loir-et-Cher est rejetée.<br/>
Article 2 : La présente décision sera notifiée au département de Loir-et-Cher et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
