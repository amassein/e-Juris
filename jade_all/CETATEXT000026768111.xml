<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026768111</ID>
<ANCIEN_ID>JG_L_2012_12_000000330757</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/76/81/CETATEXT000026768111.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 12/12/2012, 330757, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-12-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>330757</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLEZ</AVOCATS>
<RAPPORTEUR>M. Matthieu Schlesinger</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:330757.20121212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi du ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du Gouvernement, enregistré le 12 août 2009 au secrétariat du contentieux du Conseil d'Etat ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 1er et 2 de l'arrêt n° 06LY00759 du 28 mai 2009 par lesquels la cour administrative d'appel de Lyon a rejeté son recours tendant, d'une part, à l'annulation des articles 1er et 2 du jugement n° 0305547 du 13 décembre 2005 par lesquels le tribunal administratif de Lyon a fait droit à la demande de la SARL Immobilière des Gaules tendant à la réduction de sa base d'imposition à la taxe professionnelle au titre de l'année 2002 à hauteur de 58 477 F (8 914,76 euros) et, d'autre part, au rétablissement de cette société au rôle de la taxe professionnelle au titre de l'année 2002 à concurrence de la réduction prononcée par les premiers juges ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matthieu Schlesinger, Auditeur,  <br/>
<br/>
              - les observations de la SCP Boullez, avocat de la SARL Immobilière des Gaules,<br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Boullez, avocat de la SARL Immobilière des Gaules ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SARL Immobilière des Gaules, qui exerce une activité d'agence immobilière et emploie moins de cinq salariés dans ses établissements de Lyon et de Caluire (Rhône), a été assujettie à la taxe professionnelle au titre des années 1999 à 2002 en application des dispositions du 2° de l'article 1467 du code général des impôts, sur une base comprenant une fraction des recettes ; que par une réclamation du 10 septembre 2002, cette société a contesté les modalités de son assujettissement à cette taxe pour l'année 2002, en demandant notamment à l'administration fiscale de faire application, pour le calcul de la cotisation de taxe professionnelle, des dispositions du 1° de l'article 1467 ; qu'à la suite du rejet de sa réclamation en tant qu'elle contestait la prise en compte des recettes commerciales dans la base de la taxe due au titre de l'année 2000, la SARL Immobilière des Gaules a saisi le tribunal administratif de Lyon du litige l'opposant à l'administration fiscale, en demandant, à titre principal, la décharge partielle de la cotisation de taxe professionnelle relative à l'année 2000 en tant que la base de cette taxe incluait une fraction de ses recettes commerciales et en faisant valoir, à titre subsidiaire, que les recettes visées au 2° de l'article 1467, devaient s'entendre des encaissements effectifs et non des produits déterminés selon les règles d'une comptabilité commerciale ; que, par un jugement du 13 décembre 2005, les premiers juges ont confirmé l'application à la contribuable des dispositions du 2° de l'article 1467 du code général des impôts, mais fait droit à la demande subsidiaire de la SARL Immobilière des Gaules tendant à ce que seules les recettes encaissées soient prises en compte pour le calcul de l'impôt et accordé pour ce motif à la société une réduction de sa base d'imposition à la taxe professionnelle au titre de l'année 2002 à hauteur de 58 477 francs (8 914,76 euros) ; que le ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du Gouvernement se pourvoit en cassation contre l'arrêt du 28 mai 2009 par lequel la cour administrative d'appel de Lyon a rejeté son appel tendant à l'annulation des articles 1er et 2 du jugement du 13 décembre 2005 du tribunal administratif de Lyon et au rétablissement de la société au rôle de la taxe professionnelle au titre de l'année 2002 à concurrence de la réduction prononcée par les premiers juges ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 1467 du code général des impôts, dans sa rédaction applicable aux années d'imposition en litige : " La taxe professionnelle a pour base : (...) / 2° Dans le cas des titulaires de bénéfices non commerciaux, des agents d'affaires et intermédiaires du commerce, employant moins de cinq salariés, le dixième des recettes et la valeur locative des seules immobilisations passibles des taxes foncières sur les propriétés bâties et non bâties et dont le contribuable a disposé pour les besoins de son activité professionnelle pendant la période de référence définie au 1° a. " ; qu'aux termes de l'article 310 HE l'annexe II au code, dans sa rédaction alors applicable : " Les recettes servant à calculer la base d'imposition des redevables définis à l'article 310 HC s'entendent, selon le cas, de celles retenues pour l'établissement de l'impôt sur le revenu ou de l'impôt sur les sociétés ; il n'est pas tenu compte des honoraires, courtages ou commissions rétrocédés à des tiers, lorsque ces sommes ont fait l'objet de la déclaration prévue par l'article 240 du code général des impôts " ;<br/>
<br/>
              3. Considérant que, pour l'application des dispositions du 2° de l'article 1467 du code général des impôts, éclairées par les travaux préparatoires de la loi du 29 juillet 1975 de laquelle elles sont issues, le terme " recettes " ne doit s'entendre que des sommes effectivement perçues par le contribuable au cours de l'année de référence ; que l'article 3-III du décret du 23 octobre 1975, pris pour l'application de la loi du 29 juillet 1975, repris à l'article 310 HE de l'annexe II au code général des impôts n'en donne pas une définition différente ; qu'il s'ensuit qu'en  donnant cette portée aux dispositions précitées et en jugeant que le tribunal administratif de Lyon avait à bon droit exclu de la base d'imposition à la taxe professionnelle de la SARL Immobilière des Gaules au titre de l'année 2002 les créances acquises sur les tiers, pour ne retenir que les recettes effectivement encaissées, alors même que l'imposition des bénéfices commerciaux de cette société devait prendre en compte ces créances, la cour administrative d'appel de Lyon n'a pas commis d'erreur de droit ; que, par suite, le ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du Gouvernement n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SARL Immobilière des Gaules, au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi du ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du Gouvernement est rejeté. <br/>
Article 2 : L'Etat versera à la SARL Immobilière des Gaules une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'économie et des finances et à la SARL Immobilière des Gaules.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
