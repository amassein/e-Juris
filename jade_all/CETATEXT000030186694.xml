<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030186694</ID>
<ANCIEN_ID>JG_L_2015_02_000000382607</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/18/66/CETATEXT000030186694.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 02/02/2015, 382607, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-02-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382607</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Guillaume Déderen</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:382607.20150202</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 15 juillet et 18 août 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. F...A..., demeurant ... ; M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 1401550 du 13 juin 2014 par lequel le tribunal administratif de Rennes a rejeté sa protestation tendant à l'annulation des opérations électorales qui se sont déroulées le 30 mars 2014 en vue de l'élection des membres du conseil municipal de Quiberon ; <br/>
<br/>
              2°) d'annuler ces opérations électorales ;<br/>
<br/>
              3°) de mettre à la charge de M. C...E...la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 15 janvier 2015, présentée pour M. A...;<br/>
<br/>
              Vu le code électoral ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Déderen, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que M. A...relève appel du jugement du 13 juin 2014 par lequel le tribunal administratif de Rennes a rejeté sa protestation tendant à l'annulation des opérations électorales qui se sont déroulées le 30 mars 2014 en vue de l'élection des membres du conseil municipal de Quiberon ;<br/>
<br/>
              2. Considérant, en premier lieu, que M. A...soutient que les quotidiens Le Télégramme et Ouest France ont chacun publié, le 26 mars 2014, un article rapportant des déclarations de M. B...D...concernant l'attribution à ce dernier, en cas de victoire de la liste commune issue de la fusion en vue du second tour de la liste qu'il conduisait avec celle de M. E...qui était arrivée en tête au premier tour, d'un siège de conseiller communautaire, alors qu'il était, selon lui, statistiquement impossible que M. D...obtînt ce siège ;<br/>
<br/>
              3. Considérant que de telles déclarations, qui font état d'un accord électoral dont les effets sont par nature incertains, et auxquelles M. A...avait au demeurant la possibilité de répondre en temps utile, ne sauraient être qualifiées dans les circonstances de l'espèce, de manoeuvre susceptible d'avoir porté atteinte à la sincérité du scrutin ;<br/>
<br/>
              4. Considérant, en second lieu, qu'aux termes de l'article L. 48 du code électoral : " Sont applicables à la propagande les dispositions de la loi du 29 juillet 1881 sur la liberté de la presse (...) " ; qu'aux termes de l'article L. 48-2 du même code : " Il est interdit à tout candidat de porter à la connaissance du public un élément nouveau de polémique électorale à un moment tel que ses adversaires n'aient pas la possibilité d'y répondre utilement avant la fin de la campagne électorale. " ; qu'aux termes de l'article L. 49 du même code : " A partir de la veille du scrutin à zéro heure, il est interdit de distribuer ou faire distribuer des bulletins, circulaires et autres documents. / A partir de la veille du scrutin à zéro heure, il est également interdit de diffuser ou de faire diffuser par tout moyen de communication au public par voie électronique tout message ayant le caractère de propagande électorale. " ; qu'enfin, aux termes de l'article R. 26 du même code : " La campagne électorale est ouverte à partir du deuxième lundi qui précède la date du scrutin et prend fin la veille du scrutin à minuit. En cas de second tour, la campagne électorale est ouverte le lendemain du premier tour et prend fin la veille du scrutin à minuit. " ;<br/>
<br/>
              5. Considérant que, d'une part, le requérant ne peut utilement exciper des dispositions du second alinéa de l'article L. 49 mentionné ci-dessus, qui ne tendent qu'à proscrire la propagande électorale à la veille du scrutin ; que, d'autre part, les organes de presse étant libres de rendre compte de la campagne des différents candidats et de prendre position en faveur de l'un d'entre eux, la seule circonstance qu'ait été publié le 28 mars 2014 un article dans le quotidien Le Télégramme consacré aux projets de la société La Belle-Iloise dirigée par la fille de M. E...et auquel M. A...avait au demeurant la possibilité de répondre utilement, n'est pas susceptible d'avoir altéré la sincérité du scrutin ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que M. A...n'est pas fondé à demander l'annulation du jugement qu'il attaque, qui est suffisamment motivé ; que doivent être rejetées, par voie de conséquence, ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au même titre par M. E...;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : Les conclusions présentées par M. E...au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à M. F...A..., à M. C... E...et au ministre de l'intérieur.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
