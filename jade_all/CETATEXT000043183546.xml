<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043183546</ID>
<ANCIEN_ID>JG_L_2021_02_000000431475</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/18/35/CETATEXT000043183546.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 24/02/2021, 431475, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431475</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET ; HAAS</AVOCATS>
<RAPPORTEUR>M. Pierre Boussaroque</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:431475.20210224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. D... E... et M. C... B... ont demandé au tribunal administratif de Melun, par trois requêtes distinctes, en premier lieu, d'annuler pour excès de pouvoir l'arrêté du 14 juin 2006 par lequel le maire de Champigny-sur-Marne a décidé d'exercer le droit de préemption sur le terrain sis 36 bis, avenue de Stalingrad, en deuxième lieu, d'annuler pour excès de pouvoir la décision prise le 4 décembre 2006 par le maire de Champigny-sur-Marne de céder la parcelle préemptée au syndicat mixte d'action foncière du département du Val-de-Marne et, en troisième lieu, de condamner la commune de Champigny-sur-Marne à leur verser, en réparation des préjudices causés par ces deux décisions, la somme de 1 029 722,95 euros, augmentée des intérêts légaux à compter du 9 octobre 2015, avec capitalisation des intérêts. Par un jugement n°s 1508634, 1508635, 1510522 du 29 décembre 2017, le tribunal administratif de Melun a rejeté ces demandes.<br/>
<br/>
              Par un arrêt n° 18PA01031 du 11 avril 2019, la cour administrative d'appel de Paris, saisie de l'appel de M. E... et M. B..., a annulé ce jugement en tant qu'il avait déclaré la juridiction administrative incompétente pour connaître des conclusions indemnitaires des requérants puis rejeté ces conclusions indemnitaires ainsi que le surplus de leurs conclusions d'appel.<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat les 7 juin et 9 septembre 2019 et le 28 juillet 2020, M. E... et M. B... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Champigny-sur-Marne la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ; <br/>
              - la loi n° 68-1250 du 31 juillet 1968 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Boussaroque, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme A... F..., rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP de Nervo, Poupet, avocat de M. E... et de M. B... et à Me Haas, avocat de la commune de Champigny-sur-Marne ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société civile immobilière du Moulin a conclu le 6 avril 2006 une promesse de vente du terrain lui appartenant, sis 36 bis, avenue de Stalingrad à Champigny-sur-Marne, avec la société GDM Auto, locataire de ce terrain depuis 2002, dont les associés sont M. E... et M. B.... Le maire de Champigny-sur-Marne a, par un arrêté du 14 juin 2006, décidé de préempter ce bien et l'acte authentique de vente à la commune a été signé le 28 novembre 2006. Par délibération du 24 octobre 2006, le conseil municipal de Champigny-sur-Marne a décidé de vendre ce bien au syndicat d'action foncière du Val-de-Marne. L'acte de vente a été signé par le maire le 4 décembre 2006. Par trois requêtes enregistrées, pour les deux premières, le 27 octobre 2015 et, pour la troisième, le 24 décembre 2015, M. E... et M. B... ont demandé au tribunal administratif de Melun, d'une part, d'annuler pour excès de pouvoir la décision de préemption du 14 juin 2006 et la décision de cession du 4 décembre 2006 et, d'autre part, de condamner la commune à les indemniser des préjudices qu'ils estiment avoir subis du fait de ces décisions. Le tribunal administratif de Melun a rejeté ces demandes par un jugement du 29 décembre 2017. Par un arrêt du 11 avril 2019, contre lequel M. E... et M. B... se pourvoient en cassation, la cour administrative d'appel de Paris a annulé ce jugement en tant qu'il déclare la juridiction administrative incompétente pour connaître de leurs conclusions indemnitaires, puis a rejeté ces conclusions indemnitaires et le surplus de leurs conclusions d'appel.<br/>
<br/>
              Sur l'arrêt, en tant qu'il statue sur les conclusions à fin d'annulation :<br/>
<br/>
              2. La cour ayant relevé que l'arrêté de préemption du 14 juin 2006 avait été remis en mains propres au notaire chargé de la vente ayant reçu mandat, notamment de M. E..., à l'effet d'accomplir les démarches relatives à la purge du droit de préemption et que M. E... et M. B... avaient été informés par un courrier du syndicat d'action foncière du Val-de-Marne le 18 décembre 2007 que celui-ci avait acquis le bien litigieux par acte de vente du 4 décembre 2006 et que les loyers devaient lui être désormais versés, elle n'a pas commis d'erreur de droit en déduisant de ces circonstances qu'il était établi que M. E... et M. B... avaient eu connaissance de la décision de préemption au plus tard à la date de réception de ce courrier. M. E... et M. B... ayant la qualité de tiers par rapport à la décision de préemption, dont seule la société GDM avait à être rendue destinataire en sa qualité d'acquéreur évincé, le délai de recours contentieux contre cette décision a commencé à courir à leur encontre au plus tard à compter de cette date et était ainsi expiré à la date à laquelle, le 27 octobre 2015, ils ont saisi le tribunal administratif de Melun. Il a lieu de substituer d'office ce motif, qui n'implique l'appréciation d'aucune circonstance de fait, à celui retenu par l'arrêt attaqué, dont il justifie sur ce point le dispositif, pour rejeter comme tardives leurs conclusions tendant à l'annulation de la décision de préemption du 14 juin 2006.<br/>
<br/>
              3. Il résulte de ce qui précède que M. E... et M. B... ne sont pas fondés à demander l'annulation de l'arrêt qu'ils attaquent en tant qu'il rejette leurs conclusions d'excès de pouvoir.<br/>
<br/>
              Sur l'arrêt, en tant qu'il statue sur les conclusions indemnitaires :<br/>
<br/>
              4. Aux termes de l'article 1er de la loi du 31 décembre 1968 relative à la prescription des créances sur l'Etat, les départements, les communes et les établissements publics : " Sont prescrites, au profit de l'Etat, des départements et des communes, sans préjudice des déchéances particulières édictées par la loi et sous réserve des dispositions de la présente loi, toutes créances qui n'ont pas été payées dans un délai de quatre ans à partir du premier jour de l'année suivant celle au cours de laquelle les droits ont été acquis. (...) ". Aux termes de l'article 2 de cette loi : " La prescription est interrompue par : (...) Tout recours formé devant une juridiction, relatif au fait générateur, à l'existence, au montant ou au paiement de la créance, quel que soit l'auteur du recours et même si la juridiction saisie est incompétente pour en connaître, et si l'administration qui aura finalement la charge du règlement n'est pas partie à l'instance (...) ". Aux termes de l'article 3 de cette loi : " La prescription ne court ni contre le créancier qui ne peut agir, soit par lui-même ou par l'intermédiaire de son représentant légal, soit pour une cause de force majeure, ni contre celui qui peut être légitimement regardé comme ignorant l'existence de sa créance ou de la créance de celui qu'il représente légalement ".<br/>
<br/>
              5. Lorsqu'est demandée l'indemnisation du préjudice résultant de l'illégalité d'une décision administrative, le fait générateur de la créance doit être rattaché non à l'exercice au cours duquel la décision a été prise mais à celui au cours duquel elle a été valablement notifiée à son destinataire ou portée à la connaissance du tiers qui se prévaut de cette illégalité.<br/>
<br/>
              6. La cour a jugé que la décision de préemption du 14 juin 2006 et la décision de cession du 4 décembre 2006 avaient été portées à la connaissance de M. E... et M. B..., qui n'avaient pas à en être rendus destinataires, au plus tard à la réception du courrier que leur a adressé le syndicat d'action foncière du Val-de-Marne le 18 décembre 2007. Il résulte de ce qui a été dit au point précédent qu'elle n'a, dans ces conditions, pas commis d'erreur de droit en jugeant que la prescription quadriennale de la créance indemnitaire susceptible de résulter de ces décisions avait commencé à courir le premier jour de l'année suivante, de sorte que cette créance était prescrite à la date de présentation de leurs conclusions indemnitaires. En outre, ce faisant, contrairement à ce qu'ils soutiennent, elle s'est bornée à faire application à ces conclusions indemnitaires des règles de prescription, sans leur opposer une tardiveté résultant de l'expiration d'un délai de recours contentieux. <br/>
<br/>
              7. Il résulte de ce qui précède que les requérants ne sont pas fondés à demander l'annulation de l'arrêt qu'ils attaquent en tant qu'il rejette leurs conclusions indemnitaires.<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. E... et M. B... une somme de 500 euros à verser chacun à la commune de Champigny-sur-Marne au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions font en revanche obstacle à ce qu'une somme soit mise à la charge de commune de Champigny-sur-Marne, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Le pourvoi de M. E... et autre est rejeté.<br/>
Article 2 : M. E... et M. B... verseront chacun à la commune de Champigny-sur-Marne une somme de 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à M. D... E..., premier requérant dénommé, et à la commune de Champigny-sur-Marne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
