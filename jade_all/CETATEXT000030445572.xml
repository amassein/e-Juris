<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030445572</ID>
<ANCIEN_ID>JG_L_2015_03_000000367896</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/44/55/CETATEXT000030445572.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 27/03/2015, 367896</TITRE>
<DATE_DEC>2015-03-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367896</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR</AVOCATS>
<RAPPORTEUR>Mme Sophie-Justine Lieber</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:367896.20150327</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 19 avril et 20 juin 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour le centre hospitalier Pierre Oudot, agissant par son représentant légal, dont le siège est 35, avenue du Maréchal Leclerc BP 348 à Bourgoin-Jallieu (38317) ; le centre hospitalier Pierre Oudot demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 12LY01704 du 19 février 2013 par lequel la cour administrative d'appel de Lyon a rejeté sa requête tendant à l'annulation du jugement n° 0903887 du 31 mai 2012 par lequel le tribunal administratif de Grenoble a annulé, à la demande de M. B... A..., l'arrêté du 15 juin 2009 par lequel le maire de Bourgoin-Jallieu lui a délivré un permis de construire pour l'extension d'un institut de formation en soins infirmiers ;<br/>
<br/>
              2°) de renvoyer l'affaire devant la cour administrative d'appel ou, subsidiairement, réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de M. A...la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de la construction et de l'habitation ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Justine Lieber, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat du centre hospitalier Pierre Oudot et à la SCP Célice, Blancpain, Soltner, Texidor, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que, par un arrêté du 15 juin 2009, le maire de Bourgoin-Jallieu a accordé un permis de construire au centre hospitalier Pierre Oudot pour l'extension d'un institut de soins infirmiers ; que, saisi par M. A..., propriétaire d'un terrain voisin du terrain d'assiette du projet, le tribunal administratif de Grenoble a, par un jugement du 31 mai 2012, annulé pour excès de pouvoir le permis de construire ; que, par un arrêt du 19 février 2013, contre lequel le centre hospitalier Pierre Oudot se pourvoit en cassation, la cour administrative d'appel de Lyon a rejeté son appel contre ce jugement ; <br/>
<br/>
              2. Considérant, d'une part, qu'aux termes de l'article L. 2122-18 du code général des collectivités territoriales : " Le maire est seul chargé de l'administration, mais il peut, sous sa surveillance et sa responsabilité, déléguer par arrêté une partie de ses fonctions à un ou plusieurs de ses adjoints (...) " ; que les arrêtés du maire consentant, en application de ces dispositions, des délégations aux adjoints doivent définir avec une précision suffisante les limites de ces délégations ; <br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes de l'article L. 422-1 du code de l'urbanisme : " L'autorité compétente pour délivrer le permis de construire, d'aménager ou de démolir et pour se prononcer sur un projet faisant l'objet d'une déclaration préalable est : / a) Le maire, au nom de la commune, dans les communes qui se sont dotées d'un plan local d'urbanisme ou d'un document d'urbanisme en tenant lieu (...) " ; qu'aux termes de l'article L. 425-3 du même code : " Lorsque le projet porte sur un établissement recevant du public, le permis de construire tient lieu de l'autorisation prévue par l'article L. 111-8 du code de la construction et de l'habitation dès lors que la décision a fait l'objet d'un accord de l'autorité administrative compétente qui peut imposer des prescriptions relatives à l'exploitation des bâtiments en application de l'article L. 123-2 du code de la construction et de l'habitation. Le permis de construire mentionne ces prescriptions " ; qu'aux termes de l'article L. 111-8 du code de la construction et de l'habitation : " Les travaux qui conduisent à la création, l'aménagement ou la modification d'un établissement recevant du public ne peuvent être exécutés qu'après autorisation délivrée par l'autorité administrative qui vérifie leur conformité aux règles prévues aux articles L. 111-7, L. 123-1 et L. 123-2. / Lorsque ces travaux sont soumis à permis de construire, celui-ci tient lieu de cette autorisation dès lors que sa délivrance a fait l'objet d'un accord de l'autorité administrative compétente mentionnée à l'alinéa précédent " ; qu'enfin, aux termes de l'article R. 111-19-13 du même code : " L'autorisation de construire, d'aménager ou de modifier un établissement recevant le public prévue à l'article L. 111-8 est délivrée au nom de l'Etat par : / a) Le préfet, lorsque celui-ci est compétent pour délivrer le permis de construire ou lorsque le projet porte sur un immeuble de grande hauteur ; / b) Le maire, dans les autres cas " ;<br/>
<br/>
              4. Considérant qu'une délégation du maire habilitant l'un de ses adjoints à signer toutes les décisions relevant du code de l'urbanisme doit être regardée comme habilitant son titulaire à signer les arrêtés accordant un permis de construire, y compris lorsque le permis tient lieu de l'autorisation prévue par l'article L. 111-8 du code de la construction et de l'habitation pour l'exécution des travaux conduisant à la création, l'aménagement ou la modification d'un établissement recevant du public ; que le permis de construire ne peut toutefois être octroyé qu'avec l'accord de l'autorité compétente pour délivrer cette autorisation ; <br/>
<br/>
              5. Considérant qu'il ressort des énonciations de l'arrêt attaqué que le signataire de la décision attaquée a reçu délégation, par arrêté du 25 mars 2008 du maire de Bourgoin-Jallieu, pour signer en son nom " toutes décisions relevant du code de l'urbanisme et de la compétence propre du maire " ; qu'il résulte de ce qui a été dit ci-dessus qu'en jugeant que l'intéressé n'était pas compétent pour signer le permis de construire litigieux, la cour a commis une erreur de droit ; que la même erreur de droit entache le motif par lequel la cour a rejeté les conclusions subsidiaires en défense présentées par le centre hospitalier Pierre Oudot et tendant à ce que la cour fasse application des dispositions de l'article L. 600-5 du code de l'urbanisme pour ne procéder qu'à une annulation partielle de la décision attaquée, la cour s'étant fondée sur l'absence d'autorisation émise par l'autorité compétente ; que, compte tenu de l'incidence de cette erreur de droit sur le dispositif de l'arrêt, il y a lieu d'annuler celui-ci dans son entier ; <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge de M. A...la somme de 1 500 euros à verser au centre hospitalier Pierre Oudot ; qu'en revanche, ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge du centre hospitalier Pierre Oudot, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 19 février 2013 de la cour administrative d'appel de Lyon est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon. <br/>
Article 3 : M. A...versera au centre hospitalier Pierre Oudot la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de M. A...tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée au centre hospitalier Pierre Oudot et à M. B...A....<br/>
Copie en sera adressée à la commune de Bourgoin-Jallieu.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-01-02-02-03-04 COLLECTIVITÉS TERRITORIALES. COMMUNE. ORGANISATION DE LA COMMUNE. ORGANES DE LA COMMUNE. MAIRE ET ADJOINTS. POUVOIRS DU MAIRE. DÉLÉGATION DES POUVOIRS DU MAIRE. - DÉLÉGATION DU MAIRE HABILITANT UN ADJOINT À SIGNER TOUTES DÉCISIONS RELEVANT DU CODE DE L'URBANISME - PORTÉE - INCLUSION - SIGNATURE DE PERMIS TENANT LIEU DE L'AUTORISATION PRÉVUE POUR LES ERP (ART. L. 111-8 DU CCH) - CONDITION - ACCORD DE L'AUTORITÉ COMPÉTENTE EN MATIÈRE D'ERP.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">49-05-003 POLICE. POLICES SPÉCIALES. - DÉLÉGATION DU MAIRE HABILITANT UN ADJOINT À SIGNER TOUTES DÉCISIONS RELEVANT DU CODE DE L'URBANISME - PORTÉE - INCLUSION - SIGNATURE DE PERMIS TENANT LIEU DE L'AUTORISATION PRÉVUE POUR LES ERP (ART. L. 111-8 CCH) - CONDITION - ACCORD DE L'AUTORITÉ COMPÉTENTE EN MATIÈRE D'ERP.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-03-02-03 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. PROCÉDURE D'ATTRIBUTION. AUTORITÉ COMPÉTENTE POUR STATUER SUR LA DEMANDE. - DÉLÉGATION DU MAIRE HABILITANT UN ADJOINT À SIGNER TOUTES DÉCISIONS RELEVANT DU CODE DE L'URBANISME - PORTÉE - INCLUSION - SIGNATURE DE PERMIS TENANT LIEU DE L'AUTORISATION PRÉVUE POUR LES ERP (ART. L. 111-8 CCH) - CONDITION - ACCORD DE L'AUTORITÉ COMPÉTENTE EN MATIÈRE D'ERP.
</SCT>
<ANA ID="9A"> 135-02-01-02-02-03-04 Une délégation du maire habilitant l'un de ses adjoints à signer toutes les décisions relevant du code de l'urbanisme doit être regardée comme habilitant son titulaire à signer les arrêtés accordant un permis de construire, y compris lorsque le permis tient lieu de l'autorisation prévue par l'article L. 111-8 du code de la construction et de l'habitation (CCH) pour l'exécution des travaux conduisant à la création, l'aménagement ou la modification d'un établissement recevant du public (ERP). Le permis de construire ne peut toutefois être octroyé qu'avec l'accord de l'autorité compétente pour délivrer cette autorisation.</ANA>
<ANA ID="9B"> 49-05-003 Une délégation du maire habilitant l'un de ses adjoints à signer toutes les décisions relevant du code de l'urbanisme doit être regardée comme habilitant son titulaire à signer les arrêtés accordant un permis de construire, y compris lorsque le permis tient lieu de l'autorisation prévue par l'article L. 111-8 du code de la construction et de l'habitation (CCH) pour l'exécution des travaux conduisant à la création, l'aménagement ou la modification d'un établissement recevant du public (ERP). Le permis de construire ne peut toutefois être octroyé qu'avec l'accord de l'autorité compétente pour délivrer cette autorisation.</ANA>
<ANA ID="9C"> 68-03-02-03 Une délégation du maire habilitant l'un de ses adjoints à signer toutes les décisions relevant du code de l'urbanisme doit être regardée comme habilitant son titulaire à signer les arrêtés accordant un permis de construire, y compris lorsque le permis tient lieu de l'autorisation prévue par l'article L. 111-8 du code de la construction et de l'habitation (CCH) pour l'exécution des travaux conduisant à la création, l'aménagement ou la modification d'un établissement recevant du public (ERP). Le permis de construire ne peut toutefois être octroyé qu'avec l'accord de l'autorité compétente pour délivrer cette autorisation.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
