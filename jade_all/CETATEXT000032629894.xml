<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032629894</ID>
<ANCIEN_ID>JG_L_2014_11_000000375269</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/62/98/CETATEXT000032629894.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 05/11/2014, 375269</TITRE>
<DATE_DEC>2014-11-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375269</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Leïla Derouich</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:375269.20141105</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi, enregistré le 6 février 2014 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de l'intérieur ; le ministre de l'intérieur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'ordonnance n° 1310749/13 du 17 janvier 2014 par laquelle le juge des référés du tribunal administratif de Melun, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a suspendu l'exécution de la décision du ministre de l'intérieur du 7 novembre 2013 constatant la perte de validité du permis de conduire de M. B...pour solde de points nul ; <br/>
<br/>
              2°) statuant en référé, de rejeter la demande de  M. A...B... ; <br/>
<br/>
              3°) de mettre à la charge de M. B...la somme de 750 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de la route ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Leïla Derouich, auditeur,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 7 novembre 2013, le ministre de l'intérieur a constaté la perte de validité du permis de conduire de M. B...pour solde de points nul ; que le ministre de l'intérieur se pourvoit en cassation contre l'ordonnance du 17 janvier 2014 par laquelle le juge des référés du tribunal administratif de Melun a, à la demande de M. B..., suspendu cette décision en retenant, d'une part, que la condition d'urgence prévue à l'article L. 521-1 du code de justice administrative était remplie dès lors que l'intéressé exerce la profession de chauffeur de bus et, d'autre part, que le moyen tiré de ce que les informations prévues par les articles L. 223-3 et R. 223-3 ne lui avaient pas été dispensées préalablement aux retraits de points consécutifs à plusieurs des infractions qu'il avait commises créait, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision attaquée ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 223-1 du code de la route, dans sa rédaction applicable au litige : " Le permis de conduire est affecté d'un nombre de points. Celui-ci est réduit de plein droit si le titulaire du permis a commis une infraction pour laquelle cette réduction est prévue. (...) Lorsque le nombre de points est nul, le permis perd sa validité. / La réalité d'une infraction entraînant retrait de points est établie par le paiement d'une amende forfaitaire ou l'émission du titre exécutoire de l'amende forfaitaire majorée, l'exécution d'une composition pénale ou par une condamnation définitive " ; qu'aux termes de l'article L. 223-3 du même code : " Lorsque l'intéressé est avisé qu'une des infractions entraînant retrait de points a été relevée à son encontre, il est informé des dispositions de l'article L. 223-2, de l'existence d'un traitement automatisé de ces points et de la possibilité pour lui d'exercer le droit d'accès conformément aux articles L. 225-1 à L. 225-9. / Lorsqu'il est fait application de la procédure de l'amende forfaitaire ou de la procédure de composition pénale, l'auteur de l'infraction est informé que le paiement de l'amende ou l'exécution de la composition pénale entraîne le retrait du nombre de points correspondant à l'infraction reprochée, dont la qualification est dûment portée à sa connaissance ; il est également informé de l'existence d'un traitement automatisé de ces points et de la possibilité pour lui d'exercer le droit d'accès. / Le retrait de points est porté à la connaissance de l'intéressé par lettre simple quand il est effectif " ; qu'aux termes de l'article R. 223-3 du même code : " I- Lors de la constatation d'une infraction entraînant retrait de points, l'auteur de celle-ci est informé qu'il encourt un retrait de points si la réalité de l'infraction est établie dans les conditions définies à l'article L. 223-1. / II. - Il est informé également de l'existence d'un traitement automatisé des retraits et reconstitutions de points et de la possibilité pour lui d'accéder aux informations le concernant. Ces mentions figurent sur le document qui lui est remis ou adressé par le service verbalisateur. Le droit d'accès aux informations ci-dessus mentionnées s'exerce dans les conditions fixées par les articles L. 225-1 à L. 225-9. / III. - Lorsque le ministre de l'intérieur constate que la réalité d'une infraction entraînant retrait de points est établie dans les conditions prévues par le quatrième alinéa de l'article L. 223-1, il réduit en conséquence le nombre de points affecté au permis de conduire de l'auteur de cette infraction et en informe ce dernier par lettre simple. Le ministre de l'intérieur constate et notifie à l'intéressé, dans les mêmes conditions, les reconstitutions de points obtenues en application des alinéas 1 et 3 de l'article L. 223-6 (...) " ; <br/>
<br/>
              Sur l'infraction du 19 avril 2012<br/>
<br/>
              3. Considérant qu'en application du second alinéa de l'article 529-2 du code de procédure pénale, en l'absence de paiement ou de requête en exonération dans le délai de quarante-cinq jours suivant, selon les cas, la date de constatation de l'infraction ou la date d'envoi de l'avis de contravention, l'amende forfaitaire est majorée de plein droit et recouvrée en vertu d'un titre rendu exécutoire par le ministère public ; que le paiement de l'amende forfaitaire majorée établit que le contrevenant a reçu un avis d'amende forfaitaire majorée ; qu'il ressort des pièces du dossier soumis au juge des référés qu'avant même que ces mentions ne soient rendues obligatoires par un arrêté du 13 mai 2011 introduisant dans le code de procédure pénale un article A. 37-28, le formulaire d'avis d'amende forfaitaire majorée utilisé par l'administration rappelait la qualification de l'infraction au code de la route et précisait que l'émission de l'amende forfaitaire majorée pouvait entraîner un retrait de points du permis de conduire, que cette amende pouvait être contestée dans un délai de trois mois, que les retraits et reconstitutions de points faisaient l'objet d'un traitement automatisé et que le titulaire du permis pouvait accéder à ces informations ; que ces indications mettaient le contrevenant en mesure de comprendre qu'en l'absence de contestation de l'amende il serait procédé au retrait de points et portaient à sa connaissance l'ensemble des informations requises par les articles L. 223-3 et R. 223-3 précités du code de la route ; que, dans ces conditions, lorsqu'il est établi que le titulaire du permis de conduire a payé l'amende forfaitaire majorée, il découle de cette seule constatation qu'il doit être regardé comme établi que l'administration s'est acquittée envers lui de son obligation de lui délivrer, préalablement au paiement de l'amende, les informations requises, à moins que l'intéressé, à qui il appartient à cette fin de produire l'avis qu'il a nécessairement reçu, démontre avoir été destinataire d'un avis inexact ou incomplet ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis au juge des référés, notamment du relevé intégral d'information et de l'attestation de paiement établie par la trésorerie de Seine-et-Marne, que M. B...a payé l'amende forfaitaire majorée correspondant à l'infraction du 19 avril 2012 ; que, dès lors, en estimant propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision du ministre le moyen tiré de ce que l'administration n'avait pas apporté la preuve de ce que l'information prévue aux articles L. 223-3 et R. 223-3 du code de la route avait été délivrée à M.B..., le juge des référés a commis une erreur de droit ; <br/>
<br/>
              Sur les infractions commises les 9 août 2009, 27 mars 2010 et 14 novembre 2012<br/>
<br/>
              5. Considérant que le juge des référés du tribunal administratif de Melun a jugé que la preuve de la délivrance de l'information requise n'était pas davantage apportée pour les infractions commises les 9 août 2009, 27 mars 2010 et 14 novembre 2012 ; que, toutefois, il ressort des pièces du dossier qui lui était soumis, notamment du relevé intégral d'information, d'une part, qu'aucune infraction en date du 27 mars 2010 ou du 14 novembre 2012 n'était reprochée à M. B...et, d'autre part, que l'infraction du 9 août 2009 avait fait l'objet d'une décision de réattribution de points en application du deuxième alinéa de l'article L. 223-6 du code de la route et n'avait donc pas été prise en compte par le ministre lors de l'invalidation du permis pour solde de points nul ; que, par suite, en se prononçant comme il l'a fait, le juge des référés du tribunal administratif de Melun a entaché son ordonnance d'erreurs de fait ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que l'ordonnance attaquée doit être annulée ; <br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de statuer sur la demande de suspension de M. B...en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              8. Considérant qu'aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision (...) " ; <br/>
<br/>
              9. Considérant que, pour demander la suspension de la décision du 7 novembre 2013 du ministre de l'intérieur, M. B...soutient qu'il n'a pas bénéficié de l'information préalable prévue aux articles L. 223-3 et R. 223-3 du code de la route préalablement aux retraits de points de son permis de conduire, qu'il n'a pas été informé de la possibilité d'obtenir la reconstitution partielle de son nombre de points initial s'il se soumettait à une formation spécifique prévue par l'article L. 223-6 du code de la route et que les retraits de points ne lui ont pas été notifiés ; qu'aucun de ces moyens n'est, en l'état de l'instruction, de nature à faire naître un doute sérieux quant à la légalité de la décision du ministre de l'intérieur du 7 novembre 2013 ; <br/>
<br/>
              10. Considérant que l'une des conditions posées par l'article L. 521-1 du code de justice administrative n'étant pas remplie, la demande présentée par M. B...devant le juge des référés du tribunal administratif de Melun tendant à ce que soit ordonnée la suspension de la décision du 7 novembre 2013 du ministre de l'intérieur doit être rejetée ;<br/>
<br/>
              11. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ; qu'il résulte par ailleurs de ces dernières dispositions que, si une personne publique qui n'a pas eu recours au ministère d'avocat peut néanmoins demander au juge l'application de cet article au titre des frais spécifiques exposés par elle à l'occasion de l'instance, elle ne saurait se borner à faire état d'un surcroît de travail de ses services ; que les éléments avancés par le ministre de l'intérieur, qui énonçait que ce type de recours représentait une charge importante pour ses services sans faire état précisément d'autres frais que l'Etat aurait exposés pour défendre à l'instance, ne sont pas de nature à justifier qu'une somme soit mise, à ce titre, à la charge de M.B... ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Melun du 17 janvier 2014 est annulée. <br/>
<br/>
Article 2 : La demande présentée par M. B...devant le juge des référés du tribunal administratif de Melun est rejetée. <br/>
<br/>
Article 3 : Les conclusions présentées par l'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">49-04-01-04-025 POLICE. POLICE GÉNÉRALE. CIRCULATION ET STATIONNEMENT. PERMIS DE CONDUIRE. - OBLIGATION D'INFORMATION DU TITULAIRE DU PERMIS DE CONDUIRE EN CAS DE RETRAIT DE POINTS (ART. L. 223-3 ET R. 223-3 DU CODE DE LA ROUTE) - PAIEMENT DE L'AMENDE FORFAITAIRE MAJORÉE - CIRCONSTANCE ÉTABLISSANT LA DÉLIVRANCE DE L'INFORMATION REQUISE - EXISTENCE, EN PRINCIPE [RJ1].
</SCT>
<ANA ID="9A"> 49-04-01-04-025 En application du second alinéa de l'article 529-2 du code de procédure pénale, en l'absence de paiement ou de requête en exonération, l'amende forfaitaire est majorée de plein droit et recouvrée en vertu d'un titre rendu exécutoire par le ministère public. Le paiement de l'amende forfaitaire majorée établit que le contrevenant a reçu un avis d'amende forfaitaire majorée. Il ressort des pièces du dossier que le formulaire d'avis d'amende forfaitaire majorée contenait de fait des informations suffisantes pour porter à sa connaissance l'ensemble des informations requises par les articles L. 223-3 et R. 223-3 du code de la route, informations qui doivent désormais figurer dans ces avis en application de l'article A. 37-28 du code de procédure pénale issu d'un arrêté du 13 mai 2011.... ,,Dans ces conditions, lorsqu'il est établi que le titulaire du permis de conduire a payé l'amende forfaitaire majorée, il découle de cette seule constatation qu'il doit être regardé comme établi que l'administration s'est acquittée envers lui de son obligation de lui délivrer, préalablement au paiement de l'amende, les informations requises, à moins que l'intéressé, à qui il appartient à cette fin de produire l'avis qu'il a nécessairement reçu, démontre avoir été destinataire d'un avis inexact ou incomplet.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 20 novembre 2009, M. Sellem, n° 329982, p. 468.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
