<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000045118492</ID>
<ANCIEN_ID>JG_L_2022_02_000000455703</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/45/11/84/CETATEXT000045118492.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 02/02/2022, 455703, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2022-02-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>455703</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET</AVOCATS>
<RAPPORTEUR>M. Lionel Ferreira</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Céline  Guibé</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2022:455703.20220202</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Une Pièce en Plus a demandé au tribunal administratif de Montreuil de la décharger des cotisations supplémentaires de la décharger des cotisations supplémentaires de taxe annuelle sur les bureaux, les locaux commerciaux et de stockage et les surfaces de stationnement en Île-de-France auxquelles elle a été assujettie au titre des années 2012 et 2013 à raison de ses établissements situés route de Chartres à Trappes, 20, avenue de l'Europe à Vélizy-Villacoublay et 200, quai de Clichy à Clichy. Par un jugement n° 1803393 du 19 février 2019, le tribunal administratif de Montreuil a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 19VE01171 du 16 juillet 2021, la cour administrative d'appel de Versailles a, sur appel de la société Une Pièce en Plus, prononcé un non-lieu à statuer partiel sur les conclusions de la société Une Pièce en Plus et rejeté le surplus des conclusions de sa requête.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 août et 18 novembre 2021 au secrétariat du contentieux du Conseil d'Etat, la société Une Pièce en Plus demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il lui est défavorable ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire entièrement droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Lionel Ferreira, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Céline Guibé, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de la société Une Pièce en Plus ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              2. Pour demander l'annulation de l'arrêt qu'elle attaque, la société Une Pièce en Plus soutient que la cour administrative d'appel de Versailles :<br/>
              - a commis une erreur de droit et inexactement qualifié les faits ou, à tout le moins, a dénaturé les faits et pièces du dossier, en jugeant que les espaces de circulation intérieure desservant les boxes de " self-stockage " ne pouvaient être qualifiés de parties communes au sens de l'article 231 ter du code général des impôts ;<br/>
              - a commis une erreur de droit en jugeant que la société ne pouvait se prévaloir de la position formelle prise par l'administration fiscale dans sa lettre en date du 24 août 2005 qualifiant de parties communes les voies de circulation desservant les boxes individuels aménagés dans les locaux qu'elle avait pris à bail sur un autre site ;<br/>
              - a commis une erreur de droit et inexactement qualifié les faits ou, à tout le moins, dénaturé les faits et pièces du dossier en jugeant que les aires de chargement et de déchargement comprises dans les établissements litigieux devaient être qualifiées de surfaces de stationnement au sens de l'article 231 ter du code général des impôts ;<br/>
              - a commis une erreur de droit en jugeant que la société ne pouvait se prévaloir de l'application du paragraphe 280 de l'instruction BOI-IF-AUT-50-10 du 12 décembre 2013 pour soutenir que les aires de livraison comprises ne devaient pas être considérées comme des surfaces de stationnement au sens de l'article 231 ter du code général des impôts ;<br/>
              - a commis une erreur de droit et inexactement qualifié les faits en jugeant que les espaces de chargement et de déchargement devaient être qualifiés de surfaces de stationnement au sens de l'article 231 ter du code général des impôts, sans rechercher si une grande partie de ces espaces ne constituait pas des voies de circulation non imposables à cette taxe.<br/>
<br/>
              3. Eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi qui sont dirigées contre l'arrêt attaqué en tant qu'il a statué sur les cotisations supplémentaires de taxe annuelle sur les bureaux, les locaux commerciaux et de stockage et les surfaces de stationnement en Île-de-France correspondant aux espaces de circulation intérieure. En revanche, aucun des moyens soulevés n'est de nature à permettre l'admission du surplus des conclusions. <br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions du pourvoi de la société Une Pièce en Plus qui sont dirigées contre l'arrêt attaqué en tant qu'il a statué sur les cotisations supplémentaires de taxe annuelle sur les bureaux, les locaux commerciaux et de stockage et les surfaces de stationnement en Île-de-France correspondant aux espaces de circulation intérieure sont admises.<br/>
Article 2 : Le surplus des conclusions du pourvoi de la société Une Pièce en Plus n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à la société Une Pièce en Plus.<br/>
Copie en sera adressée au ministre de l'économie, des finances et de la relance.<br/>
              Délibéré à l'issue de la séance du 13 janvier 2022 où siégeaient : M. Frédéric Aladjidi, président de chambre, présidant ; Mme Catherine Fischer-Hirtz, conseillère d'Etat et M. Lionel Ferreira, maître des requêtes en service extraordinaire-rapporteur. <br/>
<br/>
              Rendu le 2 février 2022.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Frédéric Aladjidi<br/>
 		Le rapporteur : <br/>
      Signé : M. Lionel Ferreira<br/>
                 La secrétaire :<br/>
                 Signé : Mme B... A...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
