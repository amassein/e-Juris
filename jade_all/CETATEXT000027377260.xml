<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027377260</ID>
<ANCIEN_ID>JG_L_2013_04_000000349996</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/37/72/CETATEXT000027377260.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 24/04/2013, 349996, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-04-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349996</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BLANC, ROUSSEAU ; SCP PEIGNOT, GARREAU, BAUER-VIOLAS</AVOCATS>
<RAPPORTEUR>M. Romain Victor</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:349996.20130424</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi et le mémoire complémentaire, enregistrés les 8 juin et 8 septembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour le Centre départemental de gestion de la fonction publique territoriale (CGFPT) du Var, dont le siège est Les Cyclades 1766 chemin de la Planquette BP 90130 à La Garde (83867) ; le CGFPT du Var demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 1000366-1000981 du 8 avril 2011 par lequel le tribunal administratif de Toulon, faisant droit à la demande de M.A..., a annulé l'arrêté n°2009-191 du 8 juin 2009 par lequel son président a prononcé l'intégration de Mme B...dans le cadre d'emploi des attachés territoriaux ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M.A... ; <br/>
<br/>
              3°) de mettre à la charge de M. A...une somme de 3000 euros au titre de l'article L.761-1 du code de justice administrative ; <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des communes, notamment son article L. 412-2 ;<br/>
<br/>
              Vu la loi n° 53-84 du 26 janvier 1984, notamment son article 139 ter ; <br/>
<br/>
              Vu le décret n° 87-1099 du 30 décembre 1987 ;<br/>
<br/>
              Vu le décret n° 2009-414 du 15 avril 2009 ;<br/>
<br/>
              Vu l'arrêté du 27 juin 1962 du ministre de l'intérieur relatif au recrutement du personnel administratif communal ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Romain Victor, Maître des Requêtes en service extraordinaire, <br/>
<br/>
              - les observations de la SCP Peignot, Garreau, Bauer-Violas, avocat du CGFPT du Var et de la SCP Blanc, Rousseau, avocat de M.A...,<br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Peignot, Garreau, Bauer-Violas, avocat du CGFPT du Var et à la SCP Blanc, Rousseau, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au tribunal administratif que, par arrêté n° 2009-191 du 8 juin 2009, le président du centre de gestion de la fonction publique territoriale (CGFPT) du Var a prononcé l'intégration de Mme B...dans le cadre d'emploi des attachés territoriaux ; que le CGFPT du Var se pourvoit en cassation contre le jugement du 8 avril 2011 par lequel le tribunal administratif de Toulon a fait droit à la demande de M.A..., directeur territorial, tendant à l'annulation de cet arrêté ;<br/>
<br/>
              2. Considérant, en premier lieu, que les agents appartenant à une administration publique ont qualité pour déférer à la juridiction administrative les nominations illégales faites dans cette administration, lorsque ces nominations sont de nature à leur porter préjudice en retardant irrégulièrement leur  avancement ou en leur donnant pour cet avancement des concurrents qui ne satisfont pas aux conditions exigées par les lois ou règlements ; qu'aux termes de l'article 1er du décret du 30 décembre 1987 portant statut particulier du cadre d'emplois des attachés territoriaux : " Les attachés territoriaux constituent un cadre d'emplois administratif de catégorie A au sens de l'article 5 de la loi n° 84-53 du 26 janvier 1984 susvisée. / Ce cadre d'emplois comprend les grades d'attaché, d'attaché principal, de directeur territorial " ; que, dès lors, le tribunal administratif n'a pas commis d'erreur de droit en jugeant que M.A..., directeur territorial, affecté au CGFPT du Var, devait être regardé comme justifiant d'un intérêt suffisant lui donnant qualité pour agir contre la décision prononçant l'intégration de Mme B...dans le cadre d'emploi des attachés territoriaux, qui était susceptible de faire de cette dernière une concurrente pour son avancement à des grades ou emplois supérieurs ;<br/>
<br/>
              3. Considérant, en second lieu, que la notion d'emploi spécifique a été définie par l'article L. 412-2 du code des communes, qui énonce que le conseil municipal ou le comité du syndicat de communes pour le personnel communal fixe, par délibérations, les conditions de recrutement pour l'accès à ceux des emplois pour lesquels ces conditions n'ont pas été déterminées par une réglementation particulière ; que Mme B...a été recrutée par un arrêté du 30 septembre 1987 dans un emploi communal de secrétaire général des villes de 10 000 à 20 000 habitants ; que cet emploi créé par le CGFPT du Var par référence à un emploi de secrétaire général des villes de 10 000 à 20 000 habitants et régi par les dispositions de l'arrêté du 27 juin 1962 relatif aux conditions de recrutement du personnel administratif communal, n'est pas un emploi spécifique au sens de l'article L. 412-2 précité ; que, dès lors, le tribunal administratif n'a pas commis d'erreur de droit en jugeant que, faute d'occuper un emploi spécifique, Mme B...ne pouvait être intégrée dans le cadre d'emploi des attachés territoriaux sur le fondement des dispositions de l'article 139 ter de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale et du décret du 15 avril 2009 fixant les conditions d'intégration dans des cadres d'emplois de la fonction publique territoriale de certains agents titulaires d'un emploi spécifique de catégorie A ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du CGFPT du Var la somme de 3 000 euros à verser à M. A...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;  qu'en revanche, ces mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. A...qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du CGFPT du Var est rejeté. <br/>
<br/>
Article 2 : Le CGFPT du Var versera à M. A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 3 : Les conclusions présentées par le CGFPT du Var au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée au centre de gestion de la fonction publique territoriale du Var et à M. C... A....<br/>
Copie en sera transmise pour information à MmeB....<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
