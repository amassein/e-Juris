<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026454647</ID>
<ANCIEN_ID>JG_L_2012_10_000000349335</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/45/46/CETATEXT000026454647.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  2ème sous-section jugeant seule, 03/10/2012, 349335, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-10-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349335</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 2ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jacques-Henri Stahl</PRESIDENT>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Damien Botteghi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:349335.20121003</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 16 mai et 12 août 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme Gnilane B, demeurant ... ; Mme B demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08/00039 du 25 mars 2010 par lequel la cour régionale des pensions de Paris a infirmé le jugement n° 05/00052 du 8 avril 2008 par lequel le tribunal départemental des pensions de Paris a fait droit à sa demande de décristallisation de la pension militaire d'invalidité dont était titulaire son époux et condamné l'Etat à lui verser les arrérages de la pension due depuis le 1er janvier 2002, assortie des intérêts au taux légal ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel formé par le ministre de la défense ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros à verser à la SCP Delaporte, Briard, Trichet, son avocat, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment ses articles 61-1 et 62 ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code des pensions militaires d'invalidité et des victimes de la guerre ;<br/>
<br/>
              Vu la loi n° 59-1454 du 26 décembre 1959 ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu la loi n° 2002-1576 du 30 décembre 2002, notamment son article 68 ;<br/>
<br/>
              Vu la loi n° 2010-1657 du 29 décembre 2010, notamment son article 211 ;<br/>
<br/>
              Vu la décision n° 2010-1 QPC du 28 mai 2010 du Conseil constitutionnel ;<br/>
<br/>
              Vu la décision n° 2010-108 QPC du 25 mars 2011 du Conseil constitutionnel ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, Conseiller d'Etat,<br/>
<br/>
              - les observations de la SCP Delaporte, Briard, Trichet, avocat de Mme B,<br/>
<br/>
              - les conclusions de M. Damien Botteghi, Rapporteur public,<br/>
<br/>
              La parole ayant à nouveau été donnée à la SCP Delaporte, Briard, Trichet, avocat de Mme B ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. Traoré C, ressortissant sénégalais ayant servi dans l'armée française du 16 janvier 1947 au 1er octobre 1953, a été admis par arrêté du 8 juillet 1980 à compter du 1er janvier 1975 au bénéfice d'une pension militaire d'invalidité, consistant en une indemnité personnelle et viagère en application des dispositions de l'article 71 de la loi du 26 décembre 1959 de finances pour 1960 ; que M. C est décédé le 23 avril 1998 ; que sa veuve Mme Gnilane B, ressortissante sénégalaise, a sollicité le bénéfice d'une pension de réversion du chef de son époux décédé ; que, par arrêté du 21 mars 2005, le ministre de la défense a accordé à Mme B, avec jouissance rétroactive à compter du 1er janvier 2002, une pension de réversion calculée sur le fondement des dispositions de l'article 68 de la loi du 30 décembre 2002 de finances rectificative pour 2002 ; que, par une demande formée le 17 août 2005, Mme B a contesté cette décision ; que, par un jugement du 8 avril 2008, le tribunal départemental des pensions de Paris a fait droit à sa demande et enjoint à l'administration de réviser ses droits à pension ; que, toutefois, par un arrêt du 25 mars 2010, contre lequel la requérante se pourvoit en cassation, la cour régionale des pensions de Paris a infirmé ce jugement ;<br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article 61-1 de la Constitution : " Lorsque, à l'occasion d'une instance en cours devant une juridiction, il est soutenu qu'une disposition législative porte atteinte aux droits et libertés que la Constitution garantit, le Conseil constitutionnel peut être saisi de cette question sur renvoi du Conseil d'Etat ou de la Cour de cassation " ; qu'aux termes du deuxième alinéa de son article 62 : " Une disposition déclarée inconstitutionnelle sur le fondement de l'article 61-1 est abrogée à compter de la publication de la décision du Conseil constitutionnel ou d'une date ultérieure fixée par cette décision. Le Conseil constitutionnel détermine les conditions et limites dans lesquelles les effets que la disposition a produits sont susceptibles d'être remis en cause " ; qu'enfin, aux termes du troisième alinéa du même article : " Les décisions du Conseil constitutionnel ne sont susceptibles d'aucun recours. Elles s'imposent aux pouvoirs publics et à toutes les autorités administratives et juridictionnelles " ;<br/>
<br/>
              3. Considérant qu'il résulte des dispositions précitées de l'article 62 de la Constitution qu'une disposition législative déclarée contraire à la Constitution sur le fondement de l'article 61-1 n'est pas annulée rétroactivement mais abrogée pour l'avenir à compter de la publication de la décision du Conseil constitutionnel ou d'une date ultérieure fixée par cette décision ; que, par sa décision n° 2010-108 QPC en date du 25 mars 2011, le Conseil constitutionnel a jugé que " si, en principe, la déclaration d'inconstitutionnalité doit bénéficier à l'auteur de la question prioritaire de constitutionnalité et la disposition déclarée contraire à la Constitution ne peut être appliquée dans les instances en cours à la date de la publication de la décision du Conseil constitutionnel, les dispositions de l'article 62 de la Constitution réservent à ce dernier le pouvoir tant de fixer la date de l'abrogation et reporter dans le temps ses effets que de prévoir la remise en cause des effets que la disposition a produits avant l'intervention de cette déclaration " ;<br/>
<br/>
              4. Considérant que, lorsque le Conseil constitutionnel, après avoir abrogé une disposition déclarée inconstitutionnelle, use du pouvoir que lui confèrent les dispositions précitées, soit de déterminer lui-même les conditions et limites dans lesquelles les effets que la disposition a produits sont susceptibles d'être remis en cause, soit de décider que le législateur aura à prévoir une application aux instances en cours des dispositions qu'il aura prises pour remédier à l'inconstitutionnalité constatée, il appartient au juge, saisi d'un litige relatif aux effets produits par la disposition déclarée inconstitutionnelle, de les remettre en cause en écartant, pour la solution de ce litige, le cas échéant d'office, cette disposition, dans les conditions et limites fixées par le Conseil constitutionnel ou le législateur ;<br/>
<br/>
              5. Considérant que, par sa décision n° 2010-1 QPC du 28 mai 2010, le Conseil constitutionnel a déclaré contraires à la Constitution les dispositions de l'article 68 de la loi du 30 décembre 2002 de finances rectificative pour 2002, à l'exception de celles de son paragraphe VII ; qu'il a jugé que : " afin de permettre au législateur de remédier à l'inconstitutionnalité constatée, l'abrogation des dispositions précitées prendra effet à compter du 1er janvier 2011 ; afin de préserver l'effet utile de la présente décision à la solution des instances actuellement en cours, il appartient, d'une part, aux juridictions de surseoir à statuer jusqu'au 1er janvier 2011 dans les instances dont l'issue dépend de l'application des dispositions déclarées inconstitutionnelles et, d'autre part, au législateur de prévoir une application des nouvelles dispositions à ces instances en cours à la date de la présente décision " ;<br/>
<br/>
              6. Considérant que, à la suite de cette décision, l'article 211 de la loi du 29 décembre 2010 de finances pour 2011 a défini de nouvelles dispositions pour le calcul des pensions militaires d'invalidité, des pensions civiles et militaires de retraite et des retraites du combattant servies aux ressortissants des pays ou territoires ayant appartenu à l'Union française ou à la Communauté ou ayant été placés sous le protectorat ou sous la tutelle de la France et abrogé plusieurs dispositions législatives, notamment celles de l'article 71 de la loi du 26 décembre 1959 portant loi de finances pour 1960 ; que, par ailleurs, son paragraphe VI prévoit que " le présent article est applicable aux instances en cours à la date du 28 mai 2010, la révision des pensions prenant effet à compter de la date de réception par l'administration de la demande qui est à l'origine de ces instances " ; qu'enfin, aux termes du XI du même article : " Le présent article entre en vigueur au 1er janvier 2011 " ;<br/>
<br/>
              7. Considérant que, comme il a été dit, le Conseil constitutionnel a jugé qu'il appartenait au législateur de prévoir une application aux instances en cours à la date de sa décision des dispositions qu'il adopterait en vue de remédier à l'inconstitutionnalité constatée ; que l'article 211 de la loi de finances pour 2011 ne se borne pas à déterminer les règles de calcul des pensions servies aux personnes qu'il mentionne, mais abroge aussi des dispositions qui définissent, notamment, les conditions dans lesquelles est ouvert le droit à une pension de réversion ; qu'ainsi, alors même qu'il mentionne seulement la " révision des pensions ", le paragraphe VI de l'article 211 précité doit être regardé comme s'appliquant aussi aux demandes de pension de réversion ;<br/>
              8. Considérant que, pour statuer sur la demande de pension de réversion présentée par Mme B, la cour régionale des pensions de Paris s'est exclusivement fondée sur les dispositions de l'article 68 de la loi de finances rectificative pour 2002 ; qu'il s'ensuit qu'afin de préserver l'effet utile de la décision du Conseil constitutionnel à la solution de l'instance ouverte par la demande de Mme B, en permettant au juge du fond de remettre en cause, dans les conditions et limites définies par le paragraphe VI de l'article 211 de la loi de finances pour 2011, les effets produits par les dispositions mentionnées ci-dessus, il incombe au juge de cassation, sans qu'il soit besoin d'examiner les moyens du pourvoi, d'annuler l'arrêt attaqué ;<br/>
<br/>
              9. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat les sommes demandées par les avocats de Mme B au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt du 25 mars 2010 de la cour régionale des pensions de Paris est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée devant la cour régionale des pensions de Versailles.<br/>
<br/>
Article 3 : Le surplus des conclusions de la SCP Delaporte, Briard, Trichet, avocat de Mme B est rejeté.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme Gnilane B et au ministre de la défense.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
