<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027689964</ID>
<ANCIEN_ID>JG_L_2013_07_000000364507</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/68/99/CETATEXT000027689964.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 10/07/2013, 364507, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-07-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364507</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SPINOSI</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Damien Botteghi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:364507.20130710</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête et le mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat le 13 décembre 2012 et le 28 février 2013, présentés pour M. A...B..., domicilié ...; M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret en date du 23 août 2012 par lequel le Premier Ministre a accordé son extradition aux autorités espagnoles ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 500 euros sur le fondement de l'article L. 761 1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              Vu la convention européenne d'extradition du 13 décembre 1957 ; <br/>
<br/>
              Vu la convention établie sur la base de l'article K 3 du traité sur l'Union européenne, relative à l'extradition entre les Etats membres de l'Union européenne, signée à Dublin le 27 septembre 1996 ;<br/>
<br/>
              Vu le code pénal ;<br/>
<br/>
              Vu le code de procédure pénale ;<br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, Conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Damien Botteghi, Rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Spinosi, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Sur la légalité externe : <br/>
<br/>
              1.	Considérant, d'une part, qu'il ressort des mentions de l'ampliation du décret attaqué figurant au dossier et certifiée conforme par le secrétaire général du Gouvernement que le décret a été signé par le Premier ministre et contresigné par la garde des sceaux, ministre de la justice ; que l'ampliation notifiée à M. B...n'avait pas à être revêtue de ces signatures ; <br/>
<br/>
              2.	Considérant, d'autre part, que le décret attaqué comporte l'énoncé des considérations de fait et de droit qui en constituent le fondement ; qu'il satisfait ainsi à l'exigence de motivation prévue par l'article 3 de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public ; qu'aucune disposition n'exige que le décret d'extradition mentionne la qualification juridique des faits au regard de la législation de l'Etat requis ;<br/>
<br/>
              Sur la légalité interne : <br/>
<br/>
              3.	Considérant qu'il ressort des pièces du dossier que, contrairement à ce qui est soutenu, le Gouvernement a procédé à un examen complet des circonstances de l'affaire avant d'accorder l'extradition aux autorités espagnoles ;<br/>
<br/>
              4.	Considérant que, si M. B...soutient qu'il n'a pu commettre les faits délictueux qui lui sont reprochés, il résulte des principes généraux du droit applicables à l'extradition qu'il n'appartient pas aux autorités françaises, sauf en cas d'erreur évidente, de statuer sur le bien-fondé des charges retenues contre la personne recherchée ; qu'en l'espèce, il ne ressort pas des pièces du dossier qu'une erreur évidente ait été commise en ce qui concerne les faits reprochés à M. B...;<br/>
<br/>
              5.	Considérant que les faits reprochés à M. B...reçoivent, en droit français, la qualification de tentative de meurtre et d'assassinat, faits prévus et réprimés par les articles 121-4, 121-5, 221-1 et 221-3 du code pénal ; que, par suite, le moyen tiré de ce que les faits ne seraient pas punissables en droit français et de ce que la règle de la double incrimination n'aurait ainsi pas été respectée doit être écarté ;<br/>
<br/>
              6.	Considérant qu'il résulte de tout ce qui précède que M. B...n'est pas fondé à demander l'annulation du décret du 23 août 2012 accordant son extradition aux autorités espagnoles ; que, par voie de conséquence, ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B...et à la garde des sceaux, ministre de la justice.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
