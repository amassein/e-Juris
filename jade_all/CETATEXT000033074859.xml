<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033074859</ID>
<ANCIEN_ID>JG_L_2016_08_000000401472</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/07/48/CETATEXT000033074859.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 26/08/2016, 401472, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-08-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401472</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2016:401472.20160826</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 13 juillet 2016 au secrétariat du contentieux du Conseil d'Etat, MM. B...D...et C...A..., députés et membres de la commission des finances, de l'économie générale et du contrôle budgétaire de l'Assemblée nationale, demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution du décret n° 2016-732 du 2 juin 2016 portant ouverture et annulation de crédits à titre d'avance ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Ils soutiennent que : <br/>
              - leur requête est recevable, en ce qu'ils ont qualité pour contester le décret mis en cause en leur qualité de parlementaires et de membres de la commission des finances de l'Assemblée nationale ; <br/>
              - la condition d'urgence posée par l'article L. 521-1 du code de justice administrative est remplie, en ce que le décret contesté préjudicie de manière grave et immédiate aux prérogatives du Parlement ;<br/>
              - il existe un doute sérieux sur la légalité du décret contesté, d'une part faute de nouvelle consultation de la commission des finances de l'Assemblée nationale sur les modifications apportées au projet de décret après l'avis émis le 24 mai 2016, d'autre part faute que soit remplie la condition d'urgence requise par l'article 13 de la loi organique relative aux lois de finances du 1er août 2001 pour l'intervention d'un décret d'avance.<br/>
<br/>
<br/>
              	Par un mémoire en défense, enregistré le 5 août 2016, le ministre des finances et des comptes publics conclut au rejet de la requête. Il fait valoir que :<br/>
              - la qualité de parlementaire ne confère pas un intérêt donnant qualité pour contester le décret mis en cause, mais que les demandeurs peuvent se prévaloir de leur qualité de membres de la commission des finances ; <br/>
              - la demande de suspension est irrecevable, dans la mesure où le décret contesté a produit tous ses effets le 8 juin 2016, avant l'introduction de la demande ; <br/>
              - la condition d'urgence de l'article L. 521-1 du code de justice administrative n'est pas remplie ; <br/>
              - les moyens soulevés ne sont pas de nature à faire naître un doute sérieux quant à la légalité du décret. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi organique n° 2001-692 du 1er août 2001 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, MM. D... et A..., d'autre part, le Premier ministre et le ministre des finances et des comptes publics ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 24 août 2016 à 10 heures  au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Pinatel, avocat au Conseil d'Etat et à la Cour de cassation, avocat de MM. D... et A...;<br/>
<br/>
              - M. D...et M.A... ;<br/>
<br/>
              - les représentants du ministre des finances et des comptes publics ;<br/>
<br/>
              et à l'issue de laquelle l'instruction a été close ; <br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; <br/>
              2. Considérant qu'en vertu des dispositions des I et II de l'article 7 de la loi organique du 1er août 2001 relative aux lois de finances : " I. - Les crédits ouverts par les lois de finances pour couvrir chacune des charges budgétaires de l'Etat sont regroupés par mission relevant d'un ou plusieurs services d'un ou plusieurs ministères. / Une mission comprend un ensemble de programmes concourant à une politique publique définie. (...) / Toutefois, une mission spécifique regroupe les crédits des pouvoirs publics, chacun d'entre eux faisant l'objet d'une ou de plusieurs dotations. De même, une mission regroupe les crédits des deux dotations suivantes : / 1° Une dotation pour dépenses accidentelles, destinée à faire face à des calamités, et pour dépenses imprévisibles ; / 2° Une dotation pour mesures générales en matière de rémunérations dont la répartition par programme ne peut être déterminée avec précision au moment du vote des crédits. / Un programme regroupe les crédits destinés à mettre en oeuvre une action ou un ensemble cohérent d'actions relevant d'un même ministère et auquel sont associés des objectifs précis, définis en fonction de finalités d'intérêt général, ainsi que des résultats attendus et faisant l'objet d'une évaluation. / II. - Les crédits sont spécialisés par programme ou par dotation (...) " ; que le IV du même article 7 dispose que : " IV. - Les crédits ouverts sont mis à la disposition des ministres. / Les crédits ne peuvent être modifiés que par une loi de finances ou, à titre exceptionnel, en application des dispositions prévues aux articles 11 à 15, 17, 18 et 21. / La répartition des emplois autorisés entre les ministères ne peut être modifiée que par une loi de finances ou, à titre exceptionnel, en application du II de l'article 12 " ; <br/>
              3. Considérant qu'aux termes de l'article 8 de la même loi organique : " Les crédits ouverts sont constitués d'autorisations d'engagement et de crédits de paiement. / Les autorisations d'engagement constituent la limite supérieure des dépenses pouvant être engagées. Pour une opération d'investissement, l'autorisation d'engagement couvre un ensemble cohérent et de nature à être mis en service ou exécuté sans adjonction. (...) / Les crédits de paiement constituent la limite supérieure des dépenses pouvant être ordonnancées ou payées pendant l'année pour la couverture des engagements contractés dans le cadre des autorisations d'engagement (...) " ; <br/>
              4. Considérant qu'aux termes des trois premiers alinéas de l'article 13 de la même loi organique : " En cas d'urgence, des décrets d'avance pris sur avis du Conseil d'État et après avis des commissions de l'Assemblée nationale et du Sénat chargées des finances peuvent ouvrir des crédits supplémentaires sans affecter l'équilibre budgétaire défini par la dernière loi de finances. A cette fin, les décrets d'avance procèdent à l'annulation de crédits ou constatent des recettes supplémentaires. Le montant cumulé des crédits ainsi ouverts ne peut excéder 1 % des crédits ouverts par la loi de finances de l'année. / La commission chargée des finances de chaque assemblée fait connaître son avis au Premier ministre dans un délai de sept jours à compter de la notification qui lui a été faite du projet de décret. La signature du décret ne peut intervenir qu'après réception des avis de ces commissions ou, à défaut, après l'expiration du délai susmentionné. / La ratification des modifications apportées, sur le fondement des deux alinéas précédents, aux crédits ouverts par la dernière loi de finances est demandée au Parlement dans le plus prochain projet de loi de finances afférent à l'année concernée " ; <br/>
              5. Considérant que M. D...et M. A...ont formé un recours pour excès de pouvoir tendant à l'annulation du décret n°2016-732 du 2 juin 2016 portant ouverture et annulation de crédits à titre d'avance, pris sur le fondement des dispositions précédemment citées de l'article 13 de la loi organique du 1er août 2001 ; qu'ils ont demandé en outre, par une requête distincte formée le 13 juillet 2016, au juge des référés du Conseil d'Etat d'ordonner la suspension de l'exécution de ce décret sur le fondement de l'article L. 521-1 du code de justice administrative ; <br/>
              6. Considérant qu'un décret d'avance, pris sur le fondement du premier alinéa de l'article 13 de la loi organique du 1er août 2001, vise à ouvrir des crédits supplémentaires par rapport à ceux adoptés par la loi de finances sans pouvoir affecter l'équilibre défini par cette loi, ce qui implique que le décret procède, à hauteur des crédits nouvellement ouverts, à l'annulation d'autres crédits ou constate des recettes supplémentaires ; qu'un tel décret a pour seul objet, conformément à ce que prévoit le IV de l'article 7 de la loi organique, de modifier, dans les limites et conditions prévues par l'article 13 de la loi organique, le montant des crédits ouverts par la loi de finances afin de permettre qu'ils soient mis, avec leur montant modifié, à disposition des ministres ainsi autorisés à en user ; <br/>
              7. Considérant que le décret contesté, signé le 2 juin 2016, a été publié au Journal officiel le 4 juin 2016 ; qu'il ressort des pièces du dossier et des indications données au cours de l'audience publique que les ouvertures et annulations de crédit décidées par ce décret se sont traduites par des mouvements comptables qui sont intervenus les 7 et 8 juin 2016, lesquels ont modifié le montant des crédits qui ont ainsi été effectivement mis à disposition des ministres concernés ; que, dans ces conditions, et alors même que les crédits en cause n'auraient pas été utilisés par les ministres autorisés à les dépenser, le décret contesté avait reçu complète exécution le 8 juin 2016, avant que ne soient formées devant le juge des référés les conclusions tendant à la suspension de son exécution ; que, par suite, ces conclusions de référé sont dépourvues d'objet et ne peuvent qu'être rejetées ; que doivent être rejetées, en conséquence, les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. D...et M. A...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B...D..., à M. C...A..., au ministre des finances et des comptes publics et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
