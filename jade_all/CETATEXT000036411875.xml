<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036411875</ID>
<ANCIEN_ID>JG_L_2017_12_000000406112</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/41/18/CETATEXT000036411875.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 28/12/2017, 406112, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>406112</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>Mme Cécile Barrois de Sarigny</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:406112.20171228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. F...C...a demandé au tribunal administratif de Cergy-Pontoise d'annuler pour excès de pouvoir les arrêtés des 16 novembre et 18 décembre 2015 par lesquels le ministre de l'intérieur l'a assigné à résidence sur le territoire de la commune de Champagne-sur-Oise. Par deux jugements nos 1510947 et 1511520 du 18 février 2016, le tribunal administratif de Cergy-Pontoise a fait droit à ses demandes.<br/>
<br/>
              Par un arrêt n° 16VE01130 du 21 juin 2016, la cour administrative d'appel de Versailles a, sur appel du ministre de l'intérieur, annulé ces jugements et rejeté les requêtes de M.C....<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 19 décembre 2016, 20 mars et 30 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, M. C...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du ministre de l'intérieur ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 55-385 du 3 avril 1955 ;<br/>
              - la loi n° 79-587 du 11 juillet 1979 ; <br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - la loi n° 2015-1501 du 20 novembre 2015 ;<br/>
              - le décret n° 2005-850 du 27 juillet 2005 ;<br/>
              - le décret n° 2015-1475 du 14 novembre 2015 ;<br/>
              - le décret n° 2015-1476 du 14 novembre 2015 ;<br/>
              - le décret n° 2015-1478 du 14 novembre 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Barrois de Sarigny, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de M. C...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que M. F...C...a demandé l'annulation de deux arrêtés des 16 novembre et 18 décembre 2015 par lesquels le ministre de l'intérieur l'a astreint à résider sur le territoire de la commune de Champagne-sur-Oise, avec obligation de présentation plusieurs fois par jour à des horaires déterminés à la brigade de gendarmerie, tous les jours de la semaine, ainsi que de demeurer, tous les jours entre 20 heures et 6 heures, dans les locaux où il réside ; que par deux jugements en date du 18 février 2016, le tribunal administratif de Cergy-Pontoise a annulé ces arrêtés ; que M. C...se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Versailles en date du 21 juin 2016 annulant les jugements du tribunal administratif de Cergy-Pontoise du 18 février 2016 et rejetant les demandes qu'il a présentées devant ce tribunal tendant à l'annulation des deux arrêtés d'assignation à résidence pris à son encontre ; <br/>
<br/>
              2.	Considérant qu'aux termes de l'article R. 611-17 du code de justice administrative : " Le rapporteur règle, sous l'autorité du président de la chambre, la communication de la requête. Il fixe, eu égard aux circonstances de l'affaire, le délai accordé aux parties pour produire leurs mémoires. Il peut demander aux parties, pour être joints à la procédure contradictoire, toutes pièces ou tous documents utiles à la solution du litige " ; que les mémoires et documents envoyés au juge en réponse aux demandes adressées aux parties en application de cette disposition doivent être soumis au débat contradictoire entre les parties, le cas échéant après que l'instruction a été rouverte, sauf dans le cas où ces mémoires et documents sont sans incidence sur le jugement de l'affaire ; <br/>
<br/>
              3.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le ministre de l'intérieur a produit devant la cour administrative d'appel de Versailles le 3 juin 2016, en réponse à la mesure d'instruction qui lui avait été adressée le 19 mai 2016, un nouveau mémoire accompagné d'un tableau recensant l'ensemble des armes et munitions découvertes lors de la perquisition dont avait été l'objet M. C...le 16 novembre 2015 et précisant leur régime légal ; qu'il résulte des énonciations de l'arrêt attaqué que la cour s'est notamment fondée, pour prendre sa décision, sur la circonstance que des armes et munitions, dont la détention était soumise à déclaration ou autorisation, avaient été découvertes au domicile de M. C...lors de la perquisition du 16 novembre 2015 ; qu'ainsi, et alors même que la cour disposait du procès-verbal de la perquisition menée chez M.C..., communiqué en pièce jointe de la requête d'appel du ministre de l'intérieur, soumise au débat contradictoire, la réponse à la mesure d'instruction ordonnée par la cour ne peut être regardée comme ayant été sans incidence sur le jugement de l'affaire ; que par suite, en ne communiquant pas à M.C..., après avoir rouvert l'instruction, les documents qui lui ont été adressés par le ministre de l'intérieur le 3 juin 2016, la cour administrative d'appel de Versailles a méconnu le caractère contradictoire de la procédure ; que son arrêt doit dès lors, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, être annulé ; <br/>
<br/>
              4.	Considérant qu'il y a lieu, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au fond ;<br/>
<br/>
              5.	Considérant qu'aux termes de l'article 6 de la loi du 3 avril 1955, dans sa version en vigueur à la date de l'arrêté du 16 novembre 2015 ordonnant l'assignation à résidence de M. C...: " Le ministre de l'intérieur dans tous les cas peut prononcer l'assignation à résidence dans une circonscription territoriale ou une localité déterminée de toute personne résidant dans la zone fixée par le décret visé à l'article 2 dont l'activité s'avère dangereuse pour la sécurité et l'ordre publics des circonscriptions territoriales visées audit article (...) " ; que le même article dispose, dans sa version en vigueur à la date du second arrêté d'assignation à résidence de M.C..., en date du 18 décembre 2015, que : " Le ministre de l'intérieur peut prononcer l'assignation à résidence, dans le lieu qu'il fixe, de toute personne résidant dans la zone fixée par le décret mentionné à l'article 2 et à l'égard de laquelle il existe des raisons sérieuses de penser que son comportement constitue une menace pour la sécurité et l'ordre publics dans les circonscriptions territoriales mentionnées au même article 2 (...) " ; <br/>
<br/>
              6.	Considérant qu'il ressort des termes des jugements attaqués du 18 février 2016 que le tribunal administratif de Cergy-Pontoise a annulé les arrêtés des 16 novembre et 18 décembre 2015 par lesquels le ministre de l'intérieur a assigné M. C...à résidence au motif que celui-ci avait commis une erreur d'appréciation du danger et de la menace pour la sécurité et l'ordre publics que présentait l'intéressé ; qu'il ressort cependant des pièces du dossier que M. C...a fait l'objet, le 26 janvier 2015, d'un arrêté du préfet du Val-d'Oise lui ordonnant de procéder à la remise de l'ensemble des armes et munitions qu'il détenait et lui interdisant d'acquérir ou de détenir des armes ou munitions au motif qu'il présentait un danger grave pour lui-même ou pour autrui ; que, lors d'une perquisition ordonnée au domicile de l'intéressé, le 16 novembre 2015, conduite dans le cadre de l'état d'urgence déclaré le 14 novembre 2015, le lendemain des attentats commis à Paris, des armes et une quantité importante de munitions, dont une partie étaient soumises à autorisation, ont été découvertes au domicile de M. C...en dépit de l'interdiction posée par l'arrêté du 26 janvier 2015 ; qu'une note blanche établie par les services de renseignement indique que M. C...attirait régulièrement l'attention, notamment au sein du club de tir qu'il fréquentait à Roissy, en raison d'un discours prosélyte et radical sur l'islam ; que si l'intéressé produit plusieurs attestations faisant état de ses bonnes relations avec son entourage ainsi qu'avec les personnes qu'il a pu rencontrer dans le cadre de ses activités associatives, menées à la fin des années 1990 et au début des années 2000, et conteste adopter un comportement tel que celui décrit dans la note des services de renseignement, il ne remet pas en cause les résultats de la perquisition menée chez lui le 16 novembre 2015 ; que dans ces conditions, eu égard à la situation de fait prévalant à la date à laquelle les décisions portant assignation à résidence de M. C...ont été prises et compte tenu des informations dont disposait alors l'administration, c'est à tort que le tribunal administratif de Cergy-Pontoise s'est fondé sur le motif tiré de l'erreur d'appréciation du ministre de l'intérieur pour annuler les arrêtés des 16 novembre et 18 décembre 2015 ; <br/>
<br/>
              7.	Considérant, toutefois, qu'il appartient au Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner les autres moyens soulevés devant le tribunal administratif par M. C...;<br/>
<br/>
              8.	Considérant, en premier lieu, qu'aux termes de l'article 1er du décret du 27 juillet 2005 relatif aux délégations de signature des membres du gouvernement : " (...) peuvent signer, au nom du ministre ou du secrétaire d'Etat et par délégation, l'ensemble des actes, à l'exception des décrets, relatifs aux affaires des services placés sous leur autorité : / 1° Les secrétaires généraux des ministères, les directeurs d'administration centrale (...) " ; que l'article 3 du même texte précise que les personnes mentionnées aux 1° et 3° de l'article 1er peuvent donner délégation pour signer tous actes relatifs aux affaires pour lesquelles elles ont elles-mêmes reçu délégation notamment aux magistrats, aux fonctionnaires de catégorie A et aux agents contractuels chargés de fonctions d'un niveau équivalent, qui n'en disposent pas au titre de l'article 1er ; que par décision du 14 mai 2014, publiée au Journal officiel de la République française du 16 mai suivant, délégation a été donnée en cas d'empêchement de M. A...B..., directeur des libertés publiques et des affaires juridiques, à Mme E...D..., première conseillère du corps des tribunaux administratifs et des cours administratives d'appel, sous-directrice du conseil juridique et du contentieux, à l'effet de signer, au nom du ministre de l'intérieur, tout acte ou document relevant des attributions de la direction des libertés publiques et des affaires juridiques ; que, par suite, Mme D...était compétente pour signer les arrêtés contestés ; <br/>
<br/>
              9.	Considérant, en deuxième lieu, que les arrêtés litigieux comportent l'énoncé des considérations de fait et de droit qui en constituent le fondement ; qu'ils satisfont ainsi à l'exigence de motivation prévue par l'article 3 de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public, désormais codifié à l'article L. 211-5 du code des relations entre le public et l'administration ;<br/>
<br/>
              10.	Considérant, en troisième lieu, que la circonstance que les arrêtés du 16 novembre et 18 décembre 2015 ne comportent pas de numéro d'identification ni de date de notification est sans incidence sur leur légalité ; <br/>
<br/>
              11.	Considérant, en quatrième lieu, que l'article 2 de la loi du 3 avril 1955 prévoit que la prorogation de l'état d'urgence déclaré par décret en conseil des ministres au-delà de douze jours ne peut être autorisée que par la loi ; que la loi autorisant cette prorogation fixe, en vertu de l'article 3 de la loi de 1955, sa durée définitive ; que l'article 14 de la loi du 3 avril 1955 relative à l'état d'urgence dispose que " les mesures prises en application de la présente loi cessent d'avoir effet en même temps que prend fin l'état d'urgence " ; que dans sa décision n° 2015-527 QPC du 22 décembre 2015, le Conseil constitutionnel a précisé que " si le législateur prolonge l'état d'urgence par une nouvelle loi, les mesures d'assignation à résidence prises antérieurement ne peuvent être prolongées sans être renouvelées " ;  <br/>
<br/>
              12.	Considérant que la circonstance qu'un arrêté d'assignation à résidence pris sur le fondement de l'article 6 de la loi du 3 avril 1955 ne précise pas la durée de cette mesure est sans incidence sur sa légalité ; qu'en outre, il résulte de ce qui précède qu'à défaut de précision contraire sur l'arrêté pris par le ministre de l'intérieur, une mesure d'assignation à résidence prise avant l'intervention de la première loi de prorogation faisant suite à la déclaration d'état d'urgence par décret en conseil des ministres est prise pour une durée de douze jours, laquelle peut, le cas échéant, être prolongée par la loi de prorogation intervenant avant l'expiration de ce délai, qui fixe la durée définitive au-delà de laquelle la mesure ne pourra être prolongée sans être renouvelée ; qu'en l'espèce, compte tenu de l'intervention de la loi du 20 novembre 2015 prorogeant l'application de la loi n° 55-385 du 3 avril 1955 relative à l'état d'urgence et renforçant l'efficacité de ses dispositions, qui a prorogé l'état d'urgence déclaré par le décret n° 2015-1475 du 14 novembre 2015 et le décret n° 2015-1493 du 18 novembre 2015 pour une durée de trois mois à compter du 26 novembre 2015, l'arrêté du 16 novembre 2015 et celui du 18 décembre 2015 ont été pris, en l'absence de mention contraire, pour une durée allant jusqu'au 26 février 2016 ; que le moyen tiré de l'absence de mention de la durée des arrêtés litigieux ne peut dès lors, et en tout état de cause, qu'être écarté ; <br/>
<br/>
              13.	Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin d'examiner les moyens tirés de l'irrégularité des jugements attaqués, que le ministre de l'intérieur est fondé à soutenir que c'est à tort que, par les jugements du 18 février 2016, le tribunal administratif de Cergy-Pontoise a annulé les arrêtés des 16 novembre et 18 décembre 2015 ; <br/>
<br/>
              14.	Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante, les sommes demandées par M. C...au titre de ces dispositions ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 21 juin 2016 et les jugements du tribunal administratif de Cergy-Pontoise du 18 février 2016 sont annulés.<br/>
<br/>
Article 2 : Les demandes présentées par M. C...devant le tribunal administratif de Cergy-Pontoise sont rejetées. <br/>
<br/>
Article 3 : Les conclusions présentées par M. C...au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 4 : La présente décision sera notifiée à M. F...C...et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
