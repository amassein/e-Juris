<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043648158</ID>
<ANCIEN_ID>JG_L_2021_06_000000438047</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/64/81/CETATEXT000043648158.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 09/06/2021, 438047</TITRE>
<DATE_DEC>2021-06-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438047</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER ; SCP BORE, SALVE DE BRUNETON, MEGRET ; SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH</AVOCATS>
<RAPPORTEUR>M. David Guillarme</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:438047.20210609</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Bordeaux de prononcer la résiliation du marché d'assistance à maîtrise d'ouvrage et d'accompagnement juridique conclu entre la commune de Sainte-Eulalie et la société Maliegui pour la construction et la gestion d'un crématorium. Par un jugement n° 1503066 du 6 juin 2017, le tribunal administratif de Bordeaux a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17BX02648 du 28 novembre 2019, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par M. A... contre ce jugement. <br/>
<br/>
              Procédures devant le Conseil d'Etat<br/>
<br/>
              1° Sous le n° 438047, par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire enregistrés les 28 janvier et 24 août 2020 et 19 mai 2021 au secrétariat du contentieux du Conseil d'Etat, le Conseil national des barreaux demande à titre principal au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à l'appel de M. A... ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Sainte-Eulalie et de la société Maliegui la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              2° Sous le n° 438054, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 28 janvier et 26 mai 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Sainte-Eulalie et de la société Maliegui la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 71-1130 du 31 décembre 1971 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. David Guillarme, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat du Conseil national des barreaux, à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de la commune de Sainte-Eulalie et à la SCP Célice, Texidor, Perier, avocat de M. A... ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 2 juin 2021, présentée par le Conseil national des barreaux ; <br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Les pourvois du Conseil national des barreaux et de M. A... sont dirigés contre le même arrêt. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
<br/>
              Sur le pourvoi de M. A... :<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que par un marché conclu le 17 avril 2015, la commune de Sainte-Eulalie a confié à la société Maliegui une mission d'assistance à maîtrise d'ouvrage et d'accompagnement juridique pour la construction et la gestion d'un crématorium. Par un jugement du 6 juin 2017, le tribunal administratif de Bordeaux a rejeté la demande de résiliation de ce contrat formée par M. A..., avocat associé de l'association d'avocats à responsabilité professionnelle individuelle Rivière Morlon et Associés, candidat évincé à l'attribution du marché. Par l'arrêt attaqué du 28 novembre 2019, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par M. A....<br/>
<br/>
              3. Indépendamment des actions dont disposent les parties à un contrat administratif et des actions ouvertes devant le juge de l'excès de pouvoir contre les clauses réglementaires d'un contrat ou devant le juge du référé contractuel sur le fondement des articles L. 551-13 et suivants du code de justice administrative, tout tiers à un contrat administratif susceptible d'être lésé dans ses intérêts de façon suffisamment directe et certaine par sa passation ou ses clauses est recevable à former devant le juge du contrat un recours de pleine juridiction contestant la validité du contrat ou de certaines de ses clauses non réglementaires qui en sont divisibles. <br/>
<br/>
              4. Saisi d'un recours de pleine juridiction contestant la validité du contrat ou de certaines de ses clauses par un tiers justifiant que la passation de ce contrat l'a lésé dans ses intérêts de façon suffisamment directe et certaine, il appartient au juge du contrat, en présence d'irrégularités qui ne peuvent être couvertes par une mesure de régularisation et qui ne permettent pas la poursuite de l'exécution du contrat, de prononcer, le cas échéant avec un effet différé, après avoir vérifié que sa décision ne portera pas une atteinte excessive à l'intérêt général, soit la résiliation du contrat, soit, si le contrat a un contenu illicite ou s'il se trouve affecté d'un vice de consentement ou de tout autre vice d'une particulière gravité que le juge doit ainsi relever d'office, l'annulation totale ou partielle de celui-ci.<br/>
<br/>
              5. Il résulte de ce qui précède que le juge du contrat saisi par un tiers de conclusions en contestation de la validité du contrat ou de certaines de ses clauses dispose de l'ensemble des pouvoirs mentionnés au point précédent et qu'il lui appartient d'en faire usage pour déterminer les conséquences des irrégularités du contrat qu'il a relevées, alors même que le requérant n'a expressément demandé que la résiliation du contrat. Par suite, en considérant que les écritures par lesquelles M. A... faisait valoir devant elle que les vices entachant le contrat étaient de nature à entraîner son annulation constituaient des conclusions nouvelles en appel et par suite irrecevables au motif qu'il n'avait demandé au tribunal administratif que la résiliation du contrat, alors que les conclusions de M. A... devaient être regardées dès l'introduction de la requête devant le tribunal comme contestant la validité du contrat et permettant au juge, en première instance comme en appel, si les conditions en étaient remplies, de prononcer, le cas échéant d'office, l'annulation du contrat, la cour administrative d'appel de Bordeaux a commis une erreur de droit.<br/>
<br/>
              6. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que M. A... est fondé à demander l'annulation de l'arrêt qu'il attaque en tant qu'il a statué sur ses conclusions en contestation de la validité du contrat.<br/>
<br/>
              Sur le pourvoi du Conseil national des barreaux :<br/>
<br/>
              7. En raison de l'annulation prononcée par la présente décision, le pourvoi du Conseil national des barreaux a perdu son objet en tant qu'il conclut à l'annulation de l'arrêt attaqué. Il appartient dès lors au Conseil d'Etat de constater qu'il n'y a pas lieu de statuer sur ce pourvoi dans cette mesure.<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative:<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Sainte-Eulalie et de la société Maliegui le versement à M. A... d'une somme de 1 500 euros chacun au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions font en revanche obstacle à ce qu'une somme soit mise à ce titre à la charge de M. A... qui n'est pas la partie perdante dans la présente instance. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Sainte-Eulalie et de la société Maliegui le versement d'une somme au Conseil national des barreaux au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 2 à 5 de l'arrêt de la cour administrative d'appel de Bordeaux du 28 novembre 2019 sont annulés.<br/>
Article 2 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Bordeaux.<br/>
Article 3 : Il n'y a pas lieu de statuer sur le pourvoi du Conseil national des barreaux en tant qu'il conclut à l'annulation de l'arrêt de la cour administrative d'appel de Bordeaux. Les conclusions de ce pourvoi au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La commune de Sainte-Eulalie et la société Maliegui verseront chacune à M. A... une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 5 : La présente décision sera notifiée à M. B... A..., au Conseil national des barreaux, à la commune de Sainte-Eulalie et à la société Maliegui.<br/>
Copie en sera adressée au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-08-03-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS ET OBLIGATIONS DU JUGE. POUVOIRS DU JUGE DU CONTRAT. - RECOURS TARN-ET-GARONNE [RJ1] - POUVOIRS ET DEVOIRS DU JUGE - 1) POUVOIR DE RELEVER D'OFFICE UN VICE D'UNE PARTICULIÈRE GRAVITÉ - EXISTENCE [RJ1] - 2) CONSÉQUENCE - POUVOIR DU JUGE D'ANNULER UN CONTRAT ALORS QU'IL N'EST SAISI QUE D'UNE DEMANDE DE RÉSILIATION - EXISTENCE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-03-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. CONCLUSIONS. ULTRA PETITA. - ABSENCE - RECOURS TARN-ET-GARONNE [RJ1] - JUGE ANNULANT UN CONTRAT ALORS QU'IL N'EST SAISI QUE D'UNE DEMANDE DE RÉSILIATION [RJ2].
</SCT>
<ANA ID="9A"> 39-08-03-02 Saisi d'un recours de pleine juridiction contestant la validité du contrat ou de certaines de ses clauses par un tiers justifiant que la passation de ce contrat l'a lésé dans ses intérêts de façon suffisamment directe et certaine, il appartient au juge du contrat, en présence d'irrégularités qui ne peuvent être couvertes par une mesure de régularisation et qui ne permettent pas la poursuite de l'exécution du contrat, de prononcer, le cas échéant avec un effet différé, après avoir vérifié que sa décision ne portera pas une atteinte excessive à l'intérêt général, soit la résiliation du contrat, soit, si le contrat a un contenu illicite ou s'il se trouve affecté d'un vice de consentement ou de tout autre vice d'une particulière gravité que le juge doit ainsi relever d'office, l'annulation totale ou partielle de celui-ci.,,,Il résulte de ce qui précède que le juge du contrat saisi par un tiers de conclusions en contestation de la validité du contrat ou de certaines de ses clauses dispose de l'ensemble des pouvoirs mentionnés au point précédent et qu'il lui appartient d'en faire usage pour déterminer les conséquences des irrégularités du contrat qu'il a relevées, alors même que le requérant n'a expressément demandé que la résiliation du contrat.</ANA>
<ANA ID="9B"> 54-07-01-03-03 Saisi d'un recours de pleine juridiction contestant la validité du contrat ou de certaines de ses clauses par un tiers justifiant que la passation de ce contrat l'a lésé dans ses intérêts de façon suffisamment directe et certaine, il appartient au juge du contrat, en présence d'irrégularités qui ne peuvent être couvertes par une mesure de régularisation et qui ne permettent pas la poursuite de l'exécution du contrat, de prononcer, le cas échéant avec un effet différé, après avoir vérifié que sa décision ne portera pas une atteinte excessive à l'intérêt général, soit la résiliation du contrat, soit, si le contrat a un contenu illicite ou s'il se trouve affecté d'un vice de consentement ou de tout autre vice d'une particulière gravité que le juge doit ainsi relever d'office, l'annulation totale ou partielle de celui-ci.,,,Il résulte de ce qui précède que le juge du contrat saisi par un tiers de conclusions en contestation de la validité du contrat ou de certaines de ses clauses dispose de l'ensemble des pouvoirs mentionnés au point précédent et qu'il lui appartient d'en faire usage pour déterminer les conséquences des irrégularités du contrat qu'il a relevées, alors même que le requérant n'a expressément demandé que la résiliation du contrat.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 4 avril 2014, Département de Tarn-et-Garonne, n° 358994, p. 70.,,[RJ2] Rappr., s'agissant des pouvoirs du juge du référé précontractuel, CE, 20 octobre 2006, Commune d'Andeville, n° 289234, T. p. 434 ; CE, 15 décembre 2006, Société Corsica Ferries, n° 298618, p. 566.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
