<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035553011</ID>
<ANCIEN_ID>JG_L_2017_09_000000398160</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/55/30/CETATEXT000035553011.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 13/09/2017, 398160, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-09-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398160</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>MAGIERA</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Pierre-François Mourier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:398160.20170913</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et deux mémoires complémentaires, enregistrés les 22 mars, 22 juin et 15 septembre 2016 au secrétariat du contentieux du Conseil d'Etat, Mme A...B...demande au Conseil d'Etat :<br/>
<br/>
              1°) de condamner l'Etat à lui verser une indemnité de 27 500 euros en réparation des préjudices subis du fait de la durée excessive de la procédure juridictionnelle d'exécution du jugement n° 0903077 du 18 février 2011 du tribunal administratif de Toulon, augmentée des intérêts légaux et de leur capitalisation ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre-François Mourier, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il résulte de l'instruction que, par un jugement du 18 février 2011 devenu définitif, le tribunal administratif de Toulon a annulé pour excès de pouvoir la décision du 12 octobre 2009 de l'inspecteur d'académie, directeur des services départementaux de l'éducation nationale du Var, rejetant la demande de Mme B...tendant à ce que sa maladie soit reconnue comme professionnelle et a enjoint à ce dernier de se prononcer à nouveau sur sa demande ; que, saisi le 1er juillet 2011 par Mme B...sur le fondement de l'article L. 911-4 du code de justice administrative, le tribunal administratif de Toulon a, par un jugement du 29 mars 2013, rejeté sa demande d'exécution au motif que son précédent jugement avait été entièrement exécuté ; que, par une décision du 15 octobre 2014, le Conseil d'Etat statuant au contentieux a, sur le pourvoi de MmeB..., annulé ce jugement du 29 mars 2013 et enjoint au ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche de prendre, dans un délai d'un mois, une décision reconnaissant l'imputabilité au service de la maladie de Mme B...; que Mme B...demande au Conseil d'Etat d'annuler la décision du 22 janvier 2016 par laquelle le garde des sceaux, ministre de la justice a rejeté sa demande tendant au versement d'une somme d'un montant de 27 500 euros en réparation des préjudices qu'elle estime avoir subis du fait de la durée excessive de cette procédure juridictionnelle d'exécution ;<br/>
<br/>
              2. Considérant que, si la responsabilité de l'Etat est susceptible d'être engagée en raison du fonctionnement défectueux du service public de la justice, un délai excessif dans l'exécution d'une décision juridictionnelle engage, en principe, la responsabilité de la personne à qui incombait cette exécution ; que lorsque la carence de cette personne donne lieu à une procédure juridictionnelle d'exécution, celle-ci doit être jugée dans un délai raisonnable, une durée de jugement excessive étant susceptible d'engager la responsabilité de l'Etat en raison du fonctionnement défectueux du service public de la justice ; <br/>
<br/>
              3. Considérant qu'il résulte de l'instruction que la durée globale de la procédure juridictionnelle engagée par Mme B...pour obtenir l'exécution du jugement du 18 février 2011 du tribunal administratif de Toulon a été de plus de trois ans et trois mois ; que l'affaire ne présentait pas de difficulté spécifique et nécessitait, eu égard à sa nature, une diligence particulière ; qu'il ne résulte pas de l'instruction que Mme B...aurait concouru à l'allongement de cette procédure ; que, par suite, Mme B...est fondée à soutenir que son droit à un délai raisonnable de jugement a été méconnu et à demander, pour ce motif, la réparation des préjudices qu'elle a subis de ce fait ;<br/>
<br/>
              4. Considérant que Mme B...n'est pas fondée à demander la réparation du préjudice matériel lié aux frais de procédure, qui est sans lien direct avec la faute commise par le service public de la justice du fait de la durée excessive de la procédure juridictionnelle d'exécution ; qu'il sera fait, dans les circonstances de l'espèce, une juste appréciation de ses préjudices matériels et moraux et des troubles dans ses conditions d'existence en lui allouant la somme de 3 000 euros, tous intérêts compris ;<br/>
<br/>
              5. Considérant, enfin, qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros que demande Mme B...au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'Etat est condamné à verser à Mme B...la somme de 3 000 euros, tous intérêts compris à la date de la présente décision.<br/>
Article 2 : L'Etat versera la somme de 3 000 euros à Mme B...au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 4 : La présente décision sera notifiée à Madame A...B...et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
