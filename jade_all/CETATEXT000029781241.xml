<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029781241</ID>
<ANCIEN_ID>JG_L_2014_11_000000379900</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/78/12/CETATEXT000029781241.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  8ème sous-section jugeant seule, 19/11/2014, 379900, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-11-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>379900</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 8ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Patrick Quinqueton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:379900.20141119</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 6 mai 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par la commune de Châteauneuf-sur-Cher, représentée par son maire ; la commune demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret n° 2014-206 du 21 février 2014 portant délimitation des cantons dans le département du Cher ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code électoral ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu la loi n° 90-1103 du 11 décembre 1990 ;<br/>
<br/>
              Vu la loi n° 2013-403 du 17 mai 2013 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Patrick Quinqueton, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que l'article L. 3113-2 du code général des collectivités territoriales, dans sa version issue de la loi du 17 mai 2013, prévoit que : " I. - Les modifications des limites territoriales des cantons, les créations et suppressions de cantons et le transfert du siège de leur chef-lieu sont décidés par décret en Conseil d'Etat après consultation du conseil départemental qui se prononce dans un délai de six semaines à compter de sa saisine. A l'expiration de ce délai, son avis est réputé rendu. (...) III. - La modification des limites territoriales des cantons effectuée en application du I est conforme aux règles suivantes : a) Le territoire de chaque canton est défini sur des bases essentiellement démographiques ; / b) Le territoire de chaque canton est continu ; / c) Est entièrement comprise dans le même canton toute commune de moins de 3 500 habitants ; / IV. - Il n'est apporté aux règles énoncées au III que des exceptions de portée limitée, spécialement justifiées, au cas par cas, par des considérations géographiques ; ou par d'autres impératifs d'intérêt général " ;<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              2. Considérant, en premier lieu, qu'il résulte des termes mêmes des dispositions législatives précitées qu'il appartenait au Premier ministre de procéder, par décret en Conseil d'Etat, aux modifications des limites territoriales des cantons ainsi qu'à leur création ou à leur suppression ;  <br/>
<br/>
              3. Considérant, en deuxième lieu, qu'il est constant que le décret attaqué a été pris sur avis du conseil général du Cher, émis le 24 janvier 2014 après examen du projet présenté par la préfète du Cher ; qu'il ressort des pièces du dossier que l'envoi du préfet au président du conseil général comprenait le projet de décret, un rapport de présentation générale rappelant les principes retenus, la carte des nouveaux cantons, la carte des nouveaux cantons superposée à celle des anciens cantons, la carte des nouveaux cantons mentionnant les limites des établissements publics de coopération intercommunale, la carte des cantons urbains pour les communes fractionnées, un tableau des populations par canton et un tableau de répartition des communes par canton ; que ces éléments permettaient à l'assemblée départementale d'émettre un avis sur les modalités de mise en oeuvre de la nouvelle délimitation des cantons prévue par le législateur et de faire des propositions spécifiques pour le département du Cher ; que, par suite, la commune n'est pas fondée à soutenir que la consultation du conseil général aurait été irrégulière ; <br/>
<br/>
              4. Considérant, en troisième lieu, que les dispositions de l'article L. 3113-2 du code général des collectivités territoriales se bornent à prévoir la consultation du conseil général du département concerné à l'occasion de la création et de la suppression de cantons ; que ni le principe constitutionnel de libre administration des collectivités territoriales, ni l'article L. 1111-1 du code général des collectivités territoriales ni aucune autre principe n'imposait au Gouvernement de consulter l'ensemble des établissements publics de coopération intercommunale, des communes, des élus locaux, des habitants ou des électeurs du département et de leur présenter le projet de décret préalablement à son édiction ; que le requérant ne peut, à cet égard, se prévaloir des termes de la circulaire du ministre de l'intérieur du 12 avril 2013 relative à la méthodologie du redécoupage cantonal en vue de la mise en oeuvre du scrutin majoritaire aux élections départementales, laquelle est dépourvue de caractère réglementaire ;<br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              5. Considérant, en quatrième lieu, que, compte tenu des modalités de consultation qui ont été prévues par le législateur, la commune ne saurait utilement faire valoir que le  Gouvernement aurait commis une erreur manifeste d'appréciation et méconnu l'article L. 1111-1 du code général des collectivités territoriales en prenant le décret attaqué sans préalablement consulter les communes du département ; <br/>
<br/>
              6. Considérant, en cinquième lieu, que l'article 1er du décret attaqué procède à une nouvelle délimitation de l'ensemble des cantons du département du Cher, compte tenu de l'exigence de réduction du nombre des cantons de ce département de trente-cinq à dix-neuf résultant de l'application de l'article L. 191-1 du code électoral, en se fondant sur une population moyenne et en rapprochant la population de chaque canton de cette moyenne ; que le rapport entre la population du canton le moins peuplé et celle du canton le plus peuplé qui était, avant la nouvelle délimitation cantonale, de 1 à 7,5, a été réduit de 1 à 1,43 ; <br/>
<br/>
              7. Considérant qu'aucune disposition n'impose au Premier ministre de prévoir que les limites des cantons, qui sont des circonscriptions électorales, coïncident avec les limites des circonscriptions législatives ou des anciens cantons et avec les périmètres des établissements publics de coopération intercommunale figurant dans le schéma départemental de coopération intercommunale ou des " bassins de vie " définis par l'Institut national de la statistique et des études économiques ; que la proximité géographique de l'ensemble des communes composant un canton ou l'absence de disparité de superficie entre cantons ne sont pas davantage des critères de délimitation des circonscriptions électorales définis à l'article L. 3113-2 du code général des collectivités territoriales ; que, par suite, la commune ne saurait utilement soutenir que la nouvelle délimitation cantonale a méconnu ces différentes données ; <br/>
<br/>
              8. Considérant, en sixième lieu, que si la commune soutient que la nouvelle délimitation cantonale ne repose sur aucun critère objectif  ainsi que le démontrerait le rattachement des communes de l'ancien canton rural de Châteauneuf-sur-Cher au nouveau canton de Trouy (n° 17), tourné vers la ville de Bourges, il ressort toutefois des pièces du dossier que ce canton, dont il n'est pas contesté que la délimitation satisfait aux prescriptions du III de l'article L. 3113-2 du code général des collectivités territoriales, réunit les anciens cantons contigus de Châteauneuf-sur-Cher et de Levet ; que le principe de réduction par deux du nombre de cantons inhérent à la présente délimitation implique la constitution de cantons de superficie plus étendue et la réunion de communes auparavant rattachées à des cantons distincts ; que la circonstance que d'autres délimitations et rattachements auraient été possibles est sans influence sur la légalité du décret attaqué ; qu'est également inopérante la circonstance que la commune de Châteauneuf-sur-Cher, où se trouvent les services publics de proximité auxquels recourent les habitants de la commune de Saint-Loup-des-Chaumes, serait susceptible de perdre sa qualité de chef-lieu de canton, alors que le décret attaqué, en désignant les bureaux centralisateurs des nouveaux cantons, n'a ni pour objet ni pour effet de procéder au transfert du siège des chefs-lieux de canton ; que, dans ces conditions, la commune ne peut être regardée comme apportant des éléments de nature à établir que la nouvelle délimitation des cantons serait arbitraire ou reposerait sur une erreur manifeste d'appréciation ; <br/>
<br/>
              9. Considérant que le détournement de pouvoir allégué n'est pas établi ;<br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par le ministre, que la requête de la commune de Châteauneuf-sur-Cher doit être rejetée ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article  1er : La requête de la commune de Châteauneuf-sur-Cher est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la commune de Châteauneuf-sur-Cher et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
