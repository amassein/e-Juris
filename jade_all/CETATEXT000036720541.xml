<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036720541</ID>
<ANCIEN_ID>JG_L_2018_03_000000401896</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/72/05/CETATEXT000036720541.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 19/03/2018, 401896, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-03-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401896</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Matias de Sainte Lorette</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Yohann Bénard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:401896.20180319</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme B...A...ont demandé au tribunal administratif de Paris de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu auxquelles ils ont été assujettis au titre de l'année 2009, ainsi que des pénalités correspondantes. Par un jugement n° 1400442 du 24 avril 2015, le tribunal administratif de Paris a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 15PA01797 du 27 mai 2016, la cour administrative d'appel de Paris a rejeté l'appel formé par M. et Mme A... contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 27 juillet et 27 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, M. et Mme A... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matias de Sainte Lorette, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Yohann Bénard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de M. et MmeA....<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. et Mme A..., associés de la société en participation Cofina 01509, ont imputé sur le montant de leur impôt sur le revenu au titre de l'année 2009, sur le fondement des dispositions de l'article 199 undecies B du code général des impôts, une réduction d'impôt du fait d'investissements réalisés en Guadeloupe et en Guyane par l'intermédiaire de cette société. M. et Mme A...se pourvoient en cassation contre l'arrêt du 27 mai 2016 par lequel la cour administrative d'appel de Paris a rejeté leur appel contre un jugement du 24 avril 2015 du tribunal administratif de Paris rejetant leur demande tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu auxquelles ils ont été assujettis au titre de l'année 2009.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article 199 undecies B du code général des impôts, dans sa rédaction applicable au litige : " Les contribuables domiciliés en France au sens de l'article 4 B peuvent bénéficier d'une réduction d'impôt sur le revenu à raison des investissements productifs neufs qu'ils réalisent dans les départements d'outre-mer (...)dans le cadre d'une entreprise exerçant une activité agricole ou une activité industrielle, commerciale ou artisanale relevant de l'article 34 ". Aux termes du vingtième alinéa du même article : " La réduction d'impôt prévue au premier alinéa est pratiquée au titre de l'année au cours de laquelle l'investissement est réalisé. ". Aux termes de l'article 95 Q de l'annexe II à ce même code, dans sa rédaction alors applicable : " La réduction d'impôt prévue au I de l'article 199 undecies B du code général des impôts est pratiquée au titre de l'année au cours de laquelle l'immobilisation est créée par l'entreprise ou lui est livrée ou est mise à sa disposition dans le cadre d'un contrat de crédit-bail. ". Il résulte de la combinaison de ces dispositions que le fait générateur de la réduction d'impôt prévue à l'article 199 undecies B est la date de la création de l'immobilisation au titre de laquelle l'investissement productif a été réalisé ou de sa livraison effective dans le département d'outre-mer. Dans ce dernier cas, la date à retenir est celle à laquelle l'entreprise, disposant matériellement de l'investissement productif, peut commencer son exploitation effective et, dès lors, en retirer des revenus.<br/>
<br/>
              S'agissant de l'investissement réalisé en Guadeloupe :<br/>
<br/>
              3. Il ressort des énonciations de l'arrêt attaqué que la cour a relevé que M. et Mme A...ne produisaient aucun élément d'ordre financier ni aucun document susceptible d'établir que la SEP Cofina 01509 avait réellement acquis la structure ombrière à raison de laquelle ils entendaient bénéficier de la réduction d'impôt prévue par les dispositions de l'article 199 undecies B du code général des impôts. En statuant ainsi, par une appréciation souveraine des faits exempte de dénaturation, la cour, qui a suffisamment motivé son arrêt, n'a, contrairement à ce que soutiennent les requérants, pas érigé le paiement effectif de la structure en condition légale de la réduction d'impôt et n'a, dès lors, pas commis l'erreur de droit qui lui est reprochée.<br/>
<br/>
              S'agissant de l'investissement réalisé en Guyane : <br/>
<br/>
              4. Aux termes du vingt-sixième alinéa de l'article 199 undecies B du code général des impôts, dans sa rédaction résultant de l'article 21 de la loi du 27 mai 2009 pour le développement économique des outre-mer : " (...) L'octroi de la réduction d'impôt prévue au premier alinéa est subordonné au respect par les entreprises réalisant l'investissement et, le cas échéant, les entreprises exploitantes de leurs obligations fiscales et sociales et de l'obligation de dépôt de leurs comptes annuels selon les modalités prévues aux articles L. 232-21 à L. 232-23 du code de commerce à la date de réalisation de l'investissement. Sont considérés comme à jour de leurs obligations fiscales et sociales les employeurs qui, d'une part, ont souscrit et respectent un plan d'apurement des cotisations restant dues et, d'autre part, acquittent les cotisations en cours à leur date normale d'exigibilité. Pour l'application de la première phrase du présent alinéa en Nouvelle-Calédonie et en Polynésie française, les références aux dispositions du code de commerce sont remplacées par les dispositions prévues par la réglementation applicable localement. ". Ces dispositions sont applicables aux investissements réalisés à compter du 29 mai 2009, date d'entrée en vigueur de la loi. <br/>
<br/>
              5. Aux termes du I de l'article L. 232-22 du code de commerce : " Toute société à responsabilité limitée est tenue de déposer, en double exemplaire, au greffe du tribunal, pour être annexés au registre du commerce et des sociétés, dans le mois qui suit l'approbation des comptes annuels par l'assemblée ordinaire des associés ou par l'associé unique : / 1° Les comptes annuels, le rapport de gestion et, le cas échéant, les comptes consolidés, le rapport sur la gestion du groupe, les rapports des commissaires aux comptes sur les comptes annuels et les comptes consolidés, éventuellement complétés de leurs observations sur les modifications apportées par l'assemblée ou l'associé unique aux comptes annuels qui leur ont été soumis ; / 2° La proposition d'affectation du résultat soumise à l'assemblée ou à l'associé unique et la résolution d'affectation votée ou la décision d'affectation prise. ". <br/>
<br/>
              6. Après avoir jugé, par une appréciation souveraine exempte de dénaturation, qu'il n'était pas établi que la SARL Perreira Lapompe Paironne, société exploitante des investissements en litige, était à jour de ses obligations au titre de l'article L 232-22 du code de commerce à la date de réalisation supposée des investissements, la cour a pu, sans méconnaitre les dispositions citées aux points 4 et 5 ci-dessus, déduire que l'administration avait pu à bon droit refuser le bénéfice de la réduction d'impôt prévue à l'article 199 undecies B du code général des impôts à l'investissement réalisé en Guyane.<br/>
<br/>
              7. Il résulte de tout ce qui précède que M. et Mme A...ne sont pas fondés à demander l'annulation de l'arrêt qu'ils attaquent. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi et M. et Mme A...est rejeté. <br/>
Article 2 : La présente décision sera notifiée à M. et Mme B... A...et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
