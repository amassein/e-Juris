<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000022364671</ID>
<ANCIEN_ID>JG_L_2010_06_000000334454</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/22/36/46/CETATEXT000022364671.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 11/06/2010, 334454, Publié au recueil Lebon</TITRE>
<DATE_DEC>2010-06-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>334454</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Arrighi de Casanova</PRESIDENT>
<AVOCATS>BOUTHORS</AVOCATS>
<RAPPORTEUR>Mme Maud  Vialettes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Lenica Frédéric</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 7 décembre 2009 et 1er février 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Renek A, également connu sous l'identité de Wladyslaw B, élisant domicile au cabinet de Me Didier Bouthors, 5 rue Dante, à Paris (75005) ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 19 octobre 2009 accordant son extradition aux autorités polonaises ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution ;<br/>
<br/>
              Vu la convention de New-York relative au statut des apatrides du 28 septembre 1954 ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu la convention établie sur la base de l'article K.3 du traité sur l'Union européenne, relative à l'extradition entre les Etats membres de l'Union européenne, signée à Dublin le 27 septembre 1996 ;<br/>
<br/>
              Vu le code pénal ; <br/>
<br/>
              Vu le code de procédure pénale ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Maud Vialettes, Maître des Requêtes,  <br/>
<br/>
              - les observations de Me Bouthors, avocat de M. A, <br/>
<br/>
              - les conclusions de M. Frédéric Lenica, rapporteur public,<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Bouthors, avocat de M. A ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant, en premier lieu, qu'aucune disposition n'exigeait que le décret attaqué fût pris après consultation de l'Office français de protection des réfugiés et des apatrides ; que, par suite, le moyen tiré de l'absence de consultation de cet organisme ne saurait être accueilli ; <br/>
<br/>
              Considérant, en deuxième lieu, qu'aux termes de l'article 22 de la Constitution,  Les actes du Premier ministre sont contresignés, le cas échéant, par les ministres chargés de leur exécution  ; que l'exécution d'un décret d'extradition ne requiert l'intervention d'aucune décision d'application relevant de la compétence du ministre qui exerce la tutelle sur cet office ; que, dès lors, le décret attaqué n'est pas irrégulier faute d'avoir été contresigné par ce ministre ;<br/>
<br/>
              Considérant, en troisième lieu, qu'il résulte des principes généraux du droit applicables à l'extradition qu'il n'appartient pas aux autorités françaises, sauf en cas d'erreur évidente, de statuer sur le bien-fondé des charges retenues contre la personne recherchée ; qu'il ressort des pièces du dossier que le requérant n'a jamais contesté que la demande d'extradition s'appliquait à lui ; que, dès lors, nonobstant la question de savoir s'il avait perdu, ou non, la nationalité polonaise et si, partant, il pouvait être fait mention, dans la procédure d'extradition, que la personne recherchée était de nationalité polonaise, il n'apparaît pas qu'une erreur évidente aurait été commise en ce qui concerne la personne recherchée ;<br/>
<br/>
              Considérant, en quatrième lieu, qu'aux termes de l'article 1er de la convention de New-York relative au statut des apatrides du 28 septembre 1954, la qualité d'apatride est reconnue à  toute personne qu'aucun Etat ne considère comme son ressortissant en application de sa législation  ; que cette même convention stipule, à son article 7, que,  sous réserve des dispositions plus favorables prévues par cette convention, tout Etat contractant accordera aux apatrides le régime qu'il accorde aux étrangers en général  et qu'aux termes du paragraphe 1 de l'article 31 de la même convention :  Les Etats contractants n'expulseront un apatride se trouvant régulièrement sur leur territoire que pour des raisons de sécurité nationale ou d'ordre public (...)  ; que ces stipulations ne font pas obstacle à l'extradition d'un apatride ; que le requérant ne saurait davantage invoquer l'existence d'un principe général du droit de l'extradition faisant obstacle à une telle extradition, y compris vers un Etat dont la personne réclamée a eu la nationalité ; qu'ainsi, même si M. A, d'origine polonaise, soutient, sans être contredit, qu'il se serait vu reconnaître la qualité d'apatride par l'Office français de protection des réfugiés et des apatrides, il n'est pas fondé à soutenir que cette qualité ferait obstacle à son extradition vers la Pologne ;<br/>
<br/>
              Considérant, en cinquième lieu, que le requérant ne saurait utilement se fonder sur la circonstance que les Etats peuvent, dans certaines conditions, s'opposer à l'extradition de leurs nationaux, pour établir l'existence d'une méconnaissance du principe d'égalité à l'endroit des personnes apatrides, propre à entacher d'illégalité le décret accordant aux autorités polonaises sa propre extradition ; <br/>
<br/>
              Considérant, en sixième lieu, que les conventions d'extradition sont des lois de procédure qui, sauf stipulation contraire, sont applicables immédiatement aux faits survenus avant leur entrée en vigueur, même si elles ont un effet défavorable sur les intérêts de la personne réclamée ; qu'ainsi, les stipulations de la convention relative à l'extradition entre les Etats membres de l'Union européenne, signée à Dublin le 27 septembre 1996, qui se sont substituées, entre ces Etats, à compter de leur entrée en vigueur, à celles de la convention européenne d'extradition du 13 décembre 1957, ont vocation à régir la demande d'extradition de M. A ; que celui-ci ne saurait utilement se prévaloir des stipulations, relatives à la prescription, de l'article 10 de la convention de 1957, dès lors que s'y sont substituées celles du paragraphe 1 de l'article 8 de la convention du 27 septembre 1996, aux termes desquelles :  L'extradition ne peut être refusée au motif qu'il y a prescription de l'action ou de la peine selon la législation de l'Etat membre requis  ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier que la peine prononcée contre M. A, par décision définitive du 4 mars 1985 du tribunal de la Voïvodie de Lublin (Pologne), pour des faits de récidive de vol avec violence et de récidive de destruction de biens, est soumise à la prescription de trente ans selon la législation polonaise, et n'était ainsi pas prescrite en droit polonais lors de son arrestation ; que les stipulations de l'article 10 de la convention de 1996 faisant obstacle à ce que, après leur entrée en vigueur, les autorités françaises refusent d'accorder l'extradition pour un motif de prescription de la peine selon la législation française, le moyen tiré par M. A de ce que la prescription de la peine aurait été acquise en droit français, antérieurement à l'entrée en vigueur de la convention de Dublin, ne peut qu'être écarté ;<br/>
<br/>
              Considérant, en septième lieu, que M. A n'apporte aucun élément circonstancié à l'appui de ses allégations selon lesquelles le système judiciaire polonais ne respecterait pas les droits et libertés fondamentaux de la personne humaine, ainsi que l'exigent les principes généraux du droit de l'extradition ; <br/>
<br/>
              Considérant, enfin, que si une décision d'extradition est susceptible de porter atteinte, au sens de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, au droit au respect de la vie familiale, cette mesure trouve, en principe, sa justification dans la nature même de la procédure d'extradition, qui est de permettre, dans l'intérêt de l'ordre public et sous les conditions fixées par les dispositions qui la régissent, tant le jugement de personnes se trouvant en France qui sont poursuivies à l'étranger pour des crimes ou des délits commis hors de France que l'exécution, par les mêmes personnes, des condamnations pénales prononcées contre elles à l'étranger pour de tels crimes ou délits ; que ni l'ancienneté de la présence en France de M. A, ni la circonstance qu'il y aurait fondé une famille ne sont de nature à faire obstacle, dans l'intérêt de l'ordre public, à l'exécution de son extradition ; que, dès lors, le moyen tiré de la violation de l'article 8 de cette convention doit être écarté ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que M. A n'est pas fondé à demander l'annulation du décret du 19 octobre 2009 accordant son extradition aux autorités polonaises ; qu'il y a lieu, par voie de conséquence, de rejeter les conclusions qu'il présente au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A est rejetée. <br/>
Article 2 : La présente décision sera notifiée à M. Renek A et à la ministre d'Etat, garde des sceaux, ministre de la justice et des libertés.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-04-03 ÉTRANGERS. EXTRADITION. DÉCRET D'EXTRADITION. - CATÉGORIES DE PERSONNES SUSCEPTIBLES DE FAIRE L'OBJET D'UNE EXTRADITION - APATRIDES - INCLUSION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">335-05 ÉTRANGERS. RÉFUGIÉS ET APATRIDES. - APATRIDES - CATÉGORIES DE PERSONNES SUSCEPTIBLES DE FAIRE L'OBJET D'UNE EXTRADITION - INCLUSION [RJ1].
</SCT>
<ANA ID="9A"> 335-04-03 Ni la convention de New-York relative au statut des apatrides du 28 septembre 1954, ni aucun principe général du droit de l'extradition ne fait obstacle à l'extradition d'un apatride.</ANA>
<ANA ID="9B"> 335-05 Ni la convention de New-York relative au statut des apatrides du 28 septembre 1954, ni aucun principe général du droit de l'extradition ne fait obstacle à l'extradition d'un apatride.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., pour l'impossibilité, en principe, d'extrader un réfugié vers le pays qu'il a fui, Assemblée, 1er avril 1988, Bereciartua-Echarri, n° 85234, p. 135 ; s'agissant de la protection des apatrides à l'égard des mesures administratives d'éloignement, Section, 9 novembre 2007, Cheglali, n°s 261305 261354, p. 443.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
