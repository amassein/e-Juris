<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036660395</ID>
<ANCIEN_ID>JG_L_2018_02_000000409801</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/66/03/CETATEXT000036660395.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 28/02/2018, 409801, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-02-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409801</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Alexandre  Koutchouk</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:409801.20180228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société OCP Répartition a demandé au tribunal administratif de Montreuil de prononcer la décharge des cotisations supplémentaires de taxe professionnelle auxquelles elle a été assujettie au titre des années 2008 et 2009, des suppléments de cotisation foncière des entreprises qui lui ont été assignées au titre de l'année 2010 et de la cotisation foncière des entreprises établie au titre des années 2011 et 2012 dans les rôles de la commune de Saint-Ouen (Seine-saint-Denis). Par un jugement n° 1311027, 1403589 du 29 décembre 2014, le tribunal administratif a rejeté ces demandes. <br/>
<br/>
              Par un arrêt n° 15VE00665 du 28 février 2017, la cour administrative d'appel de Versailles, sur appel de la société OCP Répartition, a prononcé la décharge des suppléments de cotisation foncière des entreprises qui lui ont été assignés au titre de l'année 2010 et réformé le jugement attaqué en ce qu'il avait de contraire. <br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 14 avril et 4 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'économie et des finances demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler les articles 1er à 3 de cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société OCP Répartition.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 2009-1673 du 30 décembre 2009 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alexandre Koutchouk, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de la société OCP Répartition ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. D'une part, aux termes de l'article 2 de la loi du 30 décembre 2009 de finances pour 2010, repris à l'article 1447-0 du code général des impôts : " Il est institué une contribution économique territoriale composée d'une cotisation foncière des entreprises et d'une cotisation sur la valeur ajoutée des entreprises ". Aux termes de l'article 1640 B du code général des impôts, issu de la même loi, dans sa version applicable à l'année 2010 : " I. Pour le calcul des impositions à la cotisation foncière des entreprises au titre de l'année 2010, les communes et établissements publics de coopération intercommunale à fiscalité propre votent un taux relais, dans les conditions et limites prévues pour le taux de la taxe professionnelle par le présent code dans sa rédaction en vigueur au 31 décembre 2009, à l'exception du 4 du I de l'article 1636 B sexies. / Les impositions à la cotisation foncière des entreprises établies au titre de l'année 2010 sont perçues au profit du budget général de l'Etat. Elles sont calculées en faisant application des délibérations relatives aux exonérations et abattements prévues au I du 5. 3. 2 de l'article 2 de la loi n° 2009-1673 du 30 décembre 2009 de finances pour 2010 et en appliquant les taux communaux et intercommunaux de référence définis aux 1 à 6 du I de l'article 1640 C. / L'Etat perçoit 3 % du montant des impositions de cotisation foncière des entreprises établies au titre de l'année 2010. Ces sommes sont ajoutées au montant de ces impositions. / II. 1. a) Par dérogation aux dispositions des articles L. 2331-3, L. 3332-1, L. 4331-2, L. 5214-23, L. 5215-32, L. 5216-8 et L. 5334-4 du code général des collectivités territoriales et des articles 1379, 1586, 1599 bis, 1609 bis, 1609 quinquies C, 1609 nonies B et 1609 nonies C du présent code, les collectivités territoriales, à l'exception de la région Ile-de-France, et les établissements publics de coopération intercommunale dotés d'une fiscalité propre reçoivent au titre de l'année 2010, en lieu et place du produit de la taxe professionnelle, une compensation relais. / Le montant de cette compensation relais est, pour chaque collectivité ou établissement public de coopération intercommunale à fiscalité propre, égal au plus élevé des deux montants suivants : - le produit de la taxe professionnelle qui résulterait pour cette collectivité territoriale ou cet établissement public de l'application, au titre de l'année 2010, des dispositions relatives à cette taxe dans leur version en vigueur au 31 décembre 2009. Toutefois, pour le calcul de ce produit, d'une part, il est fait application des délibérations applicables en 2009 relatives aux bases de taxe professionnelle, d'autre part, le taux retenu est le taux de taxe professionnelle de la collectivité territoriale ou de l'établissement public pour les impositions au titre de l'année 2009 dans la limite du taux voté pour les impositions au titre de l'année 2008 majoré de 1 % ; / - le produit de la taxe professionnelle de la collectivité territoriale ou de l'établissement public au titre de l'année 2009 ". <br/>
<br/>
              2. D'autre part, aux termes du 1° de l'article L. 56 du livre des procédures fiscales, la procédure de rectification contradictoire n'est pas applicable " en matière d'impositions directes perçues au profit des collectivités locales (...) ".<br/>
<br/>
              3. Il résulte des dispositions citées au point 1 que la cotisation foncière des entreprises a été instaurée à compter du 1er janvier 2010 en remplacement de la taxe professionnelle par l'article 2 de la loi de finances pour 2010 dont les dispositions sont insérées au chapitre I " Impôts directs et taxes assimilées " du titre I " Impositions communales " de la deuxième partie du code général des impôts, consacrée aux " Impositions perçues au profit des collectivités locales et de divers organismes ". Cette cotisation constitue l'une des deux composantes de la contribution économique territoriale. Si, pour l'année de sa mise en place, il a été prévu qu'elle serait affectée au budget général de l'Etat, cette affectation constitue le premier volet d'un dispositif dont le second volet consiste en la redistribution du produit de la cotisation foncière des entreprises aux collectivités territoriales et leurs groupements, afin de maintenir au même niveau les ressources qu'ils tiraient l'année précédente de la perception de la taxe professionnelle, au moyen d'une " compensation relais " calculée sur la base du produit de la taxe professionnelle perçu en 2009 ou du produit qui résulterait, au titre de l'année 2010, de l'application des dispositions en vigueur au 31 décembre 2009 en retenant le taux de 2009, dans la limite du taux appliqué en 2008 majoré de 1%. Il suit de là que les dispositions de l'article 1640 B du code général des impôts ne sauraient être interprétées comme ayant donné à cette imposition, du seul fait de l'affectation du produit de la cotisation foncière des entreprises au titre de l'année 2010 au budget de l'Etat, le caractère d'une imposition d'Etat à laquelle la procédure contradictoire serait, en vertu des dispositions du 1° de l'article L. 56 du livre des procédures fiscales citées au point 2, applicable. <br/>
<br/>
              4. Par suite, en retenant que le supplément de cotisation foncière des entreprises mis à la charge de la société OCP Répartition au titre de l'année 2010 avait été établi à l'issue d'une procédure irrégulière en ce qu'elle n'avait pas été contradictoire, au motif que cette imposition était, pour l'année de sa mise en place, affectée au budget général de l'Etat, la cour a commis une erreur de droit. Le ministre de l'économie et des finances est dès lors, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, fondé à demander l'annulation de l'arrêt qu'il attaque en tant qu'il a déchargé la société OCP Répartition des impositions supplémentaires de cotisation foncière des entreprises mises à sa charge au titre de l'année 2010 à raison de son établissement situé à Saint-Ouen.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond dans cette mesure en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              6. Il résulte de ce qui a été dit au point 3 que la cotisation foncière des entreprises due au titre de l'année 2010 doit être regardée comme une imposition directe perçue au profit des collectivités locales au sens du 1° précité de l'article L. 56 du livre des procédures fiscales et que, dès lors, l'administration pouvait établir les impositions supplémentaires en litige sans être tenue de mettre en oeuvre la procédure de rectification contradictoire prévue à l'article L.55 du même livre. Par suite, la société OCP Répartition, qui ne conteste plus en cause d'appel que l'absence de recours par l'administration à cette procédure, n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Montreuil a rejeté sa demande. <br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que la somme que demande à ce titre la société OCP Répartition soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
Article 1er : Les articles 1er à 3 de l'arrêt de la cour administrative d'appel de Versailles du 28 février 2017 sont annulés.<br/>
Article 2 : Les conclusions présentées par la société OCP Répartition devant la cour administrative d'appel de Versailles tendant à la décharge des impositions supplémentaires de cotisation foncière des entreprises mises à sa charge au titre de l'année 2010 sont rejetées.<br/>
Article 3 : Les conclusions présentées par la société OCP Répartition au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'action et des comptes publics et à la société OCP Répartition.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
