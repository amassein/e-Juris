<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031640694</ID>
<ANCIEN_ID>JG_L_2015_12_000000372050</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/64/06/CETATEXT000031640694.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème SSJS, 16/12/2015, 372050, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372050</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP TIFFREAU, MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>M. Matias de Sainte Lorette</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:372050.20151216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Saint-Denis d'annuler la décision du 14 décembre 2010 par laquelle le recteur de l'académie de La Réunion a refusé de l'admettre à la retraite avec jouissance immédiate de sa pension à compter du 30 juin 2011 en sa qualité de père de trois enfants. Par un jugement n° 1100117 du 2 mai 2013, le tribunal administratif de Saint-Denis a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 13BX02139 du 3 septembre 2013, enregistrée le 10 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Bordeaux a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi présenté à cette cour par M.B....<br/>
<br/>
              Par un pourvoi, enregistré le 29 juillet 2013 au greffe de la cour administrative d'appel de Bordeaux, et par trois nouveaux mémoires enregistrés le 2 décembre 2013 et les 7 février et 6 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement n° 1100117 du 2 mai 2013 du tribunal administratif de Saint-Denis, de surseoir à statuer et de saisir la Cour de justice de l'Union européenne d'une question préjudicielle portant sur la conformité au droit de l'Union européenne des articles L. 24 et R. 37 du code des pensions civiles et militaires de retraite ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande, de formuler une injonction à l'encontre de l'Etat assortie d'une astreinte de 500 euros par jour en vue d'assurer l'exécution de sa décision à intervenir sur la présente affaire et de le condamner à lui verser l'arriéré de pension de retraite, majoré des intérêts aux taux légal à compter de la date d'exigibilité de cette pension et de la capitalisation de ces intérêts ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le traité instituant la Communauté européenne ; <br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code des pensions civiles et militaires de retraite ; <br/>
              - la loi n° 2003-775 du 21 août 2003 ;<br/>
              - la loi n° 2004-1485 du 30 décembre 2004 ;<br/>
              - la loi n° 2010-1330 du 9 novembre 2010 ;<br/>
              - l'arrêt C-173/13 du 17 juillet 2014 de la Cour de justice de l'Union européenne ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matias de Sainte Lorette, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Tiffreau, Marlange, de la Burgade, avocat de M. B...;<br/>
<br/>
<br/>
<br/>Sur le moyen tiré de la méconnaissance de l'article 141 du traité instituant la Communauté européenne :<br/>
<br/>
              1. Aux termes du 3° du I de l'article L. 24 du code des pensions civiles et militaires de retraite, dans sa rédaction applicable au litige en vertu des dispositions transitoires prévues à l'article 44 de la loi du 9 novembre 2010 : " I. - La liquidation de la pension intervient : / (...) 3° Lorsque le fonctionnaire civil est parent de trois enfants vivants, ou décédés par faits de guerre, ou d'un enfant vivant, âgé de plus d'un an et atteint d'une invalidité égale ou supérieure à 80 %, à condition qu'il ait, pour chaque enfant, interrompu son activité dans des conditions fixées par décret en Conseil d'Etat. Sont assimilées à l'interruption d'activité mentionnée à l'alinéa précédent les périodes n'ayant pas donné lieu à cotisation obligatoire dans un régime de retraite de base, dans des conditions fixées par décret en Conseil d'Etat. Sont assimilés aux enfants mentionnés au premier alinéa les enfants énumérés au II de l'article L. 18 que l'intéressé a élevés dans les conditions prévues au III dudit article ". En vertu des I et II de l'article R. 37 du même code, applicable au litige, le bénéfice des dispositions précitées du 3° du I de l'article L. 24 est subordonné à une interruption d'activité d'une durée continue au moins égale à deux mois dans le cadre d'un congé pour maternité, d'un congé pour adoption, d'un congé parental, d'un congé de présence parentale, ou d'une disponibilité pour élever un enfant de moins de huit ans.<br/>
<br/>
              2. Aux termes de l'article 141 du traité instituant la Communauté européenne, devenu l'article 157 du traité sur le fonctionnement de l'Union européenne : " 1. Chaque Etat membre assure l'application du principe de l'égalité des rémunérations entre travailleurs masculins et travailleurs féminins pour un même travail ou un travail de même valeur. / 2. Aux fins du présent article, on entend par rémunération, le salaire ou traitement ordinaire de base ou minimum, et tous autres avantages payés directement ou indirectement, en espèces ou en nature, par l'employeur au travailleur en raison de l'emploi de ce dernier. L'égalité de rémunération, sans discrimination fondée sur le sexe, implique : / a) que la rémunération accordée pour un même travail payé à la tâche soit établie sur la base d'une même unité de mesure ; / b) que la rémunération accordée pour un travail payé au temps soit la même pour un même poste de travail. / (...) 4. Pour assurer concrètement une pleine égalité entre hommes et femmes dans la vie professionnelle, le principe de l'égalité de traitement n'empêche pas un Etat membre de maintenir ou d'adopter des mesures prévoyant des avantages spécifiques destinés à faciliter l'exercice d'une activité professionnelle par le sexe sous-représenté ou à prévenir ou compenser des désavantages dans la carrière professionnelle ". Il résulte de ces dispositions, telles qu'interprétées par la Cour de justice de l'Union européenne, que le principe d'égalité des rémunérations s'oppose non seulement à l'application de dispositions qui établissent des discriminations directement fondées sur le sexe mais également à l'application de dispositions qui maintiennent des différences de traitement entre travailleurs masculins et travailleurs féminins sur la base de critères non fondés sur le sexe dès lors que ces différences de traitement ne peuvent s'expliquer par des facteurs objectivement justifiés et étrangers à toute discrimination fondée sur le sexe et qu'il y a discrimination indirecte en raison du sexe lorsque l'application d'une mesure nationale, bien que formulée de façon neutre, désavantage en fait un nombre beaucoup plus élevé de travailleurs d'un sexe par rapport à l'autre. Par un arrêt du 17 juillet 2014, la Cour de justice de l'Union européenne, statuant sur renvoi préjudiciel de la cour administrative d'appel de Lyon, a estimé que l'article 141 du traité instituant la Communauté européenne doit être interprété en ce sens que, sauf à pouvoir être justifié par des facteurs objectifs étrangers à toute discrimination fondée sur le sexe, tels qu'un objectif légitime de politique sociale, et à être propre à garantir l'objectif invoqué et nécessaire à cet effet, un régime de départ anticipé à la retraite tel que celui résultant des dispositions des articles L. 24 et R. 37 du code des pensions civiles et militaires de retraite, en tant qu'elles prévoient la prise en compte du congé maternité dans les conditions ouvrant droit au bénéfice en cause introduirait également une différence de traitement entre les travailleurs féminins et les travailleurs masculins contraire à cet article.<br/>
<br/>
              3. Cependant, la Cour de justice de l'Union européenne a rappelé que, s'il lui revenait de donner des " indications de nature à permettre à la juridiction nationale de statuer ", il revient exclusivement au juge national, qui est seul compétent pour apprécier les faits et pour interpréter la législation nationale, de déterminer si et dans quelle mesure les dispositions concernées sont justifiées par de tels facteurs objectifs. Par la loi du 9 novembre 2010, le législateur a modifié les dispositions sur le fondement desquelles a été prise la décision attaquée, en procédant à une extinction progressive de la mesure pour les parents de trois enfants. Ce faisant, le législateur a entendu non pas prévenir les inégalités de fait entre les hommes et les femmes fonctionnaires et militaires dans le déroulement de leur carrière et leurs incidences en matière de retraite, mais compenser à titre transitoire ces inégalités normalement appelées à disparaître. Dans ces conditions, la disposition litigieuse relative au choix d'un départ anticipé avec jouissance immédiate, prise, afin d'offrir, dans la mesure du possible, une compensation des conséquences de la naissance et de l'éducation d'enfants sur le déroulement de la carrière d'une femme, en l'état de la société française d'alors, est objectivement justifiée par un objectif légitime de politique sociale, qu'elle est propre à garantir cet objectif et nécessaire à cet effet. Par suite, les dispositions en cause ne méconnaissent pas le principe d'égalité des rémunérations tel que défini à l'article 141 du traité instituant la Communauté européenne, devenu l'article 157 du traité sur le fonctionnement de l'Union européenne.<br/>
<br/>
              Sur le moyen tiré de la méconnaissance de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales :<br/>
<br/>
              4. L'article 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales stipule que : " La jouissance des droits et libertés reconnus dans la présente Convention doit être assurée, sans distinction aucune, fondée notamment sur le sexe, la race, la couleur, la langue, la religion, les opinions politiques ou toutes autres opinions, l'origine nationale ou sociale, l'appartenance à une minorité nationale, la fortune, la naissance ou toute autre situation ". L'article 1er du premier protocole additionnel à cette même convention stipule que : " Toute personne physique ou morale a droit au respect de ses biens. Nul ne peut être privé de sa propriété que pour cause d'utilité publique et dans les conditions prévues par la loi et les principes généraux du droit international. / Les dispositions précédentes ne portent pas atteinte au droit que possèdent les Etats de mettre en vigueur les lois qu'ils jugent nécessaires pour réglementer l'usage des biens conformément à l'intérêt général ou pour assurer le paiement des impôts ou d'autres contributions ou des amendes ". Comme il a été dit ci-dessus, les articles L. 24 et R. 37 du code des pensions civiles et militaires ont pour objet de compenser les inconvénients, en termes de carrière, qui sont subis par les fonctionnaires du fait de l'interruption de leur service en raison de la naissance ou de l'éducation des enfants. Ces textes, qui fixent la durée d'interruption du service à deux mois au moins, se réfèrent aux positions statutaires permettant une telle interruption et reposent sur des critères objectifs, en rapport avec leurs buts. Ainsi, alors même qu'ils bénéficieraient principalement aux fonctionnaires de sexe féminin, ils n'ont pas méconnu les stipulations précitées de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. Dès lors, en écartant les moyens tirés de la méconnaissance de l'article 14 de cette convention et de l'article 1er de son premier protocole additionnel, le tribunal administratif de Saint-Denis n'a pas commis d'erreur de droit.<br/>
<br/>
              5. Il résulte de ce qui précède que, sans qu'il y ait lieu pour le Conseil d'Etat de saisir la Cour de justice de l'Union européenne d'une question préjudicielle portant sur la conformité au droit de l'Union européenne des dispositions du code des pensions civiles et militaires de retraite en litige, le pourvoi de M. B...doit être rejeté, y compris ses conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Le pourvoi de M. B... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. A... B..., à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
