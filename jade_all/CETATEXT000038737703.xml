<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038737703</ID>
<ANCIEN_ID>J1_L_2019_06_000001800364</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/73/77/CETATEXT000038737703.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de PARIS, 9ème chambre, 27/06/2019, 18PA00364, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-06-27</DATE_DEC>
<JURIDICTION>CAA de PARIS</JURIDICTION>
<NUMERO>18PA00364</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. JARDIN</PRESIDENT>
<AVOCATS>SEBBAN</AVOCATS>
<RAPPORTEUR>Mme Alexandra  STOLTZ-VALETTE</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme MIELNIK-MEDDAH</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
       Procédure contentieuse antérieure :<br/>
<br/>
        M. et Mme A... ont demandé au Tribunal administratif de Paris de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu mises à leur charge au titre des années 2011 et 2012 ainsi que des intérêts de retard et majorations correspondantes et celle des prélèvements sociaux supplémentaires mis à leur charge au titre de l'année 2012.<br/>
<br/>
       Par un jugement n° 1619970/1-2 du 5 décembre 2017, le Tribunal administratif de Paris     a rejeté leur demande.<br/>
<br/>
       Procédure devant la Cour :<br/>
<br/>
       Par une requête, enregistrée le 31 janvier 2018, M. et Mme A..., représentés par Me Sebban, demandent à la Cour :<br/>
<br/>
       1°) d'annuler ce jugement n° 1619970/1-2 du 5 décembre 2017 du Tribunal Administratif de Paris ;<br/>
<br/>
       2°) de prononcer la décharge sollicitée ;<br/>
<br/>
       3°) de mettre à la charge de l'Etat le versement de la somme de 15 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative ainsi que les entiers dépens.<br/>
<br/>
       Ils soutiennent que :<br/>
<br/>
       - c'est à tort que le service n'a pas fait droit à leur demande de saisine de la commission départementale des impôts pour la partie du litige ayant trait à la requalification en revenus fonciers des revenus locatifs incorporés aux bénéfices industriels et commerciaux ;<br/>
       - c'est à tort que l'administration a requalifié en bénéfice foncier les produits de location d'immeuble appartenant à Mme A...et inscrit à l'actif de l'entreprise de marchand de biens ; le service a méconnu les dispositions de l'article 155 du code général des impôts ;<br/>
       - la quote-part des dotations aux amortissements correspondants à la valeur des terrains non amortissables est excessive ; le service a méconnu les termes de l'engagement pris au cours des opérations de contrôle consistant à admettre un taux unique de 15 % pour la valeur des terrains ainsi que ceux de la proposition de rectification du 18 septembre 2013 adressée à la Sarl Mackenzi Investissements. <br/>
<br/>
       Par un mémoire en défense, enregistré le 13 avril 2018, le ministre de l'action et des comptes publics conclut au rejet de la requête.<br/>
<br/>
       Il soutient que les moyens soulevés par les requérants ne sont pas fondés.<br/>
<br/>
       Vu les autres pièces du dossier.<br/>
<br/>
       Vu :<br/>
       - le code général des impôts et le livre des procédures fiscales ;<br/>
       - le code de justice administrative.<br/>
<br/>
       Les parties ont été régulièrement averties du jour de l'audience.<br/>
<br/>
       Ont été entendus au cours de l'audience publique :<br/>
       - le rapport de Mme Stoltz-Valette ; <br/>
              - les conclusions de Mme Mielnik-Meddah, rapporteur public,<br/>
       - et les observations de Me Sebban, avocat de M. et MmeA.... <br/>
<br/>
       Considérant ce qui suit :<br/>
<br/>
       1. A l'issue de la vérification de comptabilité de l'activité de marchand de biens exercée par Mme A...sous la forme d'une entreprise individuelle, le service a notifié à M. et Mme A...des cotisations supplémentaires d'impôt sur le revenu au titre des exercices clos en 2011 et 2012 ainsi que des intérêts de retard et majorations correspondantes et des prélèvements sociaux supplémentaires au titre de l'année 2012. M. et Mme A...relèvent appel du jugement n° 1619970/1-2 du 5 décembre 2017 par lequel le Tribunal administratif de Paris a rejeté leur demande tendant à la décharge de ces impositions et pénalités.<br/>
<br/>
        Sur la régularité de la procédure d'imposition  :<br/>
<br/>
        2. Aux termes de l'article L. 59 du livre des procédures fiscales, dans sa version alors applicable : " Lorsque le désaccord persiste sur les rectifications notifiées, l'administration, si le contribuable le demande, soumet le litige à l'avis soit de la commission départementale des impôts directs et des taxes sur le chiffre d'affaires prévue à l'article 1651 du code général des impôts, soit de la commission départementale de conciliation prévue à l'article 667 du même code. Les commissions peuvent également être saisies à l'initiative de l'administration ". Aux termes de l'article L. 59 A du même livre : " I.-La commission départementale des impôts directs et des taxes sur le chiffre d'affaires intervient lorsque le désaccord porte : 1° Sur le montant du résultat industriel et commercial, non commercial, agricole ou du chiffre d'affaires, déterminé selon un mode réel d'imposition ; (...) II. Dans les domaines mentionnés au I, la commission départementale des impôts directs et des taxes sur le chiffre d'affaires peut, sans trancher une question de droit, se prononcer sur les faits susceptibles d'être pris en compte pour l'examen de cette question de droit. ".<br/>
<br/>
      3. Si M. et Mme A...soutiennent que le service les a irrégulièrement privés de la possibilité de saisir la commission départementale des impôts directs et des taxes sur le chiffre d'affaires, il résulte de l'instruction que le désaccord opposant alors l'administration aux contribuables portait sur la requalification en revenus fonciers de revenus locatifs incorporés aux bénéfices industriels et commerciaux. Ce différent, qui, en l'espèce, ne portait sur aucune question de fait mais sur l'interprétation à donner aux dispositions du 1) du II de l'article 155 du code général des impôts, n'entrait pas dans le champ de la compétence de la commission départementale des impôts directs et des taxes sur le chiffre d'affaires. Par suite M. et Mme A...ne sont pas fondés à soutenir que les impositions supplémentaires dont ils ont fait l'objet à la suite du contrôle de l'activité de marchand de biens de Mme A...ont été établies à l'issue d'une procédure irrégulière.<br/>
<br/>
         Sur le bien fondé de l'imposition :<br/>
<br/>
        En ce qui concerne la requalification de bénéfices industriels et commerciaux en bénéfices fonciers :<br/>
<br/>
        4. D'une part, aux termes de l'article 155 du code général des impôts, dans sa rédaction applicable à l'imposition au titre de l'année 2012 : " II.-1. Le bénéfice net mentionné à l'article 38 est : 1° Diminué du montant des produits qui ne proviennent pas de l'activité exercée à titre professionnel, à l'exclusion de ceux pris en compte pour la détermination de la plus-value ou moins-value de cession d'un élément d'actif immobilisé ou pour la détermination des résultats mentionnés au I ; (...) III.-1. Les charges et produits mentionnés au 1 du II sont retenus, suivant leur nature, pour la détermination : 1° Des revenus fonciers, des revenus de capitaux mobiliers, des profits mentionnés aux articles 150 ter à 150 undecies ou des plus-values de cession à titre onéreux de biens ou droits de toute nature mentionnées aux articles 150-0 A à 150 VH, selon les règles applicables à ces catégories de revenus ; (...) IV.-1. Sous réserve du 2, l'exercice à titre professionnel implique la participation personnelle, directe et continue à l'accomplissement des actes nécessaires à l'activité ".<br/>
<br/>
        5. D'autre part, aux termes du 1° du I de l'article 35 du code général des impôts : " I. présentent également le caractère de bénéfices industriels et commerciaux, pour l'application de l'impôt sur le revenu, les bénéfices réalisés par les personnes physiques désignées ci-après : 1° Personnes qui, habituellement, achètent en leur nom, en vue de les revendre, des immeubles, des fonds de commerce, des actions ou parts de sociétés immobilières ou qui, habituellement, souscrivent, en vue de les revendre, des actions ou parts créées ou émises par les mêmes sociétés ".<br/>
<br/>
       6. En application de l'article 35 du code précité, il y a lieu de prendre en compte pour la détermination des résultats imposables des marchands de biens les revenus procurés par l'exploitation des immeubles faisant partie du stock immobilier, objet de son négoce, et qui sont rattachés à son activité commerciale. Ces produits doivent être soumis à l'impôt dans la catégorie des bénéfices industriels et commerciaux. Conformément au II de l'article 155 du même code, les produits qui ne proviennent pas de l'activité exercée à titre professionnel ne sont pas inclus dans les bénéfices de l'entreprise. <br/>
<br/>
       7. A l'occasion des opérations de contrôle, le service a constaté que MmeA..., qui exerce l'activité de marchand de biens depuis 1988, a inscrit à l'actif immobilisé de son entreprise individuelle douze biens immobiliers acquis entre 1988 et 2003 et initialement comptabilisés en stock, qui sont donnés en location en vertu de baux commerciaux ou de baux d'habitation. Ces biens ont par ailleurs fait l'objet d'un plan d'amortissement. Estimant que les revenus issus de la location de ces immeubles ne se rattachent pas à l'activité de marchand de bien exercée par la contribuable, le service les a exclus du résultat professionnel et imposés comme des revenus fonciers. Le service a ainsi extourné les produits déclarés des bénéfices industriel et commerciaux, réintégré les charges correspondantes et imposé les revenus locatifs dans la catégorie des revenus fonciers entre les mains de M. et MmeA....  Pour l'application du II de l'article 155 du code général des impôts, M. et Mme A...ne peuvent utilement prétendre que l'exploitation de ces douze biens immobiliers, comptabilisés parmi l'actif immobilisé de l'entreprise, ayant donné lieu à un amortissement comptable et ayant été donnés à bail, constituerait une activité marginale ou accessoire à l'activité de marchand de bien au sens de ces dispositions. Contrairement à ce que les requérants soutiennent, les biens litigieux ne pouvaient plus être regardés comme faisant partie du stock et associés à l'activité de marchand de biens. Par suite, ils se trouvaient dépourvus de lien avec l'activité de marchand de biens, qui est en l'espèce l'activité exercée à titre professionnel au sens du II de l'article 155 du code précité. Est sans incidence la circonstance que trois de ces immeubles auraient été cédés ultérieurement entre 2010 et 2012. Dès lors, c'est à tort que MmeA..., au titre de l'imposition de l'année 2012, a déclaré les revenus tirés de la location des biens immobiliers inscrits à l'actif immobilisé de son entreprise individuelle en les incluant dans les bénéfices industriels et commerciaux tirés de son activité de marchand de biens. Par suite M. et Mme A...ne sont pas fondés à soutenir que le service a, à tort, imposé les revenus en cause comme des revenus fonciers, selon les modalités prévues par les dispositions précitées du III de l'article 155 du code général des impôts.<br/>
<br/>
        En ce qui concerne les amortissements pratiqués :<br/>
<br/>
       8. Aux termes de l'article 39 du code général des impôts, applicable en matière d'impôt sur les sociétés en vertu de l'article 209 du même code : " 1. Le bénéfice net est établi sous déduction de toutes charges, celles-ci comprenant (...) notamment : (...) / 2° (...) les amortissements réellement effectués par l'entreprise, dans la limite de ceux qui sont généralement admis d'après les usages de chaque nature d'industrie, de commerce ou d'exploitation (...) ". Aux termes de l'article 38 quinquies de l'annexe III au même code : " 1. Les immobilisations sont inscrites au bilan pour leur valeur d'origine. / Cette valeur d'origine s'entend : / a. Pour les immobilisations acquises à titre onéreux, du coût d'acquisition, c'est-à-dire du prix d'achat minoré des remises, rabais commerciaux et escompte de règlement obtenus et majorés des coûts directement engagés pour la mise en état d'utilisation du bien et des coûts d'emprunt dans les conditions prévues à l'article 38 undecies ". Aux termes de l'article 38 sexies de l'annexe III au même code : " La dépréciation des immobilisations qui ne se déprécient pas de manière irréversible, notamment les terrains (...)  donne lieu à la constitution de provisions dans les conditions prévues au 5° du 1 de l'article 39 du code général des impôts ". Il résulte de ces dispositions, d'une part, que la valeur d'origine servant de référence à l'inscription en immobilisation d'un bien acquis à titre onéreux est constituée du prix d'achat, le cas échéant majoré des coûts directement engagés pour la mise en état d'utilisation du bien et, d'autre part, que les immobilisations qui ne se déprécient pas avec le temps, comme les terrains, ne donnent pas lieu à amortissement. Il en est ainsi des terrains d'assiette des immeubles bâtis, même si ces derniers occupent toute la superficie de ces terrains.<br/>
<br/>
       9. Lorsque l'administration remet en cause la répartition, au sein du bilan d'un contribuable, entre les valeurs retenues respectivement pour un terrain et pour une construction édifiée sur ce terrain, en invoquant l'insuffisance de la valeur retenue pour le terrain, il lui appartient d'établir l'insuffisance de cette valeur. Elle doit, pour déterminer la valeur du terrain, se fonder prioritairement sur des comparaisons reposant sur des transactions réalisées sur des terrains nus et à des dates proches de celle de l'entrée du bien au bilan du contribuable. Ces terrains doivent être situés dans la même zone géographique que ce bien et présenter des droits à construire similaires. A défaut, l'administration peut évaluer la valeur de la construction à partir de son coût de reconstruction à la date de son entrée au bilan, en lui appliquant, le cas échéant, les abattements nécessaires pour prendre en compte sa vétusté et son état d'entretien.<br/>
<br/>
       10. Lorsqu'elle ne peut appliquer aucune des deux méthodes précédentes, notamment pour les immeubles les plus anciens, l'administration peut s'appuyer sur des données comptables issues du bilan d'autres contribuables pour déterminer des taux moyens relatifs aux parts respectives du terrain et de la construction et les appliquer ensuite à la valeur globale de l'immeuble en litige à sa date d'entrée au bilan. Elle doit, en ce dernier cas, se fonder sur un échantillon pertinent reposant sur un nombre de données significatif, portant sur des immeubles présentant des caractéristiques comparables s'agissant de la localisation, du type de construction, de l'état d'entretien et des possibilités éventuelles d'agrandissement. Seuls peuvent être retenus des immeubles entrés au bilan des entreprises servant de termes de comparaison à des dates proches de celle de l'entrée au bilan de l'immeuble en litige.<br/>
<br/>
       11. Il est loisible au contribuable de démontrer soit que le choix de la méthode retenue par l'administration ou sa mise en oeuvre sont erronés au regard des principes ainsi définis, soit de justifier l'évaluation qu'il a retenue en se référant à d'autres données que celles qui lui sont opposées par l'administration.<br/>
<br/>
       12. Le service a remis en cause le montant des dotations aux amortissements pratiquées par la contribuable sur les biens immobiliers figurant à l'actif immobilisé du bilan au 31 décembre 2011 au motif qu'elle avait sous-estimé la part non amortissable représentant la valeur des terrains en retenant une quote-part de 3 % revenant à la valeur du terrain.<br/>
<br/>
       13. En se prévalant, sans être contredit, de la rareté des transactions réalisées sur des terrains situés dans le secteur géographique du bien immobilier en litige et de la difficulté, s'agissant d'un immeuble ancien, de déterminer son coût de reconstruction à l'identique, le ministre justifie de l'impossibilité dans laquelle se trouvait le service de recourir aux deux premières méthodes d'évaluation ci-dessus décrites. C'est, par suite, à bon droit que l'administration s'est fondée, pour évaluer le prix de revient du terrain d'assiette des contribuables, sur les données comptables issues du bilan d'autres contribuables.<br/>
<br/>
       14. Pour remettre en cause la quote-part de 3 % revenant à la valeur du terrain, le service a, en se fondant sur des comparables, estimé que le montant de la dotation aux amortissements venant en déduction des résultats imposables de la contribuable devait être déterminé en fixant entre 15 % et 40 % la part du terrain dans le prix des biens immobiliers. S'agissant des immeubles situés respectivement à Saint-Maximin et à Sarcelles, M. et Mme A...ne contestent pas le taux de 15 % retenu par le service. Pour les autres immeubles, les requérants font valoir que les termes de comparaison retenus ne sont pas pertinents et que leurs caractéristiques ne sont pas précisées exception faite de leur situation géographique. Il résulte des termes de la proposition de rectification du 12 décembre 2013 que le service n'a indiqué que la seule adresse des termes de comparaison retenus, le prix total, la quote-part respective des terrains et des constructions et la répartition pour déterminer la quote-part revenant à la valeur du terrain. En outre, aucune des pièces fournies par l'administration ne permet d'apprécier la nature des constructions admises par le service à titre de comparaison, leur état d'entretien ainsi que leur année d'acquisition ou de construction. Dans ces conditions, l'administration, qui ne s'est pas fondée sur un échantillon pertinent reposant sur un nombre de données significatif, n'apporte pas la preuve qui lui incombe que la requérante aurait procédé à une évaluation insuffisante de la valeur des terrains, justifiant la correction du montant des dotations aux amortissements qu'elle avait comptabilisées et déduites des résultats de l'exercice clos en 2011. Par ailleurs, M. et Mme A...admettent le bien-fondé du taux de 15 % et demandent à la Cour de retenir ce taux pour la valeur des terrains en litige. Par conséquent, M. et Mme A...sont fondés à obtenir la réduction de leur base d'imposition de l'impôt sur le revenu mis à leur charge au titre de l'année 2011, en fixant pour la détermination du montant des amortissements venant en déduction de cette base imposable à 15 % la part du terrain dans le prix de revient des biens immobiliers en litige à la date de leur inscription au bilan.<br/>
<br/>
       15. Il résulte de ce qui précède que M. et Mme A...sont seulement fondés à soutenir que c'est à tort que le Tribunal administratif de Paris a rejeté leurs conclusions tendant à la réduction de la cotisation supplémentaire d'impôt sur le revenu mise à leur charge au titre de l'année 2011 ainsi que des intérêts de retard et majorations correspondantes, résultant de la remise en cause des amortissements pratiqués au regard de la répartition entre la valeur des terrains et des constructions.<br/>
<br/>
        Sur les conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
	16. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 1 500 euros au titre des frais exposés par M. et Mme A...et liés à l'instance.<br/>
       Sur les dépens :<br/>
<br/>
        17. La présente instance n'ayant occasionné aucun des frais prévus par l'article R. 761-1 du code de justice administrative, les conclusions présentées par M. et Mme A...tendant à ce que soit mis à la charge de l'Etat les dépens de l'instance doivent, en tout état de cause, être rejetées.<br/>
<br/>
DÉCIDE :<br/>
Article 1er : La base de l'impôt sur le revenu fixée à M. et Mme A...au titre de l'année 2011 est réduite en tenant compte du montant des amortissements déterminé en fixant à 15 % la part du terrain dans le prix de revient des biens immobiliers en litige à la date de leur inscription au bilan.<br/>
Article 2 : La cotisation d'impôt sur le revenu mise à la charge de M. et Mme A...au titre de l'année 2011 ainsi que les pénalités correspondantes sont réduites dans les conditions qui résultent de l'article 1er. <br/>
Article 3: Le jugement du tribunal administratif de Paris  n° 1619970/1-2 du 5 décembre 2017 est réformé en ce qu'il a de contraire au présent arrêt.<br/>
Article 4 : L'Etat versera à M. et Mme A...une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Le surplus des conclusions de la requête d'appel de M. et Mme A...est rejeté.<br/>
Article 6 : Le présent arrêt sera notifié à M. et Mme A... et au Ministre de l'Action et des comptes publics.<br/>
Copie en sera adressée à la direction de contrôle fiscal Ile-de-France (division juridique est).<br/>
Délibéré après l'audience du 13 juin 2019, à laquelle siégeaient :<br/>
<br/>
- M. Jardin, président de chambre,<br/>
- M. Dalle, président assesseur,<br/>
- Mme Stoltz-Valette, premier conseiller,<br/>
<br/>
Lu en audience publique, le 27 juin 2019.<br/>
<br/>
Le rapporteur,<br/>
A. STOLTZ-VALETTELe président,<br/>
C. JARDIN<br/>
Le greffier,<br/>
C. BUOT<br/>
       La République mande et ordonne au ministre de l'action et des comptes publics en ce qui le concerne et à tous huissiers de justice à ce requis en ce qui concerne les voies de droit commun contre les parties privées, de pourvoir à l'exécution de la présente décision.<br/>
2<br/>
N° 18PA00364<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">19-04-02-01-03 Contributions et taxes. Impôts sur les revenus et bénéfices. Revenus et bénéfices imposables - règles particulières. Bénéfices industriels et commerciaux. Évaluation de l'actif.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">19-04-02-01-03-01 Contributions et taxes. Impôts sur les revenus et bénéfices. Revenus et bénéfices imposables - règles particulières. Bénéfices industriels et commerciaux. Évaluation de l'actif. Théorie du bilan.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
