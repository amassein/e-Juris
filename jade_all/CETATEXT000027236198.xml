<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027236198</ID>
<ANCIEN_ID>JG_L_2013_03_000000360505</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/23/61/CETATEXT000027236198.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 27/03/2013, 360505</TITRE>
<DATE_DEC>2013-03-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360505</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2013:360505.20130327</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 26 juin et 26 septembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SELARL EMJ, agissant en qualité de liquidateur judiciaire de la société DG Construction, dont le siège est 62 boulevard de Sébastopol à Paris (75003) ; la SELARL EMJ demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 10LY02651 du 26 avril 2012 par lequel la cour administrative d'appel de Lyon a rejeté la requête de la société Bianco tendant à la réformation du jugement n° 0503923 du 23 septembre 2010 du tribunal administratif de Grenoble en ce qu'il rejette ses conclusions relatives aux prestations effectuées par son sous-traitant, la société DG Construction ; <br/>
<br/>
              2°) réglant l'affaire au fond, d'annuler le jugement du tribunal administratif de Grenoble en tant qu'il rejette la demande d'indemnisation de la société Bianco des préjudices subis par la société DG Construction et d'accorder l'indemnisation demandée, soit la somme de 82 647,78 euros majorée de la TVA ;<br/>
<br/>
              3°) de mettre à la charge du département de la Haute-Savoie le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu le code des marchés publics ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, Maître des Requêtes, <br/>
<br/>
              - les observations de la SCP Gatineau, Fattaccini, avocat de la SELARL EMJ, et de la SCP Boré et Salve de Bruneton, avocat du département de la Haute-Savoie,<br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Gatineau, Fattaccini, avocat de la SELARL EMJ, et à la SCP Boré et Salve de Bruneton, avocat du département de la <br/>
Haute-Savoie ;<br/>
<br/>
<br/>
<br/>1. Considérant, d'une part, que la voie du recours en cassation n'est ouverte, suivant les principes généraux de la procédure, qu'aux personnes qui ont eu la qualité de partie dans l'instance ayant donné lieu à la décision attaquée ; que, d'autre part, dans les litiges de plein contentieux, doivent être regardés comme des parties à l'instance d'appel, recevables ainsi à se pourvoir en cassation, les intervenants devant le juge d'appel qui peuvent se prévaloir d'un droit auquel la décision à rendre est susceptible de préjudicier ; <br/>
<br/>
              2. Considérant que lorsqu'un entrepreneur principal saisit le juge du contrat d'une action indemnitaire à l'encontre du maître de l'ouvrage au titre d'un différend dans l'exécution d'un marché public, le sous-traitant ne peut être regardé comme pouvant, dans le cadre de ce litige, se prévaloir d'un droit auquel la décision à rendre est susceptible de préjudicier, y compris lorsque l'entrepreneur principal entendrait demander le paiement de sommes pour des prestations effectuées par ce sous-traitant ;<br/>
<br/>
              3. Considérant que, par l'arrêt attaqué du 26 avril 2012, la cour administrative d'appel de Lyon a rejeté la requête de la société Bianco tendant à l'annulation du jugement du 23 septembre 2010 du tribunal administratif de Grenoble en tant qu'il avait rejeté ses conclusions tendant à la condamnation du département de la Haute-Savoie à l'indemniser au titre de prestations supplémentaires effectuées par les sociétés DG Construction et SERF ; que la société DG Construction étant l'un des sous-traitants de la société Bianco, elle ne pouvait, dans le cadre du litige devant la cour, se prévaloir d'un droit auquel l'arrêt à rendre était susceptible de préjudicier ; que, dans ces conditions, si la société DG Construction est intervenue en appel à l'appui de la requête de la société Bianco, elle n'avait pas la qualité de partie à l'instance devant la cour administrative d'appel de Lyon ; qu'ainsi, la SELARL EMJ, agissant en qualité de liquidateur judiciaire de la société DG Construction, n'a pas qualité pour se pourvoir en cassation contre l'arrêt de la cour ; qu'il suit de là que son pourvoi n'est pas recevable ; <br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge du département de la Haute-Savoie, qui n'est pas, dans la présente instance, la partie perdante, la somme que demande la SELARL EMJ au titre des frais exposés par elle et non compris dans les dépens ; qu'il y a lieu, en revanche, en application de ces dispositions, de mettre à la charge de la SELARL EMJ une somme de 3 000 euros à verser au département de la Haute-Savoie ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la SELARL EMJ est rejeté.<br/>
Article 2 : La SELARL EMJ versera une somme de 3 000 euros au département de la <br/>
Haute-Savoie en application de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la SELARL EMJ, agissant en qualité de liquidateur judiciaire de la société DG Construction, à la société Bianco et au département de la Haute-Savoie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-08 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - CONTENTIEUX INDEMNITAIRE - ENTREPRENEUR PRINCIPAL SAISISSANT LE JUGE DU CONTRAT D'UNE ACTION INDEMNITAIRE À L'ENCONTRE DU MAÎTRE DE L'OUVRAGE AU TITRE D'UN DIFFÉREND DANS L'EXÉCUTION D'UN MARCHÉ PUBLIC - POSSIBILITÉ POUR UN SOUS-TRAITANT DE SE PRÉVALOIR D'UN DROIT AUQUEL LA DÉCISION À RENDRE EST SUSCEPTIBLE DE PRÉJUDICIER - ABSENCE - CONSÉQUENCE - QUALITÉ DE PARTIE À L'INSTANCE - ABSENCE.
</SCT>
<ANA ID="9A"> 39-08 Lorsqu'un entrepreneur principal saisit le juge du contrat d'une action indemnitaire à l'encontre du maître de l'ouvrage au titre d'un différend dans l'exécution d'un marché public, le sous-traitant ne peut être regardé comme pouvant, dans le cadre de ce litige, se prévaloir d'un droit auquel la décision à rendre est susceptible de préjudicier, y compris lorsque l'entrepreneur principal entendrait demander le paiement de sommes pour des prestations effectuées par ce sous-traitant. Il n'a, par conséquence, pas qualité de partie à l'instance.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
