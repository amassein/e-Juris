<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030189636</ID>
<ANCIEN_ID>JG_L_2015_01_000000387329</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/18/96/CETATEXT000030189636.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 29/01/2015, 387329, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-01-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387329</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2015:387329.20150129</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le recours, enregistré le 22 janvier 2015 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de l'intérieur ; le ministre demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance du 22 décembre 2014 par laquelle le juge des référés du tribunal administratif de Toulouse, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a, d'une part, suspendu l'exécution des arrêtés du 12 décembre 2014 du préfet de l'Aveyron portant réadmission vers la Hongrie de M. D...A...et Mme B... C...et, d'autre part, enjoint au préfet de réexaminer leur demande d'admission provisoire au séjour au titre de l'asile dans un délai de quinze jours à compter de la notification de l'ordonnance ;<br/>
<br/>
              2°) de rejeter la demande de première instance présentée par M. A... et Mme C... ;<br/>
<br/>
<br/>
              il soutient que :<br/>
              - la demande de première instance de M. A...et de Mme C...est irrecevable, dès lors qu'ils ne pouvaient demander la suspension de l'exécution des arrêtés du préfet du 12 décembre 2014 que sur le fondement de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - à titre subsidiaire, les arrêtés litigieux n'ont porté une atteinte grave et manifestement illégale ni au droit de M. A...et Mme C...de solliciter le statut de réfugié ni à l'intérêt supérieur de leurs enfants, au sens de l'article 3-1 de la convention internationale relative aux droits de l'enfant, et n'ont pas méconnu l'article 6 du règlement n° 604/2013 du 26 juin 2013 du Parlement européen et du Conseil, dès lors qu'ils ne seront pas séparés de leurs parents ;<br/>
              - l'ordonnance contestée est entachée de dénaturation des faits et des pièces du dossier, dès lors que l'entretien individuel prévu aux points 5 et 6 de l'article 5 du règlement n° 604/2013 du 26 juin 2013 du Parlement européen et du Conseil a bien eu lieu ;<br/>
              - M. A... et Mme C...n'établissent pas que leur demande ne sera pas examinée avec toutes les garanties nécessaires en Hongrie ;<br/>
<br/>
<br/>
              Vu l'ordonnance attaquée ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 28 janvier 2015 présenté par M. A... et MmeC..., qui concluent au rejet du recours et à ce que soit mis à la charge de l'Etat une somme de 2 000 euros au titre de l'article 37 alinéa 2 de la loi du 10 juillet 1991 ; ils demandent en outre la suspension de l'exécution des arrêtés du préfet de la Haute-Garonne en date du 12 novembre 2014 ;<br/>
<br/>
              ils soutiennent que :<br/>
              - leur demande de première instance est recevable, dès lors qu'ils ont été privés du droit d'agir dans le délai de 48 h et n'avaient d'autre choix que de saisir la juridiction sur le fondement de l'article L. 521-2 du code de justice administrative ; en tout état de cause, elle est devenue recevable depuis que les mesures d'assignation à résidence dont ils faisaient l'objet ne sont plus susceptibles de recevoir exécution ; <br/>
              - le préfet de l'Aveyron a porté une atteinte grave et manifestement illégale à leurs droits de demandeur d'asile en organisant une procédure ne leur offrant  pas l'ensemble des garanties auxquelles ils peuvent prétendre,  notamment au moyen de l'entretien individuel prévu à l'article 5 du règlement n° 604/2013/UE du Parlement européen et du Conseil du 26 juin 2013 <br/>
              - les arrêtés litigieux méconnaissent les dispositions de l'article 3-1 de la convention internationale relative aux droits de l'enfant et l'article 6 du règlement n° 604/2013 du 26 juin 2013 du Parlement européen et du Conseil dans la mesure où, en cas de réadmission en Hongrie, les parents seront placés avec leurs enfants au centre de Debrecen ;<br/>
              - ils méconnaissent les dispositions combinées des articles 2, 3 et 13 de la CEDH en ce que les requérants risquent de voir leur demande rejetée sans examen sérieux et particulier eu égard à la procédure prévue en Hongrie ;<br/>
              - dans tous les cas, les autorités françaises ont porté une atteinte manifeste au droit d'asile en refusant de faire usage de la faculté, ouverte pour des motifs humanitaires, d'examiner leur demande alors même que l'Etat responsable était normalement la Hongrie ;<br/>
<br/>
              Vu l'intervention, enregistrée le 28 janvier 2015, présentée par l'association la Cimade, dont le siège social est 64, rue Clisson à Paris (75013), représentée par son président, qui conclut aux mêmes fins par les mêmes moyens ; elle soutient en outre qu'elle a intérêt à agir dès lors qu'elle défend les droits des personnes réfugiées et migrantes ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, le ministre de l'intérieur, d'autre part, M. A..., MmeC...  et la Cimade ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 29 janvier 2015 à 9 heures 30  au cours de laquelle ont été entendus : <br/>
<br/>
              - les représentants du ministre de l'intérieur ;<br/>
<br/>
              - Me Stoclet, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. A... et MmeC... ;<br/>
<br/>
              -  le représentant de la Cimade ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a prolongé l'instruction jusqu'à 14 heures ;<br/>
<br/>
              Vu les pièces enregistrées le 29 janvier présentées par le ministre de l'intérieur ;<br/>
<br/>
              Vu les pièces enregistrées le 29 janvier présentées par M. A... et Mme C... ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu la convention internationale relative aux droits de l'enfant ;<br/>
<br/>
              Vu le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
<br/>
              Vu la directive 2013/33/UE du 26 juin 2013 établissant les normes pour l'accueil des personnes demandant la protection internationale ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu la loi du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Sur l'intervention présentée par la Cimade :<br/>
<br/>
              1. Considérant que la Cimade a intérêt au maintien de l'ordonnance attaquée, que son intervention est par suite recevable ;<br/>
<br/>
              Sur les arrêtés portant assignation à résidence de M. A...et de MmeC... :<br/>
<br/>
              2. Considérant qu'il résulte de l'instruction que l'assignation à résidence de M. A... et de MmeC... a été décidée, par deux arrêtés du préfet de l'Aveyron du 12 décembre 2014 notifiés le jour même aux intéressés, pour une durée de quarante-cinq jours à compter de leur notification ; qu'il suit de là que les conclusions relatives à ces décisions, qui ne sont plus susceptibles de recevoir exécution, sont devenues sans objet ; qu'il n'y a dès lors pas lieu d'y statuer ;<br/>
<br/>
              Sur les arrêtés du 12 décembre 2014 ordonnant la remise de M. A...et de Mme C... aux autorités hongroises :<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ; que le droit d'asile constitue une liberté fondamentale au sens des dispositions de l'article L. 521-2 du code de justice administrative ;<br/>
<br/>
              3. Considérant qu'il résulte de l'instruction que M. A...et Mme  C..., de nationalité kosovare, sont entrés en France avec leurs cinq filles mineures, en août 2014 pour rejoindre le frère de M.A..., ressortissant français qui réside à Onet le Château, dans l'Aveyron ; qu'après s'être présentés à la préfecture de l'Aveyron, le 9 septembre 2014, pour y déposer une demande d'admission au séjour au titre de l'asile, ils ont été convoqués à la préfecture de Toulouse, le 17 septembre 2014 ; qu'après avoir constaté, au moyen du fichier " Eurodac ", que M. A...et Mme C...avaient déjà présenté une demande d'asile auprès de la Hongrie, le 25 juillet 2014, le préfet de la Haute-Garonne a sollicité leur réadmission en Hongrie ; qu'après avoir obtenu l'accord des autorités hongroises, le 30 septembre 2014, ce dernier a refusé, par arrêtés du 12 novembre 2014 pris sur le fondement de l'article L. 741-4 du code de l'entrée et du séjour des étrangers et du droit d'asile, de les admettre au séjour ; qu'ensuite, le préfet de l'Aveyron a ordonné la remise de M. A...et Mme C... aux autorités hongroises, par deux arrêtés du 12 décembre 2014 ; que, saisi sur le fondement de l'article L. 521-2 du code de justice administrative, le juge des référés du tribunal administratif de Toulouse a ordonné la suspension de l'exécution de ces deux arrêtés et a enjoint à l'autorité préfectorale de réexaminer la situation des intéressés dans un délai de quinze jours, par une ordonnance du 22 décembre 2014 ; que le ministre de l'intérieur relève appel de cette ordonnance ;<br/>
<br/>
              4. Considérant qu'il ressort des dispositions III de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile que le législateur a entendu organiser une procédure spéciale afin que le juge administratif statue rapidement sur la légalité des mesures relatives à l'éloignement des étrangers, hors la décision refusant le séjour et les mesures d'expulsion, lorsque ces derniers sont placés en rétention ou assignés à résidence ; que cette procédure est applicable quelle que soit la mesure d'éloignement, autre qu'un arrêté d'expulsion, en vue de l'exécution de laquelle le placement en rétention ou l'assignation à résidence ont été pris, y compris en l'absence de contestation de cette mesure ; qu'ainsi, dans le cas où un étranger est placé en rétention ou assigné à résidence en vue de sa remise aux autorités compétentes d'un autre Etat, il appartient au président du tribunal administratif ou au magistrat qu'il délègue de statuer, selon les dispositions du III de l'article L. 512-1, sur les conclusions dirigées contre la décision de placement en rétention ou d'assignation à résidence et sur celles dirigées contre la décision aux fins de réadmission, notifiée à l'intéressé en même temps que la mesure de placement en rétention ou d'assignation à résidence ; qu'il résulte des pouvoirs confiés au juge par les dispositions du III de l'article L. 512-1, des délais qui lui sont impartis pour se prononcer et des conditions de son intervention, que la procédure spéciale prévue par le code de l'entrée et du séjour des étrangers et du droit d'asile présente des garanties au moins équivalentes à celles des procédures régies par le livre V du code de justice administrative ; qu'il résulte de ce qui précède qu'il appartient à l'étranger qui entend contester une mesure de remise aux autorités d'un autre Etat, lorsque cette mesure de remise est accompagnée d'un placement en rétention administrative ou d'une mesure d'assignation à résidence, de saisir le juge administratif sur le fondement des dispositions du III de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile d'une demande tendant à leur annulation, assortie le cas échéant de conclusions à fin d'injonction ; que cette procédure particulière est exclusive de celles prévues par le livre V du code de justice administrative ; que, dès lors que les deux arrêtés ordonnant la remise aux autorités hongroises de M. A...et Mme C...étaient accompagnés de mesures les assignant à résidence, les conclusions présentées par les intéressés sur le fondement de l'article L. 521-2 du code de justice administrative étaient irrecevables ; qu'il s'ensuit que le ministre de l'intérieur est fondé à soutenir que c'est à tort que le juge des référés du tribunal administratif de Toulouse a fait droit à leur demande en suspendant l'exécution des arrêtés du 12 décembre 2014 ordonnant la remise M. A...et Mme C...aux autorités hongroises ; <br/>
<br/>
              5. Considérant, toutefois, que, dès lors que les mesures d'assignation à résidence de M. A...et Mme C...ne sont plus susceptibles, à la date de la présente ordonnance, de recevoir exécution, ainsi qu'il a été dit au point 2, il appartient au juge des référés du Conseil d'Etat, saisi par l'effet dévolutif de l'appel, de statuer sur les conclusions, désormais recevables, présentées sur le fondement de l'article L. 521-2 du code de justice administrative et tendant à la suspension de l'exécution des arrêtés du 12 décembre 2014 par lesquels le préfet de l'Aveyron a ordonné la remise aux autorités hongroises de M. A...et Mme C... ; qu'en revanche, les conclusions présentées pour la première fois en appel, sur le même fondement, et tendant à la suspension de l'exécution des arrêtés du préfet de la Haute-Garonne en date du 12 novembre 2014 sont irrecevables et doivent par suite être rejetées ;<br/>
<br/>
              6. Considérant que le règlement (UE) n° 603/2013 du 26 juin 2013 pose en principe dans le paragraphe 1 de son article 3 qu'une demande d'asile est examinée par un seul Etat membre ; que cet Etat est déterminé par application des critères fixés par son chapitre III, dans l'ordre énoncé par ce chapitre ; que selon le même règlement, l'application des critères d'examen des demandes d'asile est écartée en cas de mise en oeuvre, soit de la clause dérogatoire énoncée au paragraphe 1 de l'article 17 du règlement, qui procède d'une décision prise unilatéralement par un Etat membre, soit de la clause humanitaire définie par le paragraphe 2 de ce même article 17 du règlement ; que le paragraphe 2 de cet article prévoit en effet qu'un Etat membre peut, même s'il n'est pas responsable en application des critères fixés par le règlement, " rapprocher tout parent pour des raisons humanitaires fondées notamment sur des motifs familiaux ou culturels " ; que la mise en oeuvre par les autorités françaises de l'article 17 doit être assurée à la lumière des exigences définies par le second alinéa de l'article 53-1 de la Constitution, aux termes duquel : " les autorités de la République ont toujours le droit de donner asile à tout étranger persécuté en raison de son action en faveur de la liberté ou qui sollicite la protection de la France pour un autre motif " ;<br/>
<br/>
              7. Considérant qu'il ressort tant des pièces produites devant le juge des référés de première instance que des pièces produites et des débats menés au cours de l'audience que, depuis leur arrivée en France, les requérants et leurs cinq enfants sont hébergés et intégralement pris en charge par le frère de M.A..., de nationalité française ; que les cinq filles du couple sont toutes mineures, la benjamine étant âgée de deux ans et l'aînée de quatorze ans ; que cette dernière souffre de troubles psychologiques qui nécessite un suivi thérapeutique ; qu'il ressort de nombreuses attestations que depuis leur scolarisation en France en septembre 2014, les quatre filles les plus âgées ont manifesté de réelles capacités d'intégration et obtenu d'excellents résultats scolaires ; que, dans ces circonstances particulières, le refus des autorités françaises de faire usage de la faculté d'examiner la demande d'asile des intéressés alors que cet examen relève normalement de la compétence de la Hongrie, méconnaît de façon manifeste le droit constitutionnel d'asile ; qu'il résulte de ce qui précède que le ministre de l'intérieur, qui ne conteste pas l'existence d'une situation d'urgence, n'est pas fondé à se plaindre de ce que, par l'ordonnance du 22 décembre 2014, le juge des référés du tribunal administratif de Toulouse a ordonné la suspension de l'exécution de ces deux arrêtés et a enjoint à l'autorité préfectorale de réexaminer la situation des intéressés dans un délai de quinze jours ;<br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de condamner l'Etat à verser à M. A...et Mme C...une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'intervention de la CIMADE est admise.<br/>
<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions portant sur les arrêtés du 12  décembre 2014 assignant à résidence M. A...et MmeC....<br/>
Article 3 : Le recours du ministre de l'intérieur dirigé contre l'ordonnance du juge des référés du tribunal administratif de Toulouse du 22 décembre 2014 en tant qu'elle statue sur les conclusions portant sur les arrêtés du 12 décembre 2014 ordonnant la remise de M. A...et Mme C...aux autorités hongroises est rejeté.<br/>
Article 4 : Le surplus des conclusions présentées par M. A...et Mme C...est rejeté.<br/>
Article 5 : L'Etat versera à M. A...et Mme C...une somme de 2000 euros au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991.<br/>
Article 6 : La présente ordonnance sera notifiée au ministre de l'intérieur, à M. A... et Mme C... et à la Cimade. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
