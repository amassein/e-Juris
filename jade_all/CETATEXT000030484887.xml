<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030484887</ID>
<ANCIEN_ID>JG_L_2015_04_000000375712</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/48/48/CETATEXT000030484887.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 15/04/2015, 375712</TITRE>
<DATE_DEC>2015-04-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375712</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>Mme Laurence Marion</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:375712.20150415</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Paris d'annuler, d'une part, la lettre du 27 juillet 2012 par laquelle le directeur général des finances publiques lui a rappelé le terme de son affectation à l'étranger et l'a informé qu'une nouvelle affectation sur un poste non situé à l'étranger lui serait attribuée le 1er septembre 2013, ainsi que la décision implicite rejetant son recours gracieux, et, d'autre part, la décision du 5 juillet 2013 par laquelle a été prononcée sa mutation au sein de la direction spécialisée pour l'assistance publique - hôpitaux à Paris à compter du 1er septembre 2013. <br/>
<br/>
              Par un jugement n° 1219722/5-2 du 23 décembre 2013, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 24 février, 26 mai 2014 et 9 janvier 2015 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions présentées devant le tribunal administratif de Paris ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - la loi n° 84-16 du 11 janvier 1984 ; <br/>
              - le décret n° 2010 -984 du 26 août 2010 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laurence Marion, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. B...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 20 du décret du 26 août 2010 portant statut particulier du corps des agents administratifs des finances publiques : " La durée d'affectation des agents administratifs des finances publiques à l'étranger est limitée à deux ans. Cette affectation peut être renouvelée une seule fois. Une affectation à l'étranger n'est possible qu'à l'issue d'une affectation d'une durée minimale de deux ans en métropole " ;  que l'article 28 du même décret dispose : " Pour les agents administratifs des impôts et les agents d'administration du Trésor public affectés à l'étranger à la date d'entrée en vigueur du présent décret, le délai de deux ans mentionné à l'article 20 court à compter de la date d'entrée en vigueur du présent décret. " ; que selon son article 31, ce décret " entrera en vigueur le 1er septembre 2011 " ; qu'il résulte de ces dispositions que la durée de l'affectation à l'étranger des agents administratifs des finances publiques est désormais limitée à deux ans, renouvelable une fois ; qu'en vertu des dispositions combinées des articles 28 et 31, ce délai de deux ans commence à courir à compter du 1er septembre 2011 pour son application aux situations en cours ;<br/>
<br/>
              Sur le jugement attaqué en tant qu'il porte sur la lettre du 27 juillet 2012 du directeur général des finances publiques :<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que, par sa lettre du 27 juillet 2012, le directeur général des finances publiques s'est borné à informer M.B..., agent administratif principal des finances publiques affecté à la trésorerie près l'ambassade de France à Pékin depuis le 1er janvier 2008, que son affectation à l'étranger prendrait fin, en application des dispositions statutaires précitées, le 31 août 2013 et qu'il n'était pas envisagé de la renouveler au-delà de son terme normal ; qu'une telle lettre ne constitue pas une décision faisant grief et que, par suite, les conclusions à fin d'annulation pour excès de pouvoir dirigées contre elle sont irrecevables ; que ce motif d'ordre public doit être substitué aux motifs retenus par le jugement du 23 décembre 2013 dont il justifie, sur ce point, le dispositif ; que, par suite, les conclusions du pourvoi dirigées contre ce jugement en tant qu'il a rejeté les conclusions tendant à l'annulation de cette lettre doivent être rejetées ; <br/>
<br/>
              Sur le jugement attaqué en tant qu'il porte sur la décision du 5 juillet 2013 affectant M. B...à la direction spécialisée pour l'assistance publique-hôpitaux de Paris à compter du 1er septembre 2013 : <br/>
<br/>
              3. Considérant que, pour écarter le moyen tiré, par la voie de l'exception, de l'illégalité des critères dont l'application a conduit le directeur général des finances publiques à ne pas prolonger l'affectation de M. B...à l'étranger et à prononcer son affectation au sein de l'assistance publique - hôpitaux de Paris, le tribunal administratif a pu, sans commettre d'erreur de droit ni de qualification juridique, juger que les critères en cause ne revêtaient qu'un caractère indicatif ; qu'il résulte des termes mêmes du  jugement attaqué que le motif tiré de ce que ces critères ne seraient " au demeurant " pas illégaux est surabondant et que, par suite, le moyen de cassation dirigé contre ce motif est inopérant ;  <br/>
<br/>
              4. Considérant que le tribunal a jugé, d'une part, que, pour se prononcer sur la prolongation de l'affectation à l'étranger des agents concernés, aucune disposition du décret du 26 août 2010 n'interdisait à l'administration de prendre en compte d'autres éléments que leur  manière de servir, afin apprécier si l'intérêt du service justifiait le renouvellement de leur affectation et, d'autre part, qu'il ne ressortait pas des pièces du dossier que la décision de mutation aurait été prise pour des motifs étrangers à l'intérêt du service dès lors que l'administration poursuivait l'objectif de renouveler 50 % des effectifs théoriques de chaque trésorerie près les ambassades de France, afin d'assurer la continuité du service dans l'application des nouvelles dispositions aux situations en cours ; qu'en statuant ainsi, le tribunal n'a entaché son jugement ni d'erreur de droit ni de dénaturation des pièces du dossier ;<br/>
<br/>
              5. Considérant que si, aux termes de l'article 61 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat : " les autorités compétentes sont tenues de faire connaître au personnel, dès qu'elles ont lieu, les vacances de tous emplois (...) ", il est constant que M. B...n'avait pas exprimé de souhaits d'affectation, alors qu'il y avait été invité par lettre du 12 février 2013, et que la décision du 5 juillet 2013 se bornait à prononcer la mutation de l'intéressé à l'assistance publique - hôpitaux de Paris en l'invitant à se rapprocher du service des ressources humaines afin de déterminer son poste d'affectation ; que, dès lors, le tribunal n'a pas commis d'erreur de droit en écartant, pour ces motifs, le moyen tiré de l'illégalité de la décision du 5 juillet 2013, faute de publication régulière de la vacance d'emploi ; <br/>
<br/>
              6. Considérant, enfin, que, pour écarter le moyen tiré de ce que l'affectation de M. B...en métropole portait une atteinte disproportionnée à son droit au respect de sa vie privée et familiale au sens des stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, le tribunal administratif, après avoir relevé que M. B...soutenait que la décision attaquée ne tenait pas compte des circonstances que son épouse perdrait son emploi et ne pourrait procéder au rachat de trimestres manquants pour la revalorisation de sa pension de retraite et que le couple bénéficierait d'une retraite modeste ne leur permettant pas de continuer à financer les études de leur fille, en a déduit que ces circonstances n'étaient pas de nature, eu égard à l'intérêt général qui s'attache au respect des règles statutaires relatives aux durées d'affectation à l'étranger des agents administratifs des finances publiques et à l'application de ces règles statutaires aux situations en cours, à porter atteinte au respect de sa vie privée et familiale tel que garanti par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; que, ce faisant, le tribunal administratif qui, contrairement à ce que soutient M.B..., n'a pas jugé que ce moyen était inopérant et n'a pas commis d'erreur de droit en tenant compte, par un jugement suffisamment motivé, de la situation personnelle de l'intéressé, n'a pas commis d'erreur de qualification juridique ; qu'il n'a pas non plus dénaturé les pièces du dossier ni commis d'erreur de droit en écartant le moyen tiré de l'erreur manifeste d'appréciation de la situation familiale de M. B...au regard des dispositions du quatrième alinéa de l'article 60 de la loi du 11 janvier 1984 en vertu duquel " dans toute la mesure compatible avec le bon fonctionnement du service, les affectations prononcées doivent tenir compte des demandes formulées par les intéressés et de leur situation de famille " ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que le pourvoi de M. B...doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B...est rejeté. <br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et au ministre des finances et des comptes publics. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-05-01-01 FONCTIONNAIRES ET AGENTS PUBLICS. POSITIONS. AFFECTATION ET MUTATION. AFFECTATION. - LETTRE INFORMANT UN AGENT DE CE QU'IL N'EST PAS ENVISAGÉ DE RENOUVELER SON AFFECTATION - DÉCISION FAISANT GRIEF - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-01-02 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES NE CONSTITUANT PAS DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - LETTRE INFORMANT UN AGENT DE CE QU'IL N'EST PAS ENVISAGÉ DE RENOUVELER SON AFFECTATION.
</SCT>
<ANA ID="9A"> 36-05-01-01 Dispositions statutaires limitant à deux ans renouvelables la durée d'affectation à l'étranger des agents administratifs des finances publiques (art. 20 du décret n°  2010-984 du 26 août 2010). La lettre par laquelle l'administration informe un agent, environ un an à l'avance, de la date à laquelle son affectation à l'étranger prendra fin, en application des dispositions statutaires applicables, et de ce qu'il n'est pas envisagé de la renouveler ne constitue pas une décision faisant grief et n'est pas susceptible d'un recours pour excès de pouvoir.</ANA>
<ANA ID="9B"> 54-01-01-02 Dispositions statutaires limitant à deux ans renouvelables la durée d'affectation à l'étranger des agents administratifs des finances publiques (art. 20 du décret n°  2010-984 du 26 août 2010). La lettre par laquelle l'administration informe un agent, environ un an à l'avance, de la date à laquelle son affectation à l'étranger prendra fin, en application des dispositions statutaires applicables, et de ce qu'il n'est pas envisagé de la renouveler ne constitue pas une décision faisant grief et n'est pas susceptible d'un recours pour excès de pouvoir.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
