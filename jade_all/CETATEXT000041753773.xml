<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041753773</ID>
<ANCIEN_ID>JG_L_2020_03_000000410930</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/75/37/CETATEXT000041753773.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 20/03/2020, 410930, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-03-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410930</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>Mme Cécile Viton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:410930.20200320</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée Ponthieu Rabelais a demandé au tribunal administratif de Paris de prononcer la décharge de la contribution sociale à l'impôt sur les sociétés à laquelle elle a été assujettie au titre des exercices clos en 2008, 2009 et 2010. Par un jugement n° 1404219 du 26 juin 2015, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15PA03334 du 28 mars 2017, la cour administrative d'appel de Paris a rejeté l'appel formé par la société Ponthieu Rabelais contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique enregistrés les 29 mai et 28 août 2017 et le 9 octobre 2018 au secrétariat du contentieux du Conseil d'Etat, la société Ponthieu Rabelais demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Viton, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la société Ponthieu Rabelais ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Ponthieu Rabelais a acquitté la contribution sociale à l'impôt sur les sociétés au titre des exercices clos en 2008, 2009 et 2010. Par une décision du 17 janvier 2014, l'administration fiscale a rejeté sa demande de remboursement de ces impositions au motif que la condition tenant à la détention du capital social, prévue par les dispositions de l'article 235 ter ZC du code général des impôts, n'était pas remplie dès lors que les actions placées dans le trust 1988-R de droit américain ne pouvaient être regardées comme détenues par des personnes physiques. La société se pourvoit en cassation contre l'arrêt du 28 mars 2017 par lequel la cour administrative d'appel de Paris a rejeté l'appel qu'elle avait formé contre le jugement du tribunal administratif de Paris du 26 juin 2015 ayant rejeté sa demande tendant à la décharge de ces impositions.<br/>
<br/>
              2. Aux termes de l'article 235 ter ZC du code général des impôts, dans sa rédaction applicable aux années d'imposition en litige : " I.-Les redevables de l'impôt sur les sociétés sont assujettis à une contribution sociale égale à une fraction de cet impôt calculé sur leurs résultats imposables aux taux mentionnés aux I et IV de l'article 219 et diminué d'un abattement qui ne peut excéder 763 000 euros par période de douze mois (...). / Sont exonérés les redevables ayant réalisé un chiffre d'affaires de moins de 7 630 000 euros (...). Le capital des sociétés, entièrement libéré, doit être détenu de manière continue, pour 75 % au moins, par des personnes physiques ou par une société répondant aux mêmes conditions dont le capital est détenu, pour 75 % au moins, par des personnes physiques ". En réservant le bénéfice de l'exonération de cette imposition aux sociétés qui sont détenues de manière continue, à 75 % au moins, par des personnes physiques ou par des sociétés elles-mêmes directement détenues, dans la même proportion, par des personnes physiques, le législateur a entendu restreindre le champ des petites et moyennes entreprises éligibles à celles détenues de manière prépondérante et suffisamment directe par des personnes physiques.<br/>
<br/>
              3. Par ailleurs, ainsi que l'a défini le 1 du I de l'article 792-0 bis du code général des impôts, postérieurement aux années d'imposition en litige, on entend par trust l'ensemble des relations juridiques créées dans le droit d'un Etat autre que la France par une personne qui a la qualité de constituant, par acte entre vifs ou à cause de mort, en vue d'y placer des biens ou droits, sous le contrôle d'un administrateur, dans l'intérêt d'un ou de plusieurs bénéficiaires ou pour la réalisation d'un objectif déterminé.<br/>
<br/>
              4. Pour juger que la société Ponthieu Rabelais ne remplissait pas la condition de détention de son capital social par des personnes physiques, la cour administrative d'appel, après avoir relevé qu'il ressortait de l'instruction que les pouvoirs de l'administrateur, ou trustee, personne physique, sont limités par la propriété de jouissance dont disposent les bénéficiaires du trust, par les stipulations de l'acte constitutif du trust et par la circonstance que les biens mis en trust sont distincts de son patrimoine personnel, a retenu que les pouvoirs attachés à la qualité d'associé ne sont exercés ni par le trustee, ni par les bénéficiaires du trust, mais par le trust lui-même qui n'est ni une personne physique, ni une société dont le capital est détenu, pour 75 % au moins, par des personnes physiques. En jugeant ainsi que le trust était détenteur des actions de la société Ponthieu Rabelais alors qu'il ne s'agit que d'un ensemble de relations juridiques dépourvu en l'espèce de la personnalité juridique, la cour administrative d'appel a commis une erreur de droit. Par suite, et sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, la société Ponthieu Rabelais est fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              6. Il résulte de l'instruction que par acte du 30 décembre 1988, M. C... D..., résident américain, a constitué le trust 1988-R de droit américain afin d'y placer des biens ou droits, sous le contrôle de MM. B... et A..., trustees, dans l'intérêt de bénéficiaires, à savoir le constituant lui-même ou, en cas de décès de ce dernier, des membres de sa famille. A la suite du décès de M. A... le 21 mars 2006, M. B... est demeuré seul trustee. C'est en cette qualité, en non à titre personnel, que M. B... a exercé les droits de vote au sein de la société Ponthieu Rabelais de sorte qu'il convient de d'apprécier, au regard des termes de l'acte constitutif du trust, si ce dernier peut être considéré comme détenteur des actions de cette société.<br/>
<br/>
              7. Il résulte de l'instruction qu'aux termes de l'acte constitutif du trust 1988-R, le trustee devait agir pour les usages et les fins énoncés dans l'acte constitutif, et plus particulièrement dans l'intérêt des bénéficiaires afin de leur affecter le revenu net du patrimoine du trust et le capital correspondant. A cette fin, il détenait, en sa qualité de trustee, le patrimoine du trust, le gérait, investissait et réinvestissait les revenus et affectait le revenu net et le capital correspondant. Il participait aux assemblées générales de la société Ponthieu Rabelais et y exerçait son droit de vote. Il disposait d'un pouvoir discrétionnaire pour payer les dividendes aux bénéficiaires du trust ou les mettre en réserve.<br/>
<br/>
              8. Il résulte des stipulations de l'acte constitutif du trust en cause que les droits dont disposaient en l'espèce le trustee, personne physique, dans l'intérêt des bénéficiaires, également personnes physiques, permettaient de regarder comme remplie la condition tenant à la détention du capital posée par les dispositions de l'article 235 ter ZC du code général des impôts. Par suite et dans la mesure où seule la condition tenant à la détention du capital était contestée par l'administration fiscale, la société Ponthieu Rabelais est fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Paris a rejeté sa demande tendant à la décharge de la contribution sociale à l'impôt sur les sociétés à laquelle elle a été assujettie au titre des exercices clos en 2008, 2009 et 2010.<br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative, pour l'ensemble de la procédure.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 28 mars 2017 et le jugement du tribunal administratif de Paris du 26 juin 2015 sont annulés.<br/>
Article 2 : La société Ponthieu Rabelais est déchargée de la contribution sociale à l'impôt sur les sociétés à laquelle elle a été assujettie au titre des exercices clos en 2008, 2009 et 2010.<br/>
Article 3 : L'Etat versera à la société Ponthieu Rabelais la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société par actions simplifiée Ponthieu Rabelais et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
