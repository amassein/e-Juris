<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029998416</ID>
<ANCIEN_ID>JG_L_2014_12_000000368633</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/99/84/CETATEXT000029998416.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 30/12/2014, 368633, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368633</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Marie Grosset</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:368633.20141230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
		Procédure contentieuse antérieure<br/>
<br/>
              Mme C...D...veuve A...a demandé au tribunal administratif de Limoges d'annuler l'arrêté du 25 juillet 1994 lui accordant une pension d'ayant cause à la suite du décès de son époux, M. B...A..., en tant que cette décision ne prend pas en compte la bonification pour enfants mentionnée au b) de l'article L. 12 du code des pensions civiles et militaires de retraite, et d'enjoindre au ministre de procéder à la revalorisation de sa pension.<br/>
<br/>
              Par un jugement n° 1201538 du 14 mars 2013, le tribunal administratif de Limoges a, d'une part, annulé l'arrêté du 25 juillet 1994 en tant qu'il ne prend pas en compte la bonification pour enfants et, d'autre part, enjoint au ministre de l'économie et des finances de modifier, dans un délai de deux mois à compter de la notification du jugement, les conditions dans lesquelles la pension de Mme A...lui a été concédée et de revaloriser rétroactivement cette pension.<br/>
<br/>
		Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi, enregistré le 17 mai 2013 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'économie et des finances demande au Conseil d'Etat d'annuler ce jugement du tribunal administratif de Limoges du 14 mars 2013.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier.<br/>
<br/>
              Vu :<br/>
              - le code des pensions civiles et militaires ; <br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Grosset, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B...A..., alors sous-brigadier de police, a été admis à faire valoir ses droits à la retraite par un arrêté du 28 février 1994. A la suite de son décès survenu le 14 juin 1994, son épouse, Mme C...A..., a obtenu, par un arrêté du 25 juillet 1994, le bénéfice d'une pension de réversion avec effet au 1er juillet 1994. <br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 1 du code des pensions civiles et militaires de retraite : " La pension est une allocation pécuniaire personnelle et viagère accordée aux fonctionnaires civils et militaires et, après leur décès, à leurs ayants cause désignés par la loi, en rémunération des services qu'ils ont accomplis jusqu'à la cessation régulière de leurs fonctions  ". En vertu de l'article L. 38 du même code : " Les conjoints d'un fonctionnaire civil ont droit à une pension de réversion égale à 50 % de la pension obtenue par le fonctionnaire ou qu'il aurait pu obtenir au jour de son décès (...) ". L'article L. 55 du même code, dans sa rédaction applicable au litige, dispose que : " La pension et la rente viagère d'invalidité sont définitivement acquises et ne peuvent être révisées ou supprimées à l'initiative de l'administration ou sur demande de l'intéressé que dans les conditions suivantes : / A tout moment en cas d'erreur matérielle ; / Dans un délai d'un an à compter de la notification de la décision de concession initiale de la pension ou de la rente viagère, en cas d'erreur de droit (...) ". <br/>
<br/>
              3. Il résulte de ces dispositions que le caractère personnel d'une pension de retraite ne s'oppose pas à ce que le titulaire d'une pension de réversion se prévale, à l'appui d'un recours contre cette pension ou d'une demande de révision, d'une illégalité entachant le calcul de la pension de son conjoint que celui-ci n'a pas contestée, lorsque celle-ci ne peut être regardée comme définitive, en raison soit de ce qu'elle a été notifiée sans mention des voies et délais de recours, soit de ce qu'une demande de révision pouvait encore, à la date du décès du conjoint, être adressée à l'administration, dans les conditions prévues par l'article L. 55 du code des pensions civiles et militaires de retraite. <br/>
<br/>
              4. Il résulte des énonciations du jugement attaqué que le décès de M. A...est intervenu moins d'un an à compter de la notification de la décision de concession de sa pension, à une date à laquelle une demande de révision pouvait encore être adressée à l'administration, dans les conditions prévues par l'article L. 55 du code des pensions civiles et militaires de retraite. Par suite, le tribunal n'a pas commis d'erreur de droit en jugeant que Mme A...pouvait se prévaloir de ce que la pension concédée à son époux aurait dû  tenir compte de la bonification pour enfants mentionnée au b) de l'article L. 12 du même code, sans qu'y fasse obstacle la circonstance que celui-ci ne l'avait pas contestée de son vivant.<br/>
<br/>
              5. Il résulte de ce qui précède que le ministre de l'économie et des finances n'est pas fondé à demander l'annulation du jugement qu'il attaque. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre de l'économie et des finances est rejeté.<br/>
Article 2 : La présente décision sera notifiée au ministre des finances et des comptes publics et à Mme C...D...veuveA....<br/>
Copie en sera adressée au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
