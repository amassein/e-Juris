<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041808389</ID>
<ANCIEN_ID>JG_L_2020_04_000000440029</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/80/83/CETATEXT000041808389.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 15/04/2020, 440029, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-04-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440029</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:440029.20200415</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 9 avril 2020 au secrétariat du contentieux du Conseil d'Etat, M. C... G..., M. F... E..., M. K... A..., M. J... I..., Mme B... N..., M. D... L... et M. H... M... demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution des dispositions du II de l'article 12-3 du décret n° 2020-293 du 23 mars 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire, dans sa rédaction issue du décret n° 2020-360 du 28 mars 2020 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
              - ils justifient d'un intérêt pour agir contre les dispositions contestées ;<br/>
              - la condition d'urgence est remplie dès lors que les dispositions contestées portent une atteinte grave et immédiate, d'une part, aux intérêts des personnes susceptibles de se voir administrer la spécialité Rivotril dans des conditions non prévues par l'autorisation de mise sur le marché, dans le cadre de soin palliatifs, ainsi qu'aux intérêts de leurs proches qui ne sont pas associés à cette décision et, d'autre part, à la santé publique au regard notamment de leurs conséquences pour les personnes résidant en établissement hébergeant des personnes âgées dépendantes (EHPAD) ou en situation de détresse respiratoire à leur domicile ;<br/>
              - il existe un doute sérieux quant à la légalité des dispositions contestées ;<br/>
              - elles sont entachées d'incompétence en ce que, d'une part, le Rivotril ne peut être regardé comme un médicament approprié pour l'éradication de la catastrophe sanitaire au sens de l'article L. 3131-15 du code de la santé publique et, d'autre part, les dispositions de cet article ne permettent pas au Premier ministre de déroger aux dispositions de l'article L. 5121-12-1 du code de la santé publique ;<br/>
              - à titre subsidiaire, elles sont entachées " d'incompétence négative " en ce qu'elles renvoient à la société française d'accompagnement et de soins palliatifs le soin de déterminer les protocoles exceptionnels et transitoires auxquels les médecins prescrivant le Rivotril doivent se conformer dans son utilisation par dérogation aux conditions de son autorisation de mise sur le marché ;<br/>
              - elles méconnaissent l'article L. 3131-15 du code de la santé publique en ce qu'elles autorisent la prescription du Rivotril, non seulement pour des patients atteints par le covid-19, mais aussi pour des patients qui sont simplement susceptibles d'être atteints par cette pathologie ;<br/>
              - elles méconnaissent l'article L. 3131-15 du code de la santé publique en ce qu'elles ne sont pas strictement proportionnées aux risques sanitaires liés à l'éradication du covid-19 ni appropriées aux circonstances de temps et de lieu dès lors que, d'une part, elles méconnaissent le principe d'un égal accès aux soins et le droit reconnu à chacun de bénéficier des soins les plus appropriés et, d'autre part, elles portent atteinte aux exigences relatives au recueil du consentement du patient et à l'impératif de collégialité en ce qui concerne les décisions relatives aux malades en fin de vie.<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la Constitution ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - le décret n° 2020-293 du 23 mars 2020 ;<br/>
              - le décret n° 2020-360 du 28 mars 2020 ;<br/>
              - le décret n° 2020-423 du 14 avril 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
Considérant ce qui suit :<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Aux termes de l'article L. 522-3 du même code : " Lorsque la demande ne présente pas un caractère d'urgence ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable, ou qu'elle est mal fondée, le juge des référés peut la rejeter par une ordonnance motivée, sans qu'il y ait lieu d'appliquer les deux premiers alinéas de l'article L. 522-1 ".<br/>
<br/>
              Sur les dispositions applicables : <br/>
<br/>
              2. D'une part, aux termes de l'article L. 5121-12-1 du code de la santé publique : " I.- Une spécialité pharmaceutique peut faire l'objet d'une prescription non conforme à son autorisation de mise sur le marché en l'absence de spécialité de même principe actif, de même dosage et de même forme pharmaceutique disposant d'une autorisation de mise sur le marché ou d'une autorisation temporaire d'utilisation dans l'indication ou les conditions d'utilisation considérées, sous réserve qu'une recommandation temporaire d'utilisation établie par l'Agence nationale de sécurité du médicament et des produits de santé sécurise l'utilisation de cette spécialité dans cette indication ou ces conditions d'utilisation. Lorsqu'une telle recommandation temporaire d'utilisation a été établie, la spécialité peut faire l'objet d'une prescription dans l'indication ou les conditions d'utilisations correspondantes dès lors que le prescripteur juge qu'elle répond aux besoins du patient. (...) / En l'absence de recommandation temporaire d'utilisation dans l'indication ou les conditions d'utilisation considérées, une spécialité pharmaceutique ne peut faire l'objet d'une prescription non conforme à son autorisation de mise sur le marché qu'en l'absence d'alternative médicamenteuse appropriée disposant d'une autorisation de mise sur le marché ou d'une autorisation temporaire d'utilisation et sous réserve que le prescripteur juge indispensable, au regard des données acquises de la science, le recours à cette spécialité pour améliorer ou stabiliser l'état clinique de son patient. / (...) / ".<br/>
<br/>
              3. D'autre part, aux termes de l'article L. 3131-15 du code de la santé publique, applicable, en vertu de l'article 4 de cette loi, pendant une durée de deux mois à compter de l'entrée en vigueur de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 : " Dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le Premier ministre peut, par décret réglementaire pris sur le rapport du ministre chargé de la santé, aux seules fins de garantir la santé publique : / (...) / 9° En tant que de besoin, prendre toute mesure permettant la mise à la disposition des patients de médicaments appropriés pour l'éradication de la catastrophe sanitaire (...). / Les mesures prescrites en application des 1° à 10° du présent article sont strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu. Il y est mis fin sans délai lorsqu'elles ne sont plus nécessaires. (...) ". <br/>
<br/>
              Sur les circonstances et les mesures prises par le Premier ministre :<br/>
<br/>
              4. L'émergence d'un nouveau coronavirus (covid-19), de caractère pathogène et particulièrement contagieux, et sa propagation sur le territoire français ont conduit le ministre des solidarités et de la santé à prendre, par plusieurs arrêtés à compter du 4 mars 2020, des mesures sur le fondement des dispositions de l'article L. 3131-1 du code de la santé publique. En particulier, par un arrêté du 14 mars 2020, un grand nombre d'établissements recevant du public ont été fermés au public, les rassemblements de plus de 100 personnes ont été interdits et l'accueil des enfants dans les établissements les recevant et des élèves et étudiants dans les établissements scolaires et universitaires a été suspendu. Puis, par un décret du 16 mars 2020 motivé par les circonstances exceptionnelles découlant de l'épidémie de covid-19, modifié par décret du 19 mars, le Premier ministre a interdit le déplacement de toute personne hors de son domicile, sous réserve d'exceptions limitativement énumérées et devant être dûment justifiées, à compter du 17 mars à 12h, sans préjudice de mesures plus strictes susceptibles d'être ordonnées par le représentant de l'Etat dans le département. Le ministre des solidarités et de la santé a pris des mesures complémentaires par des arrêtés des 17, 19, 20, 21 mars 2020. <br/>
<br/>
              5. Par l'article 4 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19, a été déclaré l'état d'urgence sanitaire pour une durée de deux mois sur l'ensemble du territoire national. Par un nouveau décret du 23 mars 2020 pris sur le fondement de l'article L. 3131-15 du code de la santé publique issu de la loi du 23 mars 2020, le Premier ministre a réitéré les mesures qu'il avait précédemment ordonnées tout en leur apportant des précisions ou restrictions complémentaires. Leurs effets ont été prolongés en dernier lieu par décret du 14 avril 2020.<br/>
<br/>
              6. Le clonazepam est une molécule de la classe des benzodiazépines, dont les propriétés sont notamment anxiolytiques et sédatives, commercialisée par le laboratoire Roche sous le nom de marque de Rivotril en vertu, pour la spécialité pharmaceutique correspondant à sa forme injectable, d'une autorisation de mise sur le marché délivrée le 21 février 1995, avec pour indication thérapeutique le traitement de l'épilepsie. En l'absence de toute recommandation temporaire d'utilisation prise en application de l'article L. 5121-12-1 du code de la santé publique, cette spécialité ne peut être prescrite pour une autre indication qu'à la condition que le prescripteur juge indispensable, au regard des données acquises de la science, le recours à cette spécialité pour améliorer ou stabiliser l'état clinique de son patient et en l'absence d'alternative médicamenteuse appropriée disposant d'une autorisation de mise sur le marché ou d'une autorisation temporaire d'utilisation. Toutefois, par le II de l'article 12-3 du décret du 23 mars 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire, inséré par le décret du 28 mars 2020 visé ci-dessus, dont les effets ont été prolongés par décret du 14 avril 2020, le Premier ministre a, sur le fondement des dispositions citées au point 3, par dérogation à l'article L. 5121-12-1 du code de la santé publique, défini les conditions dans lesquelles cette spécialité peut, jusqu'au 11 mai 2020, faire l'objet d'une prescription non conforme à son autorisation de mise sur le marché en vue de la prise en charge des patients atteints ou susceptibles d'être atteints par le virus SARS-CoV-2 dont l'état clinique le justifie. Il a notamment prévu que le médecin qui prescrit cette spécialité dans ce cadre se conforme aux protocoles exceptionnels et transitoires relatifs, d'une part, à la prise en charge de la dyspnée et, d'autre part, à la prise en charge palliative de la détresse respiratoire, établis par la société française d'accompagnement et de soins palliatifs.<br/>
<br/>
              Sur la demande en référé :<br/>
<br/>
              7. Les requérants demandent au juge des référés du Conseil d'Etat, sur le fondement de l'article L. 521-1 du code de justice administrative, d'ordonner la suspension de l'exécution des dispositions du II de l'article 12-3 du décret du 23 mars 2020.<br/>
<br/>
              8. En premier lieu, ainsi qu'il a été dit au point 6, les dispositions attaquées ont pour objet de permettre, dans des conditions qu'elles définissent, la prescription du clonazépam injectable en vue de la prise en charge des patients atteints ou susceptibles d'être atteints par le virus SARS-CoV-2 dont l'état clinique le justifie. Une telle mesure, sécurisant la prescription d'une spécialité dans une indication non conforme à son autorisation de mise sur le marché, est en principe régie par les dispositions de l'article L. 5121-12-1 du code de la santé publique. Dès lors toutefois qu'elle se rapporte en l'espèce, contrairement à ce que soutiennent les requérants, à la mise à la disposition des patients de médicaments appropriés pour l'éradication de la catastrophe sanitaire, elle est au nombre de celles que le Premier ministre est compétent pour prendre en vertu du 9° de l'article L. 3131-15 du code de la santé publique pendant la durée d'application de cet article. En outre, le Premier ministre ne peut être regardé comme ayant subdélégué sa compétence en se référant, s'agissant des conditions auxquelles il soumet une telle prescription, aux protocoles exceptionnels et transitoires établis par la société française d'accompagnement et de soins palliatifs et mis en ligne sur son site, dont il n'apparaît pas qu'ils seraient postérieurs à la date des dispositions attaquées.<br/>
<br/>
<br/>
              9. En deuxième lieu, en régissant la prise en charge des patients non seulement " atteints " par le virus SARS-CoV2, mais aussi de ceux " susceptibles d'être atteints " par ce virus, les dispositions litigieuses ont pour seul objet et ne peuvent avoir d'autre effet que de ne pas exclure ceux d'entre eux dont un médecin aurait diagnostiqué l'état sans pour autant qu'ils aient été soumis à un test validant ce diagnostic. Elles ne peuvent ainsi être regardées comme s'étendant à des patients qui ne souffriraient pas de cette maladie.<br/>
<br/>
<br/>
              10. En troisième lieu, les dispositions attaquées imposent à tout médecin, lorsqu'il prescrit le clonazépam injectable en vue de la prise en charge des patients atteints ou susceptibles d'être atteints par le virus SARS-CoV-2 dont l'état clinique le justifie, de se conformer aux protocoles exceptionnels et transitoires relatifs à la prise en charge de la dyspnée et à la prise en charge palliative de la détresse respiratoire établis par la société française d'accompagnement et de soins palliatifs. Il résulte de ces protocoles qu'ils sont destinés à assurer l'apaisement de la souffrance, y compris en dehors d'une hospitalisation dans un service de réanimation et en l'absence d'accès au midazolam, benzodiazépine normalement préconisée, chez des patients, soit pour lesquels une décision collégiale de limitation de traitements actifs a été prise, soit qui, présentant une forme grave de la maladie, se trouvent en situation de détresse respiratoire asphyxique. Les posologies que ces protocoles précisent, conformes à celles recommandées par la Haute autorité de santé, sont celles, à ajuster par chaque médecin en fonction de la situation de son patient, adaptées pour ces malades en vue du seul soulagement de leur souffrance, s'agissant d'une spécialité contre-indiquée en cas d'insuffisance respiratoire grave lorsqu'elle est utilisée dans l'indication de son autorisation de mise sur le marché. Les dispositions attaquées sont ainsi, contrairement à ce que soutiennent les requérants, de nature à assurer à toute personne, alors même qu'elle ne serait pas hospitalisée, le respect de son droit à recevoir, sous réserve de son consentement libre et éclairé, les traitements et les soins appropriés à son état de santé, tels qu'appréciés par le médecin, notamment s'agissant de l'apaisement de la souffrance créée par une situation de dyspnée ou de détresse respiratoire ainsi, en outre, que du droit reconnu par l'article L. 1110-9 du code de la santé publique à toute personne malade dont l'état le requiert d'accéder à des soins palliatifs et à un accompagnement. Elles n'ont ni pour objet ni pour effet de déroger aux articles L. 1110-5 et suivants du code de la santé publique qui prévoient, notamment, une procédure collégiale lors d'un arrêt des traitements et de la mise en oeuvre d'une sédation profonde et continue maintenue jusqu'au décès, ou aux articles L. 1111-12, R. 4127-37-2 et R. 4127-37-3 de ce code faisant obligation au médecin de s'enquérir, avant la mise en oeuvre d'une telle sédation, de la volonté du patient hors d'état de l'exprimer, dont elles ne dispensent pas le cas échéant du respect. <br/>
<br/>
<br/>
              11. Il résulte de tout ce qui précède que les moyens soulevés par les requérants, tirés de ce que le Premier ministre aurait, par les dispositions attaquées, méconnu l'étendue de la compétence que lui attribue le 9° de l'article L. 3131-15 du code de la santé publique et pris une mesure contraire aux exigences de stricte proportionnalité et d'adéquation aux circonstances posées par cet article en ce qu'elle porterait atteinte à l'égal accès aux soins et au droit reconnu à chacun de bénéficier des soins les plus appropriés ainsi qu'aux exigences de recueil du consentement du patient et de collégialité attachées aux décisions relatives aux malades en fin de vie, ne sont manifestement pas propres à créer un doute sérieux quant à la légalité de ces dispositions. Par suite, sans qu'il soit besoin de se prononcer sur la recevabilité de la requête ni sur la condition d'urgence, leur requête doit être rejetée par application des dispositions de l'article L. 522-3 du code de justice administrative, y compris les conclusions qu'ils présentent au titre de l'article L. 761-1 de ce code.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. G... et autres est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. C... G..., premier requérant dénommé.<br/>
Copie en sera adressée au Premier ministre et au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
