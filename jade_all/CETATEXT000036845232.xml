<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036845232</ID>
<ANCIEN_ID>JG_L_2018_04_000000408373</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/84/52/CETATEXT000036845232.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 26/04/2018, 408373</TITRE>
<DATE_DEC>2018-04-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408373</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Marc Lambron</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:408373.20180426</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Paris de condamner l'Etat à lui verser la somme de 5 000 euros, augmentée des intérêts au taux légal, en réparation des préjudices résultant de son absence de logement et d'enjoindre au préfet de la région Ile-de-France de le reloger à compter de la notification du jugement, sous astreinte de 100 euros par jour de retard. Par un jugement n° 1511903 du 4 novembre 2016, le tribunal administratif a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 27 février 2017 et 29 mai 2017 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 au profit de la SCP Garreau, Bauer-Violas, Feschotte-Desbois, son avocat, qui déclare renoncer en ce cas à la part contributive de l'Etat.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la construction et de l'habitation ;<br/>
<br/>
              - la loi n° 91-617 du 10 juillet 1991 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Lambron, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de M.A....<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A...a été reconnu prioritaire et devant être relogé en urgence, sur le fondement de l'article L. 411-2-3 du code de la construction et de l'habitation, par une décision du 3 janvier 2014 de la commission de médiation de Paris, au motif que sa demande de logement social, présentée en juillet 2002, n'avait pas reçu de réponse dans le délai réglementaire ; que, par un jugement du 2 décembre 2014, le tribunal administratif de Paris, saisi par M. A...sur le fondement du I de l'article L. 441-2-3-1 du code de la construction et de l'habitation, a enjoint au préfet de la région Ile-de-France, préfet de Paris, d'assurer le relogement de l'intéressé, de son épouse et de son enfant ; que, le 16 juillet 2015,  M A...a demandé au tribunal administratif de condamner l'Etat à réparer les préjudices qu'entraînait pour lui l'absence de relogement ; qu'il se pourvoit en cassation contre le jugement du 4 novembre 2016 par lequel le tribunal administratif a rejeté cette demande ;<br/>
<br/>
              2. Considérant que, lorsqu'une personne a été reconnue comme prioritaire et comme devant être logée ou relogée d'urgence par une commission de médiation, en application des dispositions de l'article L. 441-2-3 du code de la construction et de l'habitation, la carence fautive de l'Etat à exécuter cette décision dans le délai imparti engage sa responsabilité à l'égard du seul demandeur, au titre des troubles dans les conditions d'existence résultant du maintien de la situation qui a motivé la décision de la commission, que l'intéressé ait ou non fait usage du recours en injonction contre l'Etat prévu par l'article L. 441-2-3-1 du code de la construction et de l'habitation ; que ces troubles doivent être appréciés en fonction des conditions de logement qui ont perduré du fait de la carence de l'Etat, de la durée de cette carence et du nombre de personnes composant le foyer du demandeur pendant la période de responsabilité de l'Etat, qui court à compter de l'expiration du délai de trois ou six mois à compter de la décision de la commission de médiation que les dispositions de l'article  R. 441-16-1 du code de la construction et de l'habitation impartissent au préfet pour provoquer une offre de logement ; que, dans le cas où le demandeur a été reconnu prioritaire au seul motif que sa demande de logement social n'avait pas reçu de réponse dans le délai réglementaire, son maintien dans le logement où il réside ne peut être regardé comme entraînant des troubles dans ses conditions d'existence lui ouvrant droit à réparation que si ce logement est inadapté au regard notamment de ses capacités financières et de ses besoins ;<br/>
<br/>
              3. Considérant que, pour juger que M. A...ne justifiait pas avoir subi, du fait de la carence de l'Etat, des troubles lui ouvrant droit à réparation, le tribunal administratif a retenu que, par les pièces qu'il avait produites avant la clôture de l'instruction, l'intéressé n'établissait pas que le logement qu'il occupait dans le parc privé présentait, comme il l'alléguait, un caractère insalubre ; qu'il ressort toutefois des pièces du dossier qui lui était soumis que le requérant soutenait également que son logement était suroccupé, en faisant état de sa surface et de la composition de son foyer ; qu'en ne prenant pas parti sur ce point, le tribunal administratif n'a pas légalement justifié le rejet de la demande indemnitaire dont il était saisi ; qu'il y a lieu, par suite et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, d'annuler ce jugement en tant qu'il rejette cette demande ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant qu'il ressort des pièces que l'appartement où réside M. A... avec son épouse et ses deux enfants, nés respectivement en 2009 et 2016, présente des désordres qui le rendent insalubre et ont des répercussions négatives sur l'état de santé de ses occupants ; qu'il sera fait une juste appréciation des troubles résultant de cette situation depuis le 3 juin 2014, date d'expiration du délai imparti au préfet pour exécuter la décision de la commission de médiation en faisant à l'intéressé une offre de logement, soit pendant une période de près de quatre ans, en mettant à la charge de l'Etat le versement au requérant d'une indemnité de 4 000 euros, tous intérêts compris à la date de la présente décision ;<br/>
<br/>
              6. Considérant que M. A...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de M. A..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à cette société ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Paris du 4 novembre 2016 est annulé en tant qu'il rejette la demande indemnitaire de M.A....<br/>
<br/>
Article 2 : L'Etat versera à M. A...une indemnité de 4 000 euros, tous intérêts compris à la date de la présente décision.<br/>
<br/>
Article 3 : L'Etat versera à la SCP Garreau, Bauer-Violas, Feschotte-Desbois une somme de 3 000 euros en application des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. B...A...et au ministre de la cohésion des territoires.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">38-07-01 LOGEMENT. - RESPONSABILITÉ DE L'ETAT À RAISON DE LA CARENCE FAUTIVE À ASSURER LE LOGEMENT D'UN DEMANDEUR RECONNU PRIORITAIRE ET URGENT [RJ1] - CAS D'UN DEMANDEUR RECONNU PRIORITAIRE EN RAISON DE L'ABSENCE DE RÉPONSE DANS LE DÉLAI RÉGLEMENTAIRE - DROIT À RÉPARATION AU TITRE DES TROUBLES DANS LES CONDITIONS D'EXISTENCE LIÉS AU MAINTIEN DANS LE LOGEMENT DANS LEQUEL IL RÉSIDE - ABSENCE, SAUF SI LE LOGEMENT EST INADAPTÉ AU REGARD NOTAMMENT DES CAPACITÉS FINANCIÈRES ET DES BESOINS DU DEMANDEUR [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-02-012 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICES SOCIAUX. - RESPONSABILITÉ DE L'ETAT À RAISON DE LA CARENCE FAUTIVE À ASSURER LE LOGEMENT D'UN DEMANDEUR RECONNU PRIORITAIRE ET URGENT [RJ1] - CAS D'UN DEMANDEUR RECONNU PRIORITAIRE EN RAISON DE L'ABSENCE DE RÉPONSE DANS LE DÉLAI RÉGLEMENTAIRE - DROIT À RÉPARATION AU TITRE DES TROUBLES DANS LES CONDITIONS D'EXISTENCE LIÉS AU MAINTIEN DANS LE LOGEMENT DANS LEQUEL IL RÉSIDE - ABSENCE, SAUF SI LE LOGEMENT EST INADAPTÉ AU REGARD NOTAMMENT DES CAPACITÉS FINANCIÈRES ET DES BESOINS DU DEMANDEUR [RJ2].
</SCT>
<ANA ID="9A"> 38-07-01 Dans le cas où le demandeur a été reconnu prioritaire au seul motif que sa demande de logement social n'avait pas reçu de réponse dans le délai réglementaire, son maintien dans le logement où il réside ne peut être regardé comme entraînant des troubles dans ses conditions d'existence lui ouvrant droit à réparation que si ce logement est inadapté au regard notamment de ses capacités financières et de ses besoins.</ANA>
<ANA ID="9B"> 60-02-012 Dans le cas où le demandeur a été reconnu prioritaire au seul motif que sa demande de logement social n'avait pas reçu de réponse dans le délai réglementaire, son maintien dans le logement où il réside ne peut être regardé comme entraînant des troubles dans ses conditions d'existence lui ouvrant droit à réparation que si ce logement est inadapté au regard notamment de ses capacités financières et de ses besoins.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur les principes gouvernant l'indemnisation d'un demandeur reconnu prioritaire et urgent, CE, 13 juillet 2016, Mme,, n° 382872, T. p. 945 ; CE, 16 décembre 2016, M.,, n° 383111, p. 563 ; CE, 19 juillet 2017, Consorts,n° 402172, à mentionner aux Tables.,,[RJ2] Rappr. CE, 13 octobre 2017, M.,, n° 399710, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
