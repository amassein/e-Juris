<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043678897</ID>
<ANCIEN_ID>JG_L_2021_06_000000437038</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/67/88/CETATEXT000043678897.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 17/06/2021, 437038, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437038</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP BOULLOCHE</AVOCATS>
<RAPPORTEUR>Mme Christelle Thomas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:437038.20210617</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé à la Cour nationale du droit d'asile d'annuler la décision du 3 janvier 2019 par laquelle l'Office français de protection des réfugiés et apatrides a mis fin à son statut de réfugié. Par une décision n° 19007551 du 5 décembre 2019, la Cour nationale du droit d'asile a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire, deux mémoires complémentaires et un mémoire en réplique, enregistrés les 23 décembre 2019, 2 juillet 2020, 6 août 2020 et 28 avril 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Office français de protection des réfugiés et apatrides la somme de 3 000 euros à verser à la SCP Boulloche, son avocat, au titre des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New York le 31 janvier 1967 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Christelle Thomas, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Boulloche, avocat de M. C... A... et à la SCP Foussard, Froger, avocat de l'office français de protection des refugies et apatrides ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. M. A... a demandé à la Cour nationale du droit d'asile d'annuler la décision du 3 janvier 2019 par laquelle l'Office français de protection des réfugiés et apatrides a mis fin à son statut de réfugié. La Cour nationale du droit d'asile a rejeté sa demande par une décision du 5 décembre 2019, contre laquelle M. A... se pourvoit en cassation. <br/>
<br/>
              Sur la régularité de la décision :<br/>
<br/>
              2. En premier lieu, si M. A... soutient que la Cour nationale du droit d'asile aurait omis de mentionner dans sa décision que le représentant de l'Office français de protection des réfugiés et apatrides avait été entendu au cours de l'audience, les mentions figurant sur la minute de la décision font foi jusqu'à preuve du contraire. Ce moyen ne peut donc, en tout état de cause, qu'être écarté.<br/>
<br/>
              3. En deuxième lieu, aux termes de l'article R. 733-29 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Lorsque le président de la formation de jugement décide d'ordonner un supplément d'instruction, les parties sont invitées à présenter un mémoire ou des pièces complémentaires pour les seuls besoins de ce supplément d'instruction. La même formation de jugement délibère, à l'expiration du délai imparti aux parties pour produire ces éléments ou, le cas échéant, y répliquer. Ce délai ne peut excéder une durée d'un mois à compter de la date de l'audience. Les parties ne sont convoquées à une nouvelle audience que si le président de la formation de jugement estime nécessaire de les entendre présenter des observations orales sur les seuls éléments nouveaux qui auraient été produits ". Il résulte de ces dispositions que si le président de la formation de jugement de la Cour nationale du droit d'asile peut, à l'issue de l'audience publique, ordonner un supplément d'instruction, les productions des parties pour y répondre et les observations qu'elles peuvent susciter doivent intervenir dans un délai d'un mois à compter de la date de l'audience. A l'expiration de ce délai, il appartient à la formation de jugement de délibérer.<br/>
<br/>
              4. Il ressort des énonciations de la décision attaquée qu'après la clôture de l'instruction intervenue le 25 septembre 2019 et à l'issue de l'audience tenue le 30 septembre 2019, la présidente de la formation de jugement a ordonné un supplément d'instruction le 17 octobre 2019, sur le fondement de l'article R. 733-29 du code de l'entrée et du séjour des étrangers et du droit d'asile, pour recueillir les observations des parties sur le mémoire de l'Office français de protection des réfugiés et apatrides enregistré le 25 septembre 2019 et sur la note en délibéré produite le 10 octobre 2019 par M. A.... En précisant dans la décision attaquée qu'aucune observation n'a été produite en réponse à ce supplément d'instruction, la Cour a suffisamment motivé sa décision et n'a pas méconnu les règles relatives à la clôture de l'instruction. <br/>
<br/>
              5. En troisième lieu, si M. A... soutient que rien ne permet de vérifier le respect des délais prévus à l'article R. 733-29 du code de l'entrée et du séjour des étrangers et du droit d'asile cité au point 3, il ressort des pièces de la procédure suivie devant la Cour que le délai imparti pour répondre au supplément d'instruction venait à échéance le 30 octobre 2019, soit un mois après la date de l'audience. La décision ayant été rendue le 5 décembre 2019, rien ne permet de supposer que la formation de jugement aurait délibéré sans attendre l'expiration de ce délai.<br/>
<br/>
              6. En quatrième lieu, si M. A... soutient que la Cour a omis de viser deux de ses mémoires et s'il produit à l'appui de ses allégations des rapports d'émission de télécopie du 18 septembre 2019, cette production, eu égard aux conditions techniques dans lesquelles un tel document est établi, ne saurait prévaloir sur les mentions figurant au registre du greffe de la Cour. En l'absence d'élément permettant de constater l'enregistrement de ces deux mémoires au greffe, ce moyen ne peut qu'être écarté.<br/>
<br/>
              Sur le bien-fondé de la décision :<br/>
<br/>
              7. En premier lieu, M. A... n'est pas fondé à soutenir que la Cour aurait dénaturé les pièces du dossier qui lui était soumis en relevant qu'il s'était vu reconnaître la qualité de réfugié en vertu du principe de l'unité de famille.<br/>
<br/>
              8. En deuxième lieu, il ressort des énonciations de sa décision que, pour statuer sur l'application de l'article L.711-6 du code de l'entrée et du séjour des étrangers et du droit d'asile, la Cour a précisé que " si la décision de la cour d'assises de Paris du 25 février  2016 n'est pas définitive en raison de pourvois en cassation des victimes les 29 février 2016 et 3 mars 2016, il ressort néanmoins des pièces du dossier que l'intéressé a fait l'objet d'une condamnation définitive, le 14 avril 2005, par la cour d'assises du Val d'Oise ". Par suite, le moyen tiré de ce que la cour aurait commis une erreur de droit en se référant à une décision non définitive de la cour d'assises de Paris ne peut qu'être écarté. <br/>
<br/>
              9. En troisième lieu, contrairement à ce qui est allégué à l'appui du pourvoi, il ressort des énonciations de la décision attaquée que la Cour n'a pas omis de prendre en compte l'état de santé de M. A... pour apprécier la réalité et l'actualité de la menace pour la société que constituerait sa présence en France. En jugeant qu'eu égard à la gravité croissante des infractions dont il s'est rendu coupable pour des faits de violences et de viols et à l'absence d'amélioration de son comportement et de prise de conscience de la gravité de ses actes depuis ses condamnations, il représentait toujours, en dépit des soins qu'il disait suivre et de la présence en France de son fils devenu majeur, une menace grave pour la société, la Cour n'a pas inexactement qualifié les faits de l'espèce, qu'elle a relevés au terme d'une appréciation souveraine. <br/>
<br/>
              10. En dernier lieu, M. A... ne saurait contester utilement les motifs par lesquels la Cour a relevé qu'il bénéficiait toujours de la qualité de réfugié. <br/>
<br/>
              11. Il résulte de tout ce qui précède que le pourvoi de M. A... doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                                   -----------------<br/>
<br/>
Article 1er : Le pourvoi de M. A... est rejeté <br/>
Article 2 : La présente décision sera notifiée à M. B... A... et à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
