<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025833583</ID>
<ANCIEN_ID>JG_L_2012_05_000000344388</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/83/35/CETATEXT000025833583.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 09/05/2012, 344388</TITRE>
<DATE_DEC>2012-05-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>344388</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Christian Vigouroux</PRESIDENT>
<AVOCATS>SCP PEIGNOT, GARREAU, BAUER-VIOLAS ; SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Frédéric Dieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Boulouis</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:344388.20120509</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 17 novembre 2010 et 17 février 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la COMMUNE DE FLEURY D'AUDE, représentée par son maire ; la COMMUNE DE FLEURY D'AUDE demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 07MA01397 du 6 septembre 2010 de la cour administrative d'appel de Marseille en tant qu'il a, d'une part, rejeté ses conclusions tendant à la constatation de la nullité de la convention de mandat en date du 24 mai 1988 et de ses deux avenants et de la convention de cession de créance du 27 décembre 1995 et, d'autre part, ordonné une expertise avant de statuer sur son appel du jugement n° 0204820 du 29 décembre 2006 par lequel le tribunal administratif de Montpellier a rejeté sa demande tendant à l'annulation du titre de recettes émis à son encontre par le département de l'Aude le 6 août 2002 pour avoir paiement d'une somme de 2 286 735,26 euros, correspondant à la créance détenue par la société Aude Aménagement sur la commune et cédée par la société au département de l'Aude par la convention de cession de créance conclue le 27 décembre 1995 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge du département de l'Aude le versement de la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code civil ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Dieu, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Peignot, Garreau, Bauer-Violas, avocat de la COMMUNE DE FLEURY D'AUDE et de la SCP Delaporte, Briard, Trichet, avocat du département de l'Aude, <br/>
<br/>
              - les conclusions de M. Nicolas Boulouis, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Peignot, Garreau, <br/>
Bauer-Violas, avocat de la COMMUNE DE FLEURY D'AUDE et à la SCP Delaporte, Briard, Trichet, avocat du département de l'Aude ;<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis au juge du fond que, par une convention en date du 9 février 1988, reconduite le 13 mai 1988, la COMMUNE DE FLEURY D'AUDE a confié au syndicat mixte d'aménagement et de développement de la Basse Vallée de l'Aude (SMABVA) une mission de conseil pour la création d'un observatoire sous-marin ; que, par un marché signé le 31 mars 1988, la COMMUNE DE FLEURY D'AUDE a confié à la société Rougerie la maîtrise d'oeuvre de cet ouvrage puis, par convention en date du 24 mai 1988, en a délégué la maîtrise d'ouvrage à la société d'économie mixte d'équipement et d'aménagement de l'Aude, devenue société Aude Aménagement ; que, par une convention de cession de créance conclue le 27 décembre 1995, la société Aude Aménagement a cédé au département de l'Aude, pour un montant de 15 000 000 francs, soit 2 286 735,26 euros, la créance qu'elle détenait à l'encontre de la COMMUNE DE FLEURY D'AUDE à raison de la convention de mandat qui les liait ; que, le 6 août 2002, le département a émis à l'encontre de la commune un titre exécutoire en vue de recouvrer cette somme ; que, par un jugement du 29 décembre 2006, le tribunal administratif de Montpellier a rejeté la demande de la COMMUNE DE FLEURY D'AUDE tendant à l'annulation de ce titre ; que, par l'arrêt attaqué, la cour administrative d'appel de Marseille a ordonné avant dire droit une expertise en vue de déterminer le montant exact de la créance du département de l'Aude ;<br/>
<br/>
              Considérant que la COMMUNE DE FLEURY D'AUDE n'ayant pas intérêt à demander et ne demandant pas l'annulation de l'arrêt attaqué en tant qu'il a rejeté les conclusions du département de l'Aude présentées sur le fondement de l'enrichissement sans cause, est inopérant le moyen tiré de ce que, pour rejeter ces conclusions, la cour administrative d'appel de Marseille aurait dénaturé les pièces du dossier et commis une erreur de droit en estimant que les avenants à la convention de mandat du 24 mai 1988 étaient sans lien avec le titre exécutoire litigieux et que le moyen tiré de leur nullité ne pouvait qu'être écarté ;<br/>
<br/>
<br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il a rejeté les conclusions de la COMMUNE DE FLEURY D'AUDE tendant à la constatation de la nullité de la convention de mandat et de ses avenants :<br/>
<br/>
              Considérant que la cour administrative d'appel de Marseille, saisie d'un appel contre le jugement du tribunal administratif de Montpellier statuant sur la légalité d'un titre exécutoire, n'a pas commis d'erreur de droit en rejetant comme irrecevables les conclusions de la COMMUNE DE FLEURY D'AUDE tendant à ce que soit constatée la nullité de la convention de mandat du 24 mai 1988 et de ses deux avenants ainsi que la nullité de la convention de cession de créance du 27 décembre 1995, après avoir relevé que ces conclusions, lesquelles relevaient d'un litige entre parties à un contrat, étaient nouvelles en appel, et alors même qu'elles s'appuyaient sur un moyen d'ordre public tiré de la nullité de ces contrats ; que, par suite, la COMMUNE DE FLEURY D'AUDE n'est pas fondée à demander l'annulation de l'arrêt attaqué en tant qu'il a rejeté ses conclusions présentées en appel et tendant à la constatation de la nullité de la convention de mandat et de ses avenants ;<br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il a ordonné une expertise avant de statuer sur les conclusions de la COMMUNE DE FLEURY D'AUDE tendant à l'annulation du titre exécutoire :<br/>
<br/>
              Considérant, en premier lieu, que par une appréciation souveraine non susceptible d'être discutée devant le juge de cassation, la cour administrative d'appel de Marseille a relevé que les bases de liquidation de la créance pour le recouvrement de laquelle le titre litigieux avait été émis devaient être regardées comme ayant été, préalablement à la notification de ce titre à la COMMUNE DE FLEURY D'AUDE, portées à la connaissance de celle-ci ; <br/>
<br/>
              Considérant, en deuxième lieu, que la cour, qui n'était pas tenue de répondre à l'ensemble de l'argumentation de la requérante, a suffisamment motivé son arrêt en relevant que les documents annexés au titre litigieux permettaient de connaître les bases de liquidation de la créance pour le recouvrement de laquelle il avait été émis ; <br/>
<br/>
              Considérant, en troisième lieu, qu'après avoir souverainement relevé que la convention de cession de créance du 27 décembre 1995 avait été, en application des dispositions de l'article 1690 du code civil, régulièrement notifiée au comptable de la COMMUNE DE FLEURY D'AUDE le 29 novembre 1999 par exploit d'huissier, la cour administrative d'appel de Marseille n'a pas méconnu la portée de ces dispositions ni commis d'erreur de droit en jugeant que cette seule notification avait pour effet de rendre la créance opposable à la commune ;<br/>
<br/>
              Considérant toutefois, en dernier lieu, qu'en vue de déterminer le montant exact de la créance détenue par le département de l'Aude à l'encontre de la COMMUNE DE FLEURY D'AUDE, la cour administrative d'appel de Marseille, après avoir souverainement relevé, d'une part, que le département avait apporté un commencement de preuve de sa créance mais, d'autre part, que la commune soutenait avoir réglé certaines sommes directement à son mandataire et que l'état du dossier ne lui permettait pas de déterminer dans quelle mesure la créance du département présentait un caractère exigible, certain et liquide, a désigné un expert auquel elle a demandé de lui " fournir tous les éléments factuels et comptables utiles " à la détermination de cette créance ; que, s'agissant de rechercher la réalité et le montant des paiements effectués par la COMMUNE DE FLEURY D'AUDE auprès de son mandataire, la société Aude Aménagement, il appartenait à la cour, pour déterminer le montant des sommes versées par la commune qui devait venir en diminution du montant de la créance du département de l'Aude exigée par le titre exécutoire litigieux, de se faire communiquer directement par la commune les éléments et documents permettant d'établir la réalité et le montant des paiements qu'elle avait effectués auprès de son mandataire ; qu'en recourant cependant à une expertise pour en obtenir communication et procéder à l'évaluation de la créance réelle du département, la cour administrative d'appel de Marseille a méconnu son office et commis une erreur de droit ; que, par suite, la COMMUNE DE FLEURY D'AUDE est fondée à demander l'annulation de l'arrêt attaqué en tant qu'il a ordonné une expertise avant d'évaluer le bien fondé de la créance litigieuse ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que l'arrêt attaqué doit être annulé en tant seulement qu'il a ordonné une expertise, afin d'évaluer le bien fondé de la créance litigieuse, avant de statuer sur les conclusions de la COMMUNE DE FLEURY D'AUDE tendant à l'annulation du titre exécutoire ;<br/>
<br/>
              Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la COMMUNE DE FLEURY D'AUDE et le département de l'Aude en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 3 et 4 de l'arrêt de la cour administrative d'appel de Marseille du 6 septembre 2010 sont annulés.<br/>
Article 2 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Marseille.<br/>
Article 3 : Le surplus des conclusions du pourvoi de la COMMUNE DE FLEURY D'AUDE est rejeté.<br/>
Article 4 : Les conclusions présentées par la COMMUNE DE FLEURY D'AUDE et le département de l'Aude en application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la COMMUNE DE FLEURY D'AUDE et au département de l'Aude.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-04-01 PROCÉDURE. INSTRUCTION. POUVOIRS GÉNÉRAUX D'INSTRUCTION DU JUGE. - POUVOIR DU JUGE D'OBTENIR COMMUNICATION DE PIÈCES JUSTIFIANT DE LA RÉALITÉ ET DU MONTANT DE SOMMES VERSÉES PAR UNE COMMUNE À SON MANDATAIRE, À DÉDUIRE D'UNE CRÉANCE QU'ELLE DOIT - EXISTENCE - CONSÉQUENCE - MÉCONNAISSANCE DE SON OFFICE PAR LE JUGE QUI ORDONNE UNE EXPERTISE SUR CE POINT.
</SCT>
<ANA ID="9A"> 54-04-01 Pour rechercher la réalité et le montant de paiements effectués par une commune auprès de son mandataire, il appartient au juge, afin de déterminer le montant de la créance due par cette commune, de se faire communiquer directement par cette dernière les éléments et documents permettant d'établir la réalité et le montant des paiements qu'elle a effectués auprès de son mandataire. En recourant à une expertise pour en obtenir communication et procéder à l'évaluation de la créance, le juge méconnaît son office et commet une erreur de droit.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
