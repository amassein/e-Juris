<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039728726</ID>
<ANCIEN_ID>JG_L_2019_12_000000429356</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/72/87/CETATEXT000039728726.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 31/12/2019, 429356, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-12-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429356</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE CHAISEMARTIN, DOUMIC-SEILLER</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:429356.20191231</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Strasbourg d'annuler pour excès de pouvoir la décision du 23 avril 2018 par laquelle la préfète de Loire-Atlantique a rejeté sa demande d'échange de son permis de conduire serbe contre un permis de conduire français. Par un jugement n°1806628 du 8 février 2019, le tribunal administratif a annulé cette décision et enjoint à l'administration de statuer à nouveau sur la demande de M. A....<br/>
<br/>
              Par un pourvoi, enregistré le 30 mars 2019 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M. A....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la route ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - l'arrêté du 12 janvier 2012 fixant les conditions de reconnaissance et d'échange des permis de conduire délivrés par les Etats n'appartenant ni à l'Union européenne, ni à l'Espace économique européen ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Chaisemartin, Doumic-Seiller, avocat de M. A....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A..., ressortissant serbe ayant le statut de réfugié, a sollicité le 2 octobre 2017 l'échange de son permis de conduire serbe contre un permis français. Par une décision en date du 23 avril 2018, la préfète de la Loire-Atlantique a refusé cet échange au motif que, faute d'avoir été présentée dans un délai d'un an suivant l'acquisition de sa résidence normale en France, la demande de l'intéressé était tardive. Le ministre de l'intérieur se pourvoit en cassation contre le jugement du 8 février 2019 par lequel le tribunal administratif de Strasbourg a annulé cette décision.<br/>
<br/>
              2. Aux termes de l'article R. 222-3 du code de la route : " Tout permis de conduire national, en cours de validité, délivré par un Etat ni membre de la Communauté européenne, ni partie à l'accord sur l'Espace économique européen, peut être reconnu en France jusqu'à l'expiration d'un délai d'un an après l'acquisition de la résidence normale de son titulaire ". Aux termes du II de l'article 4 de l'arrêté du 12 janvier 2012 visé ci-dessus : " Pour les ressortissants étrangers non-ressortissants de l'Union européenne, la date d'acquisition de la résidence normale est celle du début de validité du premier titre de séjour ". Enfin, aux termes du II de l'article 11 du même arrêté, le délai d'un an dans lequel un étranger qui s'est vu reconnaître la qualité de réfugié peut demander l'échange de son permis de conduire court " à compter de la date de début de validité du titre de séjour provisoire ". Il résulte de ces dispositions que, pour un réfugié mis en possession d'un titre de séjour provisoire établi à la suite de la reconnaissance de sa qualité de réfugié, le point de départ du délai d'un an imparti pour demander l'échange d'un permis délivré par un Etat n'appartenant ni à l'Union européenne ni à l'Espace économique européen court à compter de la date de délivrance de ce titre. Par ailleurs, le délai d'un an prévu par ces mêmes dispositions n'est pas un délai franc.<br/>
<br/>
              3. Il ressort des termes mêmes du jugement attaqué que, pour annuler le refus opposé à la demande de M. A..., le tribunal administratif de Strasbourg, après avoir constaté que ce dernier avait été mis, en sa qualité de réfugié, en possession d'un récépissé de demande de titre de séjour valant autorisation de séjour à compter du 30 septembre 2016, s'est fondé sur ce que le 30 septembre 2017 était un samedi pour juger que la demande de M. A..., introduite le lundi 2 octobre 2017, était recevable. Il résulte de ce qui a été dit ci-dessus que le tribunal administratif a, ce faisant, commis une erreur de droit. <br/>
<br/>
              4. Ce moyen, né du jugement attaqué, n'étant, contrairement à ce que soutient en défense M. A..., pas nouveau en cassation, le ministre de l'intérieur est, par suite, fondé à demander l'annulation du jugement qu'il attaque.<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à que soit mise à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance, la somme que demande M. A... au titre de ces dispositions et de celles de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Strasbourg du 8 février 2019 est annulé.<br/>
<br/>
Article 2 : Les conclusions de M. A... présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
<br/>
		Article 3 : L'affaire est renvoyée au tribunal administratif de Strasbourg.<br/>
<br/>
Article 4 : La présente décision sera notifiée au ministre de l'intérieur et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
