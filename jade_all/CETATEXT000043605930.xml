<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043605930</ID>
<ANCIEN_ID>JG_L_2021_05_000000445020</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/60/59/CETATEXT000043605930.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 28/05/2021, 445020, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-05-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445020</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUTET-HOURDEAUX</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:445020.20210528</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'entreprise unipersonnelle à responsabilité limitée (EURL) Angel Parachutisme a demandé au tribunal administratif de Dijon de prononcer, à concurrence d'un montant de 130 639 euros, la restitution des droits de taxe sur la valeur ajoutée qu'elle a acquittés au titre de la période du 1er janvier 2014 au 31 décembre 2015, assortie des intérêts moratoires prévus à l'article L. 208 du livre des procédures fiscales. Par un jugement n° 1701922 du 31 mai 2018, ce tribunal a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 18LY02921 du 6 août 2020, la cour administrative d'appel de Lyon a rejeté l'appel formé par l'entreprise Angel Parachutisme contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 1er octobre et 30 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, l'entreprise Angel Parachutisme demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel.<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code des transports ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Boutet-Hourdeaux, avocat de l'entreprise Angel Parachutisme ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 20 mai 2021, présentée par l'entreprise Angel Parachutisme ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que l'entreprise Angel Parachutisme a, par une réclamation contentieuse, demandé la restitution de la taxe sur la valeur ajoutée qu'elle estimait avoir acquitté à tort à raison des prestations de saut en parachute en tandem fournies au titre de la période du 1er janvier 2014 au 31 décembre 2015, correspondant à la différence entre le montant de la taxe qu'elle a déclarée au taux de 20 % et celui résultant de l'application du taux réduit de 10 % prévu par le b quater de l'article 279 du code général des impôts. L'entreprise Angel Parachutisme se pourvoit en cassation contre l'arrêt du 6 août 2020 par lequel la cour administrative d'appel de Lyon a rejeté l'appel qu'elle avait formé contre le jugement du 31 mai 2018 du tribunal administratif de Dijon rejetant sa demande de restitution.<br/>
<br/>
              Sur l'application de la loi fiscale :<br/>
<br/>
              2. Aux termes de l'article 279 du code général des impôts, dans sa rédaction applicable au litige : " La taxe sur la valeur ajoutée est perçue au taux réduit de 10 % en ce qui concerne : / (...) b quater les transports de voyageurs (...) ". Par ailleurs, aux termes du premier alinéa de l'article L. 6100-1 du code des transports : " Est dénommé aéronef pour l'application du présent code, tout appareil capable de s'élever ou de circuler dans les airs ". Selon l'article L. 6400-1 du même code " Le transport aérien consiste à acheminer par aéronef d'un point d'origine à un point de destination des passagers, des marchandises ou du courrier ". <br/>
<br/>
              3. Les prestations de saut en parachute en tandem proposées dans le cadre de baptêmes de l'air consistent à transporter par aéronef un client jusqu'à une altitude de largage prédéfinie (parachutage), pour lui permettre d'effectuer ensuite un saut en parachute biplace, dirigé par un parachutiste professionnel. Si le parachute est lui-même constitutif d'un aéronef au sens des dispositions de l'article L. 6100-1 du code des transports citées au point 2, la prestation de saut en parachute en tandem, qui constitue une fin en soi, se rattache à la pratique d'un loisir sportif et ne peut être regardée comme ayant pour objet l'acheminement d'un passager d'un point d'origine à un point de destination au sens de l'article L. 6400-1 du même code. <br/>
<br/>
              4. En premier lieu, après avoir relevé, par une appréciation souveraine exempte de dénaturation, que l'accès à l'espace aérien fourni dans le cadre du parachutage avait pour seule finalité la réalisation du saut qui lui succède, la cour administrative d'appel de Lyon a pu légalement juger que la prestation de saut en parachute biplace ne pouvait, eu égard à sa nature et à ses modalités d'exécution, être scindée en plusieurs opérations distinctes ni être regardée comme constituée d'une prestation principale et d'une prestation accessoire.<br/>
<br/>
              5. En second lieu, en jugeant que la prestation de saut en parachute biplace ne constituait pas une opération de transport aérien de passagers au sens de l'article L. 6400-1 du code des transports, pour en déduire qu'elle ne relevait pas des prestations de transport de voyageurs ouvrant droit à l'application du taux réduit de taxe sur la valeur ajoutée de 10 % prévu par le b quater de l'article 279 du code général des impôts, la cour n'a ni insuffisamment motivé son arrêt, ni, compte tenu de ce qui a été dit au point 3, commis d'erreur de droit.<br/>
<br/>
              Sur l'interprétation administrative de la loi fiscale :<br/>
<br/>
              6. Pour juger que la société requérante ne pouvait se prévaloir, sur le fondement de l'article L. 80 A du livre des procédures fiscales, des énonciations du paragraphe n° 140 de l'instruction du 25 juin 2013 publiée au bulletin officiel des finances publiques sous la référence BOI-TVA-LIQ-30-20-60-20130625, reproduisant les termes d'un rescrit par lequel l'administration fiscale a admis l'application du taux réduit de taxe sur la valeur ajoutée au profit des baptêmes de l'air réalisés au moyen d'ultras légers motorisés (ULM), la cour s'est fondée, d'une part, sur ce que la contribuable n'entrait pas dans les prévisions de cette instruction qui concerne un secteur d'activité distinct du sien et, d'autre part, sur ce qu'elle avait acquitté la taxe sur la valeur ajoutée dont elle demandait la restitution partielle sans avoir fait application d'une quelconque interprétation administrative de la loi fiscale. En statuant ainsi, la cour n'a pas commis d'erreur de droit.<br/>
<br/>
              7. Il résulte de tout ce qui précède que l'entreprise Angel Parachutisme n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de l'entreprise Angel Parachutisme est rejeté.<br/>
Article 2 : La présente décision sera notifiée à l'entreprise unipersonnelle à responsabilité limitée Angel Parachutisme et au ministre de l'économie, des finances et de la relance.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
