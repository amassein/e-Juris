<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037254017</ID>
<ANCIEN_ID>JG_L_2018_07_000000415274</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/25/40/CETATEXT000037254017.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 26/07/2018, 415274, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415274</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Olivier Gariazzo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:415274.20180726</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La SCI Le Grand But a demandé au tribunal administratif de Lille de prononcer la décharge de la cotisation de taxe d'enlèvement des ordures ménagères à laquelle elle a été assujettie au titre des années 2013 et 2014 dans les rôles de la commune de Lomme (Nord). Par un jugement n° 1537633 du 14 septembre 2017, le tribunal administratif de Montreuil, saisi par le président de la section du contentieux du Conseil d'État sur le fondement de l'article R. 351-8 du code de justice administrative, a fait droit à sa demande. <br/>
<br/>
              Par un pourvoi enregistré le 26 octobre 2017 au secrétariat du contentieux du Conseil d'État, le ministre de l'économie et des finances demande au Conseil d'État d'annuler ce jugement. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Gariazzo, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de la société Le grand but.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le ministre de l'action et des comptes publics se pourvoit en cassation contre le jugement du 14 septembre 2017 par lequel le tribunal administratif de Montreuil, saisi par le président de la section du contentieux du Conseil d'Etat sur le fondement de l'article R. 351-8 du code de justice administrative, a fait droit à la demande de la SCI Le Grand But tendant à la décharge de la cotisation de taxe d'enlèvement des ordures ménagères à laquelle elle a été assujettie au titre des années 2013 et 2014 à raison d'immeubles dont elle est propriétaire dans la commune de Lomme. <br/>
<br/>
              2. D'une part, aux termes du I de l'article 1520 du code général des impôts, applicable aux établissements publics de coopération intercommunale, dans sa rédaction applicable à l'imposition en cause : " Les communes qui assurent au moins la collecte des déchets des ménages peuvent instituer une taxe destinée à pourvoir aux dépenses du service dans la mesure où celles-ci ne sont pas couvertes par des recettes ordinaires n'ayant pas le caractère fiscal. (...) ". En vertu des articles 1521 et 1522 du même code, cette taxe a pour assiette celle de la taxe foncière sur les propriétés bâties. La taxe d'enlèvement des ordures ménagères n'a pas le caractère d'un prélèvement opéré sur les contribuables en vue de pourvoir à l'ensemble des dépenses budgétaires, mais a exclusivement pour objet de couvrir les dépenses exposées par la commune pour assurer l'enlèvement et le traitement des ordures ménagères et non couvertes par des recettes non fiscales. Ces dépenses sont constituées de la somme de toutes les dépenses de fonctionnement réelles exposées pour le service public de collecte et de traitement des déchets ménagers et des dotations aux amortissements des immobilisations qui lui sont affectées. Il en résulte que le produit de cette taxe et, par voie de conséquence, son taux, ne doivent pas être manifestement disproportionnés par rapport au montant de ces dépenses, tel qu'il peut être estimé à la date du vote de la délibération fixant ce taux. <br/>
<br/>
              3. D'autre part, aux termes de l'article L. 2333-78 du code général des collectivités territoriales, dans sa rédaction applicable au litige : " (...) À compter du 1er janvier 1993, les communes, les établissements publics de coopération intercommunale ainsi que les syndicats mixtes qui n'ont pas institué la redevance prévue à l'article L. 2333-76 créent une redevance spéciale afin d'assurer l'élimination des déchets visés à l'article L. 2224-14 (...). Cette redevance est calculée en fonction de l'importance du service rendu et notamment de la quantité des déchets éliminés. Elle peut toutefois être fixée de manière forfaitaire pour l'élimination de petites quantités de déchets. (...) ". Les déchets mentionnés à l'article L. 2224-14 du même code sont les déchets non ménagers que ces collectivités peuvent, eu égard à leurs caractéristiques et aux quantités produites, collecter et traiter sans sujétions techniques particulières. Il résulte de ces dispositions, d'une part, que l'instauration de la redevance spéciale est obligatoire en l'absence de redevance d'enlèvement des ordures ménagères, d'autre part, que, compte tenu de ce qui a été dit au point 2, la taxe d'enlèvement des ordures ménagères n'a pas pour objet de financer l'élimination des déchets non ménagers, alors même que la redevance spéciale n'aurait pas été instituée.<br/>
<br/>
              4. Il résulte de ce qui précède qu'il appartient au juge de l'impôt, pour apprécier la légalité d'une délibération fixant le taux de la taxe d'enlèvement des ordures ménagères, que la collectivité ait ou non institué la redevance spéciale prévue par l'article L. 2333-78 du code général des collectivités territoriales et quel qu'en soit le produit, de rechercher si le produit de la taxe, tel qu'estimé à la date de l'adoption de la délibération, n'est pas manifestement disproportionné par rapport au coût de collecte et de traitement des seuls déchets ménagers, tel qu'il pouvait être estimé à cette même date, non couvert par les recettes non fiscales affectées à ces opérations, c'est-à-dire n'incluant pas le produit de la redevance spéciale lorsque celle-ci a été instituée. Lorsque le contribuable se prévaut, à l'appui de sa contestation de la légalité de cette délibération, de ce que les éléments retracés dans le compte administratif ou le rapport annuel relatif au service public d'élimination des ordures ménagères établis à l'issue de l'année en litige font apparaître que le produit constaté de la taxe excède manifestement le montant constaté des dépenses d'enlèvement et de traitement des ordures ménagères non couvertes par des recettes non fiscales, il appartient au juge de rechercher, au besoin en mettant en cause l'administration et en ordonnant un supplément d'instruction, si les données prévisionnelles au vu desquelles la délibération a été prise diffèrent sensiblement de celles, constatées a posteriori, sur lesquelles le requérant fonde son argumentation.<br/>
<br/>
              5. Pour juger que la société requérante était fondée à soutenir, par la voie de l'exception, que les délibérations par lesquelles le conseil de la communauté urbaine de Lille métropole, compétente en matière de traitement et de collecte des ordures ménagères, avait fixé le taux de la taxe d'enlèvement des ordures ménagères pour les années 2013 et 2014 étaient entachées d'erreur manifeste d'appréciation, le tribunal administratif s'est référé aux seules données d'exécution résultant du rapport annuel sur le prix et la qualité du service d'élimination des déchets de la métropole de Lille et a écarté l'argumentation de l'administration fiscale selon laquelle il convenait de se fonder sur les données dont disposait l'organe délibérant lors du vote du taux, au motif qu'il ne résultait pas de l'instruction, en l'absence d'observations de la métropole de Lille à qui la requête avait été communiquée, qu'il existerait entre ces données et celles résultant de l'exécution du budget, des différences telles qu'elles remettraient en cause la disproportion du taux d'imposition. Le ministre est fondé à soutenir qu'en statuant ainsi, alors qu'il avait produit à l'instance des données, issues du budget primitif, retraçant les éléments dont disposait le conseil de la communauté urbaine à la date du vote de la délibération en cause et qu'il appartenait au tribunal, en vertu de la règle rappelée au point 4, même en l'absence d'observations de la métropole, de se prononcer sur le bien fondé de l'argumentation en défense dont il avait ainsi été saisi, le tribunal a entaché son jugement d'insuffisance de motivation.<br/>
<br/>
              6. Il résulte de ce qui précède que le ministre est fondé à demander l'annulation du jugement attaqué. Les dispositions de l'article L.761-1 du code de justice administrative font obstacle à ce qu'il soit mis à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante, le versement de la somme demandée par la SCI Le Grand But.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                                         --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Montreuil est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Montreuil.<br/>
<br/>
Article 3 : Les conclusions présentées par la SCI Le Grand But au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée au ministre de l'action et des comptes publics et à la société civile immobilière Le Grand But.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
