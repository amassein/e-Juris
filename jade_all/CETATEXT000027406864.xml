<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027406864</ID>
<ANCIEN_ID>JG_L_2013_04_000000365646</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/40/68/CETATEXT000027406864.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 26/04/2013, 365646, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-04-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365646</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Baptiste de Froment</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:365646.20130426</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 1201777 du 28 janvier 2013, enregistrée le 31 janvier 2013 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président du tribunal administratif de Toulon, avant qu'il soit statué sur la demande de M. B...D...et autres tendant à l'annulation de la décision implicite du 5 mai 2012 du préfet du Var rejetant leur recours gracieux en date du 1er mars 2012 tendant au retrait de l'arrêté du 27 décembre 2011 rendant immédiatement opposable aux constructions, ouvrages, aménagements ou exploitations nouveaux, certaines dispositions du projet de plan de prévention des risques naturels d'incendies de forêt sur le territoire de la commune de Plan-de-la-Tour prescrit par un arrêté du préfet du Var du 13 octobre 2003, a admis la recevabilité de l'intervention de M. A...C...et autres et décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des articles L. 562-1 et L. 562-2 du code de l'environnement ;<br/>
<br/>
              Vu le mémoire, enregistré le 3 décembre 2012 au greffe du tribunal administratif de Toulon, présenté par M. A...D...et les autres requérants mentionnés dans l'ordonnance n° 1201777 du 28 janvier 2013 précitée, auquel s'associent M. A...C...et les autres intervenants mentionnés dans la même ordonnance, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              M. D...et autres soutiennent que les dispositions des articles L. 562-1 et L. 562-2 du code de l'environnement, qui sont applicables au litige, méconnaissent, d'une part, le droit de propriété garanti par les articles 2 et 17 de la Déclaration des droits de l'homme et du citoyen et, d'autre part, le principe d'égalité devant les charges publiques garanti par son article 13 ;<br/>
<br/>
              Vu le mémoire, enregistré le 22 février 2013 au secrétariat du Contentieux du Conseil d'Etat, présenté par M. D...et autres, qui reprennent les termes de leur mémoire présenté devant le tribunal administratif de Toulon et les mêmes moyens ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 6 mars 2013, présenté par le ministre de l'écologie, du développement durable et de l'énergie ; le ministre soutient que les conditions posées par l'article 23-4 de l'ordonnance du 7 novembre 1958 ne sont pas remplies, et en particulier, que la question, qui n'est pas nouvelle, ne présente pas un caractère sérieux ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 26 mars 2013, présenté par M. D...et autres, qui reprennent les termes de leur précédent mémoire et les mêmes moyens ;<br/>
<br/>
              Vu les pièces desquelles il ressort que la question prioritaire de constitutionnalité a été communiquée au Premier ministre et au ministre de l'égalité des territoires et du logement, qui n'ont pas produit de mémoire ;<br/>
<br/>
	 Vu les autres pièces du dossier ;<br/>
Vu la Constitution, notamment son article 61-1 ;<br/>
Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              Vu le code de l'environnement, notamment ses articles L. 561-1, L. 562-1 et          L. 562-2 ;<br/>
Vu le code de l'expropriation pour cause d'utilité publique ; <br/>
Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Baptiste de Froment, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant que l'article L. 562-1 du code de l'environnement prévoit que l'Etat arrête des plans de prévention des risques naturels prévisibles, tels que, en particulier, les incendies de forêt ; que ces plans ont notamment pour objet, en vertu du II de cet article, de délimiter les zones exposées à ces risques et de définir, compte tenu de leur gravité, les mesures de prévention, de protection et de sauvegarde qui doivent être prises dans ces zones, lesquelles peuvent consister en l'interdiction de tout type de construction, d'ouvrage, d'aménagement ou d'exploitation ; qu'en vertu de l'article L. 562-2 du même code, lorsqu'un projet de plan de prévention des risques naturels prévisibles contient certaines des dispositions mentionnées au 1° et 2° du II de l'article L. 562-1 et que l'urgence le justifie, le préfet peut, après consultation des maires concernés, les rendre immédiatement opposables à toute personne publique ou privée, par une décision rendue publique ; que ces dispositions cessent d'être opposables si elles ne sont pas reprises dans le plan finalement approuvé ; que M. D...et autres soutiennent que les dispositions de l'article L. 562-2 ainsi que celles du 1° et 2° du II de l'article L. 562-1 du code de l'environnement méconnaissent, d'une part, le droit de propriété garanti par les articles 2 et 17 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789 et, d'autre part, le principe d'égalité devant les charges publiques garanti par son article 13 ;<br/>
<br/>
              3. Considérant, en premier lieu, que les dispositions qui viennent d'être rappelées n'ont pas pour objet ou pour effet d'autoriser une quelconque dépossession ; qu'elles n'entrent pas, dès lors, dans le champ de l'article 17 de la Déclaration des droits de l'homme et du citoyen ; <br/>
<br/>
              4. Considérant, en second lieu, que, d'une part, les dispositions contestées sont justifiées par le motif d'intérêt général impérieux tenant à la protection de la sécurité des propriétaires et occupants des zones exposées à ces risques et de leurs biens ; que les mesures prises sont déterminées, ainsi qu'il a été dit ci-dessus, en fonction de la gravité des risques ; que ces dispositions ne font pas obstacle à l'indemnisation du préjudice résultant de l'institution des servitudes prévues par les plans de prévention des risques naturels prévisibles, lorsque les intéressés subissent une charge spéciale et exorbitante, hors de proportion avec cet objectif d'intérêt général ; que, d'autre part, les requérants soutiennent que l'article L. 562-2 ne comporte pas de garanties suffisantes dans les limitations apportées au droit de propriété, notamment en ce que, à la différence de la procédure d'approbation des plans, qui intervient après enquête publique, il ne prévoit la consultation que des seuls maires et non des propriétaires et ne fixe pas de borne à sa durée d'application ; que, toutefois, cet allègement du régime procédural est justifié par des motifs d'urgence tirés de la nécessité d'assurer la sécurité des personnes et de biens ; que, contrairement à ce qui est soutenu par les requérants, l'application des prescriptions du projet de plan est provisoire, l'approbation du plan, selon les délais précisés à l'article R. 562-2, ayant pour conséquence que les dispositions rendues opposables en application de l'article L. 562-2 cessent de l'être ; qu'il résulte de ce qui précède que les dispositions de l'article L. 562 -2 du code de l'environnement ne portent pas au droit de propriété garanti par l'article 2 de la Déclaration des droits de l'homme et du citoyen une atteinte disproportionnée au but recherché et qu'elles sont en outre assorties des garanties suffisantes au regard du respect de ce même droit ;<br/>
<br/>
              5. Considérant, en troisième lieu, que les requérants font valoir que l'article L. 562-2 du code de l'environnement ne prévoit pas l'institution d'une procédure d'expropriation pour les zones exposées aux incendies de forêt, alors que l'article L. 561-1 du même code prévoit la possibilité d'exproprier les terrains exposés à certains risques, tels les mouvements ou affaissements de terrain ou les crues torrentielles ou à montée rapide ou de submersion marine ; que, toutefois, le principe d'égalité devant les charges publiques reconnu par l'article 13 de la Déclaration des droits de l'homme et du citoyen ne saurait être invoqué à l'appui d'un tel moyen ; qu'au demeurant, à supposer que les requérants aient entendu invoquer le principe d'égalité des citoyens devant la loi, les personnes exposées aux risques naturels majeurs prévisibles énumérés à l'article L. 561-1 du code de l'environnement se trouvent, au regard des objectifs de sauvegarde des vies humaines et de réduction des risques, dans une situation différente de celles situées dans des zones régies par les plans de prévention des risques naturels d'incendies de forêts ; qu'ainsi, le législateur n'a pas méconnu le principe d'égalité des citoyens devant la loi ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que la question de la conformité à la Constitution des articles L. 561-1 et L. 562-2 du code de l'environnement, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; qu'il n'y a pas lieu de la renvoyer au Conseil constitutionnel ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le tribunal administratif de Toulon.<br/>
Article 2 : La présente décision sera notifiée à M. B...D...et à M. A...C..., respectivement premier requérant et premier intervenant dénommés, et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
Copie en sera adressée au Conseil constitutionnel, au tribunal administratif de Toulon, au Premier ministre et à la ministre de l'égalité des territoires et du logement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
