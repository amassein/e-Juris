<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038815850</ID>
<ANCIEN_ID>JG_L_2019_07_000000426475</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/81/58/CETATEXT000038815850.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 24/07/2019, 426475, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426475</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:426475.20190724</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Free Mobile a demandé au juge des référés du tribunal administratif de Lyon, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de l'arrêté du 14 septembre 2018 par lequel le maire de Saint-Nizier-d'Azergues (Rhône) a refusé de lui délivrer un permis de construire pour l'installation d'une antenne-relais sur un terrain cadastré AL n° 145 situé au lieudit " Chavanne " et, à titre subsidiaire, d'enjoindre au maire de Saint-Nizier-d'Azergues de reprendre une décision dans un délai d'un mois.<br/>
<br/>
              Par une ordonnance n° 1808553 du 7 décembre 2018, le juge des référés du tribunal administratif de Lyon a rejeté sa demande. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 21 décembre 2018 et 4 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, la société Free Mobile demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Saint-Nizier-d'Azergues la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des postes et des communications électroniques ; <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, conseiller d'Etat, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la société Free Mobile ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              2.	L'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celle-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue.<br/>
<br/>
              3.	Pour rejeter la demande de suspension dont il était saisi, le juge des référés du tribunal administratif de Lyon s'est fondé sur la circonstance que la société Free Mobile avait passé un accord d'itinérance avec un autre opérateur de télécommunications permettant aux abonnés de cette société de bénéficier, sur le territoire de la commune de Saint-Nizier-d'Azergues, du réseau 3G. En se fondant sur cette circonstance pour refuser de tenir la condition d'urgence comme remplie, alors qu'elle était sans incidence sur les obligations de déploiement incombant à la société Free Mobile en vertu de son cahier des charges, le juge des référés a commis une erreur de droit. Par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société Free Mobile est fondée à demander l'annulation de l'ordonnance qu'elle attaque.<br/>
<br/>
              4.	Il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée.<br/>
<br/>
              5.	En premier lieu, eu égard à l'intérêt public qui s'attache à la couverture du territoire national par le réseau de téléphonie mobile tant 3G que 4G et aux intérêts propres de la société Free Mobile qui est soumise à un cahier des charges lui imposant notamment un taux de couverture de la population métropolitaine de 98 % au 1er janvier 2027, et en particulier à la circonstance que le territoire de la commune de Saint-Nizier-d'Azergues n'est que partiellement couvert par les réseaux de téléphonie mobile de la société requérante, la condition d'urgence doit être regardée comme remplie.<br/>
<br/>
              6.	En second lieu, en l'état de l'instruction, sont de nature à faire naître un doute sérieux quant à la légalité de la décision contestée : <br/>
              - le moyen tiré de l'absence de la mention du nom, du prénom et de la qualité de la personne qui, par délégation du maire, a signé  la décision ;<br/>
              - les moyens tirés de ce que le maire de Saint-Nizier-d'Azergues ne pouvait s'opposer au permis de construire aux motifs, qui ne sont pas tirés de la méconnaissance d'une règle d'urbanisme, que le projet aurait méconnu des dispositions du code des postes et des communications électroniques, que le terrain retenu par la société requérante pour l'édification d'une antenne ne lui semblait pas le plus adapté, que le projet ne présentait pas un intérêt public et qu'il aurait dévalué les habitations situées à proximité ;<br/>
              - les moyens tirés des erreurs d'appréciation de l'auteur de la décision quant à l'insertion environnementale du projet et à l'atteinte aux paysages.<br/>
<br/>
              7.	Il résulte de ce qui précède que la société Free Mobile est fondée à demander la suspension de l'exécution de l'arrêté qu'elle attaque. <br/>
<br/>
              8.	Il y a lieu d'enjoindre au maire de Saint-Nizier-d'Azergues de procéder à une nouvelle instruction de la demande de permis de construire déposée par la société Free Mobile dans un délai d'un mois à compter de la notification de la présente décision.<br/>
<br/>
              9.	Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Saint-Nizier-d'Azergues le versement à la société Free Mobile de la somme de 4 000 euros au titre des frais exposés devant le Conseil d'Etat et devant le tribunal administratif de Lyon et non compris dans les dépens. En revanche, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la société Free Mobile, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Lyon du 7 décembre 2018 est annulée.<br/>
<br/>
Article 2 : L'exécution de l'arrêté du 14 septembre 2018 par lequel le maire de la commune de Saint-Nizier-d'Azergues a refusé la demande de permis de construire présentée par la société Free Mobile est suspendue.<br/>
<br/>
Article 3 : Il est enjoint à la commune de Saint-Nizier-d'Azergues de procéder à un nouvel examen de la demande de permis de construire déposée par la société Free Mobile dans un délai d'un mois à compter de la notification de la présente décision.<br/>
<br/>
Article 4 : La commune de Saint-Nizier-d'Azergues versera à la société Free Mobile la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : Les conclusions de la commune de Saint-Nizier-d'Azergues tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 6 : La présente décision sera notifiée à la société Free Mobile et à la commune de Saint-Nizier-d'Azergues. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
