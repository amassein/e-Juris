<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038135377</ID>
<ANCIEN_ID>JG_L_2019_02_000000407694</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/13/53/CETATEXT000038135377.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 15/02/2019, 407694, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407694</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD</AVOCATS>
<RAPPORTEUR>Mme Céline  Guibé</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:407694.20190215</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° La SARL Coplan a demandé au tribunal administratif de Nice de prononcer la réduction des rappels de taxe sur la valeur ajoutée auxquels elle a été assujettie au titre de la période du 1er janvier 2007 au 31 décembre 2009, ainsi que des pénalités correspondantes. Par un jugement n° 1201283 du 20 juin 2014, le tribunal administratif de Nice a fait droit à sa demande.<br/>
<br/>
              Par un arrêt n° 14MA03667 du 6 décembre 2016, la cour administrative d'appel de Marseille a, sur recours du ministre des finances et des comptes publics, annulé ce jugement, remis à la charge de la SARL Coplan les rappels de taxe sur la valeur ajoutée auxquels cette dernière avait été assujettie ainsi que les pénalités correspondantes et rejeté ses conclusions formées sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Sous le n° 407694, par un pourvoi sommaire et deux mémoires complémentaires, enregistrés les 7 février et 9 mai 2017 et le 13 avril 2018 au secrétariat du contentieux du Conseil d'Etat, la SAS Oteis venant aux droits et obligations de la SARL Coplan demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° La SA Groupe Coplan a demandé au tribunal administratif de Nice de prononcer la décharge des cotisations supplémentaires d'impôt sur les sociétés auxquelles elle a été assujettie au titre des exercices clos en 2007, 2008 et 2009, ainsi que des pénalités correspondantes. Par un jugement n° 1300510 du 6 mars 2015, le tribunal administratif de Nice a prononcé la décharge des cotisations et pénalités résultant de la réintégration de frais d'assistance administrative, commerciale, juridique et financière dans le résultat de la SARL Coplan au titre de la période du 1er janvier 2007 au 31 décembre 2009 et a rejeté le surplus de sa demande. <br/>
<br/>
              Par un arrêt n° 15MA02732 du 6 décembre 2016, la cour administrative d'appel de Marseille a, sur recours du ministre des finances et des comptes publics, annulé les articles 1er et 2 du jugement du 6 mars 2015, remis à la charge de la SA Groupe Coplan les cotisations supplémentaires d'impôt sur les sociétés auxquelles elle avait été assujettie ainsi que les pénalités y afférentes et a rejeté les conclusions incidentes présentées par la société.<br/>
<br/>
              Sous le n° 407696, par un pourvoi sommaire et deux mémoires complémentaires, enregistrés les 7 février et 9 mai 2017 et le 13 avril 2018 au secrétariat du contentieux du Conseil d'Etat, la société par actions simplifiée Oteis, anciennement SA Groupe Coplan, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Céline Guibé, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, au Cabinet Briard, avocat de la SAS Oteis.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. La SARL Coplan, qui exerçait une activité de prestations d'études techniques et d'ingénierie au sein d'un groupe fiscalement intégré dont la SA Groupe Coplan était la société mère, a fait l'objet d'une vérification de comptabilité portant sur la période du 1er janvier 2007 au 31 décembre 2009. A l'issue de cette vérification, l'administration fiscale a notamment remis en cause la déduction, par la SARL Coplan, de charges relatives à des prestations d'assistance facturées à cette dernière par la SA Groupe Coplan dans le cadre de la convention signée entre elles le 1er janvier 2006, ainsi que celle de la taxe sur la valeur ajoutée afférente à ces mêmes prestations. Par deux arrêts du 6 décembre 2016, la cour administrative d'appel de Marseille a, sur le recours du ministre de l'action et des comptes publics, remis à la charge des sociétés les cotisations d'impôt sur les sociétés et les rappels de taxe sur la valeur ajoutée dont le tribunal administratif de Nice leur avait accordé la décharge. La SAS Oteis, nouvelle dénomination de la SA Groupe Coplan, demande l'annulation de ces arrêts par deux pourvois qu'il y a lieu de joindre.<br/>
<br/>
              Sur les conclusions relatives à l'impôt sur les sociétés :<br/>
<br/>
              2. Aux termes du 1 de l'article 39 du code général des impôts : " Le bénéfice net est établi sous déduction de toutes charges, celles-ci comprenant (...) notamment : / 1° Les frais généraux de toute nature (...) ". Si, en vertu des règles gouvernant l'attribution de la charge de la preuve devant le juge administratif, applicables sauf loi contraire, il incombe, en principe, à chaque partie d'établir les faits qu'elle invoque au soutien de ses prétentions, les éléments de preuve qu'une partie est seule en mesure de détenir ne sauraient être réclamés qu'à celle-ci. Il appartient, dès lors, au contribuable, pour l'application des dispositions précitées du code général des impôts, de justifier tant du montant des charges qu'il entend déduire du bénéfice net défini à l'article 38 du code général des impôts que de la correction de leur inscription en comptabilité, c'est-à-dire du principe même de leur déductibilité. Le contribuable apporte cette justification par la production de tous éléments suffisamment précis portant sur la nature de la charge en cause, ainsi que sur l'existence et la valeur de la contrepartie qu'il en a retirée. Dans l'hypothèse où le contribuable s'acquitte de cette obligation, il incombe ensuite au service, s'il s'y croit fondé, d'apporter la preuve de ce que la charge en cause n'est pas déductible par nature, qu'elle est dépourvue de contrepartie, qu'elle a une contrepartie excessive dépourvue d'intérêt pour le contribuable ou que la rémunération de cette contrepartie est excessive.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que la SARL Coplan, pour justifier tant du montant des charges qu'elle entendait déduire de son bénéfice imposable que du principe même de leur déductibilité, produisait devant la cour les factures établies par la SA Groupe Coplan et la convention du 1er janvier 2006, conclue entre la SA Groupe Coplan et ses sociétés filiales, en application de laquelle les prestations ont été fournies. Les factures fournies mentionnent des prestations qui, si elles sont regroupées par catégories telles que " frais généraux groupe ", " frais de région ", " assistance juridique et comptable ", peuvent être regardées comme correspondant aux prestations que décrit précisément la convention du 1er janvier 2006.<br/>
<br/>
              4. Le ministre, pour démontrer que ces charges étaient dépourvues de contrepartie pour la SARL Coplan, soutenait devant la cour en premier lieu, que certaines des prestations facturées correspondaient à des missions inhérentes à l'exercice du mandat social du gérant de la SARL Coplan, en énumérant toutefois des prestations, à savoir le secrétariat général, la gestion financière, le contrôle de gestion et la négociation commerciale, qui ne sont pas au nombre des prestations qui ne pourraient être fournies que par les mandataires sociaux. Il soutenait en second lieu que la société disposait de personnels administratifs susceptibles d'effectuer les tâches facturées par la SA Groupe Coplan, en se bornant à donner la liste du personnel de la SARL Coplan, qui comprend seulement deux secrétaires, une responsable de service administratif, une comptable et une assistante commerciale.<br/>
<br/>
              5. Compte tenu des éléments produits par la société et des arguments avancés par le ministre la cour ne pouvait, sans dénaturer les faits de l'espèce, juger que le ministre apportait la preuve de l'absence de réalité des prestations facturées par la SA Groupe Coplan. Ce motif suffisant à entraîner l'annulation de l'arrêt attaqué, il n'est pas nécessaire d'examiner les autres moyens du pourvoi.<br/>
<br/>
              Sur les conclusions relatives à la taxe sur la valeur ajoutée :<br/>
<br/>
              6. En vertu des dispositions combinées des articles 271, 272 et 283 du code général des impôts, un contribuable n'est pas en droit de déduire, de la taxe sur la valeur ajoutée dont il est redevable à raison de ses propres opérations, la taxe mentionnée sur une facture établie à son nom par une personne qui n'est pas le fournisseur réel de la marchandise ou de la prestation effectivement livrée ou exécutée. Dans le cas où l'auteur de la facture est régulièrement inscrit au registre du commerce et des sociétés, assujetti à la taxe sur la valeur ajoutée et se présente comme tel à ses clients, il appartient à l'administration, si elle entend refuser à celui qui a reçu la facture le droit de déduire la taxe qui y est mentionnée, d'établir qu'il s'agit d'une facture de complaisance et que le contribuable le savait ou ne pouvait l'ignorer. Si l'administration apporte des éléments suffisants en ce sens, il appartient alors au contribuable d'apporter toutes justifications utiles sur cette opération, sans qu'il ne puisse être exigé de lui des vérifications qui ne lui incombent pas.<br/>
<br/>
              7. Il résulte de ce qui est dit aux point 3 et 4 ci-dessus que la cour administrative d'appel, en jugeant que l'administration fiscale apportait la preuve, qui lui incombait, de l'absence de réalité des prestations facturées par la SA Groupe Coplan à la SARL Coplan, a entaché son arrêt de dénaturation. Ce motif suffisant à entraîner l'annulation de l'arrêt attaqué, il n'est pas nécessaire d'examiner les autres moyens du pourvoi.<br/>
<br/>
              8. Il résulte de tout ce qui précède que la SAS Oteis est fondée à demander l'annulation des arrêts qu'elle attaque. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              Sur l'appel du ministre :<br/>
<br/>
              9. Il résulte des motifs d'annulation retenus aux points 5 et 7 ci-dessus que le ministre, qui ne soulève en appel qu'un moyen tiré de l'absence de réalité des prestations facturées, n'est pas fondé à demander l'annulation des jugements des 20 juin 2014 et 6 mars 2015 du tribunal administratif de Nice.<br/>
<br/>
              Sur l'appel incident de la SA Groupe Coplan devenue SAS Oteis :<br/>
<br/>
              10. Aux termes du troisième alinéa de l'article R. 256-1 du livre des procédures fiscales dans sa rédaction applicable au litige : " Lorsqu'en application des dispositions de l'article 223 A du code général des impôts la société mère d'un groupe est amenée à supporter les droits et pénalités résultant d'une procédure de rectification suivie à l'égard d'une ou de plusieurs sociétés du groupe, l'administration adresse à la société mère, préalablement à la notification de l'avis de mise en recouvrement correspondant, un document l'informant du montant global par impôt des droits, des pénalités et des intérêts de retard dont elle est redevable. L'avis de mise en recouvrement, qui peut être alors émis sans délai, fait référence à ce document ". La SAS Oteis fait valoir qu'elle a été informée des conséquences financières du contrôle de sa filiale alors que cette dernière n'avait pas encore reçu les propositions de rectification ayant conduit aux suppléments d'impôt en litige. Toutefois, les dispositions précitées de l'article R. 256-1 du livre des procédures fiscales, qui n'ont pas pour objet de permettre l'engagement d'un débat contradictoire entre l'administration fiscale et la société mère, imposent seulement que cette dernière soit informée des conséquences financières du contrôle de l'une de ses filiales avant l'avis de mise en recouvrement des impositions correspondantes. Par suite, la SAS Oteis, qui ne conteste pas que cette information lui a été adressée préalablement à la mise en recouvrement des impositions en litige, n'est pas fondée à soutenir, par la voie de l'appel incident, que la procédure d'imposition est entachée d'irrégularité. <br/>
<br/>
              11. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat, pour l'ensemble des deux procédures, la somme de 8 000 euros à verser à la SAS Oteis, au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les arrêts nos 14MA03667 et 15MA02732 de la cour administrative d'appel de Marseille sont annulés.<br/>
<br/>
Article 2 : Les requêtes du ministre des finances et des comptes publics et l'appel incident de la SA Groupe Coplan devenue SAS Oteis sont rejetées.<br/>
<br/>
Article 3 : L'Etat versera à la SAS Oteis, la somme de 8 000 euros à au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la SAS Oteis et au ministre de l'action et des comptes publics.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
