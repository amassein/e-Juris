<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037022298</ID>
<ANCIEN_ID>JG_L_2018_06_000000410774</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/02/22/CETATEXT000037022298.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 06/06/2018, 410774</TITRE>
<DATE_DEC>2018-06-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410774</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:410774.20180606</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Ligue des droits de l'homme a demandé au tribunal administratif de Montpellier d'annuler pour excès de pouvoir les arrêtés des 25 avril et 7 juillet 2014 du maire de Béziers relatifs à la circulation des mineurs de moins de 13 ans. Par un jugement n°1402956, 1403294, 1403605 du 22 juin 2016, le tribunal administratif de Montpellier a prononcé un non-lieu à statuer sur les conclusions dirigées contre l'arrêté du 25 avril 2014 et rejeté le surplus des conclusions.<br/>
<br/>
              Par un arrêt n°16MA03385 du 20 mars 2017, la cour administrative d'appel de Marseille a partiellement annulé ce jugement, prononcé un non-lieu à statuer sur les conclusions dirigées contre l'arrêté du 25 avril 2014, annulé l'arrêté du 7 juillet 2014 en tant qu'il concerne la période du 15 juin 2014 jusqu'à son entrée en vigueur et rejeté le surplus des conclusions d'appel de la Ligue des droits de l'homme.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 22 mai et 22 août 2017 au secrétariat du contentieux du Conseil d'Etat, la Ligue des droits de l'homme demande au Conseil d'Etat :  <br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il rejette le surplus de ses conclusions ;<br/>
<br/>
              2°) réglant l'affaire au fond, dans cette mesure, de faire  droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Béziers la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code pénal ; <br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la Ligue des droits de l'homme  et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune de Béziers ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 25 avril 2014, le maire de Béziers a interdit la circulation des mineurs de 13 ans non accompagnés d'une personne majeure de 23 heures à 6 heures dans des zones correspondant au centre-ville et à la zone spéciale de sécurité de Béziers, toutes les nuits des vendredi, samedi et dimanche et durant les vacances scolaires de la zone A, entre le 15 juin  et le 15 septembre 2014. Par un arrêté du 7 juillet 2014, qui annule et remplace le précédent, le maire de Béziers a défini la même interdiction et modifié l'article 5 de l'arrêté relatif aux sanctions pénales susceptibles d'être infligées aux parents des enfants qui ne la respectent pas. Par un jugement du 22 juin 2016, le tribunal administratif de Montpellier a prononcé un non lieu à statuer sur les conclusions de la Ligue des droits de l'homme dirigées contre l'arrêté du 25 avril 2014 et rejeté celles qui tendaient à l'annulation pour excès de pouvoir de l'arrêté du 7 juillet 2014. La Ligue des droits de l'homme se pourvoit en cassation contre l'arrêt du 20 mars 2017 de la cour administrative d'appel de Marseille en tant qu'il rejette ses conclusions tendant à l'annulation de l'arrêté du 7 juillet 2014 pour la période comprise entre le 7 juillet et le 15 septembre 2014.<br/>
<br/>
              2. Ni les pouvoirs de police générale que l'Etat peut exercer en tous lieux vis-à-vis des mineurs, ni l'article 371-2 du code civil selon lequel la santé, la sécurité et la moralité de l'enfant sont confiées par la loi à ses parents, qui ont à son égard droit et devoir d'éducation, ni enfin les articles 375 à 375-8 du même code selon lesquels l'autorité judiciaire peut, en cas de carence des parents et si la santé, la sécurité ou la moralité d'un mineur sont en danger, prononcer des mesures d'assistance éducative ne font obstacle à ce que, tant pour contribuer à la protection des mineurs que pour prévenir les troubles à l'ordre public qu'ils sont susceptibles de provoquer, le maire fasse usage, en fonction de circonstances locales particulières, des pouvoirs de police générale qu'il tient des articles L. 2212-1 et suivants du code général des collectivités territoriales. Toutefois, la légalité de mesures restreignant à cette fin la liberté de circulation des mineurs est subordonnée à la condition qu'elles soient justifiées par l'existence de risques particuliers de troubles à l'ordre public auxquels ces mineurs seraient exposés ou dont ils seraient les auteurs dans les secteurs pour lesquels elles sont édictées, adaptées à l'objectif pris en compte et proportionnées.<br/>
<br/>
              3. Il ressort des termes mêmes de l'arrêté attaqué que l'interdiction qu'il édicte poursuit à la fois l'objectif de protection des mineurs de moins de 13 ans contre les violences dont ils pourraient être les victimes que celui de prévention des troubles  qu'ils pourraient causer à l'ordre public. Or, si la ville de Béziers a produit devant les juges du fond le texte de  la " déclinaison départementale  de la stratégie nationale de prévention de la délinquance pour la période 2014-2017 " dans le département de l'Hérault ainsi qu'une note du 3 juillet 2014 du commissariat central de la circonscription de Béziers, il ne ressort de ces documents  ni que la mise en cause des mineurs de moins de 13 ans présente un niveau particulièrement élevé dans les zones concernées par l'arrêté attaqué, ni que l'augmentation de la délinquance constatée, en 2013 et au premier semestre 2014, dans ces zones se soit accompagnée d'une implication croissante de ces mineurs. Dans ces conditions, en jugeant, sans que des éléments précis et circonstanciés de nature à étayer l'existence de risques particuliers relatifs aux mineurs de moins de 13 ans dans le centre ville de Béziers et dans le quartier de la Devèze ne soient soumis à son appréciation, que la mesure d'interdiction de circulation des mineurs de 13 ans contestée était justifiée par l'existence de risques particuliers et adaptée aux objectifs visés, la cour administrative d'appel de Marseille a entaché son arrêt d'inexacte qualification juridique des faits. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'arrêt du 20 mars 2017 doit être annulé dans la mesure où il est attaqué.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              5. Si, en principe, le fait qu'une décision administrative ait un champ d'application territorial fait obstacle à ce qu'une association ayant un ressort national justifie d'un intérêt lui donnant qualité pour en demander l'annulation, il peut en aller autrement lorsque la décision soulève, en raison de ses implications, notamment dans le domaine des libertés publiques, des questions qui, par leur nature et leur objet, excèdent les seules circonstances locales. Eu égard à son objet social, la Ligue des droits de l'homme a un intérêt à agir à l'encontre de l'arrêté municipal attaqué qui présente, dans la mesure notamment où il cherche à  répondre à une situation susceptible d'être rencontrée dans d'autres communes, une portée excédant son seul objet local. Il s'ensuit que la fin de non recevoir opposée par la commune de Béziers doit être écartée. <br/>
<br/>
              6. Ainsi qu'il a été dit au point 3, les documents produits par la ville de Béziers n'apportent pas d'éléments précis et circonstanciés de nature à étayer l'existence de risques particuliers relatifs aux mineurs de moins de 13 ans dans le centre ville de Béziers et dans le quartier de la Devèze pour la période visée par l'arrêté attaqué. Dès lors, l'interdiction prévue par l'arrêté attaqué du 7 juillet 2014 ne peut être regardée comme  une mesure justifiée par de tels risques. Il en résulte, sans qu'il soit besoin d'examiner les autres moyens de la requête, que la Ligue des droits de l'homme est fondée à soutenir que c'est à tort que le tribunal administratif de Montpellier a rejeté ses conclusions tendant à l'annulation de l'arrêté du maire de Béziers du 7 juillet 2014 en tant qu'il s'applique postérieurement à son entrée en vigueur.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Béziers la somme de 5000 euros à verser à la Ligue des droits de l'homme au titre de l'article L. 761-1 du code de justice administrative. Ces dernières dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la Ligue des droits de l'homme qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
              D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'article 7  de l'arrêt de la cour administrative d'appel de Marseille du 20 mars 2017 est annulé.<br/>
Article 2 : Le jugement du tribunal administratif de Montpellier du 22 juin 2016 est annulé en tant qu'il rejette les conclusions de la Ligue des droits de l'homme tendant à l'annulation de l'arrêté du 7 juillet 2014 en tant qu'il s'applique postérieurement à son entrée en vigueur.<br/>
Article 3 : L'arrêté du 7 juillet 2014 est annulé en tant qu'il s'applique postérieurement à son entrée en vigueur.<br/>
Article 4 : La commune de Béziers versera à la Ligue des droits de l'homme une somme de 5000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Les conclusions de la commune de Béziers présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à la Ligue des droits de l'homme et à la commune de Béziers.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-03-05 DROITS CIVILS ET INDIVIDUELS. LIBERTÉS PUBLIQUES ET LIBERTÉS DE LA PERSONNE. LIBERTÉ D'ALLER ET VENIR. - ARRÊTÉ MUNICIPAL RÉGLEMENTANT LA CIRCULATION DES MINEURS DE MOINS DE TREIZE ANS - 1) POSSIBILITÉ POUR LE MAIRE DE FAIRE USAGE DE SES POUVOIRS DE POLICE GÉNÉRALE TANT POUR CONTRIBUER À LA PROTECTION DE CES MINEURS QUE POUR PRÉVENIR LES TROUBLES À L'ORDRE PUBLIC QU'ILS SONT SUSCEPTIBLES DE PROVOQUER - EXISTENCE - CONDITIONS [RJ1] - 2) ARRÊTÉ DU MAIRE DE BÉZIERS INSTITUANT UN COUVRE-FEU DES MINEURS DE MOINS DE TREIZE ANS, DANS CERTAINS SECTEURS DE LA COMMUNE ET À CERTAINES PÉRIODES - LÉGALITÉ - ABSENCE, FAUTE D'ÉLÉMENTS PRÉCIS ET CIRCONSTANCIÉS DE NATURE À ÉTAYER L'EXISTENCE DE RISQUES PARTICULIERS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">49-04 POLICE. POLICE GÉNÉRALE. - ARRÊTÉ MUNICIPAL RÉGLEMENTANT LA CIRCULATION DES MINEURS DE MOINS DE TREIZE ANS - 1) POSSIBILITÉ POUR LE MAIRE DE FAIRE USAGE DE SES POUVOIRS DE POLICE GÉNÉRALE TANT POUR CONTRIBUER À LA PROTECTION DE CES MINEURS QUE POUR PRÉVENIR LES TROUBLES À L'ORDRE PUBLIC QU'ILS SONT SUSCEPTIBLES DE PROVOQUER - EXISTENCE - CONDITIONS [RJ1] - 2) ARRÊTÉ DU MAIRE DE BÉZIERS INSTITUANT UN COUVRE-FEU DES MINEURS DE MOINS DE TREIZE ANS, DANS CERTAINS SECTEURS DE LA COMMUNE ET À CERTAINES PÉRIODES - LÉGALITÉ - ABSENCE, FAUTE D'ÉLÉMENTS PRÉCIS ET CIRCONSTANCIÉS DE NATURE À ÉTAYER L'EXISTENCE DE RISQUES PARTICULIERS.
</SCT>
<ANA ID="9A"> 26-03-05 1) Ni les pouvoirs de police générale que l'Etat peut exercer en tous lieux vis-à-vis des mineurs, ni l'article 371-2 du code civil selon lequel la santé, la sécurité et la moralité de l'enfant sont confiées par la loi à ses parents, qui ont à son égard droit et devoir d'éducation, ni enfin les articles 375 à 375-8 du même code selon lesquels l'autorité judiciaire peut, en cas de carence des parents et si la santé, la sécurité ou la moralité d'un mineur sont en danger, prononcer des mesures d'assistance éducative ne font obstacle à ce que, tant pour contribuer à la protection des mineurs que pour prévenir les troubles à l'ordre public qu'ils sont susceptibles de provoquer, le maire fasse usage, en fonction de circonstances locales particulières, des pouvoirs de police générale qu'il tient des articles L. 2212-1 et suivants du code général des collectivités territoriales (CGCT). Toutefois, la légalité de mesures restreignant à cette fin la liberté de circulation des mineurs est subordonnée à la condition qu'elles soient justifiées par l'existence de risques particuliers de troubles à l'ordre public auxquels ces mineurs seraient exposés ou dont ils seraient les auteurs dans les secteurs pour lesquels elles sont édictées, adaptées à l'objectif pris en compte et proportionnées.,,,2) Recours pour excès de pouvoir dirigé contre un arrêté du maire de Béziers instituant un couvre-feu des mineurs de moins treize ans non accompagnés d'une personne majeure de 23h à 6h du matin, dans certains secteurs de la commune et pendant les week-end et vacances....  ,,Il ressort des termes mêmes de l'arrêté que l'interdiction qu'il édicte poursuit à la fois l'objectif de protection des mineurs de moins de treize ans contre les violences dont ils pourraient être les victimes que celui de prévention des troubles qu'ils pourraient causer à l'ordre public. Or, il ne ressort des documents produits par la commune de Béziers ni que la mise en cause des mineurs de treize ans présente un niveau particulièrement élevé dans les zones concernées par l'arrêté attaqué, ni que l'augmentation de la délinquance constatée dans ces zones se soit accompagnée d'une implication croissante de ces mineurs. Par suite, illégalité de la mesure, en l'absence d'éléments précis et circonstanciés de nature à étayer l'existence de risques particuliers relatifs aux mineurs de moins treize ans dans les zones concernées.</ANA>
<ANA ID="9B"> 49-04 1) Ni les pouvoirs de police générale que l'Etat peut exercer en tous lieux vis-à-vis des mineurs, ni l'article 371-2 du code civil selon lequel la santé, la sécurité et la moralité de l'enfant sont confiées par la loi à ses parents, qui ont à son égard droit et devoir d'éducation, ni enfin les articles 375 à 375-8 du même code selon lesquels l'autorité judiciaire peut, en cas de carence des parents et si la santé, la sécurité ou la moralité d'un mineur sont en danger, prononcer des mesures d'assistance éducative ne font obstacle à ce que, tant pour contribuer à la protection des mineurs que pour prévenir les troubles à l'ordre public qu'ils sont susceptibles de provoquer, le maire fasse usage, en fonction de circonstances locales particulières, des pouvoirs de police générale qu'il tient des articles L. 2212-1 et suivants du code général des collectivités territoriales (CGCT). Toutefois, la légalité de mesures restreignant à cette fin la liberté de circulation des mineurs est subordonnée à la condition qu'elles soient justifiées par l'existence de risques particuliers de troubles à l'ordre public auxquels ces mineurs seraient exposés ou dont ils seraient les auteurs dans les secteurs pour lesquels elles sont édictées, adaptées à l'objectif pris en compte et proportionnées.,,,2) Recours pour excès de pouvoir dirigé contre un arrêté du maire de Béziers instituant un couvre-feu des mineurs de moins treize ans non accompagnés d'une personne majeure de 23h à 6h du matin, dans certains secteurs de la commune et pendant les week-end et vacances....  ,,Il ressort des termes mêmes de l'arrêté que l'interdiction qu'il édicte poursuit à la fois l'objectif de protection des mineurs de moins de treize ans contre les violences dont ils pourraient être les victimes que celui de prévention des troubles qu'ils pourraient causer à l'ordre public. Or, il ne ressort des documents produits par la commune de Béziers ni que la mise en cause des mineurs de treize ans présente un niveau particulièrement élevé dans les zones concernées par l'arrêté attaqué, ni que l'augmentation de la délinquance constatée dans ces zones se soit accompagnée d'une implication croissante de ces mineurs. Par suite, illégalité de la mesure, en l'absence d'éléments précis et circonstanciés de nature à étayer l'existence de risques particuliers relatifs aux mineurs de moins treize ans dans les zones concernées.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., en précisant, CE, juge des référés, 9 juillet 2001, Préfet du Loiret, n° 235638, p. 337.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
