<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031259798</ID>
<ANCIEN_ID>JG_L_2015_10_000000384907</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/25/97/CETATEXT000031259798.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème SSJS, 01/10/2015, 384907, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-10-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384907</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>M. Timothée Paris</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:384907.20151001</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Versailles de condamner l'Etat à lui payer la somme de 3 768, 66 euros, augmentée des intérêts aux taux légal, en réparation du préjudice qu'il estime avoir subi du fait des erreurs dans la comptabilisation et dans la rémunération de ses heures de travail, et d'enjoindre à l'administration pénitentiaire d'exécuter le jugement à intervenir sous une astreinte de 50 euros par jour de retard. Par un jugement n° 0907496 du 12 avril 2012, le tribunal a rejeté cette demande.<br/>
<br/>
              Par une ordonnance n° 13VE01287 du 8 septembre 2014, la présidente de la cour administrative d'appel de Versailles a transmis au Conseil d'Etat le pourvoi, enregistrée le 23 avril 2013 au greffe de la cour, présentée par M.A.... <br/>
<br/>
              Par ce pourvoi et deux nouveaux mémoires, enregistrés les 2 mars et 12 juin 2015, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1) d'annuler ce jugement du tribunal administratif de Versailles ;<br/>
<br/>
              2) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3) de mettre à la charge de l'Etat la somme de 3 000 euros, au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et libertés fondamentales <br/>
              - le code de procédure pénale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Timothée Paris, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat de M. A...;<br/>
<br/>
              Une note en délibéré, enregistrée le 25 septembre 2015, a été présentée par M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>1. M.A..., estimant avoir réalisé entre mars 2006 et mars 2008, au sein de la maison d'arrêt de Fleury-Mérogis dans laquelle il est incarcéré, 997 heures de travail qui ne lui auraient pas été rémunérées, a saisi le tribunal administratif de Versailles d'une demande tendant à la condamnation de l'Etat à lui verser la somme de 3 768,66, euros augmentée des intérêts au taux légal à compter du mois de février 2006, en réparation du préjudice subi. Le tribunal a, par un jugement du 12 avril 2012, rejeté cette demande. M. A...se pourvoit en cassation contre ce jugement.<br/>
<br/>
              Sur la régularité du jugement attaqué :<br/>
<br/>
              2. M. A...soutient que le jugement est insuffisamment motivé en ce qu'il n'aurait pas répondu aux moyens tirés, en premier lieu, de ce qu'il était rémunéré à un taux horaire de 3,05 euros, inférieur au seuil minimum de rémunération, en deuxième lieu, de ce que l'administration et le concessionnaire n'avaient pas respecté l'évolution du taux horaire minimal entre 2006 et 2008, et, en troisième lieu, du caractère incomplet des livrets de travail établis par le concessionnaire. Il ajoute que le tribunal a insuffisamment motivé son jugement en ne se prononçant pas sur les relevés d'heures qu'il avait produits. Cependant, en exposant les particularités du contrat de concession de main-d'oeuvre, la détermination de la rémunération individuelle des détenus qui en découle, ainsi que les modalités de relevé des heures de travail qui conditionnent ce calcul, le tribunal administratif de Versailles, qui s'est bien prononcé sur l'existence d'une faute de l'administration et n'avait pas, à cette fin, à répondre à l'ensemble des arguments invoqués, n'a pas entaché son jugement d'insuffisance de motivation.<br/>
<br/>
              Sur le bien-fondé du jugement attaqué :<br/>
<br/>
              3. Aux termes de l'article 717-3 du code de procédure pénale : " Les relations de travail des personnes incarcérées ne font pas l'objet d'un contrat de travail ".  Les articles D. 102 et D. 103 de ce code, dans leur version applicable à l'espèce, disposent que : " L'organisation, les méthodes et les rémunérations du travail doivent se rapprocher autant que possible de celles des activités professionnelles extérieures afin notamment de préparer les détenus aux conditions normales du travail libre, " et que : " le travail est effectué dans les établissements pénitentiaires sous le régime du service général, de la concession de main-d'oeuvre pénale ou dans le cadre d'une convention conclue entre les établissements pénitentiaires et le service de l'emploi pénitentiaire. / Les relations entre l'organisme employeur et le détenu sont exclusives de tout contrat de travail (...)/ Les conditions de rémunération et d'emploi des détenus qui travaillent sous le régime de la concession ou pour le compte d'associations sont fixées par convention, en référence aux conditions d'emploi à l'extérieur, en tenant compte des spécificités de la production en milieu carcéral. " Son article D. 104 prévoit que : " Les concessions de travail à l'intérieur des établissements pénitentiaires font l'objet de clauses et conditions générales arrêtées par le ministre de la justice./ (...) Les concessions envisagées pour une durée supérieure à trois mois ou pour un effectif supérieur à cinq détenus font l'objet d'un contrat qui en fixe les conditions particulières notamment quant à l'effectif des détenus, au montant des rémunérations et à la durée de la concession. Ce contrat est signé par le représentant de l'entreprise concessionnaire et le directeur régional ".<br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond qu'en application, de la convention passée entre l'administration pénitentiaire et la société Grabout, pour laquelle travaillait M.A..., et du règlement intérieur des ateliers de concession de la maison d'arrêt de Fleury-Mérogis, la rémunération versée aux détenus admis à travailler sous le régime de la concession au sein de cette maison d'arrêt est calculée sur la base d'un seuil minimum de rémunération revalorisé chaque année, selon les tâches des divers ateliers. Toutes les rémunérations étant calculées et contrôlées à la fin de chaque jour de travail, l'administration établit en fin de chaque mois, pour chaque détenu, une feuille de rémunération récapitulant le cumul des gains journaliers, à partir des éléments qui lui sont transmis par le concessionnaire. Il en résulte que le seuil minimum de rémunération ne garantit pas un salaire minimum individuel, mais un minimum collectif moyen de rémunération, et que les dispositions le régissant, qui revêtent un caractère réglementaire, ne font pas obstacle à ce que la rémunération des détenus travaillant dans un même atelier puisse, selon leur productivité, être inférieure, égale ou supérieure à ce seuil minimum de rémunération.<br/>
<br/>
              5. En jugeant, en premier lieu, que M. A...ne contestait, pas, sur le principe, l'illégalité des modalités de détermination de sa rémunération en fonction de la cadence qui lui était imposée, et en regardant les écritures de l'intéressé comme contestant le caractère réalisable des cadences imposées, le tribunal administratif de Versailles, n'a, contrairement à ce que soutient le requérant, dénaturé ni les écritures produites devant lui, ni les termes du litige.<br/>
<br/>
              6. M. A...soutient, en deuxième lieu, que le tribunal aurait commis une erreur de droit et insuffisamment motivé son jugement en jugeant que la rémunération pouvait être légalement cadencée et en ne précisant pas les fondements légaux d'une telle rémunération. Mais ce moyen, nouveau en cassation, ne peut de ce fait qu'être écarté.<br/>
<br/>
              7. En troisième lieu, en jugeant que M. A...n'apportait aucune précision ni aucun élément susceptible de caractériser, en ce qui concerne l'établissement de sa rémunération, une méconnaissance des articles D. 102 et D. 103 du code de procédure pénale, alors, au demeurant, que la production des livrets de travail n'a pas été mise à la charge du requérant, le tribunal, qui n'a pas inversé la charge de la preuve, n'a entaché son jugement d'aucune erreur de droit, ni n'a méconnu les droits de la défense ou le droit à un procès équitable garanti par le premier paragraphe de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.<br/>
<br/>
              8. Dès lors, en quatrième lieu, que le seuil minimum de rémunération ne peut être regardé, pour les motifs rappelés au point 4, comme un salaire minimum individuel garanti à chacun des détenus, indépendamment de sa productivité, le moyen tiré de ce que le tribunal administratif de Versailles aurait commis une erreur de droit et dénaturé les pièces du dossier en ne regardant pas le système de rémunération en vigueur à la maison d'arrêt de Fleury-Mérogis, au motif que celui-ci conduirait à une rémunération individuelle inférieure au seuil de rémunération, comme constituant une faute de nature à engager la responsabilité de l'Etat, ne peut qu'être écarté.<br/>
<br/>
              9. En relevant, en cinquième lieu, qu'il résultait du règlement intérieur de l'établissement et du contrat de concession de main d'oeuvre pénale que le paiement intégral du taux horaire était subordonné à une cadence, et en estimant, par ailleurs, que M. A...n'établissait pas l'existence d'une rupture d'égalité, le tribunal, qui a suffisamment motivé son jugement sur ce point, n'a pas, non plus, entaché celui-ci d'erreur de droit.<br/>
<br/>
              10. Le moyen invoqué par M.A..., enfin, tiré de la méconnaissance des stipulations des articles 3 et 4 de la convention européenne de sauvegarde des droits de l'homme et libertés fondamentales, qui est nouveau en cassation, ne peut, pour ce motif, qu'être écarté.<br/>
<br/>
              11. Il résulte de ce qui précède que M. A...n'est pas fondé à demander l'annulation du jugement qu'il attaque. Son pourvoi doit donc être rejeté, y compris ses conclusions au titre des dispositions de l'article L.761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
