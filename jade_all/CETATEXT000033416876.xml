<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033416876</ID>
<ANCIEN_ID>JG_L_2016_11_000000390890</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/41/68/CETATEXT000033416876.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 16/11/2016, 390890, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-11-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390890</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX ; SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>Mme Catherine Bobo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:390890.20161116</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme I...A...veuveE..., Mme B...E...épouse C...et Mme G... E...épouse D...ont demandé au tribunal administratif de Strasbourg de mettre à la charge de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) la réparation des conséquences dommageables de la contamination de M. F...E...par le virus de l'hépatite C à la suite d'une transfusion sanguine. Par un jugement n° 1202374 du 15 avril 2014, le tribunal administratif de Strasbourg a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 14NC01149 du 9 avril 2015, la cour administrative d'appel de Nancy a rejeté l'appel des consorts E...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et deux mémoires complémentaires, enregistrés les 9 juin 2015, 9 septembre 2015 et 3 mars 2016 au secrétariat du contentieux du Conseil d'Etat, les consorts E...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de l'ONIAM la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la santé publique ;<br/>
<br/>
              - la loi n° 68-1250 du 31 décembre 1968 ;<br/>
<br/>
              - la loi n° 2016-41 du 26 janvier 2016 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Bobo, auditeur,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de Mme I...A...veuveE..., de Mme B...E...et de Mme G...E...et à la SCP Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. H... E...a reçu des transfusions de produits sanguins lors d'une intervention de chirurgie cardiaque à l'hôpital Broussais le 24 août 1981 ; qu'une hépatite C imputable à ces transfusions a été diagnostiquée en 1992 et a entraîné le décès de M. E... le 25 décembre 2000 ; que, le 1er décembre 2010, Mmes I...A..., B...C...et G...D...ont saisi l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM), sur le fondement des dispositions de l'article L. 1221-14 du code de la santé publique, d'une demande d'indemnisation des préjudices résultant pour elles de la contamination et du décès de leur époux, père et grand-père ; que l'ONIAM a opposé la prescription quadriennale à cette demande le 29 mars 2012 ; que, par un jugement du 15 avril 2014, le tribunal administratif de Strasbourg, saisi d'une demande dirigée contre cet établissement public, l'a rejetée au motif que la prescription était acquise ; que les consorts E...se pourvoient en cassation contre l'arrêt du 9 avril 2015 par lequel la cour administrative d'appel de Nancy a rejeté leur appel contre ce jugement ; <br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article L. 1142-28 du code de la santé publique, dans sa rédaction résultant du I de l'article 188 de la loi du 26 janvier 2016 : " (...) les demandes d'indemnisation formées devant l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales en application du II de l'article L. 1142-1 et des articles L. 1221-14, L. 3111-9, L. 3122-1 et L. 3131-4 se prescrivent par dix ans à compter de la consolidation du dommage " ; qu'aux termes du II du même article 188 de la loi du 26 janvier 2016, ces dispositions s'appliquent " lorsque le délai de prescription n'était pas expiré à la date de publication de la présente loi. Il est alors tenu compte du délai déjà écoulé. / Toutefois, lorsqu'aucune décision de justice irrévocable n'a été rendue, l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales applique le délai prévu au I aux demandes d'indemnisation présentées devant lui à compter du 1er janvier 2006 (...) " ; <br/>
<br/>
              3. Considérant qu'ainsi qu'il a été dit les consorts E...ont saisi l'ONIAM le 1er décembre 2010, soit postérieurement à la date fixée par les dispositions du II de l'article 188 de la loi du 26 janvier 2016 ; que, par l'effet de leur pourvoi, l'arrêt de la cour administrative d'appel de Nancy du 9 avril 2015 n'est pas devenu irrévocable ; que, dans ces conditions, il y a lieu d'appliquer au litige le délai de prescription de dix ans à compter de la consolidation du dommage prévu par les dispositions précitées de l'article L. 1142-28 du code de la santé publique modifié, alors même que ces dispositions ont été édictées postérieurement à la date à laquelle la cour administrative d'appel a statué ; <br/>
<br/>
              4. Considérant que les juges du fond ont fixé la date de consolidation au 25 décembre 2000, date du décès de M.E... ; que la demande d'indemnisation a été présentée moins de dix ans après cette date et n'était donc pas prescrite ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'arrêt attaqué, qui rejette la demande comme prescrite au motif qu'elle n'a pas été présentée dans le délai de quatre ans prévu par la loi du 31 décembre 1968, doit être annulé ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'ONIAM le versement aux requérants d'une somme globale de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 9 avril 2015 est annulé. <br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy.<br/>
Article 3 : L'ONIAM versera aux consorts E...une somme globale de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme I...A...veuveE..., Mme B...E...épouse C...et Mme G...E...épouse D...et à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
