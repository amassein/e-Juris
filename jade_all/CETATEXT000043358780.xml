<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043358780</ID>
<ANCIEN_ID>JG_L_2021_04_000000438762</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/35/87/CETATEXT000043358780.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 08/04/2021, 438762</TITRE>
<DATE_DEC>2021-04-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438762</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Sébastien Gauthier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:438762.20210408</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1°/ Sous le n° 438762, par une requête, enregistrée le 18 février 2020 au secrétariat du contentieux du Conseil d'Etat, les associations Pour Rassembler, Informer et Agir contre les Risques liés aux Technologies ElectroMagnétiques (PRIARTEM) et Agir pour l'environnement demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2019-1592 du 31 décembre 2019 modifiant le décret n° 2007-1532 du 24 octobre 2007 modifié relatif aux redevances d'utilisation des fréquences radioélectriques dues par les titulaires d'autorisations d'utilisation de fréquences délivrées par l'Autorité de régulation des communications électroniques et des postes (ARCEP) ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2°/ Sous le n° 443282, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 24 août et 7 décembre 2020, et le 14 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, la Société française du radiotéléphone (SFR) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2019-1592 du 31 décembre 2019 modifiant le décret n° 2007-1532 du 24 octobre 2007 modifié relatif aux redevances d'utilisation des fréquences radioélectriques dues par les titulaires d'autorisations d'utilisation de fréquences délivrées par l'Autorité de régulation des communications électroniques et des postes (ARCEP) ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 15 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la directive 2002/20/CE du Parlement et du Conseil du 7 mars 2002 ;<br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - le code des postes et des communications électroniques ;<br/>
              - la loi n° 2019-810 du 1er août 2019 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sébastien Gauthier, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public ; <br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les requêtes visées ci-dessus sont dirigées contre le même décret. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              Sur le litige :<br/>
<br/>
              2. Sur le fondement des articles L. 42-1 et L. 42-2 du code des postes et des communications électroniques, l'Autorité de régulation des communications électroniques et des postes et de la distribution de la presse (ARCEP) a proposé au ministre chargé des communications électroniques, le 21 novembre 2019, les modalités et les conditions d'attribution, par une procédure d'enchères, des autorisations d'utilisation de fréquences dans la bande 3,4 - 3,8 GHz en France métropolitaine pour établir et exploiter un réseau radioélectrique mobile ouvert au public, afin de permettre l'utilisation des technologies mobiles de cinquième génération, dite " 5G ".<br/>
<br/>
              3. Par un arrêté du 30 décembre 2019, la secrétaire d'Etat auprès du ministre de l'économie et des finances a repris les termes de cette proposition et fixé les prix de réserve des blocs de fréquences radioélectriques de 50 MHz et de 10 MHz pour la procédure d'enchères permettant d'attribuer les blocs de fréquences radioélectriques disponibles. <br/>
<br/>
              4. Par le décret attaqué du 31 décembre 2019, le Premier ministre a défini les modalités de calcul de la redevance due au titre de l'utilisation des fréquences de la bande<br/>
3,4 - 3,8 GHz. <br/>
<br/>
              Sur la recevabilité de la requête n° 438762 : <br/>
<br/>
              5. Le décret attaqué a pour seul objet de définir les différents éléments composant la redevance due par les titulaires d'autorisations d'utilisation de fréquences radioélectriques. Les associations Priartem et Agir pour l'environnement, qui ont pour objet la défense de l'environnement et la prévention des risques liés aux technologies électromagnétiques, ne justifient pas d'un intérêt leur donnant qualité pour former un recours pour excès de pouvoir contre ce décret. Les conclusions de la requête de ces deux associations doivent par suite être rejetées.<br/>
<br/>
              Sur les conclusions de la requête n° 443282 :<br/>
<br/>
              6. En premier lieu, aux termes du V de l'article L. 32-1 du CPCE : " Lorsque, dans le cadre des dispositions du présent code, le ministre chargé des communications électroniques et l'Autorité de régulation des communications électroniques, des postes et de la distribution de la presse envisagent d'adopter des mesures ayant une incidence importante sur un marché ou affectant les intérêts des utilisateurs finals, ils rendent publiques les mesures envisagées dans un délai raisonnable avant leur adoption et recueillent les observations qui sont faites à leur sujet. Le résultat de ces consultations est rendu public, sous réserve des secrets protégés par la loi. ".<br/>
<br/>
              7. Sur le fondement de ces dispositions, le Gouvernement a conduit une consultation publique du 28 novembre au 12 décembre 2019 portant sur le projet de décret litigieux et sur le projet d'arrêté définissant les modalités et les conditions d'attribution des autorisations d'utilisation de fréquence de la bande 3,4 - 3,8 GHz pour les réseaux de cinquième génération. <br/>
<br/>
              8. La société requérante soutient que cette consultation s'est déroulée dans des conditions irrégulières, faute que soit connue, avant leur publication au Journal officiel le samedi 7 décembre, la teneur des textes d'application de la loi du 1er août 2019 visant à préserver les intérêts de la défense et de la sécurité nationale de la France dans le cadre de l'exploitation des réseaux radioélectriques mobiles, qui a créé un régime d'autorisation pour les dispositifs matériels et logiciels nécessaires à l'exploitation des autorisations de fréquence de la bande 3,4 - 3,8 GHz pour les réseaux de cinquième génération. Toutefois, eu égard à l'objet de cette consultation et au fait que les caractéristiques du régime d'autorisation créé par la loi du 1er août 2019 étaient connues avant le début de la consultation, alors au demeurant que les projets de textes d'application de cette loi avaient eux-mêmes fait l'objet d'une consultation publique, la seule circonstance que les personnes intéressées n'aient pu en prendre connaissance que quelques jours avant la clôture de la consultation publique sur le projet de décret litigieux, n'a en tout état de cause pas rendu celle-ci irrégulière.<br/>
<br/>
              9. En deuxième lieu, contrairement à ce qui est soutenu, l'absence de prise en compte des conséquences de la loi du 1er août 2019 et de ses textes d'application par l'ARCEP dans sa proposition en date du 21 novembre 2019 et dans l'avis qu'elle a rendu le <br/>
17 décembre 201 ne saurait entacher d'irrégularité le projet de décret litigieux dont l'objet est distinct.<br/>
<br/>
              10. En troisième lieu, d'une part, aux termes de l'article 13, intitulé <br/>
" Redevances pour les droits d'utilisation et les droits de mettre en place des ressources " de la directive 2002/20/CE du Parlement européen et du Conseil du 7 mars 2002, dite " Directive Autorisation ", alors en vigueur : " Les États membres peuvent permettre à l'autorité compétente de soumettre à une redevance les droits d'utilisation des radiofréquences (...) afin de tenir compte de la nécessité d'assurer une utilisation optimale de ces ressources. / Les États membres font en sorte que ces redevances soient objectivement justifiées, transparentes, non discriminatoires et proportionnées eu égard à l'usage auquel elles sont destinées (...) ". D'autre part, il résulte des dispositions des articles L. 2124-26 et L. 2125-1 du code général de la propriété des personnes publiques que l'utilisation, par les titulaires d'autorisations, de fréquences radioélectriques disponibles sur le territoire de la République, constitue un mode d'occupation privatif du domaine public de l'Etat, qui donne lieu au paiement d'une redevance. Enfin, selon l'article L. 2125-3 du même code : " La redevance due pour l'occupation ou l'utilisation du domaine public tient compte des avantages de toute nature procurés au titulaire de l'autorisation ".<br/>
<br/>
              11. Ainsi qu'il a été dit au point 8, la loi du 1er août 2019 ainsi que le décret et l'arrêté pris pour son application ont créé un régime d'autorisation administrative, dans le but de préserver les intérêts de la défense et de la sécurité nationale, des dispositifs matériels et logiciels, installés à partir du 1er février 2019, nécessaires à l'exploitation des autorisations de fréquence de la bande 3,4 - 3,8 GHz pour les réseaux de cinquième génération.<br/>
<br/>
              12. La redevance créée par le décret attaqué se compose de parts fixes, dont les montants sont déterminés par le résultat de la phase d'attribution des blocs de fréquences de la bande 3,4 - 3,8 GHz et par celui de la phase d'enchères prévue par l'arrêté du <br/>
30 décembre 2019, et d'une part variable, dont le montant est fonction du chiffre d'affaires du titulaire de l'autorisation d'utilisation des fréquences.<br/>
<br/>
              13. En ne prenant en compte, dans la fixation des éléments composant la redevance, ni les conséquences des refus d'autorisation opposés sur le fondement de la loi du 1er août 2019 aux bénéficiaires d'autorisations d'utilisation de fréquences, ni les conséquences des refus opposés à l'installation d'équipements, lesquelles n'étaient au demeurant pas connues à la date de publication du décret, le Premier ministre n'a pas méconnu le principe d'égalité dès lors qu'il ne pouvait légalement tenir compte que des différences de situation objectives en rapport direct avec l'occupation du domaine public et qu'il ne pouvait, dès lors, définir des montants différents selon, en particulier, la situation économique et financière des futurs titulaires d'autorisations accordées à l'issue de la procédure d'enchères,.<br/>
<br/>
              14. Il résulte de tout ce qui précède que la société SFR n'est pas fondée à demander l'annulation du décret n° 2019-1592 du 31 décembre 2019 modifiant le décret n° 2007-1532 du 24 octobre 2007 modifié relatif aux redevances d'utilisation des fréquences radioélectriques dues par les titulaires d'autorisations d'utilisation de fréquences délivrées par l'Autorité de régulation des communications électroniques et des postes.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes des associations PRIARTEM, Agir pour l'environnement et SFR sont rejetées.<br/>
Article 2 : La présente décision sera notifiée aux associations PRIARTEM, Agir pour l'environnement, à la société SFR, à l'Autorité de régulation des communications électroniques, des postes et de la distribution de la presse, au ministère de l'économie, des finances et de la relance et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">51-02-004-02 POSTES ET COMMUNICATIONS ÉLECTRONIQUES. COMMUNICATIONS ÉLECTRONIQUES. - REDEVANCE DUE AU TITRE DE L'UTILISATION DES FRÉQUENCES NÉCESSAIRES À L'EXPLOITATION DE LA 5G (DÉCRET DU 31 DÉCEMBRE 2019) - ABSENCE DE PRISE EN COMPTE DES CONSÉQUENCES DES REFUS D'AUTORISATION PRÉALABLE D'EXPLOITATION DES APPAREILS PERMETTANT DE RELIER LES TERMINAUX AUX RÉSEAUX 5G (ART. L. 34-11 ET S. DU CPCE) [RJ1] - MÉCONNAISSANCE DU PRINCIPE D'ÉGALITÉ - ABSENCE.
</SCT>
<ANA ID="9A"> 51-02-004-02 La loi n° 2019-810 du 1er août 2019, codifiée aux articles L. 34-11 et suivants du code des postes et des communications électroniques (CPCE), ainsi que le décret n° 2019-1300 du 6 décembre 2019 et l'arrêté du même jour pris pour son application ont créé un régime d'autorisation administrative, dans le but de préserver les intérêts de la défense et de la sécurité nationale, des dispositifs matériels et logiciels, installés à partir du 1er février 2019, nécessaires à l'exploitation des autorisations de fréquence de la bande 3,4 - 3,8 GHz pour les réseaux de cinquième génération.... ,,La redevance due au titre de l'utilisation des fréquences de la bande 3,5 GHz pour l'exploitation d'un réseau mobile en France métropolitaine, créée par le décret n° 2019-1592 du 31 décembre 2019, se compose de parts fixes, dont les montants sont déterminés par le résultat de la phase d'attribution des blocs de fréquences de la bande 3,4 - 3,8 GHz et par celui de la phase d'enchères prévue par l'arrêté du 30 décembre 2019, et d'une part variable, dont le montant est fonction du chiffre d'affaires du titulaire de l'autorisation d'utilisation des fréquences.,,,En ne prenant en compte, dans la fixation des éléments composant la redevance, ni les conséquences des refus d'autorisation opposés sur le fondement de la loi du 1er août 2019 aux bénéficiaires d'autorisations d'utilisation de fréquences, ni les conséquences des refus opposés à l'installation d'équipements, lesquelles n'étaient au demeurant pas connues à la date de publication du décret n° 2019-1592, le Premier ministre n'a pas méconnu le principe d'égalité dès lors qu'il ne pouvait légalement tenir compte que des différences de situation objectives en rapport direct avec l'occupation du domaine public et qu'il ne pouvait, dès lors, définir des montants différents selon, en particulier, la situation économique et financière des futurs titulaires d'autorisations accordées à l'issue de la procédure d'enchères.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant des conséquences pour les opérateurs de ce régime d'autorisation préalable, CE, décision du même jour, Société Bouygues Telecom et Société SFR, n°s 442120 443279, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
