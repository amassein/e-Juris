<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034209380</ID>
<ANCIEN_ID>JG_L_2017_03_000000397035</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/20/93/CETATEXT000034209380.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 17/03/2017, 397035, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397035</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Grégory Rzepski</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:397035.20170317</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Versailles :<br/>
<br/>
              1°) d'annuler la décision implicite née du silence gardé par le maire de la commune de Saint-Martin-la-Garenne après la réception de sa réclamation indemnitaire préalable le 12 avril 2010 ;<br/>
<br/>
              2°) de condamner la commune de Saint-Martin-la-Garenne à lui verser la somme de 17 249,25 euros au titre des frais qu'elle a engagés pour la réparation du mur séparant sa propriété du chemin du Coudray ;<br/>
<br/>
              3°) de condamner la commune de Saint-Martin-la-Garenne à lui verser la somme de 2 500 euros en réparation du préjudice moral qu'elle a subi ;<br/>
<br/>
              4°) d'enjoindre à la commune de Saint-Martin-la-Garenne de prendre en charge l'entretien du chemin du Coudray, la réparation du mur de soutènement et l'interdiction de passage de gros tonnage sauf livraison de fioul aux riverains ;<br/>
<br/>
              5°) de mettre à la charge de la commune de Saint-Martin-la-Garenne le paiement d'une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par un jugement n° 1005351 du 25 mars 2014, le tribunal administratif de Versailles a condamné la commune de Saint-Martin-la-Garenne à verser à Mme A...les sommes de 17 249,25 euros au titre des frais de réfection du mur et 1 000 euros au titre de son préjudice moral.<br/>
<br/>
              Par un arrêt n° 14VE01551 du 17 décembre 2015, la cour administrative d'appel de Versailles a, sur appel de la commune de Saint-Martin-la-Garenne, annulé ce jugement et rejeté les conclusions indemnitaires de MmeA....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 16 février 2016 et 13 mai 2016 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la commune de Saint-Martin-la-Garenne ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Saint-Martin-la-Garenne la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Grégory Rzepski, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de Mme A...et à la SCP Waquet, Farge, Hazan, avocat de la commune de Saint-Martin-la-Garenne.<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que la propriété de Mme A...dans la commune de Saint-Martin-la-Garenne est située en contrebas du chemin rural du Coudray, dont elle est séparée par un mur ; qu'ayant constaté des désordres sur ce mur, Mme A...a fait procéder à des travaux de consolidation au cour de l'année 2009 ; que, par un jugement du 25 mars 2014, le tribunal administratif de Versailles a condamné la commune de Saint-Martin-la-Garenne à verser à Mme A...les sommes de 17 249,25 euros au titre des frais engagés pour la réparation du mur et de 2 500 euros au titre de son préjudice moral ; que, par un arrêt du 17 décembre 2015, la cour administrative d'appel de Versailles a, sur appel de la commune de Saint-Martin-la-Garenne, annulé ce jugement et rejeté la demande présentée devant le tribunal administratif de Versailles par Mme A...; que celle-ci se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              2. Considérant que la circonstance qu'un ouvrage n'appartienne pas à une personne publique ne fait pas obstacle à ce qu'il soit regardé comme une dépendance d'un ouvrage public s'il présente, avec ce dernier, un lien physique ou fonctionnel tel qu'il doive être regardé comme un accessoire indispensable de l'ouvrage; que si tel est le cas, la collectivité propriétaire de l'ouvrage public est responsable des conséquences dommageables causées par cet élément de l'ouvrage public ;  <br/>
<br/>
              3. Considérant que la cour administrative d'appel a relevé que le chemin du Coudray, quoique faisant partie, en sa qualité de chemin rural, du domaine privé de la commune de Saint-Martin-la-Garenne, revêtait le caractère d'un ouvrage public dès lors qu'il était affecté à la circulation publique ; qu'en jugeant qu'alors même qu'il soutiendrait l'ouvrage public constitué par le chemin rural, le mur litigieux ne pouvait être regardé comme un accessoire indispensable de cet ouvrage au motif qu'il n'appartenait pas à la commune de Saint-Martin-la-Garenne, la cour administrative d'appel de Versailles a commis une erreur de droit ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Saint-Martin-la-Garenne une somme de 3 000 euros à verser à Mme A... au titre de l'article L. 761-1 du code de justice administrative ; qu'en revanche, il n'y a pas lieu de faire droit aux conclusions présentées par la commune de Saint-Martin-la-Garenne au même titre ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 17 décembre 2015 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : La commune de Saint-Martin-la-Garenne versera une somme de 3 000 euros à Mme A... au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la commune de Saint-Martin-la-Garenne tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme B...A...et à la commune de Saint-Martin-la-Garenne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
