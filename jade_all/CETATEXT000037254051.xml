<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037254051</ID>
<ANCIEN_ID>JG_L_2018_07_000000418907</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/25/40/CETATEXT000037254051.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 26/07/2018, 418907, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418907</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BRIARD</AVOCATS>
<RAPPORTEUR>Mme Dominique Chelle </RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:418907.20180726</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A...B..., M. C...B...et M. D...B...ont demandé au juge des référés du tribunal administratif de Paris, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de l'arrêté du 18 janvier 2018 par lequel le préfet de la région d'Ile-de-France, préfet de Paris, les a mis en demeure de faire cesser définitivement, dans un délai de trois mois, l'occupation aux fins d'habitation du logement, dont ils sont propriétaires indivis, situé 21, rue Saint-Jacques à Paris. Par une ordonnance n°1801592/9 du 22 février 2018, le juge des référés a suspendu l'exécution de cet arrêté. <br/>
<br/>
              Par un pourvoi, enregistré le 9 mars 2018 au secrétariat du contentieux du Conseil d'Etat, le ministre des solidarités et de la santé  demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande des consortsB.... <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ; <br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dominique Chelle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Briard, avocat des consortsB....<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              2. Considérant qu'il résulte de ces dispositions que la condition d'urgence à laquelle est subordonné le prononcé d'une mesure de suspension doit être regardée comme remplie lorsque la décision administrative contestée préjudicie de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il en va ainsi, alors même que cette décision n'aurait un objet ou des répercussions que purement financiers et que, en cas d'annulation, ses effets pourraient être effacés par une réparation pécuniaire ; qu'il appartient au juge des référés, saisi d'une demande tendant à la suspension d'une telle décision, d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de celle-ci sur la situation de ce dernier ou, le cas échéant, des personnes concernées, sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ;<br/>
<br/>
              3. Considérant que pour retenir l'urgence, le juge des référés du tribunal administratif de Paris a estimé qu'il ressortait des pièces du dossier que, eu égard au comportement de la locataire, qui avait refusé plusieurs offres de relogement, l'exécution de l'arrêté attaqué imposerait à l'indivision propriétaire du logement de la laisser occuper durablement les lieux sans payer de loyer ; qu'en fondant ainsi l'urgence sur la seule perte de ces loyers, sans rechercher si cette privation, d'un montant de 322 euros mensuels hors charges, était, eu égard à la situation des requérants, de nature à caractériser une atteinte grave et immédiate à leurs intérêts, le juge des référés a entaché son ordonnance d'une erreur de droit ; que celle-ci doit, par suite, être annulée ; <br/>
<br/>
              4. Considérant qu'il a lieu dans les circonstances de l'espèce, de faire application de l'article L. 821-2 du code de justice administrative et de régler l'affaire au titre de la procédure de référé engagée ;<br/>
<br/>
              5. Considérant qu'en se bornant à faire valoir qu'ils se trouvent privés, par l'effet de l'arrêté attaqué et du refus de l'intéressée d'être relogée, du montant du loyer dû par l'occupante du local, sans apporter de précisions permettant d'apprécier la gravité de l'atteinte que cette privation porte à leurs intérêts, les consorts B...ne justifient pas d'une situation de nature à faire regarder comme satisfaite la condition d'urgence à laquelle est subordonné le prononcé d'une mesure de suspension, alors qu'un intérêt public s'attache à l'impératif de préserver la santé de l'occupante d'un logement impropre à l'habitation ; que, dès lors, l'une des conditions posées par l'article L. 521-1 du code de justice administrative n'étant pas remplie, la demande de suspension présentée par les consorts B...devant le tribunal administratif de Paris doit être rejetée ;<br/>
<br/>
              6. Considérant que les dispositions de l'article L.761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées par les consorts B...sur le fondement de ces dispositions tant en première instance qu'en cassation ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 22 février 2018 du juge des référés du tribunal administratif de Paris est annulée.<br/>
Article 2 : La demande présentée par les consorts B...devant le juge des référés du tribunal administratif de Paris est rejetée.<br/>
Article 3 : Les conclusions présentées par les consorts B...devant le Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la ministre des solidarités et de la santé et à M. A... B..., premier requérant dénommé. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
