<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032112591</ID>
<ANCIEN_ID>JG_L_2016_02_000000384424</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/11/25/CETATEXT000032112591.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème SSR, 26/02/2016, 384424</TITRE>
<DATE_DEC>2016-02-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384424</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLOCHE ; SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Charline Nicolas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2016:384424.20160226</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le syndicat mixte de chauffage urbain de la Défense a demandé au tribunal administratif de Paris de condamner la société de climatisation interurbaine de la Défense à lui verser la somme de 45 782 443,46 euros à la suite de l'expiration du contrat de concession signé le 28 février 1968 et relatif aux installations de chauffage et climatisation du site de la Défense. Par un jugement n° 0707354 du 31 décembre 2010, le tribunal a fait partiellement droit à sa demande et a condamné la société de climatisation interurbaine de la Défense à lui verser la somme de 28 536 000 euros HT.<br/>
<br/>
              Par un arrêt n° 11PA01119 du 11 juillet 2014, la cour administrative d'appel de Paris, saisie en appel par la société de climatisation interurbaine de la Défense et par la voie de l'appel incident par le syndicat mixte de chauffage urbain de la Défense, a condamné la société de climatisation interurbaine de la Défense à verser au syndicat mixte de chauffage urbain de la Défense la somme de 2 270 000 euros HT, réformé le jugement du tribunal en ce qu'il a de contraire à son arrêt et rejeté le surplus des conclusions des parties. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 11 septembre, 11 décembre 2014 et 9 septembre 2015 et 1er février 2016 au secrétariat du contentieux du Conseil d'Etat, le syndicat mixte de chauffage urbain de la Défense (SICUDEF) demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) de mettre à la charge de la société de climatisation interurbaine de la Défense la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 93-122 du 29 janvier 1993 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Charline Nicolas, auditeur,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boulloche, avocat du syndicat mixte de chauffage urbain de la Défense, et à la SCP Odent, Poulet, avocat de la société de climatisation interurbaine de la Défense ;<br/>
<br/>
<br/>
<br/>1. Considérant que par une convention conclue le 22 février 1968, le syndicat mixte de chauffage urbain de la Défense (SICUDEF) a confié à la société de climatisation interurbaine de la Défense (CLIMADEF) la production et la distribution de chaud et de froid dans le quartier de la Défense ; que cette concession venait d'être prorogée pour une durée de vingt ans à compter du 1er septembre 1982, par un avenant du 10 mars 1994, lorsqu'est survenue, le 30 mars 1994, l'explosion accidentelle d'une chaudière, sur le site de la centrale, située au 2 rue d'Alençon, à Nanterre (Hauts-de-Seine) ; qu'à la suite de cet accident, outre les dispositions prises dans l'urgence pour assurer la continuité du service, les parties ont prévu les conditions du rétablissement des moyens de production et de distribution par une convention signée le 11 décembre 1995 ; qu'un avenant n° 1, s'y substituant, a été adopté le 7 juin 1996 ; que cet avenant prévoyait notamment la répartition à l'avenir des moyens de production sur deux sites avec une implantation nouvelle située rue Noël Pons à Nanterre, le financement du projet par le concessionnaire sans aucune modification tarifaire, un allongement de la durée de la convention pour assurer l'amortissement des investissements supplémentaires, dans le respect des dispositions de l'article 40 de la loi du 29 janvier 1993 relative à la prévention de la corruption et à la transparence de la vie économique et des procédures publiques, et, enfin, des conditions suspensives à la prorogation, à lever par le SICUDEF ; que, toutefois, par une délibération du 30 juin 1998, le SICUDEF a estimé que si la société CLIMADEF avait bien contracté une promesse de vente portant sur le terrain de la rue Noël Pons, son projet ne pouvait être agréé et les conditions suspensives prévues par l'avenant n° 1 ne pouvaient être levées, notamment en ce qui concerne le respect des conditions posées par l'article 40 de la loi du 29 janvier 1993 ; que, par la même délibération, le SICUDEF a décidé de lancer un appel à la concurrence pour la réattribution de la concession à compter du 1er septembre 2002 ; <br/>
<br/>
              2. Considérant qu'en vue du règlement de la concession, le SICUDEF a saisi le tribunal administratif de Paris d'une demande tendant à être indemnisé de la valeur des biens de retour dont il estime avoir été privé à l'issue de la concession ainsi que du solde du compte de provision pour travaux figurant au bilan de son concessionnaire ; que le tribunal administratif de Paris a fait partiellement droit à la demande du SICUDEF et a condamné la société CLIMADEF à lui verser la somme de 28 536 000 euros HT ; que par l'arrêt attaqué, la cour administrative d'appel de Paris a ramené cette somme à 2 270 000 euros HT ;  <br/>
<br/>
              Sur la régularité de l'arrêt :<br/>
<br/>
              3. Considérant qu'il ressort de l'arrêt attaqué que la cour administrative d'appel de Paris n'a pas répondu à la demande du SICUDEF de nommer un nouvel expert, alors qu'il ressort des pièces du dossier soumis aux juges du fond que cette demande avait été présentée dans ses écritures d'appel ; que, toutefois, le juge du fond, saisi de conclusions prescrivant une expertise, peut les rejeter par prétérition ; que le moyen d'insuffisance de motivation dirigé sur ce point contre l'arrêt attaqué doit, par suite, être écarté ; <br/>
<br/>
              Sur l'indemnité due au SICUDEF au titre des biens de retour :<br/>
<br/>
              4. Considérant que, dans le cadre d'une délégation de service public ou d'une concession de travaux mettant à la charge du cocontractant les investissements correspondant à la création ou à l'acquisition des biens nécessaires au fonctionnement du service public, l'ensemble de ces biens, meubles ou immeubles, appartient, dans le silence de la convention, dès leur réalisation ou leur acquisition à la personne publique ; qu'à l'expiration de la convention, les biens qui sont entrés, en application des principes énoncés ci-dessus, dans la propriété de la personne publique et ont été amortis au cours de l'exécution du contrat font nécessairement retour à celle-ci gratuitement, sous réserve des clauses contractuelles permettant à la personne publique, dans les conditions qu'elle détermine, de faire reprendre par son cocontractant les biens qui ne seraient plus nécessaires au fonctionnement du service public ; que, par suite, en l'absence de telles clauses, ces biens, qui ont été nécessaires au fonctionnement du service concédé à un moment quelconque de l'exécution de la convention, font retour à la personne publique à l'expiration de celle-ci, quand bien même ils ne sont plus alors nécessaires au fonctionnement du service public concédé ; <br/>
<br/>
              5. Considérant, en premier lieu, qu'il ressort des pièces du dossier soumis aux juges du fond qu'aux termes de l'article 1er du cahier des charges annexé à la convention, dans sa version en date du 10 mars 1994 : " La concession a pour objet : / - la réalisation (...) des installations de production et de distribution de chaleur et de froid destinées à assurer le chauffage, la préparation de l'eau chaude sanitaire et les climatisations dans les bâtiments à édifier sur les parties de la zone EPAD définie à l'article 1er de la convention de concession et, accessoirement, la réalisation des installations de production d'électricité. / - l'exploitation et l'entretien de ces installations pendant la durée de la concession " ; que l'article 4 de ce même cahier stipule : " Les ouvrages concédés sont les suivants : / - la centrale de production de chaleur et de froid édifiée, sur le terrain pour lequel la SNCF a donné une autorisation à CDF et GDF à cet usage, avec tout l'appareillage destiné à la production et au transport de la chaleur, du froid et de l'électricité ; / - les ouvrages et les équipements nécessaires au stockage et à la manutention du combustible ; / - les ouvrages et équipements nécessaires au stockage et à la manutention du combustible ; / - les branchements et postes de livraison tels que définis à l'article 26 ; / - l'ensemble des biens (meubles et immeubles) et installations indispensables à l'exploitation et au fonctionnement de la concession qui existaient à la date de mise en vigueur de la convention ou seraient acquis ou établis ultérieurement, notamment les extensions et renforcements réalisés en cours de concession à l'intérieur ou au-delà du périmètre concédé " ; qu'enfin, aux termes de l'article 50 du même cahier des charges : " A l'expiration de la concession ou en cas de rachat, le concessionnaire sera tenu de remettre au concédant, en état normal d'entretien, tous les biens et équipements qui font partie intégrante de la concession, tels qu'ils figurent à l'inventaire défini à l'article ci-dessus et quelle que soit leur affectation. Cette remise est faite sans indemnité à l'exclusion des dispositions prévues à l'alinéa ci-dessous " ; que la cour administrative d'appel de Paris a relevé, dans l'arrêt attaqué, qu'il résultait des stipulations précitées de l'article 1er du cahier des charges de la concession que l'installation de production d'électricité par cogénération relevait, à titre accessoire, du périmètre de la concession et formait un complément normal de l'activité concédée de production et de distribution de chaleur et de froid et que, si elle présentait une utilité pour le service, en tant qu'elle contribuait à l'alimentation en électricité de la centrale et générait des ressources complémentaires tirées de la vente à un tiers de l'électricité produite, elle ne pouvait, pour autant, être regardée comme un bien nécessaire au fonctionnement du service public ; que, ce faisant, et alors, au surplus, que ni la convention, ni son cahier des charges n'imposait au concessionnaire d'obligation relative à la production d'électricité, notamment en termes de quantité et de mode de production, et ne prévoyait de contrôle du concessionnaire sur l'activité du concédant dans ce domaine, la cour, qui a souverainement apprécié les faits qui lui étaient soumis sans les dénaturer, n'a commis ni erreur de droit, ni erreur de qualification juridique des faits et n'a pas entaché son arrêt de contradiction de motifs ; que c'est à titre surabondant que la cour a relevé qu'il pouvait être recouru, en l'absence d'installation de cogénération, à un approvisionnement extérieur en électricité, comme cela avait été le cas entre 1994 et 2002, et que la part d'électricité par cogénération n'avait représenté, au cours de l'exploitation, qu'une part limitée dans les divers services offerts par le concessionnaire ; que, par suite, les moyens dirigés contre ces motifs sont inopérants ; <br/>
<br/>
              6. Considérant, en deuxième lieu, qu'il ressort des pièces du dossier soumis aux juges du fond qu'en application de l'article 8 du cahier des charges de la concession, l'installation de production de chaleur devait justifier d'une puissance de 279 MW ; qu'il n'est pas contesté que l'explosion accidentelle du 30 mars 1994 a détruit une partie des installations utilisées pour la production de chaleur, qui n'ont été que partiellement reconstruites ; qu'en application des principes énoncés au point 4, la circonstance que des progrès en économie d'énergie ne rendaient plus nécessaires au fonctionnement du service concédé les chaufferies permettant la production de chaleur d'une puissance totale de 279 MW est sans incidence sur la qualification des biens qui ont, à un moment donné de l'exécution de la concession, été nécessaires à la production d'une telle puissance ; que, par suite, en se fondant sur une telle circonstance pour en déduire que seule une chaudière supplémentaire, dont la puissance demeurait nécessaire à la poursuite du service concédé, devait faire retour au concédant, la cour administrative d'appel de Paris a entaché son arrêt d'erreur de droit ; que, saisie d'une demande tendant à l'indemnisation de la valeur de biens de retour, il lui appartenait de rechercher si, alors que des installations revêtant le caractère de biens de retour avaient été détruites, la collectivité concédante avait entendu, au titre de ses prérogatives et pouvoirs dans l'exécution de la concession, renoncer à la reconstitution de ces biens et accepter une diminution de la puissance thermique prévue à l'origine ; <br/>
<br/>
              7. Considérant, en troisième et dernier lieu, qu'il ressort des pièces du dossier soumis aux juges du fond que la société CLIMADEF a acquis, au cours de la concession, le terrain situé rue d'Alençon à Nanterre ; que le terrain en cause, qui a été acquis aux fins d'exécution de la concession, constitue le terrain d'assiette de la centrale de production de chaleur et de froid, ouvrage de la concession ; que, dans ces conditions, la cour administrative d'appel de Paris, en jugeant que le terrain en cause ne constituait pas un bien nécessaire au fonctionnement du service public en cause et en rejetant, pour ce motif, la demande indemnitaire du SICUDEF présentée au titre de la moins value sur ce terrain, a commis une erreur de qualification juridique ; <br/>
<br/>
              Sur le rejet de l'appel incident du SICUDEF :<br/>
<br/>
              8. Considérant qu'aux termes de l'article 45 du cahier des charges de la concession : " Outre la tenue du compte de résultat, du bilan et de ses annexes, le concessionnaire établit extra-comptablement un compte conventionnel appelé " fonds de travaux d'extension, de renouvellement, et de gros entretien " (...) En fin d'exercice, le compte est crédité : 1) d'un montant égal aux droits de raccordements encaissés par le concessionnaire au cours de l'exercice / 2) d'un montant annuel égal au onzième de 45 790 609,10 francs représentant le solde au 1er octobre 1991 du montant des droits de raccordement affectés antérieurement au 1er septembre 1982 à l'apurement des déficits des comptes d'exploitation de la concession (...) / 3) à compter du 1er octobre 1992, du produit du solde des engagements figurant à l'alinéa 2) ci-dessus (...) par un taux égal au taux accordé par la Caisse des dépôts aux collectivités territoriales pour les emprunts d'une durée de dix ans / En fin d'exercice, le compte est débité d'un montant égal aux travaux d'extension et de renouvellement portés en immobilisation au cours de l'exercice et aux travaux de gros entretien réalisés dans l'exercice. / Si, en début d'exercice le solde prévisionnel du compte conventionnel, après déduction éventuelle d'imputations antérieures, s'avérait insuffisant pour absorber l'imputation prévue au titre de l'exercice considéré, le concessionnaire devrait obtenir du concédant un accord sur les dispositions envisagées pour faire face aux travaux dont le montant sera imputé au compte conventionnel ; Au terme de la concession, si le compte conventionnel fait apparaître un solde créditeur, le concessionnaire verse au concédant une somme égale au montant ainsi déterminé " ; qu'après avoir relevé, au terme d'une appréciation souveraine, que le solde de ce compte était déficitaire en fin d'exercice, la cour administrative d'appel de Paris a pu juger sans dénaturer les stipulations précitées que le SICUDEF ne pouvait, par suite, prétendre au versement d'une somme représentant le solde de ce compte et rejeter, par voie de conséquence, l'appel incident du SICUDEF ; <br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que l'arrêt de la cour administrative d'appel de Paris doit être annulé seulement en tant qu'il statue sur l'indemnité à verser au SICUDEF au titre de l'installation d'une chaufferie complémentaire et sur la moins-value du terrain situé rue d'Alençon à Nanterre ; <br/>
<br/>
              10. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge du SICUDEF, qui n'est pas, dans la présente instance, la partie perdante ; qu'en revanche, il y a lieu de mettre à la charge de la société CLIMADEF la somme de 3 000 euros au titre de ces mêmes dispositions ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 11 juillet 2014 est annulé en tant qu'il statue sur la demande indemnitaire au titre de l'installation d'une chaufferie supplémentaire et au titre de la moins-value du terrain situé rue d'Alençon à Nanterre. <br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Paris.  <br/>
Article 3 : La société de climatisation interurbaine de la Défense versera au syndicat mixte de chauffage urbain de la Défense une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. Ses conclusions présentées au même titre sont rejetées. <br/>
Article 4 : Le surplus des conclusions du pourvoi du syndicat mixte de chauffage urbain de la Défense est rejeté. <br/>
Article 5 : La présente décision sera notifiée au syndicat mixte de chauffage urbain de la Défense et à la société de climatisation interurbaine de la Défense.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">24-01-02-03 DOMAINE. DOMAINE PUBLIC. RÉGIME. CONSÉQUENCES DU RÉGIME DE LA DOMANIALITÉ PUBLIQUE SUR D'AUTRES LÉGISLATIONS. - DÉLÉGATIONS DE SERVICE PUBLIC ET CONCESSIONS DE TRAVAUX - BIENS DITS DE RETOUR [RJ1] - 1) CIRCONSTANCE QUE DES PROGRÈS TECHNIQUES INTERVENUS AU COURS DE L'EXÉCUTION DE LA CONCESSION NE RENDAIENT PLUS NÉCESSAIRES AU FONCTIONNEMENT DU SERVICE CERTAINS BIENS - CIRCONSTANCE SANS INCIDENCE SUR LEUR QUALIFICATION - 2) CAS D'UNE DESTRUCTION AU COURS DE L'EXÉCUTION DE LA CONCESSION - APPRÉCIATION À PORTER SUR UNE DEMANDE D'INDEMNISATION DE LA VALEUR DE BIENS DE RETOUR.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39 MARCHÉS ET CONTRATS ADMINISTRATIFS. - DÉLÉGATIONS DE SERVICE PUBLIC ET CONCESSIONS DE TRAVAUX - BIENS DITS DE RETOUR [RJ1] - 1) CIRCONSTANCE QUE DES PROGRÈS TECHNIQUES INTERVENUS AU COURS DE L'EXÉCUTION DE LA CONCESSION NE RENDAIENT PLUS NÉCESSAIRES AU FONCTIONNEMENT DU SERVICE CERTAINS BIENS - CIRCONSTANCE SANS INCIDENCE SUR LEUR QUALIFICATION - 2) CAS D'UNE DESTRUCTION AU COURS DE L'EXÉCUTION DE LA CONCESSION - APPRÉCIATION À PORTER SUR UNE DEMANDE D'INDEMNISATION DE LA VALEUR DE BIENS DE RETOUR.
</SCT>
<ANA ID="9A"> 24-01-02-03 1) Cahier des charges d'une concession prévoyant que l'installation de production de chaleur devait justifier d'une puissance de 279 MW. La circonstance que des progrès en économie d'énergie ne rendaient plus nécessaires au fonctionnement du service concédé les chaufferies permettant la production de chaleur d'une puissance totale de 279 MW est sans incidence sur la qualification des biens qui ont, à un moment donné de l'exécution de la concession, été nécessaires à la production d'une telle puissance. Erreur de droit du juge qui se fonde sur cette circonstance pour en déduire que seules les installations dont la puissance demeurait nécessaire à la poursuite du service concédé devaient faire retour au concédant.,,,2) Une explosion accidentelle a détruit une partie des installations de production de chaleur nécessaires au fonctionnement du service. Saisi,  à l'issue de la concession, d'une demande du concédant tendant à l'indemnisation de la valeur de biens de retour, il appartient au juge de rechercher si, alors que des installations revêtant le caractère de biens de retour avaient été détruites, la collectivité concédante avait entendu, au titre de ses prérogatives et pouvoirs dans l'exécution de la concession, renoncer à la reconstitution de ces biens et accepter une diminution de la puissance thermique prévue à l'origine.</ANA>
<ANA ID="9B"> 39 1) Cahier des charges d'une concession prévoyant que l'installation de production de chaleur devait justifier d'une puissance de 279 MW. La circonstance que des progrès en économie d'énergie ne rendaient plus nécessaires au fonctionnement du service concédé les chaufferies permettant la production de chaleur d'une puissance totale de 279 MW est sans incidence sur la qualification des biens qui ont, à un moment donné de l'exécution de la concession, été nécessaires à la production d'une telle puissance. Erreur de droit du juge qui se fonde sur cette circonstance pour en déduire que seules les installations dont la puissance demeurait nécessaire à la poursuite du service concédé devaient faire retour au concédant.,,,2) Une explosion accidentelle a détruit une partie des installations de production de chaleur nécessaires au fonctionnement du service. Saisi,  à l'issue de la concession, d'une demande du concédant tendant à l'indemnisation de la valeur de biens de retour, il appartient au juge de rechercher si, alors que des installations revêtant le caractère de biens de retour avaient été détruites, la collectivité concédante avait entendu, au titre de ses prérogatives et pouvoirs dans l'exécution de la concession, renoncer à la reconstitution de ces biens et accepter une diminution de la puissance thermique prévue à l'origine.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, Assemblée, 21 décembre 2012, Commune de Douai, n° 342788, p. 477.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
