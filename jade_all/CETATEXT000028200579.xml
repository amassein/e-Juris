<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028200579</ID>
<ANCIEN_ID>JG_L_2013_11_000000349879</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/20/05/CETATEXT000028200579.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 06/11/2013, 349879, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-11-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349879</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Eric Aubry</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier De Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:349879.20131106</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 6 juin et 30 août 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme C...A...épouseB..., demeurant... ; Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10PA01654 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant à l'annulation du jugement n°0601466/3-3 du 3 février 2010 du tribunal administratif de Paris en tant qu'il a rejeté ses conclusions tendant à ce qu'il soit enjoint au préfet de police de lui accorder le bénéfice du regroupement familial pour ses deux enfants et aux autorités consulaires françaises en Chine de délivrer un visa à ses enfants ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu l'ordonnance n° 45-2658 du 2 novembre 1945 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Eric Aubry, Conseiller d'Etat, <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que Mme A..., épouseB..., qui était alors de nationalité chinoise et titulaire d'une carte de résident, a déposé, le 13 décembre 2001, une demande en vue de solliciter le bénéfice du regroupement familial pour ses enfants, Jian Hu et Guanghui Hu, demeurant ...; que le préfet de police a rejeté cette demande le 19 mars 2004 ; que le ministre de l'emploi, du travail et de la cohésion sociale a rejeté les 30 mai et 25 novembre 2005 le recours hiérarchique de Mme A...contre cette décision ; que, par jugement en date du 3 février 2010, le tribunal administratif de Paris a annulé, à la demande de MmeA..., la décision du ministre en date du 25 novembre 2005 mais a rejeté ses conclusions tendant à ce qu'il soit enjoint au préfet de police d'autoriser le regroupement familial puis aux autorités consulaires françaises en Chine de délivrer un visa à ses enfants ; que par un arrêt du 28 mars 2011 contre lequel Mme A...se pourvoit en cassation, la cour administrative d'appel de Paris a rejeté l'appel que celle-ci avait formé contre ce jugement, en tant qu'il avait rejeté ses conclusions à fin d'injonction ; <br/>
<br/>
              2. Considérant, en premier lieu, que, d'une part, aux termes de l'article L. 911-1 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution. " ; que, d'autre part, aux termes de l'article 29 de l'ordonnance du 2 novembre 1945 relative aux conditions d'entrée et de séjour des étrangers en France alors applicable, devenu l'article L. 411-2 du code de l'entrée et du séjour des étrangers et du droit d'asile : " I. - Le ressortissant étranger qui séjourne régulièrement en France depuis au moins un an, sous couvert d'un des titres d'une durée de validité d'au moins un an prévus par la présente ordonnance ou par des conventions internationales, peut demander à bénéficier de son droit à être rejoint, au titre du regroupement familial, par son conjoint et les enfants du couple mineurs de dix-huit ans. (...) " ; que le juge de l'injonction, saisi de conclusions présentées au titre de l'article L. 911-1 du code de justice administrative, est tenu de statuer sur ces conclusions en tenant compte de la situation de droit et de fait existant à la date de sa décision ;<br/>
<br/>
              3. Considérant que pour rejeter l'appel formé par Mme A...contre le jugement du 3 février 2010, en tant que ce jugement rejette ses conclusions tendant à ce qu'il soit enjoint au préfet de police d'autoriser le regroupement familial puis aux autorités consulaires françaises en Chine de délivrer un visa à ses enfants, la cour administrative d'appel de Paris a relevé que Mme A...avait acquis la nationalité française en 2007 et que sa demande ne relevait plus, en tout état de cause, des dispositions du code de l'entrée et du séjour des étrangers et du droit d'asile relatives au regroupement familial ; qu'en se fondant sur un tel motif, la cour administrative d'appel de Paris n'a pas commis d'erreur de droit dès lors que l'acquisition par Mme A...de la nationalité française faisait par elle-même obstacle à ce qu'elle puisse bénéficier d'une autorisation de regroupement familial pour ses enfants sur le fondement de l'article L. 411-2 du code de l'entrée et du séjour des étrangers en France, qui n'est applicable qu'aux ressortissants étrangers ;<br/>
<br/>
              4. Considérant, en second lieu, que le moyen tiré de la violation, par le jugement du 3 février 2010 du tribunal administratif de Paris, des articles 8 et 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, relatifs respectivement au droit à la vie privée et familiale et au droit au recours, n'a pas été invoqué devant la cour administrative d'appel de Paris ; que, par suite, Mme A...ne peut utilement soulever ce moyen pour contester le bien-fondé de l'arrêt qu'elle attaque ; <br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que Mme A...n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
 Article 1er : Le pourvoi de Mme A...est rejeté. <br/>
 Article 2 : La présente décision sera notifiée à Mme C...A...et au ministre de l'intérieur. <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
