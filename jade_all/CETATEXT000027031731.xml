<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027031731</ID>
<ANCIEN_ID>JG_L_2013_02_000000353917</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/03/17/CETATEXT000027031731.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème sous-section jugeant seule, 04/02/2013, 353917, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-02-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353917</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS</AVOCATS>
<RAPPORTEUR>M. Fabrice Aubert</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:353917.20130204</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la décision du 23 mars 2012 par laquelle le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions de M. B...A...dirigées contre l'arrêt n° 11BX00002 du 6 septembre 2011 de la cour administrative d'appel de Bordeaux en tant qu'il a statué sur la légalité des titres de perception n° 728 du 12 juillet 2000 et n° 1805 du 3 octobre 2002 ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le décret du 29 octobre 1936 modifié ;<br/>
<br/>
              Vu le décret n° 58-430 du 11 avril 1958 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabrice Aubert, Auditeur,  <br/>
<br/>
              - les observations de Me Haas, avocat de M.A...,<br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Haas, avocat de M. A...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 5 du décret du 11 avril 1958 alors en vigueur : " Lorsque le compte de cumul arrêté dans les conditions fixées ci-dessus fait apparaître soit un dépassement de la limite de cumul des rémunérations, soit le cumul des émoluments qui ne peuvent être perçus qu'au titre d'un seul emploi, un relevé de compte est adressé à l'agent. Le relevé du compte arrêté au 31 décembre est renvoyé au plus tard le 30 juin de l'année suivante. Si le compte est clos en cours d'année, le relevé est envoyé dans le délai de six mois suivant l'arrêté du compte. (..) / Dans le délai d'un mois, l'intéressé doit renvoyer le relevé communiqué revêtu d'une mention reconnaissant son exactitude ou faire connaître ses observations. Dans ce dernier cas, la collectivité vérifie le relevé et le transmet à nouveau à l'agent après l'avoir éventuellement modifié. L'intéressé doit faire connaître son acquiescement ou ses observations dans le même délai. / Tout relevé qui n'a pas été renvoyé dans les délais prévus est réputé certifié exact et complet par l'intéressé. / En cas de désaccord persistant, l'organisme qui tient le compte notifie à l'agent le montant auquel est arrêté le relevé. (..) " ; <br/>
<br/>
              2. Considérant que le délai de six mois prévu au premier alinéa de l'article 5 du décret du 11 avril 1958 cité ci-dessus, qui a pour double objet de fixer les modalités d'application de l'article 12 du décret du 29 octobre 1936 et d'instituer une procédure garantissant les droits des intéressés, est prescrit à peine de nullité de la procédure engagée après son expiration et des ordres de reversement consécutifs à cette procédure ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.A..., trésorier principal du Trésor public, occupait l'emploi de chef de poste à la trésorerie de Cenon, en Gironde, avant de prendre sa retraite le 7 juillet 2000 ; que l'administration lui a notifié, au titre du cumul de ses rémunérations pour les années 1999 et 2000, des relevés de comptes de cumul, et a par suite émis respectivement le 12 juillet 2000 pour 5 902,44 euros et le 3 octobre 2002 pour 9 656 euros des titres de perception correspondant aux trop perçus de rémunération pour les années en cause ; que par l'arrêt qu'il conteste, la cour administrative d'appel de Bordeaux, confirmant un jugement du tribunal administratif de Bordeaux, a rejeté les conclusions tendant à l'annulation de ces titres de perception ;<br/>
<br/>
              Sur les conclusions dirigées contre l'arrêt en tant qu'il statue sur le titre de perception émis au titre du compte de cumul pour l'année 1999 :<br/>
<br/>
              4. Considérant que, pour rejeter les conclusions de M.A..., la cour a constaté que le compte de cumul pour l'année 1999 lui avait été adressé le 15 juin 2000, soit dans le délai de six mois mentionné par les dispositions citées ci-dessus ; que, par suite, l'unique moyen du pourvoi sur ce point tiré de ce que la cour aurait commis une erreur de droit dans l'application de ces dispositions ne peut qu'être écarté ; que les conclusions tendant à l'annulation de l'arrêt sur ce point doivent par suite être rejetées ;<br/>
<br/>
              Sur les conclusions dirigées contre l'arrêt en tant qu'il statue sur le titre de perception émis au titre du compte de cumul pour l'année 2000 :<br/>
<br/>
              5. Considérant que pour rejeter ces conclusions de M.A..., la cour a retenu que le délai d'établissement du compte de cumul n'était pas prescrit à peine de nullité ; qu'elle a, ce faisant, commis une erreur de droit ; que dès lors, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'arrêt attaqué doit être annulé en tant qu'il a statué sur la légalité du titre de perception émis au titre du compte de cumul pour l'année 2000 ; <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application de l'article L. 821-2 du code de justice administrative et de régler l'affaire au fond dans cette mesure ; <br/>
<br/>
              7. Considérant qu'aux termes de l'article 6 du décret du 11 avril 1958 précité, alors en vigueur et applicable à l'espèce : " Les rémunérations sont inscrites au compte individuel de cumul de l'année de payement. / Toutefois, elles sont inscrites au compte de cumul de l'année du service fait, lorsque l'intéressé en fait la demande. " ; qu'il résulte de ces dispositions, combinées avec celles, précitées, du deuxième alinéa de l'article 5 du même décret, que, lorsque l'intéressé souhaite bénéficier de l'inscription des rémunérations au compte individuel de cumul de l'année du service fait, il doit en faire la demande à l'administration dans le délai d'un mois après réception de son relevé de compte ;<br/>
<br/>
              8. Considérant qu'il résulte de l'instruction que M. A...a demandé par un courrier du 12 novembre 2002 le rattachement à l'année du service fait des rémunérations qu'il a perçues au titre notamment de l'année 1999 ; que cette demande, dont il n'est pas contesté qu'elle est intervenue au-delà du délai d'un mois après la réception par M. A...de son relevé de compte de cumul pour cette année, établi au plus tard le 15 mai 2001, était tardive ; que, dès lors, M. A...n'est, en tout état de cause, pas fondé à soutenir que le titre de perception émis en vue du recouvrement des sommes dépassant la limite du cumul autorisé, serait intervenu à l'issue d'une procédure irrégulière en raison du refus de l'administration de rapporter à l'année du service fait le montant des rémunérations accessoires perçues, pour déterminer le plafond de référence du cumul ;<br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que M. A...n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Bordeaux a rejeté sa demande d'annulation du titre de perception émis à son encontre au titre du compte de cumul pour l'année 2000 ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas la partie perdante, la somme demandée à ce titre par M.A... ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 6 septembre 2011 de la cour administrative d'appel de Bordeaux est annulé en tant qu'il a statué sur la légalité du titre de perception n° 1805 du 3 octobre 2002 émis au titre du compte de cumul pour l'année 2000. <br/>
Article 2 : Les conclusions de la requête de M. A...devant la cour administrative d'appel de Bordeaux tendant à l'annulation du titre de perception n° 1805 du 3 octobre 2002 sont rejetées.<br/>
Article 3 : Le surplus des conclusions du pourvoi de M. A...est rejeté.<br/>
Article 4 : La présente décision sera notifiée à M. B...A...et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
