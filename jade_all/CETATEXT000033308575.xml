<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033308575</ID>
<ANCIEN_ID>JG_L_2016_10_000000390790</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/30/85/CETATEXT000033308575.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème chambre jugeant seule, 20/10/2016, 390790, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-10-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390790</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème chambre jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Matias de Sainte Lorette</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:390790.20161020</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme C...E...ont demandé au tribunal administratif de Limoges de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles ils ont été assujettis au titre des années 2007, 2008 et 2009. Par un jugement n° 1101760 du 27 juin 2013, le tribunal administratif de Limoges a fait droit à leur demande.<br/>
<br/>
              Par un arrêt n° 13BX02674 du 7 avril 2015, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par le ministre des finances et des comptes publics contre ce jugement. <br/>
<br/>
              Par un pourvoi et un nouveau mémoire, enregistré les 5 juin 2015 et 6 juillet 2016 au secrétariat du contentieux du Conseil d'Etat, le ministre des finances et des comptes publics demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matias de Sainte Lorette, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de M. et Mme E...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le I de l'article 31 du code général des impôts dispose que : " Les charges déductibles pour la détermination du revenu net comprennent : 1° Pour les propriétés urbaines : /  (...) g) Pour les logements situés en France, acquis neufs ou en l'état futur d'achèvement entre le 1er janvier 1999 et le 2 avril 2003, et à la demande du contribuable, une déduction au titre de l'amortissement égale à 8 % du prix d'acquisition du logement pour les cinq premières années et de 2,5 % de ce prix pour les quatre années suivantes. La période d'amortissement a pour point de départ le premier jour du mois de l'achèvement de l'immeuble ou de son acquisition si elle est postérieure. (...) / Le bénéfice de la déduction est subordonné à une option qui doit être exercée lors du dépôt de la déclaration des revenus de l'année d'achèvement de l'immeuble ou de son acquisition si elle est postérieure. Cette option est irrévocable pour le logement considéré et comporte l'engagement du propriétaire de louer le logement nu pendant au moins neuf ans à usage d'habitation principale à une personne autre qu'un membre de son foyer fiscal (...) ". Aux termes de l'article 2 quindecies de l'annexe III à ce code : " I.-Pour bénéficier de la déduction au titre de l'amortissement prévue au g du 1° du I de l'article 31 du code général des impôts, les contribuables sont tenus de joindre à leur déclaration des revenus de l'année d'achèvement de l'immeuble ou de son acquisition si elle est postérieure : / 1° L'option formulée dans une note annexe établie sur un imprimé fourni par l'administration, qui comporte les éléments suivants : / (...) d) L'engagement de louer le logement non meublé, pendant une durée de neuf ans au moins, à des personnes qui en font leur habitation principale ;  / (...) IV. - Pendant la période couverte par l'engagement de location mentionné aux I, II et III, le contribuable joint à chacune de ses déclarations des revenus un état établi conformément à un modèle fixé par l'administration et faisant apparaître, pour chaque logement, le détail du calcul du montant de la déduction pratiquée au titre de l'amortissement ainsi qu'une note indiquant le nom des locataires de l'immeuble. (...) ". Il résulte de ces dispositions que le bénéfice de l'avantage fiscal qu'elles prévoient est subordonné à la condition que le locataire fasse effectivement de l'immeuble qui lui est loué par le contribuable son habitation principale. <br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que M. et Mme E... sont propriétaires d'un appartement situé 3 rue Pasteur à Limoges, au titre duquel ils ont souscrit l'option prévue au g) du 1° du I de l'article 31 du code général des impôts. A la suite d'un contrôle sur pièces portant sur les années 2007 à 2009, l'administration a remis en cause la déduction au titre de l'amortissement pratiquée par M. et Mme E... sur le fondement de ces dispositions au motif que le bien loué ne constituait pas la résidence principale des locataires. Le ministre des finances et des comptes publics se pourvoit en cassation contre l'arrêt du 7 avril 2015 par lequel la cour administrative d'appel de Bordeaux a rejeté son appel formé contre le jugement du 27 juin 2013 par lequel le tribunal administratif de Limoges a déchargé M. et Mme E...des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles ils ont été assujettis au titre des années 2007, 2008 et 2009. <br/>
<br/>
              3. Il ressort des énonciations de l'arrêt attaqué que la cour a relevé que l'administration avait remis en cause le bénéfice de l'avantage fiscal au motif que l'appartement en cause était occupé par M. A...D..., caution mentionnée au bail, et non par le locataire, M. B... D..., qui se trouvait toujours domicilié.à son ancienne adresse Elle a toutefois jugé que M. et Mme E...n'avaient pas méconnu les obligations fiscales découlant pour eux des dispositions du g) du 1° du I de l'article 31 du code général des impôts, en ne vérifiant pas l'effectivité de l'occupation de leur appartement par M. B...D..., dès lors d'une part que le titulaire du bail est présumé occuper le logement décrit au contrat d'habitation et d'autre part que l'administration fiscale n'établissait, ni n'alléguait, aucune négligence, ni aucune fraude de leur part qui pourrait être à l'origine de la situation de fait constatée par le service vérificateur. Il résulte de ce qui a été dit au point 1 ci-dessus qu'en subordonnant la remise en cause de l'avantage fiscal à l'existence d'une négligence ou d'une fraude de la part du contribuable, la cour a commis une erreur de droit. Par suite, le ministre des finances et des comptes publics est fondé à demander l'annulation de l'arrêt qu'il attaque. <br/>
<br/>
              4. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 7 avril 2015 de la cour administrative d'appel de Bordeaux est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux.<br/>
Article 3 : Les conclusions de M. et Mme E...sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'économie et des finances et à M. et Mme C...E.à son ancienne adresse<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
