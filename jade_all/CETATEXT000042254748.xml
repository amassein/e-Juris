<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042254748</ID>
<ANCIEN_ID>JG_L_2020_08_000000442564</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/25/47/CETATEXT000042254748.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 11/08/2020, 442564, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-08-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>442564</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:442564.20200811</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au juge des référés du tribunal administratif de Marseille, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'enjoindre au préfet des Bouches-du-Rhône de lui remettre une autorisation provisoire de séjour lui permettant de présenter sa demande d'asile à l'Office français de protection des réfugiés et apatrides (OFPRA), dans un délai de trois jours à compter de la notification de l'ordonnance et sous astreinte de 100 euros par jour de retard ;<br/>
<br/>
               2°) d'enjoindre à l'Office français de l'immigration et de l'intégration (OFII) de rétablir le versement de l'allocation des demandeurs d'asile à son profit, rétroactivement à partir du mois de février 2018, sous astreinte de 100 euros par jour de retard à compter de la notification de l'ordonnance.<br/>
<br/>
              Par une ordonnance n° 2005134 du 22 juillet 2020, le juge des référés du tribunal administratif de Marseille a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 7 août 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de l'admettre au bénéfice de l'aide juridictionnelle provisoire ;<br/>
              2°) d'annuler cette ordonnance ;<br/>
<br/>
              3°) de faire droit à ses demandes de première instance ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat le versement à son conseil de la somme de 2 000 euros au titre des dispositions combinées des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - l'ordonnance qu'il attaque est irrégulière en ce que le juge des référés du tribunal administratif de Marseille ne pouvait se fonder sur la circonstance selon laquelle il aurait été placé en fuite pour rejeter sa demande, dès lors notamment qu'il ne s'est pas soustrait de façon intentionnelle et systématique au contrôle de l'autorité administrative alors que l'arrêté prononçant son assignation à résidence ne lui a pas été communiqué dans une traduction en anglais, langue qu'il comprend ;<br/>
              - la condition d'urgence est remplie dès lors, d'une part, qu'il ne perçoit plus l'allocation de demandeur d'asile et vit dans un dénuement total et, d'autre part, qu'il ne dispose plus d'une attestation de demande d'asile à jour lui permettant, dans l'attente d'une décision sur sa demande d'asile, de justifier de la régularité de son séjour en France ;<br/>
              - le refus de la préfecture de lui délivrer un récépissé de demande d'asile en procédure normale et le refus de lui permettre de présenter sa demande d'asile à l'OFPRA à l'expiration du délai de six mois prévu par l'article 29-2 du règlement UE 603/2013 portent une atteinte grave et manifestement illégale au droit d'asile ;<br/>
              - la suspension par l'OFII du bénéfice des conditions matérielles d'accueil et du versement de l'allocation de demandeur d'asile porte une atteinte grave et manifestement illégale au droit d'asile et au principe de la dignité de la personne humaine.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement UE 603/2013 du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Le droit constitutionnel d'asile, qui a le caractère d'une liberté fondamentale, a pour corollaire le droit de solliciter le statut de réfugié. S'il implique que l'étranger qui sollicite la reconnaissance de la qualité de réfugié soit, en principe, autorisé à demeurer sur le territoire jusqu'à ce qu'il ait été statué sur sa demande, ce droit s'exerce dans les conditions définies par le code de l'entrée et du séjour des étrangers et du droit d'asile. L'article L. 742-3 de ce code prévoit que l'étranger dont l'examen de la demande d'asile relève de la responsabilité d'un autre Etat peut faire l'objet d'un transfert vers l'Etat qui est responsable de cet examen en application des dispositions du règlement du Parlement européen et du Conseil du 26 juin 2013. Ce transfert peut avoir lieu pendant une période de six mois à compter de l'acceptation de la demande de prise en charge, susceptible d'être portée à dix-huit mois dans les conditions prévues à l'article 29 de ce règlement si l'intéressé " prend la fuite ".<br/>
<br/>
               3. Il résulte de l'instruction que M. A..., ressortissant gambien, est entré en France le 17 juin 2019 en provenance d'Italie où il avait précédemment déposé une demande l'asile en septembre 2015, a présenté une demande d'asile enregistrée en préfecture des Bouches-du-Rhône le 16 juillet 2019. Les autorités italiennes, saisies d'une demande de reprise en charge, ont donné leur accord au transfert le 6 septembre 2019. Le préfet des Bouches-du-Rhône a pris, le 4 décembre 2019, un arrêté de transfert vers l'Italie et un arrêté d'assignation à résidence et un nouvel arrêté notifié le 8 janvier 2020. M. A... ne s'étant pas présenté à deux contrôles en préfecture les 22 et 29 janvier 2020, il a été déclaré en fuite le 3 mars 2020, reportant ainsi le délai de son transfert vers l'Italie jusqu'au 6 mars 2021. M. A... fait appel de l'ordonnance du 22 juillet 2020 par laquelle le juge des référés du tribunal administratif de Marseille, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté sa demande tendant à ce qu'il enjoint au préfet des Bouches-du-Rhône et à l'Office français de l'immigration et de l'intégration de lui délivrer une attestation de demande d'asile en procédure normale et de rétablir les conditions matérielles d'accueil.<br/>
<br/>
              4. M. A... ne conteste pas ne pas s'être présenté à deux contrôles en préfecture les 22 et 29 janvier 2020 selon la fiche de pointage tenue en préfecture, produite au dossier de première instance. S'il soutient, comme devant le juge des référés du tribunal administratif, que le second arrêté d'assignation à résidence ne lui a pas été notifié en anglais, contrairement au premier, il n'est en tout état de cause pas contesté qu'il s'est présenté à la préfecture conformément à l'arrêté d'assignation à résidence les 8 et 15 janvier 2020 avant de cesser de s'y présenter, l'intéressé ayant quitté Marseille pour les Hautes-Alpes, et il doit ainsi être regardé comme s'étant soustrait intentionnellement à plusieurs reprises à l'exécution de son transfert. Il n'apporte en appel aucun élément de nature à infirmer l'appréciation portée par le juge des référés de première instance quant à l'absence d'atteinte grave et manifestement illégale portée à la liberté fondamentale que constitue le droit d'asile.<br/>
<br/>
              5. Il résulte de ce qui précède qu'il est manifeste que l'appel de M. A... ne peut être accueilli. Il y a donc lieu de rejeter sa requête selon la procédure prévue à l'article L. 522-3 du code de justice administrative, y compris les conclusions tendant à l'application des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
