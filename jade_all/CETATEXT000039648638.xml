<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039648638</ID>
<ANCIEN_ID>JG_L_2019_12_000000422374</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/64/86/CETATEXT000039648638.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 19/12/2019, 422374, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-12-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422374</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP BOULLOCHE</AVOCATS>
<RAPPORTEUR>M. Thibaut Félix</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:422374.20191219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé à la commission départementale d'aide sociale de la Haute-Vienne, d'une part, d'annuler la décision du président du conseil départemental de la Haute-Vienne, du 21 mai 2015, de récupérer un indu de 2 200 euros au titre de la prestation de compensation du handicap qu'elle a perçue du 1er mars 2013 au 31 décembre 2014 et, d'autre part, de prononcer la remise totale ou partielle de sa dette. Par une décision du 1er octobre 2015, la commission départementale d'aide sociale de la Haute-Vienne a rejeté la demande de Mme A.... <br/>
<br/>
              Par une décision n° 160193 du 21 février 2018, sur l'appel de Mme A..., la Commission centrale d'aide sociale a annulé la décision de la commission départementale d'aide sociale de la Haute-Vienne et accordé à Mme A... la remise intégrale de la somme de 2 200 euros. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 juillet et 18 octobre 2018 au secrétariat du contentieux du Conseil d'Etat, le département de la Haute-Vienne demande au conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision de la Commission centrale d'aide sociale ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de Mme A... ;<br/>
<br/>
              3°) de mettre à la charge de Mme A... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 2018-928 du 29 octobre 2018 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thibaut Félix, auditeur,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat du département de la Haute-Vienne, et à la SCP Boulloche, avocat de Mme A... ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 245-3 du code de l'action sociale et des familles prévoit que la prestation de compensation du handicap peut être affectée à des charges : " 1° Liées à un besoin d'aides humaines (...) / 4° Spécifiques ou exceptionnelles, comme celles relatives à l'acquisition ou l'entretien de produits liés au handicap (...) ". Aux termes de l'article L. 245-5 de ce code : " Le service de la prestation de compensation peut être suspendu ou interrompu lorsqu'il est établi, au regard du plan personnalisé de compensation et dans des conditions fixées par décret, que son bénéficiaire n'a pas consacré cette prestation à la compensation des charges pour lesquelles elle lui a été attribuée. Il appartient, le cas échéant, au débiteur de la prestation d'intenter une action en recouvrement des sommes indûment utilisées ". L'article D. 245-52 de ce code dispose que : " Le bénéficiaire de la prestation de compensation conserve pendant deux ans les justificatifs des dépenses auxquelles la prestation de compensation est affectée " et l'article D. 245-58 que : " Le président du conseil général peut à tout moment procéder ou faire procéder à un contrôle sur place ou sur pièces en vue de vérifier si les conditions d'attribution de la prestation de compensation sont ou restent réunies ou si le bénéficiaire de cette prestation a consacré cette prestation à la compensation des charges pour lesquelles elle lui a été attribuée (...) ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 6 septembre 2011, la commission des droits et de l'autonomie des personnes handicapées de la Haute-Vienne a accordé à Mme A... le bénéfice de la prestation de compensation du handicap pour son " besoin d'aides humaines ", ainsi que, à compter du 1er septembre 2011 et à hauteur de 100 euros par mois, pour des charges spécifiques. A la suite d'un contrôle des dépenses engagées par Mme A... au titre des charges spécifiques, le président du conseil départemental de la Haute-Vienne a informé l'intéressée, par un courrier du 21 mai 2015, d'un trop-perçu d'un montant de 2 200 euros pour la période allant du 1er mars 2013 au 31 décembre 2014, au motif qu'elle n'avait pu justifier des dépenses exposées à ce titre. Mme A... a demandé l'annulation de cette décision ou la remise gracieuse de sa dette à la commission départementale d'aide sociale de la Haute-Vienne, qui a rejeté sa demande. Le département de la Haute-Vienne se pourvoit en cassation contre la décision du 21 février 2018 par laquelle la Commission centrale d'aide sociale, faisant droit à l'appel de Mme A..., a accordé à celle-ci une remise totale de la somme de 2 200 euros qui lui était réclamée.<br/>
<br/>
              3. Lorsqu'il statue sur un recours dirigé contre une décision refusant ou ne faisant que partiellement droit à une demande de remise gracieuse d'un indu d'une prestation ou d'une allocation versée au titre de l'aide ou de l'action sociale, du logement ou en faveur des travailleurs privés d'emploi, il appartient au juge administratif, eu égard tant à la finalité de son intervention qu'à sa qualité de juge de plein contentieux, non de se prononcer sur les éventuels vices propres de la décision attaquée, mais d'examiner si une remise gracieuse totale ou partielle est susceptible d'être accordée, en se prononçant lui-même sur la demande au regard des dispositions applicables et des circonstances de fait dont il est justifié par l'une et l'autre parties à la date de sa propre décision. S'agissant d'un indu constaté au titre de la prestation de compensation du handicap, il y a lieu de rechercher si la situation de précarité de l'intéressé et sa bonne foi justifient que lui soit accordée une remise ou une réduction de dette.<br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que Mme A... s'est bornée à invoquer sa " situation financière difficile aggravée par un surendettement ", sans assortir cette affirmation d'aucune précision ni produire aucun justificatif. Dans ces conditions, et alors qu'aucun autre élément ne permettait de connaître la situation financière de Mme  A..., la Commission centrale d'aide sociale a dénaturé les pièces du dossier qui lui était soumis en jugeant qu'elle justifiait de ressources modiques qui caractérisaient une situation de précarité la mettant dans l'impossibilité de rembourser la somme réclamée.<br/>
<br/>
              5. Par suite, par ce moyen qu'il peut utilement soulever en cassation, et sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, le département de la Haute-Vienne est fondé à demander l'annulation de la décision de la Commission centrale d'aide sociale du 21 février 2018.<br/>
<br/>
              6. Les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce qu'une somme soit mise à ce titre à la charge du département de la Haute-Vienne, qui n'est pas la partie perdante dans la présente instance. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par ce département au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La décision de la Commission centrale d'aide sociale du 21 février 2018 est annulée.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : Les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ainsi que de l'article 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au département de la Haute-Vienne et à Mme  B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
