<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044806218</ID>
<ANCIEN_ID>JG_L_2021_12_000000448695</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/80/62/CETATEXT000044806218.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 30/12/2021, 448695, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448695</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Manon Chonavel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:448695.20211230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Les Hôpitaux universitaires de Strasbourg ont demandé au tribunal administratif de Strasbourg, d'une part, d'annuler la décision du 4 février 2019 par laquelle la caisse primaire d'assurance maladie du Bas-Rhin a refusé de prendre en charge les soins urgents dispensés à M. J... D... et, d'autre part, de mettre ces soins à la charge de la caisse primaire d'assurance maladie du Bas-Rhin. Par un jugement n° 1902093 du 13 novembre 2020, le tribunal administratif de Strasbourg a fait droit à cette demande. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 14 janvier et 14 avril 2021 au secrétariat du contentieux du Conseil d'État, la caisse primaire d'assurance maladie du Bas-Rhin demande au Conseil d'État : <br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande des Hôpitaux universitaires de Strasbourg ;<br/>
<br/>
              3°) de mettre à la charge des Hôpitaux universitaires de Strasbourg la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 2003-1312 du 30 décembre 2003 ;<br/>
              - le décret n° 2020-1073 du 18 août 2020 ;<br/>
              - l'arrêté du 10 mai 2017 fixant la liste des titres de séjour prévu au I de l'article R. 111-3 du code de la sécurité sociale ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Manon Chonavel, auditrice,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Foussard, Froger, avocat de la caisse primaire d'assurance maladie du Bas-Rhin et à Me Le Prado, avocat des Hôpitaux universitaires de Strasbourg ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. D..., détenteur d'un passeport géorgien, est entré en France le 28 novembre 2018 et a été hospitalisé le 30 novembre 2018 aux Hôpitaux universitaires de Strasbourg. Par une décision du 4 février 2019, le directeur de la caisse primaire d'assurance maladie du Bas-Rhin a refusé de faire droit à la demande des Hôpitaux universitaires de Strasbourg de prise en charge des soins urgents administrés à M. D.... Par un jugement du 13 novembre 2020, le tribunal administratif de Strasbourg a, sur la demande des Hôpitaux universitaires de Strasbourg, annulé la décision du 4 février 2019 et mis le coût des soins dispensés à M. D... à la charge de la caisse primaire d'assurance maladie du Bas-Rhin. La caisse primaire d'assurance maladie du Bas-Rhin se pourvoit en cassation contre ce jugement. Le ministre des solidarités et de la santé indique reprendre à son compte, en tant que de besoin, les conclusions de ce pourvoi en tant qu'il tend à l'annulation du jugement du tribunal administratif de Strasbourg.<br/>
<br/>
              Sur le cadre juridique du litige : <br/>
<br/>
              2. D'une part, aux termes des deux premiers alinéas de l'article L. 160-1 du code de la sécurité sociale : " Toute personne travaillant ou, lorsqu'elle n'exerce pas d'activité professionnelle, résidant en France de manière stable et régulière bénéficie, en cas de maladie ou de maternité, de la prise en charge de ses frais de santé dans les conditions fixées au présent livre. L'exercice d'une activité professionnelle et les conditions de résidence en France sont appréciées selon les règles prévues, respectivement, aux articles L. 111-2-2 et L. 111-2-3 ". Ce dernier article renvoie à un décret en Conseil d'Etat, sans préjudice des règles particulières applicables au service des prestations ou des allocations, les conditions d'appréciation de la stabilité de la résidence et de la régularité du séjour mentionnées à l'article L. 111-1 du code de la sécurité sociale, aux termes duquel notamment " la sécurité sociale (...) assure, pour toute personne travaillant ou résidant en France de façon stable et régulière, la couverture des charges de maladie, de maternité et de paternité ainsi que des charges de famille et d'autonomie ". Le I de l'article R. 111-3 du même code, pris pour l'application de ces dispositions, précise à son premier alinéa que remplissent la condition de régularité du séjour les personnes qui, au jour de la demande " sont en situation régulière au regard de la législation sur le séjour des étrangers en France ". Il renvoie, à son deuxième alinéa, à un arrêté du ministre chargé de la sécurité sociale et du ministre de l'intérieur le soin de fixer la liste des titres ou documents attestant la régularité de la situation des personnes de nationalité étrangère, qui ne sont pas ressortissants d'un Etat membre de l'Union européenne, d'un Etat partie à l'accord sur l'Espace économique européen ou de la Confédération suisse. La condition de stabilité est quant à elle appréciée au regard des critères prévus à l'article R. 111-2 du même code.<br/>
<br/>
              3. D'autre part, le 2° de l'article L. 211-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, alors en vigueur, désormais repris au 2° de l'article L. 311-1 de ce code, dispose que pour entrer en France, tout étranger doit être muni, en plus des documents et visas exigés par les conventions internationales et les règlements en vigueur, notamment " des autres documents prévus par décret en Conseil d'Etat relatifs, d'une part, à l'objet et aux conditions de son séjour et, d'autre part, s'il y a lieu, à ses moyens d'existence, à la prise en charge par un opérateur d'assurance agréé des dépenses médicales et hospitalières, y compris d'aide sociale, résultant de soins qu'il pourrait engager en France, ainsi qu'aux garanties de son rapatriement ". L'article R. 211-2 du même code, alors en vigueur, désormais repris à l'article R. 311-2, pris pour l'application de ces dispositions, précise que tout étranger qui déclare vouloir séjourner en France pour une durée n'excédant pas trois mois est tenu de présenter, pour être admis sur le territoire français, les documents mentionnés au 2° de l'article L. 211-1.<br/>
<br/>
              4. Enfin, le premier alinéa de l'article L. 251-1 du code de l'action sociale et des familles dispose que tout étranger résidant en France de manière ininterrompue depuis plus de trois mois sans remplir la condition de régularité mentionnée à l'article L. 160-1 du code de la sécurité sociale et dont les ressources ne dépassent pas le plafond mentionné à l'article L. 861-1 de ce code, c'est-à-dire celui selon lequel est ouvert le droit à une protection complémentaire en matière de santé, a droit à l'aide médicale de l'Etat pour lui-même et les ayants-droits dont cet article dresse la liste. En outre, la loi du 30 décembre 2003 de finances rectificative pour 2003, qui a subordonné à une condition de séjour ininterrompu d'au moins trois mois en France l'octroi de l'aide médicale de l'Etat prévue au premier alinéa de l'article L. 251-1 du code de l'action sociale et des familles, a également inséré dans le même code un article L. 254-1 qui prévoit la prise en charge par l'Etat des soins urgents " dont l'absence mettrait en jeu le pronostic vital ou pourrait conduire à une altération grave et durable de l'état de santé de la personne ou d'un enfant à naître ", administrés par les établissements de santé aux " étrangers résidant en France sans remplir la condition de régularité mentionnée à l'article L. 160-1 du code de la sécurité sociale et qui ne sont pas bénéficiaires de l'aide médicale de l'Etat en application de l'article L. 251-1. (...) Une dotation forfaitaire est versée à ce titre par l'Etat à la Caisse nationale de l'assurance maladie ".  <br/>
<br/>
              Sur la recevabilité du pourvoi : <br/>
<br/>
              5. D'une part, s'agissant de la représentation de l'Etat devant les tribunaux administratifs, aux termes du premier alinéa de l'article R. 431-10 du code de justice administrative : " L'Etat est représenté en défense par le préfet ou le préfet de région lorsque le litige, quelle que soit sa nature, est né de l'activité des administrations civiles de l'Etat dans le département ou la région (...) ". Toutefois, le décret du 18 août 2020 relatif à la représentation de l'Etat dans les litiges portant sur des prestations gérées pour son compte par les organismes de sécurité sociale a inséré dans le code de l'action sociale et des familles un article R. 134-2 en vertu duquel les directeurs des organismes de sécurité sociale représentent l'Etat devant le tribunal administratif dans les litiges relatifs aux décisions qu'ils prennent pour son compte concernant les prestations, allocations ou droits attribués au titre de l'aide ou de l'action sociale en application du code de l'action sociale et des familles. D'autre part, s'agissant de la représentation de l'Etat devant le Conseil d'Etat, aux termes de l'article R. 432-4 du code de justice administrative : " L'Etat est dispensé du ministère d'avocat au Conseil d'Etat soit en demande, soit en défense, soit en intervention. / Les recours et les mémoires, lorsqu'ils ne sont pas présentés par le ministère d'un avocat au Conseil d'Etat, doivent être signés par le ministre intéressé ou par le fonctionnaire ayant reçu délégation à cet effet ".<br/>
<br/>
              6. Il résulte des dispositions du code de l'action sociale et des familles citées au point 4 que les décisions par lesquelles les caisses primaires d'assurance maladie statuent sur les décisions de prise en charge des soins urgents sont, à l'instar de celles prises sur le fondement de l'article L. 251-1 du code de l'action sociale et des familles en matière d'admission à l'aide médicale de l'Etat en vertu de l'article L. 252-1 du même code, des décisions relatives aux prestations légales d'aide sociale, prises pour le compte de l'Etat, lesquelles sont susceptibles d'un recours de plein contentieux devant le juge administratif. Avant l'entrée en vigueur du décret du 18 août 2020, aucune disposition ne prévoyait que ces organismes représentent l'Etat en justice dans les litiges relatifs à ces décisions ni n'habilitait le préfet ou le ministre à leur déléguer la compétence qu'ils tiennent des dispositions du code de justice administrative citées au point précédent pour représenter l'Etat, respectivement, devant le tribunal administratif et devant le Conseil d'Etat. Si les directeurs des organismes de sécurité sociale sont désormais compétents pour représenter l'Etat devant le tribunal administratif dans les litiges relatifs aux décisions qu'ils prennent pour son compte concernant les prestations, allocations ou droits attribués au titre de l'aide ou de l'action sociale en application du code de l'action sociale et des familles, le ministre chargé des affaires sociales, auquel les jugements statuant sur ces demandes doivent être notifiés, conserve seul qualité pour se pourvoir en cassation contre ces jugements et pour défendre devant le Conseil d'Etat saisi d'un pourvoi, l'organisme de sécurité sociale ne pouvant, s'il y a lieu, être mis en cause devant le Conseil d'Etat qu'en qualité d'observateur.<br/>
<br/>
              7. Le ministre des solidarités et de la santé, à qui le pourvoi a été communiqué, s'est approprié avant la clôture de l'instruction l'ensemble des conclusions et des moyens présentés par la caisse primaire d'assurance maladie du Bas-Rhin et a déclaré maintenir ses conclusions tendant à l'annulation du jugement en litige. Les conclusions introduites à cette fin par la caisse, qui doit désormais être regardée comme observatrice dans la présence instance, ont ainsi été régularisées. <br/>
<br/>
              Sur le bien-fondé du jugement :<br/>
<br/>
              8. Il résulte de l'ensemble des dispositions citées aux points 2 à 4, éclairées par les travaux parlementaires ayant conduit à l'adoption de la loi du 30 décembre 2003, que seuls les étrangers en situation irrégulière sont susceptibles de bénéficier de l'aide médicale de l'Etat sur le fondement du premier alinéa de l'article L. 251-1 du code de l'action sociale et des familles lorsqu'ils résident de manière ininterrompue depuis plus de trois mois sur le territoire ou de la prise en charge des soins urgents et vitaux lorsqu'ils ne bénéficient pas de l'aide médicale de l'Etat, notamment au motif qu'ils ne résident pas en France de manière ininterrompue depuis plus de trois mois. Par ailleurs, ainsi qu'il a été dit au point 2, l'accès à la prise en charge des frais d'assurance maladie mentionnée à l'article L. 160-1 du code de la sécurité sociale est conditionné à une double condition de régularité et de stabilité du séjour. Si les dispositions du second alinéa du I de l'article R. 111-3 du code de la sécurité sociale renvoient à un arrêté du ministre chargé de la sécurité sociale et du ministre de l'intérieur le soin de fixer la liste des titres ou documents attestant la régularité au regard de leur séjour en France des personnes de nationalité étrangère qui ne sont pas ressortissantes d'un Etat membre de l'Union européenne, d'un Etat partie à l'accord sur l'Espace économique européen ou de la Confédération suisse, la prise en charge des soins urgents et vitaux, de même d'ailleurs que l'aide médicale de l'Etat mentionnée au premier alinéa de l'article L. 251-1 du code de l'action sociale et des familles, ne sauraient, eu égard aux conditions fixées par le législateur à leur octroi, être accordées à un étranger qui, alors même que la régularité de son séjour n'est pas attestée par l'un des titres figurant à l'article 1er de l'arrêté du 10 mai 2017 pris en application du II de l'article R. 111-3 du code de la sécurité sociale, est en situation régulière au regard de la législation sur le séjour des étrangers en France.<br/>
<br/>
              9. Par suite, en jugeant que M. D... devait être regardé comme remplissant la condition tenant à l'absence de régularité de séjour prévue par l'article L. 254-1 du code de l'action sociale et des familles au motif que le passeport géorgien dont il était détenteur et qui l'exemptait de l'obligation de présenter un visa pour des séjours de moins de trois mois ne figurait pas à l'article 1er de l'arrêté du 10 mai 2017, le tribunal administratif de Strasbourg a commis une erreur de droit.<br/>
<br/>
              10. Les Hôpitaux universitaires de Strasbourg font valoir que le tribunal administratif était fondé à retenir l'irrégularité du séjour de M. D... pour un autre motif, tenant au fait que celui-ci ne disposait pas de l'attestation de prise en charge par une assurance des dépenses médicales et hospitalières prévue par le 2° de l'article L. 211-1 et l'article R. 211-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, cités au point 3. Toutefois, d'une part, le juge de cassation ne peut substituer un autre motif au motif erroné retenu par les juges du fond que si le nouveau motif n'appelle l'appréciation d'aucune circonstance de fait nouvelle. Or le motif invoqué par les Hôpitaux universitaires de Strasbourg conduirait le juge de cassation à procéder à une telle appréciation. D'autre part et en tout état de cause, les conditions relatives à la souscription d'une assurance ainsi prévues, si elles conditionnent la régularité de l'entrée sur le territoire des ressortissants étrangers, sont sans portée lorsqu'il s'agit d'apprécier la régularité de leur séjour en application des dispositions mentionnées au point 4. Il n'y a pas lieu, dans ces conditions, de substituer au motif erroné retenu par le tribunal administratif celui que font valoir les Hôpitaux universitaires de Strasbourg.<br/>
<br/>
              11. Il résulte de tout ce qui précède que le ministre des solidarités et de la santé est fondé à demander l'annulation du jugement qu'il attaque. <br/>
<br/>
              Sur les frais de l'instance : <br/>
<br/>
              12. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit, d'une part, aux conclusions présentées par la caisse primaire d'assurance maladie du Bas-Rhin, qui n'ont pas été reprises à son compte par l'Etat, dès lors que cette dernière, comme il a été dit au point 7, doit être regardée comme observatrice et non comme partie dans la présente instance, d'autre part, aux conclusions présentées par les Hôpitaux universitaires de Strasbourg qui n'est pas, en tout état de cause, la partie gagnante dans la présente instance. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement du 13 novembre 2020 du tribunal administratif de Strasbourg est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Strasbourg.<br/>
Article 3 : Les conclusions présentées par les Hôpitaux universitaires de Strasbourg et par la caisse primaire d'assurance maladie du Bas-Rhin, au titre de l'article L. 761-1 du code de justice administrative, sont rejetées.<br/>
Article 4 : La présente décision sera notifiée aux Hôpitaux universitaires de Strasbourg, au ministre des solidarités et de la santé et à la caisse primaire d'assurance maladie du Bas-Rhin. Copie en sera adressée à M. J... D....<br/>
<br/>
Délibéré à l'issue de la séance du 29 novembre 2021 où siégeaient : Mme Maud Vialettes, présidente de chambre, présidant ; Mme Gaëlle Dumortier, présidente de chambre ; Mme A... C..., Mme F... H..., M. G... E..., M. Damien Botteghi, conseillers d'Etat et Mme Manon Chonavel, auditrice-rapporteure. <br/>
<br/>
Rendu le 30 décembre 2021.<br/>
<br/>
<br/>
                 La présidente : <br/>
                 Signé : Mme Maud Vialettes<br/>
 		La rapporteure : <br/>
      Signé : Mme Manon Chonavel<br/>
                 La secrétaire :<br/>
                 Signé : Mme I... B...<br/>
<br/>
<br/>
<br/>
La République mande et ordonne au ministre des solidarités et de la santé en ce qui le concerne ou à tous huissiers de justice à ce requis en ce qui concerne les voies de droit commun contre les parties privées, de pourvoir à l'exécution de la présente décision.<br/>
			Pour expédition conforme,<br/>
			Pour la secrétaire du contentieux, par délégation :<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
