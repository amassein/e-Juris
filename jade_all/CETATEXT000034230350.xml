<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034230350</ID>
<ANCIEN_ID>JG_L_2017_03_000000400546</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/23/03/CETATEXT000034230350.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 20/03/2017, 400546, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400546</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Camille Pascal</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:400546.20170320</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 9 et 30 juin et le 22 août 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 25 mars 2016 par lequel le Premier ministre a accordé son extradition aux autorités ukrainiennes ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat  la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le pacte international relatif aux droits civils et politiques ;<br/>
              - la convention de New-York relative aux droits de l'enfant du 26 janvier 1990 ;<br/>
<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés ; <br/>
              - la convention européenne d'extradition du 13 décembre 1957 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code pénal ; <br/>
              - le code de procédure pénale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Barrois de Sarigny, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que, par le décret attaqué, le Premier ministre a accordé aux autorités ukrainiennes l'extradition de M.A..., ressortissant ukrainien, pour l'exécution d'un mandat d'arrêt décerné le 30 avril 2013 par la cour de la région d'Uzhgorod de Transcarpatie pour des faits qualifiés, en droit ukrainien, de " trafic illicite de stupéfiants, de substances psychotropes à plusieurs reprises ou par une entente préalable et fabrication illégale, production, acquisition, achat, stockage transport ou transfert à la vente ainsi qu'à la vente illégale de stupéfiants ou de substances psychotropes " ; <br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              2.	Considérant qu'il ressort des mentions de l'ampliation du décret attaqué, certifiée conforme par le secrétaire général du Gouvernement, que, contrairement à ce qui est soutenu, le décret a été signé par le Premier ministre et contresigné par la garde des sceaux, ministre de la justice ; que l'ampliation notifiée à l'intéressé n'avait pas à être revêtue de ces signatures ;<br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              3.	Considérant, en premier lieu, que si M. A...invoque la méconnaissance, par le décret attaqué, des articles 696-2, 696-6 et 696-8 du code de procédure pénale, ces dispositions, qui ont un caractère supplétif en vertu de l'article 696 du même code, ne sont pas applicables à la demande d'extradition de M.A..., qui est fondée sur les stipulations de la convention européenne d'extradition ; qu'il en va de même des dispositions du 7° de l'article 696-4 du même code, également invoquées par M.A..., en vertu desquelles l'extradition n'est pas accordée lorsque la personne réclamée serait jugée dans l'Etat requérant par un tribunal n'assurant pas les garanties fondamentales de procédure et de protection des droits de la défense, ces règles étant reprises par les réserves émises par la France lors de la ratification de la convention européenne d'extradition du 13 décembre 1957 s'agissant de l'article 1er de cette convention ; <br/>
<br/>
              4.	Considérant, en deuxième lieu, que si M. A...fait valoir que les documents accompagnant la demande d'extradition ne seraient pas suffisamment précis et compréhensibles pour établir la réalité des charges et des poursuites à son encontre, il ressort des pièces du dossier que la demande d'extradition du 29 janvier 2015 présentée par l'Ukraine était accompagnée de l'ensemble des pièces requises par les stipulations de l'article 12 de la convention européenne d'extradition, notamment de la copie certifiée conforme du mandat d'arrêt décerné le 30 avril 2013 par la cour de la région d'Uzhgorod de Transcarpatie ; que cette décision mentionnait les faits et les infractions reprochés à M. A...et contenait la référence précise des dispositions pénales ukrainiennes prévoyant et réprimant l'infraction pour laquelle il était poursuivi, ainsi que des textes de procédure pénale applicables à la détention provisoire ; qu'ainsi, le moyen tiré de ce que le décret attaqué aurait fait droit à une demande d'extradition sans que le Premier ministre ne dispose des éléments que l'Etat requérant devait présenter aux autorités françaises en vertu des stipulations de l'article 12 de la convention européenne d'extradition doit être écarté ;<br/>
<br/>
              5.	Considérant, en troisième lieu, que si M. A...soutient que le décret attaqué porterait atteinte au principe de spécialité prévu par l'article 14 de la convention européenne d'extradition, il ne ressort pas des pièces du dossier que les autorités ukrainiennes n'entendraient pas respecter l'engagement résultant pour elles de cet article ;<br/>
<br/>
              6.	Considérant, en quatrième lieu, que les réserves émises par la France lors de la ratification de la convention européenne d'extradition du 13 décembre 1957 énoncent, s'agissant de l'article 1er, que : " L'extradition ne sera pas accordée lorsque la personne réclamée serait jugée dans l'Etat requérant par un tribunal n'assurant pas les garanties fondamentales de procédure et de protection des droits de la défense " et que : " L'extradition pourra être refusée si la remise est susceptible d'avoir des conséquences d'une gravité exceptionnelle pour la personne réclamée " ; que si M. A...soutient, en produisant la demande de statut de réfugié de son épouse, que la demande d'extradition prise à son encontre aurait, en réalité, pour objet de le persécuter à la suite de la dénonciation de la corruption au sein du service de lutte contre le trafic de stupéfiants, les risques de représailles personnelles qu'il allègue en cas de retour en Ukraine ne sont pas étayés par les pièces du dossier et ne permettent pas de lui reconnaître la qualité de réfugié ; que, par suite, les moyens tirés de ce que le Premier ministre aurait, en décidant l'extradition de l'intéressé, méconnu les articles 3 et 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ne peuvent qu'être écartés ; qu'il en est de même du moyen tiré de ce que le Premier ministre aurait commis une erreur manifeste au regard des exigences résultant des réserves émises par la France à la ratification de la convention européenne d'extradition ;<br/>
<br/>
              7.	Considérant, en cinquième lieu, que si une décision d'extradition est susceptible de porter atteinte au droit au respect de la vie familiale, au sens de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, de l'article 16 de la convention de New-York du 26 janvier 1990 relative aux droits de l'enfant ou de l'article 17 du pacte international relatif aux droits civils et politiques, cette mesure trouve, en principe, sa justification dans la nature même de la procédure d'extradition, qui est de permettre, dans l'intérêt de l'ordre public et sous les conditions fixées par les dispositions qui la régissent, tant le jugement de personnes se trouvant en France qui sont poursuivies à l'étranger pour des crimes ou des délits commis hors de France que l'exécution, par les mêmes personnes, des condamnations pénales prononcées contre elles à l'étranger pour de tels crimes ou délits ; que la circonstance que l'intéressé, marié avec une compatriote dont il a eu deux enfants, vit avec sa famille en France n'est pas de nature à faire obstacle, dans l'intérêt de l'ordre public, à l'exécution de son extradition ; que, dès lors, les moyens tirés de la violation des stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, de l'article 16 de la convention de New-York du 26 janvier 1990 relative aux droits de l'enfant ou de l'article 17 du pacte international relatif aux droits civils et politiques doivent être écartés ;<br/>
<br/>
              8.	Considérant, en dernier lieu, que l'article 7 de la convention européenne d'extradition du 13 décembre 1957, aux termes duquel " la Partie requise pourra refuser d'extrader l'individu réclamé à raison d'une infraction qui, selon sa législation, a été commise en tout ou en partie sur son territoire ", implique que les autorités d'un Etat signataire de cette convention puissent accorder l'extradition d'un étranger pour des faits commis sur le territoire de cet Etat ; qu'il ne ressort pas des pièces du dossier que le Gouvernement ait commis une erreur manifeste d'appréciation en s'abstenant de faire usage de la faculté de refuser l'extradition de M. A... pour des faits qui, pour certains, ont pu être commis en France ;<br/>
<br/>
              9.	Considérant qu'il résulte de l'ensemble de ce qui précède que M. A...n'est pas fondé à demander l'annulation pour excès de pouvoir du décret attaqué ; que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
