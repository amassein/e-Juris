<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033657445</ID>
<ANCIEN_ID>JG_L_2016_12_000000400910</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/65/74/CETATEXT000033657445.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 16/12/2016, 400910</TITRE>
<DATE_DEC>2016-12-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400910</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD</AVOCATS>
<RAPPORTEUR>M. Jean-Philippe Mochon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:400910.20161216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête, enregistrée le 23 juin 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...D...et M. C...A...demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2016-732 du 2 juin 2016 portant ouverture et annulation de crédits à titre d'avance ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi organique n° 2001-692 du 1er août 2001 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Philippe Mochon, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, avocat de M. D...et de M.A....<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'en vertu des dispositions de l'article 1er de la loi organique du 1er août 2001 relative aux lois de finances : " Dans les conditions et sous les réserves prévues par la présente loi organique, les lois de finances déterminent, pour un exercice, la nature, le montant et l'affectation des ressources et des charges de l'Etat, ainsi que l'équilibre budgétaire et financier qui en résulte (...) " ; que le IV de l'article 7 dispose que : " IV. - Les crédits ouverts sont mis à la disposition des ministres. / Les crédits ne peuvent être modifiés que par une loi de finances ou, à titre exceptionnel, en application des dispositions prévues aux articles 11 à 15, 17, 18 et 21. (...) " ;<br/>
<br/>
              2. Considérant qu'aux termes des trois premiers alinéas de l'article 13 de la même loi organique : " En cas d'urgence, des décrets d'avance pris sur avis du Conseil d'État et après avis des commissions de l'Assemblée nationale et du Sénat chargées des finances peuvent ouvrir des crédits supplémentaires sans affecter l'équilibre budgétaire défini par la dernière loi de finances. A cette fin, les décrets d'avance procèdent à l'annulation de crédits ou constatent des recettes supplémentaires. Le montant cumulé des crédits ainsi ouverts ne peut excéder 1 % des crédits ouverts par la loi de finances de l'année. / La commission chargée des finances de chaque assemblée fait connaître son avis au Premier ministre dans un délai de sept jours à compter de la notification qui lui a été faite du projet de décret. La signature du décret ne peut intervenir qu'après réception des avis de ces commissions ou, à défaut, après l'expiration du délai susmentionné. / La ratification des modifications apportées, sur le fondement des deux alinéas précédents, aux crédits ouverts par la dernière loi de finances est demandée au Parlement dans le plus prochain projet de loi de finances afférent à l'année concernée " ;<br/>
<br/>
              3. Considérant que M. D...et M.A..., députés, membres de la commission des finances de l'Assemblée nationale, demandent l'annulation pour excès de pouvoir du décret du 2 juin 2016 portant ouverture et annulation de crédits à titre d'avance, pris sur le fondement des dispositions précédemment citées de l'article 13 de la loi organique du 1er août 2001 ;<br/>
<br/>
              Sur le moyen tiré de l'irrégularité qui entacherait les consultations des commissions des finances et du Sénat au regard des modifications apportées au projet qui leur avait été notifié :<br/>
<br/>
              4. Considérant que le Gouvernement a, par le décret attaqué, procédé, à titre d'avances pour 2016, sur les missions et programmes mentionnés dans les tableaux en annexes du décret, d'une part à des ouvertures de crédit d'un montant de 1 449 650 000 euros en autorisations d'engagement et de 988 450 000 euros en crédits de paiement, et d'autre part à des annulations de crédits de mêmes montants ; que le projet de décret qu'il avait notifié aux commissions de finances de l'Assemblée nationale et du Sénat et sur lequel elles ont rendu un avis, prévoyait des ouvertures et des annulations de crédit pour un montant de 1 583 650 000 euros en autorisations d'engagement et 1 122 450 000 euros en crédit de paiement ; que cette différence traduit le choix du Gouvernement, à la suite des avis des deux commissions de finances, de ne pas procéder à des annulations de crédit qu'il envisageait initialement sur la mission recherche et enseignement supérieur pour un montant de 134 millions d'euros, en autorisations d'engagement comme en crédits de paiement, et corrélativement de diminuer du même montant les ouvertures de crédit sur la mission travail et emploi ;<br/>
<br/>
              5. Considérant que, compte tenu tant de la portée et de la nature des modifications apportées au projet de décret soumis aux commissions, l'équilibre budgétaire de la de la loi de finances restant inchangé, que de la circonstance que ces modifications portaient sur des points soulevés dans les avis des deux commissions des finances, le décret ainsi modifié ne soulevait pas de question dont les commissions des finances n'avaient pas déjà eu à connaître ; que les requérants ne sont dès lors pas fondés à soutenir que le décret serait entaché d'irrégularité pour ne pas avoir fait l'objet d'une nouvelle consultation des commissions des finances de l'Assemblée nationale et du Sénat ;<br/>
<br/>
              Sur le moyen tiré de l'absence d'urgence au sens des dispositions citées ci-dessus de l'article 13 de la loi organique relative aux lois de finances :<br/>
<br/>
              6. Considérant que les requérants soutiennent que le décret ne répond pas à la condition d'urgence posée par les dispositions de l'article 13 de la loi organique de la loi de finances en ce qui concerne les ouvertures de crédits au titre de la mission travail et emploi, au motif que les crédits concernés visaient à mettre en oeuvre des annonces faites par le Président de la République le 18 janvier 2016, soit plus de quatre mois avant la date de publication du décret ;<br/>
<br/>
              7. Considérant que, cependant, la condition d'urgence posée par le 1er alinéa de l'article 13 de la loi organique du 1er août 2001 relative aux lois de finances est une condition objective qui doit être regardée comme remplie dès lors que, à la date de publication du décret portant ouverture de crédits à titre d'avances, les crédits disponibles ne permettent pas de faire face à des dépenses indispensables ; que les requérants n'allèguent pas que les crédits ouverts par le décret attaqué ne répondent pas à cette condition ainsi définie ; que, s'ils soutiennent que ces mesures auraient pu être inscrites dans un projet de loi de finances rectificatives, une telle circonstance est inopérante dans l'appréciation de la condition d'urgence à laquelle est subordonnée l'adoption d'un décret portant ouverture et annulation de crédits à titre d'avance ; que le moyen tiré de l'absence d'urgence doit dès lors être écarté ;<br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin de statuer sur leur recevabilité de leur requête, M. D...et M. A...ne sont pas fondés à demander l'annulation du décret attaqué ; que leurs conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice doivent par suite également être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. D...et de M. A...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B...D..., à M. C...A..., ainsi qu'au ministre de l'économie et des finances et au Premier ministre.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">18-02-01-05-04-01 COMPTABILITÉ PUBLIQUE ET BUDGET. BUDGETS. BUDGET DE L'ETAT. CHARGES BUDGÉTAIRES. GESTION DES AUTORISATIONS BUDGÉTAIRES. DÉCRETS D'AVANCE. - CONDITION D'URGENCE (ART. 13 DE LA LOLF) - PORTÉE [RJ1].
</SCT>
<ANA ID="9A"> 18-02-01-05-04-01 La condition d'urgence posée par le 1er alinéa de l'article 13 de la loi organique du 1er août 2001 relative aux lois de finances (LOLF) est une condition objective qui doit être regardée comme remplie dès lors que, à la date de publication du décret portant ouverture de crédits à titre d'avances, les crédits disponibles ne permettent pas de faire face à des dépenses indispensables.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Rappr. CE, section des finances, avis, 21 novembre 2006, n° 373909, Rapport public 2007, p. 77.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
