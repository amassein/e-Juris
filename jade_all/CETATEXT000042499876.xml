<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042499876</ID>
<ANCIEN_ID>JG_L_2020_11_000000440355</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/49/98/CETATEXT000042499876.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 04/11/2020, 440355</TITRE>
<DATE_DEC>2020-11-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440355</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Manon Chonavel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:440355.20201104</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C... B... a demandé au tribunal administratif de Châlons-en-Champagne d'annuler les opérations électorales qui se sont déroulées le 15 mars 2020 dans la commune de Frignicourt pour l'élection des conseillers municipaux et communautaires. Par une ordonnance n° 2000668 du 24 mars 2020, le président du tribunal administratif de Châlons-en-Champagne a rejeté sa protestation.<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés les 30 avril, 30 juin et 20 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) d'annuler ces opérations électorales ;<br/>
<br/>
              3°) d'imputer les frais de conception, d'édition et de distribution du bulletin municipal au compte de campagne de la liste " Frignicourt agir ensemble 2020-2026 ".<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de procédure civile ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - la loi n° 2020-760 du 22 juin 2020 ;<br/>
              - l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
              - le décret n° 2020-571 du 14 mai 2020 ;<br/>
              - la décision n° 2020-849 QPC du 17 juin 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Manon Chonavel, auditrice,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. A l'issue des opérations électorales qui se sont déroulées le 15 mars 2020 à Frignicourt, commune de plus de 1 000 habitants, les dix-neuf sièges de conseillers municipaux et les trois sièges de conseillers communautaires ont été pourvus. Dix-sept des sièges de conseillers municipaux et tous les sièges de conseillers communautaires ont été attribués à des candidats de la liste " Frignicourt agir ensemble 2020-2026 ", conduite par M. H..., qui a obtenu 72,22 % des suffrages exprimés, tandis que les deux autres sièges de conseillers municipaux ont été attribués à des candidats de la liste " Frignicourt avec vous, pour demain ", conduite par M. B.... Ce dernier relève appel de l'ordonnance du 24 mars 2020 par laquelle le président du tribunal administratif de Châlons-en-Champagne a rejeté comme tardive la protestation qu'il a formée contre ces opérations électorales.<br/>
<br/>
              Sur la tardiveté de la protestation :<br/>
<br/>
              2. D'une part, aux termes de l'article R. 119 du code électoral : " Les réclamations contre les opérations électorales doivent être consignées au procès-verbal, sinon être déposées, à peine d'irrecevabilité, au plus tard à dix-huit heures le cinquième jour qui suit l'élection, à la sous-préfecture ou à la préfecture. Elles sont immédiatement adressées au préfet qui les fait enregistrer au greffe du tribunal administratif. / Les protestations peuvent également être déposées directement au greffe du tribunal administratif dans le même délai (...) ". <br/>
<br/>
              3. D'autre part, l'article 11 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 a habilité le Gouvernement, dans les conditions prévues à l'article 38 de la Constitution, à prendre par ordonnances " toute mesure, pouvant entrer en vigueur, si nécessaire, à compter du 12 mars 2020, relevant du domaine de la loi (...) 2° (...) b) Adaptant, interrompant, suspendant ou reportant le terme des délais prévus à peine de nullité, caducité, forclusion, prescription, inopposabilité, déchéance d'un droit, fin d'un agrément ou d'une autorisation ou cessation d'une mesure, à l'exception des mesures privatives de liberté et des sanctions. Ces mesures sont rendues applicables à compter du 12 mars 2020 (...) ". Sur le fondement de ces dispositions, le 3° du II de l'article 15 de l'ordonnance du 25 mars 2020 portant adaptation des règles applicables devant les juridictions de l'ordre administratif a prévu que : " Les réclamations et les recours mentionnés à l'article R. 119 du code électoral peuvent être formés contre les opérations électorales du premier tour des élections municipales organisé le 15 mars 2020 au plus tard à dix-huit heures le cinquième jour qui suit la date de prise de fonction des conseillers municipaux et communautaires élus dès ce tour, fixée par décret au plus tard au mois de juin 2020 dans les conditions définies au premier alinéa du III de l'article 19 de la loi n° 2020-290 du 23 mars 2020 susvisée ou, par dérogation, aux dates prévues au deuxième ou troisième alinéa du même III du même article ". L'article 1er du décret du 14 mai 2020 définissant la date d'entrée en fonction des conseillers municipaux et communautaires élus dans les communes dont le conseil municipal a été entièrement renouvelé dès le premier tour des élections municipales et communautaires organisé le 15 mars 2020 prévoit que : " (...) les conseillers municipaux et communautaires élus dans les communes dans lesquelles le conseil municipal a été élu au complet lors du scrutin organisé le 15 mars 2020 entrent en fonction le 18 mai 2020 ".<br/>
<br/>
              4. Il résulte de l'ensemble de ces dispositions, combinées avec celles du second alinéa de l'article 642 du code de procédure civile selon lesquelles " Le délai qui expirerait normalement un samedi, un dimanche ou un jour férié ou chômé est prorogé jusqu'au premier jour ouvrable suivant ", que les réclamations contre les opérations électorales qui se sont déroulées le 15 mars 2020 pouvaient être formées au plus tard le lundi 25 mai 2020 à dix-huit heures.<br/>
<br/>
              5. Il résulte de l'instruction que la protestation de M. F... a été enregistrée le 23 mars 2020 au greffe du tribunal administratif de Châlons-en-Champagne, soit après l'expiration du délai normalement imparti par l'article R. 119 du code électoral mais avant le terme du délai indiqué au point 4, découlant de l'application des dispositions du 3° du II de l'article 15 de l'ordonnance du 25 mars 2020. Ces dernières dispositions devant être regardées comme ayant relevé de la forclusion encourue les protestations enregistrées entre l'expiration du délai normalement imparti et leur entrée en vigueur le 27 mars 2020, ainsi qu'elles pouvaient le faire sur le fondement du 2° du I de l'article 11 de la loi du 23 mars 2020, il y a lieu d'annuler l'ordonnance attaquée.<br/>
<br/>
              6. Le délai imparti au tribunal administratif par l'article 17 de l'ordonnance du 25 mars 2020, dans sa rédaction issue de la loi du 22 juin 2020 tendant à sécuriser l'organisation du second tour des élections municipales et communautaires de juin 2020 et à reporter les élections consulaires, pour statuer sur la protestation de M. B..., est expiré. Dès lors, il y a lieu pour le Conseil d'Etat de statuer immédiatement sur cette protestation. A ce titre, aucun des griefs qu'il soulève dans sa requête présentée devant le Conseil d'Etat le 30 avril 2020 ne peut, dès lors que le délai de protestation n'était pas expiré à cette date, être écarté comme irrecevable au motif qu'il serait nouveau.<br/>
<br/>
              Sur la campagne et la propagande électorales :<br/>
<br/>
              7. En premier lieu, aux termes de l'article L. 52-1 du code électoral : " Pendant les six mois précédant le premier jour du mois d'une élection et jusqu'à la date du tour de scrutin où celle-ci est acquise, l'utilisation à des fins de propagande électorale de tout procédé de publicité commerciale par la voie de la presse ou par tout moyen de communication audiovisuelle est interdite. / A compter du premier jour du sixième mois précédant le mois au cours duquel il doit être procédé à des élections générales, aucune campagne de promotion publicitaire des réalisations ou de la gestion d'une collectivité ne peut être organisée sur le territoire des collectivités intéressées par le scrutin. Sans préjudice des dispositions du présent chapitre, cette interdiction ne s'applique pas à la présentation, par un candidat ou pour son compte, dans le cadre de l'organisation de sa campagne, du bilan de la gestion des mandats qu'il détient ou qu'il a détenus. Les dépenses afférentes sont soumises aux dispositions relatives au financement et au plafonnement des dépenses électorales contenues au chapitre V bis du présent titre ".<br/>
<br/>
              8. Il résulte de l'instruction qu'un bulletin municipal a été diffusé le lundi 9 mars 2020, six jours avant le jour du scrutin, alors que ses deux précédentes diffusions dataient de février 2016 et septembre 2018. Toutefois, si ce bulletin comporte un éditorial d'une page du maire sortant, celui-ci n'était pas candidat et se bornait à indiquer " souhaite[r] pour tous une commune qui continue la gestion maîtrisée : condition d'un dynamisme dans tous les domaines ", sans évoquer la candidature de la liste conduite par M. H..., conseiller municipal sortant. Ce dernier ne fait pas davantage l'objet d'un traitement particulier dans les dix autres pages du bulletin, qui traitent de réalisations de la municipalité sans excéder l'objet d'une telle publication, qui est d'informer les habitants sur la vie de leur commune, sans employer un ton polémique ou dresser un bilan exagérément avantageux de ces réalisations. S'il est fait mention des élections municipales dans la partie consacrée à la citoyenneté, présentées comme une " belle occasion pour participer à la vie communale ", ce bref paragraphe se contente de rappeler les localisations des bureaux de vote et les possibilités de stationnement, sans même mentionner les listes en présence. Enfin, l'illustration de cette publication par des photographies sur lesquelles figurent les élus de l'équipe municipale sortante, dont M. H... à cinq reprises, n'a pas excédé les limites de la communication institutionnelle. Par suite, M. B... n'est pas fondé à soutenir que la publication de ce bulletin devrait être regardée comme une campagne de promotion publicitaire prohibée par l'article L. 52-1 du code électoral ni, en tout état de cause, à demander au Conseil d'Etat d'en imputer les frais de conception, d'édition et de distribution au compte de campagne de la liste " Frignicourt agir ensemble 2020-2026 ".<br/>
<br/>
              9. En deuxième lieu, la circonstance, à la supposer avérée, que des pressions auraient été exercées par plusieurs agents municipaux sur Mme I... D..., candidate de la liste conduite par M. F..., ne saurait, alors qu'il ne résulte nullement de l'instruction qu'elles se seraient exercées sur d'autres électeurs, avoir pu altérer la sincérité du scrutin.<br/>
<br/>
              10. En troisième lieu, s'il résulte de l'instruction que des accusations à l'encontre de M. B..., liées à un litige auquel il était partie lorsqu'il était maire de Frignicourt, ont été publiées, selon lui environ une semaine avant le scrutin, sur la page du réseau social " Facebook " intitulée " Frignicourt autrement ", nom d'une liste candidate aux élections municipales de juin 2015, il ne résulte pas de l'instruction que M. B... aurait été dans l'incapacité de répondre en temps utile à ces propos, dont l'ampleur de la diffusion n'est pas établie. Dans ces circonstances et compte tenu de l'important écart des voix entre les listes candidates, ces publications, pour regrettables qu'elles soient, n'ont pas été de nature à altérer la sincérité du scrutin.<br/>
<br/>
              11. En quatrième lieu, il n'est pas contesté qu'une affiche électorale de la liste " Frignicourt, avec vous, pour demain ", figurant à l'emplacement officiel prévu à proximité des deux salles de vote de la commune, a été dégradée la veille du scrutin. Il résulte toutefois de l'instruction que cette dégradation, qui ne pouvait appeler en elle-même une réponse, ne peut être regardée, compte tenu de son caractère ponctuel et isolé, comme ayant porté atteinte à la sincérité du scrutin. <br/>
<br/>
              12. En cinquième lieu, il résulte de l'instruction, notamment de la facture produite en défense, que, contrairement à ce qu'allègue M. B..., les circulaires adressées aux électeurs par la liste " Frignicourt agir ensemble 2020-2026 " ainsi que les bulletins de vote de cette liste ont été imprimés sur un papier n'excédant pas le grammage de 70 grammes au mètre carré, conformément aux dispositions respectives des articles R. 29 et R. 30 du code électoral.<br/>
<br/>
              Sur les opérations électorales :<br/>
<br/>
              13. En premier lieu, le premier alinéa de l'article R. 42 du code électoral prévoit que : " Chaque bureau de vote est composé d'un président, d'au moins deux assesseurs et d'un secrétaire choisi par eux parmi les électeurs de la commune ". L'article R. 44 du même code dispose, à son deuxième alinéa, que chaque candidat, binôme de candidat ou liste en présence a le droit de désigner un seul assesseur titulaire par bureau de vote et, à son troisième alinéa, que des assesseurs supplémentaires peuvent être désignés par le maire parmi les conseillers municipaux dans l'ordre du tableau puis, le cas échéant, parmi les électeurs de la commune. Aux termes de l'article R. 46 du même code : " Les nom, prénoms, date et lieu de naissance et adresse des assesseurs et de leurs suppléants désignés par les candidats, binômes de candidats ou listes en présence, ainsi que l'indication du bureau de vote auquel ils sont affectés, sont notifiés au maire au plus tard à dix-huit heures le troisième jour précédant le scrutin. / Le maire délivre un récépissé de cette déclaration. Ce récépissé servira de titre et garantira les droits attachés à la qualité d'assesseur ou de suppléant (...) ". Les dispositions de cet article, qui ont pour seul objet de fixer les conditions dans lesquelles les candidats, binômes de candidats ou listes en présence peuvent faire valoir le droit de désigner un assesseur titulaire par bureau de vote qu'ils tiennent de l'article R. 44 du code électoral, ne sont pas applicables à la désignation par le maire d'assesseurs supplémentaires et n'ont ni pour objet ni pour effet d'imposer la constitution des bureaux de vote dès l'échéance qu'elles fixent pour la notification au maire des personnes à désigner. Par suite, M. B... n'est pas fondé à soutenir que la constitution des bureaux de vote aurait été irrégulière et tardive au motif que le maire l'aurait sollicité l'avant-veille du scrutin en vue qu'il lui propose avant 12 heures des assesseurs supplémentaires pour chaque bureau de vote, sans lui délivrer de récépissé. <br/>
<br/>
              14. En deuxième lieu, si M. B... soutient qu'une personne a été admise au vote au bureau n° 1 en méconnaissance des dispositions de l'article R. 59 du code électoral, en vertu desquelles nul ne peut être admis à voter s'il n'est inscrit sur la liste électorale, à l'exception des électeurs porteurs d'une décision du juge du tribunal judiciaire ordonnant leur inscription ou d'un arrêt de la Cour de cassation annulant un jugement qui aurait prononcé leur radiation, il résulte de l'instruction que l'intéressé était régulièrement inscrit sur les listes électorales du bureau n° 2. Par suite, et alors qu'il n'est pas allégué que l'admission au vote de cet électeur résulterait d'une manoeuvre ou lui aurait permis de voter deux fois, le grief doit être écarté. <br/>
<br/>
              15. En troisième lieu, aux termes du premier alinéa de l'article R. 60 du code électoral : " Les électeurs des communes de 1 000 habitants et plus doivent présenter au président du bureau, au moment du vote, en même temps que la carte électorale ou l'attestation d'inscription en tenant lieu, un titre d'identité ; la liste des titres valables est établie par arrêté du ministre de l'intérieur ". Si M. B... fait valoir que plusieurs électeurs auraient été admis au vote sans qu'un titre d'identité leur soit réclamé, il n'établit ni même n'allègue que les intéressés n'auraient pas été régulièrement inscrits sur la liste électorale ou qu'ils auraient voté sous une fausse identité. En l'absence de toute indication de nature à suggérer l'existence d'une fraude, le grief ne peut dès lors qu'être écarté.<br/>
<br/>
              16. En quatrième lieu, les allégations du requérant suivant lesquelles, d'une part, des candidats de la liste " Frignicourt agir ensemble 2020-2026 ", en se fondant sur les informations transmises par M. H..., assesseur au bureau n° 1, auraient contacté des électeurs pendant le déroulement du scrutin pour les inciter à venir voter, et, d'autre part, le maire se serait entretenu avec plusieurs électeurs dans son bureau alors qu'ils se rendaient au vote,  ne sont pas assorties des précisions permettant d'en apprécier le bien-fondé. <br/>
<br/>
              17. En cinquième lieu, si M. B... soutient, sans être contesté, que les listes d'émargement n'ont été signées par les membres du bureau qu'à l'issue du dépouillement et non, comme le prescrit l'article R. 62 du code électoral, dès la clôture du scrutin, il n'est pas établi, ni même allégué, que cette circonstance ait pu constituer une manoeuvre de nature à altérer la sincérité des opérations électorales. <br/>
<br/>
              Sur les effets de la crise sanitaire sur la sincérité du scrutin :<br/>
<br/>
              18. L'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre des solidarités et de la santé à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Dans ce contexte, le Premier ministre a adressé à l'ensemble des maires le 7 mars 2020 une lettre présentant les mesures destinées à assurer le bon déroulement des élections municipales et communautaires prévues les 15 et 22 mars 2020. Ces mesures ont été précisées par une circulaire du ministre de l'intérieur du 9 mars 2020 relative à l'organisation des élections municipales des 15 et 22 mars 2020 en situation d'épidémie de coronavirus covid-19, formulant des recommandations relatives à l'aménagement des bureaux de vote et au respect des consignes sanitaires, et par une instruction de ce ministre, du même jour, destinée à faciliter l'exercice du droit de vote par procuration. Après consultation par le Gouvernement du conseil scientifique mis en place pour lui donner les informations scientifiques utiles à l'adoption des mesures nécessaires pour faire face à l'épidémie de covid-19, les 12 et 14 mars 2020, le premier tour des élections municipales a eu lieu comme prévu le 15 mars 2020. A l'issue du scrutin, les conseils municipaux ont été intégralement renouvelés dans 30 143 communes ou secteurs. Le taux d'abstention a atteint 55,34 % des inscrits, contre 36,45 % au premier tour des élections municipales de 2014.<br/>
<br/>
              19. Au vu de la situation sanitaire, l'article 19 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 a reporté le second tour des élections, initialement fixé au 22 mars 2020, au plus tard en juin 2020 et prévu que : " Dans tous les cas, l'élection régulière des conseillers municipaux et communautaires, des conseillers d'arrondissement, des conseillers de Paris et des conseillers métropolitains de Lyon élus dès le premier tour organisé le 15 mars 2020 reste acquise, conformément à l'article 3 de la Constitution ". Ainsi que le Conseil constitutionnel l'a jugé dans sa décision n° 2020-849 QPC du 17 juin 2020, ces dispositions n'ont ni pour objet ni pour effet de valider rétroactivement les opérations électorales du premier tour ayant donné lieu à l'attribution de sièges et ne font ainsi pas obstacle à ce que ces opérations soient contestées devant le juge de l'élection.<br/>
<br/>
              20. Aux termes de l'article L. 262 du code électoral, applicable aux communes de mille habitants et plus : " Au premier tour de scrutin, il est attribué à la liste qui a recueilli la majorité absolue des suffrages exprimés un nombre de sièges égal à la moitié du nombre des sièges à pourvoir, arrondi, le cas échéant, à l'entier supérieur lorsqu'il y a plus de quatre sièges à pourvoir et à l'entier inférieur lorsqu'il y a moins de quatre sièges à pourvoir. Cette attribution opérée, les autres sièges sont répartis entre toutes les listes à la représentation proportionnelle suivant la règle de la plus forte moyenne, sous réserve de l'application des dispositions du troisième alinéa ci-après. / Si aucune liste n'a recueilli la majorité absolue des suffrages exprimés au premier tour, il est procédé à un deuxième tour (...) ". Aux termes de l'article L. 273-8 du code électoral : " Les sièges de conseiller communautaire sont répartis entre les listes par application aux suffrages exprimés lors de cette élection des règles prévues à l'article L. 262. (...) ".<br/>
<br/>
              21. Ni par ces dispositions ni par celles de la loi du 23 mars 2020 le législateur n'a subordonné à un taux de participation minimal la répartition des sièges au conseil municipal et au conseil communautaire à l'issue du premier tour de scrutin dans les communes de mille habitants et plus, lorsqu'une liste a recueilli la majorité absolue des suffrages exprimés. Le niveau de l'abstention n'est ainsi, par lui-même, pas de nature à remettre en cause les résultats du scrutin, s'il n'a pas altéré, dans les circonstances de l'espèce, sa sincérité.<br/>
<br/>
              22. En l'espèce, M. B... fait valoir que le taux de participation s'est élevé à 50,72 % dans la commune, soit un pourcentage inférieur aux élections de 2014 et de 2008, et soutient que les conditions de l'accueil des électeurs du bureau n° 2, situé dans une salle communale, connues des habitants de la commune, n'étaient pas adaptées à la situation sanitaire et seraient à l'origine d'un taux de participation inférieur à celui du bureau n° 1. Si l'écart de participation entre les deux bureaux est de plus de onze points, il ne résulte pas de l'instruction que la configuration des locaux ou les modalités de mise en oeuvre des consignes sanitaires données pour l'organisation du scrutin aient pu, en raison du contexte sanitaire, dissuader certains électeurs d'y participer au scrutin. En l'absence de l'invocation de toute autre circonstance relative au déroulement de la campagne électorale ou du scrutin dans la commune qui montrerait, en particulier, qu'il aurait été porté atteinte au libre exercice du droit de vote ou à l'égalité entre les candidats, le niveau de l'abstention constatée ne peut être regardé comme ayant altéré la sincérité du scrutin.<br/>
<br/>
              Sur les autres griefs :<br/>
<br/>
              23. Si M. B... soutient que plusieurs dizaines d'électeurs ont reçu fin janvier 2020 une notification d'intention de radiation des listes électorales ayant pu les dissuader de prendre part au scrutin alors même qu'ils étaient toujours inscrits sur la liste électorale, il ne résulte de l'instruction, ni que ces courriers aient produit un tel effet, ni qu'ils n'auraient pas été envoyés en application des dispositions de l'article L. 18 du code électoral en vertu desquelles le maire ne peut radier les électeurs ne remplissant plus les conditions d'inscription sur les listes électorales qu'à l'issue d'une procédure contradictoire.<br/>
<br/>
              24. Enfin, si M. B... soutient que sa correspondance personnelle reçue en mairie aurait été ouverte par les services municipaux durant la période pré-électorale, cette circonstance, à la supposer avérée, n'a pu porter atteinte à la sincérité du scrutin. <br/>
<br/>
              25. Il résulte de tout ce qui précède que M. B... n'est pas fondé à demander l'annulation des opérations électorales qui se sont déroulées le 15 mars 2020 pour l'élection des conseillers municipaux et communautaires de la commune de Frignicourt.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du président du tribunal administratif de Châlons-en-Champagne du 24 mars 2020 est annulée.<br/>
<br/>
Article 2 : La protestation de M. B... est rejetée.<br/>
Article 3 : La présente décision sera notifiée à M. C... B..., à Mme I... D..., à M. E... H..., premier dénommé, pour l'ensemble des autres défendeurs, et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-08-01-02 ÉLECTIONS ET RÉFÉRENDUM. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INTRODUCTION DE L'INSTANCE. DÉLAIS. - ELECTIONS MUNICIPALES DE 2020 - PREMIER TOUR (15 MARS) - DÉLAI DE CONTESTATION ÉCHÉANT LE 25 MAI - TARDIVETÉ OPPOSÉE PAR LE JUGE DE PREMIÈRE INSTANCE - RELEVÉ DE FORCLUSION EN APPEL [RJ1] - JUGE D'APPEL STATUANT IMMÉDIATEMENT EN RAISON DE L'EXPIRATION DU DÉLAI IMPARTI AU PREMIER JUGE - GRIEFS SOULEVÉS POUR LA PREMIÈRE FOIS EN APPEL - GRIEFS IRRECEVABLES [RJ2] - ABSENCE, DÈS LORS QU'ILS ONT ÉTÉ SOULEVÉS AVANT LE 25 MAI.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">28-08-05-02-02 ÉLECTIONS ET RÉFÉRENDUM. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS DU JUGE. GRIEFS. GRIEFS RECEVABLES. - ELECTIONS MUNICIPALES DE 2020 - PREMIER TOUR (15 MARS) - DÉLAI DE CONTESTATION ÉCHÉANT LE 25 MAI - TARDIVETÉ OPPOSÉE PAR LE JUGE DE PREMIÈRE INSTANCE - RELEVÉ DE FORCLUSION EN APPEL [RJ1] - JUGE D'APPEL STATUANT IMMÉDIATEMENT EN RAISON DE L'EXPIRATION DU DÉLAI IMPARTI AU PREMIER JUGE - GRIEFS SOULEVÉS POUR LA PREMIÈRE FOIS EN APPEL - GRIEFS IRRECEVABLES [RJ2] - ABSENCE, DÈS LORS QU'ILS ONT ÉTÉ SOULEVÉS AVANT LE 25 MAI.
</SCT>
<ANA ID="9A"> 28-08-01-02 Il résulte de l'article 11 de la loi n° 2020-290 du 23 mars 2020, du 3° du II de l'article 15 de l'ordonnance n° 2020-305 du 25 mars 2020 et de l'article 1er du décret n° 2020-571 du 14 mai 2020, combinés avec le second alinéa de l'article 642 du code de procédure civile (CPC), que les réclamations contre les opérations électorales qui se sont déroulées le 15 mars 2020 pouvaient être formées au plus tard le lundi 25 mai 2020 à dix-huit heures.,,,Protestation enregistrée le 23 mars 2020 au greffe du tribunal administratif, soit après l'expiration du délai normalement imparti par l'article R. 119 du code électoral mais avant le terme du délai indiqué au point précédent, découlant du 3° du II de l'article 15 de l'ordonnance du 25 mars 2020. Ces dernières dispositions devant être regardées comme ayant relevé de la forclusion encourue les protestations enregistrées entre l'expiration du délai normalement imparti et leur entrée en vigueur le 27 mars 2020, ainsi qu'elles pouvaient le faire sur le fondement du 2° du I de l'article 11 de la loi du 23 mars 2020, il y a lieu d'annuler l'ordonnance attaquée rejetant comme tardive la protestation.,,,Le délai imparti au tribunal administratif par l'article 17 de l'ordonnance du 25 mars 2020, dans sa rédaction issue de la loi n° 2020-760 du 22 juin 2020, pour statuer sur la protestation est expiré. Dès lors, il y a lieu pour le Conseil d'État de statuer immédiatement sur cette protestation.... ,,A ce titre, aucun des griefs soulevés par le requérant dans sa requête présentée devant le Conseil d'Etat le 30 avril 2020 ne peut, dès lors que le délai de protestation n'était pas expiré à cette date, être écarté comme irrecevable au motif qu'il serait nouveau.</ANA>
<ANA ID="9B"> 28-08-05-02-02 Il résulte de l'article 11 de la loi n° 2020-290 du 23 mars 2020, du 3° du II de l'article 15 de l'ordonnance n° 2020-305 du 25 mars 2020 et de l'article 1er du décret n° 2020-571 du 14 mai 2020, combinés avec le second alinéa de l'article 642 du code de procédure civile (CPC), que les réclamations contre les opérations électorales qui se sont déroulées le 15 mars 2020 pouvaient être formées au plus tard le lundi 25 mai 2020 à dix-huit heures.,,,Protestation enregistrée le 23 mars 2020 au greffe du tribunal administratif, soit après l'expiration du délai normalement imparti par l'article R. 119 du code électoral mais avant le terme du délai indiqué au point précédent, découlant du 3° du II de l'article 15 de l'ordonnance du 25 mars 2020. Ces dernières dispositions devant être regardées comme ayant relevé de la forclusion encourue les protestations enregistrées entre l'expiration du délai normalement imparti et leur entrée en vigueur le 27 mars 2020, ainsi qu'elles pouvaient le faire sur le fondement du 2° du I de l'article 11 de la loi du 23 mars 2020, il y a lieu d'annuler l'ordonnance attaquée rejetant comme tardive la protestation.,,,Le délai imparti au tribunal administratif par l'article 17 de l'ordonnance du 25 mars 2020, dans sa rédaction issue de la loi du 22 juin 2020 tendant à sécuriser l'organisation du second tour des élections municipales et communautaires de juin 2020 et à reporter les élections consulaires, pour statuer sur la protestation, est expiré. Dès lors, il y a lieu pour le Conseil d'État de statuer immédiatement sur cette protestation.... ,,A ce titre, aucun des griefs soulèves par le requérant dans sa requête présentée devant le Conseil d'Etat le 30 avril 2020 ne peut, dès lors que le délai de protestation n'était pas expiré à cette date, être écarté comme irrecevable au motif qu'il serait nouveau.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 15 juillet 2020, Elections municipales et communautaires de Saint-Sulpice-sur-Risle,, n° 440055, à mentionner aux Tables.,,[RJ2] Cf., sur l'irrecevabilité des griefs soulevés pour la première fois en appel, CE, 28 janvier 1994, Bartolone, Elections cantonales des Lilas, n° 143531, p. 41.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
