<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038405416</ID>
<ANCIEN_ID>JG_L_2019_04_000000389105</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/40/54/CETATEXT000038405416.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 05/04/2019, 389105, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-04-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389105</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Sylvain Monteillet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:389105.20190405</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une décision du 29 mars 2017, le Conseil d'Etat, statuant au contentieux sur le pourvoi de la société Morgan Stanley et Co International PLC, aux droits de laquelle vient la société Morgan Stanley France, tendant à l'annulation de l'arrêt n° 10VE01053, 11VE03805 du 27 janvier 2015 par lequel la cour administrative d'appel de Versailles a rejeté les appels formés par cette société contre les jugements n° 0813024 du 11 février 2010 et n° 1010984 du 20 octobre 2011 du tribunal administratif de Montreuil rejetant ses demandes tendant à la décharge des rappels de taxe sur la valeur ajoutée qui lui ont été réclamés au titre des périodes du 1er décembre 2002 au 30 avril 2005 et du 1er décembre 2005 au 30 avril 2009 ainsi que des pénalités correspondantes, a sursis à statuer jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur les questions suivantes :<br/>
<br/>
              - dans l'hypothèse où les dépenses supportées par une succursale établie dans un premier Etat membre sont exclusivement affectées à la réalisation des opérations de son siège établi dans un autre Etat membre, les dispositions des articles 17, paragraphes 2, 3 et 5, et 19, paragraphe 1, de la sixième directive 77/388/CEE, reprises aux articles 168, 169 et 173 à 175 de la directive 2006/112/CE, doivent-elles être interprétées en ce sens qu'elles impliquent que l'Etat membre de la succursale applique à ces dépenses le prorata de déduction de la succursale, déterminé en fonction des opérations qu'elle réalise dans son Etat d'immatriculation et des règles applicables dans cet Etat, ou le prorata de déduction du siège, ou encore un prorata de déduction spécifique combinant les règles applicables dans les Etats membres d'immatriculation de la succursale et du siège, en particulier au regard de l'existence éventuelle d'un régime d'option pour l'imposition des opérations à la taxe sur la valeur ajoutée '<br/>
              - quelles règles convient-il d'appliquer dans l'hypothèse particulière où les dépenses supportées par la succursale concourent à la réalisation de ses opérations dans son Etat d'immatriculation et à celle des opérations du siège, notamment au regard de la notion de frais généraux et du prorata de déduction '<br/>
<br/>
              Par un arrêt C-165/17 du 24 janvier 2019, la Cour de justice de l'Union européenne s'est prononcée sur ces questions.<br/>
<br/>
              Par un nouveau mémoire, enregistré le 13 février 2019, la société Morgan Stanley France, d'une part, persiste dans ses conclusions tendant à l'annulation de l'arrêt et à ce que soit mise à la charge de l'Etat une somme de 15 000 euros au titre de l'article L. 761-1 du code de justice administrative, et d'autre part, conclut à ce que l'affaire soit renvoyée, après cassation, devant la cour administrative d'appel de Versailles.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier, y compris celles visées par la décision du Conseil d'Etat du 29 mars 2017 ;<br/>
              Vu :<br/>
              - la directive 77/388/CEE du Conseil du 17 mai 1977 ;<br/>
              - la directive 2006/112/CE du Conseil du 28 novembre 2006 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - l'arrêt C-165/17 du 24 janvier 2019 de la Cour de justice de l'Union européenne ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sylvain Monteillet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de la société Morgan Stanley et Co International PLC ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la succursale parisienne de la société Morgan Stanley et Co International PLC, entreprise d'investissement de droit anglais, a fait l'objet de deux vérifications de comptabilité portant, en matière de taxe sur la valeur ajoutée, sur les périodes du 1er décembre 2002 au 30 avril 2005 et du 1er décembre 2005 au 30 avril 2009.<br/>
<br/>
              2. A l'occasion de ces contrôles, il a été constaté que la succursale, établissement stable au regard des règles applicables en matière de taxe sur la valeur ajoutée, réalisait, d'une part, des opérations bancaires et financières pour ses clients locaux pour lesquelles elle avait opté pour l'assujettissement à la taxe sur la valeur ajoutée et, d'autre part, des services au bénéfice du siège britannique en contrepartie desquels elle recevait des virements. La succursale a déduit l'intégralité de la taxe sur la valeur ajoutée ayant frappé les dépenses afférentes à ces deux catégories de prestations.<br/>
<br/>
              3. L'administration fiscale a estimé que la taxe sur la valeur ajoutée ayant grevé l'acquisition des biens et services utilisés exclusivement pour les opérations internes réalisées avec le siège ne pouvait ouvrir droit à déduction au motif que ces opérations étaient situées hors du champ d'application de la taxe sur la valeur ajoutée, mais a toutefois admis, par mesure de tempérament, la déduction d'une fraction de la taxe en cause par application du prorata de déduction du siège britannique, sous réserve des exclusions au droit à déduction en vigueur en France. S'agissant des dépenses mixtes, afférentes aux opérations réalisées à la fois avec le siège et avec les clients de la succursale, l'administration a considéré qu'elles n'étaient que partiellement déductibles et a appliqué le prorata de déduction du siège, corrigé du chiffre d'affaires de la succursale ouvrant droit à déduction, sous réserve des exclusions au droit à déduction en vigueur en France.<br/>
<br/>
              4. La société Morgan Stanley et Co International PLC, aux droits de laquelle est venue la société Morgan Stanley France, demande l'annulation de l'arrêt du 27 janvier 2015 par lequel la cour administrative d'appel de Versailles a rejeté les appels qu'elle a formés contre les jugements du tribunal administratif de Montreuil des 11 février 2010 et 20 octobre 2011 rejetant ses demandes tendant à la décharge des rappels de taxe sur la valeur ajoutée qui lui ont été réclamés par voie de conséquence des rectifications ci-dessus mentionnées.<br/>
<br/>
              5. Aux termes de l'article 17 de la sixième directive 77/388/CEE du Conseil, du 17 mai 1977, en vigueur jusqu'au 31 décembre 2006 : " 2. Dans la mesure ou` les biens et les services sont utilisés pour les besoins de ses opérations taxées, l'assujetti est autorisé 0 déduire de la taxe dont il est redevable : / a) la taxe sur la valeur ajoutée due ou acquittée pour les biens qui lui sont ou lui seront livrés et pour les services qui lui sont ou lui seront rendus par un autre assujetti ; / (...) 3. Les Etats membres accordent également 0 tout assujetti la déduction ou le remboursement de la taxe sur la valeur ajoutée visée au paragraphe 2 dans la mesure ou` les biens et les services sont utilisés pour les besoins : / a) de ses opérations relevant des activités économiques visées 0 l'article 4, paragraphe 2, effectuées à l'étranger, qui ouvriraient droit 0 déduction si ces opérations étaient effectuées 0 l'intérieur du pays ; / (...) 5. En ce qui concerne les biens et les services qui sont utilisés par un assujetti pour effectuer 0 la fois des opérations ouvrant droit 0 déduction visées aux paragraphes 2 et 3 et des opérations n'ouvrant pas droit 0 déduction, la déduction n'est admise que pour la partie de la taxe sur la valeur ajoutée qui est proportionnelle au montant afférent aux premières opérations. / Ce prorata est déterminé´ pour l'ensemble des opérations effectuées par l'assujetti conformément 0 l'article 19. (...) ". Aux termes de l'article 19 de cette même directive : " 1. Le prorata de déduction, prévu par l'article 17, paragraphe 5, premier alinéa, résulte d'une fraction comportant : / - au numérateur, le montant total, déterminé´ par année, du chiffre d'affaires, taxe sur la valeur ajoutée exclue, afférent aux opérations ouvrant droit à déduction conformément 0 l'article 17, paragraphes 2 et 3, / - au dénominateur, le montant total, déterminé par année, du chiffre d'affaires, taxe sur la valeur ajoutée exclue, afférent aux opérations figurant au numérateur ainsi qu'aux opérations qui n'ouvrent pas droit à déduction. (...) / Le prorata est déterminé´ sur une base annuelle, fixé en pourcentage et arrondi à un chiffre qui ne dépassé pas l'unité supérieure ".<br/>
<br/>
              6. Aux termes de l'article 168 de la directive 2006/112/CE du Conseil, du 28 novembre 2006, relative au système commun de taxe sur la valeur ajoutée, en vigueur à compter du 1er janvier 2007 : " Dans la mesure où les biens et les services sont utilisés pour les besoins de ses opérations taxées, l'assujetti a le droit, dans l'Etat membre dans lequel il effectue ces opérations, de déduire du montant de la taxe dont il est redevable les montants suivants: / a) la taxe sur la valeur ajoutée due ou acquittée dans cet Etat membre pour les biens qui lui sont ou lui seront livrés et pour les services qui lui sont ou lui seront fournis par un autre assujetti (...) ". Aux termes de l'article 169 de la même directive : " Outre la déduction visée à l'article 168, l'assujetti a le droit de déduire la taxe sur la valeur ajoutée y visée dans la mesure où les biens et les services sont utilisés pour les besoins des opérations suivantes : / a) ses opérations relevant des activités visées à l'article 9, paragraphe 1, deuxième alinéa, effectuées en dehors de l'Etat membre dans lequel cette taxe est due ou acquittée, qui ouvriraient droit à déduction si ces opérations étaient effectuées dans cet Etat membre (...) ". Aux termes de l'article 173 de cette même directive : " 1. En ce qui concerne les biens et les services utilisés par un assujetti pour effectuer à la fois des opérations ouvrant droit à déduction visées aux articles 168, 169 et 170 et des opérations n'ouvrant pas droit à déduction, la déduction n'est admise que pour la partie de la taxe sur la valeur ajoutée qui est proportionnelle au montant afférent aux premières opérations. / Le prorata de déduction est déterminé, conformément aux articles 174 et 175, pour l'ensemble des opérations effectuées par l'assujetti. (...) ". Aux termes de l'article 174 de la même directive : " 1. Le prorata de déduction résulte d'une fraction comportant les montants suivants : / a) au numérateur, le montant total, déterminé par année, du chiffre d'affaires, hors taxe sur la valeur ajoutée, afférent aux opérations ouvrant droit à déduction conformément aux articles 168 et 169 ; / b) au dénominateur, le montant total, déterminé par année, du chiffre d'affaires, hors taxe sur la valeur ajoutée, afférent aux opérations figurant au numérateur ainsi qu'aux opérations qui n'ouvrent pas droit à déduction (...) ". Aux termes de l'article 175 de la directive : " 1. Le prorata de déduction est déterminé sur une base annuelle, fixé en pourcentage et arrondi à un chiffre qui ne dépasse pas l'unité supérieure (...) ".<br/>
<br/>
              7. Aux termes de l'article 271 du code général des impôts : " I. 1 La taxe sur la valeur ajoutée qui a grevé les éléments du prix d'une opération imposable est déductible de la taxe sur la valeur ajoutée applicable à cette opération (...) ". Aux termes de l'article 212 de l'annexe II à ce code, applicable jusqu'au 31 décembre 2007 : " 1. Les redevables qui, dans le cadre de leurs activités situées dans le champ d'application de la taxe sur la valeur ajoutée, ne réalisent pas exclusivement des opérations ouvrant droit à déduction sont autorisés à déduire une fraction de la taxe sur la valeur ajoutée qui a grevé les biens constituant des immobilisations utilisées pour effectuer ces activités. / Cette fraction est égale au montant de la taxe déductible obtenu, après application, le cas échéant, des dispositions de l'article 207 bis, multiplié par le rapport existant entre :/ a) Au numérateur, le montant total annuel du chiffre d'affaires, taxe sur la valeur ajoutée exclue, afférent aux opérations ouvrant droit à déduction y compris les subventions directement liées au prix de ces opérations ; / b) Au dénominateur, le montant total annuel du chiffre d'affaires, taxe sur la valeur ajoutée exclue, afférent aux opérations figurant au numérateur ainsi qu'aux opérations qui n'ouvrent pas droit à déduction, et de l'ensemble des subventions, y compris celles qui ne sont pas directement liées au prix de ces opérations (...) ". Aux termes de l'article 205 de la même annexe, applicable à compter du 1er janvier 2008 : " La taxe sur la valeur ajoutée grevant un bien ou un service qu'un assujetti à cette taxe acquiert, importe ou se livre à lui-même est déductible à proportion de son coefficient de déduction ". Aux termes de l'article 206 de la même annexe : " I.-Le coefficient de déduction mentionné à l'article 205 est égal au produit des coefficients d'assujettissement, de taxation et d'admission. / II.-Le coefficient d'assujettissement d'un bien ou d'un service est égal à sa proportion d'utilisation pour la réalisation d'opérations imposables. Les opérations imposables s'entendent des opérations situées dans le champ d'application de la taxe sur la valeur ajoutée en vertu des articles 256 et suivants du code général des impôts, qu'elles soient imposées ou légalement exonérées. / III.-1. Le coefficient de taxation d'un bien ou d'un service est égal à l'unité lorsque les opérations imposables auxquelles il est utilisé ouvrent droit à déduction. / 2. Le coefficient de taxation d'un bien ou d'un service est nul lorsque les opérations auxquelles il est utilisé n'ouvrent pas droit à déduction. / 3. Lorsque le bien ou le service est utilisé concurremment pour la réalisation d'opérations imposables ouvrant droit à déduction et d'opérations imposables n'ouvrant pas droit à déduction, le coefficient de taxation est calculé selon les modalités suivantes : / 1° Ce coefficient est égal au rapport entre : / a. Au numérateur, le montant total annuel du chiffre d'affaires afférent aux opérations ouvrant droit à déduction, y compris les subventions directement liées au prix de ces opérations ; / b. Et, au dénominateur, le montant total annuel du chiffre d'affaires afférent aux opérations imposables, y compris les subventions directement liées au prix de ces opérations (...) ". <br/>
<br/>
              8. En premier lieu, dans l'arrêt du 24 janvier 2019 par lequel elle s'est prononcée sur les questions dont le Conseil d'Etat, statuant au contentieux, l'avait saisie à titre préjudiciel, la Cour de justice de l'Union européenne a dit pour droit, pour l'application des dispositions citées au point 5 et 6, qu'une succursale immatriculée dans un Etat membre, lorsqu'elle n'exerce pas une activité économique indépendante, " est en droit de déduire, dans cet Etat, la taxe sur la valeur ajoutée grevant les biens et les services acquis qui présentent un lien direct et immédiat avec la réalisation des opérations taxées, y compris celles de son siège établi dans un autre Etat membre, avec lequel cette succursale forme un seul assujetti, à condition que ces dernières opérations ouvrent également droit à déduction si elles ont été effectuées dans l'Etat d'immatriculation de ladite succursale ".<br/>
<br/>
              9. En deuxième lieu, dans ce même arrêt, la Cour de justice de l'Union européenne a dit pour droit que les dispositions citées au point 5 et 6 " doivent être interprétées en ce sens que, en ce qui concerne les dépenses supportées par une succursale immatriculée dans un État membre, qui sont affectées, exclusivement, à la fois à des opérations soumises à la taxe sur la valeur ajoutée et à des opérations exonérées de cette taxe, réalisées par le siège de cette succursale établi dans un autre État membre, il y a lieu d'appliquer un prorata de déduction résultant d'une fraction dont le dénominateur est formé par le chiffre d'affaires, hors taxe sur la valeur ajoutée, constitué par ces seules opérations et dont le numérateur est formé par les opérations taxées qui ouvriraient également droit à déduction si elles étaient effectuées dans l'État membre d'immatriculation de ladite succursale, y compris lorsque ce droit à déduction résulte de l'exercice d'une option, exercée par cette dernière, consistant à soumettre à la taxe sur la valeur ajoutée les opérations réalisées dans cet État ". <br/>
<br/>
              10. En troisième et dernier lieu, dans ce même arrêt, la Cour de justice de l'Union européenne a dit pour droit que les dispositions citées au point 5 et 6 " doivent être interprétées en ce sens que, afin de déterminer le prorata de déduction applicable aux frais généraux d'une succursale immatriculée dans un État membre, qui concourent à la réalisation à la fois des opérations de cette succursale effectuées dans cet État et des opérations réalisées par le siège de celle-ci établi dans un autre État membre, il convient de tenir compte, au dénominateur de la fraction qui constitue ce prorata de déduction, des opérations réalisées tant par ladite succursale que par ce siège, étant précisé que doivent figurer au numérateur de ladite fraction, outre les opérations taxées effectuées par la même succursale, les seules opérations taxées réalisées par ledit siège, qui ouvriraient également droit à déduction si elles étaient effectuées dans l'État d'immatriculation de la succursale concernée ".<br/>
<br/>
              11. Pour juger que la société Morgan Stanley et Co International PLC n'était pas fondée à soutenir que c'était à tort que le tribunal administratif de Montreuil avait rejeté ses demandes, la cour administrative d'appel de Versailles, après avoir relevé qu'il n'était pas allégué que la succursale parisienne serait autonome par rapport à son siège, a relevé que les opérations internes effectuées par cette succursale au bénéfice du siège britannique étaient en dehors du champ d'application de la taxe sur la valeur ajoutée. Elle en a déduit, d'une part, que la société ne pouvait prétendre à la déduction de la taxe sur la valeur ajoutée ayant grevé les dépenses engagées par la succursale exclusivement en vue de réaliser des prestations au profit de son siège londonien et, d'autre part, qu'il y avait lieu de mettre en oeuvre le régime des déductions applicable aux assujettis partiels pour déterminer les droits à déduction de la taxe sur la valeur ajoutée ayant grevé les dépenses de la succursale affectées concurremment à la réalisation des opérations effectuées au profit du siège et des opérations réalisées avec sa propre clientèle.<br/>
<br/>
              12. En statuant de la sorte alors, ainsi qu'il a été dit au point 8, qu'une succursale est en droit de déduire la taxe sur la valeur ajoutée grevant les biens et les services acquis présentant un lien direct et immédiat avec, notamment, les opérations taxées réalisées dans un autre Etat membre par le siège avec lequel la succursale forme un seul assujetti, en appliquant les proratas de déduction calculés selon la méthode définie aux points 9 et 10, la cour a commis une erreur de droit.<br/>
<br/>
              13. Il résulte de tout ce qui précède que la société Morgan Stanley France, venant aux droits de la société Morgan Stanley et Co International PLC, est fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              14. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à la société Morgan Stanley France d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt n° 10VE01053, 11VE03805 de la cour administrative d'appel de Versailles du 27 janvier 2015 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles. <br/>
Article 3 : L'Etat versera à la société Morgan Stanley France une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société Morgan Stanley France et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
