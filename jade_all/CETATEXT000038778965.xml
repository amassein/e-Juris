<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038778965</ID>
<ANCIEN_ID>JG_L_2019_07_000000420668</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/77/89/CETATEXT000038778965.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 17/07/2019, 420668, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420668</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Liza Bellulo</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:420668.20190717</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société à responsabilité limitée (SARL) Moreau Desmazeau a demandé au tribunal administratif de Clermont-Ferrand de prononcer la décharge des compléments de taxe professionnelle et de cotisation foncière des entreprises auxquels elle a été assujettie au titre, respectivement, de l'année 2009 et des années 2010 à 2012 dans les rôles de la commune de Saint-Cyr-sur-Loire (Indre-et-Loire) à raison du magasin que la société Babou y met à sa disposition. Par un jugement n° 1500034 du 6 décembre 2016, ce tribunal a fait droit à ces demandes. <br/>
<br/>
              Par un arrêt n° 17LY00458 du 20 mars 2018, la cour administrative d'appel de Lyon a rejeté l'appel formé par le ministre de l'économie et des finances contre ce jugement. <br/>
<br/>
              Par un pourvoi enregistré le 16 mai 2018 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'action et des comptes publics demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Liza Bellulo, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Babou exerce une activité de distribution au travers de magasins dont elle confie la gérance à des entreprises indépendantes. Pour son imposition à la taxe professionnelle au titre des années 2002 à 2005, l'administration fiscale avait estimé que les locaux commerciaux au moyen lesquels cette activité était exercée restaient à sa disposition pour l'exercice de son activité professionnelle. En application de l'article 1467 du code général des impôts, elle avait, dès lors, intégré leur valeur locative dans les bases d'imposition de cette société. Par des arrêts du 23 décembre 2010 devenus définitifs, la cour administrative d'appel de Lyon a cependant infirmé cette analyse au motif que les locaux étaient sous le contrôle des mandataires auxquels la société Babou en confiait l'exploitation. L'administration fiscale a alors intégré la valeur locative des magasins de la société Babou dans la base imposable à la taxe professionnelle ou à la cotisation foncière des entreprises de chacun des exploitants. La société à responsabilité limitée (SARL) Moreau Desmazeau, qui exploitait un fonds de commerce de vente au détail de produits d'équipement du foyer et de la personne sur le territoire de la commune de Saint-Cyr-sur-Loire (Indre-et-Loire) en vertu d'une convention de gérance-mandat conclue avec la société Babou à effet du 1er janvier 2006, s'est ainsi vue notifier des rehaussements de sa base d'imposition à la taxe professionnelle pour l'année 2009 et à la cotisation foncière des entreprises pour les années 2010, 2011 et 2012. Le ministre de l'action et des comptes publics se pourvoit en cassation contre l'arrêt du 20 mars 2018 par lequel la cour administrative d'appel de Lyon a rejeté l'appel qu'il avait formé contre le jugement du 6 décembre 2016 du tribunal administratif de Clermont-Ferrand prononçant la décharge des compléments de taxe professionnelle et de cotisation foncière des entreprises auquel la société Moreau Desmazeau a été assujettie au titre, respectivement, de l'année 2009 et des années 2010 à 2012.<br/>
<br/>
              2. Aux termes de l'article L. 80 B du livre des procédures fiscales : " La garantie prévue au premier alinéa de l'article L. 80 A est applicable : / 1° Lorsque l'administration a formellement pris position sur l'appréciation d'une situation de fait au regard d'un texte fiscal (...) ". Peuvent se prévaloir de cette garantie, pour faire échec à l'application de la loi fiscale, les contribuables qui se trouvent dans la situation de fait sur laquelle l'appréciation invoquée a été portée, ainsi que les contribuables qui, à la date de la prise de position de l'administration, ont été partie à l'acte ou ont participé à l'opération qui a donné naissance à cette situation, sans que les autres contribuables puissent utilement invoquer une rupture à leur détriment du principe d'égalité.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que pour rejeter, par une décision du 2 janvier 2009, la réclamation que la société Babou avait formée contre son imposition à la taxe professionnelle au titre des années 2007 et 2008 dans les rôles de la commune de Saint-Cyr-sur-Loire, l'administration s'est fondée sur ce que " conformément aux dispositions de l'article 1478-I du CGI ", ces impositions " à la TP 2007 et 2008 [avaient] été établies sur la commune de St Cyr sur Loire d'après les éléments des déclarations de TP 1003 déposées dans les services par la SAS Babou " et sur ce que " la vérification de comptabilité effectuée en 2005 par la DVNI (MonsieurA...) a considéré que la valeur locative des biens passibles de taxe foncière ainsi que la valeur locative des biens non passibles de TF [devaient] être comprises dans les bases d'imposition de la SAS Babou ". <br/>
<br/>
              4. Pour juger que la société Moreau-Desmazeau était fondée à se prévaloir de cette décision, sur le fondement des dispositions de l'article L.80 B du livre des procédures fiscales, au soutien de sa contestation des impositions mises à sa charge au titre des années 2009 et suivantes, la cour administrative d'appel s'est fondée, d'une part, sur ce que, par cette décision, le vérificateur avait réitéré, pour ce qui concerne la taxe professionnelle due au titre des années 2007 et 2008 à raison des établissements en litige situés à Saint-Cyr-sur-Loire, objet d'un contrat de gérance-mandat auquel cette société était partie, la position retenue par l'administration dans des courriers des 20 décembre 2005 et 30 mai 2007 selon laquelle la valeur locative des magasins devait être prise en compte dans les bases de taxe professionnelle de la société Babou et non dans celles des mandataires gérants et, d'autre part, sur l'absence de changements de circonstance de fait ou de droit entre cette prise de position et la date à laquelle les impositions primitives pour les années en litige avaient été établies.  <br/>
<br/>
              5. En jugeant ainsi la cour n'a, contrairement à ce que soutient le ministre, pas entaché son arrêt d'erreur de droit. <br/>
<br/>
              6. Il en résulte que le ministre n'est pas fondé à demande l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre de l'action et des comptes publics est rejeté.<br/>
Article  2 : La présente décision sera notifiée au ministre de l'action et des comptes publics et à la société à responsabilité limitée Moreau Desmazeau.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
