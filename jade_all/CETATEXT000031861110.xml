<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031861110</ID>
<ANCIEN_ID>JG_L_2015_12_000000370418</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/86/11/CETATEXT000031861110.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème SSR, 30/12/2015, 370418, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370418</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELVOLVE</AVOCATS>
<RAPPORTEUR>Mme Marie-Gabrielle Merloz</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:370418.20151230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La SARL Kertrimmo a demandé au tribunal administratif de Rennes de prononcer la décharge des cotisations de taxe locale d'équipement et de taxe départementale des espaces naturels sensibles mentionnées par l'avis d'imposition émis le 16 septembre 2010 par le directeur départemental des territoires et de la mer du Finistère. Par un jugement n° 1101867 du 21 mai 2013, le tribunal administratif de Rennes a fait droit à cette demande. <br/>
<br/>
              Par un pourvoi, enregistré le 22 juillet 2013 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'égalité des territoires et du logement demande au Conseil d'Etat d'annuler ce jugement. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code général des impôts ; <br/>
              - loi n°2009-323 du 25 mars 2009 ;<br/>
              - le décret n° 2005-850 du 27 juillet 2005 ; <br/>
              - le décret n° 2012-770 du 24 mai 2012 ;<br/>
              - l'arrêté du 7 juillet 2011 portant nomination ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Gabrielle Merloz, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delvolvé, avocat de la société Kertrimmo ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 332-11-3 du code de l'urbanisme, issu de l'article 43 de la loi du 25 mars 2009 de mobilisation pour le logement et la lutte contre l'exclusion : " Dans les zones urbaines et les zones à urbaniser délimitées par les plans locaux d'urbanisme ou les documents d'urbanisme en tenant lieu, lorsqu'une ou plusieurs opérations d'aménagement ou de construction nécessitent la réalisation d'équipements autres que les équipements propres mentionnés à l'article L. 332-15, le ou les propriétaires des terrains, le ou les aménageurs et le ou les constructeurs peuvent conclure avec la commune ou l'établissement public compétent en matière de plan local d'urbanisme... une convention de projet urbain partenarial prévoyant la prise en charge financière de tout ou partie de ces équipements./ (...) " ; qu'aux termes de l'article L. 332-11-4 du même code, dans sa rédaction applicable à l'année des impositions en litige : " Dans les communes où la taxe locale d'équipement a été instituée, les constructions édifiées dans le périmètre délimité par une convention prévue à l'article L. 332-11-3 sont exclues du champ d'application de cette taxe pendant un délai fixé par la convention, qui ne peut excéder dix ans  " ; qu'aux termes de l'article L. 332-6 du même code dans sa rédaction applicable à l'année des impositions en litige : " Les bénéficiaires d'autorisations de construire ne peuvent être tenus que des obligations suivantes : /1° Le versement de la taxe locale d'équipement prévue à l'article 1585 A du code général des impôts ou de la participation instituée dans les secteurs d'aménagement définis à l'article L. 332-9 ou dans les périmètres fixés par les conventions visées à l'article L. 332-11-3 / 2° Le versement des contributions aux dépenses d'équipements publics mentionnées à l'article L. 332-6-1. Toutefois ces contributions telles qu'elles sont définies aux 2° et 3° dudit article ne peuvent porter sur les équipements publics donnant lieu à la participation instituée dans les secteurs d'aménagement définis à l'article L. 332-9 ou dans les périmètres fixés par les conventions visées à l'article L. 332-11-3 ; / 3° La réalisation des équipements propres mentionnées à l'article L. 332-15 ; / 4° Le versement de la redevance d'archéologie préventive prévue aux articles L. 524-2 à L. 524-13 du code du patrimoine. " ; qu'aux termes de l'article L. 332-6-1 de ce même code : " Les contributions aux dépenses d'équipements publics prévus au 2° de l'article L. 332-6 sont les suivantes :/ 1° (...) c) La taxe départementale des espaces naturels sensibles prévue à l'article L. 142-2 ;/ (...) " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 19 mars 2010, le maire de la commune de Névez (Finistère) a accordé à la SARL Kertrimmo le permis de construire une résidence de tourisme, après la conclusion, le 21 janvier 2010, d'une convention de projet urbain partenarial en application des dispositions précitées de l'article L. 332-11-3 du code de l'urbanisme ; que cette convention mettait à la charge de la société une participation financière de 100 000 euros, dont le montant était révisable en fonction du coût réel des travaux, pour la réalisation du réseau d'assainissement collectif nécessaire à la résidence ; que, par un avis d'imposition émis le 16 septembre 2010, le directeur départemental des territoires et de la mer du Finistère a réclamé à la SARL Kertrimmo la taxe locale d'équipement et la taxe départementale des espaces naturels sensibles correspondant à cette construction ; qu'il ressort de la réponse adressée le 23 février 2011 à un recours gracieux de la société que l'administration a considéré que, faute de fixation par la convention du 21 janvier 2010 d'un délai d'exonération de la taxe locale d'équipement, aucune exonération ne pouvait s'appliquer ;<br/>
<br/>
              Sur les fins de non-recevoir opposées par la SARL Kertrimmo :<br/>
<br/>
              3. Considérant, d'une part, qu'aux termes de l'article 1723 sexies du code général des impôts : " Les litiges relatifs à la taxe locale d'équipement sont de la compétence des tribunaux administratifs. Les réclamations sont présentées, instruites et jugées selon les règles de procédure applicables en matière de contributions directes. L'administration compétente pour statuer sur les réclamations et produire ses observations sur les recours contentieux autres que ceux relatifs au recouvrement, est celle de l'équipement " ; que ces dispositions sont rendues applicables à la taxe départementale sur les espaces naturels sensibles par L 142-2 du code de l'urbanisme ; qu'il en résulte, contrairement à ce que soutient en défense la SARL Kertrimmo, que le ministre de l'égalité des territoires et du logement avait qualité pour se pourvoir en cassation contre le jugement attaqué, s'agissant d'un litige d'assiette ;<br/>
<br/>
              4. Considérant, d'autre part, qu'il résulte des dispositions du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement que l'adjoint au directeur des affaires juridiques au secrétariat général du ministère de l'égalité des territoires et du logement avait, par l'effet de sa nomination par arrêté du 7 juillet 2011 publié le 9 juillet suivant, qualité pour signer le pourvoi au nom du ministre de l'égalité des territoires et du logement ;<br/>
<br/>
              Sur les motifs du jugement attaqué relatifs au bien-fondé des impositions en litige :<br/>
<br/>
              5. Considérant, en premier lieu, qu'il résulte des dispositions des articles L. 332-11-3 et L. 332-11-4 du code de l'urbanisme citées au point 1 que la durée de l'exonération de taxe locale d'équipement est l'une des composantes nécessaires d'une convention de projet urbain partenarial ; qu'il suit de là que le tribunal administratif de Rennes a pu juger, sans commettre d'erreur de droit, que la durée de la convention de dix-huit mois, stipulée à l'article 6 de la convention de projet urbain partenarial conclue le 21 janvier 2010 entre la SARL Kertimmo et la commune de Névez, ne pouvait qu'être regardée comme s'appliquant à l'exonération de la taxe locale d'équipement, dès lors que cette durée n'excédait pas dix ans ; <br/>
<br/>
              6. Considérant, en second lieu, qu'en vertu de l'article L. 142-2 du code de l'urbanisme dans sa rédaction applicable au litige, la taxe départementale des espaces naturels sensibles peut être instituée par le département afin de préserver la qualité des sites, des paysages, des milieux naturels et des champs naturels d'expansion des crues et d'assurer la sauvegarde des habitats naturels ; que ces dispositions déterminent notamment les actions susceptibles d'être financées par le produit cette taxe perçue au profit du département en tant que recette grevée d'affectation spéciale, les bâtiments et aménagements exclus de son champ, les exonérations que le département peut accorder, ainsi que le taux applicable ; qu'elles précisent en outre qu'elle est " soumise aux règles qui gouvernent l'assiette, la liquidation, le recouvrement, les sanctions et le contentieux de la taxe locale d'équipement " ;<br/>
<br/>
              7. Considérant qu'il résulte de ces dispositions que le champ d'application de la taxe départementale des espaces naturels sensibles, bien que son assiette soit définie par référence aux règles qui fixent celle de la taxe locale d'équipement, est délimité par des règles particulières ; que les exonérations applicables sont définies par ces règles particulières et ne résultent pas de la transposition des dispositions applicables à la taxe locale d'équipement ; que, par suite, l'exonération de taxe locale d'équipement prévue par l'article L. 332-11-4 du code de l'urbanisme n'entraîne pas celle de la taxe départementale des espaces naturels sensibles, faute que l'article L. 142-2 du même code le prévoie expressément ; qu'ainsi, en jugeant que l'exonération prévue à l'article L. 332-11-4 du code de l'urbanisme s'appliquait également à la taxe départementale des espaces naturels sensibles, le tribunal a commis une erreur de droit ; <br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que le ministre de l'égalité des territoires et du logement est seulement fondé à demander l'annulation du jugement qu'il attaque en tant qu'il a prononcé la décharge de la taxe départementale des espaces naturels sensibles mise à la charge de la SARL Kertimmo par l'avis d'imposition émis le 16 septembre 2010 ;<br/>
<br/>
              9. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              10. Considérant qu'il résulte de ce qui a été dit aux points 6 et 7 que la société requérante n'est pas fondée à demander la décharge de la taxe départementale des espaces naturels sensibles par voie de conséquence de l'exonération de la taxe locale d'équipement ;<br/>
<br/>
              Sur les conclusions présentées par la SARL Kertrimmo au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              11. Considérant qu'il y a lieu dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 2 000 euros à verser à la SARL Kertrimmo au titre de ces dispositions ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 21 mai 2013 du tribunal administratif de Rennes est annulé en tant qu'il a prononcé la décharge de la taxe départementale des espaces naturels sensibles mise à la charge de la SARL Kertrimmo par l'avis d'imposition émis le 16 septembre 2010.<br/>
Article 2 : Les conclusions de la SARL Kertrimmo tendant à la décharge de la taxe départementale des espaces naturels sensibles mise à sa charge par l'avis d'imposition émis le 16 septembre 2010 sont rejetées.<br/>
Article 3 : L'Etat versera à la SARL Kertrimmo la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la ministre du logement, de l'égalité des territoires et de la ruralité et à la SARL Kertrimmo.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
