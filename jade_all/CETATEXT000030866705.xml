<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030866705</ID>
<ANCIEN_ID>JG_L_2015_07_000000375886</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/86/67/CETATEXT000030866705.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 10/07/2015, 375886</TITRE>
<DATE_DEC>2015-07-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375886</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SCP POTIER DE LA VARDE, BUK LAMENT</AVOCATS>
<RAPPORTEUR>M. Laurent Cytermann</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:375886.20150710</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Strasbourg :<br/>
              - d'annuler les décisions de la caisse d'allocations familiales du Bas-Rhin des 28 septembre 2009 et 22 juin 2010 rejetant ses demandes d'admission au bénéfice du revenu de solidarité active ainsi que les décisions du président du conseil général du Bas-Rhin du 20 janvier 2010 et du 23 août 2010 rejetant ses recours gracieux ;<br/>
              - d'enjoindre à la caisse d'allocations familiales et au président du conseil général de lui attribuer le revenu de solidarité active.<br/>
<br/>
              Par un jugement nos 1002993, 1005396 du 18 décembre 2012, le tribunal administratif de Strasbourg a reconnu à M. A...le droit au revenu de solidarité active (RSA) à compter du 1er août 2009 et a enjoint au président du conseil général du Bas-Rhin de procéder au calcul et au versement de la somme due à compter de cette date.<br/>
<br/>
              Par un arrêt n° 13NC00261 du 28 octobre 2013, la cour administrative d'appel de Nancy a, à la demande du département du Bas-Rhin, annulé le jugement du tribunal administratif de Strasbourg et rejeté la demande présentée par M. A...à ce tribunal.<br/>
<br/>
            Procédure devant le Conseil d'Etat  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 27 février et 28 mai 2014 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt de la cour administrative d'appel de Nancy du 28 octobre 2013 ;<br/>
<br/>
              2°) de mettre à la charge du département du Bas-Rhin la somme de 3 000 euros à verser à la SCP Gaschignard, son avocat, au titre des dispositions de l'article L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, ensemble son protocole additionnel n° 1 ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Cytermann, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de M.A..., et à la SCP Potier de la Varde, Buk Lament, avocat du département du Bas-Rhin ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B...A..., ressortissant marocain, a fait l'objet le 23 novembre 2007 d'un arrêté préfectoral refusant le renouvellement de son titre de séjour assorti d'une obligation de quitter le territoire français et a été reconduit au Maroc en février 2008 ; que, par un arrêt du 11 mai 2009, la cour administrative d'appel de Nancy a annulé cette décision au motif que M. A...assurait effectivement l'entretien de son enfant de nationalité française ; que M. A...est alors rentré en France et a obtenu une nouvelle carte de séjour temporaire " vie privée et familiale " ; que le département du Bas-Rhin lui a refusé le bénéfice du revenu de solidarité active (RSA), au motif qu'il ne justifiait pas du respect de la condition de cinq ans de résidence régulière en France avec un titre de séjour autorisant à travailler, prévue par l'article L. 262-4 du code de l'action sociale et des familles ; que M. A...se pourvoit en cassation contre l'arrêt du 28 octobre 2013 de la cour administrative d'appel de Nancy, qui a rejeté ses conclusions dirigées contre cette décision ;<br/>
<br/>
              2. Considérant que l'article L. 262-4 du code de l'action sociale et des familles dispose : " Le bénéfice du revenu de solidarité active est subordonné au respect, par le bénéficiaire, des conditions suivantes : (...) 2° Etre français ou titulaire, depuis au moins cinq ans, d'un titre de séjour autorisant à travailler. (...) " ; que le législateur a ainsi subordonné le bénéfice du RSA pour les étrangers à une condition de détention d'un titre de séjour autorisant à travailler pendant une période d'au moins cinq ans ; que cette période doit en principe être continue ; que, toutefois, si elle est interrompue du fait d'une décision de refus de titre de séjour qui a été annulée par le juge administratif, le respect de la condition posée par le législateur s'apprécie en prenant en compte la durée de détention d'un titre de séjour antérieure à la décision illégale de refus de titre et la durée de détention à compter de l'obtention d'un nouveau titre ;<br/>
<br/>
              3. Considérant qu'en jugeant que la circonstance que l'interruption de son séjour en France était imputable au refus illégal de lui délivrer un titre de séjour était, par elle-même, sans incidence sur les droits de M. A...à bénéficier du RSA, sans rechercher si la condition de détention d'un titre autorisant à travailler pendant une durée de cinq ans pouvait être regardée comme remplie en prenant en compte, selon les modalités définies au point 2 ci-dessus, la durée cumulée de détention, à la date où elle statuait, d'un titre autorisant M. A...à travailler, la cour a commis une erreur de droit ; que son arrêt doit donc être annulé ; que ce moyen suffisant à entraîner l'annulation totale de l'arrêt, il n'est pas nécessaire d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              4. Considérant que M. A...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du département du Bas-Rhin la somme de 3 000 euros à verser à la SCP Gaschignard, avocat de M.A..., sous réserve que  cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de M.A..., qui n'est pas, dans la présente instance, la partie perdante, le versement de la somme demandée par le département du Bas-Rhin ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 28 octobre 2013 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy.<br/>
Article 3 : Le département du Bas-Rhin versera à la SCP Gaschignard, avocat de M.A..., la somme de 3 000 euros en application au titre du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à M. B...A...et au département du Bas-Rhin.<br/>
Copie en sera adressée pour information à la ministre des affaires sociales, de la santé et des droits des femmes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-06 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. REVENU MINIMUM D'INSERTION (RMI). - RSA - ETRANGER - CONDITION DE DÉTENTION, PENDANT UNE PÉRIODE D'AU MOINS CINQ ANS, EN PRINCIPE CONTINUE, D'UN TITRE DE SÉJOUR AUTORISANT À TRAVAILLER - INTERRUPTION DE CETTE PÉRIODE PAR UN REFUS DE TITRE DE SÉJOUR ANNULÉ PAR LE JUGE ADMINISTRATIF - APPRÉCIATION DE LA CONDITION.
</SCT>
<ANA ID="9A"> 04-02-06 Le législateur a subordonné, par l'article  L. 262-4 du code de l'action sociale et des familles, le bénéfice du revenu de solidarité active (RSA) pour les étrangers à une condition de détention d'un titre de séjour autorisant à travailler pendant une période d'au moins cinq ans. Cette période doit en principe être continue. Toutefois, si elle est interrompue du fait d'une décision de refus de titre de séjour qui a été annulée par le juge administratif, le respect de la condition posée par le législateur s'apprécie en prenant en compte la durée de détention d'un titre de séjour antérieure à la décision illégale de refus de titre et la durée de détention à compter de l'obtention d'un nouveau titre.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
