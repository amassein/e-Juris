<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042316256</ID>
<ANCIEN_ID>JG_L_2020_02_000000439007</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/31/62/CETATEXT000042316256.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 23/02/2020, 439007, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-02-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439007</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:439007.20200223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Le Syndicat autonome de la fonction publique territoriale de La Réunion (SAFPTR) a demandé au juge des référés du tribunal administratif de La Réunion, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre à la communauté intercommunale Réunion Est (CIREST) de le rétablir dans ses droits syndicaux par l'octroi d'autorisations spéciales d'absence pour la réunion de la section syndicale du 24 février 2020, pour les agents qui sont membres de la section syndicale de la CIREST. Par une ordonnance<br/>
n° 2000159 du 21 février 2020, le juge des référés du tribunal administratif de La Réunion a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 23 février 2020 au secrétariat du contentieux du Conseil d'Etat, le SAFPTR demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler l'ordonnance contestée ;<br/>
<br/>
              2°) d'enjoindre, sur le fondement des dispositions de l'article L.521-2 du code de justice administrative, à la communauté intercommunale Réunion Est (CIREST) de le rétablir dans ses droits syndicaux par l'octroi d'autorisations spéciales d'absence pour la réunion de la section syndicale du 24 février 2020 et sous astreinte de 500 euros par jour de retard ; <br/>
<br/>
              3°) de mettre à la charge de la CIREST la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - l'ordonnance contestée méconnaît les dispositions de l'article 59 de la loi <br/>
n°84-53 du 26 janvier 1984 et celles du décret n°85-397 du 3 avril 1985 ; que les textes n'imposent pas à un syndicat d'organiser ses structures internes d'une façon particulière pour bénéficier des droits syndicaux garantis par le décret du 3 avril 1985 ; que la jurisprudence du Conseil d'Etat a conforté cette liberté d'organisation interne ; que l'ordonnance et la décision contestées sont contraires aux règles de la représentativité syndicale ; <br/>
              - la condition d'urgence est remplie dès lors que l'ordonnance et la décision contestées font obstacle à ce que les représentants du syndicat au sein de la CIREST participent à la réunion prévue le 24 février 2020 ; <br/>
              - en refusant les demandes du syndicat, la CIREST a porté une atteinte à la liberté syndicale.<br/>
<br/>
              La requête a été communiquée à la CIREST qui n'a pas produit de mémoire.<br/>
<br/>
<br/>
              Vu l'ordonnance attaquée ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 83-634 du 13 juillet 1983 ; <br/>
              - la loi n° 84-53 du 26 janvier 1984 ; <br/>
              - le décret n° 85-397 du 3 avril 1985 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, le SAFPTR et, d'autre part, la CIREST; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 23 février 2020 à 16 heures au cours de laquelle a été entendu : <br/>
<br/>
              - Me Gury, avocat au Conseil d'Etat et à la Cour de cassation, avocat du SAFPTR ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ".<br/>
<br/>
              2. Le Syndicat autonome de la fonction publique territoriale de La Réunion (SAFPTR) conteste l'ordonnance rendue par le juge des référés du tribunal administratif de<br/>
La Réunion le 21 février 2020 sur le fondement des dispositions de l'article L. 521-2 précité. L'ordonnance contestée rejette sa demande d'enjoindre au président de la communauté intercommunale Réunion Est (CIREST) de délivrer des autorisations d'absence aux membres de la section locale du SAFPTR pour participer à la réunion prévue le 24 février 2020. <br/>
<br/>
              3. Il résulte des indications données par le syndicat requérant lors de l'audience au Conseil d'Etat que la réunion pour laquelle les autorisations d'absence ont été sollicitées reste programmée pour le 24 février. Il y donc toujours lieu à statuer et la condition d'urgence est remplie.<br/>
<br/>
              4. Aux termes des dispositions de l'article 8 de la loi du 13 juillet 1983 susvisée " Le droit syndical est garanti aux fonctionnaires. Les intéressés peuvent librement créer des organisations syndicales, y adhérer et y exercer des mandats ". Aux termes des dispositions de l'article 59 de la loi du 26 janvier 1984 susvisée, " Des autorisations spéciales d'absence qui n'entrent pas en compte dans le calcul des congés annuels sont accordées : 1° Aux représentants dûment mandatés des syndicats pour assister aux congrès professionnels syndicaux fédéraux, confédéraux et internationaux et aux réunions des organismes directeurs des unions, fédérations ou confédérations dont ils sont membres élus. Les organisations syndicales qui sont affiliées à ces unions, fédérations ou confédérations disposent des mêmes droits pour leurs représentants ; (...) Un décret en Conseil d'Etat détermine les conditions d'application du présent article et notamment, pour les autorisations spéciales d'absence prévues au 1°, le niveau auquel doit se situer l'organisme directeur dans la structure du syndicat considéré et le nombre de jours d'absence maximal autorisé chaque année (...) ". Aux termes des dispositions de l'article 1er du décret du 3 avril 1985 susvisé " Les organisations syndicales des agents de la fonction publique territoriale déterminent librement leurs structures dans le respect des dispositions législatives et réglementaires en vigueur ". Aux termes de l'article 12 du même décret : " A la suite de chaque renouvellement général des comités techniques, la collectivité territoriale, l'établissement public ou le centre de gestion attribue un crédit de temps syndical aux organisations syndicales, compte tenu de leur représentativité (...) Le crédit de temps syndical comprend deux contingents : 1° Un contingent d'autorisation d'absence (...) ". Aux termes de l'article 14 du même décret : " Le contingent d'autorisations d'absence mentionné au 1° de l'article 12 est calculé au niveau de chaque comité technique, à l'exclusion des comités techniques facultatifs, proportionnellement au nombre d'électeurs inscrits sur la liste électorale du comité technique, à raison d'une heure d'autorisation d'absence pour 1 000 heures de travail accomplies par ceux-ci (...) Les agents bénéficiaires sont désignés par les organisations syndicales parmi leurs représentants en activité dans la collectivité ou l'établissement concerné (...) ". Enfin, aux termes de l'article 15 du même décret, dans sa rédaction issue du décret n°2014-1624 du 24 décembre 2014 : " Les autorisations d'absence mentionnées aux articles 16 et 17 sont accordées, sous réserve des nécessités du service, aux représentants des organisations syndicales mandatés pour assister aux congrès syndicaux ainsi qu'aux réunions de leurs organismes directeurs, dont ils sont membres élus ou pour lesquels ils sont nommément désignés conformément aux dispositions des statuts de leur organisation. / Les demandes d'autorisation doivent être formulées trois jours au moins avant la date de la réunion. Les refus d'autorisation d'absence font l'objet d'une motivation de l'autorité territoriale ". <br/>
<br/>
              5. Ces dispositions ne font pas obstacle, en premier lieu, à ce que les organisations syndicales des agents de la fonction publique territoriale constituent au sein de chaque collectivité ou établissement public au sein desquels elles sont représentées, des sections locales dotées d'organismes directeurs. Elles ne font pas obstacle, en second lieu, à ce que les mêmes organisations syndicales désignent comme bénéficiaires des autorisations d'absence des membres de ces sections pour participer aux réunions des organismes directeurs déterminés par leurs statuts, dans la limite du contingent d'autorisations d'absence mentionné au 1° de l'article 12 du décret du 3 avril 1985. <br/>
<br/>
              6. Il résulte de l'instruction menée par le juge des référés du tribunal administratif de la Réunion, d'une part, que le SAFPTR, qui a créé et déclaré une section syndicale au sein de la CIREST, a présenté le 3 février 2020 au président de cet établissement public intercommunal des demandes d'autorisation d'absence concernant plusieurs de ses représentants au sein de cet établissement pour participer à une réunion de son comité directeur le 24 février 2020, d'autre part, que les décisions de refus opposées par le président de la CIREST à plusieurs de ces demandes étaient fondées sur la circonstance que les agents concernés n'étaient pas membres des instances départementales du SAFPTR. <br/>
<br/>
              7. Il résulte de ce qui a été dit au point 5 que ces décisions de refus méconnaissent les règles rappelées au point 4 et portent ainsi une atteinte grave et manifestement illégale à la liberté syndicale, qui a le caractère d'une liberté fondamentale au sens de l'article L. 521-2 du code de justice administrative. <br/>
<br/>
              8. Dès lors, le SAFPTR est fondé à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de La Réunion a rejeté sa requête. Eu égard à l'urgence, il y a lieu d'enjoindre au président de la CIREST de prendre sans délai toute disposition pour permettre la participation des membres de la section locale du SAFPTR à la réunion organisée le 24 février 2020, sous réserve que n'y fassent obstacle ni les nécessités du service ni les règles relatives au contingent d'autorisations d'absence.<br/>
<br/>
              9. Il n'y pas lieu, dans les circonstances de l'espèce, d'assortir cette injonction d'une astreinte, ni de condamner la CIREST à payer au syndicat requérant la somme qu'il demande au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'ordonnance n° 2000159 du 21 février 2020 du juge des référés du tribunal administratif de La Réunion est annulée.<br/>
Article 2 : Il est enjoint au président de la communauté intercommunale Réunion Est (CIREST) de prendre dès la notification de la présente ordonnance toutes dispositions pour permettre la participation des membres de la section locale du SAFPTR à la réunion du comité directeur organisée le 24 février 2020, sous réserve que n'y fassent obstacle ni les nécessités du service ni les règles relatives au contingent d'autorisations d'absence.<br/>
Article 3 : Le surplus des conclusions du Syndicat autonome de la fonction publique territoriale de La Réunion est rejeté.<br/>
Article 4 : La présente ordonnance sera notifiée au Syndicat autonome de la fonction publique territoriale de La Réunion et à la CIREST.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
