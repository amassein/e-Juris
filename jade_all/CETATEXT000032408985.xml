<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032408985</ID>
<ANCIEN_ID>JG_L_2016_04_000000384223</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/40/89/CETATEXT000032408985.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 15/04/2016, 384223, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-04-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384223</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Manon Perrière</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:384223.20160415</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              L'association " comité de liaison du camping-car " a demandé au tribunal administratif de Caen d'annuler la décision implicite de rejet née du silence gardé par le maire de la commune de Carolles à la suite du courrier du 9 janvier 2012 par lequel elle lui a demandé d'abroger l'arrêté du 11 mai 2009 réglementant le stationnement des véhicules habitables sur le territoire de la commune et d'enlever des panneaux de signalisation. Par un jugement n°1200965 du 24 janvier 2013, le tribunal administratif de Caen a rejeté sa requête.<br/>
<br/>
              Par un arrêt n°13NT00862 du 4 juillet 2014, la cour administrative d'appel de Nantes a rejeté l'appel formé par l'association " comité de liaison du camping-car " contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 4 septembre et 4 décembre 2014 et le 12 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, l'association " comité de liaison du camping-car " demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Carolles le versement d'une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code général des collectivités territoriales ;<br/>
<br/>
              - la directive la directive n°92/23/CEE du 31 mars 1992 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Manon Perrière, auditeur,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, Texidor, avocat de l'association " comité de liaison du camping-car " et à la SCP Lyon-Caen, Thiriez, avocat de la commune de Carolles ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant, d'une part, qu'il ressort des pièces du dossier soumis aux juges du fond que par un courrier en date du 9 janvier 2012, l'association " comité de liaison du camping car " a demandé au maire de Carolles l'abrogation de l'arrêté du 11 mai 2009 règlementant le stationnement des véhicules habitables sur le territoire de cette commune et l'enlèvement de certains panneaux de signalisation à la fois parce que ces panneaux matérialisaient l'arrêté contesté mais aussi au motif que lesdits panneaux seraient contraires selon elle tant à l'arrêté ministériel sur la signalisation routière du 31 juillet 2002, qu'à la directive n°92/23/CEE ; qu'elle a demandé l'annulation de la décision implicite de refus née du silence conservé par le maire pendant deux mois tant en ce qui concerne le refus d'abroger l'arrêté du 11 mai 2009 qu'en ce qui concerne le refus d'enlever les panneaux litigieux dans sa requête introductive d'instance devant le tribunal administratif de Caen et a réitéré cette demande dans ses écritures devant la cour administrative d'appel ; qu'en se bornant à juger que l'éventuelle non conformité des panneaux de signalisation à la réglementation applicable, à la supposer établie, serait sans incidence sur la légalité de l'arrêté du 11 mai 2009, la cour a omis de statuer sur les conclusions dirigées contre le refus opposé par le maire de Carolles à la demande de la requérante de retirer les panneaux de signalisation en raison de la non conformité de ces panneaux avec la réglementation en vigueur ; <br/>
<br/>
              2. Considérant, d'autre part, que par son article 1er, l'arrêté litigieux interdit le stationnement de véhicules habitables sur deux parkings de la commune situés en bord de mer ; que par son article 2, il limite ce stationnement à deux heures par jour sur tous les autres parkings de la commune et, par son article 3, il n'autorise le stationnement nocturne des mêmes véhicules que sur les deux emplacements spécialement aménagés à cet effet ; que bien que ces interdictions portent sur des lieux et des modalités de stationnement différents, la cour a rejeté les conclusions dirigées contre elles de façon globale en estimant qu'elles étaient justifiées par les nuisances visuelles qu'un stationnement incontrôlé de véhicules habitables était susceptible d'entraîner sur les paysages naturels de la commune ; qu'il ressort des pièces soumises aux juges du fond que la commune dispose d'une situation environnementale particulièrement favorable en raison d'une façade maritime donnant sur la baie du Mont Saint Michel ; que si, dans ces conditions, le motif retenu pouvait légalement fonder le rejet des conclusions dirigées contre l'interdiction de stationner sur les parkings situés en bord de mer, la cour a commis une erreur de droit en ne recherchant pas de façon plus précise dans quelle mesure ce motif pouvait également justifier les limitations imposées au stationnement de ces véhicules par les articles 2 et 3 de l'arrêté attaqué ;<br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède que l'association " comité de liaison du camping car " est fondée à demander l'annulation de l'arrêt attaqué en tant, d'une part, qu'il a omis de statuer sur les conclusions dirigées contre le refus opposé par le maire de Carolles à la demande de la requérante de retirer les panneaux de signalisation en raison de la non conformité de ces panneaux avec la réglementation en vigueur et, d'autre part, qu'il se prononce sur les articles 2 et 3 de l'arrêté du 11 mai 2009 du maire de Carolles ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Carolles la somme de 3 000 euros à verser à l'association, au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'association " comité de liaison du camping car " qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 4 juillet 2014 est annulé en tant, d'une part, qu'il a omis de statuer sur les conclusions dirigées contre le refus opposé par le maire de Carolles à la demande de la requérante de retirer les panneaux de signalisation en raison de la non conformité de ces panneaux avec la réglementation en vigueur et, d'autre part, qu'il statue sur les conclusions dirigées contre les articles 2 et 3 de l'arrêté du 11 mai 2009 du maire de  Carolles.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes dans la mesure de la cassation prononcée.<br/>
<br/>
Articles 3 : La commune de Carolles versera à l'association " comité de liaison du camping car " une somme de 3000 euros au titre des dispositions de l'article L.761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions de la commune de Carolles au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à l'association " comité de liaison du camping-car " et à la commune de Carolles.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
