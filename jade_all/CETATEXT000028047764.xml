<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028047764</ID>
<ANCIEN_ID>JG_L_2013_10_000000353968</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/04/77/CETATEXT000028047764.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 07/10/2013, 353968, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-10-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353968</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP HEMERY, THOMAS-RAQUIN</AVOCATS>
<RAPPORTEUR>M. Rémi Decout-Paolini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:353968.20131007</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>VU LA PROCEDURE SUIVANTE :<br/>
<br/>
<br/>
 Procédure contentieuse antérieure<br/>
<br/>
              M. et Mme A...C...ont demandé au tribunal administratif de Marseille de condamner la commune de Velaux (Bouches-du-Rhône) à leur verser la somme de 7 423,50 euros assortie des intérêts légaux en réparation du préjudice subi du fait des négligences commises dans l'instruction de la demande de permis de construire déposée par M. B...et dans l'appréciation de la conformité des travaux réalisés par ce dernier. Par un jugement n° 0808562 du 6 juillet 2011, le tribunal administratif de Marseille a condamné la commune de Velaux à leur verser la somme de 5 423,50 euros assortie des intérêts au taux légal à compter du 24 avril 2006. <br/>
<br/>
 Procédure devant le Conseil d'Etat<br/>
<br/>
              Par une ordonnance n° 11MA03759 du 2 novembre 2011, enregistrée le 10 novembre 2011 au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Marseille a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 5 octobre 2011 au greffe de la cour, présenté par la commune de Velaux.<br/>
<br/>
              Par ce pourvoi et par un mémoire complémentaire, enregistré le 23 mars 2012 au secrétariat du contentieux du Conseil d'Etat, la commune de Velaux, représentée par la SCP Hémery, Thomas-Raquin, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement n° 0808562 du tribunal administratif de Marseille du 6 juillet 2011 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M. et MmeC... ;<br/>
<br/>
              3°) de mettre à la charge de M. et Mme C...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Le pourvoi a été communiqué à M. et Mme C..., qui n'ont pas produit de mémoire.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Ont été entendus en séance publique :<br/>
<br/>
              - le rapport de M. Rémi Decout-Paolini, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public.<br/>
<br/>
              La parole a été donnée, avant et après les conclusions, à la SCP Hémery, Thomas-Raquin, avocat de la commune de Velaux.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>CONSIDERANT CE QUI SUIT :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 22 décembre 2003, le maire de Velaux a délivré à M. et Mme C...un permis de construire une maison d'habitation dont l'alimentation en eau potable devait être assurée par un forage situé sur leur propriété. Par un arrêté du 22 octobre 2003, le maire de Velaux avait précédemment délivré à M. et Mme B... un permis de construire une maison d'habitation équipée d'un système d'assainissement non collectif. M. et Mme C...ayant dû procéder en 2005 au raccordement de leur habitation au réseau public d'eau potable, en raison de cette installation, située à proximité de leur forage, ils ont demandé au tribunal administratif de Marseille de condamner la commune de Velaux à les indemniser du préjudice subi. Par un jugement du 6 juillet 2011, contre lequel la commune de Velaux se pourvoit en cassation, le tribunal a condamné la commune à leur verser la somme de 5 423,50 euros assortie des intérêts au taux légal.<br/>
<br/>
              2. Pour condamner la commune de Velaux à indemniser M. et Mme C... du préjudice résultant de ce raccordement contraint au réseau public, le tribunal administratif de Marseille s'est fondé sur l'illégalité fautive résultant de la délivrance par le maire de Velaux, le 2 février 2006, d'un certificat de conformité de travaux à M. et MmeB.... En jugeant ainsi que le préjudice résultant pour M. et Mme C...de leur raccordement au réseau public d'eau potable était la conséquence directe de l'illégalité du certificat de conformité de travaux délivré postérieurement à ce raccordement, le tribunal a inexactement apprécié les faits de l'espèce. Par suite, son jugement doit être annulé, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, tirés de l'erreur de qualification juridique quant à l'existence d'une illégalité fautive et de l'erreur de droit quant à l'existence d'un lien de causalité direct. <br/>
<br/>
              3. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la commune de Velaux au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Marseille du 6 juillet 2011 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Marseille.<br/>
Article 3 : Les conclusions présentées par la commune de Velaux au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la commune de Velaux et à M. et Mme A...C....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
