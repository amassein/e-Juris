<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033157859</ID>
<ANCIEN_ID>JG_L_2016_09_000000396887</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/15/78/CETATEXT000033157859.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 21/09/2016, 396887, Publié au recueil Lebon</TITRE>
<DATE_DEC>2016-09-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396887</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sophie Baron</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2016:396887.20160921</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement n° 1506337 du 9 février 2016, enregistré le 10 février 2016 au secrétariat du contentieux du Conseil d'Etat, le tribunal administratif de Nantes, avant de statuer sur la demande de M. B...C...tendant à l'annulation de l'article 2 de la décision du 15 juillet 2015 par laquelle l'inspectrice du travail de la 22ème section de l'unité territoriale de Loire-Atlantique a accordé à la société Figui international l'autorisation de le licencier, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen la question de savoir si l'inspecteur du travail, saisi d'une demande d'autorisation de licenciement pour inaptitude physique d'un salarié protégé, doit refuser le licenciement comme étant en rapport avec les fonctions représentatives, lorsque l'inaptitude du salarié résulte d'une dégradation de son état de santé en lien direct avec les difficultés mises par son employeur à l'exercice de ces fonctions.<br/>
<br/>
              Des observations, enregistrées le 9 mai 2016, ont été présentées par Maître A...agissant en qualité de liquidateur judiciaire de la société Figui international.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
               Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Baron, maître des requêtes en service extraordinaire, <br/>
- les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
    REND L'AVIS SUIVANT<br/>
<br/>
<br/>
<br/>
              1. En vertu du code du travail, les salariés protégés bénéficient, dans l'intérêt de l'ensemble des travailleurs qu'ils représentent, d'une protection exceptionnelle. Lorsque le licenciement de l'un de ces salariés est envisagé, il ne doit pas être en rapport avec les fonctions représentatives normalement exercées par l'intéressé ou avec son appartenance syndicale.<br/>
<br/>
              2. Dans le cas où la demande de licenciement est motivée par l'inaptitude du salarié, il appartient à l'inspecteur du travail et, le cas échéant, au ministre, de rechercher, sous le contrôle du juge, si cette inaptitude est telle qu'elle justifie le licenciement envisagé, compte tenu des caractéristiques de l'emploi exercé à la date à laquelle elle est constatée, de l'ensemble des règles applicables au contrat de travail de l'intéressé, des exigences propres à l'exécution normale du mandat dont il est investi et de la possibilité d'assurer son reclassement dans l'entreprise. En revanche, dans l'exercice de ce contrôle, il n'appartient pas à l'administration de rechercher la cause de cette inaptitude. <br/>
<br/>
              3. Toutefois, ainsi qu'il a été indiqué au point 1, il appartient en toutes circonstances à l'autorité administrative de faire obstacle à un licenciement en rapport avec les fonctions représentatives normalement exercées par un salarié ou avec son appartenance syndicale.<br/>
<br/>
              4. Par suite, même lorsque le salarié est atteint d'une inaptitude susceptible de justifier son licenciement, la circonstance que le licenciement envisagé est également en rapport avec les fonctions représentatives normalement exercées par l'intéressé ou avec son appartenance syndicale fait obstacle à ce que l'administration accorde l'autorisation sollicitée. Le fait que l'inaptitude du salarié résulte d'une dégradation de son état de santé, elle-même en lien direct avec des obstacles mis par l'employeur à l'exercice de ses fonctions représentatives est à cet égard de nature à  révéler l'existence d'un tel rapport.<br/>
<br/>
<br/>
<br/>
<br/>
              Le présent avis sera notifié au tribunal administratif de Nantes, à M. B...C..., à Maître A...et à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>
              Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-07-01-04-01 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. CONDITIONS DE FOND DE L'AUTORISATION OU DU REFUS D'AUTORISATION. ILLÉGALITÉ DU LICENCIEMENT EN RAPPORT AVEC LE MANDAT OU LES FONCTIONS REPRÉSENTATIVES. - LICENCIEMENT POUR INAPTITUDE - CONTRÔLE DE L'ADMINISTRATION - 1) VÉRIFICATION DES CAUSES DE L'INAPTITUDE - ABSENCE - 2) CONTRÔLE DU LIEN AVEC LE MANDAT - EXISTENCE - CAS OÙ IL APPARAÎT QUE L'INAPTITUDE EST EN LIEN DIRECT AVEC DES OBSTACLES MIS PAR L'EMPLOYEUR À L'EXERCICE DU MANDAT - REFUS D'AUTORISATION DU LICENCIEMENT. [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-07-01-04-035-02 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. CONDITIONS DE FOND DE L'AUTORISATION OU DU REFUS D'AUTORISATION. MOTIFS AUTRES QUE LA FAUTE OU LA SITUATION ÉCONOMIQUE. INAPTITUDE ; MALADIE. - CONTRÔLE DE L'ADMINISTRATION - 1) VÉRIFICATION DES CAUSES DE L'INAPTITUDE - ABSENCE - 2) CONTRÔLE DU LIEN AVEC LE MANDAT - EXISTENCE - CAS OÙ IL APPARAÎT QUE L'INAPTITUDE EST EN LIEN DIRECT AVEC DES OBSTACLES MIS PAR L'EMPLOYEUR À L'EXERCICE DU MANDAT - REFUS D'AUTORISATION DU LICENCIEMENT. [RJ1].
</SCT>
<ANA ID="9A"> 66-07-01-04-01 1) Dans le cas où la demande de licenciement est motivée par l'inaptitude du salarié, il appartient à l'administration de rechercher si cette inaptitude est telle qu'elle justifie le licenciement envisagé sans rechercher la cause de cette inaptitude.... ,,2) Toutefois, il appartient en toutes circonstances à l'autorité administrative de faire obstacle à un licenciement en rapport avec les fonctions représentatives normalement exercées par un salarié ou avec son appartenance syndicale. Par suite, même lorsque le salarié est atteint d'une inaptitude susceptible de justifier son licenciement, la circonstance que le licenciement envisagé est également en rapport avec les fonctions représentatives normalement exercées par l'intéressé ou avec son appartenance syndicale fait obstacle à ce que l'administration accorde l'autorisation sollicitée. Le fait que l'inaptitude du salarié résulte d'une dégradation de son état de santé, elle-même en lien direct avec des obstacles mis par l'employeur à l'exercice de ses fonctions représentatives est à cet égard de nature à  révéler l'existence d'un tel rapport.</ANA>
<ANA ID="9B"> 66-07-01-04-035-02 1) Dans le cas où la demande de licenciement est motivée par l'inaptitude du salarié, il appartient à l'administration de rechercher si cette inaptitude est telle qu'elle justifie le licenciement envisagé sans rechercher la cause de cette inaptitude.... ,,2) Toutefois, il appartient en toutes circonstances à l'autorité administrative de faire obstacle à un licenciement en rapport avec les fonctions représentatives normalement exercées par un salarié ou avec son appartenance syndicale. Par suite, même lorsque le salarié est atteint d'une inaptitude susceptible de justifier son licenciement, la circonstance que le licenciement envisagé est également en rapport avec les fonctions représentatives normalement exercées par l'intéressé ou avec son appartenance syndicale fait obstacle à ce que l'administration accorde l'autorisation sollicitée. Le fait que l'inaptitude du salarié résulte d'une dégradation de son état de santé, elle-même en lien direct avec des obstacles mis par l'employeur à l'exercice de ses fonctions représentatives est à cet égard de nature à  révéler l'existence d'un tel rapport.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 20 novembre 2013, Mme,, n° 340591, p. 298.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
