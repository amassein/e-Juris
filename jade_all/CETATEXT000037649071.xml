<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037649071</ID>
<ANCIEN_ID>JG_L_2018_11_000000424331</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/64/90/CETATEXT000037649071.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 26/11/2018, 424331, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-11-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424331</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Vincent Uher</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:424331.20181126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 19 septembre 2018 au secrétariat du contentieux du Conseil d'État, M. B...A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir les mots : " ou à titre gratuit (succession ou donation) " du deuxième alinéa du paragraphe n° 320 des commentaires administratifs publiés au Bulletin officiel des finances publiques (BOFiP) - Impôts le 4 mars 2016 sous la référence BOI-RPPM-PVBMI-20-10-10-20 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - le code général des impôts ;<br/>
              - la loi n° 2011-900 du 29 juillet 2011 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Uher, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de M.A....<br/>
<br/>
              Vu la note en délibéré, enregistrée le 15 novembre 2018, présentée par M. A.... <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du 2 du I de l'article 150-0 A du code général des impôts, dans sa rédaction applicable au litige : " Le complément de prix reçu par le cédant en exécution de la clause du contrat de cession de valeurs mobilières ou de droits sociaux par laquelle le cessionnaire s'engage à verser au cédant un complément de prix exclusivement déterminé en fonction d'une indexation en relation directe avec l'activité de la société dont les titres sont l'objet du contrat, est imposable au titre de l'année au cours de laquelle il est reçu. / Le gain retiré de la cession ou de l'apport d'une créance qui trouve son origine dans une clause contractuelle de complément de prix visée au premier alinéa est imposé dans les mêmes conditions au titre de l'année de la cession ou de l'apport. " Aux termes du premier alinéa de l'article 150-0 B bis de ce même code, dans sa rédaction issue de la loi du 29 juillet 2011 de finances rectificative pour 2011 : " Le gain retiré de l'apport, avant qu'elle ne soit exigible en numéraire, de la créance visée au deuxième alinéa du 2 du I de l'article 150-0 A est reporté, sur option expresse du contribuable, au moment où s'opère la transmission, le rachat, le remboursement ou l'annulation des titres reçus en contrepartie de cet apport ou, lors du transfert par le contribuable de son domicile fiscal hors de France en vertu de l'article 167 bis si cet événement est antérieur. ". Il résulte de ces dispositions que les transmissions qui ont pour effet de mettre fin au report qu'elles prévoient s'entendent tant des transmissions à titre onéreux que des transmissions à titre gratuit.<br/>
<br/>
              2. Aux termes du paragraphe n° 320 des commentaires administratifs publiés le 4 mars 2016 au Bulletin officiel des finances publiques (BOFiP) - Impôts sous la référence BOI-RPPM-PVBMI-20-10-10-20 : " L'article 150-0 B bis du CGI précise que l'imposition du gain retiré de l'apport de la créance représentative d'un complément de prix à recevoir en exécution d'une clause d'indexation est reportée jusqu'au moment où s'opère la transmission, le rachat, le remboursement ou l'annulation des titres reçus en échange. / Pour l'application de cette disposition, la transmission des titres reçus en échange s'entend de leur transmission à titre onéreux (vente, échange, apport, etc.) ou à titre gratuit (succession ou donation) ".<br/>
<br/>
              3. M. A...demande l'annulation pour excès de pouvoir des mots : " ou à titre gratuit (succession ou donation) " du paragraphe n° 320 précité, lequel réitère sans y rien ajouter les dispositions du premier alinéa de l'article 150-0 B bis précité. Au soutien de sa requête, il soulève un moyen unique, présenté dans un mémoire distinct, tiré de l'absence de conformité aux droits et libertés garantis par la Constitution de cet alinéa.<br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              4. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              5. M. A...demande le renvoi au Conseil constitutionnel de la question de la conformité aux droits et libertés garantis par la Constitution du premier alinéa de l'article 150-0 B bis précité, dans sa rédaction issue de la loi du 29 juillet 2011 de finances rectificative pour 2011. Il soutient que ces dispositions sont contraires aux principes d'égalité devant la loi et d'égalité devant les charges publiques en tant qu'elles prévoient que la transmission à titre gratuit de titres grevés du report d'imposition du gain retiré de l'apport d'une créance représentative d'un complément de prix à recevoir en exécution d'une clause d'indexation met fin à ce report, alors, d'une part, que le bénéfice des autres sursis ou reports d'imposition comparables, prévus notamment par les articles 150-0 B et 150-0 B ter, ne prend pas fin en cas de transmission à titre gratuit, sans que cette différence de traitement ne soit justifiée par une différence de situation en rapport avec l'objet de loi ou par un motif d'intérêt général suffisant et, d'autre part, que le contribuable n'a pas la disposition du gain retiré de l'apport de la créance lorsqu'il transmet à titre gratuit les titres grevés de ce gain ni n'en a eu la disposition antérieurement, faute d'avoir perçu les liquidités correspondantes.<br/>
<br/>
              6. D'une part, le principe d'égalité ne s'oppose ni à ce que le législateur règle de façon différente des situations différentes ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général, pourvu que, dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la loi qui l'établit. D'autre part, en vertu de l'article 34 de la Constitution, il appartient au législateur de déterminer, dans le respect des principes constitutionnels et compte tenu des caractéristiques de chaque impôt, les règles selon lesquelles doivent être appréciées les facultés contributives. En particulier, pour assurer le respect du principe d'égalité, il doit fonder son appréciation sur des critères objectifs et rationnels en fonction des buts qu'il se propose. Cette appréciation ne doit cependant pas entraîner de rupture caractérisée de l'égalité devant les charges publiques garantie par l'article 13 de la  Déclaration de 1789.<br/>
<br/>
              7. En premier lieu, l'article 150-0 B bis du code général des impôts prévoit, sur option du contribuable, le report de l'imposition du gain retiré de l'apport de la créance représentative d'un complément de prix entrant dans les prévisions du 2 du I de l'article 150-0 A du même code, tandis que les articles 150-0 B et 150-0 B ter de ce code, invoqués par le requérant sans qu'il conteste leur conformité à la Constitution, prévoient l'application de plein droit, dans les cas et conditions qu'ils définissent, respectivement d'un sursis et d'un report d'imposition aux plus-values réalisées dans le cadre d'un apport de titres à une société soumise à l'impôt sur les sociétés. Ainsi, l'article 150-0 B bis s'applique à l'apport d'une créance représentative d'une somme que le contribuable renonce à percevoir en numéraire au bénéfice d'un investissement immédiat dans une activité économique sans lien avec la société dont la cession des titres a été assortie d'une clause de complément de prix, sans créer par lui-même aucune différence de traitement entre les apports bénéficiant de ses dispositions, alors que les articles 150-0 B et 150-0 B ter s'appliquent à l'apport de titres effectué dans le cadre d'opérations de restructuration qui revêtent par nature un caractère intercalaire en ce qu'elles ont pour objet de poursuivre, sous une autre forme, l'investissement réalisé dans l'activité économique faisant l'objet de la restructuration. Par suite, les contribuables qui apportent une créance résultant d'une clause de complément de prix et ceux qui apportent des titres dans le cadre d'une opération de restructuration sont, au regard de l'imposition des gains retirés de ces opérations d'apport respectives, dans une situation différente. Il s'en déduit que la différence de traitement opérée, en cas de transmission à titre gratuit des titres grevés de la plus-value en report ou en sursis d'imposition, selon que le gain en cause procède de l'apport d'une créance représentative d'un complément de prix ou de l'apport de titres d'une société, trouve en tout état de cause sa justification dans une différence de situation en rapport direct avec l'objet de la loi. Il en résulte que les dispositions contestées ne méconnaissent pas le principe d'égalité devant la loi fiscale.<br/>
<br/>
              8. En second lieu, les dispositions de l'article 150-0 B bis ont seulement pour objet, en vue d'éviter que le paiement immédiat des impositions dues par les personnes physiques à raison des gains découlant de l'apport de créances représentatives de certains compléments de prix ne fasse obstacle à la réalisation de transmissions d'entreprises assorties de clauses d'indexation et à l'investissement du produit de la part variable du prix de cession de ces entreprises, de différer la liquidation et le paiement de ces impositions, sans en exonérer les redevables ni même en réduire le montant. Il en découle que la seule circonstance qu'un contribuable puisse être conduit à acquitter un impôt sur le gain tiré de l'apport de la créance représentative du complément de prix à la suite de la donation des titres grevés du gain en report, sans que ni l'opération d'apport ni cette donation ne lui aient procuré par elles-mêmes les liquidités nécessaires, ne suffit pas à faire regarder l'imposition correspondante comme établie sans tenir compte des capacités contributives du contribuable, en méconnaissance du principe d'égalité devant les charges publiques. Il en résulte que les dispositions contestées ne peuvent être regardées comme méconnaissant ce principe.<br/>
<br/>
              9. Par suite, la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que le premier alinéa de l'article 150-0 B bis du code général des impôts porte atteinte aux droits et libertés garantis par la Constitution doit être écarté.<br/>
<br/>
              Sur le recours pour excès de pouvoir :<br/>
<br/>
              10. Il résulte de ce qui précède que M.A..., qui, ainsi qu'il a été dit, ne soulève à l'appui de sa requête aucun moyen autre que le moyen tiré de ce que les commentaires administratifs attaqués réitèreraient une disposition législative portant atteinte aux droits et libertés garantis par la Constitution, n'est pas fondé à demander l'annulation de ces commentaires. Ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées.<br/>
<br/>
<br/>
<br/>                    D E C I D E :<br/>
                                  --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. A....<br/>
<br/>
Article 2 : La requête de M. A... est rejetée.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M.B... A... et au ministre de l'action et des comptes publics.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
