<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036673259</ID>
<ANCIEN_ID>JG_L_2018_03_000000414859</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/67/32/CETATEXT000036673259.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 05/03/2018, 414859</TITRE>
<DATE_DEC>2018-03-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414859</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MEIER-BOURDEAU, LECUYER</AVOCATS>
<RAPPORTEUR>M. Richard Senghor</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:414859.20180305</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Nouvelle-Calédonie de prendre toutes mesures utiles de nature à préserver ses messageries électroniques. Par une ordonnance n° 1700294 du 1er septembre 2017, le juge des référés du tribunal administratif de Nouvelle-Calédonie a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 5 et 19 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant comme juge des référés, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros à la SCP Meier-Bourdeau, Lécuyer, son avocat, au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - la loi n° 2009-1436 du 24 novembre 2009 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Richard Senghor, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Meier-Bourdeau, Lécuyer, avocat de M. A...B...;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 15 février 2018, présentée par M.B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces des dossiers soumis au juge des référés que M.B..., condamné à une peine d'un an d'emprisonnement, a été écroué au centre pénitentiaire de Nouméa à compter du 2 avril 2017. Le requérant est titulaire de comptes de messagerie électronique sur lesquels il a enregistré des documents contenant des données à caractère personnel, dont il déclare ne pas détenir de copies. La société de service à laquelle il a eu recours pour créer ces comptes prévoit, dans ses conditions générales d'utilisation, que le détenteur d'un compte doit s'y connecter au moins une fois tous les six mois pour éviter que son accès ne soit bloqué et que les données archivées ne soient effacées et au moins une fois par an pour en conserver l'usage. Le requérant souhaitant maintenir ses comptes actifs afin de sauvegarder les données à caractère personnel qu'il y a enregistrées et alléguant ne disposer d'aucun contact à l'extérieur a demandé, sur le fondement de l'article L. 521-3 du code de justice administrative, au juge des référés du tribunal administratif de Nouméa de lui accorder une autorisation exceptionnelle de procéder à une connexion unique sur ses comptes de messagerie afin de relancer une période sauvegarde de six mois ou de désigner un tiers de confiance auquel il communiquerait ses identifiants d'accès afin que celui-ci puisse y procéder à sa place. Il se pourvoit en cassation contre l'ordonnance du 1er septembre 2017 par laquelle le juge des référés a rejeté ses demandes au motif qu'elles n'étaient pas au nombre de celles qu'il avait le pouvoir de satisfaire en application de l'article L. 521-3 du code de justice administrative.<br/>
<br/>
              2. Aux termes de l'article L. 521-3 du code de justice administrative : " En cas d'urgence et sur simple requête qui sera recevable même en l'absence de décision administrative préalable, le juge des référés peut ordonner toutes autres mesures utiles sans faire obstacle à l'exécution d'aucune décision administrative ". Saisi, sur le fondement de ces dispositions, d'une demande qui n'est pas manifestement insusceptible de se rattacher à un litige relevant de la compétence du juge administratif, le juge des référés peut prescrire, à des fins conservatoires ou à titre provisoire, toutes mesures que l'urgence justifie, notamment sous forme d'injonctions adressées à l'administration, à la condition que ces mesures soient utiles et ne se heurtent à aucune contestation sérieuse. En raison du caractère subsidiaire du référé régi par l'article L. 521-3, le juge saisi sur ce fondement ne peut prescrire les mesures qui lui sont demandées lorsque leurs effets pourraient être obtenus par les procédures de référé régies par les articles L. 521-1 et L 521-2  du code de justice administrative. Enfin, il ne saurait faire obstacle à l'exécution d'une décision administrative, même celle refusant la mesure demandée, à moins qu'il ne s'agisse de prévenir un péril grave.<br/>
<br/>
              3. D'une part, les données archivées par M. B...sur ses comptes de messagerie électronique doivent être regardés comme des biens personnels. D'autre part, eu égard à sa qualité de détenu, le requérant ne pouvait être autorisé à utiliser un ordinateur connecté à un réseau informatique relié avec l'extérieur du centre de détention afin d'accéder à ses comptes de messagerie électronique pour prévenir la destruction des données y figurant et en conserver l'usage. Dans ces conditions, les demandes formées par l'intéressé, qui présentent un caractère conservatoire et ne font obstacle à l'exécution d'aucune décision administrative, notamment de refus, sont, sous réserve de l'existence d'une situation d'urgence, de leur utilité pour la sauvegarde du droit mis en cause et de l'absence de contestation sérieuse, au nombre de celles dont peut être saisi le juge des référés sur le fondement de l'article L. 521-3 du code de justice administrative.<br/>
<br/>
              4. Il s'ensuit qu'en rejetant la demande dont il était saisi au motif que les mesures sollicitées ne relevaient pas de son office sans se prononcer ni sur la condition d'urgence ni sur l'utilité des mesures en cause, le juge des référés a méconnu les pouvoirs qu'il tient de l'article L. 521-3 du code de justice administrative et entaché l'ordonnance attaquée d'erreur de droit. <br/>
<br/>
              5. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, M. B...est fondé à demander l'annulation de l'ordonnance qu'il attaque. <br/>
<br/>
              6. Dans les circonstances de l'espèce, il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée devant le tribunal administratif de Nouvelle-Calédonie.<br/>
<br/>
              7. Il résulte de l'instruction et il n'est pas contesté que M. B...a achevé l'exécution de sa peine au centre pénitentiaire de Nouméa le 2 janvier 2018. Par suite, sa demande est devenue sans objet et il n'y a, dès lors, plus lieu d'y statuer.<br/>
<br/>
              8. M. B...a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Meier-Bourdeau, Lécuyer, avocat de M.B..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à la SCP Meier-Bourdeau, Lécuyer.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Nouvelle-Calédonie du 1er septembre 2017 est annulée.<br/>
Article 2 : Il n'y a pas lieu de statuer sur la demande de M. B...présentée devant le juge des référés du tribunal administratif de Nouméa.<br/>
Article 3 : L'Etat versera à la SCP Meier-Bourdeau, Lécuyer, avocat de M.B..., une somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative et du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à M. A... B...et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">37-05-02-01 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. EXÉCUTION DES JUGEMENTS. EXÉCUTION DES PEINES. SERVICE PUBLIC PÉNITENTIAIRE. - DEMANDES PRÉSENTÉES PAR UN DÉTENU TENDANT À CE QU'IL SOIT AUTORISÉ À ACCÉDER À SES COMPTES DE MESSAGERIE ÉLECTRONIQUE AFIN DE SAUVEGARDER SES DONNÉES À CARACTÈRE PERSONNEL OU À CE QU'IL LUI SOIT PERMIS DE DÉSIGNER UN TIERS DE CONFIANCE AFIN QUE CELUI-CI PUISSE Y PROCÉDER À SA PLACE - DEMANDES AU NOMBRE DE CELLES DONT PEUT ÊTRE SAISI LE JUGE DES RÉFÉRÉS SUR LE FONDEMENT DE L'ARTICLE L. 521-3 DU CJA - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-035-04-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ TENDANT AU PRONONCÉ DE TOUTES MESURES UTILES (ART. L. 521-3 DU CODE DE JUSTICE ADMINISTRATIVE). RECEVABILITÉ. - EXISTENCE - DEMANDES PRÉSENTÉES PAR UN DÉTENU TENDANT À CE QU'IL SOIT AUTORISÉ À ACCÉDER À SES COMPTES DE MESSAGERIE ÉLECTRONIQUE AFIN DE SAUVEGARDER SES DONNÉES À CARACTÈRE PERSONNEL OU À CE QU'IL LUI SOIT PERMIS DE DÉSIGNER UN TIERS DE CONFIANCE AFIN QUE CELUI-CI PUISSE Y PROCÉDER À SA PLACE [RJ1].
</SCT>
<ANA ID="9A"> 37-05-02-01 Détenu ayant demandé au juge des référés, sur le fondement de l'article L. 521-3 du code de justice administrative (CJA), de lui accorder une autorisation exceptionnelle de procéder à une connexion unique sur ses comptes de messagerie électronique afin de sauvegarder les données à caractère personnel qu'il y avait enregistrées ou de désigner un tiers de confiance auquel il communiquerait ses identifiants d'accès afin que celui-ci puisse y procéder à sa place.... ,,D'une part, les données archivées par le requérant sur ses comptes de messagerie électronique doivent être regardées comme des biens personnels. D'autre part, eu égard à sa qualité de détenu, le requérant ne pouvait être autorisé à utiliser un ordinateur connecté à un réseau informatique relié avec l'extérieur du centre de détention afin d'accéder à ses comptes de messagerie électronique pour prévenir la destruction des données y figurant et en conserver l'usage. Dans ces conditions, les demandes formées par l'intéressé, qui présentent un caractère conservatoire et ne font obstacle à l'exécution d'aucune décision administrative, notamment de refus, sont, sous réserve de l'existence d'une situation d'urgence, de leur utilité pour la sauvegarde du droit mis en cause et de l'absence de contestation sérieuse, au nombre de celles dont peut être saisi le juge des référés sur le fondement de l'article L. 521-3 du CJA.</ANA>
<ANA ID="9B"> 54-035-04-02 Détenu ayant demandé au juge des référés, sur le fondement de l'article L. 521-3 du code de justice administrative (CJA), de lui accorder une autorisation exceptionnelle de procéder à une connexion unique sur ses comptes de messagerie électronique afin de sauvegarder les données à caractère personnel qu'il y avait enregistrées ou de désigner un tiers de confiance auquel il communiquerait ses identifiants d'accès afin que celui-ci puisse y procéder à sa place.... ,,D'une part, les données archivées par le requérant sur ses comptes de messagerie électronique doivent être regardées comme des biens personnels. D'autre part, eu égard à sa qualité de détenu, le requérant ne pouvait être autorisé à utiliser un ordinateur connecté à un réseau informatique relié avec l'extérieur du centre de détention afin d'accéder à ses comptes de messagerie électronique pour prévenir la destruction des données y figurant et en conserver l'usage. Dans ces conditions, les demandes formées par l'intéressé, qui présentent un caractère conservatoire et ne font obstacle à l'exécution d'aucune décision administrative, notamment de refus, sont, sous réserve de l'existence d'une situation d'urgence, de leur utilité pour la sauvegarde du droit mis en cause et de l'absence de contestation sérieuse, au nombre de celles dont peut être saisi le juge des référés sur le fondement de l'article L. 521-3 du CJA.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur l'office du juge du référé mesures utiles, Section, 5 février 2016, M.,, n°s 393540, 393541, p. 13.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
