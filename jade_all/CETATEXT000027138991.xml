<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027138991</ID>
<ANCIEN_ID>JG_L_2013_03_000000359428</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/13/89/CETATEXT000027138991.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 04/03/2013, 359428, Publié au recueil Lebon</TITRE>
<DATE_DEC>2013-03-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359428</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Damien Botteghi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:359428.20130304</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 15 mai 2012 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11BX02932 de la cour administrative d'appel de Bordeaux en date du 20 mars 2012 en ce qu'il a, sur la requête de M. A...B..., d'une part, annulé l'arrêté du 7 octobre 2011 du préfet de la Gironde décidant de placer l'intéressé en rétention administrative en tant que cet arrêté prévoit que " le recours juridictionnel contre la décision de placement en rétention administrative ne suspend pas l'exécution de la mesure d'éloignement ", d'autre part, réformé dans cette mesure le jugement n° 1104065 du 11 octobre 2011 du magistrat désigné par le président du tribunal administratif de Bordeaux, enfin, rejeté le surplus de la requête ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel formé par M. B...devant la cour administrative d'appel de Bordeaux ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu la directive 2008/115/CE du Parlement européen et du Conseil du 16 décembre 2008 ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, Conseiller d'Etat,<br/>
<br/>
              - les observations de Me Haas, avocat de M.B...,<br/>
<br/>
              - les conclusions de M. Damien Botteghi, Rapporteur public,<br/>
<br/>
              La parole ayant à nouveau été donnée à Me Haas, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que M. B..., de nationalité tunisienne, est entré irrégulièrement en France au début de l'année 2011 ; que le préfet de la Gironde a décidé sa reconduite à la frontière par un arrêté du 5 mars 2011 ; que le recours formé par M. B...contre cet arrêté a été rejeté par un jugement du 9 mars 2011, devenu définitif, rendu par un magistrat désigné par le président du tribunal administratif de Bordeaux ; qu'ayant été interpellé à nouveau le 7 octobre 2011, l'intéressé a été placé en rétention administrative par arrêté du même jour du préfet de la Gironde ; que, par jugement du 11 octobre 2011, le magistrat désigné par le président du tribunal administratif de Bordeaux a rejeté le recours formé par M. B...contre cet arrêté du 7 octobre 2011 le plaçant en rétention ;<br/>
<br/>
              2. Considérant que, par un arrêt du 20 mars 2012, la cour administrative d'appel de Bordeaux a annulé l'arrêté du 7 octobre 2011 du préfet de la Gironde décidant le placement en rétention administrative de M. B...en tant seulement qu'il indique que " le recours juridictionnel contre la décision de placement en rétention administrative ne suspend pas l'exécution de la mesure d'éloignement ", a réformé dans cette mesure le jugement du 11 octobre 2011 et a rejeté le surplus des conclusions de M. B...dirigées contre l'arrêté le plaçant en rétention ; que le ministre de l'intérieur se pourvoit en cassation contre cet arrêt en tant qu'il a partiellement annulé l'arrêté de placement en rétention ;<br/>
<br/>
              3. Considérant que pour annuler dans cette mesure l'arrêté contesté, la cour administrative d'appel s'est fondée sur les stipulations de l'article 5, paragraphe 4 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, selon lesquelles " Toute personne privée de sa liberté par arrestation ou détention a le droit d'introduire un recours devant un tribunal, afin qu'il statue à bref délai sur la légalité de sa détention et ordonne sa libération si la détention est illégale ", en estimant que ces stipulations impliquent qu'un étranger faisant l'objet d'un placement en rétention ne puisse effectivement être éloigné avant que le juge ait statué sur le recours qu'il a, le cas échéant, introduit contre le placement en rétention ;<br/>
<br/>
              4. Considérant qu'il ressort des dispositions du paragraphe III de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile que le législateur a organisé une procédure spéciale permettant au juge administratif de statuer rapidement sur la légalité des mesures relatives à l'éloignement des étrangers, hors la décision refusant le séjour, lorsque ces derniers sont placés en rétention ou assignés à résidence, ainsi que sur la légalité des décisions de placement en rétention ou d'assignation à résidence elles-mêmes ; que le président du tribunal administratif ou le magistrat désigné à cette fin statue alors au plus tard soixante-douze heures à compter de sa saisine ; qu'en vertu de l'article L. 512-3 du même code, lorsque le tribunal administratif est saisi d'une demande d'annulation d'une obligation de quitter le territoire français, cette mesure ne peut être exécutée d'office avant que le tribunal n'ait statué ; que les stipulations de l'article 5, paragraphe 4 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, qui garantissent le droit d'une personne privée de liberté de former un recours devant un tribunal qui statue rapidement sur la légalité de la détention, n'ont ni pour objet ni pour effet de conduire à reconnaître un caractère suspensif aux recours susceptibles d'être exercés contre les mesures de placement en rétention administrative prises pour assurer l'exécution des décisions, distinctes, qui ont ordonné l'éloignement des étrangers placés en rétention ; qu'il résulte de ce qui précède que la cour administrative d'appel de Bordeaux a commis une erreur de droit et que le ministre est fondé à demander l'annulation de l'arrêt qu'il attaque en tant que cet arrêt a annulé l'arrêté du 7 octobre 2011 du préfet de la Gironde en tant qu'il indique que " le recours juridictionnel contre la décision de placement en rétention administrative ne suspend pas l'exécution de la mesure d'éloignement " et a réformé dans cette mesure le jugement du 11 octobre 2011 ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler dans cette mesure l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant qu'il ressort des éléments versés au dossier que le recours à caractère suspensif formé par M. B...contre l'arrêté du 5 mars 2011 par lequel le préfet de la Gironde avait décidé sa reconduite à la frontière a été rejeté par jugement du magistrat désigné par le président du tribunal administratif de Bordeaux en date du 9 mars 2011, devenu définitif ; qu'après avoir été placé en rétention administrative par un nouvel arrêté du 7 octobre 2011, M. B... n'a saisi le juge administratif que de conclusions dirigées contre ce dernier arrêté ;<br/>
<br/>
              7. Considérant, ainsi qu'il a été dit, que les stipulations de l'article 5 paragraphe 4 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales n'ont ni pour objet ni pour effet de conduire à reconnaître un caractère suspensif au recours exercé par M. B...contre la décision le plaçant en rétention administrative ; qu'elles n'impliquent pas davantage la suspension de l'exécution de la décision distincte qui avait ordonné sa reconduite à la frontière, dont, au demeurant, il ne demande pas l'annulation et qui était devenue définitive après le rejet du recours qu'il avait formé contre elle devant le tribunal administratif de Bordeaux ;<br/>
<br/>
              8. Considérant que les termes du paragraphe 2 de l'article 15 de la directive du Parlement européen et du Conseil du 16 décembre 2008 relative aux normes et procédures communes applicables dans les Etats membres au retour des ressortissants de pays tiers en séjour irrégulier, selon lesquels les Etats membres prévoient, en cas de placement en rétention décidé par des autorités administratives, " qu'un contrôle juridictionnel accéléré de la légalité de la rétention doit avoir lieu le plus rapidement possible à compter du début de la rétention ", n'impliquent pas davantage que le recours formé contre la mesure de rétention doive présenter un caractère suspensif ;<br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que M. B...n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le magistrat désigné par le président du tribunal administratif de Bordeaux a rejeté sa demande tendant à l'annulation de l'arrêté du préfet de la Gironde en date du 7 octobre 2011 le plaçant en rétention ;<br/>
<br/>
              10. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat, qui n'est pas dans la présente instance la partie perdante, au titre de l'article 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 20 mars 2012 est annulé en tant qu'il a annulé l'arrêté du 7 octobre 2011 du préfet de la Gironde décidant le placement en rétention administrative de M. A...B...en tant que cet arrêté indique que " le recours juridictionnel contre la décision de placement en rétention administrative ne suspend pas l'exécution de la mesure d'éloignement ".<br/>
<br/>
Article 2 : La requête présentée par M. B...devant la cour administrative d'appel de Bordeaux et les conclusions présentées sur le fondement de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à M. A...B....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335 ÉTRANGERS. - RÉTENTION ADMINISTRATIVE - RECOURS CONTRE LA DÉCISION DE PLACEMENT - 1) INVOCATION DE L'ARTICLE 5 § 4 DE LA CONV. EDH - CARACTÈRE SUSPENSIF DU RECOURS - ABSENCE - EFFET SUSPENSIF SUR LA MESURE D'ÉLOIGNEMENT - ABSENCE - 2) NON-LIEU LORSQUE LA RÉTENTION A PRIS FIN - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">335-03-03 ÉTRANGERS. OBLIGATION DE QUITTER LE TERRITOIRE FRANÇAIS (OQTF) ET RECONDUITE À LA FRONTIÈRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - RÉTENTION ADMINISTRATIVE - RECOURS CONTRE LA DÉCISION DE PLACEMENT - 1) INVOCATION DE L'ARTICLE 5 § 4 DE LA CONV. EDH - CARACTÈRE SUSPENSIF DU RECOURS - ABSENCE - EFFET SUSPENSIF SUR LA MESURE D'ÉLOIGNEMENT - ABSENCE - 2) NON-LIEU LORSQUE LA RÉTENTION A PRIS FIN - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-05-05-01 PROCÉDURE. INCIDENTS. NON-LIEU. ABSENCE. - RÉTENTION ADMINISTRATIVE - RECOURS CONTRE LA DÉCISION DE PLACEMENT - FIN DE LA RÉTENTION.
</SCT>
<ANA ID="9A"> 335 1) Les stipulations de l'article 5 paragraphe 4 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (conv. EDH), qui garantissent le droit d'une personne privée de liberté de former un recours devant un tribunal qui statue rapidement sur la légalité de la détention, n'ont ni pour objet ni pour effet de conduire à reconnaître un caractère suspensif aux recours susceptibles d'être exercés contre les mesures de placement en rétention administrative ou à suspendre, en cas de recours contre de telles mesures, l'exécution des décisions, distinctes, qui ont ordonné l'éloignement des étrangers placés en rétention, ces décisions d'éloignement pouvant du reste faire l'objet d'un recours suspensif.,,2) Les recours pour excès de pouvoir dirigés contre les mesures de placement en rétention conservent un objet lorsque la rétention a pris fin. Absence de non-lieu.</ANA>
<ANA ID="9B"> 335-03-03 1) Les stipulations de l'article 5 paragraphe 4 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (conv. EDH), qui garantissent le droit d'une personne privée de liberté de former un recours devant un tribunal qui statue rapidement sur la légalité de la détention, n'ont ni pour objet ni pour effet de conduire à reconnaître un caractère suspensif aux recours susceptibles d'être exercés contre les mesures de placement en rétention administrative ou à suspendre, en cas de recours contre de telles mesures, l'exécution des décisions, distinctes, qui ont ordonné l'éloignement des étrangers placés en rétention, ces décisions d'éloignement pouvant du reste faire l'objet d'un recours suspensif.,,2) Les recours pour excès de pouvoir dirigés contre les mesures de placement en rétention conservent un objet lorsque la rétention a pris fin. Absence de non-lieu.</ANA>
<ANA ID="9C"> 54-05-05-01 Les recours pour excès de pouvoir dirigés contre les mesures de placement en rétention conservent un objet lorsque la rétention a pris fin. Absence de non-lieu.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
