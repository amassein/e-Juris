<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043482312</ID>
<ANCIEN_ID>JG_L_2021_05_000000434503</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/48/23/CETATEXT000043482312.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 05/05/2021, 434503, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-05-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434503</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI</AVOCATS>
<RAPPORTEUR>M. Laurent Roulaud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Domingo</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:434503.20210505</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
        M. A... B... a demandé au tribunal administratif de Paris, d'une part, d'annuler pour excès de pouvoir les décisions par lesquelles la ministre de la culture lui a refusé la consultation et la communication, au format papier et au format numérique, des documents d'archives publiques conservés sous la côte " 1521 W- Tribunal de grande instance de Paris - Service des minutes civiles ", et, à titre subsidiaire, d'annuler ces décisions en tant qu'elles lui refusent la consultation et la communication de ces mêmes documents d'archives pour les jugements rendus en audience publique, d'autre part, d'enjoindre de lui communiquer les documents demandés en vue de la reproduction et de la réutilisation des informations publiques qu'ils contiennent, dans un délai raisonnable à déterminer par le tribunal, sous astreinte de 200 euros par jour de retard. <br/>
<br/>
        Par un jugement n°1806468/5-3 du 10 juillet 2019, le tribunal administratif de Paris a rejeté cette demande.<br/>
<br/>
        Par un pourvoi, un mémoire complémentaire et un mémoire en réplique, enregistrés les 10 septembre et 10 décembre 2019 et le 4 février 2021 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
        1°) d'annuler ce jugement ;<br/>
<br/>
        2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
        3°) à titre subsidiaire, de saisir la Cour de justice de l'Union européenne à titre préjudiciel ;<br/>
<br/>
        4°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
        Vu les autres pièces du dossier ;<br/>
<br/>
        Vu :<br/>
        - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
        - le code du patrimoine ;<br/>
        - le code des relations entre le public et l'administration ; <br/>
        - la loi n°72-626 du 5 juillet 1972 ;<br/>
        - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
        Après avoir entendu en séance publique :<br/>
<br/>
        - le rapport de M. Laurent Roulaud, maître des requêtes en service extraordinaire,  <br/>
<br/>
        - les conclusions de M. Laurent Domingo, rapporteur public ;<br/>
<br/>
        La parole ayant été donnée, après les conclusions, à la SCP Spinosi, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B... a présenté, le 2 mars 2017, à la ministre de la culture et de la communication, une demande de consultation anticipée des documents d'archives publiques conservées sous la côte " 1521 W - tribunal de grande instance de Paris - service des minutes civiles ", comprenant les minutes des jugements des 3ème et 4ème chambres civiles du tribunal de grande instance de Paris rendues au cours de l'année 2016. A la suite du rejet de sa demande, le 17 juillet 2017, et de l'avis de la commission d'accès aux documents administratifs du 14 décembre 2017, M. B... a demandé au tribunal administratif de Paris d'annuler la décision implicite de rejet de sa demande de consultation anticipée de ces documents d'archives et d'enjoindre la communication des documents sollicités. M. B... se pourvoit en cassation contre le jugement du 10 juillet 2019 par lequel le tribunal administratif de Paris a rejeté sa demande. <br/>
<br/>
              2. L'article L. 211-1 du code du patrimoine dispose que : " Les archives sont l'ensemble des documents, y compris les données, quels que soient leur date, leur lieu de conservation, leur forme et leur support, produits ou reçus par toute personne physique ou morale et par tout service ou organisme public ou privé dans l'exercice de leur activité ". L'article L. 211-4 de ce code prévoit que : " Les archives publiques sont : (...) 1° Les documents qui procèdent de l'activité de l'Etat (...) ". L'article L. 213-1 du même code dispose que : " Les archives publiques sont, sous réserve des dispositions de l'article L. 213-2, communicables de plein droit. / L'accès à ces archives s'exerce dans les conditions définies pour les documents administratifs à l'article L. 311-9 du code des relations entre le public et l'administration ". Aux termes de l'article L. 213-2 : " Par dérogation aux dispositions de l'article L. 213-1 : I. - Les archives publiques sont communicables de plein droit à l'expiration d'un délai de : (...) 4° Soixante-quinze ans à compter de la date du document ou du document le plus récent inclus dans le dossier, ou un délai de vingt-cinq ans à compter de la date du décès de l'intéressé si ce dernier délai est plus bref (...) c) Pour les documents relatifs aux affaires portées devant les juridictions, sous réserve des dispositions particulières relatives aux jugements, et à l'exécution des décisions de justice (...) ". Aux termes de l'article L. 213-3 : " L'autorisation de consultation de documents d'archives publiques avant l'expiration des délais fixés au I de l'article L. 213-2 peut être accordée aux personnes qui en font la demande dans la mesure où l'intérêt qui s'attache à la consultation de ces documents ne conduit pas à porter une atteinte excessive aux intérêts que la loi a entendu protéger (...) ".<br/>
<br/>
              3. Si les minutes des jugements civils dont M. B... a demandé la communication, le 2 mars 2017, à la ministre de la culture, ne présentent pas le caractère de documents administratifs communicables sur le fondement du code des relations entre le public et l'administration, elles constituent des documents d'archives publiques dont la communication est régie par les dispositions des articles L. 213-1 à L. 213-3 du code du patrimoine.<br/>
<br/>
              4. Il appartient au juge de l'excès de pouvoir, saisi de moyens en ce sens, de contrôler la régularité et le bien-fondé d'une décision de refus opposée par le ministre de la culture à une demande de consultation anticipée de documents d'archives publiques. Pour ce faire, par exception au principe selon lequel le juge de l'excès de pouvoir apprécie la légalité d'un acte administratif à la date de son édiction, il appartient au juge, eu égard à la nature des droits en cause et à la nécessité de prendre en compte l'écoulement du temps et l'évolution des circonstances de droit et de fait afin de conférer un effet pleinement utile à son intervention, de se placer à la date à laquelle il statue. <br/>
<br/>
              5. L'intérêt légitime du demandeur doit être apprécié au vu de la démarche qu'il entreprend et du but qu'il poursuit en sollicitant la consultation anticipée d'archives publiques, de la nature des documents en cause et des informations qu'ils comportent. Les risques qui doivent être mis en balance sont ceux d'une atteinte excessive aux intérêts protégés par la loi, en particulier au secret relatif aux affaires portées devant les juridictions et à la protection de la vie privée des personnes physiques mentionnées dans les jugements. La pesée de l'un et des autres s'effectue en tenant compte notamment de l'effet, eu égard à la nature des documents en cause, de l'écoulement du temps et, le cas échéant, de la circonstance que ces documents ont déjà fait l'objet d'une autorisation de consultation anticipée ou ont été rendus publics.<br/>
<br/>
              6. Il ressort des énonciations du jugement attaqué que pour juger que la ministre de la culture avait pu rejeter la demande de M. B... de consultation anticipée des minutes demandées des jugements du tribunal de grande instance de Paris rendus au cours de l'année 2016, le tribunal administratif s'est seulement fondé sur l'existence d'un risque de désorganisation des services judiciaires et d'une atteinte au secret des affaires, sans tenir compte des intérêts légitimes du demandeur et sans mettre en balance les différents intérêts en présence pour apprécier la légalité du refus de la ministre de la culture. Par suite, le tribunal administratif a entaché son jugement d'erreur de droit.<br/>
<br/>
              7. Il s'ensuit que, sans qu'il y ait lieu de saisir la Cour de justice de l'Union européenne d'une question préjudicielle et sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, M. B... est fondé à demander l'annulation du jugement qu'il attaque. <br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative. <br/>
<br/>
              9. En premier lieu, contrairement à ce que soutient la ministre de la culture en défense, le litige qui porte sur un refus de communication d'archives publiques relève de la compétence du juge administratif. <br/>
<br/>
              10. En second lieu, il ressort des pièces du dossier que la demande de consultation anticipée, présentée le 2 mars 2017 par M. B..., portait sur l'ensemble des jugements archivés de deux chambres du tribunal de grande instance de Paris sans distinguer selon que ces derniers avaient ou non été rendus publiquement. <br/>
<br/>
              11. D'une part, il résulte des dispositions de l'article L. 213-2 du code du patrimoine que les délais à l'expiration desquels deviennent communicables de plein droit les documents d'archives publiques relatifs aux affaires portées devant les juridictions sont fixés sous réserve des dispositions particulières relatives aux jugements. En vertu de l'article 11-3 de la loi du 5 juillet 1972 instituant un juge de l'exécution et relative à la réforme de la procédure civile, " Les tiers sont en droit de se faire délivrer copie des jugements délivrés publiquement ". Il s'ensuit que, s'agissant des minutes de ceux des jugements demandés qui ont été rendus publiquement, la demande, fondée sur l'article L. 213-3 du code du patrimoine, de consultation anticipée de ces documents, avant l'expiration des délais fixés par l'article L. 213-2 du même code, était dépourvue d'objet et ne pouvait qu'être rejetée.<br/>
<br/>
              12. D'autre part, s'agissant des minutes de ceux des jugements demandés qui n'ont pas été rendus publiquement, il ressort des pièces du dossier que la demande de consultation anticipée a pour but de verser les minutes de ces jugements dans un fonds détenu par la société Foresti, destiné à alimenter le site " doctrine.fr ". Il n'apparaît pas, à la date de la présente décision, que l'intérêt du demandeur puisse être de nature à justifier l'accès aux documents d'archives demandés, sans que soit portée une atteinte excessive aux intérêts protégés par la loi, en particulier au secret relatif aux affaires portées devant les juridictions et à la protection de la vie privée des personnes physiques mentionnées dans les jugements. Il s'ensuit que la ministre de la culture n'a pas commis d'erreur d'appréciation en refusant la consultation anticipée des documents demandés en tant qu'ils concernaient des jugements non rendus publiquement. Elle n'a pas méconnu les stipulations de l'article 10 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales dès lors que la restriction apportée à la liberté d'expression, prévue par la loi, répond à un but légitime et est strictement nécessaire et proportionnée. <br/>
<br/>
              13. Par ailleurs, dès lors que les jugements en litige n'ont été ni publiés, ni communiqués, est, en tout état de cause, inopérant le moyen tiré de la méconnaissance de l'article L. 321-1 du code des relations entre le public et l'administration qui autorise l'utilisation des informations publiques figurant dans des documents administratifs communiqués ou publiés. Est, de même, inopérant le moyen tiré de l'insuffisance de motivation de la décision initiale de refus à laquelle s'est substituée la décision implicite prise après l'avis de la commission d'accès aux documents administratifs. <br/>
<br/>
              14. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur la fin de non-recevoir opposée en défense, que la demande de M. B... doit être rejetée, y compris ses conclusions au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
        --------------<br/>
<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Paris du 10 juillet 2019 est annulé.<br/>
Article 2 : La demande présentée par M. B... devant le tribunal administratif de Paris est rejetée. <br/>
Article 3 : La présente décision sera notifiée à M. A... B..., au garde des sceaux, ministre de la justice et à la ministre de la culture.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
