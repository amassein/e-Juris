<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044239148</ID>
<ANCIEN_ID>JG_L_2021_10_000000450128</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/23/91/CETATEXT000044239148.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 22/10/2021, 450128, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450128</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD</AVOCATS>
<RAPPORTEUR>M. Hervé Cassagnabère</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:450128.20211022</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société par action simplifiée (SAS) Cahors Pradis a demandé au tribunal administratif de Toulouse de prononcer la décharge de la cotisation de taxe d'enlèvement des ordures ménagères à laquelle elle a été assujettie au titre de l'année 2016 dans les rôles de la commune de Pradines (Lot). Par un jugement n° 1800479 du 31 décembre 2020, le magistrat désigné par le président de ce tribunal a fait droit à sa demande.<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 24 février et 10 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'économie, des finances et de la relance demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler les articles 1er et 2 de ce jugement ;<br/>
<br/>
              2°) de renvoyer dans cette mesure l'affaire au tribunal administratif de Toulouse.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - la loi n° 2015-1786 du 29 décembre 2015 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Hervé Cassagnabère, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, au Cabinet Briard, avocat de la société Cahors Pradis ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que la société Cahors Pradis a demandé la décharge de la taxe d'enlèvement des ordures ménagères à laquelle elle a été assujettie au titre de l'année 2016, pour un montant total de 62 941 euros, dans les rôles de la commune de Pradines (Lot), à raison de locaux commerciaux dont elle est propriétaire au 5112F, chemin du Moulin de Laberaudie. Le ministre de l'économie, des finances et de la relance se pourvoit en cassation contre le jugement du 31 décembre 2020 par lequel le tribunal administratif de Toulouse a fait droit à cette demande.<br/>
<br/>
              2. Aux termes du I de l'article 1520 du code général des impôts, dans sa rédaction issue du V de l'article 57 de la loi du 29 décembre 2015, de finances rectificative pour 2015 : " Les communes qui assurent au moins la collecte des déchets des ménages peuvent instituer une taxe destinée à pourvoir aux dépenses du service de collecte et de traitement des déchets ménagers et des déchets mentionnés à l'article L. 2224-14 du code général des collectivités territoriales, dans la mesure où celles-ci ne sont pas couvertes par des recettes ordinaires n'ayant pas le caractère fiscal ". Aux termes des dispositions du A du III de ce même article 57 : " Le présent article s'applique à compter du 1er janvier 2016 ". Les déchets mentionnés à l'article L. 2224-14 du code général des collectivités territoriales s'entendent des déchets non ménagers que ces collectivités peuvent, eu égard à leurs caractéristiques et aux quantités produites, collecter et traiter sans sujétions techniques particulières. <br/>
<br/>
              3. Il résulte de ces dispositions qu'à compter du 1er janvier 2016, l'objet de la taxe d'enlèvement des ordures ménagères a été étendu au financement de l'élimination des déchets non ménagers.<br/>
<br/>
              4. Il résulte des pièces du dossier soumis au juge du fond que, par une délibération du 31 mars 2016, le conseil de la communauté d'agglomération du Grand Cahors a fixé le taux de la taxe d'enlèvement des ordures ménagères pour l'année 2016 en vue de couvrir les dépenses prévisionnelles du service de collecte et de traitement des déchets assuré par cet établissement public de coopération intercommunale.  Le tribunal administratif, pour accueillir l'exception d'illégalité soulevée par la société Cahors Pradis à l'encontre de cette délibération, s'est fondé sur ce qu'à la date de son adoption, l'objet de la taxe d'enlèvement des ordures ménagères n'incluait pas le financement de l'élimination des déchets non ménagers. En statuant ainsi, il a méconnu la règle rappelée au point 3. <br/>
<br/>
              5. Il résulte de ce qui précède que le ministre de l'économie, des finances et de la relance est fondé, sans qu'il soit besoin de se prononcer sur les autres moyens de son pourvoi, à demander l'annulation des articles 1er et 2 du jugement attaqué.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er et 2 du jugement du tribunal administratif de Toulouse du 31 décembre 2020 sont annulés.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, au tribunal administratif de Toulouse.<br/>
Article 3 : Les conclusions de la société Cahors Pradis présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'économie, des finances et de la relance et à la société par action simplifiée Cahors Pradis.<br/>
              Délibéré à l'issue de la séance du 16 septembre 2021 où siégeaient : M. Pierre Collin, président de chambre, présidant ; M. Jean-Claude Hassan, conseiller d'Etat et M. Hervé Cassagnabère, conseiller d'Etat-rapporteur. <br/>
<br/>
              Rendu le 22 octobre 2021.<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Pierre Collin<br/>
 		Le rapporteur : <br/>
      Signé : M. Hervé Cassagnabère<br/>
                 La secrétaire :<br/>
                 Signé : Mme B... A...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
