<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029214507</ID>
<ANCIEN_ID>JG_L_2014_07_000000363145</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/21/45/CETATEXT000029214507.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 09/07/2014, 363145, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>363145</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER ; FOUSSARD ; SCP RICHARD</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:363145.20140709</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 1er octobre et 28 décembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SCI 47 Claude Lorrain, dont le siège est au 47, rue Claude Lorrain à Paris (75016), représentée par son gérant ; la SCI 47 Claude Lorrain demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10PA02750 du 31 juillet 2012 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant à l'annulation du jugement n° 0711325-0812192/7-1 du 1er avril 2010 par lequel le tribunal administratif de Paris a annulé, à la demande de M. et Mme A...B..., le permis de construire et le permis de construire modificatif qui lui avaient été délivrés par le maire de Paris les 30 janvier 2007 et 27 mai 2008 pour des travaux d'extension et de surélévation de l'immeuble sis au 47 rue Claude Lorrain à Paris ;<br/>
<br/>
              2°) statuant au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de M. et Mme B...la somme de 4 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, avocat de la SCI 47 Claude Lorrain et à la SCP Richard, avocat de M. et Mme et B...;<br/>
<br/>
<br/>
<br/>
<br/>Sur l'arrêt attaqué en tant qu'il statue sur la légalité des dispositions des articles UG 7.1. et UG 10.3.1 du règlement annexé au plan local d'urbanisme de la Ville de Paris :<br/>
<br/>
              1. Considérant, d'une part, que le chapitre VIII des dispositions générales du règlement du plan local d'urbanisme (PLU) de la Ville de Paris, dans sa rédaction applicable en l'espèce, définit comme pièce principale " toute pièce destinée au séjour, au sommeil ou au travail d'une manière continue " ; qu'aux termes de l'article UG 7.1 de ce règlement : " (...)/ 1°- Façade ou partie de façade comportant des baies constituant l'éclairement premier de pièces principales:/ Lorsqu'une façade ou une partie de façade à édifier en vis-à-vis d'une limite séparative (...) comporte une ou plusieurs baies constituant l'éclairement premier de pièces principales, elle doit respecter, au droit de cette limite, un prospect minimal de 6 mètres (...). Toute pièce principale doit être éclairée par au moins une baie comportant une largeur de vue égale à 4 mètres au minimum (...)/ 2° - Façade ou partie de façade comportant des baies dont aucune ne constitue l'éclairement premier de pièces principales:/ Lorsqu'une façade ou une partie de façade à édifier en vis-à-vis d'une limite séparative (...) comporte des baies dont aucune ne constitue l'éclairement premier de pièces principales, elle doit respecter, au droit de cette limite, un prospect minimal de 2 mètres (...) " ; que, d'autre part, le chapitre VIII des dispositions générales du même règlement précise que, dans la bande E, qui constitue une zone de 20 mètres à partir de l'alignement de la voie publique et dans laquelle s'applique le gabarit-enveloppe, les constructions doivent en principe être implantées sur les limites séparatives latérales du terrain considéré aboutissant à l'alignement des voies ; qu'aux termes de l'article UG 10.3.1 du même règlement : " Les gabarits-enveloppes définis ci-après s'appliquent en vis-à-vis d'une limite séparative (...) 1°- Gabarit-enveloppe à l'intérieur de la bande E :/ Les façades ou parties de façade comportant des baies constituant l'éclairement premier de pièces principales en vis-à-vis d'une limite séparative (...) sont assujetties à un gabarit-enveloppe identique à celui défini en bordure de voie, élevé à 6 mètres de cette limite. Le point d'attache du gabarit-enveloppe est pris à 6 mètres de la limite séparative, au même niveau que celui du gabarit-enveloppe défini en bordure de voie (...) " ;<br/>
<br/>
              2. Considérant, en premier lieu, que ces dispositions déterminent sans ambiguïté les règles de prospect et de gabarit-enveloppe applicables en limite séparative, selon que les baies en façade constituent ou non l'éclairement premier de pièces principales de la construction projetée ; que, par suite, la cour  n'a pas commis d'erreur de droit en jugeant qu'elles ne méconnaissaient pas l'objectif de valeur constitutionnelle d'accessibilité et d'intelligibilité de la norme ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'aux termes de l'article L. 123-1 du code de l'urbanisme : " Les plans locaux d'urbanisme comportent un règlement qui fixe, en cohérence avec le projet d'aménagement et de développement durable, les règles générales et les servitudes (...) qui (...) définissent, en fonction des circonstances locales, les règles concernant l'implantation des constructions. A ce titre, ils peuvent : (...)/ 4° Déterminer des règles concernant l'aspect extérieur des constructions, leurs dimensions et l'aménagement de leurs abords, afin de contribuer à la qualité architecturale et à l'insertion harmonieuse des constructions dans le milieu environnant (...) " ; que l'article R. 123-9 du même code, dans sa version applicable, prévoit, en outre, que : " le règlement peut comprendre tout ou partie des règles suivantes : (...)/ 6° L'implantation des constructions par rapport aux voies et emprises publiques ;/ 7° L'implantation des constructions par rapport aux limites séparatives (...) " ; qu'aucune disposition législative ou réglementaire ne fait obstacle à ce que les règles et servitudes édictées en application des dispositions précités de l'article L. 123-1 du code de l'urbanisme régissent des situations qui font, par ailleurs, l'objet d'une réglementation en vertu d'autres textes et notamment du code de la construction et de l'habitation ; que les règles critiquées qui, d'une part, fixent à 6 mètres ou 2 mètres la distance qui sépare les façades ou parties de façades en vis-à-vis d'une limite séparative, selon que celles-ci comportent ou non des baies constituant l'éclairement premier de pièces principales et, d'autre part, les assujettit ou non, dans la bande E, à un gabarit-enveloppe identique à celui défini en bordure de voie, élevé à 6 mètres de la limite séparative et dont le point d'attache est pris à 6 mètres de cette limite, au même niveau que celui du gabarit-enveloppe défini en bordure de voie, répondent à des préoccupations d'urbanisme et de salubrité ; qu'ainsi, elles entrent dans le champ du 4° de l'article L. 123-1 du code de l'urbanisme ; que, par suite, la cour n'a pas commis d'erreur de droit en jugeant que les articles UG 7.1 et UG 10.3.1 du PLU de la Ville de Paris ne sont pas entachés d'illégalité pour avoir fait dépendre leur champ d'application de l'agencement intérieur des pièces d'habitation en distinguant celles d'entre elles qui constituent des " pièces principales ", dès lors que les règles relatives au prospect en limite séparative ainsi qu'au gabarit-enveloppe, dans la bande E, des façades comportant des baies constituant l'éclairement premier de pièces principales sont liées, eu égard à leur objet, à la nature de ces pièces ; <br/>
<br/>
              4. Considérant, en troisième lieu, que les articles R. 431-4 et suivants du code de l'urbanisme, qui énumèrent de façon limitative les documents qui doivent être joints à la demande de permis de construire, sont sans effet sur la légalité des dispositions critiquées du PLU de la Ville de Paris qui fixent les règles de prospect à partir des limites séparatives ; que, par suite, le moyen tiré de ce que la cour aurait commis une erreur de droit pour n'avoir pas relevé la méconnaissance de ces dispositions n'est pas fondé ;<br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il statue sur la légalité du permis de construire :<br/>
<br/>
              5. Considérant en premier lieu que, si pour juger que les pièces éclairées par les baies situées en rez-de-chaussée et au premier étage sur la façade située à moins de 6 mètres de la limite séparative devaient être regardées comme des " pièces principales ", au sens des dispositions précitées du PLU de la Ville de Paris, la cour s'est fondée sur les plan annexés au permis de construire initial et au premier permis de construire modificatif, délivré le 31 août 2007, elle a précisé que les deux permis de construire modificatifs suivants, délivrés respectivement les 27 mai 2008 et 29 octobre 2009, n'avaient modifié ni les caractéristiques physiques des pièces, ni leur destination affichée ; que le moyen tiré de ce que la cour aurait dénaturé les pièces du dossier en ne tenant compte que des plans annexés au permis de construire initial et au premier permis modificatif manque en fait ;<br/>
<br/>
              6. Considérant en second lieu, qu'il ressort des pièces du dossier soumis aux juges du fond que c'est par une appréciation souveraine, exempte de dénaturation, que la cour a estimé que les pièces qualifiées sur les plans de " salle de gymnastique et de massage ", de " coin repas ", de " bibliothèque " et de " chambre 1 " devaient être regardées comme des " pièces principales " au sens des dispositions précitées du PLU de la Ville de Paris, que les baies qui les éclairaient constituaient leur éclairement premier, alors même que deux de ces pièces se situaient dans le prolongement d'un " séjour ", d'une part, et d'une " chambre ", d'autre part, bénéficiant chacun d'une baie située sur une autre façade ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que la SCI 47 Claude Lorrain n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              Sur l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle, en tout état de cause, à ce qu'une somme mise à la charge de M. et MmeB..., qui ne sont pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la SCI 47 Claude Lorrain la somme de 3 000 euros que M. et Mme B...demandent au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de la SCI 47 Claude Lorrain est rejeté.<br/>
Article 2 : La SCI 47 Claude Lorrain versera à M. et Mme B...la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Les conclusions de la Ville de Paris tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la SCI 47 Claude Lorrain, à M. et Mme A...B...et à la Ville de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
