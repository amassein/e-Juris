<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029046171</ID>
<ANCIEN_ID>JG_L_2014_01_000000374354</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/04/61/CETATEXT000029046171.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 10/01/2014, 374354, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-01-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374354</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BLANC, ROUSSEAU</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2014:374354.20140110</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 2 janvier 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par Mme B...A..., élisant domicile... ; la requérante demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1309720 du 20 décembre 2013 par laquelle le juge des référés du tribunal administratif de Nantes, statuant sur le fondement de l'article <br/>
L. 521-2 du code de justice administrative, a rejeté sa demande tendant, en premier lieu, à la suspension de l'exécution des arrêtés du préfet de Maine-et-Loire du 29 avril 2013 rejetant sa demande d'admission au titre de l'asile, du 11 décembre 2013 décidant de sa remise aux autorités polonaises et du 16 décembre 2013 l'assignant à résidence, en second lieu, à ce qu'il soit enjoint au préfet de Maine-et-Loire, d'une part, de lui délivrer une autorisation provisoire de séjour et de transmettre sa demande d'asile à l'office français pour la protection des réfugiés et apatrides et, d'autre part, de procéder à un nouvel examen de sa situation dans un délai de quinze jours à compter du prononcé de l'ordonnance, sous astreinte de 100 euros par jour de retard ;  <br/>
<br/>
              2°) de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) de l'admettre au bénéfice de l'aide juridictionnelle à titre provisoire ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat le versement de la somme de 2 000 euros au profit de son avocat à la cour, qui renonce au bénéfice de l'aide juridictionnelle, en application des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
<br/>
              elle soutient que : <br/>
              - le préfet de Maine-et-Loire a porté une atteinte grave et manifestement illégale au droit d'asile ;<br/>
              - faute qu'elle se soit soustraite intentionnellement au contrôle de l'autorité administrative dans le but de faire échec à son éloignement, elle ne peut être regardée comme ayant pris la fuite au sens des dispositions de l'article 20 du règlement Dublin II  et le délai maximum de son transfert vers la Pologne n'a pu, dès lors, être porté à un an ; <br/>
<br/>
              - les décisions prises par le préfet de Maine-et-Loire à son encontre ayant été annulées par le tribunal administratif de Nantes par un jugement du 7 mai 2013, elle ne pouvait faire l'objet d'aucune décision d'éloignement jusqu'à la nouvelle décision de réadmission vers la Pologne, prise par le préfet de Maine-et-Loire le 11 décembre 2013 ;<br/>
              - le délai de six mois du règlement Dublin II, qui s'appliquait à compter du jugement du tribunal administratif du 7 mai 2013, était expiré lorsque le préfet de Maine-et-Loire à nouveau sollicité les autorités polonaises pour la réadmettre, le 26 novembre 2013 ;<br/>
<br/>
<br/>
<br/>
                          Vu l'ordonnance attaquée ; <br/>
<br/>
              Vu le mémoire en défense, enregistré le 7 janvier 2014, présenté par le ministre de l'intérieur, qui conclut au rejet de la requête  ; il soutient que :<br/>
              - le comportement de la requérante caractérise une fuite au sens des dispositions de l'article 20 de Dublin II  ; <br/>
              - le second arrêté de transfert aux autorités polonaises pris le 11 décembre 2013 a été pris dans le délai de dix-huit mois à compter du premier accord des autorités polonaises, le 11 décembre 2012 ;<br/>
<br/>
                          Vu les autres pièces du dossier ; <br/>
<br/>
              Vu la Constitution ;<br/>
<br/>
               Vu le règlement (CE) n° 343/2003 du Conseil du 18 février 2003 ;<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, Mme A...et, d'autre part, le ministre de l'intérieur ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 8 janvier 2014 à 10 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Rousseau, avocat au Conseil d'Etat et à la Cour de Cassation, avocat de Mme A...;<br/>
              - les représentants du ministre de l'intérieur ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clôturé l'instruction ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ;<br/>
<br/>
              2. Considérant que le droit constitutionnel d'asile, qui a le caractère d'une liberté fondamentale, a pour corollaire le droit de solliciter le statut de réfugié ; que, s'il implique que l'étranger qui sollicite la reconnaissance de la qualité de réfugié soit en principe autorisé à demeurer sur le territoire jusqu'à ce qu'il ait été statué sur sa demande, ce droit s'exerce dans les conditions définies par l'article L. 741-4 du code de l'entrée et du séjour des étrangers et du droit d'asile ; que le 1° de cet article permet de refuser l'admission en France d'un demandeur d'asile, lorsque l'examen de la demande d'asile relève de la compétence d'un autre Etat en application des dispositions du règlement (CE) n° 343/2003 du Conseil du 18 février 2003 ; qu'aux termes de l'article 19 de ce règlement : "  (...)  3. Le transfert du demandeur de l'État membre auprès duquel la demande d'asile a été introduite vers l'État membre responsable s'effectue conformément au droit national du premier État membre, après concertation entre les États membres concernés, dès qu'il est matériellement possible et, au plus tard, dans un délai de six mois à compter de l'acceptation de la demande de prise en charge ou de la décision sur le recours ou la révision en cas d'effet suspensif. / 4. Si le transfert n'est pas exécuté dans le délai de six mois, la responsabilité incombe à l'État membre auprès duquel la demande d'asile a été introduite. Ce délai peut être porté à un an au maximum s'il n'a pas pu être procédé au transfert en raison d'un emprisonnement du demandeur d'asile ou à dix-huit mois au maximum si le demandeur d'asile prend la fuite " ; <br/>
<br/>
              3. Considérant qu'il résulte de l'instruction que MmeA..., ressortissante russe, entrée irrégulièrement en France en novembre 2012, a sollicité l'asile en France le 29 novembre 2012 ; qu'après avoir constaté, par la consultation du fichier " Eurodac ", que ses empreintes digitales avaient été relevées le 4 novembre 2012 en Pologne, le préfet de Maine-et-Loire a, par un arrêté du 30 novembre 2012, refusé de l'admettre provisoirement au séjour au titre de l'asile ; que les autorités polonaises ont accepté, le 11 décembre 2012, de prendre en charge la demande d'asile de MmeA..., la Pologne devenant l'Etat responsable de la demande d'asile en vertu des dispositions de l'article 16 du règlement précité, dans les conditions prévues à l'article 19 de ce règlement  ; que le préfet de Maine-et-Loire a pris, d'une part, 11 décembre 2012, arrêté l'assignant à résidence, d'autre part, le 12 décembre 2012, un arrêté portant remise de l'intéressée aux autorités polonaises ; que, le 8 février 2013, le préfet de Maine-et-Loire a transmis aux autorités polonaises une notification de report de transfert, au motif d'une fuite de la requérante ; <br/>
<br/>
              4. Considérant que le juge des référés du tribunal administratif de Nantes a, par une ordonnance du 13 février 2013, suspendu l'exécution des décisions du préfet de Maine-et-Loire du 30 novembre 2012 portant refus d'admission provisoire au séjour de Mme A...et du 12 décembre portant remise aux autorités polonaises ; que, par un jugement du 7 mai 2013, le tribunal administratif de Nantes a, en premier lieu, annulé, d'une part, ces deux décisions au motif que la requérante n'avait pas reçu les informations relatives au traitement des données du système " Eurodac ", en méconnaissance des dispositions de l'article 18-1 du règlement (CE) du Conseil du 11 décembre 2000 concernant la création du système " Eurodac " pour la comparaison des empreintes digitales aux fins de l'application efficace de la convention de Dublin, d'autre part, par voie de conséquence, la décision assignant Mme A...à résidence et, en second lieu, a enjoint au préfet de Maine-et-Loire de procéder au réexamen de la situation de celle-ci, dans le délai de deux mois à compter de la notification du jugement ; <br/>
<br/>
              5. Considérant qu'en exécution du jugement du 7 mai 2013 du tribunal administratif de Nantes, le préfet de Maine-et-Loire a saisi les autorités polonaises d'une nouvelle demande de prise en charge de MmeA..., le 26 novembre 2013 ; que celles-ci ont, le 11 décembre 2012, confirmé l'accord qu'elles avaient donné le 11 décembre 2012 ; que le préfet de Maine-et-Loire a alors pris, d'une part, un nouvel arrêté de remise aux autorités polonaises le 11 décembre 2013 et, d'autre part, un arrêté d'assignation à résidence le 16 décembre suivant ; que le juge des référés du tribunal administratif de Nantes a rejeté, par une ordonnance du 20 décembre 2013, la demande de Mme A...tendant à la suspension de l'exécution de ces deux arrêtés sur le fondement de l'article L. 521-2 du code de justice administrative ; que l'intéressée relève appel de cette ordonnance ; <br/>
<br/>
              6. Considérant que l'ordonnance du 13 février 2013 par laquelle le juge des référés du tribunal administratif de Nantes a, sur le fondement de l'article L. 521-2 du code de justice administrative, suspendu l'exécution de l'arrêté préfectoral du 12 décembre 2012 décidant la remise aux autorités polonaises de Mme A...doit être regardée comme ayant empêché le délai de transfert prévu à l'article 19 du règlement du 18 février 2003 de continuer à courir ; qu'en prenant, à la suite du jugement du 7 mai 2013 qui lui faisait injonction de procéder au réexamen de la demande de MmeA..., un nouvel arrêté de remise de celle-ci aux autorités polonaises, après confirmation de l'accord de ces autorités le 2 décembre 2013, dans le délai de six mois à compter de cette dernière date, le préfet de Maine-et-Loire n'a pas commis d'illégalité manifeste ; que, dès lors, Mme A...n'est pas fondée à soutenir que c'est à tort que, par son ordonnance du 20 décembre 2013, le juge des référés du tribunal administratif de Nantes a rejeté ses demandes ; qu'ainsi, sans qu'il y ait lieu de lui accorder ni l'aide juridictionnelle à titre provisoire ni, en tout état de cause, la somme demandée en application de l'article L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, son appel doit être rejeté ; que, par voie de conséquences, ses conclusions à fin d'injonction doivent être également rejetées ; <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de Mme A...est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à Mme B...A...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
