<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034900532</ID>
<ANCIEN_ID>JG_L_2017_06_000000398822</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/90/05/CETATEXT000034900532.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 09/06/2017, 398822, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398822</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Clément Malverti</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:398822.20170609</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 15 avril et 12 juillet 2016 et le 19 janvier 2017 au secrétariat du contentieux du Conseil d'Etat, la société NC Numericable et la société SFR demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 1er mars 2016 du ministre de l'économie, de l'industrie et du numérique, de la secrétaire d'Etat chargée du commerce, de l'artisanat, de la consommation et de l'économie sociale et solidaire et de la secrétaire d'Etat chargée du numérique portant modification de l'arrêté du 3 décembre 2013 relatif à l'information préalable du consommateur sur les caractéristiques techniques des offres d'accès à l'internet en situation fixe filaire ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 10 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la directive (CE) n° 2005/29/CE du Parlement européen et du Conseil du 11 mai 2005 ;<br/>
              - le code de la consommation ;<br/>
              - le code des postes et des communications électroniques ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Clément Malverti, auditeur, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de la société NC Numericable et de la société SFR ; <br/>
<br/>
              Vu la note en délibéré, enregistrée le 23 mai 2017, présentée par la société NC Numericable et la société SFR ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que le ministre de l'économie, de l'industrie et du numérique, la secrétaire d'Etat chargée du commerce, de l'artisanat, de la consommation et de l'économie sociale et la secrétaire d'Etat chargée du numérique ont, par un arrêté du 1er mars 2016, modifié l'arrêté du 3 décembre 2013 relatif à l'information préalable du consommateur sur les caractéristiques techniques des offres d'accès à l'internet en situation fixe filaire ; que cet arrêté du 1er mars 2016 insère un article 6-1 dans l'arrêté du 3 décembre 2013 qui dispose que : " Tout message publicitaire ou document commercial d'un fournisseur de services relatif à une offre utilisant une technologie pour laquelle le débit ne varie pas significativement en fonction des caractéristiques du raccordement du consommateur au réseau fixe ouvert au public, s'il associe le terme "fibre" aux services du fournisseur alors que le raccordement du client final jusque dans son logement n'est pas réalisé en fibre optique, comporte la mention "(sauf raccordement du domicile)". / Cette mention figure à la suite de chaque utilisation du terme "fibre" ou de l'expression "fibre optique", associée aux services du fournisseur, dans des conditions d'audibilité et de lisibilité au moins égales, notamment en termes de volume sonore, de taille de caractère et de couleur. / Dans le cas d'un message publicitaire non radiophonique, la mention visée au premier alinéa est complétée par une seconde mention précisant le support physique du raccordement final et commençant par les mots : "le raccordement du domicile n'est pas en fibre optique mais en". Si elle est écrite, cette seconde mention figure dans des caractères suffisamment importants, s'inscrit de façon distincte des autres mentions rectificatives et légales et doit être clairement identifiée comme venant préciser la mention visée au premier alinéa. " ; que les sociétés Numericable et SFR demandent l'annulation pour excès de pouvoir de cet arrêté ;<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              2.	Considérant, en premier lieu, qu'aux termes du premier alinéa de l'article L. 113-3 du code de la consommation, dans sa version applicable au litige, sur le fondement duquel a été pris l'arrêté attaqué : " Tout vendeur de produit ou tout prestataire de services doit, par voie de marquage, d'étiquetage, d'affichage ou par tout autre procédé approprié, informer le consommateur sur les prix et les conditions particulières de la vente et de l'exécution des services, selon des modalités fixées par arrêtés du ministre chargé de l'économie, après consultation du Conseil national de la consommation " ; qu'en imposant aux fournisseurs de services de communication électroniques de faire figurer, dans leurs messages publicitaires et leurs documents commerciaux, des mentions relatives aux caractéristiques techniques du service qu'ils offrent, le ministre chargé de l'économie s'est borné à déterminer des modalités d'information du consommateur sur les conditions particulières de la vente et de l'exécution des services de communication électroniques ; que, par suite, il était compétent pour prendre l'arrêté attaqué sur le fondement des dispositions de l'article L. 113-3 du code de la consommation ;<br/>
<br/>
              3.	Considérant, en deuxième lieu, que la circonstance que la secrétaire d'Etat chargée du commerce, de l'artisanat, de la consommation et de l'économie sociale et la secrétaire d'Etat chargée du numérique ont signé l'arrêté attaqué ne saurait, en tout état de cause, avoir d'incidence sur sa légalité dès lors que l'arrêté comporte la signature du ministre chargé de l'économie ;<br/>
<br/>
              4.	Considérant, en dernier lieu, que, ainsi qu'il a été dit, l'arrêté attaqué est intervenu en application des dispositions de l'article L. 113-3 du code de la consommation, dans leur version applicable au litige ; que, dès lors, les dispositions du V de l'article L. 32-1 du code des postes et des communications électroniques, lesquelles ne trouvent à s'appliquer qu'aux mesures prises dans le cadre des dispositions de ce code, ne sont pas applicables en l'espèce ; que, par suite, les sociétés requérantes ne sauraient utilement soutenir que l'arrêté qu'elles attaquent n'aurait pas été pris selon la procédure prévue par les dispositions du V de l'article L. 32-1 du code des postes et des communications électroniques ;<br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              5.	Considérant, en premier lieu, que les prescriptions contenues dans l'arrêté attaqué ont pour objectif d'améliorer l'information préalable des consommateurs de services de communication électroniques en imposant aux fournisseurs de ces services de faire figurer, dans leur communication commerciale, des informations relatives aux caractéristiques techniques des offres d'accès à l'internet qu'ils proposent ; que les sociétés requérantes ne contestent pas que la technologie qui n'assure pas le raccordement en fibre optique de l'utilisateur final jusque dans son logement présente des performances moindres en matière de débit montant que la technologie assurant un raccordement en fibre optique de bout en bout et que ces différences de performance peuvent être significatives pour certains usages de l'internet ; que l'arrêté attaqué n'interdit pas aux fournisseurs utilisant la technologie qui n'assure pas le raccordement en fibre optique du client final jusque dans son logement d'utiliser le mot " fibre " dans leur communication commerciale, mais se borne à leur imposer d'y associer la mention, dans des conditions d'audibilité et de lisibilité au moins égales, " sauf raccordement du domicile " ; que, dans ces conditions, l'arrêté attaqué ne méconnaît ni le principe d'égalité ni, en tout état de cause, le principe de neutralité technologique énoncé au II de l'article L. 32-1 du code des postes et des communications électroniques et l'objectif de la promotion des investissements et de l'innovation dans les infrastructures améliorées énoncé au IV de cet article ; que, contrairement à ce qui est soutenu, il ne porte en tout état de cause pas atteinte à la liberté d'expression des fournisseurs de services de communication électroniques ; <br/>
<br/>
              6.	Considérant, en deuxième lieu, que les prescriptions de l'arrêté attaqué ne sauraient être regardées comme instituant une aide d'Etat au sens des dispositions de l'article 107 du traité sur le fonctionnement de l'Union européenne ;<br/>
<br/>
              7.	Considérant, en troisième lieu, que l'arrêté attaqué vise à renforcer l'information du consommateur sur les caractéristiques techniques des offres d'accès à l'internet et n'a pas pour objet d'interdire de manière générale une pratique commerciale déloyale au sens de la directive 2005/29/CE du Parlement européen et du Conseil du 11 mai 2005 relative aux pratiques commerciales déloyales des entreprises vis-à-vis des consommateurs dans le marché intérieur ; que, dès lors, le moyen tiré de la méconnaissance des dispositions de cette directive ne peut qu'être écarté ;<br/>
<br/>
              8.	Considérant, en dernier lieu, que le détournement de pouvoir allégué n'est pas établi ;<br/>
<br/>
              9.	Considérant qu'il résulte de tout ce qui précède, sans qu'il y ait lieu de saisir à titre préjudiciel la Cour de justice de l'Union européenne, que les sociétés NC Numericable et SFR ne sont pas fondées à demander l'annulation de l'arrêté qu'elles attaquent ; que leurs conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête des sociétés NC Numericable et SFR est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société NC Numericable, à la société SFR et au ministre de l'économie.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
