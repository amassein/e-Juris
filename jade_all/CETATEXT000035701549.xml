<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035701549</ID>
<ANCIEN_ID>JG_L_2017_10_000000409543</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/70/15/CETATEXT000035701549.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 02/10/2017, 409543, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-10-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409543</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLOCHE</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:409543.20171002</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... C...a porté plainte contre M. D... B...devant la chambre disciplinaire de première instance de Rhône-Alpes de l'ordre des médecins. Par une décision du 2 octobre 2015, la chambre disciplinaire de première instance a rejeté sa plainte.<br/>
<br/>
              Par une décision du 6 février 2017, la chambre disciplinaire nationale de l'ordre des médecins a rejeté l'appel formé par M. C... contre cette décision.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 5 avril et 5 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, M. C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du Conseil national de l'ordre des médecins et de M. B... la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son article 61-1 ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de la santé publique ;<br/>
              - la loi n° 90-568 du 2 juillet 1990 ;<br/>
              - le décret n° 2014-107 du 4 février 2014 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boulloche, avocat de M. C...;<br/>
<br/>
<br/>
<br/>
<br/>
              1.  Considérant qu'aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux " ;<br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              2.  Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de cet article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              3.  Considérant qu'aux termes du premier alinéa de l'article L. 4124-2 du code de la santé publique, dont la constitutionnalité est contestée par M.C... : " Les médecins, les chirurgiens-dentistes ou les sages-femmes chargés d'un service public et inscrits au tableau de l'ordre ne peuvent être traduits devant la chambre disciplinaire de première instance, à l'occasion des actes de leur fonction publique, que par le ministre chargé de la santé, le représentant de l'Etat dans le département, le directeur général de l'agence régionale de santé, le procureur de la République, le conseil national ou le conseil départemental au tableau duquel le praticien est inscrit " ;<br/>
<br/>
              4.  Considérant, en premier lieu, que si les dispositions attaquées réservent aux autorités publiques qu'elles désignent le pouvoir de poursuivre devant la juridiction disciplinaire un praticien chargé d'une mission de service public en raison des actes accomplis à l'occasion de sa fonction publique, elles sont sans incidence sur le droit de toute personne qui s'estimerait victime d'un manquement déontologique commis par un de ces praticiens de saisir la juridiction compétente afin d'obtenir réparation du préjudice dont il serait responsable, ou de mettre en mouvement l'action publique si les faits commis par ce médecin sont susceptibles de recevoir une qualification pénale ; qu'elles ne peuvent, par suite, être regardées comme portant une atteinte substantielle au droit des personnes intéressées d'exercer un recours effectif devant une juridiction et comme méconnaissant, par suite, les dispositions de l'article 16 de la Déclaration des droits de l'homme et du citoyen de 1789 ;<br/>
<br/>
              5. Considérant, en deuxième lieu, qu'en organisant, dans les conditions rappelées au point qui précède, les conditions dans lesquelles s'exerce la poursuite, devant les juridictions disciplinaires, des praticiens qu'elles concernent, ces dispositions ne peuvent, en tout état de cause, être regardées comme méconnaissant les dispositions de l'article 15 de la Déclaration des droits de l'homme et du citoyen de 1789 qui reconnaissent à la société " le droit de demander des comptes à tout agent public de son administration " ;<br/>
<br/>
              6.  Considérant, en troisième lieu, que, d'une part, s'agissant des praticiens chargés d'un service public en leur qualité d'agents publics, le principe d'égalité n'impose pas que les conditions de mise en oeuvre des poursuites disciplinaires à l'égard d'agents publics soient identiques à celles applicables aux autres praticiens ; que, d'autre part, s'agissant des praticiens n'ayant pas la qualité d'agent public mais qui doivent être regardés, pour certains de leurs actes, comme chargés d'un service public en raison de l'intérêt général qui s'attache à leur mission et des prérogatives qui lui sont associées, les dispositions attaquées, en prévoyant que seules les autorités publiques ou ordinales peuvent mettre en cause leur responsabilité disciplinaire, poursuivent un objectif d'intérêt général de garantir l'indépendance de ces médecins, chirurgiens-dentistes ou sages-femmes dans l'accomplissement de ces missions de service public ; que, par suite, la différence de traitement introduite par le premier alinéa de l'article L. 4124-2 du code de la santé publique, entre les médecins " chargés d'un service public " et les autres médecins, ne méconnaît pas les principes d'égalité devant la loi et d'égalité devant la justice garantis par les articles 6 et 16 de la Déclaration de 1789 ;<br/>
<br/>
              7. Considérant, enfin, que ces dispositions ne portent en tout état de cause aucune atteinte à l'objectif de valeur constitutionnelle de bonne administration de la justice ;<br/>
<br/>
              8.  Considérant qu'il résulte de tout ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; qu'ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré par M. C..., à l'appui de son pourvoi en cassation, de ce que le premier alinéa de l'article L. 4124-2 du code de la santé publique porte atteinte aux droits et libertés garantis par la Constitution, n'est pas de nature à permettre l'admission du pourvoi ;<br/>
<br/>
              Sur les autres moyens du pourvoi :<br/>
<br/>
              9.  Considérant que, pour demander l'annulation de la décision de la chambre disciplinaire nationale de l'ordre des médecins qu'il attaque, M. C... soutient également qu'elle est entachée d'erreur de droit et de dénaturation des faits en ce qu'elle fait application du décret du 4 février 2014 relatif à la création du comité médical national et de la commission de réforme nationale de la société anonyme Orange qui, d'une part, n'était pas entré en vigueur et, d'autre part, est illégal en ce qu'il renvoie à des dispositions applicables aux fonctionnaires ; qu'elle est entachée d'insuffisance de motivation, d'erreur de droit et d'inexacte qualification juridique des faits en ce qu'elle juge que le médecin concerné par sa plainte est chargé d'un service public ; qu'en jugeant sa plainte irrecevable, elle méconnaît le droit à un recours juridictionnel effectif, le droit à un procès équitable et le principe de non-discrimination garantis par les articles 6, 13 et 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; qu'aucun de ces autres moyens n'est de nature à permettre l'admission du pourvoi ;<br/>
<br/>
              10.  Considérant qu'il résulte de tout ce qui précède que le pourvoi formé par M. C... contre la décision du 6 février 2017 de la chambre disciplinaire nationale de l'ordre des médecins ne peut être admis ;<br/>
<br/>
<br/>
<br/>
<br/>
              				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M.C....<br/>
Article 2 : Le pourvoi de M. C...n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à M. A... C....<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre, à la ministre des solidarités et de la santé et à M. D... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
