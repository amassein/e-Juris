<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026807341</ID>
<ANCIEN_ID>JG_L_2012_12_000000354873</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/80/73/CETATEXT000026807341.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 19/12/2012, 354873</TITRE>
<DATE_DEC>2012-12-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354873</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PEIGNOT, GARREAU, BAUER-VIOLAS ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Cécile Chaduteau-Monplaisir</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:354873.20121219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 14 décembre 2011 et 14 mars 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour le département de l'Aveyron, représenté par le président du conseil général ; le département demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10BX02465 du 13 octobre 2011 par lequel la cour administrative d'appel de Bordeaux a annulé, d'une part, le jugement du 18 juin 2010 du tribunal administratif de Toulouse en tant qu'il a statué sur les conclusions présentées par la Sarl Labhya tendant à l'annulation de la délibération du 24 octobre 2005 et de l'acte du 24 janvier 2006 et, d'autre part, la délibération du 24 octobre 2005 par laquelle son conseil général a attribué à la Saeml Aveyron Labo un contrat de délégation de service public ayant pour objet l'exploitation du laboratoire départemental d'analyses, et lui a enjoint de négocier avec la Saeml Aveyron Labo la résolution amiable du contrat de délégation de service public ou, à défaut d'y parvenir dans un délai de six mois à compter de la notification de l'arrêt, de saisir le juge du contrat ;  <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la Sarl Labhya ;<br/>
<br/>
              3°) de mettre la somme de 5 000 euros à la charge de la Sarl Labhya en application des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de commerce ; <br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Chaduteau-Monplaisir, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Peignot, Garreau, Bauer-Violas, avocat du département de l'Aveyron et de la SCP Piwnica, Molinié, avocat de la Sarl Labhya,<br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Peignot, Garreau, Bauer-Violas, avocat du département de l'Aveyron et à la SCP Piwnica, Molinié, avocat de la Sarl Labhya ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le département de l'Aveyron a, par une délibération du 24 octobre 2005, approuvé le choix de la société d'économie mixte locale Aveyron Labo comme délégataire de la gestion du laboratoire départemental d'analyses ; que, par un courrier du 24 janvier 2006, postérieur à la signature de la convention de délégation avec la société d'économie mixte, le département a informé la Sarl Labhya, qui avait répondu à l'appel à candidature pour cette délégation, du rejet de son offre et des motifs de ce rejet ; que, par l'arrêt attaqué du 13 octobre 2011, la cour administrative d'appel de Bordeaux, après avoir annulé le jugement par lequel le tribunal administratif de Toulouse avait rejeté la demande de la Sarl Labhya, a notamment annulé la délibération du 24 octobre 2005 ; <br/>
<br/>
              2. Considérant, en premier lieu, qu'il ressort des pièces du dossier soumis aux juges du fond que le département de l'Aveyron n'a répondu que le 24 janvier 2006 aux demandes de la Sarl Labhya relatives au rejet de son offre et à l'identité de l'attributaire et que le 22 mars 2006, date à laquelle la Sarl Labhya a saisi le tribunal administratif de Toulouse, la délibération du 24 octobre 2005 n'avait pas encore fait l'objet d'une publication régulière, intervenue le 14 juin 2006 ; que, par suite et contrairement à ce que soutient le département de l'Aveyron, la cour n'a pas méconnu la portée des conclusions de la Sarl Labhya, laquelle les a précisées par un mémoire enregistré le 20 avril 2007 en désignant explicitement la délibération du 24 octobre 2005 comme objet de sa demande, en retenant que ces conclusions devaient être regardées comme contestant, dès le 22 mars 2006, la délibération attribuant la délégation de service public à la société d'économie mixte locale Aveyron Labo ; qu'elle n'a pas commis d'erreur de droit en jugeant que ces conclusions étaient recevables ;<br/>
<br/>
              3. Considérant, en second lieu, que, d'une part, aux termes de l'article <br/>
L. 1411-1 du code général des collectivités territoriales : " (...) Les délégations de service public des personnes morales de droit public relevant du présent code sont soumises par l'autorité délégante à une procédure de publicité permettant la présentation de plusieurs offres concurrentes (...). Les garanties professionnelles sont appréciées notamment dans la personne des associés et au vu des garanties professionnelles réunies en son sein. Les sociétés en cours de constitution ou nouvellement créées peuvent être admises à présenter une offre dans les mêmes conditions que les sociétés existantes. / La commission mentionnée à l'article L. 1411-5 dresse la liste des candidats admis à présenter une offre après examen de leurs garanties professionnelles et financières (...) " ; que, d'autre part, selon les articles L. 1522-1 et L. 1522-2 du même code, les collectivités territoriales peuvent créer des sociétés d'économie mixte, revêtant la forme de sociétés anonymes en principe régies par le livre II du code de commerce, dans lesquelles elles détiennent séparément ou à plusieurs, plus de la moitié du capital, la participation des actionnaires autres que les collectivités territoriales et leurs groupements ne pouvant être inférieure à 15 % du capital social ; <br/>
<br/>
              4. Considérant que, pour refuser de regarder la société d'économie mixte locale Aveyron Labo comme étant en cours de constitution et susceptible comme telle de présenter une candidature à l'attribution de la délégation de service public du laboratoire départemental d'analyses dont la recevabilité pouvait être appréciée au travers des garanties apportées par ses futurs associés, la cour administrative d'appel de Bordeaux a relevé que si, au 28 février 2005, date limite de réception des candidatures, le département avait approuvé le principe de sa création et adopté des projets de statuts, l'ensemble des personnes sollicitées pour entrer au capital n'avaient, à cette date, ni donné leur accord de principe, ni fixé le montant de leur éventuelle participation ; <br/>
<br/>
              5. Considérant que, pour porter cette appréciation sur la recevabilité de cette candidature, la cour a pu se fonder sur l'absence, à la date limite d'examen des candidatures, d'informations certaines et précises sur la participation au capital de la future société, même en présence de garanties résultant de la volonté manifestée par le département de créer la société et alors même qu'en vertu de l'article L. 210-6 du code de commerce, les personnes agissant au nom d'une société en cours de formation peuvent prendre des engagements susceptibles d'être ensuite repris par la société constituée ; que, ce faisant, la cour n'a pas commis d'erreur de qualification juridique ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que le pourvoi du département de l'Aveyron doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à sa charge la somme de 3 000 euros à verser, au même titre, à la Sarl Labhya ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du département de l'Aveyron est rejeté. <br/>
Article 2 : Le département de l'Aveyron versera la somme de 3 000 euros à la Sarl Labhya en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au département de l'Aveyron et à la Sarl Labhya. <br/>
Copie en sera adressée pour information à la Saeml Aveyron Labo.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-01-06-02 COLLECTIVITÉS TERRITORIALES. DISPOSITIONS GÉNÉRALES. DISPOSITIONS ÉCONOMIQUES. SOCIÉTÉS D'ÉCONOMIE MIXTE LOCALES. - POSSIBILITÉ, POUR LES SOCIÉTÉS EN COURS DE CONSTITUTION, DE PRÉSENTER UNE OFFRE POUR UNE DÉLÉGATION DE SERVICE PUBLIC (ART. L. 1411-1 DU CGCT) - NOTION DE SOCIÉTÉ EN COURS DE CONSTITUTION - ABSENCE D'INFORMATIONS CERTAINES ET PRÉCISES SUR LA PARTICIPATION AU CAPITAL D'UNE FUTURE SEM - EXCLUSION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-01-03-03 MARCHÉS ET CONTRATS ADMINISTRATIFS. NOTION DE CONTRAT ADMINISTRATIF. DIVERSES SORTES DE CONTRATS. DÉLÉGATIONS DE SERVICE PUBLIC. - POSSIBILITÉ, POUR LES SOCIÉTÉS EN COURS DE CONSTITUTION, DE PRÉSENTER UNE OFFRE POUR UNE DÉLÉGATION DE SERVICE PUBLIC (ART. L. 1411-1 DU CGCT) - NOTION DE SOCIÉTÉ EN COURS DE CONSTITUTION - ABSENCE D'INFORMATIONS CERTAINES ET PRÉCISES SUR LA PARTICIPATION AU CAPITAL D'UNE FUTURE SEM - EXCLUSION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">39-02-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. QUALITÉ POUR CONTRACTER. - POSSIBILITÉ, POUR LES SOCIÉTÉS EN COURS DE CONSTITUTION, DE PRÉSENTER UNE OFFRE POUR UNE DÉLÉGATION DE SERVICE PUBLIC (ART. L. 1411-1 DU CGCT) - NOTION DE SOCIÉTÉ EN COURS DE CONSTITUTION - ABSENCE D'INFORMATIONS CERTAINES ET PRÉCISES SUR LA PARTICIPATION AU CAPITAL D'UNE FUTURE SEM - EXCLUSION.
</SCT>
<ANA ID="9A"> 135-01-06-02 L'article L. 1411-1 du code général des collectivités territoriales (CGCT) prévoit que les sociétés en cours de constitution ou nouvellement créées peuvent être admises à présenter une offre dans les mêmes conditions que les sociétés existantes.... ...Pour refuser de regarder une société d'économie mixte (SEM) comme en cours de constitution et comme recevable de ce fait à présenter une offre, le juge peut se fonder sur l'absence, à la date limite d'examen des candidatures, d'informations certaines et précises sur la participation au capital de la future société, même en présence de garanties résultant de la volonté manifestée par le département de créer la société et alors même qu'en vertu de l'article L. 210-6 du code de commerce, les personnes agissant au nom d'une société en cours de formation peuvent prendre des engagements susceptibles d'être ensuite repris par la société constituée.</ANA>
<ANA ID="9B"> 39-01-03-03 L'article L. 1411-1 du code général des collectivités territoriales (CGCT) prévoit que les sociétés en cours de constitution ou nouvellement créées peuvent être admises à présenter une offre dans les mêmes conditions que les sociétés existantes.... ...Pour refuser de regarder une société d'économie mixte (SEM) comme en cours de constitution et comme recevable de ce fait à présenter une offre, le juge peut se fonder sur l'absence, à la date limite d'examen des candidatures, d'informations certaines et précises sur la participation au capital de la future société, même en présence de garanties résultant de la volonté manifestée par le département de créer la société et alors même qu'en vertu de l'article L. 210-6 du code de commerce, les personnes agissant au nom d'une société en cours de formation peuvent prendre des engagements susceptibles d'être ensuite repris par la société constituée.</ANA>
<ANA ID="9C"> 39-02-01 L'article L. 1411-1 du code général des collectivités territoriales (CGCT) prévoit que les sociétés en cours de constitution ou nouvellement créées peuvent être admises à présenter une offre dans les mêmes conditions que les sociétés existantes.... ...Pour refuser de regarder une société d'économie mixte (SEM) comme en cours de constitution et comme recevable de ce fait à présenter une offre, le juge peut se fonder sur l'absence, à la date limite d'examen des candidatures, d'informations certaines et précises sur la participation au capital de la future société, même en présence de garanties résultant de la volonté manifestée par le département de créer la société et alors même qu'en vertu de l'article L. 210-6 du code de commerce, les personnes agissant au nom d'une société en cours de formation peuvent prendre des engagements susceptibles d'être ensuite repris par la société constituée.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
