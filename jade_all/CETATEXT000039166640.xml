<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039166640</ID>
<ANCIEN_ID>JG_L_2019_10_000000429145</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/16/66/CETATEXT000039166640.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 01/10/2019, 429145, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429145</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Charles-Emmanuel Airy</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:429145.20191001</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance du 14 mars 2019, enregistrée au secrétariat de la section du contentieux du Conseil d'Etat le 26 mars 2019, le président du tribunal administratif de Paris a, sur le fondement de l'article R. 351-2 du code de justice administrative, transmis au Conseil d'Etat la requête du syndicat Justice CGC, enregistrée le 19 février 2019 au greffe de ce tribunal.<br/>
<br/>
              Par cette requête, le Syndicat Justice CGC demande au conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté de la garde des sceaux, ministre de la justice, du 20 décembre 2018 établissant la liste des organisations syndicales aptes à désigner des représentants au comité technique spécial de service placé auprès du directeur des services judiciaires et fixant le nombre de sièges de titulaires et de suppléants et, par voie de conséquence, l'arrêté de la garde des sceaux, ministre de la justice, du 8 janvier 2019 portant nomination des représentants du personnel au comité technique spécial de service placé auprès du directeur des services judiciaires ;<br/>
<br/>
              2°) d'enjoindre à la garde des sceaux, ministre de la justice, d'organiser une nouvelle consultation en application des dispositions générales du décret n° 2011-184 du 15 février 2011 dans les six semaines suivant la décision à intervenir ;<br/>
<br/>
               3°) de condamner l'Etat à lui verser la somme de 3 000 euros au titre de dommages et intérêts. <br/>
<br/>
              Il soutient que l'arrêté du 20 décembre 2018 est illégal du fait de l'illégalité de l'arrêté du 7 juin 2011 sur le fondement duquel il est pris, dès lors que ce dernier méconnaît, en l'érigeant en principe alors qu'elle n'est possible que lorsque l'intérêt du service le justifie, la faculté prévue par le décret du 15 février 2011 de déroger au scrutin de liste ou de sigle en additionnant, pour la composition d'un comité technique de périmètre plus large, les suffrages obtenus pour la composition de comités techniques de périmètre plus restreint.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 73-634 du 13 juillet 1983 ;<br/>
              - le décret n° 2011-184 du 15 février 2011 ;<br/>
              - l'arrêté du 7 juin 2011 relatif à la création d'un comité technique spécial de service placé auprès du directeur des services judiciaires ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles-Emmanuel Airy, auditeur,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
<br/>
              1. Le syndicat Justice CGC demande l'annulation pour excès de pouvoir de l'arrêté de la garde des sceaux, ministre de la justice, du 20 décembre 2018 établissant la liste des organisations syndicales aptes à désigner des représentants au comité technique spécial de service placé auprès du directeur des services judiciaires et fixant le nombre de sièges de titulaires et de suppléants et, par voie de conséquence, de l'arrêté de la garde des sceaux, ministre de la justice, du 8 janvier 2019 portant nomination des représentants du personnel au comité technique spécial de service placé auprès du directeur des services judiciaires.<br/>
<br/>
<br/>
              2. Aux termes des dispositions du 2° du III de l'article 15 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat : " III.- (...) Les représentants du personnel siégeant aux comités techniques sont élus au scrutin de liste avec représentation proportionnelle dans les conditions définies à l'article 9 bis de la loi n° 83-634 du 13 juillet 1983 précitée. / Par dérogation aux dispositions de l'alinéa précédent : / (...) 2° Les représentants du personnel siégeant aux comités techniques autres que les comités techniques ministériels et les comités techniques de proximité peuvent, lorsque des circonstances particulières le justifient, être désignés, selon le cas, par référence au nombre de voix obtenues aux élections de ces comités techniques ministériels ou de proximité ou après une consultation du personnel ". Ces dispositions ont été précisées par celles de l'article 14 du décret du 15 février 2011 relatif aux comités techniques dans les administrations et les établissements publics de l'Etat, en vertu desquelles il peut être procédé, " lorsque l'intérêt du service le justifie notamment afin de tenir compte de la difficulté d'organiser des opérations électorales communes à plusieurs départements ministériels ou à plusieurs services, et sous réserve que l'ensemble des suffrages correspondant au périmètre du comité technique à composer puisse être pris en compte, (...) pour la composition d'un comité technique de périmètre plus large, par addition des suffrages obtenus pour la composition de comités techniques de périmètre plus restreint ". <br/>
<br/>
              3. L'article 1er de l'arrêté du 7 juin 2011 relatif à la création d'un comité technique spécial de service placé auprès du directeur des services judiciaires prévoit que ce comité a compétence pour connaître de toutes les questions concernant l'ensemble des services déconcentrés placés sous son autorité.  Ce comité technique spécial constitue ainsi une instance de concertation chargée de donner son avis sur les questions et projets de textes relatifs à l'organisation et au fonctionnement des services et du personnel du service public judiciaire. Conformément à l'article 2 de l'arrêté du 7 juin 2011, la composition du comité technique spécial de service résulte de l'addition des résultats obtenus lors des élections organisées pour la composition des comités techniques placés auprès du premier président de la Cour de cassation, des premiers présidents de cour d'appel et du directeur de l'école nationale des greffes, agrégeant ainsi un total de 38 scrutins. Eu égard au nombre d'agents concernés par le comité technique spécial et à leur dispersion dans les différentes entités du service public judiciaire sur l'ensemble du territoire national, les modalités d'organisation d'une élection, au scrutin de liste avec représentation proportionnelle, des représentants du personnel siégeant au sein de ce comité soulèvent des difficultés qui permettent de regarder comme remplie la condition posée par les dispositions précitées permettant le recours à une modalité dérogatoire de désignation. Par suite, le moyen tiré de ce que l'arrêté attaqué, de même que l'arrêté du 7 juin 2011 sur la base duquel il a été pris, seraient intervenus en méconnaissance des dispositions de l'article 14 précité du décret du 15 février 2011 doit être écarté. Il en résulte que les conclusions tendant à l'annulation de l'arrêté du 20 décembre 2018 ne peuvent qu'être rejetées.<br/>
<br/>
              4. Le syndicat requérant n'est, pour les mêmes motifs, pas davantage fondé à soutenir que l'arrêté du 8 janvier 2019 serait illégal par voie de conséquence de l'illégalité de l'arrêté du 20 décembre 2018 et à en demander l'annulation.<br/>
<br/>
              5. La présente décision, qui rejette les conclusions à fin d'annulation de l'arrêté du 20 décembre 2018, n'implique aucune mesure d'exécution. Par suite, les conclusions tendant à ce qu'il soit enjoint au ministre de procéder à une nouvelle consultation en vue de la désignation des représentants des organisations syndicales ne peuvent qu'être rejetées. <br/>
<br/>
              6. Enfin, en l'absence de toute illégalité fautive de l'Etat, les conclusions tendant à la condamnation de ce dernier à verser au syndicat requérant une somme à titre de " dommages et intérêts " doivent, en tout état de cause, être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête du Syndicat Justice CGC est rejetée.<br/>
Article 2 : La présente décision sera notifiée au Syndicat Justice CGC et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
