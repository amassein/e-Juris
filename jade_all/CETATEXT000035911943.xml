<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035911943</ID>
<ANCIEN_ID>JG_L_2017_10_000000413277</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/91/19/CETATEXT000035911943.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 25/10/2017, 413277, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-10-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413277</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Vincent Uher</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:413277.20171025</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société à responsabilité limitée La Maison autonettoyante Orléans, à l'appui de sa demande tendant à la décharge de l'amende qui lui a été infligée au titre des années 2013 et 2014 en application des dispositions du III de l'article 1736 du code général des impôts, a produit un mémoire, enregistré le 26 juin 2017 au greffe du tribunal administratif d'Orléans, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lequel elle soulève une question prioritaire de constitutionnalité.<br/>
<br/>
              Par une ordonnance n° 1702045 du 10 août 2017, enregistrée le même jour au secrétariat du contentieux du Conseil d'Etat, le président de la 3ème chambre de ce tribunal, avant qu'il soit statué sur la demande de la société La Maison autonettoyante Orléans, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du III de l'article 1736 du code général des impôts. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des impôts ;<br/>
              - l'ordonnance n° 2005-1512  du 7 décembre 2005 ;<br/>
              - l'ordonnance n° 2010-420 du 27 avril 2010 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Uher, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société La Maison autonettoyante Orléans.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement de circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. D'une part, aux termes du III de l'article 1736 du code général des impôts, dans sa rédaction issue de l'ordonnance du 7 décembre 2005 relative à des mesures de simplification en matière fiscale et à l'harmonisation et l'aménagement du régime des pénalités : " Entraîne l'application d'une amende égale à 5 % des sommes non déclarées le non-respect des obligations prévues par les articles 87, 87 A, 88 et 241 ". Aux termes de l'article 87 du même code dans sa rédaction issue de l'ordonnance du 27 avril 2010 portant adaptation de dispositions résultant de la fusion de la direction générale des impôts et de la direction générale de la comptabilité publique : " Toute personne physique ou morale versant des traitements, émoluments, salaires ou rétributions imposables est tenue de remettre dans le courant du mois de janvier de chaque année, sauf application de l'article 87 A, à l'autorité compétente de l'Etat du lieu de son domicile ou du siège de l'établissement ou du bureau qui en a effectué le paiement, une déclaration dont le contenu est fixé par décret. / Cette déclaration doit, en outre, faire ressortir distinctement, pour chaque bénéficiaire appartenant au personnel dirigeant ou aux cadres, le montant des indemnités pour frais d'emploi qu'il a perçues ainsi que le montant des frais de représentation, des frais de déplacement, des frais de mission et des autres frais professionnels qui lui ont été alloués ou remboursés au cours de l'année précédente (...) ". <br/>
<br/>
              3. D'autre part, aux termes de l'article 81 du code général des impôts : " Sont affranchis de l'impôt : / 1° Les allocations spéciales destinées à couvrir les frais inhérents à la fonction ou à l'emploi et effectivement utilisées conformément à leur objet (...) ". Aux termes de l'article 83 du même code : " Le montant net du revenu imposable est déterminé en déduisant du montant brut des sommes payées et des avantages en argent ou en nature accordés : (...) / 3° Les frais inhérents à la fonction ou à l'emploi lorsqu'ils ne sont pas couverts par des allocations spéciales (...) ".<br/>
<br/>
              4. La société La Maison autonettoyante Orléans soutient que les dispositions précitées du III de l'article 1736 du code général des impôts méconnaissent le principe de proportionnalité des peines, énoncé à l'article 8 de la Déclaration des droits de l'homme et du citoyen, notamment en ce que l'amende de 5 % des sommes non déclarées qu'elles instituent s'applique aux allocations spéciales destinées à couvrir les frais inhérents à la fonction ou à l'emploi alors que ces dernières sont affranchies d'impôt sur le revenu.<br/>
<br/>
              5. Les sommes à déclarer en vertu de l'article 87 précité intègrent le montant des indemnités pour frais d'emploi et des remboursements de frais professionnels octroyés par l'employeur. Ces indemnités et remboursements constituent des allocations spéciales destinées à couvrir les frais inhérents à la fonction ou à l'emploi, lesquelles sont affranchies de l'impôt sur le revenu en vertu de l'article 81 précité sous réserve d'être effectivement utilisées conformément à leur objet. Les bénéficiaires de ces allocations pour frais d'emploi ne sont pas tenus de les intégrer dans leur déclaration de revenus lorsqu'elles remplissent les conditions pour être exonérées sous réserve, en cas d'option pour la déduction de leurs frais professionnels réels, de ne pas déduire les frais couverts par ces allocations. Pour autant, la transmission à l'administration fiscale des montants des allocations pour frais d'emploi la met en situation de contrôler que ces sommes remplissent les conditions pour être exonérées et ne font pas l'objet, en cas d'option pour la déduction des frais professionnels réels, d'une double déduction.<br/>
<br/>
              6. En réprimant notamment le manquement à l'obligation, prévue à l'article 87 précité, de transmettre à l'administration fiscale des informations relatives aux revenus salariaux versés à d'autres contribuables, la disposition contestée sanctionne le non-respect d'une obligation déclarative permettant à cette administration de procéder aux recoupements nécessaires au contrôle du respect, par les bénéficiaires des versements mentionnés à l'article 87, de leurs obligations fiscales, ainsi que de pré-remplir les déclarations de revenus de ces derniers.<br/>
<br/>
              7. En fixant l'amende encourue par l'auteur des versements en proportion des sommes versées, le législateur qui a entendu assurer l'efficacité des contrôles à sa disposition a poursuivi un but de lutte contre la fraude fiscale, qui constitue un objectif de valeur constitutionnelle. <br/>
<br/>
              8. Il a proportionné la sanction à la gravité des manquements réprimés, laquelle est appréciée à raison de l'importance des sommes non déclarées, quand bien même le Trésor public ne subirait aucun préjudice financier effectif. <br/>
<br/>
              9. Il résulte de ce qui précède que la question soulevée, qui n'est pas nouvelle, est dépourvue de caractère sérieux. Par suite, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée.<br/>
<br/>
<br/>
<br/>                         D E C I D E :<br/>
                                       --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le président de la 3ème chambre du tribunal administratif d'Orléans.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société à responsabilité limitée La Maison autonettoyante Orléans, au ministre de l'action et des comptes publics et au président du tribunal administratif d'Orléans.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
