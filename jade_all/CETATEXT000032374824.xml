<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032374824</ID>
<ANCIEN_ID>JG_L_2016_04_000000392287</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/37/48/CETATEXT000032374824.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 04/04/2016, 392287, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-04-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392287</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; COPPER-ROYER ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Camille Pascal</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:392287.20160404</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Caen la condamnation solidaire du département du Calvados et de l'entreprise Delente à lui verser une indemnité de 7 522 euros, assortie des intérêts à taux légal, en réparation du préjudice résultant d'un accident survenu le 24 janvier 2011 à l'intérieur du collège Jean Monnet de Ouistreham. Par un jugement n° 1401158 du 9 avril 2015, le tribunal administratif de Caen a rejeté cette demande.<br/>
<br/>
              Par une ordonnance n° 15NT01733 du 22 juillet 2015, le président de la cour administrative d'appel de Nantes a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi formé par Mme B...contre ce jugement.<br/>
<br/>
              Par ce pourvoi, enregistré le 5 juin 2015 au greffe de la cour administrative d'appel, et par un mémoire complémentaire, enregistré au secrétariat du contentieux du Conseil d'Etat le 9 septembre 2015, Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à la demande présentée au tribunal administratif de Caen ; <br/>
<br/>
              3°) de mettre à la charge du département du Calvados et de l'entreprise Delente la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Camille Pascal, conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
     La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de MmeB..., à Me Copper-Royer, avocat du Conseil général du Calvados et à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de l'entreprise Delente ; <br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des énonciations du jugement attaqué que Mme B... a été victime, le 24 janvier 2011, d'une chute dans un regard dont le couvercle avait été retiré, dans l'enceinte du collège Jean Monnet de Ouistreham où elle enseigne ; qu'elle se pourvoit en cassation contre le jugement du tribunal administratif de Caen du 9 avril 2015 qui a rejeté la demande indemnitaire qu'elle avait formée pour rechercher la responsabilité solidaire du département du Calvados et de l'entreprise Delente ;<br/>
<br/>
              2.	Considérant que, pour rejeter la demande dont il était saisi, le tribunal administratif a retenu que si le département ne rapportait pas la preuve d'une signalisation du danger et d'un entretien normal de l'ouvrage, l'excavation à l'origine de la chute, située en bordure d'un mur de teinte claire, était visible, à l'heure de l'accident, par une personne normalement attentive et susceptible de connaître les lieux situés à proximité de la salle des professeurs du collège où la victime enseigne ; qu'il en a déduit que l'imprudence et l'inattention de l'intéressée étaient de nature à exonérer le département et l'entreprise de toute responsabilité dans l'accidence en cause ; <br/>
<br/>
              3.	Considérant, toutefois, ainsi que l'a relevé le tribunal administratif, qu'aucune signalisation n'avertissait les personnels de l'établissement susceptibles de circuler sur le chemin dallé en cause, passage réservé aux personnels de l'établissement, du danger que représentait le regard resté ouvert à la suite des travaux entrepris par l'entreprise Delente ; qu'il ne ressort pas des pièces du dossier soumis au juge du fond qu'un dispositif de protection aurait été mis en place ; que la circonstance que Mme B...connaissait les lieux n'est pas de nature, s'agissant d'un désordre dont l'intéressée n'était pas censée avoir connaissance, à caractériser une imprudence fautive ; qu'une simple inattention, à la supposer établie, n'est pas davantage, dans des circonstances comme celles de l'espèce, constitutive d'une faute ; que, dans ces conditions, le tribunal administratif de Caen a inexactement qualifié les faits de l'espèce en jugeant que l'accident était imputable à une faute commise par Mme B...; <br/>
<br/>
              4.	Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que Mme B...est fondée à demander l'annulation du jugement qu'elle attaque ;<br/>
<br/>
              5.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du département du Calvados et de l'entreprise Delente la somme de 1 500 euros chacun au titre de l'article L. 761-1 du code de justice administrative ; que ces dispositions font, en revanche, obstacle à ce que soient mises à la charge de MmeB..., qui n'est pas la partie perdante dans la présente instance, les sommes demandées au même titre par le département du Calvados et l'entreprise Delente ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Caen du 9 avril 2015 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée devant le tribunal administratif de Caen.<br/>
Article 3 : Une somme de 1 500 euros est mise à la charge tant du département du Calvados que de l'entreprise Delente, à verser à Mme B...au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées par le département du Calvados et l'entreprise Delente au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à Mme A...B..., au département du Calvados et à l'entreprise Delente.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
