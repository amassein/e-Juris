<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000021996072</ID>
<ANCIEN_ID>JG_L_2010_03_000000316721</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/21/99/60/CETATEXT000021996072.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 17/03/2010, 316721, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2010-03-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>316721</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Arrighi de Casanova</PRESIDENT>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT ; SCP LYON-CAEN, FABIANI, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Nicolas  Polge</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Dacosta Bertrand</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 2 juin et 2 septembre 2008 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SOCIETE AUXILIAIRE DE PARCS, dont le siège est 94 rue de Provence à Paris (75009), représentée par ses représentants légaux ; la SOCIETE AUXILIAIRE DE PARCS demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt du 1er avril 2008, rectifié par l'arrêt du 11 septembre 2008, par lequel la cour administrative d'appel de Bordeaux a ramené la condamnation mise à sa charge par l'article 1er du jugement du tribunal administratif de Limoges du 16 décembre 2004 à 215 212,56 euros avec des intérêts au taux légal, et a réformé ce jugement en ce qu'il a de contraire ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel et de rejeter l'appel incident de la commune de Brive-la-Gaillarde ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Brive-la-Gaillarde le versement d'une somme de 12 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Polge, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Potier de la Varde, Buk Lament, avocat de la SOCIETE AUXILIAIRE DE PARCS et de la SCP Lyon-Caen, Fabiani, Thiriez, avocat de la commune de Brive-la-Gaillarde, <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Potier de la Varde, Buk Lament, avocat de la SOCIETE AUXILIAIRE DE PARCS et à la SCP Lyon-Caen, Fabiani, Thiriez, avocat de la commune de Brive-la-Gaillarde ; <br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis au juge du fond que, par des conventions du 21 décembre 1992, la commune de Brive-la Gaillarde a délégué à la SOCIETE AUXILIAIRE DE PARCS (SAP), pour une durée de trente ans, la gestion du service public du stationnement payant sur voirie et l'exploitation de deux parcs de stationnement publics souterrains ; qu'à ces conventions, déférées au tribunal administratif de Limoges par le préfet de la Corrèze, ont été substitués de nouveaux contrats ayant le même objet, signés le 28 octobre 1993, d'une durée ramenée à vingt-neuf ans et deux mois et qui ont à leur tour été contestés par l'autorité préfectorale ; que le tribunal administratif a annulé l'ensemble de ces contrats successifs par un jugement du 30 mai 1996, devenu définitif ; que ce même tribunal, par un jugement du 16 décembre 2004, a rejeté la demande de la SAP tendant à la condamnation de la commune à l'indemniser des préjudices subis du fait de l'exécution de ces conventions jusqu'au 15 juillet 1996 et l'a elle-même condamnée à verser à la commune de Brive-la-Gaillarde la somme de 2 139 797,67 euros au titre du remboursement des rémunérations acquittées par la commune à son délégataire en exécution des conventions ; que, par l'arrêt attaqué du 1er avril 2008, rectifié par un arrêt du 11 septembre 2008, la cour administrative d'appel de Bordeaux a ramené cette somme à 215 112,56 euros ; <br/>
<br/>
              Considérant que le cocontractant de l'administration dont le contrat est entaché de nullité ou annulé peut prétendre, sur un terrain quasi-contractuel, non pas à ce que les sommes versées en exécution du contrat fassent l'objet d'une répétition, mais au remboursement de celles de ses dépenses qui ont été utiles à la collectivité envers laquelle il s'était engagé ; que, dans le cas où le contrat en cause est une concession de service public, il peut notamment, à ce titre, demander à être indemnisé de la valeur non amortie, à la date à laquelle les biens nécessaires à l'exploitation du service font retour à l'administration, des dépenses d'investissement qu'il a consenties, ainsi que du déficit qu'il a, le cas échéant, supporté à raison de cette exploitation, compte tenu notamment des dotations aux amortissements, pour autant toutefois qu'il soit établi que ce déficit était effectivement nécessaire, dans le cadre d'une gestion normale, à la bonne exécution du service ; que, dans le cas où la nullité du contrat résulte d'une faute de l'administration, le cocontractant de celle-ci peut en outre, sous réserve du partage de responsabilités découlant le cas échéant de ses propres fautes, prétendre à la réparation du dommage imputable à la faute de l'administration ; qu'à ce titre il peut demander le paiement des sommes correspondant aux autres dépenses exposées par lui pour l'exécution du contrat et aux gains dont il a été effectivement privé par sa nullité, notamment du bénéfice auquel il pouvait prétendre, si toutefois l'indemnité à laquelle il a droit sur un terrain quasi-contractuel ne lui assure pas déjà une rémunération supérieure à celle que l'exécution du contrat lui aurait procurée ;<br/>
<br/>
              Considérant que, par l'arrêt attaqué, la cour administrative d'appel de Bordeaux a jugé que l'annulation d'une convention de délégation de service public implique en outre la possibilité, pour chacune des parties, de demander, sur le fondement de la responsabilité quasi-contractuelle, le remboursement des sommes qu'elle a versées en exécution de cette convention et qu'en l'espèce, il y avait lieu au remboursement à la commune des recettes d'exploitation perçues par la société requérante ; que dans le cadre d'un tel raisonnement, que cette dernière ne conteste pas en cassation, la cour administrative d'appel a commis une erreur de droit en ne déduisant pas du montant de ces recettes, pour déterminer le solde des obligations réciproques des parties, celui des dotations aux amortissements constituées par cette société pour financer les investissements ; que par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société requérante est fondée à demander l'annulation de l'arrêt du 1er avril 2008, rectifié par l'arrêt du 11 septembre 2008 ;<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit au conclusions présentées à ce titre par la commune de Brive-la-Gaillarde ; que dans les circonstances de l'espèce, il n'y a pas lieu de faire droit à celles que la SOCIETE AUXILIAIRE DE PARCS présente au même titre ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 1er avril 2008 de la cour administrative d'appel de Bordeaux, rectifié par l'arrêt du 11 septembre 2008, est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Bordeaux.<br/>
<br/>
Article 3 : Le surplus des conclusions du pourvoi et les conclusions de la commune de Brive-la-Gaillarde présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la SOCIETE AUXILIAIRE DE PARCS et à la commune de Brive-la-Gaillarde. <br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
