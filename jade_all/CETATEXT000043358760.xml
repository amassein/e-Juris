<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043358760</ID>
<ANCIEN_ID>JG_L_2021_03_000000450475</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/35/87/CETATEXT000043358760.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 22/03/2021, 450475, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450475</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:450475.20210322</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A... B... a demandé au juge des référés du tribunal administratif de Cergy-Pontoise, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au préfet du Val-d'Oise de la convoquer afin de lui délivrer le récépissé de demande de titre de séjour prévu à l'article L. 311-5-1 du code de l'entrée et du séjour des étrangers et du droit d'asile dans un délai de quarante-huit heures a` compter de l'ordonnance à intervenir. Par une ordonnance n° 2102888 du 5 mars 2021, le juge des référés du tribunal administratif de Cergy Pontoise a rejeté sa demande. <br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 8 et 16 mars 2021 au secrétariat du contentieux du Conseil d'Etat, Mme Gouman demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de faire droit à ses conclusions de première instance ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - sa requête est recevable ;<br/>
              - l'ordonnance du juge des référés de première instance est entachée d'une erreur de droit en ce que le juge des référés a méconnu son office dès lors que, d'une part, il a jugé que sa situation ne relevait pas d'une extrême urgence alors que les dispositions de l'article L. 521-2 du code de justice administrative ne mentionnent qu'une condition d'urgence et, d'autre part, il a refusé de faire droit à sa demande dans un délai de quarante-huit heures, tout en demandant au préfet du Val-d'Oise de prendre les mesures nécessaires dans les meilleurs délais, ce qui est un constat implicite de l'atteinte grave et manifestement illégale à son droit d'asile ;<br/>
              - la condition d'urgence est satisfaite dès lors que, en premier lieu, l'absence de récépissé de demande de titre de séjour la maintient dans une situation administrative irrégulière, en deuxième lieu, elle se trouve dans l'impossibilité d'exercer une activité professionnelle, d'ouvrir ses droits à une assurance-maladie et se trouve sans ressource depuis le 12 novembre 2019 et, en dernier lieu, elle vit sous la menace d'une mesure de rétention administrative ; <br/>
              - il est porté une atteinte grave et manifestement illégale au droit d'asile.<br/>
<br/>
              Par un mémoire en intervention, enregistré le 15 mars 2021, l'association La Cimade conclut à ce qu'il soit fait droit aux conclusions de la requête. Elle soutient que, d'une part, son intervention est recevable et, d'autre part, la situation de Mme Gouman résulte de l'illégalité des instructions ou des directives du ministre de l'intérieur, qui préconisent la dématérialisation des prises de rendez-vous et le recours aux téléservices pour l'instruction des demandes, portant une atteinte grave et manifestement illégale aux principes d'égalité d'accès et de continuité du service public. Elle s'associe également aux autres moyens de la requête.<br/>
<br/>
              Par un mémoire en défense, enregistré le 15 mars 2021, le ministre de l'intérieur conclut au non-lieu à statuer sur les conclusions aux fins d'annulation et d'injonction de la requérante. Il soutient que, par courrier du 15 mars 2021, une convocation à la préfecture du Val-d'Oise pour le 29 mars 2021 a été adressée à la requérante en vue de lui délivrer le récépissé de demande de titre de séjour prévu par l'article L. 311-5-1 du code de l'entrée et du séjour des étrangers et du droit d'asile.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, Mme Gouman et l'association La Cimade, et d'autre part, le ministre de l'intérieur ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 18 mars 2021, à 11 heures : <br/>
<br/>
              - Me Froger, avocat au Conseil d'Etat et à la Cour de cassation, avocat de Mme Gouman ;<br/>
<br/>
              - Mme Gouman ;<br/>
<br/>
              - le représentant de La Cimade ;<br/>
<br/>
              - les représentantes du ministre de l'intérieur ;<br/>
<br/>
              à l'issue de laquelle le juge des référés a prononcé la clôture de l'instruction.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". <br/>
<br/>
              2. La Cimade a un objet statutaire qui lui donne intérêt à intervenir au soutien des conclusions de Mme Gouman. Son intervention doit donc être admise<br/>
<br/>
              3. Mme Esther Gouman s'est vue reconnaître la qualité de réfugiée par une décision de l'Office français de protection des réfugiés et apatrides (OFPRA) le 27 janvier 2021. Elle a en conséquence demandé à la préfecture du Val d'Oise la délivrance d'un récépissé de titre de séjour en qualité de réfugiée. Ne parvenant pas à obtenir un rendez-vous en préfecture en vue d'obtenir un tel récépissé, elle a saisi le juge des référés du tribunal administratif de Cergy-Pontoise afin qu'il enjoigne au préfet du Val d'Oise de lui délivrer ce récépissé sous 48 heures. Par une ordonnance du 5 mars 2021, le juge des référés du tribunal administratif de Cergy-Pontoise a rejeté sa demande en estimant que la situation n'était caractérisée par aucune urgence de nature à fonder son intervention sur le fondement de l'article L. 521-2 du code de justice administrative. Mme Gouman relève appel de cette ordonnance.<br/>
<br/>
              4. Par un mémoire du 15 mars 2021, le ministre de l'intérieur a produit copie de la convocation de Mme Gouman en préfecture le 29 mars 2021 à 11.10, à fin de délivrance du récépissé du titre de séjour de réfugiée auquel elle a droit depuis le 27 janvier. Pour tardive que soit cette convocation, dont il n'a pas été contesté à l'audience que Mme Gouman l'a bien reçue, la circonstance qu'à la date de la présente ordonnance elle impose à Mme Gouman une attente de l'ordre d'une semaine n'est pas telle, même en prenant en compte la situation de précarité et d'incertitude que l'intéressée traverse depuis plusieurs mois, qu'elle constituerait l'urgence à laquelle l'article L. 521-2 du code de justice administrative conditionne l'usage des pouvoirs qu'il confère au juge des référés. Dès lors, sans qu'il soit besoin d'examiner les autres moyens de la requête, les conclusions de Mme Gouman ne peuvent qu'être rejetées.<br/>
<br/>
              5. Si Mme Gouman ne conteste pas ne pas avoir recouru au ministère d'un avocat pour relever appel de l'ordonnance qu'elle attaque, elle estime que les dispositions de l'article L. 761-1 ne font pas du ministère d'avocat la condition du versement d'une somme d'argent en application de ces dispositions. Toutefois, elle ne justifie en l'espèce d'aucune dépense qu'elle aurait engagée à l'occasion de son recours. Ses conclusions tendant à ce que l'Etat lui verse une somme d'argent sur le fondement de l'article L. 761-1 ne peuvent en conséquence qu'être rejetées.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : L'intervention de l'association La Cimade est admise.<br/>
Article 2 :  La requête de Mme Gouman est rejetée.<br/>
Article 3 : La présente ordonnance sera notifiée à Mme Esther Gouman et au ministre de l'intérieur. <br/>
Copie en sera adressée à l'association La Cimade.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
