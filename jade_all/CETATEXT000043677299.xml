<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043677299</ID>
<ANCIEN_ID>JG_L_2021_06_000000444772</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/67/72/CETATEXT000043677299.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 17/06/2021, 444772, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>444772</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>Mme Christelle Thomas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:444772.20210617</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Le Rio a demandé au juge des référés du tribunal administratif d'Orléans d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de l'arrêté du 15 juin 2020 par lequel le maire d'Olivet (Loiret) a sursis à statuer sur sa demande de permis de construire et d'enjoindre au maire de lui délivrer un permis de construire provisoire ou, à défaut, de reprendre l'instruction de sa demande. <br/>
<br/>
              Par une ordonnance n° 2002840 du 7 septembre 2020, le tribunal administratif d'Orléans, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, et un mémoire en réplique, enregistrés les 22 septembre et 7 octobre 2020 et le 19 mai 2021 au secrétariat du contentieux du Conseil d'Etat, la société Le Rio demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, d'ordonner la suspension de l'exécution de l'arrêté du 15 juin 2020 ;<br/>
<br/>
              3°) de mettre à la charge de la commune d'Olivet la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Christelle Thomas, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la Société Le Rio et à la SCP Gaschignard, avocat de la commune d'Olivet ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif d'Orléans que la société Le Rio exploitait, sur le territoire de la commune d'Olivet, une discothèque dénommée " Le Pavillon ". Un incendie l'ayant gravement endommagée dans la nuit du 2 au 3 mai 2018, le maire d'Olivet a pris un arrêté de péril imminent entraînant la fermeture de l'établissement. Pour mener à bien les travaux nécessaires à sa réouverture, la société Le Rio a présenté une demande de permis de construire, qui a recueilli un avis défavorable de l'architecte des bâtiments de France et a été rejetée par un arrêté du 4 octobre 2019 du maire d'Olivet. Le 26 novembre 2019, la société Le Rio a déposé une nouvelle demande de permis de construire à laquelle le maire a opposé un sursis à statuer par un arrêté du 15 juin 2020, pour tenir compte du nouveau plan local d'urbanisme en cours d'élaboration. La société Le Rio a saisi le juge des référés du tribunal administratif d'Orléans, sur le fondement de l'article L. 521-1 du code de justice administrative, d'une demande tendant à la suspension de l'exécution de cet arrêté. Elle se pourvoit en cassation contre l'ordonnance par laquelle le juge des référés a rejeté sa demande au motif que la condition d'urgence n'était pas remplie.<br/>
<br/>
              2. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              3. L'article L. 424-1 du code de l'urbanisme dispose que : " L'autorité compétente se prononce par arrêté sur la demande de permis (...). Il peut être sursis à statuer sur toute demande d'autorisation concernant des travaux, constructions ou installations dans les cas prévus (...) aux articles L. 153-11 (...) du présent code. (...) Le sursis à statuer doit être motivé et ne peut excéder deux ans ". Aux termes du troisième alinéa de l'article L. 153-11 du même code, l'autorité compétente chargée de la procédure d'élaboration du plan local d'urbanisme " peut décider de surseoir à statuer, dans les conditions et délai prévus à l'article L. 424-1, sur les demandes d'autorisation concernant des constructions, installations ou opérations qui seraient de nature à compromettre ou à rendre plus onéreuse l'exécution du futur plan dès lors qu'a eu lieu le débat sur les orientations générales du projet d'aménagement et de développement durable ".<br/>
<br/>
              4. L'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. La décision par laquelle l'autorité compétente sursoit à statuer sur une demande de permis de construire, en application des articles L. 153-11 et L. 424-1 du code de l'urbanisme, afin d'éviter que le projet du pétitionnaire ne compromette ou ne rende plus onéreuse l'exécution d'un futur plan local d'urbanisme en cours d'élaboration, ne crée une situation d'urgence que si le requérant justifie, en invoquant des circonstances particulières, que cette décision affecte gravement sa situation. <br/>
<br/>
              5. Il résulte des énonciations de l'ordonnance attaquée que, pour juger que la condition d'urgence n'était pas remplie, le juge des référés du tribunal administratif d'Orléans s'est fondé sur la circonstance que la cessation de l'activité de la société Le Rio ne résultait pas directement de la décision litigieuse de sursis à statuer sur sa demande de permis, mais de l'incendie de son établissement, survenu en 2018, et de la fermeture des discothèques imposée dans le contexte sanitaire de l'épidémie de COVID-19. En se fondant ainsi sur des considérations relatives à l'exploitation de l'établissement qui, compte tenu des délais nécessaires à l'obtention d'un permis de construire et à la réalisation des travaux de reconstruction du bâtiment sinistré préalables à la réouverture de l'établissement, étaient dépourvues d'incidence sur l'urgence à suspendre la décision litigieuse de sursis à statuer sur la demande de permis de construire, le juge des référés a commis une erreur de droit. Par suite, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, la société Le Rio est fondée à demander l'annulation de l'ordonnance qu'elle attaque.<br/>
<br/>
              6. Il y a eu lieu, dans les circonstances de l'espèce, de statuer sur la demande de suspension en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              7. En premier lieu, il résulte de l'instruction que la société Le Rio se trouve confrontée à d'importantes difficultés financières depuis l'incendie et la fermeture en mai 2018 de la discothèque Le Pavillon, seul établissement qu'elle exploitait et qu'elle compte réhabiliter en procédant à des travaux pour lesquels elle a sollicité un permis de construire. La décision de sursis à statuer opposée par le maire d'Olivet, qui fait obstacle à la possibilité d'obtenir une autorisation et de lancer des travaux en vue de la réouverture de l'établissement dès que la situation sanitaire le permettra, est de nature à préjudicier de manière grave et immédiate aux intérêts de cette société. Si la commune d'Olivet justifie la décision litigieuse en invoquant un projet de requalification du quartier par l'installation de placettes et la sécurisation d'un carrefour à l'emplacement de la discothèque, elle ne démontre pas l'existence d'un intérêt public s'opposant à ce que la condition de l'urgence soit en l'espèce regardée comme remplie.  <br/>
<br/>
              8. En second lieu, en l'état de l'instruction, le moyen tiré de ce que la décision de sursis à statuer méconnaît l'article L. 153-11 du code de l'urbanisme dès lors que le projet ne serait pas de nature à compromettre l'exécution du futur plan local d'urbanisme est de nature à faire naître un doute sérieux quant à la légalité de sa décision.<br/>
<br/>
              9. Pour l'application des dispositions de l'article L. 600-4-1 du code de l'urbanisme, aucun des autres moyens soulevés n'est de nature à justifier la suspension de l'exécution du sursis à statuer. <br/>
<br/>
              10. Il résulte de ce qui précède que la société requérante est fondée à demander la suspension de l'exécution de l'arrêté du 15 juin 2020 du maire d'Olivet. Il y a lieu d'enjoindre au maire d'Olivet de réexaminer la demande de permis de construire présentée par la société Le Rio dans un délai d'un mois.<br/>
<br/>
              11. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune d'Olivet la somme de 3 000 euros à verser à la société Le Rio au titre de l'article L. 761-1 du code de justice administrative. En revanche, ces dispositions font obstacle à ce qu'une somme soit mise à la charge de la société Le Rio, qui n'est pas, dans la présente instance, la partie perdante<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                                   ----------------<br/>
<br/>
Article 1er : L'ordonnance du 7 septembre 2020 du juge des référés du tribunal administratif d'Orléans est annulée.<br/>
Article 2 :  L'exécution de l'arrêté du 15 juin 2020 par lequel le maire d'Olivet a sursis à statuer sur la demande de permis de construire présentée par la société Le Rio est suspendue.<br/>
Article 3 : Il est enjoint au maire d'Olivet de réexaminer la demande de permis de construire présentée par la société Le Rio dans un délai d'un mois.<br/>
Article 4 : La commune d'Olivet versera la somme de 3 000 euros à la société Le Rio au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Les conclusions présentées par la commune d'Olivet devant le tribunal administratif d'Orléans et devant le Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à la société Le Rio et à la commune d'Olivet.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
