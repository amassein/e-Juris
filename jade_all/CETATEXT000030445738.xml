<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030445738</ID>
<ANCIEN_ID>JG_L_2015_03_000000388068</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/44/57/CETATEXT000030445738.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 11/03/2015, 388068, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-03-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388068</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2015:388068.20150311</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 17 février 2015 au secrétariat du contentieux du Conseil d'Etat, M. E...M..., l'association " Démocratie et transparence à l'université de Lyon ", M. N...P..., M. O...R..., M. G...J..., M. O...A..., Mme Q...F..., M. B...C..., M. O...K..., M. I... L... et M. D...H...demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, d'ordonner la suspension de l'exécution du décret n°2015-127 du 5 février 2015 portant approbation des statuts de la communauté d'universités et établissements "Université de Lyon".<br/>
<br/>
<br/>
              Ils soutiennent que : <br/>
              - la condition d'urgence est satisfaite au regard de l'application, au cas d'espèce, de la présomption d'urgence retenue par la décision n° 328184 du 30 décembre 2009 du Conseil d'Etat ; <br/>
              - il existe un doute sérieux quant à la légalité du décret contesté ;<br/>
              - il est issu d'une procédure irrégulière ; en effet, la composition du conseil d'administration de l'établissement qui a approuvé les statuts était irrégulière ;  son président n'avait pas été régulièrement nommé ; les comités techniques n'ont pas été régulièrement consultés sur le même projet ; <br/>
              - il méconnaît les dispositions issues de l'article 117 de la loi n° 2013-660 du 22 juillet 2013, notamment en ce qu'il fige la composition du conseil d'administration de l'établissement public de coopération scientifique, et de l'article L. 718-11 du code de l'éducation, dans leur version en vigueur à sa date de publication, ainsi que des articles L. 718-12 ; l'article L. 719-1 du même code empêchait de déléguer les règles qu'il prévoit au réglement intérieur ; <br/>
              - les statuts approuvés par le décret litigieux méconnaissent le principe d'intelligibilité de la norme et leur domaine de compétence en ce qu'il traitent des effectifs des différents collèges du conseil d'administration ;<br/>
              - les statuts sont entachés d'incompétence négative sur la définition des compétences ;<br/>
              - le CNRS n'a statué sur sa participation que postérieurement à l'approbation des statuts ;<br/>
              - les règles relatives à la présidence du conseil académique, au comité doctoral, aux biens et droits des anciens membres du PRES et à la délivrance des diplômes constituent d'autres illégalités ;<br/>
<br/>
<br/>
              Vu le décret dont la suspension de l'exécution est demandée ;<br/>
<br/>
              Vu la copie de la requête à fin d'annulation de ce décret ; <br/>
<br/>
              Par un mémoire en défense, enregistré le 2 mars 2015, la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche conclut au rejet de la requête. <br/>
<br/>
              Elle soutient que : <br/>
              - les requérants n'ont pas intérêt à agir, dès lors qu'ils ne démontrent pas en quoi le décret contesté porte atteinte aux intérêts matériels et moraux des personnels des établissements membres de la communauté d'universités et établissements (COMUE) ; <br/>
- la condition d'urgence n'est pas remplie ; <br/>
              - les moyens soulevés par les requérants ne sont pas fondés.<br/>
<br/>
              Par un mémoire en réplique, enregistré le 4 mars 2015, M. M... et autres concluent aux mêmes fins et aux mêmes moyens que ceux invoqués dans leur premier mémoire.<br/>
              Par un mémoire en défense, enregistré le 5 mars 2015, la COMUE " Université de Lyon " conclut au rejet de la requête.<br/>
<br/>
              Elle soutient que : <br/>
              - les requérants n'ont pas intérêt à agir dès lors que l'acte contesté est une mesure relative à l'organisation du service ; <br/>
              - la condition d'urgence n'est pas remplie ; <br/>
              - les moyens soulevés par les requérants ne sont pas fondés.<br/>
<br/>
              Par un nouveau mémoire en réplique, enregistré le 5 mars 2015, M. M... et autres concluent aux mêmes fins par les mêmes moyens que ceux invoqués dans leur premier mémoire ; <br/>
              Vu les pièces desquelles il ressort que la requête a été communiquée au Premier ministre ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. M...et autres et, d'autre part, le Premier ministre et la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche ainsi que la communauté d'universités et établissements " Université de Lyon " ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 6 mars 2015 14 heures 30 au cours de laquelle ont été entendus : <br/>
<br/>
              - les représentants des requérants ;<br/>
<br/>
              - les représentants de la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche ;<br/>
<br/>
              - Me Poupot, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la communauté d'universités et établissements " Université de Lyon " ; <br/>
<br/>
              - le représentant de la communauté d'universités et établissements " Université de Lyon " ; <br/>
<br/>
              et à l'issue de laquelle le juge des référés a prolongé l'instruction jusqu'au 10 mars 2015 à 14 heures ;<br/>
<br/>
              Vu les pièces, enregistrées le 9 mars 2015, produites par la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de l'éducation ;<br/>
<br/>
              - la loi n° 2013-660 du 22 juillet 2013 ; <br/>
<br/>
              - le décret n° 2007-386 du 21 mars 2007 ; <br/>
<br/>
              - le décret n° 2015-127 du 5 février 2015 ; <br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision. " ;<br/>
<br/>
              2. Considérant qu'en vertu des dispositions des articles L. 718-2 et suivants du code de l'éducation issus de la loi du 22 juillet 2013 relative à l'enseignement supérieur et à la recherche, la coopération et le regroupement des établissements d'enseignement supérieur peuvent prendre la forme d'une communauté d'universités et établissements (COMUE) ; que M. M... et autres demandent la suspension de l'exécution du décret du 5 février 2015 portant approbation des statuts de la communauté d'universités et établissements " Université de Lyon " ; que, conformément à l'article 117 de la loi du 22 juillet 2013, le pôle de recherche et d'enseignement supérieur (PRES) de Lyon est devenu, de plein droit, une COMUE à la date de publication de cette loi ; qu'il appartenait au conseil d'administration de l'ancien établissement public de coopération scientifique, devenu COMUE, en exercice à la date de publication de cette loi, d'adopter les nouveaux statuts de l'établissement pour les mettre en conformité avec la loi dans un délai d'un an ; que les nouveaux statuts, approuvés par le décret contesté, ont pour objet, d'une part, de procéder à cette mise en conformité et, d'autre part, de modifier la liste des membres de cet établissement public ; <br/>
<br/>
              3. Considérant que l'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ;<br/>
<br/>
              4. Considérant en premier lieu, que pour justifier de l'urgence qui s'attache à ce que soit ordonnée la suspension de l'exécution du décret du 5 février 2015, M. M...et autres se prévalent d'une présomption d'urgence résultant de la nature de l'établissement créé ; que, toutefois, la seule circonstance que le décret contesté entraîne un transfert de certaines compétences des membres au profit de la COMUE, lequel n'est au demeurant pas irréversible, ne saurait, à elle seule, suffire à ce que la condition d'urgence soit regardée comme remplie ; <br/>
<br/>
              5. Considérant, en deuxième lieu, qu'au titre des intérêts publics, M. M...et autres font valoir que le transfert de compétence au bénéfice de la COMUE prévu par les statuts en matière de valorisation de la recherche affecterait gravement l'activité des établissements membres en la matière ; que, toutefois, il ressort des termes mêmes des statuts que ce transfert ne concerne que les compétences en matière de valorisation de la recherche confiées à la société d'accélération du transfert de technologies (SATT) Lyon Saint Etienne, dont les actionnaires sont la Caisse des dépôts et consignations, le Centre national de la recherche scientifique (CNRS) et la COMUE " Université de Lyon " ; que ce transfert n'est pas de nature à priver les établissements membres d'autres compétences en matière de valorisation de la recherche ; qu'en outre, il ressort des pièces du dossier que le maintien de l'activité de la SATT, liée à la mise en oeuvre au niveau national du programme d'investissements d'avenir, correspond à une exigence d'intérêt général qui serait compromise par la suspension du décret contesté ; que les effets du décret contesté en termes de valorisation de la recherche ne sont donc pas de nature à caractériser une situation d'urgence ; <br/>
<br/>
              6. Considérant, en troisième lieu, que M. M...et autres soutiennent que l'entrée en vigueur immédiate du décret litigieux aurait pour effet de donner des compétences importantes à des instances manquant de légitimité démocratique ; que, toutefois, en admettant même, comme le soutiennent M. M...et autres, que les statuts de la COMUE " Université de Lyon " aient retenu des formes de représentation du personnel moins favorables que d'autres COMUE, une telle circonstance n'est pas de nature à caractériser l'urgence ; qu'au contraire la suspension du décret contesté aurait pour effet le maintien en vigueur, pour une période indéterminée, des organes de l'ancien PRES pourtant transformé en COMUE par l'effet de la loi, sans pour autant aller dans le sens des intérêts dont se prévalent les requérants ; <br/>
<br/>
              7. Considérant, par ailleurs, que M. M...et autres ne justifient d'aucun autre intérêt public précis qui serait de nature à caractériser l'urgence ; que, dès lors, la condition d'urgence ne saurait, en l'état, être regardée comme remplie ; que, par suite, sans qu'il soit besoin de statuer sur la fin de non-recevoir soulevée par la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche et par la communauté d'universités et établissements " Université de Lyon ", la requête de M. M...et autres doit être rejetée. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. M...et autres est rejetée.<br/>
<br/>
Article 2 : La présente ordonnance sera notifiée à M. E...M..., mandataire unique des autres requérants, au Premier ministre, à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche et à la communauté d'universités et établissements " Université de Lyon ".<br/>
Copie en sera adressée à l'association " Démocratie et transparence à l'université de Lyon ", à M. N... P..., à M. O...R..., à M. G...J..., à M. O...A..., à Mme Q...F..., à M. B...C..., à M. O...K..., à M. I... L... et à M. D... H....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
