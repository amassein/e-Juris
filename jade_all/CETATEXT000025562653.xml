<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025562653</ID>
<ANCIEN_ID>JG_L_2012_03_000000353511</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/56/26/CETATEXT000025562653.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 21/03/2012, 353511, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-03-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353511</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Philippe Martin</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>M. Gilles Pellissier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:353511.20120321</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 20 octobre 2011 au secrétariat du contentieux du Conseil d'Etat, présenté par le GARDE DES SCEAUX, MINISTRE DE LA JUSTICE ET DES LIBERTES, qui demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 10VE03924 du 6 octobre 2011 par laquelle le président de la 1ère chambre de la cour administrative d'appel de Versailles a rejeté son recours tendant à l'annulation de l'ordonnance du 30 novembre 2010 par laquelle le juge des référés du tribunal administratif de Versailles a désigné un expert en vue de constater les conditions de la détention et du droit d'accès aux livres de M. Mokhtar A ;<br/>
<br/>
              2°) statuant en référé, d'annuler l'ordonnance du juge des référés du tribunal et de rejeter la demande de M. A ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Gilles Pellissier, Maître des Requêtes,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'à la demande de M. A, détenu à la maison d'arrêt de Fleury-Mérogis, le juge des référés du tribunal administratif de Versailles a, en application de l'article R. 531-1 du code de justice administrative, désigné par une ordonnance du 30 novembre 2010 un expert ayant pour mission de se rendre dans cet établissement pénitentiaire pour y procéder à la constatation des conditions de détention de M. A et de l'exercice de son droit d'accès aux livres ; que le GARDE DES SCEAUX, MINISTRE DE LA JUSTICE ET DES LIBERTES se pourvoit en cassation contre l'ordonnance du 6 octobre 2011 par laquelle le juge des référés de la cour administrative d'appel de Versailles a rejeté son recours tendant à l'annulation de cette ordonnance ;<br/>
<br/>
              Considérant qu'aux termes du premier alinéa de l'article R. 811-1 du code de justice administrative : " Toute partie présente dans une instance devant le tribunal administratif ou qui y a été régulièrement appelée, alors même qu'elle n'aurait produit aucune défense, peut interjeter appel contre toute décision juridictionnelle rendue dans cette instance " ; que doit être regardée comme une partie présente à l'instance, ayant à ce titre qualité pour interjeter appel contre la décision juridictionnelle qui y a été rendue, la personne qui a été invitée par la juridiction à présenter des observations et qui, si elle ne l'avait pas été, aurait eu qualité pour former tierce opposition contre cette décision ;<br/>
<br/>
              Considérant qu'aux termes de l'article R. 531-1 du code de justice administrative : " S'il n'est rien demandé de plus que la constatation de faits, le juge des référés peut, sur simple requête qui peut être présentée sans ministère d'avocat et même en l'absence d'une décision administrative préalable, désigner un expert pour constater sans délai les faits qui seraient susceptibles de donner lieu à un litige devant la juridiction. / Avis en est donné immédiatement aux défendeurs éventuels (...) " ; que si ces dispositions imposent au juge des référés de notifier immédiatement aux défendeurs éventuels l'ordonnance par laquelle il désigne un expert pour constater des faits, elles n'ont ni pour objet ni pour effet de faire obstacle à ce qu'il mette en cause, avant de rendre son ordonnance et alors même qu'il n'y est jamais obligé, le ou les défendeurs éventuels ;<br/>
<br/>
              Considérant que si l'avis donné aux défendeurs éventuels, en application des dispositions précitées de l'article R. 531-1 du code de justice administrative, de l'ordonnance rendue par le juge des référés sur une demande de constatation des faits, n'a pas pour effet de les mettre en cause dans l'instance sur laquelle a statué le juge des référés, quand bien même ils auraient assisté aux opérations de constat, il en va différemment s'ils ont été invités par le juge des référés, avant qu'il ne statue sur la demande, à présenter leurs observations ; qu'ils ont, dans ce cas, ainsi qu'il a été dit ci-dessus, qualité pour interjeter appel contre la décision rendue dans cette instance lorsqu'ils auraient eu qualité, s'ils n'avaient pas été appelés à la cause, pour former tierce opposition à son encontre ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède qu'en jugeant, par l'ordonnance attaquée, que le GARDE DES SCEAUX, MINISTRE DE LA JUSTICE ET DES LIBERTES ne pouvait pas être mis en cause dans une procédure de constat et, par suite, ne pouvait avoir qualité pour interjeter appel de la décision rendue, le juge des référés de la cour administrative d'appel de Versailles a commis une erreur de droit ; que le GARDE DES SCEAUX, MINISTRE DE LA JUSTICE ET DES LIBERTES est par suite fondé à demander l'annulation de cette ordonnance ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 821-2 du code de justice administrative et de statuer sur l'appel présenté par le GARDE DES SCEAUX, MINISTRE DE LA JUSTICE ET DES LIBERTES contre l'ordonnance du 30 novembre 2010 par laquelle le juge des référés du tribunal administratif de Versailles a désigné un expert pour constater les conditions de détention de M. A ;<br/>
<br/>
              Considérant, d'une part, qu'il ressort des pièces du dossier que le juge des référés du tribunal administratif a, avant de statuer sur la demande de constatation des faits dont M. A l'avait saisi et ainsi qu'il en a toujours la faculté, notifié cette demande au GARDE DES SCEAUX, MINISTRE DE LA JUSTICE ET DES LIBERTES, qui a produit devant lui un mémoire en défense ; qu'ayant été ainsi mis en cause et dès lors que la mesure de constat en litige, ayant été sollicitée afin de réunir les éléments de preuve propres à permettre à M. A d'engager un recours indemnitaire à l'encontre de l'Etat à raison des conditions de sa détention, préjudice à ses droits, le GARDE DES SCEAUX, MINISTRE DE LA JUSTICE ET DES LIBERTES a qualité pour en interjeter appel ;<br/>
<br/>
              Considérant, d'autre part, que M. A a demandé que les conditions matérielles de sa détention à la maison d'arrêt de Fleury-Mérogis et les modalités d'exercice de son droit d'accès aux livres fassent l'objet d'un constat ; que si le GARDE DES SCEAUX, MINISTRE DE LA JUSTICE ET DES LIBERTES soutient que, dès lors que ces conditions étaient parfaitement connues, sa demande était dépourvue d'utilité, il ne résulte pas de l'instruction que de telles constatations qui, eu égard à leur objet, présentaient en l'espèce un caractère utile, auraient été établies dans les mêmes locaux ou des locaux similaires de cette maison d'arrêt, durant la période d'incarcération de M. A ; que, par suite, le GARDE DES SCEAUX, MINISTRE DE LA JUSTICE ET DES LIBERTES n'est pas fondé à soutenir que la demande de constat de M. A ne présentait pas de caractère utile ni, par suite, à demander l'annulation de l'ordonnance du 30 novembre 2010 du juge des référés du tribunal administratif de Versailles ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : L'ordonnance du 6 octobre 2011 du juge des référés de la cour administrative d'appel de Versailles est annulée.<br/>
<br/>
Article 2 : Le recours présenté par le GARDE DES SCEAUX, MINISTRE DE LA JUSTICE ET DES LIBERTES devant le juge des référés de la cour administrative d'appel de Versailles est rejeté.<br/>
<br/>
Article 3 : La présente décision sera notifiée au GARDE DES SCEAUX, MINISTRE DE LA JUSTICE ET DES LIBERTES et à M. Mokhtar A.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-03-02 PROCÉDURE. PROCÉDURES D'URGENCE. CONSTAT D'URGENCE. - 1) POSSIBILITÉ DE MISE EN CAUSE D'UN DÉFENDEUR - EXISTENCE [RJ1] - 2) CONSÉQUENCE - POSSIBILITÉ POUR CE DERNIER DE FAIRE APPEL DE L'ORDONNANCE DE CONSTAT D'URGENCE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-08-01-01-02 PROCÉDURE. VOIES DE RECOURS. APPEL. RECEVABILITÉ. QUALITÉ POUR FAIRE APPEL. - RÉFÉRÉ CONSTAT D'URGENCE (ART. R. 531-1 DU CJA) - 1) POSSIBILITÉ DE MISE EN CAUSE D'UN DÉFENDEUR - EXISTENCE [RJ1] - 2) CONSÉQUENCE - POSSIBILITÉ POUR CE DERNIER DE FAIRE APPEL DE L'ORDONNANCE DE CONSTAT D'URGENCE - EXISTENCE.
</SCT>
<ANA ID="9A"> 54-03-02 1) Si les dispositions de l'article R. 531-1 du code de justice administrative (CJA) imposent au juge des référés de notifier immédiatement aux défendeurs éventuels l'ordonnance par laquelle il désigne un expert pour constater des faits, elles n'ont ni pour objet ni pour effet de faire obstacle à ce qu'il mette en cause, avant de rendre son ordonnance et alors même qu'il n'y est jamais obligé, le ou les défendeurs éventuels.,,2) Si des défendeurs éventuels ont été invités par le juge des référés, avant qu'il ne statue sur la demande, à présenter leurs observations, ils ont, dans ce cas, qualité pour interjeter appel contre la décision rendue dans cette instance lorsqu'ils auraient eu qualité, s'ils n'avaient pas été appelés à la cause, pour former tierce opposition à son encontre.</ANA>
<ANA ID="9B"> 54-08-01-01-02 1) Si les dispositions de l'article R. 531-1 du code de justice administrative (CJA) imposent au juge des référés de notifier immédiatement aux défendeurs éventuels l'ordonnance par laquelle il désigne un expert pour constater des faits, elles n'ont ni pour objet ni pour effet de faire obstacle à ce qu'il mette en cause, avant de rendre son ordonnance et alors même qu'il n'y est jamais obligé, le ou les défendeurs éventuels.,,2) Si des défendeurs éventuels ont été invités par le juge des référés, avant qu'il ne statue sur la demande, à présenter leurs observations, ils ont, dans ce cas, qualité pour interjeter appel contre la décision rendue dans cette instance lorsqu'ils auraient eu qualité, s'ils n'avaient pas été appelés à la cause, pour former tierce opposition à son encontre.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 21 décembre 1979, Commune d'Arnouville-les-Gonesse c/ Miny et Morin, n° 17362, p. 487. Ab. jur. sur ce point CE, 23 octobre 1968, Ministre de l'agriculture c/ consorts Claye, n° 74477, p. 513.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
