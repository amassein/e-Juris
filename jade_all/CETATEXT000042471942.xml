<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042471942</ID>
<ANCIEN_ID>JG_L_2020_10_000000429383</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/47/19/CETATEXT000042471942.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 23/10/2020, 429383, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429383</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Paul Bernard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:429383.20201023</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. G... A... B..., Mme F... A... B... et M. H... E..., agissant en leur qualité d'héritiers de Mme C... D... et en leur nom propre, ont demandé au tribunal administratif de Mayotte de condamner l'Etat à les indemniser des préjudices subis par Mme C... D... avant son décès survenu le 29 mars 2011 ainsi que de leur préjudice moral lié au décès de cette dernière. <br/>
<br/>
              Par un jugement n° 1500009 du 22 décembre 2016, le tribunal administratif de Mayotte a condamné l'Etat à leur verser à chacun une indemnité de 10 000 euros et a rejeté le surplus de leurs demandes. <br/>
<br/>
              Par un arrêt n° 17BX00250 du 7 février 2019, la cour administrative d'appel de Bordeaux a condamné l'Etat à verser à M. G... A... B..., Mme F... A... B... et M. H... E..., d'une part, en leur qualité d'ayants droit de Mme C... D..., une somme globale de 5 000 euros et, d'autre part, au titre de leur préjudice personnel, une somme de 20 000 euros chacun, et a rejeté le surplus de leurs conclusions. <br/>
<br/>
              Par un pourvoi, enregistré le 3 avril 2019 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat d'annuler cet arrêt. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code d'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - l'ordonnance n° 2000-373 du 26 avril 2000 ; <br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Paul Bernard, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de M. A... B... et autres ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme C... D..., ressortissante comorienne née le 12 mai 1959, a entrepris le 26 mars 2011 la traversée en mer entre l'île d'Anjouan, aux Comores, et Mayotte. L'embarcation sur laquelle elle se trouvait avec une quarantaine de personnes a subi une panne de moteur, puis a fait naufrage. Certains passagers, dont Mme C... D..., ont repris place dans l'embarcation et ont entrepris de rejoindre Mayotte. Après trois jours de dérive, l'embarcation a rejoint les eaux territoriales de Mayotte. Le 29 mars 2011, les passagers ont été pris en charge par les services de secours, mais Mme C... D... est décédée quelques heures plus tard.<br/>
<br/>
              2. M. G... A... B..., Mme F... A... B... et M. H... E..., ses enfants, ont demandé au tribunal administratif de Mayotte de condamner l'Etat à les indemniser des préjudices subis par leur mère avant son décès ainsi que de leur préjudice moral lié au décès de cette dernière. Par un jugement du 22 décembre 2016, le tribunal administratif de Mayotte a retenu l'existence de manquements fautifs commis par les services de l'Etat dans la prise en charge de Mme C... D..., fixé à 50 % la part de responsabilité incombant à l'Etat et a condamné ce dernier à verser à chacun des demandeurs une indemnité de 10 000 euros. Par un arrêt du 7 février 2019, la cour administrative d'appel de Bordeaux, devant laquelle la qualification des manquements reprochés à l'Etat n'était plus contestée, a jugé que l'Etat devait être reconnu entièrement responsable du décès de Mme C... D.... Réformant en conséquence le jugement de première instance, la cour a condamné l'Etat à verser à Mme A... B..., à M. A... B... et à M. E... une somme globale de 5 000 euros en leur qualité d'ayants droit de leur mère décédée et une somme de 20 000 euros chacun au titre de leur préjudice personnel. Le ministre de l'intérieur se pourvoit en cassation contre cet arrêt en ce qu'il retient la responsabilité exclusive de l'Etat.<br/>
<br/>
              3. Pour juger que la cause déterminante du décès de Mme C... D... consistait en la succession de négligences fautives de l'Etat dans l'organisation des secours et la prise en charge de celle-ci, la cour administrative d'appel de Bordeaux s'est fondée sur les conclusions de l'expert missionné par un magistrat instructeur du tribunal de grande instance de Mamoudzou, dont il résultait qu'un traitement de la déshydratation de l'intéressée, associé à une surveillance médicale, aurait permis d'éviter l'aggravation de son état de santé et, par suite, son décès. Il ressort toutefois des énonciations de son arrêt que Mme C... D..., dont la santé était particulièrement fragile, avait pris le risque d'entreprendre une traversée dangereuse sur une embarcation de fortune. Dans ces conditions, en déduisant de l'ensemble de ces constatations que le décès de Mme C... D... était exclusivement imputable à des négligences fautives de l'Etat, écartant ainsi tout lien direct de causalité entre le risque assumé par l'intéressée et son décès, la cour a inexactement qualifié les faits de l'espèce.<br/>
<br/>
              4. Il résulte de ce qui précède que le ministre de l'intérieur est fondé à demander, par ce moyen qui n'est pas nouveau en cassation, l'annulation de l'arrêt qu'il attaque, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi. <br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt du 7 février 2019 de la cour administrative d'appel de Bordeaux est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux. <br/>
<br/>
Article 3 : Les conclusions présentées par M. A... B..., Mme A... B... et M. E... au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 4 : La présente décision sera notifiée au ministre de l'intérieur, à M. G... A... B..., à Mme F... A... B... et à M. H... E.... <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
