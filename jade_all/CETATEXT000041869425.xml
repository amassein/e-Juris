<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041869425</ID>
<ANCIEN_ID>JG_L_2020_05_000000440279</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/86/94/CETATEXT000041869425.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 07/05/2020, 440279, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-05-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440279</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:440279.20200507</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Procédure contentieuse antérieure :<br/>
<br/>
              La société Provence Lotissements a demandé au tribunal administratif de Marseille d'annuler pour excès de pouvoir l'arrêté du 19 février 2016 par lequel le maire de  Fos-sur-Mer a délivré à Mme A... B... un permis de construire valant division parcellaire et construction de 20 villas individuelles groupées à usage d'habitation, ainsi que la décision du 7 avril 2016 par laquelle le maire a rejeté le recours gracieux formé contre cet arrêté. Par un jugement n° 1604940 du 16 mars 2020, le tribunal administratif de Marseille a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée sous le n° 20MA01678, la société Provence Lotissements a demandé à la cour administrative d'appel de Marseille d'annuler ce jugement et de faire droit à sa demande. Par une ordonnance du 27 avril 2020, la présidente de la cour administrative d'appel de Marseille a transmis au Conseil d'Etat le dossier du recours formé par la société Provence Lotissements contre ce jugement, en application de l'article R. 351-2 du code de justice administrative.<br/>
<br/>
              Par une requête, enregistrée sous le n° 20MA01679, la société Provence Lotissements a demandé au juge des référés de la cour administrative d'appel de Marseille d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution du permis délivré le 19 février 2016 ainsi que de la décision du 7 avril 2016 rejetant son recours gracieux. Par une ordonnance du 27 avril 2020, la présidente de la cour a transmis au Conseil d'Etat cette requête en référé, sur le fondement de l'article R. 351-2 du code de justice administrative.<br/>
<br/>
              Procédure contentieuse devant le Conseil d'Etat :<br/>
              Le dossier du pourvoi en cassation a été enregistré sous le n° 440276 et celui de la requête en référé-suspension a été enregistré sous le n° 440279 au secrétariat du contentieux du Conseil d'Etat. <br/>
<br/>
              Sous le n° 440279, la société Provence Lotissement demande :<br/>
<br/>
              1°) de suspendre l'arrêté du 19 février 2016 du maire de Fos-sur-Mer délivrant à Mme B... un permis de construire valant division parcellaire et construction de 20 villas individuelles groupées à usage d'habitation ainsi que de la décision du 7 avril 2016 rejetant son recours gracieux ;<br/>
<br/>
              2°) de mettre à la charge de la commune de de Fos-sur-Mer et de Mme B... la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - la condition d'urgence est remplie, dès lors qu'elle est propriétaire de la parcelle voisine et, d'autre part, que l'arrêté contesté aura pour effet d'instaurer une servitude à laquelle elle s'oppose ;<br/>
              - il existe un doute sérieux quant à la légalité de l'acte attaqué ;<br/>
              - le dossier de demande de permis de construire comporte des insuffisances au regard des articles R. 431-7 à 431-12 du code de l'urbanisme et des dispositions de l'article UD 13 du plan d'occupation des sols ;<br/>
              - le projet méconnaît l'article UD 8 du plan d'occupation des sols ;<br/>
              - il méconnaît l'article UD 4 du même plan d'occupation des sols ainsi que les articles L. 111-4 et R. 111-2 du code de l'urbanisme ;<br/>
              - il méconnaît également l'article UD 3 de ce plan.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code général des impôts et le décret n° 2013-392 du 10 mai 2013 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. D'une part, aux termes de l'article R. 811-1-1 du code de justice administrative : " Les tribunaux administratifs statuent en premier et dernier ressort sur les recours contre les permis de construire ou de démolir un bâtiment à usage principal d'habitation ou contre les permis d'aménager un lotissement lorsque le bâtiment ou le lotissement est implanté en tout ou partie sur le territoire d'une des communes mentionnées à l'article 232 du code général des impôts et son décret d'application (...) ". <br/>
<br/>
              3. Il ressort des pièces du dossier que le permis de construire en litige a été délivré par le maire de la commune de Fos-sur-mer pour la réalisation de " 20 villas individuelles groupées à usage d'habitation " sur le territoire communal. La commune de Fos-sur-Mer figure sur la liste annexée au décret du 10 mai 2013 relatif au champ d'application de la taxe annuelle sur les logements vacants instituée par l'article 232 du code général des impôts. Par suite, en rejetant le recours tendant à l'annulation pour excès de pouvoir de cet arrêté et de la décision rejetant le recours gracieux formé contre cet arrêté, le tribunal administratif de Marseille a, par son jugement du 16 mars 2020, statué en premier et dernier ressort en application des dispositions de l'article R. 811-1-1 du code de justice administrative. Le recours formé contre ce jugement, transmis au Conseil d'Etat par la présidente de la cour administrative d'appel de Marseille, présente dès lors le caractère d'un recours en cassation. <br/>
<br/>
              4. D'autre part, il résulte des termes mêmes de l'article L. 521-1 du code de justice administrative qu'une demande de suspension de l'exécution d'une décision administrative ne peut être présentée au juge des référés que si la juridiction dont il dépend est elle-même saisie d'une requête en annulation ou en réformation de cette décision. Ainsi, si une requête tendant à la suspension d'une décision administrative peut être introduite devant le juge des référés lorsque la juridiction de première instance ou d'appel dont il relève est saisie d'une requête tendant à l'annulation ou à la réformation de cette décision, des conclusions à fin de suspension ne sont pas recevables devant le Conseil d'Etat, juge de cassation, qui est saisi d'une requête dirigée contre la décision juridictionnelle attaquée devant lui et qui n'a pas à examiner la légalité ou le bien-fondé de la décision administrative contestée avant d'avoir, le cas échéant, annulé cette décision. Eu égard à l'office du juge de cassation, une telle impossibilité de lui soumettre une demande de suspension tant qu'il ne s'est pas prononcé sur la légalité de la décision juridictionnelle attaquée devant lui ne méconnaît pas les exigences du droit à un recours effectif garanti par la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.<br/>
<br/>
              5. La procédure de cassation engagée contre le jugement du tribunal administratif de Marseille n'a pas, en son état, pour effet de conférer au Conseil d'Etat la qualité de juge du fond de l'arrêté du 19 février 2016 du maire de Fos-sur-Mer accordant à Mme B... un permis de construire des bâtiments à usage principal d'habitation et de la décision rejetant le recours gracieux formé contre cet arrêté. Il résulte de ce qui a été dit au point précédent que les conclusions à fin de suspension de ces décisions administratives présentées sur le fondement de l'article L. 521-1 du code de justice administrative et transmises au Conseil d'Etat par la présidente de la cour administrative d'appel de Marseille, ne sont manifestement pas recevables.<br/>
<br/>
              6. Il résulte de ce qui précède que la requête de la société Provence Lotissements présentée sur le fondement de l'article L. 521-1 du code de justice administrative doit être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, selon la procédure prévue par l'article L. 522-3 du même code.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de société Provence Lotissements présentée sur le fondement de l'article L. 521-1 du code de justice administrative est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à la société Provence Lotissements.<br/>
Copie en sera adressée à Mme A... B... et à la commune de Fos-sur-Mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
