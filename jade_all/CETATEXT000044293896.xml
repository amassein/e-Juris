<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044293896</ID>
<ANCIEN_ID>JG_L_2021_11_000000444625</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/29/38/CETATEXT000044293896.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 05/11/2021, 444625, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>444625</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; LE PRADO ; SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>M. Alexis Goin</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Marc Pichon de Vendeuil</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:444625.20211105</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Huissiers Partner Conseils a demandé au tribunal administratif d'Orléans, en premier lieu, d'annuler la décision du 3 avril 2017 par laquelle son offre pour l'obtention d'un marché public ayant pour objet l'intervention des huissiers de justice dans le recouvrement amiable des créances prises en charge par les comptables de la direction générale des finances publiques dans le département du Cher a été rejetée et la décision par laquelle ce marché public a été attribué au groupement d'intérêt économique Groupement des poursuites extérieures (GIE GPE), en deuxième lieu, d'annuler le contrat conclu le 4 avril 2017 entre le préfet et la directrice départementale des finances publiques du Cher et le GIE GPE, en troisième lieu, de condamner l'Etat à lui verser la somme de 207 526,39 euros en réparation du préjudice résultant de son éviction irrégulière ainsi que la somme de 303 121,81 euros ou, à titre subsidiaire, la somme de 311 682,40 euros, en réparation du préjudice résultant de la résiliation du précédent marché et du comportement de l'Etat. Par un jugement n° 1703019 du 18 octobre 2018, le tribunal administratif d'Orléans a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 18NT04492 du 17 juillet 2020, la cour administrative d'appel de Nantes a, sur appel de la société Huissiers Partner Conseils, annulé ce jugement en tant qu'il a rejeté la demande indemnitaire de cette société, condamné l'Etat à lui verser la somme de 207 526,39 euros et rejeté le surplus des conclusions de la société.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 17 septembre et 17 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'économie, des finances et de la relance demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il fait partiellement droit aux conclusions de la société Huissiers Partner Conseils ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les conclusions d'appel de la société Huissiers Partner Conseils ;<br/>
<br/>
              3°) de mettre à la charge de la société Huissiers Partner Conseils une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de commerce ;<br/>
              - l'ordonnance n° 45-2592 du 2 novembre 1945 ;<br/>
              - l'ordonnance n° 2015-899 du 23 juillet 2015 ;<br/>
              - la loi n° 2004-1485 du 30 décembre 2004 ;<br/>
              - le décret n° 56-222 du 29 février 1956 ;<br/>
              - le décret n° 2016-360 du 25 mars 2016 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alexis Goin, auditeur,  <br/>
<br/>
              - les conclusions de M. Marc Pichon de Vendeuil, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Foussard, Froger, avocat du ministre de l'économie, des finances et de la relance, à Me Le Prado, avocat de la société Huissiers Partner Conseils et à la SCP Thouvenin, Coudray,Grevy, avocat du GIE Groupement des poursuites extérieures ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, le 25 octobre 2016, la direction départementale des finances publiques (DDFIP) du Cher a publié un avis pour la conclusion d'un marché, passé selon la procédure adaptée, visant à confier à des huissiers de justice la phase amiable pour le recouvrement des créances prises en charge par les comptables de la direction générale des finances publiques dans ce département. La société Huissiers Partner Conseils, attributaire du précédent marché en date du 5 novembre 2012, a présenté une offre le 21 novembre 2016. Elle a été informée, le 3 avril 2017, du rejet de son offre et de l'attribution du contrat au GIE Groupement des poursuites extérieures (GIE GPE). Elle a alors saisi le tribunal administratif d'Orléans d'une demande tendant, d'une part, à l'annulation de la décision portant rejet de son offre et de la décision attribuant le marché en cause au GIE GPE, d'autre part, à l'annulation du contrat conclu le 4 avril 2017 entre la DDFIP du Cher et le GIE GPE, enfin, à la condamnation de l'Etat à lui verser la somme de 207 526,39 euros en réparation du préjudice résultant de son éviction irrégulière ainsi que la somme de 303 121,81 euros ou, à titre subsidiaire, la somme de 311 682,40 euros, en réparation de son préjudice résultant de la résiliation du précédent marché et du comportement de l'Etat. Par un jugement du 18 octobre 2018, le tribunal administratif d'Orléans a rejeté sa demande. Par un arrêt du 17 juillet 2020, la cour administrative d'appel de Nantes, après avoir annulé le jugement du tribunal administratif d'Orléans en tant qu'il a rejeté la demande indemnitaire de cette société, a condamné l'Etat à lui verser la somme de 207 526,39 euros et rejeté le surplus de ses conclusions. Le ministre de l'économie, des finances et de la relance en demande l'annulation en tant qu'il a partiellement fait droit aux conclusions de la société Huissiers Partner Conseils.<br/>
<br/>
              2. D'une part, aux termes du I de l'article 128 de la loi du 30 décembre 2004 de finances rectificative pour 2004 : " Lorsque le comptable du Trésor public est autorisé par des dispositions législatives ou réglementaires à procéder au recouvrement forcé d'une créance ou d'une condamnation pécuniaire, il peut, préalablement à la mise en œuvre de toute procédure coercitive, demander à un huissier de justice d'obtenir du débiteur ou du condamné qu'il s'acquitte entre ses mains du montant de sa dette ou de sa condamnation pécuniaire. / Les frais de recouvrement sont versés directement par le débiteur ou le condamné à l'huissier de justice (...) ". Selon l'article 1er de l'ordonnance du 2 novembre 1945 relative au statut des huissiers : " Les huissiers de justice peuvent (...) procéder au recouvrement amiable (...) de toutes créances ". <br/>
<br/>
              3. D'autre part, aux termes de l'article L. 251-2 du code de commerce : " Les personnes exerçant une profession libérale soumise à un statut législatif ou réglementaire ou dont le titre est protégé peuvent constituer un groupement d'intérêt économique ou y participer ". Selon l'article L. 123-9 du même code : " La personne assujettie à immatriculation ne peut, dans l'exercice de son activité, opposer ni aux tiers ni aux administrations publiques, qui peuvent toutefois s'en prévaloir, les faits et actes sujets à mention que si ces derniers ont été publiés au registre. (...) / Les dispositions des alinéas précédents sont applicables aux faits ou actes sujets à mention ou à dépôt même s'ils ont fait l'objet d'une autre publicité légale. Ne peuvent toutefois s'en prévaloir les tiers et administrations qui avaient personnellement connaissance de ces faits et actes ". Aux termes de l'article L. 251-8 du même code : " I. - Le contrat de groupement d'intérêt économique détermine l'organisation du groupement, sous réserve des dispositions du présent chapitre. Il est établi par écrit et publié selon les modalités fixées par décret en Conseil d'Etat. / II. - Le contrat contient notamment les indications suivantes : / (...) 2° Les nom, raison sociale ou dénomination sociale, la forme juridique, l'adresse du domicile ou du siège social et, s'il y a lieu, le numéro d'identification de chacun des membres du groupement, (...) / III. - Toutes les modifications du contrat sont établies et publiées dans les mêmes conditions que le contrat lui-même. Elles ne sont opposables aux tiers qu'à dater de cette publicité ". L'article R. 123-36 du même code prévoit que les groupements d'intérêt économique sont tenus à l'obligation d'immatriculation au registre du commerce et des sociétés. L'article R. 123-66 fait obligation à toute personne morale immatriculée de procéder à une inscription modificative dans le mois de tout fait ou acte rendant nécessaire la rectification ou le complément des mentions de l'immatriculation.<br/>
<br/>
              4. Il résulte des dispositions de l'article L. 123-9 du code de commerce citées au point précédent qu'une personne assujettie à immatriculation au registre du commerce et des sociétés ne peut pas opposer aux administrations publiques les faits et actes sujets à mention dans ce registre qui n'ont pas été publiés dans celui-ci. En revanche, l'administration peut, lorsqu'elle en a connaissance, se prévaloir de ces faits et actes alors même qu'ils n'ont pas fait l'objet d'une telle publication. Dès lors, la seule circonstance qu'un changement dans la composition du groupement d'intérêt économique candidat à l'attribution d'un marché public n'a pas été publié au registre, en méconnaissance des dispositions du code de commerce, à la date de la signature du contrat ne fait pas obstacle à ce que le pouvoir adjudicateur tienne compte de ce changement lorsqu'il en a connaissance. Ce défaut de publication n'entache donc pas, par lui-même, la régularité de la candidature du groupement.<br/>
<br/>
              5. La cour administrative d'appel de Nantes a estimé, par une appréciation souveraine non arguée de dénaturation, que la SCP Richard Lamagnère Coudray Chevalier était devenue membre du GIE GPE par un acte du 31 octobre 2016, lequel n'avait pas fait l'objet, avant la date de la signature du marché en litige, de la publicité prévue par les dispositions du code de commerce citées au point 3. En jugeant que cette candidature, signée par Me Coudray, était irrégulière au seul motif que l'acte par lequel la SCP Richard Lamagnère Coudray Chevalier était devenue membre du GIE n'avait pas été publié au registre du commerce et des sociétés à la date de la signature du marché, et en en déduisant que la candidature de ce groupement était irrégulière et que la société Huissiers Partner Conseils pouvait par suite demander l'indemnisation du préjudice causé par son éviction du marché, la cour administrative d'appel de Nantes a commis une erreur de droit.<br/>
<br/>
              6. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi du ministre, que l'arrêt de la cour administrative d'appel de Nantes doit être annulé en tant qu'il a partiellement fait droit aux conclusions de la société Huissiers Partner Conseils.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Huissiers Partner Conseils la somme de 3 000 euros à verser à l'Etat au titre de l'article L. 761-1 du code de justice administrative. Les dispositions du même article font obstacle à ce qu'une somme soit mise à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er, 2 et 4 de l'arrêt du 17 juillet 2020 de la cour administrative d'appel de Nantes sont annulés.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Nantes.<br/>
Article 3 : La société Huissiers Partner Conseils versera à l'Etat la somme de 3 000 euros sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative. Ses conclusions présentées au même titre sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'économie, des finances et de la relance et à la société Huissiers Partner Conseils.<br/>
Copie en sera adressée au groupement d'intérêt économique Groupement des poursuites extérieures.<br/>
              Délibéré à l'issue de la séance du 13 octobre 2021 où siégeaient : M. Jacques-Henri Stahl, président adjoint de la section du contentieux, présidant ; M. H... J..., M. Olivier Japiot, présidents de chambre ; M. I... L..., Mme A... K..., M. D... G..., M. E... M..., M. Jean-Yves Ollier, conseillers d'Etat et M. Alexis Goin, auditeur-rapporteur. <br/>
<br/>
              Rendu le 5 novembre 2021.<br/>
<br/>
                                   Le président : <br/>
                                   Signé : M. N... C...<br/>
<br/>
Le rapporteur : <br/>
Signé : M. Alexis Goin<br/>
<br/>
                                   La secrétaire :<br/>
                                   Signé : Mme F... B...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
