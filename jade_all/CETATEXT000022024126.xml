<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000022024126</ID>
<ANCIEN_ID>JG_L_2010_03_000000336046</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/22/02/41/CETATEXT000022024126.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 19/03/2010, 336046, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2010-03-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>336046</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Vigouroux</PRESIDENT>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Christian  Vigouroux</RAPPORTEUR>
<COMMISSAIRE_GVT/>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée au secrétariat du contentieux du Conseil d'Etat le 29 janvier 2010, présentée par M. Laurent A et Mme Renate B, demeurant ..., agissant en qualité de représentants légaux de leurs enfants mineurs Brandon et Moku Ronato ; M. A et Mme B demandent au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de la décision du 6 avril 2009 par laquelle le consul de France à Kinshasa (République démocratique du Congo) a refusé les demandes de visas de long séjour de Mme B, de Brandon et de Grâce C, en qualité respectivement de conjoint et d'enfants de ressortissant étranger bénéficiaire du statut de réfugié ;<br/>
<br/>
              2°) de suspendre, sur le fondement du même article, la décision par laquelle le consul de France à Kinshasa a confirmé sa première décision de rejet ainsi que la décision implicite de rejet de la commission de recours contre les refus de visa d'entrée en France confirmant ce refus ;<br/>
<br/>
              3°) d'enjoindre au ministre de l'immigration, de l'intégration, de l'identité nationale et du développement solidaire de délivrer à Mme B et à ses enfants les visas de long séjour sollicités dans un délai de quarante-huit heures à compter de la notification de l'ordonnance à intervenir ;<br/>
<br/>
              4°) d'enjoindre, à titre subsidiaire, au ministre de l'immigration, de l'intégration, de l'identité nationale et du développement solidaire de réexaminer leurs demandes de visa ;<br/>
<br/>
              5°) d'assortir ces injonctions d'une astreinte d'au moins 200 euros par jour de retard ;<br/>
<br/>
              6°) de mettre à la charge de l'Etat la somme de 2 500 euros sur le fondement de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              ils soutiennent que les décisions dont la suspension est demandée qui séparent leur famille et préjudicient à l'intérêt de leurs enfants créent une situation d'urgence ; que l'urgence est d'autant plus caractérisé que leur premier enfant Brandon, affecté d'une embarrure pariétale, doit subir une opération neurochirurgicale qui ne peut être effectuée en République démocratique du Congo ; qu'il existe un doute sérieux quant à la légalité des décisions dont la suspension est demandée ; qu'en effet la décision de la commission de recours contre les refus de visa est entachée d'une insuffisance de motivation ; qu'il en est de même de la décision du consul général de France à Kinshasa du 6 avril 2009 ; qu'en outre le consul général de France à Kinshasa, qui n'a pas informé les requérants de ses opérations de vérifications des actes d'état civil produits à l'appui de leur demande, a méconnu les dispositions de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec l'administration ; qu'en outre la commission de recours contre les refus de visa a commis un vice de procédure ; que le consul général de France à Kinshasa a commis une erreur de fait ; qu'en outre les décisions dont il est demandé la suspension sont entachées d'une erreur manifeste d'appréciation ; qu'elles méconnaissent l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; qu'enfin elles méconnaissent l'article 3-1 de la convention de New York relative aux droits de l'enfant ;<br/>
<br/>
<br/>
              Vu la requête à fin d'annulation des décisions dont la suspension est demandée ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 12 mars 2010 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de l'immigration, de l'intégration, de l'identité nationale et du développement solidaire, qui conclut au rejet de la requête ; il soutient que les conclusions de la requête à fin d'injonction sont irrecevables ; que la décision de la commission de recours contre les refus de visa d'entrée en France, qui s'est entièrement substituée à la décision du consul général de France à Kinshasa du 6 avril 2009, n'est pas entachée de défaut de motivation ; que le consul général de France à Kinshasa n'a pas commis d'erreur manifeste d'appréciation dès lors que les actes d'état civil fournis à l'appui des demandes de visas sont apocryphes ; qu'au surplus aucune demande de visa n'a été enregistrée auprès du consulat général de France à Kinshasa pour le second enfant allégué de M. A et de Mme D ; que les décisions dont ils demandent la suspension ne méconnaissent ainsi ni l'article 8 de la convention européenne des droits de l'homme ni l'article 3-1 de la convention internationale relative aux droits de l'enfant ; que M. A ne prouve pas entretenir de relations avec sa famille alléguée ; qu'ainsi la condition d'urgence ne peut être regardée comme remplie ;<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention de New York relative aux droits de l'enfant ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu la loi 79-587 du 11 juillet 1979 ;<br/>
<br/>
              Vu la loi n°2000-321 du 12 avril 2000 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à l'audience publique du 17 mars 2010 à 10 heures 30 d'une part M. A et, d'autre part, le ministre de l'immigration, de l'intégration, de l'identité nationale et du développement solidaire ;<br/>
<br/>
              Vu le procès-verbal de l'audience du 17 mars 2010 à 10 heures 30 au cours de laquelle ont été entendus :<br/>
<br/>
- Me Nicolaÿ, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. A;<br/>
- M. A ;<br/>
<br/>
              - le représentant du ministre de l'immigration, de l'intégration, de l'identité nationale et du développement solidaire ;<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes de l'article L. 521-1 du code de justice administrative :  Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision  ; <br/>
<br/>
              Considérant qu'il résulte des pièces du dossier soumises au juge des référés que M. A, qui bénéficie du statut de réfugié en France s'est vu opposer un refus à la demande de visa présentée pour son épouse Mme B et ses deux enfants Brandon et Grâce au double motif que le mariage et la filiation n'étaient pas établis du fait du caractère apocryphe des actes d'état civil produits ;<br/>
<br/>
              Considérant que s'il appartient, en principe, aux autorités consulaires de délivrer au conjoint et aux enfants mineurs d'un réfugié statutaire les visas qu'ils sollicitent afin de mener une vie familiale normale, elles peuvent toutefois opposer un refus à une telle demande pour un motif d'ordre public, notamment en cas de fraude ; qu'en l'état de l'instruction, il subsiste des doutes quant à l'authenticité des documents produits à l'appui des demandes de visas alors que, même s'il allègue la simple erreur matérielle, le requérant lui-même donne une identité différente pour son second enfant dans la requête introductive et dans le mémoire en réplique devant le juge des référés ; qu'en l'espèce, les extraits du registre des actes de naissance des enfants Brandon et Grâce présentent des contradictions avec l'attestation produite en vue de les conforter ; qu'en particulier, l'acte de naissance du jeune Brandon a été signé le 23 septembre 2008 et vise un jugement supplétif du lendemain ; que la date alléguée du mariage entre les requérants est le 15 décembre 2008 quand l'acte d'état civil produit mentionne le 30 décembre 2005 ; qu'à supposer que cet acte ait été rédigé en 2008, il est pourtant inséré dans un registre de l'année 2005 ; que, dans ces conditions, et malgré la production d'un nouvel acte de mariage rectifié en date du 6 août 2009, le moyen tiré de ce que l'autorité consulaire aurait opposé à tort l'absence d'authenticité de ces actes ne peut être regardé comme propre à créer en l'état de l'instruction, un doute sérieux quant à la légalité des refus de visa ;<br/>
<br/>
              Considérant qu'en l'état du dossier et eu égard aux doutes persistant sur les liens du mariage et de filiation, les moyens tirés de la méconnaissance tant du principe d'unité familiale que de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ne sont pas de nature à créer un doute sérieux quant à la légalité du refus de visa ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède qu'il y a lieu de rejeter les conclusions aux fins de suspension et d'injonction présentées par M. A et Mme B ainsi que leurs conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A et de Mme B est rejetée.<br/>
Article 2: La présente ordonnance sera notifiée à M. Laurent A, Mme Renate B et au ministre de l'immigration, de l'intégration, de l'identité nationale et du développement solidaire.<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
