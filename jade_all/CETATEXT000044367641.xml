<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044367641</ID>
<ANCIEN_ID>JG_L_2021_11_000000405548</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/36/76/CETATEXT000044367641.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 24/11/2021, 405548, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>405548</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BENABENT</AVOCATS>
<RAPPORTEUR>Mme Thalia Breton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:405548.20211124</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une décision n° 405548 du 2 décembre 2019, le Conseil d'Etat, statuant au contentieux sur le pourvoi de M. D..., a sursis à statuer jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur la question de savoir si :<br/>
<br/>
              Le bénéfice des dispositions du 1. de l'article 11 de l'annexe VIII du règlement fixant le statut des fonctionnaires des Communautés européennes ainsi que le statut applicable aux autres agents, tel que modifié par le règlement (CE, Euratom) n° 723/2004 du Conseil du 22 mars 2004, est-il réservé aux seuls fonctionnaires et agents contractuels affectés pour la première fois au sein d'une administration nationale après avoir été employés en qualité de fonctionnaire, agent contractuel ou agent temporaire dans une institution de l'Union européenne, ou est-il ouvert également aux fonctionnaires et agents contractuels retournant au service d'une administration nationale après avoir exercé des fonctions dans une institution de l'Union européenne et avoir été, pendant cette période, placés en disponibilité ou congé pour convenances personnelles '<br/>
<br/>
              Par un arrêt C-903/19 du 4 février 2021, la CJUE s'est prononcée sur cette question.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier, y compris celles visées par la décision du Conseil d'Etat, statuant au contentieux, du 2 décembre 2019 ;<br/>
<br/>
<br/>
<br/>
              Vu :<br/>
<br/>
<br/>
              - le traité sur le fonctionnement de l'Union européenne, notamment son article 267 ; <br/>
              - le règlement n°31 (CEE), n°11 CEEA des Conseils du 18 décembre 1961 fixant le statut des fonctionnaires des Communautés européennes ainsi que le régime applicable aux autres agents de ces Communautés, tel que modifié notamment par le règlement (CEE, Euratom, CECA) n° 259168 du Conseil du 29 février 1968 et le règlement (CE, Euratom) n° 723/2004 du Conseil, du 22 mars 2004 ;<br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le décret n° 2009-1052 du 26 août 2009 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Thalia Breton, auditrice,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Bénabent, avocat de M. D... ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. D..., agent titulaire de la fonction publique de l'Etat depuis le 1er septembre 2006, technicien supérieur du développement durable à la direction départementale des territoires du Bas-Rhin, a été placé en disponibilité pour convenances personnelles du 1er avril 2011 au 31 août 2013, période pendant laquelle il a occupé un emploi d'agent contractuel auprès de la Commission européenne. Après avoir réintégré son administration d'origine à l'issue de cette période de disponibilité, il a demandé le transfert, vers le régime des retraites des fonctionnaires de l'Etat, de l'équivalent actuariel de ses droits à pension acquis dans le régime de pension des fonctionnaires de l'Union européenne, en se prévalant des dispositions du 1 de l'article 11 de l'annexe VIII du statut des fonctionnaires de l'Union européenne fixé par le règlement (CEE, Euratom, CECA) n° 259168 du Conseil du 29 février 1968, modifié par le règlement (CE, Euratom) n° 723/2004 du Conseil du 22 mars 2004. Sa demande a été rejetée par deux décisions des 10 juillet et 17 septembre 2014, prises respectivement par le directeur régional de l'environnement, de l'aménagement et du logement (DREAL) d'Alsace et par son adjoint. M. D... se pourvoit en cassation contre le jugement du tribunal administratif de Strasbourg rejetant sa demande d'annulation de ces décisions. Par une décision du 2 décembre 2019, le Conseil d'Etat, statuant au contentieux, a sursis à statuer sur ce pourvoi jusqu'à ce que la Cour de justice de l'Union européenne se prononce sur la question qu'il lui avait renvoyée à titre préjudiciel. Par son arrêt C-903/19 du 4 février 2021, la Cour de justice de l'Union européenne s'est prononcée sur cette question préjudicielle. <br/>
<br/>
              2. Aux termes des dispositions du 1. de l'article 11 de l'annexe VIII du statut des fonctionnaires de l'Union européenne fixé par le règlement (CEE, Euratom, CECA) n° 259168 du Conseil du 29 février 1968, rendues applicables aux agents contractuels par le 1. de l'article 109 du régime applicable aux agents contractuels des Communautés européennes introduit par le règlement (CE, Euratom) n° 723/2004 du Conseil du 22 mars 2004 modifiant le règlement du 29 février 1968 : " 1.  Le fonctionnaire qui cesse ses fonctions pour: / - entrer au service d'une administration, d'une organisation nationale ou internationale ayant conclu un accord avec l'Union, (...) a le droit de faire transférer l'équivalent actuariel, actualisé à la date de transfert effectif, de ses droits à pension d'ancienneté, qu'il a acquis auprès de l'Union, à la caisse de pension de cette administration, de cette organisation, ou à la caisse auprès de laquelle le fonctionnaire acquiert des droits à pension d'ancienneté au titre de son activité salariée ou non salariée ". <br/>
<br/>
              3. A l'appui de son pourvoi, M D... fait valoir que le tribunal administratif de Strasbourg a entaché son jugement d'erreur de droit et a méconnu le principe d'égalité en jugeant qu'au sens des dispositions du 1 de l'article 11 de l'annexe VIII du statut des fonctionnaires de l'Union européenne fixé par le règlement (CEE, Euratom, CECA) n° 259168 du Conseil du 29 février 1968, rendues applicables aux agents contractuels par le 1 de l'article 109 du régime applicable aux agents contractuels des Communautés européennes introduit par le règlement (CE, Euratom) n° 723/2004 du Conseil du 22 mars 2004 modifiant le règlement du 29 février 1968, " l'entrée au service " doit s'entendre uniquement de l'affectation initiale de l'agent au sein d'une administration nationale, à l'exclusion de son retour à l'issue d'une disponibilité pour convenances personnelles. <br/>
<br/>
              4. Dans son arrêt C-903/19 du 4 février 2021, la Cour de justice de l'Union européenne a dit pour droit que " L'article 11, paragraphe 1, de l'annexe VIII du statut des fonctionnaires de l'Union européenne doit être interprété en ce sens que le transfert de l'équivalent actuariel des droits à pension d'ancienneté peut être demandé tant par les fonctionnaires et les agents contractuels qui intègrent pour la première fois une administration nationale après avoir été employés dans une institution de l'Union que par ceux qui y retournent après avoir exercé des fonctions dans une telle institution dans le cadre d'une mise en disponibilité ou d'un congé pour convenances personnelles ".<br/>
<br/>
              5. Par suite, en jugeant que l'entrée au service au sens de l'article 11, paragraphe 1, de l'annexe VIII du statut des fonctionnaires de l'Union européenne devait s'entendre uniquement de l'affectation initiale de l'agent au sein d'une administration, à l'exclusion de son retour à l'issue d'une disponibilité pour convenances personnelles, qui concerne le déroulement de carrière de l'agent, le tribunal administratif de Strasbourg a entaché son jugement d'erreur de droit.<br/>
<br/>
              6. Il résulte de ce qui précède que M. D... est fondé à demander l'annulation du jugement qu'il attaque.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Strasbourg du 19 octobre 2016 est annulé.<br/>
Article 2 : L'affaire est renvoyée devant le tribunal administratif de Strasbourg.<br/>
<br/>
Article 3 : L'Etat versera une somme de 3 000 euros à M. D... au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. A... D..., à la ministre de la transition écologique et solidaire et au ministre de l'économie, des finances et de la relance. <br/>
Copie en sera adressée au Premier ministre.<br/>
              Délibéré à l'issue de la séance du 8 octobre 2021 où siégeaient : Mme Maud Vialettes, présidente de chambre, présidant ; Mme Fabienne Lambolez, conseillère d'Etat et Mme Thalia Breton, auditrice-rapporteure. <br/>
<br/>
<br/>
              Rendu le 24 novembre 2021.<br/>
<br/>
                 La présidente : <br/>
                 Signé : Mme Maud Vialettes<br/>
<br/>
<br/>
 		La rapporteure : <br/>
      Signé : Mme Thalia Breton<br/>
<br/>
<br/>
                 La secrétaire :<br/>
                 Signé : Mme B... C...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
