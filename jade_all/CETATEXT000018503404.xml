<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000018503404</ID>
<ANCIEN_ID/>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/18/50/34/CETATEXT000018503404.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 26/03/2008, 286003, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2008-03-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>286003</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin</PRESIDENT>
<AVOCATS>BLANC ; ODENT</AVOCATS>
<RAPPORTEUR>Mme Claire  Legras</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Collin Pierre</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 10 octobre 2005 et 27 janvier 2006 au secrétariat du contentieux du Conseil d'Etat, présentés pour la CAISSE DES DEPOTS ET CONSIGNATIONS, dont le siège est Rue du Vergne à Bordeaux (33059 Cedex) ; la CAISSE DES DEPOTS ET CONSIGNATIONS demande au Conseil d'Etat :
              
              1°) d'annuler le jugement du 15 juillet 2005 par lequel le tribunal administratif de Bordeaux a annulé, à la demande de M. Lucien A, la décision implicite de rejet opposée à la demande de celui-ci tendant à ce qui lui soit confirmé le maintien du forfait de 1 960 heures pour le calcul de sa pension d'ouvrier d'Etat ;
              
              2°) réglant l'affaire au fond, de rejeter la demande de M. A ;
              
     
              Vu les autres pièces du dossier ;
              
              Vu la loi n° 59-1479 du 28 décembre 1959 ;
              
              Vu le décret n° 65&#143;836 du 24 septembre 1965 modifié, dans sa rédaction issue du décret n° 2002&#143;583 du 24 avril 2002 ;
              
              Vu le code de justice administrative ;
              
     
              Après avoir entendu en séance publique :
              
              - le rapport de Mme Claire Legras, Maître des Requêtes,  
              
              - les observations de Me Odent, avocat de la CAISSE DES DEPOTS ET CONSIGNATIONS et de Me Blanc, avocat de M. A, 
              
              - les conclusions de M. Pierre Collin, Commissaire du gouvernement ;
     
     <br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A, ancien ouvrier électronicien à la DCAN de Lorient, a été admis à la retraite le 4 mai 1987 et a ensuite opté le 21 août 1987 pour la liquidation de ses droits à pension en faveur d'une pension ouvrière, comme lui en ouvraient la possibilité les dispositions de l'article unique de la loi susvisée du 28 décembre 1959 ; que, par suite, le montant de sa pension résultait de l'application des dispositions de l'article 9 du décret du 24 septembre 1965 relatif au régime des pensions des ouvriers des établissements industriels de l'Etat et faisait intervenir le salaire horaire de référence correspondant à la catégorie professionnelle de l'ouvrier au moment de sa radiation des cadres et un forfait horaire fixé à 1 960 heures à la date d'ouverture de ses droits à pension ; que l'article 2 du décret du 24 avril 2002 a fixé ce forfait à 1 759 heures, pour prendre en compte l'incidence de la réduction du temps de travail dans le calcul des pensions des ouvriers des établissements industriels de l'Etat ; que, par le jugement attaqué du 15 juillet 2005, le tribunal administratif de Bordeaux a annulé la décision implicite de rejet opposée par la CAISSE DES DEPOTS ET CONSIGNATIONS à la demande de M. A tendant à ce que, pour le calcul de sa pension, le forfait soit maintenu à 1 960 heures ;
              
              Considérant qu'après avoir relevé qu'il résultait de l'article 3 du décret du 24 avril 2002 que les dispositions de son article 1er relatives au forfait ne s'appliquaient qu'à compter du 1er janvier 2002, le tribunal a estimé que ce texte ne saurait, sauf au prix d'une rétroactivité illégale, avoir pour effet de régir la situation des personnes dont les droits à pension avaient été ouverts avant cette date ; que, toutefois, il ressort des pièces du dossier soumis aux premiers juges que la modification du forfait horaire n'est que la contrepartie de la hausse du salaire horaire de référence décidée, à l'occasion de la mise en oeuvre de la réduction de 39 à 35 heures de la durée hebdomadaire du travail, pour garantir le niveau de rémunération antérieur des ouvriers comme celui des pensions servies par le Fonds spécial des pensions des ouvriers des établissements industriels de l'Etat ; qu'il est constant que cette modification du forfait horaire, si elle s'applique aux pensions liquidées avant l'entrée en vigueur du décret du 24 avril 2002, n'a pas d'incidence sur le montant des droits servis ; qu'il suit de là que la CAISSE DES DEPOTS ET CONSIGNATIONS est fondée à soutenir que le tribunal administratif a entaché son jugement d'erreur de droit et à en demander l'annulation ;
              
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond par application des dispositions de l'article L. 821-2 du code de justice administrative ;
              
              Sans qu'il soit besoin de statuer sur la recevabilité de la requête de M. A ;
              
              Considérant qu'ainsi qu'il a été dit ci-dessus, la fixation à 1 759 heures au lieu de 1 960 heures du forfait horaire pris en compte, en application des dispositions de l'article 9 du décret du 24 septembre 1965 relatif au régime des pensions des ouvriers des établissements industriels de l'Etat, pour le calcul des pensions liquidées avant l'entrée en vigueur du décret du 24 avril 2002, demeure sans incidence sur le montant des prestations servies aux personnes bénéficiant de ces pensions ; qu'il suit de là que M. A n'est pas fondé à soutenir que la décision implicite de rejet opposée par la CAISSE DES DEPOTS ET CONSIGNATIONS à sa demande tendant à ce que lui soit confirmé le maintien du forfait de 1 960 heures pour le calcul de sa pension d'ouvrier d'Etat serait entachée d'erreur de droit ;
              
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :
              
              Considérant que, dans les circonstances de l'espèce, il n'y a pas lieu de faire application de ces dispositions et de mettre à la charge de M. A la somme que demande la CAISSE DES DEPOTS ET CONSIGNATIONS au titre des frais exposés par elle et non compris dans les dépens ; que ces dispositions font par ailleurs obstacle à ce que soit mise à la charge de la CAISSE DES DEPOTS ET CONSIGNATIONS, qui n'est pas, dans la présente instance, la partie perdante, la somme que demande M. A au même titre ;
              
     
     <br/>D E C I D E :
              --------------
              
Article 1er : Le jugement du tribunal administratif de Bordeaux en date du 15 juillet 2005 est annulé.
Article 2 : La requête présentée par M. A devant le tribunal administratif de Bordeaux est rejetée.
Article 3 : Les conclusions de la CAISSE DES DEPOTS ET CONSIGNATIONS et de M. A tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.
Article 4 : La présente décision sera notifiée à la CAISSE DES DEPOTS ET CONSIGNATIONS et à M. Lucien A.
     
     
     
     <br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
