<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036694110</ID>
<ANCIEN_ID>JG_L_2018_03_000000406877</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/69/41/CETATEXT000036694110.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 09/03/2018, 406877, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-03-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>406877</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Pauline Jolivet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:406877.20180309</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 16 janvier 2017 et 25 avril 2017 au secrétariat du contentieux du Conseil d'Etat, M. B... A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 23 décembre 2016 par laquelle la Commission nationale de l'informatique et des libertés  (CNIL) a clôturé sa plainte relative à la communication de la liste des traitements automatisés de données à caractère personnel déclarés à la CNIL par le CIC EST ;<br/>
<br/>
              2°) d'annuler le refus de la CNIL de mettre en oeuvre le pouvoir qu'elle tient du III de l'article 22 de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés et de l'article 52 du décret du 20 octobre 2005 pris pour l'application de cette loi à l'égard du " correspondant informatique et libertés " du CIC EST ; <br/>
<br/>
              3°) de transmettre le litige et les faits en question au procureur de la République en application de l'article 40 du code de procédure pénale.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n°78-17 du 6 janvier 1978 ;<br/>
              - le décret nº 2005-1309 du 20 octobre 2005  ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Jolivet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 12 février 2018, présentée par M.A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier que, par une plainte déposée le 6 juin 2016, M. A...a demandé à la  Commission nationale de l'informatique et des libertés  (CNIL) que soit destitué le " correspondant informatique et libertés " de la banque CIC EST au motif qu'il n'aurait pas respecté ses obligations et que lui soit communiquée la liste complète des traitements des données déclarés par cette banque auprès d'elle. Par un courrier du 23 décembre 2016, la présidente de la CNIL a indiqué à M. A..., d'une part que l'ensemble des traitements automatisés de données à caractère personnel déclarés auprès de la CNIL lui avait été communiqué, ce que le requérant ne conteste plus et, d'autre part, qu'elle ne donnait pas suite à sa demande de destitution du " correspondant informatique et libertés " de cette banque, au motif qu'elle ne disposait d'aucun élément permettant de constater qu'il avait manqué aux devoirs de sa mission. La CNIL a fait savoir à l'intéressé qu'en conséquence, elle clôturait la plainte dont il l'avait saisi. M. A... demande l'annulation pour excès de pouvoir de cette décision. <br/>
<br/>
Sur les conclusions tendant à l'annulation de la décision de la CNIL de clôturer la plainte de M. A... :<br/>
<br/>
              2. L'article 11 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés dispose que : " la Commission nationale de l'informatique et des libertés est une autorité administrative indépendante. Elle exerce les missions suivantes : / (...) 2° Elle veille à ce que les traitements de données à caractère personnel soient mis en oeuvre conformément aux dispositions de la présente loi. / A ce titre, / (...) c) Elle reçoit les réclamations, pétitions et plaintes relatives à la mise en oeuvre des traitements de données à caractère personnel et informe leurs auteurs des suites données à celles-ci. (...) ". Il résulte de ces dispositions qu'il appartient à la CNIL, lorsqu'elle est saisie d'une demande tendant à la mise en oeuvre de ses pouvoirs, de procéder à l'examen des faits qui en sont à l'origine et de décider des suites à leur donner. Elle dispose, à cet effet, d'un large pouvoir d'appréciation et peut tenir compte de l'ensemble des intérêts généraux dont elle a la charge. <br/>
<br/>
              3. En premier lieu, il ressort des pièces du dossier que la CNIL, qui a instruit la plainte qui lui avait été adressée conformément à ces dispositions et s'est notamment assurée que le requérant avait bien été rendu destinataire de la liste complète des traitements de données le concernant auxquels procède le CIC EST, n'a pas commis d'erreur manifeste d'appréciation, en ne donnant pas suite, dans cette mesure, à la plainte dont elle avait été saisie. <br/>
<br/>
              4. En second lieu, il ressort des pièces du dossier que M.  A... est en conflit avec le CIC EST, auquel il reproche en particulier de ne pas l'avoir suffisamment alerté des risques qu'il prenait en souscrivant un prêt auprès de lui. Il a demandé à la CNIL de faire application de l'article 22 de la loi du 6 janvier 1978 et de décharger de ses fonctions le correspondant à la protection des données à caractère personnel dit " correspondant informatique et libertés " de cette banque au motif qu'il aurait manqué à son devoir d'information et de mise en garde obligeant les établissements financiers à vérifier l'aptitude d'un client à rembourser un crédit consenti au regard de ses capacités financières. <br/>
<br/>
              5. Aux termes du III de l'article 22 de la loi du 6 janvier 1978 : " Les traitements pour lesquels le responsable a désigné un correspondant à la protection des données à caractère personnel chargé d'assurer, d'une manière indépendante, le respect des obligations prévues dans la présente loi sont dispensés des formalités prévues aux articles 23 et 24, sauf lorsqu'un transfert de données à caractère personnel à destination d'un Etat non membre de la Communauté européenne est envisagé. [...] Le correspondant est une personne bénéficiant des qualifications requises pour exercer ses missions. Il tient une liste des traitements effectués immédiatement accessible à toute personne en faisant la demande et ne peut faire l'objet d'aucune sanction de la part de l'employeur du fait de l'accomplissement de ses missions. Il peut saisir la Commission nationale de l'informatique et des libertés des difficultés qu'il rencontre dans l'exercice de ses missions. En cas de non-respect des dispositions de la loi, le responsable du traitement est enjoint par la Commission nationale de l'informatique et des libertés de procéder aux formalités prévues aux articles 23 et 24. En cas de manquement constaté à ses devoirs, le correspondant est déchargé de ses fonctions sur demande, ou après consultation, de la Commission nationale de l'informatique et des libertés. " L'article 52 du décret du 20 octobre 2005 pris pour l'application de la loi informatique et libertés dispose que : " Lorsque la Commission nationale de l'informatique et des libertés constate, après avoir recueilli ses observations, que le correspondant manque aux devoirs de sa mission, elle demande au responsable des traitements de le décharger de ses fonctions (...) ".<br/>
<br/>
              6. Dès lors que l'information des clients d'un établissement bancaire quant au risque financier qu'ils prennent en recourant à l'emprunt ne relève pas des devoirs du correspondant à la protection des données à caractère personnel de cet établissement, la CNIL n'a pas commis d'erreur manifeste d'appréciation en clôturant la plainte de M. A...qui tendait à ce qu'elle demande à ce qu'il soit déchargé de ses fonctions en application des dispositions précitées et en refusant d'engager la procédure prévue à cet effet.<br/>
<br/>
Sur les conclusions à fin d'application par le Conseil d'Etat des dispositions du deuxième alinéa de l'article 40 du code de procédure pénale: <br/>
<br/>
              7. Il n'appartient pas au Conseil d'Etat, statuant au contentieux, de faire application des dispositions du deuxième alinéa de l'article 40 du code de procédure pénale. <br/>
<br/>
              8. Il résulte de tout ce qui précède que la requête de M. A...doit être rejetée.<br/>
<br/>
<br/>
<br/>
<br/>
              D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à Monsieur  B...A..., à la Commission nationale de l'informatique et des libertés et au CIC EST.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
