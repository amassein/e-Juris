<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032491578</ID>
<ANCIEN_ID>JG_L_2016_05_000000370882</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/49/15/CETATEXT000032491578.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 04/05/2016, 370882, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370882</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:370882.20160504</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une décision du 27 octobre 2015, le Conseil d'Etat, statuant au contentieux, saisi de la requête du comité d'établissement de l'unité " Clients et Fournisseurs " Ile-de-France des sociétés Electricité Réseau Distribution France (ERDF) et Gaz Réseau Distribution France (GRDF) tendant à l'annulation de l'arrêt n° 12PA02860 du 6 juin 2013 par lequel la cour administrative d'appel de Paris a fait droit à l'appel formé par les sociétés ERDF et GRDF contre le jugement nos 1201739, 1201743, 1201747du 7 juin 2012 du tribunal administratif de Paris annulant les décisions du 19 décembre 2011 des directeurs des unités " Clients et Fournisseurs " de Paris, de l'Ouest de l'Ile-de-France et de l'Est de l'Ile-de-France relatives à la réorganisation de la fonction " accueil acheminement ", a sursis à statuer jusqu'à ce que le Tribunal des conflits ait tranché la question de savoir quel est l'ordre de juridiction compétent pour connaître de ces requêtes.<br/>
<br/>
              Par une décision n° 4038 du 11 janvier 2016, le Tribunal des conflits a déclaré la juridiction administrative seule compétente pour connaître de l'action intentée par le requérant contre les décisions du 19 décembre 2011 des directeurs des unités " Clients et Fournisseurs " Ile-de-France des sociétés Electricité Réseau Distribution France (ERDF) et Gaz Réseau Distribution France (GRDF).<br/>
<br/>
<br/>
              Vu les autres pièces du dossier, y compris celles visées par la décision du Conseil d'Etat du 27 octobre 2015 ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code de l'énergie ;<br/>
              - la décision n° 4038 du 11 janvier 2016 du Tribunal des conflits ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bastien Lignereux, auditeur,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat du comité d'établissement de l'unité " Clients et Fournisseurs " Ile-de-France des sociétés Electricité Réseau Distribution France (ERDF) et Gaz Réseau Distribution France (GRDF) et à la SCP Coutard, Munier-Apaire, avocat de la société Électricité Réseau distribution France (ERDF) et de la société Gaz réseau distribution France (GRDF) ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Sur renvoi effectué par la décision du Conseil d'Etat statuant au contentieux du 27 octobre 2015 visée ci-dessus, le Tribunal des conflits, par une décision du 11 janvier 2016, a déclaré la juridiction administrative seule compétente pour connaître de l'action intentée par le comité d'établissement de l'unité " Clients et Fournisseurs Ile-de-France " des sociétés Electricité Réseau Distribution France (ERDF) et Gaz Réseau Distribution France (GRDF) contre les décisions du 19 décembre 2011 des directeurs des unités " Clients et Fournisseurs " Ile-de-France de ces sociétés.<br/>
<br/>
              2. Aux termes de l'article L. 111-71 du code de l'énergie : " Electricité de France et GDF-Suez, ainsi que leurs filiales, peuvent, par convention, créer des services communs dotés ou non de la personnalité morale. / La création d'un service commun, non doté de la personnalité morale, entre les sociétés issues de la séparation juridique des activités exercées par Electricité de France et GDF-Suez en application de l'article L. 111-57 est obligatoire, dans le secteur de la distribution, pour la construction des ouvrages, la maîtrise d'oeuvre de travaux, l'exploitation et la maintenance des réseaux, les opérations de comptage ainsi que d'autres missions afférentes à ces activités (...) / Chacune des sociétés assume les conséquences de ses activités propres dans le cadre des services communs non dotés de la personnalité morale. / Les coûts afférents aux activités relevant de chacune des sociétés sont identifiés dans la comptabilité des services communs (...) ".<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que, par trois décisions du 19 décembre 2011, les directeurs des " Unités clients et fournisseurs " des " plaques " de Paris, de l'Ouest de l'Ile-de-France et de l'Est de l'Ile-de-France du service commun aux deux sociétés ont décidé la mise en oeuvre d'une réorganisation tendant à la spécialisation, par type d'énergie, des cinq services " accueil-acheminement " de leurs unités respectives. Les services " accueil-acheminement " assurent l'interface entre les fournisseurs d'électricité ou de gaz, qui leur transmettent les demandes de leurs clients, et les entités opérationnelles chargées de réaliser les interventions techniques, notamment pour le raccordement et les opérations de comptage. Les tâches dévolues à ces services d'acheminement comprennent notamment l'accueil et l'orientation des demandes des fournisseurs, la facturation des prestations opérées à leur profit, le contrôle, la validation et la rectification des données de comptage pour l'électricité et le gaz et, enfin, le traitement de certaines réclamations. Le comité d'établissement " Clients et Fournisseurs " Ile-de-France des sociétés ERDF et GRDF se pourvoit en cassation contre l'arrêt du 6 juin 2013 par lequel la cour administrative d'appel de Paris a, sur l'appel de ces sociétés, annulé le jugement du 7 juin 2012 du tribunal administratif de Paris et rejeté sa demande dirigée contre les trois décisions du 19 décembre 2011.<br/>
<br/>
              4. En premier lieu, aux termes de l'article R. 611-1 du code de justice administrative : " (...) La requête, le mémoire complémentaire annoncé dans la requête et le premier mémoire de chaque défendeur sont communiqués aux parties avec les pièces jointes (...). / Les répliques, autres mémoires et pièces sont communiqués s'ils contiennent des éléments nouveaux. ". La cour administrative d'appel n'a pas méconnu ces dispositions en ne communiquant pas un mémoire en réplique produit par les sociétés ERDF et GRDF et enregistré après la clôture de l'instruction, dès lors qu'il ne contenait aucun élément nouveau et qu'il ne ressort pas des énonciations de l'arrêt attaqué que la cour, qui l'a visé sans l'analyser, en ait tenu compte.<br/>
<br/>
              5. En deuxième lieu, la cour a relevé que les dispositions de l'article L. 111-71 du code de l'énergie citées au point 2 n'imposaient aucun principe d'organisation des services communs facultatifs, librement institués par convention entre les sociétés EDF et GDF ou leurs filiales, ni du service commun obligatoire, lequel ne concerne que certaines missions, limitativement énumérées, dans le secteur de la distribution. Elle en a déduit que, sans qu'il soit besoin de recourir aux travaux préparatoires de la loi, il ne résultait pas de cet article que le service commun, qu'il soit obligatoire ou facultatif, implique nécessairement la mixité des activités et la polyvalence des agents et des sites sur les deux types d'énergie. Elle a jugé, en conséquence, que la spécialisation des emplois prévue par les décisions litigieuses n'était pas incompatible par elle-même avec le service commun défini par l'article L. 111-71 du code de l'énergie. En statuant ainsi, la cour, qui a suffisamment motivé son arrêt, n'a pas commis d'erreur de droit.<br/>
<br/>
              6. En troisième lieu, les motifs par lesquels la cour a jugé, d'une part, que les missions dont les services " accueil acheminement " avaient la charge ne pouvaient être rattachées à aucune des activités mentionnées au deuxième alinéa de l'article L. 111-71 comme faisant partie du service commun obligatoire, notamment pas à l'exploitation et à la maintenance des réseaux, et d'autre part, que le service commun, qui compte 60 000 salariés alors que le projet en litige concerne moins de 500 agents, ne saurait se résumer à des activités " mixtes " dès lors qu'antérieurement à l'intervention des mesures contestées, certaines de ses activités étaient déjà majoritairement " mono-énergie ", présentent un caractère surabondant. Dès lors, les moyens du pourvoi dirigés contre ces motifs doivent être écartés comme inopérants.<br/>
<br/>
              7. En quatrième lieu, en jugeant que le comité d'établissement ne démontrait pas, et qu'il n'était pas établi par les pièces du dossier, que la spécialisation des équipes et des sites aurait un impact négatif sur les synergies existantes et le service de proximité, et en jugeant qu'il ressortait des écritures des sociétés ERDF et GRDF, non sérieusement contestées par le comité d'établissement, que la réorganisation devait permettre d'améliorer la qualité du service rendu aux clients en termes d'efficacité et de professionnalisme et qu'elle aurait des effets favorables en termes de coût, la cour n'a ni entaché son arrêt de dénaturation des faits, ni inversé la charge de la preuve. Elle n'a pas davantage méconnu l'article 6, paragraphe 1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.<br/>
<br/>
              8. Enfin, la cour a jugé, au terme d'une appréciation souveraine des faits exempte de dénaturation, qu'aucune irrégularité dans la consultation des comités d'hygiène, de sécurité et des conditions de travail concernés par le projet de réorganisation litigieux ne pouvait être reprochée aux sociétés ERDF et GRDF, et que l'information dont avait bénéficié le comité d'établissement lui avait permis d'émettre valablement un avis motivé. Elle n'a pas commis d'erreur de droit en jugeant que la circonstance que ce comité ait refusé de formuler un avis sur le projet était sans incidence sur la régularité de sa consultation.<br/>
<br/>
              9. Il résulte de ce qui précède que le pourvoi du comité d'établissement " Clients et Fournisseurs " Ile-de-France des sociétés ERDF et GRDF doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées à ce titre par les sociétés ERDF et GRDF. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Le pourvoi du comité d'établissement " Clients et Fournisseurs " Ile-de-France des sociétés ERDF et GRDF est rejeté.<br/>
Article 2 : Les conclusions présentées par les sociétés ERDF et GRDF au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée au comité d'établissement " Clients et Fournisseurs " Ile-de-France des sociétés ERDF et GRDF, à la société Électricité Réseau Distribution France et à la société Gaz Réseau Distribution France.<br/>
Copie en sera adressée à la ministre de l'environnement, de l'énergie et de la mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
