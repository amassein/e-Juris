<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044271061</ID>
<ANCIEN_ID>JG_L_2021_10_000000445699</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/27/10/CETATEXT000044271061.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 28/10/2021, 445699</TITRE>
<DATE_DEC>2021-10-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445699</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Fabio Gennari</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:445699.20211028</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 26 octobre 2020 au secrétariat du contentieux du Conseil d'Etat, la société En avant Guingamp demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les décisions du 24 septembre 2020 par lesquelles le conseil d'administration de la Ligue de football professionnel a, d'une part, rejeté sa demande tendant à bénéficier de l'aide à la relégation additionnelle triplée pour sa deuxième année de relégation en 2020/2021 et, d'autre part, adopté le guide de répartition des droits audiovisuels 2020/2021 en tant qu'il ne prévoit pas l'application de l'aide variable additionnelle aux clubs relégués en Ligue 2 à l'issue de la saison 2018/2019, subsidiairement d'annuler le guide de répartition des droits audiovisuels 2020/2021 dans son ensemble ; <br/>
<br/>
              2°) de mettre à la charge de la Ligue de football professionnel la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du sport ;  <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabio Gennari, auditeur,<br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteure publique,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Foussard, Froger, avocat de la société En Avant Guingamp et à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la Ligue de football professionnel ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 333-1 du code du sport : " Les fédérations sportives, ainsi que les organisateurs de manifestations sportives mentionnés à l'article L. 331-5, sont propriétaires du droit d'exploitation des manifestations ou compétitions sportives qu'ils organisent./ Toute fédération sportive peut céder aux sociétés sportives, à titre gratuit, la propriété de tout ou partie des droits d'exploitation audiovisuelle des compétitions ou manifestations sportives organisées chaque saison sportive par la ligue professionnelle qu'elle a créée, dès lors que ces sociétés participent à ces compétitions ou manifestations sportives. La cession bénéficie alors à chacune de ces sociétés ". Aux termes de l'article L. 333-2 du même code : " Les droits d'exploitation audiovisuelle cédés aux sociétés sportives sont commercialisés par la ligue professionnelle dans des conditions et limites précisées par décret en Conseil d'Etat (...) ". Aux termes de l'article L. 333-3 du même code : " Afin de garantir l'intérêt général et les principes d'unité et de solidarité entre les activités à caractère professionnel et les activités à caractère amateur, les produits de la commercialisation par la ligue des droits d'exploitation des sociétés sont répartis entre la fédération, la ligue et les sociétés./ La part de ces produits destinée à la fédération et celle destinée à la ligue sont fixées par la convention passée entre la fédération et la ligue professionnelle correspondante./ Les produits revenant aux sociétés leur sont redistribués selon un principe de mutualisation, en tenant compte de critères arrêtés par la ligue et fondés notamment sur la solidarité existant entre les sociétés, ainsi que sur leurs performances sportives et leur notoriété ".<br/>
<br/>
              2. Le guide de répartition des droits audiovisuels pour la saison 2019-2020 adopté par la Ligue de football professionnel prévoyait, au profit des clubs relégués de Ligue 1 en Ligue 2, une aide fixe de 2 000 000 d'euros la première saison et de 1 000 000 d'euros pour la deuxième saison, ainsi qu'une aide variable additionnelle, versée uniquement la première saison, d'un montant de 500 000 euros par saison immédiatement consécutive passée en Ligue 1 au cours des dix dernières années et de 250 000 euros par saison non consécutive. Par une décision du 24 septembre 2020, le conseil d'administration de la Ligue de football professionnel a adopté les guides de répartition des droits audiovisuels relatifs à la Ligue 1 et à la Ligue 2 pour la saison 2020-2021. Le guide de répartition des droits audiovisuels pour la Ligue 1 issu de cette décision maintient le dispositif d'aide fixe mais prévoit, d'une part, que les clubs relégués de Ligue 1 en Ligue 2 à l'issue de la saison 2019-2020 percevront, lors de la première saison de relégation, soit en 2020-2021, une aide variable additionnelle d'un montant triplé, soit 1 500 000 euros par saison immédiatement consécutive passée en Ligue 1 au cours des dix dernières années et 750 000 euros par saison non consécutive et, d'autre part, qu'ils bénéficieront de cette aide variable additionnelle lors de la deuxième saison de relégation, soit en 2021/2022, à hauteur de 50 % du montant perçu lors de la première saison.<br/>
<br/>
              3. La société En avant Guingamp demande au Conseil d'Etat d'annuler la décision du 24 septembre 2020 du conseil d'administration de la Ligue de football professionnel adoptant le guide de répartition des droits audiovisuels 2020/2021 en tant qu'elle ne prévoit pas l'application de l'aide variable additionnelle aux clubs relégués en Ligue 2 à l'issue de la saison 2018/2019 et en tant qu'elle rejette sa demande tendant à bénéficier de l'aide à la relégation additionnelle triplée pour sa deuxième année de relégation en 2020/2021.<br/>
<br/>
              4. Si la Ligue de football professionnel, personne morale de droit privée, s'est vu confier, par convention conclue avec la Fédération française, la gestion du football professionnel, notamment l'organisation et la règlementation des championnats de Ligue 1 et de Ligue 2, et est à ce titre chargée d'une mission de service public administratif, les actes et décisions pris par elle ne ressortissent à la compétence de la juridiction administrative que pour autant qu'ils constituent l'exercice d'une prérogative de puissance publique pour l'accomplissement de cette mission. Il n'en va pas ainsi des décisions contestées, qui se rattachent à l'activité de la Ligue de commercialisation des droits d'exploitation audiovisuelle et qui concernent la redistribution des produits en résultant entre les sociétés sportives propriétaires de ces droits. Par suite, les conclusions aux fins d'annulation présentées par la société En avant Guingamp doivent être rejetées comme portées devant un ordre de juridiction incompétent pour en connaître. <br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la Ligue de football professionnel, qui n'est pas, dans la présente instance, la partie perdante, une somme à ce titre. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de la société requérante le versement d'une somme au même titre. <br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société En Avant Guingamp est rejetée comme portée devant une juridiction incompétente pour en connaître.<br/>
Article 2 : Les conclusions présentées par la Ligue de football professionnel au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la société En Avant Guingamp et à la Ligue de football professionnel.<br/>
Copie en sera adressée au ministre de l'éducation nationale, de la jeunesse et des sports.<br/>
              Délibéré à l'issue de la séance du 8 octobre 2021 où siégeaient : M. Jacques-Henri Stahl, président adjoint de la section du contentieux, présidant ; M. G... I..., M. Olivier Japiot, présidents de chambre ; Mme A... J..., M. C... F..., M. D... L..., M. E... K..., M. Jean-Yves Ollier, conseillers d'Etat et M. Fabio Gennari, auditeur-rapporteur. <br/>
<br/>
              Rendu le 28 octobre 2021.<br/>
<br/>
<br/>
                 Le Président : <br/>
                 Signé : M. Jacques-Henri Stahl<br/>
 		Le rapporteur : <br/>
      Signé : M. Fabio Gennari<br/>
                 La secrétaire :<br/>
                 Signé : Mme H... B...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03-02-07-04 COMPÉTENCE. - RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. - COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. - PROBLÈMES PARTICULIERS POSÉS PAR CERTAINES CATÉGORIES DE SERVICES PUBLICS. - ORGANISME PRIVÉ GÉRANT UN SERVICE PUBLIC. - DÉCISIONS DE LA LFP CONCERNANT LA REDISTRIBUTION DES PRODUITS RÉSULTANT DE LA COMMERCIALISATION DES DROITS D'EXPLOITATION AUDIOVISUELLE ENTRE LES SOCIÉTÉS SPORTIVES PROPRIÉTAIRES DE CES DROITS - EXERCICE DE PRÉROGATIVES DE PUISSANCE PUBLIQUE - ABSENCE - CONSÉQUENCE - INCOMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">63-05-01 SPORTS ET JEUX. - SPORTS. - FÉDÉRATIONS SPORTIVES. - DÉCISIONS DE LA LFP CONCERNANT LA REDISTRIBUTION DES PRODUITS RÉSULTANT DE LA COMMERCIALISATION DES DROITS D'EXPLOITATION AUDIOVISUELLE ENTRE LES SOCIÉTÉS SPORTIVES PROPRIÉTAIRES DE CES DROITS - EXERCICE DE PRÉROGATIVES DE PUISSANCE PUBLIQUE - ABSENCE - CONSÉQUENCE - INCOMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE [RJ1].
</SCT>
<ANA ID="9A"> 17-03-02-07-04 Si la Ligue de football professionnel (LFP), personne morale de droit privée, s'est vu confier, par convention conclue avec la Fédération française, la gestion du football professionnel, notamment l'organisation et la règlementation des championnats de Ligue 1 et de Ligue 2, et est à ce titre chargée d'une mission de service public administratif, les actes et décisions pris par elle ne ressortissent à la compétence de la juridiction administrative que pour autant qu'ils constituent l'exercice d'une prérogative de puissance publique pour l'accomplissement de cette mission. ......Il n'en va pas ainsi des décisions par lesquelles le conseil d'administration de la LFP adopte les guides de répartition des droits audiovisuels relatifs à la Ligue 1 et à la Ligue 2 pour la saison 2020-2021, qui se rattachent à l'activité de la Ligue de commercialisation des droits d'exploitation audiovisuelle et qui concernent la redistribution des produits en résultant entre les sociétés sportives propriétaires de ces droits. ......Par suite, les conclusions aux fins d'annulation de ces décisions ne ressortissent pas à la compétence de la juridiction administrative.</ANA>
<ANA ID="9B"> 63-05-01 Si la Ligue de football professionnel (LFP), personne morale de droit privée, s'est vu confier, par convention conclue avec la Fédération française, la gestion du football professionnel, notamment l'organisation et la règlementation des championnats de Ligue 1 et de Ligue 2, et est à ce titre chargée d'une mission de service public administratif, les actes et décisions pris par elle ne ressortissent à la compétence de la juridiction administrative que pour autant qu'ils constituent l'exercice d'une prérogative de puissance publique pour l'accomplissement de cette mission. ......Il n'en va pas ainsi des décisions par lesquelles le conseil d'administration de la LFP adopte les guides de répartition des droits audiovisuels relatifs à la Ligue 1 et à la Ligue 2 pour la saison 2020-2021, qui se rattachent à l'activité de la Ligue de commercialisation des droits d'exploitation audiovisuelle et qui concernent la redistribution des produits en résultant entre les sociétés sportives propriétaires de ces droits. ......Par suite, les conclusions aux fins d'annulation de ces décisions ne ressortissent pas à la compétence de la juridiction administrative.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. sol. contr., s'agissant des décisions d'une fédération sportive relative à l'organisation de compétitions nationales ou régionales, CE, Section, 22 novembre 1974, Fédération des industries françaises de sports, n° 89828, p. 576 ; s'agissant d'une décision d'une fédération définissant des règles de la pratique sportive, CE, 9 novembre 1994, Association para-club de Reims et Centre de parachutisme de la Marne, n° 85934, T. p. 1207.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
