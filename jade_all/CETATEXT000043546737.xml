<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043546737</ID>
<ANCIEN_ID>JG_L_2021_05_000000441660</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/54/67/CETATEXT000043546737.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 27/05/2021, 441660</TITRE>
<DATE_DEC>2021-05-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441660</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Rose-Marie Abel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:441660.20210527</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 8 juillet 2020 et 30 avril 2021 au secrétariat du contentieux du Conseil d'Etat, l'association Compassion in World Farming France (CIWF) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision implicite par laquelle le Premier ministre a refusé d'édicter le décret prévu par l'article L. 214-11 du code rural et de la pêche maritime ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre de prendre ce décret dans un délai de six mois, sous astreinte de 200 euros par jour de retard.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment, son article 21 ;<br/>
              - le code de l'environnement ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - la loi n° 2018-938 du 30 octobre 2018 ;<br/>
              - le code de justice administrative et le décret 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Rose-Marie Abel, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteure publique ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Par une lettre reçue le 23 décembre 2019, l'association CWIF France a demandé au Premier ministre de prendre les mesures nécessaires à la mise en oeuvre de l'article L. 214-11 du code rural et de la pêche maritime, dans sa rédaction résultant de l'article 66 de la loi du 30 octobre 2018 pour l'équilibre des relations commerciales dans le secteur agricole et alimentaire et une alimentation saine, durable et accessible à tous. Elle demande l'annulation pour excès de pouvoir du refus implicite qui lui a été opposé, résultant du silence gardé pendant plus de deux mois sur sa demande.<br/>
<br/>
              Sur les conclusions à fin d'annulation :<br/>
<br/>
              2. En vertu de l'article 21 de la Constitution, le Premier ministre " assure l'exécution des lois " et " exerce le pouvoir réglementaire " sous réserve de la compétence conférée au Président de la République pour les décrets en Conseil des ministres par l'article 13 de la Constitution. L'exercice du pouvoir réglementaire comporte non seulement le droit mais aussi l'obligation de prendre dans un délai raisonnable les mesures qu'implique nécessairement l'application de la loi, hors le cas où le respect d'engagements internationaux de la France y ferait obstacle.<br/>
<br/>
              3. L'effet utile de l'annulation pour excès de pouvoir du refus du pouvoir réglementaire de prendre les mesures qu'implique nécessairement l'application de la loi réside dans l'obligation, que le juge peut prescrire d'office en vertu des dispositions de l'article L. 911-1 du code de justice administrative, pour le pouvoir réglementaire, de prendre ces mesures. Il s'ensuit que lorsqu'il est saisi de conclusions aux fins d'annulation du refus d'une autorité administrative d'édicter les mesures nécessaires à l'application d'une disposition législative, le juge de l'excès de pouvoir est conduit à apprécier la légalité d'un tel refus au regard des règles applicables et des circonstances prévalant à la date de sa décision.<br/>
<br/>
              4. En premier lieu, l'article L. 214-11 du code rural et de la pêche maritime, dans sa rédaction résultant de l'article 66 de la loi du 30 octobre 2018, prévoit que  : " La mise en production de tout bâtiment nouveau ou réaménagé d'élevage de poules pondeuses élevées en cages est interdite à compter de l'entrée en vigueur de la loi n° 2018-938 du 30 octobre 2018 pour l'équilibre des relations commerciales dans le secteur agricole et alimentaire et une alimentation saine, durable et accessible à tous. / Les modalités d'application du présent article sont définies par décret. " Eu égard à l'incertitude affectant en l'espèce la portée de la notion de " bâtiment réaménagé " d'élevage de poules pondeuses, au sens de ces dispositions législatives en interdisant la mise en production, ces dispositions ne sont pas suffisamment précises pour permettre leur entrée en vigueur en l'absence du décret d'application dont elles prévoient d'ailleurs l'intervention. <br/>
<br/>
              5. En second lieu, à la date de la présente décision, il s'est écoulé plus de deux ans et demi depuis l'entrée en vigueur de la loi du 30 octobre 2018 pour l'équilibre des relations commerciales dans le secteur agricole et alimentaire et une alimentation saine, durable et accessible à tous. Le retard dans l'adoption des dispositions réglementaires nécessaires à l'application de l'article L. 214-11 du code rural et de la pêche maritime, issu de cette loi, excède ainsi le délai raisonnable qui était imparti au pouvoir réglementaire pour prendre le décret prévu à cet article.<br/>
<br/>
              6. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur l'autre moyen de la requête, que l'association CIWF France est fondée à soutenir que la décision du Premier ministre refusant de prendre le décret d'application de l'article L. 214-11 du code rural et de la pêche maritime est illégale et doit être annulée.<br/>
<br/>
              Sur les conclusions à fin d'injonction sous astreinte :<br/>
<br/>
              7. Aux termes de l'article L. 911-1 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution. / La juridiction peut également prescrire d'office cette mesure. ". Aux termes de l'article L. 911-3 du même code : " La juridiction peut assortir, dans la même décision, l'injonction prescrite (...) d'une astreinte (...) dont elle fixe la date d'effet ".<br/>
<br/>
              8. L'annulation de la décision refusant de prendre le décret mentionné à l'article L. 214-11 du code rural et de la pêche maritime implique nécessairement l'édiction de ce décret. Il y a donc lieu pour le Conseil d'Etat d'enjoindre au Premier ministre de prendre ce décret dans un délai de six mois à compter de la notification de la présente décision et, dans les circonstances de l'espèce, de prononcer à l'encontre de l'Etat, à défaut pour le Premier ministre de justifier de l'édiction de ce décret dans le délai prescrit, une astreinte de 200 euros par jour de retard jusqu'à la date à laquelle la présente décision aura reçu exécution. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La décision implicite par laquelle le Premier ministre a refusé de prendre le décret prévu à l'article L. 214-11 du code rural et de la pêche maritime est annulée.<br/>
Article 2 : Il est enjoint au Premier ministre de prendre le décret prévu à l'article L. 214 - 11 du code rural et de la pêche maritime dans un délai de six mois à compter de la notification de la présente décision.<br/>
Article 3 : Une astreinte de 200 euros par jour est prononcée à l'encontre de l'Etat s'il n'est pas justifié de l'exécution de la présente décision dans le délai mentionné à l'article 2 ci-dessus. Le Premier ministre communiquera au secrétariat du contentieux du Conseil d'Etat copie des actes justifiant des mesures prises pour exécuter la présente décision.<br/>
Article 4 : La présente décision sera notifiée à l'association Compassion in World Farming France, au Premier ministre et au ministre de l'agriculture et de l'alimentation.<br/>
Copie en sera adressée à la section du rapport et des études.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-05-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - MOTIFS. POUVOIRS ET OBLIGATIONS DE L'ADMINISTRATION. - REFUS DU POUVOIR RÉGLEMENTAIRE DE PRENDRE LES MESURES QU'IMPLIQUE NÉCESSAIREMENT L'APPLICATION DE LA LOI [RJ1] - DATE D'APPRÉCIATION DE LA LÉGALITÉ DE LA MESURE - DATE À LAQUELLE LE JUGE STATUE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-08-01-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. APPLICATION DANS LE TEMPS. ENTRÉE EN VIGUEUR. ENTRÉE EN VIGUEUR SUBORDONNÉE À L'INTERVENTION DE MESURES D'APPLICATION. - REFUS DU POUVOIR RÉGLEMENTAIRE DE PRENDRE LES MESURES QU'IMPLIQUE NÉCESSAIREMENT L'APPLICATION DE LA LOI [RJ1] - DATE D'APPRÉCIATION DE LA LÉGALITÉ DE LA MESURE - DATE À LAQUELLE LE JUGE STATUE [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-01-07 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. DEVOIRS DU JUGE. - REFUS DU POUVOIR RÉGLEMENTAIRE DE PRENDRE LES MESURES QU'IMPLIQUE NÉCESSAIREMENT L'APPLICATION DE LA LOI [RJ1] - DATE D'APPRÉCIATION DE LA LÉGALITÉ DE LA MESURE - DATE À LAQUELLE LE JUGE STATUE [RJ2].
</SCT>
<ANA ID="9A"> 01-05-01 L'exercice du pouvoir réglementaire comporte non seulement le droit mais aussi l'obligation de prendre dans un délai raisonnable les mesures qu'implique nécessairement l'application de la loi, hors le cas où le respect d'engagements internationaux de la France y ferait obstacle.,,,L'effet utile de l'annulation pour excès de pouvoir du refus du pouvoir réglementaire de prendre les mesures qu'implique nécessairement l'application de la loi réside dans l'obligation, que le juge peut prescrire d'office en vertu de l'article L. 911 1 du code de justice administrative, pour le pouvoir réglementaire, de prendre ces mesures.... ,,Il s'ensuit que lorsqu'il est saisi de conclusions aux fins d'annulation du refus d'une autorité administrative d'édicter les mesures nécessaires à l'application d'une disposition législative, le juge de l'excès de pouvoir est conduit à apprécier la légalité d'un tel refus au regard des règles applicables et des circonstances prévalant à la date de sa décision, notamment afin de déterminer si le retard dans l'adoption de ces mesures a excédé le délai raisonnable qui était imparti au pouvoir réglementaire pour ce faire.</ANA>
<ANA ID="9B"> 01-08-01-02 L'exercice du pouvoir réglementaire comporte non seulement le droit mais aussi l'obligation de prendre dans un délai raisonnable les mesures qu'implique nécessairement l'application de la loi, hors le cas où le respect d'engagements internationaux de la France y ferait obstacle.,,,L'effet utile de l'annulation pour excès de pouvoir du refus du pouvoir réglementaire de prendre les mesures qu'implique nécessairement l'application de la loi réside dans l'obligation, que le juge peut prescrire d'office en vertu de l'article L. 911 1 du code de justice administrative, pour le pouvoir réglementaire, de prendre ces mesures.... ,,Il s'ensuit que lorsqu'il est saisi de conclusions aux fins d'annulation du refus d'une autorité administrative d'édicter les mesures nécessaires à l'application d'une disposition législative, le juge de l'excès de pouvoir est conduit à apprécier la légalité d'un tel refus au regard des règles applicables et des circonstances prévalant à la date de sa décision, notamment afin de déterminer si le retard dans l'adoption de ces mesures a excédé le délai raisonnable qui était imparti au pouvoir réglementaire pour ce faire.</ANA>
<ANA ID="9C"> 54-07-01-07 L'exercice du pouvoir réglementaire comporte non seulement le droit mais aussi l'obligation de prendre dans un délai raisonnable les mesures qu'implique nécessairement l'application de la loi, hors le cas où le respect d'engagements internationaux de la France y ferait obstacle.,,,L'effet utile de l'annulation pour excès de pouvoir du refus du pouvoir réglementaire de prendre les mesures qu'implique nécessairement l'application de la loi réside dans l'obligation, que le juge peut prescrire d'office en vertu de l'article L. 911 1 du code de justice administrative, pour le pouvoir réglementaire, de prendre ces mesures.... ,,Il s'ensuit que lorsqu'il est saisi de conclusions aux fins d'annulation du refus d'une autorité administrative d'édicter les mesures nécessaires à l'application d'une disposition législative, le juge de l'excès de pouvoir est conduit à apprécier la légalité d'un tel refus au regard des règles applicables et des circonstances prévalant à la date de sa décision, notamment afin de déterminer si le retard dans l'adoption de ces mesures a excédé le délai raisonnable qui était imparti au pouvoir réglementaire pour ce faire.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 28 juillet 2000, Association France nature environnement, n° 204024, p. 322,,[RJ2] Rappr., s'agissant du refus d'abroger un acte réglementaire, CE, Assemblée, 19 juillet 2019, Association des Américains accidentels, n°s 424216 424217, p. 296.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
