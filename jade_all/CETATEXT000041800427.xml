<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041800427</ID>
<ANCIEN_ID>JG_L_2020_04_000000427122</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/80/04/CETATEXT000041800427.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 03/04/2020, 427122, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-04-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427122</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE ; SARL MEIER-BOURDEAU, LECUYER ET ASSOCIES</AVOCATS>
<RAPPORTEUR>Mme Catherine Calothy</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:427122.20200403</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A..., l'Association pour la défense du patrimoine et du paysage de la vallée de la Vingeanne et la Société pour la protection des paysages et de l'esthétique de la France ont demandé au tribunal administratif de Besançon d'annuler pour excès de pouvoir l'arrêté du préfet de la Haute-Saône du 19 février 2015 autorisant la société " Parc éolien des Ecoulottes " à exploiter sept éoliennes et un poste de livraison sur le territoire de la commune de Vars. Par un jugement n° 1501337 du 21 septembre 2017, le tribunal administratif a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 17NC02807 du 22 novembre 2018, la cour administrative d'appel de Nancy a rejeté l'appel formé par M. A... et autres contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 16 janvier et 9 avril 2019 au secrétariat du contentieux du Conseil d'Etat, M. A... et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge solidaire de l'Etat et de la société Parc éolien des Ecoulottes la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 2011/92/UE du 13 décembre 2011 ;<br/>
              - le code de l'aviation civile ;<br/>
              - le code de l'environnement ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de l'urbanisme ;<br/>
              - l'ordonnance n° 2017-80 du 26 janvier 2017 ;<br/>
              - le décret n° 2015-1229 du 2 octobre 2015<br/>
              - le décret n° 2017-81 du 26 janvier 2017 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Calothy, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange, de La Burgade, avocat de M. A... et autres et à la SARL Meier-Bourdeau Lécuyer et associés, avocat de la société " Parc éolien des Ecoulottes " ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 10 mars 2020, présentée par la société " Parc éolien des Ecoulottes ".<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que par un arrêté du 19 février 2015, le préfet de la Haute-Saône a délivré à la société " Parc éolien des Ecoulottes " une autorisation d'exploiter sept éoliennes et un poste de livraison sur le territoire de la commune de Vars. M. A... et autres ont demandé au tribunal administratif de Besançon d'annuler cet arrêté. Le tribunal administratif a rejeté leur demande. La cour administrative d'appel de Nancy a rejeté leur appel. M. A... et autres se pourvoient en cassation contre cet arrêt.<br/>
<br/>
              2. L'article 6 de la directive du 13 décembre 2011 concernant l'évaluation des incidences de certains projets publics et privés sur l'environnement a pour objet de garantir qu'une autorité compétente et objective en matière d'environnement soit en mesure de rendre un avis sur l'évaluation environnementale des projets susceptibles d'avoir des incidences notables sur l'environnement, avant leur approbation ou leur autorisation, afin de permettre la prise en compte de ces incidences. Eu égard à l'interprétation de l'article 6 de la directive du 27 juin 2001 donnée par la Cour de justice de l'Union européenne par son arrêt rendu le 20 octobre 2011 dans l'affaire C-474/10, il résulte clairement des dispositions de l'article 6 de la directive du 13 décembre 2011 que, si elles ne font pas obstacle à ce que l'autorité publique compétente pour autoriser un projet soit en même temps chargée de la consultation en matière environnementale, elles imposent cependant que, dans une telle situation, une séparation fonctionnelle soit organisée au sein de cette autorité, de manière à ce que l'entité administrative concernée dispose d'une autonomie réelle, impliquant notamment qu'elle soit pourvue de moyens administratifs et humains qui lui soient propres, et soit ainsi en mesure de remplir la mission de consultation qui lui est confiée en donnant un avis objectif sur le projet concerné.<br/>
<br/>
              3. Lorsqu'un projet est autorisé par un préfet de département autre que le préfet de région, l'avis rendu sur le projet par le préfet de région en tant qu'autorité environnementale doit, en principe, être regardé comme ayant été émis par une autorité disposant d'une autonomie réelle répondant aux exigences de l'article 6 de la directive du 13 décembre 2011, sauf dans le cas où c'est le même service qui a, à la fois, instruit la demande d'autorisation et préparé l'avis de l'autorité environnementale. En particulier, les exigences de la directive, tenant à ce que l'entité administrative appelée à rendre l'avis environnemental sur le projet dispose d'une autonomie réelle, impliquant notamment qu'elle soit pourvue de moyens administratifs et humains qui lui soient propres, ne peuvent être regardées comme satisfaites lorsque le projet a été instruit pour le compte du préfet de département par la direction régionale de l'environnement, de l'aménagement et du logement (DREAL) et que l'avis environnemental émis par le préfet de région a été préparé par la même direction, à moins que l'avis n'ait été préparé, au sein de cette direction, par le service mentionné à l'article R. 122-21 du code de l'environnement qui a spécialement pour rôle de préparer les avis des autorités environnementales.<br/>
<br/>
              4. Par suite, en jugeant que, par principe, il avait été répondu aux exigences de la directive dès lors que l'avis de l'autorité environnementale avait été émis par le préfet de région et que la décision attaquée avait été prise par le préfet de département, alors qu'il ressortait des pièces du dossier qui lui était soumis que la même unité territoriale de la direction régionale de l'environnement, de l'aménagement et du logement de Franche-Comté avait à la fois instruit la demande d'autorisation et préparé l'avis de l'autorité environnementale, la cour administrative d'appel a entaché son arrêt d'une erreur de droit. Dès lors, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, M. A... et autres sont fondés à demander l'annulation de l'arrêt qu'ils attaquent.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. A... et autres au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font en revanche obstacle à ce qu'une somme soit mise à la charge de M. A... et autres qui ne sont pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 22 novembre 2018 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy. <br/>
Article 3 : L'Etat versera à M. A... et autres une somme globale de 3 000 euros, au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la société " Parc éolien des Ecoulottes " sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5: La présente décision sera notifiée à M. B... A..., premier dénommé pour l'ensemble des requérants, à la ministre de la transition écologique et solidaire et à la société " Parc éolien des Ecoulottes ".<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
