<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038444261</ID>
<ANCIEN_ID>JG_L_2019_05_000000419982</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/44/42/CETATEXT000038444261.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 06/05/2019, 419982, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-05-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419982</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:419982.20190506</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 19 avril et 21 décembre 2018 au secrétariat du contentieux du Conseil d'Etat, la société B. Braun medical demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 17 novembre 2017 par laquelle le ministre de l'action et des comptes publics et la ministre des solidarités et de la santé ont refusé d'inscrire les dispositifs médicaux " Sequent Please " et " Sequent Please Neo " sur la liste des produits et prestations remboursables mentionnée à l'article L. 165-1 du code de la sécurité sociale et sur la liste des produits et prestations pris en charge en sus des prestations d'hospitalisation mentionnée à l'article L. 162-22-7 même du code, ainsi que la décision implicite de rejet de son recours gracieux ;<br/>
<br/>
              2°) d'enjoindre aux ministres de procéder à cette inscription ou, subsidiairement, de réexaminer ses demandes d'inscription, dans un délai de quinze jours à compter de la décision à intervenir, sous astreinte de 2 000 euros par jour de retard ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la sécurité sociale ;<br/>
              - le décret n° 2015-1649 du 11 décembre 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. D'une part, aux termes de l'article L. 165-1 du code de la sécurité sociale : " Le remboursement par l'assurance maladie des dispositifs médicaux à usage individuel (...) est subordonné à leur inscription sur une liste établie après avis d'une commission de la Haute Autorité de santé mentionnée à l'article L. 161-37 (...) ". L'article R. 165-1 du même code prévoit que : " Les produits et prestations mentionnés à l'article L. 165-1 ne peuvent être remboursés par l'assurance maladie (...) que s'ils figurent sur une liste établie par arrêté du ministre chargé de la sécurité sociale et du ministre chargé de la santé après avis de la commission spécialisée de la Haute Autorité de santé mentionnée à l'article L. 165-1 du présent code et dénommée " Commission nationale d'évaluation des dispositifs médicaux et des technologies de santé " (...) ".  L'article R. 165-4 du même code précise par ailleurs que : " Ne peuvent être inscrits sur la liste prévue à l'article L. 165-1 : (...) / 4° Les dispositifs médicaux à usage individuel qui sont utilisés pour ou pendant la réalisation d'un acte par un professionnel de santé et dont la fonction ne s'exerce pas au-delà de l'intervention du professionnel. / Par exception aux règles énoncées dans le présent article, peuvent être inscrits sur la liste prévue à l'article L. 165-1 les dispositifs médicaux relevant du 4° qui pénètrent partiellement ou entièrement à l'intérieur du corps, soit par un orifice du corps, soit à travers la surface du corps, et qui sont utilisés lors d'actes de prévention, d'investigation ou de soins hospitaliers ". Il résulte du dernier alinéa de cet article, issu du décret du 11 décembre 2015 relatif aux modalités et aux conditions d'inscription de certains produits et prestations sur la liste prévue à l'article L. 165-1 du code de la sécurité sociale, que les dispositifs médicaux invasifs peuvent désormais être inscrits sur cette liste même lorsqu'ils ne sont pas implantables. En application de ces dispositions, les ministres de l'économie et des finances et des affaires sociales et de la santé ont, par un arrêté du 4 mai 2017, complété la liste des produits et prestations remboursables, qui comprenait un titre III relatif aux dispositifs médicaux implantables, par un titre V dénommé " dispositifs médicaux invasifs non éligibles au titre III ". Enfin, selon l'article R. 165-5-1 du code de la sécurité sociale : " Sont radiés de la liste prévue à l'article L. 165-1, par arrêté du ministre chargé de la sécurité sociale et du ministre chargé de la santé : / 1° Les produits et prestations faisant exclusivement appel à des soins pratiqués par des établissements de santé et qui sont pris en charge au titre des prestations d'hospitalisation (...) ".<br/>
<br/>
              2. D'autre part, aux termes du I de l'article L. 162-22-7 du même code : " L'Etat fixe (...) les conditions dans lesquelles certains produits et prestations mentionnés à l'article L. 165-1 peuvent faire l'objet d'une prise en charge en sus des prestations d'hospitalisation (...) ". Et aux termes de son article R. 162-38 : " La liste des produits et prestations et les conditions de prise en charge des produits et prestations mentionnés à l'article L. 162-22-7 sont fixées par arrêté des ministres chargés de la santé et de la sécurité sociale ". <br/>
<br/>
              3. Le respect du principe d'égalité devant la loi et les règles de concurrence imposent aux ministres compétents de s'assurer que les différences pouvant exister dans les conditions d'inscription, sur les listes prévues par les articles L. 165-1 et L. 162-22-7 du code de la sécurité sociale, de produits étroitement comparables dans le traitement d'une même pathologie ne soient pas manifestement disproportionnées au regard des motifs susceptibles de les justifier.<br/>
<br/>
              4. Il ressort des pièces du dossier que la société B. Braun medical a demandé l'inscription des dispositifs de ballons actifs à libération contrôlée de paclitaxel " Sequent Please " et " Sequent Please Neo ", avec pour indication le traitement de la resténose clinique intra-stent, sur la liste des produits et prestations remboursables mentionnée à l'article L. 165-1 du code de la sécurité sociale et sur la liste des produits et prestations pris en charge en sus des prestations d'hospitalisation mentionnée à l'article L. 162-22-7 du même code. Il n'est pas contesté que ces dispositifs médicaux invasifs non implantés dans le corps relèvent du dernier alinéa de l'article R. 165-4 du même code et, par conséquent, du nouveau titre V de la liste des produits et prestations remboursables. Par deux avis du 28 juin 2016, la commission nationale d'évaluation des dispositifs médicaux et des technologies de santé a estimé que le service attendu de ces dispositifs était " suffisant " pour justifier leur inscription sur la liste des produits et prestations remboursables, tout en précisant qu'ils n'apportaient pas d'amélioration du service attendu par rapport à des stents actifs, dispositifs médicaux implantables relevant quant à eux du titre III de cette liste et déjà inscrits sur la liste des produits et prestations pris en charge en sus des prestations d'hospitalisation. Par une décision du 17 novembre 2017, le ministre de l'action et des comptes publics et la ministre des solidarités et de la santé ont refusé l'inscription des dispositifs médicaux litigieux sur la liste prévue par l'article L. 162-22-7 faute d'amélioration du service attendu par rapport aux comparateurs pertinents, ainsi que, par voie de conséquence, s'agissant de produits utilisés exclusivement en milieu hospitalier, leur inscription sur la liste prévue à l'article L. 165-1 du même code. La société B. Braun medical demande l'annulation pour excès de pouvoir de cette décision, ainsi que de la décision implicite de rejet de son recours gracieux.<br/>
<br/>
              5. Dans son mémoire en défense, la ministre des solidarités et de la santé fait valoir, ainsi que la notice d'information publiée sur le site Internet du ministère le mentionne, que la comparaison directe d'un dispositif médical avec un autre dispositif médical lui-même inscrit sur la liste des produits et prestations pris en charge en sus des prestations d'hospitalisation constitue un élément en faveur de l'inscription sur cette liste, même si l'amélioration du service attendu par rapport à ce comparateur n'est que mineure, voire inexistante. Elle indique toutefois que dans cette dernière hypothèse, lorsque le dispositif médical concerné relève du titre V de la liste des produits et prestations remboursables, l'inscription sur la liste des produits et prestations pris en charge en sus des prestations d'hospitalisation n'est possible qu'à la condition que le comparateur pertinent relève également du titre V de la liste des produits et prestations remboursables. <br/>
<br/>
              6. Cependant, si les dispositifs médicaux invasifs relevant du titre V de la liste prévue à l'article L. 165-1 du code de la sécurité sociale ne sont pas implantables, à la différence de ceux relevant du titre III de cette liste, cette seule différence ne fait pas obstacle par elle-même, au regard de l'objet de l'inscription sur la liste prévue par l'article L. 162-22-7 du code de la sécurité sociale, à ce qu'ils puissent être regardés comme étroitement comparables dans le traitement d'une même pathologie. Par suite, en se bornant, pour refuser l'inscription sollicitée des ballons actifs " Sequent Please " et " Sequent Please Neo " sur cette liste, à constater, d'une part, que ces dispositifs médicaux n'apportaient pas d'amélioration du service attendu par rapport aux stents actifs et, d'autre part, que ces comparateurs pertinents, déjà inscrits sur la liste des produits et prestations pris en charge en sus des prestations d'hospitalisation, ne relevaient pas du titre V de la liste des produits et prestations remboursables mais de son titre III, les ministres ont entaché leur décision d'une erreur de droit. La société B. Braun medical est donc fondée à demander, pour ce motif, l'annulation de la décision attaquée rejetant sa demande d'inscription sur la liste prévue par l'article L. 162-22-7 du code de la sécurité sociale et, par voie de conséquence, sa demande d'inscription sur la liste prévue par l'article L. 165-1 du même code, ainsi que la décision rejetant son recours gracieux.<br/>
<br/>
              7. L'exécution de la présente décision implique nécessairement, eu égard à ses motifs, le réexamen de la demande de la société requérante. Il y a lieu pour le Conseil d'Etat d'ordonner qu'il soit procédé à ce réexamen dans un délai de trois mois à compter de la notification de la présente décision. En revanche, il n'y a pas lieu, dans les circonstances de l'espèce, d'assortir cette injonction de l'astreinte demandée.<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à la société B. Braun medical au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La décision du ministre de l'action et des comptes publics et de la ministre des solidarités et de la santé du 17 novembre 2017 et la décision implicite rejetant le recours gracieux formé contre cette décision sont annulées.<br/>
Article 2 : Il est enjoint au ministre de l'action et des comptes publics et à la ministre des solidarités et de la santé de réexaminer la demande de la société B. Braun medical dans un délai de trois mois.<br/>
Article 3 : L'Etat versera à la société B. Braun medical une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions de la requête de la société B. Braun medical est rejeté.<br/>
Article 5 : La présente décision sera notifiée à la société B. Braun medical et à la ministre des solidarités et de la santé.<br/>
Copie en sera adressée au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
