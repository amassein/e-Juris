<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030926063</ID>
<ANCIEN_ID>JG_L_2015_07_000000373519</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/92/60/CETATEXT000030926063.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème SSJS, 22/07/2015, 373519, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373519</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:373519.20150722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement n° 2012-0029 du 6 février 2013, la chambre régionale des comptes d'Aquitaine-Poitou-Charentes a constitué M. B...A...débiteur au titre de l'exercice 2006 envers la caisse de l'établissement public d'hébergement pour personnes âgées dépendantes (EHPAD) " Fondation Roux " de Vertheuil de la somme 2 621,92 euros majorée des intérêts de droit à compter du 14 octobre 2010. <br/>
<br/>
              Par un arrêt n° 67934 du 24 octobre 2013, la Cour des comptes a rejeté sur ce point l'appel de M. A...contre ce jugement.<br/>
<br/>
              Par un pourvoi enregistré le 26 novembre 2013 au secrétariat du contentieux du Conseil d'Etat, le ministre délégué, chargé du budget, demande au Conseil d'Etat d'annuler cet arrêt de la Cour des comptes.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ; <br/>
              - le code des juridictions financières ;<br/>
              - la loi n° 63-156 du 23 février 1963 ;<br/>
              - la loi n° 91-73 du 18 janvier 1991 ;<br/>
              - le décret n° 62-1587 du 29 décembre 1962 ;<br/>
              - le décret n° 93-92 du 19 janvier 1993 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1.	Considérant, d'une part, qu'en vertu de l'article 60 de la loi de finances du 23 février 1963, la responsabilité personnelle et pécuniaire des comptables publics se trouve engagée dès lors notamment qu'une dépense a été irrégulièrement payée ; que selon le VI de cet article, le comptable public dont la responsabilité pécuniaire est ainsi engagée ou mise en jeu a l'obligation de verser immédiatement de ses deniers personnels une somme égale à la dépense payée à tort ; que s'il n'a pas versé cette somme, il peut être, selon le VII de ce même article, constitué en débet par le juge des comptes ; que s'agissant des comptables locaux, l'article L. 1617-2 du code général des collectivités territoriales dispose que : " Le comptable d'une commune, d'un département ou d'une région ne peut subordonner ses actes de paiement à une appréciation de l'opportunité des décisions prises par l'ordonnateur. Il ne peut soumettre les mêmes actes qu'au contrôle de légalité qu'impose l'exercice de sa responsabilité personnelle et pécuniaire " ; qu'en vertu de l'article 19 du décret du 29 décembre 1962 portant règlement général sur la comptabilité publique, les comptables publics sont, dans les conditions fixées par les lois de finances, personnellement et pécuniairement responsables de l'exercice régulier des contrôles prévus aux articles 12 et 13 ; qu'aux termes de l'article 12 de ce décret : " Les comptables sont tenus d'exercer : (...) / B. - En matière de dépenses, le contrôle : / (...) De la validité de la créance dans les conditions prévues à l'article 13 ci-après (...) " ; que l'article 13 du même décret dispose que : " En ce qui concerne la validité de la créance, le contrôle porte sur : / La justification du service fait et l'exactitude des calculs de liquidation ; / L'intervention préalable des contrôles réglementaires et la production des justifications (...) " ; qu'aux termes de l'article 37 du même décret : " Lorsque, à l'occasion de l'exercice du contrôle prévu à l'article 12 (alinéa B) ci-dessus, des irrégularités sont constatées, les comptables publics suspendent les paiements et en informent l'ordonnateur (...) " ; qu'en vertu de l'article 47 du même décret, les opérations de dépenses " doivent être appuyées des pièces justificatives prévues dans les nomenclatures établies par le ministre des finances avec, le cas échéant, l'accord du ministre intéressé. " ;<br/>
<br/>
              2.	Considérant qu'il résulte de ces dispositions que, pour apprécier la validité des créances, les comptables doivent notamment exercer leur contrôle sur la production des justifications ; qu'à ce titre, il leur revient d'apprécier si les pièces fournies présentent un caractère suffisant pour justifier la dépense engagée ; que pour établir ce caractère suffisant, il leur appartient de vérifier, en premier lieu, si l'ensemble des pièces requises au titre de la nomenclature comptable applicable leur ont été fournies et, en deuxième lieu, si ces pièces sont, d'une part, complètes et précises, d'autre part, cohérentes au regard de la catégorie de la dépense définie dans la nomenclature applicable et de la nature et de l'objet de la dépense telle qu'elle a été ordonnancée ; que si ce contrôle peut conduire les comptables à porter une appréciation juridique sur les actes administratifs à l'origine de la créance et s'il leur appartient alors d'en donner une interprétation conforme à la réglementation en vigueur, ils n'ont pas le pouvoir de se faire juges de leur légalité ; qu'enfin, lorsque les pièces justificatives fournies sont insuffisantes pour établir la validité de la créance, il appartient aux comptables de suspendre le paiement jusqu'à ce que l'ordonnateur ait produit les justifications nécessaires ;<br/>
<br/>
              3.	Considérant, d'autre part, qu'il résulte des dispositions combinées de l'article 27 de la loi du 18 janvier 1991 portant dispositions relatives à la santé publique et aux assurances sociales et de l'article 1er du décret du 19 janvier 1993 relatif à la nouvelle bonification indiciaire attachée à des emplois occupés par certains personnels de la fonction publique hospitalière que cet avantage est attribué à des agents fonctionnaires occupant certains emplois limitativement énumérés ;<br/>
<br/>
              4.	Considérant qu'il ressort des énonciations de l'arrêt attaqué que, pour constituer M.A..., comptable de l'EHPAD " Fondation Roux ", débiteur de la somme correspondant au paiement de la nouvelle bonification indiciaire versée à des agents contractuels de cet établissement au titre de l'exercice 2006, la Cour des comptes s'est fondée sur la seule circonstance que ce comptable n'avait pas suspendu les paiements concernés alors que les contrats des intéressés, en ce qu'ils prévoyaient un abondement indiciaire expressément dénommé " NBI ", méconnaissaient les dispositions législatives et réglementaires mentionnées au point 3, lesquelles n'incluaient pas les agents contractuels dans les catégories de personnes susceptibles de bénéficier de la nouvelle bonification indiciaire ; qu'en statuant ainsi, alors qu'il résulte de ce qui a été dit au point 2 que la Cour des comptes devait seulement rechercher, d'abord, si le comptable aurait dû se fonder, au regard de la nomenclature applicable, sur d'autres pièces justificatives que les contrats des intéressés qu'il a retenus pour régulièrement apprécier la validité de ces dépenses, et, ensuite, dans le cas où ces pièces correspondaient à la nomenclature, si les contrats des intéressés ne présentaient pas, quelle que soit en tout état de cause leur validité juridique, d'incohérence au regard de la nature et de l'objet de la dépense engagée, la Cour des comptes a exigé du comptable qu'il exerce un contrôle de légalité sur les pièces fournies par l'ordonnateur ; que, ce faisant, elle a entaché son arrêt d'une erreur de droit ; que, par suite, son arrêt doit être annulé ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la Cour des comptes du 24 octobre 2013 est annulé. <br/>
Article 2 : L'affaire est renvoyée à la Cour des comptes.<br/>
 Article 3 : La présente décision sera notifiée au ministre délégué, chargé du budget, à M. B... A..., à l'EHPAD " Fondation Roux " et au Procureur général près la Cour des comptes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
