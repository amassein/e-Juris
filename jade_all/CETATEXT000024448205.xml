<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024448205</ID>
<ANCIEN_ID>JG_L_2011_05_000000337280</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/44/82/CETATEXT000024448205.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 11/05/2011, 337280</TITRE>
<DATE_DEC>2011-05-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>337280</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Christian Vigouroux</PRESIDENT>
<AVOCATS>SCP LAUGIER, CASTON</AVOCATS>
<RAPPORTEUR>M. Frédéric Dieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Boulouis</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 5 mars et 7 juin 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la CAISSE DES DEPOTS ET CONSIGNATIONS dont le siège est au 56, rue de Lille à Paris (75007) ; la CAISSE DES DEPOTS ET CONSIGNATIONS demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0619293/5 du 30 décembre 2009 par lequel le tribunal administratif de Paris a, à la demande de M. A, d'une part, annulé les décisions de la caisse des dépôts et consignations ayant refusé à ce dernier une autorisation d'absence et opéré une retenue d'un trentième sur son traitement et ses primes et, d'autre part, enjoint à la CAISSE DES DEPOTS ET CONSIGNATIONS de rembourser à l'intéressé la somme de 109,35 euros ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande présentée par M. A devant le tribunal administratif de Paris ; <br/>
<br/>
              3°) de mettre à la charge de M. A le versement de la somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 61-825 du 29 juillet 1961 ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ainsi que la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Dieu, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Laugier, Caston, avocat de la CAISSE DES DEPOTS ET CONSIGNATIONS, <br/>
<br/>
              - les conclusions de M. Nicolas Boulouis, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Laugier, Caston, avocat de la CAISSE DES DEPOTS ET CONSIGNATIONS ;<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis au juge du fond que M. A, fonctionnaire titulaire à la CAISSE DES DEPOTS ET CONSIGNATIONS, ne s'est pas rendu à son travail le 21 août 2006 ; qu'il a demandé ultérieurement une autorisation d'absence d'une journée à ce titre ; que la CAISSE DES DEPOTS ET CONSIGNATIONS, ayant refusé de lui accorder cette autorisation d'absence, a opéré, pour le mois d'octobre 2006 une retenue d'un trentième de son traitement mensuel et des primes y afférentes pour absence de service fait ; que la CAISSE DES DEPOTS ET CONSIGNATIONS se pourvoit contre le jugement du 30 décembre 2009 par lequel le tribunal administratif de Paris, après avoir annulé les décisions par lesquelles elle a, d'une part, refusé de régulariser l'absence de M. A autrement que par l'imputation de la journée du 21 août 2006 sur ses congés légaux et, d'autre part, opéré une retenue d'un trentième sur le traitement versé à ce dernier au titre du mois d'octobre 2006, lui a enjoint de procéder au remboursement de la somme de 109,35 euros correspondant à cette retenue ;<br/>
<br/>
              Considérant que la décision par laquelle l'administration refuse d'accorder à l'un de ses agents, à titre discrétionnaire, une autorisation d'absence pour commodité personnelle sans retenue sur traitement revêt le caractère d'une mesure d'ordre intérieur insusceptible de recours ; qu'il ressort des pièces du dossier soumis au juge du fond que M. A avait demandé à bénéficier d'une autorisation d'absence sans retenue sur traitement pour régulariser une absence due aux démarches rendues nécessaires par un cambriolage dont il avait été victime ; que, par suite, en estimant que M. A était recevable à demander l'annulation de la décision par laquelle la CAISSE DES DEPOTS ET CONSIGNATIONS a refusé de lui accorder une autorisation d'absence sans retenue sur traitement et donc de régulariser sa journée d'absence du 21 août 2006 autrement que par l'imputation de la journée entière sur ses congés légaux, puis en annulant la retenue d'un trentième opérée sur son traitement mensuel au motif tiré de l'illégalité de ce refus, le tribunal administratif de Paris a commis une erreur de droit ; <br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Considérant en premier lieu qu'ainsi qu'il a été dit, la décision par laquelle la CAISSE DES DEPOTS ET CONSIGNATIONS a refusé de régulariser la journée d'absence de M. A, prise pour commodité personnelle, autrement que par l'imputation de la journée entière sur ses congés légaux revêt le caractère d'une mesure d'ordre intérieur insusceptible de recours ; que, par suite, M. A n'est pas fondé à en demander l'annulation ;<br/>
<br/>
              Considérant en second lieu qu'aux termes de l'article 4 de la loi du 29 juillet 1961 de finances rectificatives pour 1961:  Le traitement exigible après service fait [...] est liquidé selon les modalités édictées par la réglementation sur la comptabilité publique. / L'absence de service fait, pendant une fraction quelconque de la journée, donne lieu à une retenue dont le montant est égal à la fraction du traitement frappé d'indivisibilité en vertu de la réglementation prévue à l'alinéa précédent. / Il n'y a pas service fait : / 1°) Lorsque l'agent s'abstient d'effectuer tout ou partie de ses heures de services ...  ;<br/>
<br/>
              Considérant qu'il est constant que M. A n'a pas accompli de service pendant la journée du 21 août 2006 et a refusé de compenser cette absence par le décompte d'une journée de ses congés légaux ; que M. A ne saurait invoquer, pour contester la légalité de la décision de retenue sur traitement, ni les stipulations de l'annexe au protocole d'accord sur le temps de travail applicables aux agents de la CAISSE DES DEPOTS ET CONSIGNATIONS ni les pratiques en vigueur au sein de cet établissement, lesquelles sont dépourvues de valeur juridique et de force contraignante ; qu'en l'absence de service fait, la CAISSE DES DEPOTS ET CONSIGNATIONS pouvait légalement procéder, en application des dispositions de l'article 4 de la loi du 29 juillet 1961, à une retenue d'un trentième sur le traitement mensuel versé à M. A ; que ce dernier n'est par suite pas fondé à demander l'annulation de la décision par laquelle la CAISSE DES DEPOTS ET CONSIGNATIONS a opéré cette retenue sur le traitement qui lui a été versé au titre du mois d'octobre 2006 ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que M. A n'est pas fondé à demander l'annulation des décisions attaquées ; que, par suite, les conclusions de sa requête ne peuvent qu'être rejetées, y compris celles à fins d'injonction et celles tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la CAISSE DES DEPOTS ET CONSIGNATIONS au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
     --------------<br/>
Article 1er : Le jugement du tribunal administratif de Paris du 30 décembre 2009 est annulé.<br/>
<br/>
Article 2 : La demande présentée par M. A devant le tribunal administratif de Paris est rejetée.<br/>
<br/>
Article 3 : Les conclusions présentées par la CAISSE DES DEPOTS ET CONSIGNATIONS au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la CAISSE DES DEPOTS ET CONSIGNATIONS et à M. Jean-François A.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-07-10 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. GARANTIES ET AVANTAGES DIVERS. - REFUS D'ACCORDER UNE AUTORISATION D'ABSENCE POUR COMMODITÉ PERSONNELLE À TITRE DISCRÉTIONNAIRE - DÉCISION PRÉSENTANT LE CARACTÈRE D'UNE MESURE D'ORDRE INTÉRIEUR [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-01-02-03 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES NE CONSTITUANT PAS DES DÉCISIONS SUSCEPTIBLES DE RECOURS. MESURES D'ORDRE INTÉRIEUR. - REFUS D'ACCORDER UNE AUTORISATION D'ABSENCE POUR COMMODITÉ PERSONNELLE À TITRE DISCRÉTIONNAIRE.
</SCT>
<ANA ID="9A"> 36-07-10 La décision par laquelle l'administration refuse d'accorder à l'un de ses agents, à titre discrétionnaire, une autorisation d'absence pour commodité personnelle sans retenue sur traitement revêt le caractère d'une mesure d'ordre intérieur insusceptible de recours.</ANA>
<ANA ID="9B"> 54-01-01-02-03 La décision par laquelle l'administration refuse d'accorder à l'un de ses agents, à titre discrétionnaire, une autorisation d'absence pour commodité personnelle sans retenue sur traitement revêt le caractère d'une mesure d'ordre intérieur insusceptible de recours.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 16 mai 1980, Chevry et autres, n° 12670 et autres, p. 227 ; Ab. jur. CE, 15 février 1991, Mont, n° 64686, T. p. 1104.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
