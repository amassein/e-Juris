<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031649171</ID>
<ANCIEN_ID>JG_L_2015_12_000000384794</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/64/91/CETATEXT000031649171.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 18/12/2015, 384794, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384794</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:384794.20151218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 25 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, la SARL Loc Car Dream demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la délibération n° 2014-294 du 22 juillet 2014 par laquelle la Commission nationale de l'informatique et des libertés (CNIL) a prononcé à son encontre une sanction pécuniaire de 5 000 euros, assortie d'une publication sur les sites internet de la CNIL et de Légifrance ;<br/>
<br/>
              2°) d'enjoindre à la CNIL de publier sur son site internet ainsi que sur celui de Légifrance la décision à intervenir du Conseil d'Etat ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - la loi n° 78-17 du 6 janvier 1978 ;<br/>
              - le décret n° 2005-1309 du 20 octobre 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que, par une délibération du 22 juillet 2014, la formation restreinte de la Commission nationale de l'informatique et des libertés (CNIL) a infligé à la SARL Loc Car Dream, société de location de véhicules de luxe, une sanction pécuniaire de 5 000 euros, qu'elle a décidé de rendre publique, pour avoir manqué à l'obligation d'accomplir les formalités préalables nécessaires à la mise en oeuvre d'un traitement informatisé de données à caractère personnel relatif à la géolocalisation de véhicules de location et à la gestion des clients, de veiller à l'adéquation, à la pertinence et au caractère non excessif des données traitées, d'informer les personnes de la géolocalisation des véhicules, d'assurer la sécurité des données et de coopérer avec la CNIL ; que la SARL Loc Car Dream demande l'annulation de cette décision ;<br/>
<br/>
              Sur la légalité externe de la délibération attaquée :<br/>
<br/>
              2. Considérant en premier lieu, qu'aux termes des dispositions de l'article 13 de la loi du 6 juillet 1978 : " (...) Les membres du bureau ne sont pas éligibles à la formation restreinte (...) " ; que, contrairement à ce qui est soutenu, M. C...B..., vice-président de la formation restreinte n'était pas membre du bureau de la CNIL à la date de la décision attaquée ; que, par suite, le moyen tiré de ce que cette délibération aurait été prise par une commission restreinte comprenant irrégulièrement un membre du bureau de la CNIL manque en fait ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'en application de l'article 44 de la loi du 6 janvier 1978, il est dressé procès-verbal des vérifications et visites menées par les membres de la CNIL dans les lieux qui servent à la mise en oeuvre de traitements de données à caractère personnel et qui sont à usage professionnel, contradictoirement lorsque les vérifications et visites sont effectuées sur place ; qu'aux termes de l'article 64 du décret du 20 octobre 2005 : " (...) Le procès-verbal est signé par (...) le responsable des lieux ou son représentant (...) Le procès-verbal est notifié au responsable des lieux et au responsable des traitements par lettre recommandée avec demande d'avis de réception " ;<br/>
<br/>
              4. Considérant que le moyen tiré de ce que le procès-verbal du contrôle auquel il a été procédé dans les locaux de la SARL Loc Car Dream situés sur le site de Douvres-la-Délivrande le 20 juin 2013 aurait été signé par une personne qui n'avait pas qualité manque en fait dès lors que MmeA..., épouse du gérant de la SARL Loc Car Dream, avait été régulièrement mandatée pour ce faire par M.A..., en sa qualité de responsable des lieux ;<br/>
<br/>
              5. Considérant, en troisième lieu, qu'aux termes de l'article 46 de la loi du 6 janvier 1978 : " Les sanctions prévues au I et au 1° du II de l'article 45 sont prononcées sur la base d'un rapport établi par l'un des membres de la Commission nationale de l'informatique et des libertés (...). Ce rapport est notifié au responsable du traitement, qui peut déposer des observations et se faire représenter ou assister (...) " ; qu'aux termes de l'article 75 du décret du 20 octobre 2005 : " Le rapport prévu par l'article 46 de la loi du 6 janvier 1978 susvisée est notifié au responsable du traitement par tout moyen permettant à la commission d'apporter la preuve de la date de cette notification. Il est également transmis à la formation restreinte. / Le responsable du traitement dispose d'un délai d'un mois pour transmettre au rapporteur et à la formation restreinte ses observations écrites (...). La notification du rapport mentionne ce délai et précise que le responsable du traitement peut prendre connaissance et copie des pièces du dossier auprès des services de la commission et se faire assister ou représenter par tout conseil de son choix " ; qu'il résulte de ces dispositions, d'une part, que le délai d'un mois qu'elles prescrivent ne s'applique qu'à la notification du rapport et non à celle des procès-verbaux dressés en application de l'article 44 de la loi du 6 janvier 1978 et, d'autre part, que le responsable du traitement est informé, lorsque le rapport lui est notifié, qu'il peut prendre connaissance et copie de l'ensemble des pièces du dossier auprès des services de la CNIL ;<br/>
<br/>
              6. Considérant qu'il résulte de l'instruction que le rapport proposant de prononcer une sanction à l'encontre de la SARL Loc Car Dream, accompagné d'une convocation à la séance de la formation restreinte de la CNIL du 22 mai 2014, a été adressé au gérant de cette société, par lettre recommandée avec accusé de réception distribuée le 7 avril 2014 ; qu'à la suite d'une erreur matérielle, ce rapport comprenant en annexe non pas le procès-verbal n° 2013-198 relatif au contrôle effectué dans les locaux de la SARL Loc Car Dream situés sur le site de Douvres-la-Délivrande le 20 juin 2013, comme l'indiquait le bordereau accompagnant ce rapport, mais un rapport de contrôle concernant une autre société, la CNIL a procédé à un nouvel envoi du procès-verbal n° 2013-198 par un courrier de son secrétaire général, en recommandé avec accusé de réception, distribué le 28 avril 2014 ; que la circonstance que le procès-verbal de contrôle du 20 juin 2013 n'a pas, contrairement au rapport auquel il était annexé, été communiqué au responsable du traitement au moins un mois avant la réunion de la commission restreinte du 22 mai 2014 est sans incidence sur la régularité de la notification du rapport prévue à l'article 75 du décret du 20 octobre 2005, dès lors que le responsable du traitement était informé depuis le 7 avril 2014 qu'il pouvait en prendre connaissance et copie auprès des services de la CNIL ; qu'il résulte, au surplus, de l'instruction que copie lui en avait déjà été remise le 20 juin 2013 ;<br/>
<br/>
              Sur la légalité interne de la délibération attaquée :<br/>
<br/>
              7. Considérant qu'aux termes du I de l'article 3 de la loi du 6 janvier 1978 : " Le responsable d'un traitement de données à caractère personnel est, sauf désignation expresse par les dispositions législatives ou réglementaires relatives à ce traitement, la personne, l'autorité publique, le service ou l'organisme qui détermine ses finalités et ses moyens " ;<br/>
<br/>
              8. Considérant que, si la SARL Loc Car Dream soutient qu'elle ne saurait être regardée comme responsable du traitement contesté, au sens des dispositions du I de l'article 3 de la loi du 6 janvier 1978, au motif qu'elle ne serait pas propriétaire de l'ensemble des véhicules munis d'un dispositif de géolocalisation et objets du contrôle, il résulte de l'instruction que le contrat de location à l'origine de la plainte reçue par la CNIL a été signé par la SARL Loc Car Dream, que l'ensemble des données de géolocalisation des trente-six véhicules concernés, centralisées chez l'hébergeur Web et Solutions, est accessible depuis un seul poste de travail, dont l'épouse du gérant détient le mot de passe, situé à l'accueil commun à l'ensemble des sociétés propriétaires et que la SARL Loc Car Dream a déclaré le 11 décembre 2008 un engagement de conformité à la délibération de la CNIL n° 2006-067 du 16 mars 2006 portant adoption d'une forme de norme simplifiée concernant les traitements automatisés de données à caractère personnel destinés à " géolocaliser " les véhicules utilisés par les employés ; qu'ainsi, la SARL Loc Car Dream doit être regardée comme déterminant les finalités et les moyens du traitement litigieux ; que, par suite, en estimant qu'elle pouvait faire l'objet d'une sanction en tant que responsable du traitement, la CNIL n'a pas fait une inexacte application des dispositions citées au point 7 ;<br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que la SARL Loc Car Dream n'est pas fondée à demander l'annulation de la délibération qu'elle attaque ; que ses conclusions à fin d'injonction, comme ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la SARL Loc Car Dream est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la SARL Loc Car Dream et à la Commission nationale de l'informatique et des libertés. <br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
