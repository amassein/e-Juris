<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023218777</ID>
<ANCIEN_ID>JG_L_2010_12_000000308050</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/21/87/CETATEXT000023218777.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 10/12/2010, 308050, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2010-12-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>308050</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin</PRESIDENT>
<AVOCATS>SCP DELVOLVE, DELVOLVE</AVOCATS>
<RAPPORTEUR>M. Jean-Luc  Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Boucher Julien</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 31 juillet et 31 octobre 2007 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SARL PRUNUS, venant aux droits de la SA Groupe Hugo, dont le siège est 28, rue des Colonnes du Trône à Paris (75012), représentée par son gérant ; la SARL PRUNUS demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 31 mai 2007 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant à l'annulation du jugement du 14 juin 2005 du tribunal administratif de Paris rejetant la demande de la SA Groupe Hugo tendant à la décharge de la cotisation supplémentaire d'impôt sur les sociétés à laquelle elle a été assujettie au titre de l'année 1989 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, chargé des fonctions de Maître des Requêtes,<br/>
<br/>
              - les observations de la SCP Delvolvé, Delvolvé, avocat de la SARL PRUNUS,<br/>
<br/>
              - les conclusions de M. Julien Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Delvolvé, Delvolvé, avocat de la SARL PRUNUS ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SA Groupe Hugo a cédé le 29 septembre 1989 au prix de 1 franc à la société Sese, dont elle détenait 99 % du capital, la marque commerciale d'un salon spécialisé dans les produits de construction de second oeuvre, dénommé Equip'Baie, qui s'était tenu une première fois au parc des expositions de la porte de Versailles à Paris en novembre 1988 ; que le 5 octobre 1989, la SA Groupe Hugo a cédé la société Sese, dont l'actif était essentiellement constitué par la marque Equip'Baie et le fonds de commerce afférent, et qui ne disposait d'aucun personnel technique ou commercial, à la société Groupe Blenheim pour la somme de 11 millions de francs ; que postérieurement à ces cessions, dans le cadre de l'absorption de la société Sese par la société Groupe Blenheim, la valeur de la marque Equip'Baie au 1er septembre 1990 a été estimée à la somme de 10 605 873 francs par le commissaire aux apports ; que l'administration fiscale, estimant que la cession de la marque Equip'baie à la société Sese au prix de 1 franc constituait un acte anormal de gestion, a réintégré dans le bénéfice de l'exercice 1989 de la SA Groupe Hugo la somme de 10 605 872 francs, correspondant au montant de la marque tel que valorisé par le commissaire aux apports, et diminué du prix de cession à la société Sese ; que la SARL PRUNUS, venant aux droits de la SA Groupe Hugo, se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Paris du 31 mai 2007 rejetant son appel tendant à la décharge des impositions en litige ;<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              Considérant que, pour juger que l'administration établissait que la marque Equip'Baie avait été cédée à la société Sese pour un montant inférieur à sa valeur réelle et que cette cession avait, dans ces conditions, revêtu le caractère d'un acte anormal de gestion, la cour s'est notamment fondée sur la circonstance que les titres de la société Sese avaient été cédés le 5 octobre 1989 à la société Groupe Blenheim, quelques jours après la cession de la marque, pour une valeur de 11 000 000 francs, alors qu'ils avaient été comptablement valorisés en septembre 1988 pour un montant de 495 000 francs par leur précédent détenteur ; qu'en estimant ainsi que la valeur d'une société ne pouvait être qu'en rapport étroit avec celle de son actif, sans rechercher si d'autres méthodes de valorisation ou si des circonstances particulières pouvaient fonder l'écart de valeur constaté entre l'actif qu'elle détenait et la valeur de la société, la cour administrative d'appel de Paris a entaché son arrêt d'une erreur de droit ; que son arrêt doit, par suite, être annulé ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens soulevés par la SARL PRUNUS ;<br/>
<br/>
              Considérant qu'il est toujours loisible à l'administration de se fonder sur des éléments postérieurs à une transaction pour en établir la valeur réelle, sous réserve que ces éléments ne traduisent aucune évolution qui ferait obstacle à ce qu'ils soient valablement pris en compte comme éléments de comparaison compte tenu de la date à laquelle ils sont intervenus ; que si la valeur à laquelle les titres d'une société sont cédés peut valablement être rapprochée de la valeur de l'actif qu'elle détient pour en démontrer l'insuffisance, c'est à la condition que la méthode de valorisation d'une société par celle de son actif soit pertinente au regard des circonstances propres à l'espèce et qu'aucun élément du contexte de la transaction ne puisse influer sur le prix ; qu'en l'espèce, s'il n'est pas contesté que la marque détenue par la société constituait son principal actif, l'acquéreur, opérateur dominant sur son marché, procédait à l'achat des titres afin de prendre le contrôle d'un de ses concurrents et renforçait ainsi sa position relative sur le marché ; qu'en se bornant à soutenir que la valeur de la société était celle de son principal actif, sans tenir compte de la situation particulière pouvant expliquer la valorisation à laquelle il a été procédé, l'administration ne peut être regardée, dans les circonstances de l'espèce, comme ayant apporté la preuve, qui lui incombe, que la cession au prix de 1 franc de la marque Equip'Baie par la SA Groupe Hugo à sa filiale la société Sese relevait d'un acte anormal de gestion ; que la SARL PRUNUS est dès lors fondée à soutenir que c'est à tort que le tribunal administratif de Paris a rejeté sa demande tendant à la décharge de la cotisation supplémentaire d'impôt sur les sociétés mise à sa charge au titre de l'exercice 1989 ; qu'il y a lieu d'accorder la décharge demandée ; qu'il y a lieu également de faire application des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge de l'Etat le versement à la SARL PRUNUS  d'une somme de 4 000 euros au titre des frais exposés par elle à tous les stades de la procédure et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 31 mai 2007 et le jugement du tribunal administratif de Paris du 14 juin 2005 sont annulés.<br/>
<br/>
Article 2 : La SA Groupe Hugo est déchargée de la cotisation supplémentaire d'impôt sur les sociétés mise à sa charge au titre de l'exercice 1989.<br/>
<br/>
Article 3 : L'Etat versera à la SARL PRUNUS une somme de 4 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la SARL PRUNUS et au ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat, porte-parole du Gouvernement.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
