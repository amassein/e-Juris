<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023632401</ID>
<ANCIEN_ID>JG_L_2011_02_000000337646</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/63/24/CETATEXT000023632401.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 23/02/2011, 337646, Publié au recueil Lebon</TITRE>
<DATE_DEC>2011-02-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>337646</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. Pascal  Trouilly</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Vialettes Maud</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:337646.20110223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête et le mémoire complémentaire, enregistrés les 17 mars et 12 avril 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société par actions simplifiée BIOGARAN, dont le siège est 15, boulevard Charles de Gaulle à Colombes (92707), représentée par son président, et pour la société à responsabilité limitée ALEPT, dont le siège est 42, avenue de la Bruyère à Grenoble (38100), représentée par son gérant ; les sociétés requérantes demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir les arrêtés interministériels du 15 décembre 2009 en tant qu'ils portent radiation de la Trolamine Biogaran 0,67 %, émulsion pour application cutanée, tube de 93 g, de la liste des médicaments remboursables aux assurés sociaux mentionnée au premier alinéa de l'article L. 162-17 du code de la sécurité sociale et de la liste des médicaments agréés à l'usage des collectivités publiques et divers services publics mentionnée à l'article L. 5123-2 du code de la santé publique, ainsi que la décision du 25 mars 2010 par laquelle le ministre de la santé et des sports a rejeté leur recours gracieux ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 5 000 euros sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code de la sécurité sociale ; <br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pascal Trouilly, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Barthélemy, Matuchansky, Vexliard, avocat de la SOCIETE BIOGARAN et de la SOCIETE ALEPT et de la SCP Piwnica, Molinié, avocat de la société Johnson et Johnson Consumer France,<br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Barthélemy, Matuchansky, Vexliard, avocat de la SOCIETE BIOGARAN et de la SOCIETE ALEPT et à la SCP Piwnica, Molinié, avocat de la société Johnson et Johnson Consumer France ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant que, par deux arrêtés interministériels du 15 décembre 2009, la spécialité Trolamine, dont l'autorisation de mise sur le marché est détenue par la SOCIETE ALEPT et la commercialisation assurée par la SOCIETE BIOGARAN, a été radiée de la liste des médicaments remboursables aux assurés sociaux et de la liste de ceux qui sont agréés à l'usage des collectivités publiques ; que ces arrêtés, qui ont procédé aux mêmes radiations pour la spécialité Biafine, exploitée par la société Johnson et Johnson Consumer France dont la Trolamine est le générique, sont fondés sur le caractère insuffisant du service médical rendu par ces médicaments ; que la SOCIETE ALEPT et la SOCIETE BIOGARAN demandent l'annulation de ces arrêtés en tant qu'ils portent radiation de la Trolamine ;<br/>
<br/>
              Sur la légalité externe des décisions de radiation : <br/>
<br/>
              Considérant que l'article R. 163-14 du code de la sécurité sociale prévoit que les décisions portant radiation des listes de spécialités remboursables sont communiquées à l'entreprise avec la mention des motifs de ces décisions ; que tant les arrêtés litigieux que le courrier du 11 janvier 2010 procédant à leur communication indiquent, en se référant à l'avis  rendu par la commission de la transparence de la Haute autorité de santé le 21 octobre 2009, lui-même communiqué à la SOCIETE BIOGARAN, que la spécialité Trolamine présente un service médical rendu insuffisant pour justifier sa prise en charge par l'assurance maladie ; que, par suite, compte tenu de ce que les décisions litigieuses avaient en outre été précédées, en application des dispositions de l'article R. 163-13 du code de la sécurité sociale, d'une lettre du 2 octobre 2009 adressée par les ministres concernés à la SOCIETE BIOGARAN lui indiquant de manière complète les motifs pour lesquels la procédure de radiation de la Trolamine était engagée, les sociétés requérantes ne sont pas fondées à soutenir que ces décisions ont méconnu la règle de motivation posée par les dispositions de l'article R. 163-14 du code de la sécurité sociale ; <br/>
<br/>
              Sur le moyen de légalité interne relatif à la seule décision radiant la Trolamine de la liste des spécialités remboursables aux assurés sociaux :<br/>
<br/>
              Considérant qu'aux termes du premier alinéa de l'article L. 162-17 du code de la sécurité sociale : " Les médicaments spécialisés mentionnés à l'article L. 5121-8 du code de la santé publique (...) ne peuvent être pris en charge ou donner lieu à remboursement par les caisses d'assurance maladie, lorsqu'ils sont dispensés en officine, que s'ils figurent sur une liste établie dans les conditions fixées par décret en Conseil d'Etat. " ; qu'aux termes du I de l'article R. 163-3 du même code : " (...) Les médicaments dont le service médical rendu est insuffisant au regard des autres médicaments ou thérapies disponibles ne sont pas inscrits sur la liste " ; qu'enfin, le I de l'article R. 163-7 prévoit que : " (...) peuvent être radiés de la liste prévue au premier alinéa de l'article L. 162-17, par arrêté du ministre chargé de la sécurité sociale et du ministre chargé de la santé (...) 3° Les médicaments qui ne peuvent plus figurer sur cette liste en vertu des dispositions prévues à l'article R. 163-3 (...) " ; qu'il résulte de ces dispositions qu'un médicament dont le service médical rendu est insuffisant au regard des autres médicaments ou thérapies disponibles ne saurait en principe faire l'objet d'une inscription sur la liste prévue par l'article L. 162-17 du code de la sécurité sociale ; que si une telle inscription a néanmoins eu lieu, les ministres compétents disposent, en vertu de l'article R. 163-7 cité ci-dessus, de la faculté de procéder à tout moment à la radiation de ce médicament, si son service médical rendu demeure insuffisant ; qu'il en va de même si, postérieurement à l'inscription d'un médicament dont le service médical rendu était suffisant au moment de son inscription, ce service médical devient ultérieurement insuffisant au regard d'autres médicaments ou thérapies disponibles ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier, et notamment de trois avis successifs rendus par la commission de la transparence de la Haute autorité de santé, que la spécialité de référence Biafine, déjà inscrite sur la liste des spécialités remboursables aux assurés sociaux, ne présentait plus, en 2009, un service médical rendu suffisant ; que la spécialité Trolamine, générique de la Biafine et présentant par suite le même service médical rendu, a néanmoins été inscrite sur cette liste le 14 août 2009 ; que les ministres concernés pouvaient, dès lors, légalement se fonder sur le critère du niveau du service médical rendu par cette spécialité pour procéder à la radiation de celle-ci de la liste des médicaments remboursables aux assurés sociaux, les sociétés requérantes n'étant pas fondées à soutenir que cette radiation n'aurait été légalement possible qu'à la condition que le service médical rendu par la Trolamine soit devenu insuffisant postérieurement à son inscription ; <br/>
<br/>
              Sur le moyen de légalité interne relatif à la seule décision radiant la Trolamine de la liste des spécialités agréées à l'usage des collectivités publiques : <br/>
<br/>
              Considérant qu'aux termes de l'article L. 5123-2 du code de la santé publique : " L'achat, la fourniture, la prise en charge et l'utilisation par les collectivités publiques des médicaments définis aux articles L. 5121-8, L.5121-9-1,  L. 5121-12 et L. 5121-13 (...) sont limités, dans les conditions propres à ces médicaments fixées par le décret mentionné à l'article L. 162-17 du code de la sécurité sociale, aux produits agréés dont la liste est établie par arrêté des ministres chargés de la santé et de la sécurité sociale " ; que si aucune disposition législative ou réglementaire n'impose la radiation d'une spécialité de la liste en cause en conséquence de sa radiation de la liste des spécialités remboursables aux assurés sociaux, le motif tiré du niveau du service médical rendu est de nature à fonder légalement l'abrogation, à tout moment, d'une inscription sur la liste des spécialités agréées à l'usage des collectivités, qui revêt le caractère d'un acte réglementaire non créateur de droits ; que les sociétés requérantes ne sont pas fondées à soutenir que cette radiation n'aurait été légalement possible qu'à la condition que le service médical rendu par la Trolamine soit devenu insuffisant postérieurement à son inscription ;<br/>
<br/>
              Sur les autres moyens de légalité interne :<br/>
<br/>
              Considérant qu'il ne ressort pas du dossier que les ministres signataires se seraient crus liés par la teneur des avis successifs de la commission de la transparence ;<br/>
<br/>
              Considérant que si la commission de la transparence a admis, dans ses avis des 28 février 2007 et 26 novembre 2008 relatifs à la spécialité de référence Biafine, qu'il n'existait pas, pour le traitement des érythèmes secondaires à un traitement par radiothérapie, d'alternative thérapeutique bénéficiant d'une prise en charge par l'assurance maladie, elle a néanmoins conclu, y compris pour cette indication, à l'insuffisance du service médical rendu par la Biafine et par ses génériques ; que nonobstant l'existence de travaux dont les requérantes soutiennent qu'ils infirment la position prise par la commission de la transparence, il ressort du dossier que les ministres signataires des arrêtés litigieux ont pu, sans entacher leurs décisions d'erreur manifeste d'appréciation, ne pas maintenir la spécialité Trolamine, même pour cette seule indication, sur les deux listes de médicaments mentionnées ci-dessus ; que, d'ailleurs, cette radiation n'a pris effet qu'à compter du 1er avril 2010, la Trolamine ayant ensuite fait l'objet d'un arrêté du 1er avril 2010 prévoyant, pour une durée de trois ans, sa prise en charge au titre de l'article L. 162-17-2-1 du code de la sécurité sociale, au bénéfice de certains patients atteints d'une affection cancéreuse de longue durée ;<br/>
<br/>
              Considérant qu'en procédant le 15 décembre 2009 à la radiation de la Trolamine des listes en cause à compter seulement du 1er avril 2010, les arrêtés attaqués n'ont pas, dans les circonstances de l'espèce, et sans que les sociétés requérantes puissent utilement faire état de leur " bonne foi ", porté en tout état de cause atteinte aux principes de sécurité juridique et de confiance légitime ; <br/>
<br/>
              Considérant, enfin, qu'il ressort du dossier que les arrêtés attaqués ont été pris, ainsi qu'il a été dit, pour tirer les conséquences de l'évaluation du service médical rendu de la Trolamine et non, comme le soutiennent les sociétés requérantes, par voie de conséquence de la radiation de la spécialité de référence Biafine ; que, par ailleurs, le détournement de pouvoir allégué n'est pas établi ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par le ministre de la santé et des sports relative à l'intérêt pour agir de la SOCIETE ALEPT, les sociétés requérantes ne sont pas fondées à demander l'annulation des décisions attaquées ; que leurs conclusions présentées au titre de l'article L. 761- 1 du code de justice administrative ne peuvent, par voie de conséquence, qu'être rejetées ;<br/>
<br/>
              Considérant que la société Johnson et Johnson Consumer France a été invitée à produire des observations en qualité d'exploitante de la spécialité de référence concurrente de la spécialité générique dont la radiation est contestée ; qu'à défaut d'avoir été appelée en cause, elle n'aurait pas eu qualité pour former tierce opposition à la présente décision si celle-ci avait prononcé l'annulation des arrêtés de radiation ; qu'elle ne peut donc être regardé comme une partie pour l'application de l'article L. 761-1 du code de justice administrative ; que ses conclusions présentées sur le fondement de ces dispositions doivent, par suite, être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la SOCIETE BIOGARAN et de la SOCIETE ALEP est rejetée.<br/>
Article 2 : Les conclusions présentées par la société Johnson et Johnson sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la SOCIETE BIOGARAN, à la SOCIETE ALEPT, à la société Johnson et Johnson Consumer France et au ministre du travail, de l'emploi et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-01-02-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. QUESTIONS GÉNÉRALES. MOTIVATION. MOTIVATION SUFFISANTE. EXISTENCE. - MOTIVATION D'UN ACTE RÉGLEMENTAIRE PAR RÉFÉRENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">61-04-01 SANTÉ PUBLIQUE. PHARMACIE. PRODUITS PHARMACEUTIQUES. - RADIATION D'UNE SPÉCIALITÉ DE LA LISTE DES SPÉCIALITÉS REMBOURSABLES (ART. L. 162-17 DU CSS) - 1) RADIATION POUR INSUFFISANCE DU SERVICE MÉDICAL RENDU - LÉGALITÉ, MÊME DANS LE CAS OÙ LA SPÉCIALITÉ ÉTAIT INITIALEMENT INSCRITE SUR LA LISTE MALGRÉ CETTE INSUFFISANCE - 2) LIEN AUTOMATIQUE ENTRE LA RADIATION DE LA LISTE DES SPÉCIALITÉS REMBOURSABLES AUX ASSURÉS SOCIAUX ET RADIATION DE LA LISTE DES PRODUITS AGRÉÉS POUR L'UTILISATION PAR LES HÔPITAUX ET COLLECTIVITÉS PUBLIQUES (ART. L. 5123-2 DU CSP) -  ABSENCE - 3) LÉGALITÉ EN L'ESPÈCE DE LA RADIATION DE LA SPÉCIALITÉ - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-03-01-02-02-02 L'article R. 163-14 du code de la sécurité sociale, pris pour la transposition de l'article 6 de la directive 89/105/CEE du Conseil du 21 décembre 1988 concernant la transparence des mesures régissant la fixation des prix des médicaments à usage humain et leur inclusion dans le champ d'application des systèmes d'assurance-maladie, prévoit que les décisions portant radiation des listes de spécialités remboursables sont communiquées à l'entreprise avec la mention des motifs de ces décisions. En se référant à l'avis rendu par la commission de la transparence de la Haute autorité de santé le 21 octobre 2009, lui-même communiqué à la société fabricant la spécialité, selon lequel celle-ci présente un service médical rendu insuffisant pour justifier sa prise en charge par l'assurance maladie, l'auteur de la décision n'a pas méconnu la règle de motivation posée.</ANA>
<ANA ID="9B"> 61-04-01 1) Il résulte des dispositions des articles L. 162-17, L. 163-3 et L. 163-7 du code de la sécurité sociale (CSS) qu'un médicament dont le service médical rendu est insuffisant au regard des autres médicaments ou thérapies disponibles ne saurait en principe faire l'objet d'une inscription sur la liste des médicaments remboursables. Si une telle inscription a néanmoins eu lieu, les ministres compétents disposent, en vertu de l'article R. 163-7, de la faculté de procéder à tout moment à la radiation de ce médicament, si son service médical rendu demeure insuffisant. Il en va de même si, postérieurement à l'inscription d'un médicament dont le service médical rendu était suffisant au moment de son inscription, ce service médical devient ultérieurement insuffisant au regard d'autres médicaments ou thérapies disponibles.,,2) Aucune disposition législative ou réglementaire n'impose la radiation d'une spécialité de la liste des produits agréés pour l'utilisation par les hôpitaux et collectivités publiques prévue à l'article L. 5123-2 du code de la santé publique (CSP) en conséquence de sa radiation de la liste des spécialités remboursables aux assurés sociaux prévue à l'article L. 162-17 du code de la sécurité sociale.,,3) En l'espèce, l'administration n'a pas commis d'erreur manifeste d'appréciation en décidant de la radiation de la spécialité en raison de l'insuffisance du service médical rendu, y compris pour l'indication de traitement des érythèmes causés par des traitements de radiothérapie, pour laquelle il n'existe aucune alternative thérapeutique, compte tenu du fait qu'à compter de cette date, cette spécialité a été prise en charge au titre de l'article L. 162-17-2-1 du code de la sécurité sociale, au bénéfice de certains patients atteints d'une affection cancéreuse de longue durée.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., pour une motivation figurant dans la lettre de notification de la décision, CE, 13 juillet 2007, Biocodex, n° 291612, T. p. 645.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
