<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043289902</ID>
<ANCIEN_ID>JG_L_2021_03_000000431786</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/28/99/CETATEXT000043289902.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 24/03/2021, 431786</TITRE>
<DATE_DEC>2021-03-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431786</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>Mme Pearl Nguyên Duy</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:431786.20210324</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Par une requête sommaire, un mémoire complémentaire et deux mémoires en réplique, enregistrés les 19 juin, 18 septembre et 13 décembre 2019 et le 20 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, l'association française du jeu en ligne (AFJEL) demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la délibération du 18 avril 2019 portant communication de l'Autorité de régulation des jeux en ligne (ARJEL) relative à l'application du code de la consommation aux jeux d'argent en ligne ; <br/>
<br/>
              2°) de mettre à la charge de l'ARJEL la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la consommation ; <br/>
              - la loi n° 2010-476 du 12 mai 2010 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme C... A..., maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme B... D..., rapporteure publique.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de l'association française du jeu en ligne.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              Sur le cadre juridique du litige :<br/>
<br/>
              1. Aux termes de l'article 3 de la loi du 12 mai 2010 relative à la concurrence et à la régulation des jeux d'argent et de hasard en ligne, dans sa rédaction applicable à la date de la délibération attaquée : " I. - La politique de l'Etat en matière de jeux d'argent et de hasard a pour objectif de limiter et d'encadrer l'offre et la consommation des jeux et d'en contrôler l'exploitation afin de : / 1° Prévenir le jeu excessif ou pathologique et protéger les mineurs ; / 2° Assurer l'intégrité, la fiabilité et la transparence des opérations de jeu ; / 3° Prévenir les activités frauduleuses ou criminelles ainsi que le blanchiment de capitaux et le financement du terrorisme ; / 4° Veiller au développement équilibré et équitable des différents types de jeu afin d'éviter toute déstabilisation économique des filières concernées (...) ". Aux termes de l'article 34 de la même loi : " I. - L'Autorité de régulation des jeux en ligne est une autorité administrative indépendante. / Elle veille au respect des objectifs de la politique des jeux et des paris en ligne soumis à agrément sur le fondement des articles 11, 12 et 14. (...) ". Aux termes de l'article 38 de cette loi : " Un contrôle permanent de l'activité des opérateurs de jeux ou de paris en ligne agréés est réalisé par l'Autorité de régulation des jeux en ligne aux fins d'assurer le respect des objectifs définis à l'article 3 (...) ". Enfin, aux termes de son article 43 : " I. - Sous réserve des dispositions des articles L. 561-37 et L. 561-38 du code monétaire et financier, la commission des sanctions de l'Autorité de régulation des jeux en ligne peut prononcer, dans les conditions prévues au présent article, des sanctions à l'encontre d'un opérateur de jeux ou de paris en ligne titulaire de l'agrément prévu à l'article 21 de la présente loi. II. - Le collège de l'Autorité de régulation des jeux en ligne peut décider l'ouverture d'une procédure de sanction à l'encontre d'un opérateur de jeux ou de paris en ligne agréé ayant manqué ou manquant aux obligations législatives et réglementaires applicables à son activité, sous réserve des articles L. 561-37 et L. 561-38 du code monétaire et financier (...) ".<br/>
<br/>
              2. Il résulte des dispositions citées ci-dessus que l'Autorité de régulation des jeux en ligne (ARJEL), devenue Autorité nationale des jeux, est chargée de veiller au respect des objectifs de la politique de l'Etat en matière de jeux et de paris en ligne et, à ce titre, de prévenir le jeu excessif, de protéger les mineurs, de garantir la loyauté des opérations de jeu, de faire obstacle aux activités frauduleuses et criminelles et de veiller au développement équilibré des différents types de jeu afin d'éviter toute déstabilisation économique des filières concernées. Il appartient au collège de l'Autorité nationale des jeux de poursuivre à cette fin devant la commission des sanctions de cette autorité  les opérateurs de jeux ou de paris en ligne dont les comportements sont susceptibles de constituer des manquements aux dispositions législatives ou réglementaires applicables à leur activité, dès lors que le respect de ces dispositions concourt au respect des objectifs qui viennent d'être mentionnés et relève, par suite, des missions de contrôle que le législateur a assignées à l'Autorité nationale des jeux.<br/>
<br/>
              Sur la recevabilité du recours :<br/>
<br/>
              3. Par la délibération attaquée du 18 avril 2019, le collège de l'autorité a indiqué, en particulier à l'intention des opérateurs de jeux et paris en ligne, que certaines dispositions du code de la consommation, relatives notamment aux clauses abusives des contrats conclus entre un professionnel et un consommateur, ou aux pratiques commerciales déloyales, étaient susceptibles de s'appliquer à ces opérateurs et que, en cas de méconnaissance de ces dispositions, le collège pourrait poursuivre l'opérateur en question devant la commission des sanctions. L'association française du jeu en ligne demande l'annulation de cette décision.<br/>
<br/>
              4. Les avis, recommandations, mises en garde et prises de position adoptés par les autorités de régulation dans l'exercice des missions dont elles sont investies, peuvent être déférés au juge de l'excès de pouvoir lorsqu'ils revêtent le caractère de dispositions générales et impératives ou lorsqu'ils énoncent des prescriptions individuelles dont ces autorités pourraient ultérieurement censurer la méconnaissance. Ces actes peuvent également faire l'objet d'un tel recours lorsqu'ils sont de nature à produire des effets notables, notamment de nature économique, ou ont pour objet d'influer de manière significative sur les comportements des personnes auxquelles ils s'adressent. Dans ce dernier cas, il appartient au juge, saisi de moyens en ce sens, d'examiner les vices susceptibles d'affecter la légalité de ces actes en tenant compte de leur nature et de leurs caractéristiques, ainsi que du pouvoir d'appréciation dont dispose l'autorité de régulation.<br/>
<br/>
              5. Il résulte de ce qui précède qu'eu égard à la portée de la délibération attaquée, la fin de non-recevoir soulevée par l'Autorité nationale des jeux, tirée de ce que cette délibération n'est pas susceptible de faire l'objet d'un recours pour excès de pouvoir, doit être rejetée.<br/>
<br/>
              Sur la légalité de la délibération attaquée :<br/>
<br/>
              6. Aux termes de l'article liminaire du code de la consommation : " Pour l'application du présent code, on entend par : / - consommateur : toute personne physique qui agit à des fins qui n'entrent pas dans le cadre de son activité commerciale, industrielle, artisanale, libérale ou agricole ; / (...) - professionnel : toute personne physique ou morale, publique ou privée, qui agit à des fins entrant dans le cadre de son activité commerciale, industrielle, artisanale, libérale ou agricole, y compris lorsqu'elle agit au nom ou pour le compte d'un autre professionnel ". Par ailleurs, l'article L. 121-1 du même code, qui interdit les pratiques commerciales déloyales, les définit comme étant celles qui sont : " contraires aux exigences de la diligence professionnelle " et qui " altèrent ou sont susceptibles d'altérer de manière substantielle le comportement économique du consommateur normalement informé et raisonnablement attentif et avisé, à l'égard d'un bien ou d'un service ". Enfin, l'article L. 212-1 définit les clauses abusives comme étant celles qui, " dans les contrats conclus entre professionnels et consommateurs (...) ont pour objet ou pour effet de créer, au détriment du consommateur, un déséquilibre significatif entre les droits et obligations des parties au contrat ".<br/>
<br/>
              7. En premier lieu, il résulte de ce qui a été dit aux points 2 et 3 ci-dessus qu'en indiquant les conditions dans lesquelles elle entend poursuivre devant la commission des sanctions les opérateurs agréés de jeux et paris en ligne qui auraient commis, au regard d'obligations résultant du code de la consommation qui leur seraient applicables, des manquements lui paraissant méconnaître les objectifs que l'Autorité nationale des jeux a légalement pour mission de garantir, celle-ci n'a pas méconnu sa compétence.<br/>
<br/>
              8. En deuxième lieu, en indiquant qu'un opérateur de jeux ou de paris en ligne, qui est, aux termes de l'article 10 de la loi du 12 mai 2010 citée ci-dessus, " (...) toute personne qui, de manière habituelle, propose au public des services de jeux ou de paris en ligne comportant des enjeux en valeur monétaire et dont les modalités sont définies par un règlement constitutif d'un contrat d'adhésion au jeu soumis à l'acceptation des joueurs (...) ", est susceptible d'être regardé comme un " professionnel " au sens de l'article liminaire du code de la consommation cité au point 6, la délibération attaquée n'a pas méconnu les dispositions de cet article.<br/>
<br/>
              9. De même, en troisième lieu, en indiquant qu'un joueur ou un parieur en ligne, qui est, aux termes du même article 10 de la loi du 12 mai 2010, " (...) toute personne qui accepte un contrat d'adhésion au jeu proposé par un opérateur de jeux ou de paris en ligne (...) ", est susceptible d'être regardé comme un " consommateur " au sens du même article liminaire du code de la consommation, la délibération attaquée n'en n'a pas méconnu les dispositions.<br/>
<br/>
              10. En quatrième lieu, en estimant que les contrats de jeux ou de paris en ligne sont susceptibles de comporter des services les faisant entrer dans la catégorie des contrats de services, soumis par suite aux dispositions du code de la consommation relatives aux pratiques commerciales déloyales et aux clauses abusives, la délibération attaquée n'a, contrairement à ce que soutient l'association requérante, pas davantage méconnu les dispositions des articles L. 121-1 et L. 212-1 du code de la consommation ni les autres dispositions du même code relatives à ces contrats.<br/>
<br/>
              11. Enfin, s'il est loisible à une autorité administrative de prendre, ainsi qu'y a procédé le collège de l'autorité par l'acte attaqué, un acte à caractère général visant à faire connaître l'interprétation qu'elle retient de l'état du droit, elle n'est jamais tenue de le faire. Par suite, l'association requérante n'est pas fondée à soutenir que la délibération litigieuse serait illégale, faute de s'être prononcée sur l'application aux paris en ligne des dispositions de l'article R. 212-3 du code de la consommation relatives aux clauses qui sont, de manière irréfragable, réputées abusives.<br/>
<br/>
              12. Il résulte de tout ce qui précède que l'association française du jeu en ligne n'est pas fondée à demander l'annulation de la délibération du 18 avril 2019 qu'elle attaque. Ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées. Par ailleurs, l'Autorité nationale des jeux ne faisant pas précisément état de frais exposés pour défendre à l'instance, ses conclusions présentées au même titre ne peuvent également qu'être rejetées. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
		Article 1er : La requête de l'association française du jeu en ligne est rejetée.<br/>
<br/>
Article 2 : Les conclusions présentées par l'Autorité nationale des jeux au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à l'association française du jeu en ligne et à l'Autorité nationale des jeux.<br/>
		Copie en sera adressée au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-05-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - NOTION. ACTES À CARACTÈRE DE DÉCISION. ACTES NE PRÉSENTANT PAS CE CARACTÈRE. - ACTE DE DROIT SOUPLE - PRISE DE POSITION DE L'A[RJEL] INDIQUANT AUX OPÉRATEURS DE JEUX ET PARIS EN LIGNE QUE LEUR MÉCONNAISSANCE DE DISPOSITIONS DU CODE DE LA CONSOMMATION EST SUSCEPTIBLE DE DONNER LIEU À DES POURSUITES - ACTE SUSCEPTIBLE DE FAIRE L'OBJET D'UN RECOURS POUR EXCÈS DE POUVOIR [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-01-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES CONSTITUANT DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - ACTE DE DROIT SOUPLE - PRISE DE POSITION DE L'A[RJEL] INDIQUANT AUX OPÉRATEURS DE JEUX ET PARIS EN LIGNE QUE LEUR MÉCONNAISSANCE DE DISPOSITIONS DU CODE DE LA CONSOMMATION EST SUSCEPTIBLE DE DONNER LIEU À DES POURSUITES [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">63-01 SPORTS ET JEUX. JEUX D'ARGENT EN LIGNE. - A[RJEL] - 1) A) MISSIONS - B) POUVOIRS DE POURSUITES - 2) CONTESTATION DE L'UN DE SES ACTES DE DROIT SOUPLE INDIQUANT AUX OPÉRATEURS DE JEUX ET PARIS EN LIGNE QUE LEUR MÉCONNAISSANCE DE DISPOSITIONS DU CODE DE LA CONSOMMATION EST SUSCEPTIBLE DE DONNER LIEU À DES POURSUITES - RECEVABILITÉ - EXISTENCE [RJ1] - 3) ESPÈCE - LÉGALITÉ [RJ2].
</SCT>
<ANA ID="9A"> 01-01-05-02-02 Collège de l'Autorité de régulation des jeux en ligne (A[RJEL)] ayant délibéré d'indiquer, en particulier à l'intention des opérateurs de jeux et paris en ligne, que certaines dispositions du code de la consommation, relatives notamment aux clauses abusives des contrats conclus entre un professionnel et un consommateur et aux pratiques commerciales déloyales, étaient susceptibles de s'appliquer à ces opérateurs et que, en cas de méconnaissance de ces dispositions, le collège pourrait poursuivre l'opérateur en question devant la commission des sanctions.,,,Cette délibération, eu égard à sa portée, est susceptible de faire l'objet d'un recours pour excès de pouvoir.</ANA>
<ANA ID="9B"> 54-01-01-01 Collège de l'Autorité de régulation des jeux en ligne (A[RJEL)] ayant délibéré d'indiquer, en particulier à l'intention des opérateurs de jeux et paris en ligne, que certaines dispositions du code de la consommation, relatives notamment aux clauses abusives des contrats conclus entre un professionnel et un consommateur et aux pratiques commerciales déloyales, étaient susceptibles de s'appliquer à ces opérateurs et que, en cas de méconnaissance de ces dispositions, le collège pourrait poursuivre l'opérateur en question devant la commission des sanctions.,,,Cette délibération, eu égard à sa portée, est susceptible de faire l'objet d'un recours pour excès de pouvoir.</ANA>
<ANA ID="9C"> 63-01 1) a) L'Autorité de régulation des jeux en ligne (A[RJEL),] devenue Autorité nationale des jeux (ANJ), est chargée de veiller au respect des objectifs de la politique de l'Etat en matière de jeux et de paris en ligne et, à ce titre, de prévenir le jeu excessif, de protéger les mineurs, de garantir la loyauté des opérations de jeu, de faire obstacle aux activités frauduleuses et criminelles et de veiller au développement équilibré des différents types de jeu afin d'éviter toute déstabilisation économique des filières concernées.,,,b) Il appartient au collège de l'autorité de poursuivre à cette fin devant la commission des sanctions de cette autorité les opérateurs de jeux ou de paris en ligne dont les comportements sont susceptibles de constituer des manquements aux dispositions législatives ou réglementaires applicables à leur activité, dès lors que le respect de ces dispositions concourt au respect des objectifs qui viennent d'être mentionnés et relève, par suite, des missions de contrôle que le législateur a assignées à l'Autorité nationale des jeux.,,,2) Collège de l'A[RJEL] ayant délibéré d'indiquer, en particulier à l'intention des opérateurs de jeux et paris en ligne, que certaines dispositions du code de la consommation, relatives notamment aux clauses abusives des contrats conclus entre un professionnel et un consommateur et aux pratiques commerciales déloyales, étaient susceptibles de s'appliquer à ces opérateurs et que, en cas de méconnaissance de ces dispositions, le collège pourrait poursuivre l'opérateur en question devant la commission des sanctions.,,,Cette délibération, eu égard à sa portée, est susceptible de faire l'objet d'un recours pour excès de pouvoir.,,,3) En indiquant qu'un opérateur de jeux ou de paris en ligne, qui est, aux termes de l'article 10 de la loi n° 2010-476 du 12 mai 2010, (&#133;) toute personne qui, de manière habituelle, propose au public des services de jeux ou de paris en ligne comportant des enjeux en valeur monétaire et dont les modalités sont définies par un règlement constitutif d'un contrat d'adhésion au jeu soumis à l'acceptation des joueurs (&#133;), est susceptible d'être regardé comme un professionnel au sens de l'article liminaire du code de la consommation, la délibération attaquée n'a pas méconnu cet article liminaire.,,,En indiquant qu'un joueur ou un parieur en ligne, qui est, aux termes du même article 10 de la loi du 12 mai 2010, (&#133;) toute personne qui accepte un contrat d'adhésion au jeu proposé par un opérateur de jeux ou de paris en ligne (&#133;), est susceptible d'être regardé comme un consommateur au sens du même article liminaire du code de la consommation, la délibération attaquée n'a pas méconnu cet article liminaire.,,,En estimant que les contrats de jeux ou de paris en ligne sont susceptibles de comporter des services les faisant entrer dans la catégorie des contrats de services, soumis par suite aux dispositions du code de la consommation relatives aux pratiques commerciales déloyales et aux clauses abusives, la délibération attaquée n'a pas davantage méconnu les articles L. 121-1 et L. 212-1 du code de la consommation ni les autres dispositions du même code relatives à ces contrats.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., en ce qui concerne les conditions de recevabilité du recours pour excès de pouvoir contre les actes de droit souple d'une autorité de régulation, CE, Assemblée, 21 mars 2016, Société Fairvesta International GmbH et autres, n°s 368082 368083 368084, p. 76 ; CE, Assemblée, 21 mars 2016, Société NC Numericable, n° 390023, p. 88. Rappr., s'agissant d'une prise de position publique de la CNIL sur le maniement de ses pouvoirs de sanction, CE, 16 octobre 2019, La Quadrature du net et Caliopen, n° 433069, p. 358.,,[RJ2] Cf., en ce qui concerne les modalités d'examen de la légalité des actes de droit souple, CE, Assemblée, 21 mars 2016, Société Fairvesta International GmbH et autres, n°s 368082 368083 368084, p. 76 ; CE, Assemblée, 21 mars 2016, Société NC Numericable, n° 390023, p. 88.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
