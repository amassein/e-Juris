<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028130734</ID>
<ANCIEN_ID>JG_L_2013_10_000000372321</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/13/07/CETATEXT000028130734.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 25/10/2013, 372321, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-10-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372321</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2013:372321.20131025</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 21 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentée par l'association " L'&#140;uvre française ", dont le siège est 4 bis, rue des Caillaux, à Paris (75013), représentée par son président, et par M. A...B..., élisant domicile... ; les requérants demandent au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution du décret du Président de la République du 25 juillet 2013 portant dissolution de l'association " L'&#140;uvre française " ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement à chacun des requérants de la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              ils soutiennent que : <br/>
              - la condition d'urgence est remplie, dès lors que le décret litigieux, en premier lieu, risque de causer un trouble à l'ordre public, en deuxième lieu, viole le principe de la liberté d'expression et, en troisième lieu, porte une atteinte au droit de propriété des membres de l'association requérante ;<br/>
              - il existe plusieurs doutes sérieux quant à la légalité du décret contesté ;<br/>
              - il méconnaît les articles 4 et 24 de la loi n° 2000-321 du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations ; <br/>
              - il comporte des erreurs de fait et fait une inexacte application des 2°, 5° et 6° de l'article L. 212-1 du code de la sécurité intérieure ;<br/>
              - il méconnaît les principes relatifs à la liberté de pensée, de conscience et de religion, à la liberté d'expression et à la liberté de réunion et d'association énoncés aux articles 9 à 11 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - il est entaché d'un détournement de pouvoir ;<br/>
<br/>
<br/>
              Vu le décret dont la suspension de l'exécution est demandé ;<br/>
              Vu la copie de la requête à fin d'annulation de ce décret ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 14 octobre 2013, présenté par le ministre de l'intérieur, qui conclut au rejet de la requête ; il soutient que :<br/>
              - la condition d'urgence n'est pas remplie, dès lors que les requérants ont tardé à agir devant le juge des référés, que la dissolution de l'association requérante n'empêchera pas ses membres de participer à la vie politique et que le décret de dissolution de l'association requérante ne porte atteinte ni aux biens de l'association requérante, ni à ceux de ses membres ;<br/>
              - la procédure administrative liée à la dissolution de l'association requérante s'est déroulée dans le respect du principe du contradictoire ;<br/>
              - le décret litigieux qui repose sur la prise en compte d'éléments factuels multiples et avérés, n'est entaché d'aucune erreur de fait et d'aucune erreur manifeste d'appréciation ;<br/>
              - le décret litigieux ne méconnaît pas les articles 10 et 11 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le mémoire en réplique, enregistré le 20 octobre 2013, présenté par l'association " L'&#140;uvre française " et M.B..., qui reprennent les conclusions et les moyens de leur requête ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, l'association " L'&#140;uvre française " et M.B..., d'autre part, le Premier ministre ainsi que le ministre de l'intérieur ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 21 octobre 2013 à 11 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Boré, avocat au Conseil d'Etat et à la Cour de cassation, avocat des requérants ;<br/>
<br/>
              - M.B... ;<br/>
<br/>
              - le représentant des requérants ;<br/>
<br/>
              - la représentante du ministre de l'intérieur ;<br/>
              et à l'issue de laquelle le juge des référés a prolongé l'instruction jusqu'au 23 octobre 2013 à 12 heures ; <br/>
<br/>
              Vu le nouveau mémoire en défense, enregistré le 22 octobre 2013, présenté par le ministre de l'intérieur, qui reprend les conclusions et moyens de son précédent mémoire ;<br/>
<br/>
                          Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 24 octobre 2013, présentée par l'association " L'&#140;uvre française " et M.B... ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              Vu le code de la sécurité intérieure, notamment son article L. 212-1 ; <br/>
<br/>
              Vu la loi n° 2000-321 du 12 avril 2000 ;<br/>
                          Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 212-1 du code de la sécurité intérieure : " Sont dissous, par décret en conseil des ministres, toutes les associations ou groupements de fait : / (...) 2° (...) qui présentent, par leur forme et leur organisation militaires, le caractère de groupes de combat ou de milices privées ; (...) / 5° Ou qui ont pour but soit de rassembler des individus ayant fait l'objet de condamnation du chef de collaboration avec l'ennemi, soit d'exalter cette collaboration ; / 6° (...) qui, soit provoquent à la discrimination, à la haine ou à la violence envers une personne ou un groupe de personnes à raison de leur origine ou de leur appartenance ou de leur non-appartenance à une ethnie, une nation, une race ou une religion déterminée, soit propagent des idées ou théories tendant à justifier ou encourager cette discrimination, cette haine ou cette violence ; (...) " ; que, par décret du 25 juillet 2013, publié au Journal Officiel le 26 juillet 2013, le Président de la République a, sur le fondement des dispositions des 2°, 5° et 6° de l'article L. 212-1 du code de la sécurité intérieure, prononcé la dissolution de l'association " L'&#140;uvre française " ;<br/>
<br/>
              3. Considérant que les requérants soutiennent, en premier lieu, que les droits prévus par les articles 4 et 24 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations n'ont pas été respectés lors de la procédure administrative qui a précédé l'adoption du décret dans la mesure où ils n'ont pas été informés de l'identité de l'agent chargé de traiter leur dossier et où les arguments qu'ils ont présentés lors de l'entretien qui leur a été accordé n'ont pas fait l'objet d'un procès-verbal contradictoire et ne sont pas mentionnés dans la motivation du décret ; qu'il ressort toutefois des pièces du dossier, et qu'il n'est d'ailleurs pas sérieusement contesté, que les requérants ont eu connaissance de l'identité du fonctionnaire chargé de recueillir leurs observations ; que les dispositions de l'article 24 de la loi du 12 avril 2000 n'imposent pas la tenue d'un procès-verbal contradictoire ; que le décret contesté a pu, sans insuffisance de motivation, ne pas mentionner les arguments invoqués par les requérants dans le cadre de leurs observations orales ; que, par suite, les moyens tirés de la violation des articles 4 et 24 de la loi du 12 avril 2000 ne sont pas, en l'état de l'instruction, de nature à susciter un doute sérieux quant à la légalité du décret contesté ;<br/>
<br/>
              4. Considérant que les requérants soutiennent, en deuxième lieu, que l'association " L'Oeuvre française " n'est pas au nombre des associations visées par le 6° de l'article L. 212-1 du code de la sécurité intérieure ; que le décret contesté retient, sur ce point, que l'association propage, dans ses publications, les réunions et forums qu'elle organise, les déclarations de ses dirigeants ainsi que les articles qu'elle publie dans des revues nationalistes, une idéologie incitant à la haine et à la discrimination envers des personnes en raison de leur nationalité étrangère, de leur origine ou de leur confession musulmane ou juive ; que l'administration a produit dans le cadre de l'instruction des éléments précis et concordants, notamment en ce qui concerne les déclarations du président de l'association ainsi que les communiqués et articles diffusés par cette dernière, de nature à établir la réalité des faits allégués ; que les dénégations générales des requérants, qui se bornent à faire valoir que les statuts de l'association ainsi que la charte qu'elle a adoptée en 2000 ne contreviennent pas aux dispositions du 6° de l'article L. 212-1, que certains des propos ou écrits ne sont pas imputables aux dirigeants de l'association, sans pour autant nier qu'ils aient été tenus ou rédigés dans le cadre des activités de l'association ou en relation avec ces dernières, ne sauraient être regardées comme suffisantes pour infirmer la réalité de ces faits ou leur inexacte appréciation ; que la seule circonstance que M. B...n'aurait pas tenu les propos antisémites qui lui sont imputés par le décret litigieux n'est pas non plus suffisante à cet égard ; que, par suite, le moyen tiré de la violation du 6° de l'article L. 212-1 du code de la sécurité intérieure n'est pas de nature, en l'état de l'instruction, à susciter un doute sérieux sur la légalité du décret litigieux ;<br/>
<br/>
              5. Considérant, en troisième lieu, que les requérants soutiennent que l'association " L'&#140;uvre française " ne répond pas aux conditions prévues par le 5° de l'article L. 212-1 du code de la sécurité intérieure ; que le décret litigieux retient, sur ce point, que l'association, qui rend hommage à la mémoire de responsables du régime de Vichy et de miliciens condamnés pour collaboration ou intelligence avec l'ennemi, qui prend comme modèle le régime de Vichy, dont elle utilise certains emblèmes, et qui sert de tribune à certains négationnistes, exalte la collaboration avec l'ennemi ; que, si les requérants font valoir que les commémorations auxquelles ils participent se bornent à exalter l'engagement en faveur de la Nation et que l'association requérante ne saurait être regardée comme responsable de thèses négationnistes développées par des tiers, ces allégations générales, qui ne sont pas étayées par les pièces du dossier, ne peuvent être regardées comme suffisantes pour contrebattre les éléments précis et concordants produits par l'administration afin d'établir la réalité des faits invoqués ainsi que leur exacte appréciation ; que, par suite, le moyen tiré de la violation du 5° de l'article L. 212-1 du code de la sécurité intérieure n'est pas davantage, en l'état de l'instruction, de nature à susciter un doute sérieux sur la légalité du décret litigieux ;<br/>
<br/>
              6. Considérant, en quatrième lieu, que les requérants soutiennent que l'association " L'Oeuvre française " ne répond pas aux conditions prévues par le 2° de l'article L. 121-1 du code de la sécurité intérieure ; que, sur ce point aussi, les dénégations générales qu'ils opposent et qui ne sont nullement étayées par les pièces du dossier, ne sont pas de nature à faire regarder les motifs du décret contesté, à l'appui desquels l'administration a produit des éléments convaincants, comme fondés sur des erreurs de fait ou sur une inexacte application des dispositions rappelées ci-dessus ;<br/>
<br/>
              7. Considérant, enfin, que si les requérants invoquent la violation des stipulations des articles 9 à 11 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et soutiennent que le décret contesté serait entaché de détournement de pouvoir, ils n'apportent aucune précision de nature à permettre d'en apprécier le bien fondé ; qu'ainsi, ces moyens ne peuvent pas non plus être regardés comme étant, en l'état de l'instruction, de nature à susciter un doute sérieux sur la légalité du décret litigieux ; <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que, l'une des conditions posées par l'article L. 521-1 du code de justice administrative n'étant pas remplie, la requête de l'association " L'&#140;uvre française " et de M. B...ne peut qu'être rejetée, y compris ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de l'association " L'&#140;uvre française "  et de M. B...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à l'association " L'&#140;uvre française ", à M. A... B..., au Premier ministre et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
