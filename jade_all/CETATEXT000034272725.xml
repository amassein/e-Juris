<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034272725</ID>
<ANCIEN_ID>JG_L_2017_03_000000387640</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/27/27/CETATEXT000034272725.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 17/03/2017, 387640, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387640</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Ophélie Champeaux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:387640.20170317</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société civile immobilière (SCI) La Rainville II a demandé au tribunal administratif d'Orléans de prononcer la décharge des rappels de taxe sur la valeur ajoutée auxquels elle a été assujettie au titre de la période du 1er janvier 2006 au 31 décembre 2007, ainsi que des intérêts de retard dont ces rappels ont été assortis. Par un jugement n° 1102787 du 14 mai 2013, le tribunal administratif, après avoir décidé qu'il n'y avait plus lieu de statuer sur les sommes ayant fait l'objet d'un dégrèvement en cours d'instance, a rejeté le surplus de cette demande.<br/>
<br/>
              Par un arrêt n° 13NT02069 du 27 novembre 2014, la cour administrative d'appel de Nantes a rejeté l'appel formé par la société La Rainville II contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 3 février et 4 mai 2015 et le 11 mars 2016 au secrétariat du contentieux du Conseil d'Etat, la société La Rainville II demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Ophélie Champeaux, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la société civile immobilière La Rainville II ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à l'issue d'une vérification de comptabilité portant sur la période du 1er janvier 2006 au 31 décembre 2007, l'administration a constaté que la société La Rainville II, dont le liquidateur était depuis sa dissolution, le 20 mars 2006, M. A...B..., ne s'était pas acquittée de la taxe sur la valeur ajoutée due, en vertu du 6° de l'article 257 du code général des impôts, à raison de la cession le 29 janvier 2007, en deux lots de 636 et 640 mètres carrés, d'une partie d'un immeuble acquis le 24 décembre 1998 et situé 56-56 bis rue Pierre Brossolette à Rueil-Malmaison (Hauts-de-Seine). L'administration a, par ailleurs, entendu remettre en cause un crédit de taxe sur la valeur ajoutée reportable à la clôture de l'exercice clos le 31 décembre 2004. Les rappels de taxe sur la valeur ajoutée en résultant ont été mis en recouvrement le 10 décembre 2010, et ont été assortis des intérêts de retard prévus à l'article 1727 du code général des impôts. La requérante se pourvoit en cassation contre l'arrêt du 27 novembre 2014 par lequel la cour administrative d'appel de Nantes a rejeté son appel contre l'article 2 du jugement du 14 mai 2013 par lequel le tribunal administratif d'Orléans a rejeté le surplus de sa demande tendant à la décharge des droits et intérêts de retard demeurant à.sa charge<br/>
<br/>
              Sur l'étendue du litige :<br/>
<br/>
              2. Par une décision en date du 18 février 2016, postérieure à l'introduction du pourvoi, l'administration a accordé à la société La Rainville II un dégrèvement partiel des rappels de taxe sur la valeur ajoutée et des pénalités en litige, à hauteur de 23 607 euros. Par suite, les conclusions de son pourvoi relatives à ces sommes dégrevées sont devenues sans objet et il n'y a, dès lors, pas lieu d'y statuer.<br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué :<br/>
<br/>
              3. Aux termes de l'article 257 du code général des impôts, dans sa rédaction alors applicable : " Sont également soumis à la taxe sur la valeur ajoutée : / (...) 6° Les opérations qui portent sur des immeubles, des fonds de commerce ou des actions ou parts de sociétés immobilières et dont les résultats doivent être compris dans les bases de l'impôt sur le revenu au titre des bénéfices industriels ou commerciaux ; ". Aux termes de l'article 35 du même code : " I. Présentent également le caractère de bénéfices industriels et commerciaux, pour l'application de l'impôt sur le revenu, les bénéfices réalisés par les personnes physiques désignées ci-après : / 1° Personnes qui, habituellement, achètent en leur nom, en vue de les revendre, des immeubles, des fonds de commerce, des actions ou parts de sociétés immobilières (...) ". Il résulte de ces dispositions que les opérations portant sur des immeubles visées par le 6° de l'article 257 du code général des impôts s'entendent de celles dont le profit, lorsqu'elles sont réalisées par une personne physique, est, pour l'application de l'impôt sur le revenu, regardé comme un bénéfice industriel ou commercial. Pour autant, ces dispositions n'ont ni pour objet, ni pour effet, d'exclure de leur champ d'application les personnes assujetties à l'impôt sur les sociétés. En conséquence, c'est sans erreur de droit ni contradiction de motifs que la cour a jugé qu'était inopérant, à l'appui de la demande de décharge de taxe sur la valeur ajoutée que présentait la société requérante, le moyen tiré de ce qu'elle ne relevait pas de l'impôt sur le revenu mais de l'impôt sur les sociétés et, qu'ainsi, les premiers juges n'étaient pas tenus d'y répondre.<br/>
<br/>
              En ce qui concerne la régularité de la procédure d'imposition :<br/>
<br/>
              4. Aux termes de l'article L. 57 du livre des procédures fiscales : " L'administration adresse au contribuable une proposition de rectification qui doit être motivée de manière à lui permettre de formuler ses observations ou de faire connaître son acceptation. / (...) Lorsque l'administration rejette les observations du contribuable sa réponse doit également être motivée. ". Aux termes de l'article R. 57-1 du même livre : " La proposition de rectification prévue par l'article L. 57 fait connaître au contribuable la nature et les motifs de la rectification envisagée. L'administration invite, en même temps, le contribuable à faire parvenir son acceptation ou ses observations dans un délai de trente jours à compter de la réception de la proposition, prorogé, le cas échéant, dans les conditions prévues au deuxième alinéa de cet article. ". Il résulte de ces dispositions que, pour être régulière, une proposition de rectification doit comporter la désignation de l'impôt concerné, de l'année d'imposition et de la base d'imposition et énoncer les motifs sur lesquels l'administration entend se fonder pour justifier les rectifications envisagées, de façon à permettre au contribuable de formuler utilement ses observations. En revanche, sa régularité ne dépend pas du bien-fondé de ces motifs.<br/>
<br/>
              5. En premier lieu, la requérante soutenait devant la cour que la proposition de rectification du 5 juin 2009 était insuffisamment motivée en raison d'erreurs sur le régime de TVA applicable. Il résulte de ce qui a été dit au point 4 que la cour, qui a relevé que la proposition de rectification comportait toutes les informations nécessaires, a pu, sans erreur de droit ni dénaturation des pièces du dossier et au terme d'une motivation suffisante, juger que les erreurs alléguées étaient sans incidence sur la régularité de la procédure d'imposition.<br/>
<br/>
              6. En second lieu, en jugeant que la réponse du 3 août 2009 aux observations du contribuable contenait l'exposé suffisant des motifs pour lesquels l'administration estimait devoir maintenir les redressements, la cour a porté sur les faits ressortant des pièces du dossier qui lui était soumis, sans les dénaturer, une appréciation souveraine qui ne peut être utilement discutée devant le juge de cassation.<br/>
<br/>
              En ce qui concerne le bien-fondé de l'imposition :<br/>
<br/>
              7. En premier lieu, il résulte de ce qui a été dit au point 3 ci-dessus que la cour a pu, sans erreur de droit ni contradiction de motifs, écarter comme inopérant le moyen tiré de ce que la soumission de la société requérante à l'impôt sur les sociétés faisait obstacle à ce que des rappels de taxe sur la valeur ajoutée lui soient appliqués sur le fondement des dispositions du 6° de l'article 257 du code général des impôts.<br/>
<br/>
              8. En second lieu, il résulte des pièces du dossier soumis aux juges du fond que la société a acquis un immeuble le 24 décembre 1998 dans le cadre d'un projet de construction-vente, qu'elle a procédé à la vente d'un premier lot issu de cet immeuble le 25 mars 2004, puis à la vente de deux autres lots le 29 janvier 2007 et qu'elle avait antérieurement procédé à la vente en 1998 d'un terrain à bâtir, situé à Vendôme (Loir-et-Cher), acquis en 1997, et à un programme de construction-vente par lots d'une résidence dite EDEN et d'une usine à Châteaudun (Eure-et-Loir) entre 1998 et 2001.<br/>
<br/>
              9. A l'appui de son arrêt, la cour a relevé que la requérante avait procédé par le passé à d'autres ventes d'immeubles. En déduisant de cette circonstance et de celle que l'ensemble immobilier concerné avait fait l'objet de cessions par lots, que les opérations de revente en litige revêtaient un caractère habituel, la cour, qui n'a pas commis d'erreur de droit, a suffisamment motivé son arrêt et donné aux faits qui figuraient à son dossier leur exacte qualification. En jugeant que la circonstance que la requérante n'ait pas respecté l'engagement de construire qu'elle avait souscrit lors de l'acquisition de l'immeuble n'avait pas fait disparaître l'intention spéculative à cette date, la cour s'est livrée à une appréciation souveraine des faits exempte de dénaturation.<br/>
<br/>
              10. Il résulte de tout ce qui précède que la SCI La Rainville II n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit à ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions du pourvoi à hauteur de la somme de 23 607 euros.<br/>
Article 2 : Le surplus des conclusions du pourvoi de la société La Rainville II est rejeté.<br/>
Article 3 : La présente décision sera notifiée à la société La Rainville II et au ministre de l'économie et des finances. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
