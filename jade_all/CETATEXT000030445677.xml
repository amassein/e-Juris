<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030445677</ID>
<ANCIEN_ID>JG_L_2015_03_000000375636</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/44/56/CETATEXT000030445677.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 19/03/2015, 375636, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-03-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375636</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP TIFFREAU, MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:375636.20150319</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              La SA BNP Paribas a demandé au tribunal administratif de Montreuil de prononcer la décharge des cotisations supplémentaires d'impôt sur les sociétés et des pénalités correspondantes auxquelles elle a été assujettie au titre de l'exercice clos en 2001. Par un jugement n° 0910929 du 3 février 2011, le tribunal administratif de Montreuil a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 11VE01496 du 19 décembre 2013, la cour administrative d'appel de Versailles a, sur l'appel de la société BNP Paribas, annulé ce jugement et prononcé la décharge des impositions litigieuses.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 20 février et 21 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, le ministre délégué, chargé du budget, demande au Conseil d'Etat d'annuler l'arrêt n° 11VE01496 du 19 décembre 2013 de la cour administrative d'appel de Versailles.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bastien Lignereux, auditeur,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Tiffreau, Marlange, de la Burgade, avocat de la SA BNP Paribas.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une convention du 23 juillet 1998, la société BNP Paribas s'est engagée à garantir à sa succursale britannique, comme prix de référence pour la réalisation d'un portefeuille obligataire de pays émergents qu'elle détenait, le prix correspondant au niveau des cours du marché à la date de signature de la convention. Aux termes de cette convention, cette garantie était rémunérée par une commission de 0,25 % de la valeur du portefeuille, à laquelle pouvait s'ajouter un complément égal à 10 % de la plus-value potentielle en cas de remboursement ou de cession à un prix supérieur au prix de référence. La société BNP Paribas a comptabilisé, au titre de l'exercice clos en 2001, une charge de 5 473 186 euros au titre de cet engagement ainsi qu'une provision d'un montant de 10 916 469 euros, à raison de ce même engagement sur la base des cours des titres au 31 décembre 2001. A l'issue de la vérification de comptabilité dont la société a fait l'objet, l'administration a procédé à la réintégration de ces sommes, estimant que le mécanisme en cause constituait un acte anormal de gestion. Le ministre délégué, chargé du budget, se pourvoit en cassation contre l'arrêt du 19 décembre 2013 par lequel la cour administrative d'appel de Versailles a annulé le jugement du 3 février 2011 du tribunal administratif de Montreuil et prononcé la décharge des cotisations supplémentaires d'impôt sur les sociétés et des pénalités correspondantes auxquelles la société BNP Paribas a été assujettie au titre de l'exercice clos en 2001.<br/>
<br/>
              2. Aux termes de l'article 38 du code général des impôts, applicable en matière d'impôt sur les sociétés en vertu du I de l'article 209 du même code : " 1. (...) le bénéfice imposable est le bénéfice net, déterminé d'après les résultats d'ensemble des opérations de toute nature effectuées par les entreprises, y compris notamment les cessions d'éléments quelconques de l'actif, soit en cours, soit en fin d'exploitation./ 2. Le bénéfice net est constitué par la différence entre les valeurs de l'actif net à la clôture et à l'ouverture de la période dont les résultats doivent servir de base à l'impôt diminuée des suppléments d'apport et augmentée des prélèvements effectués au cours de cette période par l'exploitant ou par les associés. L'actif net s'entend de l'excédent des valeurs d'actif sur le total formé au passif par les créances des tiers, les amortissements et les provisions justifiées./ (...) ". Aux termes de l'article 209 du même code, dans sa rédaction applicable au litige : " I. (...) les bénéfices passibles de l'impôt sur les sociétés sont déterminés d'après les règles fixées par les articles 34 à 45, 53 A à 57 et 302 septies A bis et en tenant compte uniquement des bénéfices réalisés dans les entreprises exploitées en France ainsi que ceux dont l'imposition est attribuée à la France par une convention relative aux doubles impositions./ (...) ".<br/>
<br/>
              3. Il résulte de ces dispositions que, lorsqu'une société dont le siège est en France exerce dans une succursale à l'étranger, une activité industrielle ou commerciale, il n'y a pas lieu de tenir compte, pour la détermination des bénéfices imposables en France, des variations d'actif net imputables à des événements qui se rattachent à l'activité exercée par cette succursale. En revanche, si la succursale entretient avec le siège des relations commerciales favorisant le maintien ou le développement des activités en France de la société, celle-ci peut déduire de ses résultats imposables les pertes, subies ou régulièrement provisionnées, résultant des aides apportées à la succursale dans le cadre de ces relations.<br/>
<br/>
              4. Pour juger que la charge et la provision, mentionnées au point 1, que la société BNP Paribas avait comptabilisées au titre de l'engagement de garantie, étaient déductibles de ses résultats imposables en France, la cour a relevé que le siège et la succursale constituaient une seule et même personne morale et que leurs intérêts ne se dissociaient pas. En statuant ainsi, alors qu'elle devait rechercher si l'aide ainsi consentie par le siège à sa succursale s'inscrivait dans le cadre de relations commerciales favorisant le maintien ou le développement d'une activité exercée en France, la cour a commis une erreur de droit. Pour ce motif, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, le ministre est fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : L'arrêt du 19 décembre 2013 de la cour administrative d'appel de Versailles est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : Les conclusions de la SA BNP Paribas au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre des finances et des comptes publics et à la SA BNP Paribas.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
