<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033237384</ID>
<ANCIEN_ID>JG_L_2016_10_000000389998</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/23/73/CETATEXT000033237384.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 12/10/2016, 389998</TITRE>
<DATE_DEC>2016-10-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389998</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Yannick Faure</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:389998.20161012</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une ordonnance n° 1404378 du 6 mai 2015, enregistrée le 7 mai 2015 au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée à ce tribunal par la société Centrale d'achats de l'hospitalisation privée et publique. <br/>
<br/>
              Par cette requête, par un mémoire en réplique et par deux nouveaux mémoires, enregistrés les 19 mars 2014, 11 février 2015, 25 février 2015 et 13 mars 2015 au greffe du tribunal administratif de Paris, et par deux nouveaux mémoires, enregistrés les 28 janvier et 2 mars 2016 au secrétariat du contentieux du Conseil d'Etat, la société Centrale d'achats de l'hospitalisation privée et publique demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 8 janvier 2014 du ministre des affaires sociales et de la santé, du ministre de la défense, du ministre délégué auprès du ministre de l'économie et des finances, chargé du budget, et du ministre délégué auprès du ministre des affaires sociales et de la santé, chargé des personnes handicapées et de la lutte contre l'exclusion, portant approbation de modifications de la convention constitutive d'un groupement d'intérêt public créé dans le domaine de l'action sanitaire et sociale ;<br/>
<br/>
              2°) de mettre à la charge du groupement d'intérêt public Réseau des acheteurs hospitaliers d'Ile-de-France (RESAH-IDF) la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la défense ;<br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2011-525 du 17 mai 2011 ;<br/>
              - le décret n° 2012-91 du 26 janvier 2012 ;<br/>
              - l'arrêté du 23 mars 2012 pris en application de l'article 3 du décret n° 2012-91 du 26 janvier 2012 relatif aux groupements d'intérêt public ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yannick Faure, auditeur,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 28 septembre 2016, présentée pour le groupement d'intérêt public Réseau des acheteurs hospitaliers d'Ile-de-France.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article 98 de la loi du 17 mai 2011 de simplification et d'amélioration de la qualité du droit : " Le groupement d'intérêt public est une personne morale de droit public dotée de l'autonomie administrative et financière. Il est constitué par convention approuvée par l'Etat soit entre plusieurs personnes morales de droit public, soit entre l'une ou plusieurs d'entre elles et une ou plusieurs personnes morales de droit privé. / Ces personnes y exercent ensemble des activités d'intérêt général à but non lucratif, en mettant en commun les moyens nécessaires à leur exercice " ; qu'aux termes de l'article 100 de la même loi : " (...) L'Etat approuve la convention constitutive ainsi que son renouvellement et sa modification (...) " ;<br/>
<br/>
              2. Considérant que, par un arrêté du 8 janvier 2014 pris sur le fondement de ces dispositions, le ministre des affaires sociales et de la santé, le ministre de la défense, le ministre chargé du budget et le ministre chargé des personnes handicapées et de la lutte contre l'exclusion ont approuvé les modifications de la convention constitutive du groupement d'intérêt public Réseau des acheteurs hospitaliers d'Ile-de-France (RESAH-IDF), constitué dans le but d'organiser, dans le domaine des achats, la coopération entre les organismes et services sanitaires, sociaux et médico-sociaux franciliens qui le composent ; que la société Centrale d'achats de l'hospitalisation privée et publique demande l'annulation pour excès de pouvoir de cet arrêté ;<br/>
<br/>
              Sur les fins de non-recevoir opposées par le ministre des affaires sociales, de la santé et des droits des femmes et le groupement d'intérêt public Réseau des acheteurs hospitaliers d'Ile-de-France :<br/>
<br/>
              3. Considérant que, contrairement à ce que soutiennent le ministre des affaires sociales, de la santé et des droits des femmes et le groupement d'intérêt public Réseau des acheteurs hospitaliers d'Ile-de-France, la société Centrale d'achats de l'hospitalisation privée et publique, qui a pour objet, aux termes de l'article 2 de ses statuts, " d'effectuer toutes les opérations de référencement et de conseil ", en particulier à destination des établissements de santé, justifie d'un intérêt lui donnant qualité pour agir contre l'arrêté attaqué ;<br/>
<br/>
              Sur la légalité de l'arrêté attaqué :<br/>
<br/>
              4. Considérant qu'aux termes de l'article 3 du décret du 26 janvier 2012 relatif aux groupements d'intérêt public : " I. - Un arrêté du Premier ministre détermine les documents et informations, permettant de vérifier la légalité de la convention constitutive d'un groupement d'intérêt public et d'apprécier son contenu au regard de l'ensemble des intérêts généraux dont l'Etat a la charge, qui sont adressés à l'autorité compétente pour son approbation. (...) II. - En cas de modification de la convention constitutive, ces documents et informations comprennent notamment : / 1° La convention résultant des modifications envisagées ; / 2° La décision prise par l'organe compétent du groupement ; / 3° Les documents permettant d'attester que chacun des membres du groupement s'est prononcé valablement ; / 4° La justification du nouveau régime comptable, lorsque la modification concerne ce régime ; / 5° Les délibérations des organes compétents des membres qui adhèrent ou se retirent et, le cas échéant, leur approbation prévue par les textes qui les régissent, lorsque la modification porte sur l'adhésion ou le retrait de membres (...) " ; qu'aux termes de l'article 2 de l'arrêté du Premier ministre du 23 mars 2012, pris pour l'application de ces dispositions : " En cas de modification de la convention constitutive du groupement nécessitée par l'adhésion (...) d'un ou plusieurs membres, (...) outre les documents et informations mentionnés au II de l'article 3 du décret susvisé, sont adressés (...) les comptes prévisionnels du groupement pour les trois années à venir " ;<br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier qu'ont été transmis aux ministres compétents pour approuver les modifications apportées à la convention constitutive du groupement d'intérêt public la convention résultant des modifications envisagées et les délibérations des 15 décembre 2011, 11 mai 2012 et 14 mai 2013 de l'assemblée générale procédant à ces modifications, conformément aux dispositions des 1° et 2° du II de l'article 3 du décret du 26 janvier 2012 ; que ces modifications ne concernant pas le régime comptable, les dispositions du 4° du II du même article du même décret n'impliquaient pas, en l'espèce, que soit justifié un nouveau régime comptable ;<br/>
<br/>
              6. Considérant, par ailleurs, que l'article 17 de la convention constitutive prévoit, ainsi que le permettent les articles 99, 102 et 105 de la loi du 17 mai 2011, que le conseil d'administration peut procéder à l'admission de nouveaux membres répondant aux critères d'adhésion définis à l'article 5 ; que les délibérations du conseil d'administration ayant cet objet ne peuvent être regardées comme des modifications de la convention portant sur l'adhésion de membres ; que si les stipulations de la convention constitutive relatives aux critères d'adhésion des nouveaux membres ont été modifiées, le 11 mai 2012, pour ouvrir au service de santé des armées, constituant en vertu de l'article R. 3233-1 du code de la défense " un service interarmées " non distinct de l'Etat, la faculté d'adhérer au groupement, il ressort des pièces du dossier qu'a été transmise aux ministres qui ont pris l'arrêté attaqué une demande d'adhésion du médecin général des armées, directeur central du service des santé des armées, du 30 septembre 2011, conformément au 5° du II de l'article 3 du décret précité du 26 janvier 2012, ainsi que les comptes prévisionnels pour les trois années à venir, conformément aux dispositions de l'article 2 de l'arrêté du 23 mars 2012 ;<br/>
<br/>
              7. Considérant, en revanche, qu'il ressort des pièces du dossier que n'ont pas été transmises aux ministres les délibérations ou décisions des organes compétents des membres du groupement se prononçant sur les modifications apportées à la convention constitutive par les assemblées générales des 15 décembre 2011, 11 mai 2012 et 14 mai 2013 ; <br/>
<br/>
              8. Considérant, il est vrai, que s'agissant des établissements publics de santé, leur directeur avait compétence pour approuver les modifications apportées à la convention constitutive, en vertu des dispositions combinées des articles L. 6143-1 et L. 6143-7 du code de la santé publique, de sorte que l'approbation des modifications de la convention constitutive par ces établissements aurait pu valablement résulter du vote de leur directeur, ou d'une personne justifiant d'une délégation régulière de sa part, lors des réunions de l'assemblée générale du groupement ; que, toutefois, si les délibérations de l'assemblée générale du groupement qui ont été transmises aux ministres comportaient le nom et la qualité des personnes représentant les organismes et services adhérents, elles n'étaient pas accompagnées des pièces permettant de s'assurer que ces personnes avaient qualité pour les représenter valablement, alors qu'elles n'assumaient pas, pour la quasi-totalité d'entre elles, la direction des organismes et services concernés ; qu'en réponse à la mesure d'instruction ordonnée par la première chambre de la section du contentieux, le ministre s'est borné à transmettre les feuilles d'émargement des séances correspondantes ainsi que certaines pièces attestant des fonctions occupées par les personnes présentes, telles que des arrêtés de nomination, voire de simples avis de marché public ou organigrammes ; qu'il n'a produit, pour la plupart d'entre elles, aucune pièce attestant de leur habilitation à représenter les organismes ou services adhérents ; qu'est à cet égard sans incidence la circonstance que, en vertu de l'article 16 de la convention constitutive, aucun quorum n'était requis lors de l'assemblée générale du 14 mai 2013, qui faisait suite à une première assemblée générale au cours de laquelle le quorum n'avait pas été atteint ; <br/>
<br/>
              9. Considérant, dès lors, que la société requérante est fondée à soutenir que les membres du groupement ne se sont pas valablement prononcés sur les modifications apportées à la convention constitutive lors des assemblées générales des 15 décembre 2011, 11 mai 2012 et 14 mai 2013, et qu'en conséquence, les ministres qui ont pris l'arrêté attaqué n'ont pu légalement approuver ces modifications ;<br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens de la requête, que la société Centrale d'achats de l'hospitalisation privée et publique est fondée à demander l'annulation de l'arrêté qu'elle attaque ; que la présente décision n'appelant aucune mesure d'exécution, les conclusions aux fins d'injonction présentées par le groupement d'intérêt public Réseau des acheteurs hospitaliers d'Ile-de-France doivent être rejetées ; que, par ailleurs, dès lors qu'il ne ressort pas des pièces du dossier que l'annulation de l'arrêté attaqué soit de nature à emporter des conséquences manifestement excessives, il n'y a pas lieu, dans les circonstances de l'espèce, de reporter les effets de  son annulation à la date d'entrée en vigueur de l'arrêté interministériel du 14 avril 2015 approuvant de nouvelles modifications apportées à sa convention constitutive ;<br/>
<br/>
              Sur les frais exposés et non compris dans les dépens :<br/>
<br/>
              11. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Centrale d'achats de l'hospitalisation privée et publique, qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre une somme à la charge du groupement d'intérêt public Réseau des acheteurs hospitaliers d'Ile-de-France au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêté de la ministre des affaires sociales et de la santé, du ministre de la défense, du ministre délégué auprès du ministre de l'économie et des finances, chargé du budget, et de la ministre déléguée auprès de la ministre des affaires sociales et de la santé, chargée des personnes handicapées et de la lutte contre l'exclusion du 8 janvier 2014 portant approbation de modifications de la convention constitutive d'un groupement d'intérêt public créé dans le domaine de l'action sanitaire et sociale est annulé.<br/>
Article 2 : Les conclusions présentées par les parties au titre de l'article L. 761-1 du code de justice administrative ainsi que les conclusions aux fins d'injonction présentées par le groupement d'intérêt public Réseau des acheteurs hospitaliers d'Ile-de-France sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la société Centrale d'achats de l'hospitalisation privée et publique, au groupement d'intérêt public Réseau des acheteurs hospitaliers d'Ile-de-France, au ministre de l'économie et des finances, au ministre de la défense et à la ministre des affaires sociales et de la santé.<br/>
Copie en sera adressée à la ministre des familles, de l'enfance et des droits des femmes.<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">33-03 Établissements publics et groupements d'intérêt public. Groupements d'intérêt public.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
