<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042504503</ID>
<ANCIEN_ID>JG_L_2020_11_000000440992</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/50/45/CETATEXT000042504503.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 04/11/2020, 440992, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440992</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Martin Guesdon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:440992.20201104</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Lille, à titre principal, d'annuler la décision implicite de rejet née du silence gardé par le président de la région Hauts-de-France sur sa demande du 24 juin 2016 tendant au versement d'une subvention d'équilibre au fonds de retraite géré par l'association de gestion des oeuvres sociales du personnel de la région Picardie (AGOS) et la caisse nationale de prévoyance (CNP) pour permettre le versement de la rente viagère à laquelle il estime avoir droit et d'enjoindre à la région Hauts-de-France de verser au fonds de retraite les sommes nécessaires et, à titre subsidiaire, de condamner la région Hauts-de-France à lui verser la somme de 109 760 euros en réparation du préjudice qu'il estime avoir subi du fait de l'abstention de la région de verser une subvention d'équilibre. Par un jugement n° 1608005 du 26 octobre 2018, le tribunal administratif de Lille a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 18DA02658 du 2 avril 2020, la cour administrative d'appel de Douai, réformant ce jugement sur appel de M. B..., a annulé la décision implicite de rejet du président de la région Hauts-de-France en tant qu'elle refuse le versement à l'AGOS de la subvention d'équilibre couvrant la charge nécessaire au financement de la rente viagère acquise par M. B... à la date du 30 mars 1992, enjoint à la région de verser cette subvention dans un délai de six mois et rejeté le surplus des conclusions de M. B....<br/>
<br/>
              Procédures devant le Conseil d'Etat<br/>
<br/>
              1° Sous le n° 440992, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 2 et le 22 juin 2020 au secrétariat du contentieux du Conseil d'Etat, la région Hauts-de-France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. B... ;<br/>
<br/>
              3°) de mettre à la charge de M. B... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              2° Sous le n° 441346, par une requête enregistrée le 22 juin 2020 au secrétariat du contentieux du Conseil d'Etat, la région Hauts-de-France demande au Conseil d'Etat d'ordonner, en application de l'article R. 821-5 du code de justice administrative, qu'il soit sursis à l'exécution de l'arrêt de la cour administrative d'appel de Douai.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Martin Guesdon, auditeur,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la région Hauts-de-France ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Le pourvoi par lequel la région Hauts-de-France demande l'annulation de l'arrêt du 2 avril 2020 de la cour administrative d'appel de Douai et la requête par laquelle elle demande qu'il soit sursis à l'exécution de cet arrêt présentent à juger les mêmes questions. Il y a lieu d'y statuer par une seule décision.<br/>
<br/>
              2. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              3. Pour demander l'annulation de l'arrêt qu'elle attaque, la région Hauts-de-France soutient que la cour administrative d'appel de Douai :<br/>
<br/>
              - a statué irrégulièrement dès lors que la minute ne comporte la signature ni du rapporteur, ni du greffier d'audience ;<br/>
              - s'est insuffisamment expliquée sur les raisons pour lesquelles elle a écarté ses moyens tirés de ce que le versement de la subvention d'équilibre ne pouvait présenter de caractère obligatoire ni couvrir l'intégralité des charges correspondant aux rentes viagères ;<br/>
              - a méconnu les dispositions de l'article L. 4135-25 du code général des collectivités territoriales en jugeant que la subvention pouvait couvrir l'intégralité des charges correspondant aux rentes viagères alors qu'il résulte de ces dispositions que l'éventuelle contribution de la région est partielle et plafonnée ;<br/>
              - a, en tout état de cause, méconnu les mêmes dispositions en jugeant qu'elle était tenue de verser une subvention d'équilibre, alors qu'elles n'ont institué qu'une faculté ;<br/>
              - a commis une erreur de droit et, à tout le moins, dénaturé les pièces du dossier en jugeant que M. B... avait acquis le droit à une rente viagère pour la période antérieure au 30 mars 1992, sans rechercher s'il avait personnellement cotisé pour pouvoir se prévaloir de ce droit.<br/>
<br/>
              4. Aucun de ces moyens n'est de nature à justifier l'admission du pourvoi.<br/>
<br/>
              5. Le pourvoi formé par la région Hauts-de-France contre l'arrêt de la cour administrative d'appel de Douai n'étant pas admis, les conclusions qu'elle présente aux fins de sursis à l'exécution de cet arrêt sont devenues sans objet. Il n'y a donc plus lieu d'y statuer.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
<br/>
Article 1er : Le pourvoi de la région Hauts-de-France n'est pas admis.<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions aux fins de sursis à exécution présentées par la région Hauts-de-France.<br/>
Article 3 : La présente décision sera notifiée à la région Hauts-de-France.<br/>
Copie en sera adressée à M. A... B.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
