<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042992809</ID>
<ANCIEN_ID>JG_L_2020_12_000000447545</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/99/28/CETATEXT000042992809.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 21/12/2020, 447545, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>447545</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:447545.20201221</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 13 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative : <br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de toutes les dispositions ou de certaines d'entre elles sur tout ou partie du territoire, notamment l'Ile-de-France, des décrets n° 2020-1257 du 14 octobre 2020, n° 2020-1310 du 29 octobre 2020 et n° 2020-1519 du 4 décembre 2020 ;<br/>
<br/>
              2°) d'ordonner la suspension de l'exécution de la déclaration d'état d'urgence sanitaire du 14 octobre 2020 ;<br/>
<br/>
              3°) d'ordonner la suspension de l'exécution des mesures de confinement prévu à l'article 4 du décret du 29 octobre 2020 ;<br/>
<br/>
              4°) d'ordonner la suspension de l'exécution du II de l'article 51 du décret du 29 octobre 2020 en ce qu'il permet la prescription du Rivotril alors que la fiche de ce médicament prévoit qu'il ne doit jamais être prescrit dans le cas d'insuffisance respiratoire sévère, symptôme qui est la cause la plus fréquente de mortalité de la covid-19 ;<br/>
<br/>
              5°) d'ordonner la suspension de l'exécution des parties du décret du 29 octobre 2020 en ce qu'il autorise le préfet ou le ministre de la santé de prendre des mesures attentatoires à la liberté individuelle alors que de telles mesures ne peuvent être prises à l'écart de l'autorité judiciaire ;<br/>
              6°) d'ordonner la suspension de l'exécution sur tout ou partie du territoire, notamment à Paris et dans le Cantal, du I de l'article 18 du décret du 29 octobre 2020 en ce qu'il ferme toutes les remontées mécaniques dont le funiculaire de Montmartre ;<br/>
<br/>
              7°) de mettre à la charge de l'Etat la somme de 300 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est satisfaite eu égard, d'une part, à la gravité et à l'immédiateté de l'atteinte portée aux libertés fondamentales et, d'autre part, à la durée excessive de l'application des mesures contestées ;<br/>
              - il est porté une atteinte grave et manifestement illégale à la liberté d'aller et venir, au droit de mener un vie familiale normale, à la liberté d'entreprendre, à la liberté individuelle et au droit à la vie ; <br/>
              - les mesures contestées sont disproportionnées dès lors que, d'une part, l'efficacité d'un confinement n'est démontrée par aucune étude scientifique, particulièrement pour les personnes âgées de moins de 65 ans et, d'autre part, la situation sanitaire s'améliore dans certaines régions ;<br/>
              - l'article 51-II du décret du 29 octobre 2020 est entaché d'illégalité dès lors que la prescription et l'administration du Rivotril a un effet contre-productif sur les patients atteints de la covid-19 ;<br/>
              - l'habilitation du préfet à prendre des mesures individuelles ou générales est entachée d'illégalité ;<br/>
              - la fermeture des remontées mécaniques crée une situation de discrimination eu égard aux personnes âgées et fragiles qui ont besoins de ces équipements pour se déplacer dans leur vie quotidienne.<br/>
<br/>
              Par un mémoire distinct, enregistré le 13 décembre 2020, présenté en application de l'article 23-5 de l'ordonnance du 7 novembre 1958, M. A... demande au juge des référés du Conseil d'Etat de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article L. 3131-3 du code de la santé publique et des articles L. 3131-12 aux L.3131-20 du même code. Il soutient que ces dispositions sont applicables au litige, qu'elles n'ont pas été déclarées conformes à la Constitution et qu'elles posent une question nouvelle et sérieuse. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son préambule ;<br/>
              - le code de la santé publique ;<br/>
              - le décret n° 2020-1257 du 14 octobre 2020 ;<br/>
              - le décret n° 2020-1310 du 29 octobre 2020 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures. ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Il résulte de la combinaison des dispositions de l'ordonnance n° 58-1067 du 7 novembre 1958 avec celles du livre V du code de justice administrative qu'une question prioritaire de constitutionnalité peut être soulevée devant le juge administratif des référés statuant sur le fondement de l'article L. 521-2 de ce code. Le juge des référés peut en toute hypothèse, y compris lorsqu'une question prioritaire de constitutionnalité est soulevée devant lui, rejeter une requête qui lui est soumise pour incompétence de la juridiction administrative, irrecevabilité ou défaut d'urgence.<br/>
<br/>
              3. La seule circonstance qu'une atteinte à une liberté fondamentale, portée par une mesure administrative, serait avérée n'est pas de nature à caractériser l'existence d'une situation d'urgence justifiant l'intervention du juge des référés dans le très bref délai prévu par les dispositions de l'article L. 521-2 du code de justice administrative. Il appartient au juge des référés d'apprécier, au vu des éléments que lui soumet le requérant comme de l'ensemble des circonstances de l'espèce, si la condition d'urgence particulière requise par l'article L. 521-2 est satisfaite, en prenant en compte la situation du requérant et les intérêts qu'il entend défendre mais aussi l'intérêt public qui s'attache à l'exécution des mesures prises par l'administration.<br/>
<br/>
              4. M. A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner la suspension de l'exécution de tout ou partie des mesures mises en place par le gouvernement pour faire face à l'épidémie de covid-19.<br/>
<br/>
              5. Pour justifier de l'urgence à ce qu'il soit fait droit à ses conclusions, M. A... se borne à soutenir que la durée des mesures prises pour lutter contre l'épidémie de covid-19 est excessive et que ces mesures portent une atteinte grave et immédiate aux libertés fondamentales.<br/>
<br/>
              6. Eu égard, d'une part, à la gravité de la situation sanitaire au vu de laquelle le Président de la République a déclaré l'état d'urgence sanitaire à compter du 17 octobre 2020 et qui a conduit le Premier ministre à adopter, sur le fondement de l'article L. 3131-15 du code de la santé publique, les décrets contestés et, d'autre part, à l'intérêt public qui s'attache à leur exécution, dans un contexte de persistance de l'épidémie, la condition d'urgence particulière requise par l'article L. 521-2 du code de justice administrative n'est pas remplie.<br/>
<br/>
              7. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur la transmission au Conseil constitutionnel de la question prioritaire de constitutionnalité soulevée, que la requête de M A... doit être rejetée, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, selon la procédure prévue par l'article L. 522-3 du même code.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
