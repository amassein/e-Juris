<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038633895</ID>
<ANCIEN_ID>JG_L_2019_06_000000421871</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/63/38/CETATEXT000038633895.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 17/06/2019, 421871, Publié au recueil Lebon</TITRE>
<DATE_DEC>2019-06-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421871</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:421871.20190617</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 1er juillet 2018 au secrétariat du contentieux du Conseil d'Etat, l'association Les amis de la Terre France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2017-1845 du 29 décembre 2017 relatif à l'expérimentation territoriale d'un droit de dérogation reconnu au préfet, ainsi que la décision implicite de rejet son recours gracieux contre ce décret ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son article 37-1 ;<br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 23 mai 2019, présentée par l'association Les amis de la Terre France.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1.	L'association Les amis de la Terre France demande l'annulation pour excès de pouvoir du décret du 29 décembre 2017 relatif à l'expérimentation territoriale d'un droit de dérogation reconnu au préfet, pris en application de l'article 37-1 de la Constitution, qui permet, pendant une durée de deux ans, aux préfets des régions Pays de la Loire et Bourgogne-Franche-Comté, aux préfets des départements de ces régions et des départements du Lot, du Bas-Rhin, du Haut-Rhin et de la Creuse ainsi qu'au préfet de Mayotte et au représentant de l'Etat à Saint-Barthélemy et à Saint-Martin et, par délégation, au préfet délégué dans les collectivités de Saint-Barthélemy et de Saint-Martin, de déroger à des normes arrêtées par l'administration de l'Etat dans les matières et conditions qu'il fixe.<br/>
<br/>
              2.	En premier lieu, aux termes de l'article 22 de la Constitution : " Les actes du Premier ministre sont contresignés, le cas échéant, par les ministres chargés de leur exécution ". S'agissant d'un acte réglementaire, les ministres chargés de son exécution sont ceux qui ont compétence pour signer ou contresigner les mesures réglementaires ou individuelles que comporte nécessairement l'exécution de cet acte. Si le décret attaqué ouvre une possibilité aux préfets mentionnés à son article 1er de déroger, à l'occasion de décisions non réglementaires relevant de leur compétence, à des " normes arrêtées par l'administration de l'Etat " notamment, aux termes du 3° de son article 2, en matière d'environnement, il ne nécessite pour autant aucune mesure d'exécution de la part du ministre chargé de l'environnement. Par suite, le moyen tiré du défaut de contreseing de ce ministre ne peut être accueilli.<br/>
<br/>
              3.	En deuxième lieu, aux termes de l'article 37-1 de la Constitution : " La loi et le règlement peuvent comporter, pour un objet et une durée limités, des dispositions à caractère expérimental ". Il résulte de ces dispositions que le pouvoir réglementaire peut, dans le respect des normes supérieures, autoriser des expérimentations permettant de déroger à des normes à caractère réglementaire sans méconnaître le principe d'égalité devant la loi dès lors que ces expérimentations présentent un objet et une durée limités et que leurs conditions de mise en oeuvre sont définies de façon suffisamment précise. A cet égard, s'il peut ne pas préciser d'emblée les normes réglementaire susceptibles de faire l'objet d'une dérogation, ni, le cas échéant, les règles ayant vocation à s'y substituer, il est nécessaire qu'il identifie précisément les matières dans le champ desquelles cette dérogation est possible ainsi que les objectifs auxquels celle-ci doit répondre et les conditions auxquelles elle est soumise.<br/>
<br/>
              4.	Aux termes de l'article 1er du décret attaqué, certains préfets et représentants de l'Etat précisément identifiés peuvent " à titre expérimental et pendant une durée de deux ans à compter de (sa) publication (...) déroger à des normes arrêtées par l'administration de l'Etat dans les conditions fixées par les articles 2 à 4 ". Son article 2 précise que " le préfet peut faire usage de cette faculté (...) pour prendre des décisions non réglementaires relevant de sa compétence " dans les matières suivantes : " 1° Subventions, concours financiers et dispositifs de soutien en faveur des acteurs économiques, des associations et des collectivités territoriales ; 2° Aménagement du territoire et politique de la ville ; 3° Environnement, agriculture et forêts ; 4° Construction, logement et urbanisme ; 5° Emploi et activité économique ; 6° Protection et mise en valeur du patrimoine culturel ; 7° Activités sportives, socio-éducatives et associatives. " Par ailleurs, son article 3 soumet cette faculté à quatre conditions cumulatives de sorte qu'une dérogation ne peut légalement intervenir que lorsqu'elle est " 1° (...) justifiée par un motif d'intérêt général et l'existence de circonstances locales ", qu'elle a " 2° (...) pour effet d'alléger les démarches administratives, de réduire les délais de procédure ou de favoriser l'accès aux aides publiques ", qu'elle est " 3° (...) compatible avec les engagements européens et internationaux de la France ", et qu'elle ne porte pas " 4° (...) atteinte aux intérêts de la défense ou à la sécurité des personnes et des biens, ni une atteinte disproportionnée aux objectifs poursuivis par les dispositions auxquelles il est dérogé ". Enfin, en vertu de son article 4, la décision de dérogation prend la forme d'un arrêté motivé et publié au recueil des actes administratifs.<br/>
<br/>
              5.	Le décret attaqué autorise les préfets concernés à déroger de façon ponctuelle, pour la prise d'une décision non  réglementaire relevant de leur compétence, aux normes réglementaires applicables dans certaines matières limitativement énumérées. Ces dérogations ne peuvent être accordées que dans le respect des normes supérieures applicables. Si, ainsi que le souligne la requérante, le décret attaqué ne désigne pas précisément les normes réglementaires auxquelles il permet de déroger, il limite ces dérogations, d'une part, aux règles qui régissent l'octroi des aides publiques afin d'en faciliter l'accès, d'autre part, aux seules règles de forme et de procédure applicables dans les matières énumérées afin d'alléger les démarches administratives et d'accélérer les procédures. Enfin, il ne permet une dérogation que sous conditions qu'elle réponde à un motif d'intérêt général, qu'elle soit justifiée par les circonstances locales, qu'elle ne porte pas atteinte aux intérêts de la défense ou à la sécurité des personnes et des biens et  qu'elle ne porte pas une atteinte disproportionnée aux objectifs poursuivis par les dispositions auxquelles il est dérogé.<br/>
<br/>
              6.	Il résulte de ce qui précède que le décret contesté, dont le champ et la durée d'application sont limités, n'autorise, dans le respect des normes supérieures, que des dérogations dont l'objet est limité et dont les conditions de mise en oeuvre sont définies de façon précise. Par suite, il ne méconnaît ni les dispositions de l'article 37-1 de la Constitution, ni la loi.<br/>
<br/>
              7.	En troisième lieu, le II de l'article L. 110-1 du code de l'environnement dans sa rédaction issue de la loi du 8 août 2016 pour la reconquête de la biodiversité, énonce au nombre des principes qui, " dans le cadre des lois qui en définissent la portée ", inspire les politiques de l'environnement et notamment la gestion des ressources : " 9° Le principe de non-régression, selon lequel la protection de l'environnement, assurée par les dispositions législatives et réglementaires relatives à l'environnement, ne peut faire l'objet que d'une amélioration constante, compte tenu des connaissances scientifiques et techniques du moment. (...) ". Si l'association requérante soutient que les dispositions du décret attaqué méconnaissent ce principe, il résulte des ses termes mêmes et notamment de son article 1er qu'il ne permet pas de déroger à des normes réglementaires ayant pour objet de garantir le respect de principes consacrés par la loi tel que le principe de non-régression. Par suite, le moyen tiré de la méconnaissance de ce principe doit être écarté.<br/>
<br/>
              8.	Il résulte de tout ce qui précède que l'association Les amis de la Terre France n'est pas fondée à demander l'annulation du décret attaqué. Ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent dès lors qu'être également rejetées.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
		Article 1er : La requête de l'association Les amis de la Terre France est rejetée.<br/>
Article 2 : La présente décision sera notifiée à l'association Les amis de la Terre France et au ministre de l'intérieur.<br/>
		Copie sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. - AUTORISATION PAR LE POUVOIR RÉGLEMENTAIRE D'EXPÉRIMENTATIONS (ART. 37-1 DE LA CONSTITUTION) - 1) POSSIBILITÉ POUR LE POUVOIR RÉGLEMENTAIRE DE NE PAS PRÉCISER D'EMBLÉE LES NORMES RÉGLEMENTAIRE SUSCEPTIBLES DE FAIRE L'OBJET D'UNE DÉROGATION, NI, LE CAS ÉCHÉANT, LES RÈGLES AYANT VOCATION À S'Y SUBSTITUER - EXISTENCE - CONDITIONS [RJ1] - IDENTIFICATION DES MATIÈRES DANS LE CHAMP DESQUELLES CETTE DÉROGATION EST POSSIBLE AINSI QUE DES OBJECTIFS AUXQUELS CELLE-CI DOIT RÉPONDRE ET DES CONDITIONS AUXQUELLES ELLE EST SOUMISE - 2) APPLICATION - LÉGALITÉ DU DÉCRET AUTORISANT CERTAINS PRÉFETS IDENTIFIÉS À DÉROGER DE FAÇON PONCTUELLE, POUR LA PRISE D'UNE DÉCISION NON RÉGLEMENTAIRE RELEVANT DE LEUR COMPÉTENCE, AUX NORMES RÉGLEMENTAIRES APPLICABLES DANS CERTAINES MATIÈRES LIMITATIVEMENT ÉNUMÉRÉES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-04-005 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. CONSTITUTION ET PRINCIPES DE VALEUR CONSTITUTIONNELLE. - AUTORISATION PAR LE POUVOIR RÉGLEMENTAIRE D'EXPÉRIMENTATIONS (ART. 37-1 DE LA CONSTITUTION) - 1) POSSIBILITÉ POUR LE POUVOIR RÉGLEMENTAIRE DE NE PAS PRÉCISER D'EMBLÉE LES NORMES RÉGLEMENTAIRE SUSCEPTIBLES DE FAIRE L'OBJET D'UNE DÉROGATION, NI, LE CAS ÉCHÉANT, LES RÈGLES AYANT VOCATION À S'Y SUBSTITUER - EXISTENCE - CONDITIONS [RJ1] - IDENTIFICATION DES MATIÈRES DANS LE CHAMP DESQUELLES CETTE DÉROGATION EST POSSIBLE AINSI QUE DES OBJECTIFS AUXQUELS CELLE-CI DOIT RÉPONDRE ET DES CONDITIONS AUXQUELLES ELLE EST SOUMISE - 2) APPLICATION - LÉGALITÉ DU DÉCRET AUTORISANT CERTAINS PRÉFETS IDENTIFIÉS À DÉROGER DE FAÇON PONCTUELLE, POUR LA PRISE D'UNE DÉCISION NON RÉGLEMENTAIRE RELEVANT DE LEUR COMPÉTENCE, AUX NORMES RÉGLEMENTAIRES APPLICABLES DANS CERTAINES MATIÈRES LIMITATIVEMENT ÉNUMÉRÉES.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">44 NATURE ET ENVIRONNEMENT. - PRINCIPE DE NON-RÉGRESSION (II DE L'ARTICLE L. 110-1 DU CODE DE L'ENVIRONNEMENT) - DÉCRET AUTORISANT CERTAINS PRÉFETS IDENTIFIÉS À DÉROGER DE FAÇON PONCTUELLE, POUR LA PRISE D'UNE DÉCISION NON RÉGLEMENTAIRE RELEVANT DE LEUR COMPÉTENCE, AUX NORMES RÉGLEMENTAIRES APPLICABLES DANS CERTAINES MATIÈRES LIMITATIVEMENT ÉNUMÉRÉES - MÉCONNAISSANCE - ABSENCE, LE DÉCRET NE PERMETTANT PAS DE DÉROGER À DES NORMES RÉGLEMENTAIRES AYANT POUR OBJET DE GARANTIR LE RESPECT DE PRINCIPES CONSACRÉS PAR LA LOI TEL QUE LE PRINCIPE DE NON-RÉGRESSION.
</SCT>
<ANA ID="9A"> 01-02 1) Il résulte de l'article 37-1 de la Constitution que le pouvoir réglementaire peut, dans le respect des normes supérieures, autoriser des expérimentations permettant de déroger à des normes à caractère réglementaire sans méconnaître le principe d'égalité devant la loi dès lors que ces expérimentations présentent un objet et une durée limités et que leurs conditions de mise en oeuvre sont définies de façon suffisamment précise. A cet égard, s'il peut ne pas préciser d'emblée les normes réglementaires susceptibles de faire l'objet d'une dérogation, ni, le cas échéant, les règles ayant vocation à s'y substituer, il est nécessaire qu'il identifie précisément les matières dans le champ desquelles cette dérogation est possible ainsi que les objectifs auxquels celle-ci doit répondre et les conditions auxquelles elle est soumise.,,,2) Décret autorisant certains préfets identifiés à déroger de façon ponctuelle, pour la prise d'une décision non réglementaire relevant de leur compétence, aux normes réglementaires applicables dans certaines matières limitativement énumérées. Ces dérogations ne peuvent être accordées que dans le respect des normes supérieures applicables, constitutionnelles, conventionnelles ou législatives. Si le décret attaqué ne désigne pas précisément les normes réglementaires auxquelles il permet de déroger, il limite ces dérogations, d'une part, aux règles qui régissent l'octroi des aides publiques afin d'en faciliter l'accès, d'autre part, aux seules règles de forme et de procédure applicables dans les matières énumérées afin d'alléger les démarches administratives et d'accélérer les procédures. Enfin, il ne permet une dérogation que sous conditions qu'elle réponde à un motif d'intérêt général, qu'elle soit justifiée par les circonstances locales, qu'elle ne porte pas atteinte aux intérêts de la défense ou à la sécurité des personnes et des biens et qu'elle ne porte pas une atteinte disproportionnée aux objectifs poursuivis par les dispositions auxquelles il est dérogé.,,Le décret contesté, dont le champ et la durée d'application sont limités, n'autorise, dans le respect des normes supérieures, que des dérogations dont l'objet est limité et dont les conditions de mise en oeuvre sont définies de façon précise. Par suite, il ne méconnaît ni les dispositions de l'article 37-1 de la Constitution, ni la loi.</ANA>
<ANA ID="9B"> 01-04-005 1) Il résulte de l'article 37-1 de la Constitution que le pouvoir réglementaire peut, dans le respect des normes supérieures, autoriser des expérimentations permettant de déroger à des normes à caractère réglementaire sans méconnaître le principe d'égalité devant la loi dès lors que ces expérimentations présentent un objet et une durée limités et que leurs conditions de mise en oeuvre sont définies de façon suffisamment précise. A cet égard, s'il peut ne pas préciser d'emblée les normes réglementaires susceptibles de faire l'objet d'une dérogation, ni, le cas échéant, les règles ayant vocation à s'y substituer, il est nécessaire qu'il identifie précisément les matières dans le champ desquelles cette dérogation est possible ainsi que les objectifs auxquels celle-ci doit répondre et les conditions auxquelles elle est soumise.,,,2) Décret autorisant certains préfets identifiés à déroger de façon ponctuelle, pour la prise d'une décision non réglementaire relevant de leur compétence, aux normes réglementaires applicables dans certaines matières limitativement énumérées. Ces dérogations ne peuvent être accordées que dans le respect des normes supérieures applicables, constitutionnelles, conventionnelles ou législatives. Si le décret attaqué ne désigne pas précisément les normes réglementaires auxquelles il permet de déroger, il limite ces dérogations, d'une part, aux règles qui régissent l'octroi des aides publiques afin d'en faciliter l'accès, d'autre part, aux seules règles de forme et de procédure applicables dans les matières énumérées afin d'alléger les démarches administratives et d'accélérer les procédures. Enfin, il ne permet une dérogation que sous conditions qu'elle réponde à un motif d'intérêt général, qu'elle soit justifiée par les circonstances locales, qu'elle ne porte pas atteinte aux intérêts de la défense ou à la sécurité des personnes et des biens et qu'elle ne porte pas une atteinte disproportionnée aux objectifs poursuivis par les dispositions auxquelles il est dérogé.,,Le décret contesté, dont le champ et la durée d'application sont limités, n'autorise, dans le respect des normes supérieures, que des dérogations dont l'objet est limité et dont les conditions de mise en oeuvre sont définies de façon précise. Par suite, il ne méconnaît ni les dispositions de l'article 37-1 de la Constitution, ni la loi.</ANA>
<ANA ID="9C"> 44 Décret autorisant certains préfets identifiés à déroger de façon ponctuelle, pour la prise d'une décision non réglementaire relevant de leur compétence, aux normes réglementaires applicables dans certaines matières limitativement énumérées. Si l'association requérante soutient que les dispositions du décret attaqué méconnaissent le principe de non-régression posé par le II de l'article L. 110-1 du code de l'environnement, il résulte des termes mêmes du décret qu'il ne permet pas de déroger à des normes réglementaires ayant pour objet de garantir le respect de principes consacrés par la loi tel que le principe de non-régression. Par suite, le moyen tiré de la méconnaissance de ce principe doit être écarté.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. Cons. const., 12 août 2004, n° 2004-503 DC, loi relative aux libertés et responsabilités locales, cons. 9.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
