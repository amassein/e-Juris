<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039161395</ID>
<ANCIEN_ID>JG_L_2019_09_000000427175</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/16/13/CETATEXT000039161395.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 30/09/2019, 427175</TITRE>
<DATE_DEC>2019-09-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427175</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2019:427175.20190930</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au tribunal administratif de Toulon d'annuler la décision du 27 juillet 2018 par laquelle l'Agence de services et de paiement lui a refusé le bénéfice du " chèque énergie ". Par une ordonnance n° 1803772 du 8 janvier 2019, le vice-président du tribunal administratif de Toulon a, sur le fondement du premier alinéa de l'article R. 351-3 du code de justice administrative, transmis la demande au tribunal administratif de Lille.<br/>
<br/>
              Par une ordonnance n° 1812019 du 16 janvier 2019, enregistrée le 17 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, le vice-président du tribunal administratif de Lille a, sur le fondement du second alinéa de l'article R. 351-3 du code de justice administrative, transmis le dossier au président de la section du contentieux du Conseil d'Etat.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'énergie ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 124-1 du code de l'énergie prévoit que le " chèque énergie " est un titre spécial de paiement qui permet aux ménages dont le revenu fiscal de référence est inférieur à un plafond fixé par décret d'acquitter tout ou partie du montant des dépenses d'énergie relatives à leur logement ou de certaines dépenses qu'ils assument en vue d'améliorer la qualité environnementale ou de maîtriser la consommation d'énergie de ce logement.<br/>
<br/>
              2. En l'espèce, Mme A... demande l'annulation de la décision par laquelle l'Agence de services et de paiement lui a refusé le bénéfice de ce " chèque énergie " pour son logement situé à Rians (Var).<br/>
<br/>
              3. Un tel recours, sur lequel il appartient au juge administratif de statuer en qualité de juge de plein contentieux, est au nombre des requêtes relatives aux prestations, allocations ou droits attribués au titre de l'aide ou de l'action sociale, du logement ou en faveur des travailleurs privés d'emploi, devant être jugées selon les règles particulières de présentation, instruction et jugement fixées aux articles R. 772-5 et suivants du code de justice administrative.<br/>
<br/>
              4. Aux termes de l'article R. 312-7 du code de justice administrative : " Les litiges relatifs aux déclarations d'utilité publique, au domaine public, aux affectations d'immeubles, au remembrement, à l'urbanisme et à l'habitation, au permis de construire, d'aménager ou de démolir, au classement des monuments et des sites et, de manière générale, aux décisions concernant des immeubles relèvent de la compétence du tribunal administratif dans le ressort duquel se trouvent les immeubles faisant l'objet du litige (...) ".<br/>
<br/>
              5. La demande de Mme A..., qui est dirigée contre un refus d'aide afférente à son logement, doit être regardée comme soulevant un litige relatif à une décision concernant un immeuble, au sens des dispositions de l'article R. 312-7 du code de justice administrative. Elle relève, par suite, de la compétence en premier ressort du tribunal administratif de Toulon, dans le ressort duquel est situé le logement en cause.<br/>
<br/>
              6. Il résulte de ce qui précède qu'il y a lieu d'attribuer le jugement de la demande de Mme A... au tribunal administratif de Toulon.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement de la demande de Mme A... est attribué au tribunal administratif de Toulon.<br/>
Article 2 : La présente décision sera notifiée à Mme B... A..., au président du tribunal administratif de Lille et au président du tribunal administratif de Toulon.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-04 AIDE SOCIALE. CONTENTIEUX DE L'AIDE SOCIALE ET DE LA TARIFICATION. - INCLUSION - RECOURS DIRIGÉ CONTRE LE REFUS D'ACCORDER UN CHÈQUE ÉNERGIE (ART. L. 124-1 DU CODE DE L'ÉNERGIE) - CONSÉQUENCES - RECOURS DE PLEIN CONTENTIEUX [RJ1] - APPLICATION DES RÈGLES DE PROCÉDURES SPÉCIALES FIXÉES AUX ARTICLES R. 772-5 ET SUIVANTS DU CJA.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-05-01-02 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. COMPÉTENCE TERRITORIALE. - RECOURS DIRIGÉ CONTRE LE REFUS D'ACCORDER UN CHÈQUE ÉNERGIE (ART. L. 124-1 DU CODE DE L'ÉNERGIE) - LITIGE RELATIF À UN IMMEUBLE AU SENS DE L'ARTICLE R. 312-7 DU CJA - CONSÉQUENCE - COMPÉTENCE DU TRIBUNAL ADMINISTRATIF DANS LE RESSORT DUQUEL SE SITUE LE LOGEMENT EN CAUSE [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-02-02-01 PROCÉDURE. DIVERSES SORTES DE RECOURS. RECOURS DE PLEIN CONTENTIEUX. RECOURS AYANT CE CARACTÈRE. - RECOURS DIRIGÉ CONTRE LE REFUS D'ACCORDER UN CHÈQUE ÉNERGIE (ART. L. 124-1 DU CODE DE L'ÉNERGIE) [RJ1].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-06-01 PROCÉDURE. JUGEMENTS. RÈGLES GÉNÉRALES DE PROCÉDURE. - REQUÊTES RELATIVES À DES DROITS ATTRIBUÉS AU TITRE DE L'AIDE OU DE L'ACTION SOCIALE ET DU LOGEMENT (ART. R. 772-5 DU CJA) - INCLUSION - RECOURS DIRIGÉ CONTRE LE REFUS D'ACCORDER UN CHÈQUE ÉNERGIE (ART. L. 124-1 DU CODE DE L'ÉNERGIE).
</SCT>
<ANA ID="9A"> 04-04 Un recours contre le refus d'accorder un chèque énergie, sur lequel il appartient au juge administratif de statuer en qualité de juge de plein contentieux, est au nombre des requêtes relatives aux prestations, allocations ou droits attribués au titre de l'aide ou de l'action sociale, du logement ou en faveur des travailleurs privés d'emploi, devant être jugées selon les règles particulières de présentation, instruction et jugement fixées aux articles R. 772-5 et suivants du code de justice administrative (CJA).</ANA>
<ANA ID="9B"> 17-05-01-02 Un recours contre le refus d'accorder un chèque énergie, qui est une aide afférente au logement de l'intéressé, doit être regardé comme soulevant un litige relatif à une décision concernant un immeuble, au sens des dispositions de l'article R. 312-7 du code de justice administrative (CJA).... ,,Il relève, par suite, de la compétence en premier ressort du tribunal administratif dans le ressort duquel est situé le logement en cause.</ANA>
<ANA ID="9C"> 54-02-02-01 Un recours contre le refus d'accorder un chèque énergie, sur lequel il appartient au juge administratif de statuer en qualité de juge de plein contentieux, est au nombre des requêtes relatives aux prestations, allocations ou droits attribués au titre de l'aide ou de l'action sociale, du logement ou en faveur des travailleurs privés d'emploi, devant être jugées selon les règles particulières de présentation, instruction et jugement fixées aux articles R. 772-5 et suivants du code de justice administrative (CJA).</ANA>
<ANA ID="9D"> 54-06-01 Un recours contre le refus d'accorder un chèque énergie, sur lequel il appartient au juge administratif de statuer en qualité de juge de plein contentieux, est au nombre des requêtes relatives aux prestations, allocations ou droits attribués au titre de l'aide ou de l'action sociale, du logement ou en faveur des travailleurs privés d'emploi, devant être jugées selon les règles particulières de présentation, instruction et jugement fixées aux articles R. 772-5 et suivants du code de justice administrative (CJA).</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur la nature de plein contentieux du recours dirigé contre une décision de l'administration déterminant les droits d'une personne en matière d'aide ou d'action sociale, de logement ou au titre des dispositions en faveur des travailleurs privés d'emploi, CE, Section, 3 juin 2019,,, n° 423001, à publier au Recueil.,,[RJ2] Rappr., s'agissant des aides personnalisées au logement, CE, 10 octobre 1986, Ministre de l'urbanisme et du logement c/,, n° 55433, T. pp. 603-647-676.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
