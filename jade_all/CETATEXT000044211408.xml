<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044211408</ID>
<ANCIEN_ID>JG_L_2021_10_000000394925</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/21/14/CETATEXT000044211408.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 14/10/2021, 394925, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394925</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Réda Wadjinny-Green</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Arnaud Skzryerbak</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:394925.20211014</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Par une décision du 26 juillet 2018, le Conseil d'Etat, statuant au contentieux sur la requête de l'association La Quadrature du Net et autres tendant à l'annulation pour excès de pouvoir du décret n° 2015-1211 du 1er octobre 2015 relatif au contentieux de la mise en œuvre des techniques de renseignement soumises à autorisation et des fichiers intéressant la sûreté de l'Etat, a sursis à statuer jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur les questions suivantes : <br/>
<br/>
              1°) L'obligation de conservation généralisée et indifférenciée, imposée aux fournisseurs sur le fondement des dispositions permissives de l'article 15, paragraphe 1, de la directive du 12 juillet 2002, ne doit-elle pas être regardée, dans un contexte marqué par des menaces graves et persistantes pour la sécurité nationale, et en particulier par le risque terroriste, comme une ingérence justifiée par le droit à la sûreté garanti à l'article 6 de la Charte des droits fondamentaux de l'Union européenne et les exigences de la sécurité nationale, dont la responsabilité incombe aux seuls Etats-membres en vertu de l'article 4 du traité sur l'Union européenne '<br/>
<br/>
              2°) La directive du 12 juillet 2002 lue à la lumière de la Charte des droits fondamentaux de l'Union européenne doit-elle être interprétée en ce sens qu'elle autorise des mesures législatives, telles que les mesures de recueil en temps réel des données relatives au trafic et à la localisation d'individus déterminés, qui, tout en affectant les droits et obligations des fournisseurs d'un service de communications électroniques, ne leur imposent pas pour autant une obligation spécifique de conservation de leurs données ' <br/>
<br/>
              3°) La directive du 12 juillet 2002, lue à la lumière de la Charte des droits fondamentaux de l'Union européenne, doit-elle être interprétée en ce sens qu'elle subordonne dans tous les cas la régularité des procédures de recueil des données de connexion à une exigence d'information des personnes concernées lorsqu'une telle information n'est plus susceptible de compromettre les enquêtes menées par les autorités compétentes ou de telles procédures peuvent-elles être regardées comme régulières compte tenu de l'ensemble des autres garanties procédurales existantes, dès lors que ces dernières assurent l'effectivité du droit au recours ' <br/>
<br/>
              Par un arrêt C-511/18, C-512/18, C520/18 du 6 octobre 2020, la Cour de justice de l'Union européenne s'est prononcée sur ces questions. <br/>
<br/>
<br/>
            Vu les autres pièces du dossier, y compris celles visées par la décision du Conseil d'Etat du 26 juillet 2018 ; <br/>
<br/>
            Vu : <br/>
            - le traité sur l'Union européenne ; <br/>
            - le traité sur le fonctionnement de l'Union européenne ;<br/>
            - la Charte des droits fondamentaux de l'Union européenne ; <br/>
            - la directive 2002/58/CE du Parlement européen et du Conseil du 12 juillet 2002 ;<br/>
            - le code de la sécurité intérieure, notamment son livre VIII ;<br/>
            - l'arrêt de la Cour de justice de l'Union européenne du 4 juin 2013, ZZ contre Secretary of State for the Home Department (C-300-11) ;<br/>
            - l'arrêt de la Cour de justice de l'Union européenne du 6 octobre 2020 La Quadrature du net et autres (C-511/18, C-512/18, C520/18) ;<br/>
            - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Réda Wadjinny-Green, auditeur,  <br/>
<br/>
              - les conclusions de M. Arnaud Skzryerbak, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. L'association La Quadrature du Net et autres demandent l'annulation pour excès de pouvoir du décret du 1er octobre 2015 relatif au contentieux de la mise en œuvre des techniques de renseignement soumises à autorisation et des fichiers intéressant la sûreté de l'Etat. Par sa décision nos 394922, 394925, 397844, 397851 du 26 juillet 2018, le Conseil d'Etat, statuant au contentieux, a écarté les moyens invoqués devant lui autres que ceux tirés de la méconnaissance du droit de l'Union européenne et a sursis à statuer jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur les questions préjudicielles dont il l'a saisie. Par un arrêt en date du 6 octobre 2020, rendu dans les affaires jointes C-511/18, C-512/18 et C-520/18, la Cour de justice s'est prononcée sur ces questions.<br/>
<br/>
              En ce qui concerne l'opérance de certains moyens :<br/>
<br/>
              2. En premier lieu, les associations requérantes soutiennent, par la voie de l'exception, que les articles L. 811-4, qui prévoit que certains services, autres que les services spécialisés de renseignement, puissent être autorisés à recourir à certaines techniques de renseignement, L. 851-1 à L. 851-4 du code de la sécurité intérieure, qui définissent certaines de ces techniques, et L. 854-1, qui encadre la surveillance des communications qui sont émises ou reçues à l'étranger, seraient incompatibles avec le droit de l'Union européenne. <br/>
<br/>
              3. La contrariété d'une disposition législative aux stipulations d'un traité international ou au droit de l'Union européenne ne peut être utilement invoquée à l'appui de conclusions dirigées contre un acte réglementaire que si ce dernier a été pris pour son application ou si elle en constitue la base légale.<br/>
<br/>
              4. Le décret du 1er octobre 2015 attaqué insère au code de justice administrative un chapitre III bis relatif au contentieux de la mise en œuvre des techniques de renseignement soumises à autorisation et des fichiers intéressant la sûreté de l'Etat qui précise la composition de la formation spécialisée du Conseil d'Etat mentionnée à l'article L. 773-2 du code de justice administrative ainsi que les règles procédurales suivies devant celle-ci lorsqu'elle est saisie sur le fondement des articles L. 841-1, L. 841-2 et du III de l'article L. 853-3 du code de la sécurité intérieure. Il suit de là que les associations requérantes ne sauraient utilement contester, par la voie de l'exception, l'incompatibilité avec le droit de l'Union européenne des articles L. 811-4, L. 851-1 à L. 851-4 et L. 854-1 du code de la sécurité intérieure à l'appui de leurs conclusions dirigées contre ce décret, qui n'a pas été pris pour l'application de ces articles et dont ceux-ci ne constituent pas la base légale.<br/>
<br/>
              5. En second lieu, La Quadrature du Net soutient qu'en ne prévoyant pas de contrôle de l'exploitation des données collectées par les services de renseignement sur le fondement du livre VIII du code de la sécurité intérieure, d'une part, ni de contrôle de la conservation et de l'exploitation des données qui leur sont transmises par des services étrangers, d'autre part, les dispositions contestées méconnaissent le droit de l'Union européenne. Toutefois, il résulte clairement de l'article 1er, paragraphe 3 de la directive du 12 juillet 2002, qui prévoit qu'elle " ne s'applique pas aux activités qui ne relèvent pas du traité instituant la Communauté européenne (...) et, en tout état de cause, aux activités concernant la sécurité publique, la défense, la sûreté de l'Etat (y compris la prospérité économique de l'Etat lorsqu'il s'agit d'activités liées à la sûreté de l'Etat) ou aux activités de l'Etat dans des domaines relevant du droit pénal " que ni les règles relatives à l'exploitation par les services de renseignement des données collectées auprès des opérateurs ni celles relatives à la collecte et à l'exploitation par eux de données transmises par des services de renseignement étrangers ne régissent l'activité des fournisseurs de services de communications électroniques en leur imposant des obligations spécifiques. Il s'ensuit que ces règles ne sauraient être regardées comme mettant en œuvre le droit de l'Union européenne et que le moyen soulevé ne peut être utilement invoqué à l'encontre des dispositions attaquées.<br/>
<br/>
              En ce qui concerne le droit au recours effectif :<br/>
<br/>
              6. Les associations requérantes soutiennent ensuite, par la voie de l'exception, que les dispositions législatives du code de justice administrative qui organisent le contentieux des techniques de renseignement devant la formation spécialisée du Conseil d'Etat méconnaissent le droit au recours effectif garanti par l'article 47 de la Charte des droits fondamentaux. Ce moyen n'est opérant qu'en tant que la formation spécialisée est chargée de contrôler la mise en œuvre de techniques de renseignement faisant peser des obligations sur les opérateurs de communications électroniques.<br/>
<br/>
              7. En interprétant les exigences découlant de l'article 47 de la Charte des droits fondamentaux, la Cour de justice de l'Union européenne a estimé, dans les motifs de son arrêt du 4 juin 2013 ZZ contre Secretary of State for the Home Department (C-300-11) que : " Ce serait violer le droit fondamental à un recours juridictionnel effectif que de fonder une décision juridictionnelle sur des faits et des documents dont les parties elles-mêmes, ou l'une d'entre elles, n'ont pas pu prendre connaissance et sur lesquels elles n'ont donc pas été en mesure de prendre position ". Si la Cour admet, par le même arrêt, que, dans des cas exceptionnels, une autorité nationale s'oppose à la communication à l'intéressé des motifs précis et complets qui constituent le fondement d'une décision en invoquant des raisons relevant de la sûreté de l'Etat, elle relève que : "  le juge compétent de l'État membre concerné doit avoir à sa disposition et mettre en œuvre des techniques et des règles de droit de procédure permettant de concilier, d'une part, les considérations légitimes de la sûreté de l'État quant à la nature et aux sources des renseignements ayant été pris en considération pour l'adoption d'une telle décision et, d'autre part, la nécessité de garantir à suffisance au justiciable le respect de ses droits procéduraux, tels que le droit d'être entendu ainsi que le principe du contradictoire ". Elle en déduit l'obligation pour les Etats membres de prévoir un contrôle juridictionnel effectif " de l'existence et du bien-fondé des raisons invoquées par l'autorité nationale au regard de la sûreté de l'Etat ". La Cour de justice ajoute : " Quant aux exigences auxquelles doit répondre le contrôle juridictionnel de l'existence et du bien-fondé des raisons invoquées par l'autorité nationale compétente au regard de la sûreté de l'État membre concerné, il importe qu'un juge soit chargé de vérifier si ces raisons s'opposent à la communication des motifs précis et complets sur lesquels est fondée la décision en cause ainsi que des éléments de preuve y afférents. / Ainsi, il incombe à l'autorité nationale compétente d'apporter, conformément aux règles de procédure nationales, la preuve que la sûreté de l'État serait effectivement compromise par une communication à l'intéressé des motifs précis et complets qui constituent le fondement d'une décision (...) ". <br/>
<br/>
              8. Les dispositions des articles L. 841-1 et L. 841-2 du code de la sécurité intérieure prévoient les conditions dans lesquelles le Conseil d'Etat est compétent pour connaître des requêtes concernant la mise en œuvre des techniques de renseignement soumises à autorisation. Il peut être saisi soit par toute personne souhaitant vérifier qu'aucune technique de renseignement n'est irrégulièrement mise en œuvre et justifiant d'avoir au préalable saisi la Commission nationale de contrôle des techniques de renseignement sur le fondement de l'article L. 833-4 du même code, soit par le président de cette commission, ou trois de ses membres, lorsque le Premier ministre ne donne pas suite aux avis ou aux recommandations de la commission ou que les suites qui y sont données sont estimées insuffisantes. S'agissant des mesures de surveillance des communications électroniques internationales encadrées par le chapitre IV du titre V du livre VIII du code de la sécurité intérieure, si la personne qui pense faire l'objet d'une telle mesure de surveillance ne peut directement saisir un juge pour en contester la régularité, elle peut en revanche, sur le fondement des dispositions de l'article L. 854-9 de ce code, former une réclamation à cette fin auprès de la Commission nationale de contrôle des techniques de renseignement. Ce même article prévoit que lorsque la commission identifie un manquement, de sa propre initiative ou à la suite d'une telle réclamation, elle adresse au Premier ministre une recommandation tendant à ce qu'il y soit mis fin et que les renseignements collectés soient, le cas échéant, détruits. Elle peut également saisir le Conseil d'Etat.<br/>
<br/>
              9. L'article L. 773-3 du code de justice administrative dispose que, devant la formation spécialisée chargée d'instruire les affaires relatives à la mise en œuvre des techniques de renseignement soumises à autorisation et des fichiers intéressant la sûreté de l'Etat : " Les exigences de la contradiction mentionnées à l'article L. 5 du présent code sont adaptées à celles du secret de la défense nationale ". Saisie de conclusions tendant à ce qu'elle s'assure qu'aucune technique de renseignement n'est irrégulièrement mise en œuvre à l'égard du requérant ou de la personne concernée, il appartient à la formation spécialisée, de vérifier, au vu des éléments qui lui ont été communiqués hors la procédure contradictoire, si le requérant fait ou non l'objet d'une telle technique. Dans l'affirmative, il lui appartient d'apprécier si cette technique est mise en œuvre dans le respect du livre VIII du code de la sécurité intérieure. Lorsqu'il apparaît soit qu'aucune technique de renseignement n'est mise en œuvre à l'égard du requérant, soit que cette mise en œuvre n'est entachée d'aucune illégalité, la formation de jugement informe le requérant de l'accomplissement de ces vérifications et qu'aucune illégalité n'a été commise, sans autre précision. Dans le cas où une technique de renseignement est mise en œuvre dans des conditions qui apparaissent entachées d'illégalité, elle en informe le requérant, sans faire état d'aucun élément protégé par le secret de la défense nationale. En pareil cas, par une décision distincte dont seule l'administration compétente et la Commission nationale de contrôle des techniques de renseignement sont destinataires, la formation spécialisée annule le cas échéant l'autorisation et ordonne la destruction des renseignements irrégulièrement collectés. <br/>
<br/>
              10. La dérogation apportée, par les dispositions contestées du code de justice administrative, au caractère contradictoire de la procédure juridictionnelle a pour seul objet de porter à la connaissance des juges des éléments couverts par le secret de la défense nationale, au rang desquels figure notamment la décision même de mettre en œuvre ou non un traitement relevant du livre VIII du code de la sécurité intérieure, qui serait inévitablement révélée par une communication des motifs la fondant. En particulier, s'il résulte de ce qui a été dit au point 7 que, lorsqu'une décision administrative fait l'objet d'un recours juridictionnel, le droit de l'Union européenne ne s'oppose pas à ce qu'il soit fait obstacle à la communication au requérant des éléments la motivant lorsqu'une telle communication pourrait compromettre la sûreté de l'Etat, mais que le juge doit être mis en mesure de vérifier si la non-communication de tels motifs est justifiée au regard des circonstances de l'espèce et des raisons invoquées par l'autorité administrative, une telle exigence ne peut trouver à s'appliquer en l'espèce, dès lors que, comme il vient d'être dit, l'objet des recours porte sur l'existence même d'une décision, puis, le cas échéant, sur sa légalité. Les éléments, qui ne peuvent, dès lors, être communiqués au requérant, permettent à la formation spécialisée, qui entend les parties, de statuer en toute connaissance de cause. Les pouvoirs dont elle est investie en contrepartie, pour instruire les requêtes, relever d'office toutes les illégalités qu'elle constate et enjoindre à l'administration de prendre toutes mesures utiles afin de remédier aux illégalités constatées garantissent l'effectivité du contrôle juridictionnel qu'elle exerce. <br/>
<br/>
              11. Il s'ensuit que ni les conditions dans lesquelles la formation spécialisée peut être saisie, ni celles dans lesquelles elle remplit son office juridictionnel ne méconnaissent, contrairement à ce qui est soutenu, le droit au recours effectif des personnes qui la saisissent, garanti notamment par l'article 47 de la Charte des droits fondamentaux.<br/>
<br/>
              12. Il résulte de tout ce qui précède que, sans qu'il soit besoin de se prononcer sur les moyens soulevés en défense par le Premier ministre, la requête des associations La Quadrature du Net et autres doit être rejetée, y compris les conclusions présentées au titre de l'article L.761-1 du code de justice administrative.  <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'association La Quadrature du Net et autres est rejetée. <br/>
<br/>
Article 2 : La présente décision sera notifiée aux associations La Quadrature du Net, French Data Network, Igwan.net, au Premier ministre, à la ministre des armées, au ministre de l'intérieur et au garde des sceaux, ministre de la justice.<br/>
<br/>
Délibéré à l'issue de la séance du 22 septembre 2021 où siégeaient : M. Christophe Chantepy, président de la section du contentieux, présidant ; M. D... B..., M. Frédéric Aladjidi, présidents de chambre, M. G... A..., Mme F... C..., M. François Weil, conseillers d'Etat, M. Réda Wadjinny-Green auditeur-rapporteur ; <br/>
<br/>
                          Rendu le 14 octobre 2021.<br/>
<br/>
<br/>
<br/>
                                                  Le président : <br/>
                                                  Signé : M. Christophe Chantepy<br/>
<br/>
            Le rapporteur : <br/>
            Signé : M. Réda Wadjinny-Green<br/>
<br/>
                                                  La secrétaire :<br/>
                                                  Signé : Mme E... H...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
