<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030580656</ID>
<ANCIEN_ID>JG_L_2015_05_000000383664</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/58/06/CETATEXT000030580656.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème SSR, 11/05/2015, 383664, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-05-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383664</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Paul Bernard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2015:383664.20150511</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par un décret du 28 mai 2014, M. A...B...a été déchu de la nationalité française.<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés le 12 août 2014 et le 20 février 2015 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir ce décret ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat à la somme de 2 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son protocole additionnel n° 4 ;<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la Charte des droits fondamentaux de l'Union européenne ; <br/>
              - le code civil ;<br/>
              - le code pénal ;<br/>
              - la décision n° 383664 du 31 octobre 2014 par laquelle le Conseil d'Etat a renvoyé au Conseil constitutionnel la question de la conformité à la Constitution du 1° de l'article 25 et de l'article 25-1 du code civil, soulevée par M.B..., et a jugé qu'il n'y a avait pas lieu de renvoyer la question de la conformité à la Constitution de l'article 421-2-1 du code pénal ;<br/>
              - la décision n° 2014-439 QPC du Conseil constitutionnel du 23 janvier 2015 ;<br/>
              - l'arrêt de la Cour de justice de l'Union européenne C-135/08 du 2 mars 2010 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Paul Bernard, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que M. B...a été déchu de la nationalité française par un décret du 28 mai 2014 pris sur le fondement des articles 25 et 25-1 du code civil, au motif qu'il a été condamné par un jugement devenu définitif du tribunal de grande instance de Paris pour avoir participé à une association de malfaiteurs en vue de la préparation d'un acte de terrorisme, faits prévus par l'article 421-2-1 du code pénal ; <br/>
<br/>
              2.	Considérant que M. B...ne peut, à l'appui du recours pour excès de pouvoir qu'il a formé devant le Conseil d'Etat contre ce décret, utilement se prévaloir ni de la circonstance que le recours qu'il a formé contre le refus de lui accorder le bénéfice de l'aide juridictionnelle a été rejeté, ni du sens des décisions rendues par le Conseil d'Etat dans d'autres litiges, pour mettre en cause le caractère équitable de la procédure devant le Conseil d'Etat ;<br/>
<br/>
              3.	Considérant, d'une part, qu'aux termes de l'article 25 du code civil : " L'individu qui a acquis la qualité de Français peut, par décret pris après avis conforme du Conseil d'Etat, être déchu de la nationalité française, sauf si la déchéance a pour résultat de le rendre apatride : / 1° S'il est condamné pour un acte qualifié de crime ou délit constituant une atteinte aux intérêts fondamentaux de la Nation ou pour un crime ou un délit constituant un acte de terrorisme ; / 2° S'il est condamné pour un acte qualifié de crime ou délit prévu et réprimé par le chapitre II du titre III du livre IV du code pénal ; / 3° S'il est condamné pour s'être soustrait aux obligations résultant pour lui du code du service national ; / 4° S'il s'est livré au profit d'un Etat étranger à des actes incompatibles avec la qualité de Français et préjudiciables aux intérêts de la France " ; que, selon l'article 25-1 du code civil, dans sa rédaction résultant de la loi du 23 janvier 2006 relative à la lutte contre le terrorisme et portant dispositions diverses relatives à la sécurité et aux contrôles frontaliers, applicable à la date du décret attaqué : " La déchéance n'est encourue que si les faits reprochés à l'intéressé et visés à l'article 25 se sont produits antérieurement à l'acquisition de la nationalité française ou dans le délai de dix ans à compter de la date de cette acquisition. / Elle ne peut être prononcée que dans le délai de dix ans à compter de la perpétration desdits faits. / Si les faits reprochés à l'intéressé sont visés au 1° de l'article 25, les délais mentionnés aux deux alinéas précédents sont portés à quinze ans " ;<br/>
<br/>
              4.	Considérant, d'autre part, qu'aux termes de l'article 421-2-1 du code pénal : " Constitue également un acte de terrorisme le fait de participer à un groupement formé ou à une entente établie en vue de la préparation, caractérisée par un ou plusieurs faits matériels, d'un des actes de terrorisme mentionnés aux articles précédents " ;<br/>
<br/>
              5.	Considérant, en premier lieu, que, par décision du 31 octobre 2014, le Conseil d'Etat n'a pas renvoyé au Conseil constitutionnel la question de la conformité à la Constitution de l'article 421-2-1 du code pénal ; que, par sa décision n° 2014-439 QPC du 23 janvier 2015, le Conseil constitutionnel a jugé conformes à la Constitution les dispositions du 1° de l'article 25, pour ce qui concerne les crimes ou délits constituant des actes de terrorisme, et de l'article 25-1 du code civil, dans leur rédaction applicable au litige ; qu'il n'appartient pas au Conseil d'Etat de se prononcer sur les conditions dans lesquelles le Conseil constitutionnel a statué ; que, par suite, le moyen tiré de ce que le décret attaqué aurait été pris sur le fondement de dispositions législatives contraires à la Constitution ne peut qu'être écarté ;<br/>
<br/>
              6.	Considérant, en deuxième lieu, qu'aux termes de l'article 20 de la charte des droits fondamentaux de l'Union européenne : " Toutes les personnes sont égales en droit " ; qu'aux termes de l'article 21 de la même charte : " Dans le domaine d'application des textes et sans préjudice de leurs dispositions particulières, toute discrimination exercée en fonction de la nationalité est interdite " ; qu'en vertu de l'article 20 du traité sur le fonctionnement de l'Union européenne : " Il est institué une citoyenneté de l'Union. Est citoyen de l'Union toute personne ayant la nationalité d'un Etat membre " ; <br/>
<br/>
              7.	Considérant, ainsi que l'a relevé la Cour de justice de l'Union européenne dans son arrêt du 2 mars 2010, Rottman, C-135/08, que la définition des conditions d'acquisition et de perte de la nationalité relève de la compétence de chaque État membre de l'Union ; que, toutefois, dans la mesure où la perte de la nationalité d'un Etat membre a pour conséquence la perte du statut de citoyen de l'Union, la perte de la nationalité d'un Etat membre doit, pour être conforme au droit de l'Union, répondre à des motifs d'intérêt général et être proportionnée à la gravité des faits qui la fondent, au délai écoulé depuis l'acquisition de la nationalité et à la possibilité pour l'intéressé de recouvrer une autre nationalité ; que les termes précédemment cités de la Charte des droits fondamentaux ne font pas obstacle à ce que la perte de nationalité puisse dépendre du mode ou des conditions d'acquisition de la nationalité ; <br/>
<br/>
              8.	Considérant que l'article 25 du code civil permet de déchoir de la nationalité française les personnes qui ont acquis cette nationalité et qui ont également une autre nationalité, pour des motifs limitativement énumérés et dans un délai limité, fixé à l'article 25-1 du même code ; que le 1° de l'article 25 vise notamment les personnes qui ont été condamnées pour crime ou délit constituant un acte de terrorisme ; que l'article 25-1 ne permet la déchéance de la nationalité dans ce cas qu'à la condition que les faits aient été commis moins de quinze ans auparavant et qu'ils aient été commis soit avant l'acquisition de la nationalité française, soit dans un délai de quinze ans à compter de cette acquisition ; qu'eu égard à la gravité toute particulière des actes de terrorisme et aux conditions fixées par les articles 25 et 25-1 du code civil, les dispositions de ces articles ne sont pas incompatibles avec les exigences résultant du droit de l'Union ; <br/>
<br/>
              9.	Considérant, en troisième lieu, qu'aux termes de l'article 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " La jouissance des droits et libertés reconnus dans la présente Convention doit être assurée, sans distinction aucune (...) " ; que le droit pour un étranger d'acquérir la nationalité d'un Etat signataire de cette convention et de la conserver n'est pas au nombre des droits et libertés reconnus par celle-ci ; que, par suite, M. B...ne saurait utilement soutenir que les dispositions de l'article 25 du code civil seraient incompatibles avec les stipulations précitées ; <br/>
<br/>
              10.	Considérant que le requérant ne peut non plus utilement invoquer la méconnaissance des stipulations du protocole n° 12 à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, qui n'a été ni signé ni ratifié par la France ;<br/>
<br/>
              11.	Considérant que les termes de l'article 3 du protocole additionnel n° 4 à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, selon lesquels " Nul ne peut être expulsé, par voie de mesure individuelle ou collective, du territoire de l'Etat dont il est le ressortissant ", ne font pas obstacle à ce qu'une personne puisse être déchue, en application des dispositions du code civil, de la nationalité française ; <br/>
<br/>
              12.	Considérant, en quatrième lieu, qu'il ne ressort pas des pièces du dossier que, pour prononcer la déchéance de la nationalité française de M.B..., le Premier ministre se soit exclusivement fondé sur la condamnation prononcée par le tribunal de grande instance de Paris, sans procéder à un examen des circonstances propres à la situation du requérant ; qu'ainsi, le moyen tiré de ce que le décret attaqué serait entaché d'une erreur de droit à cet égard doit être écarté ;<br/>
<br/>
              13.	Considérant qu'il résulte de tout ce qui précède que M. B...n'est pas fondé à demander l'annulation pour excès de pouvoir du décret du 28 mai 2014 l'ayant déchu de la nationalité française ;<br/>
<br/>
              14.	Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat, qui n'est pas dans la présente instance la partie perdante, au titre des frais exposés par le requérant et non compris dans les dépens ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. B...est rejetée. <br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
