<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034113834</ID>
<ANCIEN_ID>JG_L_2017_02_000000391399</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/11/38/CETATEXT000034113834.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 27/02/2017, 391399, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-02-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391399</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Appréciation de la légalité</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>Mme Sophie Baron</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHS:2017:391399.20170227</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un arrêt n° 13/02662 du 10 juillet 2014, la cour d'appel de Chambéry a sursis à statuer sur la demande formée par Mme B...A...à l'encontre de la société Cuenod et a invité les parties à saisir la juridiction administrative de l'appréciation de la légalité de la décision du 6 juillet 2010 par laquelle l'inspecteur du travail de la Haute-Savoie a autorisé son licenciement.<br/>
<br/>
              MmeA..., agissant en exécution de cet arrêt, a demandé au tribunal administratif de Grenoble d'apprécier la légalité de la décision du 6 juillet 2010 et de déclarer que cette décision est entachée d'illégalité. Par un jugement n° 1405382 du 15 juin 2015, le tribunal administratif a déclaré que cette décision est entachée d'illégalité.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 30 juin et 30 juillet 2015, la société Cuenod demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de rejeter la demande présentée par Mme A...devant le tribunal administratif de Grenoble ;<br/>
<br/>
              3°) de mettre à la charge de Mme A...la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Baron, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de la société Cuenod et à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 6 juillet 2010, l'inspecteur du travail de la 7ème section d'inspection du travail de Haute-Savoie a autorisé la société Cuenod à licencier pour motif économique Mme A..., déléguée du personnel suppléante ; que celle-ci a saisi le juge judiciaire d'une contestation portant sur le bien-fondé de la rupture de son contrat de travail ; que, par un arrêt du 10 juillet 2014, la cour d'appel de Chambéry a sursis à statuer et invité les parties à saisir le juge administratif de la légalité de l'autorisation de licenciement du 6 juillet 2010 ; que la société Cuenod se pourvoit en cassation contre le jugement du 15 juin 2015 par lequel le tribunal administratif de Grenoble a déclaré que cette décision est entachée d'illégalité ;<br/>
<br/>
              Sur la régularité du jugement attaqué :<br/>
<br/>
              2. Considérant que la circonstance que l'arrêt du 10 juillet 2014, par lequel la cour d'appel de Chambéry a, ainsi qu'il a été dit, invité les parties à saisir la juridiction administrative, faisait l'objet d'un pourvoi devant la Cour de cassation lorsque le tribunal administratif a été saisi, n'était pas de nature à faire obstacle à ce que le tribunal statue sur la demande de MmeA... ; que la société Cuenod n'est pas fondée à soutenir qu'en refusant de faire droit à ses conclusions tendant à ce qu'il soit sursis à statuer sur cette demande, sans motiver les raisons de son refus, le tribunal administratif aurait entaché son jugement d'irrégularité ; <br/>
<br/>
              Sur le bien-fondé du jugement attaqué :<br/>
<br/>
              3. Considérant qu'en vertu des dispositions du code du travail, le licenciement des salariés légalement investis de fonctions représentatives, qui bénéficient d'une protection exceptionnelle dans l'intérêt de l'ensemble des travailleurs qu'ils représentent, ne peut intervenir que sur autorisation de l'inspecteur du travail ; que, lorsque le licenciement d'un de ces salariés est envisagé, ce licenciement ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale de l'intéressé ; que, dans le cas où la demande d'autorisation de licenciement présentée par l'employeur est fondée sur un motif de caractère économique, il appartient à l'inspecteur du travail et, le cas échéant, au ministre, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si la situation de l'entreprise justifie le licenciement du salarié ;<br/>
<br/>
              4. Considérant, d'une part, qu'il résulte des termes du jugement attaqué que le tribunal administratif a estimé, par une appréciation souveraine qui n'est pas entachée de dénaturation, que la demande d'autorisation de licenciement présentée par la société Cuenod invoquait, non pas une résiliation amiable du contrat de travail, mais un licenciement pour motif économique ; que c'est, par suite, sans erreur de droit qu'il a jugé que l'inspecteur du travail devait se prononcer sur la réalité du motif économique invoqué par l'employeur ; <br/>
<br/>
              5. Considérant, d'autre part, que le tribunal administratif n'a pas dénaturé les pièces du dossier en estimant que l'inspecteur du travail s'était cru lié, pour autoriser le licenciement sollicité, par la volonté qu'avait exprimée Mme A...de quitter son employeur ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que le pourvoi de la société Cuenod doit être rejeté ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              7. Considérant que les dispositions de cet article font obstacle à ce qu'une somme soit mise à ce titre à la charge de MmeA..., qui n'est pas, dans la présente instance, la partie perdante ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Cuenod une somme de 3 000 euros à verser à Mme A... en application de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de la société Cuenod est rejeté.<br/>
Article 2 : La société Cuenod versera à Mme A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Cuenod et à Mme B...A.... <br/>
Copie en sera adressée à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
