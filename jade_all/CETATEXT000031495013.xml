<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031495013</ID>
<ANCIEN_ID>JG_L_2015_11_000000373336</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/49/50/CETATEXT000031495013.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 18/11/2015, 373336</TITRE>
<DATE_DEC>2015-11-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373336</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Timothée Paris</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:373336.20151118</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La commune de Faa'a a demandé au tribunal administratif de la Polynésie française d'annuler la décision du secrétaire d'Etat aux transports de signer la convention du 13 janvier 2010, dénommée " protocole d'accord de partenariat stratégique entre l'Etat et la Polynésie française sur l'avenir de l'aéroport de Tahiti Faa'a ", ainsi que cette convention. Par un jugement n° 1000086 du 7 décembre 2010, le tribunal a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 11PA01931 du 31 juillet 2013, la cour administrative d'appel de Paris a rejeté l'appel formé par la commune de Faa'a contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 18 novembre 2013, 18 février 2014 et 16 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, la commune de Faa'a demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - la loi organique n° 2004-192 du 27 février 2004 ;<br/>
              - la loi n° 2004-809 du 13 août 2004 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Timothée Paris, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de la commune de Faa'a et à la SCP Lyon-Caen, Thiriez, avocat de la ministre de l'écologie, du développement durable et de l'énergie ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que la commune de Faa'a a demandé au tribunal administratif de la Polynésie française l'annulation, pour excès de pouvoir, de la décision du secrétaire d'Etat aux transports de signer la convention du 13 janvier 2010, dénommée " protocole d'accord de partenariat stratégique entre l'Etat et la Polynésie française sur l'avenir de l'aéroport de Tahiti Faa'a ", ainsi que cette convention ; que par un jugement du 7 décembre 2010, le tribunal a rejeté cette demande ; que la commune de Faa'a se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Paris du 31 juillet 2010 rejetant son appel formé contre ce jugement ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 7 de la loi organique du 27 février 2004 relative à la Polynésie française : " Dans les matières qui relèvent de la compétence de l'Etat, sont applicables en Polynésie française les dispositions législatives et réglementaires qui comportent une mention expresse à cette fin. / Par dérogation au premier alinéa, sont applicables de plein droit en Polynésie française, sans préjudice de dispositions les adaptant à son organisation particulière, les dispositions législatives et réglementaires qui sont relatives : / (...) 3° Au domaine public de l'Etat (...) " ; <br/>
<br/>
              3. Considérant que l'article 28 de la loi du 13 août 2004 relative aux libertés et responsabilités locales a prévu le transfert, dans les conditions qu'il fixe, aux collectivités territoriales ou à leurs groupements, de " la propriété, l'aménagement, l'entretien et la gestion des aérodromes civils appartenant à l'Etat ", situés dans le ressort géographique de ces collectivités ; que ces dispositions, qui ne comportent aucune mention expresse prévoyant leur application en Polynésie française, ont pour objet de transférer aux collectivités territoriales l'exercice de compétences jusque-là dévolues à l'Etat ; que, contrairement à ce que soutient la commune de Faa'a, elles ne sont pas relatives au domaine public de l'Etat au sens du 3° de l'article 7 de la loi organique relative à la Polynésie française, alors même que le transfert de compétence qu'elles prévoient est assorti du transfert des moyens pour l'exercice de celles-ci et, en particulier, de la propriété des aérodromes civils appartenant à l'Etat ; qu'il suit de là qu'elles ne sont pas applicables en Polynésie française et ne sauraient donc avoir eu pour effet de transférer de l'Etat à cette collectivité la propriété de l'aéroport de Tahiti Faa'a ni, en tout état de cause, la compétence pour son exploitation ; qu'il y a lieu de substituer ce motif à celui retenu par la cour administrative d'appel de Paris pour écarter le moyen tiré de l'incompétence du secrétaire d'Etat aux transports pour signer la convention du 13 janvier 2010 ; que les moyens du pourvoi sont, dès lors, sans incidence sur le bien-fondé de l'arrêt attaqué ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que la commune de Faa'a n'est pas fondée à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que l'Etat, qui n'est pas, dans la présente instance, la partie perdante, verse à la commune de Faa'a la somme que celle-ci demande au titre des frais exposés par elle et non compris dans les dépens ; qu'il y a lieu, en revanche, de mettre à la charge de la commune de Faa'a une somme de 2 000 euros au titre des frais exposés par l'Etat et non compris dans les dépens ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la commune de Faa'a est rejeté. <br/>
Article 2 : La commune de Faa'a versera à l'Etat une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à la commune de Faa'a et à la ministre de l'écologie, du développement durable et de l'énergie. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">46-01-02-02 OUTRE-MER. DROIT APPLICABLE. STATUTS. POLYNÉSIE FRANÇAISE. - APPLICABILITÉ DE PLEIN DROIT DE CERTAINES DISPOSITIONS LÉGISLATIVES ET RÉGLEMENTAIRES - DISPOSITIONS RELATIVES AU DOMAINE PUBLIC DE L'ETAT (3° DE L'ARTICLE 7 DE LA LOI ORGANIQUE STATUTAIRE) - NOTION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">46-01-03-02-03 OUTRE-MER. DROIT APPLICABLE. LOIS ET RÈGLEMENTS (HORS STATUTS DES COLLECTIVITÉS). COLLECTIVITÉS D'OUTRE-MER ET NOUVELLE-CALÉDONIE. POLYNÉSIE FRANÇAISE. - TRANSFERT AUX COLLECTIVITÉS TERRITORIALES DES AÉRODROMES CIVILS DE L'ETAT (ARTICLE 28 DE LA LOI DU 13 AOÛT 2004) - APPLICABILITÉ EN POLYNÉSIE FRANÇAISE - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">65-03-04-06 TRANSPORTS. TRANSPORTS AÉRIENS. AÉROPORTS. RÉGIME JURIDIQUE. - TRANSFERT AUX COLLECTIVITÉS TERRITORIALES DES AÉRODROMES CIVILS DE L'ETAT (ARTICLE 28 DE LA LOI DU 13 AOÛT 2004) - APPLICABILITÉ EN POLYNÉSIE FRANÇAISE - ABSENCE.
</SCT>
<ANA ID="9A"> 46-01-02-02 L'article 28 de la loi n° 2004-809 du 13 août 2004 transfère aux collectivités territoriales ou à leurs groupements la propriété, l'aménagement, l'entretien et la gestion des aérodromes civils appartenant à l'Etat. Ces dispositions, qui ne comportent aucune mention expresse prévoyant leur application en Polynésie française, ont pour objet de transférer aux collectivités territoriales l'exercice de compétences jusque-là dévolues à l'Etat. Elles ne sont pas relatives au domaine public de l'Etat au sens du 3° de l'article 7 de la loi organique n° 2004-192 du 27 février 2004 relative à la Polynésie française (applicabilité de plein droit en Polynésie française des dispositions législatives et réglementaires relatives au domaine public de l'Etat), alors même que le transfert de compétence qu'elles prévoient est assorti du transfert des moyens pour l'exercice de celles-ci et, en particulier, de la propriété des aérodromes civils appartenant à l'Etat. Il suit de là qu'elles ne sont pas applicables en Polynésie française.</ANA>
<ANA ID="9B"> 46-01-03-02-03 L'article 28 de la loi n° 2004-809 du 13 août 2004 transfère aux collectivités territoriales ou à leurs groupements la propriété, l'aménagement, l'entretien et la gestion des aérodromes civils appartenant à l'Etat. Ces dispositions, qui ne comportent aucune mention expresse prévoyant leur application en Polynésie française, ont pour objet de transférer aux collectivités territoriales l'exercice de compétences jusque-là dévolues à l'Etat. Elles ne sont pas relatives au domaine public de l'Etat au sens du 3° de l'article 7 de la loi organique n° 2004-192 du 27 février 2004 relative à la Polynésie française (applicabilité de plein droit en Polynésie française des dispositions législatives et réglementaires relatives au domaine public de l'Etat), alors même que le transfert de compétence qu'elles prévoient est assorti du transfert des moyens pour l'exercice de celles-ci et, en particulier, de la propriété des aérodromes civils appartenant à l'Etat. Il suit de là qu'elles ne sont pas applicables en Polynésie française.</ANA>
<ANA ID="9C"> 65-03-04-06 L'article 28 de la loi n° 2004-809 du 13 août 2004 transfère aux collectivités territoriales ou à leurs groupements la propriété, l'aménagement, l'entretien et la gestion des aérodromes civils appartenant à l'Etat. Ces dispositions, qui ne comportent aucune mention expresse prévoyant leur application en Polynésie française, ont pour objet de transférer aux collectivités territoriales l'exercice de compétences jusque-là dévolues à l'Etat. Elles ne sont pas relatives au domaine public de l'Etat au sens du 3° de l'article 7 de la loi organique n° 2004-192 du 27 février 2004 relative à la Polynésie française (applicabilité de plein droit en Polynésie française des dispositions législatives et réglementaires relatives au domaine public de l'Etat), alors même que le transfert de compétence qu'elles prévoient est assorti du transfert des moyens pour l'exercice de celles-ci et, en particulier, de la propriété des aérodromes civils appartenant à l'Etat. Il suit de là qu'elles ne sont pas applicables en Polynésie française.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
