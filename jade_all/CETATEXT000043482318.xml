<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043482318</ID>
<ANCIEN_ID>JG_L_2021_05_000000440167</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/48/23/CETATEXT000043482318.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 05/05/2021, 440167, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-05-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440167</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Jacques Reiller</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Domingo</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:440167.20210505</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
       L'association française des oeuvres pontificales missionnaires (AFOPM) a demandé au tribunal administratif de Lyon, d'une part, d'annuler la décision du 3 février 2015 par laquelle le préfet du Rhône a refusé de lui délivrer un certificat de non-opposition à l'acceptation du legs consenti par M. A... B..., d'autre part, d'enjoindre au préfet du Rhône de lui délivrer ce certificat dans un délai de deux mois. Par un jugement n° 1507701 du 28 mars 2017, le tribunal administratif de Lyon a rejeté sa demande.<br/>
<br/>
        Par un arrêt n° 17LY02137 du 13 février 2020, la cour administrative d'appel de Lyon a rejeté l'appel formé par l'association française des oeuvres pontificales missionnaires contre ce jugement.<br/>
<br/>
        Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 avril et 20 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, l'association française des oeuvres pontificales missionnaires demande au Conseil d'Etat :<br/>
<br/>
        1°) d'annuler cet arrêt ;<br/>
<br/>
        2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
        3°) de mettre à la charge de l'Etat une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
       Vu les autres pièces du dossier ;<br/>
<br/>
       Vu :<br/>
       - le code civil ;<br/>
       - le décret n° 2007-807 du 11 mai 2007 ;<br/>
       - le code de justice administrative et le décret n° 2020-1406 du 18  novembre 2020 ;<br/>
<br/>
<br/>
<br/>
       Après avoir entendu en séance publique :<br/>
<br/>
       - le rapport de M. Jacques Reiller, conseiller d'Etat,  <br/>
<br/>
       - les conclusions de M. Laurent Domingo, rapporteur public ;<br/>
<br/>
       La parole ayant été donnée, après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de l'association française des oeuvres pontificales missionnaires (AFOPM) ;<br/>
<br/>
       Vu la note en délibéré, enregistrée le 29 avril 2021, présentée par l'association française des oeuvres pontificales missionnaires ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un testament authentique reçu le 28 avril 2005, M. B... a institué " légataire universel en nue-propriété le Vatican avec obligation d'affecter ce legs prioritairement à la basilique Sainte-Marie-Majeure de Rome ". A la suite du décès de M. B... le 23 juin 2012, le préfet du Rhône a, par un courrier du 3 février 2015, refusé de délivrer un certificat de non opposition à l'association française des oeuvres pontificales missionnaires (AFOPM), dont le conseil d'administration avait accepté le legs de M. B... le 5 novembre 2013, au motif qu'elle n'était pas le légataire désigné par le testament. L'AFOPM se pourvoit en cassation contre l'arrêt du 13 février 2020 par lequel la cour administrative d'appel de Lyon a rejeté l'appel qu'elle avait formé contre le jugement du tribunal administratif de Lyon du 28 mars 2017 rejetant sa demande d'annulation de la décision du préfet du Rhône.<br/>
<br/>
              2. L'article 910 du code civil, dans sa rédaction applicable à la date de la décision attaquée, dispose que : " (...) les dispositions entre vifs ou par testament au profit des fondations, des congrégations et des associations ayant la capacité à recevoir des libéralités et, dans les départements du Bas-Rhin, du Haut-Rhin et de la Moselle, des établissements publics du culte et des associations inscrites de droit local, à l'exception des associations ou fondations dont les activités ou celles de leurs dirigeants sont visées à l'article 1er de la loi n° 2001-504 du 12 juin 2001 tendant à renforcer la prévention et la répression des mouvements sectaires portant atteinte aux droits de l'homme et aux libertés fondamentales, sont acceptées librement par celles-ci. / Si le représentant de l'Etat dans le département constate que l'organisme légataire ou donataire ne satisfait pas aux conditions légales exigées pour avoir la capacité juridique à recevoir des libéralités ou qu'il n'est pas apte à utiliser la libéralité conformément à son objet statutaire, il peut former opposition à la libéralité, dans des conditions précisées par décret, la privant ainsi d'effet. / Les libéralités consenties à des Etats étrangers ou à des établissements étrangers habilités par leur droit national à recevoir des libéralités sont acceptées librement par ces Etats ou par ces établissements, sauf opposition formée par l'autorité compétente, dans des conditions fixées par décret en Conseil d'Etat ". Les articles 6-1 à 6-7 du décret du 11 mai 2007 relatif aux associations, fondations, congrégations et établissements publics du culte et portant application de l'article 910 du code civil donnent compétence au ministre de l'intérieur pour s'opposer aux libéralités consenties aux Etats et établissements étrangers et organisent les conditions dans lesquelles s'exerce ce droit d'opposition. <br/>
              3. Il ressort des énonciations de l'arrêt attaqué que la cour administrative d'appel a jugé, par une appréciation souveraine des faits de l'espèce et des pièces du dossier, laquelle n'est pas entachée de dénaturation, que M. B..., en désignant le " Vatican " comme légataire " avec obligation d'affecter ce legs prioritairement à la basilique Sainte-Marie-Majeure de Rome ", avait entendu stipuler son legs en faveur du Saint-Siège. En l'état de cette appréciation souveraine, la cour n'a pas commis d'erreur de droit en retenant que, quels que soient les liens existants entre le Saint-Siège et l'association requérante, cette dernière ne pouvait être regardée comme le bénéficiaire du legs de M. B....<br/>
<br/>
              4. Dès lors que le Saint-Siège, propriétaire de la basilique désignée par le legs, est un sujet de droit international, la cour n'a pas commis d'erreur de droit en jugeant qu'il avait la qualité d'un Etat ou d'un établissement étranger au sens et pour l'application des dispositions du dernier alinéa de l'article 910 du code civil. C'est sans erreur de droit qu'elle en a déduit, par un arrêt suffisamment motivé, que le ministre de l'intérieur était seul compétent pour exercer, sur ce legs, le droit d'opposition prévu à l'article 910 du code civil. <br/>
<br/>
              5. Il s'ensuit qu'en jugeant que le préfet du Rhône avait pu refuser de délivrer à l'association française des oeuvres pontificales missionnaires un certificat de non opposition au legs de M. B... au motif que ce dernier avait été stipulé non pas au profit de l'association mais en faveur du Saint-Siège, la cour, qui pouvait considérer que le refus du préfet ne portait, en tout état de cause, pas atteinte au principe de séparation entre l'Eglise et l'Etat, n'a entaché son arrêt, qui est suffisamment motivé, d'aucune erreur de droit. <br/>
<br/>
              6. Il résulte de tout ce qui précède que l'association requérante n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. Son pourvoi doit dès lors être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de l'association française des oeuvres pontificales missionnaires est rejeté.<br/>
Article 2 : La présente décision sera notifiée à l'association française des oeuvres pontificales missionnaires et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
