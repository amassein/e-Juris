<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043763564</ID>
<ANCIEN_ID>JG_L_2021_07_000000437849</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/76/35/CETATEXT000043763564.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 05/07/2021, 437849</TITRE>
<DATE_DEC>2021-07-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437849</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP L. POULET-ODENT ; SCP LYON-CAEN, THIRIEZ ; CABINET COLIN - STOCLET</AVOCATS>
<RAPPORTEUR>Mme Christelle Thomas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:437849.20210705</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le syndicat de la copropriété " Les Terrasses de l'Aqueduc " a demandé au tribunal administratif de Montpellier d'annuler pour excès de pouvoir l'arrêté du 19 octobre 2018 par lequel le maire de Montpellier a accordé à la SNC Occitane Promotion un permis de construire un immeuble de 24 logements sur un terrain situé au 453, rue de la croix de Lavit, ainsi que la décision rejetant son recours gracieux. Par un jugement n° 1900700 du 21 novembre 2019, le tribunal administratif de Montpellier a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 21 janvier 2020, 25 juin 2020 et 10 juin 2021 au secrétariat du contentieux du Conseil d'Etat, le syndicat de la copropriété " Les Terrasses de l'Aqueduc " demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Montpellier la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Christelle Thomas, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la commune de Montpellier et à la SCP L. Poulet, Odent, avocat de la SNC LNC Occitane Promotion ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1.	Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté en date du 19 octobre 2018, le maire de Montpellier a accordé à la SNC LNC Occitane Promotion un permis de construire un immeuble collectif de vingt-quatre logements sur un terrain situé au 453, rue de la croix de Lavit. Le syndicat de la copropriété " Les Terrasses de l'Aqueduc ", voisin du projet, se pourvoit en cassation contre le jugement du 21 novembre 2019 par lequel le tribunal administratif de Montpellier a rejeté sa demande tendant à l'annulation pour excès de pouvoir de cet arrêté.<br/>
<br/>
              2.	Il ressort également des pièces du dossier soumis aux juges du fond que l'arrêté du 19 octobre 2018 du maire de Montpellier accordant le permis de construire litigieux à la SNC LNC Occitane Promotion est assorti d'une réserve technique relative à la rétrocession à la métropole de Montpellier d'une partie de la parcelle, d'une superficie de 164 m², aux fins de la création d'un cheminement piétonnier ouvert à la circulation du public. Pour écarter le moyen tiré de la méconnaissance de l'article 7 du règlement du plan local d'urbanisme applicable à la zone 2U1, relatif à l'implantation des constructions par rapport aux limites séparatives, le tribunal administratif a jugé que la conformité de l'autorisation de construire aux règles du plan local d'urbanisme devait être appréciée en prenant en considération cette prescription, ainsi que la division foncière en résultant nécessairement. En tenant compte des effets obligatoires attachés aux prescriptions assortissant une autorisation de construire pour juger que les règles d'implantation des constructions par rapport aux limites séparatives prévues à l'article 7 du règlement du plan local d'urbanisme ne trouvaient pas à s'appliquer, le tribunal administratif n'a pas commis d'erreur de droit.<br/>
<br/>
              3.	Toutefois, il ressort des écritures qui lui étaient soumises que le tribunal était saisi d'un moyen tiré de l'illégalité, en l'espèce, de la prescription en cause, en ce qu'elle prévoit la rétrocession à la collectivité publique d'un chemin à aménager selon ses indications. En omettant de se prononcer sur ce moyen, qui n'était pas inopérant s'agissant de la légalité d'une prescription qui devrait être prise en compte pour apprécier la légalité du permis de construire litigieux, le tribunal administratif a insuffisamment motivé son jugement. Par suite, son jugement doit être annulé pour ce motif, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi.<br/>
<br/>
              4.	Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Montpellier la somme de 3 000 euros à verser au syndicat de la copropriété " Les Terrasses de l'Aqueduc " au titre de l'article L. 761-1 du code de justice administrative. En revanche, ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge du syndicat requérant, qui n'est pas la partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 21 novembre 2019 du tribunal administratif de Montpellier est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Montpellier.<br/>
Article 3 : La commune de Montpellier versera la somme de 3 000 euros au syndicat de la copropriété " Les Terrasses de l'Aqueduc " au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la commune de Montpellier et par la SNC LNC Occitane Promotion au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée au syndicat de la copropriété " Les Terrasses de l'Aqueduc ", à la SNC LNC Occitane Promotion et à la commune de Montpellier.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-03-03-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. LÉGALITÉ INTERNE DU PERMIS DE CONSTRUIRE. LÉGALITÉ AU REGARD DE LA RÉGLEMENTATION LOCALE. - CONFORMITÉ AUX RÈGLES DU PLU RELATIVES À L'IMPLANTATION PAR RAPPORT AUX LIMITES SÉPARATIVES - APPRÉCIATION PRENANT EN COMPTE LES PRESCRIPTIONS DONT EST ASSORTI LE PERMIS [RJ1] - EXISTENCE [RJ2].
</SCT>
<ANA ID="9A"> 68-03-03-02 Permis de construire assorti d'une réserve technique relative à la rétrocession à la métropole d'une partie de la parcelle, d'une superficie de 164 m², aux fins de la création d'un cheminement piétonnier ouvert à la circulation du public.,,,La conformité de l'autorisation de construire aux règles du plan local d'urbanisme (PLU) relatives à l'implantation des constructions par rapport aux limites séparatives doit être appréciée en prenant en considération cette prescription, ainsi que la division foncière en résultant nécessairement. Par suite, les règles d'implantation des constructions par rapport aux limites séparatives, et non par rapport aux voies publiques, ne trouvaient pas à s'appliquer.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur la possibilité de subordonner la délivrance d'un permis à la création d'une servitude de passage, CE, 3 juin 2020, Société Compagnie immobilière méditerranée, n° 427781, T. pp. 1060-1069.,,[RJ2] Comp., sur l'absence de prise en compte des voies privées dont la création est prévue par le projet, CE, Section, 13 mars 1970, Epoux,, n° 74278, p. 192 ; CE, 10 février 2016, SCI Porte de Noisy - Commune de Noisy-le-Grand, n°s 383738 et autres, T. pp. 990-995.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
