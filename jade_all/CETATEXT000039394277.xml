<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039394277</ID>
<ANCIEN_ID>JG_L_2019_11_000000414695</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/39/42/CETATEXT000039394277.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 18/11/2019, 414695, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-11-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414695</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>Mme Cécile Viton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:414695.20191118</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société anonyme Leroy Merlin France a demandé au tribunal administratif de Dijon de prononcer la restitution d'une partie de la taxe sur les surfaces commerciales qu'elle a acquittée au titre des années 2010, 2011 et 2012, pour son établissement situé à Quetigny (Côte-d'Or). Par un jugement no 1401151 du 4 avril 2017, le tribunal administratif de Dijon a rejeté sa demande.<br/>
<br/>
              Par un arrêt no 17LY02235 du 28 septembre 2017, enregistré le jour même au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Lyon a, d'une part, transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 2 juin 2017 au greffe de cette cour, formé par la société Leroy Merlin France contre ce jugement en tant qu'il concerne les années 2011 et 2012 et, d'autre part, rejeté l'appel formé par la société contre ce jugement en tant qu'il concerne l'année 2010.<br/>
<br/>
              Par ce pourvoi, ainsi qu'un mémoire complémentaire et des observations complémentaires enregistrés les 15 novembre 2017 et 2 octobre 2018 au secrétariat du contentieux du Conseil d'Etat, la société Leroy Merlin France demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement, en tant qu'il se prononce sur les impositions dues au titre des années 2011 et 2012 ;<br/>
<br/>
              2°) réglant l'affaire au fond dans cette mesure, de faire droit à sa demande ; <br/>
<br/>
              3°) d'ordonner la communication du rescrit fiscal adressé à la Fédération française du négoce de l'ameublement et de l'équipement de la maison pour ses adhérents en matière de taxe sur les surfaces commerciales ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 72-657 du 13 juillet 1972 ;<br/>
              - le décret n° 95-85 du 26 janvier 1995 ; <br/>
              - la décision nos 414695 et autres du 28 septembre 2018 du Conseil d'Etat statuant au contentieux ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Viton, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de la société Leroy Merlin France ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Leroy Merlin France a été assujettie à la taxe sur les surfaces commerciales au titre des années 2010, 2011 et 2012 à raison d'un établissement situé à Quetigny (Côte d'Or). Par une décision du 5 février 2014, l'administration fiscale a rejeté sa demande, formée le 18 juillet 2013, de restitution de la somme totale de 110 619 euros, correspondant à l'application de la réduction de taux de 30 % prévue par les dispositions du A de l'article 3 du décret du 26 janvier 1995 en ce qui concerne la vente exclusive de certaines marchandises par des professions exerçant une activité nécessitant des surfaces de vente anormalement élevées. La société Leroy Merlin France se pourvoit en cassation contre le jugement du 4 avril 2017 du tribunal administratif de Dijon en tant qu'il rejette sa demande de restitution d'une partie des cotisations de taxe sur les surfaces commerciales mises à sa charge au titre des années 2011 et 2012.<br/>
<br/>
              2. Aux termes de l'article 3 de la loi du 13 juillet 1972 instituant des mesures en faveur de certaines catégories de commerçants et artisans âgés, dans sa rédaction applicable aux années d'imposition en litige : " Il est institué une taxe sur les surfaces commerciales assise sur la surface de vente des magasins de commerce de détail, dès lors qu'elle dépasse quatre cents mètres carrés des établissements ouverts à partir du 1er janvier 1960 quelle que soit la forme juridique de l'entreprise qui les exploite. (...) / Un décret prévoira (...) des réductions pour les professions dont l'exercice requiert des superficies de vente anormalement élevées (...) ". Aux termes de l'article 3 du décret du 26 janvier 1995 relatif à la taxe sur les surfaces commerciales, pris pour l'application de ces dispositions, dans sa rédaction applicable aux années d'imposition en litige : " A. - La réduction de taux prévue au dix-septième alinéa de l'article 3 de la loi du 13 juillet 1972 susvisé en faveur des professions dont l'exercice requiert des superficies de vente anormalement élevées est fixée à 30 p. 100 en ce qui concerne la vente exclusive des marchandises énumérées ci-après : / - meubles meublants ; / (...) - matériaux de construction (...) ".<br/>
<br/>
              3. En subordonnant, par le A de l'article 3 du décret du 26 janvier 1995, le bénéfice de la réduction de taux, fixée à 30 %, à la condition que l'activité de vente des marchandises qu'il énumère soit exercée à titre exclusif, le pouvoir réglementaire s'est borné à déterminer le champ d'application de la mesure de réduction de taux prévue par le législateur en faveur des professions dont l'exercice requiert des superficies de vente anormalement élevées, sans excéder les compétences qu'il tenait des dispositions législatives citées au point 2.<br/>
<br/>
              4. Par suite, le tribunal administratif de Dijon a entaché son jugement d'une erreur de droit en se fondant, avant de rejeter la demande de la requérante tendant au bénéfice de cette réduction, sur ce que le pouvoir réglementaire avait, en posant une condition d'exclusivité non prévue par la loi, entaché les dispositions du A de l'article 3 du décret du 26 janvier 1995 d'illégalité. Dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société Leroy Merlin France est fondée à demander l'annulation du jugement qu'elle attaque en tant qu'il statue sur les impositions acquittées au titre des années 2011 et 2012.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 000 euros à verser à la société Leroy Merlin France au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Dijon du 4 avril 2017 est annulé en tant qu'il statue sur la taxe sur les surfaces commerciales à laquelle la société Leroy Merlin France a été assujettie au titre des années 2011 et 2012.<br/>
Article 2 : L'affaire est renvoyée dans cette mesure au tribunal administratif de Dijon.<br/>
Article 3 : L'Etat versera une somme de 1 000 euros à la société Leroy Merlin France au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société anonyme Leroy Merlin France et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
