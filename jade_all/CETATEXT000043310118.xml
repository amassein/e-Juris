<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043310118</ID>
<ANCIEN_ID>JG_L_2021_03_000000446461</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/31/01/CETATEXT000043310118.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 30/03/2021, 446461, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>446461</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Nicolas Agnoux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:446461.20210330</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme G... F... a demandé au tribunal administratif de La Réunion d'annuler les opérations électorales qui se sont déroulées le 11 juillet 2020 en vue de l'élection des quatre adjoints de quartier de la commune du Tampon (La Réunion). Par un jugement n° 2000576 du 12 octobre 2020, le tribunal administratif de La Réunion a rejeté cette protestation. <br/>
<br/>
              Par une requête, enregistrée le 15 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, Mme F... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) de faire droit à sa protestation ; <br/>
<br/>
              3°) d'ordonner le remboursement de la somme de 1 000 euros mise à sa charge au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
              4°) d'enjoindre au maire de la commune du Tampon de communiquer les convocations distribuées aux 49 conseillers municipaux, accompagnées de leurs justificatifs de transmission, ainsi que les demandes écrites des conseillers municipaux qui auraient fait la demande de recevoir leurs convocations par écrit à leur domicile ou à une autre adresse ;<br/>
<br/>
              5°) de mettre solidairement à la charge de Mme E..., Mme A..., M. C... et M. B... une somme totale de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 2019-1461 du 27 décembre 2019 ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Agnoux, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteure publique ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le 11 juillet 2020, le conseil municipal de la commune du Tampon (La Réunion) s'est réuni en vue notamment de l'élection des quatre adjoints de quartier de la commune. Par un jugement du 12 octobre 2020, dont Mme F... demande l'annulation, le tribunal administratif de La Réunion a rejeté la protestation de la requérante tendant à l'annulation de ces opérations électorales, à l'issue desquelles Mme H...-E..., Mme A..., M. C... et M. B... ont été élus adjoints de quartier de la commune du Tampon. <br/>
<br/>
              Sur la régularité des opérations électorales : <br/>
<br/>
              2. Aux termes de l'article L. 2122-8 du code général des collectivités territoriales : " Pour toute élection du maire ou des adjoints, les membres du conseil municipal sont convoqués dans les formes et délais prévus aux articles L. 2121-10 à L. 2121-12. La convocation contient mention spéciale de l'élection à laquelle il doit être procédé. " Selon l'article L. 2121-10 du même code, dans sa version issue de la loi du 27 décembre 2019 relative à l'engagement dans la vie locale et à la proximité de l'action publique : " Toute convocation est faite par le maire. Elle indique les questions portées à l'ordre du jour. Elle est mentionnée au registre des délibérations, affichée ou publiée. Elle est transmise de manière dématérialisée ou, si les conseillers municipaux en font la demande, adressée par écrit à leur domicile ou à une autre adresse. " Aux termes de l'article L. 2121-12 du même code : " Dans les communes de 3 500 habitants et plus (...) le délai de convocation est fixé à cinq jours francs (...) ". <br/>
<br/>
              3. Il résulte de ces dispositions que les convocations aux réunions du conseil municipal doivent être envoyées aux conseillers municipaux de manière dématérialisée ou, s'ils en font expressément la demande, être adressées par écrit à leur domicile personnel ou à une autre adresse de leur choix, laquelle peut être la mairie, et qu'il doit être procédé à cet envoi dans un délai de cinq jours francs avant la réunion. La méconnaissance de ces règles est de nature à entacher d'illégalité les délibérations prises par le conseil municipal alors même que les conseillers municipaux concernés auraient été présents ou représentés lors de la séance. Il ne peut en aller différemment que dans le cas où il est établi que les convocations irrégulièrement adressées ou distribuées sont effectivement parvenues à leurs destinataires cinq jours francs au moins avant le jour de la réunion.<br/>
<br/>
              4. En premier lieu, si Mme F... soutient que les convocations destinées aux conseillers municipaux de la commune du Tampon en vue de la séance du 11 juillet 2020 au cours de laquelle a eu lieu l'élection des quatre adjoints de quartier de la commune, n'ont pas été transmises de manière dématérialisée, ni même adressées par écrit aux domiciles personnels des conseillers municipaux, lesquels n'en avaient d'ailleurs pas fait la demande, il résulte de l'instruction et il n'est pas contesté que ces convocations ont été remises à l'ensemble des élus le 5 juillet 2020, soit par des agents de la police municipale, soit par d'autres agents municipaux, essentiellement en mains propres à l'occasion de la première séance du conseil municipal qui s'est tenue à cette date. Dès lors que ces convocations à la seconde séance du conseil municipal, organisée le 11 juillet 2020 en présence de l'ensemble des conseillers municipaux, sont ainsi parvenues à leurs destinataires cinq jours francs au moins avant le jour de cette séance, le moyen tiré de l'absence de transmission dématérialisée des convocations effectivement reçues par les conseillers municipaux n'est pas de nature à entacher d'illégalité les opérations électorales qui se sont déroulées le 11 juillet 2020 en vue de l'élection des quatre adjoints de quartier de la commune du Tampon. <br/>
<br/>
              5. En second lieu, il est constant que, dans le délai de protestation de cinq jours prévu par l'article L. 2122-13 et D. 2122-2 du code général des collectivités territoriales pour contester l'élection du maire et de ses adjoints, Mme F... n'avait énoncé que le grief relatif à l'absence de transmission dématérialisée des convocations et ne citait à l'appui de sa protestation que les dispositions de l'article L. 2121-10 du code général des collectivités territoriales. Par suite, le nouveau grief soulevé en appel, qui ne présente pas un caractère d'ordre public et n'avait pas à être révélé d'office par le tribunal, tiré de ce qu'en violation des dispositions de l'article L. 2122-8 du même code les convocations ne comportaient pas de mention spéciale de l'élection des adjoints de quartier, est irrecevable.<br/>
<br/>
              Sur les frais non compris dans les dépens mis à la charge de Mme F... devant le tribunal administratif : <br/>
<br/>
              6. Par le jugement attaqué, le tribunal administratif de La Réunion a mis à la charge de Mme F..., sur le fondement de l'article L. 761-1 du code de justice administrative, le versement d'une somme totale de 1 000 euros au titre des frais non compris dans les dépens exposés en première instance par Mme H...-E..., Mme A..., M. C... et M. B.... En estimant ainsi inéquitable, " dans les circonstances de l'espèce ", de laisser à la charge des défendeurs les frais qu'ils avaient été contraints d'engager du fait de la protestation formée par Mme F..., qui était rejetée, le tribunal a suffisamment motivé son jugement. Par ailleurs, en fixant à 1 000 euros le montant de ces frais, alors qu'il n'est pas démontré ni même allégué que des difficultés financières s'opposeraient à ce que Mme F... acquitte cette somme au profit des quatre intéressés, le tribunal administratif n'a pas fait une inexacte appréciation des circonstances de l'espèce. <br/>
<br/>
              7. Il résulte de tout ce qui précède, sans qu'il soit besoin d'ordonner la production des divers documents sollicités par Mme F..., que celle-ci n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de La Réunion a rejeté sa protestation et mis à sa charge une somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Sur les conclusions de la requête tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de Mme H...-E..., Mme A..., M. C... et M. B..., qui ne sont pas les parties perdantes dans la présente instance, la somme demandée par Mme F... au titre des frais exposés par elle et non compris dans les dépens. Par ailleurs, il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de la requérante la somme demandée au même titre par les quatre défendeurs.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de Mme F... est rejetée.<br/>
Article 2 : Les conclusions de Mme H...-E..., Mme A..., M. C... et M. B... tendant au bénéfice de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à Mme G... F..., Mme K... H...-E..., Mme I... A..., M. D... C... et M. H...-J... B....<br/>
Copie en sera adressée au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
