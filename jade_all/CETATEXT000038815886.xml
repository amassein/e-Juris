<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038815886</ID>
<ANCIEN_ID>JG_L_2019_07_000000430473</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/81/58/CETATEXT000038815886.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 24/07/2019, 430473, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430473</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Nevache</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:430473.20190724</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le syndicat des copropriétaires de l'ensemble immobilier sis 86 à 94 rue Gutenberg, à l'appui de sa demande tendant à l'annulation pour excès de pouvoir des permis de construire et d'aménager un complexe sportif sur un terrain situé 84, rue Gutenberg, délivrés par le maire de Palaiseau à la société civile immobilière INRAA, ainsi que du certificat d'autorisation tacite se rapportant à cette opération, a produit un mémoire, enregistré le 21 février 2019 au greffe du tribunal administratif de Versailles, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lequel il soulève une question prioritaire de constitutionnalité.<br/>
<br/>
              Par un jugement n° 1706682 du 3 mai 2019, enregistré le 6 mai 2019 au secrétariat du contentieux du Conseil d'Etat, le tribunal administratif de Versailles, avant qu'il soit statué sur la demande du syndicat des copropriétaires, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 600-5-1 du code de l'urbanisme dans sa rédaction issue de la loi n° 2018-1021 du 23 novembre 2018.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de l'urbanisme, notamment son article L. 600-5-1 ;<br/>
              - la loi n° 2018-1021 du 23 novembre 2018 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Nevache, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gadiou, Chevallier, avocat du syndicat des copropriétaires de l'ensemble immobilier sis 86 à 94 rue Gutenberg ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes de l'article L. 600-5-1 du code de l'urbanisme, dans sa rédaction issue de l'article 80 de la loi du 23 novembre 2018 portant évolution du logement, de l'aménagement et du numérique : " Sans préjudice de la mise en oeuvre de l'article L. 600-5, le juge administratif qui, saisi de conclusions dirigées contre un permis de construire, de démolir ou d'aménager ou contre une décision de non-opposition à déclaration préalable estime, après avoir constaté que les autres moyens ne sont pas fondés, qu'un vice entraînant l'illégalité de cet acte est susceptible d'être régularisé, sursoit à statuer, après avoir invité les parties à présenter leurs observations, jusqu'à l'expiration du délai qu'il fixe pour cette régularisation, même après l'achèvement des travaux. Si une mesure de régularisation est notifiée dans ce délai au juge, celui-ci statue après avoir invité les parties à présenter leurs observations. Le refus par le juge de faire droit à une demande de sursis à statuer est motivé ". <br/>
<br/>
              3. S'il résulte de ces dispositions que le juge administratif peut, sans préjudice de la mise en oeuvre de l'article L. 600-5 du même code, être tenu de surseoir à statuer sur les conclusions dirigées contre un permis de construire, de démolir ou d'aménager ou contre une décision de non-opposition à déclaration préalable dont il est saisi, en vue de permettre la régularisation en cours d'instance d'un vice qui entache la décision litigieuse et entraîne son illégalité, c'est à la condition que, à la date à laquelle il se prononce, une autorisation d'urbanisme puisse légalement intervenir pour régulariser le projet, compte tenu de ses caractéristiques, de l'avancement des travaux et des règles d'urbanisme applicables, dans les mêmes conditions que si l'autorisation d'urbanisme initiale avait été annulée pour excès de pouvoir. Il résulte également de ces dispositions qu'il lui appartient, avant de surseoir à statuer, d'inviter les parties à présenter leurs observations jusqu'à l'expiration du délai qu'il fixe, qui doit être suffisant au regard de l'objet des observations demandées pour garantir le débat contradictoire et les droits de la défense, puis de faire de même avant de régler définitivement le fond du litige si une mesure de régularisation lui a été notifiée avant l'expiration du délai fixé à cette fin.<br/>
<br/>
              4. Ces dispositions, dont l'application immédiate aux instances en cours dès leur entrée en vigueur ne porte atteinte à aucune situation qui serait acquise ou définitivement constituée, se bornent, sans affecter la substance du droit au recours ni porter atteinte à aucun des droits des requérants, à instituer des règles de procédure concernant exclusivement les pouvoirs du juge administratif en matière de contentieux de l'urbanisme. Eu égard à leurs effets et aux garanties procédurales qu'elles prévoient, ni ces dispositions, ni leur entrée en vigueur immédiate, indépendamment de la date d'introduction du recours, ne peuvent être regardées comme portant atteinte au principe d'égalité devant la loi, aux droits de la défense et au droit à un procès équitable garantis par l'article 16 de la Déclaration des droits de l'homme et du citoyen de 1789.<br/>
<br/>
              5. Elles n'affectent pas davantage le droit des requérants de contester un permis de construire, de démolir ou d'aménager ou une décision de non-opposition à déclaration préalable devant le juge de l'excès de pouvoir et d'obtenir qu'une telle décision soit conforme aux lois et règlements applicables. Dès lors, les critiques tirées de ce que l'article L. 600-5-1 du code de l'urbanisme, en ce qu'il conduirait le juge de l'excès de pouvoir à rejeter la requête au vu d'une mesure de régularisation intervenue pendant le délai fixé par le juge, porterait atteinte au droit à un recours juridictionnel effectif et au droit de propriété, garantis par les articles 16 ainsi que 2 et 17 de la Déclaration des droits de l'homme et du citoyen de 1789, ne présentent pas non plus un caractère sérieux. <br/>
<br/>
              6. Au demeurant, la seule circonstance que le juge rejette finalement les conclusions dirigées contre la décision initiale, dont le requérant était fondé à soutenir qu'elle était illégale et dont il est, par son recours, à l'origine de la régularisation, ne devrait pas, pour l'application de l'article L. 761-1 du code de justice administrative, conduire le juge à regarder nécessairement le requérant comme étant, dans l'instance, la partie perdante pour l'essentiel. <br/>
<br/>
              7. Il résulte de ce qui a été dit précédemment que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Il n'y a, dès lors, pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée. <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
				--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil Constitutionnel la question prioritaire de constitutionnalité transmise par le tribunal administratif de Versailles. <br/>
Article 2 : La présente décision sera notifiée au syndicat des copropriétaires de l'ensemble immobilier sis 86 à 94 rue Gutenberg et à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre, à la commune de Palaiseau, à la société civile immobilière INRAA ainsi qu'au tribunal administratif de Versailles.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
