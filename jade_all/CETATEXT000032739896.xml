<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032739896</ID>
<ANCIEN_ID>JG_L_2016_06_000000389592</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/73/98/CETATEXT000032739896.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 20/06/2016, 389592, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-06-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389592</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELVOLVE ET TRICHET</AVOCATS>
<RAPPORTEUR>M. Jean-Philippe Mochon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:389592.20160620</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              L'association Sauvegarde du Trégor a demandé au tribunal administratif de Rennes d'annuler la décision du 31 octobre 2012 par laquelle le préfet des Côtes d'Armor a refusé de lui accorder un agrément départemental au titre de l'article L. 141-1 du code de l'environnement. Par un jugement n° 1200030 et 1300057 du 21 février 2014, le tribunal administratif de Rennes a fait droit à sa demande et agréé l'association au titre de l'article L. 141-1 du code de l'environnement dans le cadre du Trégor historique, de Morlaix à Tréguier.<br/>
<br/>
              Par un arrêt n° 14NT01142 du 13 février 2015, la cour administrative d'appel de Nantes a rejeté l'appel formé par le ministre de l'écologie, du développement durable et de l'énergie contre ce jugement et, faisant droit à l'appel incident formé par l'association Sauvegarde du Trégor, réformé ce jugement en agréant l'association dans le cadre de la région Bretagne.<br/>
<br/>
              Par un pourvoi enregistré le 17 avril 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'écologie, du développement durable et de l'énergie demande au Conseil d'Etat d'annuler cet arrêt. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              -	le code de l'environnement ;<br/>
              -	la loi n° 91-647 du 10 juillet 1991 ;<br/>
              -	le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Philippe Mochon, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delvolvé et Trichet, avocat de l'association Sauvegarde du Trégor ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que le ministre de l'écologie, du développement durable et de l'énergie demande l'annulation de l'arrêt du 13 février 2015 par lequel la cour administrative d'appel de Nantes a délivré à l'association Sauvegarde du Trégor un agrément au titre de l'article L. 141-1 du code de l'environnement, pour une durée de cinq ans renouvelable, dans le cadre de la région Bretagne ;<br/>
<br/>
              2. Considérant qu'en vertu de l'article L. 141-1 du code de l'environnement, dans sa rédaction issue de la loi du 27 décembre 2012 relative à la mise en oeuvre du principe de participation du public défini à l'article 7 de la Charte de l'environnement, les associations qui oeuvrent principalement pour la protection de l'environnement peuvent faire l'objet d'un agrément de l'autorité administrative valable " pour une durée limitée et dans un cadre déterminé en tenant compte du territoire sur lequel l'association exerce effectivement " ses activités ; que les décisions prises en application de ces dispositions sont soumises à un contentieux de pleine juridiction ; que les associations agréées pour la protection de l'environnement au titre de cet article justifient, en vertu de l'article L. 142-1 du même code, d'un intérêt pour agir contre toute décision administrative en rapport avec leur objet et produisant des effets dommageables pour l'environnement et peuvent, sous certaines conditions, être mandatées, en vertu de l'article L. 142-3 du code, par des personnes physiques pour agir en réparation des préjudices qu'elles ont subis à la suite d'infractions à la législation relative à la protection de l'environnement ; qu'aux termes de l'article R. 141-2 du même code : " Une association peut être agréée si, à la date de la demande d'agrément, elle justifie depuis trois ans au moins à compter de sa déclaration : / 1° D'un objet statutaire relevant d'un ou plusieurs domaines mentionnés à l'article L. 141-1 et de l'exercice dans ces domaines d'activités effectives et publiques ou de publications et travaux dont la nature et l'importance attestent qu'elle oeuvre à titre principal pour la protection de l'environnement ; / 2° D'un nombre suffisant, eu égard au cadre territorial de son activité, de membres, personnes physiques, cotisant soit individuellement, soit par l'intermédiaire d'associations fédérées ; / 3° De l'exercice d'une activité non lucrative et d'une gestion désintéressée ; / 4° D'un fonctionnement conforme à ses statuts, présentant des garanties permettant l'information de ses membres et leur participation effective à sa gestion ; / 5° De garanties de régularité en matière financière et comptable " ; qu'aux termes de l'article R. 141-3: " L'agrément est délivré dans un cadre départemental, régional ou national pour une durée de cinq ans renouvelable. Le cadre territorial dans lequel l'agrément est délivré est fonction du champ géographique où l'association exerce effectivement son activité statutaire, sans que cette activité recouvre nécessairement l'ensemble du cadre territorial pour lequel l'association sollicite l'agrément. " ;<br/>
<br/>
              3. Considérant qu'il résulte des dispositions du code de l'environnement citées ci-dessus qu'il incombe à l'autorité administrative, saisie d'une demande d'agrément, de déterminer s'il peut être délivré dans un cadre départemental, régional ou national ; que, si ces dispositions font obstacle à ce qu'elle exige que l'association exerce son activité dans l'ensemble du cadre territorial pour lequel l'agrément est susceptible de lui être délivré, elle peut légalement rejeter la demande lorsque les activités de l'association ne sont pas exercées sur une partie significative de ce cadre territorial et qu'elles ne concernent que des enjeux purement locaux ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède qu'en jugeant que l'autorité administrative ne pouvait légalement tenir compte du fait que l'association Sauvegarde du Trégor n'exerçait pas son activité sur une partie significative du cadre territorial pour lequel l'agrément était susceptible de lui être délivré, la cour administrative d'appel de Nantes a commis une erreur de droit ; que son arrêt doit par suite être annulé ; <br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 relative à l'aide juridique font obstacle à ce que soient mises à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante, les sommes demandées à ce titre ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 13 février 2015 de la cour administrative d'appel de Nantes est annulé.<br/>
<br/>
Article 2 : Les conclusions présentées par l'association Sauvegarde du Trégor au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
<br/>
      Article 3 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat et à l'association Sauvegarde du Trégor. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
