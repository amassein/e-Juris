<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034423667</ID>
<ANCIEN_ID>JG_L_2017_04_000000404818</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/42/36/CETATEXT000034423667.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 13/04/2017, 404818, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-04-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404818</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Christophe Pourreau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:404818.20170413</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par un mémoire et un mémoire complémentaire, enregistrés les 31 janvier et 28 février 2017 au secrétariat du contentieux du Conseil d'Etat, présenté en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, l'Union nationale des industries de carrières et matériaux de construction (UNICEM) et l'Union nationale des producteurs de granulats (UNPG) demandent au Conseil d'Etat, à l'appui de leur requête formée contre le décret n° 2016-1190 du 31 août 2016 relatif à l'étude préalable et aux mesures de compensation prévues à l'article L. 112-1-3 du code rural et de la pêche maritime, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions de ce même article. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de l'Union nationale des industries de carrières et matériaux de construction  et de l'Union nationale des producteurs de granulats ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 28 mars 2017, présentée par l'UNICEM et l'UNPG ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Le Syndicat français de l'industrie cimentière doit être regardé, en l'état du dossier, comme justifiant d'un intérêt lui donnant qualité pour intervenir au soutien de la requête de l'UNICEM et de l'UNPG. Dès lors, son intervention au soutien de la question prioritaire de constitutionnalité soulevée par celles-ci, présentée par un mémoire distinct, est recevable.<br/>
<br/>
              3. Aux termes de l'article L. 112-1-3 du code rural et de la pêche maritime : " Les projets de travaux, d'ouvrages ou d'aménagements publics et privés qui, par leur nature, leurs dimensions ou leur localisation, sont susceptibles d'avoir des conséquences négatives importantes sur l'économie agricole font l'objet d'une étude préalable comprenant au minimum une description du projet, une analyse de l'état initial de l'économie agricole du territoire concerné, l'étude des effets du projet sur celle-ci, les mesures envisagées pour éviter et réduire les effets négatifs notables du projet ainsi que des mesures de compensation collective visant à consolider l'économie agricole du territoire. / L'étude préalable et les mesures de compensation sont prises en charge par le maître d'ouvrage. / Un décret détermine les modalités d'application du présent article, en précisant, notamment, les projets de travaux, d'ouvrages ou d'aménagements publics et privés qui doivent faire l'objet d'une étude préalable ".<br/>
<br/>
              4. L'UNICEM et l'UNPG soutiennent que les dispositions de l'article L. 112-1-3 du code rural et de la pêche maritime, en tant qu'elles prévoient la prise en charge par le maître d'ouvrage de mesures de compensation collective, méconnaissent le principe constitutionnel de liberté d'entreprendre, garanti par l'article 4 de la Déclaration des droits de l'homme et du citoyen, ainsi que l'objectif de valeur constitutionnelle d'intelligibilité et d'accessibilité de la loi, et qu'elles sont entachées d'incompétence négative.<br/>
<br/>
              5. D'une part, aux termes de l'article 4 de la Déclaration des droits de l'homme et du citoyen : " La liberté consiste à pouvoir faire tout ce qui ne nuit pas à autrui ; ainsi, l'exercice des droits naturels de chaque homme n'a de bornes que celles qui assurent aux autres membres de la société la jouissance de ces mêmes droits. Ces bornes ne peuvent être déterminées que par la loi ". Il est loisible au législateur d'apporter à la liberté d'entreprendre, qui découle de l'article 4 de la Déclaration des droits de l'homme et du citoyen, des limitations liées à des exigences constitutionnelles ou justifiées par l'intérêt général, à la condition qu'il n'en résulte pas une atteinte disproportionnée au regard de l'objectif poursuivi. En adoptant les dispositions citées au point 3, le législateur a entendu réduire les effets négatifs notables des projets de travaux, d'ouvrages ou d'aménagements mentionnés à l'article L. 112-1-3 du code rural et de la pêche maritime et préserver l'économie agricole, en particulier le foncier agricole, face à l'utilisation croissante de terres. Il a ainsi poursuivi un but d'intérêt général. Il résulte, en outre, des travaux préparatoires que le législateur n'a assorti d'aucune sanction l'éventuelle méconnaissance par un maître d'ouvrage des mesures de compensation prescrites à ce titre, et n'a pas fait du respect de ces dernières une condition de la délivrance d'une autorisation se rapportant aux projets soumis à l'obligation d'une étude préalable. Eu égard aux objectifs qu'il s'est assignés, le législateur a ainsi adopté des mesures propres à assurer une conciliation qui n'est pas manifestement déséquilibrée entre le principe de la liberté d'entreprendre et l'intérêt général. Dès lors, les dispositions contestées ne portent pas atteinte au principe de la liberté d'entreprendre.<br/>
<br/>
              6. D'autre part, l'UNICEM et l'UNPG soutiennent que la méconnaissance, par le législateur, de l'étendue de sa compétence et de l'objectif à valeur constitutionnel d'intelligibilité et d'accessibilité de la loi porte atteinte à la liberté d'entreprendre. Compte-tenu toutefois de la diversité des mesures envisageables permettant de compenser les divers effets d'un projet sur l'économie agricole d'un territoire, il n'appartenait pas au législateur de préciser davantage, au regard du principe de liberté d'entreprendre invoqué, le contenu de ces mesures, ni leurs destinataires. En outre, la méconnaissance de l'objectif de valeur constitutionnelle d'intelligibilité et d'accessibilité de la loi ne peut, en elle-même, être invoquée à l'appui d'une question prioritaire de constitutionnalité présentée sur le fondement de l'article 61-1 de la Constitution.<br/>
<br/>
              7. Il résulte de ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Par suite, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité, soulevée par l'UNICEM et l'UNPG, et tirée de la méconnaissance par les dispositions de l'article L. 112-1-3 du code rural et de la pêche maritime des droits et libertés garantis par la Constitution.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'intervention du Syndicat français de l'industrie cimentière est admise.<br/>
Article 2 : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par l'UNICEM et l'UNPG.<br/>
Article 3 : La présente décision sera notifiée à l'Union nationale des industries de carrières et matériaux de construction (UNICEM), à l'Union nationale des producteurs de granulats (UNPG) et au ministre de l'agriculture, de l'agroalimentaire et de la forêt.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
