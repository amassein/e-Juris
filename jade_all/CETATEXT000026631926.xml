<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026631926</ID>
<ANCIEN_ID>JG_L_2012_11_000000353092</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/63/19/CETATEXT000026631926.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème sous-section jugeant seule, 14/11/2012, 353092, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-11-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353092</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Rémy Schwartz</PRESIDENT>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Nuttens</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:353092.20121114</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 3 octobre 2011 et 3 janvier 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Carlos B, demeurant ... ; M. B demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10PA05393 du 25 mai 2011 par lequel la cour administrative d'appel de Paris a, d'une part, annulé le jugement du 5 octobre 2010 par lequel le tribunal administratif de Paris a annulé l'arrêté du 25 février 2010 du préfet de police de Paris lui refusant la délivrance d'un titre de séjour et l'obligeant à quitter le territoire français et enjoint au préfet de police de Paris de lui délivrer un titre de séjour dans le délai de trois mois à compter de la notification du jugement, d'autre part, rejeté toutes ses demandes ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
<br/>
              Vu l'arrêté interministériel du 18 janvier 2008 relatif à la délivrance, sans opposition de la situation de l'emploi, des autorisations de travail aux étrangers non ressortissants d'un Etat membre de l'Union européenne, d'un autre Etat partie à l'Espace économique européen ou de la Confédération suisse ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Nuttens, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Barthélemy, Matuchansky, Vexliard, avocat de M. B,<br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Barthélemy, Matuchansky, Vexliard, avocat de M. B ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 313-14 du code de l'entrée et du séjour des étrangers et du droit d'asile dans sa version applicable au litige : " La carte de séjour temporaire mentionnée à l'article L. 313-11 ou la carte de séjour temporaire mentionnée au 1° de l'article L. 313-10  peut être délivrée, sauf si sa présence constitue une menace pour l'ordre public, à l'étranger ne vivant pas en état de polygamie dont l'admission au séjour répond à des considérations humanitaires ou se justifie au regard des motifs exceptionnels qu'il fait valoir, sans que soit opposable la condition prévue à l'article L. 311-7 " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B, de nationalité péruvienne, est entré en France, selon ses déclarations, le 24 février 2003 ; que, par un arrêté du 27 février 2010, le préfet de police de Paris a rejeté sa demande d'admission exceptionnelle au séjour, présentée sur le fondement de l'article L. 313-14 du code de l'entrée et du séjour des étrangers et du droit d'asile, et l'a obligé à quitter le territoire français dans le délai d'un mois à compter de la notification de la décision ; que, par un arrêt du 25 mai 2011 contre lequel M. B se pourvoit en cassation, la cour administrative d'appel de Paris a annulé le jugement du 5 octobre 2010 par lequel le tribunal administratif de Paris a annulé cet arrêté et a enjoint au préfet de police de Paris de lui délivrer un titre de séjour dans le délai de trois mois à compter de la notification du jugement ;<br/>
<br/>
              Sur la motivation de l'arrêt attaqué :<br/>
<br/>
              3. Considérant qu'en relevant que M. B était sans charge de famille ni famille proche en France, qu'il avait vécu au moins jusqu'à l'âge de trente-neuf ans au Pérou, où il a conservé toutes ses attaches familiales, en particulier sa femme et leurs deux enfants, et qu'il n'établit  l'existence d'aucune circonstance particulière nécessitant son maintien sur le territoire français, où il réside irrégulièrement, la cour administrative d'appel de Paris, qui n'était pas tenue de répondre à tous les arguments avancés devant elle par M. B, a, pour écarter le moyen tiré de ce que l'arrêté du préfet de police aurait porté à son droit au respect de sa vie privée et familiale une atteinte disproportionnée au regard des motifs en vue desquels il a été pris, suffisamment motivé son arrêt ; que, contrairement à ce que soutient M. B, la cour, en relevant que sa demande avait été présentée au titre de l'article L. 313-14 du code de l'entrée et du séjour des étrangers et du droit d'asile, et non de l'article L. 313-10, a précisément répondu au moyen tiré de l'absence de saisine par le préfet des services du ministère du travail préalablement à sa décision ; que le moyen tiré de ce que la cour aurait insuffisamment motivé l'arrêt attaqué ne peut, dès lors, qu'être écarté ;<br/>
<br/>
              Sur le bien fondé de l'arrêt attaqué :<br/>
<br/>
              4. Considérant qu'en présence d'une demande de régularisation présentée, sur le fondement de l'article L. 313-14 du code de l'entrée et du séjour des étrangers et du droit d'asile, par un étranger qui ne serait pas en situation de polygamie et dont la présence en France ne présenterait pas une menace pour l'ordre public, il appartient à l'autorité administrative de vérifier, dans un premier temps, si l'admission exceptionnelle au séjour par la délivrance d'une carte portant la mention " vie privée et familiale " répond à des considérations humanitaires ou se justifie au regard de motifs exceptionnels, et à défaut, dans un second temps, s'il est fait état de motifs exceptionnels de nature à permettre la délivrance, dans ce cadre, d'une carte de séjour temporaire portant la mention " salarié " ou " travailleur temporaire " ; que dans cette dernière hypothèse, un demandeur qui justifierait d'une promesse d'embauche ou d'un contrat lui permettant d'exercer une activité figurant dans la liste annexée à l'arrêté interministériel du 18 janvier 2008, ne saurait être regardé, par principe, comme attestant, par là même, des " motifs exceptionnels " exigés par la loi ; qu'il appartient, en effet, à l'autorité administrative, sous le contrôle du juge, d'examiner, notamment, si la qualification, l'expérience et les diplômes de l'étranger ainsi que les caractéristiques de l'emploi auquel il postule, dans un métier et une zone géographique caractérisés par des difficultés de recrutement et recensés comme tels dans l'arrêté du 18 janvier 2008, de même que tout élément de sa situation personnelle dont l'étranger ferait état à l'appui de sa demande, tel que l'ancienneté de son séjour en France, peuvent constituer, en l'espèce, des motifs exceptionnels d'admission au séjour ;<br/>
<br/>
              5. Considérant, en premier lieu, que c'est sans dénaturer les faits de l'espèce ni méconnaître les dispositions de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre le public et l'administration que la cour administrative d'appel de Paris a estimé que l'arrêté du 25 février 2010 du préfet de police répondait, par sa motivation, aux exigences de ces dispositions dès lors que le préfet avait relevé, d'une part, que M. B avait sollicité son admission au séjour en tant que salarié au titre de l'article L. 313-14 du code de l'entrée et du séjour des étrangers et du droit d'asile et, qu'à ce titre, sa demande ne répondait ni à des considérations humanitaires, ni à des motifs exceptionnels, d'autre part, que, dépourvu de titre de séjour, l'intéressé ne pouvait prétendre à la délivrance d'une carte de séjour temporaire sur le fondement du 1° de l'article <br/>
L. 313-10 du même code ;<br/>
<br/>
              6. Considérant, en deuxième lieu, qu'ainsi qu'il a été dit plus haut, la condition relative à l'existence de motifs exceptionnels d'admission au séjour posée par l'article L. 313-14 du code de l'entrée et du séjour des étrangers et du droit d'asile ne saurait être regardée, par principe, comme remplie par un demandeur du seul fait que celui-ci a exercé ou exerce un " métier sous tension " figurant à l'annexe de l'arrêté interministériel du 18 janvier 2008 et qu'il réside en France depuis plusieurs années ; qu'il incombe au contraire à l'autorité administrative d'examiner, notamment, la qualification, l'expérience et les diplômes de l'étranger ainsi que les caractéristiques de l'emploi auquel il postule pour déterminer si l'ensemble de ces éléments, joints à ceux relatifs à sa situation personnelle dont l'intéressé fait état, peuvent constituer, dans son cas, de tels motifs exceptionnels ; que, dès lors, la cour administrative d'appel de Paris a jugé à bon droit que le tribunal administratif de Paris, en retenant seulement, comme éléments constituant un motif exceptionnel d'admission au séjour, que le préfet de police devait prendre en compte sous peine d'erreur manifeste d'appréciation, la circonstance que M. B exerçait les fonctions de chef de chantier et l'ancienneté de son séjour en France, a commis une erreur de droit ;<br/>
<br/>
              7. Considérant, en troisième lieu, qu'ainsi qu'il a été dit, l'admission exceptionnelle au séjour est possible sur le fondement de l'article L. 313-14 du code de l'entrée et du séjour des étrangers et du droit d'asile, en l'absence de motifs humanitaires, lorsqu'à l'issue de l'examen approfondi auquel elle procède, dans les conditions rappelées ci-dessus, de la situation de l'intéressé, l'autorité administrative estime que la qualification, l'expérience et les diplômes de l'étranger, ainsi que les caractéristiques de l'emploi auquel il postule, notamment au regard du marché du travail et de la rareté des candidats aptes à exercer certains métiers qui sont recensés dans l'annexe à l'arrêté interministériel du 18 janvier 2008, constituent un motif exceptionnel d'admission au séjour ; qu'il appartient à cette autorité, pour porter cette appréciation, de rechercher l'ensemble des éléments de fait susceptibles de l'éclairer sur la réalité des motifs invoqués par le demandeur pour justifier sa demande, et au juge saisi d'un recours contre une décision de refus, de s'assurer que l'appréciation ainsi portée n'est pas manifestement erronée ; que, par suite, c'est sans commettre d'erreur de droit qu'après avoir énoncé, d'une part, que la circonstance, relevée par le préfet de police, que M. B ne justifiait d'aucune expérience professionnelle en France en tant que chef de chantier, n'était pas contestée, d'autre part, qu'il n'était ni établi, ni même allégué que l'employeur aurait recherché en vain une personne susceptible d'occuper l'emploi de chef de chantier dont se prévaut l'intéressé, la cour en a déduit que ce dernier ne pouvait prétendre, par la seule production d'une promesse d'embauche dans le secteur du bâtiment, justifier de motifs exceptionnels de nature à le faire admettre au séjour ;<br/>
<br/>
              8. Considérant, en dernier lieu, que,  contrairement à ce que soutient M. B, lequel, âgé de quarante-six ans à la date de la décision attaquée, a vécu jusqu'à l'âge de trente-neuf ans au Pérou, où résident son épouse et ses enfants, la cour n'a pas inexactement qualifié les faits de l'espèce en jugeant que l'arrêté du 25 février 2010 du préfet de police refusant à ce dernier l'admission au séjour et l'obligeant à quitter le territoire français n'avait pas méconnu les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales en portant à son droit au respect de sa vie privée et familiale une atteinte disproportionnée au but poursuivi ;<br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que M. B n'est pas fondé à demander l'annulation de l'arrêt du 25 mai 2011 de la cour administrative d'appel de Paris ; que, par suite, son pourvoi doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. Carlos B et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
