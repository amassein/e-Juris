<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029926606</ID>
<ANCIEN_ID>JG_L_2014_12_000000368294</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/92/66/CETATEXT000029926606.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section du Contentieux, 19/12/2014, 368294, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-12-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368294</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section du Contentieux</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Natacha Chicot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2014:368294.20141219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 6 mai et 6 août 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Propriano, représentée par son maire ; la commune de Propriano demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11MA02033 du 5 mars 2013 par lequel la cour administrative d'appel de Marseille, sur la requête de M. A..., tendant, d'une part, à l'annulation du jugement n° 1000201 du 24 mars 2011 par lequel le tribunal administratif de Bastia a rejeté sa demande tendant à l'annulation de la décision implicite de la commune de Propriano rejetant sa demande indemnitaire préalable ainsi qu'à la condamnation de la commune à lui payer la somme de 70 000 euros, en réparation des préjudices subis en raison de l'absence de mise à disposition d'un poste d'amarrage sur lequel il disposait d'une garantie d'usage en application d'un contrat du 14 octobre 2003, assortie des intérêts de droit à compter de la demande préalable et de leur capitalisation, d'autre part, à ce qu'il soit fait droit à sa demande de première instance, a, en premier lieu, annulé le jugement du tribunal administratif de Bastia du 24 mars 2011, en deuxième lieu, annulé la décision implicite par laquelle elle a rejeté la réclamation indemnitaire de M. A... et, en dernier lieu, l'a condamnée à payer à M. A... la somme de 55 000 euros, assortie des intérêts au taux légal à compter du 10 novembre 2009, capitalisés au 10 novembre 2010 et à chaque échéance annuelle ;<br/>
<br/>
              2°) de mettre à la charge de M. A... le versement de la somme de 7 000 euros au titre de l'article L. 761-1 du code de justice administrative ainsi que de la somme de 35 euros en application des dispositions de l'article R. 761-1 du même code ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Natacha Chicot, auditeur,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la commune de Propriano et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par convention de délégation de service public du 5 mars 2003, la commune de Propriano a confié à la société Yacht club international du Valinco la construction et l'exploitation de son port de plaisance ; que, par délibération du 4 septembre 2007, la commune de Propriano a, sur le fondement de l'article 54 de la convention de délégation de service public, résilié cette convention à raison des fautes commises par le délégataire ; que, par jugement du 24 mars 2011, le tribunal administratif de Bastia a rejeté la demande de M. A..., usager du port de plaisance, tendant à ce que la commune de Propriano soit condamnée à l'indemniser du préjudice né de l'inexécution du contrat de garantie d'usage d'un poste d'amarrage de longue durée qu'il avait conclu avec la société Yacht club international du Valinco le 14 octobre 2003 ; que la commune de Propriano se pourvoit en cassation contre l'arrêt du 5 mars 2013 par lequel la cour administrative d'appel de Marseille a annulé ce jugement et fait droit à la demande de M. A... ;<br/>
<br/>
              2. Considérant, sans préjudice des dispositions législatives applicables notamment en matière de transfert de contrat de travail, qu'en cas de résiliation d'un contrat portant exécution d'un service public, quel qu'en soit le motif, la personne publique, à laquelle il appartient de garantir la continuité du service public et son bon fonctionnement, se substitue de plein droit à son ancien cocontractant pour l'exécution des contrats conclus avec les usagers ou avec d'autres tiers pour l'exécution même du service ; qu'il n'en va toutefois ainsi que si les contrats en cause ne comportent pas d'engagements anormalement pris, c'est-à-dire des engagements qu'une interprétation raisonnable du contrat relatif à l'exécution d'un service public ne permettait pas de prendre au regard notamment de leur objet, de leurs conditions d'exécution ou de leur durée, à moins que, dans ce cas, la personne publique n'ait donné, dans le respect de la réglementation applicable,  son accord à leur conclusion ; que, pour l'application de ces règles, la substitution de la personne publique n'emporte pas le transfert des dettes et créances nées de l'exécution antérieure des contrats conclus par l'ancien cocontractant de la personne publique, qu'il s'agisse des contrats conclus avec les usagers du service public ou de ceux conclus avec les autres tiers ;<br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède qu'en jugeant, d'une part, que la commune de Propriano était tenue de venir aux droits de la société Yacht Club international de Valinco dans l'ensemble de ses rapports avec les usagers du port dès lors que la délégation de service public avait été résiliée et que, d'autre part, la commune ne pouvait, par suite, utilement se prévaloir de ce que le contrat conclu entre le requérant et cette société ne pouvait être regardé comme un engagement normalement pris pour l'exploitation du service public, la cour administrative d'appel de Marseille a commis une erreur de droit ; qu'il s'ensuit, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la commune de Propriano est fondée à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant que la commune de Propriano soutient que le contrat dont se prévaut M. A... ne peut pas être regardé comme un engagement normalement pris au sens des règles mentionnées au point 2, que rappellent d'ailleurs l'article 54 de la convention de délégation de service public, au motif que le montant de la redevance versée par M. A... ne correspondait pas aux conditions tarifaires prévues à l'article 32 de la convention de délégation de service public renvoyant à l'annexe V de cette convention ; qu'il résulte cependant de l'instruction que le contrat conclu par le requérant portait sur un emplacement de treize mètres et que si la société délégataire devait assurer l'exploitation d'emplacements de cette taille, la grille tarifaire figurant à l'annexe V mentionnait les tarifs applicables aux emplacements de douze et quatorze mètres mais pas ceux applicables à de tels emplacements de treize mètres ; qu'en prévoyant ainsi, par le contrat litigieux, le versement d'une redevance calculée en fonction des tarifs prévus pour les emplacements de douze et quatorze mètres, la société délégataire, qui s'est bornée à tirer les conséquences d'une grille tarifaire incomplète, n'a pas conclu un engagement qu'une interprétation raisonnable de la convention de délégation de service public ne lui permettait pas de prendre ;<br/>
<br/>
              6. Considérant, toutefois, qu'en application de l'article 30-2 la convention de délégation de service public conclue entre la commune de Propriano et la société Yacht club international du Valinco, la garantie d'usage de postes d'amarrage ou de mouillage accordée à une personne physique ou morale est donnée pour le seul accès à un poste dans une zone déterminée du port sans pouvoir permettre l'affectation privative d'un ou plusieurs postes déterminés ; qu'il résulte de l'article 1er du contrat conclu entre M. A... et la société Yacht club international du Valinco que son objet réside non dans l'octroi d'une garantie d'usage permettant à son bénéficiaire d'accéder à un poste d'amarrage correspondant aux dimensions de son bateau dans une zone déterminée du port de plaisance, conformément à la réglementation posée par l'article 30-2 précité de la convention de délégation, mais dans l'affectation privative d'un poste d'amarrage précisément localisé ; qu'il s'ensuit que le contrat conclu entre M. A... et la société Yacht club international du Valinco ne peut être regardé comme un engagement que la société délégataire pouvait normalement prendre ; que la commune de Propriano était, par suite, fondée à refuser de se substituer à la société Yacht club international du Valinco pour poursuivre l'exécution du contrat conclu avec M. A... ; que, dès lors, ce dernier n'est pas fondé à se plaindre de ce que, par le jugement attaqué, le tribunal administratif de Bastia a rejeté ses conclusions indemnitaires dirigées contre la commune de Propriano, lesquelles étaient présentées sur le seul terrain contractuel ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 font obstacle à ce que les sommes demandées par M. A... soient mises à la charge de la commune de Propriano qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu non plus, dans les circonstances de l'espèce, de faire droit aux conclusions de la commune de Propriano présentées sur le fondement des mêmes dispositions ; que, dans les circonstances de l'espèce, il y a lieu de laisser la contribution pour l'aide juridique à la charge de la commune de Propriano ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 5 mars 2013 est annulé.<br/>
<br/>
Article 2 : La requête présentée par M. A... devant la cour administrative d'appel de Marseille est rejetée.<br/>
<br/>
Article 3 : La contribution pour l'aide juridique est laissée à la charge de la commune de Propriano. <br/>
<br/>
Article 4 : Les conclusions présentées par les parties sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la commune de Propriano et à M. B... A....<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. CONTINUITÉ DU SERVICE PUBLIC. - RÉSILIATION D'UN CONTRAT PORTANT EXÉCUTION D'UN SERVICE PUBLIC - 1) GARANTIE DE LA CONTINUITÉ DU SERVICE PUBLIC - SUBSTITUTION DE LA PERSONNE PUBLIQUE DANS LES CONTRATS PASSÉS PAR LE COCONTRACTANT AVEC LES USAGERS ET LES AUTRES TIERS POUR L'EXÉCUTION MÊME DU SERVICE - 2) LIMITES - A) ENGAGEMENTS ANORMALEMENT PRIS, SAUF SI LA PERSONNE PUBLIQUE AVAIT DONNÉ SON ACCORD [RJ1] - B) ABSENCE DE TRANSFERT DES DETTES ET CRÉANCES NÉES DE L'EXÉCUTION ANTÉRIEURE DE CES CONTRATS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">135-01-04 COLLECTIVITÉS TERRITORIALES. DISPOSITIONS GÉNÉRALES. SERVICES PUBLICS LOCAUX. - RÉSILIATION D'UN CONTRAT PORTANT EXÉCUTION D'UN SERVICE PUBLIC - 1) GARANTIE DE LA CONTINUITÉ DU SERVICE PUBLIC - SUBSTITUTION DE LA PERSONNE PUBLIQUE DANS LES CONTRATS PASSÉS PAR LE COCONTRACTANT AVEC LES USAGERS ET LES AUTRES TIERS POUR L'EXÉCUTION MÊME DU SERVICE - 2) LIMITES - A) ENGAGEMENTS ANORMALEMENT PRIS, SAUF SI LA PERSONNE PUBLIQUE AVAIT DONNÉ SON ACCORD [RJ1] - B) ABSENCE DE TRANSFERT DES DETTES ET CRÉANCES NÉES DE L'EXÉCUTION ANTÉRIEURE DE CES CONTRATS.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">39-04-02-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. FIN DES CONTRATS. RÉSILIATION. EFFETS. - RÉSILIATION D'UN CONTRAT PORTANT EXÉCUTION D'UN SERVICE PUBLIC - 1) GARANTIE DE LA CONTINUITÉ DU SERVICE PUBLIC - SUBSTITUTION DE LA PERSONNE PUBLIQUE DANS LES CONTRATS PASSÉS PAR LE COCONTRACTANT AVEC LES USAGERS ET LES AUTRES TIERS POUR L'EXÉCUTION MÊME DU SERVICE - 2) LIMITES - A) ENGAGEMENTS ANORMALEMENT PRIS, SAUF SI LA PERSONNE PUBLIQUE AVAIT DONNÉ SON ACCORD [RJ1] - B) ABSENCE DE TRANSFERT DES DETTES ET CRÉANCES NÉES DE L'EXÉCUTION ANTÉRIEURE DE CES CONTRATS.
</SCT>
<ANA ID="9A"> 01-04-03-07-01 1) Sans préjudice des dispositions législatives applicables notamment en matière de transfert de contrat de travail, en cas de résiliation d'un contrat portant exécution d'un service public, quel qu'en soit le motif, la personne publique, à laquelle il appartient de garantir la continuité du service public et son bon fonctionnement, se substitue de plein droit à son ancien cocontractant pour l'exécution des contrats conclus avec les usagers ou avec d'autres tiers pour l'exécution même du service.... ,,2) a) Il n'en va toutefois ainsi que si les contrats en cause ne comportent pas d'engagements anormalement pris, c'est-à-dire des engagements qu'une interprétation raisonnable du contrat relatif à l'exécution d'un service public ne permettait pas de prendre au regard notamment de leur objet, de leurs conditions d'exécution ou de leur durée, à moins que, dans ce cas, la personne publique n'ait donné, dans le respect de la réglementation applicable,  son accord à leur conclusion.... ,,b) Pour l'application de ces règles, la substitution de la personne publique n'emporte pas le transfert des dettes et créances nées de l'exécution antérieure des contrats conclus par l'ancien cocontractant de la personne publique, qu'il s'agisse des contrats conclus avec les usagers du service public ou de ceux conclus avec les autres tiers.</ANA>
<ANA ID="9B"> 135-01-04 1) Sans préjudice des dispositions législatives applicables notamment en matière de transfert de contrat de travail, en cas de résiliation d'un contrat portant exécution d'un service public, quel qu'en soit le motif, la personne publique, à laquelle il appartient de garantir la continuité du service public et son bon fonctionnement, se substitue de plein droit à son ancien cocontractant pour l'exécution des contrats conclus avec les usagers ou avec d'autres tiers pour l'exécution même du service.... ,,2) a) Il n'en va toutefois ainsi que si les contrats en cause ne comportent pas d'engagements anormalement pris, c'est-à-dire des engagements qu'une interprétation raisonnable du contrat relatif à l'exécution d'un service public ne permettait pas de prendre au regard notamment de leur objet, de leurs conditions d'exécution ou de leur durée, à moins que, dans ce cas, la personne publique n'ait donné, dans le respect de la réglementation applicable,  son accord à leur conclusion.... ,,b) Pour l'application de ces règles, la substitution de la personne publique n'emporte pas le transfert des dettes et créances nées de l'exécution antérieure des contrats conclus par l'ancien cocontractant de la personne publique, qu'il s'agisse des contrats conclus avec les usagers du service public ou de ceux conclus avec les autres tiers.</ANA>
<ANA ID="9C"> 39-04-02-02 1) Sans préjudice des dispositions législatives applicables notamment en matière de transfert de contrat de travail, en cas de résiliation d'un contrat portant exécution d'un service public, quel qu'en soit le motif, la personne publique, à laquelle il appartient de garantir la continuité du service public et son bon fonctionnement, se substitue de plein droit à son ancien cocontractant pour l'exécution des contrats conclus avec les usagers ou avec d'autres tiers pour l'exécution même du service.... ,,2) a) Il n'en va toutefois ainsi que si les contrats en cause ne comportent pas d'engagements anormalement pris, c'est-à-dire des engagements qu'une interprétation raisonnable du contrat relatif à l'exécution d'un service public ne permettait pas de prendre au regard notamment de leur objet, de leurs conditions d'exécution ou de leur durée, à moins que, dans ce cas, la personne publique n'ait donné, dans le respect de la réglementation applicable,  son accord à leur conclusion.... ,,b) Pour l'application de ces règles, la substitution de la personne publique n'emporte pas le transfert des dettes et créances nées de l'exécution antérieure des contrats conclus par l'ancien cocontractant de la personne publique, qu'il s'agisse des contrats conclus avec les usagers du service public ou de ceux conclus avec les autres tiers.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, 16 juin 1922, Compagnie générale des eaux c/ ministre de la marine et ville de Toulon, n° 66707, 67094, p. 521 ; CE, 24 mars 1926, Compagnie générale des eaux c/ ville de Lyon, n° 79424, p. 327.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
