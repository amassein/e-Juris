<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027149394</ID>
<ANCIEN_ID>JG_L_2013_03_000000355815</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/14/93/CETATEXT000027149394.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 06/03/2013, 355815, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-03-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>355815</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Charles Touboul</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:355815.20130306</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée le 13 janvier 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par la société NAVX, dont le siège est 120, rue Jean-Jaurès à Levallois-Perret (92300), représentée par son représentant légal ; la société NAVX demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'article 22 du décret n° 2012-3 du 3 janvier 2012 portant diverses mesures de sécurité routière ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu la note en délibéré, enregistrée le 21 février 2013, présentée par la société NAVX ;<br/>
<br/>
              Vu la Constitution ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son premier protocole additionnel ;<br/>
<br/>
              Vu le code pénal ;<br/>
<br/>
              Vu le code de la route ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles Touboul, Maître des Requêtes,  <br/>
<br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que, par ses quatre premiers paragraphes, l'article R. 413-15 du code de la route prévoit que le fait de détenir, transporter ou utiliser " un appareil, dispositif ou produit de nature ou présenté comme étant de nature à déceler la présence ou perturber le fonctionnement d'appareils, instruments ou systèmes servant à la constatation des infractions à la législation ou à la réglementation de la circulation routière ou de permettre de se soustraire à la constatation desdites infractions " est puni de l'amende prévue pour les contraventions de la cinquième classe, que l'appareil, dispositif ou produit est saisi puis, en cas de condamnation, confisqué, que le véhicule sur lequel il est placé, adapté ou appliqué peut être saisi et que les peines complémentaires de la suspension du permis de conduire pour trois ans au plus et de la confiscation du véhicule sont encourues ; que, par son article 22, dont la société NAVX demande l'annulation pour excès de pouvoir, le décret du 3 janvier 2012 a notamment introduit à l'article R. 413-15 un paragraphe V aux termes duquel : " Les dispositions du présent article sont également applicables aux  dispositifs et produits visant à avertir ou informer de la localisation d'appareils, instruments ou systèmes servant à la constatation des infractions à la législation ou à la réglementation de la circulation routière " ; <br/>
<br/>
              Sur le moyen tiré d'une violation des dispositions de l'article 34 de la Constitution : <br/>
<br/>
              2. Considérant qu'aux termes de l'article 34 de la Constitution : " La loi fixe les règles concernant : / les droits civiques et les garanties fondamentales accordées aux citoyens pour l'exercice des libertés publiques ; (...) la détermination des crimes et délits ainsi que les peines qui leur sont applicables " ; qu'aux termes de l'article 37 de la Constitution : " Les matières autres que celles qui sont du domaine de la loi ont un caractère réglementaire... " ; <br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que la détermination des contraventions et des peines qui leur sont applicables ressortit à la compétence du pouvoir réglementaire ; qu'en incriminant le fait de détenir, transporter ou utiliser les appareils et produits visant à avertir ou informer de la localisation d'appareils, instruments ou systèmes servant à la constatation des infractions à la législation ou à la réglementation de la circulation routière, le Premier ministre n'a pas réglementé l'exercice d'une liberté publique mais s'est borné à y apporter une limitation dans l'exercice de son pouvoir de prendre des mesures de police nécessaires à la sauvegarde de l'ordre public et applicables sur l'ensemble du territoire national ; que, ce faisant, il n'a pas empiété sur le domaine de la loi ;<br/>
<br/>
              Sur le moyen tiré d'une violation des stipulations de l'article 10 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales :<br/>
<br/>
              4. Considérant qu'aux termes de l'article 10 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " 1. Toute personne a droit à la liberté d'expression. Ce droit comprend la liberté d'opinion et la liberté de recevoir ou de communiquer des informations ou des idées sans qu'il puisse y avoir ingérence d'autorités publiques et sans considération de frontière (...) / 2. L'exercice de ces libertés comportant des devoirs et des responsabilités peut être soumis à certaines formalités, conditions, restrictions ou sanctions prévues par la loi, qui constituent des mesures nécessaires, dans une société démocratique, à la sécurité nationale, à l'intégrité territoriale ou à la sûreté publique, à la défense de l'ordre et à la prévention du crime, à la protection de la santé ou de la morale, à la protection de la réputation ou des droits d'autrui, pour empêcher la divulgation d'informations confidentielles ou pour garantir l'autorité et l'impartialité du pouvoir judiciaire " ; qu'il résulte de ces stipulations que si la liberté de recevoir ou de communiquer des informations peut être soumise à des formalités, conditions, restrictions ou sanctions, de telles mesures doivent être " prévues par la loi ", être justifiées par la poursuite de certains objectifs, au nombre desquels figurent la sûreté publique et la défense de l'ordre, et être proportionnées à ces objectifs ; <br/>
<br/>
              5. Considérant, en premier lieu, que le terme de loi, au sens des stipulations précitées, peut s'entendre de dispositions réglementaires régulièrement publiées ; qu'ainsi, la société requérante n'est pas fondée à soutenir que le Premier ministre aurait méconnu l'article 10 de la convention en prenant par décret une mesure restreignant la liberté de communication ; <br/>
<br/>
              6. Considérant, en deuxième lieu, que pour respecter les stipulations de l'article 10, les mesures restreignant la liberté de communiquer ou de recevoir des informations doivent résulter de dispositions suffisamment claires et précises pour permettre aux intéressés de régler leur conduite et de prévoir les conséquences de celle-ci ; que les dispositions du V de l'article R. 413-15 du code de la route introduites par l'article 22 du décret attaqué, instituent en termes clairs et précis une nouvelle contravention visant la détention, le transport et l'usage des dispositifs et produits destinés à " avertir ou informer de la localisation d'appareils, instruments ou systèmes servant à la constatation des infractions à la législation ou à la réglementation de la circulation routière ", couramment désignés comme des " avertisseurs de radars " ; qu'ainsi, contrairement à ce que soutient la société requérante, l'exigence de " prévisibilité de la norme " découlant des stipulations de l'article 10 de la convention n'a pas été méconnue ;<br/>
<br/>
              7. Considérant, en troisième lieu, que les dispositions du V de l'article R. 413-15 du code de la route ne prohibent pas le fait d'avertir ou d'informer de la localisation d'appareils, instruments ou systèmes servant à la constatation des infractions à la législation ou à la réglementation de la circulation routière mais uniquement la détention, le transport et l'usage des dispositifs et produits ayant spécifiquement cette fonction ; que, comme cela ressort des énonciations d'un protocole d'accord du 28 juillet 2011 conclu avec les professionnels du secteur, l'objectif poursuivi par les pouvoirs publics est que l'ensemble des outils d'aide à la conduite n'indiquent plus la localisation des radars fixes ou mobiles mais seulement des " sections de voies dangereuses " qui pourront comporter ou non des radars ; que les adaptations rendues nécessaires par la mesure d'interdiction ont été entreprises par la plupart des sociétés du secteur, y compris à l'attention des personnes disposant déjà de tels dispositifs ou produits ; que cette mesure, compte tenu des impératifs de sécurité des conducteurs de véhicules automobiles et des personnes transportées ainsi que de l'utilité des radars pour inciter les conducteurs à maîtriser leur vitesse de circulation, ne porte pas à la liberté de communiquer ou de recevoir des informations une atteinte disproportionnée aux objectifs d'ordre et de sûreté publics qu'elle poursuit ; <br/>
<br/>
              Sur le moyen tiré d'une violation des stipulations de l'article 7 de la même convention :<br/>
<br/>
              8. Considérant qu'aux termes de l'article 7 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " 1. Nul ne peut être condamné pour une action ou une omission qui, au moment où elle a été commise, ne constituait pas une infraction d'après le droit national ou international. De même il n'est infligé aucune peine plus forte que celle qui était applicable au moment où l'infraction a été commise. / 2. Le présent article ne portera pas atteinte au jugement et à la punition d'une personne coupable d'une action ou d'une omission qui, au moment où elle a été commise, était criminelle d'après les principes généraux de droit reconnus par les nations civilisées " ; <br/>
<br/>
              9. Considérant, qu'ainsi qu'il a été dit plus haut, les dispositions du V de l'article R. 413-15 du code de la route respectent l'exigence de prévisibilité de la norme découlant de l'article 10 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; que le moyen tiré de ce que, faute de respecter cette exigence découlant également de l'article 7 de la convention, elles méconnaîtraient les  stipulations de cet article, ne peut qu'être écarté ;<br/>
<br/>
              Sur le moyen tiré d'une violation des stipulations de l'article 1er du premier protocole additionnel à la même convention :<br/>
<br/>
              10. Considérant qu'aux termes de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " Toute personne physique ou morale a droit au respect de ses biens. Nul ne peut être privé de sa propriété que pour cause d'utilité publique et dans les conditions prévues par la loi et les principes généraux du droit international. / Les dispositions précédentes ne portent pas atteinte au droit que possèdent les Etats de mettre en vigueur les lois qu'ils jugent nécessaires pour réglementer l'usage des biens conformément à l'intérêt général ou pour assurer le paiement des impôts ou d'autres contributions ou des amendes " ;<br/>
<br/>
              11. Considérant, en premier lieu, que pour les mêmes motifs que ceux déjà indiqués, il ne saurait être reproché aux dispositions du V de l'article R. 413-15 du code de la route de méconnaître l'exigence de prévisibilité qui découle également de l'article 1er du protocole ; <br/>
<br/>
              12. Considérant, en deuxième lieu, que ces dispositions, en tant qu'elles prévoient une contravention de cinquième classe ainsi que la saisie du dispositif ou du produit et la possibilité de saisir le véhicule concerné, ne méconnaissent pas les stipulations de l'article 1er du protocole, dont le second paragraphe réserve le droit que possèdent les Etats de mettre en vigueur les lois qu'ils jugent nécessaires pour réglementer l'usage des biens conformément à l'intérêt général ou pour assurer notamment le paiement des amendes ; <br/>
<br/>
              13. Considérant, en troisième lieu, que, si la confiscation du dispositif ou du produit ainsi que la possibilité ouverte, à titre de peine complémentaire, de confisquer le véhicule sur lequel le dispositif est placé, adapté ou appliqué peuvent emporter une privation du droit de propriété au sens du premier paragraphe du même article, de telles mesures, eu égard à la gravité de l'infraction en cause, aux biens qui peuvent en faire l'objet et, s'agissant du véhicule, au fait que la confiscation ne peut être prononcée que si le juge pénal en décide spécialement dans une affaire déterminée en tenant compte de l'ensemble des circonstances de celle-ci, ne méconnaissent pas l'exigence de juste équilibre entre l'atteinte portée au droit de propriété et les motifs d'ordre public et de sécurité des personnes qui les justifient ; <br/>
<br/>
              14. Considérant qu'il résulte de tout ce qui précède que la société NAVX n'est pas fondée à demander l'annulation de l'article 22 du décret du 3 janvier 2012 qu'elle attaque ; qu'il y a lieu, dès lors, de rejeter sa requête, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : La requête de la société NAVX est rejetée. <br/>
Article 2 : La présente décision sera notifiée à la société NAVX, au Premier ministre et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
