<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039374293</ID>
<ANCIEN_ID>JG_L_2019_11_000000419618</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/37/42/CETATEXT000039374293.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 13/11/2019, 419618</TITRE>
<DATE_DEC>2019-11-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419618</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>Mme Coralie Albumazard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2019:419618.20191113</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Investaq Energie et la société Celtique Energie Limited ont demandé au tribunal administratif de Pau d'annuler la décision implicite, résultant du silence gardé sur leur demande du 28 juin 2014, par laquelle le ministre chargé des mines a refusé, d'une part, la prolongation exceptionnelle de la validité du permis de recherches d'hydrocarbures liquides ou gazeux, dit " permis de Claracq ", pour trois années supplémentaires, et, d'autre part, le renouvellement de ce même permis pour une troisième période de validité de cinq ans. Elles ont également demandé au tribunal administratif d'enjoindre au ministre, dans un délai d'un mois à compter de la notification du jugement et sous astreinte de 5 000 euros par jour de retard, à titre principal, d'accorder la prolongation exceptionnelle du permis de Claracq, à titre subsidiaire, de renouveler le permis pour une troisième période, et à titre encore subsidiaire, de réexaminer leurs demandes. <br/>
<br/>
              Par un jugement n° 1502495 du 7 juillet 2016, le tribunal administratif de Pau a annulé la décision par laquelle le ministre de l'écologie, du développement durable et de l'énergie et le ministre de l'économie, de l'industrie et du numérique ont implicitement refusé de prolonger la deuxième période de validité du permis de Claracq et a enjoint, sous astreinte, au ministre de l'écologie, du développement durable et de l'énergie de délivrer à la société Investaq Energie et à la société Celtique Energie Limited un permis prolongeant jusqu'au 3 novembre 2017 la deuxième période de validité du permis de Claracq.<br/>
<br/>
              Par un arrêt n° 16BX03192 du 6 février 2018, la cour administrative d'appel de Bordeaux a, sur appel de la ministre de l'environnement, de l'énergie et de la mer, d'une part, annulé ce jugement, et, d'autre part, annulé la décision implicite de rejet de la deuxième prolongation de droit du permis de Claracq et enjoint au ministre d'Etat, ministre de la transition écologique et solidaire, et au ministre de l'économie et des finances d'accorder, dans un délai de deux mois, la prolongation du permis de Claracq pour une troisième période de validité de cinq ans pour une surface de 317 kilomètres carrés.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 6 avril et 6 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, la société Investaq Energie et la société Celtique Energie Limited demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il n'a pas fait droit à leurs conclusions présentées à titre principal ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leurs demandes ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - le code minier ;<br/>
              - la loi n° 79-587 du 11 juillet 1979 ;<br/>
              - le décret n° 2006-648 du 2 juin 2006 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Coralie Albumazard, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la société Investaq Energie et autre ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 25 octobre 2019, présentée par les sociétés Investaq Energie et Celtique Energie Limited ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier qu'un permis exclusif de recherches d'hydrocarbures, dit " permis de Claracq ", a été délivré à la société Celtique Energie Limited par un arrêté du ministre délégué à l'industrie du 28 septembre 2006, pour une durée de trois ans, sur une surface de 726 km2 dans les départements des Pyrénées-Atlantiques et des Landes. Par un arrêté du 17 janvier 2008, la surface de ce permis a été étendue à 828 km2. Par un arrêté du 7 septembre 2010, la validité de ce permis a été prolongée jusqu'au 3 novembre 2014, sur une surface réduite à 463 km2. Le 15 décembre 2010, la société Celtique Energie Limited a sollicité la mutation de la moitié des droits résultant de ce permis au bénéfice de la société Investaq Energie, laquelle a été accordée à ces sociétés par un arrêté des ministres du redressement productif et de l'écologie du 27 août 2013. Le 28 juin 2014, ces deux sociétés ont saisi l'autorité administrative d'une demande tendant, à titre principal, à la prolongation exceptionnelle, sur le fondement du deuxième alinéa de l'article L. 142-2 du code minier, de la deuxième période de validité du permis de Claracq pour trois ans, et, à titre subsidiaire, au renouvellement de ce permis pour une troisième période de validité de cinq ans et sur une surface de 317 km2. Par un jugement du 7 juillet 2016, le tribunal administratif de Pau a, sur demande des sociétés Investaq Energie et Celtique Energie Limited, annulé la décision implicite de rejet née du silence gardé par l'autorité administrative sur cette demande et enjoint à l'administration d'accorder à ces sociétés la prolongation jusqu'au 3 novembre 2017 de la deuxième période de validité de ce permis, sous astreinte de 3 000 euros par jour à l'expiration d'un délai de trente jours à compter de la notification du jugement. Par un arrêt du 6 février 2018 contre lequel les sociétés Investaq Energie et Celtique Energie Limited se pourvoient en cassation, la cour administrative d'appel a annulé ce jugement ainsi que la décision implicite de rejet de la deuxième demande de prolongation de droit du permis et enjoint à l'autorité administrative d'en accorder la prolongation pour une troisième période de validité de cinq ans, sur une surface de 317 km2. <br/>
<br/>
              2. D'une part, aux termes de l'article L. 122-2 du code minier : " Nul ne peut obtenir un permis exclusif de recherches s'il ne possède les capacités techniques et financières nécessaires pour mener à bien les travaux de recherches et pour assumer les obligations mentionnées dans des décrets pris pour préserver les intérêts mentionnés à l'article L. 161-1 et aux articles L. 161-1 et L. 163-1 à L. 163-9. (...) ". L'article L. 122-3 du même code dispose que le permis exclusif de recherches est accordé, après mise en concurrence, par l'autorité administrative compétente pour une durée initiale maximale de cinq ans. Aux termes de l'article L. 142-1 du même code : " La validité d'un permis exclusif de recherches peut être prolongée à deux reprises, chaque fois de cinq ans au plus, sans nouvelle mise en concurrence. / Chacune de ces prolongations est de droit, soit pour une durée au moins égale à trois ans, soit pour la durée de validité précédente si cette dernière est inférieure à trois ans, lorsque le titulaire a satisfait à ses obligations et souscrit dans la demande de prolongation un engagement financier au moins égal à l'engagement financier souscrit pour la période de validité précédente, au prorata de la durée de validité et de la superficie sollicitées ". Aux termes de l'article L. 142-2 du même code : " La superficie du permis exclusif de recherches d'hydrocarbures liquides ou gazeux, dit " permis H ", est réduite de moitié lors du premier renouvellement et du quart de la surface restante lors du deuxième renouvellement. (...) / En cas de circonstances exceptionnelles invoquées par le titulaire ou par l'autorité administrative, la durée de l'une seulement des périodes de validité d'un " permis H " peut être prolongée de trois ans au plus, sans réduction de surface. ".<br/>
<br/>
              3. D'autre part, aux termes de l'article 1er de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public, en vigueur à la date de la décision contestée, désormais repris à l'article L. 211-2 du code des relations entre le public et l'administration, doivent notamment être motivées les décisions administratives individuelles défavorables qui " refusent une autorisation, sauf lorsque la communication des motifs pourrait être de nature à porter atteinte à l'un des secrets ou intérêts protégés par les dispositions des deuxième à cinquième alinéas de l'article 6 de la loi n° 78-753 du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public ".<br/>
<br/>
              4. Il résulte des dispositions de l'article L. 142-2 du code minier que le titulaire d'un permis exclusif de recherches peut en obtenir la prolongation sans nouvelle mise en concurrence soit de droit, à deux reprises, pour une durée de cinq ans au plus, la superficie du permis étant alors réduite à l'occasion de chaque renouvellement, soit de manière dérogatoire, pour l'une des périodes de validité de ce permis, pour une durée de trois ans au plus et sans réduction de surface, en cas de circonstances exceptionnelles. Eu égard à sa portée, la décision par laquelle l'autorité administrative refuse de faire droit, en cas de circonstances exceptionnelles, à une demande de prolongation de l'une des périodes de validité d'un permis exclusif de recherches, doit être regardée comme un refus d'autorisation, au sens de l'article 1er de la loi du 11 juillet 1979 précitée, et soumise à ce titre à l'obligation de motivation. Par suite, les requérantes sont fondées à soutenir que la cour administrative d'appel a commis une erreur de droit en jugeant que la décision de refuser une telle prolongation dérogatoire n'entrait dans aucune catégorie prévue par la loi du 11 juillet 1979 et n'était pas au nombre des décisions devant être motivées.<br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que les sociétés Investaq Energie et autre sont fondées à demander l'annulation de l'arrêt qu'elles attaquent en tant qu'il a rejeté leurs conclusions présentées à titre principal. <br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt n° 16BX03192 du 6 février 2018 de la cour administrative d'appel de Bordeaux est annulé en tant qu'il n'a pas fait droit aux conclusions présentées à titre principal par les sociétés Investaq Energie et autre.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Bordeaux.<br/>
Article 3 : L'Etat versera aux sociétés Investaq Energie et Celtique Energie Limited la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société Investaq Energie, première dénommée pour l'ensemble des requérants, et à la ministre de la transition écologique et solidaire.<br/>
		Copie en sera adressée au ministre de l'économie et des finances. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-01-02-01-01-06 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. QUESTIONS GÉNÉRALES. MOTIVATION. MOTIVATION OBLIGATOIRE. MOTIVATION OBLIGATOIRE EN VERTU DES ARTICLES 1 ET 2 DE LA LOI DU 11 JUILLET 1979. DÉCISION REFUSANT UNE AUTORISATION. - REFUS DE PROROGER UN PERMIS EXCLUSIF DE RECHERCHES (ART. L. 142-2 DU CODE MINIER).
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">40-01-01 MINES ET CARRIÈRES. MINES. RECHERCHE DES MINES. - REFUS DE PROROGER UN PERMIS EXCLUSIF DE RECHERCHES (ART. L. 142-2 DU CODE MINIER) - OBLIGATION DE MOTIVATION - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-03-01-02-01-01-06 Il résulte de l'article L. 142-2 du code minier que le titulaire d'un permis exclusif de recherches peut en obtenir la prolongation sans nouvelle mise en concurrence soit de droit, à deux reprises, pour une durée de cinq ans au plus, la superficie du permis étant alors réduite à l'occasion de chaque renouvellement, soit de manière dérogatoire, pour l'une des périodes de validité de ce permis, pour une durée de trois ans au plus et sans réduction de surface, en cas de circonstances exceptionnelles.... ,,Eu égard à sa portée, la décision par laquelle l'autorité administrative refuse de faire droit, en cas de circonstances exceptionnelles, à une demande de prolongation de l'une des périodes de validité d'un permis exclusif de recherches doit être regardée comme un refus d'autorisation, au sens de l'article 1er de la loi n° 79-587 du 11 juillet 1979, et soumise à ce titre à l'obligation de motivation.</ANA>
<ANA ID="9B"> 40-01-01 Il résulte de l'article L. 142-2 du code minier que le titulaire d'un permis exclusif de recherches peut en obtenir la prolongation sans nouvelle mise en concurrence soit de droit, à deux reprises, pour une durée de cinq ans au plus, la superficie du permis étant alors réduite à l'occasion de chaque renouvellement, soit de manière dérogatoire, pour l'une des périodes de validité de ce permis, pour une durée de trois ans au plus et sans réduction de surface, en cas de circonstances exceptionnelles.... ,,Eu égard à sa portée, la décision par laquelle l'autorité administrative refuse de faire droit, en cas de circonstances exceptionnelles, à une demande de prolongation de l'une des périodes de validité d'un permis exclusif de recherches doit être regardée comme un refus d'autorisation, au sens de l'article 1er de la loi n° 79-587 du 11 juillet 1979, et soumise à ce titre à l'obligation de motivation.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
