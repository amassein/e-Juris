<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032260350</ID>
<ANCIEN_ID>JG_L_2016_03_000000394465</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/26/03/CETATEXT000032260350.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème SSJS, 16/03/2016, 394465, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-03-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394465</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP JEAN-PHILIPPE CASTON</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:394465.20160316</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... D...a demandé au tribunal administratif de La Réunion d'annuler les opérations électorales qui se sont déroulées le 5 juillet 2015 en vue de l'élection des conseillers municipaux et communautaires dans la commune de Sainte-Rose et de prononcer l'inéligibilité de M. B... C...et de ses colistiers. Par un jugement n° 1500691 du 9 septembre 2015, le tribunal administratif de la Réunion a rejeté sa protestation.<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 9 novembre 2015 et 16 février 2016 au secrétariat du contentieux du Conseil d'Etat, M. D... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) d'annuler l'élection des conseillers municipaux et communautaires dans la commune de Sainte-Rose ;<br/>
<br/>
              3°) de déclarer inéligibles M. C...et ses colistiers.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Jean-Philippe Caston, avocat de M. C...;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. A l'issue des opérations électorales qui ont eu lieu le 5 juillet 2015 en vue de la désignation des conseillers municipaux et communautaires de la commune de Sainte-Rose (La Réunion), la liste conduite par M. B...C...a obtenu 51,70 % des suffrages exprimés et 2 639 voix, soit 174 voix de plus que celle conduite par M.D..., maire sortant, qui a obtenu 48,30 % des suffrages exprimés et 2 465 voix. M. D...fait appel du jugement du 9 septembre 2015 par lequel le tribunal administratif de La Réunion a rejeté sa protestation tendant à l'annulation de ces opérations électorales et à ce que soient déclarés inéligibles M. C...et ses colistiers.<br/>
<br/>
              Sur les conclusions tendant à l'annulation des opérations électorales : <br/>
<br/>
              2. En premier lieu, aux termes du deuxième alinéa de l'article L. 52-8 du code électoral : " Les personnes morales, à l'exception des partis ou groupements politiques, ne peuvent participer au financement de la campagne électorale d'un candidat, ni en lui consentant des dons sous quelque forme que ce soit, ni en lui fournissant des biens, services ou autres avantages directs ou indirects à des prix inférieurs à ceux qui sont habituellement pratiqués ".<br/>
<br/>
              3. Il résulte de l'instruction que M.C..., sous le pseudonyme de " Patrick " qu'il utilise depuis plusieurs années comme commentateur sportif, est intervenu à l'antenne de Radio Est Réunion durant les journées précédant le scrutin du 5 juillet 2015 et plus particulièrement la veille où son intervention s'est prolongée pendant plusieurs heures. Compte tenu de leur contenu, dénué de tout rapport avec la polémique électorale, de la faible part d'audience de cette radio et de l'important écart de voix entre les deux listes,  ces interventions, à supposer même qu'elles aient constitué une participation d'une personne morale au financement de la campagne électorale, contraire aux dispositions de l'article L. 52-8 du code électoral, n'ont pas été de nature à altérer la sincérité du scrutin. <br/>
<br/>
              4. En deuxième lieu, aux termes du premier alinéa de l'article L. 106 du code électoral : " Quiconque, par des dons ou libéralités en argent ou en nature, par des promesses de libéralités, de faveurs, d'emplois publics ou privés ou d'autres avantages particuliers, faits en vue d'influencer le vote d'un ou plusieurs électeurs aura obtenu ou tenté d'obtenir leur suffrage, soit directement, soit par l'entremise d'un tiers, quiconque, par les mêmes moyens, aura déterminé ou tenté de déterminer un ou plusieurs d'entre eux à s'abstenir, sera puni de deux d'emprisonnement et d'une amende de 15 000 euros ". S'il n'appartient pas au juge de l'élection de faire application de ces dispositions en ce qu'elles édictent des sanctions pénales, il lui revient, en revanche, de rechercher si des pressions telles que celles qui y sont définies ont été exercées sur les électeurs et ont été de nature à altérer la sincérité du scrutin. <br/>
<br/>
              5. Il résulte de l'instruction que des partisans de M. C...se sont livrés auprès de deux électeurs à des remises d'espèces et à des promesses d'avantages en vue d'influencer leur vote. Cependant, compte tenu de l'écart de voix entre les deux listes, ces violations de l'article L. 106 du code électoral, pour regrettables qu'elles soient, n'ont pas été de nature à altérer la sincérité du scrutin. Si M. D...soutient que ces pratiques ne sont pas isolées, il ne produit, en appel pas plus qu'en première instance, aucun élément de nature à en attester l'étendue. <br/>
<br/>
              6. En troisième lieu, le grief tiré de la méconnaissance de l'article L. 49 du code électoral, nouveau en appel et par suite irrecevable, ne peut qu'être écarté.<br/>
<br/>
              7. Il résulte de tout ce qui précède que M. D...n'est pas fondé à soutenir que c'est à tort que, par le jugement qu'il attaque, le tribunal administratif de La Réunion a rejeté sa protestation tendant à l'annulation des opérations électorales qui se sont déroulées le 5 juillet 2015 en vue de la désignation des conseillers municipaux et communautaires de la commune de Sainte-Rose.<br/>
<br/>
              Sur les conclusions tendant à ce que M. C...et ses colistiers soient déclarés inéligibles : <br/>
<br/>
              8. Les conclusions présentées par M. D...tendant à ce que M. C... et ses colistiers soient déclarés inéligibles en application des dispositions de l'article L. 118-4 du code électoral ne peuvent qu'être rejetées, dès lors que, comme il a été dit ci-dessus, il ne résulte pas de l'instruction qu'ils aient accompli des manoeuvres frauduleuses ayant eu pour objet ou pour effet de porter atteinte à la sincérité du scrutin au sens de ces dispositions. <br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              9. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées à ce titre par M.C....<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : La requête de M. D... est rejetée.<br/>
Article 2 : Les conclusions présentées par M. C...au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à M. A... D...et à M. B... C....<br/>
Copie en sera adressée au ministre de l'intérieur et à la ministre des outre-mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
