<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044239119</ID>
<ANCIEN_ID>JG_L_2021_10_000000437254</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/23/91/CETATEXT000044239119.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 22/10/2021, 437254</TITRE>
<DATE_DEC>2021-10-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437254</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Mathieu  Le Coq</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:437254.20211022</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. D... C... a demandé au tribunal administratif de Versailles d'annuler l'arrêté du 28 mars 2014 par lequel le président du Syndicat  a refusé de reconnaître l'imputabilité de sa maladie au service, ainsi que les arrêtés de la même autorité des 7 et 15 avril 2014 le plaçant en congé de maladie ordinaire. Par un jugement n° 1404640 du 28 juin 2016, le tribunal administratif de Versailles a annulé ces arrêtés et a enjoint au syndicat de reconnaître l'imputabilité au service de la maladie du requérant et de reconstituer ses droits.<br/>
<br/>
              Par un arrêt n° 16VE02760 du 31 octobre 2019, la cour administrative d'appel de Versailles a rejeté l'appel formé par le syndicat contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés le 31 décembre 2019 et le 24 avril 2020 au secrétariat du contentieux du Conseil d'Etat, le Syndicat  demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de M. C... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 87-602 du 30 juillet 1987 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Mathieu Le Coq, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Célice, Texidor, Perier, avocat du Syndicat  et à la SCP Piwnica, Molinié, avocat de M. C... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. D... C..., ingénieur territorial en chef, exerçant alors les fonctions de responsable technique et administratif des concessions et conseiller en efficacité énergétique auprès du Syndicat, a été placé à compter du 6 juin 2012 en congé de maladie à raison d'un syndrome dépressif. Le président de ce syndicat mixte a refusé, par un arrêté du 28 mars 2014, de reconnaître l'imputabilité de cette maladie au service, et a en conséquence placé l'intéressé, par des arrêtés des 7 et 15 avril 2014, en congé de maladie ordinaire, à demi-traitement à compter du 1er avril 2014. Le syndicat mixte se pourvoit en cassation contre l'arrêt du 31 octobre 2019 par lequel la cour administrative d'appel de Versailles a rejeté son appel contre le jugement du 28 juin 2016 du tribunal administratif de Versailles annulant ces arrêtés et lui enjoignant de reconnaître l'imputabilité au service de la maladie de M. C....<br/>
<br/>
              2. Aux termes de l'article 57 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, dans sa rédaction applicable à la date de la décision en litige : " Le fonctionnaire en activité a droit : (...) 2° A des congés de maladie dont la durée totale peut atteindre un an pendant une période de douze mois consécutifs en cas de maladie dûment constatée mettant l'intéressé dans l'impossibilité d'exercer ses fonctions. Celui-ci conserve alors l'intégralité de son traitement pendant une durée de trois mois ; ce traitement est réduit de moitié pendant les neuf mois suivants. (...) / Toutefois, si la maladie provient de l'une des causes exceptionnelles prévues à l'article L. 27 du code des pensions civiles et militaires de retraite ou d'un accident survenu dans l'exercice ou à l'occasion de l'exercice de ses fonctions, le fonctionnaire conserve l'intégralité de son traitement jusqu'à ce qu'il soit en état de reprendre son service ou jusqu'à la mise à la retraite. Il a droit, en outre, au remboursement des honoraires médicaux et des frais directement entraînés par la maladie ou l'accident (...). / Dans le cas visé à l'alinéa précédent, l'imputation au service de l'accident ou de la maladie est appréciée par la commission de réforme instituée par le régime des pensions des agents des collectivités locales (...) ".<br/>
<br/>
              3. Une maladie contractée par un fonctionnaire, ou son aggravation, doit être regardée comme imputable au service si elle présente un lien direct avec l'exercice des fonctions ou avec des conditions de travail de nature à susciter le développement de la maladie en cause, sauf à ce qu'un fait personnel de l'agent ou toute autre circonstance particulière conduisent à détacher la survenance ou l'aggravation de la maladie du service.<br/>
<br/>
              4. Pour juger que la maladie dont est atteint M. C... est imputable au service, la cour administrative d'appel de Versailles a retenu, d'une part, que l'intéressé, qui ne présentait pas d'état anxio-dépressif antérieur, a vu sa manière de servir contestée à la suite du changement de président et de directrice du syndicat mixte au début de l'année 2012 et a ainsi connu une situation professionnelle très tendue qui a pu, dans les circonstances de l'espèce, être à l'origine d'une pathologie anxio-dépressive, et d'autre part, qu'il ressort des nombreux avis médicaux produits qu'il existe un lien direct et certain entre l'activité professionnelle de M. C... et le syndrome anxio-dépressif dont il est atteint. En statuant ainsi, alors que le syndicat mixte soutenait que M. C... avait adopté dès le changement de président et de directrice une attitude systématique d'opposition, sans rechercher si ce comportement était avéré et s'il était la cause déterminante de la dégradation des conditions d'exercice professionnel de M. C..., susceptible de constituer dès lors un fait personnel de nature à détacher la survenance de la maladie du service, la cour a commis une erreur de droit.<br/>
<br/>
              5. Par suite, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, le syndicat mixte est fondé à demande l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du syndicat mixte qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. C... la somme demandée par le syndicat mixte au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 31 octobre 2019 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles. <br/>
Article 3 : Les conclusions présentées par le Syndicat  et par M. C... au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au Syndicat  et à M. D... C....<br/>
<br/>
Délibéré à l'issue de la séance du 1er octobre 2021 où siégeaient : M. Jacques-Henri Stahl, président adjoint de la section du contentieux, présidant ; M. J... E..., M. Pierre Collin, présidents de chambre ; M. H... K..., M. I... F..., M. Hervé Cassagnabère, conseillers d'Etat et M. Mathieu Le Coq, maître des requêtes-rapporteur. <br/>
<br/>
              Rendu le 22 octobre 2021.<br/>
<br/>
                 Le Président : <br/>
                 Signé : M. L... B...<br/>
 		Le rapporteur : <br/>
      Signé : M. Mathieu Le Coq<br/>
                 La secrétaire :<br/>
                 Signé : Mme A... G...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-05-04-01 FONCTIONNAIRES ET AGENTS PUBLICS. - POSITIONS. - CONGÉS. - CONGÉS DE MALADIE. - MALADIE IMPUTABLE AU SERVICE - 1) NOTION [RJ1] - 2) FAIT CONDUISANT À DÉTACHER LA MALADIE DU SERVICE - ILLUSTRATION [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-07-01-03 FONCTIONNAIRES ET AGENTS PUBLICS. - STATUTS, DROITS, OBLIGATIONS ET GARANTIES. - STATUT GÉNÉRAL DES FONCTIONNAIRES DE L'ÉTAT ET DES COLLECTIVITÉS LOCALES. - DISPOSITIONS STATUTAIRES RELATIVES À LA FONCTION PUBLIQUE TERRITORIALE (LOI DU 26 JANVIER 1984). - TRAITEMENT DES FONCTIONNAIRES TERRITORIAUX EN ACTIVITÉ PLACÉS EN CONGÉ DE MALADIE (ART. 57 DE LA LOI DU 26 JANVIER 1984) - PRISE EN CHARGE DES SOINS ET MAINTIEN DU PLEIN TRAITEMENT EN CAS DE MALADIE IMPUTABLE AU SERVICE, JUSQU'À CE QUE L'AGENT SOIT EN ÉTAT DE REPRENDRE SON SERVICE - 1) NOTION DE MALADIE IMPUTABLE AU SERVICE [RJ1] - 2) FAIT PERSONNEL CONDUISANT À DÉTACHER LA MALADIE DU SERVICE - ILLUSTRATION [RJ2].
</SCT>
<ANA ID="9A"> 36-05-04-01 1) Une maladie contractée par un fonctionnaire, ou son aggravation, doit être regardée comme imputable au service si elle présente un lien direct avec l'exercice des fonctions ou avec des conditions de travail de nature à susciter le développement de la maladie en cause, sauf à ce qu'un fait personnel de l'agent ou toute autre circonstance particulière conduisent à détacher la survenance ou l'aggravation de la maladie du service.......2) Fonctionnaire, ne présentant pas d'état anxio-dépressif antérieur, ayant vu sa manière de servir contestée à la suite du changement de président et de directeur de son établissement employeur et ayant ainsi connu une situation professionnelle très tendue qui a pu, dans les circonstances de l'espèce, être à l'origine d'une pathologie anxio-dépressive. Nombreux avis médicaux étayant l'existence d'un lien direct et certain entre l'activité professionnelle de l'intéressé et le syndrome anxio-dépressif dont il est atteint.......Alors que l'établissement employeur soutient que l'intéressé a adopté dès le changement de président et de directrice une attitude systématique d'opposition, il appartient au juge de rechercher si ce comportement est avéré et s'il a été la cause déterminante de la dégradation des conditions d'exercice professionnel de l'intéressé, susceptible de constituer dès lors un fait personnel de nature à détacher la survenance de la maladie du service.</ANA>
<ANA ID="9B"> 36-07-01-03 1) Une maladie contractée par un fonctionnaire, ou son aggravation, doit être regardée comme imputable au service si elle présente un lien direct avec l'exercice des fonctions ou avec des conditions de travail de nature à susciter le développement de la maladie en cause, sauf à ce qu'un fait personnel de l'agent ou toute autre circonstance particulière conduisent à détacher la survenance ou l'aggravation de la maladie du service.......2) Fonctionnaire, ne présentant pas d'état anxio-dépressif antérieur, ayant vu sa manière de servir contestée à la suite du changement de président et de directeur de son établissement employeur et ayant ainsi connu une situation professionnelle très tendue qui a pu, dans les circonstances de l'espèce, être à l'origine d'une pathologie anxio-dépressive. Nombreux avis médicaux étayant l'existence d'un lien direct et certain entre l'activité professionnelle de l'intéressé et le syndrome anxio-dépressif dont il est atteint.......Alors que l'établissement employeur soutient que l'intéressé a adopté dès le changement de président et de directrice une attitude systématique d'opposition, il appartient au juge de rechercher si ce comportement est avéré et s'il a été la cause déterminante de la dégradation des conditions d'exercice professionnel de l'intéressé, susceptible de constituer dès lors un fait personnel de nature à détacher la survenance de la maladie du service.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 13 mars 2019, Mme Duret, n° 407795, p. 59....[RJ2] Rappr., s'agissant d'une autre circonstance alléguée conduisant à détacher la survenance de la maladie du service, CE, 11 février 1981, Ministre de l'intérieur c/ Mauger, n° 19614, T. p. 782.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
