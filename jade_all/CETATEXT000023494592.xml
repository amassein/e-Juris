<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023494592</ID>
<ANCIEN_ID>JG_L_2011_01_000000330653</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/49/45/CETATEXT000023494592.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 21/01/2011, 330653</TITRE>
<DATE_DEC>2011-01-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>330653</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS ; SCP PEIGNOT, GARREAU</AVOCATS>
<RAPPORTEUR>Mme Domitille  Duval-Arnould</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Thiellay Jean-Philippe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:330653.20110121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 10 août et 12 novembre 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. C... D..., demeurant au... ; M.  D...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08NC00546, 08NC00547 du 11 juin 2009 par lequel la cour administrative d'appel de Nancy a, d'une part, annulé le jugement du tribunal administratif de Strasbourg du 28 décembre 2007 qui avait annulé la délibération du 29 septembre 2006 du conseil municipal de la commune de Freybouse autorisant le maire à conclure avec M.  A...et l'Earl de l'Entente des contrats de baux à ferme à l'amiable pour deux parcelles agricoles et a, d'autre part, rejeté sa demande ainsi que celle du préfet de la Moselle tendant à l'annulation de cette délibération ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les requêtes en appel de M.  A...et de l'EARL de l'Entente ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Freybouse, de M.  A...et de l'EARL de l'Entente, la somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code général des collectivités territoriales ;<br/>
	Vu le code rural ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Domitille Duval-Arnould, chargée des fonctions de Maître des Requêtes,  <br/>
<br/>
              - les observations de Me Haas, avocat de M. D...et de la SCP Peignot, Garreau, avocat de M. A...et autres, <br/>
<br/>
              - les conclusions de M. Jean-Philippe Thiellay, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Haas, avocat de M. D...et à la SCP Peignot, Garreau, avocat de M. A...et autres ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une délibération du 29 septembre 2006, le conseil municipal de Freybouse a décidé de conclure à l'amiable deux baux ruraux sur des terres agricoles dont la commune est propriétaire, l'un avec M. A...pour une parcelle de 8 ha 43 et l'autre avec l'EARL de l'Entente pour une parcelle de 6 ha 10 ; que, saisi par une demande de M. D...et par un déféré du préfet de la Moselle, le tribunal administratif de Strasbourg a annulé cette délibération par un jugement du  28 décembre 2007 ; que M. D...se pourvoit en cassation contre l'arrêt du 11 juin 2009 par lequel la cour administrative d'appel de Nancy, saisie par M. A...et par l'EARL de l'Entente, a annulé ce jugement et rejeté sa demande ainsi que le déféré du préfet de la Moselle ; <br/>
<br/>
              Considérant qu'aux termes de l'article L. 411-15 du code rural, applicable aux baux ruraux : " Lorsque le bailleur est une personne morale de droit public, le bail peut être conclu soit à l'amiable, soit par voie d'adjudication / (....) Quel que soit le mode de conclusion du bail, une priorité est réservée aux exploitants qui réalisent une installation en bénéficiant de la dotation d'installation aux jeunes agriculteurs ou, à défaut, aux exploitants de la commune répondant aux conditions de capacité professionnelle et de superficie visées à l'article L. 331-2 du présent code, ainsi qu'à leurs groupements. (...) " ; <br/>
<br/>
              Considérant que, pour écarter le moyen tiré de ce que la délibération attaquée avait été prise en méconnaissance de ces dispositions, la cour administrative d'appel de Nancy a affirmé que M. D...n'établissait pas et n'alléguait pas qu'il aurait manifesté sa candidature à l'attribution des baux mentionnés par cette délibération ; qu'il est toutefois établi par les pièces du dossier soumis aux juges du fond que, comme le soutenait M.D..., celui-ci était candidat depuis plusieurs années à l'attribution de ces baux et que la commune de Freybouse en avait connaissance ; que la cour administrative d'appel a, dès lors, dénaturé les pièces du dossier ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. D...est fondé à demander l'annulation de l'arrêt de la cour administrative d'appel de Nancy du 11 juin 2009 ; <br/>
<br/>
              Considérant que, dans les circonstances de l'espèce, il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au fond ;<br/>
<br/>
              Sur la fin de non recevoir opposée par la commune de Freybouse à la demande de M.D... :<br/>
<br/>
              Considérant que M.D..., exploitant agricole à Freybouse et candidat à l'attribution des baux ruraux mentionnés par la délibération du 29 septembre 2006, justifiait d'un intérêt lui donnant qualité pour demander l'annulation de cette délibération ; que la commune de Freybouse n'est par suite pas fondée à soutenir que sa demande tendant à l'annulation de la délibération du 29 septembre 2006 ne serait pas recevable ;<br/>
<br/>
              Sur la légalité de la délibération attaquée :<br/>
<br/>
              Considérant qu'il résulte des dispositions des articles R. 343-3 et suivants du code rural, dans leur rédaction alors en vigueur, que la réalisation d'une installation en bénéficiant de la dotation d'installation aux jeunes agriculteurs prévue au 1° de l'article R. 343-3 ne constitue pas un acte instantané mais la réalisation progressive, étalée dans le temps, du projet d'installation au vu duquel et pour lequel la dotation a été accordée ; qu'il résulte de l'ensemble des dispositions alors applicables à cette dotation et notamment de celles des 5°, 6° et 7° de l'article R . 343-5 et de celles de l'article R. 343-18, alors en vigueur, que la réalisation du projet d'installation pour lequel la dotation était attribuée devait être regardée comme achevée à l'expiration d'un délai de dix ans à compter de la date à laquelle le bénéficiaire avait commencé à réaliser effectivement ce projet ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier, et notamment du certificat de conformité établi le 29 octobre 2002 par le directeur départemental de l'agriculture et de la forêt, que M. D...a commencé à compter du 16 avril 2002 à réaliser son installation conformément au projet pour lequel la dotation d'installation aux jeunes agriculteurs lui avait été accordée par une décision préfectorale du 11 mai 2001 ; que, à la date du 29 septembre 2006 à laquelle a été prise la délibération attaquée, le délai de dix ans courant à compter du 16 avril 2002 n'était pas expiré et qu'il en résulte que M. D...bénéficiait alors de la priorité instituée par les dispositions de l'article L. 411-15 du code rural ; qu'ainsi, en décidant par sa délibération du 29 septembre 2006 d'autoriser le maire à conclure des baux ruraux avec des agriculteurs qui ne bénéficiaient pas de cette priorité plutôt qu'avec M.  D...qui était candidat à leur attribution, le conseil municipal a méconnu ces dispositions ; qu'il en résulte que, sans qu'il soit besoin d'examiner les autres moyens de leur requête, M.  A...et l'EARL de l'Entente ne sont pas fondés à se plaindre de ce que, par le jugement attaqué, le tribunal administratif a annulé cette délibération ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que ces dispositions font obstacle à ce que soient mises à la charge de M. D...les sommes que demandent la commune de Freybouse, M. A...et l'EARL de l'Entente au titre des frais exposés par eux et non compris dans les dépens ; qu'il y a lieu, dans les circonstances de l'espèce, de faire application de ces dispositions et de mettre à la charge de la commune de Freybouse la somme de 3 000 euros  ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 11 juin 2009 de la cour administrative d'appel de Nancy est annulé.<br/>
<br/>
Article 2 : Les requêtes présentées par M. A...et par l'EARL de l'Entente devant la cour administrative d'appel de Nancy sont rejetées.<br/>
<br/>
Article 3 : La commune de Freybouse versera à M. D...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions de M.A..., de l'EARL de l'Entente et de la commune de Freybouse tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. C...D..., à M. B... A..., à l'EARL de l'Entente, à la commune de Freybouse et au ministre de l'agriculture, de l'alimentation, de la pêche, de la ruralité et de l'aménagement du territoire.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">03-03-02-01 AGRICULTURE ET FORÊTS. EXPLOITATIONS AGRICOLES. STATUT DU FERMAGE ET DU MÉTAYAGE. BAUX RURAUX. - 1) CONTESTATION DE L'ATTRIBUTION DE BAUX RURAUX SUR DES TERRES AGRICOLES DONT UNE COMMUNE EST PROPRIÉTAIRE (ART. L. 411-15 DU CODE RURAL) - CANDIDAT ÉVINCÉ - COMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE [RJ1] - 2) PRIORITÉ RÉSERVÉE AUX EXPLOITANTS QUI RÉALISENT UNE INSTALLATION EN BÉNÉFICIANT DE LA DOTATION D'INSTALLATION AUX JEUNES AGRICULTEURS (ART. L. 411-15 DU CODE RURAL) - PRIORITÉ PENDANT LA PÉRIODE DE RÉALISATION DU PROJET - PÉRIODE REGARDÉE COMME ACHEVÉE À L'EXPIRATION D'UN DÉLAI DE DIX ANS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">135-02-01-02-01-03 COLLECTIVITÉS TERRITORIALES. COMMUNE. ORGANISATION DE LA COMMUNE. ORGANES DE LA COMMUNE. CONSEIL MUNICIPAL. DÉLIBÉRATIONS. - 1) CONTESTATION DE L'ATTRIBUTION DE BAUX RURAUX SUR DES TERRES AGRICOLES DONT UNE COMMUNE EST PROPRIÉTAIRE (ART. L. 411-15 DU CODE RURAL) - CANDIDAT ÉVINCÉ - COMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE [RJ1] - 2) DOMAINE PRIVÉ D'UNE COMMUNE - CONCLUSION DE BAUX RURAUX - PRIORITÉ RÉSERVÉE AUX EXPLOITANTS QUI RÉALISENT UNE INSTALLATION EN BÉNÉFICIANT DE LA DOTATION D'INSTALLATION AUX JEUNES AGRICULTEURS (ART. L. 411-15 DU CODE RURAL) - PRIORITÉ PENDANT LA PÉRIODE DE RÉALISATION DU PROJET - PÉRIODE REGARDÉE COMME ACHEVÉE À L'EXPIRATION D'UN DÉLAI DE DIX ANS.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">135-02-02 COLLECTIVITÉS TERRITORIALES. COMMUNE. BIENS DE LA COMMUNE. - 1) CONTESTATION DE L'ATTRIBUTION DE BAUX RURAUX SUR DES TERRES AGRICOLES DONT UNE COMMUNE EST PROPRIÉTAIRE (ART. L. 411-15 DU CODE RURAL) - CANDIDAT ÉVINCÉ - COMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE [RJ1] - 2) DOMAINE PRIVÉ D'UNE COMMUNE - CONCLUSION DE BAUX RURAUX - PRIORITÉ RÉSERVÉE AUX EXPLOITANTS QUI RÉALISENT UNE INSTALLATION EN BÉNÉFICIANT DE LA DOTATION D'INSTALLATION AUX JEUNES AGRICULTEURS (ART. L. 411-15 DU CODE RURAL) - PRIORITÉ PENDANT LA PÉRIODE DE RÉALISATION DU PROJET - PÉRIODE REGARDÉE COMME ACHEVÉE À L'EXPIRATION D'UN DÉLAI DE DIX ANS.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">17-03-02-02-01-02 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. DOMAINE. DOMAINE PRIVÉ. AUTORISATION D'OCCUPATION. - CANDIDAT ÉVINCÉ - CONTESTATION DE L'ATTRIBUTION DE BAUX RURAUX SUR DES TERRES AGRICOLES DONT UNE COMMUNE EST PROPRIÉTAIRE (ART. L. 411-15 DU CODE RURAL) [RJ1].
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">24-02-02-02 DOMAINE. DOMAINE PRIVÉ. RÉGIME. GESTION. - CONCLUSION DE BAUX RURAUX PAR UNE COMMUNE - PRIORITÉ RÉSERVÉE AUX EXPLOITANTS QUI RÉALISENT UNE INSTALLATION EN BÉNÉFICIANT DE LA DOTATION D'INSTALLATION AUX JEUNES AGRICULTEURS (ART. L. 411-15 DU CODE RURAL) - PRIORITÉ PENDANT LA PÉRIODE DE RÉALISATION DU PROJET - PÉRIODE REGARDÉE COMME ACHEVÉE À L'EXPIRATION D'UN DÉLAI DE DIX ANS.
</SCT>
<SCT ID="8F" TYPE="PRINCIPAL">24-02-03-01-02 DOMAINE. DOMAINE PRIVÉ. CONTENTIEUX. COMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE. CONTENTIEUX DE LA GESTION. - CONTESTATION DE L'ATTRIBUTION DE BAUX RURAUX SUR DES TERRES AGRICOLES DONT UNE COMMUNE EST PROPRIÉTAIRE (ART. L. 411-15 DU CODE RURAL) - CANDIDAT ÉVINCÉ - COMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE [RJ1].
</SCT>
<ANA ID="9A"> 03-03-02-01 1) La juridiction administrative est compétente pour connaître de la contestation, par un candidat évincé, de l'attribution des baux ruraux sur des terres agricoles dont une commune est propriétaire (article L. 411-15 du code rural).,,2) Pour la conclusion des baux ruraux sur des terres agricoles dont une commune est propriétaire, une priorité est réservée aux exploitants qui réalisent une installation en bénéficiant de la dotation d'installation aux jeunes agriculteurs (article L. 411-15 du code rural). Une telle installation ne constitue pas un acte instantané mais la réalisation progressive, étalée dans le temps, du projet d'installation au vu duquel et pour lequel la dotation a été accordée. Par suite, la priorité réservée aux exploitants qui réalisent une installation en bénéficiant de la dotation d'installation aux jeunes agriculteurs joue pendant la période de réalisation du projet. Cette dernière doit être regardée comme achevée à l'expiration d'un délai de dix ans (devenu cinq ans) à compter de la date à laquelle le bénéficiaire a commencé à réaliser effectivement ce projet.</ANA>
<ANA ID="9B"> 135-02-01-02-01-03 1) La juridiction administrative est compétente pour connaître de la contestation, par un candidat évincé, de l'attribution des baux ruraux sur des terres agricoles dont une commune est propriétaire (article L. 411-15 du code rural).,,2) Pour la conclusion des baux ruraux sur des terres agricoles dont une commune est propriétaire, une priorité est réservée aux exploitants qui réalisent une installation en bénéficiant de la dotation d'installation aux jeunes agriculteurs (article L. 411-15 du code rural). Une telle installation ne constitue pas un acte instantané mais la réalisation progressive, étalée dans le temps, du projet d'installation au vu duquel et pour lequel la dotation a été accordée. Par suite, la priorité réservée aux exploitants qui réalisent une installation en bénéficiant de la dotation d'installation aux jeunes agriculteurs joue pendant la période de réalisation du projet. Cette dernière doit être regardée comme achevée à l'expiration d'un délai de dix ans (devenu cinq ans) à compter de la date à laquelle le bénéficiaire a commencé à réaliser effectivement ce projet.</ANA>
<ANA ID="9C"> 135-02-02 1) La juridiction administrative est compétente pour connaître de la contestation, par un candidat évincé, de l'attribution des baux ruraux sur des terres agricoles dont une commune est propriétaire (article L. 411-15 du code rural).,,2) Pour la conclusion des baux ruraux sur des terres agricoles dont une commune est propriétaire, une priorité est réservée aux exploitants qui réalisent une installation en bénéficiant de la dotation d'installation aux jeunes agriculteurs (article L. 411-15 du code rural). Une telle installation ne constitue pas un acte instantané mais la réalisation progressive, étalée dans le temps, du projet d'installation au vu duquel et pour lequel la dotation a été accordée. Par suite, la priorité réservée aux exploitants qui réalisent une installation en bénéficiant de la dotation d'installation aux jeunes agriculteurs joue pendant la période de réalisation du projet. Cette dernière doit être regardée comme achevée à l'expiration d'un délai de dix ans (devenu cinq ans) à compter de la date à laquelle le bénéficiaire a commencé à réaliser effectivement ce projet.</ANA>
<ANA ID="9D"> 17-03-02-02-01-02 La juridiction administrative est compétente pour connaître de la contestation, par un candidat évincé, de l'attribution des baux ruraux sur des terres agricoles dont une commune est propriétaire (article L. 411-15 du code rural).</ANA>
<ANA ID="9E"> 24-02-02-02 Pour la conclusion des baux ruraux sur des terres agricoles dont une commune est propriétaire, une priorité est réservée aux exploitants qui réalisent une installation en bénéficiant de la dotation d'installation aux jeunes agriculteurs (article L. 411-15 du code rural). Une telle installation ne constitue pas un acte instantané mais la réalisation progressive, étalée dans le temps, du projet d'installation au vu duquel et pour lequel la dotation a été accordée. Par suite, la priorité réservée aux exploitants qui réalisent une installation en bénéficiant de la dotation d'installation aux jeunes agriculteurs joue pendant la période de réalisation du projet. Cette dernière doit être regardée comme achevée à l'expiration d'un délai de dix ans (devenu cinq ans) à compter de la date à laquelle le bénéficiaire a commencé à réaliser effectivement ce projet.</ANA>
<ANA ID="9F"> 24-02-03-01-02 La juridiction administrative est compétente pour connaître de la contestation, par un candidat évincé, de l'attribution des baux ruraux sur des terres agricoles dont une commune est propriétaire (article L. 411-15 du code rural).</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. TC, 22 novembre 2010, SARL Brasserie du Théâtre c/ Commune de Reims, n° 3764, p. 590.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
