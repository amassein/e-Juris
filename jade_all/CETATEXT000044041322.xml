<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044041322</ID>
<ANCIEN_ID>JG_L_2021_09_000000445544</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/04/13/CETATEXT000044041322.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 13/09/2021, 445544, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-09-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445544</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Sébastien Ferrari</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:445544.20210913</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme B... A... ont demandé au tribunal administratif de Pau de prononcer la décharge, d'une part, des cotisations supplémentaires d'impôt sur le revenu auxquelles ils ont été assujettis au titre des années 2013, 2014 et 2015 ainsi que des pénalités correspondantes et, d'autre part, des cotisations supplémentaires de taxe d'habitation auxquelles ils ont été assujettis dans les rôles de la commune de Pau (Pyrénées-Atlantiques) au titre des années 2015 et 2016 à raison d'un immeuble situé 9 rue Montaigne. <br/>
<br/>
              Par un jugement nos 1701940, 1701941 du 7 juin 2018, ce tribunal a accordé à M. et Mme A... la décharge de l'ensemble de ces cotisations supplémentaires et des pénalités correspondantes.<br/>
<br/>
              Par un arrêt n° 18BX03560 du 20 octobre 2020, la cour administrative d'appel de Bordeaux a, sur l'appel du ministre de l'action et des comptes publics, d'une part, annulé l'article 1er de ce jugement et rétabli M. et Mme A... aux rôles de l'impôt sur le revenu au titre des années 2013, 2014 et 2015 et, d'autre part, transmis au Conseil d'Etat, en application des dispositions de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 2 octobre 2018 au greffe de cette cour, formé par le ministre de l'action et des comptes publics contre le jugement du tribunal administratif de Pau en tant qu'il a prononcé la décharge des cotisations supplémentaires de taxe d'habitation des années 2015 et 2016.<br/>
<br/>
              Par ce pourvoi et un nouveau mémoire, enregistré le 3 août 2021 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'économie, des finances et de la relance demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 2 du jugement du tribunal administratif de Pau du 7 juin 2018 ;<br/>
<br/>
              2°) réglant l'affaire au fond dans cette mesure, de rétablir M. et Mme A... au rôle de la taxe d'habitation au titre des années 2015 et 2016.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sébastien Ferrari, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à l'issue d'un contrôle sur pièces du dossier fiscal de M. et Mme A..., l'administration a partiellement remis en cause le bénéfice de l'abattement prévu par l'article 80 sexies du code général des impôts dont Mme A... s'était prévalue au titre de son activité d'assistante maternelle agréée pour le calcul de l'impôt sur le revenu au titre des années 2013, 2014 et 2015 et assujetti en conséquence les contribuables à des cotisations supplémentaires d'impôt sur le revenu. Par ailleurs, des cotisations supplémentaires de taxe d'habitation ont également été mises en recouvrement au titre des années 2015 et 2016 en conséquence de l'augmentation du revenu fiscal de référence des années 2014 et 2015. Le ministre de l'économie, des finances et de la relance se pourvoit en cassation contre l'article 2 du jugement du 7 juin 2018 par lequel le tribunal administratif de Pau a fait droit à la demande de M. et Mme A... tendant à la décharge des cotisations supplémentaires de taxe d'habitation auxquelles ils ont été assujettis au titre des années 2014 et 2015 ainsi que des pénalités correspondantes. <br/>
<br/>
              2. Aux termes de l'article 1414 A du code général des impôts, dans sa rédaction applicable aux années d'imposition en litige : " I. - Les contribuables autres que ceux mentionnés à l'article 1414, dont le montant des revenus de l'année précédente n'excède pas la limite prévue au II de l'article 1417, sont dégrevés d'office de la taxe d'habitation afférente à leur habitation principale pour la fraction de leur cotisation qui excède 3,44 % de leur revenu au sens du IV de l'article 1417 diminué d'un abattement fixé à : a. 5 424 € pour la première part de quotient familial, majoré de 1 568 euros pour les quatre premières demi-parts (...) en France métropolitaine ; (...) Ces montants d'abattements sont, chaque année, indexés comme la limite supérieure de la première tranche du barème de l'impôt sur le revenu. (...) II.-1. Pour l'application du I : a. Le revenu s'entend du revenu du foyer fiscal du contribuable au nom duquel la taxe est établie ; (...) ". Par ailleurs, aux termes de l'article 79 du même code : " Les traitements, indemnités, émoluments, salaires, pensions et rentes viagères concourent à la formation du revenu global servant de base à l'impôt sur le revenu. (...) ". Enfin, selon l'article 80 sexies de ce code : " Pour l'assiette de l'impôt sur le revenu dont sont redevables les assistants maternels (...) régis par les articles L. 421-1 et suivants et L. 423-1 et suivants du code de l'action sociale et des familles, le revenu brut à retenir est égal à la différence entre, d'une part, le total des sommes versées tant à titre de rémunération que d'indemnités pour l'entretien et l'hébergement des enfants et, d'autre part, une somme égale à trois fois le montant horaire du salaire minimum de croissance, par jour et pour chacun des enfants qui leur sont confiés. (...) Le montant de l'abattement retenu pour déterminer la rémunération imposable des assistants maternels (...) ne peut excéder le total des sommes versées tant à titre de rémunération que d'indemnités pour l'entretien et l'hébergement des enfants. (...) ".<br/>
<br/>
              3. Aux termes de l'article L. 421-4 du code de l'action sociale et des familles dans sa rédaction applicable au litige : " L'agrément de l'assistant maternel précise le nombre et l'âge des mineurs qu'il est autorisé à accueillir simultanément ainsi que les horaires de l'accueil. (...) / Le nombre d'enfants pouvant être accueillis simultanément fixé par l'agrément est sans préjudice du nombre de contrats de travail, en cours d'exécution, de l'assistant maternel. (...) ". Aux termes de l'article L. 423-22 du même code : " L'assistant maternel ne peut être employé plus de six jours consécutifs. Le repos hebdomadaire de l'assistant maternel a une durée minimale de vingt-quatre heures auxquelles s'ajoutent les heures consécutives de repos quotidien prévues à l'article L. 423-21. / L'employeur ne peut demander à un assistant maternel de travailler plus de quarante-huit heures par semaine, cette durée étant calculée comme une moyenne sur une période de quatre mois, sans avoir obtenu l'accord de celui-ci et sans respecter des conditions définies par décret. Avec l'accord du salarié, cette durée peut être calculée comme une moyenne sur une période de douze mois, dans le respect d'un plafond annuel de 2 250 heures ".<br/>
<br/>
              4. D'une part, il résulte de la combinaison des dispositions citées aux points 2 et 3 que, pour le calcul de l'abattement prévu par l'article 80 sexies du code général des impôts pour la détermination de l'assiette de l'impôt sur le revenu dont sont redevables les assistants maternels et familiaux, il y a lieu de prendre en compte le nombre d'enfants accueillis simultanément, sans préjudice du nombre de contrats de garde d'enfants en cours d'exécution, dans le respect de l'agrément qui leur est délivré par le président du conseil départemental et dans la limite du nombre d'heures effectives de garde par enfant autorisé par la convention collective nationale des assistants maternels du particulier employeur. Dans le cas où un ou plusieurs enfants sont gardés à temps partiel, l'abattement auquel ils ouvrent droit est réduit au prorata de la durée effective de garde rapportée à la durée habituelle d'accueil journalier définie par cette convention collective. <br/>
<br/>
              5. D'autre part, sous réserve des cas où la loi attribue la charge de la preuve au contribuable, il appartient au juge de l'impôt, au vu de l'instruction et compte tenu, le cas échéant, de l'abstention d'une des parties à produire les éléments qu'elle est seule en mesure d'apporter et qui ne sauraient être réclamés qu'à elle-même, d'apprécier si la situation du contribuable entre dans le champ de l'assujettissement à l'impôt ou, le cas échéant, s'il remplit les conditions légales d'une exonération.<br/>
<br/>
              6. Pour juger que l'administration fiscale n'était pas fondée à remettre en cause le montant des abattements dont les contribuables se sont prévalus sur le fondement de l'article 80 sexies du code général des impôts au titre de l'activité d'assistante maternelle exercée par Mme A... au cours des années 2013 à 2015 et, par voie de conséquence, le dégrèvement de taxe d'habitation dont ils ont bénéficié en application de l'article 1414 A du même code au titre des années 2015 et 2016, le tribunal administratif de Pau a estimé que les bulletins de salaire et les contrats de travail produits par Mme A... comportaient l'ensemble des éléments nécessaires pour établir le nombre d'enfants dont elle a assuré la garde effective, de sorte que l'administration n'était pas fondée à lui réclamer la production des plannings journaliers retraçant le nombre d'enfants gardés et la durée de garde journalière de chacun d'eux. <br/>
<br/>
              7. En statuant ainsi, alors que les plannings journaliers retraçant le nombre d'enfants gardés chaque jour et, pour chacun d'eux, la durée de garde journalière étaient nécessaires pour vérifier si le montant de l'abattement dont Mme A... se prévalait avait été calculé au prorata de la durée effective de garde des enfants qui lui étaient confiés à temps partiel, le tribunal administratif de Pau a entaché son jugement d'une erreur de droit. <br/>
<br/>
              8. Il résulte de ce qui précède que le ministre de l'économie, des finances et de la relance est fondé, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, à demander l'annulation de l'article 2 du jugement qu'il attaque. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 2 du jugement du 7 juin 2018 du tribunal administratif de Pau est annulé.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, au tribunal administratif de Pau.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'économie, des finances et de la relance et à M. et Mme B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
