<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028215083</ID>
<ANCIEN_ID>JG_L_2013_11_000000364007</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/21/50/CETATEXT000028215083.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 14/11/2013, 364007, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-11-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364007</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Louis Dutheillet de Lamothe</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESJS:2013:364007.20131114</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistré le 20 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, présenté par M. A...B..., demeurant ... ; M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir, d'une part, la délibération du 6 juin 2012 par laquelle le conseil d'administration en formation restreinte de l'université Joseph Fourier Grenoble I a déclaré infructueux le concours de recrutement d'un professeur des universités en modélisation géométrique (PR 1238), d'autre part, la décision du 24 septembre 2012 par laquelle le président de cette université a rejeté son recours gracieux ;<br/>
<br/>
              2°) d'enjoindre au conseil d'administration de l'université de prendre, dans les trois mois, une nouvelle délibération aux fins de pourvoir le poste de professeur des universités en modélisation géométrique ;<br/>
<br/>
              3°) de mettre à la charge de l'université Joseph Fourier Grenoble I la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'éducation ;<br/>
<br/>
              Vu le décret n° 84-431 du 6 juin 1984 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Louis Dutheillet de Lamothe, Auditeur,  <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il résulte des dispositions de l'article L. 952-6-1 du code de l'éducation, telles qu'elles ont été interprétées par le Conseil constitutionnel dans sa décision du 6 août 2010 les déclarant conformes à la Constitution, que, pour le recrutement d'un enseignant-chercheur, le comité de sélection, après avoir dressé la liste des candidats qu'il souhaite entendre puis procédé à leur audition, choisit, en sa qualité de jury, ceux des candidats présentant des mérites, notamment scientifiques, suffisants, et, le cas échéant, les classe selon l'ordre de leurs mérites respectifs ; que, par un avis motivé unique portant sur l'ensemble des candidats, il transmet au conseil d'administration la liste de ceux qu'il a retenus, le conseil d'administration ne pouvant ensuite proposer au ministre chargé de l'enseignement supérieur la nomination d'un candidat non sélectionné par le comité ; que le conseil d'administration, siégeant dans une formation restreinte aux enseignants-chercheurs et personnels assimilés de rang au moins égal à celui de l'emploi à pourvoir, prend, au vu de la délibération du comité de sélection, une délibération propre par laquelle il établit sa proposition ; que, dans l'exercice de telles compétences, il incombe au conseil d'administration d'apprécier l'adéquation des candidatures au profil du poste et à la stratégie de l'établissement, sans remettre en cause l'appréciation des mérites scientifiques des candidats retenus par le comité de sélection ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que M. B...a présenté sa candidature au poste mis au concours pour le recrutement d'un professeur des universités en modélisation géométrique à l'université Joseph Fourier Grenoble I ; que, par sa délibération du 25 mai 2012, le comité de sélection de cet établissement l'a classé en première position sur la liste de quatre candidats qu'il a proposée au conseil d'administration de l'université en formation restreinte ; que, pour décider, par sa délibération du 6 juin 2012, de ne pas pourvoir le poste, le conseil d'administration a estimé que " le classement proposé par le comité de sélection ne répond pas complètement aux critères scientifiques de recrutement sur un poste de ce type " ; que cette motivation ne permet pas de comprendre pourquoi le profil de M.B..., tel qu'apprécié souverainement par le comité de sélection, ne correspondait pas aux besoins de l'université pour le poste ouvert au concours ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens de la requête, M. B...est fondé à soutenir que la délibération du conseil d'administration est insuffisamment motivée et à en demander l'annulation ;<br/>
<br/>
              3. Considérant qu'il y a lieu, par voie de conséquence, d'annuler la décision du président de l'université Joseph Fourier Grenoble I du 24 septembre 2012 refusant de convoquer à nouveau le conseil d'administration pour remédier à cette illégalité ;<br/>
<br/>
              4. Considérant que l'annulation de la délibération attaquée implique que le conseil d'administration de l'université Joseph Fourier Grenoble I examine à nouveau les candidatures transmises par le comité de sélection pour le poste de professeur des universités en modélisation géométrique ; qu'il y a donc lieu, pour le Conseil d'Etat, d'ordonner au conseil d'administration de l'université Joseph Fourier Grenoble I de procéder à cet examen dans un délai de trois mois ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'université Joseph Fourier Grenoble I la somme de 1 000 euros à verser à M. B...au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : La délibération du conseil d'administration de l'université Joseph Fourier Grenoble I en formation restreinte du 6 juin 2012 et la décision du 24 septembre 2012 du président de cette université sont annulées. <br/>
Article 2 : Il est enjoint au conseil d'administration de l'université Joseph Fourier Grenoble I en formation restreinte de réexaminer les candidatures retenues par le comité de sélection pour le poste de professeur des universités en modélisation géométrique (PR 1238) dans un délai de trois mois à compter à compter de la notification de la présente décision.<br/>
Article 3 : L'université Joseph Fourier Grenoble I versera une somme de 1 000 euros à M. B...au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et à l'université Joseph Fourier Grenoble I.<br/>
Copie en sera adressée pour information à la ministre de l'enseignement supérieur et de la recherche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
