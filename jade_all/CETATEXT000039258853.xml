<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039258853</ID>
<ANCIEN_ID>JG_L_2019_10_000000422041</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/25/88/CETATEXT000039258853.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 21/10/2019, 422041, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422041</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Pauline Berne</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:422041.20191021</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Par une requête, un mémoire en réplique et deux nouveaux mémoires, enregistrés le 6 juillet 2018 puis les 28 mars, 12 avril et 26 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, le syndicat national de l'enseignement privé - Union nationale des syndicats autonomes (SNEP - UNSA) demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite du ministre de l'éducation nationale et de la jeunesse, née du silence gardé sur sa demande du 23 mars 2018 de modification de l'article 2 du décret du 22 janvier 1985 relatif aux conditions de nomination aux fonctions d'instituteur ou de professeur des écoles maître formateur et, par voie de conséquence, de l'alinéa 9 de l'article 1er de l'arrêté du 11 août 2017 fixant la liste des fonctions particulières des maîtres exerçant dans les établissements d'enseignement privés sous contrat prises en compte pour un avancement au grade de la classe exceptionnelle ;<br/>
<br/>
              2°) d'enjoindre au ministre de l'éducation nationale et de la jeunesse de prononcer les modifications sollicitées ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 100 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ;<br/>
              - le décret n° 85-88 du 22 janvier 1985 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Berne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes, d'une part, de l'article 1er du décret du 22 janvier 1985 relatif aux conditions de nomination aux fonctions d'instituteur ou de professeur des écoles maître formateur : " Il est institué un certificat d'aptitude aux fonctions d'instituteur ou de professeur des écoles maître formateur, qui est exigé des candidats aux fonctions comportant des activités d'animation, de recherche et de formation dans le cadre de la formation initiale et continue des instituteurs ou des professeurs des écoles ". Aux termes de l'article 2 de ce décret : " Le certificat d'aptitude défini à l'article premier ci-dessus est délivré à l'issue d'un examen ouvert aux instituteurs et aux professeurs des écoles titulaires justifiant, au 31 décembre de l'année de l'examen, d'au moins cinq années de services effectifs dans une classe où les instituteurs et les professeurs des écoles ont vocation à exercer ". <br/>
<br/>
              2. Aux termes, d'autre part, de l'article R. 914-60-1 du code de l'éducation : " La liste des fonctions prises en compte pour la promotion à la classe exceptionnelle des maîtres contractuels ou agréés dans les échelles de rémunération concernées est fixée par arrêté du ministre chargé de l'éducation nationale ". Aux termes du 9ème alinéa du 1er article de l'arrêté du 11 août 2017 fixant la liste des fonctions particulières des maîtres exerçant dans les établissements d'enseignement privés sous contrat prises en compte pour un avancement au grade de la classe exceptionnelle : " Les fonctions exercées au sein des établissements d'enseignement privés sous contrat relevant du code de l'éducation prises en compte pour la promotion à la classe exceptionnelle des maîtres contractuels ou agréés, en application des dispositions de l'article R. 914-60-1 susvisé sont le suivantes : (...) les fonctions analogues à celles de maîtres formateur exercées dans les organismes de formation des maîtres de l'enseignement privé sous contrat reconnus par l'Etat pour les maîtres justifiant d'une certification dans le domaine de la formation d'enseignants enregistrée au répertoire national des certifications professionnelles ". <br/>
<br/>
              3. Le syndicat national de l'enseignement privé - Union nationale des syndicats autonomes (SNEP - UNSA) demande l'annulation pour excès de pouvoir de la décision implicite par laquelle le ministre de l'éducation nationale et de la jeunesse a rejeté sa demande, en date du 23 mars 2018, tendant à modifier les dispositions citées au point 1 afin d'ouvrir l'accès au certificat d'aptitude aux fonctions d'instituteur ou de professeur des écoles maître formateur (CAFIPEMF) aux maîtres habilités par agrément ou par contrat à exercer leur fonction dans des établissements d'enseignement privés liés à l'État par contrat, et par voie de conséquence, les dispositions citées au point 2.<br/>
<br/>
              4. En premier lieu, aux termes des premier et cinquième alinéas de l'article L. 914-1 du code de l'éducation : " Les règles générales qui déterminent les conditions de service et de cessation d'activité des maîtres titulaires de l'enseignement public, ainsi que les mesures sociales et les possibilités de formation dont ils bénéficient, sont applicables également et simultanément aux maîtres justifiant du même niveau de formation, habilités par agrément ou par contrat à exercer leur fonction dans des établissements d'enseignement privés liés à l'Etat par contrat. Ces maîtres bénéficient également des mesures de promotion et d'avancement prises en faveur des maîtres de l'enseignement public / (...) Les charges afférentes à la formation initiale et continue des maîtres susvisés sont financées par l'État aux mêmes niveaux et dans les mêmes limites que ceux qui sont retenus pour la formation initiale et continue des maîtres de l'enseignement public. Elles font l'objet de conventions conclues avec les personnes physiques ou morales qui assurent cette formation dans le respect du caractère propre de l'établissement visé à l'article L. 442-1 et des accords qui régissent l'organisation de l'emploi et celle de la formation professionnelle des personnels dans l'enseignement privé sous contrat ". Il en résulte que si les maîtres qui sont habilités par agrément ou par contrat à exercer leur fonction dans des établissements d'enseignement privés liés à l'Etat par contrat et qui ont le même niveau de formation que les maîtres titulaires de l'enseignement public doivent bénéficier des mêmes possibilités de formation que ces derniers, leur formation doit, néanmoins, être assurée dans le respect, notamment, du caractère propre de leurs établissements. <br/>
<br/>
              5. Il ressort des pièces du dossier que la certification aux fonctions de " Formateur d'enseignants, de formateurs et de cadres pédagogiques ", qui a été enregistrée au Répertoire national des certifications professionnelles sous le n°15850 par arrêté du ministre du travail du 9 avril 2018, est accessible à tous les maîtres habilités par agrément ou par contrat à exercer leur fonction dans des établissements d'enseignement privés liés à l'Etat par contrat. Il n'est pas établi que cette certification, qui tient compte du caractère propre des établissements où ces maîtres sont affectés, ne permettrait pas à ces derniers de disposer des mêmes possibilités de formation que celle que le décret contesté réserve aux seuls instituteurs et professeurs des écoles titulaires de l'enseignement public. Par suite, le moyen tiré de ce que les dispositions citées au point 1 méconnaissent l'article L. 914-1 du code de l'éducation doit être écarté.<br/>
<br/>
              6. En deuxième lieu, le principe d'égalité ne s'oppose pas à ce que l'autorité investie du pouvoir réglementaire règle de façon différente des situations différentes ni à ce qu'elle déroge à l'égalité pour des raisons d'intérêt général, pourvu que dans l'un comme dans l'autre cas, la différence de traitement qui en résulte soit en rapport avec l'objet de la norme qui l'établit et ne soit pas manifestement disproportionnée au regard des motifs susceptibles de la justifier. Alors même que les maîtres habilités par agrément ou par contrat à exercer leur fonction dans des établissements d'enseignement privés liés à l'État par contrat sont des agents publics, ils sont placés dans une situation différente des maîtres titulaires de l'enseignement public, seuls ces derniers pouvant être affectés dans les établissements d'enseignement publics. La différence de traitement entre les premiers et les seconds en matière d'accès aux certifications précitées est en rapport avec l'objet des dispositions contestées qui est d'assurer la formation des futurs formateurs pour chaque type d'établissements. Le moyen tiré de ce que le décret contesté méconnaîtrait le principe d'égalité doit, dès lors, être écarté.<br/>
<br/>
              7. En troisième lieu, il résulte de l'article 1er de l'arrêté du 10 mai 2017 fixant la liste des conditions d'exercice et des fonctions particulières des personnels des corps enseignants d'éducation et de psychologue au ministère de l'éducation nationale, de l'enseignement supérieur et de la recherche prises en compte pour un avancement à la classe exceptionnelle, qui est applicable aux maîtres titulaires de l'enseignement public, que sont au nombre des fonctions que cet article énumère, celles de maître formateur visées par le décret du 22 janvier 1985. Les dispositions mentionnées au point 2 prévoient quant à elles que sont au nombre des fonctions prises en compte pour un avancement à la classe exceptionnelle des maîtres contractuels ou agréés des établissements d'enseignement privés sous contrat les fonctions analogues à celles de maître formateur exercées dans les organismes de formation des maîtres de l'enseignement privé sous contrat reconnus par l'Etat pour les maîtres justifiant d'une certification dans le domaine de la formation d'enseignants enregistrée au répertoire national des certifications professionnelles. Compte tenu de ce qui a été jugé au point 5 concernant l'équivalence des possibilités de certification ouvertes aux maîtres titulaires de l'enseignement public et aux maîtres contractuels ou agréés des établissements d'enseignement privés sous contrat, et dès lors que les fonctions de maître formateur exercées par les premiers et celles analogues exercées par les seconds sont prises en compte de manière identique pour leur avancement à la classe exceptionnelle, en application respectivement de l'arrêté du 10 mai 2017 et de l'arrêté du 11 août 2017, le moyen tiré de ce que les dispositions visées au point 2 méconnaîtraient le principe posé par l'article R. 914-60 du code de l'éducation selon lesquels ces deux catégories de maîtres doivent être évaluées dans les mêmes conditions doit être écarté.<br/>
<br/>
              8. Il résulte de tout ce qui précède que le syndicat requérant n'est pas fondé à demander l'annulation de la décision qu'il attaque. Ses conclusions présentées à fin d'injonction et au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par conséquent, qu'être également rejetées.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête du syndicat national de l'enseignement privé - Union nationale des syndicats autonomes (SNEP - UNSA) est rejetée.<br/>
Article 2 : La présente décision sera notifiée au SNEP - UNSA, au Premier ministre et au ministre de l'éducation nationale et de la jeunesse.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
