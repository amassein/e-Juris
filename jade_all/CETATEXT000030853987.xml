<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030853987</ID>
<ANCIEN_ID>JG_L_2015_07_000000390830</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/85/39/CETATEXT000030853987.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 01/07/2015, 390830, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390830</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2015:390830.20150701</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique enregistrés les  19  et 26 juin 2015 au secrétariat du contentieux du Conseil d'Etat, l'association des utilisateurs et distributeurs de l'agrochimie européenne (AUDACE) demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de l'arrêté du ministre de l'agriculture, de l'agroalimentaire et de la forêt du 27 mars 2015 fixant le taux de la taxe sur la vente de produits  phytopharmaceutiques ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - il existe un doute sérieux quant à la légalité de l'arrêté contesté ; <br/>
              - le ministre des finances et des comptes publics et le ministre de l'agriculture, de l'agroalimentaire et de la forêt n'étaient pas compétents pour prendre l'arrêté litigieux ; <br/>
              - la loi pour l'application de laquelle l'arrêté litigieux a été pris méconnaît les articles 28, 52, 56 et 74 du règlement communautaire 1107/2009, ainsi que les articles 110 et 34 du traité sur le fonctionnement de l'Union européenne ;<br/>
              - l'arrêté comporte un effet rétroactif contraire à l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la condition d'urgence est remplie dès lors que l'arrêté porte une atteinte grave et immédiate aux intérêts qu'elle défend et à un intérêt public.<br/>
<br/>
<br/>
              Vu l'arrêté dont la suspension de l'exécution est demandée ;<br/>
<br/>
              Vu la copie de la requête à fin d'annulation de cet arrêté ; <br/>
<br/>
              Par un mémoire en défense, enregistré le 19 juin 2015, le ministre de l'agriculture, de l'agroalimentaire et de la forêt conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par la requérante ne sont pas fondés.<br/>
<br/>
              Par un mémoire en réplique, enregistré le 26 juin 2015, l'association des utilisateurs et distributeurs de l'agrochimie européenne conclut aux mêmes fins par les mêmes moyens ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code rural et de la pêche maritime ;<br/>
              - la loi n° 2014-1655 du 29 décembre 2014 de finances rectificative pour 2014, notamment son article 104 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, l'association des utilisateurs et distributeurs de l'agrochimie européenne et, d'autre part, le ministre de l'agriculture, de l'agroalimentaire et de la forêt ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 29 juin 2015 à 10 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Blancpain, avocat au Conseil d'État et à la Cour de cassation, avocat de l'association des utilisateurs et distributeurs de l'agrochimie européenne ;<br/>
<br/>
              - les représentants de l'association des utilisateurs et distributeurs de l'agrochimie européenne ;<br/>
<br/>
              - les représentants du ministre de l'agriculture, de l'agroalimentaire et de la forêt ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ; <br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; que l'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celui-ci porte atteinte de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ; que l'urgence doit être appréciée objectivement et, s'agissant d'un acte réglementaire, en tenant compte de la nature et de l'importance des modifications qu'il apporte à l'état antérieur du droit ; <br/>
<br/>
              2. Considérant que l'article 104 de la loi n° 2014-1655 du 29 décembre 2014 a instauré une taxe sur les ventes de produits phytopharmaceutiques bénéficiant d'une autorisation de mise sur le marché ou d'un permis de commerce parallèle, due par le titulaire de l'autorisation ou du permis ; que l'arrêté du 27 mars 2015 dont la suspension est demandée fixe le taux de la taxe à 0,2 % du chiffre d'affaires hormis le produit des ventes à l'exportation ; que, par un courrier du 17 avril 2015, l'Agence nationale de sécurité sanitaire de l'alimentation, de l'environnement et du travail, chargée par le législateur du recouvrement de cette taxe, a demandé aux personnes redevables de procéder à son paiement au titre de leur chiffre d'affaires de l'année 2014 ; qu'il résulte des débats lors de l'audience publique que les personnes titulaires d'un permis de commerce parallèle, dont la requérante défend les intérêts, n'ont pas encore procédé à ce paiement ;<br/>
<br/>
              3. Considérant qu'à supposer que le paiement de la taxe sur les ventes de produits phytopharmaceutiques porterait une atteinte grave et immédiate à la situation des personnes redevables, il serait loisible à ces dernières, si elles s'y croyaient fondées, de former devant le tribunal administratif un recours aux fins d'annulation des titres de recouvrement qui pourraient être émis à leur encontre ; qu'un tel recours aurait pour effet de suspendre l'exécution de ces titres ; que, dans ces conditions, il n'apparaît pas que l'urgence justifie la suspension de l'exécution de l'arrêté du 27 mars 2015 fixant le taux de la taxe en question ; qu'il s'ensuit que les conclusions tendant à la suspension de l'exécution de cet arrêté doivent être rejetées, ainsi que les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de l'association des utilisateurs et distributeurs de l'agrochimie européenne est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à l'association des utilisateurs et distributeurs de l'agrochimie européenne et au ministre de l'agriculture, de l'agroalimentaire et de la forêt.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
