<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041647187</ID>
<ANCIEN_ID>JG_L_2020_02_000000424335</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/64/71/CETATEXT000041647187.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 26/02/2020, 424335</TITRE>
<DATE_DEC>2020-02-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424335</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Damien Pons</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:424335.20200226</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A... B... a demandé au tribunal administratif de Lyon d'annuler la décision du 13 septembre 2017 par laquelle le président de la métropole de Lyon, à la suite de son recours administratif préalable obligatoire, a confirmé la réduction, à compter de juin 2017, du montant du revenu de solidarité active dont elle bénéficiait et d'enjoindre au président de la métropole de Lyon de lui accorder le bénéfice du revenu de solidarité active dans un délai de quinze jours ou, à tout le moins, de statuer à nouveau sur sa demande.<br/>
<br/>
              Par un jugement n° 1709022 du 19 juillet 2018, le tribunal administratif de Lyon a annulé la décision du président de la métropole de Lyon et a renvoyé Mme B... devant ce dernier pour la détermination de son droit au revenu de solidarité active à compter du mois de juin 2017.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 19 septembre et 19 décembre 2018 au secrétariat du contentieux du Conseil d'Etat, la métropole de Lyon demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de Mme B... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Damien Pons, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Texidor, Perier, avocat de la métropole de Lyon ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que le montant du revenu de solidarité active dont bénéficiait Mme B..., gérante et associée unique de la société Cabinet Social, entreprise unipersonnelle à responsabilité limitée assujettie à l'impôt sur les sociétés, a été réduit à partir du mois de juin 2017, en raison de la prise en compte dans ses ressources du bénéfice de 11 383 euros dégagé par la société au titre de l'année 2016, correspondant à un revenu mensuel moyen de 949 euros. Saisi d'un recours administratif préalable par Mme B..., le président de la métropole de Lyon, par une décision du 13 septembre 2017, a confirmé la réduction du montant du revenu de solidarité active qu'elle percevait. Par un jugement du 19 juillet 2018, contre lequel la métropole de Lyon se pourvoit en cassation, le tribunal administratif de Lyon a annulé cette décision et renvoyé Mme B... devant le président de la métropole pour la détermination de ses droits au revenu de solidarité active à compter du mois de juin 2017.<br/>
<br/>
              2. L'article L. 262-2 du code de l'action sociale et des familles dispose que : " (...) Le revenu de solidarité active est une allocation qui porte les ressources du foyer au niveau du montant forfaitaire (...) ". Aux termes de l'article L. 262-3 du même code: " (...) L'ensemble des ressources du foyer, y compris celles qui sont mentionnées à l'article L. 132-1, est pris en compte pour le calcul du revenu de solidarité active, dans des conditions fixées par un décret en Conseil d'Etat qui détermine notamment : / (...) 2° Les modalités d'évaluation des ressources (...) ". Le premier alinéa de l'article L. 132-1 de ce code dispose que : " Il est tenu compte, pour l'appréciation des ressources des postulants à l'aide sociale, des revenus professionnels et autres et de la valeur en capital des biens non productifs de revenu, qui est évaluée dans les conditions fixées par voie réglementaire. (...) ". Aux termes de l'article R. 262-6 du même code : " Les ressources prises en compte pour la détermination du montant du revenu de solidarité active comprennent, sous les réserves et selon les modalités figurant au présent chapitre, l'ensemble des ressources, de quelque nature qu'elles soient, de toutes les personnes composant le foyer, et notamment les avantages en nature ainsi que les revenus procurés par des biens mobiliers et immobiliers et par des capitaux. / Les dispositions de l'article R. 132-1 sont applicables au revenu de solidarité active ". Enfin, l'article R. 132-1 de ce code prévoit que : " Pour l'appréciation des ressources des postulants prévue à l'article L. 132-1, les biens non productifs de revenu, à l'exclusion de ceux constituant l'habitation principale du demandeur, sont considérés comme procurant un revenu annuel égal à 50 % de leur valeur locative s'il s'agit d'immeubles bâtis, à 80 % de cette valeur s'il s'agit de terrains non bâtis et à 3 % du montant des capitaux ".<br/>
<br/>
              3. Pour l'application de ces dispositions, lorsque le bénéficiaire du revenu de solidarité active détient des parts d'une société à responsabilité limitée ou d'une entreprise unipersonnelle à responsabilité limitée et n'est pas soumis aux dispositions des articles R. 262-18 ou R. 262-19 du code de l'action sociale et des familles applicables aux revenus professionnels relevant de l'impôt sur le revenu dans la catégorie des bénéfices agricoles, des bénéfices industriels et commerciaux ou des bénéfices non commerciaux, du fait des bénéfices dégagés par cette société, il y a lieu, pour déterminer le montant des ressources qu'il retire de ces parts, de tenir compte des seuls bénéfices de la société dont il a effectivement disposé, c'est-à-dire qui lui ont été distribués. A défaut de distribution de tout ou partie des bénéfices réalisés par la société, ces ressources ne peuvent être évaluées que sur la base forfaitaire, applicable aux biens non productifs de revenus, prévue par les articles L. 132-1 et R. 132-1 du code de l'action sociale et des familles. Pour déterminer la valeur des parts sociales à laquelle appliquer le taux de 3 %, l'administration et, le cas échéant, le juge peuvent tenir compte de leur valeur nominale, sauf à disposer d'éléments leur permettant de déterminer une valeur aussi proche que possible, à la date où les ressources sont évaluées, de celle qu'aurait entraîné le jeu normal de l'offre et de la demande, par exemple en s'appuyant sur le montant de l'actif net comptable de la société.<br/>
<br/>
              4. Par suite, en jugeant qu'aucune disposition législative ou réglementaire ne permet, pour déterminer les droits au revenu de solidarité active, de tenir compte des bénéfices non distribués d'une société commerciale dont l'allocataire détient des droits sociaux et en s'abstenant de procéder à l'évaluation forfaitaire des ressources que l'allocataire est supposé pouvoir retirer dans une telle hypothèse des parts sociales qu'il détient, dont la valeur peut être appréciée, notamment, en tenant compte des bénéfices dégagés par la société, le tribunal administratif a commis une erreur de droit.<br/>
<br/>
              5. Il résulte de ce qui précède que la métropole de Lyon est fondée, sans qu'il soit besoin de se prononcer sur les autres moyens de son pourvoi, à demander l'annulation du jugement qu'elle attaque. <br/>
<br/>
              6. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme B... la somme que la métropole de Lyon demande au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Lyon du 19 juillet 2018 est annulé. <br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Lyon. <br/>
Article 3 : Les conclusions présentées par la métropole de Lyon sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la métropole de Lyon et à Mme A... B....<br/>
Copie en sera adressée à la ministre des solidarités et de la santé. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-06 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. REVENU MINIMUM D'INSERTION (RMI). - RSA - RESSOURCES PRISES EN COMPTE POUR LE CALCUL DE L'ALLOCATION - ALLOCATAIRE DÉTENTEUR DE PARTS D'UNE SARL OU D'UNE EURL ET NON SOUMIS AUX RÈGLES APPLICABLES AUX BÉNÉFICES AGRICOLES, BIC OU BNC [RJ1] - 1) PRISE EN COMPTE DES SEULS BÉNÉFICES EFFECTIVEMENT DISTRIBUÉS PAR LA SOCIÉTÉ [RJ2] - 2) CAS DE L'ABSENCE DE DISTRIBUTION DE BÉNÉFICES - A) APPLICATION DE L'ÉVALUATION FORFAITAIRE (ART. L. 132-1 ET R. 132-1 DU CASF) [RJ3] - B) DÉTERMINATION DE LA VALEUR DES PARTS SOCIALES - MÉTHODE [RJ4].
</SCT>
<ANA ID="9A"> 04-02-06 1) Pour l'application des articles L. 132-1, L. 262-2, L. 262-3, R. 262-6 et R. 132-1 du code de l'action sociale et des familles (CASF), lorsque le bénéficiaire du revenu de solidarité active (RSA) détient des parts d'une société à responsabilité limitée (SARL) ou d'une entreprise unipersonnelle à responsabilité limitée (EURL) et n'est pas soumis aux dispositions des articles R. 262-18 ou R. 262-19 du CASF applicables aux revenus professionnels relevant de l'impôt sur le revenu dans la catégorie des bénéfices agricoles, des bénéfices industriels et commerciaux (BIC) ou des bénéfices non commerciaux (BNC), du fait des bénéfices dégagés par cette société, il y a lieu, pour déterminer le montant des ressources qu'il retire de ces parts, de tenir compte des seuls bénéfices de la société dont il a effectivement disposé, c'est-à-dire qui lui ont été distribués.... ,,2) a) A défaut de distribution de tout ou partie des bénéfices réalisés par la société, ces ressources ne peuvent être évaluées que sur la base forfaitaire, applicable aux biens non productifs de revenus, prévue par les articles L. 132-1 et R. 132-1 du CASF.... ,,b) Pour déterminer la valeur des parts sociales à laquelle appliquer le taux de 3 %, l'administration et, le cas échéant, le juge peuvent tenir compte de leur valeur nominale, sauf à disposer d'éléments leur permettant de déterminer une valeur aussi proche que possible, à la date où les ressources sont évaluées, de celle qu'aurait entraîné le jeu normal de l'offre et de la demande, par exemple en s'appuyant sur le montant de l'actif net comptable de la société.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur le régime applicable selon que l'allocataire est propriétaire d'un immeuble ou de parts d'une SCI, CE, décision du même jour, Métropole de Lyon, n° 424379, à mentionner aux Tables.,,[RJ2] Cf. CE, 14 juin 2017,,et,, n° 401637, T. p. 465.,,[RJ3] Rappr., pour les parts d'une société anonyme, CE, 19 juillet 2018,,, n° 412075, inédite au Recueil ; pour un immeuble non productif de revenus situé à l'étranger, CE, 8 juillet 2019,,, n° 422162, à mentionner aux Tables.,,[RJ4] Rappr., s'agissant de l'évaluation de la valeur vénale des titres d'une société non cotée, CE, 30 septembre 2019, Société Hôtel Restaurant Luccotel, n° 419855, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
