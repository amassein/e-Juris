<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037882307</ID>
<ANCIEN_ID>JG_L_2018_12_000000419880</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/88/23/CETATEXT000037882307.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 26/12/2018, 419880, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419880</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; GOLDMAN</AVOCATS>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:419880.20181226</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Lyon : <br/>
              - d'annuler la décision du 2 février 2016 de la caisse d'allocations familiales du Rhône en tant qu'elle porte sur la récupération d'un indu d'aides exceptionnelles de fin d'année au titre de 2013, 2014, et 2015 et la décision du 16 novembre 2016 rejetant son recours gracieux contre cette décision ; <br/>
              - de la décharger de l'obligation de payer cette somme et d'enjoindre à la caisse d'allocations familiales du Rhône de lui rembourser les sommes retenues ;<br/>
              - d'annuler la décision du 19 juillet 2016 par laquelle le président de la métropole de Lyon a rejeté son recours administratif préalable obligatoire formé le 31 mars 2016 contre la décision du 2 février 2016 en tant qu'elle prononce la récupération d'indus de revenu de solidarité active ;<br/>
              - de la décharger de l'obligation de payer cette somme et d'enjoindre à la caisse d'allocations familiales du Rhône de lui rembourser les sommes retenues ;<br/>
              - subsidiairement, de faire droit à sa demande de remise gracieuse de ces indus. <br/>
<br/>
              Par un jugement n°s 1608150, 1608780 du 21 novembre 2017, le tribunal administratif de Lyon a réformé la décision du président de la métropole de Lyon du 19 juillet 2016 pour ramener le montant de l'indu de revenu de solidarité active à 4 455,02 euros, a annulé les décisions de la caisse d'allocations familiales du Rhône des 2 février et 16 novembre 2016 en tant qu'elles portent sur l'indu d'aide exceptionnelle de fin d'année au titre de l'année 2015, a déchargé Mme B...de l'obligation de rembourser les sommes correspondant à des indus de revenu de solidarité active au titre de la période du 1er juillet 2015 au 31 décembre 2015 et à un indu d'aide exceptionnelle de fin d'année 2015 et a rejeté le surplus des conclusions de sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 16 avril et 16 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat d'annuler le jugement du tribunal administratif de Lyon du 21 novembre 2017 en tant qu'il rejette le surplus des conclusions de sa demande. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - l'arrêté du 5 mai 2014 fixant les conditions d'agrément des agents et des praticiens-conseils chargés du contrôle de l'application des législations de sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Goldman, avocat de MmeB..., et à Me Le Prado, avocat de la métropole de Lyon.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que MmeB..., bénéficiaire du revenu de solidarité active depuis décembre 2013, a fait l'objet d'un contrôle de la caisse d'allocations familiales du Rhône le 8 juin 2015, à la suite duquel cette caisse lui a réclamé, par une décision du 2 février 2016, le remboursement d'un indu de revenu de solidarité active pour la période du 1er décembre 2013 au 31 décembre 2015 et d'un indu d'aides exceptionnelles de fin d'année versées au titre de 2013, 2014 et 2015. MmeB..., après avoir formé un recours administratif contre cette décision de récupération et sollicité la remise gracieuse de sa dette, a saisi le tribunal administratif de Lyon. Elle se pourvoit en cassation contre le jugement du 21 novembre 2017 en tant qu'il rejette les conclusions de sa demande relatives à la récupération et au refus de remise gracieuse de l'indu de revenu de solidarité active pour la période du 1er décembre 2013 au 30 juin 2015 et de l'indu d'aides exceptionnelles de fin d'année au titre de 2013 et 2014, par des moyens qui portent, d'une part, sur la récupération de l'indu de revenu de solidarité active et, d'autre part, sur le refus de remise gracieuse.  <br/>
<br/>
              Sur le jugement, en tant qu'il statue sur la récupération de l'indu de revenu de solidarité active pour la période du 1er décembre 2013 au 30 juin 2015 :<br/>
<br/>
              2. Aux termes de l'article L. 262-40 du code de l'action sociale et des familles : " (...) Les organismes chargés de son versement réalisent les contrôles relatifs au revenu de solidarité active selon les règles, procédures et moyens d'investigation applicables aux prestations de sécurité sociale. (...) ". Selon le premier alinéa de l'article L. 114-10 du code de la sécurité sociale, dans sa rédaction applicable au litige : " Les directeurs des organismes de sécurité sociale confient à des agents chargés du contrôle, assermentés et agréés dans des conditions définies par arrêté du ministre chargé de la sécurité sociale, le soin de procéder à toutes vérifications ou enquêtes administratives concernant l'attribution des prestations et la tarification des accidents du travail et des maladies professionnelles. (...) Ces agents ont qualité pour dresser des procès-verbaux faisant foi jusqu'à preuve du contraire ". Les conditions d'agrément des agents des caisses d'allocations familiales exerçant une mission de contrôle sont définies par un arrêté du ministre des affaires sociales et de la santé du 5 mai 2014, pour les agents qui n'étaient pas déjà en fonction à sa date d'entrée en vigueur.<br/>
<br/>
              3. Il résulte de l'ensemble de ces dispositions que tant l'absence d'agrément que l'absence d'assermentation des agents de droit privé désignés par les caisses d'allocations familiales pour conduire des contrôles sur les déclarations des bénéficiaires du revenu de solidarité active sont de nature à affecter la validité des constatations des procès-verbaux qu'ils établissent à l'issue de ces contrôles et à faire ainsi obstacle à ce qu'elles constituent le fondement d'une décision déterminant pour l'avenir les droits de la personne contrôlée ou remettant en cause des paiements déjà effectués à son profit en ordonnant la récupération d'un indu. En outre, la valeur probante attachée par les dispositions précitées de l'article L. 114-10 du code de la sécurité sociale aux procès-verbaux dressés par ces agents ne saurait s'étendre aux mentions qu'ils comportent quant à l'agrément et à l'assermentation de leur auteur.<br/>
<br/>
              4. Le tribunal administratif de Lyon, saisi d'une contestation portant tant sur l'agrément que sur l'assermentation de l'agent de la caisse d'allocations familiales du Rhône qui a procédé le 8 juin 2015 au contrôle de la situation de MmeB..., s'est fondé, pour écarter ce moyen, sur la copie, produite par la métropole de Lyon, de la décision d'agrément de l'agent de contrôle du 9 décembre 2014. En statuant ainsi, alors que cette décision ne faisait aucunement mention de l'assermentation de l'agent et qu'il lui appartenait, le cas échéant, à défaut d'élément produit en défense sur ce point de nature à lui permettre de former sa conviction, de mettre en oeuvre ses pouvoirs d'instruction en invitant l'administration à compléter ses productions, le tribunal a commis une erreur de droit. Mme B...est, en conséquence, fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi dirigés contre le jugement en tant qu'il statue sur la récupération de l'indu de revenu de solidarité active pour la période du 1er décembre 2013 au 30 juin 2015, à en demander l'annulation dans cette mesure.<br/>
<br/>
              Sur le jugement, en tant qu'il statue sur la demande de remise gracieuse :<br/>
<br/>
              5. Aux termes de l'article L. 262-46 du code de l'action sociale et des familles : " Tout paiement indu de revenu de solidarité active est récupéré par l'organisme chargé du service de celui-ci ainsi que, dans les conditions définies au présent article, par les collectivités débitrices du revenu de solidarité active. / (...) La créance peut être remise ou réduite par le président du conseil départemental en cas de bonne foi ou de précarité de la situation du débiteur, sauf si cette créance résulte d'une manoeuvre frauduleuse ou d'une fausse déclaration ". Il résulte de ces dispositions qu'un allocataire du revenu de solidarité active ne peut bénéficier d'une remise gracieuse de la dette résultant d'un paiement indu d'allocation, quelle que soit la précarité de sa situation, lorsque l'indu trouve sa cause dans une manoeuvre frauduleuse de sa part ou dans une fausse déclaration, laquelle doit s'entendre comme désignant les inexactitudes ou omissions qui procèdent d'une volonté de dissimulation de l'allocataire caractérisant de sa part un manquement à ses obligations déclaratives.<br/>
<br/>
              6. Le tribunal a rejeté les conclusions de Mme B...tendant à la remise gracieuse des indus qui lui étaient réclamés au motif qu'ils trouvaient leur cause dans une fausse déclaration de sa part. S'il a également relevé que la requérante, au surplus, n'apportait aucune justification ni élément probant de nature à établir que le remboursement de ces indus excèderait ses capacités contributives, ce motif revêtait un caractère surabondant. Dès lors, le moyen tiré de ce que le tribunal aurait ainsi dénaturé les pièces du dossier qui lui était soumis ne saurait, quel qu'en soit le bien-fondé, entraîner l'annulation du jugement attaqué.<br/>
<br/>
              7. Il résulte de tout ce qui précède que Mme B...est fondée à demander l'annulation du jugement qu'elle attaque en tant seulement qu'il rejette ses conclusions relatives à la récupération d'un indu de revenu de solidarité active pour la période du 1er décembre 2013 au 30 juin 2015.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er et 4 du jugement du tribunal administratif de Lyon du 21 novembre 2017 sont annulés en tant qu'ils rejettent les conclusions de Mme B...relatives à la récupération d'un indu de revenu de solidarité active pour la période du 1er décembre 2013 au 30 juin 2015.<br/>
Article 2 : Le surplus des conclusions du pourvoi de Mme B...est rejeté.<br/>
Article 3 : La présente décision sera notifiée à Mme A...B..., à la métropole de Lyon et à la ministre des solidarités et de la santé.<br/>
Copie en sera adressée à la caisse d'allocations familiales du Rhône.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
