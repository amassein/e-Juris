<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033364642</ID>
<ANCIEN_ID>JG_L_2016_11_000000393904</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/36/46/CETATEXT000033364642.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 09/11/2016, 393904, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-11-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393904</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP DELVOLVE ET TRICHET</AVOCATS>
<RAPPORTEUR>Mme Marie Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:393904.20161109</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Paris d'ordonner une expertise médicale en vue d'obtenir tous les éléments utiles sur les préjudices qu'elle estime avoir subis du fait de la prise du Mediator et de condamner solidairement l'Etat et l'Agence nationale de sécurité du médicament et des produits de santé à lui verser une provision de 15 000 euros à valoir sur l'indemnisation de ses préjudices. Par un jugement n° 1312466 du 7 août 2014, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14PA04146 du 31 juillet 2015, la cour administrative d'appel de Paris a rejeté l'appel formé contre ce jugement par MmeA..., qui ramenait à  10 000 euros la somme demandée à titre de provision.<br/>
<br/>
              Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 5 octobre 2015 et 6 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt de la cour administrative d'appel de Paris du 31 juillet 2015 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Sirinelli, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delvolvé et Trichet, avocat de Mme A...et à Me Le Prado, avocat de l'Agence nationale de sécurité du médicament et des produits de santé.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que, par un jugement du 7 août 2014, le tribunal administratif de Paris a jugé que l'absence de suspension ou de retrait de l'autorisation de mise sur le marché du Mediator était constitutive d'une carence fautive de l'Agence française de sécurité sanitaire des produits de santé de nature à engager la responsabilité de l'Etat à partir du 7 juillet 1999 et a rejeté la demande de Mme A...tendant à la désignation d'un expert et à l'octroi d'une provision au motif qu'elle avait été exposée au Mediator en 1996 et 1997 ; que, par un arrêt du 31 juillet 2015, la cour administrative d'appel de Paris a rejeté l'appel de Mme A...contre ce jugement ; que Mme A...demande l'annulation de cet arrêt en le critiquant en tant que, conformément au jugement du 7 août 2014, il n'a retenu l'existence d'une faute de l'Etat qu'à compter du 7 juillet 1999 ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société les Laboratoires Servier a obtenu en 1974 une autorisation de mise sur le marché de sa spécialité Mediator, ayant pour principe actif le benfluorex, initialement indiquée dans certaines hypercholestérolémies et hypertriglycéridémies endogènes de l'adulte et comme adjuvant du régime dans le diabète asymptomatique avec surcharge pondérale ; qu'en dépit d'une structure chimique différant pour partie de celle des fenfluramines, le benfluorex est transformé, lors de son absorption, en plusieurs métabolites, dont la norfenfluramine, qui est également le principal métabolite des fenfluramines ; que le danger des fenfluramines, dérivés de l'amphétamine utilisés pour leurs propriétés anorexigènes, était suffisamment établi en 1995 pour que les autorités sanitaires françaises décident de restreindre leur prescription, puis, en 1997, suspendent les autorisations de mise sur le marché des spécialités à base de ce principe actif ; que, toutefois, il n'a été procédé à la suspension puis au retrait de l'autorisation de mise sur le marché du Mediator qu'en novembre 2009 et juillet 2010 ;<br/>
<br/>
              3. Considérant qu'en vertu de l'article L. 601 du code de la santé publique, une spécialité pharmaceutique ne pouvait être distribuée sans avoir reçu au préalable une autorisation de mise sur le marché, délivrée par le ministre chargé de la santé pour une durée de cinq ans renouvelable, subordonnée à la justification par le fabricant, notamment, qu'il avait fait procéder à la vérification de l'intérêt thérapeutique du produit et de son innocuité dans des conditions normales d'emploi, et susceptible d'être suspendue ou retirée ; que la loi du 4 janvier 1993 relative à la sécurité en matière de transfusion sanguine et de médicament a transféré cette compétence à la nouvelle Agence du médicament qu'elle a créée et a inséré dans le code de la santé publique un article L. 567-2 disposant que cette agence était notamment chargée : " 1° De participer à l'application des lois et règlements relatifs : a) Aux essais, à la fabrication, à l'importation, à l'exportation, à la mise sur le marché des médicaments à usage humain (...) ; 3° De recueillir et d'évaluer les informations sur les effets inattendus ou toxiques des médicaments et produits mentionnés au a du 1° (...) ; 7° de procéder à toutes expertises et contrôles techniques relatifs à la qualité : a) Des produits et objets mentionnés au présent article (...) ; 10° De recueillir les données, notamment en terme d'évaluation scientifique et technique, nécessaires à la préparation des décisions relatives à la politique du médicament et de participer à l'application des décisions prises en la matière (...) " ; que l'article L. 793-1 du même code, issu de la loi du 1er juillet 1998 relative au renforcement de la veille sanitaire et du contrôle de la sécurité sanitaire des produits destinés à l'homme, entré en vigueur le 9 mars 1999, a substitué à l'Agence du médicament un nouvel établissement public, l'Agence française de sécurité sanitaire des produits de santé, en prévoyant notamment que celle-ci " procède à l'évaluation des bénéfices et des risques liés à l'utilisation [des médicaments] à tout moment opportun et notamment lorsqu'un élément nouveau est susceptible de remettre en cause l'évaluation initiale " ; que son article L. 793-2 prévoyait, en outre, que cette agence " procède ou fait procéder à toute expertise et à tout contrôle technique " relatifs, notamment, aux médicaments et qu'elle " recueille et évalue les informations sur les effets inattendus, indésirables ou néfastes " des médicaments ; qu'enfin, selon la période considérée, le ministre chargé de la santé ou le directeur général de ces agences, agissant au nom de l'Etat en vertu successivement des articles L. 567-4 et L. 793-4 du code de la santé publique, pouvait décider de suspendre ou de retirer l'autorisation de mise sur le marché d'un médicament pour les motifs précisés à l'article R. 5139 du code de la santé publique, notamment " lorsqu'il apparaît que la spécialité pharmaceutique est nocive dans les conditions normales d'emploi ou que l'effet thérapeutique fait défaut " ;<br/>
<br/>
              4. Considérant, en premier lieu, qu'eu égard tant à la nature des pouvoirs conférés par ces dispositions aux autorités chargées de la police sanitaire relative aux médicaments, qu'aux buts en vue desquels ces pouvoirs leur ont été attribués, la responsabilité de l'Etat peut être engagée par toute faute commise dans l'exercice de ces attributions ; que si la cour a fait mention, pour apprécier la responsabilité de l'Etat au cours de la période allant de l'autorisation de mise sur le marché du Mediator, en 1974, à l'année 1994, des moyens de contrôle à la disposition de l'administration et de sa méconnaissance des risques liés à la prise de ce médicament, il ne ressort pas des motifs de son arrêt qu'elle aurait subordonné la reconnaissance de la responsabilité de l'Etat, durant cette période, à la caractérisation d'une faute d'une certaine gravité ; qu'ainsi, la requérante n'est pas fondée à soutenir que la cour aurait, pour cette raison, commis une erreur de droit ;<br/>
<br/>
              5. Considérant, en second lieu, que la cour administrative d'appel de Paris a jugé qu'en 1995, la proximité pharmacologique entre le benfluorex et les fenfluramines restait mal connue et que les autorités sanitaires ne disposaient pas d'information sur l'existence d'effets indésirables en lien avec le benfluorex ; que ce n'est qu'à compter de la mi-1999, compte tenu des nouveaux éléments d'information dont disposaient alors les autorités sanitaires, notamment sur les effets indésirables du benfluorex et sur la concentration sanguine en norfenfluramine à la suite de son absorption, qu'elle a regardé tant les dangers du benfluorex que le déséquilibre entre les bénéfices et les risques tenant à l'utilisation du Mediator comme suffisamment caractérisés pour que l'abstention de prendre les mesures adaptées, consistant en la suspension ou le retrait de l'autorisation de mise sur le marché du Mediator, constitue une faute de nature à engager la responsabilité de l'Etat ;<br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond, notamment du rapport remis par l'Inspection générale des affaires sociales en janvier 2011 à la suite de la mission d'enquête sur le Mediator qui lui avait été confiée, que le benfluorex, molécule distincte des fenfluramines, a été présenté par la société les Laboratoires Servier, dès les années soixante-dix, comme ayant des propriétés métaboliques et cliniques différentes de celles des fenfluramines, et que son autorisation de mise sur le marché a été sollicitée et obtenue pour des indications thérapeutiques distinctes ; que s'il ressort du compte rendu d'une réunion du comité technique de pharmacovigilance de l'Agence du médicament tenue en juillet 1995, ainsi que le fait valoir MmeA..., que le centre régional de pharmacovigilance de Besançon avait relevé que le benfluorex possédait " une structure voisine de celle des anorexigènes ", il n'en résulte pas que le métabolisme de cette molécule ait été alors mieux connu ; que, par suite, c'est sans dénaturer les pièces du dossier qui lui était soumis que la cour a estimé qu'il n'existait pas, en 1995, d'élément nouveau sur la parenté pharmacologique entre les amphétamines et le Mediator ; qu'elle a donné aux faits de l'espèce une exacte qualification juridique en déduisant de cette circonstance et de l'absence d'effets indésirables graves connus résultant de la prise du Mediator, relevées au terme de son appréciation souveraine, que le défaut de mesure de suspension ou de retrait de l'autorisation de mise sur le marché de la spécialité, toutefois soumise depuis mai 1995 à une enquête de pharmacovigilance portant sur ses effets indésirables, ne constituait pas, dès 1995, une faute de nature à engager la responsabilité de l'Etat ; que son arrêt est suffisamment motivé sur les points contestés ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que Mme A...n'est pas fondée à demander l'annulation de l'arrêt de la cour administrative d'appel de Paris qu'elle attaque ; que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme A...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme B...A...et à la ministre des affaires sociales et de la santé.<br/>
Copie en sera adressée à l'Agence nationale de sécurité du médicament et des produits de santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
