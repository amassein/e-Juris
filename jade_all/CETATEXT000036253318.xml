<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036253318</ID>
<ANCIEN_ID>JG_L_2017_12_000000390713</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/25/33/CETATEXT000036253318.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 22/12/2017, 390713</TITRE>
<DATE_DEC>2017-12-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390713</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP HEMERY, THOMAS-RAQUIN ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:390713.20171222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C...A...a porté plainte contre Mme D...B...devant la chambre disciplinaire de première instance d'Aquitaine de l'ordre des médecins. Le conseil départemental de Gironde de l'ordre des médecins s'est associé à la plainte. Par une décision n° 1231-1231/QPC du 3 juin 2014, la chambre disciplinaire de première instance a infligé à Mme B...la sanction d'interdiction d'exercer la médecine pendant six mois, dont deux avec sursis. <br/>
<br/>
              Par une décision n° 12411-12411/QPC du 3 avril 2015, la chambre disciplinaire nationale de l'ordre des médecins a, sur appel de Mme B..., annulé cette décision et rejeté la plainte de Mme A...et du conseil départemental de Gironde de l'ordre des médecins. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 3 juin et 3 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, le conseil départemental de Gironde de l'ordre des médecins demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette décision ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de Mme B... ;<br/>
<br/>
              3°) de mettre à la charge de Mme B...une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Hémery, Thomas-Raquin, avocat du conseil départemental de Gironde de l'ordre des médecins, à la SCP Potier de la Varde, Buk Lament, Robillot, avocat de Mme B...et à la SCP Matuchansky, Poupot, Valdelièvre, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que MmeA..., médecin qui était locataire, pour son exercice professionnel, d'un local appartenant à MmeB..., également médecin, a saisi la chambre disciplinaire de première instance d'Aquitaine de l'ordre des médecins d'une plainte contre cette dernière, à laquelle le conseil départemental de Gironde de l'ordre des médecins s'est associé, en invoquant divers griefs liés à cette location ; que, par une décision du 3 avril 2015 contre laquelle le conseil départemental se pourvoit en cassation, la chambre disciplinaire nationale de l'ordre des médecins a annulé la décision de la chambre disciplinaire de première instance du 3 juin 2014 infligeant à Mme B... la sanction d'interdiction d'exercer la médecine pendant six mois et rejeté la plainte de Mme A...et du conseil départemental ;<br/>
<br/>
              2. Considérant, en premier lieu, que le moyen tiré de ce que la minute de la décision de la chambre disciplinaire nationale de l'ordre des médecins ne serait signée ni par le président de la formation de jugement ni par le greffier manque en fait ;  <br/>
<br/>
              3. Considérant, en deuxième lieu, qu'il ressort des pièces du dossier soumis aux juges du fond que le médecin désigné par le directeur général de l'agence régionale de santé qui a, en application des dispositions de l'article L. 4132-9 du code de la santé publique alors en vigueur, siégé avec voix consultative lors de la séance de la chambre disciplinaire de première instance d'Aquitaine de l'ordre des médecins du 17 mai 2014 au cours de laquelle a été examinée la plainte visant MmeB..., avait, au titre de ses fonctions au sein de l'agence régionale de santé, été antérieurement saisi par le conseil départemental de l'ordre des médecins des faits reprochés à l'intéressée et avait alors préconisé une inspection sur place, en en informant le conseil départemental ; qu'en jugeant que cette circonstance était susceptible de porter atteinte à l'équité du procès et au principe d'impartialité rappelés par les stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, la chambre disciplinaire nationale de l'ordre des médecins n'a pas entaché sa décision d'erreur de droit ; <br/>
<br/>
              4. Considérant, en troisième lieu, que, d'une part, il ressort des pièces du dossier soumis aux juges du fond que Mme A...et le conseil départemental n'ont pas invoqué, au soutien de leur plainte, l'absence pure et simple d'un local adéquat au sens des dispositions de l'article R. 4127-71 du code de la santé publique mais ont seulement contesté les conditions de jouissance du local dont la praticienne mise en cause était propriétaire, en raison des désagréments causés par la présence mitoyenne d'une société commerciale spécialisée dans les soins cosmétiques ; que, par suite, en indiquant qu'il n'était " pas contesté " que le cabinet médical en question " répondait aux exigences de l'article R. 4127-71 ", la chambre disciplinaire nationale n'a ni dénaturé les pièces du dossier ni entaché sa décision de contradiction des motifs ; que, d'autre part, en estimant que Mme B...ne pouvait être tenue pour responsable de l'existence d'une communication directe entre le cabinet de Mme A...et les locaux de la société voisine, que celle-ci avait mis fin à cette situation une fois qu'elle lui avait été signalée et, enfin, que l'occupation occasionnelle, par un tiers, d'un bureau mitoyen au cabinet de Mme A... ne pouvait lui être imputée, la chambre disciplinaire nationale a porté sur les pièces du dossier qui lui était soumis une appréciation souveraine, qui n'est pas entachée de dénaturation et a pu, en tout état de cause, en déduire, sans entacher sa décision d'inexacte qualification juridique des faits, que Mme B...n'avait pas manqué aux obligations découlant des dispositions de l'article R. 4127-71 du code de la santé publique ; <br/>
<br/>
              5. Considérant, en quatrième lieu, qu'en estimant que Mme B... n'était liée à l'activité de la société Skin Lasers et Cosmetics par aucun intérêt, la chambre nationale de discipline s'est livrée à une appréciation souveraine des  pièces du dossier exempte de dénaturation ;  <br/>
<br/>
              6. Considérant, enfin, qu'en jugeant que la présentation erronée de Mme A... comme collaboratrice de Mme B...sur le site internet de cette dernière ne révélait, eu égard aux circonstances dans lesquelles cette simple erreur avait été commise puis rectifiée, aucun manquement déontologique de sa part, la chambre disciplinaire nationale n'a entaché sa décision ni d'erreur de droit ni d'inexacte qualification juridique des faits ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que le conseil départemental de Gironde de l'ordre des médecins n'est pas fondé à demander l'annulation de la décision attaquée ;<br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de Mme B...qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge du conseil départemental de Gironde de l'ordre des médecins la somme de 3 500 euros à verser à cette dernière au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi du conseil départemental de Gironde de l'ordre des médecins est rejeté.<br/>
Article 2 : Le conseil départemental de Gironde de l'ordre des médecins versera à Mme B...la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au conseil départemental de Gironde de l'ordre des médecins et à Mme D...B.... <br/>
Copie en sera adressée à Mme C...A...et au Conseil national de l'ordre des médecins.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-055-01-06-02 DROITS CIVILS ET INDIVIDUELS. CONVENTION EUROPÉENNE DES DROITS DE L'HOMME. DROITS GARANTIS PAR LA CONVENTION. DROIT À UN PROCÈS ÉQUITABLE (ART. 6). VIOLATION. - MÉDECIN AYANT SIÉGÉ AVEC VOIX CONSULTATIVE LORS D'UNE SÉANCE DE LA CHAMBRE DISCIPLINAIRE DE PREMIÈRE INSTANCE DE L'ORDRE DES MÉDECINS EXAMINANT UNE PLAINTE VISANT UN MÉDECIN ALORS QU'IL AVAIT ÉTÉ ANTÉRIEUREMENT SAISI PAR LE CONSEIL DÉPARTEMENTAL DE L'ORDRE DES MÉDECINS DES FAITS REPROCHÉS ET AVAIT PRÉCONISÉ UNE INSPECTION CONCERNANT L'INTÉRESSÉ - COMPOSITION SUSCEPTIBLE DE PORTER ATTEINTE À L'ÉQUITÉ DU PROCÈS ET AU PRINCIPE D'IMPARTIALITÉ - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-03 PROCÉDURE. JUGEMENTS. COMPOSITION DE LA JURIDICTION. - CHAMBRE DISCIPLINAIRE DE PREMIÈRE INSTANCE DE L'ORDRE DES MÉDECINS - COMPOSITION - MÉDECIN AYANT SIÉGÉ AVEC VOIX CONSULTATIVE LORS D'UNE SÉANCE DE LA CHAMBRE DISCIPLINAIRE DE PREMIÈRE INSTANCE DE L'ORDRE DES MÉDECINS EXAMINANT UNE PLAINTE VISANT UN MÉDECIN ALORS QU'IL AVAIT ÉTÉ ANTÉRIEUREMENT SAISI PAR LE CONSEIL DÉPARTEMENTAL DE L'ORDRE DES MÉDECINS DES FAITS REPROCHÉS ET AVAIT PRÉCONISÉ UNE INSPECTION CONCERNANT L'INTÉRESSÉ - COMPOSITION SUSCEPTIBLE DE PORTER ATTEINTE À L'ÉQUITÉ DU PROCÈS ET AU PRINCIPE D'IMPARTIALITÉ - EXISTENCE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">55-04-01 PROFESSIONS, CHARGES ET OFFICES. DISCIPLINE PROFESSIONNELLE. PROCÉDURE DEVANT LES JURIDICTIONS ORDINALES. - COMPOSITION DE LA CHAMBRE DISCIPLINAIRE DE PREMIÈRE INSTANCE DE L'ORDRE DES MÉDECINS - MÉDECIN AYANT SIÉGÉ AVEC VOIX CONSULTATIVE LORS D'UNE SÉANCE DE LA CHAMBRE DISCIPLINAIRE DE PREMIÈRE INSTANCE DE L'ORDRE DES MÉDECINS EXAMINANT UNE PLAINTE VISANT UN MÉDECIN ALORS QU'IL AVAIT ÉTÉ ANTÉRIEUREMENT SAISI PAR LE CONSEIL DÉPARTEMENTAL DE L'ORDRE DES MÉDECINS DES FAITS REPROCHÉS ET AVAIT PRÉCONISÉ UNE INSPECTION CONCERNANT L'INTÉRESSÉ - FAITS SUSCEPTIBLES DE PORTER ATTEINTE À L'ÉQUITÉ DU PROCÈS ET AU PRINCIPE D'IMPARTIALITÉ - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 26-055-01-06-02 Médecin désigné par le directeur général de l'agence régionale de santé (ARS) qui a, en application de l'article L. 4132-9 du code de la santé publique (CSP) alors en vigueur, siégé avec voix consultative lors de la séance de la chambre disciplinaire de première instance de l'ordre des médecins au cours de laquelle a été examinée la plainte visant un médecin, et qui avait, au titre de ses fonctions au sein de l'ARS été antérieurement saisi par le conseil départemental de l'ordre des médecins des faits reprochés à l'intéressé et avait alors préconisé une inspection sur place, en en informant le conseil départemental.,,,Cette circonstance est susceptible de porter atteinte à l'équité du procès et au principe d'impartialité rappelés par les stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.</ANA>
<ANA ID="9B"> 54-06-03 Médecin désigné par le directeur général de l'agence régionale de santé (ARS) qui a, en application de l'article L. 4132-9 du code de la santé publique (CSP) alors en vigueur, siégé avec voix consultative lors de la séance de la chambre disciplinaire de première instance de l'ordre des médecins au cours de laquelle a été examinée la plainte visant un médecin, et qui avait, au titre de ses fonctions au sein de l'ARS été antérieurement saisi par le conseil départemental de l'ordre des médecins des faits reprochés à l'intéressé et avait alors préconisé une inspection sur place, en en informant le conseil départemental.,,,Cette circonstance est susceptible de porter atteinte à l'équité du procès et au principe d'impartialité rappelés par les stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.</ANA>
<ANA ID="9C"> 55-04-01 Médecin désigné par le directeur général de l'agence régionale de santé (ARS) qui a, en application de l'article L. 4132-9 du code de la santé publique (CSP) alors en vigueur, siégé avec voix consultative lors de la séance de la chambre disciplinaire de première instance de l'ordre des médecins au cours de laquelle a été examinée la plainte visant un médecin, et qui avait, au titre de ses fonctions au sein de l'ARS été antérieurement saisi par le conseil départemental de l'ordre des médecins des faits reprochés à l'intéressé et avait alors préconisé une inspection sur place, en en informant le conseil départemental.,,,Cette circonstance est susceptible de porter atteinte à l'équité du procès et au principe d'impartialité rappelés par les stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 4 juillet 2003, M. Dubreuil, n° 234353, p. 313. Rappr. CE, 3 octobre 2003, M. Patet, n° 182743, p. 389.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
