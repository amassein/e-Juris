<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033958332</ID>
<ANCIEN_ID>JG_L_2017_01_000000389254</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/95/83/CETATEXT000033958332.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 30/01/2017, 389254, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-01-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389254</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>Mme Déborah Coricon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:389254.20170130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La SCI Château du Grand Bois a demandé au tribunal administratif de Nantes d'annuler la décision du 18 décembre 2009 par laquelle le directeur général de l'Etablissement national des produits de l'agriculture et de la mer (FranceAgriMer) a rejeté sa demande de versement de l'aide à la restructuration et à la reconversion du vignoble.<br/>
<br/>
              Par un jugement n° 1004185 du 7 mai 2013, le tribunal administratif a annulé la décision litigieuse.<br/>
<br/>
              Par un arrêt n° 13NT01973 du 5 février 2015, la cour administrative d'appel de Nantes, faisant droit à l'appel de FranceAgriMer, a annulé ce jugement et rejeté les conclusions de la demande de première instance de la SCI Château du Grand Bois.<br/>
<br/>
              Par un pourvoi et un mémoire complémentaire, enregistrés le 7 avril et 7 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, la SCI Château du Grand Bois demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de FranceAgriMer ;<br/>
<br/>
              3°) de mettre à la charge de FranceAgriMer la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - Le Traité sur le fonctionnement de l'Union européenne ; <br/>
              - le règlement (CE) n° 479/2008 du Conseil du 29 avril 2008 ;<br/>
              - le règlement (CE) n° 555/2008 de la Commission du 27 juin 2008 ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - l'ordonnance n° 2009-325 du 25 mars 2009 ;<br/>
              - le décret n° 2009-153 du 11 février 2009 ;<br/>
              - le décret n° 2009-340 du 27 mars 2009 ;<br/>
              - l'arrêté du 26 mai 2009 relatif aux conditions d'attribution de l'aide à la restructuration et à la reconversion du vignoble ;<br/>
              - l'arrêté du 3 juin 2009 relatif aux modalités d'octroi de l'aide à la restructuration et à la reconversion du vignoble pour la campagne 2008-2009 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Déborah Coricon, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la SCI Château du Grand Bois et à la SCP Didier, Pinet, avocat de l'Etablissement national des produits de l'agriculture et de la mer (FranceAgriMer) ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la SCI Château du Grand Bois a présenté le 29 juillet 2009 une demande d'aide à la restructuration et à la reconversion du vignoble pour la campagne 2008/2009. Par décision du 18 décembre 2009, le directeur général de FranceAgriMer a rejeté cette demande au motif qu'il avait été constaté, lors de contrôles effectués sur place les 27 août et 15 septembre 2009 par un agent de FranceAgriMer, que sur certaines parcelles l'arrachage des vignes n'avait pas été réalisé dans des conditions conformes à la réglementation en vigueur. Par un jugement du 7 mai 2013, le tribunal administratif de Nantes a annulé cette décision à la demande de la SCI Château du Grand Bois. Celle-ci se pourvoit en cassation contre l'arrêt du 5 février 2015 par lequel la cour administrative d'appel de Nantes, faisant droit à l'appel de FranceAgriMer, a annulé ce jugement et rejeté ses conclusions dirigées contre la décision litigieuse. <br/>
<br/>
              2. Aux termes de l'article 72 du règlement (CE) n° 555/2008 de la Commission du 27 juin 2008 fixant les modalités d'application du règlement (CE) n° 479/2008 du Conseil portant organisation commune du marché vitivinicole, en ce qui concerne les programmes d'aide, les échanges avec les pays tiers, le potentiel de production et les contrôles dans le secteur vitivinicole : " Versement de la prime : Le versement de la prime à l'arrachage est effectué après vérification que l'arrachage a effectivement eu lieu et au plus tard pour le 15 octobre de l'année d'acceptation de la demande par l'État membre conformément à l'article 102, paragraphe 5, du règlement (CE) no 479/2008. ". Aux termes de l'article 78 du même règlement : " Contrôles sur place : 1. Les contrôles sur place sont effectués de manière inopinée. Un préavis limité au strict nécessaire peut toutefois être donné, pour autant que cela ne nuise pas à l'objectif du contrôle. Le préavis ne dépasse pas 48 heures, sauf dans des cas dûment justifiés ou dans le cas des mesures pour lesquelles des contrôles sur place systématiques sont prévus. (..) 3. La demande ou les demandes d'aide concernées sont rejetées si les bénéficiaires ou leur représentant empêchent la réalisation du contrôle sur place. ". Aux termes de l'article 95 de ce même texte : "Personnes soumises aux contrôles : 1. Les personnes physiques ou morales ainsi que les groupements de ces personnes dont les activités professionnelles peuvent faire l'objet des contrôles visés par le présent règlement ne font pas obstacle à ces contrôles et sont tenus de les faciliter à tout moment. ". <br/>
<br/>
              3. La SCI Château du Grand Bois soutient que la cour administrative d'appel de Nantes a commis une erreur de droit en jugeant que la circonstance que l'agent chargé du contrôle avait pénétré sans son autorisation sur sa propriété était sans incidence sur la légalité de la décision de rejet de sa demande d'aide à la restructuration ou à la reconversion du vignoble. La réponse à ce moyen dépend de la question de savoir si les dispositions citées au point 2 ou les principes généraux du droit de l'Union européenne autorisent les agents chargés d'effectuer les contrôles sur place à pénétrer sur les terres d'une exploitation agricole sans avoir obtenu l'accord de l'exploitant. Dans l'hypothèse d'une réponse positive, se posent alors deux questions subséquentes, relatives, d'une part, au fait de savoir si une distinction doit être établie selon que les terres en causes sont closes ou non, et, d'autre part, à la compatibilité des dispositions en cause avec le principe d'inviolabilité du domicile tel que garanti par l'article 8 de la convention européenne des droits de l'homme. Ces questions sont déterminantes pour la solution du litige que doit trancher le Conseil d'Etat et présentent une difficulté sérieuse. Par suite, il y a lieu d'en saisir la Cour de justice de l'Union européenne en application de l'article 267 du traité sur le fonctionnement de l'Union européenne et, jusqu'à ce que celle-ci se soit prononcée, de surseoir à statuer sur le pourvoi de la SCI Château du Grand Bois.  <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Il est sursis à statuer sur le pourvoi présenté par la SCI Château du Grand Bois jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur les questions suivantes : <br/>
- Les dispositions des articles 76, 78 et 81 du règlement d'application du 27 juin 2008 autorisent-elles les agents qui procèdent à un contrôle sur place à pénétrer sur les terres d'une exploitation agricole sans avoir obtenu l'accord de l'exploitant '<br/>
- Dans l'hypothèse ou il serait répondu positivement à cette première question, y a-t-il lieu de distinguer selon que les terres en cause sont closes ou non '<br/>
- Dans l'hypothèse ou il serait répondu positivement à cette première question, les dispositions des articles 76, 78 et 81 du règlement d'application du 27 juin 2008 sont-elles compatibles avec le principe d'inviolabilité du domicile tel que garanti par l'article 8 de la convention européenne des droits de l'homme '<br/>
Article 2 : La présente décision sera notifiée à la SCI Château du Grand Bois, à l'Etablissement national des produits de l'agriculture et de la mer et au président de la Cour de justice de l'Union européenne. <br/>
Copie en sera adressée pour information au Premier ministre et au ministre de l'agriculture, de l'agroalimentaire et de la forêt.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
