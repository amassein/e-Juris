<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028569956</ID>
<ANCIEN_ID>JG_L_2014_02_000000371352</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/56/99/CETATEXT000028569956.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème / 10ème SSR, 05/02/2014, 371352, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-02-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>371352</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème / 10ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLEZ</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:371352.20140205</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 16 août et 18 novembre 2013 au secrétariat du contentieux du Conseil d'État, présentés pour la société EBC Pharmexport, dont le siège est 85, rue de Lourmel à Paris (75015) ; la société EBC Pharmexport demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11PA04812 du 13 juin 2013 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant à l'annulation du jugement n° 0913518 du 19 septembre 2011 du tribunal administratif de Paris rejetant sa demande en décharge des cotisations supplémentaires d'impôt sur les sociétés et de contribution additionnelle sur cet impôt auxquelles elle a été assujettie au titre des années 2002 et 2003, des cotisations supplémentaires de taxe d'apprentissage et de participation des employeurs à l'effort de construction auxquelles elle a été assujettie au titre de l'année 2002, des cotisations supplémentaires de participation des employeurs au développement de la formation professionnelle continue auxquelles elle a été assujettie au titre des années 2002 et 2003 et des rappels de taxe sur la valeur ajoutée auxquels elle a été assujettie au titre de la période du 1er janvier 2002 au 31 décembre 2002 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code général des impôts ;<br/>
<br/>
              Vu l'ordonnance n° 2005-1512 du 7 décembre 2005 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boullez, avocat de la société Ebc Pharmexport ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'État fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux " ;<br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'État (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article 240 du code général des impôts " 1. Les personnes physiques qui, à l'occasion de l'exercice de leur profession versent à des tiers des commissions, courtages, ristournes commerciales ou autres, vacations, honoraires occasionnels ou non, gratifications et autres rémunérations, doivent déclarer ces sommes (....) / 2. Les dispositions des 1 et 1 bis sont applicables à toutes les personnes morales ou organismes, quel que soit leur objet ou leur activité (...) " ; que le premier alinéa de l'article 238 du même code, alors en vigueur, dispose, dans sa rédaction antérieure à son abrogation à compter du 1er janvier 2006 par l'ordonnance du 7 décembre 2005 relative à des mesures de simplification en matière fiscale et à l'harmonisation et l'aménagement du régime des pénalités : " Les personnes physiques et les personnes morales qui n'ont pas déclaré les sommes visées au premier alinéa du 1 de l'article 240 perdent le droit de les porter dans leurs frais professionnels pour l'établissement de leurs propres impositions. Toutefois, cette sanction n'est pas applicable, en cas de première infraction, lorsque les intéressés ont réparé leur omission, soit spontanément, soit à la première demande de l'administration, avant la fin de l'année au cours de laquelle la déclaration devait être souscrite. " ;<br/>
<br/>
              4. Considérant, en premier lieu, que si la société EBC Pharmexport soutient que l'article 238 du code général des impôts, en tant qu'il prévoit que le fait, pour un tiers déclarant, de ne pas se conformer aux obligations déclaratives prévues à l'articles 240 du même code entraîne la réintégration au résultat imposable des sommes non déclarées, porterait atteinte au principe de proportionnalité des peines résultant de l'article 8 de la Déclaration des droits de l'homme et du citoyen, la disposition contestée sanctionne le non respect d'obligations permettant à l'administration fiscale de procéder aux recoupements nécessaires au contrôle du respect, par les bénéficiaires des versements, de leurs obligations fiscales et, ainsi, contribue au respect de l'objectif de valeur constitutionnelle de lutte contre la fraude fiscale ; qu'en fixant la sanction encourue en proportion des sommes versées, du fait de leur réintégration au résultat imposable, le législateur l'a proportionnée, compte tenu du taux de l'impôt alors applicable, à la gravité des manquements réprimés, appréciée à raison de l'importance des sommes non déclarées ; qu'ainsi, le moyen tiré de ce que l'article 238 du code général des impôts méconnaît les principes de nécessité et de proportionnalité des peines n'est pas sérieux ;<br/>
<br/>
              5. Considérant, en second lieu, que si la société EBC Pharmexport soutient que les mêmes dispositions méconnaissent le principe d'individualisation des peines, résultant également de l'article 8 de la Déclaration des droits de l'homme et du citoyen, il ressort de leur texte même que le législateur a prévu, pour la première infraction et en cas de manquement involontaire du déclarant, que la sanction n'est pas applicable lorsque l'intéressé a procédé spontanément à la réparation de son omission avant la fin de l'année où la déclaration devait être souscrite ; qu'ainsi, compte tenu de l'objectif de valeur constitutionnelle de lutte contre la fraude fiscale poursuivi par ces dispositions, le moyen tiré de la méconnaissance du principe d'individualisation des peines n'est pas non plus sérieux ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; que, par suite, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que l'article 238 du code général des impôts, dans sa rédaction antérieure à son abrogation par l'ordonnance du 7 décembre 2005, porte atteinte aux droits et libertés garantis par la Constitution doit être regardé comme non sérieux ;<br/>
<br/>
              Sur les autres moyens :<br/>
<br/>
              7. Considérant que, pour demander l'annulation de l'arrêt qu'elle attaque, la société EBC Pharmexport soutient en outre que la cour a omis de répondre au moyen tiré de ce que la sanction qui lui a été infligée sur le fondement de l'article 238 du code général des impôts en raison de la non-déclaration de certaines sommes versées à des tiers ne devait concerner que des règlements effectifs et non des sommes comptabilisées en charge ; que la cour a insuffisamment motivé son arrêt et commis une erreur de droit en ne recherchant pas s'il convenait de faire application des dispositions répressives plus douces issues de l'ordonnance du 7 décembre 2005, qui a abrogé l'article 238 du code général des impôts et créé une sanction nouvelle, prévue à l'article 1736 de ce code ;<br/>
<br/>
              8. Considérant qu'aucun de ces moyens n'est de nature à permettre l'admission du pourvoi ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société EBC Pharmexport.<br/>
Article 2 : Le pourvoi de la société EBC Pharmexport n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à la société EBC Pharmexport et au ministre de l'économie et des finances.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
