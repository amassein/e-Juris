<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036796133</ID>
<ANCIEN_ID>JG_L_2018_04_000000407785</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/79/61/CETATEXT000036796133.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 11/04/2018, 407785, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-04-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407785</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX ; SCP DELAMARRE, JEHANNIN</AVOCATS>
<RAPPORTEUR>Mme Karin Ciavaldini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:407785.20180411</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société civile immobilière (SCI) Les Jardins de Gutenberg a demandé au tribunal administratif de Rennes de prononcer la décharge des cotisations de taxe d'enlèvement des ordures ménagères auxquelles elle a été assujettie au titre de l'année 2014 dans les rôles de la commune de Quimper (Finistère) à raison de deux immeubles dont elle est propriétaire. Par un jugement n° 1601444 du 30 novembre 2016, ce tribunal a fait droit à sa demande. <br/>
<br/>
              Par un pourvoi sommaire, un pourvoi rectificatif et un mémoire complémentaire, enregistrés les 8 février, 9 février et 4 mai 2017 au secrétariat de la section du contentieux du Conseil d'Etat, la communauté d'agglomération Quimper Bretagne occidentale demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de la SCI Les jardins de Gutenberg ;<br/>
<br/>
              3°) de mettre à la charge de la SCI Les Jardins de Gutenberg la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Karin Ciavaldini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delamarre, Jéhannin, avocat de la communauté d'agglomération Quimper Bretagne occidentale et à la SCP Baraduc, Duhamel, Rameix, avocat de la SCI Les Jardins de Gutenberg.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes des dispositions du I de l'article 1520 du code général des impôts, applicable aux établissements publics de coopération intercommunale, dans sa rédaction applicable au litige : " Les communes qui assurent au moins la collecte des déchets des ménages peuvent instituer une taxe destinée à pourvoir aux dépenses du service dans la mesure où celles-ci ne sont pas couvertes par des recettes ordinaires n'ayant pas le caractère fiscal (...) ". Aux termes du II de l'article 316 de l'annexe II à ce code : " II. Les rôles de la taxe d'enlèvement des ordures ménagères sont établis et recouvrés et les réclamations présentées, instruites et jugées comme en matière de contributions directes. ". Il résulte de ces dispositions que la taxe d'enlèvement des ordures ménagères, qui revêt le caractère d'un impôt local, est établie, liquidée et recouvrée par les services de l'Etat pour le compte de la commune ou de l'établissement public de coopération intercommunale qui en est le bénéficiaire légal. Par suite, ces services ont seuls qualité pour agir dans les litiges auxquels peuvent donner lieu l'assiette et le recouvrement de la taxe d'enlèvement des ordures ménagères. <br/>
<br/>
              2. Il en découle que la communauté d'agglomération Quimper Bretagne occidentale n'avait pas la qualité de partie en première instance et n'a pas qualité pour demander l'annulation du jugement du 30 novembre 2016 par lequel le tribunal administratif de Rennes a accueilli la demande de la société civile immobilière (SCI) Les Jardins de Gutenberg tendant à la décharge de la cotisation de taxe d'enlèvement des ordures ménagères à laquelle elle a été assujettie au titre de l'année 2014 à raison de deux immeubles dont elle est propriétaire à Quimper. Son pourvoi est, dès lors, irrecevable. <br/>
<br/>
              3. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de la SCI Les Jardins de Gutenberg, qui n'est pas la partie perdante dans la présente instance, le versement de la somme que demande, à ce titre, la communauté d'agglomération Quimper Bretagne occidentale. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la communauté d'agglomération Quimper Bretagne occidentale le versement d'une somme de 3 000 euros à la SCI Les Jardins de Gutenberg sur le fondement des mêmes dispositions.   <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la communauté d'agglomération Quimper Bretagne occidentale est rejeté.<br/>
Article 2 : La communauté d'agglomération Quimper Bretagne occidentale versera la somme de 3 000 euros à la SCI les Jardins de Gutenberg au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la communauté d'agglomération Quimper Bretagne occidentale, à la société civile immobilière les Jardins de Gutenberg et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
