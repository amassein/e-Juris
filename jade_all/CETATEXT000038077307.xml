<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038077307</ID>
<ANCIEN_ID>JG_L_2019_01_000000408469</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/07/73/CETATEXT000038077307.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 30/01/2019, 408469, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-01-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408469</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Ophélie Champeaux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:408469.20190130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Paris de prononcer la décharge des cotisations d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre des années 2007 à 2009 et des pénalités correspondantes. Par un jugement nos 1216664, 1316806 du 12 novembre 2014, le tribunal a dit n'y avoir lieu à statuer à hauteur de la somme de 4 647 euros en matière d'impôt sur le revenu et de 1 366 euros en matière de contributions sociales, et a rejeté le surplus de sa demande.<br/>
<br/>
              Par un arrêt n° 15PA00200 du 30 décembre 2016, la cour administrative d'appel de Paris a rejeté l'appel formé par M. B...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 28 février et 29 mai 2017 et le 6 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'État : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention du 10 octobre 1995 entre la France et l'Espagne en vue d'éviter les doubles impositions et de prévenir l'évasion et la fraude fiscales en matière d'impôts sur le revenu et sur la fortune ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la décision du Conseil constitutionnel n° 2016-610 QPC du 10 février 2017 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Ophélie Champeaux, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de M.B....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B... a fait l'objet d'un examen de sa situation fiscale personnelle, à l'issue duquel l'administration a estimé qu'il avait son domicile en France et l'a, par suite, assujetti à des cotisations supplémentaires d'impôt sur le revenu, dans la catégorie des traitements et salaires et des capitaux mobiliers, et de contributions sociales au titre des années 2007 à 2009, assorties d'une majoration pour manquement délibéré. M. B...se pourvoit en cassation contre l'arrêt du 30 décembre 2016 de la cour administrative d'appel de Paris rejetant l'appel formé contre le jugement du 12 novembre 2014 par lequel le tribunal administratif de Paris avait rejeté ses demandes de décharge.<br/>
<br/>
              2. Par une décision du 27 mars 2018, l'administration fiscale a prononcé un dégrèvement partiel des suppléments de contributions sociales en litige, pour un montant de 17 752 euros en droits et pénalités, résultant de l'abandon de l'application du coefficient multiplicateur de 1,25 prévu au premier alinéa du 7 de l'article 158 du code général des impôts, conformément à la réserve d'interprétation formulée par le Conseil constitutionnel dans sa décision n° 2016-610 QPC du 10 février 2017. Il en résulte qu'il n'y a plus lieu de statuer, dans cette mesure, sur les conclusions du pourvoi de M.B....<br/>
<br/>
              3. Si une convention bilatérale conclue en vue d'éviter les doubles impositions peut, en vertu de l'article 55 de la Constitution, conduire à écarter, sur tel ou tel point, la loi fiscale nationale, elle ne peut pas, par elle même, directement servir de base légale à une décision relative à l'imposition. Par suite, il incombe au juge de l'impôt, lorsqu'il est saisi d'une contestation relative à une telle convention, de se placer d'abord au regard de la loi fiscale nationale pour rechercher si, à ce titre, l'imposition contestée a été valablement établie et, dans l'affirmative, sur le fondement de quelle qualification. Il lui appartient toutefois ensuite, en rapprochant cette qualification des stipulations de la convention, de déterminer, en fonction des moyens invoqués devant lui ou même, s'agissant de déterminer le champ d'application de la loi, d'office, si cette convention fait ou non obstacle à l'application de la loi fiscale.<br/>
<br/>
              4. Aux termes du premier alinéa de l'article 4 A du code général des impôts : " Les personnes qui ont en France leur domicile fiscal sont passibles de l'impôt sur le revenu en raison de l'ensemble de leurs revenus ". Aux termes de l'article 4 B du même code : " 1. Sont considérées comme ayant leur domicile fiscal en France au sens de l'article 4 A : / a. Les personnes qui ont en France leur foyer ou le lieu de leur séjour principal ; / b. Celles qui exercent en France une activité professionnelle, salariée ou non, à moins qu'elles ne justifient que cette activité y est exercée à titre accessoire ; / (...) ".<br/>
<br/>
              5. Aux termes de l'article 4 de la convention conclue entre la France et l'Espagne le 10 octobre 1995 en vue d'éliminer les doubles impositions et de prévenir l'évasion et la fraude fiscales en matière d'impôts sur le revenu et sur la fortune  : " 1. Au sens de la présente Convention, l'expression " résident d'un Etat contractant " désigne toute personne qui, en vertu de la législation de cet Etat, est assujettie à l'impôt en raison de son domicile, de sa résidence, de son siège de direction ou de tout autre critère de nature analogue. (...) / 2. Lorsque, selon les dispositions du paragraphe 1, une personne physique est un résident des deux Etats contractants, sa situation est réglée de la manière suivante : /  a) Cette personne est considérée comme un résident de l'Etat où elle dispose d'un foyer d'habitation permanent ; (...) ". Aux termes de l'article 15 de cette convention : " 1. (...) les salaires, traitements et autres rémunérations similaires qu'un résident d'un Etat contractant reçoit au titre d'un emploi salarié ne sont imposables que dans cet Etat, à moins que l'emploi ne soit exercé dans l'autre Etat contractant. Si l'emploi y est exercé, les rémunérations reçues à ce titre sont imposables dans cet autre Etat (...) ". Enfin aux termes de son article 24 : " 1. En ce qui concerne la France, les doubles impositions sont évitées de la manière suivante : /a) Les revenus qui proviennent d'Espagne, et qui sont imposables ou ne sont imposables que dans cet Etat, conformément aux dispositions de la présente Convention, sont pris en compte pour le calcul de l'impôt français lorsque leur bénéficiaire est un résident de France et qu'ils ne sont pas exemptés de l'impôt sur les sociétés en application de la législation interne française ".<br/>
<br/>
              6. En premier lieu, la cour a pu, sans erreur de droit, faire application des stipulations de l'article 4 de la convention franco-espagnole après avoir jugé que M.B..., qui tire des revenus de son activité de représentant légal de la société de droit espagnol Reiki Forum International, devait être regardé comme ayant son domicile fiscal en France en application des dispositions de l'article 4 A et du a du 1 de l'article 4 B du code général des impôts citées ci-dessus, sans avoir à rechercher s'il était également imposable en Espagne en vertu de la législation espagnole.<br/>
<br/>
              7. En deuxième lieu, la cour n'a pas dénaturé les faits de l'espèce en relevant qu'il ne résultait pas de l'instruction que M. B...aurait disposé personnellement et effectivement du bien immobilier dont il est propriétaire en Espagne, qui était mis gratuitement à la disposition de la société dont il était gérant. Elle n'a pas commis d'erreur de droit en en déduisant qu'il n'avait pas dans ce pays de foyer d'habitation permanent.<br/>
<br/>
              8. En troisième lieu, la cour n'a pas commis d'erreur de droit en ne relevant pas d'office le moyen tiré de ce que M. B...aurait été imposable en Espagne, en application de l'article 15 de la convention franco-espagnole cité ci-dessus, à raison des salaires qu'il percevait de la société Reiki Forum International, dès lors qu'il ne ressort pas des pièces du dossier qui lui était soumis que ces salaires auraient été perçus à raison d'un emploi exercé en Espagne. Il en découle que la cour n'a pas davantage commis d'erreur de droit en ne faisant pas, d'office, application de l'article 24 de la même convention.<br/>
<br/>
              9. En dernier lieu, la cour n'a pas dénaturé les pièces du dossier en estimant, après avoir relevé que le requérant était seul maître de l'affaire de la société Reiki Forum International et qu'il exerçait son activité professionnelle en France, qu'il ne pouvait ignorer qu'il avait sa résidence en France. Dès lors, elle n'a pas entaché son arrêt d'une erreur de qualification juridique en jugeant que l'administration établissait que M. B... entrait dans les prévisions de l'article 1729 du code général des impôts sur le fondement desquelles ont été appliquées des majorations pour manquement délibéré.<br/>
<br/>
              10. Il résulte de tout ce qui précède que M. B...n'est pas fondé à demander, à hauteur des impositions maintenues à sa charge, l'annulation de l'arrêt qu'il attaque. <br/>
<br/>
              11. Par suite, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat, qui n'est pas, pour l'essentiel, la partie perdante au sens de ces dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions du pourvoi en tant qu'elles portent, à hauteur de 17 752 euros, sur les cotisations supplémentaires de contributions sociales mises à la charge de M. B... au titre des années 2007 à 2009.<br/>
Article 2 : Le surplus des conclusions du pourvoi de M. B...est rejeté.<br/>
Article 3 : La présente décision sera notifiée à M. A...B...et au ministre de l'action et des comptes publics. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
