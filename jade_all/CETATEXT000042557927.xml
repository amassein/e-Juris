<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042557927</ID>
<ANCIEN_ID>JG_L_2020_11_000000421749</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/55/79/CETATEXT000042557927.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 23/11/2020, 421749, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421749</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU ; SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>Mme Françoise Tomé</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:421749.20201123</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Le président du conseil régional d'Auvergne-Rhône-Alpes de l'ordre des vétérinaires a porté plainte contre la société Sudelvet Conseil et M. C... devant la chambre régionale de discipline de Rhône-Alpes de l'ordre des vétérinaires. Par une décision du 2 mars 2018, la chambre régionale de discipline a infligé, à la société Sudelvet Conseil, la sanction de la réprimande et, à M. C..., la sanction de la réprimande, accompagnée de l'interdiction de faire partie d'un conseil de l'ordre pendant une période de dix ans.<br/>
<br/>
              Par une décision du 17 avril 2018, la chambre nationale de discipline de l'ordre des vétérinaires a rejeté les appels de la société Sudelvet Conseil et de M. C... contre cette décision.<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 25 juin et 25 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, la société Sudelvet Conseil et M. C... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leurs appels ;<br/>
<br/>
              3°) de mettre à la charge du conseil régional d'Auvergne-Rhône-Alpes de l'ordre des vétérinaires la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme D... A..., conseillère d'Etat,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la société Sudelvet Conseil et de M. C... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 241-17 du code rural et de la pêche maritime, dans sa rédaction application à l'espèce : " I.-Les personnes exerçant légalement la profession de vétérinaire peuvent exercer en commun la médecine et la chirurgie des animaux dans le cadre : (...) / 2° De sociétés d'exercice libéral ; /(...). / Cet exercice en commun ne peut être entrepris qu'après inscription de la société au tableau de l'ordre mentionné à l'article L. 242-4, dans les conditions prévues par ce dernier. / II.-Les sociétés mentionnées au I répondent aux conditions cumulatives suivantes : / 1° Plus de la moitié du capital social et des droits de vote doit être détenue, directement ou par l'intermédiaire des sociétés inscrites auprès de l'ordre, par des personnes exerçant légalement la profession de vétérinaire en exercice au sein de la société ; / 2° La détention, directe ou indirecte, de parts ou d'actions du capital social est interdite : / a) Aux personnes physiques ou morales qui, n'exerçant pas la profession de vétérinaire, fournissent des services, produits ou matériels utilisés à l'occasion de l'exercice professionnel vétérinaire ; / b) Aux personnes physiques ou morales exerçant, à titre professionnel ou conformément à leur objet social, une activité d'élevage, de production ou de cession, à titre gratuit ou onéreux, d'animaux ou de transformation des produits animaux ; / 3° Les gérants, le président de la société par actions simplifiée, le président du conseil d'administration ou les membres du directoire doivent être des personnes exerçant légalement la profession de vétérinaire ; / 4° L'identité des associés est connue et l'admission de tout nouvel associé est subordonnée à un agrément préalable par décision collective prise à la majorité des associés mentionnés au 1°. Pour les sociétés de droit étranger, cette admission intervient dans les conditions prévues par leurs statuts ou par le droit qui leur est applicable. / III.-Les sociétés communiquent annuellement au conseil régional de l'ordre dont elles dépendent la liste de leurs associés et la répartition des droits de vote et du capital, ainsi que toute modification de ces éléments. / IV. -Lorsqu'une société ne respecte plus les conditions mentionnées au présent article, le conseil régional de l'ordre compétent la met en demeure de s'y conformer dans un délai qu'il détermine et qui ne peut excéder six mois. A défaut de régularisation dans le délai fixé, le conseil régional peut, après avoir informé la société de la mesure envisagée et l'avoir invitée à présenter ses observations dans les conditions prévues à l'article 24 de la loi n° 2000-321 du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations, prononcer la radiation de la société du tableau de l'ordre des vétérinaires ". <br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que le conseil régional d'Auvergne- Rhône-Alpes de l'ordre des vétérinaires a porté plainte contre M. C... et contre la société vétérinaire d'exercice libéral par actions simplifiée Sudelvet Conseil pour méconnaissance de l'obligation mentionnée au III de l'article L. 241-17 du code rural et de la pêche maritime dont les dispositions sont citées au point 1 et de l'obligation de confraternité. M. C... et la société Sudelvet Conseil se pourvoient en cassation contre la décision du 26 avril 2017 par laquelle la chambre nationale de discipline de l'ordre des vétérinaires a rejeté leurs appels contre la décision de la chambre de discipline de Rhône-Alpes de l'ordre des vétérinaires infligeant, à la société Sudelvet Conseil, une réprimande et, à M. C..., la sanction de la réprimande, accompagnée de l'interdiction de faire partie d'un conseil de l'ordre pendant une période de dix ans.<br/>
<br/>
              Sur la décision attaquée en tant qu'elle concerne M. C... :<br/>
<br/>
              3. En premier lieu, il ressort des pièces du dossier soumis aux juges du fond que M. C... a présenté en appel un seul moyen de régularité, tiré de ce qu'il n'avait été mis en cause au titre de la procédure disciplinaire suivie en première instance qu'en sa qualité de gérant de la société Sudelvet Conseil et non à titre personnel. Dès lors, il ne saurait utilement soutenir en cassation que la chambre nationale de discipline a commis une erreur de droit en jugeant que la procédure de première instance était régulière alors qu'il faisait valoir que les modalités de désignation du rapporteur et les mentions de la convocation à l'audience méconnaissaient les principes du droit à un procès équitable garantis par l'article 6, paragraphe 1, de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. Pour les mêmes motifs, il ne saurait davantage reprocher à la cour d'avoir implicitement jugé que ces moyens étaient irrecevables. <br/>
<br/>
              4. En deuxième lieu, en estimant qu'il n'avait pas été répondu de manière complète aux demandes d'information adressées par les instances ordinales, la chambre nationale de discipline s'est livrée à une appréciation souveraine des pièces du dossier exempte de dénaturation.<br/>
<br/>
              5. En dernier lieu, aux termes du I de l'article L. 242-7 du code rural et de la pêche maritime, dans sa rédaction applicable : "  La chambre de discipline peut appliquer aux personnes physiques mentionnées aux articles L. 241-1 et L. 241-3 les sanctions disciplinaires suivantes : / 1° L'avertissement ; / 2° La réprimande, accompagnée ou non de l'interdiction de faire partie d'un conseil de l'ordre pendant un délai qui ne peut excéder dix ans ; / 3° La suspension temporaire du droit d'exercer la profession pour une durée maximum de dix ans dans un périmètre qui ne pourra excéder le ressort de la chambre régionale qui a prononcé la suspension. Cette sanction entraîne l'inéligibilité de l'intéressé à un conseil de l'ordre pendant toute la durée de la suspension ; / 4° La suspension temporaire du droit d'exercer la profession pour une durée maximum de dix ans dans les départements de métropole et en Guadeloupe, en Guyane, à la Martinique, à La Réunion, à Mayotte, à Saint-Barthélemy, à Saint-Martin ou à Saint-Pierre-et-Miquelon. Cette sanction comporte l'interdiction définitive de faire partie d'un conseil de l'ordre (...) ". En infligeant à M. C... une réprimande accompagnée de l'interdiction de faire partie d'un conseil de l'ordre pendant dix ans, en raison, notamment, du non-respect des diligences à l'égard des instances ordinales imposées aux vétérinaires exerçant dans le cadre de sociétés d'exercice libéral par les dispositions du III de l'article L. 241-17 du code rural et de la pêche maritime citées au point 1, la chambre nationale de discipline de l'ordre des vétérinaires n'a pas prononcé une sanction hors de proportion avec les fautes retenues.  <br/>
<br/>
              6. Il résulte de ce qui précède que M. C... n'est pas fondé à demander l'annulation de la décision attaquée en tant qu'elle le concerne. Par voie de conséquence, les conclusions présentées en son nom au titre de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées. <br/>
<br/>
              Sur la décision attaquée en tant qu'elle concerne la société Sudelvet Conseil :<br/>
<br/>
              7. Aux termes du II de l'article L. 242-7 du code rural et de la pêche maritime, dans sa rédaction applicable : " Sans préjudice des sanctions disciplinaires pouvant être prononcées, le cas échéant, à l'encontre des personnes physiques mentionnées au I exerçant en leur sein, les sociétés mentionnées aux articles L. 241-3 et L. 241-17 peuvent se voir appliquer, dans les conditions prévues au I, les sanctions disciplinaires suivantes : / 1° L'avertissement ; / 2° La suspension temporaire du droit d'exercer la profession pour une durée maximale de dix ans, sur tout ou partie du territoire national ; / 3° La radiation ". En infligeant à la société Sudelvet Conseil une réprimande non prévue par les dispositions précitées du II de l'article L. 242-7 du code rural et de la pêche maritime ni par aucun texte, la chambre nationale de discipline de l'ordre des vétérinaires a méconnu le principe de légalité des peines. <br/>
<br/>
              8. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, la société Sudelvet Conseil est fondée à demander l'annulation de la décision attaquée en tant qu'elle la concerne. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge du conseil régional Auvergne-Rhône-Alpes de l'ordre des vétérinaires la somme que demande la société Sudelvet Conseil au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du 17 avril 2018 de la chambre nationale de discipline de l'ordre des vétérinaires est annulée en tant qu'elle concerne la société Sudelvet Conseil.<br/>
Article 2 : L'affaire est renvoyée à la chambre nationale de discipline de l'ordre des vétérinaires dans la limite de la cassation ainsi prononcée.<br/>
Article 3 : Les conclusions présentées par la société Sudelvet Conseil au titre des dispositions de l'article L. 761-1 du code de justice administrative et les conclusions présentées par M. C..., sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à la société Sudelvet Conseil, à M.  B... C... et au conseil régional Auvergne-Rhône-Alpes de l'ordre des vétérinaires. <br/>
Copie en sera adressée au Conseil national de l'ordre des vétérinaires.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
