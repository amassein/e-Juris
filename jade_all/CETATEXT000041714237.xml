<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041714237</ID>
<ANCIEN_ID>JG_L_2020_03_000000436693</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/71/42/CETATEXT000041714237.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 11/03/2020, 436693</TITRE>
<DATE_DEC>2020-03-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436693</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAMARRE, JEHANNIN ; SCP ZRIBI, TEXIER</AVOCATS>
<RAPPORTEUR>Mme Cécile Isidoro</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:436693.20200311</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B... A..., en appui à son appel du jugement du 2 mai 2019 par lequel le tribunal administratif de Nancy a rejeté sa demande tendant à ce que la commune d'Epinal soit condamnée à lui verser une somme de 25 000 euros en réparation du préjudice subi du fait qu'une nouvelle concession funéraire a été attribuée sur l'emplacement de la sépulture de sa fille, a produit un mémoire, enregistré le 19 juillet 2019 au greffe de  la cour administrative de Nancy, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, par lequel il soulève une question prioritaire de constitutionnalité.<br/>
<br/>
              Par une ordonnance n° 19NC02091-QPC du 10 décembre 2019, enregistrée au secrétariat du contentieux du Conseil d'Etat le 12 décembre 2019, le premier vice-président, président de la 1ère chambre de la cour administrative d'appel de Nancy, avant qu'il soit statué sur l'appel de M. A..., a décidé, par application des dispositions de l'article 23-2 de l'ordonnance du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 2223-15 du code général des collectivités territoriales. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des collectivités territoriales, notamment son article L. 2223-15 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Isidoro, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delamarre, Jéhannin, avocat de M. A... et à la SCP Zribi et Texier, avocat de la commune d'Epinal ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux. <br/>
<br/>
              2. Aux termes de l'article L. 2223-15 du code général des collectivités territoriales : " Les concessions sont accordées moyennant le versement d'un capital dont le montant est fixé par le conseil municipal. / Les concessions temporaires, les concessions trentenaires et les concessions cinquantenaires sont renouvelables au prix du tarif en vigueur au moment du renouvellement. / A défaut du paiement de cette nouvelle redevance, le terrain concédé fait retour à la commune. Il ne peut cependant être repris par elle que deux années révolues après l'expiration de la période pour laquelle le terrain a été concédé. / Dans l'intervalle de ces deux années, les concessionnaires ou leurs ayants cause peuvent user de leur droit de renouvellement ". <br/>
<br/>
              3. Il résulte des dispositions des 3ème et 4ème alinéas de cet article qu'après l'expiration d'une concession, et si les concessionnaires ou leurs ayants-droits n'ont pas usé de leur droit à renouvellement dans les deux ans suivant son expiration, le terrain objet de cette concession funéraire, qui appartient au domaine public de la commune, fait retour à cette dernière. Par ailleurs, les monuments et emblèmes funéraires qui ont pu être édifiés ou apposés sur le terrain par les titulaires de cette concession, et qui n'ont pas été repris par ces derniers, sont intégrés au domaine privé de la commune à l'expiration de ce délai de deux ans. Enfin, il appartient au maire de rechercher par tout moyen utile d'informer les titulaires d'une concession ou leurs ayants-droits de l'extinction de la concession et de leur droit à en demander le renouvellement dans les deux ans qui suivent. <br/>
<br/>
              4. En premier lieu, en prévoyant, d'une part, le retour à la commune du terrain concédé, qui fait partie du domaine public communal, deux ans après l'expiration de la concession et, d'autre part, que les monuments et emblèmes funéraires intègrent le domaine privé de la commune lorsqu'au cours des deux années suivant la fin de la concession funéraire le concessionnaire ou ses ayants-droits n'ont pas manifesté le souhait de la renouveler, les dispositions contestées n'entraînent pas une privation de propriété au sens de l'article 17 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789. <br/>
<br/>
              5. En deuxième lieu, les dispositions contestées ont pour objet d'éviter que les cimetières ne soient progressivement remplis de sépultures à l'abandon et de les maintenir dans un état de dignité compatible avec le respect dû aux morts et aux sépultures. Elles poursuivent ainsi un objectif d'intérêt général. Par suite, le transfert dans le domaine privé de la commune des monuments et emblèmes installés sur la sépulture non réclamés dans le délai légal ne porte pas au droit de propriété une atteinte disproportionnée au regard de l'objectif poursuivi. Le grief tiré de l'atteinte au droit de propriété garanti par l'article 2 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789 ne peut donc être regardé comme présentant un caractère sérieux.<br/>
<br/>
              6. Enfin, ainsi qu'il a été dit au point 3, il appartient au maire de rechercher par tout moyen utile d'informer les titulaires d'une concession ou leurs ayants-droits de l'extinction de la concession et de leur droit à en demander le renouvellement dans les deux ans qui suivent. Par suite, le grief tiré de la méconnaissance de la garantie des droits proclamée à l'article 16 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789 ne peut être regardé comme présentant un caractère sérieux.<br/>
<br/>
              7. Il résulte de ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Par suite, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le premier vice-président, président de la 1ère chambre de la cour administrative d'appel de Nancy. <br/>
Article 2 : La présente décision sera notifiée à Monsieur A... et au ministre de l'intérieur. <br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre, à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales et à la cour administrative d'appel de Nancy.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-02-06 COLLECTIVITÉS TERRITORIALES. COMMUNE. BIENS DE LA COMMUNE. CIMETIÈRES. - CONCESSIONS FUNÉRAIRES EXPIRÉES ET NON RENOUVELÉES (ART. L.2223-15 DU CGCT) - 1) CONSÉQUENCES - A) RETOUR DU TERRAIN AU DOMAINE PUBLIC DE LA COMMUNE - B) INTÉGRATION DES MONUMENTS ET EMBLÈMES FUNÉRAIRES NON REPRIS AU DOMAINE PRIVÉ DE LA COMMUNE - 2) FORMALITÉS - OBLIGATION D'INFORMER LES TITULAIRES OU LEURS AYANTS-DROITS [RJ1].
</SCT>
<ANA ID="9A"> 135-02-02-06 1) a) Il résulte des dispositions des 3e et 4e alinéas de l'article L. 2223-15 du code général des collectivités territoriales (CGCT) qu'après l'expiration d'une concession, et si les concessionnaires ou leurs ayants-droits n'ont pas usé de leur droit à renouvellement dans les deux ans suivant son expiration, le terrain objet de cette concession funéraire, qui appartient au domaine public de la commune, fait retour à cette dernière.... ,,b) Les monuments et emblèmes funéraires qui ont pu être édifiés ou apposés sur ce terrain par les titulaires de cette concession, et qui n'ont pas été repris par ces derniers, sont intégrés au domaine privé de la commune à l'expiration de ce délai de deux ans.... ,,2) Il appartient au maire de rechercher par tout moyen utile à informer les titulaires d'une concession ou leurs ayants-droits de l'extinction de la concession et de leur droit à en demander le renouvellement dans les deux ans qui suivent.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Ab. jur. CE, 26 juillet 1985,,et autres, n° 36749, T. p. 524.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
