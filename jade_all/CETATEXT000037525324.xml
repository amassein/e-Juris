<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037525324</ID>
<ANCIEN_ID>JG_L_2018_10_000000403354</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/52/53/CETATEXT000037525324.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 24/10/2018, 403354, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-10-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403354</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:403354.20181024</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme D...C...a porté plainte contre Mme A...B...devant la chambre régionale de discipline de Provence-Alpes-Côte d'Azur-Corse de l'ordre des vétérinaires. Par une décision du 5 décembre 2014, la chambre régionale de discipline a infligé à Mme B...la sanction de suspension du droit d'exercer la profession de vétérinaire sur le territoire national pendant une période de deux mois, dont quarante-cinq jours avec sursis. <br/>
<br/>
              Par une décision du 8 juillet 2016, la chambre supérieure de discipline de l'ordre national des vétérinaires a rejeté l'appel formé par Mme B... contre cette décision.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un nouveau mémoire et un mémoire en réplique, enregistrés les 8 septembre et 7 décembre 2016, 16 juin 2017 et 5 avril 2018 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de Mme B...et à la SCP Rousseau, Tapie, avocat du conseil national de l'ordre des vétérinaires ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 27 septembre 2018, présentée par MmeB... ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B..., exerçant la profession de vétérinaire en tant que salariée d'une association de protection animale mentionné au III de l'article L. 214-6-1 du code rural et de la pêche maritime, a fait l'objet d'une plainte devant la juridiction disciplinaire de l'ordre des vétérinaires ; que par une décision du 5 décembre 2014, la chambre régionale de discipline l'a suspendue du droit d'exercer la profession de vétérinaire sur le territoire national pendant une période de deux mois, dont quarante-cinq jours avec sursis ; que par la décision contre laquelle elle se pourvoit en cassation, la chambre supérieure de discipline a rejeté l'appel qu'elle a formé contre cette sanction ;  <br/>
<br/>
              2. Considérant qu'aux termes des deux premiers alinéas de l'article R. 242-50 du code rural et de la pêche maritime, dans leur rédaction alors applicable : " Il est interdit de donner des consultations gratuites ou payantes dont peut tirer un bénéfice moral ou matériel une personne physique ou morale non habilitée légalement à exercer la profession vétérinaire et extérieure au contrat de soin. / Seules font exception aux dispositions du précédent alinéa les associations dont l'objet est la protection des animaux et qui sont habilitées par les dispositions du VI de l'article L. 214-6 à gérer des établissements dans lesquels les actes vétérinaires sont dispensés aux animaux des personnes dépourvues de ressources suffisantes. Ces actes sont gratuits. Les vétérinaires exerçant dans ces établissements ne peuvent être rétribués que par ceux-ci ou par l'association qui les gère, à l'exclusion de toute autre rémunération. Ils doivent obtenir des engagements pour le respect des dispositions qui précédent sous la forme d'un contrat qui garantit en outre leur complète indépendance professionnelle " ;<br/>
<br/>
              3. Considérant que, pour juger que Mme B...avait méconnu l'obligation de faire assurer la gratuité des soins, la chambre supérieure de discipline de l'ordre des vétérinaires s'est, en partie, fondée sur la circonstance que son contrat de travail prévoyait qu'elle devait obtenir la garantie de la gratuité des soins et le respect des dispositions déontologiques citées au point précédent et qu'elle avait, par suite, méconnu " ses obligations contractuelles " en pratiquant son art sans se soucier de l'incidence financière de sa pratique ; qu'en retenant ainsi une méconnaissance par l'intéressée de ses obligations contractuelles, alors qu'il ressortait des pièces du dossier qui lui était soumis que les stipulations en cause visaient seulement à garantir à Mme B...le respect, par son employeur, du principe de gratuité des soins, la chambre supérieure de discipline a dénaturé la portée de ces mêmes stipulations ; qu'elle ne pouvait par suite, fût-ce très partiellement, se fonder sur ce grief pour confirmer la sanction infligée en première instance à MmeB... ; que cette dernière est, par conséquent, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, fondée à demander l'annulation de la décision qu'elle attaque ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision de la chambre supérieure de discipline des vétérinaires du 8 juillet 2016 est annulée.<br/>
Article 2 : L'affaire est renvoyée à la chambre supérieure de discipline des vétérinaires.<br/>
<br/>
Article 3 : La présente décision sera notifiée à Mme A...B...et à Mme D...C....  <br/>
Copie en sera adressée au ministre de l'agriculture et de l'alimentation et au Conseil national de l'ordre des vétérinaires.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
