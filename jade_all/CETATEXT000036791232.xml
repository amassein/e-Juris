<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036791232</ID>
<ANCIEN_ID>JG_L_2018_04_000000413349</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/79/12/CETATEXT000036791232.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 11/04/2018, 413349</TITRE>
<DATE_DEC>2018-04-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413349</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Paul Bernard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:413349.20180411</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 6 juillet 2017, l'Agence française de lutte contre le dopage a prononcé à l'encontre de M. A...B...la sanction d'interdiction de participer pendant deux ans aux manifestations sportives organisées ou autorisées par la Fédération française d'équitation et d'interdiction de prendre part aux compétitions et manifestations sportives organisées ou autorisées par la Fédération française d'équitation, la Société hippique française, la Fédération française du sport d'entreprise, la Fédération sportive et culturelle de France, la Fédération sportive et gymnique du travail et l'Union française des oeuvres laïques d'éducation physique, a confirmé, sur le fondement des dispositions de l'article L. 232-23-2 du code du sport, la décision du 11 janvier 2017 de l'organe disciplinaire de première instance de la Fédération française d'équitation annulant les résultats obtenus le 2 octobre 2016 dans les épreuves 6 et 16 et a décidé qu'un  résumé de la sanction serait publié après sa notification. <br/>
<br/>
              Par une requête, enregistrée le 11 août 2017 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette décision ; <br/>
<br/>
              2°) de mettre à la charge de l'Agence française de lutte contre le dopage la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du sport ;<br/>
              - la loi n° 2012-158 du 1er février 2012 ;<br/>
              - la loi n° 2016-41 du 26 janvier 2016 ; <br/>
              - l'ordonnance n° 2010-379 du 14 avril 2010 ; <br/>
              - l'ordonnance n° 2015-1207 du 30 septembre 2015 ; <br/>
              - la décision n° 2017-688 QPC du 2 février 2018 du Conseil constitutionnel ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Paul Bernard, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M. B...et à la SCP Matuchansky, Poupot, Valdelièvre, avocat de l'agence française de lutte contre le dopage ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes du premier alinéa de l'article 61-1 de la Constitution : " Lorsque, à l'occasion d'une instance en cours devant une juridiction, il est soutenu qu'une disposition législative porte atteinte aux droits et libertés que la Constitution garantit, le Conseil constitutionnel peut être saisi de cette question sur renvoi du Conseil d'Etat ou de la Cour de cassation (...) " ; qu'aux termes du deuxième alinéa de son article 62 : " Une disposition déclarée inconstitutionnelle sur le fondement de l'article 61-1 est abrogée à compter de la publication de la décision du Conseil constitutionnel ou d'une date ultérieure fixée par cette décision. Le Conseil constitutionnel détermine les conditions et limites dans lesquelles les effets que la disposition a produits sont susceptibles d'être remis en cause " ; qu'enfin, aux termes du troisième alinéa du même article : " Les décisions du Conseil constitutionnel ne sont susceptibles d'aucun recours. Elles s'imposent aux pouvoirs publics et à toutes les autorités administratives et juridictionnelles " ; <br/>
<br/>
              2.	Considérant qu'il résulte des dispositions précitées de l'article 62 de la Constitution que lorsque le Conseil constitutionnel, saisi sur le fondement de l'article 61-1, use du pouvoir que lui confèrent les dispositions de l'article 62 en déterminant, après avoir déclaré inconstitutionnelle une disposition législative, les conditions et limites dans lesquelles les effets que la disposition a produits sont susceptibles d'être remis en cause, il appartient au juge administratif, saisi d'un litige relatif aux effets produits par la disposition déclarée inconstitutionnelle, de les remettre en cause en écartant, pour la solution de ce litige, le cas échéant d'office, cette disposition, dans les conditions et limites fixées par le Conseil constitutionnel ;<br/>
<br/>
              3.	Considérant qu'il résulte de l'instruction que M. B...a été soumis à un contrôle antidopage à l'occasion d'un concours de saut d'obstacles  qui s'est tenu à Lège-Cap-Ferret le 2 octobre 2016 ; que le résultat de ce contrôle ayant fait apparaître la présence d'une substance figurant sur la liste des substances dites " spécifiées " interdites, M. B...a fait l'objet, le 11 janvier 2017, d'une sanction d'interdiction de participer pendant trois mois aux manifestations sportives organisées par la Fédération française d'équitation, prononcée par l'organe disciplinaire de première instance de lutte contre le dopage de cette fédération ; que, sur le fondement du 3° de l'article L. 232-22 du code du sport, le collège de l'Agence française de lutte contre le dopage a décidé, le 9 février 2017, de se saisir de sa propre initiative des faits relevés à l'encontre de M. B...; que, par une décision du 6 juillet 2017, dont M. B...demande l'annulation, le collège de l'Agence a infligé à l'intéressé la sanction d'interdiction de participer pendant deux ans aux manifestations sportives organisées par la Fédération française d'équitation ainsi que par la Société hippique française, la Fédération française du sport d'entreprise, la Fédération sportive et culturelle de France, la Fédération sportive et gymnique du travail et l'Union française des oeuvres laïques d'éducation physique et a confirmé l'annulation des résultats obtenus par M. B...lors du concours de saut d'obstacles du 2 octobre 2016 ; <br/>
<br/>
              4.	Considérant qu'aux termes de l'article L. 232-22 du code du sport, issu de l'ordonnance du 14 avril 2010 relative à la santé des sportifs et à la mise en conformité du code du sport avec les principes du code mondial antidopage, ratifiée par la loi du 1er février 2012 visant à renforcer l'éthique du sport et les droits des sportifs : " En cas d'infraction aux dispositions des articles L. 232-9, L. 232-9-1, L. 232-10, L. 232-14-5, L. 232-15, L. 232-15-1 ou L. 232-17, l'Agence française de lutte contre le dopage exerce un pouvoir de sanction dans les conditions suivantes : / (...) 3° Elle peut réformer les décisions prises en application de l'article L. 232-21. Dans ces cas, l'agence se saisit, dans un délai de deux mois à compter de la réception du dossier complet, des décisions prises par les fédérations agréées " ;<br/>
<br/>
              5.	Considérant que, par sa décision n° 2017-688 QPC du 2 février 2018, le Conseil constitutionnel a déclaré contraires à la Constitution les dispositions du 3° de l'article L. 232-22 du code du sport ; que s'il a reporté au 1er septembre 2018 la date de l'abrogation de ces dispositions et a décidé qu'il y avait lieu de juger que " pour préserver le rôle régulateur confié par le législateur à l'agence française de lutte contre le dopage jusqu'à l'entrée en vigueur d'une nouvelle loi ou, au plus tard, jusqu'au 1er septembre 2018, le 3° de l'article L. 232-22 du code du sport impose à l'agence française de lutte contre le dopage de se saisir de toutes les décisions rendues en application de l'article L. 232-21 du même code postérieurement à la présente décision et de toutes les décisions rendues antérieurement à cette décision dont elle ne s'est pas encore saisie dans les délais légaux ", il a expressément jugé que " la déclaration d'inconstitutionnalité peut être invoquée dans toutes les instances relatives à une décision rendue sur le fondement de l'article L. 232-21 dont l'agence s'est saisie en application des dispositions contestées et non définitivement jugées à la date de la présente décision " ; <br/>
<br/>
              6.	Considérant que l'instance engagée par M. B... avant que n'intervienne la décision du Conseil constitutionnel n'était pas définitivement jugée à la date de cette décision ; que M. B...peut dès lors, conformément à ce qu'a jugé le Conseil constitutionnel, se prévaloir de l'inconstitutionnalité des dispositions du 3° de l'article L. 232-22 du code du sport ; qu'il est, en conséquence, fondé à demander l'annulation de la décision de l'Agence française de lutte contre le dopage qu'il attaque, qui a été prise sur le fondement des dispositions déclarées contraires à la Constitution ;  <br/>
<br/>
              7.	Considérant que si les parties intéressées peuvent, en vertu de l'article L. 232-24 du code du sport, former un recours de pleine juridiction contre les décisions de l'Agence française de lutte contre le dopage prises en application de l'article L. 232-22 du même code, il n'appartient pas au Conseil d'Etat, lorsque, saisi d'un tel recours, il annule la décision de sanction prise par l'Agence, de se substituer à l'Agence pour apprécier s'il y a lieu d'infliger à l'intéressé une sanction à raison des faits qui lui sont reprochés ; que, par suite, les conclusions présentées par l'Agence, tendant à ce que le Conseil d'Etat inflige lui-même une sanction à M. B... à raison des résultats du contrôle antidopage effectué le 2 octobre 2016, ne peuvent qu'être rejetées ; <br/>
<br/>
              8.	Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de sa requête, M. B...est fondé à demander l'annulation de la décision du 6 juillet 2017 de l'Agence française de lutte contre le dopage ;<br/>
<br/>
              9.	Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M.B..., qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, de mettre à la charge de l'Agence française de lutte contre le dopage une somme de 3 000 euros à verser à M. B...au titre des frais exposés et non compris dans les dépens ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La décision du 6 juillet 2017 de l'Agence française de lutte contre le dopage est annulée. <br/>
<br/>
Article 2 : Les conclusions de l'Agence française de lutte contre le dopage sont rejetées.<br/>
<br/>
Article 3 : L'Agence française de lutte contre le dopage versera à M. B...la somme de 3 000 euros sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et à l'Agence française de lutte contre le dopage. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-07-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. POUVOIRS DU JUGE DE PLEIN CONTENTIEUX. - RECOURS CONTRE LES DÉCISIONS DE SANCTIONS PRISES PAR L'AGENCE FRANÇAISE DE LUTTE CONTRE LE DOPAGE - POSSIBILITÉ POUR LE JUGE, EN CAS D'ANNULATION DE LA DÉCISION, D'INFLIGER UNE SANCTION - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">63-05-05 SPORTS ET JEUX. SPORTS. - RECOURS DE PLEINE JURIDICTION CONTRE LES DÉCISIONS DE SANCTIONS PRISES PAR L'AGENCE FRANÇAISE DE LUTTE CONTRE LE DOPAGE - POSSIBILITÉ POUR LE JUGE, EN CAS D'ANNULATION DE LA DÉCISION, D'INFLIGER UNE SANCTION - ABSENCE.
</SCT>
<ANA ID="9A"> 54-07-03 Si les parties intéressées peuvent, en vertu de l'article L. 232-24 du code du sport, former un recours de pleine juridiction contre les décisions de l'Agence française de lutte contre le dopage (AFLD) prises en application de l'article L. 232-22 du même code, il n'appartient pas au Conseil d'Etat, lorsque, saisi d'un tel recours, il annule la décision de sanction prise par l'Agence, de se substituer à cette dernière pour apprécier s'il y a lieu d'infliger à l'intéressé une sanction à raison des faits qui lui sont reprochés.</ANA>
<ANA ID="9B"> 63-05-05 Si les parties intéressées peuvent, en vertu de l'article L. 232-24 du code du sport, former un recours de pleine juridiction contre les décisions de l'Agence française de lutte contre le dopage (AFLD) prises en application de l'article L. 232-22 du même code, il n'appartient pas au Conseil d'Etat, lorsque, saisi d'un tel recours, il annule la décision de sanction prise par l'Agence, de se substituer à cette dernière pour apprécier s'il y a lieu d'infliger à l'intéressé une sanction à raison des faits qui lui sont reprochés.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
