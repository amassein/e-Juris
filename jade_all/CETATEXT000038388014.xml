<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038388014</ID>
<ANCIEN_ID>JG_L_2019_04_000000421099</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/38/80/CETATEXT000038388014.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 17/04/2019, 421099, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-04-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421099</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Matias de Sainte Lorette</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:421099.20190417</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Par une requête, enregistrée le 31 mai 2018 au secrétariat du contentieux du Conseil d'Etat sur le n° 421099, la société MEI Partners demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) de suspendre la décision par laquelle le ministre de l'économie et des finances a refusé de notifier les aides sous formes de garantie autonome à première demande de l'établissement public Bpifrance, octroyées aux émissions d'emprunts obligataires de la société Bpifrance Financement SA, jusqu'à ce qu'il ait été statué au fond dans la requête n° 421061 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Par une requête, enregistrée le 14 juin 2018 au secrétariat du contentieux du Conseil d'Etat sous le n° 421495, la société MEI Partners demande au juge des référés du Conseil d'Etat, sur le fondement de l'article R. 541-1 du code de justice administrative :<br/>
<br/>
              1°) de condamner l'Etat à lui verser une provision de 27 704 640 euros, qui, en l'absence de paiement avant le 21 juin 2018, sera portée à 30 475 104 euros, majorée des intérêts de 0,89 % par an à compter de cette même date, à valoir sur l'indemnité de gestion d'affaires qu'elle a demandée dans sa requête n° 421061 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matias de Sainte Lorette, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les deux demandes en référé de la société MEI Partners se rattachent au même litige au principal. Il y a lieu de les joindre pour y statuer par une même décision.<br/>
<br/>
              Sur les dispositions applicables au litige :<br/>
<br/>
              2. En premier lieu, d'une part, le premier alinéa de l'article L. 521-1 du code de justice administrative dispose que : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". D'autre part, aux termes de l'article R. 541-1 du code de justice administrative : " Le juge des référés peut, même en l'absence d'une demande au fond, accorder une provision au créancier qui l'a saisi lorsque l'existence de l'obligation n'est pas sérieusement contestable ".<br/>
<br/>
              3. En second lieu, l'article R. 522-8-1 du même code dispose que : " Par dérogation aux dispositions du titre V du livre III du présent code, le juge des référés qui entend décliner la compétence de la juridiction rejette les conclusions dont il est saisi par voie d'ordonnance ". Enfin, il résulte de l'article L. 511-2 du même code qu'une demande présentée sur le fondement de l'article L. 521-1 de ce code peut être renvoyée à une formation de jugement dans les conditions de droit commun.<br/>
<br/>
              Sur les conclusions à fin de suspension en référé :<br/>
<br/>
              4. Par une décision n° 421061 du 14 décembre 2018, le Conseil d'Etat, statuant au contentieux a décliné sa compétence pour statuer sur les conclusions de la requête de la société MEI Partners dirigées contre la décision du ministre de l'économie et des finances refusant de notifier à la Commission européenne des aides qui auraient été octroyées sous forme de garantie à première demande par l'établissement public Bpifrance à la société Bpifrance Financement SA.<br/>
<br/>
              5. Il résulte de ce qui précède que les conclusions tendant à la suspension de l'exécution de cette décision doivent être rejetées en vertu de l'article R. 522-8-1 du code de justice administrative.<br/>
<br/>
              Sur les conclusions à fin de versement d'une provision en référé :<br/>
<br/>
              6. Par la décision n° 421061 du 14 décembre 2018 mentionnée au point 4, le Conseil d'Etat, statuant au contentieux a également décliné sa compétence pour statuer sur les conclusions de la même requête par lesquelles la société MEI Partners demandait qu'il soit enjoint à l'Etat, sous astreinte, de justifier de la récupération effective des montants d'aides illégales, en principal et intérêts, et d'informer la présidente de la section du rapport et des études du Conseil d'Etat de l'indemnisation de la société requérante au titre de sa gestion d'affaires. Il a attribué le jugement de ces conclusions au tribunal administratif de Paris.<br/>
<br/>
              7. Le juge des référés du Conseil d'Etat ne peut être régulièrement saisi en premier et dernier ressort d'une demande tendant à la mise en oeuvre de l'une des procédures régies par le livre V du code de justice administrative que pour autant que le litige principal auquel se rattache la mesure provisoire sollicitée ressortit elle-même à la compétence directe du Conseil d'Etat. Par ailleurs, la demande présentée devant le juge des référés du Conseil d'Etat ne relevant pas du titre II livre V du code de justice administrative, auquel cas il y aurait lieu de la rejeter en application de l'article R. 522-8-1 de ce code, mais étant une demande de référé provision relevant du titre IV du livre V du même code, il y a lieu de faire application de l'article R. 351-1 de ce même code et d'attribuer son jugement au tribunal administratif de Paris.<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que l'Etat, qui n'est pas la partie perdante dans la présente instance, soit condamné à verser à la société MEI Partners les sommes que celle-ci demande au titre des frais exposés et non compris dans les dépens.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête n° 421099 de la société MEI Partners est rejetée.<br/>
<br/>
Article 2 : Le jugement de la requête n° 421495 est attribué au tribunal administratif de Paris.<br/>
Article 3 : La présente décision sera notifiée à la société MEI Partners et au ministre de l'économie et des finances.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
