<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025635796</ID>
<ANCIEN_ID>J6_L_2012_03_000001002510</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/63/57/CETATEXT000025635796.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Cour Administrative d'Appel de Marseille, 8ème chambre - formation à 3, 20/03/2012, 10MA02510, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-03-20</DATE_DEC>
<JURIDICTION>Cour Administrative d'Appel de Marseille</JURIDICTION>
<NUMERO>10MA02510</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre - formation à 3</FORMATION>
<TYPE_REC>plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. GONZALES</PRESIDENT>
<AVOCATS>SCP D'AVOCATS CGCB &amp; ASSOCIES</AVOCATS>
<RAPPORTEUR>M. Jean-Baptiste  BROSSIER</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme VINCENT-DOMINGUEZ</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée au greffe de la Cour le 1er juillet 2010 sous le n° 10MA02510, présentée par le cabinet d'avocats CGCB, pour M. Francis A, demeurant ... ; <br/>
<br/>
       M. Francis A demande à la Cour :<br/>
<br/>
       1°)	d'annuler le jugement n° 0803833 du 9 avril 2010 par lequel le tribunal administratif de Nîmes a rejeté sa demande tendant à la condamnation de l'État à lui verser une indemnité de 20.000 euros, augmentée des intérêts au taux légal et du produit de leur capitalisation, en raison du caractère fautif des écritures du ministre qui attentent à son honneur, décomposée en 15.000 euros au titre des accusations mensongères réitérées et 5.000 euros au titre des propos diffamatoires de son chef de service, ensemble la somme de 1.500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
       2°)	de condamner l'État à lui verser ladite indemnité de 20.000 euros ;<br/>
<br/>
       3°)	de mettre à la charge de l'État la somme de 1.000 euros au titre de l'article <br/>
L. 761-1 du code de justice administrative ;<br/>
<br/>
       Vu le jugement attaqué ; <br/>
<br/>
       Vu les autres pièces du dossier ;<br/>
<br/>
       Vu la loi modifiée n° 83-634 du 13 juillet 1983 portant droits et obligations des fonctionnaires ;<br/>
<br/>
       Vu la loi modifiée n° 84-16 du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'État ;<br/>
<br/>
       Vu le code de justice administrative ; <br/>
<br/>
       Les parties ayant été régulièrement averties du jour de l'audience ;<br/>
<br/>
       Après avoir entendu au cours de l'audience publique du 14 février 2012 :<br/>
<br/>
       - le rapport de M. Brossier, rapporteur,<br/>
<br/>
       - les conclusions de Mme Vincent-Dominguez, rapporteur public ; <br/>
<br/>
       Considérant que M. A, attaché principal de 1ère classe des services déconcentrés du ministère chargé de l'équipement, a été mise à disposition du service départemental de l'architecture et du patrimoine (SDAP) du Gard à compter du <br/>
1er septembre 1993, en qualité d'adjoint au chef de service et placé sous l'autorité fonctionnelle du chef de ce service ; qu'il a été muté au centre d'études sur les réseaux, les transports, l'urbanisme et les constructions publiques (CERTU) de Lyon, par arrêté du 20 octobre 2006, en qualité de consultant juridique avec effet au 1er novembre 2006 ; que par l'article 5 devenu définitif du jugement n° 0606874-0606875-0607147-0700854-0701179-0707057-0707058-0707059-0707060 rendu le 26 juin 2008, le tribunal administratif de Lyon a annulé pour excès de pouvoir cette mutation ; que par l'article 7 devenu définitif de ce jugement, ledit Tribunal a décidé la suppression, dans les écritures contentieuses du ministre chargé de l'équipement, de mentions injurieuses, outrageantes ou diffamatoires au sens de l'article L. 741-2 du code de justice administrative ; que M. A demande une indemnisation en raison du caractère fautif des écritures du ministre qui attentent à son honneur ;<br/>
<br/>
       Sur la recevabilité :<br/>
<br/>
       Considérant qu'il ressort des termes de la requête introductive d'appel que la fin de non-recevoir opposée par le ministre intimé, tirée de l'insuffisante motivation de l'argumentation devant la Cour de M. A qui ne critiquerait pas la réponse des premiers juges, manque en fait ; <br/>
<br/>
       Sur le bien-fondé :<br/>
<br/>
       Considérant qu'aux termes des alinéas 3, 4 et 5 de l'article 41 de la loi du 29 juillet 1881, aux dispositions desquelles renvoie l'article L. 741-2 du code de justice administrative : " Ne donneront lieu à aucune action en diffamation, injure ou outrage ni les propos tenus ou les écrits produits devant une commission d'enquête créée, en leur sein, par l'Assemblée nationale ou le Sénat, par la personne tenue d'y déposer, sauf s'ils sont étrangers à l'objet de l'enquête, ni le compte rendu fidèle des réunions publiques de cette commission fait de bonne foi. Ne donneront lieu à aucune action en diffamation, injure ou outrage, ni le compte rendu fidèle fait de bonne foi des débats judiciaires, ni les discours prononcés ou les écrits produits devant les tribunaux. Pourront néanmoins les juges, saisis de la cause et statuant sur le fond, prononcer la suppression des discours injurieux, outrageants ou diffamatoires, et condamner qui il appartiendra à des dommages-intérêts. Pourront toutefois les faits diffamatoires étrangers à la cause donner ouverture, soit à l'action publique, soit à l'action civile des parties, lorsque ces actions leur auront été réservées par les tribunaux, et, dans tous les cas, à l'action civile des tiers " ; qu'en vertu de ces dispositions, les tribunaux peuvent ordonner la suppression des mentions injurieuses, outrageantes ou diffamatoires contenues dans les productions des parties et tiennent, en outre, de ces dispositions le pouvoir de condamner l'auteur de ces mentions à des dommages-intérêts ; <br/>
<br/>
       Considérant qu'il résulte de l'instruction que M. A, qui demande l'indemnisation en raison du caractère fautif des écritures du ministre qui attentent à son honneur, réclame la somme totale de 20.000 euros qu'il décompose en 15.000 euros au titre des accusations mensongères réitérées et 5.000 euros au titre des propos diffamatoires de son chef de service ; que compte tenu de son argumentation, il doit être regardé comme demandant la réparation des conséquences dommageables de la présence de mentions injurieuses, outrageantes ou diffamatoires dans l'instance contentieuse susmentionnée qui s'est tenue devant le tribunal administratif de Lyon, alors même que la suppression de passages identiques avait déjà été décidée dans une précédente instance qui s'est déroulée devant la Cour ; que par le jugement attaqué, le Tribunal administratif de Nîmes a rejeté cette demande d'indemnisation au motif que M. A ne pouvait se prévaloir d'une faute commise par l'administration par méconnaissance de la chose jugée, dès lors que les décisions juridictionnelles en cause ne sont pas, en ce qu'elles ont ordonné la suppression desdites mentions, revêtues de l'autorité absolue de la chose jugée qui s'attache à l'annulation pour excès de pouvoir des décisions administratives et aux motifs qui en constituent le support nécessaire ; <br/>
<br/>
       Considérant, en premier lieu, qu'il résulte de l'instruction que dans un arrêt de n° 02MA00019 du 13 décembre 2005, la Cour de céans a décidé, en application de <br/>
l'article L. 741-2 du code de justice administrative, de supprimer les mentions injurieuses, outrageantes ou diffamatoires contenues dans les écritures contentieuses du ministre chargé de l'équipement enregistrées le 19 juin 2003, commençant par les mots " c'est dans ce contexte de conflit " se terminant par les mots " imitant la signature de son chef de service " à la page 2, ainsi que le passage " qu'il ait établi en outre un faux rapport en 1997 en vue de sa nomination à l'emploi de conseiller d'administration de l'équipement " ; <br/>
<br/>
       Considérant, en deuxième lieu, qu'il résulte de l'instruction que dans les instances n° 0606875 et n° 0607147, sur lesquelles le Tribunal administratif de Lyon a statué le <br/>
26 juin 2008, ledit tribunal a décidé, en application de l'article L. 741-2 du code de justice administrative, de supprimer les passages injurieux, outrageants ou diffamatoires contenus dans les écritures contentieuses du ministre chargé de l'équipement enregistrées le 11 février 2008 et comportant les mentions "c'est dans ce contexte de conflit ouvert que le requérant, désireux de postuler à un emploi fonctionnel de conseiller d'administration de l'équipement au titre de l'année 1997, a produit un faux rapport sur sa manière de servir en imitant la signature de son chef de service ", " le requérant a d'ailleurs produit un faux rapport sur sa manière de servir en imitant la signature de son chef de service ", ainsi que l'indication, " aux motifs qu'il n'est pas satisfait du déroulement de sa carrière et des réponses négatives faites à ses demandes de mutation. Par ses actions, il espère amener le maire de Nîmes à intervenir auprès du ministre de l'équipement " ;<br/>
<br/>
       Considérant, en troisième lieu, qu'il résulte de ce qui précède que l'État (ministère chargé de l'équipement) a réitéré dans ses écritures devant le tribunal administratif de Lyon des passages injurieux, outrageants ou diffamatoires, alors même que de tels passages avaient déjà été supprimés par une autre juridiction dans une instance précédente l'opposant à M. A et que ces passages ont trait, en partie, à la même mention erronée relative à l'imitation par l'intéressé de la signature de son chef de service ; qu'en réitérant ainsi à l'encontre de M. A des passages injurieux, outrageants ou diffamatoires, l'État a commis une faute de nature à engager sa responsabilité, nonobstant l'absence d'autorité pouvant exister entre la chose jugée par l'arrêt de 2005 de la Cour de céans et celle jugée par le tribunal administratif de Lyon en 2008 ; qu'il s'ensuit que M. A est fondé à demander l'annulation du jugement attaqué du tribunal administratif de Nîmes ; qu'il y a lieu pour la Cour, par l'effet dévolutif de l'appel, de statuer sur les conclusions indemnitaires de M. A ; <br/>
<br/>
       Considérant qu'il résulte de l'instruction que M. A est fondé à se prévaloir d'un préjudice moral, né de l'atteinte à son honneur, en lien suffisamment direct et certain avec la faute susmentionnée ; qu'il sera fait une juste appréciation des circonstances très particulières de l'espèce en lui allouant à ce titre la somme de 500 euros, tous intérêt confondus ; que le surplus de ses conclusions indemnitaires doit être rejeté ; <br/>
<br/>
       Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
       Considérant qu'aux termes des dispositions de l'article L. 761-1 du code de justice administrative : " Dans toutes les instances, le juge condamne la partie tenue aux dépens ou, à défaut, la partie perdante, à payer à l'autre partie la somme qu'il détermine, au titre des frais exposés et non compris dans les dépens. Le juge tient compte de l'équité ou de la situation économique de la partie condamnée. Il peut, même d'office, pour des raisons tirées des mêmes considérations, dire qu'il n'y a pas lieu à cette condamnation " ; <br/>
<br/>
       Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'État la somme de 500 euros au titre des frais non compris dans les dépens exposés par l'appelant devant la Cour ;<br/>
DECIDE :<br/>
Article 1er :	Le jugement attaqué susvisé du tribunal administratif de Nîmes est annulé.<br/>
Article 2	:	L'État (ministère de l'écologie, du développement durable, des transports et du logement) est condamné à verser à M. A une indemnité de 500 (cinq cents) euros, tous intérêts confondus, en réparation de son préjudice moral.<br/>
Article 3	:	L'État (ministère de l'écologie, du développement durable, des transports et du logement) versera à M. A la somme de 500 (cinq cents) euros au titre des frais non compris dans les dépens qu'il a exposés en appel.<br/>
Article 4	:	Le surplus de l'appel n° 10MA02510 de M. A est rejeté.<br/>
Article 5	:	Le présent arrêt sera notifié à M. Francis A et au ministre de l'écologie, du développement durable, des transports et du logement.<br/>
''<br/>
''<br/>
''<br/>
''<br/>
N° 10MA025102<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-13-03 Fonctionnaires et agents publics. Contentieux de la fonction publique. Contentieux de l'indemnité.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
