<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032892436</ID>
<ANCIEN_ID>JG_L_2016_07_000000399829</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/89/24/CETATEXT000032892436.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section, 13/07/2016, 399829, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>399829</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>Mme Marie Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2016:399829.20160713</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...C...et Mme A...D..., épouse C...ont demandé au juge des référés du tribunal administratif de Clermont-Ferrand, sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
              - d'enjoindre au préfet du Puy-de-Dôme d'organiser leur accueil et de leur fournir des conditions d'hébergement, d'habillement et de nourriture décentes ;<br/>
              - d'enjoindre au département du Puy-de-Dôme de financer à leur profit un hébergement de type hôtelier par l'attribution d'une aide financière mensuelle.<br/>
<br/>
              Par une ordonnance n° 1600721 du 29 avril 2016, le juge des référés a enjoint au département du Puy-de-Dôme d'accorder, dès la notification de son ordonnance, une aide financière à M. et Mme C...pour se loger avec leurs deux enfants mineurs.<br/>
<br/>
              Par une requête, deux nouveaux mémoires et un mémoire en réplique, enregistrés les 17 mai, 24 mai, 2 juin et 29 juin 2016 au secrétariat du contentieux du Conseil d'Etat, le département du Puy-de-Dôme demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative : <br/>
<br/>
              1°) d'annuler ou de réformer cette ordonnance ; <br/>
<br/>
              2°) de rejeter la demande de première instance de M. et Mme C....<br/>
<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 25 mai 2016 et les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la convention internationale relative aux droits de l'enfant ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Sirinelli, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat du département du Puy-de-Dôme, à la SCP Sevaux, Mathonnet, avocat de M. et Mme B...C... ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ; <br/>
<br/>
              2. Considérant qu'il résulte de l'instruction que Mme C..., ressortissante russe, d'origine tchétchène, a été hébergée et prise en charge par l'Etat au centre d'accueil pour demandeurs d'asile de Cébazat, dans le Puy-de-Dôme, à compter du mois de juillet 2015, avec ses deux enfants, nés le 31 décembre 2013 et le 24 novembre 2015 ; que cette prise en charge a cessé le 21 avril 2016, à la suite du rejet définitif de sa demande d'asile, le 8 mars 2016 ; qu'elle a alors sollicité, avec son conjoint dont la seconde demande de réexamen au titre de l'asile avait été définitivement rejetée en avril 2014, un hébergement d'urgence auprès de l'Etat, tout en saisissant le département du Puy-de-Dôme d'une demande identique le 25 avril 2016 ; que, dans la nuit du 27 au 28 avril, M. et Mme C...ont pu bénéficier d'un hébergement fourni par l'Etat ; qu'ils ont saisi le lendemain, sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative, le juge des référés du tribunal administratif de Clermont-Ferrand d'une demande tendant à ce qu'il soit enjoint au préfet du Puy-de-Dôme et au département du Puy-de-Dôme de leur fournir, sans délai, un hébergement, notamment par le biais d'une aide financière mensuelle ; que le département du Puy-de-Dôme relève appel de l'ordonnance du juge des référés du tribunal administratif de Clermont-Ferrand du 29 avril 2016, qui lui a enjoint d'accorder à M. et MmeC..., dès la notification de son ordonnance, une aide financière afin qu'ils puissent se loger avec leurs deux enfants ;<br/>
<br/>
              Sur l'intervention de l'Assemblée des départements de France :<br/>
<br/>
              3. Considérant que l'Assemblée des départements de France justifie, eu égard à son objet statutaire et aux questions soulevées par le litige, d'un intérêt suffisant pour intervenir au soutien de l'appel du département du Puy-de-Dôme ; qu'ainsi, son intervention est recevable ; <br/>
<br/>
              Sur l'appel du département :<br/>
<br/>
              4. Considérant, d'une part, que l'article L. 345-2 du code de l'action sociale et des familles prévoit que, dans chaque département, est mis en place, sous l'autorité du préfet, " un dispositif de veille sociale chargé d'accueillir les personnes sans abri ou en détresse " ; que l'article L. 345-2-2 dispose que : " Toute personne sans abri en situation de détresse médicale, psychique ou sociale a accès, à tout moment, à un dispositif d'hébergement d'urgence (...) " ; qu'aux termes de l'article L. 345-2-3 : " Toute personne accueillie dans une structure d'hébergement d'urgence doit pouvoir y bénéficier d'un accompagnement personnalisé et y demeurer, dès lors qu'elle le souhaite, jusqu'à ce qu'une orientation lui soit proposée (...) " ; qu'aux termes de l'article L. 121-7 du même code : " Sont à la charge de l'Etat au titre de l'aide sociale : (...) 8° Les mesures d'aide sociale en matière de logement, d'hébergement et de réinsertion, mentionnées aux articles L. 345-1 à L. 345-3 (...) " ;<br/>
<br/>
              5. Considérant, d'autre part, qu'aux termes du premier alinéa de l'article L. 222-2 du code de l'action sociale et des familles : " L'aide à domicile est attribuée sur sa demande, ou avec son accord, à la mère, au père ou, à défaut, à la personne qui assume la charge effective de l'enfant, lorsque la santé de celui-ci, sa sécurité, son entretien ou son éducation l'exigent et, pour les prestations financières, lorsque le demandeur ne dispose pas de ressources suffisantes " ; qu'aux termes de l'article L. 222-3 du même code : " L'aide à domicile comporte, ensemble ou séparément : (...) - le versement d'aides financières, effectué sous forme soit de secours exceptionnels, soit d'allocations mensuelles, à titre définitif ou sous condition de remboursement, éventuellement délivrés en espèces " ; qu'aux termes de l'article L. 222-5 du même code : " Sont pris en charge par le service de l'aide sociale à l'enfance sur décision du président du conseil départemental : 1° Les mineurs qui ne peuvent demeurer provisoirement dans leur milieu de vie habituel et dont la situation requiert un accueil (...) 4° Les femmes enceintes et les mères isolées avec leurs enfants de moins de trois ans qui ont besoin d'un soutien matériel et psychologique, notamment parce qu'elles sont sans domicile (...) " ; qu'il résulte des dispositions de l'article L. 122-1 du même code que les prestations légales versées au titre de l'aide sociale à l'enfance sont à la charge du département dans lequel les bénéficiaires ont leur domicile de secours ou, à défaut, dans lequel ils résident au moment de leur demande d'admission à l'aide sociale ;<br/>
<br/>
              6. Considérant qu'il appartient aux autorités de l'Etat, sur le fondement des dispositions citées au point 4, de mettre en oeuvre le droit à l'hébergement d'urgence reconnu par la loi à toute personne sans abri qui se trouve en situation de détresse médicale, psychique ou sociale ; qu'une carence caractérisée dans l'accomplissement de cette mission peut faire apparaître, pour l'application de l'article L. 521-2 du code de justice administrative, une atteinte grave et manifestement illégale à une liberté fondamentale lorsqu'elle entraîne des conséquences graves pour la personne intéressée ; qu'il incombe au juge des référés d'apprécier dans chaque cas les diligences accomplies par l'administration en tenant compte des moyens dont elle dispose ainsi que de l'âge, de l'état de la santé et de la situation de famille de la personne intéressée ; que, les ressortissants étrangers qui font l'objet d'une obligation de quitter le territoire français ou dont la demande d'asile a été définitivement rejetée et qui doivent ainsi quitter le territoire en vertu des dispositions de l'article L. 743-3 du code de l'entrée et du séjour des étrangers et du droit d'asile n'ayant pas vocation à bénéficier du dispositif d'hébergement d'urgence, une carence constitutive d'une atteinte grave et manifestement illégale à une liberté fondamentale ne saurait être caractérisée, à l'issue de la période strictement nécessaire à la mise en oeuvre de leur départ volontaire, qu'en cas de circonstances exceptionnelles ; que constitue une telle circonstance, en particulier lorsque, notamment du fait de leur très jeune âge, une solution appropriée ne pourrait être trouvée dans leur prise en charge hors de leur milieu de vie habituel par le service de l'aide sociale à l'enfance, l'existence d'un risque grave pour la santé ou la sécurité d'enfants mineurs, dont l'intérêt supérieur doit être une considération primordiale dans les décisions les concernant ;<br/>
<br/>
              7. Considérant, enfin, que la compétence de l'Etat en matière d'hébergement d'urgence n'exclut pas l'intervention du département par la voie d'aides financières destinées à permettre temporairement l'hébergement des familles lorsque la santé des enfants, leur sécurité, leur entretien ou leur éducation l'exigent, sur le fondement de l'article L. 222-3 précité du code de l'action sociale et des familles ; que, toutefois, de telles prestations ne sont pas d'une nature différente de celles que l'Etat pourrait fournir en cas de saturation des structures d'hébergement d'urgence ; que les besoins des enfants ne sauraient faire l'objet d'une appréciation différente selon la collectivité amenée à prendre en charge, dans l'urgence, l'hébergement de la famille ; qu'ainsi, dès lors que ne sont en cause ni des mineurs relevant d'une prise en charge par le service de l'aide sociale à l'enfance en application de l'article L. 222-5 du même code, ni des femmes enceintes ou des mères isolées avec leurs enfants de moins de trois ans mentionnées au 4° du même article, l'intervention du département ne revêt qu'un caractère supplétif, dans l'hypothèse où l'Etat n'aurait pas accompli les diligences qui lui reviennent, et ne saurait entraîner une quelconque obligation à la charge du département dans le cadre d'une procédure d'urgence qui a précisément pour objet de prescrire, à l'autorité principalement compétente, les diligences qui s'avéreraient nécessaires ;<br/>
<br/>
              8. Considérant que la demande de M. et Mme C... portait seulement sur la fourniture d'un hébergement d'urgence pour leur famille ou de prestations équivalentes ; qu'il résulte de ce qui a été dit ci-dessus, et sans qu'il soit besoin d'examiner les autres moyens soulevés par le département du Puy-de-Dôme et par l'Assemblée des départements de France, que c'est à tort que le juge des référés, après avoir relevé l'absence de carence caractérisée des autorités de l'Etat dans la mise en oeuvre du droit de M. et Mme C...à l'hébergement d'urgence, constitutive d'une atteinte grave et manifestement illégale à une liberté fondamentale, a constaté l'existence d'une telle carence de la part de l'administration départementale et a enjoint au département de leur accorder une aide financière pour se loger avec leurs enfants mineurs ; qu'il y a lieu, par suite, d'annuler les articles 2 et 3 de l'ordonnance du juge des référés du tribunal administratif de Clermont-Ferrand du 29 avril 2016 ;<br/>
<br/>
              Sur les conclusions de M. et Mme C...dirigées contre l'Etat :<br/>
<br/>
              9. Considérant, d'une part, qu'il résulte de l'instruction, et notamment de l'audience qui s'est tenue devant le juge des référés du Conseil d'Etat, que M. et Mme C... ne disposent d'aucun hébergement depuis le 22 avril 2016, et vivent ainsi dans la rue avec leurs deux enfants mineurs, sous la seule réserve des mesures prises en exécution de l'article 2 de l'ordonnance du juge des référés du tribunal administratif, annulé par la présente décision ; que leur situation révèle, dans ces conditions, une situation d'urgence de nature à justifier l'intervention du juge des référés dans les conditions prévues par l'article L. 521-2 du code de justice administrative ;<br/>
<br/>
              10. Considérant, d'autre part, que l'Etat a accompli des efforts très importants pour accroître les capacités d'hébergement d'urgence dans le département du Puy-de-Dôme au cours des années récentes, sans parvenir pour autant à répondre à l'ensemble des besoins les plus urgents, y compris par un hébergement hôtelier ; qu'il résulte, toutefois, de l'instruction que la fille des demandeurs est née le 24 novembre 2015 ; qu'au regard du très jeune âge de cette enfant, constitutif d'une circonstance exceptionnelle au sens du point 6 ci-dessus, et des diligences accomplies par l'administration, qui n'a fourni à la famille C...qu'un hébergement pour une nuit depuis la fin de la prise en charge de la mère et des enfants dans le cadre du dispositif national d'accueil des demandeurs d'asile, l'Etat doit être regardé comme ayant porté une atteinte grave et manifestement illégale à une liberté fondamentale ; qu'il y a lieu, en conséquence, d'enjoindre au préfet du Puy-de-Dôme d'assurer l'hébergement d'urgence des demandeurs et de leurs enfants à compter de la notification de la présente ordonnance ; que cette injonction est sans incidence sur les mesures que l'Etat pourrait prendre pour assurer l'éloignement de la familleC... ; <br/>
<br/>
              Sur les conclusions présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 :<br/>
<br/>
              11. Considérant que M. et Mme C...ont obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, leur avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Sevaux, Mathonnet, avocat de M. et MmeC..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat une somme de 1 500 à verser à cette société ; qu'en revanche, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du département du Puy-de-Dôme, qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'intervention de l'Assemblée des départements de France est admise.<br/>
Article 2 : Les articles 2 et 3 de l'ordonnance du juge des référés du tribunal administratif de Clermont-Ferrand du 29 avril 2016 sont annulés.<br/>
Article 3 : Il est enjoint au préfet du Puy-de-Dôme d'assurer l'hébergement d'urgence de M. et MmeC..., ainsi que de leurs enfants, à compter de la notification de la présente ordonnance. <br/>
Article 4 : L'Etat versera à la SCP Sevaux, Mathonnet, avocat de M. et MmeC..., une somme de 1 500 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat. <br/>
Article 5 : Le surplus des conclusions présentées par M. et Mme C... devant le tribunal administratif de Clermont-Ferrand est rejeté.<br/>
Article 6 : La présente décision sera notifiée au département du Puy-de-Dôme, à M. B...C...et Mme A...D..., épouseC..., à la ministre des affaires sociales et de la santé et à l'Assemblée des départements de France.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
