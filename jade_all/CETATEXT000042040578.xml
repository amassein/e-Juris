<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042040578</ID>
<ANCIEN_ID>JG_L_2020_06_000000441199</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/04/05/CETATEXT000042040578.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 19/06/2020, 441199, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441199</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:441199.20200619</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 15 juin 2020 au secrétariat du contentieux du Conseil d'Etat, M. E... I..., M. H... F..., l'association Victimes Coronavirus Covid-19 France, M. D... A..., Mme G... B..., M. J... C..., l'association Ametist, l'association Vaincre lyme et l'association LNPLV demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de l'article 6-2 de l'arrêté du ministre des solidarités et de la santé du 23 mars 2020 prescrivant les mesures d'organisation et de fonctionnement du système de santé nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire, créé par arrêté du 26 mai 2020 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Ils soutiennent que : <br/>
              - la condition d'urgence est remplie, eu égard, d'une part, à la gravité de l'atteinte portée aux libertés fondamentales que constituent la liberté de prescription du médecin, le droit à la vie des patients et leur droit de recevoir les traitements et les soins appropriés à leur état de santé, et d'autre part à l'absence de justification de telles atteintes par des motifs tirés de la lutte contre la propagation du virus à l'origine de la maladie dite covid-19 ;<br/>
              - il est porté une atteinte grave et manifestement illégale aux libertés précitées dès lors que les dispositions contestées, d'une part, sont entachées d'incompétence puisque seul le Premier ministre peut, en application de l'article L. 3131-15 du code de la santé publique, limiter les conditions dans lesquelles un médicament est mis à disposition des patients dans le cadre de l'épidémie de covid-19, et d'autre part, méconnaissent l'habilitation donnée au ministre de la santé par l'article L. 3131-16 du même code, qui ne permet pas de porter atteinte au dispositions législatives protégeant liberté de prescription des médecins et qui est limitée aux mesures visant à mettre fin à la catastrophe sanitaire.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
            - le code de la santé publique ;<br/>
            - la loi n°2020-290 du 23 mars 2020 ;<br/>
            - la loi n°2020-546 du 11 mai 2020 ;<br/>
            - le décret n°2020-260 du 16 mars 2020 ;<br/>
            - le décret n°2020-293 du 23 mars 2020 ;<br/>
            - le décret n° 2020-337 du 26 mars 2020<br/>
            - le décret n°2020-545 du 11 mai 2020 ;<br/>
            - le décret n°2020-548 du 11 mai 2020 ;<br/>
            - le décret n° 2020-630 du 26 mai 2020 ;<br/>
              -  le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              Sur l'office du juge des référés et les libertés fondamentales en jeu :<br/>
<br/>
              2. Il résulte de la combinaison des dispositions des articles L. 511-1 et L. 521-2 du code de justice administrative qu'il appartient au juge des référés, lorsqu'il est saisi sur le fondement de l'article L. 521-2 et qu'il constate une atteinte grave et manifestement illégale portée par une personne morale de droit public à une liberté fondamentale, résultant de l'action ou de la carence de cette personne publique, de prescrire les mesures qui sont de nature à faire disparaître les effets de cette atteinte, dès lors qu'existe une situation d'urgence caractérisée justifiant le prononcé de mesures de sauvegarde à très bref délai et qu'il est possible de prendre utilement de telles mesures. Celles-ci doivent, en principe, présenter un caractère provisoire, sauf lorsque aucune mesure de cette nature n'est susceptible de sauvegarder l'exercice effectif de la liberté fondamentale à laquelle il est porté atteinte. Le caractère manifestement illégal de l'atteinte doit s'apprécier notamment en tenant compte des moyens dont dispose l'autorité administrative compétente et des mesures qu'elle a déjà prises.<br/>
<br/>
              3. Pour l'application de l'article L. 521-2 du code de justice administrative, la liberté d'exercice et l'indépendance professionnelle des médecins dans la pratique de leur art, qui s'exerce dans l'intérêt des patients, le droit à la vie et le droit de recevoir les traitements et les soins appropriés à son état de santé constituent des libertés fondamentales au sens des dispositions de cet article. <br/>
              Sur les circonstances ainsi que les mesures prises par le Premier ministre et le ministre des solidarités et de la santé :<br/>
<br/>
              4. L'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre des solidarités et de la santé puis le Premier ministre à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Le législateur, par l'article 4 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19, a déclaré l'état d'urgence sanitaire pour une durée de deux mois à compter du 24 mars 2020, puis, par l'article 1er de la loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ses dispositions, a prorogé cet état d'urgence sanitaire jusqu'au 10 juillet 2020 inclus. Au vu de l'évolution de la situation sanitaire, de nouvelles mesures générales ont été adoptées par deux décrets du 11 mai 2020, puis par un décret du 31 mai 2020, pour assouplir progressivement les sujétions imposées afin de faire face à l'épidémie.<br/>
<br/>
              5. Le sulfate d'hydroxychloroquine est commercialisé par le laboratoire Sanofi sous le nom de marque de Plaquenil, en vertu d'une autorisation de mise sur le marché initialement délivrée le 27 mai 2004, avec pour indications thérapeutiques le traitement symptomatique d'action lente de la polyarthrite rhumatoïde, le lupus érythémateux discoïde, le lupus érythémateux subaigu, le traitement d'appoint ou prévention des rechutes des lupus systémiques et la prévention des lucites. En application de l'article L. 5121-12-1 du code de la santé publique, et en l'absence de toute recommandation temporaire d'utilisation, cette spécialité ne pouvait être prescrite pour une autre indication, en l'absence d'alternative médicamenteuse appropriée disposant d'une autorisation de mise sur le marché ou d'une autorisation temporaire d'utilisation, qu'à la condition que le prescripteur juge indispensable, au regard des données acquises de la science, le recours à cette spécialité pour améliorer ou stabiliser l'état clinique de son patient.<br/>
<br/>
              6. Suite à un avis sur les recommandations thérapeutiques dans la prise en charge du covid-19 du 25 mars 2020 du Haut Conseil de la santé publique, le Premier ministre, par un décret du 25 mars 2020 pris sur le fondement du 9° de l'article L. 3131-15 du code de la santé publique, modifié par un décret du lendemain 26 mars, a complété d'un article 12-2 le décret du 23 mars 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire, pour prévoir notamment les conditions dans lesquelles l'hydroxychloroquine peut être prescrite, dispensée et administrée aux patients atteints de covid-19, en dehors des indications de l'autorisation de mise sur le marché du Plaquenil, spécialité pharmaceutique à base d'hydroxychloroquine. A ce titre, d'une part, par dérogation aux dispositions du code de la santé publique relatives aux autorisations de mise sur le marché, il a autorisé, sous la responsabilité d'un médecin, la prescription, la dispensation et l'administration de l'hydroxychloroquine aux patients atteints de covid-19, dans les établissements de santé qui les prennent en charge, ainsi que, pour la poursuite de leur traitement si leur état le permet et sur autorisation du prescripteur initial, à domicile, en précisant que ces prescriptions interviennent, après décision collégiale, dans le respect des recommandations du Haut Conseil de la santé publique et, en particulier, de l'indication pour les patients atteints de pneumonie oxygéno-requérante ou d'une défaillance d'organe. D'autre part, il a prévu, au 5ème alinéa, que " La spécialité pharmaceutique PLAQUENIL (c), dans le respect des indications de son autorisation de mise sur le marché, et les préparations à base d'hydroxychloroquine ne peuvent être dispensées par les pharmacies d'officine que dans le cadre d'une prescription initiale émanant exclusivement de spécialistes en rhumatologie, médecine interne, dermatologie, néphrologie, neurologie ou pédiatrie ou dans le cadre d'un renouvellement de prescription émanant de tout médecin ".<br/>
<br/>
              7. Ces dispositions ont été reprises à l'identique à l'article 17 du décret n° 20208545 du 11 mai 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire, qui abroge notamment l'article 12-2 du décret du 25 mars 2020 et prévoit son application les 11 et 12 mai 2020, puis à l'article 19 du décret n° 2020-548 du même jour ayant le même objet, qui abroge le précédent et est entré en vigueur dès sa publication au Journal officiel de la République française le 12 mai 2020. <br/>
<br/>
              8. A la suite d'un nouvel avis du Haut Conseil de la santé publique relatif à l'utilisation de l'hydroxychloroquine dans le covid-19 du 24 mai 2020, le Premier ministre a abrogé, par décret du 26 mai 2020, l'article 19 du décret précité et le ministre des solidarités et de la santé, par un arrêté du même jour pris sur le fondement de l'article L. 3131-16 du code de la santé publique, a repris les dispositions du 5ème alinéa cité au point 6, à l'article 6-2 de l'arrêté du 23 mars 2020 prescrivant les mesures d'organisation et de fonctionnement du système de santé nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire. <br/>
<br/>
              Sur la demande en référé : <br/>
<br/>
              9. Les requérants demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, de suspendre l'exécution de l'article 6-2 de l'arrêté du 23 mars 2020 mentionné au point précédent. Ils soutiennent que ses dispositions portent une atteinte grave et manifestement illégale à la liberté de prescription et, par suite, au droit à la vie et au droit de recevoir les traitements et les soins appropriés à son état de santé dès lors que le ministre des solidarités et de la santé n'était ni compétent, ni habilité sur le fondement de l'article L. 3131-16 du code de la santé publique, à prendre les dispositions contestées.<br/>
<br/>
              10. Aux termes de l'article L. 3131-15 du code de la santé publique, applicable du 24 mars au 10 juillet 2020 par l'effet des lois des 23 mars et 11 mai 2020 : " Dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le Premier ministre peut, par décret réglementaire pris sur le rapport du ministre chargé de la santé, aux seules fins de garantir la santé publique : / (...) / 9° En tant que de besoin, prendre toute mesure permettant la mise à la disposition des patients de médicaments appropriés pour l'éradication de la catastrophe sanitaire (...). / Les mesures prescrites en application des 1° à 10° du présent article sont strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu. Il y est mis fin sans délai lorsqu'elles ne sont plus nécessaires. (...) ". Une mesure visant à permettre la prescription, la dispensation et l'administration d'une spécialité pharmaceutique, en dehors des indications de son autorisation de mise sur le marché, aux patients atteints de covid-19, alors même qu'elle ne s'applique que dans les établissements de santé qui les prennent en charge, sous certaines conditions, ainsi qu'à domicile, pour la poursuite de leur traitement si leur état le permet et sur autorisation du prescripteur initial, entre dans le champ de ces dispositions. <br/>
<br/>
              11. Aux termes de l'article L. 3131-16 du même code, applicable dans les mêmes conditions : " Dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le ministre chargé de la santé peut prescrire, par arrêté motivé, toute mesure réglementaire relative à l'organisation et au fonctionnement du dispositif de santé, à l'exception des mesures prévues à l'article L. 3131-15, visant à mettre fin à la catastrophe sanitaire mentionnée à l'article L. 3131-12. / (...) / Les mesures prescrites en application du présent article sont strictement nécessaires et proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu. Il y est mis fin sans délai lorsqu'elles ne sont plus nécessaires. ". Sur le fondement de ces dispositions, le ministre chargé de la santé est habilité, durant l'état d'urgence sanitaire et dans les circonscriptions territoriales où il est déclaré, à prescrire toute mesure réglementaire nécessaire pour adapter, de façon temporaire, l'organisation et le fonctionnement du dispositif de santé pour répondre à la situation sanitaire causée par la catastrophe mentionnée à l'article L. 3131-12, y compris pour modifier des dispositions législatives et réglementaires du code de la santé publique s'y rapportant. <br/>
<br/>
              12. Aux termes du second alinéa du I de l'article L. 5121-12-1 du même code: " En l'absence de recommandation temporaire d'utilisation dans l'indication ou les conditions d'utilisation considérées, une spécialité pharmaceutique ne peut faire l'objet d'une prescription non conforme à son autorisation de mise sur le marché qu'en l'absence d'alternative médicamenteuse appropriée disposant d'une autorisation de mise sur le marché ou d'une autorisation temporaire d'utilisation et sous réserve que le prescripteur juge indispensable, au regard des données acquises de la science, le recours à cette spécialité pour améliorer ou stabiliser l'état clinique de son patient ".<br/>
<br/>
              13. En premier lieu, il résulte de ce qui a été dit aux points 10 et 11 qu'alors que le Premier ministre abrogeait les dispositions par lesquelles il avait autorisé par décret, sur le fondement de l'article L. 3131-15 du code de la santé publique, jusqu'au 26 mai 2020, la prescription, la dispensation et l'administration, sous certaines conditions dans les établissements de santé, de la spécialité pharmaceutique Plaquenil, en dehors des indications de son autorisation de mise sur le marché, le ministre de la santé était compétent, sur le fondement de l'article L. 3131-16 du même code, pour reprendre, par arrêté, les dispositions prévoyant que sa délivrance par les pharmacies d'officine est strictement réservée à ces indications.<br/>
<br/>
              14. En second lieu, il ressort des motifs de l'arrêté du 26 mai 2020 qui a créé les dispositions contestées, publiés au Journal officiel de la République française que ces dernières ont été prises " eu égard aux dernières données scientifiques concernant les risques qui s'attachent à l'utilisation de l'hydroxychloroquine dans la prise en charge des patients atteints du Covid 19 ". Les requérants ne soutiennent pas que les données acquises de la science conduisent à regarder le recours à la spécialité pharmaceutique Plaquenil comme étant indispensable pour améliorer ou stabiliser l'état clinique des patients atteints par la maladie précité, au sens et pour l'application des dispositions précitées de l'article L. 5121-12-1 du code de la santé publique qui encadrent la liberté de prescription des médecins. Et ils n'apportent, en tout état de cause, aucun élément à l'appui de leur allégation selon laquelle les dispositions contestées n'auraient été motivées que par le risque de tension sur l'approvisionnement de cette spécialité pour les patients qui y ont recours dans le cadre des indications de son autorisation de mise sur le marché. Ainsi, il ne résulte pas de l'instruction que les dispositions contestées auraient excédé le champ de l'habilitation prévue par l'article L. 3131-16 du code de la santé publique.<br/>
<br/>
              15. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur la condition d'urgence, qu'il est manifeste que la présente demande en référé n'est pas fondée. Il y a lieu, dès lors, de rejeter les conclusions de la requête par application de l'article L. 522-3 du code de justice administrative, y compris celles qui sont présentées au titre de l'article L. 761-1 du même code.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. I... et autres est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. E... I..., premier requérant dénommé.<br/>
Copie sera adressée pour information au ministre de la solidarité et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
