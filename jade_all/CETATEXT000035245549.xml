<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035245549</ID>
<ANCIEN_ID>JG_L_2017_07_000000402172</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/24/55/CETATEXT000035245549.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 19/07/2017, 402172</TITRE>
<DATE_DEC>2017-07-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402172</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELVOLVE ET TRICHET</AVOCATS>
<RAPPORTEUR>M. Marc Lambron</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:402172.20170719</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...C...et son fils M. A...C...ont demandé au tribunal administratif de Paris de condamner l'Etat à leur verser une indemnité de 3 900 euros en réparation des préjudices résultant de leur absence de relogement. Par un jugement n° 1513120/3-2 du 31 mars 2016, le tribunal administratif a rejeté leur demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 5 août et 2 novembre 2016 au secrétariat du contentieux du Conseil d'Etat, les consorts C...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Delvolvé-Trichet, leur avocat, au titre des articles L. 761-1 du code de justice administrative  et 37 de la loi du 10 juillet 1991;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la construction et de l'habitation ;<br/>
<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              - le code justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Lambron, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delvolvé et Trichet, avocat des consortsC....<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme C...a été reconnue comme prioritaire et devant être relogée en urgence sur le fondement de l'article L. 441-2-3 du code de la construction et de l'habitation par une décision du 3 janvier 2014 de la commission de médiation de Paris, au motif qu'un arrêt du 9 décembre 2014 de la cour d'appel de Paris lui ordonnait de libérer le logement qu'elle occupait ; que Mme C... et son fils majeur, qui n'ont pas demandé au tribunal administratif d'enjoindre au préfet de les reloger sur le fondement de l'article L. 441-2-3-1 du code de la construction et de l'habitation, ont demandé au tribunal administratif de Paris de condamner l'Etat à leur verser 3 900 euros en réparation du préjudice subi du fait de leur absence de relogement ; qu'ils se pourvoient en cassation contre le jugement du 31 mars 2016 par lequel le tribunal administratif a rejeté cette demande au motif que les intéressés ne justifiaient pas d'un préjudice réel, direct et certain résultant de la carence fautive de l'Etat ;<br/>
<br/>
              2. Considérant que, lorsqu'une personne a été reconnue comme prioritaire et comme devant être logée ou relogée d'urgence par une commission de médiation, en application des dispositions de l'article L. 441-2-3 du code de la construction et de l'habitation, la carence fautive de l'Etat à exécuter cette décision dans le délai imparti engage sa responsabilité à l'égard du seul demandeur, au titre des troubles dans les conditions d'existence résultant du maintien de la situation qui a motivé la décision de la commission, alors même que l'intéressé n'a pas fait usage du recours en injonction contre l'Etat prévu par l'article L. 441-2-3-1 du code de la construction et de l'habitation ; que ces troubles doivent être appréciés en fonction des conditions de logement qui ont perduré du fait de la carence de l'Etat, de la durée de cette carence et du nombre de personnes composant le foyer du demandeur pendant la période de responsabilité de l'Etat, qui court à compter de l'expiration du délai de trois ou six mois à compter de la décision de la commission de médiation que les dispositions de l'article R. 441-16-1 du code de la construction et de l'habitation impartissent au préfet pour provoquer une offre de logement ; <br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède qu'ayant constaté que le préfet n'avait pas proposé un relogement à Mme C...dans le délai prévu par le code de la construction et de l'habitation à compter de la décision de la commission de médiation, le tribunal administratif de Paris ne pouvait, sans commettre une erreur de droit, juger que cette carence, constitutive d'une faute de nature à engager la responsabilité de l'Etat, ne causait à l'intéressée aucun préjudice, au motif que l'arrêt ordonnant son expulsion n'avait pas été exécuté, alors qu'il était constant que la situation qui avait motivé la décision de la commission perdurait et que l'intéressée justifiait de ce fait de troubles dans ses conditions d'existence lui ouvrant droit à réparation dans les conditions indiquées au point 2 ; que, par suite, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, Mme C...est fondée à demander l'annulation du jugement attaqué ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Delvolvé-Trichet, avocat des requérants, renonce à percevoir, en cas d'admission définitive à l'aide juridictionnelle, la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat le versement à la SCP Delvolvé-Trichet de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er  : Le jugement du 31 mars 2016 du tribunal administratif de Paris est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Paris.<br/>
Article 3 : L'Etat versera à la SCP Delvolvé-Trichet, avocat des requérants, une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 et de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à Mme B...C..., à M. A...C...et au ministre de la cohésion des territoires.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">38-07-01 LOGEMENT. - RESPONSABILITÉ DE L'ETAT À RAISON DE LA CARENCE FAUTIVE À ASSURER LE LOGEMENT D'UN DEMANDEUR RECONNU PRIORITAIRE ET URGENT [RJ1] - 1) ENGAGEMENT SUBORDONNÉ À L'USAGE PAR L'INTÉRESSÉ DU RECOURS EN INJONCTION (ART. L. 441-2-3-1 DU CCH) - ABSENCE - 2) CALCUL DU PRÉJUDICE - DÉBUT DE LA PÉRIODE DE RESPONSABILITÉ DE L'ETAT - EXPIRATION DU DÉLAI IMPARTI AU PRÉFET POUR PROVOQUER UNE OFFRE DE LOGEMENT APRÈS LA DÉCISION DE LA COMMISSION DE MÉDIATION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-02-012 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICES SOCIAUX. - DALO - RESPONSABILITÉ DE L'ETAT À RAISON DE LA CARENCE FAUTIVE DE L'ETAT À ASSURER LE LOGEMENT D'UN DEMANDEUR RECONNU PRIORITAIRE ET URGENT [RJ1] - 1) ENGAGEMENT SUBORDONNÉ À L'USAGE PAR L'INTÉRESSÉ DU RECOURS EN INJONCTION (ART. L. 441-2-3-1 DU CCH) - ABSENCE - 2) CALCUL DU PRÉJUDICE - DÉBUT DE LA PÉRIODE DE RESPONSABILITÉ DE L'ETAT - EXPIRATION DU DÉLAI IMPARTI AU PRÉFET POUR PROVOQUER UNE OFFRE DE LOGEMENT APRÈS LA DÉCISION DE LA COMMISSION DE MÉDIATION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-04-03-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. ÉVALUATION DU PRÉJUDICE. TROUBLES DANS LES CONDITIONS D'EXISTENCE. - DALO - TROUBLES DANS LES CONDITIONS D'EXISTENCE RÉSULTANT DE L'ABSENCE DE RELOGEMENT D'UN DEMANDEUR RECONNU PRIORITAIRE ET URGENT - APPRÉCIATION [RJ1] - DÉBUT DE LA PÉRIODE DE RESPONSABILITÉ DE L'ETAT - EXPIRATION DU DÉLAI IMPARTI AU PRÉFET POUR PROVOQUER UNE OFFRE DE LOGEMENT APRÈS LA DÉCISION DE LA COMMISSION DE MÉDIATION.
</SCT>
<ANA ID="9A"> 38-07-01 1) Lorsqu'une personne a été reconnue comme prioritaire et comme devant être logée ou relogée d'urgence par une commission de médiation, en application des dispositions de l'article L. 441-2-3 du code de la construction et de l'habitation (CCH), la carence fautive de l'Etat à exécuter cette décision dans le délai imparti engage sa responsabilité à l'égard du seul demandeur, au titre des troubles dans les conditions d'existence résultant du maintien de la situation qui a motivé la décision de la commission, alors même que l'intéressé n'a pas fait usage du recours en injonction contre l'Etat prévu par l'article L. 441-2-3-1 du code de la construction et de l'habitation.,,,2) Ces troubles doivent être appréciés en fonction des conditions de logement qui ont perduré du fait de la carence de l'Etat, de la durée de cette carence et du nombre de personnes composant le foyer du demandeur pendant la période de responsabilité de l'Etat, qui court à compter de l'expiration du délai de trois ou six mois à compter de la décision de la commission de médiation que l'article R. 441-16-1 du CCH impartit au préfet pour provoquer une offre de logement.</ANA>
<ANA ID="9B"> 60-02-012 1) Lorsqu'une personne a été reconnue comme prioritaire et comme devant être logée ou relogée d'urgence par une commission de médiation, en application des dispositions de l'article L. 441-2-3 du code de la construction et de l'habitation (CCH), la carence fautive de l'Etat à exécuter cette décision dans le délai imparti engage sa responsabilité à l'égard du seul demandeur, au titre des troubles dans les conditions d'existence résultant du maintien de la situation qui a motivé la décision de la commission, alors même que l'intéressé n'a pas fait usage du recours en injonction contre l'Etat prévu par l'article L. 441-2-3-1 du code de la construction et de l'habitation.,,,2) Ces troubles doivent être appréciés en fonction des conditions de logement qui ont perduré du fait de la carence de l'Etat, de la durée de cette carence et du nombre de personnes composant le foyer du demandeur pendant la période de responsabilité de l'Etat, qui court à compter de l'expiration du délai de trois ou six mois à compter de la décision de la commission de médiation que l'article R. 441-16-1 du CCH impartit au préfet pour provoquer une offre de logement.</ANA>
<ANA ID="9C"> 60-04-03-03 Les troubles dans les conditions d'existence résultant du maintien de la situation qui a motivé la décision de la commission de médiation doivent être appréciés en fonction des conditions de logement qui ont perduré du fait de la carence de l'Etat, de la durée de cette carence et du nombre de personnes composant le foyer du demandeur pendant la période de responsabilité de l'Etat, qui court à compter de l'expiration du délai de trois ou six mois à compter de la décision de la commission de médiation que l'article R. 441-16-1 du code de la construction et de l'habitation impartit au préfet pour provoquer une offre de logement.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, 13 juillet 2016, Mme,, n° 382872, T. p. 945 ; CE, 16 décembre 2016, M.,, n° 383111, p. 563.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
