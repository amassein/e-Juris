<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027061612</ID>
<ANCIEN_ID>JG_L_2013_02_000000365256</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/06/16/CETATEXT000027061612.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 06/02/2013, 365256, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-02-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365256</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE ; BROUCHOT</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2013:365256.20130206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée le 16 janvier 2013 au secrétariat du contentieux du Conseil d'Etat, présentée pour l'association Sortir du nucléaire Cornouaille, dont le siège est 53 impasse de l'Odet à Quimper (29000), l'association Agir pour un environnement et un développement durables, dont le siège est 10 rue Hegel à Brest (29200), l'association Bretagne vivante, dont le siège est 186 rue Anatole France BP 63121 à Brest (29231) cedex 3, l'association Consommation logement et cadre de vie, dont le siège est 8 B rue des Douves à Quimper (29000), l'association Eau et rivières de Bretagne, dont le siège est 7 place du Champ au Roy à Guingamp (22200), l'association Groupe mammalogique breton, dont le siège est Maison de la Rivière à Sizun (29450), l'association Vivre dans les monts d'Arrée, dont le siège est CAL route de Berrien à Huelgoat (29690), représentées par leurs représentants légaux ; les associations requérantes demandent au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner, sur le fondement de l'article L. 123-16 du code de l'environnement, la suspension de l'exécution du décret n° 2011-886 du 27 juillet 2011 autorisant Electricité de France à procéder aux opérations de démantèlement partiel de l'installation nucléaire de base n° 162 dénommée EL4-D, installation d'entreposage de matériels de la centrale nucléaire des monts d'Arrée, située sur le territoire de la commune de Loqueffret (département du Finistère) ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              elles soutiennent que :<br/>
              - la commission d'enquête a émis un avis défavorable ;<br/>
              - il existe un doute sérieux quant à la légalité du décret contesté ;<br/>
              - le projet litigieux n'a pas fait l'objet d'un débat public, en méconnaissance des dispositions de la directive du Conseil du 27 juin 1985 concernant l'évaluation des incidences de certains projets publics et privés sur l'environnement et de la convention d'Aarhus du 25 juin 1998 ;<br/>
              - le décret contesté autorise des opérations de démantèlement de niveau 3 sans que les travaux de niveau 2 prévus par les décrets précédents soient achevés ;<br/>
              - ces opérations sont en contradiction avec les préconisations d'Electricité de France ;<br/>
              - au surplus, la condition d'urgence est remplie dès lors que les opérations de démantèlement ont commencé ;<br/>
<br/>
<br/>
              Vu le décret dont la suspension de l'exécution est demandée ;<br/>
<br/>
              Vu la copie de la requête à fin d'annulation de ce décret ; <br/>
<br/>
              Vu le mémoire en défense, enregistré le 31 janvier 2013, présenté pour la société Electricité de France, qui conclut au rejet de la requête et à ce que la somme de 1 500 euros soit mise à la charge de l'association Sortir du nucléaire Cornouaille et autres au titre de l'article L. 761-1 du code de justice administrative ; elle soutient que : <br/>
              - certaines associations requérantes n'ont pas d'intérêt pour agir ;<br/>
              - certaines des associations requérantes se trouvent représentées par des personnes n'ayant pas qualité à le faire ;<br/>
              - l'intérêt général justifie la poursuite de l'exécution du décret contesté ;<br/>
              - aucun des moyens soulevés n'est de nature à faire naître un doute sérieux quant à la légalité de la décision contestée ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 1er février 2013 présenté par la ministre de l'écologie, du développement durable et de l'énergie, qui conclut au rejet de la requête ; elle soutient que ;<br/>
              - la requête au principal est irrecevable ;<br/>
              - les dispositions de l'article L. 123-16 ne sont pas applicables en l'espèce ;<br/>
              - aucun des moyens soulevés n'est de nature à faire naître un doute sérieux quant à la légalité de la décision contestée ;<br/>
<br/>
              Vu le mémoire en réplique, enregistré le 4 février 2013, présenté pour l'association Sortir du nucléaire Cornouaille et autres qui reprennent les conclusions de leur requête et les mêmes moyens ; elles soutiennent en outre qu'elles ont qualité et intérêt pour agir ;<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu la convention d'Aarhus du 25 juin 1998 sur l'accès à l'information, la participation du public aux processus décisionnels et l'accès à la justice en matière d'environnement ;<br/>
<br/>
              Vu la directive 85/337/CEE du Conseil du 27 juin 1985 modifiée par la directive 97/11/CE du Conseil du 3 mars 1997 et la directive 2003/35/CE du Parlement européen et du Conseil du 26 mai 2003 ;<br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, l'association Sortir du nucléaire Cornouaille et autres, d'autre part, le Premier ministre, la ministre de l'écologie, du développement durable et de l'énergie, le ministre de l'économie et des finances et la société Electricité de France ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 4 février 2013 à 10 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Brouchot, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'association Sortir du nucléaire Cornouaille et autres ; <br/>
<br/>
              - la représentante de l'association Sortir du nucléaire Cornouailles et autres ;<br/>
<br/>
              - les représentants de la ministre de l'écologie, du développement durable et de l'énergie ; <br/>
<br/>
              - Me Coutard, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la société Electricité de France ;<br/>
<br/>
              - les représentants de la société Electricité de France ;<br/>
<br/>
              et à l'issue de laquelle l'instruction a été close ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l 'article L. 123-16 du code de l'environnement, reproduit à l'article L. 554-12 du code de justice administrative, dans sa rédaction applicable en l'espèce : " Le juge administratif des référés, saisi d'une demande de suspension d'une décision prise après des conclusions défavorables du commissaire-enquêteur ou de la commission d'enquête, fait droit à cette demande si elle comporte un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de celle-ci " ; <br/>
<br/>
              2. Considérant que si la ministre de l'écologie, du développement durable et de l'énergie soutient que les dispositions du décret contesté n'auraient pas fait l'objet de conclusions défavorables de la commission d'enquête, aucun des moyens soulevés n'est, en tout état de cause, en l'état de l'instruction, propre à créer un doute sérieux quant à leur légalité; <br/>
<br/>
               3. Considérant qu'il résulte de ce qui précède que la requête de l'association Sortir du nucléaire Cornouailles et autres ne peut qu'être rejetée, sans qu'il soit besoin de statuer sur sa recevabilité ;<br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la société Electricité de France sur le fondement des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de l'association Sortir du nucléaire Cornouaille et autres est rejetée.<br/>
Article 2 : Les conclusions présentées par la société Electricité de France au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente ordonnance sera notifiée à l'association Sortir du nucléaire Cornouaille, premier requérant dénommé, au Premier ministre, à la ministre de l'écologie, du développement durable et de l'énergie, au ministre de l'économie et des finances et à la société Electricité de France.<br/>
Les autres requérants seront informés de la présente décision par Maître Brouchot, avocat au Conseil d 'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
