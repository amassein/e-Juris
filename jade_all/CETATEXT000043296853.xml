<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043296853</ID>
<ANCIEN_ID>JG_L_2021_03_000000438146</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/29/68/CETATEXT000043296853.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 26/03/2021, 438146, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438146</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>OCCHIPINTI</AVOCATS>
<RAPPORTEUR>Mme Fanélie Ducloz</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:438146.20210326</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés les 31 janvier et 18 août 2020 au secrétariat du contentieux du Conseil d'Etat, la Confédération nationale des avocats (CNA) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet du Premier ministre née du silence gardé sur sa demande tendant à l'abrogation des décrets n° 2009-1524 du 9 décembre 2009 relatif à la procédure d'appel avec représentation obligatoire en matière civile, n° 2010-1647 du 28 décembre 2010 modifiant la procédure d'appel avec représentation obligatoire en matière civile et n° 2017-891 du 6 mai 2017 relatif aux exceptions d'incompétence et à l'appel en matière civile ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de procédure civile ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Fanélie Ducloz, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à Me Occhipinti, avocat de la Confédération nationale des avocats ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Par un courrier du 20 novembre 2019, dont il a été accusé réception le 22 novembre 2019, la Confédération nationale des avocats (CNA) a saisi le Premier ministre d'une demande tendant à l'abrogation des décrets du 9 décembre 2009 relatif à la procédure d'appel avec représentation obligatoire en matière civile, du 28 décembre 2010 modifiant la procédure d'appel avec représentation obligatoire en matière civile et du 6 mai 2017 relatif aux exceptions d'incompétence et à l'appel en matière civile. Elle demande l'annulation pour excès de pouvoir de la décision implicite de rejet résultant du silence gardé par le Premier ministre sur cette demande.<br/>
<br/>
              2. En premier lieu, aux termes du premier paragraphe de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " Toute personne a droit à ce que sa cause soit entendue équitablement, publiquement et dans un délai raisonnable, par un tribunal indépendant et impartial, établi par la loi, qui décidera, soit des contestations sur ses droits et obligations de caractère civil, soit du bien-fondé de toute accusation en matière pénale dirigée contre elle. (...) ".<br/>
<br/>
              3. L'obligation pour l'appelant de mentionner expressément dans la déclaration d'appel les chefs du jugement de première instance qu'il entend critiquer, de signifier sa déclaration d'appel à l'intimé lorsque ce dernier n'a pas constitué avocat dans le délai d'un mois suivant l'envoi par le greffe de la lettre de notification de la déclaration ou en cas de retour au greffe de la lettre de notification, ou de déposer ses conclusions dans un délai de trois mois à compter de sa déclaration à peine de caducité de celle-ci, et plus largement les différentes dispositions procédurales contraignantes imposées aux parties à une procédure d'appel en matière civile dans un objectif de bonne administration de la justice et afin d'améliorer la célérité et l'efficacité de la procédure d'appel avec représentation obligatoire, ne portent pas une atteinte excessive au droit d'accès au juge et ne méconnaissent aucune exigence découlant de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. <br/>
<br/>
              4. En deuxième lieu, la circonstance que le délai moyen de jugement devant les cours d'appel est passé de 11,5 mois en 2009 à 13,5 mois en 2018, alors que l'un des objectifs assignés à la réforme mise en place en 2009 était d'améliorer l'efficacité et la célérité des procédures d'appel civiles, n'est pas de nature à établir, à elle seule, que les décisions attaquées seraient entachées d'une erreur manifeste d'appréciation.<br/>
<br/>
              5. En troisième lieu, le détournement de pouvoir allégué n'est pas établi.<br/>
<br/>
              6. Il résulte de ce qui précède que la Confédération nationale des avocats n'est pas fondée à demander l'annulation de la décision qu'elle attaque. Par suite, sa requête, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative, ne peut qu'être rejetée.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la Confédération nationale des avocats est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la Confédération nationale des avocats, au Premier ministre et au garde des sceaux, ministre de la justice.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
