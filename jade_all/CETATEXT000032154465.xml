<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032154465</ID>
<ANCIEN_ID>JG_L_2016_03_000000386354</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/15/44/CETATEXT000032154465.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère SSJS, 04/03/2016, 386354, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-03-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386354</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS</AVOCATS>
<RAPPORTEUR>M. Marc Thoumelou</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:386354.20160304</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 10 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, la Confédération générale du travail - Force ouvrière demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2014-1157 du 9 octobre 2014 relatif au fonds de financement des droits liés au compte personnel de prévention de la pénibilité ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - la loi n° 2015-994 du 17 août 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Thoumelou, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de la Confédération générale du travail - Force ouvrière ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du I de l'article L. 4162-17 du code du travail, issu de la loi du 20 janvier 2014 garantissant l'avenir et la justice du système de retraites : " Il est institué un fonds chargé du financement des droits liés au compte personnel de prévention de la pénibilité. / Ce fonds est un établissement public de l'Etat ". En vertu de l'article L. 4162-19 du même code, les recettes de ce fonds sont constituées par : " 1° Une cotisation due par les employeurs au titre des salariés qu'ils emploient et qui entrent dans le champ d'application du compte personnel de prévention de la pénibilité défini à l'article L. 4162-1, dans les conditions définies au I de l'article L. 4162-20 ; / 2° Une cotisation additionnelle due par les employeurs ayant exposé au moins un de leurs salariés à la pénibilité, au sens du deuxième alinéa de l'article L. 4162-2, dans les conditions définies au II de l'article L. 4162-20 ; / 3° Toute autre recette autorisée par les lois et règlements ". Aux termes de l'article L. 4162-20 de ce code, dans sa rédaction issue de la loi du 20 janvier 2014, en vigueur à la date d'édiction du décret attaqué : " I. - La cotisation mentionnée au 1° de l'article L. 4162-19 est égale à un pourcentage, fixé par décret, dans la limite de 0,2 % des rémunérations ou gains, au sens du premier alinéa de l'article L. 242-1 du code de la sécurité sociale, perçus par les salariés entrant dans le champ d'application du compte personnel de prévention de la pénibilité défini à l'article L. 4162-1 du présent code. / II. - La cotisation additionnelle mentionnée au 2° de l'article L. 4162-19 est égale à un pourcentage fixé par décret et compris entre 0,3 % et 0,8 % des rémunérations ou gains mentionnés au I du présent article perçus par les salariés exposés à la pénibilité, au sens du deuxième alinéa de l'article L. 4162-2, au cours de chaque période. Un taux spécifique, compris entre 0,6 % et 1,6 %, est appliqué au titre des salariés ayant été exposés simultanément à plusieurs facteurs de pénibilité (...) ".<br/>
<br/>
              2. Sur le fondement des dispositions de l'article L. 4162-20 du code du travail citées ci-dessus, le Premier ministre a, par le décret du 9 octobre 2014 relatif au fonds de financement des droits liés au compte personnel de prévention de la pénibilité, fixé le taux de la cotisation mentionnée au 1° de l'article L. 4162-19 à zéro pour les années 2015 et 2016, et à 0,01 % à compter de 2017, et celui de la cotisation mentionnée au 2° du même article pour les salariés exposés à un seul facteur de pénibilité à 0,1 % en 2015 et 2016 et 0,2 % à compter de 2017, ainsi que, pour les salariés exposés à plusieurs facteurs de pénibilité, à 0,2 % en 2015 et 2016 et 0,4 % à compter de 2017. La Confédération générale du travail - Force ouvrière doit être regardée comme demandant l'annulation de ce décret en tant seulement qu'il n'a pas fixé à un niveau plus élevé les taux de ces cotisations.<br/>
<br/>
              Sur le taux de la cotisation mentionnée au 1° de l'article L. 4162-19 du code du travail pour les années 2015 et 2016 :<br/>
<br/>
              3. Aux termes du II de l'article 31 de la loi du 17 août 2015 relative au dialogue social et à l'emploi : " Aucune cotisation mentionnée au I de l'article L. 4162-20 du code du travail n'est due en 2015 et 2016 ". Par cette disposition, le législateur a entièrement réglé, pour les années 2015 et 2016, la question du taux de la cotisation mentionnée au 1° de l'article L. 4162-20 du code du travail. Intervenue postérieurement à l'enregistrement de la requête de la Confédération générale du travail - Force ouvrière, elle a pour effet de rendre sans objet les conclusions de la confédération requérante tendant à l'annulation du décret du 9 octobre 2014 en tant qu'il n'a pas fixé à un niveau plus élevé le taux de cette cotisation pour les deux années 2015 et 2016. Par suite, il n'y a plus lieu de statuer sur ces conclusions.<br/>
<br/>
              Sur le taux de la cotisation mentionnée au 1° de l'article L. 4162-19 du code du travail à compter de 2017 et sur ceux de la cotisation mentionnée au 2° du même article :<br/>
<br/>
              En ce qui concerne la fin de non-recevoir opposée par le ministre des affaires sociales, de la santé et des droits des femmes :<br/>
<br/>
              4. En vertu de l'article 1er de ses statuts, la Confédération générale du travail - Force ouvrière a pour objet de défendre les intérêts moraux et matériels, économiques et professionnels des salariés. Elle justifie ainsi d'un intérêt lui donnant qualité pour demander l'annulation des dispositions du décret du 9 octobre 2014 par lesquelles le Premier ministre a fixé le taux des cotisations destinées à abonder le fonds chargé du financement des droits liés au compte personnel de prévention de la pénibilité, dont les recettes permettront la mise en oeuvre effective des droits reconnus en ce domaine aux salariés. Par suite, le ministre des affaires sociales, de la santé et des droits des femmes n'est pas fondé à soutenir que les conclusions de la confédération requérante seraient irrecevables. <br/>
<br/>
              En ce qui concerne le taux de la cotisation mentionnée au 1° de l'article L. 4162-19 du code du travail à compter de 2017 :<br/>
<br/>
              5. Aux termes de l'article L. 4162-21 du code du travail : " Pour la fixation du taux des cotisations définies aux 1° et 2° de l'article L. 4162-19 et du barème de points spécifique à chaque utilisation du compte défini à l'article L. 4162-4, il est tenu compte des prévisions financières du fonds pour les cinq prochaines années et, le cas échéant, des recommandations du comité de suivi mentionné à l'article L. 114-4 du code de la sécurité sociale ". Il ne ressort pas des pièces du dossier qu'en fixant à 0,01 %, à compter de l'année 2017, le taux de la cotisation définie au 1° de l'article L. 4162-19, le décret attaqué serait, compte tenu, d'une part, de l'importance de l'assiette à laquelle ce taux est appliqué et, d'autre part, des besoins financiers du fonds prévus au cours des années suivant sa mise en place, entaché d'une erreur manifeste d'appréciation.<br/>
<br/>
              En ce qui concerne les taux de la cotisation mentionnée au 2° de l'article L. 4162-19 du code du travail :<br/>
<br/>
              6. Les dispositions contestées du décret attaqué fixent, pour la cotisation prévue au 2° de l'article L. 4162-19 du code du travail, due par les employeurs au titre des salariés ayant été exposés à un seul facteur de pénibilité au-delà des seuils d'exposition mentionnés à l'article L. 4162-2, un taux de 0,1 % pour les années 2015 et 2016, et de 0,2 % à compter de l'année 2017. Au titre des salariés ayant été exposés à plusieurs facteurs de pénibilité au-delà des mêmes seuils, elles fixent un taux de 0,2 % pour les années 2015 et 2016, et de 0,4 % à compter de l'année 2017. Ces dispositions méconnaissent ainsi les taux minimaux de 0,3 et 0,6 % fixés par les dispositions, citées au point 1, du II de l'article L. 4162-20, dans leur rédaction en vigueur à la date de l'édiction du décret attaqué. Par suite, et alors même que les dispositions de l'article L. 4162-20 ont été ultérieurement modifiées par la loi du 17 août 2015, la confédération requérante est fondée à soutenir que les dispositions du décret du 9 octobre 2014 fixant les taux de la cotisation additionnelle due par les employeurs ayant exposé au moins un de leurs salariés à la pénibilité sont illégales. Ces dispositions, qui sont divisibles des autres dispositions du décret, doivent donc être annulées en tant qu'elles n'ont pas fixé des taux de cotisation plus élevés.<br/>
<br/>
              7. Il résulte de tout ce qui précède que la Confédération générale du travail - Force ouvrière est fondée à demander l'annulation du décret qu'elle attaque en tant seulement qu'il n'a pas fixé à un niveau plus élevé les taux de la cotisation additionnelle due par les employeurs ayant exposé au moins un de leurs salariés à la pénibilité au sens du deuxième alinéa de  l'article L. 4162-2 du code du travail.<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              8. Il y a lieu de mettre à la charge de l'Etat le versement à la Confédération générale du travail - Force ouvrière d'une somme de 2 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions de la requête de la Confédération générale du travail - Force ouvrière tendant à l'annulation du décret du 9 octobre 2014 en tant qu'il a fixé à zéro le taux de la cotisation mentionnée au 1° de l'article L. 4162-19 du code du travail pour les années 2015 et 2016. <br/>
Article 2 : Le décret du 9 octobre 2014 relatif au fonds de financement des droits liés au compte personnel de prévention de la pénibilité est annulé en tant qu'il n'a pas fixé à un niveau plus élevé les taux de la cotisation mentionnée au 2° de l'article L. 4162-19 du code du travail.<br/>
Article 3 : L'Etat versera à la Confédération générale du travail - Force ouvrière une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions de la requête de la Confédération générale du travail - Force ouvrière est rejeté.<br/>
Article 5 : La présente décision sera notifiée à la Confédération générale du travail - Force ouvrière, au Premier ministre et à la ministre des affaires sociales et de la santé.<br/>
Copie en sera adressée à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
