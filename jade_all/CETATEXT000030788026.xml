<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030788026</ID>
<ANCIEN_ID>JG_L_2015_06_000000388409</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/78/80/CETATEXT000030788026.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 26/06/2015, 388409</TITRE>
<DATE_DEC>2015-06-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388409</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>Mme Charline Nicolas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:388409.20150626</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...D...et ses colistiers ont demandé au tribunal administratif de Cayenne d'annuler les opérations électorales qui se sont déroulées le 16 novembre 2014 en vue de la désignation des conseillers municipaux de la commune de Camopi (Guyane). <br/>
<br/>
              Par un jugement n° 1401254 du 29 janvier 2015, le tribunal administratif de Cayenne a rejeté cette protestation.<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 2 mars et le 4 juin 2015 au secrétariat du contentieux du Conseil d'Etat, M. D...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) d'annuler les opérations électorales qui se sont déroulées le 16 novembre 2014 dans la commune de Camopi ; <br/>
<br/>
              3°) de mettre à la charge de M. C...B...le versement de la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Charline Nicolas, auditeur,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de M. B...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'à l'issue des élections municipales partielles qui se sont déroulées dans la commune de Camopi, en Guyane, le 16 novembre 2014, la liste conduite par M. C...B..., dénommée " Ensemble pour bâtir Camopi ", a obtenu un total de 297 voix, soit 52,01 % des suffrages exprimés, contre 274 à la liste " Wayapuku Wan, fier de l'être ", conduite par M. A...D...;<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 120 du code électoral : " Le tribunal administratif prononce sa décision dans le délai de deux mois à compter de l'enregistrement de la réclamation au greffe (bureau central ou greffe annexe) et la notification en est faite dans les huit jours à partir de sa date, dans les conditions fixées à l'article R. 751-3 du code de justice administrative. (...) " ; qu'aux termes de l'article R. 121 du même code : " Faute d'avoir statué dans les délais ci-dessus fixés, le tribunal administratif est dessaisi. (...) " ; <br/>
<br/>
              3. Considérant qu'il résulte de l'instruction que la protestation de M. D... et de ses colistiers a été enregistrée au greffe du tribunal administratif de Cayenne le 21 novembre 2014 ; que, le 29 janvier 2015, date à laquelle le jugement attaqué a été rendu, le délai de deux mois fixé par les dispositions citées ci-dessus était expiré et le tribunal administratif se trouvait dessaisi de la protestation ; que son jugement est, dès lors, entaché d'irrégularité et doit, pour ce motif, être annulé ; qu'il y a lieu, par suite, pour le Conseil d'Etat, de statuer sur la protestation ; <br/>
<br/>
              4. Considérant qu'aux termes de l'article L. 247 du code électoral : " Par dérogation à l'article L. 227, les électeurs sont convoqués pour les élections partielles, par arrêté du sous préfet. L'arrêté de convocation est publié dans la commune quinze jours au moins avant l'élection " ; qu'alors même que le délai de cinq jours prévu par l'article R. 119 du code électoral est expiré, M. D...est recevable à invoquer le grief, d'ordre public, tiré de la méconnaissance de ces dispositions ; <br/>
<br/>
              5. Considérant qu'il résulte de l'instruction que l'arrêté préfectoral portant convocation des élections a été affiché le 6 novembre 2014 au bureau de vote de Trois-Sauts, soit dans un délai inférieur au délai prescrit par les dispositions citées ci-dessus du code électoral ; qu'eu égard aux difficultés de diffusion de l'information liées aux caractéristiques géographiques de cette commune ainsi qu'aux problèmes de circulation aggravés par le bas niveau du fleuve Oyapok, la méconnaissance des dispositions de l'article L. 247 du code électoral a été, dans les circonstances de l'espèce, de nature à altérer la sincérité du scrutin ; que, par suite, sans qu'il soit besoin d'examiner les autres griefs de la protestation, M. D...est fondé à soutenir que les opérations électorales du 16 novembre 2014 doivent être annulées ; <br/>
<br/>
              6. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. D...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Cayenne du 29 janvier 2015 est annulé.<br/>
Article 2 : Les opérations électorales qui se sont déroulées le 16 novembre 2014 dans la commune de Camopi sont annulées. <br/>
Article 3 : Les conclusions de M. D...présentées  au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à M. A...D..., à M. C...B...et à la ministre des outre-mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-08-05-02-01 ÉLECTIONS ET RÉFÉRENDUM. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS DU JUGE. GRIEFS. GRIEFS D'ORDRE PUBLIC. - GRIEF TIRÉ DE L'IRRÉGULARITÉ DE LA CONVOCATION DES ÉLECTEURS EN VUE D'ÉLECTIONS MUNICIPALES PARTIELLES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-04-01-02 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS D'ORDRE PUBLIC À SOULEVER D'OFFICE. EXISTENCE. - CONTENTIEUX ÉLECTORAL - GRIEF TIRÉ DE L'IRRÉGULARITÉ DE LA CONVOCATION DES ÉLECTEURS EN VUE D'ÉLECTIONS MUNICIPALES PARTIELLES.
</SCT>
<ANA ID="9A"> 28-08-05-02-01 Le grief tiré de la méconnaissance de l'article L. 247 du code électoral, en l'absence de convocation des électeurs pour des élections municipales partielles par un arrêté du sous préfet publié quinze jours au moins avant l'élection, est d'ordre public.</ANA>
<ANA ID="9B"> 54-07-01-04-01-02 Le grief tiré de la méconnaissance de L. 247 du code électoral, en l'absence de convocation des électeurs pour des élections municipales partielles par un arrêté du sous préfet publié quinze jours au moins avant l'élection, est d'ordre public.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
