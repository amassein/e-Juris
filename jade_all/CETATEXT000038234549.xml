<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038234549</ID>
<ANCIEN_ID>JG_L_2019_03_000000411790</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/23/45/CETATEXT000038234549.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 15/03/2019, 411790</TITRE>
<DATE_DEC>2019-03-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411790</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT</AVOCATS>
<RAPPORTEUR>M. Jean-Yves Ollier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:411790.20190315</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° M. C...B...a demandé au tribunal administratif de la Polynésie française, à titre principal, d'annuler les titres de recettes émis le 17 novembre 2015 par l'administrateur général des finances publiques en Polynésie française, correspondant à des trop-perçus constatés sur l'indemnité temporaire de retraite qui lui a été versée pour la période du 1er janvier 2009 au 31 décembre 2014 et, à titre subsidiaire, de condamner l'Etat à lui verser la somme de 32 565 euros en réparation du préjudice qu'il estime avoir subi du fait de la négligence fautive de l'administration. <br/>
<br/>
              Par un jugement n° 1600361 du 24 mars 2017, le tribunal administratif de la Polynésie française a rejeté sa demande.<br/>
<br/>
              Sous le n° 411790, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 22 juin et 21 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Sous le n° 411799, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 22 juin et 21 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, M. D... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - le code civil ;<br/>
              - la loi organique n° 2004-192 du 27 février 2004 ;<br/>
              - la loi n° 2004-193 du 27 février 2004 ;<br/>
              - la loi n° 2008-561 du 17 juin 2008 ;<br/>
              - la loi n° 2008-1443 du 30 décembre 2008 ;<br/>
              - le décret n° 2009-114 du 30 janvier 2009 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Yves Ollier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Balat, avocat de M. B...et de M.D....<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les pourvois visés ci-dessus présentent à juger les mêmes questions. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Il ressort des pièces des dossiers soumis au juge du fond que les 17 et 18 novembre 2015, l'administrateur général des finances publiques en Polynésie française a émis à l'encontre, respectivement, de MM. B...etD..., militaires retraités employés en qualité de pilotes de ligne par la compagnie Air Tahiti Nui, six titres de recettes correspondant à des trop-perçus constatés sur l'indemnité temporaire de retraite qui leur a été versée pour la période du 1er janvier 2009 au 31 décembre 2014. MM. B...et D...ont formé le 15 janvier 2016 des recours préalables à l'encontre de ces titres exécutoires, qui ont été implicitement rejetés. Ils ont demandé au tribunal administratif de la Polynésie française d'annuler ces titres de recettes et de condamner l'Etat à leur verser la somme respectivement de 32 565 euros et de 41 757 euros en réparation du préjudice qu'ils estiment avoir subi du fait de la négligence fautive de l'administration. Par deux jugements du 24 mars 2017, le tribunal administratif de la Polynésie française a décidé qu'il n'y avait pas lieu de transmettre au Conseil d'Etat la question prioritaire de constitutionnalité soulevée par M. D...et rejeté les requêtes. MM. B...et D...se pourvoient en cassation contre ces jugements.<br/>
<br/>
              3. Aux termes de l'article 137 de la loi du 30 décembre 2008 de finances rectificative pour 2008 : " I.   L'indemnité temporaire accordée aux fonctionnaires pensionnés relevant du code des pensions civiles et militaires de retraite majore le montant en principal de la pension d'un pourcentage fixé par décret selon la collectivité dans laquelle ils résident. / L'indemnité temporaire est accordée aux pensionnés qui justifient d'une résidence effective dans les collectivités suivantes : La Réunion, Mayotte, Saint-Pierre-et-Miquelon, la Nouvelle-Calédonie, Wallis-et-Futuna et la Polynésie française. / (...) VI.   Les services de la direction générale des finances publiques contrôlent l'attribution des indemnités temporaires. A ce titre, les demandeurs et les bénéficiaires, les administrations de l'Etat, les collectivités territoriales ainsi que les opérateurs de téléphonie fixe et de téléphonie mobile sont tenus de communiquer les renseignements, justifications ou éclaircissements nécessaires à la vérification des conditions d'octroi et de l'effectivité de la résidence. / L'indemnité temporaire cesse d'être versée dès lors que la personne attributaire cesse de remplir les conditions d'effectivité de la résidence précisées par décret. / En cas d'infraction volontaire aux règles d'attribution des indemnités temporaires, leur versement cesse et les intéressés perdent définitivement le bénéfice de l'indemnité visée ". Selon l'article 8 du décret du 30 janvier 2009 relatif à l'indemnité temporaire accordée aux personnels retraités relevant du code des pensions civiles et militaires de retraite : " Le pensionné souscrit chaque année une déclaration de résidence auprès du comptable compétent pour le versement de l'indemnité à une date fixée par ce dernier. Il déclare à cette occasion ses absences sur la période écoulée. / Le comptable peut exiger toute pièce lui permettant de vérifier les conditions de résidence, notamment les documents de voyage du pensionné ". Aux termes de l'article 9 de ce décret : " L'indemnité temporaire cesse d'être due lorsque le bénéficiaire quitte définitivement le territoire. Le versement de l'indemnité temporaire cesse à compter de la date du départ du territoire. / Lorsque le total des absences du territoire est inférieur à trois mois au cours de l'année civile, le versement de l'indemnité est maintenu. Cette durée est proratisée en cas d'installation ou de départ définitif en cours d'année. / Pour les absences dont la durée cumulée est supérieure à trois mois, le paiement de l'indemnité temporaire est suspendu et reprend sans effet rétroactif à compter du premier jour du quatrième mois suivant le mois du retour. / Les absences pour raisons médicales donnant lieu à évacuation sanitaire ne sont pas prises en compte dans la computation des périodes d'absence, sous réserve de la production des pièces justificatives ".<br/>
<br/>
              4. En premier lieu, la circonstance que le tribunal administratif, dont les jugements ont visé le code de justice administrative, se soit abstenu de viser les dispositions précises de ce code en application desquelles le président du tribunal a statué seul sur les requêtes est dépourvue d'incidence sur la régularité de ces jugements.<br/>
<br/>
              5. En deuxième lieu, il résulte des termes mêmes des dispositions précitées de l'article 9 du décret du 30 janvier 2009 que seules les absences pour raisons médicales donnant lieu à évacuation sanitaire ne sont pas prises en compte dans la computation des périodes d'absence du territoire. Dès lors, contrairement à ce que soutiennent MM. B...etD..., les déplacements pour raisons professionnelles en dehors du territoire de la Polynésie française entrent dans le calcul de la durée cumulée des absences qui, lorsqu'elle est supérieure à trois mois, entraîne la suspension du paiement de l'indemnité temporaire de retraite. Par suite, en estimant que les déplacements de nature professionnelle de MM. B...et D...devaient être inclus dans le décompte de leurs absences du territoire pour la liquidation de l'indemnité temporaire de retraite, le tribunal administratif de la Polynésie française n'a pas commis d'erreur de droit.<br/>
<br/>
              6. En troisième lieu, le tribunal administratif de la Polynésie française n'a pas non plus commis d'erreur de droit en jugeant que MM. B...et D...ne pouvaient utilement se prévaloir de deux réponses ministérielles à des questions parlementaires.<br/>
<br/>
              7. En dernier lieu, aux termes de l'article 2262 du code civil applicable à la Polynésie française : " Toutes les actions, tant réelles que personnelles, sont prescrites par trente ans, sans que celui qui allègue cette prescription soit obligé d'en rapporter un titre ou qu'on puisse lui opposer l'exception déduite de la mauvaise foi ". Selon l'article 2277 du même code, également applicable en Polynésie française : " Se prescrivent par cinq ans les actions en paiement : / Des salaires ; / Des arrérages des rentes perpétuelles et viagères et de ceux des pensions alimentaires ; / Des loyers et fermages ; / Des intérêts des sommes prêtées, / et généralement de tout ce qui est payable par année ou à termes périodiques plus courts ".<br/>
<br/>
              8. D'une part, la prescription quinquennale prévue par l'article 2277 du code civil s'applique à toutes les actions relatives aux créances périodiques, notamment aux accessoires des pensions de retraite telle l'indemnité temporaire de retraite, en cause dans les présents litiges, sans qu'il y ait lieu de distinguer selon qu'il s'agit d'une action en paiement ou en restitution de ce paiement. Cette prescription quinquennale porte sur le délai pour exercer une action en paiement ou en restitution, mais non sur la détermination de la créance elle-même. Par ailleurs, lorsqu'une dette est payable par termes successifs, la prescription se divise comme la dette elle-même et court contre chacune de ses fractions à compter de sa date d'exigibilité, de sorte que l'action en remboursement des sommes versées indûment se prescrit à compter de leurs dates d'exigibilité successives. En outre la prescription quinquennale prévue par l'article 2277 du code civil ne court pas lorsque la créance, même périodique, dépend d'éléments qui ne sont pas connus du créancier et doivent résulter de déclarations que le débiteur est tenu de faire.<br/>
<br/>
              9. D'autre part, il résulte des dispositions précitées de l'article 8 du décret du 30 janvier 2009 que le bénéficiaire de l'indemnité temporaire de retraite, dont le paiement est conditionné par l'effectivité de la résidence locale, est tenu, chaque année, de déclarer ses absences du territoire de la Polynésie française, notamment, ainsi qu'il a été dit précédemment, celles qui sont dues à des déplacements professionnels. A défaut de telles déclarations ou si ces déclarations sont inexactes, les absences du territoire polynésien n'étant pas connues de l'administration, la prescription quinquennale prévue par l'article 2277 du code civil ne court pas.<br/>
<br/>
              10. Par suite, en jugeant que l'administration pouvait procéder à la répétition des sommes indûment versées au titre de l'indemnité temporaire de retraite sur plusieurs années antérieures à celle de son contrôle sur la résidence effective de ses bénéficiaires, lesquels étaient légalement tenus de déclarer leurs absences du territoire,  le tribunal administratif de la Polynésie française, qui n'était pas tenu de se prononcer sur tous les détails de l'argumentation de MM. B... etD..., a suffisamment motivé son jugement et n'a pas commis d'erreur de droit.<br/>
<br/>
              11. Il résulte de ce qui précède que les pourvois de MM. B...et D...doivent être rejetés, y compris leurs conclusions tendant à l'application des dispositions de l'article L 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les pourvois de M. B...et M. D...sont rejetés.<br/>
Article 2 : La présente décision sera notifiée à M. C...B...et M. A...D...et au ministre de l'action et des comptes publics.<br/>
Copie en sera adressée à la ministre des outre-mer et au Haut-commissaire de la République en Polynésie française.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">48-02-01 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. QUESTIONS COMMUNES. - 1) RÈGLES DE PRESCRIPTION APPLICABLES - PRESCRIPTION QUINQUENNALE (ART. 2277 DU CODE CIVIL) - ACTION EN PAIEMENT OU EN RESTITUTION DE L'INDU - DISTINCTION SANS INCIDENCE [RJ1] - 2) POINT DE DÉPART DU DÉLAI DE PRESCRIPTION D'UNE DETTE PAYABLE PAR TERMES SUCCESSIFS - DATE D'EXIGIBILITÉ DE CHACUNE DES FRACTIONS DE LA DETTE - EXCEPTION LORSQUE LA CRÉANCE DÉPEND D'ÉLÉMENTS QUI NE SONT PAS CONNUS DU CRÉANCIER ET DOIVENT RÉSULTER DE DÉCLARATIONS QUE LE DÉBITEUR EST TENU DE FAIRE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">48-02-01-07-02 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. QUESTIONS COMMUNES. DÉCHÉANCE ET SUSPENSION. SUSPENSION. - INDEMNITÉ TEMPORAIRE DE RETRAITE (DÉCRET DU 30 JANVIER 2009) - COMPUTATION DES PÉRIODES D'ABSENCE DU TERRITOIRE ENTRAÎNANT LA SUSPENSION DE CETTE INDEMNITÉ (ART. 9 DE CE DÉCRET) - PRISE EN COMPTE DES DÉPLACEMENTS POUR RAISONS PROFESSIONNELLES EN DEHORS DU TERRITOIRE - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">48-03-03 PENSIONS. RÉGIMES PARTICULIERS DE RETRAITE. PENSIONS DES FONCTIONNAIRES DE LA FRANCE D'OUTRE-MER. - INDEMNITÉ TEMPORAIRE DE RETRAITE (DÉCRET DU 30 JANVIER 2009) - COMPUTATION DES PÉRIODES D'ABSENCE DU TERRITOIRE ENTRAÎNANT LA SUSPENSION DE CETTE INDEMNITÉ (ART. 9 DE CE DÉCRET) - PRISE EN COMPTE DES DÉPLACEMENTS POUR RAISONS PROFESSIONNELLES EN DEHORS DU TERRITOIRE - ABSENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">48-03-05 PENSIONS. RÉGIMES PARTICULIERS DE RETRAITE. PENSIONS DIVERSES. - INDEMNITÉ TEMPORAIRE DE RETRAITE (DÉCRET DU 30 JANVIER 2009) - COMPUTATION DES PÉRIODES D'ABSENCE DU TERRITOIRE ENTRAÎNANT LA SUSPENSION DE CETTE INDEMNITÉ (ART. 9 DE CE DÉCRET) - PRISE EN COMPTE DES DÉPLACEMENTS POUR RAISONS PROFESSIONNELLES EN DEHORS DU TERRITOIRE - ABSENCE.
</SCT>
<ANA ID="9A"> 48-02-01 1) La prescription quinquennale prévue à l'article 2277 du code civil s'applique à toutes les actions relatives aux créances périodiques, notamment aux accessoires des pensions de retraite telle l'indemnité temporaire de retraite, sans qu'il y ait lieu de distinguer selon qu'il s'agit d'une action en paiement ou en restitution de ce paiement. Cette prescription quinquennale porte sur le délai pour exercer une action en paiement ou en restitution, mais non sur la détermination de la créance elle-même.... ...2) Par ailleurs, lorsqu'une dette est payable par termes successifs, la prescription se divise comme la dette elle-même et court contre chacune de ses fractions à compter de sa date d'exigibilité, de sorte que l'action en remboursement des sommes versées indûment se prescrit à compter de leurs dates d'exigibilité successives. En outre la prescription quinquennale prévue par l'article 2277 du code civil ne court pas lorsque la créance, même périodique, dépend d'éléments qui ne sont pas connus du créancier et doivent résulter de déclarations que le débiteur est tenu de faire.</ANA>
<ANA ID="9B"> 48-02-01-07-02 Il résulte de l'article 9 du décret n° 2009-114 du 30 janvier 2009 que seules les absences pour raisons médicales donnant lieu à évacuation sanitaire ne sont pas prises en compte dans la computation des périodes d'absence du territoire. Dès lors, les déplacements pour raisons professionnelles en dehors du territoire entrent dans le calcul de la durée cumulée des absences qui, lorsqu'elle est supérieure à trois mois, entraîne la suspension du paiement de l'indemnité temporaire de retraite.</ANA>
<ANA ID="9C"> 48-03-03 Il résulte de l'article 9 du décret n° 2009-114 du 30 janvier 2009 que seules les absences pour raisons médicales donnant lieu à évacuation sanitaire ne sont pas prises en compte dans la computation des périodes d'absence du territoire de la Polynésie française. Dès lors, les déplacements pour raisons professionnelles en dehors du territoire entrent dans le calcul de la durée cumulée des absences qui, lorsqu'elle est supérieure à trois mois, entraîne la suspension du paiement de l'indemnité temporaire de retraite.</ANA>
<ANA ID="9D"> 48-03-05 Il résulte de l'article 9 du décret n° 2009-114 du 30 janvier 2009 que seules les absences pour raisons médicales donnant lieu à évacuation sanitaire ne sont pas prises en compte dans la computation des périodes d'absence du territoire. Dès lors, les déplacements pour raisons professionnelles en dehors du territoire entrent dans le calcul de la durée cumulée des absences qui, lorsqu'elle est supérieure à trois mois, entraîne la suspension du paiement de l'indemnité temporaire de retraite.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant des actions en paiement ou en restitution de l'indu en matière de rémunération, CE, 12 mars 2010, Mme Vatin, n° 309118, T. p. 822. Comp. Cass. civ. 1ère, 21 février 2006, n° 04-15.962, Bull. I, n° 98, p. 93 et Civ. 2ème, 20 mars 2008, n° 07-1026, Bull. II, n° 73.,,[RJ2] Cf. CE, CE, 12 mars 2013, Mme Vatin, n° 356276, T. pp. 523-660. Rappr. Cass. Ass. Plèn., 7 juillet 1978, n° 76-15.485, Bull. Ass. Pl. n° 4, p. 5 ; Cass. soc. 1er février 2011, n° 10-30.160, Bull. V, n° 44.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
