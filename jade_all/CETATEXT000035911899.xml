<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035911899</ID>
<ANCIEN_ID>JG_L_2017_10_000000403264</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/91/18/CETATEXT000035911899.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 26/10/2017, 403264, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-10-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403264</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP LEVIS</AVOCATS>
<RAPPORTEUR>Mme Dorothée Pradines</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:403264.20171026</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Cobat Constructions a demandé au tribunal administratif d'Amiens d'annuler la décision du 30 juillet 2013 par laquelle l'Office français de l'immigration et de l'intégration a mis à sa charge une contribution spéciale d'un montant de 50 400 euros pour l'emploi de trois ressortissants étrangers démunis d'autorisation de travail. Par un jugement n° 1302608 du 4 février 2014, le tribunal administratif d'Amiens a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14DA00674 du 7 juillet 2016, la cour administrative d'appel de Douai a rejeté l'appel formé par la société Cobat Constructions contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 6 septembre et 6 décembre 2016 au secrétariat du contentieux du Conseil d'Etat, la société Cobat Constructions demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt de la cour administrative d'appel de Douai du 7 juillet 2016 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Office français de l'immigration et de l'intégration la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dorothée Pradines, auditeur,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société Cobat Constructions, et à la SCP Lévis, avocat de l'Office français de l'immigration et de l'intégration.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, le 3 mai 2011, les gendarmes de la compagnie de gendarmerie de Méru ont constaté la présence sur un chantier de construction, situé à Méru, dans l'Oise, de trois ressortissants kosovars employés par la société Cobat Constructions, qui étaient dépourvus d'autorisation de travail et dont les déclarations d'embauche ont été souscrites pendant les opérations de contrôle. Après transmission du procès-verbal faisant état de ces faits au directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi, celui-ci a indiqué à la société Cobat Constructions, par courrier du 1er février 2012, que les dispositions de l'article L. 8253-1 du code du travail étaient susceptibles de lui être appliquées, en l'invitant à présenter ses observations. Par une décision du 30 juillet 2013, l'Office français de l'immigration et de l'intégration a mis à sa charge la contribution spéciale prévue par l'article L. 8253-1 du code du travail, à hauteur de 50 400 euros. La société Cobat Constructions se pourvoit en cassation contre l'arrêt par lequel la cour administrative d'appel de Douai, confirmant le jugement du tribunal administratif d'Amiens du 4 février 2014, a rejeté sa demande tendant à l'annulation de la décision de l'Office français de l'immigration et de l'intégration du 30 juillet 2013.<br/>
<br/>
              2. Le premier alinéa de l'article L. 8251-1 du code du travail dispose que nul ne peut, directement ou indirectement, " embaucher, conserver à son service ou employer pour quelque durée que ce soit un étranger non muni du titre l'autorisant à exercer une activité salariée en France ". Aux termes de l'article L. 8253-1 du même code, dans sa rédaction applicable au litige, eu égard, d'une part, à la date des faits sanctionnés et, d'autre part, à la date à laquelle les juges d'appel se sont prononcés : " Sans préjudice des poursuites judiciaires pouvant être intentées à son encontre, l'employeur qui a employé un travailleur étranger en méconnaissance des dispositions du premier alinéa de l'article L. 8251-1 acquitte, pour chaque travailleur étranger sans titre de travail, une contribution spéciale. Le montant de cette contribution spéciale est déterminé dans des conditions fixées par décret en Conseil d'Etat. Il est, au plus, égal à 5 000 fois le taux horaire du minimum garanti prévu à l'article L. 3231-12. Ce montant peut être minoré en cas de non-cumul d'infractions ou en cas de paiement spontané par l'employeur des salaires et indemnités dus au salarié étranger sans titre mentionné à l'article R. 8252-6. Il est alors, au plus, égal à 2 000 fois ce même taux. Il peut être majoré en cas de réitération et est alors, au plus, égal à 15 000 fois ce même taux. / L'Office français de l'immigration et de l'intégration est chargé de constater et de liquider cette contribution. (...) ". Aux termes de l'article L. 8271-17 du même code, dans sa rédaction applicable à la procédure d'édiction de la sanction litigieuse : " Outre les inspecteurs et contrôleurs du travail, les agents et officiers de police judiciaire, les agents de la direction générale des douanes sont compétents pour rechercher et constater, au moyen de procès-verbaux transmis directement au procureur de la République, les infractions aux dispositions de l'article L. 8251-1 relatif à l'emploi d'un étranger sans titre de travail ". Aux termes de l'article R. 8253-2 du même code, dans sa rédaction applicable : " Un exemplaire des procès-verbaux établi par les fonctionnaires mentionnés à l'article L. 8271-17, constatant l'embauche ou l'emploi d'un étranger non muni d'un titre l'autorisant à exercer une activité salariée en France, est transmis au directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi du département dans lequel l'infraction a été constatée ". Aux termes de l'article R. 8253-3 du même code, dans sa rédaction applicable : " Le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi indique à l'employeur, par lettre recommandée avec avis de réception, que les dispositions de l'article L. 8253-1 sont susceptibles de lui être appliquées et qu'il peut présenter ses observations dans un délai de quinze jours ". Enfin, aux termes de l'article R. 8253-5, alors en vigueur : " Le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi adresse, avec son avis, au directeur général de l'Office français de l'immigration et de l'intégration le procès-verbal ainsi que les observations de l'employeur, s'il en a été produit ".<br/>
<br/>
              3. S'agissant des mesures à caractère de sanction, le respect du principe général des droits de la défense, applicable même sans texte, suppose que la personne concernée soit informée, avec une précision suffisante et dans un délai raisonnable avant le prononcé de la sanction, des griefs formulés à son encontre et puisse avoir accès aux pièces au vu desquelles les manquements ont été retenus, à tout le moins, avant l'entrée en vigueur du code des relations entre le public et l'administration, lorsqu'elle en fait la demande. <br/>
<br/>
              4. Si aucune des dispositions précitées du code du travail ne prévoit expressément que le procès-verbal constatant l'infraction aux dispositions de l'article L. 8251-1 relatif à l'emploi d'un étranger non autorisé à travailler en France, et fondant le versement de la contribution spéciale, soit communiqué au contrevenant, le silence de ces dispositions sur ce point ne saurait faire obstacle à cette communication, en particulier lorsque la personne visée en fait la demande, afin d'assurer le respect de la procédure contradictoire préalable à la liquidation de la contribution spéciale, qui revêt le caractère d'une sanction administrative. Il appartient seulement à l'administration, le cas échéant, d'occulter ou de disjoindre, préalablement à la communication du procès-verbal, celles de ses mentions qui seraient étrangères à la constatation de l'infraction sanctionnée par la liquidation de la contribution spéciale et susceptibles de donner lieu à des poursuites pénales. <br/>
<br/>
              5. La cour a jugé que la société requérante n'était pas fondée à soutenir que la procédure de sanction avait méconnu le principe général des droits de la défense en raison du défaut de communication du procès-verbal d'infraction, dès lors qu'aucune disposition législative ou réglementaire ne subordonnait la légalité de la procédure suivie à la transmission de cette pièce à la société concernée. Il résulte de ce qui a été dit ci-dessus qu'en statuant ainsi, sans rechercher si la société requérante avait demandé la communication du procès-verbal, la cour a commis une erreur de droit.<br/>
<br/>
              6. Par suite, la société Cobat Constructions est fondée à demander l'annulation de l'arrêt de la cour administrative d'appel de Douai qu'elle attaque. Le moyen d'erreur de droit retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens du pourvoi.<br/>
<br/>
              7. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Office français de l'immigration et de l'intégration la somme que la société Cobat Constructions demande au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise au même titre à la charge de la société Cobat Constructions, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Douai du 7 juillet 2016 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Douai.<br/>
Article 3 : Les conclusions des parties présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la société Cobat Constructions et à l'Office français de l'immigration et de l'intégration. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
