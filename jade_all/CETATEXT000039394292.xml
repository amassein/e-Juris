<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039394292</ID>
<ANCIEN_ID>JG_L_2019_11_000000423979</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/39/42/CETATEXT000039394292.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 18/11/2019, 423979, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-11-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423979</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT</AVOCATS>
<RAPPORTEUR>Mme Cécile Viton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:423979.20191118</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              La société par actions simplifiées (SAS) Sofaplast a demandé au tribunal administratif de Paris de condamner l'Etat à lui verser la somme de 841 186 euros, assortie des intérêts à taux légal et des intérêts moratoires, en réparation du préjudice qu'elle estime avoir subi en raison du rejet par l'administration fiscale de la demande d'agrément déposée le 19 décembre 2011, au bénéfice de la société Sofaplast 2012, par la société Alcyom, au titre du financement de ses investissements productifs neufs en Nouvelle-Calédonie. <br/>
<br/>
              Par une ordonnance du 16 février 2017, le président du tribunal administratif de Paris a transmis cette demande au tribunal administratif de Nouvelle-Calédonie. <br/>
<br/>
              Par un jugement n° 1700123 du 14 septembre 2017, le tribunal administratif de Nouvelle-Calédonie a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 17PA03545 du 6 juillet 2018, la cour administrative d'appel de Paris a, sur appel de la société Sofaplast, annulé le jugement du tribunal administratif de Nouvelle-Calédonie et fait droit à sa demande. <br/>
<br/>
              1° Sous le n° 423979, par un pourvoi et un mémoire en réplique enregistrés les 10 septembre 2018 et 21 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'action et des comptes publics demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 423984, par une requête et un mémoire en réplique enregistrés les 10 septembre 2018 et 21 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'action et des comptes publics demande au Conseil d'Etat d'ordonner le sursis à exécution de l'arrêt du 6 juillet 2018 de la cour administrative d'appel de Paris. <br/>
<br/>
              Il soutient que l'exécution de cet arrêt risque d'entraîner des conséquences difficilement réparables, compte tenu de la situation économique difficile de la société Sofaplast, et qu'il existe des moyens sérieux et de nature à justifier l'infirmation de la solution retenue par la cour administrative d'appel de Paris.<br/>
<br/>
              Par un mémoire en défense et un autre mémoire, enregistrés les 2 et 9 janvier 2019, la société Sofaplast conclut au rejet de la demande de sursis à exécution et demande que la somme de 3 500 euros soit mise à la charge de l'Etat en application de l'article L. 761-1 du code de justice administrative. Elle soutient que les moyens soulevés par le ministre de l'action et des comptes publics ne sont pas fondés.<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de l'environnement de la province Sud de Nouvelle-Calédonie ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Viton, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, Robillot, avocat de la société Sofaplast ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Le pourvoi en cassation et la requête aux fins de sursis à exécution du ministre de l'action et des comptes publics sont dirigés contre le même arrêt de la cour administrative d'appel de Paris. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que la société Sofaplast, entreprise industrielle implantée en Nouvelle-Calédonie, a donné mandat, le 15 décembre 2011, à la société Alcyom pour assurer le financement de ses investissements à Nouméa dans le cadre d'une société de portage, en bénéficiant de l'avantage fiscal prévu en faveur des investissements productifs neufs en Nouvelle-Calédonie par l'article 199 undecies B du code général des impôts. En application des dispositions du même article, la société Alcyom a sollicité le 19 décembre 2011, pour le compte de la société de portage Sofaplast 2012, l'agrément préalable du ministre chargé du budget pour bénéficier de cet avantage fiscal. Cet agrément a été refusé par une décision du 29 janvier 2013 du ministre chargé du budget, prise sur le fondement du III de l'article 217 undecies du code général des impôts. Cette décision a été annulée par un jugement du 18 juin 2014 du tribunal administratif de Paris devenu définitif. Le ministre de l'action et des comptes publics se pourvoit en cassation contre l'arrêt du 6 juillet 2018 par lequel la cour administrative d'appel de Paris a annulé le jugement du 14 septembre 2017 du tribunal administratif de Nouvelle-Calédonie et condamné l'Etat à verser à la société Sofaplast la somme de 841 186 euros, assortie des intérêts au taux légal à compter du 26 janvier 2017, date de la réception par l'administration de sa réclamation préalable, en réparation du préjudice subi du fait du refus illégal d'agrément préalable de l'administration. <br/>
<br/>
              3. Aux termes de l'article 199 undecies B du code général des impôts dans sa version applicable au litige : " I. Les contribuables domiciliés en France au sens de l'article 4 B peuvent bénéficier d'une réduction d'impôt sur le revenu à raison des investissements productifs neufs qu'ils réalisent (...) en Nouvelle-Calédonie (...) dans le cadre d'une entreprise exerçant une activité agricole ou une activité industrielle, commerciale ou artisanale relevant de l'article 34. / (...) La réduction d'impôt prévue au présent I s'applique, dans les conditions prévues au vingt-sixième alinéa, aux investissements réalisés par une société soumise de plein droit à l'impôt sur les sociétés dont les actions sont détenues intégralement et directement par des contribuables, personnes physiques, domiciliés en France au sens de l'article 4 B. (...) L'application de cette disposition est subordonnée au respect des conditions suivantes : / 1° Les investissements ont reçu un agrément préalable du ministre chargé du budget dans les conditions prévues au III de l'article 217 undecies ; (...) ". Aux termes du III de l'article 217 undecies du même code : " (...) L'agrément est délivré lorsque l'investissement : / a) Présente un intérêt économique pour le département dans lequel il est réalisé ; il ne doit pas porter atteinte aux intérêts fondamentaux de la nation ou constituer une menace contre l'ordre public ou laisser présumer l'existence de blanchiment d'argent ; b) Poursuit comme l'un de ses buts principaux la création ou le maintien d'emplois dans ce département ; c) S'intègre dans la politique d'aménagement du territoire, de l'environnement et de développement durable ; d) Garantit la protection des investisseurs et des tiers. (...) ".  <br/>
<br/>
              4. En premier lieu, par un jugement du 18 juin 2014 devenu définitif, le tribunal administratif de Paris a annulé la décision de refus de l'agrément préalable prévu à l'article 199 undecies B du code général des impôts au motif que l'administration n'était pas fondée à invoquer un défaut d'intérêt économique des investissements en cause. L'autorité absolue de la chose jugée qui s'attache à ce jugement fait obstacle à ce que le ministre de l'action et des comptes publics soutienne que la cour a commis une erreur de droit en écartant son moyen tiré de ce que les investissements envisagés par la société Sofaplast à Nouméa ne présentaient aucun intérêt économique pour la Nouvelle-Calédonie dans la mesure où ils consistaient en un simple remplacement de l'appareil productif dont le financement pouvait être assuré grâce aux dotations aux amortissements.<br/>
<br/>
              5. En deuxième lieu, les dispositions rappelées au point 2 ci-dessus instituent, au profit des sociétés qui remplissent les conditions qu'elles fixent, un droit au bénéfice de l'agrément préalable qu'elles prévoient. Elles ne permettent au ministre chargé du budget ni de refuser cet agrément, ni de limiter le montant des investissements productifs pour lesquels il est délivré en se fondant sur d'autres conditions que celles qui sont prévues par la loi. Par suite, le ministre de l'action et des comptes publics n'est pas fondé à soutenir que la cour a commis une erreur de droit en jugeant que la société Sofaplast 2012 avait droit à la délivrance de l'agrément qu'elle sollicitait alors que, selon lui, le refus d'agrément préalable aurait pu être justifié par l'absence de caractère suffisamment incitatif de l'avantage fiscal sollicité au vu de la situation financière saine de la société, condition qui n'est pas prévue par la loi.<br/>
<br/>
              6. En troisième lieu, le ministre de l'action et des comptes publics soutient, pour la première fois en cassation, que le projet d'investissement de la société Sofaplast ne s'intégrait pas dans la politique d'aménagement du territoire, de l'environnement et du développement durable de la Nouvelle-Calédonie au motif que l'avis du 20 avril 2012 du haut-commissaire de la République en Nouvelle-Calédonie relevait que ses deux sites industriels situés à Ducos et au lieu-dit " PK4 " n'avaient pas été régulièrement déclarés en tant qu'installations classées pour la protection de l'environnement (ICPE). Toutefois, il ressort des pièces du dossier soumis aux juges du fond que la direction de l'industrie, des mines et de l'énergie de Nouvelle-Calédonie a délivré à la société Sofaplast le 31 mai 2012, soit postérieurement à l'avis du haut-commissaire de la République en Nouvelle-Calédonie mais antérieurement à la décision de rejet de l'agrément préalable du 29 janvier 2013, une attestation de dépôt de dossier de déclaration au titre de la réglementation relative aux ICPE pour chacun de ses deux sites. Dès lors, le moyen du ministre de l'action et des comptes publics ne peut, en tout état de cause, qu'être écarté. <br/>
<br/>
              7. Il résulte de ce qui a été dit aux points 4 à 6 ci-dessus que la cour administrative d'appel de Paris n'a entaché son arrêt ni d'erreur de droit, ni d'erreur de qualification juridique en jugeant que la demande d'agrément préalable de la société Sofaplast 2012 ne méconnaissait aucune des conditions posées au III de l'article 217 undecies du code général des impôts et que cette société avait droit à la délivrance de l'agrément prévu à l'article 199 undecies B du même code. <br/>
<br/>
              8. En quatrième et dernier lieu, la cour administrative d'appel de Paris n'a pas inexactement qualifié les faits en jugeant qu'un lien de causalité direct entre la faute de l'administration et le préjudice subi par la société Sofaplast était établi dès lors que, contrairement à ce que soutient le ministre, la société n'était pas tenue d'attendre le jugement du tribunal administratif de Paris relatif à la légalité du refus d'agrément préalable avant de réaliser l'investissement en cause, mais pouvait directement rechercher la responsabilité pour faute de l'administration à raison de la perte de l'avantage fiscal dont elle était en droit de bénéficier au titre de son investissement réalisé dès 2013 grâce à d'autres moyens de financement.  <br/>
<br/>
              9. Il résulte de tout ce qui précède que le ministre de l'action et des comptes publics n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque. Par suite, ses conclusions tendant à ce qu'il soit sursis à l'exécution de cet arrêt sont devenues sans objet.<br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme globale de 5 000 euros à verser à la société Sofaplast au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi n° 423979 du ministre de l'action et des comptes publics est rejeté. <br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions de la requête n° 423984 du ministre de l'action et des comptes publics.<br/>
<br/>
Article 3 : L'Etat versera à la société Sofaplast une somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : La présente décision sera notifiée au ministre de l'action et des comptes publics et à la société Sofaplast. <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
