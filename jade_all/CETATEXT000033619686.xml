<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033619686</ID>
<ANCIEN_ID>JG_L_2016_12_000000388335</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/61/96/CETATEXT000033619686.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 15/12/2016, 388335</TITRE>
<DATE_DEC>2016-12-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388335</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Karin Ciavaldini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:388335.20161215</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Caen d'annuler l'arrêté du 2 juin 2012 par lequel le maire de la commune d'Urou-et-Crennes (Orne) a refusé de l'autoriser à créer un accès sur la rue des Haras au droit de sa propriété. Par un jugement n° 1201523 du 7 juin 2013, le tribunal a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 13NT02355 du 30 décembre 2014, la cour administrative d'appel de Nantes a, sur appel de M.A..., annulé ce jugement et l'arrêté du 2 juin 2012 du maire de la commune d'Urou-et-Crennes.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 27 février et 28 mai 2015 au secrétariat du contentieux du Conseil d'Etat, la commune d'Urou-et-Crennes demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de M. A...la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Karin Ciavaldini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de la commune d'Urou-et-Crennes et à la SCP Lyon-Caen, Thiriez, avocat de M. A....<br/>
<br/>
<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 2 juin 2012, le maire de la commune d'Urou-et-Crennes (Orne) a refusé à M. A... l'autorisation de créer un accès pour véhicules sur la rue des Haras au droit de sa propriété. M. A...a demandé au tribunal administratif de Caen l'annulation de cet arrêté. Par un jugement du 7 juin 2013, le tribunal a rejeté cette demande. Par un arrêt du 30 décembre 2014, la cour administrative d'appel de Nantes a annulé ce jugement et la décision du 2 juin 2012. La commune d'Urou-et-Crennes se pourvoit en cassation contre cet arrêt.<br/>
<br/>
              2. En premier lieu, si la fiche de suivi de la requête mentionne la réception par la cour, le 11 décembre 2014, d'une " lettre " émanant de M. A..., il ressort des indications portées sur cette fiche, qui ne sont pas utilement contestées par la commune, que cette " lettre " correspond à un message électronique, dont le greffe de la cour avait été destinataire en copie, adressé par M. A...à son conseil et lui demandant de bien vouloir produire un mémoire en désistement. Contrairement à ce que soutient la commune, la cour n'a pas commis d'irrégularité en n'estimant pas, sur le fondement du seul courriel mentionné ci-dessus, que M. A... se serait désisté de sa requête d'appel. <br/>
<br/>
              3. En deuxième lieu, sauf dispositions législatives contraires, les riverains d'une voie publique ont le droit d'accéder librement à leur propriété, et notamment, d'entrer et de sortir des immeubles à pied ou avec un véhicule. Dans le cas d'une voie communale, le maire ne peut refuser d'accorder un tel accès, qui constitue un accessoire du droit de propriété, que pour des motifs tirés de la conservation et de la protection du domaine public ou de la sécurité de la circulation sur la voie publique. Lorsque l'accès à la voie publique avec un véhicule est de nature à mettre en cause la sécurité de la circulation, le maire n'est pas tenu de permettre l'accès en modifiant l'emprise de la voie publique. Toutefois, il ne peut refuser un tel accès sans rechercher si un aménagement léger sur le domaine public, qui serait légalement possible, ne serait pas de nature à permettre de faire droit à la demande dans de bonnes conditions de sécurité. La réalisation et l'entretien de cet aménagement destiné à assurer la sécurité de la circulation sur la voie publique incombent à la commune, mais l'autorisation peut être subordonnée à la condition que le pétitionnaire accepte de prendre à sa charge tout ou partie du coût de la réalisation et de l'entretien de l'aménagement en cause, compte tenu de son utilité éventuelle pour des besoins généraux de la circulation sur la voie publique.  <br/>
<br/>
              4. Pour estimer que le refus opposé par la commune à la demande de M. A... était entaché d'une erreur d'appréciation, la cour a notamment relevé, par des motifs dont il n'est pas soutenu qu'ils seraient entachés d'inexactitude matérielle ou de dénaturation, que, s'il ressortait des pièces du dossier qui lui était soumis que l'accès envisagé sur la rue des Haras s'effectuait dans une courbe légère n'offrant aucune visibilité sur la gauche à un conducteur sortant de la propriété de M.A..., la pose d'un miroir approprié en face de l'accès à la propriété du requérant, dont la commune n'établissait pas qu'elle serait irréalisable, permettrait de procurer aux véhicules sortant du terrain de M. A... une visibilité satisfaisante sur la gauche. En tenant compte, pour apprécier la légalité de la décision contestée, de la possibilité de procéder à cet aménagement léger et des effets en résultant, la cour qui, contrairement à ce que soutient la commune, n'a pas jugé qu'il incombait à cette dernière d'en prendre en charge le coût, n'a pas commis d'erreur de droit. <br/>
<br/>
              5. En troisième lieu, la cour, qui a relevé, par une appréciation souveraine non entachée de dénaturation et par un arrêt suffisamment motivé, que la pose d'un miroir approprié permettrait de procurer aux véhicules sortant du terrain de M. A...une visibilité satisfaisante sur la gauche, que la vitesse des véhicules dans le bourg demeurait suffisamment réduite pour permettre aux conducteurs circulant rue des Haras d'apercevoir en temps utile un véhicule sortant de l'accès dont M. A...sollicitait la création, que la configuration de la rue favorisait une allure modérée des véhicules au droit de la propriété de M. A...et que le trafic restait relativement peu important dans cette rue, n'a pas inexactement qualifié les faits qui lui étaient soumis en en déduisant que le maire de la commune avait entaché sa décision d'une erreur d'appréciation en refusant l'accès demandé par M. A... au motif qu'il serait de nature à mettre en péril la sécurité des usagers de la voie publique.<br/>
<br/>
              6. Il résulte de ce qui précède que la commune d'Urou-et-Crennes n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. A...qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune d'Urou-et-Crennes le versement à M. A...au même titre de la somme de 3 000 euros.<br/>
<br/>
<br/>
<br/>                 D E C I D E :<br/>
                                --------------<br/>
<br/>
Article 1er : Le pourvoi de la commune d'Urou-et-Crennes est rejeté. <br/>
<br/>
Article 2 : La commune d'Urou-et-Crennes versera à M. A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la commune d'Urou-et-Crennes et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">71-02-04-01 VOIRIE. RÉGIME JURIDIQUE DE LA VOIRIE. DROITS ET OBLIGATIONS DES RIVERAINS ET USAGERS. RIVERAINS. - DROIT DES RIVERAINS D'ACCÉDER LIBREMENT À LEUR PROPRIÉTÉ [RJ1] - INCLUSION - DROIT D'ACCÈS AVEC UN VÉHICULE - FACULTÉ POUR LE MAIRE DE REFUSER CET ACCÈS - EXISTENCE - CONDITIONS.
</SCT>
<ANA ID="9A"> 71-02-04-01 Sauf dispositions législatives contraires, les riverains d'une voie publique ont le droit d'accéder librement à leur propriété et, notamment, d'entrer et de sortir des immeubles à pied ou avec un véhicule. Dans le cas d'une voie communale, le maire ne peut refuser d'accorder un tel accès, qui constitue un accessoire du droit de propriété, que pour des motifs tirés de la conservation et de la protection du domaine public ou de la sécurité de la circulation sur la voie publique.,,,Lorsque l'accès à la voie publique avec un véhicule est de nature à mettre en cause la sécurité de la circulation, le maire n'est pas tenu de permettre l'accès en modifiant l'emprise de la voie publique. Toutefois, il ne peut refuser un tel accès sans rechercher si un aménagement léger sur le domaine public, qui serait légalement possible, ne serait pas de nature à permettre de faire droit à la demande dans de bonnes conditions de sécurité. La réalisation et l'entretien de cet aménagement destiné à assurer la sécurité de la circulation sur la voie publique incombent à la commune, mais l'autorisation peut être subordonnée à la condition que le pétitionnaire accepte de prendre à sa charge tout ou partie du coût de la réalisation et de l'entretien de l'aménagement en cause, compte tenu de son utilité éventuelle pour  des besoins généraux de la circulation sur la voie publique.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 20 octobre 1961, Hamon, n° 51808, T. p. 1213 ; CE, 26 mai 1965, Ministre des travaux publics c/ Epoux Tebaldini, n° 61896, p. 304; CE, 19 janvier 2001, Département de Tarn-et-Garonne, n° 197026, p. 30.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
