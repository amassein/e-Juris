<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037022321</ID>
<ANCIEN_ID>JG_L_2018_06_000000414511</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/02/23/CETATEXT000037022321.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  2ème chambre jugeant seule, 06/06/2018, 414511, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414511</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 2ème chambre jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Paul Bernard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:414511.20180606</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 21 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, Mme B...A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 21 juillet 2017 par laquelle le ministre d'Etat, ministre de l'intérieur a refusé de modifier le décret du 2 septembre 2016 lui accordant la nationalité française pour y porter le nom de l'enfant Omichessan Maéra Emma Fatoumata ;<br/>
<br/>
              2°) d'enjoindre au ministre de procéder à la modification du décret dans le délai d'un mois ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Paul Bernard, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes de l'article 22-1 du code civil : " L'enfant mineur dont l'un des deux parents acquiert la nationalité française, devient français de plein droit s'il a la même résidence habituelle que ce parent ou s'il réside alternativement avec ce parent dans le cas de séparation ou divorce./ Les dispositions du présent article ne sont applicables à l'enfant d'une personne qui acquiert la nationalité française par décision de l'autorité publique ou par déclaration de nationalité que si son nom est mentionné dans le décret ou dans la déclaration " ;<br/>
<br/>
              2.	Considérant qu'il résulte de ces dispositions qu'un enfant mineur ne peut devenir français de plein droit par l'effet du décret qui confère la nationalité française à l'un de ses parents qu'à condition, d'une part, que ce parent ait porté son existence, sauf impossibilité ou force majeure, à la connaissance de l'administration chargée d'instruire la demande préalablement à la signature du décret et, d'autre part, qu'il ait, à la date du décret, résidé avec ce parent de manière stable et durable sous réserve, le cas échéant, d'une résidence en alternance avec l'autre parent en cas de séparation ou de divorce ;<br/>
<br/>
              3.	Considérant que Mme A...a acquis la nationalité française par décret du 2 septembre 2016 ; qu'elle a demandé la modification de ce décret pour faire bénéficier sa fille, Omichessan Maéra Emma Fatoumata, de la nationalité française en conséquence de sa naturalisation ; qu'elle demande au Conseil d'Etat l'annulation pour excès de pouvoir de la décision du 21 juillet 2017 par laquelle le ministre d'Etat, ministre de l'intérieur, a refusé la modification du décret du 2 septembre 2016 pour y porter mention du nom de sa fille ; <br/>
<br/>
              4.	Considérant qu'il ressort des éléments versés au dossier que Mme A...fait valoir, sans que ses dires soient utilement démentis par le ministre, qu'elle a adressé l'acte de naissance de sa fille, née le 2 mars 2016, peu de temps après la naissance aux services de la préfecture de la Seine-Saint-Denis, conformément à ce qui lui avait été indiqué lors du dépôt de sa demande de naturalisation ; qu'elle a, de nouveau, remis cet acte de naissance le 10 mai 2016 à la préfecture à l'occasion de la modification d'indications portées sur sa carte de résident ; qu'elle doit ainsi être regardée comme ayant dûment porté à la connaissance de l'administration chargée d'instruire sa demande de naturalisation la naissance de sa fille, avant l'intervention, le 2 septembre 2016, du décret lui accordant la nationalité française ; que, dès lors, Mme A...est fondée à soutenir que le refus de modifier le décret pour y porter le nom de sa fille méconnaît les dispositions de l'article 22-1 du code civil ;<br/>
<br/>
              5.	Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de sa requête, Mme A...est fondée à demander l'annulation de la décision du 21 juillet 2017 par laquelle le ministre d'Etat, ministre de l'intérieur, a refusé de modifier le décret du 2 septembre 2016 prononçant sa naturalisation afin d'y faire figurer le nom de sa fille Omichessan Maéra Emma Fatoumata ; <br/>
<br/>
              6.	Considérant que l'exécution de la présente décision implique nécessairement que le décret du 2 septembre 2016 accordant la nationalité française à Mme A... soit modifié pour y porter le nom de l'enfant ; qu'il y a lieu d'enjoindre au ministre de l'intérieur de proposer au Premier ministre de modifier ainsi ce décret dans un délai de deux mois à compter de la notification de la présente décision, sans qu'il soit nécessaire, dans les circonstances de l'espèce, d'assortir cette injonction d'une astreinte ;<br/>
              7.	Considérant qu'il y a lieu de mettre à la charge de l'Etat la somme de 1 500 euros à verser à Mme A...au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La décision du 21 juillet 2017 par laquelle le ministre d'Etat, ministre de l'intérieur a refusé de modifier le décret du 2 septembre 2016 accordant la nationalité française à Mme A...pour y porter le nom de l'enfant Omichessan Maéra Emma Fatoumata est annulée. <br/>
<br/>
Article 2 : Il est enjoint au ministre d'Etat, ministre de l'intérieur, de proposer au Premier ministre de modifier le décret du 2 septembre 2016 accordant la nationalité française à Mme A..., pour y porter le nom de l'enfant Omichessan Maéra Emma Fatoumata, dans le délai de deux mois à compter de la notification de la présente décision. <br/>
<br/>
Article 3 : L'Etat versera à Mme A...la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme B...A...et au ministre d'Etat, ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
