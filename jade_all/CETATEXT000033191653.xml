<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033191653</ID>
<ANCIEN_ID>JG_L_2016_10_000000389451</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/19/16/CETATEXT000033191653.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 03/10/2016, 389451, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-10-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389451</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Pierre Lombard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:389451.20161003</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a saisi le tribunal administratif de Paris d'une demande tendant à la condamnation du centre d'action sociale de la ville de Paris à lui verser une somme de 146 000 euros en réparation du préjudice causé par l'attitude de l'administration face au harcèlement moral et sexuel qu'elle estimait avoir subi sur son lieu de travail.<br/>
<br/>
              Par un jugement n°1114961/5-4 du 22 janvier 2013, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13PA04763 du 13 février 2015, la cour administrative d'appel de Paris a annulé ce jugement et fait partiellement fait droit à la demande de Mme A...en condamnant le centre d'action sociale de la ville de Paris à lui verser une somme de 5 000 euros en réparation de son préjudice moral et des troubles dans ses conditions d'existence, sous réserve que le versement soit subordonné à la subrogation de ce centre, à concurrence de cette somme, dans les droits détenus par Mme A...à l'encontre de M.C....<br/>
<br/>
              Par un pourvoi et un mémoire complémentaire, enregistrés les 13 avril et 13 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il lui fait grief ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire intégralement droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la Ville de Paris la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Lombard, auditeur,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de Mme A...et à la SCP Foussard, Froger, avocat du centre d'action sociale de la ville de Paris ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme A...a exercé de 1994 à 2003 les fonctions d'infirmière au sein de l'établissement d'hébergement pour personnes âgées dépendantes (EHPAD) Anselme Payen, qui relève du centre d'action sociale de la ville de Paris (CASVP) ; que, le 15 mars 2003, elle a déposé plainte pour harcèlement sexuel et moral à l'encontre d'un aide-soignant placé sous son autorité d'avril à décembre 2002 au sein de cet EHPAD, M. C... ; que, par un arrêt rendu par la cour d'appel de Paris le 8 décembre 2008 au terme de la procédure pénale ainsi engagée, M. C... a été définitivement condamné à verser à Mme A...une somme de 5 000 euros en réparation de son préjudice moral ; que Mme A...a déposé le 22 avril 2011 auprès du CASVP une demande préalable tendant à la réparation du préjudice qu'elle estimait avoir subi du fait de l'administration, qui, bien qu'informée des faits de harcèlement dont M. C... était l'auteur, n'avait, selon elle, pas pris les mesures nécessaires pour les faire cesser ; que cette demande ayant été implicitement rejetée, Mme A...a saisi le tribunal administratif de Paris de conclusions indemnitaires, lesquelles ont été rejetées par un jugement du 2  janvier 2013 ;  que, par un arrêt du 13 février 2015, la cour administrative d'appel de Paris a condamné le CASVP à verser à Mme A...une somme de 5 000 euros, en en subordonnant le versement à ce que le CASVP soit, à concurrence de cette somme, subrogé dans les droits détenus par Mme A... à l'encontre de M.C..., mais rejeté le surplus des conclusions de MmeA... ; que celle-ci se pourvoit contre cet arrêt en tant qu'il lui fait grief ;<br/>
<br/>
              2. Considérant que la cour a jugé que si Mme A...avait informé sa hiérarchie de difficultés rencontrées dans ses relations de travail avec son subordonné, elle avait négligé de lui faire part des agissements imputables à M. C...et dont elle s'estimait personnellement victime ; qu'il ressort toutefois des pièces du dossier soumis aux juges du fond que, notamment par deux lettres adressées en août 2002 à la directrice de l'EHPAD, Mme A... avait informé celle-ci du comportement observé et des propos tenus par M. C... à son encontre ; que, dès lors, la cour a dénaturé les pièces du dossier sur ce point ; que, par suite et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, son arrêt doit être annulé en tant qu'il rejette le surplus des conclusions d'appel de MmeA... ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du CASVP la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 13 février 2015 est annulé en tant qu'il rejette le surplus des conclusions d'appel de MmeA....<br/>
Article 2 : L'affaire est renvoyée, dans la mesure de la cassation ainsi prononcée, à la cour administrative d'appel de Paris.<br/>
Article 3 : Le CASVP versera à Mme A...la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à Mme B...A...et au centre d'action sociale de la ville de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
