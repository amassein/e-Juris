<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041688017</ID>
<ANCIEN_ID>JG_L_2020_02_000000438852</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/68/80/CETATEXT000041688017.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 28/02/2020, 438852, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-02-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438852</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:438852.20200228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme A... B... veuve C... a demandé au juge des référés du tribunal administratif de Marseille, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre à l'Assistance publique-Hôpitaux de Marseille (AP-HM) de prendre toutes mesures utiles afin de permettre l'exportation des gamètes de M. D... C... vers un établissement de santé situé en Espagne autorisé à pratiquer les procréations médicalement assistées et d'y procéder dans le délai de huit jours suivant la notification de la décision à intervenir. Par une ordonnance n° 2000944 du 10 février 2020, le juge des référés du tribunal administratif de Marseille a rejeté sa demande. <br/>
<br/>
              Par une requête, enregistrée le 19 février 2020 au secrétariat du contentieux du Conseil d'Etat, Mme C... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à ses conclusions de première instance.<br/>
<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - le juge des référés a commis une erreur de droit en estimant que l'autorisation ne pouvant être accordée que par l'Agence de biomédecine, l'Assistance publique - Hôpitaux de Marseille n'avait commis aucune illégalité en n'accordant pas l'autorisation ;<br/>
              - la condition d'urgence est remplie dès lors que, d'une part, elle est privée de la possibilité de concevoir un enfant de M. D... C..., d'autre part, la loi espagnole n'autorise le recours à une insémination artificielle en vue d'une conception posthume que dans les douze mois suivant la mort du mari, ce délai arrivant à échéance le 23 mars 2020 et, enfin, la destruction des gamètes de son époux peut intervenir à tout moment ;<br/>
              - il est porté une atteinte grave et manifestement illégale à son droit au respect de la vie privée et familiale et à son droit de décider d'avoir un enfant protégés par les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme dès lors que son époux avait explicitement consenti à une paternité post-mortem ;<br/>
              - sa situation constitue une circonstance particulière de nature à justifier l'autorisation de transfert des gamètes de son époux vers l'Espagne.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Eu égard à son office, qui consiste à assurer la sauvegarde des libertés fondamentales, il appartient au juge des référés, saisi sur le fondement de l'article L. 521-2 du code de justice administrative, de prendre, en cas d'urgence, toutes les mesures qui sont de nature à remédier aux effets résultant d'une atteinte grave et manifestement illégale portée, par une autorité administrative, à une liberté fondamentale, y compris lorsque cette atteinte résulte de l'application de dispositions législatives qui sont manifestement incompatibles avec les engagements européens ou internationaux de la France, ou dont la mise en oeuvre entraînerait des conséquences manifestement contraires aux exigences nées de ces engagements.<br/>
<br/>
              3. Aux termes de l'article L. 2141-2 du code de la santé publique : " L'assistance médicale à la procréation a pour objet de remédier à l'infertilité d'un couple ou d'éviter la transmission à l'enfant ou à un membre du couple d'une maladie d'une particulière gravité. (...) L'homme et la femme formant le couple doivent être vivants, en âge de procréer et consentir préalablement au transfert des embryons ou à l'insémination. Font obstacle à l'insémination ou au transfert des embryons le décès d'un des membres du couple, le dépôt d'une requête en divorce ou en séparation de corps ou la cessation de la communauté de vie, ainsi que la révocation par écrit du consentement par l'homme ou la femme auprès du médecin chargé de mettre en oeuvre l'assistance médicale à la procréation. " L'article L. 2141-11 de ce même code dispose que : " Toute personne dont la prise en charge médicale est susceptible d'altérer la fertilité, ou dont la fertilité risque d'être prématurément altérée, peut bénéficier du recueil et de la conservation de ses gamètes ou de ses tissus germinaux, en vue de la réalisation ultérieure, à son bénéfice, d'une assistance médicale à la procréation, ou en vue de la préservation et de la restauration de sa fertilité. Ce recueil et cette conservation sont subordonnés au consentement de l'intéressé et, le cas échéant, de celui de l'un des titulaires de l'autorité parentale, ou du tuteur, lorsque l'intéressé, mineur ou majeur, fait l'objet d'une mesure de tutelle. (...) ". Le III de l'article R. 2141-18 du même code dispose également que : " Il est mis fin à la conservation des gamètes ou des tissus germinaux en cas de décès de la personne. Il en est de même si, n'ayant pas répondu à la consultation selon les modalités fixées par l'arrêté prévu aux articles<br/>
R. 2142-24 et R. 2142- 27, elle n'est plus en âge de procréer ". <br/>
<br/>
              4. En outre, l'article L. 2141-11-1 du même code dispose: " L'importation et l'exportation de gamètes ou de tissus germinaux issus du corps humain sont soumises à une autorisation délivrée par l'Agence de la biomédecine. (...). / Seuls les gamètes et les tissus germinaux recueillis et destinés à être utilisés conformément aux normes de qualité et de sécurité en vigueur, ainsi qu'aux principes mentionnés aux articles L. 1244-3, L. 1244-4,<br/>
L. 2141-2, L. 2141-3, L. 2141-7 et L. 2141-11 du présent code et aux articles 16 à 16-8 du code civil, peuvent faire l'objet d'une autorisation d'importation ou d'exportation. (...) ". <br/>
<br/>
              5. Il résulte de ces dispositions qu'en principe, la conservation de gamètes ne peut être autorisée en France qu'en vue de la réalisation d'une assistance médicale à la procréation entrant dans les prévisions légales du code de la santé publique et que la conservation des gamètes ne peut être poursuivie après le décès du donneur.<br/>
<br/>
              5. M. D... C... a effectué un dépôt de paillettes de sperme au sein du service de biologie de la reproduction du Centre d'étude et de conservation des oeufs et du sperme (CECOS) de l'hôpital de La Conception à Marseille. Il s'est marié le 30 janvier 2019 avec Mme A... B... et est décédé le 23 mars. Mme C... a, par courrier du 25 mai 2019, sollicité le transfert vers un établissement de santé espagnol des paillettes de sperme de son époux. Par courrier du 10 janvier 2020, le CECOS a adressé à l'Agence de la biomédecine la demande d'exportation. Par une ordonnance du 10 février 2020, dont il est relevé appel, le juge des référés du tribunal administratif de Marseille a rejeté la demande de Mme C... tendant à ce qu'il soit fait injonction à l'Assistance publique-Hôpitaux de Marseille (AP-HM) de prendre toutes mesures utiles afin de permettre l'exportation des gamètes de M. D... C... vers un établissement de santé, situé en Espagne et autorisé à pratiquer les procréations médicalement assistées, dans un délai de huit jours suivant la notification de sa décision.<br/>
              6. En premier lieu, ainsi que l'a estimé le juge des référés du tribunal administratif de Marseille, en s'abstenant de faire droit à la demande d'exportation, dont elle avait en outre saisi le 10 janvier 2020 l'Agence de biomédecine, compétente pour y donner ou non suite en application de l'article L. 2141-11-1 du code de la santé publique, l'AP-HM n'a pas, contrairement à ce qui est soutenu, porté une atteinte grave et manifestement illégale aux droits protégés par les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme. <br/>
<br/>
              7. En second lieu, et en tout état de cause, l'interdiction posée par l'article L. 2141-2 du code de la santé publique d'utiliser, en cas de décès du mari, les gamètes de celui-ci au profit de sa veuve, relève de la marge d'appréciation dont chaque Etat dispose pour l'application de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et elle ne porte pas, par elle-même, une atteinte disproportionnée au droit au respect de la vie privée et familiale, tel qu'il est garanti par les stipulations de l'article 8 de cette convention. Les dispositions de l'article L. 2141-11-1 de ce même code qui interdisent également que des gamètes conservés en France puissent faire l'objet d'un déplacement, s'ils sont destinés à être utilisés, à l'étranger, à des fins qui sont prohibées sur le territoire national, visent à faire obstacle à tout contournement des dispositions de l'article L. 2141-2 et ne méconnaissent pas davantage, par elles-mêmes, les exigences nées de l'article 8 de la convention européenne.<br/>
<br/>
              8. Toutefois, la compatibilité de la loi avec les stipulations de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ne fait pas obstacle à ce que, dans certaines circonstances particulières, l'application de dispositions législatives puisse constituer une ingérence disproportionnée dans les droits garantis par cette convention. Il appartient par conséquent au juge d'apprécier concrètement si, au regard des finalités des dispositions législatives en cause, l'atteinte aux droits et libertés protégés par la convention qui résulte de la mise en oeuvre de dispositions, par elles-mêmes compatibles avec celle-ci, n'est pas excessive.<br/>
<br/>
              9. La demande tendant à ce que les gamètes de M. C... soient déplacées vers un établissement médical espagnol résulte d'un projet parental auquel celui-ci avait consenti de son vivant. Toutefois, il n'est pas contesté que la demande d'exportation en Espagne n'est fondée que sur la possibilité légale d'y faire procéder à une insémination artificielle post-mortem, Mme C..., de nationalité française, n'entretenant aucun lien avec l'Espagne et ne faisant état d'aucune circonstance particulière de nature à établir que la décision contestée porterait une atteinte excessive aux stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. <br/>
<br/>
              10. Il résulte de tout ce qui précède que Mme C... n'est pas fondée à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Marseille a rejeté sa demande. Sa requête ne peut dès lors qu'être rejetée, selon la procédure prévue à l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de Mme C... est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à Mme A... B... veuve C....<br/>
Copie en sera adressée à l'Assistance publique-Hôpitaux de Marseille (AP-HM).<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
