<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042737145</ID>
<ANCIEN_ID>JG_L_2020_12_000000428717</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/73/71/CETATEXT000042737145.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 23/12/2020, 428717, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428717</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>Mme Cécile Chaduteau-Monplaisir</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:428717.20201223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° Sous le n° 428717, par une requête sommaire et un mémoire complémentaire, enregistrés les 11 mars et 6 juin 2019 au secrétariat du contentieux du Conseil d'Etat, la Fédération des syndicats des travailleurs du rail - Sud Rail demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision GRH00924 du directeur de la cohésion et des ressources humaines du groupe public ferroviaire du 15 janvier 2019 relative à la mise en oeuvre des dispositions de la loi du 21 août 2007 relative au dialogue social et à la continuité du service public ;<br/>
<br/>
              2°) de mettre à la charge de la SNCF, de SNCF Mobilités et de SNCF Réseau la somme de 6 000 euros au titre de l'article L.761-1 du code de justice administrative. <br/>
<br/>
<br/>
              2° Sous le n° 428867, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 15 mars et 17 juin 2019 et le 24 mars 2020, la Fédération nationale des travailleurs cadres et techniciens des chemins de fers français (Fédération CGT des cheminots) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision GRH00924 du directeur de la cohésion et des ressources humaines du groupe public ferroviaire du 15 janvier 2019 relative à la mise en oeuvre des dispositions de la loi du 21 août 2007 relative au dialogue social et à la continuité du service public ;<br/>
<br/>
              2°) de mettre à la charge de la SNCF la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
               - le code des transports ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... D..., maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme A... C..., rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la Fédération des syndicats des travailleurs du rail - Sud Rail, à la SCP Spinosi, Sureau, avocat de la société nationale des chemins de fer français et à la SCP Thouvenin, Coudray, Grevy, avocat de la Fédération nationale des travailleurs CGT des cheminots ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 1222-2 du code des transports : " Après consultation des usagers lorsqu'existe une structure les représentant, l'autorité organisatrice de transport définit les dessertes prioritaires en cas de perturbation prévisible du trafic. (...) ". Aux termes de l'article L. 1222-3 de ce code : " Pour assurer les dessertes prioritaires, l'autorité organisatrice de transports détermine différents niveaux de service en fonction de l'importance de la perturbation. (...) ". Aux termes de l'article L. 1222-4 du même code : " L'entreprise de transport élabore : / 1° Un plan de transports adapté aux priorités de desserte et aux niveaux de service définis par l'autorité organisatrice de transports, qui précise, pour chaque niveau de service, les plages horaires et les fréquences à assurer (...) ". Aux termes de l'article L. 1222-7 de ce code : " Dans les entreprises de transports, l'employeur et les organisations syndicales représentatives concluent un accord collectif de prévisibilité du service applicable en cas de perturbation prévisible du trafic. / L'accord collectif de prévisibilité du service recense, par métier, fonction et niveau de compétence ou de qualification, les catégories d'agents et leurs effectifs, ainsi que les moyens matériels, indispensables à l'exécution, conformément aux règles de sécurité en vigueur applicables à l'entreprise, de chacun des niveaux de service prévus dans le plan de transport adapté. / Il fixe les conditions dans lesquelles, en cas de perturbation prévisible, l'organisation du travail est révisée et les personnels disponibles réaffectés afin de permettre la mise en oeuvre du plan de transports adapté. En cas de grève, les personnels disponibles sont les personnels de l'entreprise non grévistes. / A défaut d'accord applicable, un plan de prévisibilité est défini par l'employeur. Un accord collectif de prévisibilité du service qui entre en vigueur s'applique en lieu et place du plan de prévisibilité (...) ". Aux termes de l'article L. 1324-7 du même code : " En cas de grève, les salariés relevant des catégories d'agents mentionnées dans l'accord collectif ou le plan de prévisibilité prévus à l'article L. 1222-7 informent, au plus tard quarante-huit heures avant de participer à la grève, le chef d'entreprise ou la personne désignée par lui de leur intention d'y participer. (...) ". Enfin, l'article L. 1324-8 de ce code prévoit qu'" Est passible d'une sanction disciplinaire le salarié qui n'a pas informé son employeur de son intention de participer à la grève dans les conditions prévues à l'article L. 1324-7. Cette sanction disciplinaire peut également être prise à l'encontre du salarié qui, de façon répétée, n'a pas informé son employeur de son intention de renoncer à participer à la grève ou de reprendre son service ".<br/>
<br/>
              2. Par deux requêtes qu'il y a lieu de joindre pour statuer par une même décision, la Fédération des syndicats des travailleurs du rail - Sud Rail et la Fédération CGT des cheminots demandent l'annulation pour excès de pouvoir de la décision du 15 janvier 2019 par laquelle le directeur de la cohésion et des ressources humaines du groupe public ferroviaire a modifié, en l'étendant de trois à seize catégories d'agents, la liste des salariés qui sont tenus de faire connaître à leur employeur, la SNCF, SNCF Réseau ou SNCF Mobilités, leur intention de participer à une grève, telle qu'elle résultait du plan de prévisibilité initialement défini par l'employeur le 30 mai 2008, sur le fondement de l'article L. 1222-7 du code des transports. Eu égard aux moyens qu'elles invoquent, elles doivent être regardées comme demandant l'annulation du seul 4. de cette décision, relatif à la déclaration individuelle d'intention.<br/>
<br/>
              Sur la légalité externe : <br/>
<br/>
              3. Il résulte des dispositions de l'article L. 1222-7 du code des transports qu'un plan de prévisibilité du service applicable en cas de perturbation prévisible du trafic ne peut être élaboré par l'employeur qu'à défaut de conclusion d'un accord avec les organisations syndicales représentatives. Par suite, l'employeur ne saurait modifier significativement le plan de prévisibilité élaboré par lui en l'absence d'accord collectif de prévisibilité du service qu'après avoir, de nouveau, engagé avec les organisations syndicales représentatives des négociations en vue de la signature d'un accord, sans qu'elles aient pu aboutir à la conclusion d'un tel accord. <br/>
<br/>
              4. Il ressort des pièces des dossiers et il n'est pas contesté que, préalablement à la modification du plan de prévisibilité, l'ensemble des organisations syndicales représentatives du personnel ont été conviées à une table ronde, le 18 décembre 2018, afin que leur soit soumis le projet d'extension de la liste, résultant jusqu'alors de la décision du directeur de la cohésion et des ressources humaines ferroviaires du 30 mai 2008, des catégories d'agents tenus à l'obligation de déclaration individuelle d'intention de participer à une grève. Contrairement à ce que soutiennent les fédérations requérantes, les organisations représentatives ont disposé d'informations et d'un délai suffisants pour se prononcer en toute connaissance de cause sur le projet, dont il ressort des pièces des dossiers qu'il n'a été modifié postérieurement à la table ronde, dans le projet soumis à leur signature, que sur des points débattus à cette occasion. Enfin, leur courrier du 4 janvier 2019, par lequel elles ont annoncé qu'elles ne signeraient pas l'accord proposé, a reçu une réponse motivée. Dans ces conditions, les fédérations requérantes ne sont pas fondées à soutenir que la décision attaquée procèderait d'une modification du plan de prévisibilité opérée unilatéralement par l'employeur sans qu'il ait préalablement engagé les négociations requises par l'article L. 1222-7 du code des transports en vue de parvenir sur ce point à un accord. <br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              En ce qui concerne la désignation des catégories d'agents soumis à l'obligation de déclaration individuelle :<br/>
<br/>
              5. Il résulte des dispositions citées au point 1 que l'article L. 1222-7 du code des transports impose que, dans les entreprises de transports, l'accord collectif de prévisibilité ou, à défaut d'un tel accord, le plan de prévisibilité défini par l'employeur, recense par métier, fonction et niveau de compétence ou de qualification, les catégories d'agents et leurs effectifs indispensables à l'exécution de chacun des niveaux de service, préalablement déterminés selon l'importance de la perturbation prévisible du trafic par l'autorité organisatrice de transports, en fonction desquels le plan de transport adapté à chacun de ces différents niveaux de service est élaboré par cette autorité en application de l'article L. 1222-4 du même code. Il en résulte également que les salariés soumis à l'obligation d'informer le chef d'entreprise ou la personne désignée par lui, au plus tard quarante-huit heures à l'avance, de leur intention de participer à une grève sont tous ceux relevant des catégories d'agents ainsi mentionnées dans l'accord collectif ou le plan de prévisibilité, cet acte étant le seul dont l'article L. 1324-7 du code des transports, combiné avec l'article L. 1222-7 auquel il renvoie, prévoit l'existence et auquel ces articles imposent de recenser les effectifs et les catégories d'agents indispensables à l'exécution de chacun des niveaux de service par métier, fonction et niveau de compétence ou de qualification.<br/>
<br/>
              6. Il ressort des pièces des dossiers que les dispositions en litige de la décision attaquée constituent, non le plan de prévisibilité dans son ensemble, mais seulement la partie de ce plan déterminant les catégories d'agents soumis à l'obligation de déclaration d'intention. Sauf à ce que les catégories n'aient pas été désignées avec une précision suffisante pour que la négociation puisse se tenir utilement, la circonstance que ces dispositions ne les rattachent pas elles-mêmes à un recensement par métier, fonction et niveau de compétence ou de qualification et n'en précisent pas les effectifs est sans incidence sur leur légalité. Il en résulte seulement que l'obligation de déclaration d'intention à laquelle sont soumis les agents relevant des catégories mentionnées au plan de prévisibilité, qui apporte un aménagement aux conditions d'exercice par ceux-ci de leur droit de grève et à défaut de laquelle ils sont passibles d'une sanction disciplinaire, ne saurait leur être opposée tant que ces catégories n'ont pas été précisées par métier, fonction et niveau de compétence ou de qualification, ainsi que l'impose l'article L. 1222-7 du code des transports. <br/>
<br/>
              7. En l'espèce, s'il n'est pas contesté que certaines des seize catégories d'agents soumis à déclaration individuelle d'intention, telles que celles les désignant par leur lieu d'affectation, ne sont pas mentionnées par métier, fonction et niveau de compétence ou de qualification, il ressort des pièces des dossiers que leur désignation sous cette forme n'a pas fait obstacle à l'identification des agents auxquelles elles se rapportaient et à leur examen lors de la négociation collective. <br/>
<br/>
              8. Par suite, les fédérations requérantes ne sont pas fondées à soutenir que le recensement des catégories d'agents tenus à l'obligation de déclaration individuelle d'intention aurait en l'espèce été opéré en méconnaissance de l'article L. 1222-7 du code des transports.<br/>
<br/>
              En ce qui concerne le bien-fondé des catégories d'agents soumis à l'obligation de déclaration d'intention :<br/>
<br/>
              9. Il résulte des dispositions citées au point 1 que ne peuvent être soumises à obligation de déclaration d'intention que les catégories d'agents indispensables à l'exécution des niveaux de service prévus dans le plan de transport adapté en cas de perturbation prévisible du trafic, le Conseil constitutionnel ayant, dans sa décision n° 2007-556 DC du 16 août 2007, précisé que cette obligation ne saurait être étendue à l'ensemble des salariés et jugé qu'elle n'était opposable qu'aux seuls salariés dont la présence détermine directement l'offre de services.<br/>
<br/>
              10. Si le plan de prévisibilité initialement défini le 30 mai 2008 ne mentionnait que les trois catégories des agents de conduite, des agents d'accompagnement des trains et des agents des postes d'aiguillage, il n'en résulte pas qu'aucune autre catégorie d'agents ne pourrait être regardée comme indispensable à l'exécution des niveaux de service prévus dans le plan de transport adapté. En l'espèce, contrairement à ce que soutiennent les fédérations requérantes, alors même que les dispositions attaquées conduisent à étendre de façon importante le nombre des agents soumis à une telle obligation, concernant désormais plus de la moitié des agents du groupe public ferroviaire et plus de deux tiers des agents de SNCF Mobilités, elles n'ont pas fait de l'article L. 1222-7 du code des transports une inexacte application en y ajoutant des catégories d'agents participant à l'information des usagers ainsi qu'à des activités de maintenance. Il en va de même des catégories d'agents qui exercent des missions en lien direct avec la circulation des trains, la circonstance que les agents de certaines de ces catégories soient aisément remplaçables étant sans incidence à cet égard, l'obligation de déclaration préalable ayant notamment pour objet de le permettre en vue d'exécuter le niveau de service prévu. Enfin, tel est le cas de la catégorie des agents des services internes de sécurité de la SNCF, dont la présence est nécessaire pour assurer la sécurité des voyageurs. Par suite, les fédérations requérantes ne sont pas fondées à soutenir que l'ajout de ces catégories méconnaît l'article L. 1222-7 du code des transports. <br/>
<br/>
              En ce qui concerne les modalités de la déclaration individuelle d'intention de participer à une grève :<br/>
<br/>
              11. D'une part, si les dispositions critiquées laissent aux établissements la faculté de proposer au choix des agents deux seulement des cinq modalités de déclaration individuelle d'intention qu'elles prévoient, il n'en résulte aucune méconnaissance du principe d'égalité. <br/>
<br/>
              12. D'autre part, aux termes de l'article L. 1324-7 du code des transports, dont il revient à l'employeur d'imposer le respect à un éventuel sous-traitant : " Les informations issues de ces déclarations individuelles ne peuvent être utilisées que pour l'organisation du service durant la grève. Elles sont couvertes par le secret professionnel. Leur utilisation à d'autres fins ou leur communication à toute personne autre que celles désignées par l'employeur comme étant chargées de l'organisation du service est passible des peines prévues à l'article 226-13 du code pénal ". En ce qu'elle prévoit que, lorsque la déclaration par appel téléphonique est l'une des modalités retenues par l'établissement, cet appel est pris en charge par une plate-forme téléphonique, qui a pour seule mission de recevoir cette déclaration des agents auxquels la date et l'heure de leur prise de service auront été communiqués, non par cette plate-forme, mais par leur employeur, la décision attaquée ne méconnaît pas les dispositions de l'article L. 1324-7 du code des transports et n'est entachée d'aucune erreur manifeste d'appréciation. <br/>
<br/>
              13. Il résulte de tout ce qui précède que les fédérations requérantes ne sont pas fondées à demander l'annulation pour excès de pouvoir de la décision du 15 janvier 2019.<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              14. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la SNCF, de SNCF Voyageurs ou de SNCF Réseau, qui ne sont pas, dans la présente instance, les parties perdantes. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge des fédérations requérantes une somme au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes de la Fédération des syndicats des travailleurs du rail - Sud Rail et de la Fédération CGT des cheminots sont rejetées.<br/>
Article 2 : Les conclusions présentées par la SNCF, SNCF Voyageurs et SNCF Réseau au titre de l'article L.761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la Fédération des syndicats des travailleurs du rail - Sud Rail, à la Fédération CGT des cheminots et à la SNCF, premier défendeur dénommé, pour l'ensemble des défendeurs.<br/>
Copie en sera adressée à la ministre du travail, de l'emploi et de l'insertion. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
