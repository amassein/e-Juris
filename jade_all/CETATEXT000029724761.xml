<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029724761</ID>
<ANCIEN_ID>JG_L_2014_11_000000383587</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/72/47/CETATEXT000029724761.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 07/11/2014, 383587</TITRE>
<DATE_DEC>2014-11-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383587</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>FOUSSARD ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Natacha Chicot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:383587.20141107</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et les mémoires complémentaires, enregistrés les 8, 18 et 28 août 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour le ministre des finances et des comptes publics ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1411103/3-5 du 24 juillet 2014 par laquelle le juge des référés du tribunal administratif de Paris, statuant en application de l'article L. 551-1 du code de justice administrative, a, à la demande de la société BearingPoint France, enjoint au service des achats de l'Etat de reprendre la procédure d'attribution du lot n° 2 d'un accord-cadre relatif à la réalisation de prestations d'assistance à maîtrise d'ouvrage de projets informatiques, tierce recette applicative et assistance sur les logiciels libres en y incluant l'offre de la société BearingPoint France ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société BearingPoint France ;<br/>
<br/>
              3°) de mettre à la charge de la société BearingPoint France le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des marchés publics ;<br/>
<br/>
              Vu l'arrêté du 14 décembre 2009 relatif à la dématérialisation des procédures de passation des marchés publics ;<br/>
<br/>
              Vu l'arrêté du 11 octobre 2012 portant création d'un traitement dénommé " plate-forme des achats de l'Etat " ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Natacha Chicot, auditeur,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Foussard, avocat du ministre des finances et des comptes publics, et à la SCP Piwnica, Molinié, avocat de la société BearingPoint France ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte des dispositions de l'article L. 551-1 du code de justice administrative que le président du tribunal administratif ou le magistrat qu'il délègue peut être saisi, avant la conclusion d'un contrat de commande publique ou de délégation de service public, d'un manquement, par le pouvoir adjudicateur, à ses obligations de publicité et de mise en concurrence ; qu'aux termes de l'article L. 551-2 du même code : " Le juge peut ordonner à l'auteur du manquement de se conformer à ses obligations et suspendre l'exécution de toute décision qui se rapporte à la passation du contrat, sauf s'il estime, en considération de l'ensemble des intérêts susceptibles d'être lésés et notamment de l'intérêt public, que les conséquences négatives de ces mesures pourraient l'emporter sur leurs avantages. / Il peut, en outre, annuler les décisions qui se rapportent à la passation du contrat et supprimer les clauses ou prescriptions destinées à figurer dans le contrat et qui méconnaissent lesdites obligations " ; qu'enfin, aux termes de l'article L. 551-10 de ce code : " Les personnes habilitées à engager les recours prévus aux articles L. 551-1 et L. 551-5 sont celles qui ont un intérêt à conclure le contrat et qui sont susceptibles d'être lésées par le manquement invoqué, ainsi que le représentant de l'Etat dans le cas où le contrat doit être conclu par une collectivité territoriale ou un établissement public local. (...) " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Paris que, par un avis d'appel public à la concurrence publié le 5 octobre 2013, le service des achats de l'Etat, service à compétence nationale rattaché au ministre en charge du budget, a lancé une procédure d'appel d'offres restreint en vue de la conclusion d'un accord-cadre portant sur la " réalisation de prestations d'assistance à maîtrise d'ouvrage de projets informatiques, tierce recette applicative et assistance sur les logiciels " constitué de quatre lots ; que le dépôt des candidatures et des offres s'effectuait exclusivement sur la plateforme interministérielle de dématérialisation des achats de l'Etat dite " PLACE " ; que, par une décision du 24 juin 2014, l'offre de la société BearingPoint France, candidate au titre du lot n° 2, a été rejetée comme irrégulière au motif que l'acte d'engagement dématérialisé qu'elle avait déposé sur la plateforme PLACE n'était pas assorti d'une signature électronique ; que le ministre des finances et des comptes publics se pourvoit en cassation contre l'ordonnance du 24 juillet 2014 par laquelle le juge des référés du tribunal administratif de Paris, saisi par la société BearingPoint France sur le fondement de l'article L. 551-1 du code de justice administrative, a enjoint au service des achats de l'Etat de reprendre la procédure d'attribution du lot n° 2 en y incluant l'offre de la société BearingPoint France, au motif que le pouvoir adjudicateur ne pouvait, pour le seul motif tiré de l'absence de signature électronique de l'acte d'engagement de la société BearingPoint France, estimer son offre irrecevable ; <br/>
<br/>
              3. Considérant qu'aux termes du III de l'article 53 du code des marchés publics : " Les offres inappropriées, irrégulières et inacceptables sont éliminées (...) " ; qu'aux termes de l'article 11 de ce code : " (...) Pour les marchés passés selon les procédures formalisées, l'acte d'engagement et, le cas échéant, les cahiers des charges en sont les pièces constitutives. / L'acte d'engagement est la pièce signée par un candidat à un accord-cadre ou à un marché public dans laquelle le candidat présente son offre ou sa proposition (...) " ; qu'aux termes de l'article 48 du même code : " I - Les offres sont présentées sous la forme de l'acte d'engagement défini à l'article 11. / L'acte d'engagement pour un marché ou un accord-cadre passé selon une procédure formalisée, lorsque l'offre est transmise par voie électronique, est signé électroniquement dans des conditions fixées par arrêté du ministre chargé de l'économie. / (...) " ; qu'aux termes du IV de l'article 56 du même code : " Dans les cas où la transmission électronique est obligatoire et dans ceux où elle est une faculté donnée aux candidats, le pouvoir adjudicateur assure la confidentialité et la sécurité des transactions sur un réseau informatique accessible de façon non discriminatoire, selon des modalités fixées par arrêté du ministre chargé de l'économie " ; qu'aux termes de l'article 5 de l'arrêté du 14 décembre 2009 relatif à la dématérialisation des procédures de passation des marchés publics : " Le dépôt des candidatures et des offres transmises par voie électronique ou sur support physique électronique donne lieu à un accusé de réception indiquant la date et l'heure de réception " ; qu'enfin, l'article 3.7.7. du guide utilisateur de la plate-forme PLACE, auquel renvoyait le règlement de la consultation, précise que l'accusé de réception électronique, qui constitue la preuve de dépôt du dossier opposable par le soumissionnaire, indique " chaque fichier transmis, son poids, ainsi que le nom du jeton de signature associé le cas échéant et son poids " ;<br/>
<br/>
              4. Considérant, en premier lieu, qu'il résulte de ces dispositions qu'une offre dont l'acte d'engagement n'est pas, avant la date limite de remise des offres, signé par une personne dûment mandatée ou habilitée à engager l'entreprise candidate est irrégulière et doit être éliminée comme telle avant même d'être examinée ; que le juge des référés du tribunal administratif de Paris ne pouvait ainsi, sans commettre d'erreur de droit, juger qu'à défaut de signature électronique de l'acte de d'engagement, la signature électronique des autres documents composant l'offre de la société BearingPoint France suffisait à établir l'engagement juridique de cette société ; <br/>
<br/>
              5. Considérant, en second lieu, qu'il est constant que l'accusé de réception transmis à la société BearingPoint France ne mentionnait aucun " jeton " de signature associé à l'acte d'engagement ; qu'en jugeant, dans ces conditions, que cette absence de mention ne suffisait pas à établir que l'acte d'engagement de la société requérante n'était pas accompagné de sa signature électronique, au motif que n'étaient démontrés, ni l'absence de dysfonctionnement de la plateforme PLACE ni l'existence d'un dispositif signalant aux candidats le défaut de signature électronique de leurs documents, le juge des référés du tribunal administratif de Paris a également commis une erreur de droit ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'ordonnance attaquée doit être annulée ;<br/>
<br/>
              7. Considérant que, dans les circonstances de l'espèce, il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée par la société BearingPoint France ;<br/>
<br/>
              8. Considérant qu'il résulte de l'instruction et n'est d'ailleurs pas contesté que la société BearingPoint France a reçu le 17 mars à 14 h 19 un message électronique, adressé par la plate-forme PLACE, attestant du dépôt de son offre sur cette plateforme aux mêmes date et heure ; qu'il ressort de ce document qui, conformément aux dispositions citées au point 3, précise la nature des fichiers enregistrés et constitue la preuve de leur dépôt par les candidats, que si l'acte d'engagement a bien été enregistré sur la plate-forme, aucune mention du nom et du poids du " jeton " de signature associé ne figurait dans la liste des documents dont il était accusé réception ; que la société BearingPoint France qui a donc eu connaissance, après le dépôt de son offre, de ce que l'engagement juridique enregistré sur la plate-forme n'était pas accompagné de sa signature électronique et pouvait ainsi, le cas échéant, décider de compléter son offre avant la date limite de remise des offres, soit le 17 mars 2014 à 17 h 00, ne peut, dès lors, utilement soutenir que cette absence résulterait d'un dysfonctionnement de la plate-forme ; qu'elle ne peut pas non plus utilement soutenir qu'elle n'aurait pas été informée de l'absence de signature électronique de l'acte d'engagement par un dispositif d'alerte spécifique, dès lors qu'en tout état de cause, ni les dispositions de l'article 56 du code des marchés publics ni les documents de la consultation ne prévoyaient la mise en place d'un tel dispositif ;  <br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que la société BearingPoint France n'est pas fondée à demander l'annulation de la procédure de passation du marché litigieux ; que, par suite, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative doivent être rejetées ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à sa charge, sur le fondement des mêmes dispositions, le versement à l'Etat d'une somme de 3 000 euros au titre de la procédure conduite devant le Conseil d'Etat ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 24 juillet 2014 du juge des référés du tribunal administratif de Paris est annulée.<br/>
Article 2 : La demande présentée par la société BearingPoint France devant le juge des référés du tribunal administratif de Paris ainsi que ses conclusions présentées devant le Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La société BearingPoint France versera à l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée au ministre des finances et des comptes publics et à la société BearingPoint France.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-02-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. FORMALITÉS DE PUBLICITÉ ET DE MISE EN CONCURRENCE. - UTILISATION D'UNE PLATE-FORME ÉLECTRONIQUE POUR LE DÉPÔT DES OFFRES - ABSENCE DE SIGNATURE ÉLECTRONIQUE DE L'ACTE D'ENGAGEMENT D'UN CANDIDAT - 1) MOYEN TIRÉ D'UN DYSFONCTIONNEMENT DE LA PLATE-FORME - INOPÉRANCE DÈS LORS QUE LA PLATE-FORME A ENVOYÉ UN MESSAGE DONT IL RÉSULTAIT QU'AUCUNE SIGNATURE ÉLECTRONIQUE N'AVAIT ÉTÉ ENREGISTRÉE - 2) OBLIGATION DE METTRE EN PLACE UN DISPOSITIF SPÉCIFIQUE D'ALERTE DES CANDIDATS SUR L'ABSENCE D'ENREGISTREMENT D'UNE SIGNATURE ÉLECTRONIQUE - ABSENCE.
</SCT>
<ANA ID="9A"> 39-02-005 1) Ayant reçu le 17 mars à 14h19 un message électronique, adressé par la plate-forme électronique de dépôt des offres, qui attestait du dépôt de son offre, précisait la nature des fichiers enregistrés et ne comportait aucune mention du nom et du poids du  jeton  de signature associé dans la liste des documents dont il était accusé réception, le candidat, qui a donc eu connaissance, après le dépôt de son offre, de ce que l'engagement juridique enregistré sur la plate-forme n'était pas accompagné de sa signature électronique et pouvait ainsi, le cas échéant, décider de compléter son offre avant la date limite de remise des offres, soit le 17 mars 2014 à 17h00, ne peut, dès lors, utilement soutenir que cette absence résulterait d'un dysfonctionnement de la plate-forme.... ,,2) Le candidat ne peut pas non plus utilement soutenir qu'il n'aurait pas été informé de l'absence de signature électronique de l'acte d'engagement par un dispositif d'alerte spécifique, dès lors qu'en tout état de cause, ni les dispositions de l'article 56 du code des marchés publics ni les documents de la consultation ne prévoyaient la mise en place d'une tel dispositif.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
