<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000007850362</ID>
<ANCIEN_ID>JGXLX1994X07X0000024078</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/07/85/03/CETATEXT000007850362.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'Etat, 7 /10 SSR, du 29 juillet 1994, 124078, inédit au recueil Lebon</TITRE>
<DATE_DEC>1994-07-29</DATE_DEC>
<JURIDICTION>Conseil d'Etat</JURIDICTION>
<NUMERO>124078</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7 /10 SSR</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Lambron</RAPPORTEUR>
<COMMISSAIRE_GVT>Lasvignes</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>    Vu la requête enregistrée le 15 mars 1991 au secrétariat du Contentieux du Conseil d'Etat, présentée pour la CAISSE DES ECOLES D'EPINAY-SUR-SEINE, dont le siège est à l'hôtel de ville d'Epinay-sur-Seine (93806), représentée par le maire de ladite ville agissant comme président de la caisse ; la CAISSE DES ECOLES D'EPINAY-SUR-SEINE demande au Conseil d'Etat :<br/>    1°) d'annuler le jugement du 29 juin 1990 par lequel le tribunal administratif de Paris a, sur déféré du préfet de la Seine-Saint-Denis, annulé deux marchés passés par la CAISSE DES ECOLES D'EPINAY-SUR-SEINE avec la société "La Normandie à Paris" ainsi que les délibérations du 12 novembre 1988 du comité de la caisse approuvant lesdits marchés ;<br/>    2°) de rejeter le déféré présenté par le préfet de la Seine-Saint-Denis devant le tribunal administratif de Paris ;<br/>
<br/>    Vu les autres pièces du dossier ;<br/>    Vu la loi n°82-213 du 2 mars 1982 modifiée ;<br/>    Vu le code des marchés publics ;<br/>    Vu le code des tribunaux administratifs et des cours administratives d'appel ;Vu l'ordonnance n° 45-1708 du 31 juillet 1945, le décret n° 53-934 du 30 septembre 1953 et la loi n° 87-1127 du 31 décembre 1987 ;<br/>    Après avoir entendu en audience publique :<br/>    - le rapport de M. Lambron, Maître des Requêtes,<br/>    - les observations de la SCP Masse-Dessen, Georges, Thouvenin, avocat de la CAISSE DES ECOLES D'EPINAY-SUR-SEINE,<br/>    - les conclusions de M. Lasvignes, Commissaire du gouvernement ;<br/>
<br/>    Considérant qu'aux termes de l'article 3 de la loi n°82-213 du 2 mars 1982 relative aux droits et libertés des communes, des départements et des régions : "Le représentant de l'Etat dans le département défère au tribunal administratif les actes mentionnés au paragraphe II de l'article précédent qu'il estime contraire à la légalité dans les deux mois suivants leur transmission" ;<br/>    Considérant qu'il ressort des pièces du dossier que le Préfet de la Seine-Saint-Denis a adressé le 7 février 1989 au maire d'Epinay-sur-Seine, président de la caisse des écoles de cette commune, une lettre par laquelle il lui exposait d'une part que les délibérations du comité de ladite caisse en date du 12 décembre 1988 acceptant la passation de deux marchés négociés avec la société "La Normandie à Paris" étaient entachées d'illégalité, et lui demandait d'autre part de rapporter ces délibérations et de résilier les marchés correspondants ; qu'en l'absence de dispositions législatives ou réglementaires organisant une procédure particulière en la matière, cette demande devait être regardée comme constituant un recours gracieux qui, ayant été formé dans le délai du recours contentieux, a interrompu ce délai ; que par lettre en date du 10 avril 1989 reçue en préfecture le 13 avril 1989, le président de la CAISSE DES ECOLES D'EPINAY-SUR-SEINE a rejeté le recours gracieux ainsi formé ; qu'ainsi le déféré du préfet de la Seine-Saint-Denis enregistré au greffe du tribunal administratif de Paris le 13 juin 1989, soit avant l'expiration du nouveau délai de recours contentieux que le rejet du recours gracieux avait fait courir, n'était pas tardif ; que c'est dès lors à bon droit que les premiers juges l'ont estimé recevable ;<br/>
<br/>    Considérant qu'aux termes de l'article 309 du code des marchés publics : "Les collectivités locales et leurs établissements publics peuvent conclure des marchés négociés pour des travaux, fournitures ou services dont la valeur n'excède pas, pour le montant total de l'opération, des seuils fixés pour chaque catégorie, par arrêté conjoint du ministre de l'économie et des finances et des ministres de tutelle" ; qu'aux termes de l'arrêté du 7 janvier 1982, le seuil au-dessous duquel les collectivités locales peuvent conclure des marchés négociés est fixé à 350 000 F ; qu'il résulte des pièces du dossier que les deux marchés en litige, d'un montant de 350 000 F chacun, ont été conclus à la même date avec la même entreprise, en vue de l'approvisionnement en produits laitiers des établissements scolaires d'Epinay-sur-Seine ; que dans ces conditions, alors même que ces deux marchés portaient sur des produits lactés susceptibles d'être distribués à différentes catégories d'élèves et qu'ils faisaient l'objet d'une imputation sur des chapitres différents du budget de la caisse des écoles, il s'agissait sous l'apparence de marchés distincts de la réalisation d'une même opération dont le montant global s'élevait à 700 000 F, dépassant ainsi le seuil prévu par les dispositions précitées de l'article 309 du code des marchés publics ; que, dès lors, le président de la CAISSE DES ECOLES D'EPINAY-SUR-SEINE n'est pas fondé à soutenir que c'est à tort que, par le jugement attaquéen date du 29 juin 1990, le tribunal administratif de Paris a annulé les marchés négociés passés avec la société "La Normandie à Paris" pour la fourniture de produits laitiers à la CAISSE DES ECOLES D'EPINAY-SUR-SEINE, ainsi que les délibérations susvisées du 12 décembre 1988 approuvant lesdits marchés ;<br/>Article 1er : La requête de la CAISSE DES ECOLES D'EPINAY-SUR-SEINE est rejetée.<br/>Article 2 : La présente décision sera notifiée à la CAISSE DES ECOLES D'EPINAY-SUR-SEINE, au préfet de la Seine-Saint-Denis et au ministre d'Etat, ministre de l'intérieur et de l'aménagement du territoire.<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8AA" TYPE="PRINCIPAL">135-02-01-02 COLLECTIVITES LOCALES - QUESTIONS COMMUNES ET COOPERATION - CONTROLE DE LA LEGALITE DES ACTES DES AUTORITES LOCALES - DEFERE PREFECTORAL - DELAI DU DEFERE</SCT>
<SCT ID="8BA" TYPE="PRINCIPAL">39-02-02-05 MARCHES ET CONTRATS ADMINISTRATIFS - FORMATION DES CONTRATS ET MARCHES - MODE DE PASSATION DES CONTRATS - MARCHE NEGOCIE</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS>
<LIEN cidtexte="" datesignatexte="" id="" naturetexte="" nortexte="" num="" numtexte="" sens="source" typelien="CITATION">Arrêté 1982-01-07</LIEN>
<LIEN cidtexte="" datesignatexte="" id="" naturetexte="" nortexte="" num="" numtexte="" sens="source" typelien="CITATION">Code des marchés publics 309</LIEN>
<LIEN cidtexte="" datesignatexte="" id="" naturetexte="" nortexte="" num="" numtexte="" sens="source" typelien="CITATION">Loi 82-213 1982-03-02 art. 3</LIEN>
</LIENS>
</TEXTE_JURI_ADMIN>
