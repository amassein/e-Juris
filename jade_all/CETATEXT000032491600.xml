<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032491600</ID>
<ANCIEN_ID>JG_L_2016_05_000000383699</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/49/16/CETATEXT000032491600.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 04/05/2016, 383699, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383699</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP DE CHAISEMARTIN, COURJON ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:383699.20160504</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. et Mme A...B... ont demandé au tribunal administratif de Montreuil d'annuler pour excès de pouvoir la décision du 16 octobre 2012 par laquelle le maire de Gournay-sur-Marne a rejeté leur demande tendant au retrait du permis de construire délivré le 24 mars 2011 à la SARL Montoit Immobilier. Par un jugement n° 1210222 du 17 octobre 2013, le tribunal administratif de Montreuil a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 13VE03713 du 19 juin 2014, la cour administrative d'appel de Versailles a, sur appel de M. et MmeB..., annulé ce jugement et enjoint au maire de Gournay-sur-Marne de retirer l'arrêté litigieux et d'ordonner l'arrêt des travaux entrepris sur le fondement du permis de construire délivré à la SARL Montoit Immobilier.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              1°, sous le n° 383699, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 14 août et 13 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, la SARL Montoit Immobilier demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. et MmeB....<br/>
<br/>
<br/>
              2°, sous le n° 384220, par un pourvoi sommaire et deux mémoires complémentaires, enregistrés les 4 septembre 2014, 4 décembre 2014 et 18 février 2015 au secrétariat du contentieux du Conseil d'Etat, la commune de Gournay-sur-Marne demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. et MmeB....<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Chaisemartin, Courjon, avocat de la SARL Montoit Immobilier, à la SCP Foussard, Froger, avocat de M. et Mme A...B... et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la commune de Gournay-sur-Marne ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que les pourvois de la SARL Montoit Immobilier et de la commune de Gournay-sur-Marne sont dirigés contre le même arrêt ; qu'il y a lieu de les joindre pour statuer par une même décision ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par arrêté du 24 mars 2011, affiché en mairie et sur le terrain à compter du 31 mai 2011, le maire de Gournay-sur-Marne a accordé un permis de construire à la SARL Montoit Immobilier ; que, d'une part, le recours gracieux formé par Mme B... contre ce permis le 13 avril 2011 a été rejeté par une décision du maire du 22 avril 2011, qui est devenue définitive ; que, d'autre part, la demande d'annulation pour excès de pouvoir de ce permis de construire présentée par M. B... devant le juge administratif a été rejetée pour tardiveté par arrêt de la cour administrative d'appel de Versailles du 10 avril 2014 au motif que le délai de recours contentieux avait commencé à courir le 31 mars 2011 et que le recours gracieux formé Mme B... n'avait pu proroger le délai de recours contentieux à l'encontre du permis litigieux, faute d'avoir fait l'objet, dans les délais, de la notification prévue à l'article R. 600-1 du code de l'urbanisme ;<br/>
<br/>
              3. Considérant qu'en l'absence de toute modification dans les circonstances de fait ou dans la réglementation d'urbanisme applicable, la décision du maire de Gournay-sur-Marne du 16 octobre 2012 rejetant le recours gracieux de M. et Mme B... lui demandant à nouveau de retirer le permis de construire, dont ni l'objet ni la cause juridique n'étaient différents de ceux de la demande initiale, avait le caractère d'une décision purement confirmative de sa décision du 22 avril 2011 ; qu'elle n'a, dès lors, pu avoir pour effet de rouvrir le délai de recours contentieux ; qu'il suit de là que la cour administrative d'appel de Versailles a entaché son arrêt d'une erreur de droit en jugeant qu'elle avait le caractère d'une décision faisant grief et que, par suite, le demande tendant à son annulation était recevable ; que son arrêt doit être annulé pour ce motif, sans qu'il soit besoin de statuer sur les autres moyens du pourvoi ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant que, comme il a été dit ci-dessus, la décision du maire de Gournay-sur-Marne du 16 octobre 2012 rejetant le recours gracieux de M. et Mme B... lui demandant à nouveau de retirer le permis de construire, dont ni l'objet ni la cause juridique n'étaient différents de ceux de la demande initiale, présente le caractère d'une décision purement confirmative de sa décision du 22 avril 2011 devenue définitive ; qu'il s'ensuit que M. et Mme B... ne sont pas fondés à se plaindre de ce que, par le jugement attaqué, le tribunal administratif de Montreuil a rejeté leur demande tendant à l'annulation de la décision du 16 octobre 2012 ;<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la commune de Gournay-sur-Marne et de la SARL Montoit Immobilier, qui ne sont pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 19 juin 2014 est annulé.<br/>
<br/>
Article 2 : La requête présentée par M. et Mme B... devant la cour administrative d'appel de Versailles ainsi que leurs conclusions présentées devant le Conseil d'Etat et tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. et Mme A...B..., à la SARL Montoit Immobilier et à la commune de Gournay-sur-Marne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
