<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039655792</ID>
<ANCIEN_ID>JG_L_2019_12_000000423020</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/65/57/CETATEXT000039655792.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 20/12/2019, 423020, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-12-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423020</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>Mme Christelle Thomas</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Anne Iljic</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:423020.20191220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme B... A... ont demandé au tribunal administratif de Paris de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles ils ont été assujettis au titre de l'année 2007 et des pénalités correspondantes. Par un jugement n° 1314357 du 5 janvier 2016, le tribunal administratif de Paris a prononcé la réduction, en droits et en pénalités, des impositions résultant de la taxation de la somme de 3 672 248 euros correspondant à la fraction de la plus-value réalisée par M. A... le 29 mai 2007, dans la catégorie des revenus de capitaux mobiliers, et a rejeté le surplus des conclusions de la demande de M. et Mme A....<br/>
<br/>
              Par un arrêt n° 16PA00892 du 12 avril 2018, la cour administrative d'appel de Paris, sur appel de M. et Mme A... :<br/>
<br/>
              1°) les a déchargés des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles ils ont été assujettis au titre de l'année 2007, restant en litige, ainsi que des pénalités correspondantes, à raison de la taxation dans la catégorie des traitements et salaires du gain réalisé correspondant aux parts de la société CDA apportées directement par M. A... à la société civile Marin de Caumartin et de la taxation selon le régime des plus-values de cession de valeurs mobilières, sur le fondement des dispositions de l'article 150-0 A du code général des impôts, du gain correspondant aux parts de la société CDA apportées par la société Marin des Tilleuls à la société Marin de Caumartin ;<br/>
<br/>
              2°) a réformé le jugement du tribunal administratif de Paris du 5 janvier 2016 en ce qu'il a de contraire à cet arrêt ;<br/>
<br/>
              3°) a rejeté le surplus des conclusions de M. et Mme A....<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 8 août et 8 novembre 2018 et le 19 juin 2019 au secrétariat du contentieux du Conseil d'Etat, M. et Mme A... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Christelle Thomas, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Anne Iljic, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de M. et Mme A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. et Mme A... ont créé, le 15 avril 2007, la société civile Marin des Tilleuls, qui relève du régime d'imposition des sociétés de personnes et qui a pour objet la constitution et la gestion d'un portefeuille de valeurs mobilières. Le même jour, ils ont constitué la société civile Marin de Caumartin qui a le même objet social et qui a opté pour son assujettissement à l'impôt sur les sociétés. Le 3 mai 2007, M. A... a cédé 396 000 titres qu'il détenait de la société Compagnie de l'Audon (CDA) à la société civile Marin des Tilleuls, laquelle les a immédiatement apportés à la société Marin de Caumartin. M. A... a également fait directement apport à la société Marin de Caumartin de 154 080 titres de la société CDA. M. et Mme A... ont reçu en contrepartie des titres de la société Marin de Caumartin. Les plus-values d'un montant total de 10 492 138 euros constatées lors de ces opérations d'apport ont été placées automatiquement sous le régime du sursis d'imposition prévu par l'article 150-0 B du code général des impôts. Le 29 mai 2007, la société CDA a procédé au rachat de ses propres titres auprès de la société Marin de Caumartin pour un prix identique à leur valeur d'apport. A l'issue d'un contrôle sur pièces du dossier fiscal de M. et Mme A..., l'administration fiscale a considéré que les apports des titres de la société CDA à la société Marin de Caumartin, préalablement au rachat de ses propres titres par la société CDA, avaient eu pour seul objet d'éviter l'imposition immédiate que M. et Mme A... auraient dû supporter si, à défaut d'interposition de la société Marin de Caumartin, M. A... avait cédé lui-même directement à la société CDA les 550 080 titres de cette société qu'il détenait initialement. L'administration fiscale a, pour ce motif, remis en cause le bénéfice du sursis d'imposition en mettant en oeuvre la procédure de répression des abus de droit prévue à l'article L. 64 du livre des procédures fiscales. Elle a, par ailleurs, estimé que le gain correspondant au montant des plus-values d'apport constituait, pour partie, un complément de salaire accordé à M. A... à raison de ses fonctions dans le groupe Wendel et l'a taxé à concurrence de 65 % de son montant total dans la catégorie des traitements et salaires et, pour le surplus, dans celle des revenus de capitaux mobiliers. M. et Mme A... ont, en conséquence de cette rectification, été assujettis à des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales au titre de l'année 2007, assorties de la majoration de 80 % pour abus de droit prévue au b) de l'article 1729 du code général des impôts. Par un jugement en date du 5 janvier 2016, le tribunal administratif de Paris, faisant partiellement droit à la demande de M. et Mme A..., a prononcé la réduction des impositions et pénalités mises à leur charge, au motif que la fraction des plus-values imposée dans la catégorie des revenus de capitaux mobiliers devait être taxée selon le régime des plus-values de cession de valeurs mobilières et a rejeté le surplus des conclusions de leur demande. Par un arrêt du 12 avril 2018, la cour administrative d'appel de Paris a prononcé la décharge des impositions et pénalités restant en litige à raison de la taxation dans la catégorie des traitements et salaires de la part du gain résultant des titres apportés directement par M. A... à la société Marin de Caumartin, et dans la catégorie des plus-values de cession de valeurs mobilières, de la part du gain résultant des titres apportés par la société Marin des Tilleuls à la société Marin de Caumartin. M. et Mme A... se pourvoient en cassation contre cet arrêt. Par la voie du pourvoi incident, le ministre de l'action et des comptes publics demande l'annulation de cet arrêt en tant qu'il a statué sur l'imposition de la part du gain résultant des titres apportés par la société Marin des Tilleuls à la société Marin de Caumartin.<br/>
<br/>
              2. L'article R. 611-1 du code de justice administrative, dans sa rédaction alors applicable, dispose que : " La requête et les mémoires, ainsi que les pièces produites par les parties, sont déposés ou adressés au greffe. / La requête, le mémoire complémentaire annoncé dans la requête et le premier mémoire de chaque défendeur sont communiqués aux parties avec les pièces jointes dans les conditions prévues aux articles R. 611-3, R. 611-5 et R. 611-6. / Les répliques, autres mémoires et pièces sont communiqués s'ils contiennent des éléments nouveaux ". Aux termes de l'article R. 611-7 du même code, dans sa rédaction alors applicable : " Lorsque la décision lui paraît susceptible d'être fondée sur un moyen relevé d'office, le président de la formation de jugement ou, au Conseil d'Etat, la chambre chargée de l'instruction en informe les parties avant la séance de jugement et fixe le délai dans lequel elles peuvent, sans qu'y fasse obstacle la clôture éventuelle de l'instruction, présenter leurs observations sur le moyen communiqué ". <br/>
<br/>
              3. Il ressort des pièces de la procédure devant la cour que, le 25 janvier 2018, la cour administrative d'appel a communiqué aux parties un moyen d'ordre public relatif à la catégorie d'imposition de la somme en litige en indiquant que les parties pouvaient présenter leurs observations sur ce moyen jusqu'à l'audience fixée au 1er février 2018. Dans ses observations en réponse du 31 janvier 2018, le ministre de l'action et des comptes publics ne se bornait pas à répondre au moyen d'ordre public soulevé par la cour mais sollicitait, si la cour entendait y faire droit, une substitution de base légale. Si la cour n'était pas tenue de communiquer aux parties les observations produites à la suite de la communication du moyen qu'elle avait relevé d'office, il en allait différemment si elle entendait faire droit à la demande de substitution de base légale sollicitée par le ministre à cette occasion. En accueillant cette demande, alors qu'elle n'a communiqué aux requérants le mémoire du ministre qui la formulait que le 31 janvier tout en maintenant l'audience publique à la date du 1er février, la cour a méconnu le principe du contradictoire et a ainsi rendu son arrêt au terme d'une procédure irrégulière. Dès lors, et sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, M. et Mme A... sont fondés à demander l'annulation de l'arrêt qu'ils attaquent. Par suite, le pourvoi incident du ministre de l'action et des comptes publics a perdu son objet et il n'y a pas lieu d'y statuer.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. et Mme A... au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                            --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 12 avril 2018 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : Il n'y a pas lieu de statuer sur le pourvoi incident du ministre de l'action et des comptes publics. <br/>
Article 4 : L'Etat versera à M. et Mme A... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à M. et Mme B... A... et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
