<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039799823</ID>
<ANCIEN_ID>JG_L_2020_01_000000434430</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/79/98/CETATEXT000039799823.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 08/01/2020, 434430, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-01-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434430</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP BOULLOCHE ; SCP COUTARD, MUNIER-APAIRE ; CORLAY</AVOCATS>
<RAPPORTEUR>Mme Mélanie Villiers</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:434430.20200108</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La communauté d'agglomération du Grand Angoulême a demandé au juge des référés du tribunal administratif de Poitiers, sur le fondement des dispositions de l'article R. 541-1 du code de justice administrative, d'une part, de condamner solidairement les sociétés Fra Architectes, OTEIS, Bureau Alpes contrôles, ALM Allain, BG2C et Longeville, à lui verser, à titre de provision, la somme de 149 457,45 euros toutes taxes comprises, ainsi que les intérêts au taux légal, en réparation des désordres affectant sa médiathèque, d'autre part, de mettre à la charge solidaire des mêmes sociétés la somme de 6 112,32 euros au titre des frais d'expertise toutes taxes comprises. Par un jugement n° 1700562 du 19 décembre 2018, le tribunal administratif de Poitiers a rejeté sa demande. <br/>
<br/>
              Par une ordonnance n° 19BX00002 du 23 août 2019, le juge des référés de la cour administrative d'appel de Bordeaux a, sur appel de la communauté d'agglomération du Grand Angoulême, annulé ce jugement, condamné la société Fra Architectes à lui verser la somme de 2 768,24 euros à titre de provision et rejeté le surplus des conclusions des parties. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 9 septembre et 23 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, la communauté d'agglomération du Grand Angoulême demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance en tant qu'elle a rejeté le surplus de ses conclusions d'appel ;<br/>
<br/>
              2°) statuant en référé, de faire droit au surplus de ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge solidaire des sociétés Fra Architectes, OTEIS, Bureau Alpes contrôles, ALM Allain, BG2C et Longeville la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des marchés publics ;<br/>
              - l'arrêté du 8 septembre 2009 portant approbation du cahier des clauses administratives générales applicables aux marchés de travaux ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Mélanie Villiers, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la communauté d'agglomération du Grand Angoulême, à la SCP Boulloche, avocat de la société Fra Architectes et de la société Bureau Alpes contrôles, à la SCP Coutard, Munier-Apaire, avocat de la société OTEIS et à Me Corlay, avocat de la société ALM Allain ; <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés de la cour administrative d'appel de Bordeaux, en premier lieu, que, par acte d'engagement du 15 février 2010, la communauté d'agglomération du Grand Angoulême a confié la maîtrise d'oeuvre de la réalisation d'une médiathèque à Angoulême à un groupement conjoint comprenant la société Loci Anima, désormais dénommée Fra Architectes, mandataire, la société Ginger Séchaud Bossuyt, bureau d'études, aux droits de laquelle est venue la société OTEIS, la société Avel Acoustique et la société Fabrique Créative. Ce contrat portait sur une mission de base à laquelle s'ajoutaient une mission d'exécution et une mission de synthèse. Le lot n° 3 " Gros oeuvre-terrassement-chapes-maçonnerie " a été confié aux sociétés ALM Allain, BG2C et Longeville. Le contrôle technique des travaux a été confié à la société Bureau Alpes contrôles.  <br/>
<br/>
              2. En deuxième lieu, alors que les travaux de construction étaient en cours, de nombreuses infiltrations d'eau dans le local des centrales de traitement d'air ont été constatées à partir du mois de juin 2014. La communauté d'agglomération du Grand Angoulême a sollicité une expertise, ordonnée par le juge des référés du tribunal administratif de Poitiers le 14 septembre 2015 et dont le rapport a été rendu le 4 juillet 2016, mettant en cause un défaut d'étanchéité des gaines d'échappement dits " carneaux " enterrés et l'absence de réalisation d'un fourreau d'évacuation des eaux de ruissellement pour un rejet en pleine terre, alors que cette prestation, prévue initialement, n'a pas fait l'objet de plans d'exécution. <br/>
<br/>
              3. Enfin, suivant les préconisations de l'expert, des travaux de dévoiement des petits carneaux, remplacés par des gaines internes au bâtiment, et d'étanchéité des grands carneaux ont été réalisés pendant l'expertise. La réception de l'ouvrage avec réserve a été prononcée le 30 juillet 2015 et la levée des réserves est intervenue le 24 novembre 2015 sur proposition de la société Loci Anima, maître d'oeuvre. Saisi par la communauté d'agglomération du Grand Angoulême sur le fondement des dispositions de l'article R. 541-1 du code de justice administrative, le tribunal administratif de Poitiers, par un jugement du 19 décembre 2018, a rejeté sa demande de condamner les sociétés Loci Anima, OTEIS, Bureau Alpes contrôles, ALM Allain, BG2C et Longeville à lui verser à titre de provision la somme de 149 457,45 euros toutes taxes comprises. La communauté d'agglomération se pourvoit en cassation contre l'ordonnance du 23 août 2019 du juge des référés de la cour administrative d'appel de Bordeaux en tant que celui-ci, après avoir annulé ce jugement et condamné la société Fra Architectes à lui verser la somme de 2 768,24 euros à titre de provision sur les frais d'expertise, a rejeté le surplus des conclusions de sa requête.<br/>
<br/>
              4. Aux termes de l'article R. 541-1 du code de justice administrative : " Le juge des référés peut, même en l'absence d'une demande au fond, accorder une provision au créancier qui l'a saisi lorsque l'existence de l'obligation est non sérieusement contestable. Il peut, même d'office, subordonner le versement de la provision à la constitution d'une garantie ". Il résulte de ces dispositions que, pour regarder une obligation comme non sérieusement contestable, il appartient au juge des référés de s'assurer que les éléments qui lui sont soumis par les parties sont de nature à en établir l'existence avec un degré suffisant de certitude. <br/>
<br/>
              Sur les conclusions du pourvoi relatives à la responsabilité des entreprises titulaires du lot n° 3 :<br/>
<br/>
              5. La réception est l'acte par lequel le maître de l'ouvrage déclare accepter l'ouvrage avec ou sans réserve. Elle met fin aux rapports contractuels entre le maître de l'ouvrage et les constructeurs en ce qui concerne la réalisation de l'ouvrage. Si elle interdit, par conséquent, au maître de l'ouvrage d'invoquer, après qu'elle a été prononcée, et sous réserve de la garantie de parfait achèvement, des désordres apparents causés à l'ouvrage ou des désordres causés aux tiers, dont il est alors réputé avoir renoncé à demander la réparation, elle ne met fin aux obligations contractuelles des constructeurs que dans cette seule mesure. Ainsi la réception demeure, par elle-même, sans effet sur les droits et obligations financiers nés de l'exécution du marché, à raison notamment de retards ou de travaux supplémentaires, dont la détermination intervient définitivement lors de l'établissement du solde du décompte définitif. Seule l'intervention du décompte général et définitif du marché a pour conséquence d'interdire au maître de l'ouvrage toute réclamation à cet égard.<br/>
<br/>
              6. Il ressort des énonciations de l'ordonnance attaquée que le litige porte sur le remboursement à la communauté d'agglomération des travaux mentionnés au point 3, qu'elle a commandés et payés aux entreprises titulaires du lot n° 3 afin de permettre l'achèvement de l'ouvrage. En jugeant que la réception des travaux sans réserve faisait obstacle à tout remboursement du coût de ces travaux à la communauté d'agglomération, alors que cette réception ne mettait pas fin aux droits et obligations financiers nés de l'exécution du marché, le juge des référés de la cour administrative d'appel de Bordeaux a commis une erreur de droit. <br/>
<br/>
              Sur les conclusions du pourvoi relatives à la responsabilité des maîtres d'oeuvre :<br/>
<br/>
              7. La responsabilité des maîtres d'oeuvre pour manquement à leur devoir de conseil peut être engagée, dès lors qu'ils se sont abstenus d'appeler l'attention du maître d'ouvrage sur des désordres affectant l'ouvrage et dont ils pouvaient avoir connaissance, en sorte que la personne publique soit mise à même de ne pas réceptionner l'ouvrage ou d'assortir la réception de réserves.<br/>
<br/>
              8. D'une part, il ressort des énonciations de l'ordonnance attaquée, qui n'est pas contestée sur ce point, que les désordres en litige sont apparus et étaient connus tant des maîtres d'oeuvre que du maître d'ouvrage avant la réception du chantier et que les travaux pour y remédier ont été réalisés avant la levée des réserves. Dès lors, le juge des référés de la cour administrative d'appel n'a, en tout état de cause, pas commis d'erreur de droit ni dénaturé les pièces du dossier en déchargeant la société Fra Architectes, venue aux droits de la société Loci Anima, d'une quote-part de 70 % de sa responsabilité, et en déchargeant les autres maîtres d'oeuvre de la totalité de leur responsabilité. <br/>
<br/>
              9. D'autre part, en jugeant que l'étendue du préjudice indemnisable de la communauté d'agglomération impliquait d'apprécier si les travaux en litige étaient nécessaires à l'achèvement de l'ouvrage dans les règles de l'art ou constituaient une plus-value et en en déduisant que l'obligation invoquée par la communauté d'agglomération ne pouvait pas être regardée comme non sérieusement contestable, le juge des référés de la cour administrative d'appel n'a pas commis d'erreur de droit ni inexactement qualifié les faits qui lui étaient soumis.<br/>
<br/>
              10. Il résulte de tout ce qui précède que la communauté d'agglomération du Grand Angoulême est fondée à demander l'annulation de l'ordonnance qu'elle attaque, qui est suffisamment motivée, en tant seulement qu'elle a rejeté ses conclusions dirigées contre les entreprises titulaires du lot n° 3.<br/>
<br/>
              11. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la communauté d'agglomération du Grand Angoulême le versement de la somme de 3 000 euros à la société OTEIS et de 1 500 euros à chacune des sociétés Fra Architectes et Bureau Alpes contrôles sur le fondement de l'article L. 761-1 du code de justice administrative. Les mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de ces trois sociétés qui ne sont pas, dans la présente instance, la partie perdante. Enfin, il y a lieu de mettre à la charge des sociétés ALM Allain, BG2C et Longeville la somme de 1 000 euros chacune à verser à la communauté d'agglomération du Grand Angoulême, au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 23 août 2019 du juge des référés de la cour administrative d'appel de Bordeaux est annulée en tant qu'elle a rejeté les conclusions de la communauté d'agglomération du Grand Angoulême dirigées contre les entreprises titulaires du lot n° 3 du marché de construction de la médiathèque d'Angoulême.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, au juge des référés de la cour administrative d'appel de Bordeaux.<br/>
Article 3 : La communauté d'agglomération du Grand Angoulême versera, d'une part, à la société OTEIS une somme de 3 000 euros et, d'autre part, à chacune des sociétés Fra Architectes et Bureau Alpes contrôles une somme de 1 500 euros, au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Les sociétés ALM Allain, BG2C et Longeville verseront une somme de 1 000 euros chacune à la communauté d'agglomération du Grand Angoulême au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Le surplus des conclusions du pourvoi de la communauté d'agglomération du Grand Angoulême est rejeté.<br/>
Article 6 : La présente décision sera notifiée à la communauté d'agglomération du Grand Angoulême, à la société Fra Architectes, à la société OTEIS et à la société Bureau Alpes contrôles.<br/>
Copie en sera adressée à la société ALM Allain, à la société BG2C et à la société Longeville.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
