<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033255616</ID>
<ANCIEN_ID>JG_L_2016_10_000000386400</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/25/56/CETATEXT000033255616.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 17/10/2016, 386400</TITRE>
<DATE_DEC>2016-10-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386400</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; RICARD</AVOCATS>
<RAPPORTEUR>M. Laurent Huet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:386400.20161017</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Nice d'annuler pour excès de pouvoir la délibération du conseil d'administration restreint de l'université de Nice-Sophia Antipolis du 4 juin 2012 et la décision du même jour par laquelle la présidente de cette université a interrompu le concours de recrutement de maître de conférence en expérimentation optique et physique des lasers, astrophysique relativiste observationnelle (concours n° 30-34 MCF 362) et n'a pas transmis au ministre chargé de l'enseignement supérieur le classement opéré par le comité de sélection pour nomination. Par un jugement n° 1203817 du 6 juin 2013, le tribunal administratif a annulé la délibération du conseil d'administration restreint et rejeté la demande d'annulation de la décision de la présidente de l'université.<br/>
<br/>
              Par un arrêt n° 13MA002950 du 26 septembre 2014, la cour administrative d'appel de Marseille a, sur appel de M.B..., d'une part, annulé la décision du 4 juin 2012 de la présidente de l'université de Nice-Sophia Antipolis, d'autre part, réformé le jugement du 6 juin 2013 du tribunal administratif de Nice en ce qu'il avait de contraire à cette annulation et, enfin, enjoint à la présidente de l'université de ressaisir le conseil d'administration en vue du réexamen de la candidature de M. B...au vu de l'avis motivé émis par le comité de sélection et de communiquer le nom du candidat proposé au ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche, dans le délai d'un mois à compter de sa décision.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire et un mémoire en réplique, enregistrés les 12 décembre 2014, 12 mars 2015, 7 janvier 2016 et 4 mars 2016 au secrétariat du contentieux du Conseil d'Etat, l'université de Nice-Sophia Antipolis demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. B....<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 84-430 du 6 juin 1984 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Huet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de l'université de Nice-Sophia Antipolis et à Me Ricard, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'université de Nice-Sophia Antipolis a ouvert au recrutement un poste de maître de conférence en expérimentation optique et physique des lasers, astrophysique relativiste observationnelle (concours n° 30-34 MCF 362) ; que, par une délibération du 23 mai 2012, le comité de sélection de cette université a établi une liste de cinq candidats sur laquelle M. B...figurait en première position ; que, par une décision du 4 juin 2012, le conseil d'administration restreint de l'université a émis un avis réservé sur cette délibération ; que, le même jour, la présidente de l'université a décidé d'interrompre le concours de recrutement et de ne transmettre aucun nom au ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche ; que, saisi par M.B..., le tribunal administratif de Nice a, par un jugement du 6 juin 2013, annulé la délibération du conseil d'administration restreint de l'université de Nice-Sophia Antipolis mais rejeté les conclusions dirigées contre la décision de la présidente de l'université ; que, sur appel de M.B..., la cour administrative d'appel de Marseille a, par l'arrêt attaqué du 26 septembre 2014, d'une part, annulé la décision du 4 juin 2012 de la présidente de l'université, d'autre part, réformé le jugement du tribunal administratif de Nice en ce qu'il avait de contraire à cette annulation et, enfin, enjoint à la présidente de l'université de ressaisir le conseil d'administration restreint en vue du réexamen de la candidature de M. B...au vu de l'avis motivé émis par le comité de sélection et de communiquer au ministre le nom du candidat proposé dans le délai d'un mois à compter de sa décision ;<br/>
<br/>
              2. Considérant que pour annuler la décision de la présidente de l'université en date du 4 juin 2012, la cour administrative d'appel de Marseille s'est fondée sur deux motifs tirés, l'un, de ce que cette décision n'était pas suffisamment motivée, l'autre, de ce que l'atteinte au principe d'impartialité des membres du comité de sélection retenue par la présidente de l'université pour justifier sa décision n'était pas fondée ;<br/>
<br/>
              3. Considérant que, saisi d'un pourvoi dirigé contre une décision juridictionnelle reposant sur plusieurs motifs dont l'un est erroné, le juge de cassation, à qui il n'appartient pas de rechercher si la juridiction aurait pris la même décision en se fondant uniquement sur les autres motifs, doit, hormis le cas où ce motif erroné présenterait un caractère surabondant, accueillir le pourvoi ; qu'il en va cependant autrement lorsque la décision juridictionnelle attaquée prononce l'annulation pour excès de pouvoir d'un acte administratif, dans la mesure où l'un quelconque des moyens retenus par le juge du fond peut suffire alors à justifier son dispositif d'annulation ; qu'en pareille hypothèse - et sous réserve du cas où la décision qui lui est déférée aurait été rendue dans des conditions irrégulières - il appartient au juge de cassation, si l'un des moyens reconnus comme fondés par cette décision en justifie légalement le dispositif, de rejeter le pourvoi ; que, toutefois, en raison de l'autorité de chose jugée qui s'attache aux motifs constituant le soutien nécessaire du dispositif de la décision juridictionnelle déférée, le juge de cassation ne saurait, sauf à méconnaître son office, prononcer ce rejet sans avoir, au préalable, censuré celui ou ceux de ces motifs qui étaient erronés ;<br/>
<br/>
              4. Considérant que la seule circonstance qu'un membre du jury d'un concours connaisse un candidat ne suffit pas à justifier qu'il s'abstienne de participer aux délibérations de ce concours ; qu'en revanche, le respect du principe d'impartialité exige que, lorsqu'un membre du jury d'un concours a avec l'un des candidats des liens, tenant à la vie personnelle ou aux activités professionnelles, qui seraient de nature à influer sur son appréciation, ce membre doit non seulement s'abstenir de participer aux interrogations et aux délibérations concernant ce candidat mais encore concernant l'ensemble des candidats au concours ; qu'en outre, un membre du jury qui a des raisons de penser que son impartialité pourrait être mise en doute ou qui estime, en conscience, ne pas pouvoir participer aux délibérations avec l'impartialité requise, doit également s'abstenir de prendre part à toutes les interrogations et délibérations de ce jury en vertu des principes d'unicité du jury et d'égalité des candidats devant celui-ci ;<br/>
<br/>
              5. Considérant que si la cour pouvait sans erreur de droit tenir compte de la nature hautement spécialisée de la discipline en cause pour apprécier l'intensité des liens pouvant exister entre les membres du jury et les candidats au regard du respect du principe d'impartialité, eu égard au très faible nombre de spécialistes de la discipline, elle a commis une erreur de qualification juridique des faits en retenant qu'en l'espèce, il n'y avait eu aucune atteinte au principe d'impartialité alors qu'elle avait relevé dans son arrêt l'existence de liens étroits entre l'un des candidats et sept des douze membres du comité de sélection, dont le président de ce comité, caractérisés notamment par le fait que quatre membres du comité avaient cosigné avec l'intéressé dix-sept des vingt-neuf articles scientifiques publiés entre 2000 et 2012 dont il se prévalait à l'appui de sa candidature et que le président du comité en avait lui-même cosigné six avec l'intéressé ; <br/>
<br/>
              6. Considérant toutefois qu'en jugeant que la décision du 4 juin 2012 de la présidente de l'université, qui se bornait à faire état de vices de procédure pour interrompre le concours de recrutement et ne pas transmettre de proposition au ministre chargé de l'enseignement supérieur, n'était pas suffisamment motivée, la cour administrative d'appel, qui n'a pas dénaturé les pièces du dossier, n'a entaché sur ce point son arrêt d'aucune erreur de droit ; que le moyen ainsi retenu est, à lui seul, de nature à justifier l'annulation de la décision de la  présidente de l'université en date du 4 juin 2012 ; que, par suite, l'université de Nice-Sophia Antipolis n'est pas fondée à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que le pourvoi de l'université de Nice-Sophia Antipolis doit être rejeté ;<br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'université de Nice-Sophia Antipolis le versement à Me Ricard, avocat de M.B..., de la somme de 2 000 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cet avocat renonce à percevoir la part contributive de l'Etat au titre de l'aide juridictionnelle ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de l'université de Nice-Sophia Antipolis est rejeté.<br/>
Article 2 : L'université de Nice-Sophia Antipolis versera à Me Ricard, avocat de M.B..., la somme de 2 000 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cet avocat renonce à percevoir la part contributive de l'Etat au titre de l'aide juridictionnelle.<br/>
Article 3 : La présente décision sera notifiée à l'université de Nice-Sophia Antipolis et à M. A... B....<br/>
Copie en sera adressée à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. - PRINCIPES D'IMPARTIALITÉ ET D'UNICITÉ DU JURY DE CONCOURS - CONSÉQUENCES SUR LA POSSIBILITÉ DE PARTICIPER À UN JURY [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-03-02-03 FONCTIONNAIRES ET AGENTS PUBLICS. ENTRÉE EN SERVICE. CONCOURS ET EXAMENS PROFESSIONNELS. ORGANISATION DES CONCOURS - JURY. - PRINCIPE D'UNICITÉ DU JURY DE CONCOURS - CAS OÙ UN MEMBRE DU JURY NE PEUT EXAMINER L'UN DES CANDIDATS EN RAISON DU PRINCIPE D'IMPARTIALITÉ - CONSÉQUENCE - IMPOSSIBILITÉ DE PARTICIPER AU JURY [RJ1].
</SCT>
<ANA ID="9A"> 01-04-03-07 La seule circonstance qu'un membre du jury d'un concours connaisse un candidat ne suffit pas à justifier qu'il s'abstienne de participer aux délibérations de ce concours. En revanche, le respect du principe d'impartialité exige que, lorsqu'un membre du jury d'un concours a avec l'un des candidats des liens, tenant à la vie personnelle ou aux activités professionnelles, qui seraient de nature à influer sur son appréciation, ce membre doit non seulement s'abstenir de participer aux interrogations et aux délibérations concernant ce candidat mais encore concernant l'ensemble des candidats au concours. En outre, un membre du jury qui a des raisons de penser que son impartialité pourrait être mise en doute ou qui estime, en conscience, ne pas pouvoir participer aux délibérations avec l'impartialité requise, doit également s'abstenir de prendre part à toutes les interrogations et délibérations de ce jury en vertu des principes d'unicité du jury et d'égalité des candidats devant celui-ci.</ANA>
<ANA ID="9B"> 36-03-02-03 La seule circonstance qu'un membre du jury d'un concours connaisse un candidat ne suffit pas à justifier qu'il s'abstienne de participer aux délibérations de ce concours. En revanche, le respect du principe d'impartialité exige que, lorsqu'un membre du jury d'un concours a avec l'un des candidats des liens, tenant à la vie personnelle ou aux activités professionnelles, qui seraient de nature à influer sur son appréciation, ce membre doit non seulement s'abstenir de participer aux interrogations et aux délibérations concernant ce candidat mais encore concernant l'ensemble des candidats au concours. En outre, un membre du jury qui a des raisons de penser que son impartialité pourrait être mise en doute ou qui estime, en conscience, ne pas pouvoir participer aux délibérations avec l'impartialité requise, doit également s'abstenir de prendre part à toutes les interrogations et délibérations de ce jury en vertu des principes d'unicité du jury et d'égalité des candidats devant celui-ci.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur le principe d'unicité, CE, 17 juin 1927, Sieur Bouvet, p. 676. Comp., s'agissant d'un examen, CE, Section, 17 juillet 2008, Mme Baysse, n° 291997, p. 302. Ab. jur., sur ce point non mentionné aux Tables, CE, 8 juin 2015, M. Zegbi, n° 370539, aux Tables sur un autre point.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
