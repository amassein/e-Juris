<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039627774</ID>
<ANCIEN_ID>JG_L_2019_12_000000419220</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/62/77/CETATEXT000039627774.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 16/12/2019, 419220</TITRE>
<DATE_DEC>2019-12-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419220</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLOCHE ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Frédéric Pacoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:419220.20191216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C... D... et Mme A... B... épouse D... ont demandé au tribunal administratif de Montreuil d'annuler pour excès de pouvoir la décision du 24 septembre 2008 par laquelle le maire de Montreuil a préempté un immeuble situé 188 bis, boulevard de la Boissière et la décision du 19 février 2015 par laquelle il a rejeté leur demande tendant à son retrait. Par un jugement n° 1503398 du 21 décembre 2015, le tribunal administratif de Montreuil a fait droit à cette demande.<br/>
<br/>
              Par un arrêt n° 16VE00463 du 25 janvier 2018, la cour administrative d'appel de Versailles a, sur l'appel formé par la commune de Montreuil, annulé ce jugement et rejeté la demande présentée par M. et Mme D... devant le tribunal administratif de Montreuil.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un nouveau mémoire et un mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat les 23 mars, 25 juin 2018, 9 janvier 2019 et 4 octobre 2019, M. et Mme D... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la commune de Montreuil ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Montreuil la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Pacoud, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boulloche, avocat de M. et Mme D... et à la SCP Foussard, Froger, avocat de la commune de Montreuil ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, le 24 septembre 2008, le maire de Montreuil a décidé de préempter un immeuble situé 188 bis, boulevard de la Boissière, pour l'acquisition duquel M. et Mme D... avaient conclu une promesse de vente. Par un jugement du 21 décembre 2015, le tribunal administratif de Montreuil, saisi par M. et Mme D..., a annulé cette décision de préemption ainsi que celle du 19 février 2015 par laquelle le maire de Montreuil avait rejeté leur demande tendant à son retrait. M. et Mme D... se pourvoient en cassation contre l'arrêt du 25 janvier 2018 par lequel la cour administrative d'appel de Versailles a annulé ce jugement et rejeté leur demande de première instance.<br/>
<br/>
              2. En premier lieu, aux termes du premier alinéa de l'article R. 421-1 du code de justice administrative, dans sa rédaction applicable à la date de la décision en litige : " Sauf en matière de travaux publics, la juridiction ne peut être saisie que par voie de recours formé contre une décision, et ce, dans les deux mois à partir de la notification ou de la publication de la décision attaquée ". L'article R. 421-5 du code de justice administrative dispose que : " Les délais de recours contre une décision administrative ne sont opposables qu'à la condition d'avoir été mentionnés, ainsi que les voies de recours, dans la notification de la décision ". L'acquéreur évincé étant au nombre des personnes, destinataires de la décision de préemption, auxquelles cette décision doit être notifiée, il résulte de ces dispositions que ce délai ne lui est pas opposable si elle ne lui a pas été notifiée avec l'indication des voies et délais de recours. <br/>
<br/>
              3. Toutefois, le principe de sécurité juridique, qui implique que ne puissent être remises en cause sans condition de délai des situations consolidées par l'effet du temps, fait obstacle à ce que puisse être contestée indéfiniment une décision administrative individuelle qui a été notifiée à son destinataire, ou dont il est établi, à défaut d'une telle notification, que celui-ci a eu connaissance. En de telles hypothèses, si le non-respect de l'obligation d'informer l'intéressé sur les voies et les délais de recours, ou l'absence de preuve qu'une telle information a bien été fournie, ne permet pas que lui soient opposés les délais de recours fixés par le code de justice administrative, le destinataire de la décision ne peut exercer de recours juridictionnel au-delà d'un délai raisonnable. En règle générale et sauf circonstances particulières dont se prévaudrait le requérant, ce délai ne saurait, sous réserve de l'exercice de recours administratifs pour lesquels les textes prévoient des délais particuliers, excéder un an à compter de la date à laquelle une décision expresse lui a été notifiée ou de la date à laquelle il est établi qu'il en a eu connaissance. <br/>
<br/>
              4. La règle énoncée ci-dessus, qui a pour seul objet de borner dans le temps les conséquences de la sanction attachée au défaut de mention des voies et délais de recours, ne porte pas atteinte à la substance du droit au recours, mais tend seulement à éviter que son exercice, au-delà d'un délai raisonnable, ne mette en péril la stabilité des situations juridiques et la bonne administration de la justice, en exposant les défendeurs potentiels à des recours excessivement tardifs. Il appartient dès lors au juge administratif d'en faire application au litige dont il est saisi, quelle que soit la date des faits qui lui ont donné naissance.<br/>
<br/>
              5. En l'espèce, la cour administrative d'appel de Versailles a souverainement constaté que si M. et Mme D... n'avaient pas reçu notification de la décision de préemption du 24 septembre 2008, ils avaient toutefois demandé à la commune de Montreuil des informations sur l'état d'avancement du projet pour lequel le droit de préemption avait été exercé, par une lettre du 18 mars 2013 à laquelle était jointe une copie intégrale de la décision de préemption ne mentionnant pas les voies et les délais de recours. Il résulte de ce qui a été dit aux points précédents que la cour n'a pas commis d'erreur de droit en jugeant que, si le délai de recours de deux mois mentionné au premier alinéa de l'article R. 421-1 du code de justice administrative n'était pas opposable à M. et Mme D..., la lettre du 18 mars 2013 était en revanche de nature à établir qu'à cette dernière date ils avaient connaissance de la décision de préemption, pour en déduire que leur recours, enregistré au tribunal administratif de Montreuil le 17 avril 2015, était tardif pour avoir été présenté au-delà du délai raisonnable dans lequel il pouvait être exercé, un tel délai étant opposable à l'acquéreur évincé par une décision de préemption, sans qu'il soit ce faisant porté atteinte au droit au recours. La seule circonstance que la commune de Montreuil n'ait pas répondu à leur demande postérieure d'information sur les dispositions prises pour mettre en oeuvre le projet de construction n'étant pas susceptible de constituer une circonstance particulière de nature à faire obstacle à ce que leur recours soit regardé comme présenté au-delà du délai raisonnable, la cour n'a ni insuffisamment motivé son arrêt ni commis d'erreur de droit en ne se prononçant pas explicitement sur ce point.<br/>
<br/>
              6. En deuxième lieu, l'exercice, au-delà du délai de recours contentieux contre un acte administratif, d'un recours gracieux tendant au retrait de cet acte ne saurait avoir pour effet de rouvrir le délai de recours. Par suite, le rejet d'une telle demande n'est, en principe, et hors le cas où l'administration a refusé de faire usage de son pouvoir de retirer un acte administratif obtenu par fraude, pas susceptible de recours.  <br/>
<br/>
              7. Il résulte des énonciations de l'arrêt de la cour administrative d'appel de Versailles, non arguées de dénaturation, que M. et Mme D... ont demandé le 4 février 2015 au maire de Montreuil de retirer la décision de préemption du 24 septembre 2008 et que cette demande a été rejetée par une décision du 19 février 2015. Dès lors, les requérants n'étaient pas recevables à demander l'annulation de la décision rejetant leur demande de retrait de la décision de préemption, formée après l'expiration du délai initial du recours contentieux contre cet acte. Ce motif, qui n'appelle l'appréciation d'aucune circonstance de fait supplémentaire doit être substitué à celui retenu par l'arrêt du 25 janvier 2018, dont il justifie légalement le dispositif. Par suite, le moyen soulevé par les requérants à l'encontre du motif retenu par cet arrêt doit être écarté comme inopérant.<br/>
<br/>
              8. Il résulte de tout ce qui précède que le pourvoi de M. et Mme D... doit être rejeté, y compris, en conséquence, les conclusions qu'ils présentent au titre de l'article L. 761-1 du code de justice administrative. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à ce même titre une somme à la charge de M. et Mme D.... <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de M. et Mme D... est rejeté. <br/>
Article 2 : Les conclusions présentées par la commune de Montreuil au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à M. et Mme C... D... et à la commune de Montreuil. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-02-01-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PROCÉDURES D'INTERVENTION FONCIÈRE. PRÉEMPTION ET RÉSERVES FONCIÈRES. DROITS DE PRÉEMPTION. - DÉCISION DE PRÉEMPTION DEVANT ÊTRE NOTIFIÉE À L'ACQUÉREUR ÉVINCÉ [RJ1] - CONSÉQUENCE - DÉLAI DE RECOURS NE POUVANT LUI ÊTRE OPPOSÉ EN L'ABSENCE DE NOTIFICATION RÉGULIÈRE.
</SCT>
<ANA ID="9A"> 68-02-01-01 L'acquéreur évincé étant au nombre des personnes, destinataires de la décision de préemption, auxquelles cette décision doit être notifiée, il résulte de l'article R. 421-5 du code de justice administrative (CJA) que le délai de recours prévu par l'article R. 421-1 du même code ne lui est pas opposable si elle ne lui a pas été notifiée avec l'indication des voies et délais de recours.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 14 novembre 2007, SCI du Marais, n° 305620, T. pp. 1121. Rappr., s'agissant de la notification au propriétaire intéressé, CE, 15 mai 2002, Ville de Paris c/ Association cultuelle des témoins de Jéhovah de Paris, n° 230015, p. 173 ; s'agissant de la notification à l'adjudicataire en cas de vente par adjudication, CE, 17 décembre 2008, Office d'habitation du Gers, n° 304840, T. p. 962.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
