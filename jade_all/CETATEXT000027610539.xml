<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027610539</ID>
<ANCIEN_ID>JG_L_2013_06_000000365253</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/61/05/CETATEXT000027610539.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 24/06/2013, 365253, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-06-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365253</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:365253.20130624</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le mémoire, enregistré le 16 avril 2013 au secrétariat du contentieux du Conseil d'Etat, présenté pour M. A...B..., demeurant..., en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 ; M. B... demande au Conseil d'Etat, à l'appui de son pourvoi tendant à l'annulation de l'arrêt n° 11NT03219 du 14 novembre 2012 par lequel la cour administrative d'appel de Nantes a rejeté sa requête tendant à l'annulation du jugement n° 0803654 du 19 octobre 2011 du tribunal administratif de Nantes rejetant sa demande de décharge de la cotisation supplémentaire d'impôt sur le revenu à laquelle il a été assujetti au titre de l'année 2005 et des pénalités correspondantes, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution du 1 de l'article 80 duodecies du code général des impôts ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'État (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant que le 1 de l'article 80 duodecies du code général des impôts, dans sa rédaction issue de l'article 1er de la loi de finances rectificative pour 2000, est applicable au présent litige ; que cette disposition n'a pas déjà été déclarée conforme à la Constitution par le Conseil constitutionnel ;<br/>
<br/>
              3. Considérant qu'il résulte des termes mêmes de cette disposition qu'à l'exception des indemnités qui y sont limitativement énumérées, toute somme perçue par le salarié à l'occasion de la rupture de son contrat de travail revêt un caractère imposable, que cette indemnité compense une perte de salaires ou qu'elle répare un préjudice d'une autre nature ; qu'il en va notamment ainsi des indemnités perçues par un salarié en exécution d'une transaction conclue avec son employeur à la suite d'une " prise d'acte " de la rupture de son contrat de travail, qui ne peuvent bénéficier, en aucune circonstance et quelle que soit la nature du préjudice qu'elles visent à réparer, d'une exonération d'impôt sur le revenu ; que, dès lors, le moyen tiré de ce que le 1 de l'article 80 duodecies du code général des impôts porte atteinte aux droits et libertés garantis par la Constitution, notamment au principe d'égalité devant les charges publiques garanti par l'article 13 de la Déclaration des droits de l'homme et du citoyen, soulève une question présentant un caractère sérieux ; qu'ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La question de la conformité à la Constitution du 1 de l'article 80 duodecies du code général des impôts, dans sa rédaction issue de l'article 1er de la loi de finances rectificative pour 2000, est renvoyée au Conseil constitutionnel.<br/>
Article 2 : Il est sursis à statuer sur le pourvoi de M. B...jusqu'à ce que le Conseil constitutionnel ait tranché la question de constitutionnalité ainsi soulevée.<br/>
Article 3 : La présente décision sera notifiée à M. A...B...et au ministre délégué auprès du ministre de l'économie et des finances, chargé du budget.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
