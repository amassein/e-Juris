<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044344706</ID>
<ANCIEN_ID>JG_L_2021_11_000000442887</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/34/47/CETATEXT000044344706.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 18/11/2021, 442887, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>442887</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CARBONNIER ; SCP DELAMARRE, JEHANNIN</AVOCATS>
<RAPPORTEUR>Mme Christelle Thomas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Domingo</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:442887.20211118</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société pour la protection des paysages et de l'esthétique de la France (SPPEF) a demandé au tribunal administratif de Rennes d'annuler l'arrêté du 29 septembre 2015 par lequel le maire de la commune de Matignon (Côtes d'Armor) ne s'est pas opposé à la déclaration préalable de M. A... portant sur la réhabilitation d'un bâtiment agricole ainsi que la décision de rejet de son recours tendant au retrait de cette décision et d'enjoindre au maire de retirer cet arrêté. Par un jugement n° 1603058 du 25 janvier 2019, le tribunal administratif de Rennes a annulé l'arrêté du 29 septembre 2015 du maire de Matignon et rejeté le surplus des conclusions de la demande.<br/>
<br/>
              Par un arrêt n° 19NT01134 du 7 février 2020, la cour administrative d'appel de Nantes a, sur appel de M. A..., annulé ce jugement, rejeté la demande tendant à l'annulation de l'arrêté du 29 septembre 2015 du maire de Matignon, annulé sa décision de refus implicite de retirer cet arrêté et enjoint à ce maire de procéder à ce retrait dans un délai de trois mois à compter de la notification de son arrêt.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 17 août et 16 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - la loi du 15 juin 1943 d'urbanisme ;<br/>
              - l'ordonnance n° 45-2542 du 27 octobre 1945 ;<br/>
              - la loi n° 67-1253 du 30 décembre 1967 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Christelle Thomas, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Domingo, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à Me Carbonnier, avocat de M. B... A... et à la SCP Delamarre, Jéhannin, avocat de la commune de Matignon ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le 31 juillet 2015, M. A... a déposé une déclaration préalable de travaux portant sur la réhabilitation d'un hangar agricole à Matignon. Par un jugement du 25 janvier 2019, le tribunal administratif de Rennes, saisi par la société pour la protection des paysages et de l'esthétique de la France (SPPEF), a annulé pour excès de pouvoir l'arrêté du 29 septembre 2015 par lequel le maire de Matignon ne s'est pas opposé à cette déclaration préalable. M. A... se pourvoit en cassation contre l'arrêt du 7 février 2020 par lequel la cour administrative d'appel de Nantes a annulé ce jugement, rejeté la demande de la SPPEF tendant à l'annulation de l'arrêté du 29 septembre 2015 du maire de Matignon, annulé la décision de refus implicite du maire de Matignon de retirer cet arrêté et enjoint à ce dernier de procéder à ce retrait dans un délai de trois mois à compter de la notification de son arrêt.<br/>
<br/>
              2. Il incombe au juge de l'excès de pouvoir, saisi d'un moyen tendant à l'annulation de la décision par laquelle l'autorité administrative a refusé de faire usage de son pouvoir d'abroger ou de retirer un acte administratif obtenu par fraude, d'une part, de vérifier la réalité de la fraude alléguée et, d'autre part, de contrôler que l'appréciation de l'administration sur l'opportunité de procéder ou non à l'abrogation ou au retrait n'est pas entachée d'erreur manifeste, compte tenu notamment de la gravité de la fraude et des atteintes aux divers intérêts publics ou privés en présence susceptibles de résulter soit du maintien de l'acte litigieux soit de son abrogation ou de son retrait. <br/>
<br/>
              3. Il ressort des énonciations de l'arrêt attaqué que, pour juger que M. A... ne pouvait ignorer qu'il aurait dû présenter non pas une déclaration préalable portant sur des travaux de réhabilitation mais une demande de permis de construire portant sur l'ensemble du bâtiment, la cour s'est fondée sur la circonstance que les attestations qu'il avait produites ne justifiaient pas de ce que la construction existante, qui a été édifiée sans autorisation alors que sa surface est de 250 m², l'aurait été antérieurement à la loi du 15 juin 1943 imposant la délivrance d'un permis de construire. Toutefois, la fraude est caractérisée lorsqu'il ressort des pièces du dossier que le demandeur a eu l'intention de tromper l'administration pour obtenir une décision indue. Par suite, en se bornant, pour caractériser l'élément intentionnel de la fraude, à relever que M. A... ne justifiait pas de la date de construction du bâtiment objet de sa déclaration de travaux et n'établissait dès lors pas que ce dernier n'entrait pas dans le champ de l'obligation de permis de construire instituée par la loi du 15 juin 1943 d'urbanisme reprise pour l'essentiel par l'ordonnance du 27 octobre 1945 relative au permis de construire, la cour a commis une erreur de droit. <br/>
<br/>
              4. Il résulte de ce qui précède que M. A... est fondé à demander l'annulation de l'arrêt qu'il attaque, sans qu'il soit besoin de se prononcer sur les autres moyens de son pourvoi.<br/>
<br/>
              5. Les conclusions de M. A... présentées, au titre de l'article L. 761-1 du code de justice administrative, contre l'Etat, qui n'est pas partie à la présente instance, ne peuvent qu'être rejetées. Ces dispositions font obstacle à ce qu'une somme soit mise à la charge de M. A... qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
                                   D E C I D E :<br/>
                                   ----------------<br/>
<br/>
Article 1er : L'arrêt du 7 février 2020 de la cour administrative d'appel de Nantes est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
Article 3 : Les conclusions de M. A... et de la commune de Matignon présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. B... A... et à la commune de Matignon.<br/>
Copie en sera adressée à la société pour la protection des paysages et de l'esthétique de la France.<br/>
<br/>
Délibéré à l'issue de la séance du 25 octobre 2021 où siégeaient : Mme Nathalie Escaut, conseillère d'Etat, présidant ; M. Alexandre Lallet, conseiller d'Etat et Mme Christelle Thomas, maître des requêtes en service extraordinaire-rapporteur. <br/>
<br/>
<br/>
              Rendu le 18 novembre 2021.<br/>
<br/>
                                   La présidente : <br/>
                                   Signé : Mme Nathalie Escaut<br/>
<br/>
La rapporteure : <br/>
Signé : Mme Christelle Thomas<br/>
<br/>
                                   La secrétaire :<br/>
                                   Signé : Mme C... D...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
