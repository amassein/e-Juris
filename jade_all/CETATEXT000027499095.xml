<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027499095</ID>
<ANCIEN_ID>JG_L_2013_05_000000366865</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/49/90/CETATEXT000027499095.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 31/05/2013, 366865</TITRE>
<DATE_DEC>2013-05-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366865</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Anissia Morel</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2013:366865.20130531</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu, enregistré le 15 mars 2013 au secrétariat du contentieux du Conseil d'Etat, le jugement du 8 mars 2013 par lequel le tribunal administratif d'Amiens, avant de statuer sur la demande de M. A...tendant à l'annulation des décisions du ministre de l'intérieur retirant des points de son permis de conduire et constatant la perte de validité de celui-ci, a décidé, par application de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat en soumettant à son examen les questions suivantes :<br/>
<br/>
              1°) Pendant la période du délai probatoire, le ministre de l'intérieur est-il tenu de notifier au conducteur novice une lettre recommandée avec demande d'avis de réception l'informant de son obligation de se soumettre à la formation spécifique mentionnée au troisième alinéa de l'article L. 223-6 du code de la route '<br/>
<br/>
              2°) Selon le deuxième alinéa de l'article L. 223-1 du code de la route, durant un délai probatoire de trois ans à compter de la date d'obtention du permis de conduire, ramené à deux ans lorsque le titulaire du permis a suivi un apprentissage anticipé de la conduite, le nombre de points affectés au permis de conduire est inférieur au nombre maximal de points ; le délai probatoire, tel que fixé par l'article L. 223-1 du code de la route, est-il prolongé lorsque le conducteur novice a commis, pendant le délai probatoire, une infraction au code de la route faisant obstacle à ce que le permis de conduire probatoire soit affecté du nombre maximal de points à l'issue du délai probatoire ' <br/>
<br/>
              3°) Dans le cas où le ministre de l'intérieur aurait l'obligation de notifier au conducteur novice le courrier prévu à l'article R. 223-4 du code de la route, l'absence de notification de ce courrier affecte-t-elle la légalité de la décision " 48 SI " portant invalidation du permis de conduire d'un conducteur novice '<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu, enregistrées le 13 mai 2013, les observations du ministre de l'intérieur ;<br/>
<br/>
              Vu, enregistrées le 16 mai 2013, les observations présentées par M. B...A... ;<br/>
<br/>
              Vu les pièces du dossier transmis par le tribunal administratif ;<br/>
<br/>
              Vu le code de la route ;<br/>
<br/>
              Vu le code de justice administrative, notamment ses articles L. 113-1 et R. 113-1 à R. 113-4 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
               Après avoir entendu en séance publique :<br/>
<br/>
- le rapport de Mme Anissia Morel, Auditeur, <br/>
- les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
REND L'AVIS SUIVANT<br/>
<br/>
<br/>
<br/>
              1. L'article L. 223-1 du code de la route, dans sa rédaction issue de la loi n° 2011-267 du 14 mars 2011, dispose que : " Le permis de conduire est affecté d'un nombre de points. Celui-ci est réduit de plein droit si le titulaire du permis a commis une infraction pour laquelle cette réduction est prévue. / A la date d'obtention du permis de conduire, celui-ci est affecté de la moitié du nombre maximal de points. Il est fixé un délai probatoire de trois ans. Au terme de chaque année de ce délai probatoire, le permis est majoré d'un sixième du nombre maximal de points si aucune infraction ayant donné lieu à un retrait de points n'a été commise depuis le début de la période probatoire. Lorsque le titulaire du permis de conduire a suivi un apprentissage anticipé de la conduite, ce délai probatoire est réduit à deux ans et cette majoration est portée au quart du nombre maximal de points. / Lorsque le nombre de points est nul, le permis perd sa validité. / (...) ". Les conditions des retraits de points sont fixées à l'article L. 223-3 qui prévoit dans son dernier alinéa que : " Le retrait de points est porté à la connaissance de l'intéressé par lettre simple quand il est effectif ". L'article R. 223-1 fixe à douze le nombre maximal de points du permis de conduire et à six le nombre de points dont le permis est affecté à la date de son obtention. Il précise que le délai probatoire court de la date d'obtention du permis. <br/>
<br/>
              2. Aux termes du quatrième alinéa de l'article L. 223-6 du même code : " Le titulaire du permis de conduire qui a commis une infraction ayant donné lieu à retrait de points peut obtenir une récupération de points s'il suit un stage de sensibilisation à la sécurité routière. Lorsque le titulaire du permis de conduire a commis une infraction ayant donné lieu à un retrait de points égal ou supérieur au quart du nombre maximal de points et qu'il se trouve dans la période du délai probatoire défini à l'article L. 223-1, il doit se soumettre à cette formation spécifique qui se substitue à l'amende sanctionnant l'infraction ". Précisant les modalités d'application de ces dispositions législatives, l'article R. 223-4 prévoit que : "  I. - Lorsque le conducteur titulaire du permis de conduire a commis, pendant le délai probatoire défini à l'article L. 223-1, une infraction ayant donné lieu au retrait d'au moins trois points, la notification du retrait de points lui est adressée par lettre recommandée avec demande d'avis de réception. Cette lettre l'informe de l'obligation de se soumettre à la formation spécifique mentionnée au troisième alinéa de l'article L. 223-6 dans un délai de quatre mois. / II. - Le fait de ne pas se soumettre à la formation spécifique mentionnée au I dans le délai de quatre mois est puni de l'amende prévue pour les contraventions de la 4e classe (...) ". <br/>
<br/>
              Sur le terme du délai probatoire :<br/>
<br/>
              3. Il résulte des dispositions de l'article L. 223-1 du code de la route que le délai probatoire est de deux ans lorsque le titulaire du permis de conduire a suivi un apprentissage anticipé de la conduite et de trois ans dans les autres cas. Aucune disposition ne prévoit une prorogation de ce délai lorsque l'intéressé a commis, avant sa date normale d'expiration, une infraction ayant donné lieu à un retrait de points.<br/>
<br/>
              Sur la notification par lettre recommandée avec avis de réception des retraits de trois points ou plus consécutifs à des infractions commises pendant le délai probatoire :<br/>
<br/>
              4. En prévoyant, au I de l'article R. 223-4 du code de la route, qu'un retrait de trois points ou plus consécutif à une infraction commise pendant le délai probatoire doit être notifié à l'intéressé par lettre recommandée avec demande d'avis de réception, alors que les retraits de points sont normalement notifiés par lettre simple conformément aux dispositions du dernier alinéa de l'article L. 223-3 du même code, le pouvoir réglementaire a tenu compte de l'obligation faite à l'intéressé de se soumettre à une formation dans un délai de quatre mois, sous peine d'une sanction pénale qui ne saurait être prononcée en l'absence d'une preuve certaine de notification, mais n'a pas entendu faire dépendre d'une notification par lettre recommandée avec demande d'avis de réception la légalité du retrait de points. <br/>
<br/>
              5. Par suite, s'il appartient à l'administration de respecter la règle prévue à l'article R. 223-4, la circonstance qu'elle n'est pas en mesure d'établir qu'un retrait de trois points ou plus consécutif à une infraction commise pendant la période probatoire a été notifié à l'intéressée par lettre recommandée avec demande d'avis de réception est sans incidence sur la légalité de ce retrait. Une telle circonstance n'est pas davantage de nature à entacher d'illégalité la décision par laquelle le ministre de l'intérieur constate que le permis a perdu sa validité en raison de ce retrait combiné avec des retraits consécutifs à d'autres infractions. A supposer que l'intéressé n'ait pas reçu auparavant notification de certains des retraits de points, le ministre les lui rend opposables en les mentionnant dans la récapitulation des retraits qu'il fait figurer dans sa décision. Il peut dès lors en tenir compte légalement pour constater que le solde des points est nul. <br/>
<br/>
              Le présent avis sera notifié au président du tribunal administratif d'Amiens, à M. B...A...et au ministre de l'intérieur. <br/>
<br/>
              Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">49-04-01-04-025 POLICE. POLICE GÉNÉRALE. CIRCULATION ET STATIONNEMENT. PERMIS DE CONDUIRE. - RETRAIT DE TROIS POINTS OU PLUS CONSÉCUTIF À DES INFRACTIONS COMMISES PENDANT LA PÉRIODE PROBATOIRE - OBLIGATION DE NOTIFICATION PAR LRAR (ART. R. 223-4 DU CODE DE LA ROUTE) - MÉCONNAISSANCE - 1) INCIDENCE SUR LA LÉGALITÉ DU RETRAIT DE POINTS - ABSENCE - 2) INCIDENCE SUR LA LÉGALITÉ DU CONSTAT DE PERTE DE VALIDITÉ DU PERMIS - ABSENCE.
</SCT>
<ANA ID="9A"> 49-04-01-04-025 1) S'il appartient à l'administration de respecter la règle prévue à l'article R. 223-4 du code de la route, la circonstance qu'elle n'est pas en mesure d'établir qu'un retrait de trois points ou plus consécutif à une infraction commise pendant la période probatoire a été notifié à l'intéressé par lettre recommandée avec accusé de réception (LRAR) est sans incidence sur la légalité de ce retrait.... ,,2) Une telle circonstance n'est pas davantage de nature à entacher d'illégalité la décision par laquelle le ministre de l'intérieur constate que le permis a perdu sa validité en raison de ce retrait combiné avec des retraits consécutifs à d'autres infractions. A supposer que l'intéressé n'ait pas reçu auparavant notification de certains des retraits de points, le ministre les lui rend opposables en les mentionnant dans la récapitulation des retraits qu'il fait figurer dans sa décision. Il peut dès lors en tenir compte légalement pour constater que le solde des points est nul.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
