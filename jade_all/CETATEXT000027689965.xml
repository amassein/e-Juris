<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027689965</ID>
<ANCIEN_ID>JG_L_2013_07_000000367316</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/68/99/CETATEXT000027689965.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 05/07/2013, 367316, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-07-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367316</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; FOUSSARD</AVOCATS>
<RAPPORTEUR>M. Pascal Trouilly</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:367316.20130705</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 2 avril 2013 au secrétariat du contentieux du Conseil d'Etat, présenté pour M. A...B..., demeurant... ; M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1302946 du 7 mars 2013 par laquelle le juge des référés du tribunal administratif de Paris a rejeté sa demande tendant, d'une part, à la suspension de l'exécution de la décision du 4 décembre 2012 du président du conseil général de Paris mettant fin à son droit au revenu de solidarité active ainsi que de la décision du 24 janvier 2013 rejetant son  recours gracieux contre cette décision, d'autre part, à ce que soit ordonné le rétablissement de son droit au revenu de solidarité active à compter du 1er décembre 2012, sous astreinte de 150 euros par jour de retard ;<br/>
<br/>
              2°) de mettre à la charge du département de Paris et de la caisse d'allocations familiales de Paris la somme de 2 000 euros à verser à la SCP F. Rocheteau et C. Uzan-Sarano, son avocat, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pascal Trouilly, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M.B..., et à Me Foussard, avocat du département de Paris ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Paris que, par un courrier du 4 décembre 2012, la caisse d'allocations familiales de Paris a fait savoir à M. B...qu'il ne remplissait plus les conditions pour bénéficier du revenu de solidarité active ; que, par une décision du 24 janvier 2013, le président du conseil général de Paris a rejeté le recours gracieux par lequel M. B...avait sollicité le rétablissement de son droit à l'allocation du revenu de solidarité active ; que M. B...se pourvoit en cassation contre l'ordonnance du 7 mars 2013 par laquelle le juge des référés du tribunal administratif de Paris a rejeté sa demande tendant à la suspension de l'exécution de la décision mettant fin à son droit au revenu de solidarité active et à ce que soit ordonné le rétablissement de ce droit ; <br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; qu'aux termes du second alinéa de l'article R. 522-1 du même code : " A peine d'irrecevabilité, les conclusions tendant à la suspension d'une décision administrative ou de certains de ses effets doivent être présentées par requête distincte de la requête à fin d'annulation ou de réformation et accompagnées d'une copie de cette dernière " ;<br/>
<br/>
              3. Considérant que le juge des référés a rejeté la requête de M. B... comme manifestement irrecevable au motif que la copie de la requête tendant à l'annulation des décisions litigieuses n'était pas jointe à la requête à fin de suspension ; que, toutefois, il ressort des pièces du dossier soumis au juge des référés que la copie du recours en annulation était au nombre des pièces jointes à la demande de suspension, conformément à l'inventaire annexé à cette demande ; que, par suite, M. B...est fondé à soutenir que le juge des référés s'est fondé sur des faits matériellement inexacts ; que si le département de Paris soutient que le recours en annulation était insuffisamment motivé, un tel motif ne peut, en tout état de cause, être substitué au motif retenu à tort par le juge des référés; que, dès lors et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, l'ordonnance attaquée doit être annulée ; <br/>
<br/>
              4. Considérant qu'il y a lieu de renvoyer M. B...devant le juge des référés du tribunal administratif de Paris pour qu'il soit statué sur sa demande ;<br/>
<br/>
              5. Considérant que M. B...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP F. Rocheteau et C. Uzan-Sarano, avocat de M.B..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge du département de Paris une somme de 1 000 euros à verser à la SCP F. Rocheteau et C. Uzan-Sarano ; que les dispositions de l'article L. 761-1 du code de justice administrative font en revanche obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la caisse d'allocations familiales de Paris, qui n'est pas partie à la présente instance ; qu'elles font, de même, obstacle à ce qu'il soit fait droit à la demande du département de Paris présentée au même titre ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Paris du 7 mars 2013 est annulée.<br/>
Article 2 : L'affaire est renvoyée au juge des référés du tribunal administratif de Paris.<br/>
Article 3 : Le département de Paris versera à la SCP F. Rocheteau et C. Uzan-Sarano, avocat de M.B..., une somme de 1 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : Les conclusions du département de Paris présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. A...B...et au département de Paris.<br/>
Copie en sera adressée pour information à la caisse d'allocations familiales de Paris.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
