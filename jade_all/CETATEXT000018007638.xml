<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000018007638</ID>
<ANCIEN_ID/>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/18/00/76/CETATEXT000018007638.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 14/11/2007, 290147</TITRE>
<DATE_DEC>2007-11-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>290147</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin</PRESIDENT>
<AVOCATS>ODENT ; RICARD</AVOCATS>
<RAPPORTEUR>M. Alexandre  Lallet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mlle Courrèges</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 13 février et 31 mai 2006 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme Michelle A, demeurant ... ; Mme A demande au Conseil d'Etat :
              
              1°) d'annuler l'arrêt du 25 octobre 2005 par lequel la cour administrative d'appel de Nantes a, à la demande de la commune de Rouans, d'une part, annulé le jugement du 20 janvier 2005 du tribunal administratif de Nantes ayant annulé la délibération du 23 mai 2003 du conseil municipal de Rouans approuvant la modification du plan d'occupation de sols de la commune, d'autre part, rejeté la demande de Mme A présentée devant le tribunal administratif de Nantes et, enfin, mis à sa charge le versement de la somme de 1 500 euros au titre de l'article L. 761&#143;1 du code de justice administrative ; 
              
              2°) statuant au fond, de rejeter la requête d'appel présentée par la commune de Rouans ;
     
	
              Vu les autres pièces du dossier ;
              
              Vu le code civil ;
              
              Vu le code de l'urbanisme ;
              
              Vu la loi n° 2002-276 du 27 février 2002 ;
              
              Vu la loi n° 2003-590 du 2 juillet 2003 ;
              
              Vu le décret du 5 novembre 1870 relatif à la promulgation des lois et décrets ;
              
              Vu le code de justice administrative ;
              
              
     
              Après avoir entendu en séance publique :
              
              - le rapport de M. Alexandre Lallet, Auditeur,  
              
              - les observations de Me Odent, avocat de Mme A et de Me Ricard, avocat de la commune de Rouans, 
              
              - les conclusions de Mlle Anne Courrèges, Commissaire du gouvernement ;
              
              
              
     
     <br/>Considérant qu'aux termes de l'article L. 122&#143;2 du code de l'urbanisme, dans sa rédaction résultant de la loi du 27 février 2002 : « En l'absence d'un schéma de cohérence territoriale applicable, les zones naturelles et les zones d'urbanisation future délimitées par les plans locaux d'urbanisme des communes ne peuvent pas être ouvertes à l'urbanisation. / Toutefois, une extension limitée de l'urbanisation peut être prévue par les plans locaux d'urbanisme et les cartes communales avec l'accord du préfet. Cet accord est donné après avis de la commission départementale des sites et de la chambre d'agriculture qui apprécient l'impact de l'urbanisation sur l'environnement et les activités agricoles (&#133;) / Les dispositions du présent article ne sont pas applicables dans les communes situées à plus de quinze kilomètres de la périphérie d'une agglomération de plus de 15 000 habitants au sens du recensement général de la population, et à plus de quinze kilomètres du rivage de la mer (&#133;) / Les dispositions du présent article sont applicables à compter du 1er juillet 2002 » ; que si la loi du 2 juillet 2003 a modifié l'article L. 122&#143;2 précité en excluant de son champ d'application l'ouverture à l'urbanisation des zones à urbaniser délimitées avant le 1er juillet 2002, sans abroger ou modifier le dernier alinéa du même article reproduit ci-dessus, il ne ressort pas des dispositions de la loi du 2 juillet 2003 que le législateur ait entendu conférer une portée rétroactive aux modifications apportées par celle-ci à l'article L. 122-2 du code de l'urbanisme ; que, dès lors, ces dispositions, dont l'application n'est pas manifestement impossible en l'absence de décret, sont entrées en vigueur, conformément aux dispositions du décret du 5 novembre 1870 alors en vigueur, un jour franc après la publication de la loi qui les a introduites - soit le 5 juillet 2003, et non le 1er juillet 2002 ;
              
              Considérant que, pour faire droit à la requête de la commune de Rouans dirigée contre le jugement du tribunal administratif de Nantes annulant la délibération du 23 mai 2003 par laquelle le conseil municipal de Rouans avait modifié son plan d'occupation des sols en transformant une zone d'urbanisation future classée Nab en zone urbaine Ubz, la cour administrative d'appel s'est fondée sur ce que les dispositions de l'article L. 122&#143;2 du code de l'urbanisme dans leur rédaction issue de la loi du 2 juillet 2003 étaient applicables, en vertu du dernier alinéa du même article, à compter du 1er juillet 2002, pour en déduire que la délibération litigieuse, relative à une zone d'urbanisation future délimitée avant le 1er juillet 2002, n'était pas soumise aux prescriptions prévues par ce même article ; qu'il résulte de ce qui a été dit ci&#143;dessus qu'elle a, ce faisant, commis une erreur de droit ; que dès lors, et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, son arrêt doit être annulé ;
              
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821&#143;2 du code de justice administrative ;
              
              Considérant qu'il résulte de l'article R. 123&#143;18 du code de l'urbanisme, dans sa rédaction applicable en l'espèce, que les zones qui peuvent être urbanisées à l'occasion soit d'une modification du plan d'occupation des sols soit de la création d'une zone d'aménagement concerté ou de la réalisation d'opérations d'aménagement ou de constructions compatibles avec un aménagement cohérent de la zone tel qu'il est défini par le règlement, peuvent être classées en zone NA ; que le rapport de présentation du plan d'occupation des sols de la commune de Rouans élaboré à l'occasion de sa révision en 2001 définissait la zone Nab comme une « zone d'urbanisation future comprenant l'extension de l'urbanisation sous forme de tissu urbain lâche, en ordre discontinu », précisait que « cette zone sera équipée par l'aménageur » et prévoyait que celle&#143;ci « n'est urbanisable que sous la forme d'opération d'une certaine importance » ; que le règlement du plan d'occupation des sols alors applicable à cette zone autorisait sous conditions les constructions nouvelles à usage d'habitation ainsi que celles destinées aux équipements collectifs, aux commerces, aux établissements artisanaux, aux bureaux ou aux services ; qu'ainsi, les parcelles classées en zone Nab étaient ouvertes à l'urbanisation à la date de la délibération litigieuse ; que, dès lors, en classant en zone urbaine Ubz des parcelles qui étaient antérieurement classées en zone Nab, cette délibération, si elle a assoupli les conditions auxquelles est subordonnée la légalité des constructions sur ces parcelles, n'a pu avoir pour effet d'ouvrir ces dernières à l'urbanisation ; qu'il suit de là que les formalités prescrites par l'article L. 122&#143;2 du code de l'urbanisme préalablement à l'ouverture à l'urbanisation de zones à urbaniser n'étaient, en l'espèce, pas requises ;
              
              Considérant qu'il résulte de ce qui précède que c'est à tort que le tribunal administratif de Nantes s'est fondé sur la méconnaissance des dispositions précitées de l'article L. 122&#143;2 du code de l'urbanisme par la commune de Rouans pour annuler la délibération du 23 mai 2003 de son conseil municipal ;
              
              Considérant, toutefois, qu'il appartient au Conseil d'Etat, saisi du litige par l'effet dévolutif de l'appel, d'examiner les autres moyens soulevés par Mme A devant le tribunal administratif de Nantes ;
              
              Considérant, en premier lieu, qu'aux termes de l'article L. 123&#143;13 du code de l'urbanisme, dans sa rédaction alors applicable : « (&#133;) Un plan local d'urbanisme peut également être modifié par délibération du conseil municipal après enquête publique à condition qu'il ne soit pas porté atteinte à son économie générale et : / - que la modification n'ait pas pour effet de réduire un espace boisé classé ou une protection édictée en raison de la valeur agricole des terres, des risques de nuisance, de la qualité des sites, des paysages ou des milieux naturels ; / - que la modification ne comporte pas de graves risques de nuisance (&#133;) » ; 
              
              Considérant qu'il ressort des pièces du dossier que la modification contestée du plan d'occupation des sols de la commune de Rouans est limitée à un espace d'une superficie de 6 ha 6 a situé en limite nord&#143;ouest du centre du bourg ; qu'elle a pour objet de classer en zone urbaine Ubz des parcelles précédemment inscrites en zone d'urbanisation future Nab, dans la logique de la vocation assignée à une telle zone, et pour lesquelles le rapport de présentation du plan d'occupation des sols révisé en 2001 indiquait qu'elles sont « fortement pressenties pour y recevoir une occupation mixte habitats + commerces » ; que les installations classées pour la protection de l'environnement soumises à déclaration ne sont admises en zone Ubz que si elles sont nécessaires à la vie des habitants du quartier et sous la condition que « soient mises en oeuvre toutes dispositions utiles pour rendre compatibles avec les milieux environnants et pour éviter les pollutions, des nuisances ou des dangers non maîtrisables » ; que, dans ces conditions, la modification en cause ne peut être regardée comme comportant de graves risques de nuisance et ne porte pas atteinte à l'économie générale du plan d'occupation des sols ; que, par suite, la commune de Rouans pouvait légalement recourir à la procédure de modification et non de révision pour procéder à ces changements ;
              
              Considérant, en deuxième lieu, qu'aux termes de l'article R. 123&#143;17 du code de l'urbanisme : « (&#133;) le plan local d'urbanisme ne peut être approuvé qu'après avis de la chambre d'agriculture (&#133;). Il en va de même en cas de révision (&#133;) » ; qu'il ressort des termes mêmes de cet article que la consultation de la chambre d'agriculture n'est requise qu'en cas d'approbation ou de révision du plan local d'urbanisme ; que, par suite, Mme A ne peut utilement soutenir que la délibération litigieuse, qui se borne à modifier le plan d'occupation des sols sans le réviser, serait irrégulière faute de consultation préalable de la chambre d'agriculture ;
              
              Considérant, en troisième et dernier lieu, qu'ainsi qu'il est dit ci&#143;dessus, seules des installations classées pour la protection de l'environnement soumises à déclaration correspondant à des besoins de la population peuvent être autorisées dans la zone Ubz, à condition que toutes les précautions soient prises pour préserver l'environnement et éviter les nuisances ; que l'approbation du règlement autorisant de telles implantations n'est pas, dès lors, entachée d'erreur manifeste d'appréciation ;
              
              Considérant qu'il résulte de ce qui précède que la commune de Rouans est fondée à soutenir que c'est à tort que, par le jugement du 20 janvier 2005 attaqué, le tribunal administratif de Nantes a annulé la délibération du 23 mai 2003 du conseil municipal approuvant la modification du plan d'occupation des sols communal ;
              
              Considérant que les dispositions de l'article L. 761&#143;1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la commune de Rouans, qui n'est pas la partie perdante dans la présente instance ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme A une somme à ce même titre ;
              
     
     <br/>D E C I D E :
--------------
Article 1er : L'arrêt de la cour administrative d'appel de Nantes en date du 25 octobre 2005 et le jugement du tribunal administratif de Nantes en date du 20 janvier 2005 sont annulés. 
              
Article 2 : La demande présentée par Mme A devant le tribunal administratif de Nantes est rejetée.
              
Article 3 : Le surplus des conclusions présenté par la commune de Rouans et par Mme A est rejeté.
              
Article 4 : La présente décision sera notifiée à Mme Michelle A et à la commune de Rouans.
Copie en sera adressée pour information au ministre d'Etat, ministre de l'écologie, du développement et de l'aménagement durables.
 
 
                 
                 <br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D'OCCUPATION DES SOLS ET PLANS LOCAUX D'URBANISME. LÉGALITÉ DES PLANS. MODIFICATION ET RÉVISION DES PLANS. PROCÉDURES DE MODIFICATION. - NOTION D'OUVERTURE À L'URBANISATION AU SENS DE L'ARTICLE L. 122-2 DU CODE DE L'URBANISME - ABSENCE - PARCELLES CLASSÉES EN ZONE NAB OUVERTES À L'URBANISATION ANTÉRIEUREMENT À LA DÉLIBÉRATION AUTORISANT LA MODIFICATION DU PLAN D'OCCUPATION DES SOLS - CLASSEMENT EN ZONE UBZ NE FAISANT QU'ASSOUPLIR LES CONDITIONS DE LÉGALITÉ DES CONSTRUCTIONS SUR CES PARCELLES [RJ1].
</SCT>
<ANA ID="9A">Il résulte de l'article R. 123-18 du code de l'urbanisme, dans sa rédaction applicable en l'espèce, que les zones qui peuvent être urbanisées à l'occasion soit d'une modification du plan d'occupation des sols soit de la création d'une zone d'aménagement concerté ou de la réalisation d'opérations d'aménagement ou de constructions compatibles avec un aménagement cohérent de la zone tel qu'il est défini par le règlement, peuvent être classées en zone NA. Le rapport de présentation du plan d'occupation des sols de la commune de Rouans, élaboré à l'occasion de sa révision en 2001, définissait la zone NAb comme une « zone d'urbanisation future comprenant l'extension de l'urbanisation sous forme de tissu urbain lâche, en ordre discontinu », précisait que « cette zone serait équipée par l'aménageur » et prévoyait que celle-ci « n'était urbanisable que sous la forme d'opération d'une certaine importance ». Le règlement du plan d'occupation des sols alors applicable à cette zone autorisait sous conditions les constructions nouvelles à usage d'habitation ainsi que celles destinées aux équipements collectifs, aux commerces, aux établissements artisanaux, aux bureaux ou aux services. Ainsi, les parcelles classées en zone NAb étaient ouvertes à l'urbanisation à la date de la délibération litigieuse par laquelle le conseil municipal avait modifié le plan d'occupation des sols. Dès lors, en classant en zone urbaine UBz des parcelles qui étaient antérieurement classées en zone NAb, cette délibération, si elle a assoupli les conditions auxquelles est subordonnée la légalité des constructions sur ces parcelles, n'a pas pu avoir pour effet d'ouvrir ces dernières à l'urbanisation. Il suit de là que les formalités prescrites par l'article L. 122-2 du code de l'urbanisme préalablement à l'ouverture à l'urbanisation de zones à urbaniser n'étaient, en l'espèce, pas requises.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
