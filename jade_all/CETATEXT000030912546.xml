<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030912546</ID>
<ANCIEN_ID>JG_L_2015_07_000000372907</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/91/25/CETATEXT000030912546.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème SSR, 17/07/2015, 372907, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372907</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>Mme Julia Beurton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2015:372907.20150717</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire, enregistré le 17 avril 2015 au secrétariat du contentieux du Conseil d'Etat, la caisse autonome de retraite des médecins de France, M.E..., M.D..., M.I..., M. J...et Mme C...demandent au Conseil d'Etat, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de leur requête tendant à l'annulation pour excès de pouvoir des décisions implicites de rejet nées du silence gardé par le Premier ministre et par le ministre des affaires sociales et de la santé sur leur demande tendant à l'abrogation des articles D. 134-2 à D. 134-9 du code de la sécurité sociale ou, à titre subsidiaire, des articles D. 134-3 à D. 134-5, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des articles L. 134-1 et L. 134-2 du code de la sécurité sociale. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de la sécurité sociale, notamment ses articles L. 134-1 et L. 134-2 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Julia Beurton, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la caisse autonome de retraite des médecins de France, de M. A...E..., de M. G...D..., de M. F...I..., de M. B...J...et de Mme H...C...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 134-1 du code de la sécurité sociale : " Il est institué une compensation entre les régimes obligatoires de sécurité sociale comportant un effectif minimum, autres que les régimes complémentaires au sens des articles L. 635-1, L. 644-1 et L. 921-4 du présent code et du I de l'article 1050 du code rural. Cette compensation porte sur les charges de l'assurance vieillesse au titre des droits propres. / La compensation tend à remédier aux inégalités provenant des déséquilibres démographiques et des disparités de capacités contributives entre les différents régimes. Toutefois, tant que les capacités contributives de l'ensemble des non-salariés ne pourront être définies dans les mêmes conditions que celles des salariés, la compensation entre l'ensemble des régimes de salariés et les régimes de non-salariés aura uniquement pour objet de remédier aux déséquilibres démographiques. / La compensation prévue au présent article est calculée sur la base d'une prestation de référence et d'une cotisation moyenne ; elle est opérée après application des compensations existantes. / Les soldes qui en résultent entre les divers régimes sont fixés par arrêtés interministériels, après consultation de la commission de compensation prévue à l'article L. 114-3 " ; qu'aux termes de l'article L. 134-2 du même code : " Des décrets fixent les conditions d'application de l'article L. 134-1 et déterminent notamment : / 1°) l'effectif minimum nécessaire pour qu'un régime de sécurité sociale puisse participer à la compensation instituée par cet article ; / 2°) les modalités de détermination des bases de calcul des transferts opérés au titre de la compensation prévue à cet article " ; <br/>
<br/>
              3. Considérant que les articles L. 134-1 et L. 134-2 du code de la sécurité sociale sont applicables au présent litige ; que ces dispositions n'ont pas déjà été déclarées conformes à la Constitution par le Conseil constitutionnel ; que le moyen tiré de ce qu'elles portent atteinte aux droits et libertés garantis par la Constitution, notamment au principe d'égalité devant les charges publiques garanti par l'article 13 de la Déclaration des droits de l'homme et du citoyen en ce qu'elles instaurent une compensation entre l'ensemble des régimes de retraite de base de salariés et les régimes de non-salariés fondée sur des bases uniquement démographiques, soulève une question présentant un caractère sérieux ; qu'ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité à la Constitution des articles L. 134-1 et L. 134-2 du code de la sécurité sociale est renvoyée au Conseil constitutionnel.<br/>
<br/>
Article 2 : Il est sursis à statuer sur la requête de la caisse autonome de retraite des médecins de France, de MM.E..., D..., I...et J...et K...C...jusqu'à ce que le Conseil constitutionnel ait tranché la question de constitutionnalité ainsi soulevée.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la caisse autonome de retraite des médecins de France, premier requérant dénommé, au Premier ministre et à la ministre des affaires sociales, de la santé et des droits des femmes.<br/>
Les autres requérants seront informés de la présente décision par Me Foussard, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
