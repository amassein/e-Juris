<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034078389</ID>
<ANCIEN_ID>JG_L_2017_02_000000395722</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/07/83/CETATEXT000034078389.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 23/02/2017, 395722, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-02-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395722</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:395722.20170223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Marseille, d'une part, d'annuler pour excès de pouvoir la décision du 14 octobre 2008 par laquelle le président du conseil d'administration de La Poste a prononcé à son encontre la sanction d'exclusion temporaire de fonctions pour une durée de deux ans et, d'autre part, de condamner La Poste à lui verser une indemnité correspondant aux traitements dont il a été privé entre la date de l'exclusion temporaire et la date de sa réintégration.<br/>
<br/>
              Par un jugement n° 0808803 du 25 novembre 2010, le tribunal administratif de Marseille a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 11MA00325 du 17 janvier 2014, la cour administrative d'appel de Marseille, réformant le jugement du tribunal administratif sur l'appel de M.B..., a annulé la décision du 14 octobre 2008 mais rejeté comme irrecevables les conclusions indemnitaires.<br/>
<br/>
              Par une décision n° 376598, 381828 du 27 février 2015, le Conseil d'Etat, statuant au contentieux, saisi par La Poste, a annulé l'arrêt de la cour administrative d'appel en tant qu'il avait annulé la décision du 14 octobre 2008 et réformé en conséquence le jugement du tribunal administratif puis a renvoyé, dans cette mesure, l'affaire à la cour administrative de Marseille.<br/>
<br/>
              Par un second arrêt n° 15MA01156 du 3 novembre 2015, la cour administrative d'appel de Marseille a, de nouveau, annulé la décision du 14 octobre 2008 ainsi que le jugement du tribunal administratif en tant qu'il avait rejeté la demande de M. B...dirigée contre cette décision.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 31 décembre 2015 et 31 mars 2016 au secrétariat du contentieux, La Poste demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce dernier arrêt de la cour administrative d'appel de Marseille ;<br/>
<br/>
              2°) de mettre à la charge de M. B...la somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - la loi n° 90-568 du 2 juillet 1990 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Thomas Haas, avocat de La Poste. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des énonciations de l'arrêt attaqué que M. B..., fonctionnaire de La Poste, facteur au centre courrier de Marseille, a fait l'objet, par décision du 14 octobre 2008, d'une sanction disciplinaire d'exclusion temporaire de fonctions pour une durée de deux ans pour des faits de refus d'obéissance caractérisés et récurrents, d'agression verbale et physique à l'encontre de son supérieur hiérarchique, de dégradation d'une des portes d'accès au local de la direction et d'attitude dilatoire au cours de l'enquête interne ; que La Poste se pourvoit en cassation contre l'arrêt du 3 novembre 2015 par lequel la cour administrative d'appel de Marseille, sur renvoi du Conseil d'Etat après cassation, a prononcé l'annulation pour excès de pouvoir de la décision du 14 octobre 2008 et annulé, dans cette mesure, le jugement du tribunal administratif de Marseille qui avait rejeté la demande présentée en première instance par M. B...contre cette décision ;<br/>
<br/>
              2.	Considérant qu'en vertu de l'article 66 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, applicables aux fonctionnaires de La Poste en vertu de la loi du 2 juillet 1990 relative à l'organisation du service public de la poste et à France Télécom, les sanctions disciplinaires susceptibles d'être infligées aux fonctionnaires de l'Etat sont réparties en quatre groupes ; que relèvent du premier groupe les sanctions de l'avertissement et du blâme, du deuxième groupe celles de la radiation du tableau d'avancement, de l'abaissement d'échelon, de l'exclusion temporaire de fonctions pour une durée maximale de quinze jours et du déplacement d'office, du troisième groupe celles de la rétrogradation et de l'exclusion temporaire de fonctions pour une durée de trois mois à deux ans et du quatrième groupe celles de la mise à la retraite d'office et de la révocation ; que, selon le dernier alinéa de l'article 66, l'exclusion temporaire de fonctions peut être assortie d'un sursis total ou partiel, sans pouvoir avoir pour effet, pour l'exclusion relevant du troisième groupe, de ramener la durée de l'exclusion à moins d'un mois ; <br/>
<br/>
              3.	Considérant qu'il appartient au juge de l'excès de pouvoir, saisi de moyens en ce sens, de rechercher si les faits reprochés à un agent public ayant fait l'objet d'une sanction disciplinaire constituent des fautes de nature à justifier une sanction et si la sanction retenue est proportionnée à la gravité de ces fautes ; <br/>
<br/>
              4.	Considérant que la constatation et la caractérisation des faits reprochés à l'agent relèvent, dès lors qu'elles sont exemptes de dénaturation, du pouvoir souverain des juges du fond ; que le caractère fautif de ces faits est susceptible de faire l'objet d'un contrôle de qualification juridique de la part du juge de cassation ; que l'appréciation du caractère proportionné de la sanction au regard de la gravité des fautes commises relève, pour sa part, de l'appréciation des juges du fond et n'est susceptible d'être remise en cause par le juge de cassation que dans le cas où la solution qu'ils ont retenue quant au choix, par l'administration, de la sanction est hors de proportion avec les fautes commises ;<br/>
<br/>
              5.	Considérant, en premier lieu, que, s'agissant du manquement reproché à M. B...consistant à avoir observé une attitude dilatoire au cours de l'enquête administrative diligentée par La Poste sur les événements qui se sont produits les 7 mars, 21 et 23 mai 2008, la cour administrative d'appel de Marseille a jugé que ce fait, dans les circonstances de l'espèce et alors même que M. B...avait nié avoir eu lors de ces événements le comportement qui lui était reproché, ne présentait pas le caractère d'une faute disciplinaire ; qu'en jugeant ainsi, en l'état de ses constatations souveraines qui sont exemptes de dénaturation, que l'attitude observée par M. B...au cours de l'enquête interne diligentée par La Poste avant la procédure disciplinaire ne présentait pas un caractère fautif, la cour n'a pas inexactement qualifié les faits de l'espèce ;<br/>
<br/>
              6.	Considérant, en deuxième lieu, que des pièces nouvelles ne peuvent être produites pour la première fois en cassation ; qu'aucune pièce versée au dossier des juges du fond n'établissait que M. B...avait déjà été sanctionné à titre disciplinaire au cours de sa carrière ; que La Poste ne saurait, par suite, soutenir que la cour administrative d'appel aurait dénaturé les pièces du dossier en retenant que M. B...n'avait jamais précédemment fait l'objet de sanctions ; <br/>
<br/>
              7.	Considérant, en troisième lieu, que la cour a jugé que les faits reprochés à M. B...de refus d'obéissance récurrents à l'encontre de ses supérieurs hiérarchiques, d'agression verbale et d'agression physique à l'encontre de son supérieur hiérarchique, et de dégradation volontaire de la porte d'accès à la direction étaient caractérisés et constituaient des fautes disciplinaires de nature à justifier une sanction ; qu'elle a toutefois estimé, après avoir relevé que ces fautes avaient été commises sur une durée très courte, entre mars et mai 2008 à l'occasion d'un conflit social aigu, que l'agression physique n'avait pas excédé une bousculade et que M. B...n'avait pas été antérieurement sanctionné, que la sanction d'exclusion temporaire de fonctions pour une durée de deux ans, qui relève du troisième groupe en vertu des dispositions de l'article 66 de la loi du 11 janvier 1984, était disproportionnée par rapport aux fautes commises ;<br/>
<br/>
              8.	Considérant qu'en statuant ainsi, la cour administrative d'appel, qui n'a pas omis de tenir compte, pour porter son appréciation, de la nature des fonctions de l'intéressé et des obligations qui incombent aux agents de La Poste, n'a pas commis d'erreur de droit ; <br/>
<br/>
              9.	Considérant qu'en jugeant que la sanction de l'exclusion temporaire pour une durée de deux ans était disproportionnée par rapport aux fautes commises, la cour s'est livrée à une appréciation des faits de l'espèce qui n'est pas susceptible d'être remise en cause par le juge de cassation, dès lors qu'il ne ressort pas des pièces du dossier soumis aux juges du fond que le prononcé d'une sanction moins lourde que l'exclusion temporaire pour une durée de deux ans sans sursis aurait été, en raison de son caractère insuffisant, hors de proportion avec les fautes commises par M. B...; <br/>
<br/>
              10.	Considérant qu'il résulte de ce qui précède que La Poste n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              11.	Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de M.B..., qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de La Poste est rejeté.<br/>
Article 2 : La présente décision sera notifiée à La Poste et à M. A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
