<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043310121</ID>
<ANCIEN_ID>JG_L_2021_03_000000447182</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/31/01/CETATEXT000043310121.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 29/03/2021, 447182, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>447182</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Thomas Pez-Lavergne</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:447182.20210329</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. G... F... a demandé au tribunal administratif de Melun d'annuler les opérations électorales qui se sont tenues le 28 juin 2020 pour l'élection des conseillers municipaux et communautaires de la commune de Voulx (Seine-et-Marne). <br/>
<br/>
              Par un jugement n° 2004870 du 5 novembre 2020, le tribunal administratif de Melun a rejeté sa protestation.<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 2 décembre 2020 et 10 mars 2021 au secrétariat du contentieux du Conseil d'Etat, M. F... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) d'annuler les opérations électorales ;<br/>
<br/>
              3°) de mettre à la charge de M. D... A... la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Pez-Lavergne, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M. D... A... ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 18 mars 2021, présentée par M. F... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. A l'issue du second tour des élections municipales et communautaires de la commune de Voulx qui s'est déroulé le 28 juin 2020, la liste " Agir pour l'avenir de Voulx ", conduite par M. D... A..., a obtenu 416 voix (50,91 % des suffrages exprimés). Cette liste ayant obtenu la majorité des suffrages exprimés, 15 des 19 sièges que compte le conseil municipal de cette commune lui ont été attribués, en application des dispositions de l'article L. 262 du code électoral. Quatre candidats figurant sur la liste " Tournez Voulx vers l'avenir ", conduite par M. G... F..., qui a obtenu 353 voix (43,20 % des suffrages exprimés), ont également été élus conseillers municipaux. Enfin, la liste " Ensemble vivons Voulx ", conduite par Mme E... C..., qui a obtenu 48 voix (3,80 % des suffrages exprimés), ne s'est vue attribuer aucun siège. Par un jugement du 5 novembre 2020, le tribunal administratif de Melun a rejeté la protestation par laquelle M. F... demandait l'annulation de ces opérations électorales.<br/>
<br/>
              2. En premier lieu, il résulte de l'instruction que, lors de la séance du conseil municipal du 19 décembre 2019, M. B..., adjoint au maire chargé des finances de la commune et candidat sur la liste de M. A..., a fait état des importants impayés d'eau à l'égard de la commune d'un futur candidat à l'élection municipale, sur un ton critique et dans des termes qui permettaient d'identifier M. F..., lequel s'est levé et a quitté la salle après avoir répondu à ces allégations. Plusieurs témoignages et pièces produits par l'appelant attestent que ces impayés, dont l'intéressé reconnaît d'ailleurs l'existence et le montant même s'il conteste le bien-fondé des sommes en cause, ont ensuite constitué un thème important de la campagne électorale. Dès lors, d'une part, qu'il n'est pas contesté que M. F... a pu répondre à ces attaques et que, d'autre part, les critiques émises au cours de la campagne quant à ces impayés n'ont pas dépassé les limites de la polémique électorale, la divulgation de ces informations, en décembre 2019, et leur discussion ultérieure n'ont pas été de nature à porter atteinte à la sincérité du scrutin qui s'est tenu en juin 2020.<br/>
<br/>
              3. En deuxième lieu, s'il n'est pas contesté que plusieurs affiches de la liste " Tournez Voulx vers l'avenir " ont subi des dégradations, y compris par l'apposition de graffitis injurieux, dans la nuit du 7 au 8 mars 2020, ces agissements ponctuels, pour regrettables qu'ils soient, ne peuvent être regardés, compte tenu du délai les séparant des opérations électorales, comme étant de nature à altérer la sincérité du scrutin.<br/>
<br/>
              4. En troisième lieu, les griefs tirés de la méconnaissance des articles L. 52-1 et L. 52-8 du code électoral ont été invoqués tardivement devant les premiers juges et sont donc irrecevables.<br/>
<br/>
              5. En quatrième lieu, si M. A..., maire sortant, a distribué des courriers recommandés pendant la période de confinement du printemps 2020, en accord avec les services postaux, M. F... n'apporte aucun élément au soutien de ses allégations selon lesquelles il aurait ainsi poursuivi sa campagne électorale.<br/>
<br/>
              6. En cinquième lieu, M. F..., auquel le maire a, par deux courriers du 25 novembre 2020 et 10 février 2021, refusé l'utilisation d'un local communal pour y tenir une réunion publique, n'établit aucunement que d'autres listes, y compris celle de M. A..., auraient bénéficié d'une telle faculté, la séance du conseil municipal du 19 décembre 2019 ne pouvant à cet égard être assimilée à une réunion partisane. Par suite, son moyen tiré de ce que le maire aurait ainsi rompu l'égalité entre les candidats ne peut qu'être écarté.<br/>
<br/>
              7. En sixième lieu, si M. F... soutient que la liste du maire sortant aurait versé aux électeurs des sommes d'argent afin d'obtenir leur suffrage, les quelques témoignages qu'il produit, qui émanent pour la plupart de membres de sa liste, ne permettent pas d'établir l'existence de tels achats de vote. Il ne résulte pas davantage de l'instruction, et notamment des témoignages dont se prévaut M. F..., que cette liste aurait fait pression sur des électeurs pour qu'ils expriment leur vote en sa faveur.<br/>
<br/>
              8. En septième lieu, les candidats de la liste " Tournez Voulx vers l'avenir " ont eu la possibilité de répondre, par une diffusion du lendemain, au tract distribué le jeudi 25 juin 2020 par la liste du maire sortant, tract qui n'excédait pas par son contenu les limites de la polémique électorale et ne comportait pas d'éléments nouveaux. La diffusion de ce tract ne peut ainsi être regardée comme constitutive d'une manoeuvre.<br/>
<br/>
              9. En huitième lieu, en vertu de l'article L. 260 du code électoral, applicable aux communes de 1 000 habitants et plus : " Les conseillers municipaux sont élus au scrutin de liste à deux tours, avec dépôt de listes comportant au moins autant de candidats que de sièges à pourvoir, et au plus deux candidats supplémentaires, sans adjonction ni suppression de noms et sans modification de l'ordre de présentation, sous réserve de l'application des dispositions prévues au deuxième alinéa de l'article L. 264 ". Aux termes de l'article L. 66 de ce code : " Les bulletins ne contenant pas une désignation suffisante (...) n'entrent pas en compte dans le résultat du dépouillement ". Selon l'article R. 66-2 du même code : " Sont nuls et n'entrent pas en compte dans le résultat du dépouillement : / (...) / 2° Les bulletins non conformes aux dispositions de l'article L. 52-3 ; (...) ". Aux termes de l'article L. 52-3 de ce code : " Les bulletins de vote ne peuvent pas comporter : / 1° D'autres noms de personne que celui du ou des candidats ou de leurs remplaçants éventuels (...) ". Ces dispositions n'ont pas pour effet de rendre nuls les suffrages dès lors que les électeurs ont émis un vote contenant une désignation suffisante de la liste en faveur de laquelle ils ont entendu se prononcer.<br/>
<br/>
              10. Il est constant que plusieurs erreurs matérielles affectaient les bulletins de la liste " Agir pour l'avenir de Voulx ", tenant à l'omission de prénoms de certains candidats, à la mention du nom d'épouse de certaines colistières de M. A... et à la reprise incomplète du nom de l'un des candidats. Toutefois, eu égard au mode de scrutin, qui impliquait que les électeurs, en application des dispositions de l'article L. 260 du code électoral, votent pour une liste complète, et à la circonstance que ces inexactitudes, qui ne faisaient pas obstacle à l'identification des candidats par les électeurs et ne concernaient que des candidats figurant en sixième position puis au-delà de la dixième position, ces imprécisions n'ont ni pu induire les électeurs en erreur sur l'identité de la liste pour laquelle ils votaient, ni méconnu les dispositions combinées des articles L. 52-3 et R. 66-2 du code électoral. Dans ces conditions, les électeurs ayant utilisé ces bulletins ont clairement manifesté leur intention de voter pour la liste conduite par M. A.... L'appelant n'est donc pas fondé à soutenir que ces bulletins auraient dû être écartés comme nuls. <br/>
<br/>
<br/>
              11. En neuvième lieu, aux termes de l'article R. 117-4 du code électoral : " Dans les communes de 1 000 habitants et plus, les bulletins de vote doivent comporter, sur leur partie gauche, précédé des termes " Liste des candidats au conseil municipal ", le titre de la liste des candidats au mandat de conseiller municipal, ainsi que le nom de chaque candidat composant la liste dans l'ordre de présentation et, pour tout candidat ressortissant d'un Etat membre de l'Union européenne autre que la France, l'indication de sa nationalité. (...) "<br/>
<br/>
              12. Il résulte de l'instruction que le nom de Mme H..., candidate de la liste " Agir pour l'avenir de Voulx " et ressortissante du Portugal, était suivi sur les bulletins de vote de l'adjectif " Portugaise ", placé entre parenthèses. Contrairement à ce que soutient M. F..., cette désignation est conforme aux prescriptions précitées de l'article R. 117-4 du code électoral.<br/>
<br/>
              13. En dixième et dernier lieu, le premier alinéa de l'article L. 62 du code électoral dispose que : " A son entrée dans la salle du scrutin, l'électeur (...) prend, lui-même, une enveloppe. Sans quitter la salle du scrutin, il doit se rendre isolément dans la partie de la salle aménagée pour le soustraire aux regards pendant qu'il met son bulletin dans l'enveloppe ; (...) ".<br/>
<br/>
              14. Si l'appelant soutient que plusieurs électeurs du bureau n° 2 auraient exprimé leur suffrage sans se rendre dans l'isoloir, cette circonstance, à la supposer établie, n'est pas de nature à entraîner l'annulation de l'élection dès lors qu'il n'est pas établi, ni même allégué, que le nombre d'électeurs qui se seraient soustraits à cette obligation serait de nature à modifier le résultat du scrutin.<br/>
<br/>
              15. Il résulte de tout ce qui précède que M. F... n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Melun a rejeté sa protestation. Ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par conséquent, qu'être rejetées. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par les défendeurs sur le même fondement.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de M. F... est rejetée.<br/>
<br/>
Article 2 : Les conclusions présentées par M. A... et autres au titre de l'article L. 7611 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. G... F..., à M. D... A..., représentant unique pour l'ensemble des défendeurs ainsi qu'au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
