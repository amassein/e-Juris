<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000018396840</ID>
<ANCIEN_ID/>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/18/39/68/CETATEXT000018396840.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 07/03/2008, 298460, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2008-03-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>298460</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Arrighi de Casanova</PRESIDENT>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Christine  Grenier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mlle Courrèges Anne</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 30 octobre 2006 et 30 janvier 2007 au secrétariat du contentieux du Conseil d'Etat, présentés pour le SYNDICAT NATIONAL DES BIOLOGISTES DES HOPITAUX, dont le siège est 28, boulevard Pasteur à Paris (75015), représenté par son président ; le syndicat demande au Conseil d'Etat :
              
              1°) d'annuler pour excès de pouvoir le décret n° 2006-717 du 19 juin 2006 relatif aux personnels médicaux, pharmaceutiques et odontologiques hospitaliers et modifiant le code de la santé publique (dispositions réglementaires), ainsi que la décision implicite par laquelle le ministre de la santé et des solidarités a rejeté sa demande du 27 juin 2006 dirigée contre ce décret ;
              
              2°) de mettre à la charge de l'Etat le versement de la somme de 3 500 euros au titre de l'article L. 761&#143;1 du code de justice administrative ;
              
     
	
              Vu les autres pièces du dossier ;
              
              Vu la Constitution, notamment son Préambule ;
              
              Vu le code de la santé publique ;
              
              Vu l'ordonnance n° 2004-688 du 12 juillet 2004 ;
              
              Vu l'ordonnance n° 2005-1112 du 1er septembre 2005 ;
              
              Vu le décret n° 2005-840 du 20 juillet 2005 ; 
              
              Vu le code de justice administrative ;
              
     
              Après avoir entendu en séance publique :
              
              - le rapport de Mme Christine Grenier, chargée des fonctions de Maître des requêtes,  
              
              - les observations de la SCP Waquet, Farge, Hazan, avocat du SYNDICAT NATIONAL DES BIOLOGISTES DES HOPITAUX, 
              
              - les conclusions de Mlle Anne Courrèges, Commissaire du gouvernement ;
              
              
     
     <br/>
              Sur la légalité externe :
              
              Considérant, en premier lieu, qu'il ressort de la copie de la minute de la section sociale du Conseil d'Etat, telle qu'elle a été produite au dossier par le ministre, que le moyen tiré de ce que le texte publié diffèrerait du projet initial du Gouvernement ou de celui adopté par la section sociale manque en fait ;
              
              Considérant, en deuxième lieu, que ni le huitième alinéa du Préambule de la Constitution de 1946, aux termes duquel « tout travailleur participe, par l'intermédiaire de ses délégués, à la détermination collective des conditions de travail », ni aucune autre disposition n'imposaient au pouvoir réglementaire de consulter le syndicat requérant préalablement à l'édiction du décret attaqué ; que le moyen tiré du défaut de consultation préalable du requérant ne saurait, dès lors, être accueilli ;
              
              Considérant, en troisième lieu, qu'aux termes de l'article R. 6141&#143;1 du code de la santé publique : « Le conseil supérieur des hôpitaux peut être appelé à donner son avis sur les questions relatives à l'organisation et au fonctionnement administratif, financier et médical des établissements de santé ainsi que sur les questions relatives au statut des différentes catégories de personnel médical qui y sont attachées » ; qu'il résulte des termes mêmes de ces dispositions que la consultation du conseil supérieur des hôpitaux ne revêt pas un caractère obligatoire ; que, par suite, le moyen tiré du défaut de consultation de cet organisme doit être écarté ;
              
              Sur la légalité interne :
              
              Considérant, en premier lieu, qu'en édictant le décret attaqué, le pouvoir réglementaire se borne à tirer les conséquences de la suppression, par l'ordonnance du 1er septembre 2005, du terme de « biologiste » de plusieurs dispositions législatives du code de la santé publique ; qu'en vertu de l'article R. 6152&#143;302 de ce code régissant les conditions d'accès au corps des praticiens hospitaliers, cette spécialité peut être exercée par des médecins ou des pharmaciens ; que les modifications issues du décret attaqué n'ont, contrairement à ce que soutient le requérant, ni pour objet ni pour effet de supprimer la catégorie des praticiens hospitaliers qui portent le titre de biologiste dans les établissements publics de santé, et qui résulte notamment des articles R. 6152&#143;3, R. 6152&#143;203 et R. 6152&#143;302 ;
              
              Considérant, en deuxième lieu, que, contrairement à ce que soutient le requérant, le décret attaqué n'a ni pour objet ni pour effet de modifier la composition de l'ordre national des pharmaciens, notamment de sa section G qui comprend les pharmaciens biologistes, définie par l'article L. 4231&#143;2 de ce code, non plus que les conditions de fonctionnement des laboratoires d'analyse de biologie médicale et le mode de désignation de leur directeur définies par les articles L. 6211&#143;1 et suivants du code de la santé publique, lesquels ne concernent au demeurant pas les praticiens hospitaliers ; qu'il ne modifie pas davantage les règles de déontologie médicale, issues des articles R. 4235&#143;71 à R. 4235&#143;77 de ce code, qui sont propres aux pharmaciens biologistes ;
              
              Considérant, en troisième lieu, qu'il appartient à chaque établissement de santé participant au service public hospitalier d'assurer la continuité des soins, y compris dans les structures de biologie médicale, et de veiller à ce que les patients reçoivent des soins appropriés à leur état ; que l'article R. 6152&#143;28 du code de la santé publique, s'il confie la responsabilité médicale d'assurer la continuité des soins aux médecins et autres membres du corps médical de l'établissement, prévoit, d'une part, dans son 1°, que les médecins, odontologistes, pharmaciens et autres membres du corps médical assurent le travail de jour et de nuit dans les services organisés en temps continu dans lesquels ils sont affectés et, d'autre part, dans son 2°, qu'ils participent à la continuité des soins dans les autres services ou départements de l'établissement public de santé ; que, dès lors, le syndicat requérant n'est fondé à soutenir ni que les pharmaciens biologistes des établissements publics de santé ne participent pas à la continuité des soins dans les structures dans lesquelles ils sont affectés et, au&#143;delà, de l'établissement, ni que l'article R. 6152&#143;28, qui ne méconnaît aucune disposition législative, serait entaché d'une illégalité ; que, pour les mêmes motifs, le moyen tiré de l'illégalité de l'article R. 6152&#143;221, dans sa rédaction issue du décret attaqué qui reprend les mêmes dispositions s'agissant des praticiens exerçant à temps partiel, doit être écarté ; 
              
              Considérant, en quatrième lieu, qu'il résulte des dispositions combinées du 3° de l'article R. 6152&#143;28 et de l'article R. 6152&#143;31 du code de la santé publique auquel il renvoie, que le remplacement des praticiens hospitaliers pour congés ou absence est assuré par les praticiens de même discipline exerçant dans le même établissement ; qu'il suit de là que le moyen tiré de ce que le remplacement des médecins biologistes ne pourrait plus être assuré par des pharmaciens biologistes n'est pas fondé ;
              
              Considérant, en cinquième lieu, que le XII de l'article 2 du décret attaqué, qui se borne à supprimer le mot « biologiste » de l'article R. 6125&#143;221 auquel renvoie l'article R. 6152&#143;222, n'a ni pour objet, ni pour effet, de modifier les conditions d'exercice, par les praticiens hospitaliers biologistes exerçant à temps partiel, d'une activité libérale ; que les restrictions posées par cette disposition, s'agissant notamment des activités pouvant être exercées par les pharmaciens, ne découlent pas du décret attaqué ; qu'il suit de là que le moyen tiré de ce qu'il modifierait les conditions d'exercice de ces praticiens est inopérant ; 
              
              Considérant, enfin, que le renvoi, par l'article R. 6152&#143;201 du code de la santé publique, aux dispositions des articles L. 6411&#143;5 et L. 6414&#143;22 relatifs à l'établissement de santé de Mayotte et à ses personnels, qui ont été abrogées par l'ordonnance du 12 juillet 2004, ne résulte pas du décret litigieux mais de la codification de cet article opérée par le décret du 20 juillet 2005, à l'encontre duquel les délais de recours sont expirés ; que, dès lors, le moyen tiré de l'illégalité dont serait entachée cette disposition ne peut qu'être écarté ; qu'en outre, le moyen tiré de l'illégalité qui affecterait, pour les mêmes motifs, l'article R. 6152&#143;12, qui ne comporte aucune référence aux articles L. 6411&#143;5 et L. 6414&#143;22 et qui n'a, au surplus, pas été modifié par le décret du 19 juin 2006, est inopérant ;
              
              Considérant qu'il résulte de tout ce qui précède, et sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par le ministre, que la requête du SYNDICAT NATIONAL DES BIOLOGISTES DES HOPITAUX doit être rejetée, y compris, par voie de conséquence, ses conclusions présentées au titre de l'article L. 761&#143;1 du code de justice administrative ;
              
     
     <br/>D E C I D E :
--------------
Article 1er : La requête du SYNDICAT NATIONAL DES BIOLOGISTES DES HOPITAUX est rejetée
Article 2 : La présente décision sera notifiée au SYNDICAT NATIONAL DES BIOLOGISTES DES HOPITAUX, au Premier ministre et au ministre de la santé, de la jeunesse et des sports.
                 
                 <br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
