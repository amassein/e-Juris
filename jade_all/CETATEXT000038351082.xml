<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038351082</ID>
<ANCIEN_ID>JG_L_2019_04_000000411862</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/35/10/CETATEXT000038351082.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 08/04/2019, 411862, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-04-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411862</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL ; SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>Mme Laurence Franceschini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:411862.20190408</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance n° 1702519 du 26 juin 2017, enregistrée le même jour au secrétariat du contentieux du Conseil d'Etat, le président de la 1ère chambre du tribunal administratif de Rennes a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée à ce tribunal par M. B...A....<br/>
<br/>
              Par cette requête, un mémoire de régularisation et un nouveau mémoire, enregistrés respectivement les 31 mai et 16 novembre 2017 et 16 mars 2018, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 28 mars 2017 de la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat, portant déclaration d'utilité publique d'un ouvrage de transport d'électricité, avec mise en compatibilité des documents d'urbanisme des communes d'Erquy, Hénansal et Saint-Alban ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ;<br/>
              - le code de l'énergie ;<br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laurence Franceschini, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de M. A...et à la SCP Sevaux, Mathonnet, avocat de la société RTE.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par un arrêté du 28 mars 2017, le ministre chargé de l'énergie a déclaré d'utilité publique, en vue de l'institution de servitudes, les travaux tendant à la création d'une liaison électrique à 225 000 volts sous-marine et souterraine entre le poste de livraison de la société Ailes Marines SAS, qui exploite un parc éolien situé au large de la commune de Saint-Brieuc, et le poste RTE de la Doberie, avec mise en compatibilité des documents d'urbanisme des communes d'Erquy, Hénansal et Saint-Alban.<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              2. En vertu des 1° et 6° de l'article R. 123-9 du code de l'environnement dans leur rédaction alors en vigueur, l'arrêté d'ouverture de l'enquête publique précise " l'objet de l'enquête, notamment les caractéristiques principales du projet, plan ou programme, la date à laquelle celle-ci sera ouverte et sa durée ", ainsi que " Le cas échéant, la date et le lieu des réunions d'information et d'échange envisagées ". Aux termes des dispositions du I et du II de l'article R. 123-11 du même code dans sa version alors en vigueur : " I. - Un avis portant les indications mentionnées à l'article R. 123-9 à la connaissance du public est publié en caractères apparents quinze jours au moins avant le début de l'enquête et rappelé dans les huit premiers jours de celle-ci dans deux journaux régionaux ou locaux diffusés dans le ou les départements concernés. Pour les projets, plans ou programmes d'importance nationale, cet avis est, en outre, publié dans deux journaux à diffusion nationale quinze jours au moins avant le début de l'enquête./ II.-L'autorité compétente pour ouvrir et organiser l'enquête désigne les lieux où cet avis doit être publié par voie d'affiches et, éventuellement, par tout autre procédé. / Pour les projets, sont au minimum désignées toutes les mairies des communes sur le territoire desquelles se situe le projet. Pour les plans et programmes de niveau départemental ou régional, sont au minimum désignées les préfectures et sous-préfectures.(...) / L'avis d'enquête est également publié sur le site internet de l'autorité compétente pour ouvrir et organiser l'enquête, lorsque celle-ci dispose d'un site.".<br/>
<br/>
              3. En premier lieu, il ressort des pièces du dossier que, par avis d'enquête publique, le préfet des Côtes d'Armor a informé le public qu'il serait procédé, du jeudi 4 août au jeudi 29 septembre 2016, à une enquête publique sur l'utilité publique de la liaison électrique sous-marine et souterraine entre le poste de livraison Baie de Saint-Brieuc de la société Ailes Marines SAS et le poste RTE de la Doberie, sur le territoire des communes d'Erquy, Hénansal et Saint-Alban, avec mise en compatibilité des documents d'urbanisme de ces communes. L'avis décrit ainsi suffisamment l'objet de l'enquête, alors même qu'il ne précise pas le nombre et les caractéristiques des éoliennes ni le coût du projet. Il résulte par ailleurs de l'avis lui-même que le moyen tiré de ce qu'il ne mentionne pas les dates et lieux des réunions d'information et d'échange envisagées manque en fait.<br/>
<br/>
              4. En deuxième lieu, s'il est soutenu qu'aucun affichage de l'avis n'a eu lieu dans les mairies d'Etables-sur-mer, d'Hillion, de Langueux, de Planguenoual et de Morieux et qu'une seule permanence a été organisée en mairie de Saint-Alban, ces seules circonstances ne permettent pas d'établir que les dispositions précitées ont été méconnues. Par ailleurs, il ressort des pièces du dossier que si l'avis d'enquête publique n'avait pas été initialement affiché dans la mairie d'Etables-sur-mer, siège de la commune nouvelle Binic-Etables-sur-mer, mais seulement dans la mairie annexe de Binic, les services préfectoraux ont cependant, à la demande de la commission d'enquête, procédé, dès le 4 août 2016, jour d'ouverture de l'enquête publique, à l'affichage de l'avis d'enquête publique et organisé des permanences en mairie d'Etables-sur-mer les 18 août et 1er septembre 2016. Les moyens tirés à l'insuffisance de ces affichages doivent, par suite, être écartés. <br/>
<br/>
              5. En troisième lieu, il ressort des pièces du dossier que l'avis d'enquête publique a été publié puis rappelé dans les journaux à diffusion locale Ouest France - Côte d'Armor, Ouest France Ile-et-Vilaine et Le Télégramme - Côte d'Armor les 12 juillet 2016 puis 4 août 2016, ainsi que dans le journal Le Penthièvre - Côte d'Armor le 14 juillet 2016 puis 4 août 2016. Il a par ailleurs été publié les 15 et 16 juillet 2016 dans les journaux Les Echos et Le Marin. Le moyen tiré de la méconnaissance des dispositions précitées doit, par suite, être écarté, la circonstance que le journal Le Marin ne serait pas à diffusion nationale étant sans incidence sur ce point, le projet soumis à enquête publique, qui vise à procéder au raccordement au réseau de transport d'électricité, par une ligne sous-marine et souterraine d'une longueur de 49 kilomètres dont 33 kilomètres en mer, d'un parc éolien situé au large de Saint-Brieuc afin d'en transmettre la production électrique, ne pouvant être regardé comme un projet d'importance nationale au sens de l'article R. 123-11 du code de l'environnement.<br/>
<br/>
              6. En quatrième lieu, si, en application du I de l'article L. 123-13 du code de l'environnement dans sa version en vigueur à la date de l'arrêté, " Le commissaire enquêteur ou la commission d'enquête conduit l'enquête de manière à permettre au public de disposer d'une information complète sur le projet, plan ou programme, et de participer effectivement au processus de décision ", ces dispositions n'imposent pas que soit prévue une permanence dans chacune des communes situées à proximité du projet litigieux ni que plusieurs permanences soient tenues au siège des communes les plus concernées par ce projet.<br/>
<br/>
              7. Ainsi, si la commission d'enquête, qui a organisé pendant 37 demi-journées des permanences dans 15 communes, n'a pas organisé de permanence dans les communes d'Hillion, de Langueux, de Planguenoual et de Morieux et si une seule permanence a été organisée dans la commune de Saint-Alban, ces circonstances ne sont pas de nature par elles-mêmes à entacher d'irrégularité l'enquête publique et il ne ressort pas des pièces du dossier que les habitants de ces dernières communes auraient été insuffisamment informés.<br/>
<br/>
              8. En cinquième lieu, aux termes de l'article L. 123-9 du code de l'environnement : " La durée de l'enquête publique est fixée par l'autorité compétente chargée de l'ouvrir et de l'organiser. Elle ne peut être inférieure à trente jours pour les projets, plans et programmes faisant l'objet d'une évaluation environnementale ". Il ressort des pièces du dossier que l'enquête publique a été organisée du 4 août au 29 septembre 2016, sur une période de près de deux mois située, contrairement à ce qui est soutenu, en partie en dehors des vacances scolaires d'été. Par suite, le moyen tiré de la méconnaissance de l'article L. 123-9 du code de l'environnement peut être rejeté.<br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              En ce qui concerne la méconnaissance du principe de précaution :<br/>
<br/>
              9. Aux termes de l'article 1er de la Charte de l'environnement : " Chacun a le droit de vivre dans un environnement équilibré et respectueux de la santé ". Aux termes de son article 5 : " Lorsque la réalisation d'un dommage, bien qu'incertaine en l'état des connaissances scientifiques, pourrait affecter de manière grave et irréversible l'environnement, les autorités publiques veillent, par application du principe de précaution et dans leurs domaines d'attributions, à la mise en oeuvre de procédures d'évaluation des risques et à l'adoption de mesures provisoires et proportionnées afin de parer à la réalisation du dommage ". Aux termes du 1° du II de l'article L.110-1 du code de l'environnement, la protection et la gestion des espaces, ressources et milieux naturels s'inspirent notamment du " principe de précaution, selon lequel l'absence de certitudes, compte tenu des connaissances scientifiques et techniques du moment, ne doit pas retarder l'adoption de mesures effectives et proportionnées visant à prévenir un risque de dommages graves et irréversibles à l'environnement à un coût économiquement acceptable ".<br/>
<br/>
              10. Une opération qui méconnaît les exigences du principe de précaution ne peut légalement être déclarée d'utilité publique. Il appartient dès lors à l'autorité compétente de l'Etat, saisie d'une demande tendant à ce qu'un projet soit déclaré d'utilité publique, de rechercher s'il existe des éléments circonstanciés de nature à accréditer l'hypothèse d'un risque de dommage grave et irréversible pour l'environnement ou d'atteinte à l'environnement susceptible de nuire de manière grave à la santé, qui justifierait, en dépit des incertitudes subsistant quant à sa réalité et à sa portée en l'état des connaissances scientifiques, l'application du principe de précaution. Si cette condition est remplie, il lui incombe de veiller à ce que des procédures d'évaluation du risque identifié soient mises en oeuvre par les autorités publiques ou sous leur contrôle et de vérifier que, eu égard, d'une part, à la plausibilité et à la gravité du risque, d'autre part, à l'intérêt de l'opération, les mesures de précaution dont l'opération est assortie afin d'éviter la réalisation du dommage ne sont ni insuffisantes, ni excessives. Il appartient au juge, saisi de conclusions dirigées contre l'acte déclaratif d'utilité publique et au vu de l'argumentation dont il est saisi, de vérifier que l'application du principe de précaution est justifiée, puis de s'assurer de la réalité des procédures d'évaluation du risque mises en oeuvre et de l'absence d'erreur manifeste d'appréciation dans le choix des mesures de précaution.<br/>
<br/>
              11. Il ressort des pièces du dossier, notamment des éléments relevés par l'étude d'impact, que l'existence d'un risque de leucémies infantiles résultant de l'exposition aux ondes électromagnétiques pour des populations dites " exposées " doit être regardée comme une hypothèse suffisamment plausible en l'état des connaissances scientifiques pour justifier l'application du principe de précaution, même si la société RTE fait valoir, au vu de trois études scientifiques publiées en 2013, 2014 et 2015, qu'aucune élévation du risque de leucémie infantile n'a été détectée à proximité des lignes électriques. <br/>
<br/>
              12. Il ressort des pièces du dossier, notamment de l'étude d'impact qui rappelle l'état des connaissances scientifiques et relève que " la liaison souterraine n'émet pas de champ électrique " en raison des précautions prises, que le tracé retenu suit le " fuseau de moindre impact ", notamment le tracé des routes départementales et communales existantes et que RTE devra mettre en place un dispositif pertinent de surveillance et de mesure des ondes électromagnétiques dans le cadre d'un plan de contrôle et de surveillance, conformément à l'obligation résultant de l'article L. 323-13 du code de l'énergie. Ces mesures ne peuvent être regardées comme manifestement insuffisantes pour parer à la réalisation du risque allégué. Il en va de même, en tout état de cause, pour les risques résultant de l'exposition des troupeaux aux ondes électromagnétiques. Il résulte de ce qui précède que le moyen tiré de la méconnaissance du principe de précaution doit être écarté.<br/>
<br/>
              En ce qui concerne l'utilité publique du projet :<br/>
<br/>
              13. Une opération ne peut être légalement déclarée d'utilité publique que si les atteintes à la propriété privée, le coût financier, les inconvénients d'ordre social, la mise en cause de la protection et de la valorisation de l'environnement et l'atteinte éventuelle à d'autres intérêts publics qu'elle comporte ne sont pas excessifs eu égard à l'intérêt qu'elle présente.<br/>
<br/>
              14. En premier lieu, il ressort des pièces du dossier que le projet déclaré d'utilité publique s'inscrit dans le cadre de la réalisation d'un parc éolien d'ampleur et contribue, par suite, à la politique énergétique qui, en application de l'article L. 100-1 du code de l'énergie, " préserve la santé humaine et l'environnement, en particulier en luttant contre l'aggravation de l'effet de serre et contre les risques industriels majeurs, en réduisant l'exposition des citoyens à la pollution de l'air (...)". Il vise également, conformément aux dispositions de l'article L. 100-2 du même code, à " 3° Diversifier les sources d'approvisionnement énergétique, réduire le recours aux énergies fossiles, diversifier de manière équilibrée les sources de production d'énergie et augmenter la part des énergies renouvelables dans la consommation d'énergie finale ". Enfin, il contribue, ainsi que le relève la commission d'enquête, à l'objectif de porter la production d'énergies renouvelables en Bretagne à 3600 Mégawatts à l'horizon 2020, dont 100 Mégawatts d'éolien en mer. L'opération présente ainsi un intérêt général. <br/>
<br/>
              15. En second lieu, si M. A...soutient que les impacts sur l'environnement ainsi que le coût du projet excèdent ses avantages, il ressort des pièces du dossier, ainsi que des développements précédents, que le maître d'ouvrage a prévu des mesures pour éviter, réduire et le cas échéant compenser les effets du projet sur l'environnement, notamment par le choix du tracé afin d'éviter les habitats sensibles. Par suite, les inconvénients allégués par le requérant ne sont pas de nature à retirer au projet son caractère d'utilité publique.<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              16. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas dans la présente instance la partie perdante, la somme de 5 000 euros que M. A...demande en application des dispositions de l'article L. 761-1 du code de justice administrative. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A...la somme de 5 000 euros que la société RTE demande en application des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : Les conclusions présentées par la société RTE sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. B...A..., au ministre d'Etat, ministre de la transition écologique et solidaire et à la société RTE.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
