<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036070718</ID>
<ANCIEN_ID>JG_L_2017_11_000000402917</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/07/07/CETATEXT000036070718.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 21/11/2017, 402917, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-11-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402917</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:402917.20171121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif d'Amiens d'annuler la décision du 2 octobre 2015 du ministre du l'intérieur constatant la perte de validité de son permis de conduire pour solde de points nul, ainsi que la décision du 1er février 2016 rejetant son recours gracieux. Par un jugement n° 1600507 du 21 juin 2016, le tribunal administratif a rejeté sa demande.   <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 29 août et 29 novembre 2016, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;   <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.     <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la route ; <br/>
<br/>
              - le code de procédure pénale ;  <br/>
<br/>
              - l'arrêté du 4 décembre 2014 relatif au paiement immédiat des amendes forfaitaires constatées par procès-verbal électronique ;<br/>
<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de M. B....<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que l'article R. 49 du code de procédure pénale prévoit, dans son II issu du décret du 26 mai 2009, que le procès-verbal constatant une contravention pouvant donner lieu à une amende forfaitaire " peut être dressé au moyen d'un appareil sécurisé dont les caractéristiques sont fixées par arrêté du garde des sceaux, ministre de la justice, permettant le recours à une signature manuscrite conservée sous forme numérique " ; qu'en vertu des dispositions de l'article A. 37-14 du même code, issu d'un arrêté du 2 juin 2009, ultérieurement reprises à l'article A. 37-19, issu d'un arrêté du 13 mai 2011 et modifié par un arrêté du 6 mai 2014, l'appareil électronique sécurisé permet d'enregistrer, pour chaque procès-verbal, d'une part, la signature de l'agent verbalisateur, d'autre part, celle du contrevenant qui est invité à l'apposer " sur une page écran qui lui présente un résumé non modifiable des informations concernant la contravention relevée à son encontre, informations dont il reconnaît ainsi avoir eu connaissance " ; qu'en vertu des dispositions du II de l'article A. 37-27-2, issu d'un arrêté du 4 décembre 2014, en cas d'infraction entraînant retrait de points, le résumé non modifiable des informations concernant la contravention relevée précise qu'elle entraîne retrait de points et comporte l'ensemble des éléments mentionnés aux articles L. 223-3 et R. 223-3 du code de la route ; <br/>
<br/>
              2. Considérant que, lorsqu'une infraction entraînant retrait de points est constatée au moyen d'un appareil conforme aux dispositions issues de l'arrêté du 4 décembre 2014, dont la mise en oeuvre a été généralisée à l'occasion d'une mise à jour logicielle effectuée le 15 avril 2015, l'agent verbalisateur invite le contrevenant à apposer sa signature sur une page écran où figure l'ensemble des informations relatives au retrait de points exigées par la loi ; que, dès lors, la signature apposée par l'intéressé et conservée par voie électronique établit que ces informations lui ont été délivrées ; que la mention certifiée par l'agent selon laquelle le contrevenant a refusé d'apposer sa signature sur la page qui lui était présentée possède la même valeur probante ; <br/>
<br/>
              3. Considérant, toutefois, qu'il ressort des pièces du dossier soumis au juge du fond que l'infraction constatée le 28 mars 2015 a fait l'objet d'un procès-verbal dressé à l'aide d'un appareil électronique qui n'était pas conforme aux dispositions issues de l'arrêté du 4 décembre 2014 et qui n'assurait pas la délivrance à l'intéressé, lors de la constatation de l'infraction, des informations relatives aux retraits de points prévue aux articles L. 223-3 et R. 223-3 du code de la route ; que, par suite, la circonstance, relevée par le jugement, que M. B... avait signé le procès-verbal, n'était pas de nature à établir la délivrance de ces informations ; que si le tribunal administratif, pour écarter le moyen de l'intéressé tiré de ce qu'elles n'avaient pas été portées à sa connaissance, a en outre retenu qu'il devait " être présumé avoir reçu l'avis d'amende forfaitaire puis le titre exécutoire de l'avis d'amende forfaitaire majorée ", documents qui comportent les informations requises, il a, en faisant jouer une telle présomption, entaché son jugement d'une erreur de droit ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à M. B...de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif d'Amiens du 21 juin 2016 est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée au tribunal administratif d'Amiens. <br/>
<br/>
Article 3 : L'Etat versera à M. B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
