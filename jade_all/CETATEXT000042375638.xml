<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042375638</ID>
<ANCIEN_ID>JG_L_2020_09_000000430243</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/37/56/CETATEXT000042375638.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 28/09/2020, 430243, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-09-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430243</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE ; SCP DELAMARRE, JEHANNIN</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:430243.20200928</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A... B... a demandé au tribunal administratif de Cergy-Pontoise de condamner la commune de Sarcelles à lui verser, d'une part, la somme de 5 964,66 euros en règlement de l'indemnité d'administration et de technicité et de l'indemnité spéciale mensuelle de fonctions dont elle estime avoir été illégalement privée et, d'autre part, la somme de 3 000 euros en réparation du préjudice moral ayant résulté de cette perte de revenus. Par un jugement n° 1609575 du 29 janvier 2019, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 19VE01137 du 18 avril 2019, enregistrée le 23 avril 2019 au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Versailles a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 29 mars 2019 au greffe de cette cour, présenté par Mme B.... Par ce pourvoi et par deux nouveaux mémoires, enregistrés les 4 juin et 9 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Sarcelles la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange, de la Burgade, avocat de Mme B... et à la SCP Delamarre, Jéhannin, avocat de la Commune de Sarcelles.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme B..., agent de la commune de Sarcelles, a demandé au tribunal administratif de Cergy-Pontoise, premièrement de condamner la commune de Sarcelles à lui verser la somme de 5 964,66 euros en règlement, d'une part de l'indemnité d'administration et de technicité dont elle estimait avoir été illégalement privée entre les mois de janvier 2012 et octobre 2013 et entre les mois d'octobre 2015 et novembre 2016 et, d'autre part, de l'indemnité spéciale mensuelle de fonctions dont elle estimait avoir été illégalement privée entre les mois d'octobre 2015 et novembre 2016 et, deuxièmement, de condamner la commune à lui verser la somme de 3 000 euros en réparation du préjudice moral qu'elle estimait avoir subi à raison de cette perte de revenus. Elle demande l'annulation du jugement du 29 janvier 2019 par lequel le tribunal administratif a rejeté sa demande.<br/>
<br/>
              2. Aux termes de l'article R. 811-1 du code de justice administrative : " Toute partie présente dans une instance devant le tribunal administratif ou qui y a été régulièrement appelée (...) peut interjeter appel contre toute décision juridictionnelle rendue dans cette instance. / Toutefois, le tribunal administratif statue en premier et dernier ressort : / (...) 8° Sauf en matière de contrat de la commande publique sur toute action indemnitaire ne relevant pas des dispositions précédentes, lorsque le montant des indemnités demandées est inférieur au montant déterminé par les articles R. 222-14 et R. 222-15 ; / (...) / Par dérogation aux dispositions qui précèdent, en cas de connexité avec un litige susceptible d'appel, les décisions portant sur les actions mentionnées au 8° peuvent elles-mêmes faire l'objet d'un appel (...) ". <br/>
<br/>
              3. La demande qui, émanant d'un fonctionnaire ou d'un agent public, tend seulement au versement de traitements, rémunérations, indemnités, avantages ou soldes impayés, sans chercher la réparation d'un préjudice distinct du préjudice matériel objet de cette demande pécuniaire, ne revêt pas le caractère d'une action indemnitaire au sens des dispositions, citées ci-dessus, du 8° de l'article R. 811-1 du code de justice administrative. Par suite, une telle demande n'entre pas, quelle que soit l'étendue des obligations qui pèseraient sur l'administration au cas où il y serait fait droit, dans le champ de l'exception en vertu de laquelle le tribunal administratif statue en dernier ressort.<br/>
<br/>
              4. Par suite, la demande de Mme B... tendant à ce que la commune de Sarcelles soit condamnée à lui verser les montants d'indemnité d'administration et de technicité et d'indemnité spéciale mensuelle de fonctions dont elle estime avoir été illégalement privée ne revêt pas le caractère d'une action indemnitaire au sens des dispositions du 8° de l'article R. 811-1 du code de justice administrative. Dès lors, le jugement du 29 janvier 2019 du tribunal administratif de Cergy-Pontoise n'a, en tant qu'il statue sur cette demande, pas été rendu en dernier ressort. La requête de Mme B... ne présente donc pas, en tant qu'elle est dirigée contre ce jugement pris dans cette même mesure, le caractère d'un pourvoi en cassation mais celui d'un appel, qui ressortit à la compétence de la cour administrative d'appel de Versailles.<br/>
<br/>
              5. Par ailleurs, les conclusions de Mme B... tendant à la condamnation de la commune de Sarcelles à lui verser la somme de 3 000 euros au titre du préjudice moral qu'elle estime avoir subi en raison de cette perte de revenu présentent un lien de connexité avec sa demande mentionnée au point précédent. Par suite, le jugement du 29 janvier 2019 du tribunal administratif de Cergy-Pontoise peut, en tant qu'il statue sur ces conclusions, faire également l'objet d'un appel devant la cour administrative d'appel de Versailles, en application des dispositions de l'avant dernier alinéa de l'article R. 811-1 du code de justice administrative cité ci-dessus.<br/>
<br/>
              6. Il résulte de tout ce qui précède qu'il y a lieu, en application des dispositions de l'article R. 351-1 du code de justice administrative, d'attribuer le jugement de la requête de Mme B... à la cour administrative d'appel de Versailles.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement de la requête de Mme B... est attribué à la cour administrative d'appel de Versailles.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme A... B... et au président de la cour administrative d'appel de Versailles.<br/>
		Copie en sera adressée à la commune de Sarcelles.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
