<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044361894</ID>
<ANCIEN_ID>JG_L_2021_10_000000456654</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/36/18/CETATEXT000044361894.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 14/10/2021, 456654, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>456654</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:456654.20211014</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... C... A... a demandé au juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, de faire cesser l'atteinte grave et manifestement illégale portée par la ministre des armées à son droit au respect à la vie et à son droit de ne pas être soumis à des traitements inhumains et dégradants par le refus de lui octroyer le bénéfice de la protection fonctionnelle et, d'autre part, d'enjoindre à la ministre des armées de lui délivrer un visa ainsi qu'à sa famille, de mettre en œuvre les mesures propres à assurer sa sécurité, en les évacuant d'Afghanistan en urgence vers les Emirats arabes unis ou en prenant toute autre mesure utile, de prendre en charge matériellement et financièrement son voyage jusqu'en France et de mettre en œuvre toute mesure de nature à assurer de façon pérenne sa sécurité, en organisant matériellement son accueil en France, en lui délivrant un titre de séjour et en prenant en charge l'assistance juridique et les frais y afférents. Par une ordonnance n° 2117452 du 18 août 2021, le juge des référés du tribunal administratif de Paris a rejeté sa requête. <br/>
<br/>
              Par une requête, enregistrée le 13 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, M. B... C... A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de faire droit à ses conclusions de première instance ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre des dispositions combinées de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - l'ordonnance a été rendue à l'issue d'une procédure irrégulière dès lors que rien n'indique que le mémoire déposé le 18 août 2021 par la ministre des armées ait été communiqué à lui-même ou à son conseil ; <br/>
              - le juge des référés a insuffisamment motivé son ordonnance en ne retenant explicitement aucun motif d'intérêt général susceptible de faire obstacle au bénéfice de la protection fonctionnelle ;<br/>
              - un motif d'intérêt général ne peut, sauf circonstances particulières qu'il appartient au juge des référés de caractériser, suffire à justifier le rejet de la demande de protection statutaire lorsque sont en cause le droit à la vie et la protection contre les traitements inhumains et dégradants garantis par les articles 2 et 3 de la convention européenne de sauvegarde des droits e l'homme et des libertés fondamentales ; <br/>
              - la condition d'urgence est satisfaite dès lors que la dégradation brutale et soudaine de la situation sécuritaire en Afghanistan est de nature à justifier que des mesures soient prises pour assurer la sécurité de la population locale ;  <br/>
              - le refus d'accorder le bénéfice de la protection fonctionnelle porte une atteinte grave et manifestement illégale à plusieurs libertés fondamentales. <br/>
<br/>
              Par un mémoire en défense, enregistré le 4 octobre 2021, le ministre de l'Europe et des affaires étrangères conclut au rejet de la requête. Il soutient que les moyens ne sont pas fondés. <br/>
<br/>
              Par un mémoire en défense, enregistré le 5 octobre 2021, la ministre des armées conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas satisfaite et que les moyens soulevés ne sont pas fondés. <br/>
<br/>
              La requête a été communiquée au ministre de l'intérieur qui n'a pas produit d'observations. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. B... C... A... et, d'autre part, la ministre des armées, le ministre de l'Europe et des affaires étrangères et le ministre de l'intérieur ; <br/>
<br/>
              Ont été entendus lors de l'audience publique du 5 octobre 2021 à 14h30 : <br/>
<br/>
              - Me Texier, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. A... ; <br/>
<br/>
              - les représentants de la ministre des armées ;  <br/>
<br/>
              - les représentants du ministre de l'Europe et des affaires étrangères ;<br/>
<br/>
              à l'issue de laquelle le juge des référés a clos l'instruction.  <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". <br/>
<br/>
              2. Il résulte de l'instruction que M. A..., ressortissant afghan, a exercé les fonctions d'interprète auprès des forces françaises en Afghanistan entre mai 2011 et avril 2012. Le 17 février 2018, il a sollicité le bénéfice de la protection fonctionnelle pour lui-même et sa famille en raison des menaces dont ils feraient l'objet en Afghanistan du fait de sa collaboration avec les forces armées françaises. A la suite de la prise de contrôle de la ville de Kaboul par les Talibans, M. A... a demandé au juge des référés du tribunal administratif de Paris, sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre à l'administration de lui délivrer un visa ainsi qu'à sa famille, de mettre en œuvre les mesures propres à assurer sa sécurité en les évacuant d'Afghanistan et d'organiser son accueil en France en lui délivrant un titre de séjour. Par une ordonnance du 18 août 2021, le juge des référés du tribunal administratif de Paris a rejeté sa demande. M. A... relève appel de cette ordonnance. <br/>
<br/>
              3. L'organisation d'opérations d'évacuation à partir d'un territoire étranger et de rapatriement vers la France n'est pas détachable de la conduite des relations internationales de la France. Par suite, la juridiction administrative n'est pas compétente pour connaître des demandes tendant à ce que des rapatriements soient ordonnés. En revanche, des conclusions tendant à ce que soit ordonné à l'administration de prendre, en urgence, les mesures permettant aux ressortissants afghans employés par l'armée française qui peuvent bénéficier de la protection fonctionnelle de faire valoir leur droit par la délivrance de visas ou de toute autre mesure équivalente ne peuvent être regardés comme échappant à la compétence que le juge des référés du Conseil d'Etat tient des dispositions de l'article L. 521-2 du code de justice administrative.<br/>
<br/>
              4. Il résulte de l'instruction et des échanges tenus au cours de l'audience publique que, postérieurement à l'introduction de son appel devant le Conseil d'Etat, M. A... a rejoint la France par un vol organisé depuis l'Afghanistan vers la France via les Emirats Arabes Unis, qu'un visa lui a été remis à l'aéroport Charles-de-Gaulle et qu'une attestation de demandeur d'asile lui a été remise le 1er octobre 2021. Il résulte également des échanges tenus au cours de l'audience publique que l'administration s'est engagée à examiner, dans le cadre de la réunification familiale, les demandes de visas qui pourraient être présentées par les membres de la famille de M. A... qui se sont trouvés dans l'impossibilité de le suivre lors de l'évacuation de Kaboul. Il n'apparaît donc pas, en l'état de l'instruction, que l'administration aurait porté une atteinte grave et manifestement illégale aux libertés fondamentales de M. A....<br/>
<br/>
              5. Il résulte de ce qui précède qu'il y a lieu de rejeter la requête de M. A..., y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : Compte tenu de l'engagement pris par l'administration et rappelé au point 4, la requête de M. A... est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à M. B... C... A..., à la ministre des armées et au ministre de l'Europe et des affaires étrangères.<br/>
Copie en sera adressée au ministre de l'intérieur.<br/>
Fait à Paris, le 14 octobre 2021<br/>
Signé : Mathieu Hérondart<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
