<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033132164</ID>
<ANCIEN_ID>JG_L_2016_09_000000383784</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/13/21/CETATEXT000033132164.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 19/09/2016, 383784, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-09-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383784</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LESOURD</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Anton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:383784.20160919</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Montreuil de la décharger des cotisations de taxe sur les logements vacants auxquelles elle a été assujettie au titre des années 2008 à 2010 et des cotisations de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre des années 1995 et 1998 à 2010 à raison d'un immeuble situé 3, place du Caprice à Noisy-le-Grand. Par un jugement n° 1310097 du 30 juin 2014, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 19 août 2014 et 17 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de procédure civile ;<br/>
              - le code de justice administrative ;		<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Anton, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lesourd, avocat de Mme B...;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que Mme B...a été assujettie à des cotisations de taxe foncière sur les propriétés bâties au titre des années 1995 et 1998 à 2010 à raison de deux appartements situés à Noisy-le-Grand (Seine-Saint-Denis), à des cotisations de taxe sur les logements vacants au titre des années 2008 à 2010 correspondant à l'un de ces appartements et à des cotisations de taxe d'habitation au titre des années 2007 à 2010 correspondant à l'autre de ces appartements, qu'elle occupe ; qu'elle se pourvoit en cassation contre le jugement du 30 juin 2014 par lequel le tribunal administratif de Montreuil a rejeté sa demande de décharge relative à ces impositions ;<br/>
<br/>
              Sur la taxe sur les locaux vacants :   <br/>
<br/>
              2. Considérant que le litige portant sur la taxe sur les logements vacants instituée par les dispositions de l'article 232 du code général des impôts, dont le produit, en vertu du VIII de cet article, est affecté à l'Agence nationale pour l'amélioration de l'habitat, établissement public de l'Etat, ne saurait être regardé comme étant relatif à un impôt local au sens de l'article R. 222-13 du code de justice administrative selon lequel  " Le président du tribunal administratif ou le magistrat qu'il désigne à cette fin (...) statue en audience publique (...) : (...) / 5° Sur les recours relatifs aux (...) impôts locaux autres que la taxe professionnelle " ; qu'ainsi, malgré  les dispositions du VII de l'article 232 du code général des impôts qui prévoient que le contentieux de cette taxe est régi comme en matière de taxe foncière sur les propriétés bâties, il n'est pas au nombre des litiges dans lesquels les tribunaux administratifs statuent en premier et dernier ressort en application des dispositions du deuxième alinéa de l'article R. 811-1 du code de justice administrative ; que, dès lors, le pourvoi de Mme B...dirigé contre le jugement qu'il attaque doit dans cette mesure être regardé comme un appel relevant de la compétence de la cour administrative d'appel de Versailles ;<br/>
<br/>
              Sur la taxe foncière sur les propriétés bâties :<br/>
<br/>
              3. Considérant que le juge, auquel il incombe de veiller à la bonne administration de la justice, n'a aucune obligation de faire droit à une demande de délai supplémentaire formulée par une partie  pour produire un mémoire et peut, malgré cette demande, mettre au rôle l'affaire, hormis le cas où des motifs tirés des exigences du débat contradictoire l'imposeraient ; qu'il n'a pas davantage à motiver le refus qu'il oppose à une telle demande ; qu'aucune disposition du code de justice administrative ne lui impose de viser cette demande de délai supplémentaire ; <br/>
<br/>
              4 Considérant qu'il ressort des pièces du dossier que l'administration fiscale a produit un mémoire en défense enregistré le 14 avril 2014 au greffe du tribunal administratif de Montreuil, communiqué le même jour à la requérante avec un délai de vingt jours pour y répondre ; que, par un courrier enregistré le 5 mai 2014, cette dernière a demandé à bénéficier d'un délai supplémentaire de trois mois en se prévalant de la technicité du dossier justifiant le recours à un avocat et du délai de six mois dont l'administration avait bénéficié pour la production de son mémoire en défense ; qu'elle n'a ainsi fait état d'aucune circonstance constituant un motif tiré des exigences du débat contradictoire qui aurait imposé au tribunal de faire droit à la demande et de reporter l'audience fixée le 7 mai 2014 au 5 juin suivant ; que, dès lors, le tribunal, qui n'était pas tenu de viser cette demande, en refusant de lui donner un délai supplémentaire pour produire son mémoire en réplique et en enrôlant l'affaire, n'a méconnu ni le caractère contradictoire de la procédure contentieuse, ni les droits de la défense, ni, en tout état de cause, les stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; qu'il a suffisamment motivé son jugement ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que les conclusions du pourvoi de Mme B...présentées au titre de la taxe foncière sur les propriétés bâties et des dispositions de l'article L. 761-1 du code de justice administrative  doivent être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement de la requête d'appel de Mme B...en ce qui concerne la taxe sur les locaux vacants est attribué à la cour administrative d'appel de Versailles.<br/>
Article 2 : Les  conclusions du pourvoi de Mme B...sont  rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme A...B...et au ministre de l'économie et des finances. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
