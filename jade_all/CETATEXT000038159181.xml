<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038159181</ID>
<ANCIEN_ID>JG_L_2019_02_000000411043</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/15/91/CETATEXT000038159181.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 22/02/2019, 411043, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411043</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>Mme Cécile Renault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:411043.20190222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Pau d'annuler la décision implicite par laquelle le ministre de la défense a refusé de lui confier le poste de chef d'équipe qu'impliquait, selon lui, son affectation en qualité de responsable de la gestion des matériels au sein de l'antenne du groupement de soutien des bases de défense à Tarbes et de condamner l'Etat à lui verser les rappels de rémunération afférents à cet emploi qu'il aurait dû occuper à partir du 1er octobre 2011. Par un jugement n° 1302038 du 20 février 2015, le tribunal administratif de Pau a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15BX01355 du 27 mars 2017, la cour administrative d'appel de Bordeaux a rejeté l'appel de M. B...contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 30 mai et 30 août 2017 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Renault, auditrice,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M.B....<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M.B..., né en 1955, qui était, depuis le 1er septembre 1997, affecté au 1er régiment de hussards parachutistes de Tarbes, a été nommé, le 27 octobre 2005, " ouvrier des techniques de l'électronique ", groupe VI. Cet établissement ayant fait l'objet d'une réorganisation dans le cadre des restructurations du ministère de la défense, son emploi a été supprimé. Par décision du 29 août 2011, il a été affecté, à compter du 1er octobre 2011, sur un poste de technicien de gestion des stocks au sein du groupement de soutien à la base de défense de Pau-Bayonne-Tarbes. M. B...a, le 1er août 2012, saisi l'administration d'une demande tendant à être affecté sur un poste de chef d'équipe ainsi que le commandait, selon lui, l'exécution de la décision d'affectation du 29 août 2011 et au rappel de rémunération correspondant à compter du 1er octobre 2011. En l'absence de réponse, il a renouvelé ce recours par lettre du 27 juin 2013, notifiée à l'administration le 5 juillet 2013. Ce recours a également été implicitement rejeté. Par un jugement du 20 février 2015, le tribunal administratif de Pau a rejeté sa demande tendant, d'une part, à l'annulation du rejet implicite opposé à sa demande d'affectation sur un poste de chef d'équipe et, d'autre part, à la condamnation de l'Etat à lui verser le rappel de rémunération afférent. M. B...se pourvoit en cassation contre l'arrêt du 27 mars 2017 par lequel la cour administrative d'appel de Bordeaux a rejeté sa demande tendant à l'annulation de ce jugement.<br/>
<br/>
              2. Il ressort des écritures de M. B...devant les juges du fond que celui-ci soutenait que la décision d'affectation du 29 août 2011 impliquait que lui soient confiées des fonctions d'encadrement, conformément à la fiche de poste à laquelle faisait référence cette décision, et que le refus du ministre de l'affecter sur un poste comportant de telles responsabilités était, par suite, illégal. Dès lors, la cour administrative d'appel de Bordeaux a méconnu la portée de ses écritures en estimant que M. B...ne se prévalait que d'éléments postérieurs à cette décision ou entendait en invoquer l'illégalité, et a entaché son arrêt d'insuffisance de motivation en ne répondant pas au moyen qu'il avait ainsi soulevé.<br/>
<br/>
              3. La cour administrative d'appel de Bordeaux a, il est vrai, relevé, par un motif qui n'est pas contesté en cassation, que les conclusions d'excès de pouvoir dirigées contre le refus du ministre de donner à M. B...l'affectation qui, selon lui, découlait de la décision du 29 juillet 2011, étaient irrecevables en raison de leur tardiveté. M. B...n'est donc pas fondé à demander l'annulation de l'arrêt attaqué en tant qu'il statue sur ses conclusions d'excès de pouvoir. Il est en revanche fondé, compte tenu de ce qui a été dit au point 2, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, à en demander l'annulation en tant qu'il statue sur ses conclusions indemnitaires.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement de la somme de 3 000 euros à M.B..., au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 27 mars 2017 est annulé en tant qu'il statue sur les conclusions indemnitaires présentées par M.B....<br/>
Article 2 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Bordeaux. <br/>
Article 3 : L'Etat versera à M. B...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions du pourvoi est rejeté.<br/>
Article 5 : La présente décision sera notifiée à M. A...B...et à la ministre des armées.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
