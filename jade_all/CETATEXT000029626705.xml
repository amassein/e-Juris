<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029626705</ID>
<ANCIEN_ID>JG_L_2014_10_000000362831</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/62/67/CETATEXT000029626705.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème SSJS, 24/10/2014, 362831, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-10-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362831</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:362831.20141024</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. B... A...a demandé au tribunal administratif d'Orléans de prononcer, d'une part, la réduction des rappels de taxe sur la valeur ajoutée auxquels il a été assujetti au titre de la période du 1er janvier 1996 au 30 septembre 1999 ainsi que des pénalités correspondantes et, d'autre part, la décharge des cotisations supplémentaires d'impôt sur le revenu auxquelles il a été assujetti au titre des années 1996 à 1998 ainsi que des pénalités correspondantes. Par un jugement nos 0200186 et 0200729 du 19 septembre 2006, le tribunal administratif d'Orléans lui a notamment accordé la décharge de la cotisation supplémentaire d'impôt sur le revenu à laquelle il a été assujetti au titre de l'année 1996 et des pénalités correspondantes.<br/>
<br/>
              Par un arrêt nos 06NT01955 et 07NT00059 du 23 avril 2008, la cour administrative d'appel de Nantes, sur le recours du ministre chargé du budget contre ce jugement, l'a réformé et a notamment remis à la charge de M. A... la cotisation supplémentaire d'impôt sur le revenu à laquelle il a été assujetti au titre de l'année 1996 et les pénalités correspondantes.<br/>
<br/>
              Par une décision n° 318037 du 5 mai 2011, le Conseil d'État statuant au contentieux a annulé l'arrêt nos 06NT01955 et 07NT00059 du 23 avril 2008 de la cour administrative d'appel de Nantes en tant qu'il s'est prononcé sur les conclusions de la requête de M. A... tendant à la décharge de la cotisation supplémentaire d'impôt sur le revenu à laquelle il a été assujetti au titre de l'année 1996 et des pénalités correspondantes et a renvoyé l'affaire à la cour, dans cette mesure.<br/>
<br/>
              Par un arrêt n° 11NT01412 du 12 juillet 2012, la cour administrative d'appel de Nantes, statuant dans la mesure du renvoi ainsi opéré, a rejeté le recours formé par le ministre de l'économie, des finances et de l'industrie contre le jugement nos 0200186 et 0200729 du 19 septembre 2006 du tribunal administratif d'Orléans en tant qu'il a accordé à M. A... la décharge de la cotisation supplémentaire d'impôt sur le revenu à laquelle il a été assujetti au titre de l'année 1996 et des pénalités correspondantes.<br/>
<br/>
Procédure devant le Conseil d'État<br/>
<br/>
              Par un pourvoi enregistré le 17 septembre 2012 au secrétariat du contentieux du Conseil d'État, le ministre de l'économie et des finances demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11NT01412 du 12 juillet 2012 de la cour administrative d'appel de Nantes ;<br/>
<br/>
              2°) réglant l'affaire au fond, de remettre à la charge de M. A... la cotisation supplémentaire d'impôt sur le revenu à laquelle il a été assujetti au titre de l'année 1996 ainsi que les pénalités correspondantes.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bastien Lignereux, auditeur,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de M. A...;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'une vérification de comptabilité, l'administration a remis en cause l'application du report d'imposition prévu par l'article 151 octies du code général des impôts aux plus-values réalisées en 1996 par M. B... A...à l'occasion des apports en société de deux fonds de commerce de restaurant et d'un droit au bail pour l'exploitation d'un tel fonds de commerce, qu'il avait respectivement acquis en 1992, 1993 et 1995 en qualité de marchand de biens, bénéficiant à ce titre de l'exonération de droits de mutation prévue par l'article 1115 du code général des impôts. Après avoir réclamé en vain contre la requalification, par l'administration, de ces plus-values d'apport en bénéfices professionnels imposables en application de l'article 35 du code général des impôts, M. A... a porté le litige devant le tribunal administratif d'Orléans qui, par un jugement du 19 septembre 2006, lui a notamment accordé la décharge de la cotisation supplémentaire d'impôt sur le revenu à laquelle il a été assujetti au titre de l'année 1996. Le ministre de l'économie et des finances se pourvoit en cassation contre l'arrêt du 12 juillet 2012 par lequel la cour administrative d'appel de Nantes, statuant sur renvoi du Conseil d'État qui avait annulé son précédent arrêt pour un motif de régularité, a rejeté son recours contre le jugement du tribunal.<br/>
<br/>
              2. Aux termes, d'une part, du I de l'article 35 du code général des impôts : " Présentent également le caractère de bénéfices industriels et commerciaux, pour l'application de l'impôt sur le revenu, les bénéfices réalisés par les personnes physiques désignées ci-après : / 1° Personnes qui, habituellement, achètent en leur nom, en vue de les revendre, des immeubles, des fonds de commerce (...) ". L'application de ces dispositions est subordonnée à la double condition que les opérations procèdent d'une intention spéculative et présentent un caractère habituel. Aux termes, d'autre part, de l'article 151 octies du même code, dans sa version applicable à l'imposition en litige : " I. Les plus-values soumises au régime des articles 39 duodecies à 39 quindecies et réalisées par une personne physique à l'occasion de l'apport à une société soumise à un régime réel d'imposition de l'ensemble des éléments de l'actif immobilisé affectés à l'exercice d'une activité professionnelle ou de l'apport d'une branche complète d'activité peuvent bénéficier des dispositions suivantes : / a. L'imposition des plus-values afférentes aux immobilisations non amortissables fait l'objet d'un report jusqu'à la date de la cession à titre onéreux ou du rachat des droits sociaux reçus en rémunération de l'apport de l'entreprise ou jusqu'à la cession de ces immobilisations par la société si elle est antérieure. "<br/>
<br/>
              3. Pour confirmer la décharge de la cotisation supplémentaire d'impôt sur le revenu en litige, la cour a relevé que si M. A..., qui exerçait de façon habituelle au cours de l'année d'imposition en litige son activité déclarée de marchand de biens, avait acquis en cette qualité les fonds de commerce et le droit au bail litigieux et bénéficié à ce titre, sous réserve d'un engagement de revente dans le délai de quatre ans, du régime d'exonération des droits de mutation prévu par l'article 1115 du code général des impôts, il n'était pas contesté qu'il avait inscrit ces acquisitions à l'actif de son entreprise individuelle et non au répertoire retraçant son activité de marchand de biens, qu'il avait exploité les fonds litigieux avant leur apport en société et qu'il avait continué de le faire en sa qualité de gérant des sociétés bénéficiaires de ces apports, en contrepartie desquels il avait reçu des titres de participation. La cour en a déduit que, nonobstant les stipulations des actes d'acquisition, ce contribuable ne pouvait être regardé comme ayant effectivement réalisé les acquisitions litigieuses dans une intention spéculative en sa qualité de marchand de biens, de sorte que si la condition d'habitude permettant l'application des dispositions précitées du I de l'article 35 du code général des impôts était remplie, celle tenant à l'intention spéculative du contribuable ne l'était pas.<br/>
<br/>
              4. Le ministre est toutefois fondé à soutenir que l'administration fiscale est en droit d'opposer au contribuable les conséquences du régime fiscal pour lequel il a opté, sans que ce contribuable puisse utilement se prévaloir, ultérieurement, de ce qu'il ne remplissait pas les conditions auxquelles le bénéfice de ce régime est subordonné, ce qui aurait permis à l'administration de le remettre en cause dès les premiers effets de l'option. Dès lors, en jugeant que les plus-values réalisées par M. A...lors de l'apport en société des fonds de commerce et du droit au bail litigieux ne constituaient pas des bénéfices entrant dans le champ d'application du 1° du I de l'article 35 du code général des impôts, alors que cette qualification résultait de la décision prise par le contribuable de se placer sous le régime auquel renvoient ces dispositions, la cour a commis une erreur de droit. <br/>
<br/>
              5. Il résulte de ce qui précède que l'arrêt de la cour administrative de Nantes doit être annulé. Aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire ". Il y a lieu, par suite, de régler l'affaire au fond.<br/>
<br/>
              6. Comme il est dit au point 4, dès lors que M. A...avait choisi de se placer, lors de l'acquisition des fonds de commerce et du droit au bail litigieux, sous le régime des marchands de biens, les plus-values réalisées par lui lors de l'apport en société des biens en cause constituaient des bénéfices entrant dans le champ d'application du 1° du I de l'article 35 du code général des impôts et ne pouvaient bénéficier des dispositions de l'article 151 octies du même code. Dès lors, c'est à tort que le tribunal administratif s'est fondé sur la circonstance que M. A...exploitait à titre personnel les fonds de commerce litigieux pour juger qu'il était fondé à demander le bénéfice du report d'imposition prévu à l'article 151 octies du code général des impôts.<br/>
<br/>
              7. Il appartient au Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner les autres moyens soulevés par M. A... tant devant le tribunal administratif d'Orléans que devant la cour administrative d'appel de Nantes.<br/>
<br/>
              8. Il résulte de l'instruction que M. A... exerçait, au cours des années concernées, la double activité de restaurateur et de marchand de biens. La circonstance qu'il ait sollicité, en 1991, sa radiation du répertoire des métiers en tant que marchand de biens est sans incidence sur la qualification de son activité dès lors qu'il est constant qu'après cette date il a procédé, non seulement aux acquisitions litigieuses, mais aussi à de nombreuses ventes de biens immobiliers acquis antérieurement. Il n'est pas fondé à soutenir que l'option pour le régime des marchands de biens opérée lors de l'acquisition des biens en cause aurait résulté d'une erreur de sa part, alors que cet option a été formulée trois fois, en 1992, 1993 et 1995, et correspondait, selon les affirmations non contestées de l'administration, à une activité qu'il exerçait à titre professionnel depuis 1987. Enfin, M. A...n'est fondé à se prévaloir, sur le fondement des articles L. 80 A et L. 80 B du livre des procédures fiscales, ni de la circonstance que lors d'un précédent contrôle fiscal l'administration se serait abstenue de remettre en cause l'amortissement des biens mobiliers exploités par lui dans le cadre de son activité de restaurateur, ni de la réponse ministérielle Collery du 8 novembre 1973 dans les prévisions de laquelle il n'entre pas.<br/>
<br/>
              9. Il résulte de tout ce qui précède que le ministre est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif d'Orléans a accordé à M. A...la décharge des cotisations supplémentaires d'impôt sur le revenu mises à sa charge au titre de l'année 1996 et des pénalités correspondantes.<br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 12 juillet 2012 de la cour administrative d'appel de Nantes est annulé. Le jugement du 19 septembre 2006 du tribunal administratif d'Orléans est annulé en tant qu'il statue sur les cotisations supplémentaires d'impôt sur le revenu mises à la charge de M. A...au titre de l'année 1996 et les pénalités correspondantes.<br/>
Article 2 : Les cotisations supplémentaires d'impôt sur le revenu auxquelles M. A... a été assujetti au titre de l'année 1996 ainsi que les pénalités correspondantes sont remises à sa charge.<br/>
Article 3 : Les conclusions de M. A...présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre des finances et des comptes publics et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
