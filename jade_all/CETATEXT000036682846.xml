<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036682846</ID>
<ANCIEN_ID>JG_L_2018_03_000000407905</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/68/28/CETATEXT000036682846.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 07/03/2018, 407905, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-03-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407905</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BRIARD</AVOCATS>
<RAPPORTEUR>M. Frédéric Pacoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:407905.20180307</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le centre hospitalier de Carpentras a demandé au tribunal interrégional de la tarification sanitaire et sociale de Lyon d'annuler l'arrêté du 26 mars 2013 du président du conseil général de Vaucluse fixant les tarifs applicables pour l'exercice 2013 à l'établissement d'hébergement pour personnes âgées dépendantes (EHPAD) " La Lègue " qu'il gère, en tant qu'il arrête le résultat net de l'exercice 2011, pour la section " hébergement ", à un montant déficitaire de 69 107,34 euros et non à un montant positif de 6 692,66 euros. Par un jugement n° 13-84-12 du 16 juin 2014, le tribunal interrégional de la tarification sanitaire et sociale de Lyon a annulé l'arrêté du 26 mars 2016 et fixé le résultat de l'exercice 2011 au montant arrêté par le conseil de surveillance du centre hospitalier.<br/>
<br/>
              Par un arrêt n° A.2014.23 du 13 mai 2016, la Cour nationale de la tarification sanitaire et sociale a rejeté l'appel formé par le département de Vaucluse contre ce jugement du tribunal interrégional de la tarification sanitaire et sociale de Lyon du 16 juin 2014.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 13 février et 15 mai 2017 au secrétariat du contentieux du Conseil d'Etat, le département de Vaucluse demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt de la Cour nationale de la tarification sanitaire et sociale du 13 mai 2016 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier de Carpentras la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de la santé publique ; <br/>
              - le décret n° 2016-1815 du 21 décembre 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Pacoud, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Briard, avocat du département de Vaucluse.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 26 mars 2013, le président du conseil général de Vaucluse a fixé les tarifs applicables à compter du 1er avril 2013 à l'établissement d'hébergement pour personnes âgées dépendantes (EHPAD) " La Lègue ", géré par le centre hospitalier de Carpentras, en fixant le résultat net de l'exercice 2011 pour l'activité d'hébergement à un déficit de 69 107,34 euros, alors que le conseil de surveillance de l'établissement public l'avait fixé à un excédent de 6 692,66 euros. Par un jugement du 16 juin 2014, le tribunal interrégional de la tarification sanitaire et sociale de Lyon a annulé cet arrêté et rétabli le résultat au niveau fixé par le conseil de surveillance de l'établissement. Par un arrêt du 13 mai 2016, la Cour nationale de la tarification sanitaire et sociale a rejeté l'appel formé par le département de Vaucluse contre ce jugement.<br/>
<br/>
              2. Aux termes de l'article L. 6111-3 du code de la santé publique : " Les établissements de santé publics et privés peuvent créer et gérer les services et établissements sociaux et médico-sociaux mentionnés aux articles L. 312-1 et L. 344-1 du code de l'action sociale et des familles. / Les services et établissements créés en application de l'alinéa précédent doivent répondre aux conditions de fonctionnement et de prise en charge et satisfaire aux règles de procédure énoncées par le code susmentionné (...) ". Les articles R. 314-75 et suivants du code de l'action sociale et des familles fixent ainsi les règles budgétaires de financement applicables aux services et établissements sociaux ou médico-sociaux gérés par un établissement public de santé. L'article R. 314-75 dispose à ce titre que : " Les activités mentionnées à l'article R. 314-1 qui sont gérées par un établissement public de santé sont, conformément aux dispositions de l'article R. 6145-12 du code de la santé publique, retracées dans le cadre d'un compte de résultat prévisionnel annexe de cet établissement ", en renvoyant à des dispositions du code de la santé publique pour la présentation et l'exécution de ce compte de résultat prévisionnel annexe et, sous certaines réserves, aux dispositions des articles R. 314-14 à R. 314-43-1 du code de l'action sociale et des familles pour la présentation des propositions budgétaires et la procédure de fixation du tarif. L'article R. 314-76 précise, dans sa rédaction applicable au litige, que : " L'autorité de tarification du budget annexe social ou médico-social est tenue informée par le directeur de l'établissement de santé de toute affectation de résultats dans le budget dont elle fixe le tarif. / Cette affectation prend en compte, le cas échéant, la réformation des résultats opérée dans les conditions prévues à l'article R. 314-52 ". Cet article R. 314-52 prévoit, dans sa rédaction alors applicable, que : " L'autorité de tarification peut, avant de procéder à l'affectation d'un résultat, en réformer d'office le montant en écartant les dépenses qui sont manifestement étrangères, par leur nature ou par leur importance, à celles qui avaient été envisagées lors de la procédure de fixation du tarif, et qui ne sont pas justifiées par les nécessités de la gestion normale de l'établissement ".<br/>
<br/>
              3. Il résulte de ces dispositions que l'article R. 314-51 du code de l'action sociale et des familles, selon lequel " l'affectation du résultat du budget général, ou le cas échéant des budgets principal et annexes, (...) est décidée par l'autorité de tarification ", n'est pas applicable aux établissements et services sociaux ou médico-sociaux gérés par un établissement public de santé, pour lesquels cette décision relève de la compétence du conseil de surveillance de l'établissement public en application de l'article L. 6143-1 du code de la santé publique. En revanche, celles des dispositions de l'article R. 314-52 du même code auxquelles renvoie l'article R. 314-76 permettent à l'autorité de tarification, dans le cadre de la procédure de fixation du tarif, sans remettre en cause l'affectation du résultat décidé par le conseil de surveillance de l'établissement public, d'en réformer d'office le montant en écartant les dépenses qui sont manifestement étrangères à celles qui avaient été envisagées lors de la procédure de fixation du tarif et ne sont pas justifiées par les nécessités de la gestion normale de l'établissement, ainsi d'ailleurs que le précise désormais l'article R. 314-76 lui-même, dans sa rédaction issue du 35° de l'article 2 du décret du 21 décembre 2016 modifiant les dispositions financières applicables aux établissements et services sociaux et médico-sociaux mentionnés au I de l'article L. 312-1 du code de l'action sociale et des familles.<br/>
<br/>
              4. Aussi, en jugeant que l'article R. 314-76 du code de l'action sociale et des familles ne donnait pas compétence au président du conseil général, dans le cadre de la procédure de fixation du tarif, pour réformer le résultat d'un établissement ou service social ou médico-social géré par un établissement public de santé, tel qu'il ressort du compte arrêté par le conseil de surveillance de cet établissement, la Cour nationale de la tarification sanitaire et sociale a commis une erreur de droit. Toutefois, il ressort des pièces du dossier soumis aux juges du fond que la réformation du résultat tel qu'il ressortait du compte de résultat prévisionnel annexe, au titre de l'exercice 2011, de l'établissement d'hébergement pour personnes âgées dépendantes géré par le centre hospitalier de Carpentras, découlait de la remise en cause de la reprise de provisions. Les dispositions combinées des articles R. 314-76 et R. 314-52 n'autorisent pas l'autorité de tarification à réformer le résultat pour une telle raison. Ce motif, qui répond à un moyen invoqué devant les juges du fond et dont l'examen n'implique l'appréciation d'aucune circonstance de fait, doit être substitué au motif erroné en droit retenu par l'arrêt attaqué, dont il justifie le dispositif. <br/>
<br/>
              5. Il suit de là que le département de Vaucluse n'est pas fondé à demander l'annulation de l'arrêt de la Cour nationale de la tarification sanitaire et sociale, lequel est suffisamment motivé.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du centre hospitalier de Carpentras, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du département de Vaucluse est rejeté.<br/>
Article 2 : La présente décision sera notifiée au département de Vaucluse et au centre hospitalier de Carpentras.<br/>
Copie en sera adressée à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
