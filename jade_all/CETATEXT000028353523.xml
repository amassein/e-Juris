<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028353523</ID>
<ANCIEN_ID>JG_L_2013_12_000000351682</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/35/35/CETATEXT000028353523.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 20/12/2013, 351682</TITRE>
<DATE_DEC>2013-12-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>351682</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SPINOSI</AVOCATS>
<RAPPORTEUR>M. Didier-Roland Tabuteau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:351682.20131220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 8 août et 8 novembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la fédération autonome de la fonction publique territoriale (FAFPT) ; la fédération requérante demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir les articles 5, 20, 33, et 49 du décret n° 2011-636 du 8 juin 2011 portant dispositions relatives aux personnels des offices publics de l'habitat ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu la Constitution, notamment son Préambule ;<br/>
<br/>
              Vu le code de la construction et de l'habitation ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984 ;<br/>
<br/>
              Vu la loi n° 2007-290 du 5 mars 2007 ;<br/>
<br/>
              Vu l'ordonnance n° 2007-137 du 1er février 2007 ;<br/>
<br/>
              Vu le décret n° 85-397 du 3 avril 1985 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier-Roland Tabuteau, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Spinosi, avocat de la fédération autonome de la fonction publique territoriale ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Sur la légalité externe du décret attaqué :<br/>
<br/>
              1. Considérant que, lorsqu'un décret doit être pris en Conseil d'Etat, le texte retenu par le Gouvernement ne peut être différent à la fois du projet qu'il avait soumis au Conseil d'Etat et du texte adopté par ce dernier ; qu'il ressort des pièces du dossier, notamment de la copie de la minute de la section de l'administration du Conseil d'Etat, produite par le Premier ministre, qu'aucune disposition du décret dont l'annulation est demandée ne diffère à la fois du projet soumis au Conseil d'Etat et du texte adopté par ce dernier ; que, par suite, le moyen tiré de la méconnaissance des règles qui gouvernent l'examen par le Conseil d'Etat des projets de décret doit être écarté ;<br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              2. Considérant qu'aux termes du VI de l'article 120 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, dans sa rédaction résultant de l'ordonnance du 1er février 2007 ratifiée par la loi du 5 mars 2007 : " Les fonctionnaires et agents non titulaires des offices publics de l'habitat participent avec les salariés de l'établissement à l'organisation et au fonctionnement de leur établissement ainsi qu'à la gestion de son action sociale par l'intermédiaire des institutions représentatives prévues aux titres II et III du livre IV du code du travail pour lesquelles ils sont électeurs et éligibles par dérogation à l'article 9 de la loi du 13 juillet 1983 précitée et à la sous-section 2 de la section 3 du chapitre II de la présente loi. / Les dispositions relatives à l'hygiène, à la sécurité, aux conditions de travail et à la médecine du travail prévues aux titres III et IV du livre II du code du travail s'appliquent aux fonctionnaires et agents non titulaires des offices publics de l'habitat. / Les institutions représentatives prévues au titre III du livre II et aux titres II et III du livre IV se substituent pour les personnels visés à l'alinéa précédent aux comités techniques prévus par la présente loi. / Les dispositions du chapitre II du titre Ier du livre IV du code du travail sont applicables aux fonctionnaires et agents non titulaires mentionnés ci-dessus. / Les dispositions du code du travail mentionnées aux alinéas précédents peuvent faire l'objet d'adaptations par décret en Conseil d'Etat, sous réserve d'assurer les mêmes garanties à ces personnels " ; qu'en outre, aux termes de l'article L. 421-25 du code de la construction et de l'habitation : " Le droit syndical s'exerce dans les offices publics de l'habitat dans des conditions définies par un décret en Conseil d'Etat, pris par dérogation aux dispositions de l'article L. 2141-10 du code du travail. Ce décret cesse de plein droit d'être en vigueur à la date de signature d'un accord collectif ayant le même objet conclu au niveau national entre les représentants de la Fédération nationale des offices publics de l'habitat et les représentants des organisations syndicales " ;<br/>
<br/>
              En ce qui concerne l'article 5 :<br/>
<br/>
              3. Considérant qu'il résulte des dispositions législatives rappelées ci-dessus que le législateur a entendu instituer dans les offices publics de l'habitat des comités d'entreprise et leur confier les attributions des comités techniques pour les agents de droit public qu'ils emploient ; que, par suite, l'article 5 du décret attaqué a pu légalement préciser que les comités d'entreprise des offices publics de l'habitat exerçaient à l'égard des agents publics employés par ces offices " l'ensemble des compétences relevant des comités techniques prévus à l'article 33 de la loi du 26 janvier 1984 " ; que les fonctionnaires territoriaux qui exercent leurs activités dans les offices publics de l'habitat sont dans une situation différente de ceux en fonction dans des collectivités territoriales ou des établissements publics dotés de comités techniques ; qu'ainsi, en prévoyant que les représentants des salariés de droit privé et ceux des agents de droit public ne seraient pas consultés de manière séparée lorsque les questions posées devant les instances représentatives du personnel les concernent de manière exclusive, alors que ces questions sont examinées par des comités techniques composés de représentants des seuls agents publics lorsque les agents territoriaux sont en fonction dans des collectivités territoriales ou des établissements publics à caractère administratif, le pouvoir réglementaire n'a pas méconnu le principe d'égalité entre les personnels relevant de la fonction publique territoriale ;<br/>
<br/>
              En ce qui concerne les articles 20 et 49 :<br/>
<br/>
              4. Considérant qu'aux termes de l'article 20 du décret attaqué : " Les mises à disposition ou les décharges d'activité de membres du personnel des offices publics de l'habitat en vue de l'exercice d'activités syndicales et pour lesquelles les offices publics de l'habitat apportent une contribution financière s'appliquent dans les conditions du régime spécial des offices publics de l'habitat défini par l'accord collectif étendu, relatif à l'exercice du droit syndical signé entre la Fédération nationale des offices publics de l'habitat et les organisations syndicales représentatives " ; qu'aux termes de l'article 49 du même décret : " Les fonctionnaires territoriaux et les agents non titulaires de droit public employés par les offices publics de l'habitat bénéficient, outre des décharges dont ils bénéficient en application des articles 17 à 20 du présent décret, des décharges d'activité de service prévues pour les agents des collectivités territoriales et des établissements publics affiliés aux centres de gestion de la fonction publique territoriale dans les conditions fixées par les articles 17 et 18 du décret du 3 avril 1985 susvisé. Toutefois, ils ne sont pas pris en compte dans l'effectif des agents servant au calcul de l'étendue de ces décharges d'activité de service " ; <br/>
<br/>
              5. Considérant qu'il résulte des dispositions du VI de l'article 120 de la loi du 26 janvier 1984 et de l'article L. 421-25 du code de la construction et de l'habitation citées au point 2 que le législateur a entendu soumettre les fonctionnaires et agents publics non titulaires des offices publics aux dispositions du chapitre II du titre Ier du livre IV du code du travail, relatif à l'exercice du droit syndical dans les entreprises, devenu le titre IV du livre I de la deuxième partie du code du travail après la refonte de celui-ci, ainsi que, dans l'attente d'un accord collectif propre aux offices publics de l'habitat, à des dispositions particulières prévues par décret en Conseil d'Etat ; qu'ainsi, ceux-ci sont régis, en vertu de la loi, par les dispositions prévues par les articles L. 2143-13 et suivants, reprenant celles de l'article L. 412-20, du code du travail, relatives aux heures de délégations, ainsi que, à titre transitoire, par les dispositions réglementaires prises sur le fondement de l'article L. 421-25 du code de la construction puis par les clauses plus favorables des accords collectifs applicables aux offices publics de l'habitat ; que, par suite, la fédération requérante ne peut utilement soutenir que l'article 20 du décret attaqué méconnaîtrait le principe d'égalité de traitement entre fonctionnaires territoriaux et le principe de participation des travailleurs à la détermination de leurs conditions de travail résultant du huitième alinéa du Préambule de la Constitution du 27 octobre 1946 en ce qu'il prévoit que les mises à disposition ou les décharges d'activité de membres du personnel des offices publics de l'habitat en vue de l'exercice d'activités syndicales s'appliquent dans les conditions prévues par un accord collectif étendu soumis aux dispositions du code du travail ; <br/>
<br/>
              6. Considérant que, par l'article 49 du décret attaqué, le pouvoir réglementaire a permis que les fonctionnaires et les agents non titulaires de droit public employés par les offices publics de l'habitat puissent également bénéficier de l'octroi des décharges d'activité de service prévues par les articles 17 et 18 du décret du 3 avril 1985 relatif à l'exercice du droit syndical dans la fonction publique territoriale, dès lors que les organisations syndicales entre lesquelles les heures sont réparties souhaitent désigner certains d'entre eux ; que les dispositions du VI de l'article 120 de la loi du 26 janvier 1984 ayant expressément dérogé aux dispositions de la même loi relatives à l'exercice du droit syndical, dont les dispositions de son article 100 relatives au calcul des décharges d'activité de service par les centres de gestion pour les collectivités et établissements obligatoirement affiliés, la fédération requérante n'est pas fondée à soutenir que l'article 49 du décret attaqué, en ce qu'il prévoit que les fonctionnaires territoriaux et les agents non titulaires de droit public employés par les offices publics de l'habitat ne sont pas pris en compte dans l'effectif des agents servant au calcul de l'étendue des décharges d'activité de service par les centres de gestion de la fonction publique territoriale, méconnaîtrait l'article 100 de la loi du 26 janvier 1984, les articles 17 et 18 du décret du 3 avril 1985 et le principe d'égalité de traitement entre fonctionnaires publics territoriaux ; <br/>
<br/>
              En ce qui concerne l'article 33 :<br/>
<br/>
              7. Considérant qu'aux termes de l'article 33 du décret attaqué : " Des autorisations spéciales d'absence, dont la durée n'est pas imputée sur celle du congé payé annuel, sont accordées aux salariés relevant du présent titre pour certains événements, conformément à un accord collectif d'entreprise ou à un accord collectif étendu, conclu au niveau national entre les représentants de la Fédération nationale des offices publics de l'habitat et les représentants des organisations syndicales représentatives " ; qu'il résulte des dispositions de l'article 120 de la loi du 26 janvier 1984 rappelées précédemment que le régime des autorisations d'absence ne relève pas des domaines pour lesquels le législateur a souhaité que les salariés et les agents de droit public des offices publics de l'habitat soient soumis à des dispositions communes ; que, par suite, la fédération requérante ne peut utilement invoquer la méconnaissance du principe d'égalité pour contester la légalité des dispositions de l'article 33 du décret attaqué en tant qu'elles s'appliquent aux seuls agents de droit privé, lesquels sont dans une situation juridique distincte de celle des agents de droit public ; qu'au surplus, les dispositions de l'article 33 peuvent bénéficier aux fonctionnaires placés en position de détachement auprès des offices publics de l'habitat en vertu de l'article 47 du même décret et les agents de la fonction publique territoriale peuvent également, alors même que les dispositions de l'article 59 de la loi du 26 janvier 1984 n'ont pas fait l'objet du décret d'application nécessaire à leur entrée en vigueur, bénéficier d'autorisations spéciales d'absence n'entrant pas en compte dans le calcul des congés annuels à l'occasion de certains événements, sur décision du chef de service ;<br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que les conclusions de la fédération autonome de la fonction publique territoriale tendant à l'annulation des articles 5, 20, 33, et 49 du décret du 8 juin 2011 portant dispositions relatives aux personnels des offices publics de l'habitat doivent être rejetées, de même que, par voie de conséquence, ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la fédération autonome de la fonction publique territoriale est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la fédération autonome de la fonction publique territoriale, au Premier ministre et à la ministre de l'égalité des territoires et du logement. <br/>
Copie en sera adressée au ministre de l'intérieur, au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social et à la ministre de la réforme de l'Etat, de la décentralisation et de la fonction publique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-07-01-03 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. STATUT GÉNÉRAL DES FONCTIONNAIRES DE L'ÉTAT ET DES COLLECTIVITÉS LOCALES. DISPOSITIONS STATUTAIRES RELATIVES À LA FONCTION PUBLIQUE TERRITORIALE (LOI DU 26 JANVIER 1984). - ARTICLE 59 DE LA LOI DU 26 JANVIER 1984 (AUTORISATIONS D'ABSENCE) - ABSENCE D'INTERVENTION DU DÉCRET D'APPLICATION NÉCESSAIRE À SON ENTRÉE EN VIGUEUR - CONSÉQUENCE - POSSIBILITÉ POUR LES AGENTS DE LA FONCTION PUBLIQUE TERRITORIALE DE BÉNÉFICIER D'AUTORISATIONS D'ABSENCE - EXISTENCE, SUR DÉCISION DU CHEF DE SERVICE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-07-10 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. GARANTIES ET AVANTAGES DIVERS. - AUTORISATIONS D'ABSENCE - BÉNÉFICE OUVERT, DANS LE SILENCE DES TEXTES, AUX AGENTS DE LA FONCTION PUBLIQUE TERRITORIALE - EXISTENCE, SUR DÉCISION DU CHEF DE SERVICE [RJ1].
</SCT>
<ANA ID="9A"> 36-07-01-03 Les agents de la fonction publique territoriale peuvent, alors même que les dispositions de l'article 59 de la loi n° 84-53 du 26 janvier 1984 n'ont pas fait l'objet du décret d'application nécessaire à leur entrée en vigueur, bénéficier d'autorisations spéciales d'absence n'entrant pas en compte dans le calcul des congés annuels à l'occasion de certains événements, sur décision du chef de service.</ANA>
<ANA ID="9B"> 36-07-10 Les agents de la fonction publique territoriale peuvent, alors même que les dispositions de l'article 59 de la loi n° 84-53 du 26 janvier 1984 n'ont pas fait l'objet du décret d'application nécessaire à leur entrée en vigueur, bénéficier d'autorisations spéciales d'absence n'entrant pas en compte dans le calcul des congés annuels à l'occasion de certains événements, sur décision du chef de service.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 29 décembre 1993, Decroix, n° 102628, inédite au Recueil (sol. impl.) ; CE, 12 février 1997, Mlle Henny, n° 125893, T. p. 891 sur un autre point.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
