<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030668776</ID>
<ANCIEN_ID>JG_L_2015_04_000000389026</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/66/87/CETATEXT000030668776.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 23/04/2015, 389026, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-04-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389026</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT ; SCP ROGER, SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2015:389026.20150423</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 27 mars et 14 avril 2015 au secrétariat du contentieux du Conseil d'Etat, M. B... A...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision du 13 mars 2015 par laquelle le Conseil national de l'ordre des pédicures-podologues a rejeté son recours contre la décision du Conseil régional de l'ordre des pédicures-podologues de Rhône-Alpes du 16 décembre 2014 rejetant sa demande d'inscription au tableau de l'ordre ;<br/>
<br/>
              2°) d'enjoindre au Conseil national de l'ordre des pédicures-podologues de l'inscrire au tableau de l'ordre des pédicures-podologues de PACA, dans un délai de 15 jours et sous astreinte de 500 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'ordre des pédicures-podologues la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence est remplie dès lors que la décision de refus d'inscription au tableau de l'ordre porte une atteinte grave et immédiate à sa situation financière, au droit d'exercice d'une profession et à la liberté d'établissement ; <br/>
              - il existe un doute sérieux quant à la légalité de la décision attaquée ;<br/>
              - le Conseil national de l'ordre a entaché sa décision d'incompétence négative et d'erreur de droit et a méconnu les principes d'égalité et de sécurité juridique en considérant que la circonstance que l'école européenne de pédicurie-podologie (EEPP) délivre un diplôme à titre privé et non au nom de l'Etat belge justifiait le refus d'inscription au tableau de l'ordre ; <br/>
              - le Conseil national de l'ordre des pédicures-podologues, étant dans une situation de compétence liée au regard de l'autorisation d'exercice octroyée par préfet de la région Rhône-Alpes, aurait dû accepter l'inscription au tableau et, a, de ce fait, commis une erreur de droit. <br/>
<br/>
<br/>
              Vu la décision dont la suspension de l'exécution est demandée ;<br/>
<br/>
              Vu la copie de la requête à fin d'annulation de cette décision ; <br/>
<br/>
              Par deux mémoires en défense, enregistrés les 9 et 15 avril 2015, le Conseil national de l'ordre des pédicures-podologues conclut au rejet de la requête et à ce qu'il soit mis à la charge de M. A...la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative. Il soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par le requérant ne sont pas fondés.<br/>
<br/>
              Vu les autres pièces du dossier :<br/>
<br/>
              Vu : <br/>
              - la directive 2005/36/CE du 7 septembre 2005 ;<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part M. A... et, d'autre part, le Conseil national de l'ordre des pédicures-podologues ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 16 avril 2015 à 10 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Mathonnet, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M.A... ;<br/>
<br/>
              - le représentant de M.A... ;<br/>
<br/>
              - Me Barthelémy, avocat au Conseil d'Etat et à la Cour de cassation, avocat du Conseil national de l'ordre des pédicures-podologues ; <br/>
<br/>
- la représentante du Conseil national de l'ordre des pédicures-podologues ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ; <br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; <br/>
<br/>
              2. Considérant que l'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celui-ci porte atteinte de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ; que l'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire ;<br/>
<br/>
              3. Considérant, d'une part, qu'aux termes des dispositions de l'article L. 4322-4 du code de la santé publique, applicable aux pédicures-podologues : " L'autorité compétente peut, après avis d'une commission composée notamment de professionnels, autoriser individuellement à exercer la profession de pédicure-podologue les ressortissants d'un Etat membre de l'Union européenne ou d'un autre Etat partie à l'accord sur l'Espace économique européen, qui ont suivi avec succès un cycle d'études postsecondaires et qui, sans posséder le diplôme prévu à l'article L. 4322-3, sont titulaires : (...) 2° Ou, lorsque les intéressés ont exercé dans un Etat, membre ou partie, qui ne réglemente pas l'accès à cette profession ou son exercice, d'un titre de formation délivré par un Etat, membre ou partie, attestant de la préparation à l'exercice de la profession, accompagné d'une attestation justifiant, dans cet Etat, de son exercice à temps plein pendant deux ans au cours des dix dernières années ou à temps partiel pendant une durée correspondante au cours de la même période. Cette condition n'est pas applicable lorsque la formation conduisant à cette profession est réglementée (...) " ; <br/>
<br/>
              4. Considérant, d'autre part, qu'aux termes de l'article L. 4311-16 du même code, rendu applicable aux pédicures-podologues par l'article L. 4322-2 du code : " Le conseil de l'ordre refuse l'inscription au tableau de l'ordre si le demandeur ne remplit pas les conditions de compétence, de moralité et d'indépendance exigées pour l'exercice de la profession " ; et qu'aux termes de l'article R. 4112-2 du même code, rendu applicable aux pédicures-podologues par l'article R. 4323-1 du code : " I.- A la réception de la demande, le président du conseil départemental désigne un rapporteur parmi les membres du conseil. Ce rapporteur procède à l'instruction de la demande et fait un rapport écrit. / Le conseil vérifie les titres du candidat et demande communication du bulletin n° 2 du casier judiciaire de l'intéressé. Il refuse l'inscription si le demandeur est dans l'un des trois cas suivants: 1° Il ne remplit pas les conditions nécessaires de moralité et d'indépendance ; 2° Il est établi, dans les conditions fixées au II, qu'il ne remplit pas les conditions nécessaires de compétence ; 3° Il est constaté, dans les conditions fixées au III, une infirmité ou un état pathologique incompatible avec l'exercice de la profession. / II.-En cas de doute sérieux sur la compétence professionnelle du demandeur, le conseil départemental saisit, par une décision non susceptible de recours, le conseil régional ou interrégional qui diligente une expertise. Le rapport d'expertise est établi dans les conditions prévues aux II, III, IV, VI et VII de l'article R. 4124-3-5 et il est transmis au conseil départemental./ S'il est constaté, au vu du rapport d'expertise, une insuffisance professionnelle rendant dangereux l'exercice de la profession, le conseil départemental refuse l'inscription et précise les obligations de formation du praticien. La notification de cette décision mentionne qu'une nouvelle demande d'inscription ne pourra être acceptée sans que le praticien ait au préalable justifié avoir rempli les obligations de formation fixées par la décision du conseil départemental. " ;<br/>
<br/>
<br/>
              5. Considérant que s'il résulte des dispositions précitées du code de la santé publique qu'il n'appartient pas au Conseil national de l'ordre des pédicures-podologues de refuser l'inscription d'un candidat au tableau de l'ordre pour incompétence du demandeur du seul fait que le titre de formation qui a été délivré à celui-ci par une école en Belgique n'est pas reconnu dans ce pays et qu'il lui revenait, s'il entendait retenir un motif tiré de l'insuffisance professionnelle de M.A..., en l'absence d'expertise diligentée sur ce point par le conseil régional, de faire procéder lui-même à une expertise pour apprécier la compétence de l'intéressé, il résulte néanmoins de l'instruction que le requérant n'a pas été placé par la décision dont il demande la suspension dans l'impossibilité de poursuivre une activité professionnelle qu'il exerçait auparavant ; que les éléments qu'il avance pour établir le préjudice que lui a causé cette décision ne permettent pas d'établir que les effets de la décision contestée sont de nature à caractériser une urgence justifiant que l'exécution de la décision soit suspendue ; <br/>
<br/>
              6. Considérant que, par suite, la demande de suspension de la décision du 13 mars 2015 présentée par M. A... ne peut qu'être rejetée ; que les dispositions de l'article L. 761-1 du code de justice administrative font par suite obstacle à ce qu'une somme soit mise à ce titre à la charge du Conseil national de l'ordre des pédicures-podologues qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de M. A...la somme de 500 euros sur le fondement des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : M. A...versera au Conseil national de l'ordre des pédicures-podologues la somme de 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente ordonnance sera notifiée à M. B...A...et au Conseil national de l'ordre des pédicures-podologues. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
