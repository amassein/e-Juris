<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038088247</ID>
<ANCIEN_ID>JG_L_2019_02_000000422658</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/08/82/CETATEXT000038088247.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 01/02/2019, 422658, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422658</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:422658.20190201</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 27 juillet et 25 octobre 2018 au secrétariat du contentieux du Conseil d'Etat, Mme B...A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 16 avril 2018 par laquelle le commandant en second de la région de gendarmerie de Basse-Normandie et du groupement départemental de gendarmerie du Calvados lui a infligé une sanction du premier groupe de dix jours d'arrêts, assortis d'une dispense d'exécution ;<br/>
<br/>
              2°) d'enjoindre à la ministre des armées de procéder à l'effacement dans son dossier de toute référence à cette décision ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la défense ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de MmeA....<br/>
<br/>
              Vu la note en délibéré, enregistrée le 18 janvier 2019, présentée par Mme A....<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier que MmeA..., capitaine de gendarmerie affectée à la compagnie de Deauville, est détachée depuis le 1er janvier 2015 comme chargée de mission au sein du bureau de la performance et de la cohérence opérationnelle de la région de gendarmerie de Basse-Normandie. Par une décision du 16 avril 2018, le commandant en second de cette région de gendarmerie et du groupement départemental de gendarmerie du Calvados a prononcé à son encontre une sanction de dix jours d'arrêts, assortis d'une dispense d'exécution, au motif que sa note relative à la réserve citoyenne rendue le 15 septembre 2017, malgré les conseils prodigués et les délais supplémentaires accordés par sa hiérarchie, ne répondait pas aux instructions données et n'était pas conforme à ce qui est attendu de la part d'un officier. <br/>
<br/>
              2. Il appartient au juge de l'excès de pouvoir, saisi de moyens en ce sens, de rechercher si les faits reprochés à un agent public ayant fait l'objet d'une sanction disciplinaire sont établis, s'ils constituent une faute de nature à justifier une sanction et si la sanction retenue est proportionnée à la gravité de ces fautes.<br/>
<br/>
              3. Il ressort des pièces du dossier, notamment des termes mêmes de la décision litigieuse du 16 avril 2018, que la sanction infligée à Mme A...est fondée sur le fait que l'intéressée, qui s'est vue confier en début d'année 2017 la réalisation d'une étude sur la réserve citoyenne dans le ressort de la région de gendarmerie de Normandie, a fourni un travail dont " les résultats ne sont pas à la hauteur de ce qui lui a été demandé par sa hiérarchie " et a remis avec retard un document qui " n'est pas conforme à ce qui est attendu de la part d'un officier de gendarmerie ". Si un tel motif est de nature à révéler, de la part de l'intéressée, une insuffisance professionnelle, il n'est pas à lui seul, en l'absence d'autres circonstances susceptibles de caractériser un manquement aux devoirs, sujétions et obligations de l'état militaire, de nature à justifier une sanction disciplinaire. Par suite, sans qu'il soit besoin d'examiner les autres moyens de la requête, Mme A...est fondée à demander l'annulation de la sanction qu'elle attaque.<br/>
<br/>
              4. Il résulte de ce qui précède qu'il y a lieu d'enjoindre à la ministre des armées de procéder à la suppression dans le dossier de Mme A...de toute mention de la décision de sanction du 16 avril 2018.<br/>
<br/>
              5. Il y a également lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à Mme A...au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du 16 avril 2018 infligeant à Mme A...une sanction de dix jours d'arrêts, assortie d'une dispense d'exécution, est annulée.<br/>
Article 2 : Il est enjoint à la ministre des armées de procéder à la suppression dans le dossier de Mme A...de toute mention de la décision de sanction du 16 avril 2018 annulée par la présente décision.<br/>
Article 3 : L'Etat versera à Mme A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à Mme B...A...et à la ministre des armées.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
