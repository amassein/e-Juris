<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037513382</ID>
<ANCIEN_ID>JG_L_2018_10_000000421746</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/51/33/CETATEXT000037513382.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 22/10/2018, 421746, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-10-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421746</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:421746.20181022</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 421746, par deux mémoires, enregistrés les 27 juillet et 10 octobre 2018 au secrétariat du contentieux du Conseil d'Etat, présentés en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, M. C... D...demande au Conseil d'Etat, à l'appui de son pourvoi tendant à l'annulation de la décision n° 12865 du 14 juin 2018 par laquelle la chambre disciplinaire nationale de l'ordre des médecin lui a infligé la sanction d'interdiction d'exercer la médecine pendant trois mois, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 4124-6 du code de la santé publique.<br/>
<br/>
<br/>
              2° Sous le n° 422665, par deux mémoires, enregistrés les 27 juillet et 10 octobre 2018, présentés en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, M. D... demande au Conseil d'Etat, à l'appui de sa requête tendant à ce qu'il soit sursis à l'exécution de la décision de la chambre disciplinaire nationale de l'ordre des médecins du 14 juin 2018 mentionnée au 1°, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés que la Constitution garantit des dispositions de l'article L. 4124-6 du code de la santé publique.<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes ;   <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de M. D...et à la SCP Rousseau, Tapie, avocat de M. et Mme B...A...et autres ;<br/>
<br/>
<br/>
<br/>
<br/>1.  Considérant que les questions prioritaires de constitutionnalité soulevées par M. D... sont dirigées contre les mêmes dispositions du code de la santé publique ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2.  Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé y compris pour la première fois en cassation à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de cet article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              3.  Considérant que M. D... soutient que les dispositions de l'article L. 4124-6 du code de la santé publique méconnaissent les principes constitutionnels de légalité et d'individualisation des peines, résultant des articles 7, 8 et 9 de la Déclaration des droits de l'homme et du citoyen de 1789, qui imposent la motivation des jugements de condamnation, pour la culpabilité comme pour la peine ;<br/>
<br/>
              4.  Considérant que les dispositions contestées de l'article L. 4124-6 du code de la santé publique, qui fixent la liste des sanctions disciplinaires qui peuvent être prononcées à l'encontre d'un médecin, d'un chirurgien-dentiste ou d'une sage-femme et précisent le régime de ces sanctions, ne sont pas relatives à la forme ou au prononcé des décisions juridictionnelles ; que, par suite, le moyen tiré de ce qu'elles dispenseraient le juge disciplinaire de l'obligation de motiver le choix de la sanction qu'il prononce est inopérant ;<br/>
<br/>
              5.  Considérant, au surplus, que le V de l'article L. 4124-7 du code de la santé publique et le V de l'article L. 4122-3 du même code disposent que les décisions des juridictions disciplinaires des ordres des médecins, des chirurgiens-dentistes et des sages-femmes " doivent être motivées " ; qu'en application de ces dispositions, il appartient au juge disciplinaire de motiver sa décision en énonçant les motifs pour lesquels il retient l'existence d'une faute disciplinaire ainsi que la sanction qu'il inflige ; qu'en revanche, notamment, il n'est, en principe, tenu de justifier spécifiquement ni de l'éventuelle différence entre la sanction infligée en appel et celle infligée en première instance ni, pour la sanction qu'il retient, du choix de sa période d'exécution ; <br/>
<br/>
              6.  Considérant qu'il résulte de ce qui a été dit au point 4 que les questions soulevées, qui ne sont pas nouvelles, ne présentent pas un caractère sérieux ; qu'il n'y a pas lieu de les renvoyer au Conseil constitutionnel ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel les questions prioritaires de constitutionnalité soulevées par M. D... sous les nos 421746 et 422665.<br/>
Article 2 : La présente décision sera notifiée à M. C... D....<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre et à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
