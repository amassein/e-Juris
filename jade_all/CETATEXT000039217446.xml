<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039217446</ID>
<ANCIEN_ID>JG_L_2019_10_000000425645</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/21/74/CETATEXT000039217446.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 14/10/2019, 425645</TITRE>
<DATE_DEC>2019-10-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425645</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CORLAY</AVOCATS>
<RAPPORTEUR>M. Thomas Janicot</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:425645.20191014</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La commune de Chambon-sur-Dolore, la section de commune de Malvieille, la section de commune de l'Hôpital, la section de commune de Les Ayes, la section de commune de La Mas, la section de commune de Frideroche, M. C... E..., M. B... G..., M. H... A..., M. I... J... et M. F... D... ont demandé au tribunal administratif de Clermont-Ferrand d'annuler l'arrêté du 15 mars 2016 de la préfète du Puy-de-Dôme portant émission d'office de titres de recettes en vertu d'un jugement ordonnant le recouvrement de sommes indûment perçues par les membres des sections de commune de Chambon-sur-Dolore.<br/>
<br/>
              Par un jugement n° 1600773 du 23 mai 2018, le tribunal administratif de Clermont-Ferrand a rejeté leur demande.<br/>
<br/>
              Par une ordonnance n° 18LY02643 du 24 septembre 2018, le président de la troisième chambre de la cour administrative d'appel de Lyon a rejeté l'appel formé par les requérants contre ce jugement.<br/>
<br/>
              Par un pourvoi enregistré le 26 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, la commune de Chambon-sur-Dolore, la section de commune de Malvieille, la section de commune de l'Hôpital, la section de commune de Les Ayes, la section de commune de La Mas, la section de commune de Frideroche, M. C... E..., M. B... G..., M. H... A..., M. I... J... et M. F... D... demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 80-539 du 16 juillet 1980 ; <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Janicot, auditeur,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Corlay, avocat de la société section de commune de Malvieille, de la société section de commune de l'hôpital, de la société section de commune de Les Ayes, de la société section de commune du Mas, de la société section de commune de Frideroche, de M. C... E..., de M. B... G..., de M. H... A..., de M. I... J..., de M. F... D... et de la commune de Chambon-sur-Dolore ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un jugement du 3 décembre 2013, confirmé par un arrêt de la cour administrative d'appel de Lyon du 21 avril 2015, le tribunal administratif de Clermont-Ferrand a annulé la délibération du 25 novembre 2011 par laquelle le conseil municipal de la commune de Chambon-sur-Dolore a décidé de répartir les produits de la vente des coupes de bois intervenues en 2010 entre les ayants-droit affouagistes des sections de commune de Les Ayes, de Frideroche, de l'Hôpital, du Mas et de Malvieille. Par une mise en demeure du 7 janvier 2016, la préfète du Puy-de-Dôme a demandé au maire de Chambon-sur-Dolore de recouvrer les sommes perçues par les ayants-droit de ces sections de commune sur le fondement de cette délibération. Puis, par un arrêté du 15 mars 2016, elle a décidé l'émission d'office des titres de recettes permettant ce recouvrement. La commune de Chambon-sur-Dolore, les sections de commune de Malvieille, de l'Hôpital, de Les Ayes, de La Mas, de Frideroche ainsi que M. C... E..., M. B... G..., M. H... A..., M. I... J... et M. F... D..., qui sont membres de ces sections, se pourvoient en cassation contre l'ordonnance du 24 septembre 2018, par laquelle le président de la troisième chambre de la cour administrative d'appel de Lyon a confirmé le jugement du 23 mai 2018 du tribunal administratif de Clermont-Ferrand, qui a rejeté leur demande tendant à l'annulation de cet arrêté. <br/>
<br/>
              2. Aux termes de l'article L. 2411-1 du code général des collectivités territoriales : " I. Constitue une section de commune toute partie d'une commune possédant à titre permanent et exclusif des biens ou des droits distincts de ceux de la commune. / La section de commune est une personne morale de droit public ". Aux termes de l'article L. 2411-2 du même code : " La gestion des biens et droits de la section est assurée par le conseil municipal et par le maire. Lorsqu'elle est constituée en application de l'article L. 2411-3, la commission syndicale et son président exercent les fonctions de gestion prévues au I de l'article L. 2411-6, aux articles L. 2411-8 et L. 2411-10, au II de l'article L. 2411-14, ainsi qu'aux articles L.2411-18 et L. 2412-1 et sont consultés dans les cas prévus au II de l'article L. 2411-7, L. 2411-11, L. 2412-2, L. 2411-15 et L. 2411-18 ". Aux termes du I de l'article L. 2411-6 du même code : " I. - Sous réserve des dispositions de l'article L. 2411-15, la commission syndicale délibère sur les objets suivants : / (...) 2° Vente, échange et location pour neuf ans ou plus de biens de la section autres que la vente prévue au 1° du II ; / (...) / Les actes nécessaires à l'exécution de ces délibérations sont passés par le président de la commission syndicale ". Aux termes de l'article L. 2411-15 du même code : " Lorsque la commission syndicale est constituée et sous réserve des dispositions du II de l'article L. 2411-6, le changement d'usage ou la vente de tout ou partie des biens de la section est décidé sur proposition du conseil municipal ou de la commission syndicale par un vote concordant du conseil municipal statuant à la majorité absolue des suffrages exprimés et de la commission syndicale, qui se prononce à la majorité de ses membres. / En l'absence d'accord ou de vote du conseil municipal ou de la commission syndicale dans un délai de six mois à compter de la transmission de la proposition, le représentant de l'Etat dans le département statue, par arrêté motivé, sur le changement d'usage ou la vente ". Aux termes de l'article L. 2412-1 du même code : " I. - Le budget de la section, qui constitue un budget annexe de la commune, est établi en équilibre réel en section de fonctionnement et en section d'investissement. / Le projet de budget est élaboré par la commission syndicale et soumis pour adoption au conseil municipal. (...) / Toutefois, lorsque la commission syndicale n'est pas constituée, il n'est pas établi de budget annexe de la section à partir de l'exercice budgétaire suivant. (...) / Le conseil municipal établit alors un état spécial annexé au budget de la commune, dans lequel sont retracées les dépenses et les recettes de la section. / II. - Les revenus en espèces des biens de la section et, le cas échéant, le produit de la vente de ceux-ci figurent dans le budget annexe ou l'état spécial annexé relatif à la section (...). III. - La commission syndicale peut, de sa propre initiative ou sur demande de la moitié des électeurs de la section formulée dans les conditions prévues par un décret en Conseil d'Etat, demander au maire de rendre compte de l'exécution du budget annexe de la section et de l'application des règles prescrites à l'article L. 2411-10 ". Aux termes, enfin, de l'article R.2342-4 du même code : " Les produits des communes, des établissements publics communaux et intercommunaux et de tout organisme public résultant d'une entente entre communes ou entre communes et toute autre collectivité publique ou établissement public, qui ne sont pas assis et liquidés par les services fiscaux de l'Etat en exécution des lois et règlements en vigueur, sont recouvrés : / - soit en vertu de jugements ou de contrats exécutoires ; / - soit en vertu de titres de recettes ou de rôles émis et rendus exécutoires par le maire en ce qui concerne la commune et par l'ordonnateur en ce qui concerne les établissements publics / (...) "<br/>
<br/>
              3. Par ailleurs, aux termes du IV de l'article 1er de la loi du 16 juillet 1980 relative aux astreintes prononcées en matière administrative et à l'exécution des jugements par les personnes morales de droit public : " L'ordonnateur d'une collectivité territoriale ou d'un établissement public local est tenu d'émettre l'état nécessaire au recouvrement de la créance résultant d'une décision juridictionnelle passée en force de chose jugée dans le délai de deux mois à compter de la date de notification de la décision de justice. / Faute de dresser l'état dans ce délai, le représentant de l'Etat adresse à la collectivité territoriale ou à l'établissement public local une mise en demeure d'y procéder dans le délai d'un mois ; à défaut, il émet d'office l'état nécessaire au recouvrement correspondant. / En cas d'émission de l'état par l'ordonnateur de la collectivité ou de l'établissement public local après mise en demeure du représentant de l'Etat, ce dernier peut néanmoins autoriser le comptable à effectuer des poursuites en cas de refus de l'ordonnateur. / L'état de recouvrement émis d'office par le représentant de l'Etat est adressé au comptable de la collectivité territoriale ou de l'établissement public local pour prise en charge et recouvrement, et à la collectivité territoriale ou à l'établissement public local pour inscription budgétaire et comptable ".<br/>
<br/>
              4. Il résulte de ces dispositions, qui sont applicables aux sections de commune, que le recouvrement des créances qui résulte, pour la section d'une commune, de l'annulation d'une délibération du conseil municipal de cette commune prévoyant la répartition entre les membres de la section des produits de la vente de l'un des biens de cette dernière, est assuré par le maire de la commune, au budget de laquelle les recettes en cause sont rattachées, sous forme de budget annexe ou d'état spécial annexé. C'est, dès lors, au maire que doit être adressée, par le représentant de l'Etat, la mise en demeure préalable à l'émission d'office, par ce dernier, d'états de recouvrement des créances en cause.<br/>
<br/>
              5. En premier lieu, le président de la troisième chambre de la cour administrative d'appel de Lyon, dont l'ordonnance est suffisamment motivée et qui n'a pas dénaturé les pièces du dossier en estimant que la mise en demeure du 7 janvier 2016 avait été adressée au maire de Chambon-sur-Dolore en sa qualité d'ordonnateur des sections de commune, n'a commis d'erreur de droit ni en jugeant que les dispositions citées au points 3 sont applicables aux sections de commune, ni en écartant le moyen tiré de l'irrégularité de la procédure. <br/>
<br/>
              6. En deuxième lieu, s'il ne résulte pas des motifs et du dispositif du jugement du tribunal administratif de Lyon du 3 décembre 2013 qui a annulé la délibération du 25 novembre 2011, pas plus que de ceux de l'arrêt de la cour administrative d'appel de Lyon du 21 avril 2015 qui a confirmé ce jugement, que les membres des sections de commune de Les Ayes, de Frideroche, de l'Hôpital, du Mas et de Malvieille auraient été condamnés au paiement de sommes d'argent pour un montant déterminé, l'application des dispositions du IV de l'article 1er de la loi du 16 juillet 1980 n'est, à la différence de celle du II du même article, pas subordonnée à une telle condition. Par suite et dès lors que ces décisions juridictionnelles impliquaient le reversement des sommes qui ont été illégalement versées sur le fondement de la délibération précitée, le président de la troisième chambre n'a pas commis d'erreur de droit en jugeant que l'arrêté litigieux avait pu légalement être pris en application de ces dispositions.<br/>
<br/>
              7. En troisième et dernier lieu, sous réserve de dispositions législatives ou réglementaires contraires et hors le cas où il est satisfait à une demande du bénéficiaire, l'administration ne peut retirer une décision individuelle créatrice de droits, si elle est illégale, que dans le délai de quatre mois suivant la prise de cette décision. Une décision administrative explicite accordant un avantage financier crée des droits au profit de son bénéficiaire alors même que l'administration avait l'obligation de refuser cet avantage. En revanche, n'ont pas cet effet les mesures qui se bornent à procéder à la liquidation de la créance née d'une décision prise antérieurement.<br/>
<br/>
              8. Le président de la troisième chambre de la cour administrative d'appel de Lyon n'a pas commis d'erreur de droit, en écartant, par adoption des motifs du jugement de première instance, le moyen tiré de ce que le versement des sommes à chaque ayant-droit affouagiste ne pouvait plus être remis en cause au motif que si le montant de ces sommes n'avait pas été mentionné dans la délibération du conseil municipal du 25 novembre 2011, il était le résultat d'une simple opération de liquidation tenant compte, d'une part, du montant global revenant, aux termes de cette délibération, à chaque section de commune, et d'autre part, du rôle établi le 8 mai 2010 auquel celle-ci a renvoyé.<br/>
<br/>
              9. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur la fin de non-recevoir soulevée par le ministre, que les requérants ne sont pas fondés à demander l'annulation de l'ordonnance qu'ils attaquent. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la commune de Chambon-sur-Dolore, des sections de commune de Malvieille, de l'Hôpital, de Les Ayes, de La Mas et de Frideroche, de M. C... E..., de M. B... G..., de M. H... A..., de M. I... J... et de M. F... D... est rejeté. <br/>
Article 2 : La présente décision sera notifiée à la commune de Chambon-sur-Dolore, aux sections de commune de Malvieille, de l'Hôpital, de Les Ayes, de La Mas et de Frideroche, à M. C... E..., à M. B... G..., à M. H... A..., à M. I... J..., à M. F... D... et au ministre de l'intérieur. <br/>
Copie en sera adressée au préfet du Puy-de-Dôme. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-01-07-01 COLLECTIVITÉS TERRITORIALES. DISPOSITIONS GÉNÉRALES. DISPOSITIONS FINANCIÈRES. PRINCIPES GÉNÉRAUX. - RECOUVREMENT D'OFFICE DES CRÉANCES RÉSULTANT D'UNE DÉCISION JURIDICTIONNELLE PASSÉE EN FORCE DE CHOSE JUGÉE (IV DE L'ART. 1ER DE LA LOI DU 16 JUILLET 1980) - APPLICABILITÉ AUX SECTIONS DE COMMUNE - EXISTENCE, LE MAIRE DE LA COMMUNE ÉTANT RESPONSABLE DU RECOUVREMENT DES CRÉANCES DE CES SECTIONS - CONSÉQUENCE - MISE EN DEMEURE D'ÉMETTRE L'ÉTAT DE RECOUVREMENT DEVANT ÊTRE ADRESSÉ AU MAIRE PAR LE PRÉFET.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-07 PROCÉDURE. JUGEMENTS. EXÉCUTION DES JUGEMENTS. - RECOUVREMENT D'OFFICE DES CRÉANCES RÉSULTANT D'UNE DÉCISION JURIDICTIONNELLE PASSÉE EN FORCE DE CHOSE JUGÉE (IV DE L'ART. 1ER DE LA LOI DU 16 JUILLET 1980) - APPLICABILITÉ AUX SECTIONS DE COMMUNE - EXISTENCE, LE MAIRE DE LA COMMUNE ÉTANT RESPONSABLE DU RECOUVREMENT DES CRÉANCES DE CES SECTIONS - CONSÉQUENCE - MISE EN DEMEURE D'ÉMETTRE L'ÉTAT DE RECOUVREMENT DEVANT ÊTRE ADRESSÉ AU MAIRE PAR LE PRÉFET.
</SCT>
<ANA ID="9A"> 135-01-07-01 Il résulte du IV de l'article 1er de la loi n° 80-539 du 16 juillet 1980, qui est applicable aux sections de commune, que le recouvrement des créances qui résulte, pour la section d'une commune, de l'annulation d'une délibération du conseil municipal de cette commune prévoyant la répartition entre les membres de la section des produits de la vente de l'un des biens de cette dernière, est assuré par le maire de la commune, au budget de laquelle les recettes en cause sont rattachées, sous forme de budget annexe ou d'état spécial annexé.... ,,C'est, dès lors, au maire que doit être adressée, par le représentant de l'Etat, la mise en demeure préalable à l'émission d'office, par ce dernier, d'états de recouvrement des créances en cause.</ANA>
<ANA ID="9B"> 54-06-07 Il résulte du IV de l'article 1er de la loi n° 80-539 du 16 juillet 1980, qui est applicable aux sections de commune, que le recouvrement des créances qui résulte, pour la section d'une commune, de l'annulation d'une délibération du conseil municipal de cette commune prévoyant la répartition entre les membres de la section des produits de la vente de l'un des biens de cette dernière, est assuré par le maire de la commune, au budget de laquelle les recettes en cause sont rattachées, sous forme de budget annexe ou d'état spécial annexé.... ,,C'est, dès lors, au maire que doit être adressée, par le représentant de l'Etat, la mise en demeure préalable à l'émission d'office, par ce dernier, d'états de recouvrement des créances en cause.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
