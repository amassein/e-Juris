<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036945762</ID>
<ANCIEN_ID>JG_L_2018_05_000000413267</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/94/57/CETATEXT000036945762.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 25/05/2018, 413267</TITRE>
<DATE_DEC>2018-05-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413267</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP FOUSSARD, FROGER ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Julie Burguburu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:413267.20180525</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Les associations Présence les Terrasses de la Garonne, France Nature Environnement Midi-Pyrénées et Nature Midi-Pyrénées ont demandé au juge des référés du tribunal administratif de Toulouse, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de l'arrêté du préfet de Haute-Garonne du 12 juillet 2017 portant dérogation aux interdictions de capture, enlèvement, destruction, perturbation intentionnelle de spécimens d'espèces animales protégées, de destruction, altération, dégradation de sites de reproduction ou d'aires de repos d'espèces animales protégées et d'arrachage et d'enlèvement de spécimens d'espèces végétales protégées, dans le cadre de la réalisation du centre commercial et de loisirs " Val Tolosa ", sur le territoire de la commune de Plaisance du Touch (Haute-Garonne).<br/>
<br/>
              Par une ordonnance n° 1703391 du 28 juillet 2017, le juge des référés du tribunal administratif de Toulouse a prononcé la suspension de cet arrêté.<br/>
<br/>
              Par un pourvoi et un nouveau mémoire enregistrés le 10 août 2017 et le 7 mai 2018 au secrétariat du contentieux du Conseil d'Etat, la SAS PCE et la SNC Foncière Toulouse Ouest (FTO) demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé de rejeter la demande aux fins de suspension présentée devant le tribunal administratif de Toulouse par les associations Présence les Terrasses de la Garonne, et autres ;<br/>
<br/>
              3°) de mettre à la charge des associations Présence les Terrasses de la Garonne, France Nature Environnement Midi-Pyrénées et Nature Midi-Pyrénées la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - la directive n° 92/43/CEE du Conseil du 21 mai 1992 ;<br/>
              - le code de l'environnement ;<br/>
              - le code de l'urbanisme ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Julie Burguburu, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la SAS PCE et autre et à la SCP Foussard, Froger, avocat des associations Présence les Terrasses de la Garonne et autres.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis au juge des référés qu'à la suite d'une demande présentée par les sociétés PCE et FTO, titulaires d'un permis de construire pour la construction d'un centre commercial et de loisirs dit " Val Tolosa ", délivré le 5 août 2016 par le maire de la commune de Plaisance du Touch (Haute-Garonne), le préfet de la Haute-Garonne leur a accordé, par un arrêté du 12 juillet 2017 pris sur le fondement des dispositions du 4° du I de l'article L. 411-2 du code de l'environnement, une dérogation aux interdictions figurant au 1° et 2° du I de l'article L. 411-1 du même code relatives à la protection des espèces animales et végétales, portant au total sur quarante-six espèces protégées. Les sociétés requérantes se pourvoient en cassation contre l'ordonnance du juge des référés du tribunal administratif de Toulouse du 12 juillet 2017 ayant suspendu l'exécution de cet arrêté en application de l'article L. 521-1 du code de justice administrative.<br/>
<br/>
              Sur les moyens relatifs à la condition d'urgence :<br/>
<br/>
              3. En premier lieu, pour juger que l'urgence justifiait la suspension de l'exécution de l'arrêté du 12 juillet 2017 sur le fondement de l'article L. 521-1 du code de justice administrative, le juge des référés du tribunal administratif de Toulouse s'est notamment fondé sur la présence sur le site d'espèces protégées au plan national, les risques induits pour ces espèces par les dérogations accordées et l'imminence de la réalisation des travaux de construction. Ce faisant, il a, eu égard à son office, suffisamment motivé son ordonnance et n'a ni commis d'erreur de droit ni dénaturé les pièces du dossier. Par ailleurs, si les sociétés requérantes soutiennent que le juge des référés aurait commis une erreur de droit et dénaturé les pièces du dossier en se fondant notamment sur l'existence de risques à l'égard de l'espèce sérapias en coeur, il résulte des termes mêmes de l'ordonnance attaquée que celle-ci se borne à relever la présence, qui n'était pas contestée,  de cette espèce sur le site.<br/>
<br/>
              4. En deuxième lieu, le juge des référés a pu, sans erreur de droit dans le cadre de son appréciation globale de l'urgence au vu de la situation d'espèce, tenir compte, en complément des risques induits pour des espèces protégées et de l'imminence de la réalisation de travaux, de la circonstance que les sociétés bénéficiaires de l'arrêté en cause avaient fait l'objet d'une procédure de manquement et d'une mise en demeure du fait des condition d'exécution d'une précédente dérogation prise en application des dispositions de l'article L. 411-2 du code de l'environnement et que les mesures d'évitement, de réduction et de compensation prévues par la dérogation litigieuse pourraient également ne pas être respectées par les sociétés requérantes.<br/>
<br/>
              5. Enfin si les requérantes soutiennent que le juge des référés a omis de répondre à un moyen en défense tiré de l'intérêt général s'attachant à la réalisation du centre commercial et de loisirs dit " Val Tolosa " et à l'urgence de permettre l'exécution de l'arrêté attaqué sans attendre le jugement de la requête au fond, il ressort des écritures devant le juge des référés que les parties en défense n'ont développé aucune argumentation de nature à formaliser un tel moyen. Par suite, l'ordonnance du juge des référées n'est entachée à ce titre ni d'une insuffisance de motivation, ni d'erreur de droit.<br/>
<br/>
              Sur les moyens relatifs à l'existence, en l'état de l'instruction, d'un doute sérieux quant à la légalité de l'arrêté préfectoral attaqué :<br/>
<br/>
              6. Le I de l'article L. 411-1 du code de l'environnement comporte une série d'interdictions visant à assurer la conservation d'espèces animales ou végétales protégées et de leurs habitats. Figurent ainsi, au 1° de cet article, " La destruction ou l'enlèvement des oeufs ou des nids, la mutilation, la destruction, la capture ou l'enlèvement, la perturbation intentionnelle, la naturalisation d'animaux de ces espèces ou, qu'ils soient vivants ou morts, leur transport, leur colportage, leur utilisation, leur détention, leur mise en vente, leur vente ou leur achat ", et au 2° du même article, " La destruction, la coupe, la mutilation, l'arrachage, la cueillette ou l'enlèvement de végétaux de ces espèces, de leurs fructifications ou de toute autre forme prise par ces espèces au cours de leur cycle biologique, leur transport, leur colportage, leur utilisation, leur mise en vente, leur vente ou leur achat, la détention de spécimens prélevés dans le milieu naturel ". Toutefois, le 4° du I de l'article L. 411-2 du même code permet à l'autorité administrative de délivrer des dérogations à ces interdictions dès lors que dont remplies les trois conditions distinctes et cumulatives tenant d'une part, à l'absence de solution alternative satisfaisante, d'autre part, à la condition de ne pas nuire " au maintien, dans un état de conservation favorable, des populations des espèces concernées dans leur aire de répartition naturelle " et, enfin, à la justification de la dérogation par l'un des cinq motifs qu'il énumère limitativement. Parmi ces motifs, figure : " c) (...) l'intérêt de la santé et de la sécurité publiques ou (pour) d'autres raisons impératives d'intérêt public majeur, y compris de nature sociale ou économique, et (pour) des motifs qui comporteraient des conséquences bénéfiques primordiales pour l'environnement ". <br/>
<br/>
              7. Il résulte de ces dispositions qu'un projet d'aménagement ou de construction d'une personne publique ou privée susceptible d'affecter la conservation d'espèces animales ou végétales protégées et de leurs habitats ne peut être autorisé, à titre dérogatoire, que s'il répond, par sa nature et compte tenu notamment du projet urbain dans lequel il s'inscrit, à une raison impérative d'intérêt public majeur. En présence d'un tel intérêt, le projet ne peut cependant être autorisé, eu égard aux atteintes portées aux espèces protégées appréciées en tenant compte des mesures de réduction et de compensation prévues, que si, d'une part, il n'existe pas d'autre solution satisfaisante et, d'autre part, cette dérogation ne nuit pas au maintien, dans un état de conservation favorable, des populations des espèces concernées dans leur aire de répartition naturelle. <br/>
<br/>
              8. Pour juger que le moyen tiré de ce que le projet de centre commercial " Val Tolosa " ne répondrait pas aux conditions posées par l'article L. 411-2 du code de l'environnement était propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de l'arrêté attaqué, le juge des référés s'est approprié l'analyse développée par la cour administrative d'appel de Bordeaux relativement à un précédent arrêté de dérogation délivré aux sociétés requérantes concernant le projet en cause qui l'avait conduite à écarter une telle qualification et s'est en outre fondé sur le fait que l'accord cadre signé le 31 mars 2016 signé entre les sociétés requérantes, le président du conseil départemental et le maire de Plaisance-du-Touch, ainsi que les différentes modifications apportées au projet, venant renforcer son caractère d'intérêt général, ne permettait néanmoins pas de caractériser un intérêt public majeur à la réalisation de ce projet. Eu égard à l'office que lui attribue l'article L. 521-1 du code de justice administrative, le juge des référés du tribunal administratif de Toulouse n'a, ce faisant, pas commis d'erreur de droit et n'a pas davantage entaché son ordonnance de dénaturation. <br/>
<br/>
              9. Il résulte de tout ce qui précède que les sociétés requérantes ne sont pas fondées à demander l'annulation de l'ordonnance du juge des référés du tribunal administratif de Toulouse qu'elles attaquent. <br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la SAS PCE et autre une somme de 1 000 euros  à verser aux associations Présence les Terrasses de la Garonne, France Nature Environnement Midi-Pyrénées et Nature Midi-Pyrénées chacune sur le fondement de l'article L. 761-1 du code de justice administrative. En revanche, les dispositions de cet article font obstacle à ce qu'il soit fait droit aux conclusions de la SAS PCE et autre tendant aux mêmes fins.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
      Article 1er : Le pourvoi de la SAS PCE et autre est rejeté. <br/>
<br/>
Article 2 : La SAS PCE et autre verseront aux associations Présence les Terrasses de la Garonne, France Nature Environnement Midi-Pyrénées et Nature Midi-Pyrénées, une somme  de 1 000 euros chacune au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la SAS PCE, première dénommée, pour l'ensemble des requérants, à l'association Présence les Terrasses de la Garonne, première dénommée pour l'ensemble des défendeurs, au ministre d'Etat, ministre de la transition écologique et solidaire et à la commune de Plaisance-du-Touch.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
