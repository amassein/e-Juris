<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036800413</ID>
<ANCIEN_ID>JG_L_2018_04_000000417333</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/80/04/CETATEXT000036800413.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 09/04/2018, 417333</TITRE>
<DATE_DEC>2018-04-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417333</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:417333.20180409</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 417333, par une requête enregistrée le 15 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, la Confédération générale du travail demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution des articles 3 et 8 de l'ordonnance n° 2017-1385 du 22 septembre 2017 relative au renforcement de la négociation collective ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              S'agissant de l'article 3 de l'ordonnance contestée, la Confédération générale du travail soutient que :<br/>
<br/>
              - il y a urgence à en suspendre l'exécution, dès lors que ses modalités d'application ont été précisées par le décret n° 2017-1880 du 29 décembre 2017, que cet article permet l'application du régime juridique prévu au nouvel article L. 2254-2 du code du travail à l'ensemble des accords conclus afin de répondre aux nécessités liées au fonctionnement de l'entreprise ou en vue de préserver, ou de développer l'emploi, ce qui excède le champ de la loi d'habilitation, et que les salariés qui refuseraient la modification de leur contrat de travail induite par l'accord d'entreprise validé seront brutalement licenciés, sans pouvoir contester utilement la rupture de leur contrat de travail, alors que l'illégalité du dispositif est certaine ;<br/>
<br/>
              - il y a un doute sérieux sur sa légalité ;<br/>
<br/>
              - il méconnaît le champ de la loi d'habilitation du 15 septembre 2017, d'une part, en visant, par une définition imprécise, des accords qui ne sont pas concernés par cette loi, et, d'autre part, en prévoyant que le licenciement qui repose sur un motif spécifique constitue une cause réelle et sérieuse, assimilation que cette loi n'autorise pas ;<br/>
<br/>
              - en prévoyant que les stipulations de l'accord d'entreprise se substituent de plein droit aux clauses contraires et incompatibles du contrat de travail, y compris en matière de rémunération, de durée du travail et de mobilité professionnelle ou géographique interne à l'entreprise, il porte à la liberté contractuelle une atteinte grave qui n'est justifiée par aucun motif général ;<br/>
<br/>
              - il méconnaît le principe d'égalité en permettant à l'employeur de ne licencier que les salariés de son choix qui auront refusé la modification du contrat de travail découlant de l'accord d'entreprise validé ;<br/>
<br/>
              - il porte atteinte au droit de mener une vie familiale normale garanti par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, car il permet à l'accord d'entreprise de modifier les contrats de travail, notamment en matière de temps de travail, de rémunération ou de conditions de mobilité professionnelle ou géographique ; <br/>
<br/>
              - il méconnaît le droit à l'emploi, dès lors, d'une part, qu'il pose un motif spécifique de licenciement constitué par le refus du salarié de se voir imposer les modifications de son contrat de travail induites par un accord collectif, et, d'autre part, qu'il exclut le salarié licencié du bénéfice du reclassement ;<br/>
<br/>
              - il méconnaît le droit au recours, dès lors que la cause réelle et sérieuse du licenciement est reconnue par avance ;<br/>
<br/>
              - il méconnaît le paragraphe 2 de l'article 2 de la directive 89/59/CE du 20 juillet 1998 concernant le rapprochement des législations des Etats membres relatives aux licenciements collectifs, en l'absence de procédure d'information ou de consultation sur les licenciements.<br/>
<br/>
               S'agissant de l'article 8 de l'ordonnance, la Confédération générale du travail soutient que :<br/>
<br/>
              - il y a urgence à en suspendre l'exécution, dès lors que ses modalités d'application ont été précisées par le décret n° 2017-1880 du 29 décembre 2017 que ses dispositions permettent à l'employeur de contourner le cas échéant l'opposition des syndicats représentatifs et majoritaires, sans qu'aucune mesure précise n'encadre une telle situation, et que l'identification du " personnel " visé à l'article L. 2232-22 est imprécise ;<br/>
<br/>
              - il y a un doute sérieux sur sa légalité ;<br/>
<br/>
              - il méconnaît le champ de la loi d'habilitation du 15 septembre 2017, qui ne parle que des conditions de mise en oeuvre de la négociation collective, alors qu'une consultation des salariés sur un projet d'accord revient à contourner toute négociation dans les entreprises de moins de onze salariés et dans celles qui comptent entre onze et vingt salariés ;<br/>
<br/>
              - il est entaché d'incompétence négative, car il renvoie au pouvoir réglementaire le soin de fixer les modalités d'organisation de la consultation des salariés, dispositions qui relèvent des principes fondamentaux de niveau législatif, sans définir les garanties de nature à assurer les droits constitutionnels que les salariés tirent des cinquième et huitième alinéas du préambule de la Constitution de 1946 et de l'article 11 de la Déclaration des droits de l'homme et du citoyen ;<br/>
<br/>
              - il porte atteinte au principe d'intelligibilité et de clarté de la norme, puisqu'il prévoit que le projet d'accord d'entreprise doit être ratifié à la majorité des deux tiers du personnel, sans définir de quel personnel il s'agit ;<br/>
<br/>
              - il porte atteinte à la liberté syndicale, en l'absence d'obligation de l'employeur d'une entreprise de moins de cinquante salariés d'informer les organisations syndicales représentatives de la négociation en cours.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 417413, par une requête enregistrée le 17 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, la Confédération générale du travail demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de l'ordonnance n° 2017-1386 du 22 septembre 2017 relative à la nouvelle organisation du dialogue social et économique dans l'entreprise et favorisant l'exercice et la valorisation des responsabilités syndicales ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              La Confédération générale du travail soutient que :<br/>
<br/>
              - il y a urgence à en suspendre l'exécution, dès lors que ses modalités d'application ont été précisées par le décret n° 2017-1819 du 29 décembre 2017, la disparition d'institutions représentatives du personnel porte une atteinte grave et immédiate aux intérêts qu'elle entend défendre, que la suppression du comité d'hygiène, de sécurité et des conditions de travail et la réduction des attributions, des moyens d'actions et financiers des nouveaux comités sociaux et économiques auront pour effet la dégradation de la santé et la sécurité des salariés, enfin que l'impossibilité de demander l'organisation d'élections professionnelles dans les six mois suivant l'établissement d'un procès-verbal de carence entraînera une carence de long terme dans la représentation salariale ;<br/>
<br/>
              - il existe un doute sérieux quant à la légalité de l'ordonnance contestée ;<br/>
<br/>
              - l'ordonnance est entachée d'une irrégularité de procédure, dès lors que ses dispositions ne correspondent ni au texte du projet soumis par le Gouvernement au Conseil d'Etat, ni au texte résultant de l'avis émis par le Conseil d'Etat ;<br/>
<br/>
              - elle a été prise sur l'habilitation donnée au Gouvernement par la loi n° 2017-1340 du 15 septembre 2017, qui méconnaît elle-même les stipulations des articles 2, 8 et 11 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et les dispositions des articles 10 et 11 de la directive 89/391/CEE du 12 juin 1989 ;<br/>
<br/>
              - la suppression du comité d'hygiène, de sécurité et des conditions de travail méconnaît le droit à la santé des salariés et leur droit de participation à la détermination collective des conditions de travail dans l'entreprise, protégés par les huitième et onzième alinéas du préambule de la Constitution de 1946, dès lors que cette suppression n'est pas assortie des garanties nécessaires dans le cadre de la création du comité social et économique ;<br/>
<br/>
              - le nouvel article L. 2312-5 du code du travail méconnaît le principe d'égalité, en ce qu'il institue une différence de traitement, en premier lieu, entre le comité social et économique des entreprises de plus de cinquante salariés et celui des entreprises de onze à quarante-neuf salariés tant au regard des prérogatives des membres de ces comités que la personnalité civile de ces derniers, et, en deuxième lieu, entre les membres des comités sociaux et économiques chargés des attributions en matière de sécurité, de santé et de conditions de travail dans les entreprises de onze à deux cent quatre-vingt-dix-neuf salariés et les membres de la commission en charge de ces attributions au sein des comités sociaux et économiques des entreprises de plus de trois cents salariés ;<br/>
<br/>
              - le nouvel article L. 2314-30 du code du travail, par l'exigence de parité qu'il énonce pour les élections professionnelles, porte à la liberté syndicale, garantie par le sixième alinéa de ce préambule et l'article 11 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, une atteinte disproportionnée au regard de l'objectif poursuivi ;<br/>
<br/>
              - le nouvel article L. 2314-33 du code du travail méconnaît le principe de participation énoncé par le huitième alinéa du préambule de la Constitution de 1946, en ce qu'il limite à trois le nombre de mandats successifs des membres des comités sociaux et économiques, excepté dans les entreprises de moins de 50 salariés, sans possibilité d'y déroger par un accord collectif ;<br/>
<br/>
              - cet article méconnaît la liberté syndicale, garantie par le sixième alinéa du préambule de la Constitution de 1946, l'article 11 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et l'article 3 de la convention n° 87 de l'Organisation internationale du travail, en ce que la limitation du nombre de mandats successifs dans les comités sociaux et économiques prive les organisations syndicales de la possibilité de choisir librement les représentants aptes à se présenter aux élections professionnelles ;<br/>
<br/>
              - le nouvel article L. 2315-80 du code du travail porte atteinte au droit à la protection de la santé des salariés et au droit de participation des salariés à la détermination des conditions de travail dans l'entreprise, dès lors qu'il met à la charge des comités sociaux et économiques 20 % du coût d'expertise décidée par l'institution représentative du personnel, s'agissant notamment du recours à l'expertise en cas de projet important modifiant les conditions de santé et de sécurité ou les conditions de travail prévu au 2° du nouvel article L. 2315-96 ;<br/>
<br/>
              - cet article méconnaît le principe d'égalité en ce qu'il institue une différence injustifiée entre les salariés et leurs représentants d'une entreprise dont le comité économique et social bénéficie d'un budget suffisant pour participer au financement des expertises et ceux dont le comité économique et social ne le pourra pas ;<br/>
<br/>
              - les nouveaux articles L. 2321-1 à L. 2321-10 du code du travail méconnaissent les principes d'intelligibilité et de clarté de la norme en ce qu'ils ne permettent pas de déterminer, d'une part, la composition des conseils d'entreprise, le mode de désignation de leurs membres et, d'autre part, le mode de calcul de la validité d'un accord collectif d'entreprise conclu par des membres du conseil d'entreprise si aucun d'eux ne dispose d'un score personnel ;<br/>
<br/>
              - ces articles méconnaissent la liberté syndicale garantie par le sixième alinéa du préambule de la Constitution de 1946 et l'article 11 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales en ce que certaines organisations syndicales réunissant au moins 50 % des suffrages peuvent décider, par accord collectif, de confier l'exercice du droit à la négociation collective à une institution élue du personnel, le conseil d'entreprise, ce qui prive les organisations syndicales de ce droit, à l'exception des accords relatifs aux élections professionnelles et à un plan de sauvegarde de l'emploi.<br/>
<br/>
<br/>
<br/>
              3° Sous le n° 417416, par une requête enregistrée le 17 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, la Confédération générale du travail demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution des articles 4 et 16 de l'ordonnance n° 2017-1387 du 22 septembre 2017 relative à la prévisibilité et la sécurisation des relations de travail ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              S'agissant de l'article 4 de l'ordonnance contestée, la Confédération générale du travail soutient que :<br/>
<br/>
              - il y a urgence à en suspendre l'exécution, dès lors que ses modalités d'application ont été précisées par le décret n° 2017-1702 du 15 décembre 2017 et qu'il permet à l'employeur de licencier sans avoir à formuler un motif précis dans la lettre de licenciement, privant ainsi les salariés du droit de connaître le motif de leur licenciement ;<br/>
<br/>
              - il y a un doute sérieux sur sa légalité ;<br/>
<br/>
              - la nouvelle rédaction de l'article L. 1235-2 du code du travail méconnaît le champ du c) du 1° de l'article 3 de la loi du 15 septembre 2017, qui n'habilitait pas le Gouvernement à permettre à l'employeur de modifier, après la notification de la lettre de licenciement, les motifs énoncés dans cette lettre, et encore moins à remettre en cause la jurisprudence de la chambre sociale de la Cour de cassation, qui impose à l'employeur d'énoncer un ou plusieurs motifs précis dans la lettre de licenciement, sous peine d'absence de cause réelle et sérieuse ;<br/>
              - les dispositions de l'article L. 1235-2 du code du travail sont entachées d'incompétence négative, dès lors qu'elles renvoient au pouvoir réglementaire le soin de fixer les délais, mais aussi les conditions, dans lesquels l'employeur pourra préciser les motifs énoncés dans la lettre de licenciement ;<br/>
<br/>
              - ces dispositions portent atteinte au droit à l'emploi, dès lors qu'elles n'imposent plus à l'employeur de motiver la lettre de licenciement, ni, par suite, de justifier celui-ci ;<br/>
<br/>
              - ces dispositions méconnaissent les stipulations de l'article 7 de la convention n° 158 de l'Organisation internationale du travail, en permettant à l'employeur de justifier le licenciement à posteriori, alors que ces stipulations exigent la connaissance des motifs par un salarié avant son licenciement afin de lui permettre de se défendre contre les allégations formulées à son encontre ;<br/>
<br/>
              - ces dispositions méconnaissent le droit à un recours effectif protégé par l'article 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, dès lors que l'insuffisante motivation de la lettre de licenciement privera le salarié de la faculté d'exercer un recours utile contre son licenciement.<br/>
<br/>
              S'agissant de l'article 16 de l'ordonnance contestée, la Confédération générale du travail soutient que :<br/>
<br/>
              - il y a urgence à en suspendre l'exécution, dès lors que ses modalités d'application ont été précisées par le décret n° 2017-1725 du 21 décembre 2017 et qu'il revoit le périmètre des possibilités de reclassement, ce qui va conduire à priver de manière irrémédiable des salariés de leur emploi ;<br/>
<br/>
              -  il y a un doute sérieux sur sa légalité ;<br/>
<br/>
              - il méconnaît le champ de la loi du 15 septembre 2017, qui habilitait le Gouvernement à préciser les conditions dans lesquelles l'employeur satisfait à son obligation de reclassement, mais non à revoir la définition ou la portée de l'obligation de reclassement elle-même ;<br/>
<br/>
              - les nouvelles dispositions de l'article L. 1233-4 du code du travail portent atteinte au droit à l'emploi, dès lors qu'elles permettent à l'employeur de s'affranchir de l'obligation d'adresser à chaque salarié une offre de reclassement personnalisée.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 2018-217 du 29 mars 2018 ;<br/>
      - le code de justice administrative ;		<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les requêtes visées ci-dessus présentant à juger les mêmes questions, il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". L'article L. 522-3 de ce code prévoit que le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              3. Postérieurement à l'introduction des requêtes, le législateur a, par les articles 1er, 3 et 10 de la loi du 29 mars 2018 susvisée, ratifié les ordonnances n° 2017-1385, n° 2017-1386 et n° 2017-1387 du 22 septembre 2017. La légalité de ces ordonnances n'est donc plus susceptible d'être discutée par la voie du recours pour excès de pouvoir. Ainsi, les conclusions des requêtes tendant à ce que le juge des référés du Conseil d'Etat suspende, sur le fondement de l'article L. 521-1 du code de justice administrative, l'exécution de tout ou partie de ces ordonnances sont devenues sans objet.<br/>
<br/>
              4. Il n'y pas lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat les sommes que la Confédération générale du travail demande au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions des requêtes de la Confédération générale du travail tendant à la suspension de l'exécution des ordonnances n° 2017-1385, n° 2017-1386 et n° 2017-1387 du 22 septembre 2017.<br/>
Article 2 : Le surplus des conclusions des requêtes est rejeté.<br/>
Article 3 : La présente ordonnance sera notifiée à la Confédération générale du travail.<br/>
Copie en sera adressée au Premier ministre et à la ministre du travail.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-045 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. - RATIFICATION D'UNE ORDONNANCE POSTÉRIEUREMENT À L'INTRODUCTION D'UNE REQUÊTE TENDANT À LA SUSPENSION PARTIELLE DE SON EXÉCUTION (ART. L. 521-1 DU CJA) - CONSÉQUENCE - NON-LIEU [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-035-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CODE DE JUSTICE ADMINISTRATIVE). - RATIFICATION D'UNE ORDONNANCE POSTÉRIEUREMENT À L'INTRODUCTION, SUR LE FONDEMENT DE L'ARTICLE L. 521-1 DU CJA, D'UNE REQUÊTE TENDANT À LA SUSPENSION PARTIELLE DE SON EXÉCUTION - CONSÉQUENCE - NON-LIEU [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-05-05-02 PROCÉDURE. INCIDENTS. NON-LIEU. EXISTENCE. - RATIFICATION D'UNE ORDONNANCE POSTÉRIEUREMENT À L'INTRODUCTION, SUR LE FONDEMENT DE L'ARTICLE L. 521-1 DU CJA, D'UNE REQUÊTE TENDANT À LA SUSPENSION PARTIELLE DE SON EXÉCUTION [RJ1].
</SCT>
<ANA ID="9A"> 01-01-045 Législateur ayant procédé à la ratification d'une ordonnance postérieurement à l'introduction, sur le fondement de l'article L. 521-1 du code de justice administrative (CJA), d'une requête tendant à la suspension de l'exécution de certains de ses articles. La légalité de cette ordonnance n'étant plus susceptible d'être discutée par la voie du recours pour excès de pouvoir, les conclusions de la requête tendant à ce que le juge des référés du Conseil d'Etat suspende l'exécution de tout ou partie de cette dernière sont devenues sans objet.</ANA>
<ANA ID="9B"> 54-035-02 Législateur ayant procédé à la ratification d'une ordonnance postérieurement à l'introduction, sur le fondement de l'article L. 521-1 du code de justice administrative (CJA), d'une requête tendant à la suspension de l'exécution de certains de ses articles. La légalité de cette ordonnance n'étant plus susceptible d'être discutée par la voie du recours pour excès de pouvoir, les conclusions de la requête tendant à ce que le juge des référés du Conseil d'Etat suspende l'exécution de tout ou partie de cette dernière sont devenues sans objet.</ANA>
<ANA ID="9C"> 54-05-05-02 Législateur ayant procédé à la ratification d'une ordonnance postérieurement à l'introduction, sur le fondement de l'article L. 521-1 du code de justice administrative (CJA), d'une requête tendant à la suspension de l'exécution de certains de ses articles. La légalité de cette ordonnance n'étant plus susceptible d'être discutée par la voie du recours pour excès de pouvoir, les conclusions de la requête tendant à ce que le juge des référés du Conseil d'Etat suspende l'exécution de tout ou partie de cette dernière sont devenues sans objet.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant d'un recours en rectification d'erreur matérielle, CE, 12 juin 2002, M.,, n° 241851, p. 217 ; s'agissant d'un refus d'abroger certains articles d'une ordonnance ratifiée, CE, 23 octobre 2002, Société Laboratoires Juva santé, n° 232945, T. pp. 650-881.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
