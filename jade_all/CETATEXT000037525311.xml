<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037525311</ID>
<ANCIEN_ID>JG_L_2018_10_000000383070</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/52/53/CETATEXT000037525311.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 24/10/2018, 383070, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-10-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383070</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Luc Nevache</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:383070.20181024</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une décision du 24 février 2016, le Conseil d'Etat, statuant au contentieux, après avoir annulé la décision implicite par laquelle le Premier ministre avait refusé de prendre le décret d'application prévu au deuxième alinéa de l'article L. 146-5 du code de l'action sociale et des familles et enjoint au Premier ministre de prendre ce décret, a prononcé une astreinte de cent euros par jour à l'encontre de l'Etat s'il ne justifiait pas, dans les neuf mois suivant sa notification, avoir exécuté cette décision.<br/>
<br/>
              Par une décision du 31 mars 2017, le Conseil d'Etat, statuant au contentieux, a procédé à la liquidation provisoire de l'astreinte au titre de la période comprise entre le 2 décembre 2016 et le 24 mars 2017. <br/>
<br/>
              La section du rapport et des études du Conseil d'Etat a exécuté les diligences qui lui incombent en vertu du code de justice administrative. <br/>
<br/>
              Par deux nouveaux mémoires, enregistrés au secrétariat du contentieux du Conseil d'Etat les 12 mars et 18 avril 2018, M. B...A...et l'Association nationale pour l'intégration des personnes handicapées moteurs concluent à ce qu'il soit procédé à une nouvelle liquidation de l'astreinte et à sa majoration à 250 euros par jour de retard. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Nevache, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Par une décision du 24 février 2016, sur la demande de M. A...et de l'Association nationale pour l'intégration des personnes handicapées moteurs, le Conseil d'Etat statuant au contentieux a annulé la décision implicite par laquelle le Premier ministre avait refusé de prendre le décret d'application prévu au deuxième alinéa de l'article L. 146-5 du code de l'action sociale et des familles, enjoint au Premier ministre de prendre ce décret dans le délai de neuf mois à compter de la notification de cette décision et prononcé une astreinte de cent euros par jour à l'encontre de l'Etat s'il ne justifiait pas, dans ce délai, l'avoir exécutée. <br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 911-7 du code de justice administrative : " En cas d'inexécution totale ou partielle ou d'exécution tardive, la juridiction procède à la liquidation de l'astreinte qu'elle avait prononcée ".<br/>
<br/>
              3. La décision du Conseil d'Etat du 24 février 2016 a été notifiée au Premier ministre le 2 mars 2016. Par une décision du 31 mars 2017, le Conseil d'Etat a procédé à la liquidation provisoire de l'astreinte prononcée par cette décision au titre de la période comprise entre le 2 décembre 2016 et le 24 mars 2017. A la date du 15 octobre 2018, le Premier ministre n'avait pas communiqué à la section du rapport et des études du Conseil d'Etat copie des actes justifiant des mesures prises pour exécuter la décision du 24 février 2016. Le Premier ministre doit être, par suite, regardé comme n'ayant pas, à cette date, exécuté cette décision. Il y a lieu, dès lors, et en dépit de l'adoption par l'Assemblée nationale en première lecture, le 17 mai 2018, d'une proposition de loi visant notamment à modifier le deuxième alinéa de l'article L. 146-5 du code de l'action sociale et des familles, de procéder au bénéfice de M. A...et de l'Association nationale pour l'intégration des personnes handicapées moteurs à la liquidation de l'astreinte pour la période du 25 mars 2017 au 15 octobre 2018, au taux de cent euros par jour, soit cinquante-sept mille euros dont quatorze mille deux cent cinquante euros pour M. A...et quarante-deux mille sept cent cinquante euros pour l'association. <br/>
<br/>
              4. Il n'y a pas lieu, en revanche, de majorer le taux de l'astreinte fixé par la décision du 24 février 2016.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'Etat est condamné à verser la somme de quatorze mille deux cent cinquante euros à M. A...et la somme de quarante-deux mille sept cent cinquante euros à l'Association nationale pour l'intégration des personnes handicapées moteurs.  <br/>
Article 2 : Le surplus des conclusions de M.  A...et de l'Association nationale pour l'intégration des personnes handicapées moteurs est rejeté.<br/>
Article 3 : La présente décision sera notifiée à M. B... A..., à l'Association nationale pour l'intégration des personnes handicapées moteurs, au Premier ministre, à la ministre des solidarités et de la santé et à la secrétaire d'Etat auprès du Premier ministre, chargée des personnes handicapées.<br/>
Copie en sera adressée au ministère public près la Cour de discipline budgétaire et financière et à la section du rapport et des études du Conseil d'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
