<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044945852</ID>
<ANCIEN_ID>JG_L_2021_12_000000459195</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/94/58/CETATEXT000044945852.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 24/12/2021, 459195, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>459195</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:459195.20211224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
              Par une requête, enregistrée le 7 décembre 2021 au secrétariat du contentieux du Conseil d'Etat, la formation politique Les Patriotes demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution du décret n° 2021-1521 du 25 novembre 2021 modifiant le décret n° 2021-699 du 1er juin 2021 prescrivant les mesures générales nécessaires à la gestion de la sortie de crise sanitaire ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
              - la condition d'urgence est satisfaite dès lors que, d'une part, la réduction de la durée de validité des tests porte une atteinte grave à leurs libertés fondamentales et, d'autre part, ils subissent les conséquences de cette réduction de manière particulièrement forte en ce que le passe sanitaire leur est nécessaire pour exercer leur activité professionnelle ; <br/>
              - il existe un doute sérieux quant à la légalité de la décision contestée ; <br/>
              - le décret attaqué est entaché d'un détournement de pouvoir dès lors qu'il a pour objectif implicite d'imposer la vaccination contre la Covid-19 aux personnes non-vaccinées ;<br/>
              - ce décret porte une atteinte disproportionnée au droit de disposer de son corps dès lors que, en premier lieu, il est matériellement difficile pour certains professionnels pour lesquels le passe sanitaire est obligatoire d'obtenir un test toutes les vingt-quatre heures, en deuxième lieu, il n'existe pas de besoin social impérieux justifiant de vacciner toute la population alors que seule une partie de la population est à risque, en troisième lieu, aucune objection de conscience ne permet d'échapper à cette obligation et, en dernier lieu, l'efficacité des vaccins est limitée ;<br/>
              - à titre subsidiaire, la fin de la validité du passe sanitaire à compter du 15 décembre 2021 pour les personnes vaccinées de plus de 65 ans porte une atteinte disproportionnée à leurs libertés fondamentales ;<br/>
              - le dispositif du passe sanitaire porte atteinte au principe d'égalité en ce que, d'une part, les personnes vaccinées ne sont pas soumises à l'obligation de réaliser un test, dès lors que la vaccination n'empêche pas la transmission du virus et, d'autre part, il n'inclut pas les personnes présentant un test sérologique positif à la Covid-19.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2021-689 du 31 mai 2021 ; <br/>
              - la loi n° 2021-1040 du 5 août 2021 ;<br/>
              - la loi n° 2021-1465 du 10 novembre 2021 ;<br/>
              - le décret n° 2021-699 du 1er juin 2021 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. La formation politique les Patriotes demande au juge des référés du Conseil d'Etat, statuant sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution du décret n° 2021-1521 du 25 novembre 2021 modifiant le décret n° 2021-699 du 1er juin 2021 prescrivant les mesures générales nécessaires à la gestion de la sortie de crise sanitaire. <br/>
<br/>
              Sur le cadre juridique du litige :<br/>
<br/>
              3. Le A du II de l'article 1er de la loi du 31 mai 2021 relative à la gestion de la sortie de crise sanitaire, dans sa rédaction issue de la loi du 5 août 2021, prévoit que : " A compter du 2 juin 2021 et jusqu'au 15 novembre 2021 inclus, le Premier ministre peut, par décret pris sur le rapport du ministre chargé de la santé, dans l'intérêt de la santé publique et aux seules fins de lutter contre la propagation de l'épidémie de covid-19 :1° Imposer aux personnes âgées d'au moins douze ans souhaitant se déplacer à destination ou en provenance du territoire hexagonal, de la Corse ou de l'une des collectivités mentionnées à l'article 72-3 de la Constitution, ainsi qu'aux personnels intervenant dans les services de transport concernés, de présenter le résultat d'un examen de dépistage virologique ne concluant pas à une contamination par la covid-19, un justificatif de statut vaccinal concernant la covid-19 ou un certificat de rétablissement à la suite d'une contamination par la covid-19 ; / 2° Subordonner à la présentation soit du résultat d'un examen de dépistage virologique ne concluant pas à une contamination par la covid-19, soit d'un justificatif de statut vaccinal concernant la covid-19, soit d'un certificat de rétablissement à la suite d'une contamination par la covid-19 l'accès à certains lieux, établissements, services ou évènements (...) ". Aux termes de l'article 2-2 du décret du 1er juin 2021 prescrivant les mesures générales nécessaires à la gestion de la sortie de crise sanitaire, dans sa rédaction résultant du décret du 14 octobre 2021 : " Pour l'application du présent décret : / 1° Sont de nature à justifier de l'absence de contamination par la covid-19 un examen de dépistage RT-PCR ou un test antigénique d'au plus 72 heures dans les conditions prévues par le présent décret. Le type d'examen admis peut être circonscrit aux seuls examens de dépistage RT-PCR ou à certains tests antigéniques si la situation sanitaire, et notamment les variants du SARS-CoV-2 en circulation, l'exige. / 2° Un justificatif du statut vaccinal est considéré comme attestant d'un schéma vaccinal complet : a) De l'un des vaccins contre la covid-19 ayant fait l'objet d'une autorisation de mise sur le marché délivrée par la Commission européenne après évaluation de l'Agence européenne du médicament ou dont la composition et le procédé de fabrication sont reconnus comme similaires à l'un de ces vaccins par l'Agence nationale de sécurité des médicaments et des produits de santé :- s'agissant du vaccin "COVID-19 Vaccine Janssen", 28 jours après l'administration d'une dose ; / - s'agissant des autres vaccins, 7 jours après l'administration d'une deuxième dose, sauf en ce qui concerne les personnes ayant été infectées par la covid-19, pour lesquelles ce délai court après l'administration d'une dose ;/ b) D'un vaccin dont l'utilisation a été autorisée par l'Organisation mondiale de la santé et ne bénéficiant pas de l'autorisation ou de la reconnaissance mentionnées au a, à condition que toutes les doses requises aient été reçues, 7 jours après l'administration d'une dose complémentaire d'un vaccin à acide ribonucléique (ARN) messager bénéficiant d'une telle autorisation ou reconnaissance ; / 3° Un certificat de rétablissement à la suite d'une contamination par la covid-19 est délivré sur présentation d'un document mentionnant un résultat positif à un examen de dépistage RT-PCR ou à un test antigénique réalisé plus de onze jours et moins de six mois auparavant. Ce certificat n'est valable que pour une durée de six mois à compter de la date de réalisation de l'examen ou du test mentionnés à la phrase précédente. ". Aux termes du I de l'article 47-1 : " Les personnes majeures et, à compter du 30 septembre 2021, les personnes mineures âgées d'au moins douze ans et deux mois doivent, pour être accueillies dans les établissements, lieux, services et évènements mentionnés aux II et III, présenter l'un des documents suivants : / 1° Le résultat d'un examen de dépistage ou d'un test mentionné au 1° de l'article 2-2 réalisé moins de 72 heures avant l'accès à l'établissement, au lieu, au service ou à l'évènement. Les seuls tests antigéniques pouvant être valablement présentés pour l'application du présent 1° sont ceux permettant la détection de la protéine N du SARS-CoV-2 ; / 2° Un justificatif du statut vaccinal délivré dans les conditions mentionnées au 2° de l'article 2-2 ; / 3° Un certificat de rétablissement délivré dans les conditions mentionnées au 3° de l'article 2-2. / La présentation de ces documents est contrôlée dans les conditions mentionnées à l'article 2-3. / A défaut de présentation de l'un de ces documents, l'accès à l'établissement, au lieu, au service ou à l'évènement est refusé, sauf pour les personnes justifiant d'une contre-indication médicale à la vaccination dans les conditions prévues à l'article 2-4. " En vertu du IV de ce même article : " IV.- Le présent article est applicable, à compter du 30 août 2021, aux salariés, agents publics, bénévoles et aux autres personnes qui interviennent dans les lieux, établissements, services ou évènements concernés, lorsque leur activité se déroule dans les espaces et aux heures où ils sont accessibles au public, à l'exception des activités de livraison et sauf intervention d'urgence. ". Par le décret du 25 novembre 2021 dont la formation politique Les Patriotes demande la suspension de tout ou de certaines des dispositions, le Premier ministre a notamment, en premier lieu, modifié le a) du 2° de l'article 2-2 du décret du 1er juin 2021 pour prévoir qu'à partir du 15 décembre 2021, le schéma vaccinal ne serait regardé comme complet, pour les personnes ayant reçu le vaccin " COVID-19 Vaccine Janssen " et pour celles de soixante-cinq ou plus ayant reçu un autre vaccin mentionné au a) qu'à la condition d'avoir reçu une dose complémentaire d'un vaccin à acide ribonucléique (ARN) messager remplissant les conditions mentionnées au premier alinéa du présent a) entre 1 et 2 mois suivant l'injection de la dose initiale, s'agissant du vaccins Janssen, entre 5 et 7 mois suivant l'injection de la dernière dose requise, s'agissant des autres vaccins. En deuxième lieu, le décret porte, à l'article 47-1 du décret du 1er juin 2021, de 72 à 24 heures la durée de validité du résultat d'un examen de dépistage ou d'un test mentionné au 1° de l'article 2-2. <br/>
<br/>
              Sur la durée de validité des examens de dépistage :<br/>
<br/>
              4. La formation politique Les Patriotes soutient que la décision de réduire à moins de 24 heures la durée pendant laquelle le résultat négatif d'un test ou d'un examen de dépistage permet aux personnes qui ne peuvent justifier ni d'un schéma vaccinal complet ni d'un certificat de rétablissement à la suite d'une contamination par la Covid-19 d'accéder aux établissements, lieux, services et événements mentionnés aux II et III de l'article 47-1 du décret porte une atteinte illégale au principe d'égalité et au droit au respect de la vie privée dès lors qu'elle aggrave, sans aucune justification sanitaire et de manière disproportionnée, l'atteinte à ces droits et libertés que constitue l'obligation pour accéder à certains lieux et activités de détenir un certificat de vaccination ou de rétablissement ou une justification de dépistage récent (" passe sanitaire ").<br/>
<br/>
              5. Il résulte des dernières données scientifiques que la circulation du virus SARS-CoV 2 s'est considérablement accélérée ces dernières semaines sur le territoire métropolitain, avec, selon les données publiées par Santé publique France, un peu moins de 50 000 nouveaux cas confirmés à la date du 19 décembre 2021, un taux de reproduction effectif de 1,2 pour la semaine du 6 décembre  et une forte augmentation du taux d'incidence qui, si elle touche toutes les classes d'âge, est particulièrement élevée chez les personnes entre 12 et 17 ans et entre 30 et 49 ans, qui sont également celles qui présentent le plus faible taux de vaccination. Cette forte reprise épidémique risque de se traduire, à court terme, par une augmentation des hospitalisations, y compris en soins critiques, alors que les lits en réanimation sont déjà occupés à hauteur de 46 % par des patients atteints du virus et que les établissements hospitaliers de plusieurs régions ont commencé à déprogrammer des interventions afin de permettre l'hospitalisation de malades de la covid. Ce risque est accru par l'apparition d'un nouveau variant dont les effets sont encore mal connus. <br/>
<br/>
              6. En premier lieu, la loi du 31 mai 2021 modifiée a donné au Premier ministre le pouvoir de prendre les mesures citées au point 2, au nombre desquelles celle consistant à " subordonner à la présentation soit du résultat d'un examen de dépistage virologique ne concluant pas à une contamination par la covid-19, soit d'un justificatif de statut vaccinal concernant la covid-19, soit d'un certificat de rétablissement à la suite d'une contamination par la covid-19 l'accès à certains lieux, établissements, services ou évènements ". Si la présentation de l'un de ces trois documents suffit à permettre l'accès à ces lieux et activités, où les risques de transmission du virus sont particulièrement élevés, les garanties dont ils attestent au regard du risque sanitaire ne sont pas les mêmes. Ainsi, alors qu'il n'est pas sérieusement contesté que la vaccination, si elle ne la supprime pas, diminue la contagiosité de la personne vaccinée et réduit de manière importante le risque qu'elle développe une forme grave de la maladie, le résultat négatif d'un examen de dépistage établit uniquement que la personne n'est pas porteuse du virus au moment où l'examen a été réalisé. En décidant, compte tenu des circonstances particulières de forte reprise épidémique décrites au point précédent, de réduire la durée pendant laquelle une personne non vaccinée justifiant du résultat négatif d'un examen de dépistage est susceptible d'avoir été contaminée postérieurement à cet examen et, par conséquent, en se rendant sur les lieux et en participant aux activités soumises à l'exigence d'un passe sanitaire, de contaminer d'autres personnes vaccinées ou justifiant seulement d'un test négatif, exposant ces dernières à un risque plus élevé de développer une forme grave de la maladie, le Premier ministre n'a pas, contrairement à ce que soutient la formation politique requérante, fondé cette mesure sur des considérations étrangères à la lutte contre la propagation de l'épidémie et à la protection de la santé publique. <br/>
<br/>
              7. En deuxième lieu, la formation politique Les Patriotes soutient que le délai pour obtenir les résultats d'un test ou d'un examen de dépistage serait tel qu'il serait matériellement impossible ou extrêmement difficile de pouvoir justifier d'un résultat négatif tôt le matin ou le lendemain d'un dimanche ou d'un jour férié. Toutefois, si les résultats d'un test PCR peuvent prendre plusieurs heures et être difficiles à obtenir tôt le matin, il n'en va pas de même des tests antigéniques, qui peuvent être réalisés en pharmacie et dont le résultat est délivré moins d'une demi-heure après sa réalisation.<br/>
<br/>
              8. En troisième lieu, la formation politique Les Patriotes fait valoir que l'obligation pour les personnes ne pouvant justifier de l'une des deux autres conditions permettant l'obtention du passe sanitaire de présenter le résultat négatif d'un examen ou d'un test de dépistage réalisé moins de 24 h avant de se rendre sur les lieux, événements ou d'exercer une activité pour lesquels il est exigé fait peser sur elles une contrainte matérielle et financière d'une importance telle que l'atteinte qu'elle représente aux droits et libertés qu'ils invoquent est disproportionnée et équivaut à une obligation illégale à la vaccination. La mesure litigieuse représente effectivement une contrainte matérielle accrue ainsi qu'une charge financière réelle, en particulier pour les personnes qui, parce qu'elles exercent un emploi soumis à la possession du passe sanitaire, doivent y recourir quotidiennement. Toutefois, compte tenu, d'une part, de ce que, ainsi qu'il a été dit aux points précédents, le contexte sanitaire justifie des mesures de précaution plus importantes, d'autre part, de ce que l'arrêté du 14 octobre 2021 ayant supprimé le remboursement des examens et des tests de dépistage a maintenu leur gratuité dans un certain nombre de cas et notamment pour les mineurs, des conséquences de l'absence de présentation du " passe sanitaire " qui ne peut être opposée à l'accès aux biens et services de première nécessité et qui conduit, pour les salariés, à la mise en place de solutions de substitution, lorsqu'elles sont possibles et, enfin, de ce qu'il n'existe plus aujourd'hui de difficultés pour les personnes qui le peuvent et le souhaitent de se faire vacciner gratuitement, la décision de réduire à moins de 24 heures la durée de validité d'un résultat négatif d'un examen de dépistage, qui ne saurait être regardée comme ayant pour objet de contraindre à la vaccination, n'a pas pour effet de rendre l'exigence de présentation du passe sanitaire dans certaines circonstances disproportionnée au regard des objectifs de santé publique qu'elle poursuit.<br/>
<br/>
              9. En quatrième et dernier lieu, il résulte des dispositions précitées du A du II de l'article 1er de la loi du 31 mai 2021 que l'accès à certains lieux, activités et événements ne peut être subordonné qu'à l'un des trois documents qu'elles mentionnent. Par suite, la requérante ne peut utilement soutenir que le Premier ministre aurait méconnu le principe d'égalité de traitement en n'imposant pas aux personnes vaccinées la justification d'un résultat négatif à un examen de dépistage, au motif que la vaccination ne supprime pas toute contagiosité ou en ne prévoyant pas la désactivation de leur passe sanitaire lorsqu'elles sont testées positives, circonstance qui au demeurant les contraint à demeurer chez elles pendant au moins sept jours. N'est pas plus sérieux, pour le même motif, le moyen tiré de ce que le Premier ministre aurait méconnu le même principe en ne prévoyant pas le cas des personnes présentant un test sérologique positif. <br/>
<br/>
              10. Il résulte de l'ensemble de ce qui précède que les moyens des requêtes dirigés contre le décret litigieux en tant qu'il porte à moins de 24 heures la durée pendant laquelle le résultat négatif à un examen de dépistage permet l'accès aux lieux, événements et activités soumis à la présentation du passe sanitaire ne sont de nature ni à faire naître, un doute sérieux quant à sa légalité, ni à caractériser une atteinte grave et manifestement illégale à une liberté fondamentale.<br/>
<br/>
              Sur l'obligation, pour les personnes de 65 ans ou plus, de recevoir une dose complémentaire de vaccin pour que leur schéma vaccinal reste reconnu comme complet :  <br/>
<br/>
              11. Contrairement à ce que soutient la requérante, la seule circonstance que la défense immunitaire apportée par la vaccination diminue au bout de plusieurs mois, rendant nécessaire, comme le recommandent toutes les autorités de santé, dont la Haute Autorité de santé dans son avis du 18 novembre, un rappel vaccinal, n'a pas pour effet de rendre l'exigence de présentation du passe sanitaire dans certaines circonstances disproportionnée au regard des objectifs de santé publique qu'elle poursuit. Ce moyen n'est par suite, pas de nature à faire naître un doute sérieux sur la légalité de cette disposition.<br/>
              12. Il résulte de l'ensemble de ce qui précède, et sans qu'il soit besoin de se prononcer sur la condition d'urgence, que la demande de la formation politique Les Patriotes tendant, sur le fondement des dispositions de l'article L. 521-1, à la suspension de l'exécution du décret du 25 novembre 2021 ne peut qu'être rejetée selon la procédure prévue par l'article L. 522-3 du même code.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la formation politique Les patriotes est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à la formation politique Les patriotes. <br/>
Fait à Paris, le 24 décembre 2021<br/>
Signé : Gilles Pellissier<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
