<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038088231</ID>
<ANCIEN_ID>JG_L_2019_02_000000411061</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/08/82/CETATEXT000038088231.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 01/02/2019, 411061, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411061</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Sara-Lou Gerber</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:411061.20190201</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Le Parc du Béarn a demandé à la cour administrative de Bordeaux d'annuler la décision implicite de refus de permis de construire valant autorisation d'exploitation commerciale, née du silence gardé par le maire de Lons (Pyrénées-Atlantiques) sur sa demande, déposée le 17 mai 2016, relative à la création d'un centre commercial dénommé " Les Portes du Béarn " d'une surface de vente de 15 343 m². Par une ordonnance n° 17BX00889 du 29 mars 2017, la présidente de la 1ère chambre de la cour administrative d'appel de Bordeaux a rejeté sa requête.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 31 mai et 22 août 2017, la société Le Parc du Béarn demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge des " parties perdantes " la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
     Vu les autres pièces du dossier.<br/>
<br/>
  Vu : <br/>
- le code de commerce ;<br/>
- le code de l'urbanisme ;<br/>
- le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sara-Lou Gerber, Auditeur,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de la société Le Parc du Béarn ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Le Parc du Béarn a déposé auprès du maire de Lons (Pyrénées-Atlantiques), le 17 mai 2016, une demande de permis de construire valant autorisation d'exploitation commerciale en vue de la création d'un ensemble commercial d'une surface totale de vente de 15 343 m². Cette demande a fait l'objet, le 6 juillet 2016, d'un avis favorable de la commission départementale d'aménagement commercial des Pyrénées-Atlantiques. Toutefois, sur recours des sociétés Alis et Willis et du préfet des Pyrénées-Atlantiques, la commission nationale d'aménagement commercial a, le 27 octobre 2016, donné un avis défavorable au projet. La société le Parc du Béarn a demandé à la cour administrative d'appel de Bordeaux d'annuler pour excès de pouvoir la décision implicite de refus de permis de construire valant autorisation d'exploitation commerciale née, selon elle, du silence gardé par le maire de Lons sur sa demande. Par l'ordonnance dont cette société demande l'annulation, la présidente de la 1ère chambre de la cour administrative d'appel de Bordeaux a rejeté sa requête au motif que le silence gardé par le maire avait fait naître une autorisation tacite et que la requête de la société était, par suite, dépourvue d'objet.<br/>
<br/>
              2. Aux termes de l'article L. 425-4 du code de l'urbanisme, "  Lorsque le projet est soumis à autorisation d'exploitation commerciale au sens de l'article L. 752-1 du code de commerce, le permis de construire tient lieu d'autorisation dès lors que la demande de permis a fait l'objet d'un avis favorable de la commission départementale d'aménagement commercial ou, le cas échéant, de la Commission nationale d'aménagement commercial ". Par ailleurs, l'article R. 424-2 du code de l'urbanisme dispose que " par exception au b de l'article R*424-1, le défaut de notification d'une décision expresse dans le délai d'instruction vaut décision implicite de rejet dans les cas suivants : (...) h) Lorsque le projet relève de l'article L. 425-4 ou a été soumis pour avis à la commission départementale d'aménagement commercial sur le fondement de l'article L. 752-4 du code de commerce et que la commission départementale d'aménagement commercial ou, le cas échéant, de la Commission nationale d'aménagement commercial a rendu un avis défavorable ". Il résulte des termes mêmes de ces dispositions que le silence gardé par l'autorité administrative sur une demande de permis de construire valant autorisation d'exploitation commerciale fait naître une décision tacite de rejet de cette demande. Par suite, en jugeant qu'à l'expiration du délai d'instruction de la demande de permis de construire déposée par la société Le Parc du Béarn en vue de la réalisation d'un projet soumis à autorisation d'exploitation commerciale, cette société s'était trouvée titulaire d'une autorisation tacite, la présidente de la 1ère chambre de la cour administrative d'appel de Bordeaux a entaché son ordonnance d'erreur de droit.<br/>
<br/>
              3. Il résulte de ce qui a été dit ci-dessus que, sans qu'il soit besoin d'examiner l'autre moyen de sa requête, la société le Parc du Béarn est fondée à demander l'annulation de l'ordonnance attaquée du 29 mars 2017. <br/>
<br/>
              4. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la société Le Parc du Béarn au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance de la présidente de la 1ère chambre de la cour administrative d'appel de Bordeaux du 29 mars 2017 est annulée.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux. <br/>
Article 3 : Les conclusions présentées par la société Le Parc du Béarn sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la société Le Parc du Béarn et au maire de Lons. <br/>
Copie en sera adressée au ministre de l'économie et des finances. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
