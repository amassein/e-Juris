<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029800147</ID>
<ANCIEN_ID>JG_L_2014_11_000000380568</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/80/01/CETATEXT000029800147.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème SSJS, 13/11/2014, 380568, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-11-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380568</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Pauline Pannier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:380568.20141113</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 23 mai 2014 au secrétariat du contentieux du Conseil d'Etat, présenté par M. B...A..., demeurant ... ; M. A...demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret n° 2014-160 du 17 février 2014 portant délimitation des cantons dans le département du Calvados ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code électoral ;<br/>
<br/>
              Vu le code général des collectivités territoriales ; <br/>
<br/>
              Vu la loi n° 2013-403 du 17 mai 2013 ;<br/>
<br/>
              Vu le décret n° 2013-938 du 18 octobre 2013, modifié par le décret n° 2014-112 du 6 février 2014 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Pannier, auditeur,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 191-1 du code électoral, résultant de la loi du 17 mai 2013 relative à l'élection des conseillers départementaux, des conseillers municipaux et des conseillers communautaires et modifiant le calendrier électoral : " Le nombre de cantons dans lesquels sont élus les conseillers départementaux est égal, pour chaque département, à la moitié du nombre de cantons existant au 1er janvier 2013, arrondi à l'unité impaire supérieure si ce nombre n'est pas entier impair. / Le nombre de cantons dans chaque département comptant plus de 500 000 habitants ne peut être inférieur à dix-sept. Il ne peut être inférieur à treize dans chaque département comptant entre 150 000 et 500 000 habitants " ; qu'aux termes de l'article L. 3113-2 du code général des collectivités territoriales, dans sa rédaction issue de la même loi, applicable à la date du décret attaqué : " I. - Les modifications des limites territoriales des cantons, les créations et suppressions de cantons et le transfert du siège de leur chef-lieu sont décidés par décret en Conseil d'Etat après consultation du conseil général qui se prononce dans un délai de six semaines à compter de sa saisine. A l'expiration de ce délai, son avis est réputé rendu. (...) / III. - La modification des limites territoriales des cantons effectuée en application du I est conforme aux règles suivantes : / a) Le territoire de chaque canton est défini sur des bases essentiellement démographiques ; / b) Le territoire de chaque canton est continu ; / c) Est entièrement comprise dans le même canton toute commune de moins de 3 500 habitants ; / IV. - Il n'est apporté aux règles énoncées au III que des exceptions de portée limitée, spécialement justifiées, au cas par cas, par des considérations géographiques (...) ou par d'autres impératifs d'intérêt général " ;<br/>
<br/>
              2. Considérant que le décret attaqué a, sur le fondement de l'article L. 3113-2 du code général des collectivités territoriales, procédé à une nouvelle délimitation des cantons du département du Calvados, compte tenu de l'exigence de réduction du nombre des cantons de ce département résultant de l'article L. 191-1 du code électoral ;<br/>
<br/>
              Sur la légalité externe du décret attaqué : <br/>
<br/>
              3. Considérant qu'il résulte des termes mêmes des dispositions législatives précitées qu'il appartenait au pouvoir réglementaire de procéder, par décret en Conseil d'Etat, à une nouvelle délimitation territoriale de l'ensemble des cantons ; <br/>
<br/>
              4. Considérant que les dispositions de l'article L. 3113-2 du code général des collectivités territoriales se bornent à prévoir la consultation du conseil général du département concerné à l'occasion de l'opération de création et suppression de cantons ; qu'aucune disposition législative ou réglementaire n'imposait de procéder, préalablement à l'intervention du décret attaqué, à une consultation des maires et des présidents des établissements publics de coopération intercommunale du département faisant l'objet d'un remodelage des limites cantonales ;<br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              5. Considérant qu'il résulte des dispositions de l'article L. 3113-2 du code général des collectivités territoriales que le territoire de chaque canton doit être établi sur des bases essentiellement démographiques, qu'il doit être continu et que toute commune de moins de 3 500 habitants doit être entièrement comprise dans un même canton, seules des exceptions de portée limitée et spécialement justifiées pouvant être apportées à ces règles ; que ni ces dispositions, ni aucun autre texte non plus qu'aucun principe n'imposent au pouvoir réglementaire de prévoir que les limites des cantons, qui sont des circonscriptions électorales, coïncident avec les limites des cartes des établissements publics de coopération intercommunale, des schémas de cohérence territoriale ou des " bassins de vie " définis par l'Institut national de la statistique et des études économiques ; <br/>
<br/>
              6. Considérant que le requérant critique les choix opérés par le décret et invoque la distribution illogique des communes de l'ancien canton de Cambremer et des communes membres de la communauté de communes " lntercom ", ainsi que l'incohérence du rattachement des communes du canton de Saint-Pierre-sur-Dives, de l'ancien canton d'Orbec et de l'ancien canton de Trouville ; que, toutefois, et alors qu'il n'est pas contesté que ce redécoupage respecte les critères définis par le III de l'article L. 3113-2 du code général des collectivités territoriales, le requérant n'apporte aucun élément de nature à établir que les choix auxquels il a ainsi été procédé reposeraient sur une erreur manifeste d'appréciation ;<br/>
<br/>
              7. Considérant que le détournement de pouvoir allégué n'est pas établi ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que M. A...n'est pas fondé à demander l'annulation du décret attaqué ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er: La requête de M. A...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au ministre de l'intérieur.<br/>
Copie en sera adressée pour information au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
