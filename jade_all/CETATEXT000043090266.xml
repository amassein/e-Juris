<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043090266</ID>
<ANCIEN_ID>JG_L_2021_02_000000429790</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/09/02/CETATEXT000043090266.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 01/02/2021, 429790</TITRE>
<DATE_DEC>2021-02-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429790</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET ; BALAT</AVOCATS>
<RAPPORTEUR>Mme Fanélie Ducloz</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:429790.20210201</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C... B..., Mme K... B..., M. D... E..., Mme G... E..., M. A... H... et Mme J... H... ont demandé au tribunal administratif de Toulon d'annuler pour excès de pouvoir le permis de construire tacite délivré le 21 septembre 2016 par le préfet du Var à la société Le Castellet-Faremberts en vue de la réalisation de cent-vingt logements sur un terrain situé Hameau du Brûlat au Castellet, et le permis de construire modificatif délivré le 23 janvier 2018 à cette même société, ainsi que la décision implicite de rejet de leur recours gracieux. Par un jugement n° 170864-182274 du 14 février 2019, le tribunal administratif a fait droit à leurs demandes.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 15 avril et 12 juillet 2019 et le 8 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, la société Le Castellet-Faremberts demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de M. B... et autres la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - le code de l'urbanisme ;<br/>
              - la loi n° 2018-1021 du 23 novembre 2018 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme I... F..., maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de la société Le Castellet-Faremberts et à Me Balat, avocat de M. et Mme E... et autres ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le préfet du Var a accordé à la société Le Castellet-Faremberts un permis de construire tacite en date du 21 septembre 2016 pour la réalisation de 120 logements sociaux, pour une surface de plancher totale de 8 849,76 m2, sur les parcelles cadastrées section AC n° 25, A n° 1164 à 1771 et 1760, d'une superficie de près de 3,3 hectares, situées au lieu-dit Hameau du Brûlat, au Castellet. Un permis modificatif a été délivré à cette société le 23 janvier 2018. M. B... et autres ont demandé au tribunal administratif de Toulon d'annuler le permis de construire et le permis de construire modificatif, ainsi que la décision implicite de rejet de leur recours gracieux. Par un jugement du 14 février 2019 contre lequel la société Le Castellet-Faremberts se pourvoit en cassation, le tribunal administratif a fait droit à leur demande.<br/>
<br/>
              2. En premier lieu, d'une part, aux termes du II de l'article L. 122-1 du code de l'environnement, dans sa rédaction alors applicable : " Les projets qui, par leur nature, leur dimension ou leur localisation, sont susceptibles d'avoir des incidences notables sur l'environnement ou la santé humaine font l'objet d'une évaluation environnementale en fonction de critères et de seuils définis par voie réglementaire et, pour certains d'entre eux, après un examen au cas par cas effectué par l'autorité environnementale (...) ", le projet au sens de ces disposition étant défini par le 1° du I du même article comme " la réalisation de travaux de construction, d'installations ou d'ouvrages, ou d'autres interventions dans le milieu naturel ou le paysage, y compris celles destinées à l'exploitation des ressources du sol ". Aux termes du III du même article : " (...) Lorsqu'un projet est constitué de plusieurs travaux, installations, ouvrages ou autres interventions dans le milieu naturel ou le paysage, il doit être appréhendé dans son ensemble, y compris en cas de fractionnement dans le temps et dans l'espace et en cas de multiplicité de maîtres d'ouvrage, afin que ses incidences sur l'environnement soient évaluées dans leur globalité ". L'article R. 122-2 du même code, dans sa rédaction alors applicable, dispose : " I. - Les projets relevant d'une ou plusieurs rubriques énumérées dans le tableau annexé au présent article font l'objet d'une évaluation environnementale, de façon systématique ou après un examen au cas par cas, en application du II de l'article L. 122-1, en fonction des critères et des seuils précisés dans ce tableau. (...) ". Ce tableau, dans sa rédaction alors applicable, soumet, s'agissant des travaux, ouvrages, aménagements ruraux et urbains, à la procédure de l'examen au cas par cas les " Travaux, constructions et opérations d'aménagement constitués ou en création qui soit créent une surface de plancher supérieure ou égale à 10 000 m2 et inférieure à 40 000 m2 et dont le terrain d'assiette ne couvre pas une superficie supérieure ou égale à 10 hectares, soit couvre un terrain d'assiette d'une superficie supérieure ou égale à 5 ha et inférieure à 10 ha et dont la surface de plancher créée est inférieure à 40 000 m2. "<br/>
<br/>
              3. D'autre part, aux termes de l'article R. 431-16 du code de l'urbanisme : " Le dossier joint à la demande de permis de construire comprend en outre, selon les cas : a) L'étude d'impact ou la décision de l'autorité environnementale dispensant le projet d'évaluation environnementale lorsque le projet relève du tableau annexé à l'article R. 122-2 du code de l'environnement. L'autorité compétente pour délivrer l'autorisation d'urbanisme vérifie que le projet qui lui est soumis est conforme aux mesures et caractéristiques qui ont justifié la décision de l'autorité environnementale de ne pas le soumettre à évaluation environnementale (...) ".<br/>
<br/>
              4. Pour juger que le projet faisant l'objet du permis de construire en litige aurait dû être soumis à un examen au cas par cas afin de déterminer s'il devait donner lieu à une étude d'impact, le tribunal a estimé que le projet à prendre en compte pour l'application du 1° du I de l'article L. 122-1 du code de l'environnement n'était pas le seul projet de la société Le Castellet-Faremberts faisant l'objet du permis de construire attaqué, mais qu'il fallait y incorporer celui identifié sur la parcelle adjacente cadastrée A 1759 au motif qu'ils formaient un projet global commun. Toutefois, en se fondant sur la perspective que cet autre projet avait la même finalité de construction de logements sociaux, sur la présence dans les plans annexés au dossier de la demande du permis de construire attaqué de deux passages menant à la parcelle A 1759, et sur la circonstance que ces projets, dont le second n'était, au demeurant, qu'hypothétique, s'inscrivaient dans le projet d'urbanisation de la zone tel qu'il ressort du plan local d'urbanisme, sans rechercher s'il existait entre eux des liens de nature à caractériser le fractionnement d'un projet unique, le tribunal a commis une erreur de droit.<br/>
<br/>
              5. En second lieu, aux termes de l'article 1 AU 3 du règlement du plan local d'urbanisme de la commune du Castellet : " (...) Les unités foncières doivent être desservies par des voies publiques ou privées répondant à l'importance et à la destination de la construction ou de l'ensemble des constructions qui y sont édifiées. Dans tous les cas, toute voie publique ou privée, desservant des unités foncières destinées à recevoir des constructions ne peut avoir une largeur de chaussée inférieure à 6 mètres. (...) ". La conformité d'un immeuble à de telles prescriptions d'un plan local d'urbanisme s'apprécie non par rapport à l'état initial de la voie mais en tenant compte des prévisions inscrites dans le plan local d'urbanisme à l'égard de celle-ci et des circonstances de droit et de fait déterminantes pour leur réalisation qui doit être certaine dans son principe comme dans son échéance de réalisation. <br/>
<br/>
              6. S'il ressort des pièces du dossier soumis aux juges du fond que le permis modificatif délivré à la société Le Castellet-Faremberts précise qu'" il est prévu au sud un élargissement et le futur raccordement avec le chemin des Faremberts étudié avec la DDTM " et que " l'accès au terrain sera conforme avec le projet étudié avec la DDTM (...) ", le tribunal a estimé, par une appréciation souveraine exempte de dénaturation, qu'il n'était pas établi que la réalisation de l'élargissement de la voie d'accès au terrain d'assiette, en l'état inférieure aux six mètres requis par l'article 1 AU 3 du règlement du plan local d'urbanisme, aurait fait l'objet d'une programmation par les collectivités publiques compétentes, aucune précision n'étant apportée quant à son calendrier de réalisation et à ses modalités de mise en oeuvre. <br/>
<br/>
              7. Aux termes de l'article L. 600-5-1 du code de l'urbanisme, dans sa rédaction issue de la loi du 23 novembre 2018 portant évolution du logement, de l'aménagement et du numérique, applicable au litige : " Sans préjudice de la mise en oeuvre de l'article L. 600-5, le juge administratif qui, saisi de conclusions dirigées contre un permis de construire, de démolir ou d'aménager ou contre une décision de non-opposition à déclaration préalable estime, après avoir constaté que les autres moyens ne sont pas fondés, qu'un vice entraînant l'illégalité de cet acte est susceptible d'être régularisé, sursoit à statuer, après avoir invité les parties à présenter leurs observations, jusqu'à l'expiration du délai qu'il fixe pour cette régularisation, même après l'achèvement des travaux. Si une mesure de régularisation est notifiée dans ce délai au juge, celui-ci statue après avoir invité les parties à présenter leurs observations. Le refus par le juge de faire droit à une demande de sursis à statuer est motivé. ". <br/>
<br/>
              8. Il résulte de ces dispositions que lorsque le ou les vices affectant la légalité de l'autorisation d'urbanisme dont l'annulation est demandée, sont susceptibles d'être régularisés, le juge doit surseoir à statuer sur les conclusions dont il est saisi contre cette autorisation. Il invite au préalable les parties à présenter leurs observations sur la possibilité de régulariser le ou les vices affectant la légalité de l'autorisation d'urbanisme. Le juge n'est toutefois pas tenu de surseoir à statuer, d'une part, si les conditions de l'article L. 600-5 du code de l'urbanisme sont réunies et qu'il fait le choix d'y recourir, d'autre part, si le bénéficiaire de l'autorisation lui a indiqué qu'il ne souhaitait pas bénéficier d'une mesure de régularisation. Un vice entachant le bien-fondé de l'autorisation d'urbanisme est susceptible d'être régularisé, même si cette régularisation implique de revoir l'économie générale du projet en cause, dès lors que les règles d'urbanisme en vigueur à la date à laquelle le juge statue permettent une mesure de régularisation qui n'implique pas d'apporter à ce projet un bouleversement tel qu'il en changerait la nature même.<br/>
<br/>
              9. Le motif tiré de la méconnaissance de l'article 1 AU 3 du règlement du plan local d'urbanisme, qui ne met en cause que le caractère certain de la date d'échéance des travaux, par le département du Var, d'élargissement de la voie d'accès à la construction projetée, apparaît susceptible de faire l'objet d'une mesure de régularisation en application de l'article L. 600-5-1 du code de l'urbanisme rappelé au point 7 et n'est, par suite, pas de nature à justifier à lui seul la décision d'annulation du tribunal administratif de Toulon. Dès lors, il y a lieu de faire droit aux conclusions à fin d'annulation du jugement attaqué, présentées par la société Le Castellet-Faremberts.<br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la société Le Castellet-Farembert, qui n'est pas la partie perdante dans la présente instance, la somme que demandent M. B... et autres au titre des frais exposés par eux et non compris dans les dépens. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B... et autres une somme globale de 3 000 euros qui sera versée à la société Le Castellet-Faremberts.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 14 février 2019 du tribunal administratif de Toulon est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Toulon.<br/>
<br/>
Article 3 : M. B..., Mme B..., M. E..., Mme E..., M. H... et Mme H... verseront à la société Le Castellet-Faremberts la somme globale de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4: La présente décision sera notifiée à la société Le Castellet-Faremberts et à M. C... B... premier requérant dénommé.<br/>
Copie en sera adressée à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales et à la commune du Castellet.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">44-006-03-01-01 NATURE ET ENVIRONNEMENT. - ETUDE D'IMPACT JOINTE À LA DEMANDE DE PERMIS DE CONSTRUIRE (ART. R. 431-16 DU CODE DE L'URBANISME) - DÉTERMINATION DE LA SOUMISSION DU PROJET À CETTE OBLIGATION - PRISE EN COMPTE DES PROJETS ADJACENTS - FRACTIONNEMENT D'UN PROJET UNIQUE (III DE L'ART. L. 122-1 DU CODE DE L'ENVIRONNEMENT) - EXISTENCE - PROJETS DISTINCTS PARTICIPANT DE L'URBANISATION D'UNE MÊME ZONE - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-03-02-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. PROCÉDURE D'ATTRIBUTION. DEMANDE DE PERMIS. - SOUMISSION DU PROJET À L'OBLIGATION DE FOURNIR UNE ÉTUDE D'IMPACT (ART. R. 431-16 DU CODE DE L'URBANISME) - PRISE EN COMPTE DES PROJETS ADJACENTS - FRACTIONNEMENT D'UN PROJET UNIQUE (III DE L'ART. L. 122-1 DU CODE DE L'ENVIRONNEMENT) - EXISTENCE - PROJETS DISTINCTS PARTICIPANT DE L'URBANISATION D'UNE MÊME ZONE - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 44-006-03-01-01 Article R. 431-16 du code de l'urbanisme prévoyant que le dossier joint à la demande de permis de construire comprend l'étude d'impact ou la décision de l'autorité environnementale dispensant le projet d'évaluation environnementale lorsque le projet relève du tableau annexé à l'article R. 122-2 du code de l'environnement.,,,Le projet de construction existant sur une parcelle adjacente au terrain d'assiette du projet pour lequel le permis de construire est sollicité ne peut être pris en compte, pour déterminer s'il y a lieu, en application de ces dispositions, de joindre une étude d'impact au dossier de demande, que s'il existe entre eux des liens de nature à caractériser le fractionnement d'un projet unique et non au seul motif qu'ils s'inscrivent dans le projet d'urbanisation de la zone tel qu'il ressort du plan local d'urbanisme.</ANA>
<ANA ID="9B"> 68-03-02-01 Article R. 431-16 du code de l'urbanisme prévoyant que le dossier joint à la demande de permis de construire comprend l'étude d'impact ou la décision de l'autorité environnementale dispensant le projet d'évaluation environnementale lorsque le projet relève du tableau annexé à l'article R. 122-2 du code de l'environnement.,,,Le projet de construction existant sur une parcelle adjacente au terrain d'assiette du projet pour lequel le permis de construire est sollicité ne peut être pris en compte, pour déterminer s'il y a lieu, en application de ces dispositions, de joindre une étude d'impact au dossier de demande, que s'il existe entre eux des liens de nature à caractériser le fractionnement d'un projet unique et non au seul motif qu'ils s'inscrivent dans le projet d'urbanisation de la zone tel qu'il ressort du plan local d'urbanisme.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant du permis d'aménager un lotissement, CE, 28 novembre 2018, Commune de la Turballe et Société Loti Ouest Atlantique, n°s 419315 419323, T. pp. 787-954-955.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
