<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042885755</ID>
<ANCIEN_ID>J6_L_2020_12_000002000882</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/88/57/CETATEXT000042885755.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de MARSEILLE, 6ème chambre, 21/12/2020, 20MA00882 - 20MA01032, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-21</DATE_DEC>
<JURIDICTION>CAA de MARSEILLE</JURIDICTION>
<NUMERO>20MA00882 - 20MA01032</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. FEDOU</PRESIDENT>
<AVOCATS>VIALE</AVOCATS>
<RAPPORTEUR>Mme Christine  MASSE-DEGOIS</RAPPORTEUR>
<COMMISSAIRE_GVT>M. THIELÉ</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
       Procédure contentieuse antérieure :<br/>
<br/>
       Mme C... E... épouse A... a demandé au tribunal administratif de Marseille d'annuler l'arrêté, en date du 26 septembre 2019, par lequel le préfet des Bouches-du-Rhône a refusé de lui délivrer un titre de séjour, lui a assigné l'obligation de quitter le territoire français dans le délai de trente jours et a désigné le pays à destination duquel, passé ce délai, elle pourrait être renvoyée d'office. <br/>
<br/>
       Par un jugement n° 1908935 du 9 janvier 2020, le tribunal administratif de Marseille a rejeté cette demande.<br/>
<br/>
       Procédure devant la Cour :<br/>
<br/>
       I. - Par une requête enregistrée le 19 février 2020 sous le n° 20MA00882, Mme E... épouse A..., représentée par Me B..., demande à la Cour :<br/>
<br/>
       1°) d'annuler ce jugement du tribunal administratif de Marseille ;<br/>
<br/>
       2°) d'annuler l'arrêté du préfet des Bouches-du-Rhône du 26 septembre 2019 ;<br/>
<br/>
       3°) de faire injonction au préfet des Bouches-du-Rhône de réexaminer sa situation dans un délai de quinze jours suivant la notification de l'arrêt à venir ;<br/>
<br/>
       4°) de l'admettre au bénéfice de l'aide juridictionnelle provisoire ;<br/>
<br/>
       5°) de condamner l'Etat à verser à son conseil la somme de 1 500 euros en application des dispositions combinées de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
       Elle soutient que :<br/>
       - l'arrêté litigieux a été pris en violation de l'article 3-1 de la convention internationale relatives aux droits de l'enfant ;<br/>
       - il méconnait l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
       - ayant été précédemment bénéficiaire d'un titre de séjour pour raisons de santé, le refus de séjour contesté viole le principe d'égalité vis-à-vis d'elle-même, le suivi psychologique imposé par son état de santé n'étant par ailleurs pas disponible au Maroc.<br/>
<br/>
<br/>
       Par un mémoire en défense enregistré le 29 juillet 2020, le préfet des Bouches-du-Rhône conclut au rejet de la requête. <br/>
<br/>
       Il soutient que les moyens soulevés par Mme E... épouse A... ne sont pas fondés. <br/>
<br/>
<br/>
       Par ordonnance du 16 septembre 2020, la clôture de l'instruction a été fixée au 13 octobre 2020. <br/>
<br/>
<br/>
       Par une décision du 4 septembre 2020, la demande de Mme E... épouse A... tendant au bénéfice de l'aide juridictionnelle a été rejetée.<br/>
<br/>
<br/>
       II. - Par une requête enregistrée le 2 mars 2020 sous le n° 20MA01032, Mme E... épouse A..., représentée par Me B..., demande à la Cour :<br/>
<br/>
       1°) d'ordonner qu'il soit sursis à l'exécution du jugement du tribunal administratif de Marseille n° 1908935 du 9 janvier 2020, en application de l'article R. 811-17 du code de justice administrative ;<br/>
<br/>
       2°) de faire injonction au préfet des Bouches-du-Rhône de lui délivrer une autorisation provisoire de séjour dans la semaine suivant la notification de l'arrêt à venir et de réexaminer sa situation ; <br/>
<br/>
       3°) de condamner l'Etat à verser à son conseil la somme de 500 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
       Elle soutient que :<br/>
       - l'exécution du jugement attaqué risque de l'exposer à des conséquences difficilement réparables ;<br/>
       - les moyens invoqués dans sa requête au fond sont sérieux.<br/>
<br/>
<br/>
       Par un mémoire en défense enregistré le 29 juillet 2020, le préfet des Bouches-du-Rhône conclut au rejet de la requête. <br/>
<br/>
       Il soutient que les moyens soulevés par Mme E... épouse A... ne sont pas fondés. <br/>
<br/>
<br/>
       Par ordonnance du 16 septembre 2020, la clôture de l'instruction a été fixée au 13 octobre 2020. <br/>
<br/>
<br/>
       Par une décision du 4 septembre 2020, la demande de Mme E... épouse A... tendant au bénéfice de l'aide juridictionnelle a été rejetée.<br/>
<br/>
<br/>
       Vu les autres pièces des dossiers.<br/>
<br/>
<br/>
        Vu :<br/>
       - la convention internationale relative aux droits de l'enfant ;<br/>
       - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
       - l'accord franco-marocain du 9 octobre 1987 ;<br/>
       - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
       - la loi n° 91-647 du 10 juillet 1991 ;<br/>
       - le code de justice administrative.<br/>
<br/>
       Le président de la formation de jugement a dispensé le rapporteur public, sur sa proposition, de prononcer des conclusions à l'audience.<br/>
<br/>
       Les parties ont été régulièrement averties du jour de l'audience.<br/>
<br/>
       A été entendu au cours de l'audience publique le rapport de Mme D... F..., rapporteure. <br/>
<br/>
<br/>
       Considérant ce qui suit :<br/>
<br/>
       1. Les requêtes nos 20MA00882 et 20MA01032 sont dirigées contre le même jugement et ont fait l'objet d'une instruction commune. Il y a lieu de les joindre pour y statuer par un même arrêt.<br/>
<br/>
       2. Mme E... épouse A..., née en 1985 et de nationalité marocaine, relève appel et demande le sursis à exécution du jugement, en date du 9 janvier 2020, par lequel le tribunal administratif de Marseille a rejeté son recours pour excès de pouvoir dirigé contre l'arrêté du préfet des Bouches-du-Rhône du 26 septembre 2019 lui refusant la délivrance d'un titre de séjour, lui assignant l'obligation de quitter le territoire français dans le délai de trente jours et désignant le pays à destination duquel, passé ce délai, elle pourrait être renvoyée d'office.<br/>
<br/>
<br/>
<br/>
<br/>
       Sur la requête n° 20MA00882 :<br/>
<br/>
       En ce qui concerne la demande d'aide juridictionnelle provisoire :<br/>
<br/>
<br/>
       3. La demande de Mme E... épouse A... tendant à son admission au bénéfice de l'aide juridictionnelle ayant été rejetée par une décision du bureau d'aide juridictionnelle du 4 septembre 2020, il n'y a pas lieu de statuer sur ses conclusions tendant à l'octroi de l'aide juridictionnelle à titre provisoire, qui ont perdu leur objet.<br/>
<br/>
<br/>
<br/>
       En ce qui concerne la légalité de l'arrêté contesté en tant qu'il porte refus de séjour et obligation de quitter le territoire français dans un délai de trente jours :<br/>
<br/>
<br/>
       4. En premier lieu, Mme E... épouse A... reprend devant la Cour le moyen tiré de la violation de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. Cependant, la requérante se borne à produire, en plus des justificatifs déjà versés aux débats de première instance, divers documents administratifs, tels une attestation de suivi de formation datée du 7 novembre 2019, des factures de gaz et d'électricité des années 2017 à 2019 ou encore des avis d'imposition concernant ces mêmes années faisant état d'un revenu nul ainsi que des contrats à durée déterminée et six bulletins de salaires pour de très courtes durées pour les mois d'octobre et de novembre 2019, qui ne sauraient suffire à démontrer l'insertion sociale significative alléguée depuis le 18 février 2014, date déclarée de son entrée sur le territoire national et alors même qu'elle y séjourne depuis de manière continue. De même, la circonstance que son fils, né en 2017, a récemment été admis en crèche ne permet pas d'établir sa " pleine intégration " en France ainsi que celle de sa famille. Par suite, Mme E... épouse A..., ainsi que l'a jugé le tribunal, n'est pas fondée à soutenir que la décision portant refus de séjour et lui faisant obligation de quitter le territoire français a méconnu les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.<br/>
<br/>
<br/>
       5. En deuxième lieu, aux termes de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Sauf si sa présence constitue une menace pour l'ordre public, la carte de séjour temporaire portant la mention " vie privée et familiale " est délivrée de plein droit : / (...) / 11° A l'étranger résidant habituellement en France, si son état de santé nécessite une prise en charge médicale dont le défaut pourrait avoir pour lui des conséquences d'une exceptionnelle gravité et si, eu égard à l'offre de soins et aux caractéristiques du système de santé dans le pays dont il est originaire, il ne pourrait pas y bénéficier effectivement d'un traitement approprié. La condition prévue à l'article L. 313-2 n'est pas exigée. La décision de délivrer la carte de séjour est prise par l'autorité administrative après avis d'un collège de médecins du service médical de l'Office français de l'immigration et de l'intégration (...) ".<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
       6. Les documents dont se prévaut Mme E... épouse A... font apparaître qu'elle a présenté en 2015 des troubles anxio-dépressifs qui ont nécessité une hospitalisation en septembre 2016 ainsi que la mise en place d'un traitement combinant plusieurs médicaments, outre un suivi spécialisé. Toutefois, les documents médicaux versés aux débats ne permettent pas de remettre en cause l'avis émis le 6 février 2019 par le collège de médecins de l'Office français de l'immigration et de l'intégration, avis dont le préfet s'est approprié la teneur et selon lequel, si l'état de santé de la requérante nécessite une prise en charge médicale à défaut de laquelle elle serait exposée à des conséquences d'une exceptionnelle gravité, elle pourra effectivement bénéficier, dans le pays dont elle est originaire, de soins appropriés. A cet égard, l'unique certificat médical daté du 19 décembre 2018 exprimant une opinion contraire, en l'occurrence celui qui a été établi dans le cadre de la demande de titre de séjour aux fins de transmission à l'Office français de l'immigration et de l'intégration, est dépourvu de tout élément de justification circonstancié, en rapport avec d'éventuelles spécificités de la pathologie de la requérante ou avec les ressources sanitaires du Maroc. La requérante, de nationalité marocaine, en se prévalant de l'oeuvre cinématographique " Un divan à Tunis " et de son synopsis évoquant les difficultés rencontrées par une psychanalyste dans l'exercice de sa profession dans une banlieue de Tunis, ne démontre pas celles auxquelles elle serait personnellement confrontée pour bénéficier d'un suivi thérapeutique en cas de retour dans son pays d'origine. La circonstance, par ailleurs, que Mme E... épouse A... avait précédemment bénéficié d'un titre de séjour pour raisons de santé ne saurait par elle-même caractériser une erreur d'appréciation au regard des dispositions citées au point précédent, ni moins encore, en tout état de cause, une " rupture d'égalité vis-à-vis de la requérante elle-même ", ce d'autant que le certificat médical du 19 décembre 2018 mentionne que son état de santé s'est depuis lors amélioré. Le refus de titre de séjour contesté ne peut, dans ces conditions, être regardé comme procédant d'une inexacte application de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, d'une erreur d'appréciation ou d'une violation du principe d'égalité.<br/>
<br/>
<br/>
<br/>
       7. En troisième lieu, aux termes de l'article 3-1 de la convention internationale des droits de l'enfant : " Dans toutes les décisions qui concernent les enfants, qu'elles soient le fait d'institutions publiques ou privées de protection sociale, des tribunaux, des autorités administratives ou des organes législatifs, l'intérêt supérieur de l'enfant doit être une considération primordiale. ". Il résulte de ces stipulations que, dans l'exercice de son pouvoir d'appréciation, l'autorité administrative doit accorder une attention primordiale à l'intérêt supérieur des enfants dans toutes les décisions les concernant.<br/>
<br/>
<br/>
<br/>
       8. Mme E... épouse A... soutient que l'arrêté contesté en tant qu'il porte refus de séjour et obligation de quitter le territoire français dans un délai de trente jours méconnait les stipulations citées au point 7. Toutefois, ces décisions n'impliquent pas, par elles-mêmes, l'éclatement de la cellule familiale, le père de l'enfant, au vu des pièces du dossier, faisant l'objet d'une mesure de même nature. Elles n'ont, dès lors, pas méconnu les stipulations de l'article 3-1 de la convention internationale des droits de l'enfant du 20 novembre 1989. <br/>
<br/>
<br/>
<br/>
       En ce qui concerne la légalité de l'arrêté contesté en tant qu'il fixe le pays de destination : <br/>
<br/>
       9. Mme E... épouse A... soutient, et il ressort du dossier de première instance, que la décision fixant le pays de destination de son époux indique qu'il sera éloigné vers la Turquie, " pays dont il a la nationalité ou qui lui a délivré un titre de voyage en cours de validité ou encore à destination de tout autre pays dans lequel il établit être légalement admissible " alors qu'elle-même sera éloignée vers le Maroc, " pays dont elle a la nationalité ou qui lui a délivré un titre de voyage en cours de validité ou encore à destination de tout autre pays dans lequel elle établit être légalement admissible ". Chacun de ces deux arrêtés, faute de limiter l'éloignement de l'étranger vers les pays où son conjoint ainsi que son enfant sont légalement admissibles, permet de renvoyer les époux dans un pays différent, ce qui aurait nécessairement pour effet de séparer, même provisoirement, l'enfant de l'un de ses parents, ainsi que le fait valoir Mme E... épouse A..., ce que le préfet ne conteste pas. La mise à exécution, dans ces conditions, des décisions fixant le pays de destination de chacun des membres du couple méconnaît les stipulations de l'article 3-1 de la convention internationale des droits de l'enfant, rappelées au point 7, qui font obstacle à l'exécution simultanée de mesures d'éloignement de chacun des parents vers deux pays différents. Il en résulte que la décision litigieuse doit être annulée en tant qu'elle emporte éloignement de Mme E... épouse A... à destination d'un pays différent de celui vers lequel son époux est éloigné.<br/>
<br/>
       10. Il résulte de tout ce qui précède que Mme E... épouse A... est seulement fondée à soutenir que c'est à tort, que par le jugement attaqué, le tribunal administratif de Marseille a rejeté sa demande en tant qu'elle portait sur la décision fixant le pays de destination duquel elle serait éloignée. <br/>
<br/>
       En ce qui concerne les conclusions aux fins d'injonction :<br/>
<br/>
       11. Considérant qu'aux termes de l'article L. 911-1 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution ". <br/>
<br/>
       12. L'annulation de la décision en date 26 septembre 2019 en tant qu'elle prévoit la possibilité de l'éloigner à destination d'un pays différent du pays de destination de son époux n'implique aucune mesure d'exécution. Par suite, les conclusions aux fins d'injonction doivent être rejetées.<br/>
<br/>
       Sur la requête n° 20MA01032 :<br/>
<br/>
       13. Le présent arrêt statuant au fond, les conclusions aux fins de sursis à exécution du jugement du 9 janvier 2020, ainsi que celles tendant à ce qu'il soit fait injonction à l'autorité préfectorale de délivrer à la requérante une autorisation provisoire de séjour, sont devenues sans objet. Il n'y a donc pas lieu d'y statuer.<br/>
<br/>
        Sur les frais liés au litige :<br/>
<br/>
       14. Mme E... épouse A... n'a pas obtenu le bénéfice de l'aide juridictionnelle. Par suite, son conseil, Me B..., ne peut se prévaloir, dans chacune des deux instances, des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 relative à l'aide juridique.<br/>
<br/>
<br/>
D É C I D E  :<br/>
<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions formées par Mme E... épouse A... tendant à ce qu'elle soit admise à l'aide juridictionnelle à titre provisoire présentée dans le cadre de l'instance n° 20MA00882.<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions de la requête n° 20MA01032 tendant à ce qu'il soit sursis à l'exécution du jugement du tribunal administratif de Marseille du 9 janvier 2020 et à ce que soit adressée une injonction au préfet des Bouches-du-Rhône.<br/>
<br/>
Article 3 : L'arrêté du préfet des Bouches-du-Rhône en date du 26 septembre 2019 est annulé en tant qu'il rend possible l'éloignement de Mme E... épouse A... à destination d'un pays différent du pays de renvoi de son époux. <br/>
Article 4 : Le jugement du 9 janvier 2020 du tribunal administratif de Marseille est réformé en ce qu'il a de contraire au présent arrêt.<br/>
Article 5 : Le surplus des conclusions de la requête n° 20MA00882 est rejeté.<br/>
Article 6 : Les conclusions présentées par le conseil de Mme E... épouse A... dans les instances n° 20MA00882 et n° 20MA01032 au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 7 : Le présent arrêt sera notifié à Mme C... E... épouse A..., à Me B... et au ministre de l'intérieur.<br/>
Copie en sera adressée au préfet des Bouches-du-Rhône et au procureur de la République près le tribunal judiciaire de Marseille<br/>
       Délibéré après l'audience du 7 décembre 2020, où siégeaient :<br/>
<br/>
       - M. Guy Fédou, président,<br/>
       - Mme D... F..., présidente assesseure,<br/>
       - M. François Point, premier conseiller.<br/>
<br/>
<br/>
       Rendu public par mise à disposition au greffe, le 21 décembre 2020.<br/>
2<br/>
Nos 20MA00882-20MA01032<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-01 Étrangers. Séjour des étrangers.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
