<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028215063</ID>
<ANCIEN_ID>JG_L_2013_11_000000358046</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/21/50/CETATEXT000028215063.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 18/11/2013, 358046, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-11-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>358046</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Dominique Versini-Monod</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:358046.20131118</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>VU LA PROCEDURE SUIVANTE :<br/>
<br/>
              Par une requête, enregistrée le 28 mars 2012 au secrétariat du contentieux du Conseil d'Etat, le syndicat SUD Travail Affaires sociales demande au Conseil d'Etat d'annuler la note de service du ministre du travail, de l'emploi et de la santé DAGEMO/RH3 n° 2012-06 du 1er février 2012 relative à la mise en oeuvre du compte mobilité géographique pour le corps de l'inspection du travail.<br/>
<br/>
              Par un mémoire en défense, enregistré le 23 octobre 2012, le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social conclut au rejet de la requête.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Ont été entendus en séance publique :<br/>
<br/>
              - le rapport de Mme Dominique Versini-Monod, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>CONSIDERANT CE QUI SUIT :<br/>
<br/>
              1. Par la note de service attaquée, le ministre du travail, de l'emploi et de la santé rappelle que, sous réserve du passage du siège de la direction régionale des entreprises, de la concurrence, de la consommation, du travail et de l'emploi à l'unité territoriale du département chef-lieu de région, la mobilité géographique est considérée comme obligatoire pour les changements de grade dans le corps de l'inspection du travail. Puis il indique que, désormais, dans le cadre du " compte mobilité géographique ", une promotion au grade de directeur adjoint du travail pourra intervenir sans exiger de mobilité géographique si l'agent a précédemment effectué deux mobilités, dont l'une dans le grade d'inspecteur du travail, et qu'une promotion au grade de directeur du travail pourra intervenir sans mobilité géographique lorsqu'un agent aura précédemment effectué une mobilité géographique dans le grade de directeur adjoint du travail.  <br/>
<br/>
              2. L'interprétation que par voie, notamment, de circulaires ou d'instructions l'autorité administrative donne des lois et règlements qu'elle a pour mission de mettre en oeuvre n'est pas susceptible d'être déférée au juge de l'excès de pouvoir lorsque, étant dénuée de caractère impératif, elle ne saurait, quel qu'en soit le bien-fondé, faire grief. En revanche, les dispositions impératives à caractère général d'une circulaire ou d'une instruction doivent être regardées comme faisant grief. Tel est le cas des dispositions de la note de service attaquée, qui excluent la possibilité d'une promotion sur place pour l'agent qui ne satisfait pas à certaines conditions de mobilité antérieure.<br/>
<br/>
              3. Aux termes de l'article 58 de la loi n° 84-16 du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat : " L'avancement de grade a lieu de façon continue d'un grade au grade immédiatement supérieur. (...) / L'avancement de grade peut être subordonné à la justification d'une durée minimale de formation professionnelle au cours de la carrière. / Pour les fonctionnaires relevant des corps de catégorie A, il peut également être subordonné à l'occupation préalable de certains emplois ou à l'exercice préalable de certaines fonctions correspondant à un niveau particulièrement élevé de responsabilité. / (...) / Sauf pour les emplois laissés à la décision du Gouvernement, l'avancement de grade a lieu, selon les proportions définies par les statuts particuliers, suivant l'une ou plusieurs des modalités ci-après : / 1° Soit au choix, par voie d'inscription à un tableau annuel d'avancement, établi après avis de la commission administrative paritaire, par appréciation de la valeur professionnelle et des acquis de l'expérience professionnelle des agents ; / (...) / Les décrets portant statut particulier fixent les principes et les modalités de la sélection professionnelle, notamment les conditions de grade et d'échelon requises pour y participer. / Les promotions doivent avoir lieu dans l'ordre du tableau ou de la liste de classement. / Tout fonctionnaire bénéficiant d'un avancement de grade est tenu d'accepter l'emploi qui lui est assigné dans son nouveau grade. Sous réserve de l'application des dispositions de l'article 60, son refus peut entraîner la radiation du tableau d'avancement ou, à défaut, de la liste de classement ". Aux termes du quatrième alinéa de l'article 60 de la même loi : " Dans toute la mesure compatible avec le bon fonctionnement du service, les affectations prononcées doivent tenir compte des demandes formulées par les intéressés et de leur situation de famille (...) ". Il résulte de la combinaison de ces dispositions que l'affectation des fonctionnaires bénéficiant d'un avancement de grade est prononcée au regard de l'intérêt du service, compte tenu cependant des souhaits exprimés par les intéressés et de leur situation de famille.<br/>
<br/>
              4. Aux termes du deuxième alinéa de l'article 10 de la même loi : " Les statuts particuliers de corps interministériels ou communs à plusieurs départements ministériels (...) peuvent déroger, après avis du Conseil supérieur de la fonction publique de l'Etat, à certaines des dispositions du statut général qui ne correspondraient pas aux besoins propres à l'organisation de la gestion de ces corps au sein de chacun de ces départements ministériels ou établissements ". Tel n'est pas le cas du décret n° 2003-770 du 20 août 2003 portant statut particulier du corps de l'inspection du travail, dont l'article 14 dispose que : " L'avancement de grade a lieu exclusivement au choix, après inscription à un tableau d'avancement dans les conditions ci-après : / a) Peuvent être promus directeurs adjoints du travail les inspecteurs du travail ayant atteint le 5e échelon de leur grade et exercé effectivement les fonctions d'inspecteur pendant au moins cinq années ; / b) Peuvent être promus directeurs du travail les directeurs adjoints du travail comptant un an d'ancienneté dans le 3e échelon (...) ". En particulier, ce décret ne subordonne pas l'avancement de grade, non plus que l'affectation à certains emplois, à l'accomplissement d'une obligation de mobilité.<br/>
<br/>
              5. Il est loisible au ministre chargé du travail de rendre publics les critères qui le guident pour les nominations et les mutations des membres du corps de l'inspection du travail, dans le respect du principe d'égalité et compte tenu d'objectifs légitimes tels qu'une plus grande mobilité des personnes concernées ou une meilleure adéquation des profils aux emplois. Toutefois, en l'absence de dispositions législatives ou réglementaires expresses, l'application de ces critères ne saurait en aucun cas le conduire à fixer des règles nouvelles ou à écarter le principe selon lequel il revient aux autorités administratives de se livrer à un examen particulier des données propres à chaque dossier. En l'absence de disposition législative ou réglementaire prévoyant une telle condition, le ministre chargé du travail ne pouvait compétemment subordonner la promotion de grade d'un membre du corps de l'inspection du travail à l'accomplissement d'une mobilité au moment de cette promotion ou antérieurement à celle-ci. Par suite, le syndicat requérant est fondé à soutenir que la note de service qu'il attaque est illégale et à en demander l'annulation. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La note de service du ministre du travail, de l'emploi et de la santé du 1er février 2012 relative à la mise en oeuvre du compte mobilité géographique pour le corps de l'inspection du travail est annulée.<br/>
Article 2 : La présente décision sera notifiée au syndicat SUD Travail Affaires sociales et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
