<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032571694</ID>
<ANCIEN_ID>JG_L_2016_05_000000376672</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/57/16/CETATEXT000032571694.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 20/05/2016, 376672, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>376672</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD</AVOCATS>
<RAPPORTEUR>Mme Esther de Moustier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:376672.20160520</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>      Vu la procédure suivante :<br/>
<br/>
              	La société par actions simplifiée Faisanderie a demandé au tribunal administratif de Paris de prononcer la décharge des cotisations d'impôt sur les sociétés et de contribution additionnelle sur les revenus locatifs auxquelles elle a été assujettie au titre des exercices 2003 à 2007, des contributions additionnelles à l'impôt sur les sociétés et des cotisations minimales de taxe professionnelle auxquelles elle a été assujettie au titre des exercices 2004 et 2005, des contributions sociales auxquelles elle a été assujettie au titre de l'année 2005 ainsi que des pénalités correspondantes. <br/>
<br/>
              Par un jugement n°s 1115280, 1115283, 115285 du 30 novembre 2012, le tribunal administratif de Paris a, d'une part, fait droit partiellement aux conclusions de la société Faisanderie en prononçant la décharge des cotisations d'impôt sur les sociétés et de contribution annuelle sur les revenus locatifs au titre des exercices 2003 à 2005, des contributions additionnelles à l'impôt sur les sociétés et des contributions sociales au titre des exercices 2004 et 2005 et des cotisations minimales de taxe professionnelle au titre des années 2003 à 2005 et en ramenant la pénalité appliquée, en vertu de l'article 1728 du code général des impôts, sur les droits au titre des années 2006 et 2007, au taux de 10 %, et, d'autre part, rejeté le surplus de ses demandes.<br/>
<br/>
              Par un arrêt n°s 13PA00488, 13PA00934 du 4 février 2014, la cour administrative d'appel de Paris a rejeté le recours présenté par le ministre de l'économie et des finances tendant à l'annulation des articles 1er et 2 de ce jugement et à ce que soient remises à la charge de la société Faisanderie les impositions et pénalités dont elle avait été déchargée.  <br/>
<br/>
              Par un pourvoi et un mémoire en réplique enregistrés les 24 mars 2014 et 29 avril 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre des finances et des comptes publics demande au Conseil d'Etat d'annuler l'article 1er de cet arrêt du 4 février 2014 en tant que la cour administrative d'appel de Paris a rejeté ses conclusions ;   <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Esther de Moustier, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, avocat de la société Faisanderie ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Faisanderie, de droit luxembourgeois, a été créée le 24 juillet 2003 avec pour objet la réalisation d'opérations immobilières et a été immatriculée au registre du commerce du Luxembourg, lieu de son siège social ; qu'elle a acquis en novembre 2003 un immeuble situé à Paris qu'elle a revendu par lots ; qu'à l'issue d'une procédure de visite et de saisie, diligentée dans des locaux situés à Paris sur le fondement de l'article L. 16 B du livre des procédures fiscales, l'administration fiscale a estimé que la société y disposait d'un établissement stable ; que, dans le cadre de la vérification de comptabilité dont cette société a fait l'objet, l'administration a regardé l'activité de son établissement stable en France comme occulte ; qu'elle a exercé son droit de reprise dans le délai prévu au deuxième alinéa de l'article L. 169 du livre des procédures fiscales et a appliqué au montant des impositions  mises à la charge de la société la pénalité prévue au c. du 1 de l'article 1728 du code général des impôts ; que le ministre des finances et des comptes publics demande l'annulation de l'article 1er de l'arrêt du 4 février 2014 de la cour administrative d'appel de Paris en tant qu'il rejette son recours tendant à l'annulation des articles 1er et 2 du jugement du 20 novembre 2012 par lesquels le tribunal administratif de Paris a prononcé la décharge, au profit de la société Faisanderie, des cotisations d'impôt sur les sociétés et de contribution additionnelle à cet impôt au titre des années 2003 à 2005 et de contributions sociales au titre des années 2004 et 2005, des contributions annuelles sur les revenus locatifs et des cotisations minimales de taxe professionnelle au titre des années 2003 à 2005 et a ramené les pénalités mises à sa charge au titre des années 2006 et 2007 au taux de 10 % prévu au a. du 1 de l'article 1728 du code général des impôts ; <br/>
<br/>
              Sur le bien-fondé de l'arrêt en tant qu'il statue sur les impositions et les pénalités au titre des années 2003, 2004 et 2005 :  <br/>
<br/>
              2. Considérant qu'en vertu du second alinéa de l'article L. 169 du livre des procédures fiscales l'administration des impôts dispose, pour l'impôt sur le revenu et l'impôt sur les sociétés, par exception au délai prévu par le premier alinéa, d'un délai plus long pour exercer son droit de reprise lorsque le contribuable n'a pas déposé dans le délai légal les déclarations qu'il était tenu de souscrire et n'a pas fait connaître son activité à un centre de formalités des entreprises ou au greffe du tribunal de commerce ;<br/>
<br/>
              3. Considérant que l'instruction publiée au bulletin officiel des impôts sous le n° 13 L-4-97 du 30 octobre 1997, reprise par la documentation administrative de base sous la référence 13 L-1218 dans sa version du 1er juillet 2002, indique que " lorsque l'une quelconque des déclarations incombant au contribuable a été souscrite dans les délais, et alors même que la déclaration au centre de formalités des entreprises ou au greffe du tribunal de commerce n'a pas été effectuée, le délai spécial n'est pas applicable " et que " compte tenu de l'intention du législateur qui est de n'opposer le délai spécial qu'aux activités réellement clandestines, il est également précisé que ce délai ne peut s'appliquer, s'agissant d'une activité déterminée, à un impôt donné pour lequel le contribuable est défaillant lorsque celui-ci a souscrit, dans les délais, des déclarations au titre d'autres impôts concernant cette même activité " ; que cette instruction précise aussi que le délai spécial n'est pas applicable au cas où " le contribuable a souscrit les déclarations de TVA afférentes à une activité professionnelle mais non celle se rapportant aux revenus catégoriels (IR) ou aux résultats (IS) correspondants " ; <br/>
<br/>
              4. Considérant que la cour a relevé que la société s'était identifiée auprès du service des impôts des entreprises de la direction des résidents à l'étranger et des services généraux pour les ventes immobilières effectuées sur le territoire français et y avait déposé les déclarations de taxe sur la valeur ajoutée correspondantes en tant que société non-résidente, fiscalement domiciliée au  Luxembourg; qu'en en déduisant, après avoir relevé que la société ne s'était pas fait connaître d'un centre de formalités des entreprises et n'avait pas déposé les déclarations qu'elle était tenue de souscrire du fait de son activité imposable en France par l'intermédiaire de son établissement stable, que conformément à l'instruction précitée, dont se prévalait la société sur le fondement de l'article L. 80 A du livre des procédures fiscale, le délai spécial de reprise prévu à l'article L. 169 du livre des procédures fiscales n'était pas applicable, de sorte que les impositions dues au titre des années 2003, 2004 et 2005 devaient être regardées comme prescrites, la cour, dont l'arrêt est suffisamment motivé sur ce point, n'a pas commis d'erreur de droit ;<br/>
<br/>
              Sur le bien-fondé de l'arrêt en tant qu'il statue sur les pénalités au titre des années 2006 et 2007 : <br/>
<br/>
              5. Considérant qu'aux termes du 1 de l'article 1728 du code général des impôts, dans sa rédaction applicable au litige : " Le défaut de production dans les délais prescrits d'une déclaration ou d'un acte comportant l'indication d'éléments à retenir pour l'assiette ou la liquidation de l'impôt entraîne l'application, sur le montant des droits mis à la charge du contribuable ou résultant de la déclaration ou de l'acte déposé tardivement, d'une majoration de : a. 10 % en l'absence de mise en demeure ou en cas de dépôt de la déclaration ou de l'acte dans les trente jours suivant la réception d'une mise en demeure, notifiée par pli recommandé, d'avoir à le produire dans ce délai ; (...) c. 80 % en cas de découverte d'une activité occulte " ; qu'il résulte de ces dispositions, éclairées par les travaux parlementaires qui ont précédé l'adoption de la loi de laquelle elles sont issues, que dans le cas où un contribuable n'a ni déposé dans le délai légal les déclarations qu'il était tenu de souscrire, ni fait connaître son activité à un centre de formalités des entreprises ou au greffe du tribunal de commerce, l'administration doit être réputée apporter la preuve, qui lui incombe, de l'exercice occulte de l'activité professionnelle si le contribuable n'est pas lui même en mesure d'établir qu'il a commis une erreur justifiant qu'il ne se soit acquitté d'aucune de ces obligations déclaratives ; <br/>
<br/>
              6. Considérant qu'en jugeant que l'administration n'apportait pas la preuve que l'activité exercée par l'établissement stable dont la société disposait en France présentait un caractère occulte au sens du c. du 1 de l'article 1728 du code général des impôts au motif que la société avait souscrit des déclarations de taxe sur la valeur ajoutée correspondant aux opérations de ventes de biens immobiliers sur le territoire français qu'elle avait réalisées, alors que, comme elle l'avait relevé, ces déclarations n'avaient pas été déposées au titre de cet établissement stable et ne révélaient pas les conditions réelles de l'exercice de l'activité en cause, la cour a inexactement qualifié les faits qui lui étaient soumis ;  <br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que le ministre des finances et des comptes publics est fondé à demander l'annulation de l'article 1er de l'arrêt du 4 février 2014 en tant seulement que la cour a statué sur les pénalités au titre des années 2006 et 2007 ;<br/>
<br/>
              8. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la société Faisanderie au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>              D E C I D E :<br/>
                           --------------<br/>
<br/>
Article 1er : L'article 1er de l'arrêt du 4 février 2014 de la cour administrative d'appel de Paris est annulé en tant qu'il statue sur les pénalités mises à la charge de la société DC Immobilière au titre des années 2006 et 2007.<br/>
<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Paris.<br/>
<br/>
Article 3 : Le surplus des conclusions du pourvoi du ministre des finances et des comptes publics est rejeté.<br/>
<br/>
Article 4 : Les conclusions présentées par la société Faisanderie au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée au ministre des finances et des comptes publics et à la société par actions simplifiée Faisanderie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
