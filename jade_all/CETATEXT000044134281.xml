<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044134281</ID>
<ANCIEN_ID>JG_L_2021_09_000000447987</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/13/42/CETATEXT000044134281.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 29/09/2021, 447987, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-09-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>447987</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP SPINOSI</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Marc Pichon de Vendeuil</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:447987.20210929</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Bastia d'annuler la décision implicite de rejet née du silence gardé par le directeur du service d'incendie et de secours (SIS) de la Haute-Corse sur sa demande de protection fonctionnelle reçue le 10 juin 2020. Par une ordonnance n° 2001142 du 26 octobre 2020, le président du tribunal administratif de Bastia a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 20MA03987 du 9 novembre 2020, le président de la 8ème chambre de la cour administrative d'appel de Marseille a rejeté l'appel formé par M. B... contre cette ordonnance.<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 21 décembre 2020 et 31 mai 2021 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du SIS de la Haute-Corse la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des relations entre le public et l'administration ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - l'ordonnance n° 2020-306 du 25 mars 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Marc Pichon de Vendeuil, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. B... et à la SCP Spinosi, avocat du SIS de Haute Corse ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B..., commandant au sein du service d'incendie et de secours (SIS) de la Haute-Corse, a sollicité, par une demande reçue le 10 juin 2020 par le directeur du SIS, le bénéfice de la protection fonctionnelle de la part de son employeur. Il a saisi le tribunal administratif de Bastia d'une requête, enregistrée le 22 octobre 2020, tendant à l'annulation de la décision implicite de rejet née du silence gardé par le directeur du SIS de la Haute-Corse sur sa demande. Le président du tribunal administratif de Bastia a rejeté cette requête pour tardiveté. Par l'ordonnance attaquée, le président de la 8ème chambre de la cour administrative d'appel de Marseille a rejeté, pour le même motif, l'appel formé par M. B... contre l'ordonnance du président du tribunal administratif.<br/>
<br/>
              2. D'une part, aux termes de l'article de l'article L. 231-4 du code des relations entre le public et l'administration : " (...) le silence gardé par l'administration pendant deux mois vaut décision de rejet : / (...) 5° Dans les relations entre l'administration et ses agents ".<br/>
<br/>
              3. D'autre part, aux termes de l'article 6 de l'ordonnance du 25 mars 2020 relative à la prorogation des délais échus pendant la période d'urgence sanitaire et à l'adaptation des procédures pendant cette même période : " Le présent titre s'applique aux administrations de l'Etat, aux collectivités territoriales, à leurs établissements publics administratifs (...) ". L'article 7 de la même ordonnance dispose : " (...) les délais à l'issue desquels une décision, un accord ou un avis de l'un des organismes ou personnes mentionnés à l'article 6 peut ou doit intervenir ou est acquis implicitement et qui n'ont pas expiré avant le 12 mars 2020 sont, à cette date, suspendus jusqu'à la fin de la période mentionnée au I de l'article 1er. / Le point de départ des délais de même nature qui auraient dû commencer à courir pendant la période mentionnée au I de l'article 1er est reporté jusqu'à l'achèvement de celle-ci. / (...) ". La période mentionnée au I de l'article 1er de cette ordonnance s'étend entre le 12 mars 2020 et le 23 juin 2020 inclus.<br/>
<br/>
              4. Il résulte de ces dispositions que le point de départ du délai de deux mois, fixé par l'article L. 231-4 du code des relations entre le public et l'administration, à l'issue duquel est née la décision implicite de rejet par le directeur du SIS de la Haute-Corse de la demande de M. B..., a été reporté au 24 juin 2020. Par suite, le délai de recours contentieux contre la décision implicite de rejet courait jusqu'au 24 octobre 2020 inclus. Il suit de là que le président de la 8ème chambre de la cour administrative d'appel de Marseille a commis une erreur de droit en jugeant que la requête dirigée contre cette décision, enregistrée le 22 octobre 2020 au greffe du tribunal administratif de Bastia, était tardive. Dès lors, M. B... est fondé à demander l'annulation de l'ordonnance qu'il attaque.<br/>
<br/>
              5. Si le SIS de la Haute-Corse soutient que la requête présentée par M. B... devant le tribunal administratif était irrecevable faute de satisfaire aux exigences de motivation prévues par l'article R. 411-1 du code de justice administrative et demande que ce motif soit substitué au motif juridiquement erroné retenu par l'ordonnance attaquée, il ne peut pas être fait droit à cette demande qui suppose une appréciation de fait de la part du juge de cassation.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du SIS de la Haute-Corse la somme de 2 000 euros à verser à M. B... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'ordonnance du 9 novembre 2020 du président de la 8ème chambre de la cour administrative d'appel de Marseille est annulée.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : Le SIS de la Haute-Corse versera une somme de 2 000 euros à M. B... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A... B... et au service d'incendie et de secours de la Haute-Corse.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
