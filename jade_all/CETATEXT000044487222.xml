<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044487222</ID>
<ANCIEN_ID>JG_L_2021_12_000000432654</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/48/72/CETATEXT000044487222.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 13/12/2021, 432654, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432654</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>OCCHIPINTI ; SCP CELICE, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>Mme Catherine Brouard-Gallet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:432654.20211213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Le directeur de l'Institut d'études politiques de Paris a engagé contre Mme B... C... des poursuites disciplinaires devant la section disciplinaire de cet établissement. Par une décision du 16 décembre 2015, la section disciplinaire a infligé à Mme C... la sanction de l'exclusion définitive de l'établissement.<br/>
<br/>
              Par une décision du 15 avril 2019, le Conseil national de l'enseignement supérieur et de la recherche (CNESER), statuant en matière disciplinaire, a rejeté l'appel formé par Mme C... contre cette décision. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 15 juillet et 15 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, Mme C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L.761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'éducation ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Brouard-Gallet, conseillère d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à Me Occhipinti, avocat de Mme C... et à la SCP Célice, Texidor, Perier, avocat de l'Institut d'études politiques de Paris ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que par une décision du 16 décembre 2015, la section disciplinaire de l'Institut d'études politiques de Paris a estimé que Mme C..., étudiante en troisième année, alors en stage à l'ambassade de France près les Nations-Unies au titre de sa scolarité, avait, par des propos de nature antisémite, d'une exceptionnelle gravité, tenus lors d'un échange sur la question israélo-palestinienne et rendus publics sur un réseau social, compromis le bon fonctionnement de l'établissement duquel elle s'était elle-même désolidarisée de ce fait, et lui a infligé la sanction de l'exclusion définitive. Mme C... se pourvoit en cassation contre la décision du 15 avril 2019 par laquelle le Conseil national de l'enseignement supérieur et de la recherche (CNESER), statuant en matière disciplinaire, a rejeté l'appel qu'elle avait formé contre la décision de la section disciplinaire de l'Institut d'études politiques de Paris du 16 décembre 2015.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond et des énonciations de la décision attaquée que le CNESER, statuant en matière disciplinaire, a omis de viser dans sa décision les moyens soulevés par Mme C... devant lui et tirés de l'irrégularité de la procédure suivie devant la section disciplinaire de l'Institut d'études politiques de Paris et d'y répondre. Mme C... est, par suite, fondée à soutenir que la décision qu'elle attaque est entachée d'irrégularité et à en demander l'annulation pour ce motif, sans qu'il soit besoin de se prononcer sur les autres moyens de son pourvoi.<br/>
<br/>
              3. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par Mme C... au titre de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de Mme C... qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du Conseil national de l'enseignement supérieur et de la recherche, statuant en matière disciplinaire, du 15 avril 2019 est annulée.<br/>
Article 2 : L'affaire est renvoyée au Conseil national de l'enseignement supérieur et de la recherche, statuant en matière disciplinaire.<br/>
Article 3 : Les conclusions de l'Institut d'études politiques de Paris et de Mme C... présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à Mme B... C... et à l'Institut d'études politiques de Paris.<br/>
Copie en sera adressée à la ministre de l'enseignement supérieur, de la recherche et de l'innovation.<br/>
<br/>
              Délibéré à l'issue de la séance du 8 novembre 2021 où siégeaient : Mme Maud Vialettes, présidente de chambre, présidant ; Mme Carine Soulay, conseillère d'Etat et Mme Catherine Brouard-Gallet, conseillère d'Etat en service extraordinaire-rapporteure. <br/>
<br/>
              Rendu le 13 décembre 2021.<br/>
<br/>
<br/>
<br/>
                 La présidente : <br/>
                 Signé : Mme Maud Vialettes<br/>
 		La rapporteure : <br/>
      Signé : Mme Catherine Brouard-Gallet<br/>
                 La secrétaire :<br/>
                 Signé : Mme A... D...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
