<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029626738</ID>
<ANCIEN_ID>JG_L_2014_10_000000374044</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/62/67/CETATEXT000029626738.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème SSJS, 24/10/2014, 374044, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-10-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374044</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:374044.20141024</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              La société Mecamidi a demandé au tribunal administratif de Toulouse de prononcer la décharge des rappels de taxe sur la valeur ajoutée ainsi que des pénalités correspondantes auxquels elle a été assujettie au titre de la période du 1er janvier 2002 au 30 novembre 2004. Par un jugement n°s 0702789, 0702790, 0702791 du 7 juin 2011, le tribunal administratif de Toulouse a rejeté ces demandes.<br/>
<br/>
              Par un arrêt n° 11BX01945 du 17 octobre 2013, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par la société Mecamidi contre ce jugement.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés le 17 décembre 2013 et le 17 mars 2014 au secrétariat du contentieux du Conseil d'Etat, la société Mecamidi demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11BX01945 du 17 octobre 2013 de la cour administrative d'appel de Bordeaux ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bastien Lignereux, auditeur,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de la société Mecamidi ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 8 octobre 2014, présentée pour la société Mecamidi ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux. "<br/>
<br/>
              2. Pour demander l'annulation de l'arrêt qu'elle attaque, la société Mecamidi soutient que la cour administrative d'appel de Bordeaux :<br/>
              - l'a insuffisamment motivé en se bornant à juger que le contrat du 4 décembre 2001 n'avait pu régulièrement lui transférer la garantie de recettes consentie par la société Ficoz à la SEMB sans détailler les raisons pour lesquelles un tel transfert ne pouvait intervenir régulièrement ;<br/>
              - a dénaturé les pièces du dossier et méconnu les dispositions de l'article L. 57 du livre des procédures fiscales en jugeant que l'administration l'avait mise à même de discuter utilement le redressement proposé en indiquant dans la proposition de redressement qu'elle acceptait la déduction d'un montant égal à 10 % du montant global du contrat dont la société était titulaire, correspondant aux pourcentages habituellement retenus pour des prestations de maîtrise d'oeuvre ;<br/>
              - a dénaturé les pièces du dossier et méconnu les dispositions de l'article R. 256-1 du livre des procédures fiscales en jugeant qu'en l'absence de modification ultérieure des bases d'impositions issues de la proposition de rectification, l'avis de mise en recouvrement du 7 novembre 2006 avait pu se borner à indiquer le montant des droits et pénalités et à viser la proposition de rectification du 29 juin 2005, alors que la réponse aux observations du 2 septembre 2005, non visée par l'avis de mise en recouvrement, avait modifié les bases et droits en matière d'impôt sur les sociétés ;<br/>
              - n'a pas tiré les conséquences de ses propres constatations et a entaché son arrêt d'une contradiction de motifs et, par suite, méconnu les dispositions de l'article 39-1° du code général des impôts lorsqu'elle s'est prononcée sur la déductibilité des honoraires de maîtrise d'oeuvre ;<br/>
              - a dénaturé les stipulations du contrat du 4 décembre 2001 en estimant que ce contrat excluait explicitement du champ des garanties les pertes d'exploitation, alors que son article 9.7 prévoyait expressément qu'elle devait garantir la recette annuelle de la centrale de fonctionnement conformément à la convention signée le 17 avril 2000 entre les sociétés SEMB et Ficoz ;<br/>
              - a dénaturé les pièces du dossier et commis une erreur de droit en écartant le moyen tiré de la déductibilité de l'intégralité de la taxe sur la valeur ajoutée ayant grevé le montant de la prestation en litige, alors que l'administration ne contestait pas la réalité des prestations facturées ;<br/>
              - a dénaturé les pièces du dossier en estimant que la société ne pouvait ignorer l'absence de réalité des prestations facturées par la société Ficoz et qu'elle ne pouvait non plus ignorer avoir pris en charge la garantie de recettes d'exploitation de la centrale d'Ogosta sans y être tenue juridiquement.<br/>
<br/>
              3. Eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi qui sont dirigées contre l'arrêt attaqué en tant qu'il s'est prononcé sur le bien-fondé des rappels de taxe sur la valeur ajoutée et sur les pénalités. En revanche, s'agissant des autres conclusions dirigées contre l'arrêt attaqué, aucun de ces moyens soulevés n'est de nature à permettre l'admission des conclusions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Les conclusions du pourvoi de la société Mecamidi qui sont dirigées contre l'arrêt attaqué en tant qu'il s'est prononcé sur le bien-fondé des rappels de taxe sur la valeur ajoutée et sur les pénalités sont admises.<br/>
Article 2 : Le surplus des conclusions du pourvoi de la société Mecamidi n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à la société Mecamidi et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
