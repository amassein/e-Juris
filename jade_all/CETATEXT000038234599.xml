<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038234599</ID>
<ANCIEN_ID>JG_L_2019_03_000000422488</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/23/45/CETATEXT000038234599.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 15/03/2019, 422488</TITRE>
<DATE_DEC>2019-03-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422488</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET ; SCP DELAMARRE, JEHANNIN</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Nevache</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:422488.20190315</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Nancy d'annuler la décision du 14 avril 2017 par laquelle le président du conseil départemental de Meurthe-et-Moselle a mis fin à sa prise en charge en qualité de jeune majeur au titre de l'aide sociale à l'enfance et d'enjoindre à cette autorité de le prendre en charge jusqu'à ses 21 ans ou, subsidiairement, de réexaminer sa situation. Par un jugement n° 1701034 du 3 juillet 2018, le tribunal administratif de Nancy a annulé la décision du 14 avril 2017 et enjoint au président du conseil départemental de Meurthe-et-Moselle de réexaminer la situation de M. B...dans un délai de deux mois.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 et 27 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, le département de Meurthe-et-Moselle demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M.B.... <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - le code du travail ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Nevache, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delamarre, Jéhannin, avocat du département de Meurthe-et-Moselle et à la SCP Sevaux, Mathonnet, avocat de M.B....<br/>
<br/>
              Vu la note en délibéré, enregistrée le 22 février 2019, présentée pour M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que M.B..., né le 10 février 1998, de nationalité guinéenne, est arrivé en France alors qu'il était mineur et a été confié au service de l'aide sociale à l'enfance du département de Meurthe-et-Moselle par décisions judiciaires, à compter du 24 avril 2015 et jusqu'à sa majorité le 10 février 2016. Il a ensuite bénéficié, sur décisions du président du conseil départemental, d'une prise en charge par le même service sous la forme d'un " contrat jeune majeur " renouvelé à quatre reprises, jusqu'au 17 avril 2017, pour l'accompagner dans son projet de formation en alternance. Le préfet de Meurthe-et-Moselle a refusé de lui délivrer un titre de séjour et lui a fait obligation de quitter le territoire français dans un délai de trente jours, par une décision du 1er juin 2016, contre laquelle M. B...a formé un recours rejeté par le tribunal administratif de Nancy le 28 février 2017. Le président du conseil départemental ayant décidé le 14 avril 2017 de mettre fin à la prise en charge de l'intéressé par le service de l'aide sociale à l'enfance à compter du 18 avril suivant, M. B...a demandé au tribunal administratif de Nancy d'annuler cette décision et d'enjoindre au département de le prendre en charge jusqu'à ses 21 ans. Par un jugement du 3 juillet 2018, contre lequel le département se pourvoit en cassation, le tribunal a annulé la décision du président du conseil départemental et lui a enjoint de réexaminer la situation de M. B...dans un délai de deux mois.<br/>
<br/>
              Sur le pourvoi du département de Meurthe-et-Moselle :<br/>
<br/>
              2. D'une part, aux termes de l'article L. 111-2 du code de l'action sociale et des familles : " Les personnes de nationalité étrangère bénéficient dans les conditions propres à chacune de ces prestations : / 1° Des prestations d'aide sociale à l'enfance ; / (...) / Elles bénéficient des autres formes d'aide sociale, à condition qu'elles justifient d'un titre exigé des personnes de nationalité étrangère pour séjourner régulièrement en France ". Le sixième et le septième et dernier alinéas de l'article L. 222-5 du même code prévoient que, sur décision du président du conseil départemental : " Peuvent être également pris en charge à titre temporaire par le service chargé de l'aide sociale à l'enfance les mineurs émancipés et les majeurs âgés de moins de vingt et un ans qui éprouvent des difficultés d'insertion sociale faute de ressources ou d'un soutien familial suffisants / Un accompagnement est proposé aux jeunes mentionnés au 1° du présent article devenus majeurs et aux majeurs mentionnés à l'avant-dernier alinéa , au-delà du terme de la mesure, pour leur permettre de terminer l'année scolaire ou universitaire engagée ". <br/>
<br/>
              3. D'autre part, aux termes de l'article L. 311-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " (...) tout étranger âgé de plus de dix-huit ans qui souhaite séjourner en France pour une durée supérieure à trois mois doit être titulaire de l'un des documents de séjour suivants : / (...) / 3° une carte de séjour temporaire (...) ". L'article L. 313-11 du même code prévoit que : " Sauf si sa présence constitue une menace pour l'ordre public, la carte de séjour temporaire portant la mention " vie privée et familiale " est délivrée de plein droit : / (...) / 2° bis A l'étranger dans l'année qui suit son dix-huitième anniversaire ou entrant dans les prévisions de l'article L. 311-3, qui a été confié, depuis qu'il a atteint au plus l'âge de seize ans, au service de l'aide sociale à l'enfance et sous réserve du caractère réel et sérieux du suivi de la formation, de la nature de ses liens avec la famille restée dans le pays d'origine et de l'avis de la structure d'accueil sur l'insertion de cet étranger dans la société française (...) ". L'article  L. 313-15 du même code prévoit qu'" A titre exceptionnel et sauf si sa présence constitue une menace pour l'ordre public, la carte de séjour temporaire prévue aux 1° et 2° de l'article L. 313-10 portant la mention "salarié" ou la mention "travailleur temporaire" peut être délivrée, dans l'année qui suit son dix-huitième anniversaire, à l'étranger qui a été confié à l'aide sociale à l'enfance entre l'âge de seize ans et l'âge de dix-huit ans et qui justifie suivre depuis au moins six mois une formation destinée à lui apporter une qualification professionnelle, sous réserve du caractère réel et sérieux du suivi de cette formation, de la nature de ses liens avec sa famille restée dans le pays d'origine et de l'avis de la structure d'accueil sur l'insertion de cet étranger dans la société française. (...) ". Enfin aux termes de l'article R. 5221-1 du code du travail : " Pour exercer une activité professionnelle salariée en France, les personnes suivantes doivent détenir une autorisation de travail (...) : / 1° Etranger non ressortissant d'un Etat membre de l'Union européenne, d'un autre Etat partie à l'Espace économique européen ou de la Confédération suisse (...) ". <br/>
<br/>
              5. Il résulte des dispositions des articles L. 111-2 et L. 222-5 du code de l'action sociale et des familles que la circonstance qu'un jeune étranger de moins de vingt et un ans soit en situation irrégulière au regard du séjour ne fait pas obstacle à sa prise en charge à titre temporaire par le service chargé de l'aide sociale à l'enfance. Toutefois, sous réserve de l'hypothèse dans laquelle un accompagnement doit être proposé au jeune pour lui permettre de terminer l'année scolaire ou universitaire engagée, le président du conseil départemental, qui dispose, sous le contrôle du juge, d'un large pouvoir d'appréciation pour accorder ou maintenir la prise en charge par ce service d'un jeune majeur de moins de vingt et un ans éprouvant des difficultés d'insertion sociale faute de ressources ou d'un soutien familial suffisants, peut prendre en considération les perspectives d'insertion qu'ouvre une prise en charge par l'aide sociale à l'enfance et à ce titre, notamment, tenir compte, pour les étrangers, de leur situation au regard du droit au séjour et au travail, particulièrement lorsqu'une autorisation de travail est nécessaire à leur projet d'insertion sociale et professionnelle, ainsi que, le cas échéant, des possibilités de régularisation de cette situation compte tenu de la formation suivie. <br/>
<br/>
              6. Il suit de là qu'en jugeant que le président du conseil départemental ne pouvait prendre en considération la situation de M. B...au regard du droit au séjour pour décider de poursuivre ou non sa prise en charge par l'aide sociale à l'enfance en tant que jeune majeur sur le fondement du sixième alinéa de l'article L. 222-5 du code de l'action sociale et des familles, alors que l'autorité préfectorale avait rejeté sa demande de titre de séjour et qu'il ne pouvait, de ce fait, disposer d'une autorisation administrative de travail, le tribunal administratif de Nancy a commis une erreur de droit. <br/>
<br/>
              7. Dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, le département de Meurthe-et-Moselle est fondé à demander l'annulation du jugement qu'il attaque.<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              Sur la légalité de la décision du président du conseil départemental de Meurthe-et-Moselle du 14 avril 2017 :<br/>
<br/>
              9. En premier lieu, il ressort des pièces du dossier que la décision attaquée a été signée par le responsable du service des mineurs non accompagnés et des jeunes majeurs du département de Meurthe-et-Moselle, qui avait reçu délégation, en vertu de l'article 17A-2 de l'arrêté du président du conseil départemental du 28 décembre 2016, pour signer " les actes relatifs à la gestion administrative et au traitement des situations individuelles (...) des majeurs étrangers suivis par le service ". Par suite, M. B...n'est pas fondé à soutenir que cette décision aurait été signée par une autorité incompétente faute de disposer d'une délégation de signature.<br/>
<br/>
              10. En deuxième lieu, l'article R. 223-2 du code de l'action sociale et des familles, applicable à l'aide sociale à l'enfance, dispose que : " Les décisions d'attribution, de refus d'attribution, de modification de la nature ou des modalités d'attribution d'une prestation doivent être motivées (...) ". La décision refusant de poursuivre la prise en charge de M. B...par le service de l'aide sociale à l'enfance au titre des jeunes majeurs mentionne l'article L. 222-5 du code de l'action sociale et des familles, rappelle l'accompagnement que le département lui a apporté dans ses démarches administratives et indique que la décision du préfet de Meurthe-et-Moselle de ne pas l'admettre au séjour, contre laquelle son recours a été rejeté par un jugement du 27 février 2017 du tribunal administratif de Nancy, ne permet plus au département de l'accompagner dans ses démarches et de travailler dans la perspective de son insertion sociale. Cette décision comporte ainsi l'énoncé des considérations de droit et de fait qui en constituent le fondement. Il s'ensuit que le requérant n'est pas fondé à soutenir qu'elle serait insuffisamment motivée.<br/>
<br/>
              11. En troisième lieu, d'une part, il résulte des dispositions du dernier alinéa de l'article L. 222-5 du code de l'action sociale et des familles citées au point 2 que lorsqu'un jeune majeur est pris en charge par le service d'aide sociale à l'enfance sur décision du président du conseil départemental, en raison des difficultés d'insertion sociale qu'il rencontre faute de ressources ou d'un soutien familial suffisants, et que la mesure arrive à son terme en cours d'année scolaire ou universitaire, le département doit lui proposer un accompagnement, qui peut prendre la forme de toute mesure adaptée à ses besoins et à son âge, pour lui permettre de ne pas interrompre l'année scolaire ou universitaire engagée. <br/>
<br/>
              12. D'autre part, aux termes de l'article L. 337-3-1 du code de l'éducation, dans sa rédaction en vigueur à la date de la décision attaquée : " Les centres de formation d'apprentis peuvent accueillir, pour une durée maximale d'un an, les élèves ayant au moins atteint l'âge de quinze ans pour leur permettre de suivre, sous statut scolaire, une formation en alternance destinée à leur faire découvrir un environnement professionnel correspondant à un projet d'entrée en apprentissage tout en leur permettant de poursuivre l'acquisition du socle commun de connaissances, de compétences et de culture mentionné à l'article L. 122-1-1. / A tout moment, l'élève peut : / - soit signer un contrat d'apprentissage (...) ; / - soit reprendre sa scolarité dans un collège ou un lycée (...) ". L'article D. 337-172 du même code précise que ces formations " (...) sont dénommées "dispositif d'initiation aux métiers en alternance" et sont destinées à faire découvrir un environnement professionnel correspondant à un projet d'entrée en apprentissage " et l'article D. 337-175 que : " La durée de la formation, d'une durée maximale d'un an, est modulée en fonction du projet pédagogique de l'élève ".<br/>
<br/>
              13. Il ressort des pièces du dossier que M.B..., qui était accompagné en tant que jeune majeur par le département, sur décision du président du conseil départemental, au titre du sixième alinéa de l'article L. 222-5, depuis sa majorité en février 2016, était accueilli par le centre de formation des apprentis de Toul depuis la rentrée 2016, dans le cadre du dispositif d'initiation aux métiers par l'alternance, dans l'attente d'une entrée en apprentissage, envisagée dans le but d'obtenir un certificat d'aptitude professionnelle " peintre-applicateur de revêtement " mais subordonnée à la régularisation de sa situation administrative. Par suite, M. B..., accueilli dans un dispositif destiné à lui faire découvrir, notamment par l'accomplissement de stages, un environnement professionnel en vue d'un projet d'entrée en apprentissage, ne peut être regardé comme ayant engagé une année scolaire pour l'achèvement de laquelle le département aurait été tenu de lui proposer un accompagnement. Il n'est, dès lors, pas fondé à soutenir que le président du conseil départemental aurait méconnu les dispositions du dernier alinéa de l'article L. 222-5 du code de l'action sociale et des familles.  <br/>
<br/>
              14. En dernier lieu, il ressort des pièces du dossier que M. B...a été pris en charge par le service de l'aide sociale à l'enfance du département de Meurthe-et-Moselle, de sa majorité le 10 février 2016 jusqu'au 17 avril 2017, et, à ce titre, a bénéficié d'un hébergement à l'hôtel et d'une aide financière, a été accompagné dans ses démarches pour obtenir un titre de séjour puis pour contester devant le tribunal administratif le refus de titre qui lui a été opposé le 1er juin 2016, et a été accueilli par un centre de formation des apprentis, dans le cadre du dispositif d'initiation aux métiers en alternance, en vue de la conclusion d'un contrat d'apprentissage, pour lequel il bénéficiait d'une promesse d'embauche renouvelée le 26 septembre 2016. Contrairement à ce que soutient M.B..., sa situation au regard du droit au séjour faisait obstacle, eu égard notamment aux dispositions des articles R. 5221-6 et R. 5221-22 du code du travail, à ce qu'il puisse obtenir, même à titre provisoire, une autorisation de travail et ainsi conclure un contrat d'apprentissage. En estimant qu'il ne lui était plus possible de poursuivre l'accompagnement de M. B...dans le projet d'insertion sociale et professionnelle qui était l'objet des " contrats jeune majeur " successifs passés avec lui, ainsi que celui-ci en avait été préalablement informé, et en refusant, dans ces circonstances, de renouveler sa prise en charge par l'aide sociale à l'enfance comme jeune majeur, le président du conseil départemental n'a commis ni erreur de droit ni erreur manifeste d'appréciation.            <br/>
<br/>
              15. Il résulte de tout ce qui précède que M. B...n'est pas fondé à demander l'annulation de la décision du 14 avril 2017 par laquelle le président du conseil départemental de Meurthe-et-Moselle a refusé de poursuivre au-delà du 17 avril suivant la prise en charge par le service de l'aide sociale à l'enfance dont il avait bénéficié depuis sa majorité. Par suite, ses conclusions à fin d'injonction ne peuvent qu'être également rejetées. <br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              16. Les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce qu'une somme soit mise à ce titre à la charge du département de Meurthe-et-Moselle, qui n'est pas, dans la présente instance, la partie perdante.  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Nancy du 3 juillet 2018 est annulé. <br/>
Article 2 : La demande présentée par M. B...devant le tribunal administratif de Nancy est rejetée.  <br/>
Article 3 : Les conclusions présentées par la SCP Sevaux, Mathonnet au titre des dispositions de l'article 37 de la loi du 10 juillet 1991 sont rejetées. <br/>
Article 4 : La présente décision sera notifiée au département de Meurthe-et-Moselle et à M. A...B....<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-02 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. AIDE SOCIALE À L'ENFANCE. - PRISE EN CHARGE DES JEUNES MAJEURS, DE MOINS DE VINGT-ET-UN ANS, ÉPROUVANT DES DIFFICULTÉS D'INSERTION SOCIALE (6E ET 7E AL. DE L'ART. L. 222-5 DU CASF) [RJ1] - 1) PRINCIPE - IMPOSSIBILITÉ DE PRENDRE EN CHARGE UN JEUNE ÉTRANGER EN SITUATION IRRÉGULIÈRE AU REGARD DU SÉJOUR - ABSENCE - 2) TEMPÉRAMENT - POSSIBILITÉ, POUR LE PRÉSIDENT DU CONSEIL DÉPARTEMENTAL, DANS L'EXERCICE DE SON POUVOIR D'APPRÉCIATION, DE PRENDRE EN CONSIDÉRATION LA SITUATION AU REGARD DU DROIT AU SÉJOUR ET DU TRAVAIL - EXISTENCE.
</SCT>
<ANA ID="9A"> 04-02-02 1) Il résulte des articles L. 111-2 et L. 222-5 du code de l'action sociale et des familles (CASF) que la circonstance qu'un jeune étranger de moins de vingt-et-un ans soit en situation irrégulière au regard du séjour ne fait pas obstacle à sa prise en charge à titre temporaire par le service chargé de l'aide sociale à l'enfance.... ,,2) Toutefois, sous réserve de l'hypothèse dans laquelle un accompagnement doit être proposé au jeune pour lui permettre de terminer l'année scolaire ou universitaire engagée, le président du conseil départemental, qui dispose, sous le contrôle du juge, d'un large pouvoir d'appréciation pour accorder ou maintenir la prise en charge par ce service d'un jeune majeur de moins de vingt-et-un ans éprouvant des difficultés d'insertion sociale faute de ressources ou d'un soutien familial suffisants, peut prendre en considération les perspectives d'insertion qu'ouvre une prise en charge par l'aide sociale à l'enfance et à ce titre, notamment, tenir compte, pour les étrangers, de leur situation au regard du droit au séjour et au travail, particulièrement lorsqu'une autorisation de travail est nécessaire à leur projet d'insertion sociale et professionnelle, ainsi que, le cas échéant, des possibilités de régularisation de cette situation compte tenu de la formation suivie.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 21 décembre 2018, M.,, n° 420393, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
