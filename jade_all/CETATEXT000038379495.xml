<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038379495</ID>
<ANCIEN_ID>JG_L_2019_04_000000412330</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/37/94/CETATEXT000038379495.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 12/04/2019, 412330, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-04-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412330</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Céline  Guibé</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:412330.20190412</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 10 juillet 2017 et le 25 mars 2019 au secrétariat du contentieux du Conseil d'Etat, la société SRD demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2017-847 du 9 mai 2017 relatif à la péréquation des charges de distribution d'électricité ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'énergie ;<br/>
              - la loi n° 2015-992 du 17 août 2015 ;<br/>
              - le décret n° 2016-158 du 18 février 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Céline Guibé, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 121-29 du code de l'énergie instaure une péréquation en vue de répartir entre les gestionnaires des réseaux publics de distribution d'électricité les charges résultant de la mission d'exploitation des réseaux publics qui leur incombe en vertu de l'article L. 121-4 du même code. Ces charges comprennent tout ou partie des coûts supportés par les gestionnaires et qui, en raison des particularités des réseaux qu'ils exploitent ou de leur clientèle, ne sont pas couverts par la part relative à l'utilisation de ces réseaux dans les tarifs réglementés de vente d'électricité et par les tarifs d'utilisation des réseaux publics de distribution. Alors que la péréquation était auparavant exclusivement mise en oeuvre à partir d'une formule forfaitaire, la loi du 17 août 2015 relative à la transition énergétique pour la croissance verte a ouvert à certains gestionnaires de réseaux la possibilité d'opter pour une péréquation en fonction de leurs charges réelles d'exploitation. <br/>
<br/>
              2. Aux termes de l'article L. 121-29, dans sa rédaction issue de la loi du 17 août 2015 relative à la transition énergétique pour la croissance verte : " (...) Les montants à percevoir ou à verser au titre de cette péréquation sont déterminés, de manière forfaitaire, à partir d'une formule de péréquation fixée par décret en Conseil d'Etat. / Toutefois, s'ils estiment que la formule forfaitaire de péréquation ne permet pas de prendre en compte la réalité des coûts d'exploitation exposés, les gestionnaires de réseaux publics de distribution d'électricité qui desservent plus de 100 000 clients et ceux qui interviennent dans les zones non interconnectées au réseau métropolitain continental peuvent renoncer au bénéfice du système de péréquation forfaitaire et opter pour une péréquation de leurs coûts d'exploitation, établie à partir de l'analyse de leurs comptes et qui tient compte des particularités physiques de leurs réseaux ainsi que de leurs performances d'exploitation. La Commission de régulation de l'énergie procède à l'analyse des comptes pour déterminer les montants à percevoir. / La gestion comptable des opérations liées à la péréquation est assurée par la société mentionnée au 1° de l'article L. 111-52. / Les coûts résultant des mécanismes de péréquation sont couverts par les tarifs d'utilisation des réseaux publics de distribution d'électricité. / Un décret en Conseil d'Etat, pris après avis de la Commission de régulation de l'énergie, précise les modalités d'application du présent article ". La société SRD demande l'annulation du décret du 9 mai 2017 relatif à la péréquation des charges de distribution d'électricité, qui a été pris pour l'application de ces dispositions.<br/>
<br/>
              Sur la légalité externe du décret attaqué :<br/>
<br/>
              3. L'organisme dont une disposition législative ou réglementaire prévoit la consultation avant l'intervention d'un texte doit être mis à même d'exprimer son avis sur l'ensemble des questions soulevées par ce texte. Par suite, dans le cas où, après avoir recueilli son avis, l'autorité compétente pour prendre le texte envisage d'apporter à son projet des modifications qui posent des questions nouvelles, elle doit le consulter à nouveau. En l'espèce, toutefois, il ressort des pièces versées au dossier que les modifications qui ont été apportées au projet de décret après la consultation du Conseil supérieur de l'énergie, à laquelle il avait été procédé en application du 1° de l'article R. 142-1 du code de l'énergie, ne soulevaient pas de question nouvelle imposant une nouvelle consultation. Ainsi, le moyen tiré de l'irrégularité de la consultation du Conseil supérieur de l'énergie ayant précédé l'adoption du décret attaqué ne peut qu'être écarté.<br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              En ce qui concerne la mention du " conseil de la péréquation des charges de la distribution d'électricité " : <br/>
<br/>
              4. L'article R. 121-58 du code de l'énergie, dans sa rédaction issue du décret attaqué, dispose que le ministre chargé de l'énergie arrête les valeurs des coefficients servant au calcul de la péréquation forfaitaire, ainsi que les montants des dotations et des contributions correspondants " après avis du conseil de la péréquation des charges de la distribution d'électricité ". Toutefois, aucune disposition du code de l'énergie ne prévoit l'existence d'un tel conseil. Il ressort du rapprochement des dispositions contestées avec celles de l'article R. 121-58, dans sa rédaction antérieurement applicable, et celles des articles R. 121-45 à R. 121-48 du code de l'énergie, qui fixent la composition, les missions et les règles de fonctionnement du conseil du Fonds de péréquation de l'électricité, que la mention d'un " conseil de la péréquation des charges de la distribution d'électricité " résulte, comme le fait valoir le ministre en défense, d'une erreur matérielle et doit en réalité être entendue comme faisant référence au conseil du Fonds de péréquation de l'électricité. Dans ces conditions, la société SRD ne peut utilement soutenir que le pouvoir réglementaire aurait méconnu l'étendue de sa compétence en ne fixant pas la composition, les missions et les règles de fonctionnement du " conseil de la péréquation des charges de la distribution d'électricité ", ni que le décret serait entaché de contradiction en ce qu'il fixerait des attributions communes à deux instances consultatives distinctes.<br/>
<br/>
              En ce qui concerne les missions confiées à la société ENEDIS : <br/>
<br/>
              5. En premier lieu, il résulte des termes mêmes de l'article L. 121-29 du code de l'énergie que le législateur a décidé que la gestion comptable des opérations liées à la péréquation serait assurée par la société mentionnée au 1° de l'article L. 111-52 du même code, c'est-à-dire à la société Enedis. Si la société requérante soutient que la désignation de la société Enedis méconnaîtrait les principes constitutionnels d'égalité devant la loi et devant les charges publiques, cette désignation ne résulte pas du décret attaqué mais des dispositions de l'article L. 121-29 du code de l'énergie. La contestation revient ainsi à mettre en cause la conformité de la loi à la Constitution et ne saurait être soulevée devant le juge de l'excès de pouvoir que selon les modalités régissant la présentation des questions prioritaires de constitutionnalité, lesquelles n'ont, en l'espèce, pas été respectées.<br/>
<br/>
              6. En deuxième lieu, si la société requérante soutient que les modalités d'application de la loi, fixées par le décret attaqué, auraient pour conséquence de confier à la société Enedis des missions excédant celles de la gestion comptable des opérations liées à la péréquation et seraient contraires au principe d'impartialité, compte tenu de sa qualité de gestionnaire de réseau contributeur du Fonds de péréquation de l'électricité, il résulte de l'article R. 121-49 du code de l'énergie, tel qu'issu du décret attaqué, que la société Enedis procède au recouvrement des contributions et au versement des dotations, à la tenue du compte de péréquation et à la conservation des pièces justificatives des opérations et des documents de comptabilité de la péréquation des charges de distribution d'électricité, de l'article R. 121-50 qu'elle notifie à chaque contributeur le montant dont il est redevable, qu'elle le met en demeure en cas de défaut de versement dans le délai prescrit et qu'elle verse aux bénéficiaires le montant qui leur est dû et de l'article R. 121-51 que le secrétariat du Fonds peut demander communication de documents comptables permettant de justifier le montant des recettes d'exploitation des réseaux déclarées par les gestionnaires de réseaux. <br/>
<br/>
              7. Ces dispositions issues du décret attaqué n'attribuent à la société Enedis aucune prérogative excédant celles qui sont nécessaires à l'exercice de la mission qui lui incombe en vertu de l'article L. 121-29 du code de l'énergie. En particulier, contrairement à ce que soutient la société requérante, aucune des dispositions du décret attaqué ne prévoit que cette société intervienne, par l'élaboration de simulations chiffrées, dans la procédure à l'issue de laquelle le ministre chargé de l'énergie arrête les valeurs des coefficients servant au calcul de la péréquation forfaitaire en application de l'article R. 121-58 du code de l'énergie. Par ailleurs, il résulte de l'article R. 121-49 de ce code, qui impose la tenue d'un compte de péréquation des charges de distribution d'électricité, que les opérations liées à la péréquation et les autres activités de la société Enedis doivent faire l'objet d'une séparation comptable. Dans ces conditions, le moyen soulevé ne peut qu'être écarté.<br/>
<br/>
              En ce qui concerne la différence de traitement entre les gestionnaires de réseaux selon qu'ils couvrent des zones interconnectées ou non au réseau métropolitain continental : <br/>
<br/>
              8. Le principe d'égalité ne s'oppose pas à ce que l'autorité investie du pouvoir réglementaire règle de façon différente des situations différentes ni à ce qu'elle déroge à l'égalité pour des raisons d'intérêt général, pourvu que la différence de traitement qui en résulte soit, dans l'un comme l'autre cas, en rapport avec l'objet de la norme qui l'établit et ne soit pas manifestement disproportionnée au regard des différences de situation susceptibles de la justifier. <br/>
<br/>
              9. Selon les deux premiers alinéas du I de l'article R. 121-60 du code de l'énergie, tels qu'issus du décret attaqué : " I. - Les gestionnaires de réseaux publics de distribution d'électricité, mentionnés au quatrième alinéa de l'article L. 121-29, qui souhaitent opter pour une péréquation établie à partir de l'analyse de leurs comptes présentent leur demande à la Commission de régulation de l'énergie au plus tard le 31 mars de l'année qui précède celle au titre de laquelle ils souhaitent que cette option soit appliquée. / Toutefois, pour les demandes présentées par les gestionnaires de réseaux des zones non interconnectées au réseau métropolitain continental, cette option s'applique dès l'année au cours de laquelle elle a été formulée ". Ces dispositions attachent une prise d'effet plus rapide à l'option pour la péréquation établie en fonction des charges réelles d'exploitation pour les gestionnaires des réseaux des zones non interconnectées au réseau métropolitain continental. Cette différence de traitement au bénéfice de ces gestionnaires, qui supportent des charges spécifiques en raison de l'absence d'interconnexion des réseaux qu'ils exploitent au réseau métropolitain continental, est toutefois en rapport avec l'objet des dispositions en cause et n'est pas manifestement disproportionnée au regard de la différence de situation entre ces gestionnaires et ceux dont le réseau s'inscrit dans une zone interconnectée au réseau métropolitain continental. Par suite, le moyen tiré de la méconnaissance, par le décret attaqué, du principe d'égalité doit être écarté.<br/>
<br/>
              En ce qui concerne la durée d'application de l'option pour la péréquation à partir des charges réelles :<br/>
<br/>
              10. Le dernier alinéa du I de l'article R. 121-60 du code de l'énergie prévoit que la demande de péréquation établie à partir de l'analyse des comptes concerne la période allant jusqu'à la fin de l'application du tarif d'utilisation des réseaux publics de distribution d'électricité en cours à la date de la demande. Le caractère pluriannuel de l'option ainsi exercée permet à la Commission de régulation de l'énergie d'établir le niveau des dotations de manière à inciter les gestionnaires concernés à réduire leurs charges d'exploitation au cours de la période tarifaire, alors qu'ils peuvent anticiper le niveau de leurs recettes qui dépend, pour l'essentiel, de l'application du tarif d'utilisation des réseaux publics de distribution d'électricité. Si la société requérante fait valoir que les gestionnaires de réseaux sont ainsi contraints d'exercer leur option avant de connaître les valeurs des coefficients servant au calcul de la péréquation forfaitaire arrêtés annuellement par le ministre en charge de l'énergie, les dispositions en cause ne méconnaissent pas l'article L. 121-9 du code de l'énergie, dès lors que celui-ci ne fixe pas la périodicité selon laquelle les opérateurs sont susceptibles d'exercer l'option et qu'il prévoit que la péréquation établie par la Commission de régulation de l'énergie tient compte des performances d'exploitation des gestionnaires de réseaux.<br/>
<br/>
              En ce qui concerne les délais de dépôt et d'examen des demandes :<br/>
<br/>
              11. En premier lieu, l'article R. 121-61 du code de l'énergie prévoit que la Commission de régulation de l'énergie notifie au gestionnaire des réseaux le niveau des dotations attribuées avant le 31 juillet de l'année au titre de laquelle elles sont versées. La société SRD fait valoir que cette date est trop tardive pour permettre aux gestionnaires de remédier aux inefficacités éventuellement identifiées par la Commission de régulation de l'énergie et de corriger la trajectoire de leurs charges d'exploitation. Toutefois, l'article L. 121-9 du code de l'énergie impose de fixer le niveau de la péréquation en tenant compte à la fois des charges d'exploitation effectivement supportées par les gestionnaires de réseaux concernés et de leurs performances d'exploitation. Dans ce cadre, en retenant comme date de notification du montant des dotations le 31 juillet de l'année considérée, le décret attaqué ne peut être regardé comme étant entaché d'une erreur manifeste d'appréciation.<br/>
<br/>
              12. En deuxième lieu, les dispositions du I de l'article R. 121-60 citées au point 8 prévoient que les demandes d'option pour la péréquation à partir des charges réelles doivent être déposées auprès de la Commission de régulation de l'énergie au plus tard le 31 mars de l'année précédant celle au titre de laquelle elle sera appliquée. Par dérogation à ces dispositions, l'article 3 du décret attaqué prévoit que les gestionnaires peuvent opter pour ce mécanisme au titre de l'année 2016 en présentant leur demande au plus tard le 19 mai 2017 et au titre des années 2017 et 2018 en présentant leur demande au plus tard le 30 juin 2017. Ces dispositions ont entendu permettre, avec des délais resserrés, une application rapide du mécanisme de péréquation à partir des charges réelles, en le rendant applicable aux années 2016 et 2017. Dans ces conditions, les brefs délais impartis à cet effet pour présenter les demandes d'option ne peuvent être regardés comme étant entachés d'erreur manifeste d'appréciation. Au demeurant, l'option pour une péréquation en fonction des charges réelles d'exploitation avait été prévue par la loi du 17 août 2015 relative à la transition énergétique pour la croissance verte et le ministre défendeur fait valoir, sans être contredit, que les modalités de mise en oeuvre de la loi ont fait l'objet d'une large concertation avec les opérateurs concernés, les délais de présentation des demandes ayant été fixés pour tenir compte des souhaits émis par les gestionnaires de réseaux lors de l'examen du projet de décret par le Conseil supérieur de l'énergie le 31 janvier 2017. Dans ces conditions, le moyen soulevé ne peut qu'être écarté.<br/>
<br/>
              13. En dernier lieu, la société SRD fait valoir que l'article 3 du décret attaqué n'a pas prévu de délai spécifique pour l'examen des demandes déposées au titre des années 2016 et 2017, ce dont il résulterait, compte tenu des dispositions de l'article R. 121-61 du code de l'énergie fixant la date de notification des dotations attribuées au 31 juillet de l'année au titre de laquelle elles sont versées, un délai d'instruction variable, allant d'un mois au titre de l'année 2017 à seize mois à compter de l'année 2019. Toutefois, les délais d'instruction inférieurs à seize mois concernent uniquement la période transitoire antérieure à l'année 2019. Par ailleurs, le délai fixé par l'article R. 121-61 du code de l'énergie n'est pas prescrit à peine de nullité et les délibérations de la Commission de régulation de l'énergie statuant sur les demandes des gestionnaires de réseaux présentées au titre des années 2016 et 2017 n'ont été adoptées que le 27 septembre 2017. Dans ces conditions, le moyen tiré de ce que le décret attaqué serait entaché d'erreur manifeste d'appréciation, faute d'avoir fixé un délai spécifique pour l'examen des demandes déposées au cours de la période transitoire, ne peut qu'être écarté. <br/>
<br/>
              14. Il résulte de tout ce qui précède que la société SRD n'est pas fondée à demander l'annulation pour excès de pouvoir du décret qu'elle attaque et que sa requête, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, doit être rejetée. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société SRD est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société SRD, au Premier ministre et au ministre d'Etat, ministre de la transition écologique et solidaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
