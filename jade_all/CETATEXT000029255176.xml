<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029255176</ID>
<ANCIEN_ID>JG_L_2014_07_000000350379</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/25/51/CETATEXT000029255176.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème SSR, 16/07/2014, 350379, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>350379</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2014:350379.20140716</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 27 juin et 27 septembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A... Bedoian, demeurant... ; M. Bedoian demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09PA05749 du 26 avril 2011 par lequel la cour administrative d'appel de Paris a rejeté son appel contre le jugement n°0618391 du 23 juillet 2009 par lequel le tribunal administratif de Paris a rejeté ses conclusions tendant à ce que l'Etat soit condamné à lui verser une somme de 6 625 540 euros et le capital représentatif d'une rente annuelle de 15 000 euros, ainsi que les intérêts au taux légal sur ces sommes à compter du 27 septembre 2006 en réparation du préjudice qu'il estime avoir subi du fait de divers agissements de la commission bancaire ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de procédure pénale ;<br/>
<br/>
              Vu le décret du 26 octobre 1849 déterminant les formes de procédure du tribunal des conflits ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de M. Bedoian et à la SCP Célice, Blancpain, Soltner, avocat de l'Autorité de contrôle prudentiel et de résolution ;<br/>
<br/>
<br/>
<br/>1. Considérant que le second alinéa de l'article 40 du code de procédure pénale dispose : " Toute autorité constituée, tout officier public ou fonctionnaire qui, dans l'exercice de ses fonctions, acquiert la connaissance d'un crime ou d'un délit, est tenu d'en donner avis sans délai au procureur de la République et de transmettre à ce magistrat tous les renseignements, procès-verbaux et actes qui y sont relatifs. " ; qu'il ressort des pièces du dossier soumis aux juges du fond que, par lettre du 29 janvier 1992, le secrétaire général de la commission bancaire a, en application de ces dispositions, signalé au procureur de la République près le tribunal de grande instance de Paris des faits, révélés par une enquête réalisée pour le compte de cette autorité administrative dans le cadre de l'accomplissement de sa mission de contrôle et de surveillance des établissements de crédit, qui lui paraissaient susceptibles de donner lieu à des poursuites contre M. Bedoian, président de la Banque d'arbitrage et de crédit ; qu'une information judiciaire a été ouverte contre X, le 5 mars 1992, à la suite de ce signalement ; que la commission bancaire s'est constituée partie civile le 17 avril 1992 en application de l'article 85 de la loi du 24 janvier 1984 relative au contrôle et à l'activité des établissements de crédit, alors en vigueur; que M. Bedoian a été inculpé, le 18 novembre 1992, de communication de renseignements sciemment inexacts à la commission bancaire, d'infractions à la législation sur les sociétés anonymes et les sociétés à responsabilité limitée et d'abus de biens sociaux ; que, le 16 juillet 2003, le vice-président du tribunal de grande instance de Paris a prononcé à son bénéfice une ordonnance de non-lieu, en l'absence de charges suffisantes contre lui ; que les appels formés contre cette ordonnance par la commission bancaire et par la Banque d'arbitrage et de crédit ont été rejetés par la cour d'appel de Paris par un arrêt du 19 novembre 2003 ; que leurs pourvois contre cet arrêt ont été rejetés comme irrecevables par un arrêt de la Cour de cassation du 26 janvier 2005 ; <br/>
<br/>
              2. Considérant qu'il ressort également des pièces du dossier soumis aux juges du fond que M. Bedoian, reprochant notamment à la commission bancaire d'avoir transmis à l'autorité judiciaire des faits erronés ou insusceptibles de revêtir une qualification pénale et d'avoir continûment manifesté à son égard une volonté de nuire en se portant partie civile et en contestant l'ordonnance de non-lieu mentionnée ci-dessus, a demandé au tribunal administratif de Paris de condamner l'Etat à lui verser une somme de 6 625 540 euros et le capital représentatif d'une rente annuelle de 15 000 euros, ainsi que les intérêts au taux légal sur ces sommes à compter du 27 septembre 2006, en réparation des préjudices qu'il estime avoir subis du fait du comportement de cette autorité ; que par un jugement du 23 juillet 2009, le tribunal administratif de Paris a rejeté cette demande comme portée devant un ordre de juridiction incompétent pour en connaître ; que, par l'arrêt attaqué du 26 avril 2011, la cour administrative d'appel de Paris, après avoir annulé ce jugement, d'une part, a jugé que les conclusions de M. Bedoian tendant à la condamnation de l'Etat au titre des conséquences dommageables résultant de fautes qu'aurait commises la commission bancaire en se portant partie civile et en exerçant les voies de recours contre l'ordonnance de non-lieu du 16 juillet 2003 avaient été portées devant un ordre de juridiction incompétent pour en connaître et, d'autre part, a rejeté ses conclusions tendant à la condamnation de l'Etat au titre des conséquences dommageables résultant de la lettre du 29 janvier 1992 mentionnée ci-dessus ; <br/>
<br/>
              3. Considérant, en premier lieu, qu'il appartient à la juridiction administrative de connaître des demandes tendant à la mise en cause de la responsabilité de l'Etat à raison des préjudices causés par l'action d'une autorité administrative ; qu'il en va différemment lorsque les actes dommageables imputés à celle-ci sont indissociables du fonctionnement du service public de la justice ; qu'en particulier, les actes intervenus au cours d'une procédure judiciaire ne peuvent être appréciés, soit en eux-mêmes, soit dans leurs conséquences, que par l'autorité judiciaire ; que la cour n'a pas commis d'erreur de droit en jugeant que la commission bancaire avait accompli des actes indissociables de la procédure judiciaire en se constituant partie civile dans le cadre des poursuites pénales ouvertes par le parquet, puis en décidant de faire appel contre l'ordonnance de non-lieu du 16 juillet 2003 et de se pourvoir en cassation contre le rejet de cet appel, et en en déduisant que les conclusions de M. Bedoian tendant à la condamnation de l'Etat à raison des fautes éventuellement commises par la commission bancaire à l'occasion de ces différentes décisions étaient portées devant un ordre de juridiction incompétent pour en connaître ;<br/>
<br/>
              4. Considérant, en second lieu, qu'aux termes de l'article 35 ajouté au décret du 26 octobre 1849 par l'article 6 du décret du 25 juillet 1960 portant réforme de la procédure des conflits d'attribution et reproduit à l'article R. 771-2 du code de justice administrative : " Lorsque le Conseil d'Etat statuant au contentieux, la Cour de cassation ou toute autre juridiction statuant souverainement et échappant ainsi au contrôle tant du Conseil d'Etat que de la Cour de cassation, est saisi d'un litige qui présente à juger, soit sur l'action introduite, soit sur une exception, une question de compétence soulevant une difficulté sérieuse, et mettant en jeu la séparation des autorités administratives et judiciaires, la juridiction saisie peut, par décision ou arrêt motivé qui n'est susceptible d'aucun recours, renvoyer au Tribunal des conflits le soin de décider sur cette question de compétence " ;<br/>
<br/>
              5. Considérant que la cour a jugé que la transmission au procureur de la République, en application de l'article 40 du code de procédure pénale, des informations recueillies par une autorité administrative n'a pas par elle-même pour effet d'ouvrir une des procédures relevant du service public de la justice ; qu'elle en a déduit qu'elle était compétente pour se prononcer sur la responsabilité de l'Etat à raison des fautes qu'aurait commises, selon M. Bedoian, la commission bancaire en transmettant au procureur, par la lettre du 29 janvier 1992 mentionnée au point 1, des informations le concernant ; <br/>
<br/>
              6. Considérant que la détermination de la juridiction compétente pour connaître des conclusions de M. Bedoian tendant à l'indemnisation du préjudice qu'il estime avoir subi du fait de la mise en oeuvre, par la commission bancaire, de l'article 40 du code de procédure pénale présente à juger une question soulevant une difficulté sérieuse et de nature à justifier le recours à la procédure prévue par l'article 35 du décret du 26 octobre 1849 ; que, par suite, il y a lieu de renvoyer au Tribunal des conflits la question de savoir si l'action introduite par M. Bedoian sur ce point relève ou non de la compétence de la juridiction administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Le pourvoi de M. Bedoian est rejeté en tant qu'il tend à l'annulation de l'article 3 de l'arrêt du 26 avril 2011 de la cour administrative d'appel de Paris rejetant comme portées devant une juridiction incompétente pour en connaître ses conclusions tendant à la condamnation de l'Etat à raison des fautes qu'aurait commises la commission bancaire en se constituant partie civile et en exerçant les voies de recours contre l'ordonnance de non-lieu du 16 juillet 2003.<br/>
Article 2 : Les conclusions du pourvoi de M. Bedoian relatives à l'indemnisation du préjudice qu'il estime avoir subi du fait de la mise en oeuvre, par la commission bancaire, de l'article 40 du code de procédure pénale sont renvoyées au Tribunal des conflits. <br/>
Article 3 : Il est sursis à statuer sur les conclusions de M. Bedoian mentionnées à l'article précédent jusqu'à ce que le Tribunal des conflits ait tranché la question de savoir quel est l'ordre de juridiction compétent pour statuer sur ces conclusions. <br/>
Article 4 : La présente décision sera notifiée à M. A... Bedoian et à l'Autorité de contrôle prudentiel et de résolution.<br/>
Copie en sera adressée pour information au ministre des finances et des comptes publics.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
