<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038713909</ID>
<ANCIEN_ID>JG_L_2019_06_000000425935</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/71/39/CETATEXT000038713909.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 28/06/2019, 425935</TITRE>
<DATE_DEC>2019-06-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425935</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Appréciation de la légalité</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP JEAN-PHILIPPE CASTON ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Pauline Berne</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:425935.20190628</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance du 30 avril 2018, le juge des référés du tribunal de grande instance de Grasse a sursis à statuer au litige opposant la société Veolia Eau - Compagnie générale des Eaux à l'association syndicale libre les jardins d'Isis et aux autres requérants et transmis au tribunal administratif de Nice la question préjudicielle tirée de l'illégalité des deux derniers alinéas de l'article 4-1 du règlement de service de l'eau de la ville d'Antibes du 5 avril 2012. Par un jugement n° 1802327 du 30 octobre 2018, le tribunal administratif de Nice a déclaré que les deux derniers alinéas de l'article 4-1 du règlement de service de l'eau de la ville d'Antibes ne sont pas illégaux.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux mémoires en réplique, enregistrés les 3 et 19 décembre 2018 et 29 janvier et 10 avril 2019 au secrétariat du contentieux du Conseil d'Etat, l'association syndicale libre (ASL) les jardins d'Isis, M. T...I..., M. G...D..., M. U...E..., M. AC...S..., M. O...H..., M. M... C..., M. AF...Z..., M. L...F..., M. R...AA..., Mme A...V..., M. N... AB...et Mme K...AE..., Mme Y...P..., M. J...X..., M. AG... Q...et M. W...AD...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond de déclarer que les dispositions attaquées du règlement de service de l'eau précité sont illégales ;<br/>
<br/>
              3°) de mettre à la charge de la société Veolia Eau - Compagnie générale des eaux la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ; <br/>
              - le code de la consommation ;<br/>
              - la loi n° 2000-1208 du 13 décembre 2000 ;<br/>
              - le décret n° 2003-408 du 28 avril 2003 ;<br/>
               - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Berne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Jean-Philippe Caston, avocat de l'association syndicale libre les jardins d'Isis, de M. T...I...,  M. G...D..., M. U...E..., M. AC...S..., M. O...H..., M. M... C..., M. AF...Z..., M. L...F..., M. R...AA..., Mme A...V..., M. N... AB...et Mme K...AE..., Mme Y...P..., M. J...X..., M. AG... Q...et M. W...AD...et à la SCP Piwnica, Molinié, avocat de la société Veolia Eau - Compagnie générale des Eaux ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 31 mai 2019 présentée par l'association syndicale libre les jardins d'Isis et autres requérants ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que l'ensemble immobilier les jardins d'Isis est composé de quinze villas, édifiées en 1985 sur le territoire de la commune d'Antibes où la société Veolia Eau - Compagnie générale des eaux est en charge du service de distribution d'eau. Par une ordonnance du 30 avril 2018, le juge des référés du tribunal de grande instance de Grasse, saisi par la société Veolia d'une demande tendant à la condamnation de l'ASL les jardins d'Isis à lui laisser un accès afin de procéder à l'installation d'un compteur général en limite de copropriété et à lui payer une provision au titre de l'installation de ce compteur général, a transmis au tribunal administratif de Nice la question préjudicielle tirée de l'illégalité des deux derniers alinéas de l'article 4-1 du règlement de service de l'eau de la ville d'Antibes du 5 avril 2012, au regard, d'une part, des dispositions de l'article 93 de la loi du 13 décembre 2000 relative à la solidarité et au renouvellement urbains et du décret du 28 avril 2003 pris pour son application et relatif à l'individualisation des contrats de fourniture d'eau et, d'autre part, des dispositions du code de la consommation prohibant les clauses abusives. Par un jugement du 30 octobre 2018, le tribunal administratif de Nice a déclaré que ces deux alinéas, aux termes desquels " (...) Pour un immeuble collectif ou un ensemble immobilier de logements, le compteur du branchement est le compteur général d'immeuble. Pour les copropriétés individualisées mais ne possédant pas de compteur général en limite de copropriété, en application de l'article 93 de la loi n° 2000-1208 du 13 décembre 2000 et le décret n° 2003-408 du 28 avril 2003, l'Exploitant du service est en droit d'imposer l'installation d'un compteur général délimitant le domaine public et ce à la charge des copropriétaires (...) ", ne sont pas illégaux. L'ASL les jardins d'Isis ainsi que MM.I..., D..., E..., S..., H..., C..., Z..., F..., AA..., B...V..., M. AB...et MmeAE..., Mme P..., MM. X..., Q...et AD...demandent l'annulation de ce jugement.<br/>
<br/>
              2. En premier lieu, le tribunal administratif a relevé qu'aucune disposition n'interdisait l'existence, en cas de présence d'un compteur général, des compteurs individuels requis pour l'individualisation des contrats de fourniture d'eau prévue par les dispositions de l'article 93 du 13 décembre 2000 relative à la solidarité et au renouvellement urbains et aux dispositions du décret du 28 avril 2003 pris pour son application et relatif à l'individualisation des contrats de fourniture d'eau. Il a ensuite estimé que les dispositions contestées ne constituaient pas une clause abusive au sens de l'article L. 132-1 du code de la consommation, dans sa rédaction applicable en l'espèce, aux motifs qu'un compteur général pouvait permettre de quantifier la consommation utilisée par les parties communes d'un immeuble et de s'assurer qu'aucune consommation de l'immeuble n'échappe au comptage et à la facturation. Ce faisant, le tribunal s'est borné à répondre à la question dont il était saisi par le juge des référés du tribunal de grande instance de Grasse et n'a pas soulevé d'office un moyen qu'il aurait été tenu de communiquer aux parties.<br/>
<br/>
              3. En deuxième lieu, le tribunal administratif s'est borné, conformément aux termes de la question préjudicielle dont il était saisi, à statuer sur la légalité des deux derniers alinéas de l'article 4-1 du règlement de service de l'eau de la ville d'Antibes, sans se prononcer sur l'applicabilité de ce règlement au cas d'espèce, et notamment l'existence de parties communes ou la nature publique ou privée du réseau d'eau situé sur le terrain de l'ensemble immobilier les jardins d'Isis en amont des compteurs individuels de chacune des villas. Le moyen tiré de ce qu'il aurait commis une erreur de fait au regard des caractéristiques de cet ensemble immobilier ne peut donc qu'être écarté. <br/>
<br/>
              4. En troisième lieu, aux termes de l'article 93 de la loi du 13 décembre 2000 relative à la solidarité et au renouvellement urbains : " Tout service public de distribution d'eau destinée à la consommation humaine est tenu de procéder à l'individualisation des contrats de fourniture d'eau à l'intérieur des immeubles collectifs à usage principal d'habitation et des ensembles immobiliers de logements dès lors que le propriétaire en fait la demande. / [...] Le propriétaire qui a formulé la demande prend en charge les études et les travaux nécessaires à l'individualisation des contrats de fourniture d'eau, notamment la mise en conformité des installations aux prescriptions du code de la santé publique et la pose de compteurs d'eau. / Les conditions d'organisation et d'exécution du service public de distribution d'eau doivent être adaptées pour préciser les modalités de mise en oeuvre de l'individualisation des contrats de fourniture d'eau, dans le respect de l'équilibre économique du service conformément à l'article L. 2224-1 du code général des collectivités territoriales. Lorsque la gestion des compteurs des immeubles concernés par l'individualisation n'est pas assurée par la collectivité responsable du service public ou son délégataire, cette gestion est confiée à un organisme public ou privé compétent conformément aux dispositions du code des marchés publics ". Aux termes des dispositions de l'article 1er du décret du 28 avril 2003 pris en application de l'article 93 de la loi du 13 décembre 2000 et relatif à l'individualisation des contrats de fourniture d'eau : " La personne morale, de droit public ou privé, chargée de l'organisation du service public de distribution d'eau adapte les conditions d'organisation et d'exécution de ce service afin de permettre l'individualisation des contrats de fourniture d'eau. / Les règles applicables aux conditions d'organisation et d'exécution de ce service définissent notamment les relations entre l'exploitant du service de distribution d'eau et les abonnés, les modalités de fourniture de l'eau, les obligations du service, les règles applicables aux abonnements, les conditions de mise en service des branchements et compteurs et les modalités de paiement des prestations et fournitures d'eau. / L'adaptation à laquelle la personne morale chargée de l'organisation du service public de distribution d'eau doit procéder porte notamment sur les prescriptions techniques que doivent respecter les installations de distribution d'eau des immeubles collectifs d'habitation et des ensembles immobiliers de logements, et qui sont nécessaires pour procéder à l'individualisation des contrats de fourniture d'eau, dans le respect des dispositions du code de la santé publique ".<br/>
<br/>
              5. Il résulte de l'ensemble de ces dispositions que, dès lors que le propriétaire en fait la demande, le service public de distribution d'eau est tenu de procéder à l'individualisation des contrats de fourniture d'eau au sein notamment des ensembles immobiliers de logements. Pour ce faire, ces dispositions prévoient que les conditions d'organisation et d'exécution de ce service doivent être adaptées, dans le respect de l'équilibre économique du service tel que prévu à l'article L. 2224-1 du code général des collectivités territoriales, cette adaptation étant assurée par la personne morale chargée de l'organisation du service public de distribution d'eau qui doit en particulier définir les règles applicables aux conditions d'organisation, notamment celles portant sur les relations entre l'exploitant du service de distribution d'eau et les abonnés. Ces dispositions, destinées à encourager l'individualisation des contrats de distribution d'eau, n'ont ainsi pas eu pour effet d'interdire le maintien ou l'installation de compteurs généraux en sus des compteurs individuels nécessaires à cette individualisation. Par suite, le tribunal administratif de Nice n'a pas commis d'erreur de droit en jugeant que les dispositions contestées du règlement du service des eaux de la ville d'Antibes, permettant à l'exploitant du service public de distribution d'imposer aux copropriétaires l'installation d'un compteur général, n'étaient pas contraires aux dispositions de l'article 93 de la loi du 13 décembre 2000 et de son décret d'application du 28 avril 2003. <br/>
<br/>
              6. En quatrième et dernier lieu, aux termes de l'article L. 132-1 du code de la consommation, dans sa rédaction applicable en l'espèce : " Dans les contrats conclus entre professionnels et non-professionnels ou consommateurs, sont abusives les clauses qui ont pour objet ou pour effet de créer, au détriment du non-professionnel ou du consommateur, un déséquilibre significatif entre les droits et obligations des parties au contrat (...) / Ces dispositions sont applicables quels que soient la forme ou le support du contrat (...) / Les clauses abusives sont réputées non écrites. / L'appréciation du caractère abusif des clauses au sens du premier alinéa ne porte ni sur la définition de l'objet principal du contrat ni sur l'adéquation du prix ou de la rémunération au bien vendu ou au service offert. / Le contrat restera applicable dans toutes ses dispositions autres que celles jugées abusives s'il peut subsister sans lesdites clauses. / Les dispositions du présent article sont d'ordre public ". Le caractère abusif d'une clause s'apprécie non seulement au regard de cette clause elle-même mais aussi compte tenu de l'ensemble des stipulations du contrat et, lorsque celui-ci a pour objet l'exécution d'un service public, des caractéristiques particulières de ce service.<br/>
<br/>
              7. En jugeant, en réponse à la question préjudicielle qui lui était renvoyée, sans se prononcer sur le statut juridique ou les caractéristiques matérielles de l'ensemble immobilier les jardins d'Isis, qu'au regard des caractéristiques particulières du service public de distribution d'eau, l'installation d'un compteur général, en cas d'individualisation des contrats de fourniture d'eau, permet dans l'intérêt de ce service, tant de s'assurer qu'aucune consommation de la copropriété, parties communes incluses, n'échappe au comptage et à la facturation, participant ainsi à l'équilibre économique du service, que de constater la délimitation entre la partie privée du réseau, collective ou individuelle, et la partie publique, et qu'en conséquence l'obligation d'installer un tel compteur général, prévue par les deux derniers alinéas de l'article 4-1 du règlement de service de l'eau de la ville d'Antibes ne présente pas le caractère d'une clause abusive au sens des dispositions de l'article L. 132-1 du code de la consommation, le tribunal administratif de Nice n'a commis ni erreur de fait ni erreur de droit, quand bien même cette délimitation pourrait  être matérialisée par d'autres éléments, tels que la pose d'une simple vanne ou, dans le cadre d'une habitation individuelle, le branchement particulier. <br/>
<br/>
              8. Il résulte de tout ce qui précède que le pourvoi de l'ASL les jardins d'Isis et des propriétaires qu'elle regroupe doit être rejeté.<br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Veolia Eau-Compagnie générale des eaux, qui n'est pas, dans la présente instance la partie perdante. En revanche, il y a lieu, dans les circonstances de l'espèce, de mettre solidairement à la charge de l'ASL les jardins d'Isis et des autres requérants la somme de 3 000 euros à verser à la société Véolia Eau-Compagnie générales des eaux, au titre de ces mêmes dispositions. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de l'ASL les jardins d'Isis est rejeté<br/>
Article 2 : L'ASL les jardins d'Isis et les autres requérants verseront à la société Veolia Eau - Compagnie générale des eaux une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à l'association syndicale libre les jardins d'Isis, premier dénommé pour l'ensemble des requérants, et à la société Veolia Eau - Compagnie générale des Eaux. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-03-03-04 COLLECTIVITÉS TERRITORIALES. COMMUNE. ATTRIBUTIONS. SERVICES COMMUNAUX. EAU. - DROIT À L'INDIVIDUALISATION DES CONTRATS DE FOURNITURE D'EAU AU SEIN DES ENSEMBLES IMMOBILIERS DE LOGEMENTS (ART. 93 DE LA LOI DU 13 DÉCEMBRE 2000 ET ART. 1ER DU DÉCRET DU 28 AVRIL 2003) - PORTÉE - INDIVIDUALISATION N'INTERDISANT PAS LE MAINTIEN OU L'INSTALLATION DE COMPTEURS GÉNÉRAUX.
</SCT>
<ANA ID="9A"> 135-02-03-03-04 Il résulte des dispositions de l'article 93 de la loi n° 2000-1208 du 13 décembre 2000 et de l'article 1er du décret n° 2003-408 du 28 avril 2003 que, dès lors que le propriétaire en fait la demande, le service public de distribution d'eau est tenu de procéder à l'individualisation des contrats de fourniture d'eau au sein notamment des ensembles immobiliers de logements. Pour ce faire, ces dispositions prévoient, d'une part, que les conditions d'organisation et d'exécution de ce service doivent être adaptées, dans le respect de l'équilibre économique du service tel que prévu à l'article L. 2224-1 du code général des collectivités territoriales (CGCT), cette adaptation étant assurée par la personne morale chargée de l'organisation du service public de distribution d'eau qui doit en particulier définir les règles applicables aux conditions d'organisation, notamment celles portant sur les relations entre l'exploitant du service de distribution d'eau et les abonnés.... ,,Ces dispositions, destinées à encourager l'individualisation des contrats de distribution d'eau, n'ont ainsi pas eu pour effet d'interdire le maintien ou l'installation de compteurs généraux en sus des compteurs individuels nécessaires à cette individualisation.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
