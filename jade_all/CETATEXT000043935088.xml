<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043935088</ID>
<ANCIEN_ID>JG_L_2021_06_000000452888</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/93/50/CETATEXT000043935088.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 09/06/2021, 452888, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>452888</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:452888.20210609</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. D... A... B... a demandé au juge des référés du tribunal administratif de Nice, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, de l'admettre au bénéfice de l'aide juridictionnelle provisoire et, d'autre part, d'enjoindre au préfet des Alpes-Maritimes de procéder à l'enregistrement de sa demande de titre de séjour présentée sur le fondement du 11° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile et de lui adresser un formulaire à compléter par son médecin, dans un délai de 8 jours à compter de la notification de l'ordonnance à intervenir, sous astreinte de 200 euros par jour de retard. Par une ordonnance n° 2102287 du 29 avril 2021, le juge des référés l'a admis au bénéfice de l'aide juridictionnelle provisoire et a rejeté le surplus des conclusions de sa demande.<br/>
<br/>
              Par une requête, enregistrée le 22 mai 2021 au secrétariat du contentieux du Conseil d'Etat, M. C... demande au juge des référés du Conseil d'Etat, sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de l'admettre au bénéfice de l'aide juridictionnelle provisoire ;<br/>
<br/>
              2°) d'annuler l'ordonnance du juge des référés du tribunal administratif de Nice en tant qu'elle rejette le surplus des conclusions de sa demande ;<br/>
<br/>
              3°) de faire droit, dans cette mesure, à sa demande de première instance ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est remplie car son état de santé exige qu'il continue d'être suivi médicalement en France et s'oppose à ce qu'il soit renvoyé dans son pays d'origine ;<br/>
              - en effet, en le plaçant en situation irrégulière, la décision litigieuse le prive de protection sociale et met en péril son suivi médical dès lors que, contrairement à ce qu'a relevé le juge des référés du tribunal administratif de Nice, la décision de la Cour nationale du droit d'asile lui a bien été notifiée le 16 février 2021, ce qui lui a fait perdre son droit au séjour en qualité de demandeur d'asile ainsi que les droits sociaux y afférents ;<br/>
              - le défaut d'enregistrement de sa demande de titre de séjour le place dans une situation d'extrême vulnérabilité ;<br/>
              - il fait état de circonstances nouvelles au sens de l'article L. 431-2 du code de l'entrée et du séjour des étrangers et du droit d'asile dès lors que la possibilité de solliciter un titre de séjour d'étranger malade ne lui a été révélée qu'à la suite d'un entretien avec une psychologue en janvier 2021 ; du fait des troubles psychiques dont il est affecté et de sa mauvaise maîtrise de la langue française, il n'avait pas compris l'information qui lui avait été délivrée au moment de la présentation de sa demande d'asile ;<br/>
              - le refus d'enregistrer sa demande de titre de séjour porte une atteinte manifestement illégale à son droit au séjour et à son droit à la santé.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures. " En application de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Il résulte de l'instruction menée en première instance que M. A... B..., ressortissant guinéen, est entré en France en septembre 2018 et a déposé une demande d'asile, qui a été rejetée le 23 décembre 2019 par une décision du directeur général de l'Office français de protection des réfugiés et apatrides, confirmée par la Cour nationale du droit d'asile le 9 février 2021. Il a alors déposé auprès de la préfecture des Alpes-Maritimes, le 25 février 2021, une demande de titre de séjour en faisant valoir la nécessité pour lui de recevoir des soins médicaux en France. La préfecture ayant refusé d'enregistrer cette demande le 10 avril 2021, il a saisi, sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative, le juge des référés du tribunal administratif de Nice qui a rejeté sa demande d'injonction par l'ordonnance attaquée du 29 avril 2021.<br/>
<br/>
              3. Aux termes de l'article L. 311-6 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Lorsqu'un étranger a présenté une demande d'asile qui relève de la compétence de la France, l'autorité administrative, après l'avoir informé des motifs pour lesquels une autorisation de séjour peut être délivrée et des conséquences de l'absence de demande sur d'autres fondements à ce stade, l'invite à indiquer s'il estime pouvoir prétendre à une admission au séjour à un autre titre et, dans l'affirmative, l'invite à déposer sa demande dans un délai fixé par décret. Il est informé que, sous réserve de circonstances nouvelles, notamment pour des raisons de santé, et sans préjudice de l'article L. 511-4, il ne pourra, à l'expiration de ce délai, solliciter son admission au séjour. " Aux termes de l'article R. 311-3-2 du même code : " Pour l'application de l'article L. 311-6, les demandes de titres de séjour sont déposées par le demandeur d'asile dans un délai de deux mois. Toutefois, lorsqu'est sollicitée la délivrance du titre de séjour mentionné au 11° de l'article L. 313-11, ce délai est porté à trois mois. " Aux termes de l'article L. 313-11 : " Sauf si sa présence constitue une menace pour l'ordre public, la carte de séjour temporaire portant la mention " vie privée et familiale " est délivrée de plein droit : (...) 11° A l'étranger résidant habituellement en France, si son état de santé nécessite une prise en charge médicale dont le défaut pourrait avoir pour lui des conséquences d'une exceptionnelle gravité et si, eu égard à l'offre de soins et aux caractéristiques du système de santé dans le pays dont il est originaire, il ne pourrait pas y bénéficier effectivement d'un traitement approprié. (...) "<br/>
<br/>
              4. Il est constant que M. A... B... a été dûment informé de la teneur des dispositions citées au point précédent lors de sa demande d'asile mais n'a pas déposé, dans le délai de 3 mois, de demande de titre de séjour au titre du 11° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile. Il n'a présenté une telle demande que le 25 février 2021, après le rejet définitif de sa demande d'asile. S'il fait valoir que ce n'est qu'à la suite d'un rendez-vous avec une psychologue en janvier 2021 qu'il a pris conscience de la possibilité pour lui de prétendre à un titre de séjour à ce titre, cette circonstance n'est pas au nombre de celles permettant de présenter une demande de titre de séjour après l'expiration du délai résultant des dispositions citées ci-dessus de l'article L. 311-6 du code de l'entrée et du séjour des étrangers et du droit d'asile. Dès lors, en refusant d'enregistrer la demande de titre de séjour de M. A... B..., le préfet des Alpes-Maritimes ne peut être regardé comme ayant porté une atteinte grave et manifestement illégale à une liberté fondamentale. Sans qu'il soit besoin d'examiner la condition d'urgence, M. A... B... n'est, par suite, pas fondé à demander l'annulation de l'ordonnance qu'il attaque, ses conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative ne pouvant qu'être rejetées, sans qu'il y ait lieu de l'admettre au bénéfice de l'aide juridictionnelle provisoire.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. A... B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. D... A... B....<br/>
Copie en sera adressée au préfet des Alpes-Maritimes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
