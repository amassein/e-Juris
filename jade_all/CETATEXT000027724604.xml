<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027724604</ID>
<ANCIEN_ID>JG_L_2013_07_000000361451</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/72/46/CETATEXT000027724604.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  5ème sous-section jugeant seule, 17/07/2013, 361451, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-07-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361451</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 5ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Appréciation de la légalité</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Anne-Françoise Roul</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:361451.20130717</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu l'ordonnance n° 110854 du 19 juillet 2012, enregistrée le 26 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président du tribunal administratif de Nancy a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête de M. B...transmise à ce tribunal par le président du tribunal administratif de Paris ; <br/>
<br/>
              Vu la requête, enregistrée le 23 septembre 2011 au greffe du tribunal administratif de Paris, présentée par M. A... B..., demeurant... ; M. B..., agissant en exécution d'un jugement de la juridiction de proximité d'Epinal du 20 juin 2001, demande au juge administratif :<br/>
<br/>
              1°) d'apprécier la légalité des délibérations du Conseil national de l'ordre des pharmaciens fixant le taux et les modalités de recouvrement des cotisations ordinales pour les années 2006/2007, 2007/2008 et 2008/2009 et de déclarer que ces délibérations sont entachées d'illégalité ;  <br/>
<br/>
              2°) de mettre à la charge du Conseil national de l'ordre des pharmaciens la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne-Françoise Roul, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que le Conseil national de l'ordre des pharmaciens ayant engagé à l'encontre de M. B...devant la juridiction de proximité d'Epinal une action en vue du recouvrement des cotisations ordinales dont il ne s'était pas acquitté au titre des années 2006/2007, 2007/2008 et 2008/2009, l'intéressé a excipé de l'illégalité des délibérations du Conseil national de l'ordre des pharmaciens fixant le taux et les modalités de recouvrement de ces cotisations ; que la juridiction de proximité d'Epinal a sursis à statuer jusqu'à ce que la juridiction administrative se soit prononcée sur la légalité de ces délibérations ; <br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article L. 4233-4 du code de la santé publique, dans sa rédaction en vigueur à la date des délibérations litigieuses : " Les frais d'installation et de fonctionnement des différents conseils de l'ordre ainsi que les indemnités de déplacement et de présence des membres des conseils sont répartis entre l'ensemble des personnes physiques ou morales inscrites aux tableaux par les soins du conseil national " ; qu'en application de ces dispositions, le Conseil national de l'ordre des pharmaciens a, par délibérations des 6 mars 2006, 12 mars 2007 et 10 mars 2008, arrêté le montant des cotisations dues par les personnes inscrites aux différents tableaux de l'ordre pour les années 2006/2007, 2007/2008 et 2008/2009 ; que M. B... soutient que ces délibérations sont illégales en tant qu'elles fixent le montant de la cotisation due par les pharmaciens inscrits au tableau de la section H, lequel, ainsi que le prévoit l'article L. 4232-1 du même code, regroupe notamment ceux qui, comme le requérant, exercent dans des établissements de santé ; <br/>
<br/>
              3. Considérant, en premier lieu, qu'en vertu des dispositions de l'article L. 4221-1 du code de la santé publique, nul ne peut exercer la profession de pharmacien s'il n'est inscrit au tableau de l'ordre ; qu'il résulte des termes mêmes de l'article L. 4233-4 précité du même code que le législateur a voulu que les frais d'installation et de fonctionnement des différents conseils de l'ordre des pharmaciens ainsi que les indemnités de déplacement et de présence des membres des conseils soient répartis entre l'ensemble des personnes physiques ou morales inscrites aux tableaux de l'ordre ; qu'en assujettissant à des cotisations annuelles les pharmaciens inscrits au tableau de la section H, le conseil national s'est conformé à ces dispositions législatives ; que, par suite, le moyen tiré de ce que les délibérations litigieuses, en obligeant ces pharmaciens à acquitter une cotisation, méconnaîtraient le principe du libre exercice d'une activité professionnelle ne saurait être accueilli ; <br/>
<br/>
              4. Considérant, en second lieu, qu'il résulte des dispositions précitées de l'article L. 4233-4 du code de la santé publique que les cotisations fixées par le Conseil national de l'ordre des pharmaciens n'ont pas le caractère de redevances pour services rendus mais ont pour objet de procurer à l'ordre les ressources nécessaires à son fonctionnement et à l'accomplissement des missions que lui confient les articles L. 4231-1 et L. 4232-2 du même code ; que, par suite, le requérant ne peut utilement soutenir que les cotisations exigées des pharmaciens relevant de la section H ne seraient pas proportionnelles aux services qui leur sont rendus par l'ordre ; <br/>
<br/>
              5. Considérant, enfin, qu'il ne ressort pas des pièces du dossier qu'en fixant à 216 euros, 226 euros et 243 euros le montant des cotisations dues par les pharmaciens relevant de la section H pour les années 2006/2007, 2007/2008 et 2008/2009, le Conseil national de l'ordre des pharmaciens aurait entaché ses délibérations d'une erreur manifeste d'appréciation ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par le Conseil national de l'ordre de pharmaciens, que M. B... n'est pas fondé à soutenir que les délibérations contestées seraient entachées d'illégalité ; <br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge du Conseil national de l'ordre des pharmaciens, qui n'est pas la partie perdante, la somme que demande M. B... au titre des frais exposés par lui et non compris dans les dépens ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le Conseil national de l'ordre des pharmaciens sur le même fondement ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : La requête de M. B...est rejetée.<br/>
<br/>
Article 2 : Les conclusions présentées par le Conseil national de l'ordre des pharmaciens au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. A...B...et au Conseil national de l'ordre des pharmaciens.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
