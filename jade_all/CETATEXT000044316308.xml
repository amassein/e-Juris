<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044316308</ID>
<ANCIEN_ID>JG_L_2021_11_000000456139</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/31/63/CETATEXT000044316308.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 10/11/2021, 456139</TITRE>
<DATE_DEC>2021-11-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>456139</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SARL DELVOLVE ET TRICHET</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Domingo</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:456139.20211110</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés les 30 août et 13 octobre 2021 au secrétariat du contentieux du Conseil d'Etat, M. L... M... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2021-866 du 30 juin 2021 portant convocation des électeurs et organisation de la consultation sur l'accession à la pleine souveraineté de la Nouvelle-Calédonie ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 400 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
                         Vu les autres pièces du dossier ;<br/>
<br/>
             Vu :<br/>
               -la Constitution, notamment son article 77 ; <br/>
               - l'accord sur la Nouvelle-Calédonie signé à Nouméa le 5 mai 1998 ; <br/>
               - la loi organique n° 99-209 du 19 mars 1999 ;<br/>
      - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, conseillère d'Etat,  <br/>
<br/>
              - les conclusions de M. Laurent Domingo, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SARL Delvolvé et Trichet, avocat de M. L... M... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. M. M... demande l'annulation pour excès de pouvoir du décret du 30 juin 2021 portant convocation des électeurs et organisation de la consultation sur l'accession à la pleine souveraineté de la Nouvelle-Calédonie.<br/>
<br/>
              2. Aux termes de l'article 76 de la Constitution : " Les populations de la Nouvelle-Calédonie sont appelées à se prononcer avant le 31 décembre 1998 sur les dispositions de l'accord signé à Nouméa le 5 mai 1998 et publié le 27 mai 1998 au Journal officiel de la République française. / (...) Les mesures nécessaires à l'organisation du scrutin sont prises par décret en Conseil d'Etat délibéré en conseil des ministres ". Son article 77 dispose que : " Après approbation de l'accord lors de la consultation prévue à l'article 76, la loi organique, prise après avis de l'assemblée délibérante de la Nouvelle-Calédonie, détermine, pour assurer l'évolution de la Nouvelle-Calédonie dans le respect des orientations définies par cet accord et selon les modalités nécessaires à sa mise en œuvre : / (...) -les conditions et les délais dans lesquels les populations intéressées de la Nouvelle-Calédonie seront amenées à se prononcer sur l'accession à la pleine souveraineté (...) ". <br/>
<br/>
              3. Selon l'article 108 de la loi organique du 19 mars 1999 relative à la Nouvelle-Calédonie : " (...) Le président et les membres du gouvernement restent en fonction jusqu'à l'expiration du mandat du congrès qui les a élus/ (...) le gouvernement assure l'expédition des affaires courantes jusqu'à l'entrée en fonction du nouveau gouvernement ". L'article 216 de la même loi organique prévoit que : " I. - La consultation sur l'accession à la pleine souveraineté prévue par l'article 77 de la Constitution est organisée conformément aux dispositions du présent titre. / II. - Les électeurs sont convoqués par décret en conseil des ministres, après consultation du gouvernement et du congrès de la Nouvelle-Calédonie. Le décret fixe le texte de la question posée, les modalités d'organisation du scrutin et notamment les modalités de remboursement par l'Etat des dépenses faites pour la campagne par les partis ou groupements politiques habilités dans les conditions posées au 2° du III de l'article 219 (...) ". L'article 217 de la même loi dispose que : " La consultation est organisée au cours du mandat du congrès qui commencera en 2014 ; elle ne peut toutefois intervenir au cours des six derniers mois précédant l'expiration de ce mandat. Sa date est fixée par une délibération du congrès adoptée à la majorité des trois cinquièmes de ses membres. Elle doit être de six mois au moins postérieure à cette délibération. Si, à l'expiration de l'avant-dernière année du mandat du congrès commençant en 2014, celui-ci n'a pas fixé la date de la consultation, elle est organisée à une date fixée par le Gouvernement de la République, dans les conditions prévues au II de l'article 216, dans la dernière année du mandat./ Si la majorité des suffrages exprimés conclut au rejet de l'accession à la pleine souveraineté, une deuxième consultation sur la même question peut être organisée à la demande écrite du tiers des membres du congrès, adressée au haut-commissaire et déposée à partir du sixième mois suivant le scrutin. La nouvelle consultation a lieu dans les dix-huit mois suivant la saisine du haut-commissaire à une date fixée dans les conditions prévues au II de l'article 216 (...) / Si, lors de la deuxième consultation, la majorité des suffrages exprimés conclut à nouveau au rejet de l'accession à la pleine souveraineté, une troisième consultation peut être organisée dans les conditions prévues aux deuxième et troisième alinéas du présent article. Pour l'application de ces mêmes deuxième et troisième alinéas, le mot : "deuxième" est remplacé par le mot : "troisième" (...) ".<br/>
<br/>
              4. Aux termes du 5 - relatif à l'évolution de l'organisation politique de la Nouvelle-Calédonie - du document d'orientation de l'accord sur la Nouvelle-Calédonie signé à Nouméa le 5 mai 1998, dit " accord de Nouméa " : " Au cours du quatrième mandat (de cinq ans) du Congrès, une consultation électorale sera organisée. La date de cette consultation sera déterminée par le Congrès, au cours de ce mandat, à la majorité qualifiée des trois cinquièmes./ Si le Congrès n'a pas fixé cette date avant la fin de l'avant-dernière année de ce quatrième mandat, la consultation sera organisée, à une date fixée par l'Etat, dans la dernière année du mandat./ La consultation portera sur le transfert à la Nouvelle-Calédonie des compétences régaliennes, l'accès à un statut international de pleine responsabilité et l'organisation de la citoyenneté en nationalité./ Si la réponse des électeurs à ces propositions est négative, le tiers des membres du Congrès pourra provoquer l'organisation d'une nouvelle consultation qui interviendra dans la deuxième année suivant la première consultation. Si la réponse est à nouveau négative, une nouvelle consultation pourra être organisée selon la même procédure et dans les mêmes délais. Si la réponse est encore négative, les partenaires politiques se réuniront pour examiner la situation ainsi créée (...) ". Le 6.5 du même accord stipule que : " Un comité des signataires sera mis en place pour:/ - prendre en compte les avis qui seront formulés par les organismes locaux consultés sur l'accord ;/ - participer à la préparation des textes nécessaires pour la mise en œuvre de l'accord ;/ - veiller au suivi de l'application de l'accord ".<br/>
<br/>
              5. En premier lieu, si les stipulations du 6.5 de l'accord de Nouméa, citées au point 4, qui ont valeur constitutionnelle, prévoient la mise en place d'un comité des signataires chargé notamment de participer à la préparation des textes nécessaires pour la mise en œuvre de l'accord, elles n'imposent pas sa consultation préalablement à l'édiction du décret en conseil des ministres qui, en application des dispositions des articles 76 et 77 de la Constitution et de l'article 216 de la loi organique du 19 mars 1999 relative à la Nouvelle-Calédonie, est pris après consultation du gouvernement et du congrès de la Nouvelle-Calédonie et porte convocation des électeurs et organisation de la consultation sur l'accession à la pleine souveraineté du territoire. Il s'ensuit que le moyen tiré de ce que le décret attaqué serait entaché d'irrégularité faute de consultation du comité des signataires ne peut qu'être écarté.<br/>
<br/>
              6. En deuxième lieu, le gouvernement de la Nouvelle-Calédonie a été consulté sur le décret attaqué le 22 juin 2021, ainsi que l'imposaient les dispositions de l'article 216 de la loi organique du 19 mars 1999 relative à la Nouvelle-Calédonie, citées au point 3. La circonstance que cette consultation ait été effectuée alors que le gouvernement de la Nouvelle-Calédonie élu par le congrès le 17 février 2021 n'avait pas encore, à la date à laquelle il a été consulté sur le projet de décret litigieux, désigné son président et que, par suite, conformément aux dispositions de l'article 108 de la loi organique également citées au point 3, ce nouveau gouvernement n'était pas encore entré en fonctions n'est pas de nature à entacher le décret d'irrégularité. <br/>
<br/>
              7. En troisième lieu, il résulte des dispositions de l'article 217 de la loi organique du 19 mars 1999 relative à la Nouvelle-Calédonie, citées au point 3, que le Conseil constitutionnel a déclarées conformes à l'accord de Nouméa et à la Constitution dans sa décision n° 99-410 DC du 15 mars 1999, que le législateur organique, s'agissant des éventuelles deuxième et troisième consultations référendaires sur l'accession à la pleine souveraineté qui doivent avoir lieu, aux termes du 5 de l'accord de Nouméa, cité au point 4, dans la deuxième année suivant la consultation précédente, a entendu, d'une part, que la demande en soit formulée par un tiers des membres du congrès auprès du haut-commissaire de la République six mois au moins après la date du précédent scrutin et, d'autre part, que cette nouvelle consultation intervienne au plus tard dans les dix-huit mois qui suivent cette demande.<br/>
<br/>
              8. En l'espèce, il ressort des pièces du dossier que la deuxième consultation sur l'accession à la pleine souveraineté de la Nouvelle-Calédonie s'est déroulée le 4 octobre 2020 et a recueilli une majorité de suffrages exprimés en faveur du rejet de l'accession à la pleine souveraineté, que la demande écrite d'organisation d'une troisième consultation a été adressée par un tiers des membres du congrès au haut-commissaire de la République le 8 avril 2021, soit plus de six mois après la deuxième consultation, et que le décret attaqué fixe au 12 décembre 2021, soit avant l'expiration du délai de 18 mois suivant la saisine du haut-commissaire de la République, la date de la troisième consultation. Il s'ensuit que le moyen tiré de ce que le décret attaqué méconnaît le délai fixé par le point 5 de l'accord de Nouméa ne peut qu'être écarté.<br/>
<br/>
              9. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur la fin de non-recevoir soulevée par le ministre des outre-mer, que M. M... n'est pas fondé à demander l'annulation du décret qu'il attaque. Il s'ensuit que ses conclusions doivent être rejetées, y compris celles tendant à ce qu'une somme soit mise à la charge de l'Etat au titre de l'article L. 761-1 du code de justice administrative. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. M... la somme que le ministre des outre-mer demande au titre des mêmes dispositions. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. M... est rejetée.<br/>
Article 2 : Les conclusions présentées par le ministre des outre-mer au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. L... M..., au Président de la République, au Premier ministre, au ministre l'intérieur et au ministre des outre-mer.<br/>
              Délibéré à l'issue de la séance du 13 octobre 2021 où siégeaient : Mme Christine Maugüé, présidente adjointe de la section du contentieux, présidant ; M. G... F..., M. Frédéric Aladjidi, présidents de chambre ; Mme I... B..., M. J... C..., Mme A... K..., M. D... E..., M. François Weil conseillers d'Etat et Mme Isabelle Lemesle, conseillère d'Etat-rapporteure.<br/>
<br/>
              Rendu le 10 novembre 2021<br/>
<br/>
<br/>
                 La Présidente : <br/>
                 Signé : Mme Christine Maugüé<br/>
 		La rapporteure : <br/>
      Signé : Mme Isabelle Lemesle<br/>
                 La secrétaire :<br/>
                 Signé : Mme H... N...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-02-06 ACTES LÉGISLATIFS ET ADMINISTRATIFS. - VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. - PROCÉDURE CONSULTATIVE. - COMPOSITION DE L'ORGANISME CONSULTÉ. - DÉCRET DE CONVOCATION POUR LA CONSULTATION SUR L'ACCESSION DE LA NOUVELLE-CALÉDONIE À LA PLEINE SOUVERAINETÉ - CONSULTATION PRÉALABLE CONDUITE AUPRÈS DU GOUVERNEMENT DE LA NOUVELLE-CALÉDONIE CHARGÉ DE L'EXPÉDITION DES AFFAIRES COURANTES - RÉGULARITÉ - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">46-01-02-01 OUTRE-MER. - DROIT APPLICABLE. - STATUTS. - NOUVELLE-CALÉDONIE. - DÉCRET DE CONVOCATION POUR LA CONSULTATION SUR L'ACCESSION À LA PLEINE SOUVERAINETÉ - CONSULTATION PRÉALABLE DU GOUVERNEMENT DE LA NOUVELLE-CALÉDONIE CONDUITE AUPRÈS DU GOUVERNEMENT CHARGÉ DE L'EXPÉDITION DES AFFAIRES COURANTES - RÉGULARITÉ - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-03-02-06 Gouvernement de la Nouvelle-Calédonie ayant été consulté le 22 juin 2021 sur le décret n° 2021-866 du 30 juin 2021 portant convocation des électeurs et organisation de la consultation sur l'accession à la pleine souveraineté de la Nouvelle-Calédonie, ainsi que l'imposait l'article 216 de la loi organique n° 99-209 du 19 mars 1999. Article 108 de la même loi organique prévoyant que le gouvernement assure l'expédition des affaires courantes jusqu'à l'entrée en fonction du nouveau gouvernement.......La circonstance que cette consultation ait été effectuée alors que le gouvernement de la Nouvelle-Calédonie élu par le congrès le 17 février 2021 n'avait pas encore, à la date à laquelle il a été consulté sur le projet de décret litigieux, désigné son président et que, par suite, conformément à l'article 108 de la loi organique, ce nouveau gouvernement n'était pas encore entré en fonctions, n'est pas de nature à entacher le décret d'irrégularité.</ANA>
<ANA ID="9B"> 46-01-02-01 Gouvernement de la Nouvelle-Calédonie ayant été consulté le 22 juin 2021 sur le décret n° 2021-866 du 30 juin 2021 portant convocation des électeurs et organisation de la consultation sur l'accession à la pleine souveraineté de la Nouvelle-Calédonie, ainsi que l'imposait l'article 216 de la loi organique n° 99-209 du 19 mars 1999. Article 108 de la même loi organique prévoyant que le gouvernement assure l'expédition des affaires courantes jusqu'à l'entrée en fonction du nouveau gouvernement.......La circonstance que cette consultation ait été effectuée alors que le gouvernement de la Nouvelle-Calédonie élu par le congrès le 17 février 2021 n'avait pas encore, à la date à laquelle il a été consulté sur le projet de décret litigieux, désigné son président et que, par suite, conformément à l'article 108 de la loi organique, ce nouveau gouvernement n'était pas encore entré en fonctions, n'est pas de nature à entacher le décret d'irrégularité.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
