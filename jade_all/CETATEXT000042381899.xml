<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042381899</ID>
<ANCIEN_ID>JG_L_2020_09_000000432063</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/38/18/CETATEXT000042381899.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 28/09/2020, 432063</TITRE>
<DATE_DEC>2020-09-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432063</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAMARRE, JEHANNIN ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>Mme Marie Walazyc</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:432063.20200928</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... C... a demandé au tribunal administratif de Dijon d'annuler pour excès de pouvoir la délibération du 28 mai 2015 par laquelle le conseil municipal de Montagny-lès-Beaune a exercé le droit de préemption sur une propriété située 3, rue de l'Eglise, ainsi que la décision implicite par laquelle son recours gracieux a été rejeté. Par un jugement n° 1502835 du 27 mai 2016, le tribunal administratif de Dijon a annulé cette délibération et cette décision. <br/>
<br/>
              M. C... a demandé au tribunal administratif de Dijon, sur le fondement de l'article L. 911-4 du code de justice administrative, d'assurer l'exécution de ce jugement. Par un jugement n° 1700543 du 9 octobre 2018, le tribunal a enjoint à la commune de Montagny-lès-Beaune, dans un délai d'un mois à compter de la notification de sa décision, de mettre M. C... en mesure d'acquérir le bien situé 3, rue de l'Eglise à Montagny-lès-Beaune. <br/>
<br/>
              Par un arrêt n°s 18LY04015, 18LY04017 du 18 juin 2019, la cour administrative d'appel de Lyon a rejeté l'appel formé contre ce jugement par la commune de Montagny-lès-Beaune, porté à un mois à compter de la notification de l'arrêt le délai accordé à la commune pour proposer à M. C... l'acquisition du bien situé 3 rue de l'Eglise, prononcé, sur l'appel incident de M. C..., une astreinte de 50 euros par jour de retard à l'encontre de la commune et dit n'y avoir pas lieu à statuer sur la requête tendant au sursis à exécution du jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 28 juin et 30 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, la commune de Montagny-lès-Beaune demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de M. C... la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - la loi n° 2014-366 du 24 mars 2014 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... D..., maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delamarre, Jehannin, avocat de la commune de Montagny-lès-Beaune et à la SCP Gaschignard, avocat de M. C... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une délibération du 28 mai 2015, le conseil municipal de Montagny-lès-Beaune a décidé d'exercer le droit de préemption urbain sur une parcelle supportant une maison d'habitation, située 3, rue de l'Eglise. Par un jugement du 27 mai 2016, statuant sur la demande de M. C..., acquéreur évincé, le tribunal administratif de Dijon a annulé cette délibération et la décision implicite rejetant le recours gracieux exercé par M. C.... En exécution de ce jugement, le maire de la commune a adressé à l'ancien propriétaire, qui l'a refusée, une proposition d'acquisition de ce bien. M. C... a alors demandé au maire de lui adresser une semblable proposition, puis, sa demande ayant été implicitement rejetée, a saisi le tribunal administratif de Dijon pour qu'il assure l'exécution de son jugement du 27 mai 2016. Par un jugement du 9 octobre 2018, le tribunal a enjoint à la commune de Montagny-lès-Beaune de mettre M. C... à même d'acquérir le bien en litige dans un délai d'un mois. La commune se pourvoit en cassation contre l'arrêt du 18 juin 2019 par lequel la cour administrative d'appel de Lyon a rejeté son appel contre ce jugement et assorti l'injonction prononcée d'une astreinte de 50 euros par jour de retard à l'expiration d'un délai d'un mois à compter de la notification de l'arrêt. <br/>
<br/>
              2. Aux termes de l'article L. 911-4 du code de justice administrative : " En cas d'inexécution d'un jugement ou d'un arrêt, la partie intéressée peut demander à la juridiction, une fois la décision rendue, d'en assurer l'exécution. / Si le jugement ou l'arrêt dont l'exécution est demandée n'a pas défini les mesures d'exécution, la juridiction saisie procède à cette définition. Elle peut fixer un délai d'exécution et prononcer une astreinte ". <br/>
<br/>
              3. Aux termes de l'article L. 213-11-1 inséré dans le code de l'urbanisme par la loi du 24 mars 2014 pour l'accès au logement et un urbanisme rénové : " Lorsque, après que le transfert de propriété a été effectué, la décision de préemption est annulée ou déclarée illégale par la juridiction administrative, le titulaire du droit de préemption propose aux anciens propriétaires ou à leurs ayants cause universels ou à titre universel l'acquisition du bien en priorité. / Le prix proposé vise à rétablir, sans enrichissement injustifié de l'une des parties, les conditions de la transaction à laquelle l'exercice du droit de préemption a fait obstacle. A défaut d'accord amiable, le prix est fixé par la juridiction compétente en matière d'expropriation, conformément aux règles mentionnées à l'article L. 213-4. / A défaut d'acceptation dans le délai de trois mois à compter de la notification de la décision juridictionnelle devenue définitive, les anciens propriétaires ou leurs ayants cause universels ou à titre universel sont réputés avoir renoncé à l'acquisition. / Dans le cas où les anciens propriétaires ou leurs ayants cause universels ou à titre universel ont renoncé expressément ou tacitement à l'acquisition dans les conditions mentionnées aux trois premiers alinéas du présent article, le titulaire du droit de préemption propose également l'acquisition à la personne qui avait l'intention d'acquérir le bien, lorsque son nom était inscrit dans la déclaration mentionnée à l'article L. 213-2 ". La déclaration mentionnée à l'article L 213-2 du code est celle que doit faire le propriétaire à la mairie avant toute aliénation soumise au droit de préemption urbain ou au droit de préemption dans une zone d'aménagement différé ou un périmètre provisoire de zone. <br/>
<br/>
              4. En vertu de ces dispositions, il appartient au juge administratif, saisi de conclusions en ce sens par l'ancien propriétaire ou l'acquéreur évincé, d'exercer les pouvoirs qu'il tient des articles L. 911-1 et suivants du code de justice administrative afin d'ordonner, le cas échéant sous astreinte, les mesures qu'implique l'annulation, par le juge de l'excès de pouvoir, d'une décision de préemption, sous réserve de la compétence du juge judiciaire, en cas de désaccord sur le prix auquel l'acquisition du bien doit être proposée, pour fixer ce prix.<br/>
<br/>
              5. En premier lieu, si les dispositions de l'article L. 213-11-1 du code de l'urbanisme font obligation au titulaire du droit de préemption, en cas de renonciation des anciens propriétaires ou de leurs ayants cause à l'acquisition du bien ayant fait l'objet d'une décision de préemption annulée ou déclarée illégale par le juge administratif après le transfert de propriété, de proposer cette acquisition à la personne qui avait l'intention d'acquérir le bien, lorsque son nom a été mentionné dans la déclaration d'intention d'aliéner, elles définissent ainsi les mesures qu'il incombe à la collectivité titulaire du droit de préemption de prendre de sa propre initiative à la suite de la décision du juge administratif. Elles n'ont ni pour objet ni pour effet de faire obstacle à ce que le juge, saisi de conclusions en ce sens par l'acquéreur évincé, alors même que son nom ne figurait pas sur ce document, enjoigne à cette collectivité de lui proposer l'acquisition du bien.<br/>
<br/>
              6. La cour administrative d'appel de Lyon a jugé que la circonstance que le nom de M. C..., bénéficiaire d'une promesse de vente portant sur le bien en litige, n'avait pas été mentionné dans la déclaration d'aliéner ne faisait pas obstacle à ce que la juridiction compétente, saisie de conclusions en ce sens sur le fondement de l'article L. 911-4 du code de justice administrative, enjoigne à la commune de Montagny-lès-Beaune, à la suite de l'annulation de la décision de préempter ce bien et de la renonciation de l'ancien propriétaire, de proposer à M. C..., acquéreur évincé, d'en faire l'acquisition. Il résulte de ce qui a été dit au point 5 qu'en statuant ainsi, la cour n'a pas commis d'erreur de droit. <br/>
<br/>
              7. En second lieu, il appartient au juge administratif, saisi, sur le fondement des articles L. 911-1 et suivants du code de justice administrative,  de conclusions tendant à ce qu'il enjoigne au titulaire du droit de préemption de proposer l'acquisition d'un bien illégalement préempté à son ancien propriétaire puis, le cas échéant, à l'acquéreur évincé, de vérifier si, au regard de l'ensemble des intérêts en présence, la cession du bien ne porterait pas une atteinte excessive à l'intérêt général. <br/>
<br/>
              8. La cour administrative d'appel de Lyon a relevé, au terme d'une appréciation souveraine des pièces du dossier exempte de dénaturation, que le bien préempté consistait en une maison d'habitation ainsi qu'une grange et un garage, que la commune, face aux difficultés de mise aux normes d'accessibilité de la salle des fêtes du village, projetait d'y aménager une salle permettant l'accueil de diverses activités et qu'il ne résultait pas de l'instruction que des travaux modifiant substantiellement l'immeuble auraient déjà été exécutés pour les besoins de ce projet. En jugeant, compte tenu de ces circonstances, tenant à la nature du bien préempté, au projet poursuivi par la commune et aux travaux entrepris à cette fin, qu'une cession du bien à M. C... ne pouvait être regardée comme portant une atteinte excessive à l'intérêt général, la cour a exactement qualifié les faits qui lui étaient soumis. <br/>
<br/>
              9. Il résulte de tout ce qui précède que la commune de Montagny-lès-Beaune n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de M. C..., qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Montagny-lès-Beaune une somme de 3 000 euros à verser à M. C... au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de la commune de Montagny-lès-Beaune est rejeté. <br/>
Article 2 : La commune de Montagny-lès-Beaune versera à M. C... une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à la commune de Montagny-lès-Beaune et à M. A... C....<br/>
Copie en sera adressée à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-02-01-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PROCÉDURES D'INTERVENTION FONCIÈRE. PRÉEMPTION ET RÉSERVES FONCIÈRES. DROITS DE PRÉEMPTION. - ANNULATION D'UNE DÉCISION DE PRÉEMPTION [RJ1] - FACULTÉ, POUR L'ACQUÉREUR ÉVINCÉ DONT LE NOM NE FIGURAIT PAS SUR LA DÉCLARATION D'INTENTION D'ALIÉNER, DE SAISIR LE JUGE AFIN QUE L'ACQUISITION DU BIEN LUI SOIT PROPOSÉE - EXISTENCE.
</SCT>
<ANA ID="9A"> 68-02-01-01 Si l'article L. 213-11-1 du code de l'urbanisme fait obligation au titulaire du droit de préemption, en cas de renonciation des anciens propriétaires ou de leurs ayants cause à l'acquisition du bien ayant fait l'objet d'une décision de préemption annulée ou déclarée illégale par le juge administratif après le transfert de propriété, de proposer cette acquisition à la personne qui avait l'intention d'acquérir le bien, lorsque son nom a été mentionné dans la déclaration d'intention d'aliéner, il définit ainsi les mesures qu'il incombe à la collectivité titulaire du droit de préemption de prendre de sa propre initiative à la suite de la décision du juge administratif. Il n'a ni pour objet ni pour effet de faire obstacle à ce que le juge, saisi de conclusions en ce sens par l'acquéreur évincé, alors même que son nom ne figurait pas sur ce document, enjoigne à cette collectivité de lui proposer l'acquisition du bien.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de l'office du juge de l'exécution, CE, décision du même jour, Ville de Paris, n° 436978, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
