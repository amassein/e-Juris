<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029893507</ID>
<ANCIEN_ID>JG_L_2014_12_000000368365</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/89/35/CETATEXT000029893507.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 12/12/2014, 368365, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-12-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368365</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:368365.20141212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 7 mai et 7 août 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour les sociétés Lloyd's France SA, AIG Europe, Westminster Aviation Insurance Group, Wurttembergische Versicherung AG, Assicurazioni Generali London, Polygon  Insurance CO LTD, Allianz Versicherungs AG, Arab Insurance Group (BSC), Allianz Marine et Aviation France et New Hampshire Insurance Compagny ; les sociétés requérantes demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11VE00383 du 24 janvier 2013 par lequel la cour administrative d'appel de Versailles a rejeté leurs requêtes tendant, d'une part, à l'annulation du jugement n° 0504142 du 2 décembre 2010 par lequel le tribunal administratif de Cergy-Pontoise a rejeté leur demande de condamnation de l'Etat à leur rembourser les sommes versées à la société Air Liberté en réparation des dommages subis par cette dernière du fait de l'accident survenu à l'aéroport de Roissy-Charles-de-Gaulle, d'autre part, à la condamnation de l'Etat à verser la contre-valeur en euros de la somme de 1 828 042,30 dollars  à Lloyd's France SA, correspondant à la somme réglée à la société Air Liberté en remboursement des frais de réparation de l'aéronef accidenté, et de la somme de 650 000 dollars à la société New Hampshire Insurance Company, correspondant à la somme versée à la société Air Liberté au titre du rachat partiel de la franchise applicable à la police d'assurance de l'aéronef, à la majoration de ces sommes des intérêts de droit décomptés à partir de la demande préalable du 4 octobre 2004, et, à titre subsidiaire, à ce que soit ordonnée une expertise afin de déterminer l'étendue des préjudices subis au titre de la police corps et au titre de la police rachat de franchise ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 8 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
            Vu les autres pièces du dossier ;<br/>
<br/>
            Vu le code des assurances ; <br/>
<br/>
            Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la société Lloyd's France SA et autres, et à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat du ministre de l'écologie, du développement durable et de l'énergie ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite de l'accident survenu le 25 mai 2000 à un aéronef de la société Air Liberté sur l'aéroport de Roissy-Charles-de-Gaulle, cette société et ses assureurs ont saisi le ministre chargé des transports d'une demande d'indemnisation des préjudices résultant de cet accident ; que, pour ce qui concerne les compagnies d'assurance représentées par la société Lloyd's France, cette demande tendait au remboursement d'une somme de 1 828 042,30 dollars au titre des versements effectués ; que, pour ce qui concerne la société New Hampshire Insurance Company, la demande tendait au remboursement de la somme de 650 000 dollars correspondant au rachat d'une partie de la franchise d'assurance restant à la charge de la société Air Liberté ; qu'après rejet implicite de cette demande, les requérantes ont saisi le tribunal administratif de Cergy-Pontoise d'une demande de condamnation de l'Etat à leur verser ces sommes ; qu'elles ont relevé appel du jugement du 2 décembre 2010 rejetant leur demande devant la cour administrative d'appel de Versailles ; qu'elles se pourvoient en cassation contre l'arrêt du 24 janvier 2013 par lequel la cour administrative d'appel de Versailles a rejeté leur appel ;<br/>
              2. Considérant qu'aux termes de l'article L. 121-12 du code des assurances : " L'assureur qui a payé l'indemnité d'assurance est subrogé, jusqu'à concurrence de cette indemnité, dans les droits et actions de l'assuré contre les tiers qui, par leur fait, ont causé le dommage ayant donné lieu à la responsabilité de l'assureur (...) " ; qu'il appartient à l'assureur qui demande à bénéficier de la subrogation prévue par ces dispositions de justifier par tout moyen du paiement effectif de l'indemnité à son assuré ;<br/>
<br/>
              3. Considérant que, pour rejeter la demande des assureurs représentés par la société Lloyd's France, la cour administrative d'appel de Versailles a estimé que les sociétés requérantes n'apportaient pas la preuve d'un paiement effectif à la société Air Liberté d'indemnités dues en réparation des dommages subis par l'aéronef et qu'elles ne démontraient pas avoir procédé à bon droit à une compensation avec une dette que la société Air Liberté aurait contracté à leur égard ; que, pour rejeter la demande de la compagnie New Hampshire Insurance, la cour a estimé que la réalité d'un paiement n'était pas davantage établie ; qu'elle en a conclu que les sociétés requérantes ne pouvaient pas prétendre être, conformément aux dispositions précitées de l'article L. 121-12 du code des assurances, subrogées dans les droits à indemnisation de la société Air Liberté en réparation des conséquences dommageables de l'accident survenu le 25 mai 2000 ;  <br/>
<br/>
              4. Considérant toutefois qu'il ressort des pièces du dossier soumis aux juges du fond que les sociétés requérantes avaient produit devant les juges du fond, en premier lieu, un " formulaire de quittance finale " portant le numéro de la police d'assurance souscrite par la société Air Liberté auprès de la société New Hampshire Insurance Company, par lequel la société Air Liberté accepte le montant du rachat de sa franchise pour 650 000 dollars, demande aux assureurs de payer cette somme à son réparateur Boeing et précise que le paiement de cette somme emporte subrogation des assureurs dans ses droits, en deuxième lieu, une télécopie adressée par la société Air Liberté à son courtier, la société Marsh, faisant état des sommes dues et donnant son accord sur les modalités d'indemnisation, en troisième lieu, des documents informatiques retraçant les paiements effectués par le courtier et relatifs à ce sinistre, dont les montants correspondent aux sommes figurant dans les premiers documents ; qu'avait en outre été produite devant la cour administrative d'appel une attestation du courtier indiquant, d'une part, que les sommes qu'il avait versées pour l'indemnisation du dommage causé par l'accident avaient été avancées par les assureurs dont les noms étaient indiqués et, d'autre part, qu'une partie de l'indemnité avait été payée par compensation avec les primes d'assurance dues par la société Air Liberté à ses assureurs ; qu'en se bornant à écarter les justificatifs qui lui étaient ainsi fournis, la cour administrative d'appel de Versailles a dénaturé les pièces du dossier qui lui était soumis ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'arrêt attaqué doit être annulé ;<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge des sociétés requérantes, qui ne sont pas les parties perdantes dans la présente instance, la somme que demande le ministre de l'écologie, du développement durable et de l'énergie au titre des frais exposés par lui et non compris dans les dépens ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme demandée au même titre par les sociétés requérantes ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 24 janvier 2013 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
<br/>
Article 3 : Les conclusions des sociétés requérantes et du ministre de l'écologie, du développement durable et de l'énergie présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée aux sociétés Lloyd's France SA, AIG Europe, Westminster Aviation Insurance Group, Wurttembergische Versicherung AG, Assicurazioni Generali London, Polygon Insurance CO LTD, Allianz Versicherungs AG, Arab Insurance Group (BSC), Allianz Marine et Aviation France et New Hampshire Insurance Company et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
