<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029214487</ID>
<ANCIEN_ID>JG_L_2014_07_000000358410</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/21/44/CETATEXT000029214487.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 09/07/2014, 358410, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>358410</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Frédéric Bereyziat</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:358410.20140709</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 10 avril et 5 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A... B..., demeurant ... ; M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10LY01504 du 7 février 2012 par lequel la cour administrative d'appel de Lyon a rejeté sa requête tendant à l'annulation du jugement n° 0900124 du 11 juin 2010 du tribunal administratif de Clermont-Ferrand rejetant sa demande tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre des années 2004 à 2006 et des pénalités correspondantes ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu la loi n° 96 1181 du 30 décembre 1996, notamment son article 115 ;<br/>
<br/>
              Vu la décision du Conseil constitutionnel n° 99-424 DC du 29 décembre 1999 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Béreyziat, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que l'article L. 12 du livre des procédures fiscales autorise l'administration fiscale à procéder à l'examen contradictoire de la situation des personnes physiques au regard de l'impôt sur le revenu ; que, par ailleurs, l'article L. 47 du même livre dispose qu'une vérification de comptabilité ne peut être engagée sans que le contribuable en ait été informé par l'envoi ou la remise d'un avis de vérification ; que, toutefois, aux termes du premier alinéa de l'article L. 47 B de ce livre : " Au cours d'une procédure d'examen de situation fiscale personnelle, l'administration peut examiner les opérations figurant sur des comptes financiers utilisés à la fois à titre privé et professionnel et demander au contribuable tous éclaircissements ou justifications sur ces opérations sans que cet examen et ces demandes constituent le début d'une procédure de vérification de comptabilité " ; que l'article L. 47 C du même livre dispose en outre : " Lorsque, au cours d'un examen contradictoire de la situation fiscale personnelle, sont découvertes des activités occultes ou mises en évidence des conditions d'exercice non déclarées de l'activité d'un contribuable, l'administration n'est pas tenue d'engager une vérification de comptabilité pour régulariser la situation fiscale du contribuable au regard de cette activité " ; qu'en vertu du c) du 1 de l'article 1728 du code général des impôts, le défaut de production d'une déclaration dans les délais prescrits entraîne l'application d'une majoration égale, en cas de découverte d'une activité occulte, à 80 pour cent du montant des droits mis à la charge du contribuable ; que l'article L. 169 du livre des procédures fiscales, dans sa rédaction alors applicable, issue de l'article 115 de la loi du 30 décembre 1996 portant loi de finances pour 1997 et antérieure à l'entrée en vigueur du IV de l'article 18 de la loi de finances rectificative du 30 décembre 2009, disposait : " Pour l'impôt sur le revenu et l'impôt sur les sociétés, le droit de reprise de l'administration des impôts s'exerce jusqu'à la fin de la troisième année qui suit celle au titre de laquelle l'imposition est due./ Par exception aux dispositions du premier alinéa, le droit de reprise de l'administration s'exerce jusqu'à la fin de la dixième année qui suit celle au titre de laquelle l'imposition est due, lorsque le contribuable n'a pas déposé dans le délai légal les déclarations qu'il était tenu de souscrire et n'a pas fait connaître son activité à un centre de formalités des entreprises ou au greffe du tribunal de commerce " ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A...B..., salarié du cabinet d'expertise comptable Berthon, a fait l'objet d'un examen de sa situation fiscale personnelle portant sur la période du 1er janvier 2004 au 31 décembre 2006 ; qu'au cours de cet examen, l'administration fiscale a découvert que l'intéressé se livrait à des détournements de fonds vers ses comptes bancaires privés, au préjudice de son employeur ; que M. B...s'est vu proposer, à l'issue de ces opérations de contrôle, des rectifications correspondant à la réintégration, dans ses bases d'imposition à l'impôt sur le revenu et aux contributions sociales, des sommes qu'il avait ainsi détournées ; que ces rectifications ont donné lieu à l'établissement de cotisations supplémentaires à ces impositions, au titre des années 2004 à 2006, assorties pour l'impôt sur le revenu de la majoration de 80 pour cent des droits éludés prévue au c) du 1 de l'article 1728 du code général des impôts et dont M. B...a demandé au juge de l'impôt la décharge, en droits et pénalités ;<br/>
<br/>
              3. Considérant qu'il ressort des énonciations de l'arrêt attaqué que, pour rejeter ces conclusions en décharge, la cour a jugé, en premier lieu, que les détournements de fonds révélés au cours de l'examen de la situation fiscale personnelle de l'intéressé étaient constitutifs d'une activité occulte et que l'administration fiscale pouvait, par suite, en application des dispositions de l'article L. 47 C du livre des procédures fiscales, régulièrement tirer les conséquences de ces détournements sur la situation fiscale personnelle du contribuable sans engager au préalable de vérification de comptabilité portant sur l'activité ainsi révélée, en second lieu, que l'administration pouvait à bon droit assortir les rectifications opérées de la sorte de la majoration de 80 pour cent mentionnée ci-dessus, dès lors que l'activité occulte de détournement de fonds à laquelle M. B...s'était livré n'avait fait l'objet d'aucune déclaration ; <br/>
<br/>
              4. Considérant qu'en statuant ainsi, par un arrêt suffisamment motivé et alors que les détournements de fonds opérés par M. B...ne pouvaient être regardés comme relevant de l'activité salariée exercée par l'intéressé, la cour n'a pas, contrairement à ce que soutient le contribuable, méconnu les dispositions de l'article L. 47 C du livre des procédures fiscales, ni celles de l'article L. 169 du même livre et du c) du 1 de l'article 1728 du code général des impôts ; qu'elle n'a pas davantage donné aux faits de l'espèce une qualification juridique inexacte ; que, dès lors, M. B...n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et au ministre chargé des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
