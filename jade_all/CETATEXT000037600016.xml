<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037600016</ID>
<ANCIEN_ID>JG_L_2018_11_000000417877</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/60/00/CETATEXT000037600016.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 09/11/2018, 417877, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-11-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417877</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SCP HEMERY, THOMAS-RAQUIN, LE GUERER</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:417877.20181109</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le Conseil national de l'ordre des masseurs-kinésithérapeutes et le conseil départemental de l'Isère de l'ordre des masseurs-kinésithérapeutes ont saisi la chambre disciplinaire de première instance de l'ordre des masseurs-kinésithérapeutes de Rhône-Alpes d'une plainte contre M. B...A...et M. C...A.... Par décision du 3 mai 2016, la chambre disciplinaire de première instance a infligé à M. B...A...et M. C...A...la sanction de l'avertissement.<br/>
<br/>
              Par une décision du 15 décembre 2017, la chambre disciplinaire nationale de l'ordre des masseurs-kinésithérapeutes, saisie par le Conseil national de l'ordre des masseurs-kinésithérapeutes a réformé la décision de la chambre disciplinaire de première instance et prononcé à l'encontre de M. B...A...et M. C...A...la sanction de l'interdiction de pratiquer la masso-kinésithérapie pendant une durée de deux ans.<br/>
<br/>
              Par un pourvoi et un mémoire complémentaire, enregistrés les 2 et 12 février 2018 au secrétariat du contentieux du Conseil d'Etat, M. B...A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision en tant qu'elle lui inflige une sanction et qu'elle rejette la demande qu'il a présentée au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              2°) de mettre à la charge de l'ordre national des masseurs-kinésithérapeutes une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
 - le code de la santé publique ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de M. B...A...et à la SCP Hémery, Thomas-Raquin, Le Guerer, avocat du Conseil national de l'ordre des masseurs-kinésithérapeutes.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier que le Conseil national de l'ordre des masseurs-kinésithérapeutes et le conseil départemental de l'Isère de l'ordre des masseurs-kinésithérapeutes ont saisi la chambre disciplinaire de première instance de l'ordre des masseurs-kinésithérapeutes de Rhône-Alpes d'une plainte contre M. B...A...et M. C... A... ; que, par décision du 3 mai 2016, la chambre disciplinaire de première instance a infligé aux intéressés la sanction de l'avertissement ; que, par décision du 15 décembre 2017, la chambre disciplinaire nationale, saisie par le conseil départemental, a réformé la décision de la chambre disciplinaire de première instance et prononcé contre M. B... A...et M. C...A...la sanction de l'interdiction de pratiquer la masso-kinésithérapie pendant une durée de deux ans ; que M. B...A...se pourvoit en cassation contre cette décision en tant qu'elle lui inflige cette sanction ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 4321-64 du code de la santé publique : " Lorsque le masseur-kinésithérapeute participe à une action d'information de caractère éducatif et sanitaire auprès d'un public non professionnel, quel qu'en soit le moyen de diffusion, il ne fait état que de données suffisamment confirmées, fait preuve de prudence et a le souci des répercussions de ses propos auprès du public. Il se garde à cette occasion de toute attitude publicitaire, soit personnelle, soit en faveur des organismes où il exerce ou auxquels il prête son concours. Il ne promeut pas une cause qui ne soit pas d'intérêt général. (...) " ; qu'aux termes de l'article R. 4321-65 du même  code : " Le masseur-kinésithérapeute ne divulgue pas dans les milieux professionnels une nouvelle pratique insuffisamment éprouvée sans accompagner sa communication des réserves qui s'imposent. Il ne fait pas une telle divulgation auprès d'un public non professionnel " ; qu'aux termes de l'article R. 4321-67 de ce code : " La masso-kinésithérapie ne doit pas être pratiquée comme un commerce. Sont interdits tous procédés directs ou indirects de publicité, exception faite des cas prévus aux articles R. 4321-124 et R. 4321-125. En particulier, les vitrines doivent être occultées et ne porter aucune mention autre que celles autorisées par l'article R. 4321-123 " ; qu'aux termes de l'article R. 4321-80 du même code : " Dès lors qu'il a accepté de répondre à une demande, le masseur-kinésithérapeute s'engage personnellement à assurer au patient des soins consciencieux, attentifs et fondés sur les données actuelles de la science " ; qu'aux termes de l'article R. 4321-87 de ce code : " Le masseur-kinésithérapeute ne peut conseiller et proposer au patient ou à son entourage, comme étant salutaire ou sans danger, un produit ou un procédé, illusoire ou insuffisamment éprouvé. Toute pratique de charlatanisme est interdite " ;<br/>
<br/>
              3. Considérant que, pour prononcer contre M. B...A...et son frère, M. C... A..., la sanction de l'interdiction d'exercer la masso-kinésithérapie pour une durée de deux ans, la chambre disciplinaire nationale s'est fondée sur les circonstances que les intéressés avaient fait l'apologie, par un ouvrage, des interviews et des conférences et, pendant un temps, sur un site internet, d'une méthode thérapeutique, dénommée " thérapie quantique intégrative ", non fondée sur les données acquises de la science, et que la pratique de cette méthode dans les locaux de kinésithérapie était de nature à créer une confusion ; qu'elle en a déduit qu'à supposer même que M. B...A..., comme il le faisait valoir, ne pratiquait pas cette thérapie et qu'il ne " délivrait pas un enseignement en tant que tel ", les deux masseurs-kinésithérapeutes poursuivis avaient méconnu les dispositions précitées des articles R. 4321-64, R. 4321-65, R. 4321-67, R. 4321-80 et R. 4321-87 du code de la santé publique ; <br/>
<br/>
              4. Considérant que la chambre disciplinaire nationale a souverainement retenu, par une décision suffisamment motivée sur ce point, que la pratique de la " thérapie quantique intégrative ", développée par M. B...A...avec son frère, était insuffisamment éprouvée ; qu'elle en a déduit, sans commettre d'erreur de qualification juridique, qu'en faisant la promotion de cette méthode, tant auprès du public que des milieux professionnels, sans accompagner cette communication des réserves qui s'imposaient, l'intéressé avait manqué aux obligations déontologiques qui lui incombaient en vertu des articles R. 4321-64 et R. 4321-65 précités du code de la santé publique ;<br/>
<br/>
              5. Considérant, en revanche, qu'en retenant à l'encontre de M. B...A...les griefs tirés de ce qu'il avait méconnu son obligation, prévue à l'article R. 4321-80 du code de la santé publique, d'assurer à ses patients des soins consciencieux, attentifs et fondés sur les données actuelles de la science et qu'il avait, en méconnaissance de l'article R. 4321-87 de ce code, conseillé ou proposé à ces derniers ou à leur entourage, comme salutaire ou sans danger, un procédé illusoire ou insuffisamment éprouvé, alors qu'elle n'avait pas pris parti sur le point de savoir s'il pratiquait la " thérapie quantique intégrative ", la chambre disciplinaire nationale a commis une erreur de droit ;<br/>
<br/>
              6. Considérant, en outre, qu'en reprochant à M. A...d'avoir pratiqué la masso-kinésithérapie comme un commerce, en méconnaissance de l'article R. 4321-67 du code de la santé publique, en se fondant sur la seule circonstance qu'il avait fait la promotion d'une méthode thérapeutique qu'il avait développée avec son frère, sans rechercher s'il avait cherché à tirer avantage de cette promotion pour développer son activité de masseur-kinésithérapeute en s'adressant à un public non professionnel ou s'il avait cherché à tirer avantage de sa qualité de masseur-kinésithérapeute en vue de la promotion de cette méthode à des fins commerciales, la chambre disciplinaire nationale a également commis une erreur de droit ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que M. B...A...est fondé à demander l'annulation de la décision attaquée en tant qu'elle lui inflige une sanction ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du Conseil national de l'ordre des masseurs-kinésithérapeutes le versement à M. A...d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce que soit mise à la charge de M. A... la somme que le conseil national demande au même titre ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La décision de la chambre disciplinaire nationale de l'ordre des masseurs-kinésithérapeutes du 15 décembre 2017 est annulée en tant qu'elle inflige à M. B...A...la sanction d'interdiction d'exercer la masso-kinésithérapie pendant une durée de deux ans.<br/>
<br/>
Article 2 : L'affaire est renvoyée devant la chambre disciplinaire nationale de l'ordre des masseurs-kinésithérapeutes dans la mesure de la cassation prononcée.<br/>
<br/>
Article 3 : Le Conseil national de l'ordre des masseurs-kinésithérapeutes versera à M. A... une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées par le Conseil national de l'ordre des masseurs-kinésithérapeutes au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. B...A...et au Conseil national de l'ordre des masseurs-kinésithérapeutes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
