<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038713949</ID>
<ANCIEN_ID>JG_L_2019_07_000000421573</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/71/39/CETATEXT000038713949.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 01/07/2019, 421573, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421573</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; BALAT</AVOCATS>
<RAPPORTEUR>Mme Déborah Coricon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:421573.20190701</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Toulon de condamner la commune de la Valette-du-Var (Var) à lui verser, au titre de la reconstitution de sa carrière, un montant correspondant aux salaires nets et primes non perçus entre mai 2006 et décembre 2011 et de réparer les préjudices qu'elle estime avoir subis du fait des décisions du maire de la Valette-du-Var rejetant ses demandes de réintégration. Par un jugement n° 1302939 du 6 mai 2016, le tribunal administratif de Toulon a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 16MA02684 du 17 avril 2018, la cour administrative d'appel de Marseille a rejeté l'appel formé par Mme A...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 juin et 18 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de la Valette-du-Var la somme de 3 000 euros au titre de l'article L.761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 86-68 du 13 janvier 1986 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Déborah Coricon, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de Mme A...et à Me Balat, avocat de la commune de la Valette-du-Var ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme B...A..., recrutée comme agent administratif dans les services de la commune de la Valette-du-Var (Var), a été placée en position de disponibilité pour convenances personnelles à compter du 6 décembre 2001. Après que le maire de la Valette-du-Var a rejeté deux demandes de réintégration de Mme A...en décembre 2005 et en mai 2006, Mme A...a, à nouveau, demandé à être réintégrée par courrier du 21 novembre 2007. Par un jugement n° 0801142 du 17 décembre 2009, devenu définitif, le tribunal administratif de Toulon a annulé la décision implicite par laquelle le maire de la Valette-du-Var a rejeté cette nouvelle demande. Par la suite, MmeA..., qui a été réintégrée dans les services de la commune de la Valette-du-Var le 1er décembre 2011, a demandé au tribunal administratif de Toulon de condamner cette commune à l'indemniser des préjudices qu'elle estime avoir subi entre mai 2006 et décembre 2011 du fait de son maintien en position de disponibilité. Par un jugement du 6 mai 2016, le tribunal administratif de Toulon a rejeté cette demande. Mme A...se pourvoit en cassation contre l'arrêt du 17 avril 2018 par lequel la cour administrative d'appel de Marseille a rejeté l'appel qu'elle a formé contre ce jugement.<br/>
<br/>
              2. Aux termes du second alinéa de l'article 72 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, dans sa rédaction applicable au litige : " La disponibilité est prononcée, soit à la demande de l'intéressé, soit d'office à l'expiration des congés prévus aux 2°, 3° et 4° de l'article 57. Le fonctionnaire mis en disponibilité qui refuse successivement trois postes qui lui sont proposés dans le ressort territorial de son cadre d'emploi, emploi ou corps en vue de la réintégration peut être licencié après avis de la commission administrative paritaire ". Aux termes de l'article 26 du décret du 13 janvier 1986 relatif aux positions de détachement, hors cadres, de disponibilité et de congé parental des fonctionnaires territoriaux, dans sa rédaction applicable au litige : " Sauf dans le cas où la période de mise en disponibilité n'excède pas trois mois, le fonctionnaire mis en disponibilité sur sa demande fait connaître à son administration d'origine sa décision de solliciter le renouvellement de la disponibilité ou de réintégrer son cadre d'emplois d'origine trois mois au moins avant l'expiration de la disponibilité. (...) Le fonctionnaire qui a formulé avant l'expiration de la période de mise en disponibilité une demande de réintégration est maintenu en disponibilité jusqu'à ce qu'un poste lui soit proposé dans les conditions prévues à l'article 97 de la loi du 26 janvier 1984 précitée (...) ". Il résulte de ces dispositions que le fonctionnaire mis en disponibilité pour convenance personnelle a le droit, sous réserve de la vacance d'un emploi correspondant à son grade, d'obtenir sa réintégration à l'issue d'une période de disponibilité. Si ces textes n'imposent pas à l'autorité dont relève le fonctionnaire de délai pour procéder à cette réintégration, celle-ci doit intervenir, en fonction des vacances d'emplois qui se produisent, dans un délai raisonnable. Dans le cas où la collectivité dont relève l'agent qui a demandé sa réintégration ne peut lui proposer un emploi correspondant à son grade, elle doit saisir le centre national de la fonction publique territoriale ou le centre de gestion local afin qu'il lui propose tout emploi vacant correspondant à son grade.<br/>
<br/>
              3. Pour juger que l'illégalité externe dont était entachée la décision implicite du maire de la Valette-du-Var rejetant la demande de réintégration formulée par Mme A...le 21 novembre 2007 n'avait pas de lien de causalité direct avec les préjudices dont celle-ci demandait réparation, dès lors que cette décision n'apparaissait pas entachée, sur le fond, d'illégalité, la cour a relevé qu'il ne résultait pas de l'instruction que la commune ait été en mesure de proposer à cette date un emploi correspondant à son grade. La cour, en statuant ainsi, alors que la commune n'avait produit aucun élément relatif aux postes sur lesquels Mme A...aurait pu être réintégrée et sur leur indisponibilité mais s'était bornée à affirmer que celle-ci n'établissait pas que le refus d'intégration ne serait pas justifié au fond, la cour a commis une erreur de droit.  <br/>
<br/>
              4. Il résulte de ce qui précède que Mme A...est fondée à demander l'annulation de l'arrêt qu'elle attaque. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de la Valette-du-Var le versement à Mme A...d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. Les dispositions de cet article font obstacle à ce qu'une somme soit mise à la charge de Mme A...qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt n° 16MA02684 de la cour administrative de Marseille du 17 avril 2018 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille. <br/>
Article 3 : La commune de la Valette-du-Var versera à Mme A...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la commune de la Valette-du-Var au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme B...A...et à la commune de la Valette-du-Var.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
