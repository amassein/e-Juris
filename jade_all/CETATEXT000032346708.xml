<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032346708</ID>
<ANCIEN_ID>JG_L_2016_03_000000394396</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/34/67/CETATEXT000032346708.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème SSJS, 30/03/2016, 394396, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-03-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394396</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>Mme Dominique Bertinotti</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:394396.20160330</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C...A...a demandé au tribunal administratif de Nantes d'annuler les opérations électorales qui se sont déroulées les 22 et 29 mars 2015 en vue de l'élection des conseillers départementaux dans le canton Le Mans 7 (Sarthe). Par un jugement n° 1502903 du 20 octobre 2015, le tribunal administratif de Nantes a annulé ces opérations électorales.<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 4 novembre et 4 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, Mme E...F...et M. B...D...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) de rejeter la protestation de M. A...;<br/>
<br/>
              3°) de mettre à la charge de M. A...la somme de 4 000 euros au titre de l'article L 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dominique Bertinotti, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de Mme F...et de M. D...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'à l'issue des opérations électorales qui se sont déroulées le 22 mars 2015, pour le premier tour du scrutin organisé en vue de la désignation des conseillers départementaux du canton n° 16 (Le Mans 7) du département de la Sarthe, le binôme constitué par M. G...et Mme I...a obtenu 1 997 voix, soit 25,46 % des suffrages exprimés et 11,20 % des électeurs inscrits, le binôme constitué par M. D...et Mme F...1 963 voix, soit 25,02 % des suffrages exprimés et 11 % des électeurs inscrits, et le binôme constitué par M. A... et Mme H...1 961 voix, soit 25 % des suffrages exprimés et 10,99 % des électeurs inscrits ; que, par un jugement du 20 octobre 2015, le tribunal administratif de Nantes a, sur la protestation de M.A..., retenu que le nombre de suffrages exprimés, lors de ce premier tour de scrutin, était supérieur au nombre d'émargements de six unités et a annulé en conséquence l'ensemble des opérations électorales des 22 et 29 mars 2015 dans ce canton ; que Mme F...et M. D...relèvent appel de ce jugement ; <br/>
<br/>
              2.	Considérant qu'aux termes des dispositions de l'article L. 210-1 du code électoral : " ... Nul binôme ne peut être candidat au second tour s'il ne s'est présenté au premier tour et s'il n'a obtenu un nombre de suffrages égal au moins à 12,5 % du nombre des électeurs inscrits. / Dans le cas où un seul binôme de candidats remplit ces conditions, le binôme ayant obtenu après celui-ci le plus grand nombre de suffrages au premier tour peut se maintenir au second. / Dans le cas où aucun binôme de candidats ne remplit ces conditions, les deux binômes ayant obtenu le plus grand nombre de suffrages au premier tour peuvent se maintenir au second " ;<br/>
<br/>
              3.	Considérant qu'il résulte de l'instruction que, lors du premier tour de scrutin, le nombre de suffrages enregistrés par les machines à voter des bureaux de vote n° 44 et n° 70 de la commune du Mans a été supérieur d'une et deux unités au nombre des signatures portées sur la liste d'émargement ; que le nombre des enveloppes trouvées dans l'urne du bureau de vote n° 5 de la commune d'Allonnes a été supérieur de trois unités au nombre des signatures portées sur la liste d'émargement ; que l'allégation selon laquelle trois électeurs auraient voté dans ce bureau de vote en omettant de signer la liste d'émargement n'est pas de nature à justifier cet écart ; que, dans une telle hypothèse, il appartient, même en l'absence de manoeuvre, au juge de l'élection de retrancher les six suffrages excédentaires du nombre de voix obtenu par les deux binômes ayant accédé au second tour, afin d'apprécier si ces irrégularités ont été de nature à exercer une influence sur l'issue du premier tour et, en conséquence, sur le second tour ; qu'après déduction des six suffrages excédentaires, le binôme constitué par M. D...et Mme F... obtiendrait 1 957 voix, soit un nombre de voix inférieur à celui obtenu par le binôme constitué par M. A...et MmeH..., qui aurait ainsi pu, dans cette hypothèse, être présent au second tour ; qu'il résulte de ce qui précède que Mme F...et M. D...ne sont pas fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Nantes a annulé les opérations électorales qui se sont déroulées les 22 et 29 mars 2015 dans le canton n° 16 (Le Mans 7) du département de la Sarthe ; <br/>
<br/>
              4.	Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de M.A..., qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. A...au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                                          --------------<br/>
<br/>
Article 1er : La requête de Mme F...et M. D...est rejetée.<br/>
<br/>
Article 2 : Les conclusions de M. A...présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à Mme E...F..., à M. B...D..., à M. C... A...et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
