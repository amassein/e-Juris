<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028337161</ID>
<ANCIEN_ID>JG_L_2013_12_000000361575</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/33/71/CETATEXT000028337161.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 13/12/2013, 361575</TITRE>
<DATE_DEC>2013-12-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361575</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:361575.20131213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 2 août 2012 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de l'intérieur ; le ministre demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 11BX02804 du 31 mai 2012 par lequel la cour administrative d'appel de Bordeaux a, à la demande de Mme A...D..., épouseB..., d'une part, annulé le jugement n° 1004851 du 25 mars 2011 par lequel le tribunal administratif de Toulouse a rejeté la demande de l'intéressée tendant à l'annulation de l'arrêté du 15 juillet 2011 par lequel le préfet de la Haute-Garonne a rejeté sa demande de titre de séjour et lui a fait obligation de quitter le territoire français en fixant comme pays de renvoi celui dont elle a la nationalité, d'autre part, enjoint à ce préfet de réexaminer sa situation ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les conclusions d'appel de Mme B...; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 6 décembre 2013, présentée par le ministre de l'intérieur ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, Auditeur,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme A...D..., ressortissante congolaise entrée sur le territoire français en 2005, a épousé le 20 mars 2010 M. C...B..., de nationalité française ; qu'elle a sollicité, sur le fondement des dispositions du 4° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, la délivrance d'un titre de séjour en qualité de conjoint de Français ; que, par un arrêté du 19 juillet 2010, le préfet de la Haute-Garonne a rejeté sa demande de titre de séjour et a assorti son refus d'une obligation de quitter le territoire en fixant le Congo comme pays de renvoi ; que le tribunal administratif de Toulouse a, par un jugement du 25 mars 2011, rejeté la demande présentée par Mme B...tendant à l'annulation de ces décisions ; que, par un arrêt du 31 mai 2012 contre lequel le ministre de l'intérieur se pourvoit en cassation, la cour administrative d'appel de Bordeaux a annulé ce jugement ainsi que l'arrêté du 19 juillet 2010 et a enjoint au préfet de la Haute-Garonne de réexaminer la situation de Mme B...dans un délai de deux mois à compter de sa décision ; <br/>
<br/>
              2. Considérant, d'une part, qu'aux termes du premier alinéa de l'article           L. 312-2 du code de l'entrée et du séjour des étrangers et du droit d'asile, la commission du titre de séjour " est saisie par l'autorité administrative lorsque celle-ci envisage de refuser de délivrer ou de renouveler une carte de séjour temporaire à un étranger mentionné à l'article L. 313-11 ou de délivrer une carte de résident à un étranger mentionné aux articles L. 314-11 et L. 314-12, ainsi que dans le cas prévu à l'article L. 431-3 " ; qu'aux termes du premier alinéa de l'article  R. 312-2 de ce code : " Le préfet ou, à Paris, le préfet de police saisit pour avis la commission lorsqu'il envisage de refuser de délivrer ou de renouveler l'un des titres mentionnés aux articles L. 313-11, L. 314-11 et L. 314-12 à l'étranger qui remplit effectivement les conditions qui président à leur délivrance " ; <br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que le préfet n'est tenu de saisir la commission du titre de séjour, lorsqu'il envisage de refuser un titre mentionné à l'article        L. 312-2, que du cas des étrangers qui remplissent effectivement l'ensemble des conditions de procédure et de fond auxquelles est subordonnée la délivrance d'un tel titre, et non de celui de tous les étrangers qui se prévalent des articles auxquels les dispositions de l'article L. 312-2 ci-dessus renvoient ; <br/>
<br/>
              4. Considérant, d'autre part, qu'aux termes de l'article L. 313-11 du même code, dans sa rédaction applicable au présent litige : " Sauf si sa présence constitue une menace pour l'ordre public, la carte de séjour temporaire portant la mention " vie privée et familiale " est délivrée de plein droit : /(...) 4° A l'étranger ne vivant pas en état de polygamie, marié avec un ressortissant de nationalité française, à condition que la communauté de vie n'ait pas cessé depuis le mariage, que le conjoint ait conservé la nationalité française et, lorsque le mariage a été célébré à l'étranger, qu'il ait été transcrit préalablement sur les registres de l'état civil français ; (...) " ; qu'aux termes de l'article L. 311-7 de ce code : " Sous réserve des engagements internationaux de la France et des exceptions prévues par les dispositions législatives du présent code, l'octroi de la carte de séjour temporaire et celui de la carte de séjour " compétences et talents " sont subordonnés à la production par l'étranger d'un visa pour un séjour d'une durée supérieure à trois mois " ; qu'aux termes du 6ème alinéa de l'article        L. 211-2-1 du code : " Lorsque la demande de visa de long séjour émane d'un étranger entré régulièrement en France, marié en France avec un ressortissant de nationalité française et que le demandeur séjourne en France depuis plus de six mois avec son conjoint, la demande de visa de long séjour est présentée à l'autorité administrative compétente pour la délivrance d'un titre de séjour " ; <br/>
<br/>
              5. Considérant qu'il résulte de la combinaison de ces dispositions que la production d'un visa de long séjour délivré, le cas échéant, selon les modalités fixées au 6ème alinéa de l'article L. 211-2-1, est au nombre des conditions auxquelles est subordonnée la délivrance d'une carte de séjour temporaire sur le fondement du 4° de l'article L. 313-11 ; que, dès lors, ainsi qu'il a été dit au point 3, le préfet peut refuser une telle carte de séjour en se fondant sur le défaut de production par l'étranger d'un visa de long séjour sans avoir à saisir au préalable la commission du titre de séjour ;<br/>
<br/>
              6. Considérant que, pour annuler l'arrêté litigieux, la cour a relevé que, alors même que l'intéressée ne justifiait pas du visa de long séjour dans un cas où il est exigé, elle remplissait les conditions prescrites par le 4° de l'article L. 313-11 pour prétendre à la délivrance de plein droit d'une carte de séjour temporaire et que le préfet était par suite légalement tenu, avant d'opposer un refus de séjour, de consulter la commission du titre de séjour ; qu'en se fondant sur ce motif pour annuler l'arrêté litigieux, la cour administrative d'appel de Bordeaux a entaché son arrêt d'une erreur de droit ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, que le ministre de l'intérieur est fondé à demander l'annulation de l'arrêt qu'il attaque ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 31 mai 2012 de la cour administrative d'appel de Bordeaux est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à Mme A...D..., épouseB....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-01-03-02 ÉTRANGERS. SÉJOUR DES ÉTRANGERS. REFUS DE SÉJOUR. PROCÉDURE. - CONSULTATION OBLIGATOIRE DE LA COMMISSION DU TITRE DE SÉJOUR - CHAMP - REFUS D'UN TITRE DE SÉJOUR MENTIONNÉ À L'ARTICLE L. 312-2 DU CESEDA À UN ÉTRANGER REMPLISSANT EFFECTIVEMENT LES CONDITIONS DE PROCÉDURE ET DE FOND POUR L'OBTENIR - ETRANGER SOLLICITANT UNE CARTE DE SÉJOUR TEMPORAIRE (4° DE L'ART. L. 313-11) SANS PRODUIRE DE VISA DE LONG SÉJOUR - EXCLUSION [RJ1].
</SCT>
<ANA ID="9A"> 335-01-03-02 Le préfet n'est tenu de saisir la commission du titre de séjour, lorsqu'il envisage de refuser un titre de séjour mentionné à l'article L. 312-2 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), que du cas des étrangers qui remplissent effectivement l'ensemble des conditions de procédure et de fond auxquelles est subordonnée la délivrance d'un tel titre.,,,La production d'un visa de long séjour délivré, le cas échéant, selon les modalités fixées au sixième alinéa de l'article L. 211-2-1 du CESEDA, est au nombre des conditions auxquelles est subordonnée la délivrance d'une carte de séjour temporaire sur le fondement du 4° de l'article L. 313-11 du même code.,,,Dès lors, le préfet peut refuser une telle carte de séjour en se fondant sur le défaut de production par l'étranger d'un visa de long séjour sans avoir à saisir au préalable la commission du titre de séjour.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 27 mai 1994, Oncul, n° 118879, p. 268 ; CE, 29 décembre 1995, Ghazouani, n° 140023, p. 474.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
