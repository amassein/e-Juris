<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042671492</ID>
<ANCIEN_ID>JG_L_2020_12_000000446997</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/67/14/CETATEXT000042671492.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 07/12/2020, 446997, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>446997</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:446997.20201207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 30 novembre 2022 au secrétariat du contentieux du Conseil d'Etat, Mme B... A... demande au juge des référés, statuant sur le fondement de l'article L. 521-2 du code de justice administrative de suspendre l'exécution de l'article 1er du décret n° 2020-1454 du 27 novembre 2020 modifiant le décret n° 2020-1310 du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire. <br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - la condition d'urgence est satisfaite dès lors que les dispositions contestées préjudicient de manière grave et immédiate à sa situation en ce qu'elles portent atteinte à sa liberté d'aller et venir ainsi qu'à celle de sa famille ; <br/>
              - les dispositions contestées portent une atteinte grave et manifestement illégale à la liberté d'aller et venir, au droit de mener une vie familiale normale, au principe de légalité des délits et des peines et au principe de personnalité des sanctions pénales ; <br/>
              - la mesure de confinement constitue une anticipation illégale du non-respect des gestes barrières conduisant à sanctionner l'ensemble de la population et occulte la défaillance des autorités publiques à faire respecter les règles de distanciation sociale et les gestes barrières ; <br/>
              - les dispositions contestées sont entachées d'illégalité manifeste dès lors que le taux de mortalité réelle de l'épidémie est incertain, que les autorités publiques ont failli à faire respecter les règles de distanciation physique et les gestes barrières au sein des établissements recevant du public, que le système de santé français est en mesure d'absorber l'augmentation potentielle du nombre de malades et que le confinement constitue en réalité une sanction non pénale du non-respect potentiel des gestes barrières ;<br/>
              - elles méconnaissent le principe de légalité des délits et des peines dès lors que la répression de la seule intention délictuelle ou criminelle est interdite ;<br/>
              - elles méconnaissent le principe d'égalité des citoyens devant la loi dès lors que les exceptions prévues au confinement, qui ne sont pas fondées sur des critères précis et objectifs, sont discriminatoires.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le décret n° 2020-1310 du 29 octobre 2020 modifié par le décret n° 2020-1454 du 27 novembre 2020 ;<br/>
              -  le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. <br/>
<br/>
              2. La circonstance qu'une atteinte à une liberté fondamentale, portée par une mesure administrative, serait avérée n'est pas de nature à caractériser l'existence d'une situation d'urgence justifiant l'intervention du juge des référés dans le très bref délai prévu par les dispositions de l'article L. 521-2 du code de justice administrative. Il appartient au juge des référés d'apprécier, au vu des éléments que lui soumet le requérant comme de l'ensemble des circonstances de l'espèce, si la condition d'urgence particulière requise par l'article L. 521-2 est satisfaite, en prenant en compte la situation du requérant et les intérêts qu'il entend défendre mais aussi l'intérêt public qui s'attache à l'exécution des mesures prises par l'administration. <br/>
<br/>
              3. Mme A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, de suspendre l'exécution de l'article 1er du décret du 27 novembre 2020 qui assouplit la mesure de confinement prévue par l'article 4 du décret du 29 octobre 2020. <br/>
<br/>
              4. Pour justifier de l'urgence à ordonner la suspension demandée, Mme A... se borne à soutenir que la mesure de confinement prise pour faire face à l'épidémie de covid-19 porte atteinte à sa liberté d'aller et venir ainsi qu'à celle de sa famille. <br/>
<br/>
              5. Eu égard, d'une part, à l'assouplissement du confinement, prévu par l'article 1er du décret contesté, compte tenu de l'amélioration de la situation sanitaire constatée ces dernières semaines, d'autre part, à l'intérêt public qui s'attache à l'exécution de cette mesure dans un contexte où les indicateurs sanitaires, certes en diminution constante après le passage du pic épidémique de la seconde vague de covid-19, restent à un niveau élevé témoignant ainsi d'une circulation virale encore importante sur le territoire national, la condition d'urgence particulière requise par l'article L. 521-2 du code de justice administrative n'est pas satisfaite. <br/>
<br/>
              6. Il résulte de ce qui précède que la requête de Mme A... doit être rejetée, selon la procédure prévue à l'article L. 522-3 du code de justice administrative. <br/>
<br/>
<br/>
<br/>			O R D O N N E :<br/>
              			------------------<br/>
<br/>
Article 1er : La requête de Mme A... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme B... A.... <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
