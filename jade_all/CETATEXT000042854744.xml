<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042854744</ID>
<ANCIEN_ID>JG_L_2020_12_000000438787</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/85/47/CETATEXT000042854744.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 31/12/2020, 438787, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438787</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; LE PRADO ; SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:438787.20201231</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme D... C... et M. A... B... ont demandé au tribunal administratif de Lyon, à titre principal, de condamner les Hospices civils de Lyon (HCL) et leur assureur, la société hospitalière d'assurances mutuelles (SHAM) à les indemniser des préjudices consécutifs à l'opération subie par Mme C... le 12 novembre 2007 à l'hôpital cardiologique Louis-Pradel ou, à titre subsidiaire, de condamner l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) à prendre les préjudices de Mme C... en charge sur le fondement du II de l'article L. 1142-1 du code de la santé publique. Par un jugement n° 1500088 du 6 juin 2017, le tribunal administratif de Lyon a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 17LY03055 du 19 décembre 2019, la cour administrative d'appel de Lyon a, sur appel de Mme C... et de M. B..., annulé le jugement du tribunal administratif de Lyon et condamné les HCL et la SHAM à verser solidairement à Mme C... et M. B... la somme globale de 23 110 euros ainsi que la somme de 1 762,50 euros à l'Etat.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 février et 11 mai 2020 au secrétariat du contentieux du Conseil d'Etat, Mme C... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il rejette le surplus de ses conclusions ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge des Hospices civils de Lyon la somme de 4 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de Mme C..., à Me Le Prado, avocat des Hospices civils de Lyon et de la société hospitalière d'assurances mutuelles et à la SCP Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales.<br/>
<br/>
              Mme C... a présenté une note en délibéré, enregistrée le 18 décembre 2020 ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme C..., atteinte du syndrome de Marfan, a subi une opération de l'aorte, destinée à prévenir une dissection ultérieure, le 12 novembre 2007 aux Hospices civils de Lyon (HCL). Une lésion accidentelle du muscle cardiaque au cours de cette intervention a toutefois porté son insuffisance cardiaque de 60 % à 80%. Elle a demandé au tribunal administratif de Lyon de condamner les HCL et la société hospitalière d'assurances mutuelles (SHAM) ou, subsidiairement, l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) à réparer ce préjudice. Par un jugement du 6 juin 2017, le tribunal administratif a rejeté sa demande. Par un arrêt du 19 décembre 2019, la cour administrative d'appel de Lyon a, sur son appel, annulé ce jugement et retenu que la responsabilité des HCL était engagée au titre d'un défaut d'information lui ayant fait perdre 15% de chance de se soustraire à l'opération. Elle a, en conséquence, condamné les HCL et la SHAM à lui verser diverses sommes en réparation de ses préjudices. Mme C... demande l'annulation de cet arrêt en tant qu'il rejette le surplus de ses conclusions. <br/>
<br/>
              Sur l'arrêt en tant qu'il fixe les préjudices mis à la charge des Hospices civils de Lyon au titre du défaut d'information préalable à l'opération du 12 novembre 2007 :<br/>
<br/>
              2. En premier lieu, le moyen tiré de ce que la cour administrative d'appel aurait commis des erreurs matérielles et entaché son arrêt de contradiction de motifs dans le calcul du montant total des sommes que les Hospices civils de Lyon a été condamné à lui verser n'est pas assorti des précisions permettant d'en apprécier le bien-fondé. <br/>
<br/>
              3. En deuxième lieu, en jugeant que le remplacement de la baignoire de Mme C... par une douche ne présentait pas un lien suffisamment direct avec l'aggravation de son insuffisance cardiaque, la cour administrative d'appel n'a pas dénaturé les pièces du dossier qui lui était soumis et n'a pas inexactement qualifié les faits.<br/>
<br/>
              4. En troisième lieu, en estimant que Mme C... n'établissait pas la nécessité de l'assistance d'une tierce personne postérieurement à la date de consolidation de son état de santé, fixée au 12 novembre 2010, la cour n'a pas dénaturé les pièces du dossier qui lui était soumis.<br/>
<br/>
              5. En revanche, s'agissant de ce même besoin d'assistance pour la période antérieure à la date de consolidation du 12 novembre 2010, Mme C... est fondée a soutenir que la cour a commis une erreur de droit en en déduisant la majoration pour tierce-personne de sa pension civile d'invalidité, qui avait été concédée pour une période débutant le 14 septembre 2015. L'arrêt attaqué doit donc être annulé dans cette mesure.<br/>
<br/>
              6. En quatrième lieu, en estimant que les diverses évaluations immobilières présentées par Mme C... ne permettaient pas d'établir l'existence d'un préjudice relatif à un surcoût de logement adapté, la cour s'est livrée à une appréciation souveraine des pièces du dossier, exempte de dénaturation. En rejetant pour ce motif les conclusions présentées au titre de ce poste de préjudice, elle n'a pas commis d'erreur de droit.<br/>
<br/>
              7. Enfin, toutefois, en estimant, pour juger que la faute des Hospices civils de Lyon n'ouvrait à Mme C... aucun droit à l'indemnisation de ses préjudices professionnels, qu'elle présentait dès septembre 2007 une asthénie et une dyspnée liées à son insuffisance mitrale et qu'ainsi, les préjudices professionnels invoqués devaient être regardés comme sans lien avec l'intervention du 12 novembre 2007, alors qu'il ressortait du dossier qui lui était soumis qu'aucun élément ne permettait de présumer que l'opération, dont le résultat n'a d'ailleurs pas empêché Mme C... de recouvrer une large autonomie dans les gestes du quotidien, ne permettrait pas une récupération suffisante pour que cette patiente, alors âgée de 47 ans, retrouve son activité professionnelle antérieure de téléopérateur, la cour a dénaturé les pièces du dossier qui lui était soumis. Son arrêt doit donc être également attaqué dans cette mesure.<br/>
<br/>
              Sur l'arrêt en tant qu'il se prononce sur l'existence d'une faute commise lors de l'opération du 12 novembre 2007 :<br/>
<br/>
              8. En estimant, ainsi qu'il résulte des termes mêmes de son arrêt, que la dilatation évolutive de l'aorte de Mme C..., ainsi que les risques élevés liés à toute réintervention à moyen terme sur l'aorte après chirurgie première de la valve mitrale, justifiaient la décision de pratiquer une réparation combinée mitro-aortique, la cour administrative d'appel a porté sur les pièces du dossier qui lui était soumis une appréciation souveraine, exempte de dénaturation. En en déduisant que les HCL n'avaient pas commis de faute en procédant à cette opération, elle a exactement qualifié les faits.<br/>
<br/>
              Sur l'arrêt en tant qu'il se prononce, à titre subsidiaire, sur l'engagement de la solidarité nationale :<br/>
<br/>
              9. La condition d'anormalité du dommage prévue par les dispositions de l'article L. 1142-1 du code de la santé publique doit toujours être regardée comme remplie lorsque les conséquences de l'acte médical ne sont pas notablement plus graves que celles auxquelles le patient était exposé par sa pathologie en l'absence de traitement, mais que la survenance du dommage présentait une probabilité faible.<br/>
<br/>
              10. Il résulte des termes mêmes de l'arrêt attaqué que, pour juger que la condition d'anormalité du dommage n'était pas remplie, la cour administrative d'appel a retenu que le risque qui s'est réalisé présentait une fréquence évaluée en principe à 0,2%, mais que la pathologie présentée par Mme C... l'avait porté à une fréquence " largement " supérieure. En se fondant ainsi sur une simple majoration, non évaluée, d'un taux de risque de 0,2% pour en déduire que le critère de faible probabilité du risque mentionné ci-dessus n'était pas rempli, elle a inexactement qualifié les faits de l'espèce. <br/>
<br/>
              11. Il résulte de tout ce qui précède qu'il y a lieu d'annuler l'arrêt de la cour administrative d'appel de Lyon en tant seulement que, s'agissant des conséquences du défaut d'information préalable à l'intervention du 12 novembre 2007, il se prononce sur le préjudice de tierce personne avant consolidation et sur les préjudices professionnels et en tant que, s'agissant des conséquences de l'intervention, il statue sur l'engagement de la solidarité nationale.<br/>
<br/>
              12. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge des HCL une somme de 3 000 euros à verser à Mme C... au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce que soit mise à la charge de Mme C..., qui n'est pas, dans la présente instance, la partie perdante, la somme que demande l'ONIAM au même titre.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 19 décembre 2019 de la cour administrative d'appel de Lyon est annulé en tant que, s'agissant des conséquences du défaut d'information préalable à l'intervention du 12 novembre 2007, il se prononce sur le préjudice de tierce personne avant consolidation et sur les préjudices professionnels et en tant que, s'agissant des conséquences de l'intervention, il statue sur l'engagement de la solidarité nationale.<br/>
<br/>
Article 2 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Lyon. <br/>
<br/>
Article 3 : Les Hospices civils de Lyon verseront une somme de 3 000 euros à Mme C... au titre de l'article L. 761-1 du code de justice administrative. Les conclusions présentées par l'ONIAM au même titre sont rejetées.<br/>
<br/>
      Article 4 : Le surplus des conclusions des parties est rejeté.<br/>
<br/>
Article 5 : Le présent arrêt sera notifié à Mme D... C..., aux Hospices civils de Lyon, à la société hospitalière d'assurances mutuelles et à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
