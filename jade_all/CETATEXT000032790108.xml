<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032790108</ID>
<ANCIEN_ID>JG_L_2016_06_000000386957</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/79/01/CETATEXT000032790108.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 27/06/2016, 386957</TITRE>
<DATE_DEC>2016-06-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386957</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Marc Lambron</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:386957.20160627</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Dijon de condamner le centre hospitalier Henri Dunant à lui verser la somme de 46 714, 34 euros à titre de dommages et intérêts pour privation d'indemnités journalières du 15 juillet 2009 au 28 février 2012, la somme de 46 714, 34 euros à titre de dommages et intérêts pour perte de traitements sur la même période ainsi que la somme de 15 000 euros à titre de dommages et intérêts pour troubles dans les conditions d'existence, en réparation des préjudices qu'elle estime avoir subis du fait de l'illégalité de la décision du 14 avril 2010 du directeur de ce centre hospitalier l'ayant placée en disponibilité d'office à compter du 15 juillet 2009. Par un jugement n° 1202207 du 11 avril 2013, le tribunal administratif a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 13LY01393 du 4 novembre 2014, la cour administrative d'appel de Lyon a rejeté la requête de Mme B...contre ce jugement.<br/>
<br/>
              Par un pourvoi et un mémoire complémentaire, enregistrés les 6 janvier et 3 avril 2015 au secrétariat du contentieux du Conseil d'Etat, Mme A...B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier Henri Dunant une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la loi n° 86-33 du 9 janvier 1986 ;<br/>
<br/>
              - l'arrêté du 4 août 2004 relatif aux commissions de réforme des agents de la fonction publique territoriale et de la fonction publique hospitalière ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Lambron, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de MmeB... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que MmeB..., recrutée en 1980 par le centre hospitalier Henri Dunant en qualité de masseur kinésithérapeuthe, et ayant souffert de diverses pathologies depuis l'année 2003, a été placée en position de disponibilité d'office, à compter du 15 juillet 2009, par une décision du 14 avril 2010 du directeur de cet établissement ; que cette décision a été annulée par un jugement du 28 février 2012 du tribunal administratif de Dijon, devenu définitif, au motif que la procédure suivie devant la commission de réforme n'avait pas été régulière ; que Mme B...a formé, le 12 juillet 2012, une demande auprès du centre hospitalier afin que celui-ci l'indemnise des pertes de traitement et d'indemnités journalières qu'elle estime avoir subies entre le 15 juillet 2009 et le 28 février 2012,  évaluées à un montant total de 93 428,68 euros, ainsi que des troubles de toute nature dans ses conditions d'existence causés par cette privation de revenus, évalués à 15 000 euros ; que cette demande ayant été implicitement rejetée, Mme B...a saisi le tribunal administratif de Dijon d'une requête tendant à ce que le centre hospitalier soit condamné à lui verser les indemnités demandées, laquelle a été rejetée par un jugement du 11 avril 2013 ; que, par un arrêt du 4 novembre 2014 contre lequel Mme B...se pourvoit en cassation, la cour administrative d'appel de Lyon a rejeté son appel contre ce jugement ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, pour justifier des pertes de traitements qu'elle estime avoir subies, Mme B...a fait valoir, pour la première fois en appel, dans un mémoire enregistré le 1er août 2014, qu'elle avait droit au maintien de son plein traitement jusqu'à l'adoption de la décision définitive prise à l'issue de la procédure ayant justifié la saisine de la commission de réforme, en vertu des dispositions de la dernière phrase de l'article 13 de l'arrêté du 4 août 2004 relatif aux commissions de réforme des agents de la fonction publique territoriale et de la fonction publique hospitalière, aux termes desquelles : " Le traitement auquel l'agent avait droit, avant épuisement des délais en cours à la date de saisie de la commission de réforme, lui est maintenu durant les délais mentionnés et en tout état de cause jusqu'à l'issue de la procédure justifiant la saisie de la commission de réforme " ; que la cour a rejeté la requête de Mme B... sans répondre à ce moyen, qui n'était pas inopérant ; que, dès lors, elle a insuffisamment motivé son arrêt ; que celui-ci doit être annulé pour ce motif, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier Henri Dunant une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 4 novembre 2014 de la cour administrative d'appel de Lyon est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
<br/>
Article 3 : Le centre hospitalier Henri Dunant versera à Mme B...une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme A...B...et au centre hospitalier Henri Dunant.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-08-02-02-005-03-01 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. RÉGULARITÉ EXTERNE. FORME. MOTIVATION. - INSUFFISANCE DE MOTIVATION - ABSENCE DE RÉPONSE À UN MOYEN IRRECEVABLE QUI N'EST PAS INOPÉRANT - EXISTENCE (SOL. IMPL.) [RJ1].
</SCT>
<ANA ID="9A"> 54-08-02-02-005-03-01 Une cour qui s'abstient de répondre à un moyen qui n'est pas inopérant motive insuffisamment son arrêt, y compris si le moyen en cause est irrecevable.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., s'agissant d'un moyen inopérant, CE, Section, 25 mars 1960, Sieur,, n° 35805, p. 234 ; CE, Section, 10 juillet 1964, Sieur,, n° 4953, p. 397 ; CE, 27 mai 1994, M.,, n° 121819, p. 266, aux Tables sur d'autres points.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
