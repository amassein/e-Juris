<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042844848</ID>
<ANCIEN_ID>JG_L_2020_12_000000411547</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/84/48/CETATEXT000042844848.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 30/12/2020, 411547, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411547</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD</AVOCATS>
<RAPPORTEUR>M. Mathieu  Le Coq</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:411547.20201230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société Saupiquet a demandé au tribunal administratif de Rennes d'annuler le titre de perception émis le 18 février 2013 en vue du recouvrement d'une somme de 165 369,37 euros. <br/>
<br/>
              Par un jugement n° 1400492 du 25 juin 2015, le tribunal administratif a annulé ce titre de perception.<br/>
<br/>
              Par un arrêt n° 15NT02618 du 14 avril 2017, la cour administrative d'appel de Nantes a rejeté l'appel formé par le ministre de l'environnement, de l'énergie et de la mer, chargé des relations internationales sur le climat, contre ce jugement.<br/>
<br/>
              Par un pourvoi, enregistré le 15 juin 2017 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'agriculture et de l'alimentation demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - l'arrêt C-549/09 du 20 octobre 2011 de la Cour de justice de l'Union européenne ;<br/>
              - l'arrêt C-212/19 du 17 septembre 2020 de la Cour de justice de l'Union européenne ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Mathieu Le Coq, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, au Cabinet Briard, avocat de la société Saupiquet ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision n° 2005/239/CE du 14 juillet 2004, concernant certaines mesures d'aide mises à exécution par la France en faveur des aquaculteurs et des pêcheurs, la Commission européenne a déclaré incompatibles avec le marché commun les aides accordées par la République française sous forme d'allégements de charges sociales entre le 15 avril et le 15 octobre 2000 aux pêcheurs, afin de remédier aux dommages causés par le naufrage du navire Erika le 12 décembre 1999 et par la tempête des 27 et 28 décembre 1999. Elle a ordonné la récupération immédiate et effective de ces aides. Par un arrêt Commission c/ France du 20 octobre 2011 (C-549/09), la Cour de justice de l'Union européenne a constaté que la France avait manqué à ses obligations en ne récupérant pas auprès des bénéficiaires les aides déclarées illégales et incompatibles avec le marché commun par la décision du 14 juillet 2004. Pour l'exécution de la décision de la Commission, un titre de perception a été émis le 18 février 2013 à l'encontre de la société Saupiquet en vue de la récupération du montant des allègements de cotisations salariales consenties entre le 15 avril et le 15 octobre 2000, assorti des intérêts de retard. Par un jugement du 25 juin 2015, le tribunal administratif de Rennes a annulé le titre de perception attaqué. Par un arrêt du 14 avril 2017, la cour administrative d'appel de Nantes a rejeté l'appel formé par le ministre de l'environnement, de l'énergie et de la mer contre ces jugements. Le ministre de l'agriculture et de l'alimentation se pourvoit contre cet arrêt.<br/>
<br/>
              2. Pour rejeter l'appel du ministre, la cour a jugé que les entreprises de pêche, chargées de précompter sur les salaires de ses employés les cotisations salariales dont ils sont redevables, n'étaient pas les bénéficiaires des allègements de cotisations salariales qui ont eu pour effet d'augmenter le salaire net versé aux salariés et que la décision 2005/239/CE de la Commission n'imposait pas que les entreprises ayant bénéficié d'une réduction de leurs cotisations patronales soient également contraintes de rembourser la part salariale de cet allègement.<br/>
<br/>
              3. Par un arrêt Compagnie des Pêches de Saint-Malo du 17 septembre 2020 (C-212/19) par lequel elle s'est prononcée sur les questions dont le Conseil d'Etat l'avait saisie à titre préjudiciel, la Cour de justice de l'Union européenne a dit pour droit que la décision 2005/239/CE de la Commission est invalide en tant qu'elle qualifie d'aide d'Etat incompatible avec le marché commun l'allègement de cotisations salariales aux pêcheurs pour la période du 15 avril au 15 octobre 2000 et en exige la récupération. Elle a jugé que la Commission avait commis une erreur de droit en retenant que les allègements de cotisations salariales en cause étaient des mesures procurant un avantage aux entreprises de pêche en ce qu'elles auraient été dispensées de certaines charges qu'elles auraient normalement dû supporter alors que, les cotisations salariales n'étant pas supportées par les entreprises en leur qualité d'employeur, mais étant à la charge des salariés, ces derniers sont les bénéficiaires effectifs de ces allègements.<br/>
<br/>
              4. En raison des effets qui s'y attachent, l'annulation pour excès de pouvoir d'un acte administratif, qu'il soit ou non réglementaire, emporte, lorsque le juge est saisi de conclusions recevables, l'annulation par voie de conséquence des décisions administratives consécutives qui n'auraient pu légalement être prises en l'absence de l'acte annulé ou qui sont en l'espèce intervenues en raison de l'acte annulé. Il en va ainsi, notamment, des décisions qui ont été prises en application de l'acte annulé et de celles dont l'acte annulé constitue la base légale. Il incombe au juge administratif, lorsqu'il est saisi de conclusions recevables dirigées contre de telles décisions consécutives, de prononcer leur annulation par voie de conséquence, le cas échéant en relevant d'office un tel moyen qui découle de l'autorité absolue de chose jugée qui s'attache à l'annulation du premier acte. Lorsque les juridictions de l'Union ont annulé ou déclaré invalide un acte émanant des institutions et organes de l'Union, ces règles sont applicables devant le juge administratif saisi de conclusions recevables dirigées contre les décisions administratives consécutives prises par les autorités nationales. <br/>
<br/>
              5. La déclaration d'invalidité de la décision 2005/239/CE de la Commission, prononcée par la Cour de justice de l'Union européenne dans l'arrêt du 17 septembre 2020, en tant qu'elle qualifie d'aide d'Etat incompatible avec le marché commun l'allègement de cotisations salariales accordé par la France aux pêcheurs pour la période du 15 avril au 15 octobre 2000 et en exige la récupération, emporte, par voie de conséquence, l'illégalité du titre de perception émis le 18 février 2013 pour l'exécution de cette décision et tendant à la restitution, par la société Saupiquet, du montant des allègements de cotisations salariales pendant cette période, assorti des intérêts de retard. Ce motif, qui est d'ordre public et dont l'examen n'implique l'appréciation d'aucune circonstance de fait, doit être substitué aux motifs retenus par l'arrêt attaqué dont il justifie le dispositif. Par suite, le pourvoi du ministre de l'agriculture et de l'alimentation doit être rejeté.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à la société Saupiquet au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
 Article 1er : Le pourvoi du ministre de l'agriculture et de l'alimentation est rejeté.<br/>
<br/>
 Article 2 : L'Etat versera une somme de 3 000 euros à la société Saupiquet au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
 Article 3 : La présente décision sera notifiée au ministre de l'agriculture et de l'alimentation et à la société Saupiquet. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
