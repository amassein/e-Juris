<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039351217</ID>
<ANCIEN_ID>JG_L_2019_11_000000416076</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/35/12/CETATEXT000039351217.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 07/11/2019, 416076, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-11-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416076</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Sylvain Monteillet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:416076.20191107</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et trois mémoires complémentaires, enregistrés le 28 novembre 2017, les 27 février et 19 septembre 2018 et le 1er février 2019 au secrétariat du contentieux du Conseil d'Etat, la société de viticulture du Jura demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêté du ministre de l'agriculture et de l'alimentation, du ministre de l'économie et des finances et du ministre de l'action et des comptes publics du 28 septembre 2017 homologuant le cahier des charges de l'appellation d'origine contrôlée (AOC) " Corrèze " ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le règlement (CE) n° 607/2009 de la Commission du 14 juillet 2009 ;<br/>
              - le règlement (UE) n° 1308/2013 du Parlement européen et du Conseil du 17 décembre 2013 ; <br/>
              - le code de la consommation ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - le décret n° 2017-775 du 4 mai 2017 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 21 octobre 2019, présentée par le président de l'INAO.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sylvain Monteillet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la société Viticulture du Jura et à la SCP Didier, Pinet, avocat de l'Institut national de l'origine et de la qualité (INAO) ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 641-6 du code rural et de la pêche maritime : " La reconnaissance d'une appellation d'origine contrôlée est proposée par l'Institut national de l'origine et de la qualité (...) ". Aux termes de l'article L. 641-7 du même code : " La reconnaissance d'une appellation d'origine contrôlée est prononcée par un arrêté du ou des ministres intéressés qui homologue un cahier des charges où figurent notamment la délimitation de l'aire géographique de production de cette appellation ainsi que ses conditions de production (...) ".<br/>
<br/>
              2. Par un arrêté du 28 septembre 2017, le ministre de l'agriculture et de l'alimentation, le ministre de l'économie et des finances et le ministre de l'action et des comptes publics ont homologué le cahier des charges de l'appellation d'origine contrôlée (AOC) " Corrèze ". La société de viticulture du Jura demande au Conseil d'Etat l'annulation pour excès de pouvoir de cet arrêté. <br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              3. Aux termes de l'article L. 642-5 du code rural et de la pêche maritime : " L'Institut national de l'origine et de la qualité, dénommé " INAO ", est un établissement public administratif de l'Etat chargé de la mise en oeuvre des dispositions législatives et réglementaires relatives aux signes d'identification de la qualité et de l'origine énumérés au 1° de l'article L. 640-2. / A ce titre, l'Institut, notamment : / 1° Propose la reconnaissance des produits susceptibles de bénéficier des signes d'identification de la qualité et de l'origine et la révision de leurs cahiers des charges [...] ". Aux termes de l'article L. 642-6 du même code : " L'Institut national de l'origine et de la qualité comprend (...) des comités nationaux spécialisés dans les différentes catégories de produits valorisés ou les différents signes d'identification de la qualité et de l'origine (...) ". Aux termes de l'article L. 642-9 du même code : " Chacun des comités nationaux exerce notamment les compétences dévolues à l'Institut national de l'origine et de la qualité par les 1° (...) de l'article L. 642-5 pour les produits et les signes qui sont de sa compétence ". Aux termes de l'article R. 642-6 du même code : " L'Institut national de l'origine et de la qualité comprend les cinq comités suivants : / 1° Le comité national des appellations d'origine relatives aux vins et aux boissons alcoolisées, et des boissons spiritueuses (...) ".<br/>
<br/>
              4. Il ressort des pièces du dossier que le comité national des appellations d'origine relatives aux vins et boissons alcoolisées et des eaux-de-vie a proposé la reconnaissance de l'AOC " Corrèze " par une délibération du 3 mai 2017. Si, après cette délibération et avant la publication de l'arrêté attaqué, les mots " eaux-de-vie " figurant dans la dénomination de ce comité ont été remplacés, par l'article 3 du décret du 4 mai 2017 relatif à la valorisation des produits agricoles, forestiers ou alimentaires et des produits de la mer, par les mots " boissons spiritueuses ", ce changement de dénomination, qui répond à un souci d'harmoniser les termes employés avec le droit de l'Union européenne, n'a modifié ni les compétences ni la composition de ce comité national. Dans ces conditions, le moyen tiré de ce que l'arrêté attaqué a été pris à l'issue d'une procédure irrégulière, faute d'avoir été précédé de la consultation du comité compétent, doit être écarté.<br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              5. Aux termes de l'article 93 du règlement (UE) n° 1308/2013 du Parlement européen et du Conseil du 17 décembre 2013 portant organisation commune des marchés des produits agricoles : " 1. Aux fins de la présente section, on entend par : / a) "appellation d'origine", le nom d'une région, d'un lieu déterminé ou, dans des cas exceptionnels et dûment justifiés, d'un pays, qui sert à désigner un produit visé à l'article 92, paragraphe 1, satisfaisant aux exigences suivantes : / i) sa qualité et ses caractéristiques sont dues essentiellement ou exclusivement à un milieu géographique particulier et aux facteurs naturels et humains qui lui sont inhérents (...) ".  Aux termes du paragraphe 2 de l'article 94 de ce même règlement : " Le cahier des charges permet aux parties intéressées de vérifier le respect des conditions de production associées à l'appellation d'origine ou à l'indication géographique. / Le cahier des charges comporte au minimum les éléments suivants : / (...) les éléments qui corroborent le lien visé à l'article 93, paragraphe 1, point a) i) (...) ". Aux termes de l'article 7 du règlement (CE) n° 607/2009 de la Commission du 14 juillet 2009 fixant certaines modalités d'application du règlement (CE) n° 479/2008 du Conseil en ce qui concerne les appellations d'origine protégées et les indications géographiques protégées, les mentions traditionnelles, l'étiquetage et la présentation de certains produits du secteur vitivinicole : " 1.  Les éléments qui corroborent le lien géographique visé à l'article 35, paragraphe 2, point g), du règlement (CE) no 479/2008 expliquent dans quelle mesure les caractéristiques de la zone géographique délimitée influent sur le produit final. / Dans le cas de demandes couvrant différentes catégories de produits de la vigne, les éléments corroborant le lien sont démontrés pour chacun des produits de la vigne concernée. / 2.  Pour une appellation d'origine, le cahier des charges contient : / a) des informations détaillées sur la zone géographique, notamment les facteurs naturels et humains, contribuant au lien ; / b) des informations détaillées sur la qualité ou les caractéristiques du produit découlant essentiellement ou exclusivement du milieu géographique ; / c) une description de l'interaction causale entre les éléments visés au point a) et ceux visés au point b) ".<br/>
<br/>
              6. Aux termes de l'article L. 641-5 du code rural et de la pêche maritime : " Peuvent bénéficier d'une appellation d'origine contrôlée les produits agricoles, forestiers ou alimentaires et les produits de la mer, bruts ou transformés, qui remplissent les conditions fixées par les dispositions de l'article L. 115-1 du code de la consommation, possèdent une notoriété dûment établie et dont la production est soumise à des procédures comportant une habilitation des opérateurs, un contrôle des conditions de production et un contrôle des produits ". Aux termes de l'article L. 431-1 du code de la consommation : " Constitue une appellation d'origine la dénomination d'un pays, d'une région ou d'une localité servant à désigner un produit qui en est originaire et dont la qualité ou les caractères sont dus au milieu géographique, comprenant des facteurs naturels et des facteurs humains ". <br/>
<br/>
              7. Il résulte de ces dispositions que l'homologation d'un cahier des charges d'une appellation d'origine contrôlée, qui n'est pas une simple indication de provenance géographique, ne peut légalement intervenir que si ce cahier précise les éléments qui permettent d'attribuer à une origine géographique déterminée et aux facteurs naturels et humains qui lui sont inhérents la qualité, les caractéristiques particulières et la réputation du produit qui fait l'objet de l'appellation et s'il met en lumière de manière circonstanciée le lien géographique et l'interaction causale entre la zone géographique et la qualité, la réputation et les autres caractéristiques du produit. En outre, il découle nécessairement de ces dispositions qu'elles ne permettent de reconnaître un tel lien que pour une production existante, attestée dans l'aire géographique concernée à la date de l'homologation et depuis un temps suffisant pour établir ce lien. <br/>
<br/>
              8. En premier lieu, il ressort des pièces du dossier que le cahier des charges de l'AOC " Corrèze " homologué par l'arrêté attaqué comporte au X de son chapitre Ier, d'une part, une description des facteurs naturels, en particulier de la géomorphologie et du climat des zones concernées, ainsi que des développements, au titre des interactions causales, sur la qualité et les caractéristiques spécifiques des vins résultant de ce milieu géographique, notamment de l'exposition des terrains au sud de la montagne limousine et des conditions climatiques favorables à la pratique du séchage naturel des fruits. Ce cahier des charges comporte, d'autre part, outre le rappel des étapes historiques du développement de la vigne depuis l'époque gallo-romaine dans l'aire concernée et des techniques traditionnelles de fabrication du vin " paillé " dans cette zone, des indications relatives aux facteurs humains propres à la viticulture dans l'aire concernée, qui assurent les caractéristiques et la qualité décrites de l'appellation et qui  tiennent notamment aux choix des cépages adaptés au terroir local et à la conduite de la vigne, ainsi que, s'agissant du vin de paille, aux techniques spécifiques employées, qui sont détaillées dans les parties prescriptives du cahier des charges. Par suite, le moyen tiré de ce que l'arrêté attaqué serait entaché d'une erreur d'appréciation au motif que le contenu du cahier des charges ne préciserait pas suffisamment le lien entre, d'une part, la qualité et les caractéristiques de l'appellation et, d'autre part, le milieu géographique particulier ainsi que les facteurs naturels et humains qui lui sont inhérents doit être écarté. <br/>
<br/>
              9. En deuxième lieu, si la culture de la vigne dans la zone concernée a été presque totalement abandonnée après la crise du phylloxéra à la fin du XIXème siècle, il ressort des pièces du dossier que cette culture, relancée dans les années 1980, s'est depuis lors consolidée, le cahier des charges homologué par l'arrêté attaqué mentionnant ainsi la création d'un syndicat du vin paillé en 1993, de la cave des coteaux de la Vézère en 2003 et de la fédération des vins de la Corrèze en 2010. Ces vins ont fait l'objet d'une indication géographique protégée reconnue par un arrêté du 2 novembre 2011. C'est donc sans erreur d'appréciation que la condition d'une production existante, attestée dans l'aire géographique concernée à la date de l'homologation et depuis un temps suffisant pour établir le lien d'interaction causale requis, a été regardée comme satisfaite. A cet égard, le faible volume de la production et le nombre limité de viticulteurs ne font pas obstacle, par eux-mêmes, à la possibilité de bénéficier d'une AOC en vertu des dispositions mentionnées aux points 5 et 6 ci-dessus. Dès lors, le moyen tiré de ce que l'arrêté attaqué serait entaché d'une erreur d'appréciation en ce qu'il regarde la condition d'antériorité comme remplie pour les produits concernés par l'AOC " Corrèze " doit être écarté.<br/>
<br/>
              10. En troisième lieu, si le cahier des charges homologué par l'arrêté attaqué fait référence, dans sa partie relative aux interactions causales, au " bassin de Meyssac ", cette référence doit être regardée comme visant la géologie des lieux. La circonstance que l'aire géographique délimitée par ce cahier des charges ne recoupe pas le bassin de vie de Meyssac au regard des critères utilisés par l'Institut national de la statistique et des études économiques n'entache pas l'arrêté attaqué d'erreur d'appréciation sur ce point.<br/>
<br/>
              11. En quatrième lieu, aux termes de l'article 112 du règlement précité du Parlement européen et du Conseil du 17 décembre 2013 : " On entend par "mention traditionnelle", une mention employée de manière traditionnelle dans un État membre (...) : / (...) b) pour désigner la méthode de production ou de vieillissement ou la qualité, la couleur, le type de lieu ou un événement particulier lié à l'histoire du produit bénéficiant d'une appellation d'origine protégée ou d'une indication géographique protégée ".<br/>
              12. Pour l'application de ces dispositions, le règlement précité de la Commission du 14 juillet 2009 a prévu, par ses articles 29 à 48, les modalités selon lesquelles les mentions traditionnelles sont définies, reconnues, protégées et modifiées par la Commission à la demande des autorités compétentes des Etats membres ou des pays tiers ou des organisations professionnelles représentatives établies dans les pays tiers. En vertu de l'article 40 de ce règlement, les mentions traditionnelles protégées sont répertoriées et définies dans la base de données électronique E-Bacchus. Cette base énonce, pour ce qui concerne la mention traditionnelle " vin de paille " : " AOP ", " Arbois ", " Côtes du Jura ", " L'Etoile ", "Hermitage": expression liée à une méthode d'élaboration qui consiste en une sélection de raisins provenant des variétés de raisin établies dans la réglementation nationale, mis à sécher pendant une période minimale de six semaines sur des lits de paille ou des claies, ou suspendus. Vieillissement pendant un minimum de trois ans à partir de la date de pressurage comprenant la maturation dans un contenant en bois pendant un minimum de 18 mois ".<br/>
<br/>
              13. D'une part, le V du chapitre Ier du cahier des charges homologué par l'arrêté attaqué désigne les cépages entrant dans le champ de l'AOC pour les vins rouges, les vins susceptibles de bénéficier de la dénomination géographique complémentaire " Coteaux de la Vézère " et les vins susceptibles de bénéficier de la mention " vin de paille ". La circonstance que certains cépages, notamment chardonnay B et sauvignon B, ne soient prévus que pour les vins pouvant bénéficier de la mention " vin de paille " n'est pas de nature, contrairement à ce que soutient la requérante, à les exclure du bénéfice de l'AOC et, partant, de la possibilité de faire usage de cette mention.<br/>
<br/>
              14. D'autre part, il ressort des pièces du dossier que le cahier des charges homologué par l'arrêté attaqué subordonne l'utilisation de la mention " vin de paille " au respect des conditions figurant dans la définition de cette mention, telles que rappelées au point 12 ci-dessus, lesquelles sont exclusivement relatives à la sélection des raisins et aux méthodes de séchage et de vieillissement. Les autres critères invoqués par la société requérante, relatifs notamment au rendement des vignes, aux pratiques oenologiques, au titre alcoométrique volumique acquis, à la teneur en acidité volatile ou aux modalités des obligations déclaratives précédant chaque récolte, ne figurent pas parmi ces conditions et ne sont donc pas au nombre de ceux auxquels les dispositions du droit de l'Union européenne rappelées ci-dessus subordonnent la licéité de l'utilisation de la mention traditionnelle " vin de paille ". Par suite, le moyen tiré de ce que, à défaut d'imposer, au regard de ces critères, des obligations équivalentes à celles des AOC bénéficiant déjà d'une mention " vin de paille ", l'arrêté attaqué serait entaché d'erreur de droit et d'erreur d'appréciation ne peut qu'être écarté.<br/>
<br/>
              15. Il résulte de tout ce qui précède, sans qu'il soit besoin de statuer sur la recevabilité de la requête, que la société de viticulture du Jura n'est pas fondée à demander l'annulation de l'arrêté qu'elle attaque.<br/>
<br/>
              16. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société de viticulture du Jura le versement à l'INAO d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à la charge de l'État qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société de viticulture du Jura est rejetée.<br/>
Article 2 : La société civile de viticulture du Jura versera à l'Institut national de l'origine et de la qualité une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société de viticulture du Jura, au ministre de l'agriculture et de l'alimentation, au ministre de l'économie et des finances, au ministre de l'action et des comptes publics, à l'Institut national de l'origine et de la qualité et à la Fédération des vins de Corrèze.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
