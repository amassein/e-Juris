<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036040103</ID>
<ANCIEN_ID>J5_L_2017_10_000001700609</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/04/01/CETATEXT000036040103.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de NANCY, 3ème chambre - formation à 3, 03/10/2017, 17NC00609, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-10-03</DATE_DEC>
<JURIDICTION>CAA de NANCY</JURIDICTION>
<NUMERO>17NC00609</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre - formation à 3</FORMATION>
<TYPE_REC>excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. MARINO</PRESIDENT>
<AVOCATS>KIPFFER</AVOCATS>
<RAPPORTEUR>M. Jean-Marc  GUERIN-LEBACQ</RAPPORTEUR>
<COMMISSAIRE_GVT>M. COLLIER</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
       Procédure contentieuse antérieure :<br/>
       M.C... A... et Mme E...D...épouse A...ont demandé au tribunal administratif de Nancy d'annuler les arrêtés en date du 13 octobre 2016 par lesquels le préfet de Meurthe-et-Moselle a refusé de leur délivrer un titre de séjour, leur a fait obligation de quitter le territoire français dans un délai de trente jours et a fixé le pays à destination duquel ils pourraient être reconduits en cas d'exécution forcée de la mesure d'éloignement.<br/>
<br/>
       Par un jugement n° 1601661, 1601662 du 13 octobre 2016, le tribunal administratif de Nancy a rejeté leurs demandes.<br/>
<br/>
<br/>
       Procédure devant la cour :<br/>
<br/>
       I. Par une requête enregistrée le 8 mars 2017 sous le n° 17NC00606, M. C... A..., représenté par MeB..., demande à la cour :<br/>
<br/>
       1°) d'annuler le jugement du tribunal administratif de Nancy du 13 octobre 2016 ;<br/>
<br/>
       2°) de renvoyer l'affaire devant un autre tribunal administratif afin qu'il soit statué sur sa demande tendant à l'annulation de l'arrêté du 13 octobre 2016 pris à son encontre ;<br/>
<br/>
       3°) de mettre à la charge de l'Etat une somme de 2 513 euros au titre de la première instance et une somme du même montant au titre de l'instance d'appel, à verser à son conseil sur le fondement des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
       Il soutient que les premiers juges ont manqué à leur obligation d'impartialité et entaché leur jugement d'une erreur de droit et d'une erreur d'appréciation.<br/>
<br/>
       Par un mémoire en défense enregistré le 2 mai 2017, le préfet de Meurthe-et-Moselle conclut au rejet de la requête.<br/>
<br/>
       Il soutient que les moyens d'irrégularité soulevés à l'encontre du jugement attaqué n'appellent aucune observation de sa part.<br/>
<br/>
       II. Par une requête enregistrée le 8 mars 2017 sous le n° 17NC00609, Mme E...D...épouseA..., représentée par MeB..., demande à la cour :<br/>
<br/>
       1°) d'annuler le jugement du tribunal administratif de Nancy du 13 octobre 2016 ;<br/>
<br/>
       2°) de renvoyer l'affaire devant un autre tribunal administratif afin qu'il soit statué sur sa demande tendant à l'annulation de l'arrêté du 13 octobre 2016 pris à son encontre ;<br/>
<br/>
       3°) de mettre à la charge de l'Etat une somme de 2 513 euros au titre de la première instance et une somme du même montant au titre de l'instance d'appel, à verser à son conseil sur le fondement des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
       Elle expose les mêmes moyens que ceux qui sont soulevés dans la requête susvisée, enregistrée sous le n° 17NC00606.<br/>
<br/>
       Par un mémoire en défense enregistré le 2 mai 2017, le préfet de Meurthe-et-Moselle conclut au rejet de la requête.<br/>
<br/>
       Il soutient que les moyens d'irrégularité soulevés à l'encontre du jugement attaqué n'appellent aucune observation de sa part.<br/>
<br/>
       M. et Mme A...ont été admis au bénéfice de l'aide juridictionnelle totale par deux décisions du 6 février 2017.<br/>
       Vu les autres pièces des dossiers.<br/>
<br/>
       Vu :<br/>
       - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
       - la loi n° 91-647 du 10 juillet 1991 ;<br/>
       - le code de justice administrative.<br/>
<br/>
       Le président de la formation de jugement a dispensé le rapporteur public, sur sa proposition, de prononcer des conclusions à l'audience.<br/>
<br/>
       Les parties ont été régulièrement averties du jour de l'audience.<br/>
<br/>
       Le rapport de M. Guérin-Lebacq a été entendu au cours de l'audience publique.<br/>
<br/>
<br/>
<br/>
       1. Considérant que M. et MmeA..., ressortissants albanais nés respectivement le 2 mars 1972 et le 29 décembre 1981, sont entrés irrégulièrement en France le 10 octobre 2012 afin de solliciter la reconnaissance du statut de réfugié ; que leurs demandes d'asile ont été rejetées par deux décisions de l'Office français de protection des réfugiés et apatrides (OFPRA) du 8 juillet 2013, confirmées par deux décisions de la Cour nationale du droit d'asile (CNDA) du 28 novembre 2014 ; que par ailleurs, M. et Mme A...ont demandé la délivrance d'une autorisation de séjour à raison de l'état de santé de leur fille mineure ; que par deux arrêtés du 6 novembre 2015, le préfet de Meurthe-et-Moselle a refusé de délivrer un titre de séjour aux requérants, leur a fait obligation de quitter le territoire français dans un délai de trente jours et a fixé l'Albanie comme pays à destination duquel ils pourraient être reconduits d'office à l'expiration du délai de départ volontaire ; que, par deux requêtes qu'il y a lieu de joindre pour statuer par un seul arrêt, M. et Mme A... relèvent appel du jugement du 13 octobre 2016 par lequel le tribunal administratif de Nancy a rejeté leur demande tendant à l'annulation de ces deux arrêtés ;<br/>
<br/>
       2. Considérant, en premier lieu, que les requérants soutenaient devant le tribunal administratif de Nancy que, pour refuser de leur délivrer une autorisation de séjour, le préfet s'était estimé, à tort, en situation de compétence liée à l'égard de l'avis rendu par le médecin de l'agence régionale de santé sur la situation de leur fille malade ; qu'il n'est pas établi qu'en écartant ce moyen au motif qu'il ressort des termes mêmes des décisions contestées que le préfet a examiné la situation de l'enfant sans s'estimer en situation de compétence liée, les premiers juges auraient manqué à leur obligation d'impartialité ;<br/>
       3. Considérant, en second lieu, que dans l'hypothèse où les premiers juges auraient commis, comme le soutiennent M. et MmeA..., une erreur d'appréciation et une erreur de droit susceptibles de remettre en cause, dans le cadre de l'effet dévolutif de l'appel, les motifs qu'ils ont retenus pour écarter le moyen cité au point précédent, ces prétendues erreurs resteraient, en tout état de cause, sans incidence sur la régularité du jugement attaqué ; qu'au surplus, M. et Mme A...n'apportent à l'instance aucun élément de nature à remettre en cause l'appréciation portée par le préfet de Meurthe-et-Moselle qui a estimé qu'un traitement approprié à la situation de leur fille existait dans le pays d'origine de la famille ;<br/>
       4. Considérant qu'il résulte de tout ce qui précède que M. et Mme A...ne sont pas fondés à demander l'annulation du jugement attaqué ; que, par voie de conséquence, leurs conclusions à fin d'injonction ainsi que celles présentées sur le fondement des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 relative à l'aide juridique ne peuvent qu'être rejetées ;<br/>
<br/>
D E C I D E :<br/>
<br/>
      Article 1er : Les requêtes de M. et Mme A... sont rejetées.<br/>
<br/>
      Article 2 : Le présent arrêt sera notifié à M. C... A..., à Mme E...D...épouse A...et au ministre de l'intérieur.<br/>
<br/>
      Copie en sera adressée au préfet de Meurthe-et-Moselle.<br/>
<br/>
2<br/>
N° 17NC00606, 17NC00609<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-01 Étrangers. Séjour des étrangers.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">335-03 Étrangers. Obligation de quitter le territoire français (OQTF) et reconduite à la frontière.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
