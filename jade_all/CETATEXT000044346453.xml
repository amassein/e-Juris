<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044346453</ID>
<ANCIEN_ID>JG_L_2021_11_000000440237</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/34/64/CETATEXT000044346453.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 19/11/2021, 440237</TITRE>
<DATE_DEC>2021-11-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440237</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP GATINEAU, FATTACCINI, REBEYROL</AVOCATS>
<RAPPORTEUR>M. Eric Buge</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:440237.20211119</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée Guisnel location a demandé au tribunal administratif de Montreuil de condamner l'Agence centrale des organismes de sécurité sociale (ACOSS) à lui verser la somme de 219 662 euros, majorée des intérêts et de la capitalisation des intérêts, en réparation du préjudice qu'elle estime avoir subi du fait de l'illégalité de l'interprétation que cette dernière a donnée, notamment dans sa lettre collective n° 2004-46 du 2 mars 2004, de l'arrêté du 20 décembre 2002 relatif aux frais professionnels déductibles pour le calcul des cotisations de sécurité sociale. Par une ordonnance n° 1911828 du 28 novembre 2019, le président de la 5ème chambre du tribunal administratif de Montreuil a rejeté cette demande comme portée devant un ordre de juridiction incompétent pour en connaître.<br/>
<br/>
              Par une ordonnance n° 19VE04180 du 4 février 2020, le président de la 4ème chambre de la cour administrative d'appel de Versailles a rejeté l'appel formé par la société contre cette ordonnance.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 24 avril et 9 juin 2020 au secrétariat du contentieux du Conseil d'État, la société Guisnel location demande au Conseil d'État : <br/>
<br/>
              1°) d'annuler l'ordonnance du président de la 4ème chambre de la cour administrative d'appel de Versailles du 4 février 2020 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Agence centrale des organismes de sécurité sociale la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Eric Buge, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la société Guisnel location et à la SCP Gatineau, Fattaccini, Rebeyrol, avocat de l'Agence centrale des organismes de sécurité sociale ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. D'une part, aux termes de l'article L. 142-8 du code de la sécurité sociale : " Le juge judiciaire connaît des contestations relatives : / 1° Au contentieux de la sécurité sociale défini à l'article L. 142-1 (...) ". En vertu de ce dernier article : " Le contentieux de la sécurité sociale comprend les litiges relatifs : / 1° A l'application des législations et réglementations de sécurité sociale et de mutualité sociale agricole ; / 2° Au recouvrement des contributions, versements et cotisations mentionnés au 5° de l'article L. 213-1 ; / 3° Au recouvrement des contributions, versements et cotisations mentionnés aux articles L. 1233-66, L. 1233-69, L. 3253-18, L. 5422-6, L. 5422-9, L. 5422-11, L. 5422-12 et L. 5424-20 du code du travail ".<br/>
<br/>
              2. D'autre part, l'article L. 225-1-1 du code de la sécurité sociale donne notamment pour mission à l'Agence centrale des organismes de sécurité sociale, qui, en vertu de l'article L. 225-2 du même code, est un établissement public national à caractère administratif, d'assurer l'application homogène des lois et des règlements relatifs aux cotisations et aux contributions de sécurité sociale recouvrées par les unions de recouvrement des cotisations de sécurité sociale et d'allocations familiales et les caisses générales de sécurité sociale et d'harmoniser les positions prises par ces organismes de recouvrement. Il incombe également à l'Agence centrale des organismes de sécurité sociale d'assurer une application homogène par ces organismes des dispositions relatives aux autres contributions et cotisations dont ils assurent le recouvrement.<br/>
<br/>
              3. Les actes par lesquels l'Agence centrale des organismes de sécurité sociale indique l'interprétation qu'il convient de retenir des dispositions législatives et réglementaires relatives aux cotisations et contributions dont les unions de recouvrement des cotisations de sécurité sociale et d'allocations familiales et les caisses générales de sécurité sociale assurent le recouvrement ont la nature d'actes administratifs. Une action en responsabilité fondée sur l'illégalité d'un tel acte relève par nature de la juridiction administrative, alors même que les contentieux individuels auxquels donne lieu le recouvrement des cotisations et contributions mentionnés à l'article L. 142-1 du code de la sécurité sociale relèvent de la compétence des juridictions de l'ordre judiciaire en application de l'article L. 142-8 du même code.<br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que la société Guisnel location a demandé au tribunal administratif de Montreuil de condamner l'Agence centrale des organismes de sécurité sociale à lui verser la somme de 219 662 euros en réparation du préjudice qu'elle estime avoir subi du fait de l'illégalité de l'interprétation donnée par l'agence, notamment dans sa lettre collective n° 2004-46 du 2 mars 2004, de l'arrêté du 20 décembre 2002 relatif aux frais professionnels déductibles pour le calcul des cotisations de sécurité sociale. Il résulte de ce qui a été dit au point précédent qu'une telle demande ressortit à la compétence de la juridiction administrative. Par suite, le président de la 4ème chambre de la cour administrative d'appel de Versailles a commis une erreur de droit en jugeant que la demande présentée par la requérante devant le tribunal administratif de Montreuil avait été portée devant un ordre de juridiction incompétent pour en connaître.<br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur l'autre moyen du pourvoi, que la société Guisnel location est fondée à demander l'annulation de l'ordonnance qu'elle attaque.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              7. Pour les motifs exposés ci-dessus, c'est à tort que le président de la 5ème chambre du tribunal administratif de Montreuil a rejeté la demande de la société Guisnel location comme portée devant un ordre de juridiction incompétent pour en connaître. Il y a lieu, par suite, d'annuler son ordonnance du 28 novembre 2019 et de statuer immédiatement sur cette demande par la voie de l'évocation.<br/>
<br/>
              8. La société requérante demande la condamnation de l'Agence centrale des organismes de sécurité sociale à lui verser une indemnité réparant le seul préjudice correspondant au montant des cotisations de sécurité sociale, contributions au régime d'assurance chômage et  cotisations à l'assurance de garantie des salaires qu'elle a indûment acquittées en conséquence de l'illégalité de l'interprétation que l'agence a donnée de l'arrêté du 20 décembre 2002 relatif aux frais professionnels déductibles pour le calcul des cotisations de sécurité sociale. De telles conclusions ont le même objet que des conclusions tendant à la contestation du montant de ces cotisations et contributions. Par suite, l'existence d'une voie de recours devant les juridictions de l'ordre judiciaire, en application de l'article L. 142-8 du code de la sécurité sociale, en vue du règlement d'un tel litige s'oppose à ce qu'elle engage une action mettant en cause la responsabilité de l'Agence centrale des organismes de sécurité sociale en raison de l'illégalité de l'interprétation donnée par celle-ci des dispositions dont il lui a été fait application.<br/>
<br/>
              9. Il résulte de ce qui précède que l'Agence centrale des organismes de sécurité sociale est fondée à soutenir que la demande présentée par la société Guisnel location est irrecevable et qu'elle ne peut, par suite, qu'être rejetée.<br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Agence centrale des organismes de sécurité sociale qui n'est pas, dans la présente instance, la partie perdante. En revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de société Guisnel location le versement d'une somme de 1 500 euros à l'Agence centrale des organismes de sécurité sociale au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'ordonnance du président de la 4ème chambre de la cour administrative d'appel de Versailles du 4 février 2020 et l'ordonnance du président de la 5ème chambre du tribunal administratif de Montreuil du 28 novembre 2019 sont annulées.<br/>
Article 2 : La demande présentée par la société Guisnel location devant le tribunal administratif de Montreuil et le surplus de ses conclusions présentées devant la cour administrative d'appel et devant le Conseil d'Etat sont rejetés.<br/>
Article 3 : La société Guisnel location versera une somme de 1 500 euros à l'Agence centrale des organismes de sécurité sociale au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à la société par actions simplifiée Guisnel location et à l'Agence centrale des organismes de sécurité sociale.<br/>
              Délibéré à l'issue de la séance du 20 octobre 2021 où siégeaient : Mme Christine Maugüé, présidente adjointe de la Section du Contentieux, présidant ; Mme A... M..., Mme D... L..., présidentes de chambre ; M. B... K..., Mme C... F..., Mme H... J..., M. I... G..., M. Damien Botteghi, conseillers d'Etat et M. Eric Buge, maître des requêtes en service extraordinaire-rapporteur. <br/>
<br/>
<br/>
Rendu le 19 novembre 2021.<br/>
<br/>
<br/>
                 La présidente : <br/>
                 Signé : Mme Christine Maugüé<br/>
 		Le rapporteur : <br/>
      Signé : M. Eric Buge<br/>
                 La secrétaire :<br/>
                 Signé : Mme N... E...<br/>
<br/>
<br/>
<br/>
<br/>
La République mande et ordonne au ministre des solidarités et de la santé en ce qui le concerne ou à tous huissiers de justice à ce requis en ce qui concerne les voies de droit commun contre les parties privées, de pourvoir à l'exécution de la présente décision.<br/>
			Pour expédition conforme,<br/>
			Pour le secrétaire du contentieux, par délégation :<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03-01-02-04 COMPÉTENCE. - RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. - COMPÉTENCE DÉTERMINÉE PAR DES TEXTES SPÉCIAUX. - ATTRIBUTIONS LÉGALES DE COMPÉTENCE AU PROFIT DES JURIDICTIONS JUDICIAIRES. - COMPÉTENCE DES JURIDICTIONS JUDICIAIRES EN MATIÈRE DE PRESTATIONS DE SÉCURITÉ SOCIALE. - EXCLUSION - ACTION EN RESPONSABILITÉ FONDÉE SUR L'ILLÉGALITÉ D'UNE INTERPRÉTATION DES TEXTES PAR L'ACOSS [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-03 PROCÉDURE. - INTRODUCTION DE L'INSTANCE. - EXCEPTION DE RECOURS PARALLÈLE. - EXISTENCE - ACTION EN RESPONSABILITÉ PORTANT SUR LE SEUL PRÉJUDICE CORRESPONDANT AU MONTANT DE COTISATIONS SOCIALES INDÛMENT VERSÉES EN RAISON D'UNE INTERPRÉTATION ILLÉGALE DES TEXTES PAR L'ACOSS.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">62-01-01-01 SÉCURITÉ SOCIALE. - ORGANISATION DE LA SÉCURITÉ SOCIALE. - RÉGIME DE SALARIÉS. - RÉGIME GÉNÉRAL. - ACTION EN RESPONSABILITÉ FONDÉE SUR L'ILLÉGALITÉ D'UNE INTERPRÉTATION DES TEXTES PAR L'ACOSS - 1) COMPÉTENCE DU JUGE ADMINISTRATIF - EXISTENCE [RJ1] - 2) CONCLUSIONS TENDANT À L'INDEMNISATION DU SEUL PRÉJUDICE CORRESPONDANT AU MONTANT DE COTISATIONS SOCIALES INDÛMENT VERSÉES EN RAISON DE CETTE INTERPRÉTATION - EXCEPTION DE RECOURS PARALLÈLE - EXISTENCE - CONSÉQUENCE - IRRECEVABILITÉ.
</SCT>
<ANA ID="9A"> 17-03-01-02-04 Les actes par lesquels l'Agence centrale des organismes de sécurité sociale (ACOSS) indique l'interprétation qu'il convient de retenir des dispositions législatives et réglementaires relatives aux cotisations et contributions dont les unions de recouvrement des cotisations de sécurité sociale et d'allocations familiales et les caisses générales de sécurité sociale assurent le recouvrement ont la nature d'actes administratifs. Une action en responsabilité fondée sur l'illégalité d'un tel acte relève par nature de la juridiction administrative, alors même que les contentieux individuels auxquels donne lieu le recouvrement des cotisations et contributions mentionnés à l'article L. 142-1 du code de la sécurité sociale (CSS) relèvent de la compétence des juridictions de l'ordre judiciaire en application de l'article L. 142-8 du même code.</ANA>
<ANA ID="9B"> 54-01-03 Des conclusions tendant à l'indemnisation du seul préjudice correspondant au montant des cotisations et contributions sociales indûment acquittées en conséquence de l'illégalité de l'interprétation que l'Agence centrale des organismes de sécurité sociale (ACOSS) a donnée des dispositions législatives et réglementaires applicables ont le même objet que des conclusions tendant à la contestation du montant de ces cotisations et contributions. ......Par suite, l'existence d'une voie de recours devant les juridictions de l'ordre judiciaire, en application de l'article L. 142-8 du CSS, en vue du règlement d'un tel litige s'oppose à ce qu'elle engage une action mettant en cause la responsabilité de l'ACOSS en raison de l'illégalité de l'interprétation donnée par celle-ci des dispositions dont il lui a été fait application.</ANA>
<ANA ID="9C"> 62-01-01-01 1) Les actes par lesquels l'Agence centrale des organismes de sécurité sociale (ACOSS) indique l'interprétation qu'il convient de retenir des dispositions législatives et réglementaires relatives aux cotisations et contributions dont les unions de recouvrement des cotisations de sécurité sociale et d'allocations familiales et les caisses générales de sécurité sociale assurent le recouvrement ont la nature d'actes administratifs. Une action en responsabilité fondée sur l'illégalité d'un tel acte relève par nature de la juridiction administrative, alors même que les contentieux individuels auxquels donne lieu le recouvrement des cotisations et contributions mentionnés à l'article L. 142-1 du code de la sécurité sociale (CSS) relèvent de la compétence des juridictions de l'ordre judiciaire en application de l'article L. 142-8 du même code.......2) Des conclusions tendant à l'indemnisation du seul préjudice correspondant au montant des cotisations et contributions sociales indûment acquittées en conséquence de l'illégalité de l'interprétation que l'ACOSS a donnée des dispositions législatives et réglementaires applicables ont le même objet que des conclusions tendant à la contestation du montant de ces cotisations et contributions. ......Par suite, l'existence d'une voie de recours devant les juridictions de l'ordre judiciaire, en application de l'article L. 142-8 du CSS, en vue du règlement d'un tel litige s'oppose à ce qu'elle engage une action mettant en cause la responsabilité de l'ACOSS en raison de l'illégalité de l'interprétation donnée par celle-ci des dispositions dont il lui a été fait application.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de la compétence du juge administrative pour connaître de la légalité d'une telle interprétation, CE, 11 octobre 2012, Caisse interprofessionnelle des congés payés de la région parisienne (CICPRP), n° 354383, T. pp. 663-996 ; CE, 13 juin 2018, CCI France, n° 404485, T. pp. 592-655-670-814-927.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
