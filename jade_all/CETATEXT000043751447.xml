<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043751447</ID>
<ANCIEN_ID>JG_L_2021_07_000000445565</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/75/14/CETATEXT000043751447.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 01/07/2021, 445565, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445565</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Lionel Ferreira</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Céline  Guibé</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:445565.20210701</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° La société Triumph International a demandé au tribunal administratif de Strasbourg de prononcer la réduction des cotisations de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre des années 2015 et 2016 à raison de l'établissement qu'elle exploite à Obernai (Bas-Rhin). Par un jugement no 1802134 du 29 juillet 2020, le tribunal administratif de Strasbourg, après avoir prononcé un non-lieu à statuer à hauteur du dégrèvement intervenu en cours d'instance, a partiellement fait droit à sa demande et rejeté le surplus de ses conclusions.<br/>
<br/>
              Sous le n° 445565, par un pourvoi, un mémoire complémentaire et un mémoire en réplique, enregistrés le 22 octobre 2020 ainsi que les 21 janvier et 14 juin 2021 au secrétariat du contentieux du Conseil d'Etat, la société Triumph International demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'État la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° La société Triumph International a demandé au tribunal administratif de Strasbourg de prononcer la réduction des cotisations de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre de l'année 2017 à raison de l'établissement qu'elle exploite à Obernai (Bas-Rhin). Par un jugement no 1804099 du 29 juillet 2020, le tribunal administratif de Strasbourg, après avoir partiellement fait droit à sa demande, a rejeté le surplus de ses conclusions. <br/>
<br/>
              Sous le n° 445566, par un pourvoi, un mémoire complémentaire et un mémoire en réplique, enregistrés le 22 octobre 2020 ainsi que les 21 janvier et 14 juin 2021 au secrétariat du contentieux du Conseil d'Etat, la société Triumph International demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'État la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Lionel Ferreira, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Céline Guibé, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société Triumph International ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces des dossiers soumis aux juges du fond que la société Triumph International exploite à Obernai (Bas-Rhin) un entrepôt affecté à la réception, au stockage et à la préparation de commandes et l'expédition de marchandises. Elle a fait l'objet d'une vérification de comptabilité portant sur les exercices clos en 2012, 2013 et 2014, au terme de laquelle l'administration fiscale a qualifié son entrepôt d'établissement industriel, au sens de l'article 1499 du code général des impôts. Elle a demandé au tribunal administratif de Strasbourg de prononcer la réduction des cotisations de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie à raison de cet entrepôt au titre des années 2015 à 2017. Par deux jugements du 29 juillet 2020, après avoir prononcé, dans la première affaire, un non-lieu à statuer à hauteur du dégrèvement intervenu en cours d'instance, le tribunal administratif a prononcé des décharges partielles puis a rejeté le surplus de ses conclusions. La société Triumph International se pourvoit en cassation contre ces jugements par deux pourvois qui présentent à juger les mêmes questions. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Eu égard aux moyens qu'elle invoque, la société requérante doit être regardée comme ne demandant l'annulation des jugements attaqués qu'en tant qu'ils ont rejeté le surplus de ses conclusions relatives aux immobilisations autres que celles dont il était soutenu qu'elles relevaient d'un autre établissement que l'entrepôt d'Obernai mentionné au point précédent.<br/>
<br/>
              Sur le dégrèvement intervenu en cours d'instance : <br/>
<br/>
              3. Par décision du 4 juin 2021, l'administrateur général des finances publiques chargé de la direction des vérifications nationales et internationales a accordé un dégrèvement au titre des années 2015 et 2016 à hauteur, respectivement, de 4 884 euros et 5 011 euros. Dans cette mesure, les conclusions du pourvoi n°445565  sont devenues sans objet. <br/>
<br/>
              Sur le surplus des conclusions n°445565 et sur le pourvoi n°445566 : <br/>
<br/>
              4. En premier lieu, aux termes de l'article 1499 du code général des impôts: " La valeur locative des immobilisations industrielles passibles de la taxe foncière sur les propriétés bâties est déterminée en appliquant au prix de revient de leurs différents éléments, revalorisé à l'aide des coefficients qui avaient été prévus pour la révision des bilans, des taux d'intérêt fixés par décret en Conseil d'Etat (...) ". Il résulte de ces dispositions que le prix de revient des immobilisations industrielles passibles de la taxe foncière, évalué selon la méthode comptable, est celui qui est inscrit à l'actif du bilan et l'administration peut se fonder sur les énonciations comptables opposables à la société pour inclure dans la valeur locative des immobilisations le montant des travaux inscrits en tant qu'immobilisations, sauf pour la société à démontrer que ces travaux constitueraient en réalité des charges déductibles.<br/>
<br/>
              5. Après avoir relevé que la société requérante se prévalait des intitulés de ses écritures comptables pour tenter de démontrer que certaines des immobilisations comptabilisées n'entraient pas dans le champ d'application de la taxe foncière sur les propriétés bâties, le tribunal administratif a pu, sans méconnaître les règles applicables en matière de charge de la preuve, juger que ces seuls intitulés comptables, qui portaient sur des comptes de constructions, ne pouvaient être regardés, eu égard notamment à leur caractère insuffisamment précis et explicite, comme des justifications probantes, permettant de remettre en cause les bases d'imposition établies par l'administration à partir des écritures comptables de la société. <br/>
<br/>
              6. En deuxième lieu, le tribunal administratif a suffisamment motivé les jugements attaqués en jugeant qu'il ne résultait pas des factures produites par la société requérante au titre des années 1989 à 2016 pour certaines immobilisations que celles-ci n'entreraient pas, eu égard à leur nature, dans le champ d'application des dispositions des articles 1447 et 1381 du code général des impôts.<br/>
<br/>
              7. En troisième lieu, le paragraphe 230 du Bulletin officiel des finances publiques, publié sous la référence BOI-IF-TFB-20-20-10-20 prévoit, dans son premier alinéa que " Les changements de caractéristiques physiques ne sont pris en compte que lorsqu'ils ont une incidence sur le prix de revient comptable des immobilisations, c'est-à-dire, en fait, lorsqu'ils revêtent le caractère de grosses réparations amortissables ou d'installations ou d'agencements nouveaux " et dans son second alinéa qu' " il est cependant admis que le complément de valeur locative résultant des changements du premier type (grosses réparations) ne soit pas calculé sur la base de la valeur d'immobilisation ajoutée au bilan à l'issue des travaux mais sur une base inférieure tenant compte du fait que ces derniers ne créent pas une immobilisation nouvelle mais confortent seulement une immobilisation ancienne. Ainsi, si les travaux de réparation considérés n'apportent aucune amélioration à l'établissement, il n'y a pas lieu de calculer de complément de valeur locative ".<br/>
<br/>
              8. En relevant qu'au regard des factures produites, la nature exacte des travaux effectivement réalisés était insuffisamment précise, le tribunal administratif a suffisamment répondu à l'invocation non seulement du second alinéa mais également du premier alinéa du paragraphe précité. <br/>
<br/>
              9. En quatrième et dernier lieu, pour apprécier, en application de l'article 1495 du code général des impôts et de l'article 324 B de son annexe III, la consistance des propriétés qui entrent, en vertu de ses articles 1380 et 1381, dans le champ de la taxe foncière sur les propriétés bâties, il est tenu compte, non seulement de tous les éléments d'assiette mentionnés par ces deux derniers articles mais également des biens faisant corps avec eux. Sont toutefois exonérés de cette taxe, en application du 11° de l'article 1382 du même code, ceux de ces biens qui font partie des outillages, autres installations et moyens matériels d'exploitation d'un établissement industriel, c'est-à-dire ceux de ces biens qui relèvent d'un établissement qualifié d'industriel au sens de l'article 1499, qui sont spécifiquement adaptés aux activités susceptibles d'être exercées dans un tel établissement et qui ne sont pas au nombre des éléments mentionnés aux 1° et 2° de l'article 1381.<br/>
<br/>
              10. Il résulte de ce qui précède qu'en jugeant que les outillages, autres installations et moyens matériels d'exploitation des établissements industriels mentionnés au 11° de l'article 1382 du code général des impôts s'entendent de ceux qui participent directement à l'activité industrielle de l'établissement et sont dissociables des immeubles, le tribunal administratif de Strasbourg a commis une erreur de droit. Dès lors qu'il n'est pas établi par la décision mentionnée au point 3 que l'intégralité des immobilisations pour lesquelles la société Triumph International a réclamé le bénéfice de cette exonération entre dans le champ du dégrèvement qui a été prononcé, cette société est, par suite, seulement fondée à demander l'annulation des jugements attaqués en tant qu'ils se sont prononcés sur les outillages, autres installations et moyens matériels d'exploitation pour lesquels elle demandait à bénéficier de l'exonération prévue par le 11° de l'article 1382 du code général des impôts et qui n'ont, le cas échéant pas été pris en compte dans le dégrèvement précité.<br/>
<br/>
              11. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 1 500 euros à verser à la société Triumph International au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a plus lieu de statuer, à hauteur du dégrèvement prononcé le 4 juin 2021, sur les conclusions de la société Triumph International.<br/>
Article 2 : Les deux jugements du 29 juillet 2020 du tribunal administratif de Strasbourg sont annulés en tant qu'ils se sont prononcés sur les outillages, autres installations et moyens matériels d'exploitation pour lesquels la société Triumph International demandait à bénéficier de l'exonération prévue par le 11° de l'article 1382 du code général des impôts et qui n'ont, le cas échéant, pas été pris en compte dans le dégrèvement mentionné à l'article précédent.<br/>
<br/>
Article 3 : Les affaires sont renvoyées, dans cette mesure, au tribunal administratif de Strasbourg.<br/>
<br/>
Article 4 : L'Etat versera à la société Triumph International une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Le surplus des conclusions des pourvois n° 445565 et 445566 est rejeté.<br/>
<br/>
Article 6 : La présente décision sera notifiée à la société Triumph International et au ministre de l'économie, des finances et de la relance.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
