<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028219082</ID>
<ANCIEN_ID>JG_L_2013_11_000000363815</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/21/90/CETATEXT000028219082.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 20/11/2013, 363815, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-11-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>363815</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:363815.20131120</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 9 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par la Confédération française du commerce et de gros interentreprises et du commerce international, dont le siège est 18, rue des Pyramides à Paris (75001) ; elle demande au Conseil d'Etat d'annuler, pour excès de pouvoir, les précisions publiées au Bulletin officiel des finances publiques - Impôt sous le titre BOI-TFP-TSC, à jour au 12 septembre 2012, en tant qu'elles prévoient les conditions d'assujettissement à la taxe sur les surfaces commerciales des établissements réalisant à la fois des ventes au détail et des ventes en gros ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 72-657 du 13 juillet 1972 ;<br/>
<br/>
              Vu la loi n° 73-1193 du 27 décembre 1973 ;<br/>
<br/>
              Vu le décret n° 95-85 du 26 janvier 1995 ;<br/>
<br/>
              Vu le code général des impôts ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article 3 de la loi du 13 juillet 1972, dans sa rédaction alors applicable : " Il est institué une taxe sur les surfaces commerciales assise sur la surface de vente des magasins de commerce de détail, dès lors qu'elle dépasse 400 mètres carrés des établissements ouverts à partir du 1er janvier 1960 quelle que soit la forme juridique de l'entreprise qui les exploite. (...)/ La surface de vente des magasins de commerce de détail, prise en compte pour le calcul de la taxe, et celle visée à l'article L. 720-5 du code de commerce, s'entendent des espaces affectés à la circulation de la clientèle pour effectuer ses achats, de ceux affectés à l'exposition des marchandises proposées à la vente, à leur paiement, et de ceux affectés à la circulation du personnel pour présenter les marchandises à la vente. (...) " ;<br/>
<br/>
              2. Considérant que le paragraphe 320 du Bulletin officiel des finances publiques - impôts, publié sous le titre BOI-TFP-TSC, à jour au 12 septembre 2012, indique, pour la détermination du chiffre d'affaires par mètre carré, dont le taux de la taxe sur les surfaces commerciales, dite " TaSCom " est une fonction croissante, que " Lorsqu'un établissement réalise à la fois des ventes au détail de marchandises en l'état et une autre activité (prestations de services, ventes en gros ...), le chiffre d'affaires à prendre en considération au titre de la TaSCom est celui des seules ventes au détail en l'état, dès lors que ces deux activités font l'objet de comptes distincts. / Les ventes de produits effectuées auprès des professionnels qui ne revendent pas en l'état ces produits, tels que les restaurateurs ou les entreprises de travaux immobiliers, sont prises en compte pour la détermination du chiffre d'affaires par m² " ; que la Confédération française du commerce et de gros interentreprises et du commerce international demande l'annulation, pour excès de pouvoir, de cette disposition ;<br/>
<br/>
              3. Considérant, en premier lieu, qu'il ne résulte pas de l'article 3 de la loi du 13 juillet 1972 que les surfaces de vente au détail prises en compte pour le calcul de la taxe devraient être situées dans des établissements réalisant exclusivement des ventes au détail ; que le remplacement, par l'article 130 de la loi du 30 décembre 1996, de l'expression initiale " surface des locaux de vente destinés à la vente au détail ", par l'expression " surface de vente des magasins de commerce de détail ", reprise des dispositions de la loi du 27 décembre 1973 d'orientation du commerce et de l'artisanat, n'a pas pour effet de rendre applicable au régime de la taxe sur les surfaces commerciales la définition du " commerce de détail " retenue pour l'application du régime de l'autorisation préalable d'exploitation ; que, par suite, le moyen tiré de ce que les dispositions contestées du bulletin méconnaissent celles de l'article 3 de la loi du 13 juillet 1972 doit être écarté ; <br/>
<br/>
              4. Considérant, en deuxième lieu, qu'aux termes de l'article 1er du décret du 26 janvier 1995 relatif à la taxe d'aide au commerce et à l'artisanat, dans sa version applicable en l'espèce : " (...) Lorsqu'un établissement réalise à la fois des ventes au détail de marchandises en l'état et une autre activité, le chiffre d'affaires à prendre en considération au titre de la taxe sur les surfaces commerciales est celui des ventes au détail en l'état, dès lors que les deux activités font l'objet de comptes distincts. " ; qu'ainsi qu'il a été dit, les dispositions de l'article 3 de la loi du 13 juillet 1972 n'interdisent pas l'assujettissement à la taxe sur les surfaces commerciales des activités de commerce de détail réalisées dans des établissements pratiquant également le commerce en gros ou d'autres activités, à concurrence du chiffre d'affaires relatif à la surface de commerce de détail ; que, dès lors, le moyen tiré de ce que les dispositions contestées seraient illégales en ce qu'elles réitèrent les dispositions précitées du décret du 26 janvier 1995 qui seraient elles-mêmes illégales pour étendre le champ d'application de la taxe à des établissements commerciaux " mixtes ", doit être écarté ; que la précision du paragraphe 320 du bulletin contesté, selon laquelle le chiffre d'affaires à prendre en considération au titre de la taxe est celui des " seules " ventes au détail en l'état, dès lors que les deux activités font l'objet de comptes distincts, alors que les dispositions du décret du 26 janvier 1995 ne comportent pas l'adjectif " seules ", n'a ni pour objet ni pour effet d'assujettir à la taxe d'autres activités que celles de vente au détail en l'état ; que, dès lors, le moyen tiré de ce que ces dispositions ajouteraient illégalement à celles du décret doit également être écarté ; <br/>
<br/>
              5. Considérant, en troisième lieu, qu'il résulte, ainsi qu'il a été dit, tant des dispositions de la loi du 13 juillet 1972 que de celles du décret du 26 janvier 1995, reprises dans le premier alinéa du paragraphe 320 attaqué, que le chiffre d'affaires à prendre en compte pour le calcul de la taxe sur les surfaces commerciales est celui réalisé par les surfaces de ventes au détail, en l'état, sans qu'il y ait lieu de distinguer selon que l'acheteur est un particulier ou un professionnel ; qu'il s'en déduit que les ventes au détail en l'état à des professionnels, tant pour leurs besoins propres que lorsqu'ils incorporent les produits qu'ils ont ainsi achetés dans les produits qu'ils vendent ou les prestations qu'ils fournissent, doivent être prises en compte pour la détermination du  chiffre d'affaires par mètre carré, à la différence des ventes à des professionnels revendant en l'état, l'activité de ces derniers relevant alors d'une activité de grossiste ou d'intermédiaire ; que, par suite, le moyen tiré de ce que les dispositions contestées étendent illégalement le champ d'application de la taxe à des ventes faites à des professionnels, alors que des ventes au détail ne peuvent être faites qu'à des consommateurs finaux  doit, par suite, être écarté ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que la requête de la Confédération française du commerce et de gros interentreprises et du commerce international doit être rejetée, y compris les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>		D E C I D E :<br/>
              		--------------<br/>
<br/>
 Article 1er : La requête de la Confédération française du commerce et de gros interentreprises et du commerce international est rejetée. <br/>
<br/>
 Article 2 : La présente décision sera notifiée à la Confédération française du commerce et de gros interentreprises et du commerce international et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
