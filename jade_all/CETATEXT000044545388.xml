<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044545388</ID>
<ANCIEN_ID>JG_L_2021_12_000000447231</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/54/53/CETATEXT000044545388.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 21/12/2021, 447231</TITRE>
<DATE_DEC>2021-12-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>447231</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Bertrand Mathieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Philippe Ranquet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:447231.20211221</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... I... a demandé au tribunal administratif de Nantes d'annuler la décision du 9 mars 2018 par laquelle le préfet des Bouches du Rhône a rejeté sa demande d'acquisition de la nationalité française, ainsi que la décision implicite par laquelle le ministre de l'intérieur a rejeté son recours administratif.<br/>
<br/>
              Par une ordonnance n° 1809473 du 12 avril 2019, la présidente de la 2ème chambre du tribunal administratif de Nantes a rejeté sa demande comme irrecevable.<br/>
<br/>
              Par un arrêt n° 19NT01951 du 6 octobre 2020, la cour administrative d'appel de Nantes a, sur appel de M. I..., d'une part, annulé cette ordonnance ainsi que la décision du 24 octobre 2018 du ministre de l'intérieur portant rejet du recours administratif, et d'autre part, enjoint au ministre de l'intérieur de procéder au réexamen de la demande de naturalisation de M. I... dans un délai de deux mois. <br/>
<br/>
              Par un pourvoi, enregistré le 4 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. I....<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le code pénal ;<br/>
              - le code de procédure pénale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bertrand Mathieu, conseiller d'Etat en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Philippe Ranquet, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Il ressort des pièces du dossier soumis aux juges du fond que M. I..., ressortissant tunisien, a présenté une demande de naturalisation qui a été déclarée irrecevable par une décision du 9 mars 2018 du préfet des Bouches-du-Rhône, confirmée par décision du 24 octobre 2018 du ministre de l'intérieur, en raison de la condamnation à deux ans d'emprisonnement, dont un an avec sursis, dont il avait fait l'objet le 24 novembre 2000 par le tribunal correctionnel d'Aix-en-Provence pour transport, détention, offre ou cession, acquisition non autorisée de stupéfiants et usage illicite de stupéfiants. Par un arrêt du 6 octobre 2020, contre lequel le ministre de l'intérieur se pourvoit en cassation, la cour administrative d'appel de Nantes a annulé la décision du 24 octobre 2018 du ministre de l'intérieur et enjoint à celui-ci de réexaminer la demande de naturalisation de l'intéressé dans un délai de deux mois.<br/>
<br/>
              2.	D'une part, aux termes de l'article 21-27 du code civil : " Nul ne peut acquérir la nationalité française ou être réintégré dans cette nationalité s'il a été l'objet soit d'une condamnation pour crimes ou délits constituant une atteinte aux intérêts fondamentaux de la Nation ou un acte de terrorisme, soit, quelle que soit l'infraction considérée, s'il a été condamné à une peine égale ou supérieure à six mois d'emprisonnement, non assortie d'une mesure de sursis. (...) Les dispositions du présent article ne sont pas applicables (...) au condamné ayant bénéficié d'une réhabilitation de plein droit ou d'une réhabilitation judiciaire conformément aux dispositions de l'article 133-12 du code pénal, ou dont la mention de la condamnation a été exclue du bulletin nº 2 du casier judiciaire, conformément aux dispositions des articles 775-1 et 775-2 du code de procédure pénale ". <br/>
<br/>
              3.	D'autre part, l'article 133-12 du code pénal dispose que : " Toute personne frappée d'une peine criminelle, correctionnelle ou contraventionnelle peut bénéficier, soit d'une réhabilitation de plein droit dans les conditions prévues à la présente section, soit d'une réhabilitation judiciaire accordée dans les conditions prévues par le code de procédure pénale ". Aux termes de l'article 133-13 de ce code : " La réhabilitation est acquise de plein droit à la personne physique condamnée qui n'a, dans les délais ci-après déterminés, subi aucune condamnation nouvelle à une peine criminelle ou correctionnelle : / (...) 3° Pour la condamnation unique à un emprisonnement n'excédant pas dix ans ou pour les condamnations multiples à l'emprisonnement dont l'ensemble ne dépasse pas cinq ans, après un délai de dix ans à compter soit de l'expiration de la peine subie, soit de la prescription accomplie. / (...) Lorsqu'il s'agit de condamnations assorties en tout ou partie du sursis, du sursis probatoire ou du sursis avec obligation d'accomplir un travail d'intérêt général, les délais de réhabilitation courent, pour chacune de ces condamnations et y compris en cas de condamnations multiples, à compter de la date à laquelle la condamnation est non avenue ". <br/>
<br/>
              4.	Enfin, aux termes de l'article 775 du code de procédure pénale : " Le bulletin n° 2 est le relevé des fiches du casier judiciaire applicables à la même personne, à l'exclusion de celles concernant les décisions suivantes : / (...) 5° Les condamnations ayant fait l'objet d'une réhabilitation de plein droit ou judiciaire (...) ".<br/>
<br/>
              5.	Il résulte des dispositions de l'article 21-27 du code civil que si elles font obstacle à ce que des personnes ayant fait l'objet de certaines condamnations puissent acquérir la nationalité française, elles ne trouvent pas à s'appliquer à l'étranger condamné qui a bénéficié d'une réhabilitation de plein droit dans les conditions énoncées par l'article 133-13 du code pénal. La circonstance que la condamnation n'aurait pas été effacée du bulletin n° 2, comme le prévoit le 5° de l'article 775 du code de procédure pénale, est sans incidence à cet égard.<br/>
<br/>
              6.	Pour juger que le ministre de l'intérieur avait fait une inexacte application des dispositions de l'article 21-27 du code civil, la cour administrative d'appel de Nantes a relevé que M. I... devait être regardé comme bénéficiant de la réhabilitation légale au titre de sa condamnation par jugement du 24 novembre 2000 du tribunal correctionnel d'Aix-en-Provence, alors même que celle-ci continuait de figurer au bulletin n° 2 extrait de son casier judiciaire. Il résulte de ce qui a été dit au point précédent qu'en statuant ainsi, elle n'a pas commis d'erreur de droit. Si le ministre de l'intérieur fait valoir que le sursis dont était assorti la condamnation a été révoqué et produit à l'appui de son pourvoi des éléments faisant état d'une autre condamnation correctionnelle en novembre 2006, ce moyen est nouveau en cassation.<br/>
<br/>
              7.	Il résulte de tout ce qui précède que le ministre de l'intérieur n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi du ministre de l'intérieur est rejeté.<br/>
Article 2 : La présente décision sera notifiée au ministre de l'intérieur et à M. B... I....<br/>
<br/>
              Délibéré à l'issue de la séance du 1er décembre 2021 où siégeaient : Mme Christine Maugüé, présidente adjointe de la section du contentieux, présidant ; M. J... K..., M. Olivier Japiot, présidents de chambre ; M. C... H..., Mme A... L..., M. F... N..., M. G... M..., M. Jean-Yves Ollier, conseillers d'Etat et M. Bertrand Mathieu, conseiller d'Etat en service extraordinaire-rapporteur. <br/>
<br/>
              Rendu le 21 décembre 2021.<br/>
<br/>
<br/>
                 La Présidente : <br/>
                 Signé : Mme Christine Maugüé<br/>
 		Le rapporteur : <br/>
      Signé : M. Bertrand Mathieu<br/>
                 La secrétaire :<br/>
                 Signé : Mme D... E...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-01-01-01-03 DROITS CIVILS ET INDIVIDUELS. - ÉTAT DES PERSONNES. - NATIONALITÉ. - ACQUISITION DE LA NATIONALITÉ. - NATURALISATION. - REFUS DE NATURALISATION FONDÉ SUR UNE CONDAMNATION PÉNALE (ART. 21-27 DU CODE CIVIL) - INTÉRESSÉ AYANT BÉNÉFICIÉ D'UNE RÉHABILITATION DE PLEIN DROIT - ILLÉGALITÉ.
</SCT>
<ANA ID="9A"> 26-01-01-01-03 Si l'article 21-27 du code civil fait obstacle à ce que des personnes ayant fait l'objet de certaines condamnations puissent acquérir la nationalité française, il ne trouve pas à s'appliquer à l'étranger condamné qui a bénéficié d'une réhabilitation de plein droit dans les conditions énoncées par l'article 133-13 du code pénal. La circonstance que la condamnation n'aurait pas été effacée du bulletin n° 2, comme le prévoit le 5° de l'article 775 du code de procédure pénale (CPP), est sans incidence à cet égard.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
