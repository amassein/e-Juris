<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029724735</ID>
<ANCIEN_ID>JG_L_2014_11_000000368159</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/72/47/CETATEXT000029724735.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  2ème sous-section jugeant seule, 05/11/2014, 368159, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-11-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368159</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 2ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Camille Pascal</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:368159.20141105</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 30 avril et 30 juillet 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme B...A..., domiciliée chez FranceTerre d'Asile 4 rue de Doudeauville Dom 106505 BP 383 à Paris (75018) ; Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 12013444 du 28 février 2013 par laquelle la Cour nationale du droit d'asile a rejeté sa demande tendant à l'annulation de la décision du 30 décembre 2011 du directeur de l'Office français de protection des réfugiés et apatrides (OFPRA) rejetant sa demande d'asile et à ce que lui soit reconnue la qualité de réfugié ou, à défaut, à ce que lui soit accordé le bénéfice de la protection subsidiaire ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses demandes présentées devant la Cour nationale du droit d'asile ;<br/>
<br/>
              3°) de mettre à la charge de l'Office français de protection des réfugiés et apatrides le versement de la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention de Genève du 28 juillet 1951 et le protocole signé à New York le 31 janvier 1967 ; <br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le traité sur l'Union européenne, notamment son protocole n° 24 ;<br/>
<br/>
              Vu la directive 2005/85/CE du Conseil du 1er décembre 2005 relative à des normes minimales concernant la procédure d'octroi et de retrait du statut de réfugié dans les Etats membres ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Camille Pascal, conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de Mme A...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que MmeA..., ressortissante russe d'origine tchétchène, s'est vu reconnaître le statut de réfugié par les autorités polonaises sur le fondement des persécutions auxquelles elle était exposée en Fédération de Russie ; qu'elle soutient qu'elle a été l'objet, sur le territoire polonais, de mauvais traitements et d'une tentative de meurtre de la part de son compagnon, également réfugié en Pologne ; qu'elle a quitté la Pologne pour la France en avril 2009, sans avoir été préalablement admise au séjour ; qu'elle soutient que son compagnon l'a rejointe, a tenté de l'assassiner, a été condamné à une peine d'un an de prison ferme par le tribunal de grande instance de Nanterre le 19 avril 2010 et a de nouveau tenté de la retrouver, si bien qu'elle a dû changer d'hébergement ; qu'ayant sollicité le statut de réfugié auprès de l'Office français de protection des réfugiés et apatrides, elle a vu sa demande rejetée par une décision du 30 décembre 2011 ; qu'elle a alors saisi la Cour nationale du droit d'asile qui, par une décision du 28 février 2013, a rejeté sa demande ; qu'elle se pourvoit en cassation contre cette décision ;<br/>
<br/>
              2.	Considérant, en premier lieu, que le moyen tiré de ce que la décision attaquée aurait été rendue à la suite d'une procédure irrégulière faute pour les mémoires échangés entre les parties d'avoir été régulièrement notifiés, n'est pas assorti des précisions nécessaires pour en apprécier le bien-fondé ;<br/>
<br/>
              3.	Considérant, en second lieu, qu'aux termes du 2 du A de l'article 1er de la convention de Genève du 28 juillet 1951 et du protocole signé à New York le 31 janvier 1967, doit être considérée comme réfugiée toute personne " qui, craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut, ou, du fait de cette crainte, ne veut se réclamer de la  protection de ce pays ; ou qui, si elle n'a pas de nationalité et se trouve hors du pays dans lequel elle avait sa résidence habituelle à la suite de tels événements, ne peut ou, en raison de ladite crainte, ne veut y retourner " ; qu'aux termes du 1 de l'article 33 de cette même convention : " Aucun des Etats contractants n'expulsera ou ne refoulera, de quelque manière que ce soit, un réfugié sur les frontières des territoires où sa vie ou sa liberté serait menacée en raison de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques " ;<br/>
<br/>
              4.	Considérant qu'il résulte de ces stipulations que lorsqu'une personne s'est vu reconnaître le statut de réfugié dans un Etat partie à la convention de Genève, sur le fondement de persécutions subies dans l'Etat dont elle a la nationalité, elle ne peut plus, aussi longtemps que le statut de réfugié lui est maintenu et effectivement garanti dans l'Etat qui lui a reconnu ce statut, revendiquer auprès d'un autre Etat, sans avoir été préalablement admise au séjour, le bénéfice des droits qu'elle tient de la convention de Genève à raison de ces persécutions ; <br/>
<br/>
              5.	Considérant, toutefois, qu'une personne qui, s'étant vu reconnaître le statut de réfugié dans un Etat partie à la convention de Genève, sur le fondement de persécutions subies dans l'Etat dont elle a la nationalité, demande néanmoins l'asile en France, doit, s'il est établi qu'elle craint avec raison que la protection à laquelle elle a conventionnellement droit sur le territoire de l'Etat qui lui a déjà reconnu le statut de réfugié n'y est plus effectivement assurée, être regardée comme sollicitant pour la première fois la reconnaissance du statut de réfugié ; qu'il appartient, en pareil cas, aux autorités françaises d'examiner sa demande au regard des persécutions dont elle serait, à la date de sa demande, menacée dans le pays dont elle a la nationalité ; <br/>
<br/>
              6.	Considérant qu'eu égard au niveau de protection des libertés et des droits fondamentaux dans les Etats membres de l'Union européenne, lorsque le demandeur s'est vu en premier lieu reconnaître le statut de réfugié par un Etat membre de l'Union européenne, les craintes dont il fait état quant au défaut de protection dans cet Etat membre doivent en principe être présumées non fondées, sauf à ce que l'intéressé apporte, par tout moyen, la preuve contraire ; que le fait que le demandeur n'ait pas sollicité ou tenté de solliciter la protection des autorités de l'Etat membre qui lui a, en premier lieu, reconnu la qualité de réfugié peut être pris en compte, entre autres éléments, par le juge de l'asile pour apprécier le bien-fondé de sa demande ; <br/>
<br/>
              7.	Considérant que si la Cour nationale du droit d'asile a estimé que les craintes exprimées par Mme A...à l'égard de son ancien compagnon pouvaient être tenues pour fondées, elle a relevé, pour rejeter la demande dont elle était saisie, que la Pologne devait être en l'espèce regardée comme l'Etat garantissant l'exercice des droits attachés à la qualité de réfugiée, que l'intéressée n'établissait pas avoir sollicité ou tenté de solliciter la protection des autorités polonaises, et qu'elle n'avait pas été admise au séjour avant son arrivée en France ; qu'en se prononçant ainsi, la Cour n'a pas commis d'erreur de droit ni dénaturé les pièces du dossier ;<br/>
<br/>
              8.	Considérant qu'il résulte de ce qui précède que Mme A...n'est pas fondée à demander l'annulation de la décision qu'elle attaque ; que les conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme A...est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme B...A...et à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
