<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041965042</ID>
<ANCIEN_ID>JG_L_2020_06_000000436620</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/96/50/CETATEXT000041965042.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 05/06/2020, 436620</TITRE>
<DATE_DEC>2020-06-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436620</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Sylvain Humbert</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:436620.20200605</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire, enregistré le 10 mars 2020 au secrétariat du contentieux du Conseil d'Etat, présenté en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, M. B... A... demande au Conseil d'Etat, à l'appui de son pourvoi tendant à l'annulation du jugement n° 1802383 du 14 octobre 2019 du tribunal administratif de Rennes rejetant sa demande tendant à l'annulation de la décision du 5 avril 2018 par laquelle le ministre de l'action et des comptes publics a rejeté sa demande de révision de sa pension de retraite en lui refusant d'inclure dans les bases de liquidation la bonification d'ancienneté prévue par l'article L. 12 ter du code des pensions civiles et militaires de retraite, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions de cet article.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et ses articles 34 et 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 et la loi organique n° 2020-365 du 30 mars 2020 ;<br/>
              -le code des pensions civiles et militaires de retraite ;<br/>
              - la loi n° 2003-775 du 21 août 2003 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sylvain Humbert, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes de l'article L. 1 du code des pensions civiles et militaires de retraite : " La pension est une allocation pécuniaire personnelle et viagère accordée aux fonctionnaires civils et militaires et, après leur décès, à leurs ayants cause désignés par la loi, en rémunération des services qu'ils ont accomplis jusqu'à la cessation régulière de leurs fonctions. " Aux termes de l'article L. 12 ter du même code, dans sa rédaction issue de l'article 49 de la loi du 21 août 2003 portant réforme des retraites, applicable au litige : " Les fonctionnaires, élevant à leur domicile un enfant de moins de vingt ans atteint d'une invalidité égale ou supérieure à 80 %, bénéficient d'une majoration de leur durée d'assurance d'un trimestre par période d'éducation de trente mois, dans la limite de quatre trimestres ".<br/>
<br/>
              3. En instituant la majoration de pension pour enfant handicapé de l'article L. 12 ter du code des pensions civiles et militaires de retraite, le législateur a, ainsi qu'il ressort des travaux parlementaires préparatoires à l'adoption de la loi du 21 août 2003, entendu faire bénéficier de cet avantage tous les fonctionnaires, y compris les fonctionnaires militaires.<br/>
<br/>
              4. Par suite, le moyen soulevé par M. A... tiré de ce que les dispositions de l'article L. 12 ter du code des pensions civiles et militaires de retraite, telles que résultant de la loi du 21 août 2003, méconnaîtraient le principe d'égalité devant la loi garanti par l'article 6 de la Déclaration des droits de l'homme et du citoyen ainsi que le onzième alinéa du Préambule de la Constitution du 27 octobre 1946, en ce qu'elles excluraient les militaires du bénéfice de la bonification qu'elles prévoient, ne peut être regardé comme soulevant une question sérieuse. <br/>
<br/>
              5. Il s'ensuit qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité que M. A... a soulevé à l'appui de son pourvoi.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. A....<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B... A... et au ministre de l'action et des comptes publics.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">48-02-01-05-01 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. QUESTIONS COMMUNES. AVANTAGES FAMILIAUX. MAJORATION POUR ENFANTS. - MAJORATION DE PENSION POUR ENFANT HANDICAPÉ (ART. L. 12 TER DU CPCMR) - CHAMP D'APPLICATION - ENSEMBLE DES FONCTIONNAIRES, Y COMPRIS MILITAIRES.
</SCT>
<ANA ID="9A"> 48-02-01-05-01 En instituant la majoration de pension pour enfant handicapé de l'article L. 12 ter du code des pensions civiles et militaires de retraite (CPCMR), le législateur a, ainsi qu'il ressort des travaux parlementaires préparatoires à l'adoption de la loi n° 2003-775 du 21 août 2003, entendu faire bénéficier de cet avantage tous les fonctionnaires, y compris les fonctionnaires militaires.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
