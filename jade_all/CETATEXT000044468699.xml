<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044468699</ID>
<ANCIEN_ID>JG_L_2021_12_000000433968</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/46/86/CETATEXT000044468699.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 09/12/2021, 433968</TITRE>
<DATE_DEC>2021-12-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433968</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SARL MEIER-BOURDEAU, LECUYER ET ASSOCIES</AVOCATS>
<RAPPORTEUR>Mme Pauline Berne</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:433968.20211209</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société SAS Brocard Pierre a demandé au tribunal administratif de Châlons-en-Champagne d'annuler la décision du 9 octobre 2015 par laquelle le directeur général de l'établissement national des produits de l'agriculture et de la mer (FranceAgriMer) a rejeté sa demande d'aide à l'investissement pour un montant de 21 896 euros. Par un jugement n° 1600659 du 5 mars 2018, le tribunal administratif de Châlons-en-Champagne a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 18NC01363 du 27 juin 2019, la cour administrative d'appel de Nancy a, sur l'appel de la société SAS Brocard Pierre, annulé ce jugement et la décision du directeur général de FranceAgriMer du 9 octobre 2015. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 27 août et 25 novembre 2019 au secrétariat du contentieux du Conseil d'Etat, FranceAgriMer demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les demandes de la société Brocard Pierre ;<br/>
<br/>
              3°) de mettre à la charge de la société SAS Brocard Pierre la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le règlement (CE) n° 479/2008 du Conseil du 29 avril 2008 portant organisation commune du marché vitivinicole ;<br/>
              - le règlement n° 555/2008 de la Commission du 27 juin 2008 relatif aux modalités de mise en œuvre des mesures retenues au titre du plan national d'aide au secteur vitivinicole financé par les enveloppes nationales définies par le règlement (CE) n° 479/2008 du Conseil de l'Union européenne du 29 avril 2008 ;<br/>
              - le règlement (UE) n° 1308/2013 du Parlement européen et du Conseil du 17 décembre 2013 portant organisation commune des marchés des produits agricoles et abrogeant les règlements (CEE) n° 922/72, (CEE) n° 1037/2001 et (CE) n° 1234/2007 du Conseil ;<br/>
              - la loi n° 79-587 du 11 juillet 1979 ;<br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - le décret n° 2013-172 du 25 février 2013 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Berne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SARL Meier-Bourdeau, Lecuyer et associés, avocat de l'établissement national des produits de L'agriculture et de la mer (FranceAgriMer) ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le 6 janvier 2014, la société par actions simplifiées (SAS) Brocard Pierre, producteur récoltant de champagne, a déposé une demande d'aide pour l'acquisition de matériels auprès de l'établissement national des produits de l'agriculture et de la mer (FranceAgriMer) notamment chargé de la mise en œuvre du programme adopté par la France en application de l'article 7 du règlement (CE) n° 479/2008 du Conseil de l'Union européenne du 29 avril 2008 portant organisation commune du marché vitivinicole auquel a succédé le règlement (UE) n° 1308/2013 du Parlement européen et du Conseil du 17 décembre 2013 portant organisation commune des marchés des produits agricoles. En application de l'article 5 de la décision du directeur général de FranceAgriMer FILITL/SEMD 2013-76 du 4 décembre 2013, qui précise les modalités d'examen des demandes d'aide, FranceAgriMer, par un courrier du 20 janvier 2014, a accusé réception de la demande de la société et autorisé celle-ci à commencer les travaux à compter du 6 janvier 2014. Par un courrier de notification du 25 août 2014 accompagné d'une décision d'octroi de l'aide, elle lui a ensuite notifié une décision d'éligibilité à une aide pour un montant de 20 230 euros, correspondant à 57 800 euros de dépenses éligibles. Par un courrier du 9 octobre 2015, FranceAgriMer, après avoir procédé à l'instruction du dossier de demande de paiement de la société, a toutefois informé celle-ci du rejet de sa demande de versement de l'aide, faute d'avoir respecté l'ensemble des conditions d'éligibilité prévues. Par un jugement du 5 mars 2018, le tribunal administratif de Châlons-en-Champagne a rejeté la demande de la société tendant à l'annulation de cette décision de refus de paiement. FranceAgriMer se pourvoit en cassation contre l'arrêt du 27 juin 2019 par lequel la cour administrative d'appel de Nancy a annulé ce jugement et fait droit aux conclusions de la société.<br/>
<br/>
              2. Aux termes de la décision du directeur général de FranceAgriMer FILITL/SEMD 2013-76 du 4 décembre 2013, qui, en application de l'article 1er du décret du 25 février 2013  relatif au programme d'aide national au secteur vitivinicole pour les exercices financiers 2014 à 2018, précise notamment la procédure d'instruction des demandes d'aides aux programmes d'investissement des entreprises pour les exercices financiers de cette période, la demande d'aide donne lieu, tout d'abord, à un accusé de réception, sans engagement financier, valant autorisation de commencer les travaux à compter de la date de la demande, ensuite, après que le dossier a été complété et instruit par FranceAgriMer, à une décision d'octroi de l'aide, précisant les dépenses éligibles, le montant de l'aide et les obligations du bénéficiaire, et enfin, sur demande de paiement présentée par ce dernier, assortie des éléments permettant de vérifier la réalisation des actions prévues conformément aux conditions posées, et après contrôle de cette réalisation par FranceAgriMer, à  une décision de versement de l'aide. <br/>
<br/>
              3. Aux termes de l'article 1er de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public, dans sa rédaction en vigueur à la date de la décision en litige : " Les personnes physiques ou morales ont le droit d'être informées sans délai des motifs des décisions administratives individuelles défavorables qui les concernent. / A cet effet, doivent être motivées les décisions qui : (...) retirent ou abrogent une décision créatrice de droits ; (...) refusent un avantage dont l'attribution constitue un droit pour les personnes qui remplissent les conditions légales pour l'obtenir (...) ". Aux termes de l'article 24 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations, dans sa rédaction alors en vigueur : " Exception faite des cas où il est statué sur une demande, les décisions individuelles qui doivent être motivées en application des articles 1er et 2 de la loi n° 79-587 du 11 juillet 1979 relative à la motivation des actes administratifs et à 1'amélioration des relations entre l'administration et le public n'interviennent qu'après que la personne intéressée a été mise à même de présenter des observations écrites et, le cas échéant, sur sa demande, des observations orales. "<br/>
<br/>
              4. D'une part, la décision du 25 août 2014 par laquelle FranceAgriMer a octroyé à la société Brocard Pierre une aide d'un montant de 20 230 euros constituait une décision créatrice de droits, quand bien même ces droits étaient subordonnés au respect de diverses conditions et à la présentation, dans un délai de deux mois après la date limite de réalisation des travaux, d'une demande de paiement assortie des justificatifs permettant de vérifier ce respect. La décision du 9 octobre 2015 par laquelle FranceAgriMer a refusé de verser cette aide à la société Brocard Pierre, qui était motivée par le constat, ressortant des justificatifs produits par la société à l'appui de sa demande de paiement de l'aide, que la condition concernant la date de commencement autorisé des travaux n'avait pas été respectée, se bornait à exécuter cette décision d'octroi en tirant les conséquences du non-respect des conditions posées par cette dernière et n'en constituait donc pas le retrait. <br/>
<br/>
              5. D'autre part, compte tenu des droits créés par la décision d'octroi de l'aide, cette décision de refus de versement doit être regardée comme refusant un avantage dont l'attribution constitue un droit pour les personnes qui remplissent les conditions légales pour l'obtenir, et devait, à ce titre, en application des dispositions de l'article 1er de la loi du 11 juillet 1979, être motivée. Toutefois, dès lors que cette décision faisait suite à une demande de la société tendant au versement de l'aide octroyée, après examen des justificatifs à fournir à l'appui de cette demande, elle n'était pas au nombre des décisions soumises par les dispositions précitées de l'article 24 de la loi du 12 avril 2000 à la procédure contradictoire qu'elles instituent. <br/>
<br/>
              6. Il résulte de ce qui précède qu'en jugeant que la décision du 9 octobre 2015 de FranceAgriMer rejetant la demande de versement de l'aide constituait le retrait d'une décision créatrice de droits d'une part, et qu'elle était intervenue à l'issue d'une procédure irrégulière faute d'avoir fait l'objet d'une procédure contradictoire d'autre part, la cour a entaché sa décision d'erreurs de droit. <br/>
<br/>
              7. Il résulte de ce qui précède que FranceAgriMer est fondée à demander l'annulation de l'arrêt qu'elle attaque. <br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la SAS Brocard Pierre le versement d'une somme de 3 000 euros à FranceAgriMer au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt du 27 juin 2019 de la cour administrative d'appel de Nancy est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy. <br/>
Article 3 : La société SAS Brocard Pierre versera à FranceAgriMer la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à FranceAgriMer et à la société SAS Brocard Pierre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-06-02-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. - DIFFÉRENTES CATÉGORIES D'ACTES. - ACTES ADMINISTRATIFS - CLASSIFICATION. - ACTES INDIVIDUELS OU COLLECTIFS. - ACTES CRÉATEURS DE DROITS. - 1) OCTROI D'UNE AIDE AUX PROGRAMMES D'INVESTISSEMENT DES ENTREPRISES - CARACTÈRE D'ACTE CRÉATEUR DE DROIT - EXISTENCE - 2) REFUS DE VERSER CETTE AIDE POUR NON-RESPECT D'UNE DES CONDITIONS PRÉVUES - A) RETRAIT DE LA DÉCISION D'OCTROI - ABSENCE [RJ1] - B) OBLIGATION DE MOTIVATION - EXISTENCE - C) SOUMISSION À UNE PROCÉDURE CONTRADICTOIRE - ABSENCE, DÈS LORS QU'ELLE FAIT SUITE À UNE DEMANDE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-03-01-02-01-01-04 ACTES LÉGISLATIFS ET ADMINISTRATIFS. - VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. - QUESTIONS GÉNÉRALES. - MOTIVATION. - MOTIVATION OBLIGATOIRE. - MOTIVATION OBLIGATOIRE EN VERTU DES ARTICLES 1 ET 2 DE LA LOI DU 11 JUILLET 1979. - DÉCISION REFUSANT UN AVANTAGE DONT L'ATTRIBUTION CONSTITUE UN DROIT. - REFUS DE VERSER UNE AIDE AUX PROGRAMMES D'INVESTISSEMENT DES ENTREPRISES EN RAISON DU NON-RESPECT D'UNE DES CONDITIONS PRÉVUES.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">01-03-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. - VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. - PROCÉDURE CONTRADICTOIRE. - REFUS DE VERSER UNE AIDE AUX PROGRAMMES D'INVESTISSEMENT DES ENTREPRISES EN RAISON DU NON-RESPECT D'UNE DES CONDITIONS PRÉVUES, PRISE À LA SUITE D'UNE DEMANDE DE PAIEMENT [RJ2].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">01-09-01-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. - DISPARITION DE L'ACTE. - RETRAIT. - RETRAIT DES ACTES CRÉATEURS DE DROITS. - REFUS DE VERSER UNE AIDE AUX PROGRAMMES D'INVESTISSEMENT DES ENTREPRISES EN RAISON DU NON-RESPECT D'UNE DES CONDITIONS PRÉVUES - RETRAIT DE LA DÉCISION D'OCTROI - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 01-01-06-02-01 1) La décision par laquelle FranceAgriMer octroie une aide aux programmes d'investissement des entreprises constitue une décision créatrice de droits, quand bien même ces droits sont subordonnés au respect de diverses conditions et à la présentation, dans un délai de deux mois après la date limite de réalisation des travaux, d'une demande de paiement assortie des justificatifs permettant de vérifier ce respect. ......2) a) La décision par laquelle FranceAgriMer refuse de verser cette aide au bénéficiaire, motivée par le constat, ressortant des justificatifs produits par le bénéficiaire à l'appui de sa demande de paiement de l'aide, que la condition concernant la date de commencement autorisé des travaux n'a pas été respectée, se borne exécuter cette décision d'octroi en tirant les conséquences du non-respect des conditions posées par cette dernière et n'en constitue donc pas le retrait. ......b) Compte tenu des droits créés par la décision d'octroi de l'aide, cette décision de refus de versement doit être regardée comme refusant un avantage dont l'attribution constitue un droit pour les personnes qui remplissent les conditions légales pour l'obtenir, et doit, à ce titre, en application de l'article 1er de la loi n° 79-587 du 11 juillet 1979, être motivée. ......c) Toutefois, dès lors que cette décision fait suite à une demande du bénéficiaire tendant au versement de l'aide octroyée, après examen des justificatifs à fournir à l'appui de cette demande, elle n'est pas au nombre des décisions soumises par l'article 24 de la loi n° 2000-321 du 12 avril 2000 à la procédure contradictoire qu'il institue.</ANA>
<ANA ID="9B"> 01-03-01-02-01-01-04 La décision par laquelle FranceAgriMer octroie une aide aux programmes d'investissement des entreprises constitue une décision créatrice de droits, quand bien même ces droits sont subordonnés au respect de diverses conditions et à la présentation, dans un délai de deux mois après la date limite de réalisation des travaux, d'une demande de paiement assortie des justificatifs permettant de vérifier ce respect. ......La décision par laquelle FranceAgriMer refuse de verser cette aide au bénéficiaire, motivée par le constat, ressortant des justificatifs produits par le bénéficiaire à l'appui de sa demande de paiement de l'aide, que la condition concernant la date de commencement autorisé des travaux n'a pas été respectée, se borne exécuter cette décision d'octroi en tirant les conséquences du non-respect des conditions posées par cette dernière et n'en constitue donc pas le retrait. ......Compte tenu des droits créés par la décision d'octroi de l'aide, cette décision de refus de versement doit être regardée comme refusant un avantage dont l'attribution constitue un droit pour les personnes qui remplissent les conditions légales pour l'obtenir, et doit, à ce titre, en application de l'article 1er de la loi n° 79-587 du 11 juillet 1979, être motivée.</ANA>
<ANA ID="9C"> 01-03-03 La décision par laquelle FranceAgriMer octroie une aide aux programmes d'investissement des entreprises constitue une décision créatrice de droits, quand bien même ces droits sont subordonnés au respect de diverses conditions et à la présentation, dans un délai de deux mois après la date limite de réalisation des travaux, d'une demande de paiement assortie des justificatifs permettant de vérifier ce respect. ......La décision par laquelle FranceAgriMer refuse de verser cette aide au bénéficiaire, motivée par le constat, ressortant des justificatifs produits par le bénéficiaire à l'appui de sa demande de paiement de l'aide, que la condition concernant la date de commencement autorisé des travaux n'a pas été respectée, se borne exécuter cette décision d'octroi en tirant les conséquences du non-respect des conditions posées par cette dernière et n'en constitue donc pas le retrait. ......Dès lors que cette décision fait suite à une demande du bénéficiaire tendant au versement de l'aide octroyée, après examen des justificatifs à fournir à l'appui de cette demande, elle n'est pas au nombre des décisions soumises par l'article 24 de la loi n° 2000-321 du 12 avril 2000 à la procédure contradictoire qu'il institue.</ANA>
<ANA ID="9D"> 01-09-01-02 La décision par laquelle FranceAgriMer octroie une aide aux programmes d'investissement des entreprises constitue une décision créatrice de droits, quand bien même ces droits sont subordonnés au respect de diverses conditions et à la présentation, dans un délai de deux mois après la date limite de réalisation des travaux, d'une demande de paiement assortie des justificatifs permettant de vérifier ce respect. ......La décision par laquelle FranceAgriMer refuse de verser cette aide au bénéficiaire, motivée par le constat, ressortant des justificatifs produits par le bénéficiaire à l'appui de sa demande de paiement de l'aide, que la condition concernant la date de commencement autorisé des travaux n'a pas été respectée, se borne exécuter cette décision d'octroi en tirant les conséquences du non-respect des conditions posées par cette dernière et n'en constitue donc pas le retrait.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., s'agissant d'une décision imposant au bénéficiaire d'une aide agricole versée en application d'un texte de l'Union européenne de reverser les montants indûment perçus, CE, Section, 13 mars 2015, Office de développement de l'économie agricole d'outre-mer, n° 364612, p. 84....[RJ2] Comp., s'agissant du retrait d'une subvention, CE, 4 octobre 2021, Agence de l'eau Rhône Méditerranée Corse, n° 438695, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
