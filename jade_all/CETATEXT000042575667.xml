<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042575667</ID>
<ANCIEN_ID>JG_L_2020_11_000000426545</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/57/56/CETATEXT000042575667.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 27/11/2020, 426545, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426545</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY ; SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:426545.20201127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Lyon de condamner l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) à lui verser la somme de 343 707,52 euros en réparation des préjudices qu'il estime avoir subis en raison de l'aléa thérapeutique dont il a été victime ainsi qu'une somme de 10 000 euros au titre de dommages et intérêts. Par un jugement n° 1305586 du 7 avril 2016, le tribunal administratif a condamné l'ONIAM à verser à M. B... la somme de 54 242,92 euros au titre des préjudices imputables à l'aléa thérapeutique dont il a été victime et a rejeté le surplus de sa demande.<br/>
<br/>
              Par un arrêt n° 16LY01977 du 25 octobre 2018, la cour administrative d'appel de Lyon a, sur appel de M. B..., porté à 71 917,41 euros la somme que l'ONIAM est condamné à lui verser et rejeté le surplus de sa requête.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 24 décembre 2018, 25 mars 2019 et 8 juin 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il rejette le surplus de son appel ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'ONIAM la somme de 3500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grévy, avocat de M. B... et à la SCP Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B... a subi aux hospices civils de Lyon une opération de chirurgie du genou à la suite de laquelle il a développé une algodystrophie occasionnant des douleurs importantes. A défaut d'accord avec l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) sur l'indemnisation de plusieurs chefs de préjudices, il a saisi le tribunal administratif de Lyon qui, par un jugement du 7 avril 2016, a condamné l'ONIAM à lui verser la somme de 54 242,92 euros. Sur appel de M. B..., la cour administrative d'appel de Lyon a porté la condamnation de l'ONIAM à la somme de 71 917,40 euros. M. B... se pourvoit en cassation contre cet arrêt en tant qu'il rejette le surplus de ses conclusions. Par une décision du 10 février 2020, le Conseil d'Etat statuant au contentieux a admis uniquement les conclusions de son pourvoi dirigées contre l'arrêt attaqué en tant qu'il se prononce sur l'indemnisation des gains professionnels entre septembre 2009 et septembre 2011 ainsi que sur l'indemnisation du préjudice d'aménagement du logement.<br/>
<br/>
              Sur l'indemnisation des gains professionnels entre septembre 2009 et septembre 2011 :<br/>
<br/>
              2. Il ressort des énonciations, non contestées, de l'arrêt attaqué que le salaire annuel moyen de M. B... devait être évalué à 31 324 euros au titre des années 2006 à 2008 et aurait dû se maintenir à ce niveau entre 2009 et 2011. Par suite, en estimant à 9 066 euros, après soustraction d'un revenu effectivement perçu de 40 299,73 euros, l'indemnité due à M. B... au titre des pertes de gains professionnels, l'arrêt attaqué a, par son évaluation insuffisante des salaires que l'intéressé aurait dû percevoir pendant la période de deux ans allant de septembre 2009 à septembre 2011, entaché de dénaturation son appréciation des pièces du dossier.<br/>
<br/>
              Sur le préjudice d'aménagement du logement :<br/>
<br/>
              3. Il ressort des termes mêmes de l'arrêt attaqué que l'infirmité ayant résulté, pour M. B..., de l'intervention subie dans l'établissement de santé, imposait des travaux d'aménagement de son logement. Par suite, en écartant l'intégralité de la demande indemnitaire présentée à ce titre par le requérant au motif que, parmi les travaux qu'il avait réalisés et dont il demandait le remboursement, la part exacte de ceux qui découlaient nécessairement de son état de santé n'était pas établie, la cour a également entaché son arrêt d'une dénaturation des pièces du dossier qui lui était soumis.<br/>
<br/>
              4. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de M. B..., qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de faire application de ces dispositions et de mettre à la charge de l'ONIAM une somme de 3000 euros à verser, à ce titre, à M. B....<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 25 octobre 2018 est annulé en tant qu'il ne fixe qu'à 9 066 euros l'indemnité allouée à M. B... au titre des pertes de gains professionnels entre septembre 2009 et septembre 2011 et en tant qu'il statue sur l'indemnisation des frais d'aménagement du logement.<br/>
<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Lyon dans la limite de la cassation prononcée à l'article 1er.<br/>
<br/>
Article 3 : L'ONIAM versera à M. B... la somme de 3000 euros au titre des dispositions de l'article L.761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions de l'ONIAM présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. B..., à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales et au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
