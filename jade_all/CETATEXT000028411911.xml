<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028411911</ID>
<ANCIEN_ID>JG_L_2013_12_000000368540</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/41/19/CETATEXT000028411911.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 26/12/2013, 368540, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-12-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368540</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Esther de Moustier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:368540.20131226</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 15 mai 2013 au secrétariat du contentieux du Conseil d'Etat, présentée par la société Deutsche Bahn AG, dont le siège est situé Potsdamer Platz 2, à Berlin (10785), Allemagne, représentée par ses fondés de pouvoir ; la société demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'instruction publiée au Bulletin officiel des finances publiques sous le titre BOI-TFP-IFER-70, à jour au 19 mars 2013, en tant qu'elle prévoit les conditions d'assujettissement à l'imposition forfaitaire sur les entreprises de réseaux (IFER) des entreprises de transport ferroviaire qui n'ont pas leur siège en France ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat une somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;  <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et le premier protocole additionnel à cette convention ;<br/>
<br/>
              Vu le traité sur le fonctionnement de l'Union européenne ;<br/>
<br/>
              Vu la directive 95/18/CE du Conseil du 19 juin 1995 ;<br/>
<br/>
              Vu la directive 2001/14/CE du Parlement européen et du Conseil du 26 février 2001 ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Esther de Moustier, Auditeur,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article 56 du traité sur le fonctionnement de l'Union européenne : " Dans le cadre des dispositions ci-après, les restrictions à la libre prestation des services à l'intérieur de l'Union sont interdites à l'égard des ressortissants des Etats membres établis dans un Etat membre autre que celui du destinataire de la prestation. " ; qu'aux termes du premier alinéa de l'article 58 du même traité : " 1. La libre circulation des services, en matière de transports, est régie par les dispositions du titre relatif aux transports. " ; que si l'article 56 précité ne s'applique pas en tant que tel aux services de transports, ces derniers étant régis par le titre du traité relatif aux transports, le principe de la libre prestation des services n'en est pas moins applicable à cette activité ; que ce principe s'oppose à l'application de toute réglementation nationale ayant pour effet de rendre la prestation de services entre Etats-membres plus difficile que la prestation de services purement interne à un Etat-membre ; qu'une telle réglementation ne peut être admise que si elle se justifie par des raisons impérieuses d'intérêt général et est proportionnée à la réalisation des objectifs qu'elle poursuit, c'est-à-dire si elle est propre à garantir ces objectifs et si elle ne va pas au-delà de ce qui est nécessaire pour les atteindre ; <br/>
<br/>
              2. Considérant que la loi du 30 décembre 2009 de finances pour 2010 a créé un article 1635-0 quinquies au sein du code général des impôts instaurant, au profit des collectivités territoriales ou de leurs établissements publics de coopération intercommunale, une imposition forfaitaire sur les entreprises de réseaux ; qu'aux termes de l'article 1599 quater A du code général des impôts : " I.- L'imposition forfaitaire mentionnée à l'article 1635-0  quinquies s'applique au matériel roulant utilisé sur le réseau ferré national pour des opérations de transport de voyageurs. / II.- L'imposition forfaitaire est due chaque année par l'entreprise de transport ferroviaire qui dispose, pour les besoins de son activité professionnelle au 1er janvier de l'année d'imposition, de matériel roulant ayant été utilisé l'année précédente sur le réseau ferré national pour des opérations de transport de voyageurs. / III.- (...) Les matériels roulants retenus pour le calcul de l'imposition sont ceux dont les entreprises ferroviaires ont la disposition au 1er janvier de l'année d'imposition et qui sont destinés à être utilisés sur le réseau ferré national pour des opérations de transport de voyageurs. (...)  Ne sont pas retenus pour le calcul de l'imposition les matériels roulants destinés à circuler en France exclusivement sur les sections du réseau ferré national reliant, d'une part, une intersection entre le réseau ferré national et une frontière entre le territoire français et le territoire d'un Etat limitrophe et, d'autre part, la gare française de voyageurs de la section concernée la plus proche de cette frontière. / (...) IV.- Le redevable de la taxe déclare, au plus tard le deuxième jour ouvré suivant le 1er mai de l'année d'imposition, le nombre de matériels roulants par catégorie. " ; <br/>
<br/>
              3. Considérant que les entreprises de transport ferroviaire sont assujetties à l'imposition forfaitaire sur les entreprises de réseaux à raison de leur matériel roulant destiné à être utilisé sur le réseau ferré national pour des opérations de transport de voyageurs, pour un montant forfaitaire, indistinctement selon le lieu de leur siège, sans que soit prise en compte la circulation effective de ces matériels roulants sur le réseau ferré national ; qu'il n'est pas contesté que le matériel roulant des entreprises de transport ferroviaire n'ayant pas leur siège en France qui circule sur le réseau ferré national circule également, de manière non résiduelle, sur d'autres réseaux ferrés que le réseau français, tandis que le matériel roulant des opérateurs nationaux circule à titre principal sur le réseau ferré national ; que le ministre ne soutient pas que la part de l'imposition forfaitaire sur les entreprises de réseaux acquittée par les opérateurs ferroviaires établis hors de France serait proportionnée à leur circulation effective sur le réseau ferré national ; que l'imposition forfaitaire sur les entreprises de réseaux en matière de transport ferroviaire, pèse proportionnellement deux fois plus lourdement sur les opérateurs établis hors de France que sur les opérateurs nationaux ; que la restriction que cette imposition apporte ainsi à l'exercice de leur activité par les entreprises de transport ferroviaire établies hors de France porte atteinte à la libre prestation de services ; qu'une telle atteinte n'est pas proportionnée aux objectifs de l'imposition en cause, dès lors qu'il n'est pas établi que ceux-ci ne pourraient être atteints par l'instauration d'une taxation tenant compte de la circulation effective sur le réseau ferré national des matériels roulants au titre desquels les opérateurs ferroviaires n'ayant pas leur siège en France sont assujettis ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que la société Deutsche Bahn AG est fondée à soutenir que l'instruction BOI-TFP-IFER-70 publiée au Bulletin officiel des finances publiques le 19 mars 2013, qui réitère des dispositions législatives méconnaissant les stipulations de l'article 56 du traité sur le fonctionnement de l'Union européenne, est illégale en tant qu'elle prévoit les conditions d'assujettissement à l'imposition forfaitaire sur les entreprises de réseaux des entreprises de transport ferroviaire qui n'ont pas leur siège en France, et à en demander l'annulation sur ce point ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à la société Deutsche Bahn AG, au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>		D E C I D E :<br/>
              		--------------<br/>
<br/>
 Article 1er : L'instruction fiscale BOI-TFP-IFER-70 est annulée en tant qu'elle prévoit les conditions d'assujettissement à l'imposition forfaitaire sur les entreprises de réseaux due au titre du matériel ferroviaire roulant utilisé sur le réseau ferré national pour les opérations de transport de voyageurs des entreprises de transport ferroviaire qui n'ont pas leur siège en France.<br/>
<br/>
 Article 2 : L'Etat versera à la société Deutsche Bahn AG une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
 Article 3 : La présente décision sera notifiée à la Société Deutsche Bahn AG et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
