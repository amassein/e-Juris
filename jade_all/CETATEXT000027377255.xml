<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027377255</ID>
<ANCIEN_ID>JG_L_2013_04_000000344749</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/37/72/CETATEXT000027377255.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 29/04/2013, 344749, Publié au recueil Lebon</TITRE>
<DATE_DEC>2013-04-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>344749</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. Marc Perrin de Brichambaut</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:344749.20130429</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 6 décembre 2010 et 7 février 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme A...E..., veuveB..., demeurant..., Mme D...B..., demeurant au..., venant aux droits de M. C...B..., leur mari et père décédé ; MmeE..., veuve B...et Mme B...demandent au Conseil d'Etat d'annuler l'arrêt n° 09/00112-09/00125 du 17 juin 2010 par lequel la cour régionale des pensions d'Aix-en-Provence, faisant droit à l'appel formé par le ministre de la défense contre le jugement n° 07-06536 du tribunal départemental des pensions du Var en date du 25 août 2009 reconnaissant à M. C... B...un droit à pension au taux de 100 % au titre d'un carcinome bronchique jugé imputable à son service dans la marine nationale, a infirmé ce jugement et rejeté les prétentions des intéressées ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des pensions militaires d'invalidité et des victimes de la guerre ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Perrin de Brichambaut, Conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de Mme A...E..., veuve B...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du 2° de l'article L. 2 du code des pensions militaires d'invalidité et des victimes de la guerre, ouvrent droit à pension les infirmités résultant de " maladies contractées par le fait ou à l'occasion du service " ; que l'article L. 3 institue une présomption d'imputabilité, qui bénéficie à l'intéressé à condition que la maladie ait été constatée après le 90ème jour de service effectif et avant le 60ème jour suivant le retour du militaire dans ses foyers et que soit établie médicalement la filiation entre la maladie et l'infirmité invoquée ; qu'aux termes du 2° de l'article L. 43, la veuve d'un militaire a droit à pension si la mort de ce dernier a été causée par des maladies contractées ou aggravées par le fait ou à l'occasion du service, dans les conditions définies aux articles L. 2 et L. 3 ; <br/>
<br/>
              2. Considérant qu'il résulte des dispositions combinées des articles L. 2 et L. 3 que, lorsque le demandeur d'une pension ne peut bénéficier de la présomption légale d'imputabilité et que, par ailleurs, cette imputabilité n'est pas admise par l'administration, il incombe à l'intéressé d'apporter la preuve de l'imputabilité de l'affection au service par tous moyens de nature à emporter la conviction des juges ; que, dans les cas où est en cause une affection à évolution lente et susceptible d'être liée à l'exposition du militaire à un environnement ou à des substances toxiques, il appartient aux juges du fond de prendre en considération les éléments du dossier relatifs à l'exposition du militaire à cet environnement ou à ces substances, eu égard notamment aux tâches ou travaux qui lui sont confiés, aux conditions dans lesquelles il a été conduit à les exercer, aux conditions et à la durée de l'exposition ainsi qu'aux pathologies que celle-ci est susceptible de provoquer ; qu'il revient ensuite aux juges du fond de déterminer si, au vu des données admises de la science, il existe une probabilité suffisante que la pathologie qui affecte le demandeur soit en rapport avec son activité professionnelle ; que, lorsque tel est le cas, la seule circonstance que la pathologie pourrait avoir été favorisée par d'autres facteurs ne suffit pas, à elle seule, à écarter la preuve de l'imputabilité, si l'administration n'est pas en mesure d'établir que ces autres facteurs ont été la cause déterminante de la pathologie ; <br/>
<br/>
              3. Considérant que, pour juger, contrairement au tribunal départemental des pensions militaires du Var, que le cancer broncho-pulmonaire dont a souffert M. B...et qui a causé son décès n'était pas lié au service, la cour ne s'est fondée que sur la " spécialité " de manoeuvrier de l'intéressé, pour en déduire que son exposition à l'amiante avait été limitée, sans prendre en compte ni les différentes tâches que M. B...a exercées au cours de ses trente années de service sur différents navires de la marine nationale, ni les conditions particulières dans lesquelles il les a exercées, ni, enfin, les conditions et la durée de son exposition à l'amiante ; que, faute d'avoir pris en compte ces éléments, sur lesquels l'intéressé avait fourni des justifications, la cour a commis une erreur de droit ; que, dès lors, et sans qu'il soit besoin d'examiner les autres moyens de leur pourvoi, les requérantes sont fondées à demander l'annulation de l'arrêt de la cour ;<br/>
<br/>
              4. Considérant que, dans les circonstances de l'espèce, il y a lieu de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant qu'il résulte de l'instruction que M. C...B...a été diagnostiqué le 3 janvier 2007 comme souffrant d'un carcinome broncho-pulmonaire entraînant une invalidité de 100 % ; qu'il a saisi le tribunal départemental des pensions militaires du Var de la décision du ministre de la défense du 15 octobre 2007 rejetant sa demande de pension, déposée le 23 février 2007 au titre de cette pathologie ; <br/>
<br/>
              6. Considérant, en premier lieu, qu'il résulte de l'instruction que, sur les navires de la marine nationale construits jusqu'à la fin des années quatre-vingt, l'amiante était utilisée de façon courante comme isolant pour calorifuger tant les tuyauteries que certaines parois et certains équipements de bord, de même que les réacteurs et moteurs des avions de l'aéronavale ; que ces matériaux d'amiante ont tendance à se déliter du fait des contraintes physiques imposées à ces matériels, de la chaleur, du vieillissement du calorifugeage, ou de travaux d'entretien en mer ou au bassin ; qu'en conséquence, les marins servant sur les bâtiments de la marine nationale, qui ont vécu et travaillé dans un espace souvent confiné, sont susceptibles d'avoir été exposés à l'inhalation de poussières d'amiante ; <br/>
<br/>
              7. Considérant, en deuxième lieu, qu'il résulte de l'instruction que M. C...B..., né en 1950, s'est engagé le 27 novembre 1967 dans la marine nationale ; qu'il a, au cours de sa carrière, été affecté, pour la période du 8 mai 1968 au 5 juin 1998, sur dix navires, ainsi que dans les ports de Hao et de Mururoa ; qu'il a notamment été affecté, pendant sept ans, sur le porte-avions " Clémenceau ", sur lequel les gaines entourant les sources de chaleur et les protections thermiques des soutes à munitions et à carburant contenaient une quantité d'amiante significative ; qu'il ressort de l'attestation du directeur du personnel militaire de la marine du 7 février 2007 que M. B...a, au cours de ses affectations ou mises pour emploi, été exposé aux risques liés à l'inhalation de poussières d'amiante ; qu'eu égard aux tâches particulières qui lui étaient confiées, M. B...a été particulièrement exposé à ces risques ; qu'il a ainsi, durant ses affectations au port de Mururoa, à Fangataufa et à la base avancée de Hao, effectué pendant quatre années des tâches d'entretien d'engins comportant notamment le démontage de tuyauteries, des réparations sur des plaques d'amiante et le changement de joints d'amiante, sans disposer de protections contre l'inhalation de poussières d'amiante ; qu'il a été, durant sept ans, directeur du pont d'envol et du hangar aéronautique sur le porte-avions " Clémenceau " et était alors directement exposé aux poussières d'amiante produites par les avions et leurs réacteurs ; qu'il était en outre revêtu d'une tenue en amiante, dont il assurait lui-même l'entretien ; que sa cabine était directement située sous le pont d'envol ; qu'il ressort  par ailleurs des témoignages versés au dossier que l'intéressé a été conduit à effectuer des travaux d'entretien sur les navires sur lesquels il a servi, qui le mettaient en contact avec des éléments amiantés ; qu'enfin, les travaux effectués à terre pendant les périodes d'indisponibilité l'ont conduit à effectuer des réparations dans un milieu où l'amiante était présente en abondance ; <br/>
<br/>
              8. Considérant, en troisième lieu, qu'il est admis, sur le plan scientifique, que l'inhalation de poussières d'amiante, sur une durée longue, peut, à plus ou moins long terme, et parfois vingt à trente ans après l'exposition, être la cause de cancers bronchiques mortels ; qu'il ressort, au surplus, de deux certificats médicaux établis le 16 janvier 2007 par le chef du service de pneumologie de l'hôpital d'instruction des armées Sainte-Anne et le 17 juillet 2007 par un médecin de l'unité d'oncologie thoracique de l'hôpital Sainte-Marguerite à Marseille que le cancer bronchique de M. B...est lié à une exposition professionnelle à l'amiante ;<br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que, compte tenu, d'une part, du lien, admis par la science, entre l'exposition à l'amiante et les cancers bronchiques, d'autre part, des éléments établissant que M. B... a été exposé pendant trente ans à un environnement professionnel à forte présence d'amiante ainsi que des conditions dans lesquelles il exerçait ses fonctions, la preuve de l'imputabilité au service de sa pathologie doit être regardée comme établie ; <br/>
<br/>
              10. Considérant, enfin, que si le ministre de la défense fait valoir que M. B...aurait été consommateur de tabac, il ne résulte pas de l'instruction que cette tabagie constituerait la cause déterminante ou exclusive de sa pathologie ; <br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui précède que le ministre de la défense n'est pas fondé à demander l'annulation du jugement du tribunal départemental des pensions du Var reconnaissant à M. B...le droit à une pension militaire d'invalidité au taux de 100 % ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt du 17 juin 2010 de la cour régionale des pensions d'Aix-en Provence est annulé.<br/>
<br/>
Article 2 : L'appel présenté par le ministre de la défense devant la cour régionale des pensions d'Aix-en-Provence est rejeté.<br/>
<br/>
Article 3 : La présente décision sera notifiée à Mme A...E..., veuveB..., à Mme D... B...et au ministre de la défense.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">48-01-02-03-01 PENSIONS. PENSIONS MILITAIRES D'INVALIDITÉ ET DES VICTIMES DE GUERRE. CONDITIONS D'OCTROI D'UNE PENSION. IMPUTABILITÉ. LIEN DE CAUSALITÉ MÉDICALE. - PREUVE DE L'IMPUTABILITÉ AU SERVICE, EN L'ABSENCE DE PRÉSOMPTION LÉGALE - AFFECTION À ÉVOLUTION LENTE - MÉTHODE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. POUVOIRS DU JUGE DE PLEIN CONTENTIEUX. - JURIDICTION DES PENSIONS - PENSIONS MILITAIRES D'INVALIDITÉ ET DES VICTIMES DE GUERRE - PREUVE DE L'IMPUTABILITÉ AU SERVICE EN L'ABSENCE DE PRÉSOMPTION LÉGALE - AFFECTIONS À ÉVOLUTION LENTE - MÉTHODE [RJ1].
</SCT>
<ANA ID="9A"> 48-01-02-03-01 Il résulte des dispositions combinées des articles L. 2 et L. 3 du code de pensions militaires d'invalidité et des victimes de la guerre que, lorsque le demandeur d'une pension ne peut bénéficier de la présomption légale d'imputabilité et que, par ailleurs, cette imputabilité n'est pas admise par l'administration, il incombe à l'intéressé d'apporter la preuve de l'imputabilité de l'affection au service par tous moyens de nature à emporter la conviction des juges. Dans les cas où est en cause une affection à évolution lente et susceptible d'être liée à l'exposition du militaire à un environnement ou à des substances toxiques, il appartient aux juges du fond de prendre en considération les éléments du dossier relatifs à l'exposition du militaire à cet environnement ou à ces substances, eu égard notamment aux tâches ou travaux qui lui sont confiés, aux conditions dans lesquelles il a été conduit à les exercer, aux conditions et à la durée de l'exposition ainsi qu'aux pathologies que celle-ci est susceptible de provoquer. Il revient ensuite aux juges du fond de déterminer si, au vu des données admises de la science, il existe une probabilité suffisante que la pathologie qui affecte le demandeur soit en rapport avec son activité professionnelle. Lorsque tel est le cas, la seule circonstance que la pathologie pourrait avoir été favorisée par d'autres facteurs ne suffit pas, à elle seule, à écarter la preuve de l'imputabilité, si l'administration n'est pas en mesure d'établir que ces autres facteurs ont été la cause déterminante de la pathologie.</ANA>
<ANA ID="9B"> 54-07-03 Il résulte des dispositions combinées des articles L. 2 et L. 3 du code de pensions militaires d'invalidité et des victimes de la guerre que, lorsque le demandeur d'une pension ne peut bénéficier de la présomption légale d'imputabilité et que, par ailleurs, cette imputabilité n'est pas admise par l'administration, il incombe à l'intéressé d'apporter la preuve de l'imputabilité de l'affection au service par tous moyens de nature à emporter la conviction des juges. Dans les cas où est en cause une affection à évolution lente et susceptible d'être liée à l'exposition du militaire à un environnement ou à des substances toxiques, il appartient aux juges du fond de prendre en considération les éléments du dossier relatifs à l'exposition du militaire à cet environnement ou à ces substances, eu égard notamment aux tâches ou travaux qui lui sont confiés, aux conditions dans lesquelles il a été conduit à les exercer, aux conditions et à la durée de l'exposition ainsi qu'aux pathologies que celle-ci est susceptible de provoquer. Il revient ensuite aux juges du fond de déterminer si, au vu des données admises de la science, il existe une probabilité suffisante que la pathologie qui affecte le demandeur soit en rapport avec son activité professionnelle. Lorsque tel est le cas, la seule circonstance que la pathologie pourrait avoir été favorisée par d'autres facteurs ne suffit pas, à elle seule, à écarter la preuve de l'imputabilité, si l'administration n'est pas en mesure d'établir que ces autres facteurs ont été la cause déterminante de la pathologie.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, 28 juillet 2004, Ministre de la défense c/ Emmanuelli, n° 246170, T. p. 849.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
