<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034330317</ID>
<ANCIEN_ID>JG_L_2017_03_000000389769</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/33/03/CETATEXT000034330317.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 31/03/2017, 389769</TITRE>
<DATE_DEC>2017-03-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389769</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:389769.20170331</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Nancy d'annuler les décisions du ministre de l'intérieur portant retrait de points à la suite d'infractions au code de la route commises les 6 décembre 2004, 3 mai et 27 juin 2005, 16 janvier 2007, 17 janvier 2008, 14 janvier 2009, 13 août 2010, 9 mars, 23 mai, 10 juin, 22 juin 2011 et 20 avril, 3 mai et 4 décembre 2012. Par un jugement n° 1301653 du 24 février 2015, le tribunal administratif a prononcé un non-lieu à statuer concernant les conclusions dirigées contre les décisions du ministre de l'intérieur portant retrait de points à la suite des infractions commises les 14 janvier 2009, 10 juin 2011 et 3 mai 2012, annulé les décisions portant retrait de points consécutives aux infractions des 6 décembre 2004, 3 mai et 27 juin 2005, 13 août 2010, 9 mars, 23 mai et 22 juin 2011 et 20 avril 2012 et rejeté le surplus des conclusions. <br/>
<br/>
              Par un pourvoi, enregistré le 24 avril 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il annule les décisions de retrait de points consécutives aux infractions des 6 décembre 2004, 3 mai et 27 juin 2005, 13 août 2010, 9 mars, 23 mai et 22 juin 2011 et 20 avril 2012  ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la requête de M.A... ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la route ; <br/>
<br/>
              - la loi n° 69-3 du 3 janvier 1969 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond qu'à la suite d'infractions commises entre décembre 2004 et décembre 2012, le ministre de l'intérieur a procédé au retrait de vingt-huit points sur le capital de points affecté au permis de conduire de M.A... ; que celui-ci a demandé au tribunal administratif de Nancy d'annuler les décisions de retrait de points ; que, par un jugement du 24 février 2015, le tribunal a constaté qu'il n'y avait pas lieu de statuer sur les conclusions dirigées contre les décisions de retrait de points faisant suite aux infractions commises les 14 janvier 2009, 10 juin 2011 et 3 mai 2012, annulé les décisions de retrait de point consécutives aux infractions des 6 décembre 2004, 3 mai et 27 juin 2005, 13 août 2010, 9 mars, 23 mai et 22 juin 2011 et 20 avril 2012 et rejeté le surplus des conclusions de la requête ; que le ministre de l'intérieur se pourvoit en cassation contre ce jugement en tant qu'il annule huit décisions de retrait de points ;<br/>
<br/>
              2. Considérant que la notification d'une décision relative au permis de conduire doit être regardée comme régulière lorsqu'elle est faite à une adresse correspondant effectivement à une résidence de l'intéressé ; que, s'agissant des personnes dépourvues de domicile fixe, la notification peut être régulièrement effectuée à une adresse déclarée à l'administration et où l'intéressé est en mesure de recevoir son courrier ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis au juge du fond qu'un pli recommandé contenant une décision constatant la perte de validité du permis de conduire du requérant pour solde de points nul, qui récapitulait l'ensemble des retraits de points mentionnés au point 1, a été adressé par le service du Fichier national du permis de conduire à " M. A...B...- 88320 Martigny-les-Bains " et a été retourné à ce service revêtu de mentions selon lesquelles il avait été présenté à cette adresse le 5 avril 2013 puis mis en instance au bureau de poste où il n'avait pas été réclamé ; que la commune de Martigny-les-Bains était la commune de rattachement choisie par l'intéressé au titre de la loi du 3 janvier 1969 relative à l'exercice des activités ambulantes et au régime applicable aux personnes circulant en France sans domicile ni résidence fixe, alors en vigueur ; qu'en se fondant sur la circonstance que la législation du permis de conduire ne figure pas au nombre des matières énumérées à l'article 10 de la loi du 3 janvier 1969 dans lesquelles le rattachement à une commune produit tout ou partie des effets attachés au domicile, pour juger que la notification n'avait pas été régulière et que, par suite, le recours contentieux présenté le 17 juillet 2013 ne pouvait être regardé comme tardif, sans rechercher si cette notification avait été faite à une adresse déclarée à l'administration et où l'intéressé était en mesure de recevoir son courrier, le tribunal administratif a commis une erreur de droit ; que l'article 2 de son jugement doit, par suite, être annulé ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler dans cette mesure l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant qu'en vertu des dispositions de l'article R. 421-1 du code de justice administrative, le destinataire d'une décision administrative individuelle dispose, pour déférer cette décision devant la juridiction administrative, d'un délai de deux mois à compter de la notification qui doit lui en être faite ; <br/>
<br/>
              6. Considérant, d'une part, qu'il résulte de ce qui a été dit au point 2 que le requérant n'est pas fondé à soutenir que dans la mesure où il est dépourvu de domicile fixe, les décisions relatives à son permis de conduire ne pouvaient lui être notifiées que par voie administrative ;<br/>
<br/>
              7. Considérant, d'autre part, qu'ainsi qu'il a été dit au point 3, une décision récapitulant l'ensemble des retraits de points litigieux a été notifiée le 5 avril 2013 à l'adresse " M. A...B...- 88320 Martigny-les-Bains " ; que cette adresse était mentionnée sur la carte nationale d'identité de l'intéressé ainsi que sur le livret spécial de circulation de circulation qui lui avait délivré en application de l'article 1er de la loi du 3 janvier 1969 mentionnée ci-dessus ; que son permis de conduire mentionnait les références du livret de circulation ; que l'adresse figurait sur un relevé d'information intégral relatif à son permis de conduire édité le 5 juillet 2013 ; que le pli recommandé contenant la décision a été renvoyé à l'administration revêtu de la mention " avisé, non réclamé " et non de la mention " inconnu à l'adresse indiquée " ; qu'il ressort de l'ensemble de ces éléments que la notification a été faite à une adresse déclarée à l'administration et à laquelle l'intéressé était en mesure de recevoir son courrier ; que dans ces conditions, la notification doit être regardée comme régulière ; que, par suite, le ministre de l'intérieur est fondé à soutenir que la demande présentée par M. A...devant le tribunal administratif de Nancy le 17 juillet 2013 est tardive et, par suite, irrecevable ; que cette demande doit être rejetée en tant qu'elle porte sur les infractions commises les 6 décembre 2004, 3 mai et 27 juin 2005, 13 août 2010, 9 mars, 23 mai et 22 juin 2011 et 20 avril 2012 ;<br/>
<br/>
<br/>
<br/>
<br/>
              D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'article 2 du jugement du tribunal administratif de Nancy du 24 février 2015 est annulé.<br/>
<br/>
Article 2 : Les conclusions présentées par M. A...devant le tribunal administratif de Nancy tendant à l'annulation des décisions portant retrait de points consécutives aux infractions des 6 décembre 2004, 3 mai et 27 juin 2005, 13 août 2010, 9 mars, 23 mai et 22 juin 2011 et 20 avril 2012 sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à M. B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">49-04-01-04 POLICE. POLICE GÉNÉRALE. CIRCULATION ET STATIONNEMENT. PERMIS DE CONDUIRE. - NOTIFICATION DES DÉCISIONS RELATIVES AU PERMIS DE CONDUIRE - RÉGULARITÉ - 1) CONDITION - ADRESSE CORRESPONDANT EFFECTIVEMENT À UNE RÉSIDENCE DE L'INTÉRESSÉ - 2) CAS DES PERSONNES DÉPOURVUES DE DOMICILE FIXE - A) ADRESSE DÉCLARÉE À L'ADMINISTRATION ET OÙ L'INTÉRESSÉ EST EN MESURE DE RECEVOIR SON COURRIER - B) ESPÈCE.
</SCT>
<ANA ID="9A"> 49-04-01-04 1) La notification d'une décision relative au permis de conduire doit être regardée comme régulière lorsqu'elle est faite à une adresse correspondant effectivement à une résidence de l'intéressé.,,,2) a) S'agissant des personnes dépourvues de domicile fixe, la notification peut être régulièrement effectuée à une adresse déclarée à l'administration et où l'intéressé est en mesure de recevoir son courrier.... ,,b) Décision récapitulant l'ensemble des retraits de points litigieux notifiée à l'adresse mentionnée sur la carte nationale d'identité de l'intéressé ainsi que sur le livret spécial de circulation qui lui avait délivré en application de l'article 1er de la loi n° 69-3 du 3 janvier 1969 alors en vigueur. Son permis de conduire mentionnait les références du livret de circulation. L'adresse figurait sur un relevé d'information intégral relatif à son permis de conduire édité le 5 juillet 2013. Le pli recommandé contenant la décision a été renvoyé à l'administration revêtu de la mention avisé, non réclamé et non de la mention inconnu à l'adresse indiquée.... ,,Il ressort de l'ensemble de ces éléments que la notification a été faite à une adresse déclarée à l'administration et à laquelle l'intéressé était en mesure de recevoir son courrier. Dans ces conditions, la notification doit être regardée comme régulière.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
