<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030286055</ID>
<ANCIEN_ID>JG_L_2015_02_000000367335</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/28/60/CETATEXT000030286055.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 25/02/2015, 367335</TITRE>
<DATE_DEC>2015-02-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367335</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Jean-Baptiste de Froment</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:367335.20150225</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 2 avril et 2 juillet 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la communauté d'agglomération de Mantes-en-Yvelines, représentée par son président, dont le siège est rue des Pierrettes à Magnanville (78200) ; la communauté d'agglomération de Mantes-en-Yvelines demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 11VE02847 et 11VE03099 du 6 décembre 2012 par lequel la cour administrative d'appel de Versailles a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0911772-1006288 du 30 mai 2011 du tribunal administratif de Versailles annulant, à la demande de la SCI Quasida, les arrêtés des 29 juillet 2009 et 12 avril 2010 par lesquels le maire de la commune de Rosny-sur-Seine, agissant au nom de l'Etat, a accordé à la requérante deux permis pour la construction avec extension d'une station d'épuration, d'autre part, au rejet de la demande de la SCI Quasida ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la SCI Quasida une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'environnement :<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Baptiste de Froment, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la communauté d'agglomération de Mantes-en-Yvelines et à la SCP Delaporte, Briard, Trichet, avocat de la SCI Quasida ;<br/>
<br/>
<br/>
<br/>
<br/>l. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la communauté d'agglomération de Mantes-en-Yvelines été autorisée à exploiter une station d'épuration sur le territoire de la commune de Rosny-sur-Seine au titre de la législation des installations classées pour la protection de l'environnement ; que, par deux arrêtés des 29 juillet 2009 et 12 avril 2010, le maire de Rosny-sur-Seine a délivré des permis de construire afin de permettre la modernisation de cette installation ; qu'en application des dispositions de l'article R. 512-33 du code de l'environnement, l'exploitant a informé l'administration le 15 décembre 2009 des modifications concomitantes susceptibles d'entraîner un changement notable des éléments du dossier de demande d'autorisation de cette installation ; que par un jugement du 30 avril 2011, le tribunal administratif de Versailles a annulé ces arrêtés ; que, par un arrêt du 6 décembre 2012, contre lequel la communauté d'agglomération de Mantes-en-Yvelines se pourvoit en cassation, la cour administrative d'appel de Versailles a rejeté l'appel de la communauté d'agglomération dirigé contre ce jugement ; <br/>
<br/>
              2. Considérant qu'en vertu de l'article L. 421-6 du code de l'urbanisme, le permis de construire a pour objet de vérifier que les travaux projetés sont conformes aux dispositions législatives et réglementaires relatives à l'utilisation des sols, à l'implantation, la destination, la nature, l'architecture, les dimensions, l'assainissement des constructions et à l'aménagement de leurs abords ; que l'article R. 431-16 du même code, relatif à certaines pièces complémentaires qui doivent être jointes à la demande de permis de construire en fonction de la situation ou de la nature du projet, dans sa rédaction en vigueur à la date des arrêtés attaqués, dispose que : " Le dossier joint à la demande de permis de construire comprend en outre, selon les cas : / a) L'étude d'impact, lorsqu'elle est prévue en application du code de l'environnement ; (...) " ; que les articles R. 122-5 et suivants du code de l'environnement, dans leur rédaction en vigueur à la date des arrêtés litigieux, dressent la liste des travaux, ouvrages ou aménagements soumis à une étude d'impact, notamment lorsqu'ils sont subordonnés à la délivrance d'un permis de construire ; <br/>
<br/>
              3. Considérant qu'il résulte de l'ensemble de ces dispositions que l'obligation de joindre l'étude d'impact au dossier de demande de permis de construire prévue par l'article R. 431-16 du code de l'urbanisme ne concerne que les cas où l'étude d'impact est exigée en vertu des dispositions du code de l'environnement pour des projets soumis à autorisation en application du code de l'urbanisme ; que, par suite, en se fondant, pour annuler les permis attaqués, sur l'absence d'étude d'impact sans rechercher si celle-ci était exigée pour un projet soumis à autorisation en application du code de l'urbanisme, la cour a méconnu, au prix d'une erreur de droit, la portée des dispositions de l'article R. 431-16 du code de l'urbanisme ; que, dès lors, et sans qu'il soit besoin d'examiner les moyens du pourvoi, la communauté d'agglomération de Mantes-en-Yvelines est fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la SCI Quasida la somme de 2 000 euros à verser à la communauté d'agglomération de Mantes-en-Yvelines en application des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, ces dispositions font obstacle à ce qu'une somme soit mise à la charge de la communauté d'agglomération de Mantes-en-Yvelines, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 6 décembre 2012 de la cour administrative d'appel de Versailles est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : La SCI Quasida versera  à la communauté d'agglomération de Mantes-en-Yvelines la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions de la la SCI Quasida présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la communauté d'agglomération de Mantes-en-Yvelines et à la SCI Quasida.<br/>
Copie en sera adressée pour information à la ministre du logement, de l'égalité du territoire et de la ruralité.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-03-02-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. PROCÉDURE D'ATTRIBUTION. DEMANDE DE PERMIS. - COMPOSITION DU DOSSIER DE DEMANDE - ETUDE D'IMPACT  PRÉVUE EN APPLICATION DU CODE DE L'ENVIRONNEMENT  (ART. R. 431-16 DU CODE DE L'URBANISME) - NOTION - INCLUSION DES SEULES HYPOTHÈSES OÙ LE CODE DE L'ENVIRONNEMENT EXIGE UNE ÉTUDE D'IMPACT POUR DES PROJETS SOUMIS À AUTORISATION AU TITRE DE LA LÉGISLATION DE L'URBANISME.
</SCT>
<ANA ID="9A"> 68-03-02-01 L'obligation de joindre l'étude d'impact au dossier de demande de permis de construire prévue par l'article R. 431-16 du code de l'urbanisme ne concerne que les cas où l'étude d'impact est exigée en vertu des dispositions du code de l'environnement pour des projets soumis à autorisation en application du code de l'urbanisme.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
