<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030286064</ID>
<ANCIEN_ID>JG_L_2015_02_000000371706</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/28/60/CETATEXT000030286064.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème SSJS, 25/02/2015, 371706</TITRE>
<DATE_DEC>2015-02-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>371706</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>Mme Anissia Morel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:371706.20150225</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Marseille d'annuler les décisions des 18 juin et 29 juillet 2010 du directeur du centre hospitalier Edmond Garcin d'Aubagne refusant de reconnaître l'imputabilité au service d'une pathologie dont elle est atteinte. Par un jugement n° 1005293 du 27 mai 2013, le tribunal administratif a annulé ces décisions.<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 28 août et 28 novembre 2013 au secrétariat du contentieux du Conseil d'Etat, le centre hospitalier Edmond Garcin d'Aubagne demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de rejeter la demande de Mme A...;<br/>
<br/>
              3°) de mettre à la charge de Mme A... la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
<br/>
              - le code de la sécurité sociale ;<br/>
<br/>
              - la loi n° 86-33 du 9 janvier 1986 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anissia Morel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse -Dessen, Thouvenin, Coudray, avocat du centre hospitalier Edmond Garcin d'Aubagne et à la SCP Boré, Salve de Bruneton, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 41 de la loi du 9 janvier 1986 portant dispositions statutaires applicables à la fonction publique hospitalière : " Le fonctionnaire en activité a droit : (...) 2° A des congés de maladie dont la durée totale peut atteindre un an pendant une période de douze mois consécutifs en cas de maladie dûment constatée mettant l'intéressé dans l'impossibilité d'exercer ses fonctions. Celui-ci conserve alors l'intégralité de son traitement pendant une durée de trois mois ; ce traitement est réduit de moitié pendant les neuf mois suivants. Le fonctionnaire conserve, en outre, ses droits à la totalité du supplément familial de traitement et de l'indemnité de résidence. / Toutefois, si la maladie provient de l'une des causes exceptionnelles prévues à l'article L. 27 du code des pensions civiles et militaires de retraite ou d'un accident survenu dans l'exercice ou à l'occasion de l'exercice de ses fonctions, le  fonctionnaire conserve l'intégralité de son traitement jusqu'à ce qu'il soit en état de reprendre son service ou jusqu'à sa mise à la retraite. Il a droit, en outre, au remboursement des honoraires médicaux et des frais directement entraînés par la maladie ou l'accident. / Dans le cas visé à l'alinéa précédent, l'imputation au service de la maladie ou de l'accident est appréciée par la commission de réforme instituée par le régime des pensions des agents des collectivités locales " ; qu'au nombre des causes exceptionnelles prévues à l'article L. 27 du code des pensions civiles et militaires de retraite figurent notamment les maladies contractées ou aggravées en service ; qu'aux termes de l'article L. 461-1 du code de la sécurité sociale : " (...) Est présumée d'origine professionnelle toute maladie désignée dans un tableau de maladies professionnelles et contractées dans les conditions mentionnées à ce tableau. (...) " ;<br/>
<br/>
              2. Considérant qu'aucune disposition ne rend applicables aux fonctionnaires hospitaliers qui demandent le bénéfice des dispositions combinées du 2° de l'article 41 de la loi du 9 janvier 1986 et de l'article L. 27 du code des pensions civiles et militaires de retraite les dispositions de l'article L. 461-1 du code de la sécurité sociale instituant une présomption d'origine professionnelle pour toute maladie désignée dans un tableau de maladies professionnelles et contractées dans des conditions mentionnées à ce tableau ; qu'il suit de là qu'en se fondant sur les dispositions de cet article pour annuler les décisions des 18 juin et 29 juillet 2010 du directeur du centre hospitalier Edmond Garcin d'Aubagne refusant de reconnaître l'imputabilité au service de la pathologie dont Mme A... est atteinte, le tribunal administratif de Marseille a commis une erreur de droit ; que, dès lors, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, le jugement du 27 mai 2013 du tribunal administratif de Marseille doit être annulé ;<br/>
<br/>
              3. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le centre hospitalier Edmond Garcin d'Aubagne au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge du centre hospitalier Edmond Garcin d'Aubagne qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Marseille du 27 mai 2013 est annulé.<br/>
Article 2 : L'affaire est renvoyée devant le tribunal administratif de Marseille.<br/>
Article 3 : Le surplus des conclusions du pourvoi du centre hospitalier Edmond Garcin d'Aubagne est rejeté.<br/>
Article 4 : Les conclusions de Mme A...présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée au centre hospitalier Edmond Garcin d'Aubagne et à Mme B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-07-01-04 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. STATUT GÉNÉRAL DES FONCTIONNAIRES DE L'ÉTAT ET DES COLLECTIVITÉS LOCALES. DISPOSITIONS STATUTAIRES RELATIVES À LA FONCTION PUBLIQUE HOSPITALIÈRE (LOI DU 9 JANVIER 1986). - RECONNAISSANCE D'UNE MALADIE CONTRACTÉE EN SERVICE (ARTICLE 41 DE LA LOI DU 9 JANVIER 1986 ET ARTICLE L. 27 DU CPCMR) - APPLICABILITÉ DES DISPOSITIONS DE L'ARTICLE L. 461-1 DU CSS - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-07-10-01 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. GARANTIES ET AVANTAGES DIVERS. PROTECTION EN CAS D'ACCIDENT DE SERVICE. - FONCTION PUBLIQUE HOSPITALIÈRE - RECONNAISSANCE D'UNE MALADIE CONTRACTÉE EN SERVICE (ARTICLE 41 DE LA LOI DU 9 JANVIER 1986 ET ARTICLE L. 27 DU CPCMR) - APPLICABILITÉ DES DISPOSITIONS DE L'ARTICLE L. 461-1 DU CSS - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 36-07-01-04 Aucune disposition ne rend applicables aux fonctionnaires hospitaliers qui demandent le bénéfice des dispositions combinées du 2° de l'article 41 de la loi n° 86-33 du 9 janvier 1986 et de l'article L. 27 du code des pensions civiles et militaires de retraite (CPCMR), les dispositions de l'article L. 461-1 du code de la sécurité sociale (CSS) instituant une présomption d'origine professionnelle pour toute maladie désignée dans un tableau de maladies professionnelles et contractées dans des conditions mentionnées à ce tableau.</ANA>
<ANA ID="9B"> 36-07-10-01 Aucune disposition ne rend applicables aux fonctionnaires hospitaliers qui demandent le bénéfice des dispositions combinées du 2° de l'article 41 de la loi n° 86-33 du 9 janvier 1986 et de l'article L. 27 du code des pensions civiles et militaires de retraite (CPCMR), les dispositions de l'article L. 461-1 du code de la sécurité sociale (CSS) instituant une présomption d'origine professionnelle pour toute maladie désignée dans un tableau de maladies professionnelles et contractées dans des conditions mentionnées à ce tableau.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Rappr., pour les fonctionnaires de l'Etat, CE, 23 juillet 2012, Ministre du budget c. Mme Lami-Hurier, n° 349726, T. p. 811 ; CE, 7 juillet 2000, M. Laffray, n° 213037, T. p. 1060.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
