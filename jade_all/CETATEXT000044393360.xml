<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044393360</ID>
<ANCIEN_ID>JG_L_2021_11_000000439742</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/39/33/CETATEXT000044393360.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 30/11/2021, 439742</TITRE>
<DATE_DEC>2021-11-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439742</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:439742.20211130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... I... a demandé au tribunal administratif d'Orléans d'annuler la décision du 4 avril 2016 par laquelle la commission régionale des recours en matière de contrôle des structures des exploitations agricoles de la région Centre lui a infligé une sanction pécuniaire de 78 223 euros. Par un jugement n° 1601842 du 19 avril 2018, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 18NT02315 du 24 janvier 2020, la cour administrative d'appel de Nantes a rejeté l'appel formé par M. I... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 24 mars et 24 juin 2020 au secrétariat du contentieux du Conseil d'Etat, M. I... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu<br/>
              - le code des relations entre le public et l'administration ; <br/>
              - le code rural et de la pêche maritime ;<br/>
              - la loi n° 2006-11 du 5 janvier 2006 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Foussard, Froger, avocat de M. I....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. I..., associé-exploitant de la société civile d'exploitation agricole (SCEA) Chemin de Bulty, située dans le département des Ardennes, a racheté en avril 2014 la quasi-totalité (99%) des parts sociales de l'entreprise agricole à responsabilité limitée (EARL) La Cheptellière, située dans le département d'Indre-et-Loire, dont il est devenu exploitant-associé et cogérant. Estimant que cette acquisition était soumise à autorisation d'exploiter, le préfet d'Indre-et-Loire a mis M. I... en demeure de régulariser sa situation puis, l'intéressé n'y ayant pas déféré, lui a enjoint de cesser l'exploitation des terres concernées et lui a infligé, le 22 octobre 2015, une sanction pécuniaire de 78 223 euros. Saisie par M. I... d'un recours contre cette sanction, la commission des recours en matière de contrôle des structures des exploitations agricoles de la région Centre a, par une décision du 4 avril 2016, prononcé la même sanction à l'encontre de l'intéressé. Celui-ci se pourvoit en cassation contre l'arrêt du 24 janvier 2020 par lequel la cour administrative d'appel de Nantes a rejeté son appel formé contre le jugement du 19 avril 2018 par lequel le tribunal administratif d'Orléans a rejeté sa demande d'annulation de la sanction prononcée par la commission des recours.<br/>
<br/>
              2. L'article L. 331-7 du code rural et de la pêche maritime dispose que, lorsqu'elle constate qu'un fonds est exploité contrairement aux dispositions régissant le contrôle des structures des exploitations agricoles " (...) l'autorité administrative met l'intéressé en demeure de régulariser sa situation dans un délai qu'elle détermine et qui ne saurait être inférieur à un mois. / La mise en demeure mentionnée à l'alinéa précédent prescrit à l'intéressé soit de présenter une demande d'autorisation, soit, si une décision de refus d'autorisation est intervenue, de cesser l'exploitation des terres concernées. / Lorsque l'intéressé, tenu de présenter une demande d'autorisation, ne l'a pas formée dans le délai mentionné ci-dessus, l'autorité administrative lui notifie une mise en demeure de cesser d'exploiter dans un délai de même durée. / (...) Si, à l'expiration du délai imparti pour cesser l'exploitation des terres concernées, l'autorité administrative constate que l'exploitation se poursuit dans des conditions irrégulières, elle peut prononcer à l'encontre de l'intéressé une sanction pécuniaire d'un montant compris entre 304,90 et 914,70 euros par hectare (...) ". Aux termes de l'article L. 331-8 du même code : " La décision prononçant la sanction pécuniaire mentionnée à l'article L. 331-7 est notifiée à l'exploitant concerné, qui peut la contester, avant tout recours contentieux, dans le mois de sa réception, devant une commission des recours dont la composition et les règles de fonctionnement sont fixées par décret en Conseil d'Etat. / Les recours devant cette commission sont suspensifs. Leur instruction est contradictoire. / La commission, qui statue par décision motivée, peut soit confirmer la sanction, soit décider qu'en raison d'éléments tirés de la situation de la personne concernée il y a lieu de ramener la pénalité prononcée à un montant qu'elle détermine dans les limites fixées à l'article L. 331-7, soit décider qu'en l'absence de violation établie des dispositions du présent chapitre il n'y a pas lieu à sanction. Dans les deux premiers cas, la pénalité devient recouvrable dès notification de sa décision. / La décision de la commission peut faire l'objet, de la part de l'autorité administrative ou de l'intéressé, d'un recours de pleine juridiction devant le tribunal administratif ". Enfin, aux termes de l'article R. 331-11 de ce code : " La procédure d'instruction des recours est contradictoire. / La décision de la commission des recours ne peut intervenir qu'après que l'exploitant sanctionné et le préfet de région auteur de la décision ont été mis à même de présenter leurs observations écrites. / Ceux-ci sont informés qu'ils seront entendus par la commission des recours s'ils en font la demande. Ils peuvent se faire assister ou représenter. / La commission des recours peut demander à l'administration ou à l'auteur du recours de lui communiquer tous documents utiles à l'instruction du dossier. Elle peut aussi convoquer les personnes de son choix ". Il résulte de ces dispositions, qui organisent un recours préalable obligatoire contre toute décision de sanction prononcée par l'administration devant la commission prévue à l'article L. 331-8 du code rural et de la pêche maritime, que la procédure suivie devant cet organisme, eu égard à ses caractéristiques, et la décision de cet organisme prononçant une nouvelle sanction, ou décidant qu'il n'y a pas lieu à sanction, se substituent entièrement à la procédure suivie devant l'administration et à la décision de sanction prise par celle-ci.<br/>
<br/>
              3. En premier lieu, il résulte de ce qui vient d'être dit qu'en jugeant, par adoption des motifs du jugement du tribunal administratif, que le moyen par lequel M. I... contestait la régularité de la sanction prononcée le 22 octobre 2015 par le préfet d'Indre-et-Loire était inopérant, la cour n'a pas commis d'erreur de droit.<br/>
<br/>
              4. En deuxième lieu, l'article L. 121-1 du code des relations entre le public et l'administration dispose que : " Exception faite des cas où il est statué sur une demande, les décisions individuelles qui doivent être motivées en application de l'article L. 211-2, ainsi que les décisions qui, bien que non mentionnées à cet article, sont prises en considération de la personne, sont soumises au respect d'une procédure contradictoire préalable ", les modalités de mise en œuvre de cette procédure contradictoire étant fixées par les articles L. 122-1 et L. 122-2 du même code.<br/>
<br/>
              5. Toutefois, aux termes de l'article L. 121-2 de ce code : " Les dispositions de l'article L. 121-1 ne sont pas applicables : (...) / 3° Aux décisions pour lesquelles des dispositions législatives ont instauré une procédure contradictoire particulière ". Par suite, en jugeant, par adoption des motifs du jugement du tribunal administratif, que le moyen tiré par M. I..., dans le cadre de la procédure contradictoire particulière prévue, devant la commission des recours, par l'article L. 331-8 du code rural et de la pêche maritime, d'une violation des dispositions de l'article L. 122-2 du code des relations entre le public et l'administration était inopérant, la cour n'a pas davantage commis d'erreur de droit.<br/>
<br/>
              6. En troisième lieu, aux termes du I de l'article L. 331-2 du code rural et de la pêche maritime, dans sa rédaction applicable à l'espèce : " Sont soumises à autorisation préalable les opérations suivantes : / 1° Les installations, les agrandissements ou les réunions d'exploitations agricoles au bénéfice d'une exploitation agricole mise en valeur par une ou plusieurs personnes physiques ou morales, lorsque la surface totale qu'il est envisagé de mettre en valeur excède le seuil fixé par le schéma directeur départemental des structures. (...) / 5° Les agrandissements ou réunions d'exploitations pour les biens dont la distance par rapport au siège de l'exploitation du demandeur est supérieure à un maximum fixé par le schéma directeur départemental des structures, sans que ce maximum puisse être inférieur à cinq kilomètres ; / (...) / Pour déterminer la superficie totale mise en valeur, il est tenu compte des superficies exploitées par le demandeur sous quelque forme que ce soit (...) ". Il résulte de ces dispositions que sont notamment soumises au régime de l'autorisation préalable les opérations d'agrandissement d'une surface agricole mise en valeur par une personne physique, lorsque la surface totale qu'elle envisage de mettre en valeur excède le seuil fixé par le schéma directeur départemental des structures. Il en va ainsi lorsque l'agrandissement de la surface agricole résulte d'un rachat, par une personne physique, de parts d'une société à objet agricole, si cette personne participe effectivement aux travaux et doit, par suite, être regardée comme mettant en valeur les surfaces exploitées par cette société.<br/>
<br/>
              7. Par ailleurs, la loi du 5 janvier 2006 d'orientation agricole, qui a retiré de la liste des opérations soumises à autorisation mentionnées au point 6 certaines modifications dans la répartition des parts ou actions des sociétés à objet agricole, ne saurait avoir eu pour effet, contrairement à ce que soutient M. I..., d'exempter d'autorisation celles des opérations d'extension mentionnées au point précédent qui se traduiraient par une modification dans la répartition des parts ou actions des sociétés à objet agricole.<br/>
<br/>
              8. Par suite, en jugeant, alors qu'il ressortait des pièces du dossier qui lui était soumis et qu'il n'était au demeurant pas contesté que M. I... participait effectivement aux travaux d'exploitation des terres de l'EARL La Cheptellière, que le rachat des parts de cette société constituait une opération d'agrandissement de son exploitation au sens des dispositions de l'article L. 331-2 du code rural et de la pêche maritime, la cour administrative d'appel n'a pas non plus commis d'erreur de droit.<br/>
<br/>
              9. En déduisant ensuite des dispositions du schéma directeur des structures  du département d'Indre-et-Loire du 21 juillet 2010, qui prévoit que sont soumis à autorisation préalable les agrandissements ou réunions d'exploitations agricoles lorsque la surface totale dépasse 102 hectares ou lorsque la distance du nouveau bien par rapport au siège de l'exploitation est supérieure à 15 km, que l'opération en cause, qui portait l'exploitation de M. I..., par l'appropriation d'un bien situé à plus de 300 km de son siège, à une superficie de plus de 170 hectares, constituait, à ce double titre, une opération soumise à autorisation, la cour a exactement qualifié les faits qui lui étaient soumis.<br/>
<br/>
              10. En quatrième lieu, en déduisant de ce que les dispositions des articles L. 331-1, L. 331-2 et L. 331-7 du code rural et de la pêche maritime fixent de manière suffisamment claire les obligations des exploitants agricoles et les sanctions encourues en cas de manquement à ces obligations que M. I... n'avait pu légitimement se méprendre sur la portée de ses obligations, elle a porté sur les pièces du dossier une appréciation souveraine, exempte de dénaturation comme d'erreur de droit.<br/>
<br/>
              11. Enfin, en cinquième lieu, il appartient au juge de cassation de vérifier que la sanction retenue n'est pas hors de proportion avec la faute commise et qu'elle a pu, dès lors, être légalement prise. Contrairement à ce que soutient M. I..., la cour administrative d'appel a pu légalement estimer qu'une sanction de 450 euros par hectare était, compte tenu de ce que son montant se situait dans la partie basse de la fourchette de 304,90 à 914,70 euros par hectare fixée par l'article L. 331-7 du code rural et de la pêche maritime, justifiée par son refus de régulariser sa situation en présentant une demande d'autorisation d'exploitation.<br/>
<br/>
              12. Il résulte de tout ce qui précède que le pourvoi de M. I... doit être rejeté, y compris, par voie de conséquence, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de M. I... est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A... I... et au ministre de l'agriculture et de l'alimentation.<br/>
              Délibéré à l'issue de la séance du 20 octobre 2021 où siégeaient : M. Christophe Chantepy, président de la section du contentieux, présidant ; M. Denis Piveteau, président de chambre ; Mme M... J..., M. A... B..., M. L... C..., Mme F... K..., M. E... H..., M. Cyril Roger-Lacan, conseillers d'Etat et M. Jean-Dominique Langlais, conseiller d'Etat-rapporteur. <br/>
<br/>
              Rendu le 30 novembre 2021.<br/>
                 Le président : <br/>
                 Signé : M. Christophe Chantepy<br/>
 		Le rapporteur : <br/>
      Signé : M. Jean-Dominique Langlais<br/>
                 Le secrétaire :<br/>
                 Signé : M. D... G...<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">03-03-03-01-01 AGRICULTURE ET FORÊTS. - EXPLOITATIONS AGRICOLES. - CUMULS ET CONTRÔLE DES STRUCTURES. - CUMULS D'EXPLOITATIONS. - CHAMP D'APPLICATION DE LA LÉGISLATION SUR LES CUMULS. - AGRANDISSEMENT D'UNE SURFACE AGRICOLE - INCLUSION - AGRANDISSEMENT RÉSULTANT DU RACHAT DE PARTS D'UNE SOCIÉTÉ À OBJET AGRICOLE AVEC PARTICIPATION EFFECTIVE AUX TRAVAUX [RJ3].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">03-03-03-01-06 AGRICULTURE ET FORÊTS. - EXPLOITATIONS AGRICOLES. - CUMULS ET CONTRÔLE DES STRUCTURES. - CUMULS D'EXPLOITATIONS. - CONTENTIEUX. - RAPO CONTRE LA SANCTION POUR EXPLOITATION IRRÉGULIÈRE D'UN FONDS AGRICOLE (ART. L. 331-8 DU CRPM) - CONSÉQUENCES - 1) SUBSTITUTION À LA PROCÉDURE INITIALE DE LA PROCÉDURE SUIVIE DEVANT LA COMMISSION DE RECOURS [RJ1] - 2) INOPÉRANCE DES MOYENS TIRÉS DE L'IRRÉGULARITÉ DE LA PREMIÈRE DÉCISION [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-01-02-01 PROCÉDURE. - INTRODUCTION DE L'INSTANCE. - LIAISON DE L'INSTANCE. - RECOURS ADMINISTRATIF PRÉALABLE. - RAPO CONTRE LA SANCTION POUR EXPLOITATION IRRÉGULIÈRE D'UN FONDS AGRICOLE (ART. L. 331-8 DU CRPM) - CONSÉQUENCES - 1) SUBSTITUTION À LA PROCÉDURE INITIALE DE LA PROCÉDURE SUIVIE DEVANT LA COMMISSION DE RECOURS [RJ1] - 2) INOPÉRANCE DES MOYENS TIRÉS DE L'IRRÉGULARITÉ DE LA PREMIÈRE DÉCISION [RJ2].
</SCT>
<ANA ID="9A"> 03-03-03-01-01 Il résulte du I de l'article L. 331-2 du code rural et de la pêche maritime (CRPM) que sont notamment soumises au régime de l'autorisation préalable les opérations d'agrandissement d'une surface agricole mise en valeur par une personne physique, lorsque la surface totale qu'elle envisage de mettre en valeur excède le seuil fixé par le schéma directeur départemental des structures.......Il en va ainsi lorsque l'agrandissement de la surface agricole résulte d'un rachat, par une personne physique, de parts d'une société à objet agricole, si cette personne participe effectivement aux travaux et doit, par suite, être regardée comme mettant en valeur les surfaces exploitées par cette société.......Par ailleurs, la loi n° 2006-11 du 5 janvier 2006, qui a retiré de la liste des opérations soumises à autorisation certaines modifications dans la répartition des parts ou actions des sociétés à objet agricole, ne saurait avoir eu pour effet d'exempter d'autorisation les opérations d'extension mentionnées plus haut qui se traduiraient par une modification dans la répartition des parts ou actions des sociétés à objet agricole.</ANA>
<ANA ID="9B"> 03-03-03-01-06 1) Il résulte des articles L. 331-7, L. 331-8 et R. 331-11 du code rural et de la pêche maritime (CRPM), qui organisent un recours préalable obligatoire (RAPO) contre toute décision de sanction prononcée par l'administration devant la commission des recours prévue à l'article L. 331-8 du même code, que la procédure suivie devant cet organisme, eu égard à ses caractéristiques, et la décision de cet organisme prononçant une nouvelle sanction, ou décidant qu'il n'y a pas lieu à sanction, se substituent entièrement à la procédure suivie devant l'administration et à la décision de sanction prise par celle-ci.......2) Par suite, sont inopérants les moyens contestant la régularité de la décision de sanction initiale.</ANA>
<ANA ID="9C"> 54-01-02-01 1) Il résulte des articles L. 331-7, L. 331-8 et R. 331-11 du code rural et de la pêche maritime (CRPM), qui organisent un recours préalable obligatoire (RAPO) contre toute décision de sanction prononcée par l'administration devant la commission des recours prévue à l'article L. 331-8 du même code, que la procédure suivie devant cet organisme, eu égard à ses caractéristiques, et la décision de cet organisme prononçant une nouvelle sanction, ou décidant qu'il n'y a pas lieu à sanction, se substituent entièrement à la procédure suivie devant l'administration et à la décision de sanction prise par celle-ci.......2) Par suite, sont inopérants les moyens contestant la régularité de la décision de sanction initiale.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., en précisant, CE, 2 juillet 2021, MM. Julienne, n° 432802, à mentionner aux Tables. ...[RJ2] Comp., s'agissant, en principe, de l'opérance, à l'encontre des décisions prises sur RAPO, de certains moyens tirés des vices de procédure qui affectent la décision initiale, CE, Section 18 novembre 2005, Houlbreque, n° 270075, p. 514....[RJ3] Cf., sur la condition de mise en valeur de la surface agricole, CE, 2 juillet 2021, MM. Julienne, n° 432801, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
