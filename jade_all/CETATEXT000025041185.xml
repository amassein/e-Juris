<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025041185</ID>
<ANCIEN_ID>JG_L_2011_12_000000353308</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/04/11/CETATEXT000025041185.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 20/12/2011, 353308, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2011-12-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353308</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Bernard Stirn</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>M. Guillaume Odinet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:353308.20111220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 1102962 du 7 octobre 2011, enregistrée le 12 octobre 2011 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président de la première chambre du tribunal administratif de Nice, avant qu'il soit statué sur la demande du SYNDICAT INTERCOMMUNAL DES EAUX DES CORNICHES ET DU LITTORAL (SIECL), tendant à l'annulation de l'arrêté du préfet des Alpes-Maritimes du 24 juin 2011 portant répartition des ouvrages du service public de l'eau entre le SIECL et la Communauté urbaine Nice Côte d'Azur (CUNCA), a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 5211-25-1 du code général des collectivités territoriales ;<br/>
<br/>
              Vu le mémoire, enregistré le 11 août 2011 au greffe du tribunal administratif de Nice, présenté par le SIECL, dont le siège est 27 chemin du Vinaigrier à Nice (06300), représenté par son président, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Odinet, Auditeur, <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              Considérant qu'aux termes de l'article L. 5211-25-1 du code général des collectivités territoriales : " En cas de retrait de la compétence transférée à un établissement public de coopération intercommunale : / 1° Les biens meubles et immeubles mis à la disposition de l'établissement bénéficiaire du transfert de compétences sont restitués aux communes antérieurement compétentes et réintégrés dans leur patrimoine pour leur valeur nette comptable, avec les adjonctions effectuées sur ces biens liquidées sur les mêmes bases. Le solde de l'encours de la dette transférée afférente à ces biens est également restituée à la commune propriétaire ; / 2° Les biens meubles et immeubles acquis ou réalisés postérieurement au transfert de compétences sont répartis entre les communes qui reprennent la compétence ou entre la commune qui se retire de l'établissement public de coopération intercommunale et l'établissement ou, dans le cas particulier d'un syndicat dont les statuts le permettent, entre la commune qui reprend la compétence et le syndicat de communes. Il en va de même pour le produit de la réalisation de tels biens, intervenant à cette occasion. Le solde de l'encours de la dette contractée postérieurement au transfert de compétences est réparti dans les mêmes conditions entre les communes qui reprennent la compétence ou entre la commune qui se retire et l'établissement public de coopération intercommunale ou, le cas échéant, entre la commune et le syndicat de communes. A défaut d'accord entre l'organe délibérant de l'établissement public de coopération intercommunale et les conseils municipaux des communes concernés, cette répartition est fixée par arrêté du ou des représentants de l'Etat dans le ou les départements concernés. Cet arrêté est pris dans un délai de six mois suivant la saisine du ou des représentants de l'Etat dans le ou les départements concernés par l'organe délibérant de l'établissement public de coopération intercommunale ou de l'une des communes concernées. / (...) " ; que le SYNDICAT INTERCOMMUNAL DES EAUX DES CORNICHES ET DU LITTORAL (SIECL) soutient que ces dispositions méconnaissent le droit de propriété des personnes publiques, garanti notamment par l'article 17 de la Déclaration des droits de l'homme et du citoyen de 1789 ;<br/>
<br/>
<br/>
              Considérant que, par les dispositions précitées de l'article L. 5211-25-1 du code général des collectivités territoriales, le législateur a notamment défini les modalités de répartition, entre les personnes publiques concernées, en cas de retrait d'une compétence transférée à un établissement public de coopération intercommunale, des biens acquis ou réalisés par cet établissement postérieurement au transfert ; qu'une telle répartition entre personnes publiques n'exige, quelles qu'en soient les modalités concrètes, ni procédure d'expropriation ni, s'agissant des biens soumis au régime de la domanialité publique, déclassement préalable ; que le législateur a pu, sans méconnaître le droit de propriété garanti par l'article 17 de la Déclaration des droits de l'homme et du citoyen, prévoir que, dans l'hypothèse où les personnes publiques concernées ne parviendraient pas à un accord, le représentant de l'Etat fixe, sous le contrôle du juge de l'excès de pouvoir, la répartition des biens, afin d'éviter toute solution de continuité dans l'exercice, par ces personnes publiques, de leurs compétences ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la question de constitutionnalité soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; que, par suite, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question invoquée par le SIECL ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le président de la première chambre du tribunal administratif de Nice.<br/>
Article 2 : La présente décision sera notifiée au SYNDICAT INTERCOMMUNAL DES EAUX DES CORNICHES ET DU LITTORAL, à la Communauté urbaine de Nice Côte d'Azur et au ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration. <br/>
Copie en sera adressée au Conseil constitutionnel et au tribunal administratif de Nice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
