<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031861382</ID>
<ANCIEN_ID>JG_L_2015_12_000000391974</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/86/13/CETATEXT000031861382.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème SSJS, 30/12/2015, 391974, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391974</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLEZ</AVOCATS>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:391974.20151230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés respectivement les 22 juillet et 23 octobre 2015 au secrétariat du contentieux du Conseil d'Etat, la SC " Nathalie Cudel " a demandé au Conseil d'Etat d'annuler l'arrêt de la cour administrative d'appel de Nancy n° 14NC01368 du 13 mai 2015 en tant qu'il a partiellement rejeté son appel contre un jugement du tribunal administratif de Châlons-en-Champagne n° 1200739 du 29 avril 2014 rejetant partiellement sa demande tendant à la décharge de la cotisation supplémentaires d'impôt sur les sociétés  à laquelle elle a été assujettie au titre de l'année 2006. <br/>
<br/>
              A l'appui de ce pourvoi, la SC " Nathalie Cudel " a, en application de l'article 23-5 de l'ordonnance n°58-1067 du 7 novembre 1958, demandé au Conseil d'Etat, par un mémoire distinct et un nouveau mémoire enregistrés les 23 octobre et 16 décembre 2015, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du 2 de l'article 38 du code général des impôts. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bastien Lignereux, auditeur,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boullez, avocat de la SC Nathalie Cudel ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes du 2 de l'article 38 du code général des impôts : " Le bénéfice net est constitué par la différence entre les valeurs de l'actif net à la clôture et à l'ouverture de la période dont les résultats doivent servir de base à l'impôt diminuée des suppléments d'apport et augmentée des prélèvements effectués au cours de cette période par l'exploitant ou par les associés. L'actif net s'entend de l'excédent des valeurs d'actif sur le total formé au passif par les créances des tiers, les amortissements et les provisions justifiés ".<br/>
<br/>
              3. La SC " Nathalie Cudel " soutient, en premier lieu, que ces dispositions méconnaissent le principe d'égalité devant l'impôt garanti par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen en ce qu'elles n'édictent aucun critère objectif et rationnel permettant de fixer les modalités d'assiette en cas de rectification de l'évaluation des parts de sociétés non cotées à la suite d'une cession par une personne physique exerçant son activité professionnelle dans le cadre d'une société et permettent à l'administration, puis au juge de l'impôt d'opérer un choix discrétionnaire entre les différentes méthodes d'évaluation des immobilisations conduisant à fixer arbitrairement la valeur vénale des parts sociales cédées.<br/>
<br/>
              4. Toutefois, il résulte d'une jurisprudence constante, rappelée notamment dans la décision n° 229446 du 14 novembre 2003 du Conseil d'Etat statuant au contentieux, que la valeur vénale de titres non cotés en bourse doit être appréciée compte tenu de tous les éléments permettant d'obtenir un chiffre aussi voisin que possible de celui qu'aurait entraîné le jeu normal de l'offre et de la demande à la date où la cession est intervenue. Le pouvoir reconnu à l'administration fiscale de rectifier les valeurs déclarées par le contribuable s'exerce sous le contrôle du juge de l'impôt qui vérifie le respect par l'administration de ce principe comme la pertinence des méthodes d'évaluation retenues, parmi les méthodes objectives couramment pratiquées en la matière, au regard des caractéristiques propres à chaque situation. Dès lors, le grief tiré de ce que les dispositions litigieuses sont contraires aux articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen ne présente pas un caractère sérieux.<br/>
<br/>
              5. La requérante soutient, en second lieu qu'en adoptant les dispositions litigieuses, le législateur a méconnu l'étendue de sa propre compétence ainsi que le principe de clarté de la loi qui résultent de l'article 34 de la Constitution et l'objectif à valeur constitutionnelle d'accessibilité et d'intelligibilité de la loi qui découle des articles 4, 5, 6 et 16 de la Déclaration des droits de l'homme et du citoyen, en ce qu'elles ne fixent aucune modalité d'assiette que devrait respecter l'administration fiscale lorsqu'elle rectifie le prix de cession des parts sociales des sociétés non cotées ou de leur usufruit temporaire et qu'elles reportent ainsi sur les autorités administratives et juridictionnelles le soin de fixer ces règles. <br/>
<br/>
              6. Cependant, les dispositions contestées fixent avec une clarté et une précision suffisante les modalités de détermination du bénéfice imposable. Le législateur, en ne détaillant pas les règles d'évaluation que l'administration doit respecter en cas de rectification de la valeur des droits et parts cédés, n'a pas reporté sur des autorités administratives ou juridictionnelles la fixation de règles ou de principes que la Constitution place dans le domaine de la loi. Ces dispositions, complétées par la jurisprudence mentionnée au point 4, permettent au contribuable d'avoir une connaissance suffisamment précise des règles d'évaluation applicables en la matière. Par suite, le grief tiré de ce que le législateur aurait méconnu l'étendue de sa propre compétence ainsi que le principe de clarté de la loi et l'objectif à valeur constitutionnelle d'accessibilité et d'intelligibilité ne présente pas, en tout état de cause, un caractère sérieux.<br/>
<br/>
              7. Il résulte de ce qui précède que la question posée, qui n'est pas nouvelle, est dépourvue de caractère sérieux. Ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que les dispositions du 2 de l'article 38 du code général des impôts portent atteinte aux droits et libertés garantis par la Constitution doit être écarté. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la SC " Nathalie Cudel ". <br/>
Article 2 : La présente décision sera notifiée à la SC " Nathalie Cudel " et au ministre des finances et des comptes publics.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
