<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000045061482</ID>
<ANCIEN_ID>JG_L_2022_01_000000460331</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/45/06/14/CETATEXT000045061482.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 19/01/2022, 460331, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2022-01-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>460331</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP OHL, VEXLIARD</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2022:460331.20220119</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique enregistrés les 11 et 16 janvier 2022 au secrétariat du contentieux du Conseil d'Etat, Mme A... B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner au ministre de l'éducation nationale, de la jeunesse et des sports de prendre, à titre provisoire et transitoire, une mesure autorisant, pendant une durée d'au minimum huit semaines à compter de l'ordonnance à intervenir, les élèves de 12 ans qui seront déclarés cas contact à risque et ne seront pas vaccinés ou de manière incomplète, à pouvoir se rendre en classe en présentant un test antigénique à J+0 négatif ;<br/>
<br/>
              2°) à défaut, d'ordonner au ministre de l'éducation nationale, de la jeunesse et des sports de mettre en place un enseignement à distance quotidien pour les élèves de 12 ans et plus qui ne peuvent plus se présenter en classe durant sept jours ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - elle justifie d'un intérêt à agir dès lors qu'elle est mère d'une enfant née le 9 janvier 2010, scolarisée en classe de sixième, qui ne recevra sa première injection vaccinale que le 16 janvier 2022 et qui, déclarée cas contact à risque le 10 janvier 2022 est contrainte à l'isolement en dépit d'un test négatif ;<br/>
              - la condition d'urgence est satisfaite dès lors que le protocole mis en place par le ministre de l'éducation nationale contraint à un isolement de sept jours les enfants qui atteignent l'âge de douze ans sans avoir la possibilité de présenter un test antigénique négatif pour rester en classe alors qu'ils n'ont pas eu le temps d'accomplir la totalité de leur parcours vaccinal et qu'aucun enseignement à distance n'est prévu pour ces élèves ;<br/>
              - il est porté une atteinte grave et manifestement illégale au droit à l'éducation ;<br/>
              - le protocole contesté méconnaît le droit à l'éducation garanti par l'article L. 111-1 du code de l'éducation dès lors que les enfants de 12 ans et plus non vaccinés ou sans parcours vaccinal complet déclarés cas contacts à risque sont contraints à l'isolement sans qu'aucun enseignement à distance ne soit mis en place et sans que cet isolement puisse être levé en cas d'obtention d'un test négatif durant celui-ci ;<br/>
              - il porte une atteinte grave et manifestement illégale au principe d'égalité en ce qu'il crée une différence de traitement disproportionnée entre les enfants de 11 ans et ceux de 12 ans et plus dès lors que, d'une part, il n'existe pas de donnée scientifique qui permette d'affirmer qu'un élève de 12 ans et plus non vacciné ou vacciné de manière complète serait plus contagieux qu'un élève de 11 ans non vacciné et, d'autre part, les élèves ayant atteint l'âge de 12 ans au début de l'année 2022 se trouvent dans l'impossibilité d'obtenir un parcours vaccinal complet avant l'entrée en vigueur du protocole ; <br/>
              - il n'est pas assorti de dispositions transitoires pour prévoir le cas des élèves de 12 ans et plus qui ne sont pas encore en mesure de disposer d'un schéma vaccinal complet.<br/>
<br/>
              Par un mémoire en intervention, enregistré le 13 janvier 2022, l'Association des parents d'élèves de l'enseignement public PEEP Lavoisier et l'Association départementale et académique des parents d'élèves de l'enseignement public ADA PEEP Paris concluent à ce qu'il soit fait droit à la requête de Mme B.... Elles soutiennent que leur intervention est recevable, et reprennent les mêmes moyens que la requérante.<br/>
<br/>
              Par un mémoire en défense, enregistré le 14 janvier 2022, le ministre de l'éducation nationale, de la jeunesse et des sports conclut au rejet de la requête. Il soutient en premier lieu, que les conclusions de Mme B... ne relèvent pas de l'office du juge des référés, en second lieu, que la condition d'urgence n'est pas satisfaite, et que les moyens soulevés ne sont pas fondés. <br/>
<br/>
              Par un nouveau mémoire en intervention, enregistré le 16 janvier 2022, l'Association des parents d'élèves de l'enseignement public PEEP Lavoisier et l'Association départementale et académique des parents d'élèves de l'enseignement public ADA PEEP Paris soutiennent que leur intervention est recevable et s'associent aux moyens de la requête.<br/>
<br/>
              La requête a été communiquée au Premier ministre qui n'a pas produit d'observations. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de l'éducation ; <br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2021-689 du 31 mai 2021 ;<br/>
              - la loi n° 2021-1040 du 5 août 2021 ; <br/>
              - le décret n° 2021-699 du 1er juin 2021 ;<br/>
              - le de´cret n° 2021-724 du 7 juin 2021 ;<br/>
              - le décret n° 2021-1059 du 7 août 2021 ;<br/>
              - le décret n° 2021-1268 du 30 septembre 2021 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, Mme Janura, l'Association des parents d'élèves de l'enseignement public PEEP Lavoisier et l'Association départementale et académique des parents d'élèves de l'enseignement public ADA PEEP Paris et d'autre part, le Premier ministre et le ministre de l'éducation nationale, de la jeunesse et des sports ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 17 janvier 2022, à 11 heures : <br/>
<br/>
              - Me Vexliard, avocate au Conseil d'Etat et à la Cour de cassation, avocate de Mme Janura, de l'Association des parents d'élèves de l'enseignement public PEEP Lavoisier et de l'Association départementale et académique des parents d'élèves de l'enseignement public ADA PEEP Paris ;<br/>
<br/>
              - les représentants du ministre de l'éducation nationale, de la jeunesse et des sports ;<br/>
<br/>
              à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". <br/>
<br/>
              2. L'Association des parents d'élèves de l'enseignement public PEEP Lavoisier et de l'Association départementale et académique des parents d'élèves de l'enseignement public ADA PEEP Paris justifient d'un intérêt suffisant pour intervenir au soutien de la requête de Mme Janura.<br/>
<br/>
              3. Le ministre de l'éducation nationale, de la jeunesse et des sports a défini, depuis le 3 janvier 2022, un protocole sanitaire prévoyant un isolement de sept jours pour les élèves de douze ans et plus sans vaccination ou avec une vaccination incomplète lorsqu'ils sont contacts à risque d'une personne testée positive au covid-19. Ce même protocole a prévu que les élèves de moins de douze ans qui sont également contact à risque pourraient rester en classe s'ils subissent un test négatif immédiat, à J+2 et J+4, les autotests étant admis à partir du 14 janvier 2022. Mme Janura demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au ministre de l'éducation nationale, de la jeunesse et des sports de prendre, à titre provisoire et transitoire, une mesure autorisant pendant une durée d'au minimum huit semaines les élèves de douze ans qui seront déclarés cas contact à risque et ne seront pas vaccinés ou de manière incomplète à pouvoir se rendre en classe en présentant un test négatif et, à titre subsidiaire, d'enjoindre au ministre de l'éducation nationale, de la jeunesse et des sports de mettre en place un enseignement à distance quotidien pour ces élèves. <br/>
<br/>
              4. Il résulte de l'instruction que le protocole sanitaire contesté transpose en milieu scolaire les règles sanitaires prises sur le fondement des recommandations du Haut conseil de la santé publique figurant dans son avis du 31 décembre 2021 dans le cadre d'une propagation très rapide du variant Omicron. Ces règles sanitaires prévoient l'absence de quarantaine pour les personnes contacts lorsqu'ils possèdent un schéma vaccinal complet et le maintien d'une quarantaine de 7 jours pleins pour les personnes contacts avec un schéma vaccinal incomplet et les personnes non vaccinées. Le protocole contesté applique ces règles aux mineurs de plus de douze ans auxquels s'appliquent de manière général la réglementation liée au statut vaccinal depuis le 30 septembre 2021. <br/>
<br/>
              5. Mme Janura ne conteste pas l'application de ces règles mais estime qu'il serait nécessaire de prévoir une période transitoire permettant aux élèves atteignant l'âge de douze ans de pouvoir se rendre en classe en présentant un test négatif comme cela est prévu pour les enfants de moins de douze ans. Il résulte toutefois de l'instruction que la vaccination a été ouverte à l'ensemble des enfants à compter du 23 décembre et que la durée pour obtenir une vaccination complète a été réduite à trois semaines. L'ensemble des enfants atteignant l'âge de douze ans sont donc dorénavant susceptibles de disposer d'un schéma vaccinal complet. Si, comme l'a fait valoir à juste titre Mme Janura lors de l'audience publique, tel n'est pas encore le cas pour certains enfants venant juste d'avoir douze ans, compte tenu des délais d'obtention des rendez-vous de vaccination, cette situation a vocation à disparaître de manière rapide. Par suite, il n'apparaît pas que le protocole contesté porterait une atteinte grave et manifestement illégale au droit à l'éducation en ne prévoyant pas une dérogation transitoire au principe d'isolement pour les élèves de douze ans ne disposant pas d'un schéma vaccinal complet lorsqu'ils sont contacts à risque d'une personne testée positive au covid-19.<br/>
<br/>
              6. Il résulte de ce qui précède que, sans qu'il soit besoin de statuer sur la condition d'urgence, la demande de Mme Janura doit être rejetée, y compris ses concluions au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : L'intervention de l'Association des parents d'élèves de l'enseignement public PEEP Lavoisier et de l'Association départementale et académique des parents d'élèves de l'enseignement public ADA PEEP Paris est admise.<br/>
Article 2 : La requête de Mme Janura est rejetée.<br/>
Article 3 : La présente ordonnance sera notifiée à Mme Cécile Janura, au ministre de l'éducation nationale, de la jeunesse et des sports et à l'Association des parents d'élèves de l'enseignement public PEEP Lavoisier, première intervenante dénommée.<br/>
Copie sera adressée au Premier ministre.<br/>
<br/>
Fait à Paris, le 19 janvier 2022<br/>
Signé : Mathieu Hérondart<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
