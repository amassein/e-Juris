<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043741128</ID>
<ANCIEN_ID>JG_L_2021_06_000000452635</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/74/11/CETATEXT000043741128.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 04/06/2021, 452635, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>452635</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:452635.20210604</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 17 mai 2021 au secrétariat du contentieux du Conseil d'Etat, l'association Interprofessionnelle des fruits et légumes frais (INTERFEL) demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision du 20 novembre 2020 de refus d'extension par laquelle le ministre de l'agriculture et de l'alimentation a refusé d'étendre l'accord interprofessionnel " Pomme - Calibrage au poids " relatif aux campagnes 2021 à 2023 conclu dans le cadre de l'association Interfel ;<br/>
<br/>
              2°) d'ordonner la suspension de l'exécution de la décision du ministre de l'agriculture et de l'alimentation rejetant implicitement le recours administratif formé par l'association Interfel contre cette décision ; <br/>
<br/>
              3°) d'enjoindre au ministre de l'agriculture et de l'alimentation, au titre de l'article L. 911-1 du code de justice administrative, de procéder, dans le délai d'un mois à compter de la notification de l'ordonnance à intervenir, à la publication d'un avis relatif à la décision tacite d'extension de l'accord interprofessionnel " Pomme - Calibrage au poids " relatif aux campagnes 2020 à 2023 conclu dans le cadre de l'association Interfel, conformément à l'article L. 632-4 du code rural et de la pêche maritime, sous astreinte de 500 euros par jour de retard ; <br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative et de le condamner aux entiers dépens.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - la condition d'urgence est satisfaite dès lors que la décision de refus d'extension des accords relatifs au calibrage au poids des pommes pour la campagne 2021 à 2023 préjudicie de manière suffisamment grave et immédiate aux intérêts qu'elle a pour objet légal et statutaire de défendre en ce que, d'une part, la perte de caractère obligatoire de ces normes entraîne un risque important de méconnaissance de celles-ci qui aboutira à une désorganisation du marché et à un risque de mise sur le marché de produits ne disposant pas du niveau de qualité équivalent à celui préexistant et, d'autre part, ces effets peuvent déjà être constatés dans la mesure où l'accord interprofessionnel régissant le calibrage des pommes était supposé entrer en vigueur le 1er janvier 2021, pour une durée de trois ans ; <br/>
              - il existe un doute sérieux quant à la légalité de la décision contestée ; <br/>
              - la décision de refus est intervenue hors du délai prévu par l'article L. 632-4 du code rural et de la pêche maritime, qui courait jusqu'au 7 novembre 2020, ce qui implique que, d'une part, le demande d'extension formulée par l'association Interfel est réputée acceptée et, d'autre part, l'administration était nécessairement tenue de publier dans les meilleurs délais un avis d'acceptation tacite de la demande ; <br/>
              - elle est entachée d'incompétence dès lors que la décision litigieuse émane de l'adjoint au sous-directeur des filières agroalimentaires alors même que les décisions de refus d'extension doivent être signées par la direction générale de la performance économique et environnementale des entreprises (DGPE) ; <br/>
              - elle est entachée d'un défaut de motivation dès lors que la décision est manifestement dépourvue de clarté et de précision ; <br/>
              - elle méconnaît les principes de sécurité juridique et de confiance légitime dès lors que l'administration, en modifiant sa position et en refusant d'étendre l'accord relatif au calibrage des pommes, va à l'encontre de la confiance que les opérateurs ont pu placer dans la stabilité des relations juridiques qui prévalaient ; <br/>
              - elle est entachée de détournement de pouvoir, l'administration ayant exercé un contrôle d'opportunité et non de légalité, en refusant d'étendre l'accord interprofessionnel au motif que l'amélioration qualitative des produits ne serait pas suffisamment justifiée ; <br/>
              - elle est entachée d'erreur manifeste d'appréciation dès lors qu'elle ne prend pas en compte les éléments de justification relatifs au caractère qualitatif des restrictions prévues dans l'accord, notamment sur le mode de calibrage, qui participe de la préservation et de l'amélioration de la qualité du produit au profit du consommateur. <br/>
<br/>
              Par un mémoire en défense, enregistré le 25 mai 2021, le ministre de l'agriculture et de l'alimentation conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas satisfaite, et qu'aucun des moyens soulevés n'est de nature à créer un doute sérieux sur la légalité de la décision contestée.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le règlement (UE) n° 1308/2013 du Parlement européen et du Conseil du 17 décembre 2013 ; <br/>
              - le règlement d'exécution (UE) n° 543/2011 de la Commission du 7 juin 2011 ; <br/>
              - le code rural et de la pêche maritime ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, l'association Interprofessionnelle des fruits et légumes frais (INTERFEL), et d'autre part, le ministre de l'agriculture et de l'alimentation ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 27 mai 2021, à 14 heures 30 : <br/>
<br/>
              - les représentants de l'association Interprofessionnelle des fruits et légumes frais ; <br/>
<br/>
              - les représentants du ministre de l'agriculture et de l'alimentation et du ministre de l'économie, des finances et de la relance ;  <br/>
<br/>
              à l'issue de laquelle le juge des référés a clos l'instruction.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              2. Il résulte de l'instruction que l'association Interprofessionnelle des fruits et légumes frais (INTERFEL) a conclu plusieurs accords interprofessionnels successifs sur le calibrage au poids des pommes. Ces accords ont été étendus par des arrêtés du ministre chargé de l'agriculture. L'association INTERFEL a conclu, le 10 juin 2020, un nouvel accord interprofessionnel sur le calibrage au poids des pommes pour les campagnes 2021 à 2023. Elle a sollicité l'extension de cet accord au ministre de l'agriculture et de l'alimentation par une demande enregistrée le 7 juillet 2020. L'association INTERFEL demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, d'ordonner la suspension de l'exécution de la décision du 20 novembre 2020 refusant d'étendre cet accord interprofessionnel ainsi que de la décision rejetant son recours administratif contre cette décision.<br/>
<br/>
              3. En premier lieu, en vertu de l'article L. 632-3 du code rural et de la pêche maritime, les accords conclus dans le cadre d'une organisation interprofessionnelle reconnue peuvent être étendus, pour une durée déterminée, par arrêté de l'autorité administrative compétente dès lors qu'ils prévoient des actions communes ou visant un intérêt commun conformes à l'intérêt général et compatibles avec la législation de l'Union européenne. Ces arrêtés ont un caractère réglementaire. Aux termes de l'article L. 632-4 du même code : " (...) L'autorité compétente dispose d'un délai de deux mois à compter de la réception de la demande présentée par l'organisation interprofessionnelle pour statuer sur l'extension sollicitée. (...) Lorsque la communication de documents complémentaires est nécessaire à l'instruction de la demande d'extension, l'autorité compétente peut prolonger ce délai de deux mois non renouvelables. (...) Si, au terme du délai qui lui est imparti pour statuer sur la demande d'extension, l'autorité compétente n'a pas notifié sa décision, cette demande est réputée acceptée. / Les décisions de refus d'extension doivent être motivées. ". Aux termes de l'article L. 243-3 du code des relations entre le public et l'administration : " L'administration ne peut retirer un acte réglementaire ou un acte non réglementaire non créateur de droits que s'il est illégal et si le retrait intervient dans le délai de quatre mois suivant son édiction ".<br/>
<br/>
              4. Il résulte des dispositions de l'article L. 632-4 du code rural et de la pêche maritime que la réception de la demande d'extension le 7 juillet 2020 et la demande d'éléments complémentaires adressée à l'association INTERFEL par le ministère de l'agriculture et de l'alimentation le 7 septembre 2020 étaient de nature à faire naître une décision implicite d'acceptation à l'expiration d'un délai de quatre mois. L'expiration de ce délai ne faisait toutefois pas obstacle à ce que l'administration retire cette décision implicite dans le respect des conditions fixées par l'article L. 243-3 du code des relations entre le public et l'administration. Le moyen tiré de ce que la décision contestée serait illégale au motif que la demande d'extension de l'accord interprofessionnel était réputée acceptée n'est donc pas, en l'état de l'instruction, de nature à créer un doute sérieux quant à la légalité de la décision.<br/>
<br/>
              5. En deuxième lieu, les moyens tirés de ce que la décision litigieuse serait entachée d'incompétence, faute pour son auteur de disposer d'une délégation de signature régulière, et serait irrégulière faute de comprendre la motivation exigée par le dernier alinéa de l'article L. 632-4 du code rural et de la pêche maritime ne sont pas, en l'état de l'instruction, de nature à fait naître un doute sérieux quant à la légalité de la décision.<br/>
<br/>
              6. En troisième lieu, en vertu de l'article 74 du règlement (UE) n° 1308/2013 du Parlement européen et du Conseil du 17 décembre 2013 portant organisation commune des marchés des produits agricoles, les produits pour lesquels des normes de commercialisation ont été fixées ne peuvent être commercialisés dans l'Union que s'ils sont conformes auxdites normes. En application des dispositions de l'article 75 du même règlement, le règlement d'exécution (UE) n° 543/2011 de la Commission du 7 juin 2011 fixe des normes de commercialisation spécifiques pour les pommes. Le III de la partie 1 de la partie B de l'annexe 1 de ce règlement d'exécution comprend ainsi des dispositions concernant le calibrage des pommes. Il prévoit notamment que " Le calibre est déterminé par le diamètre maximal de la section équatoriale ou par le poids ". <br/>
<br/>
              7. Il résulte de l'instruction que, pour refuser l'extension de l'accord interprofessionnel, le ministre de l'agriculture et de l'alimentation s'est notamment fondé sur le fait que cet accord prescrit un mode obligatoire de mesure de calibre au poids alors que la norme européenne applicable à la pomme laisse le choix aux opérateurs de calibrer les fruits au poids ou au diamètre. Un tel motif tiré de l'incompatibilité de l'accord avec la législation européenne était de nature à fonder la décision refusant l'extension de l'accord en application des dispositions rappelées aux points 3 et 6. Par suite, les moyens tirés de ce que la décision litigieuse serait entachée de détournement de pouvoir et d'erreur manifeste d'appréciation ne paraissent pas, en l'état de l'instruction, propres à créer un doute sérieux quant à sa légalité.<br/>
<br/>
              8. En dernier lieu, il résulte des dispositions de l'article L. 632-3 du code rural et de la pêche maritime que l'extension des accords conclus dans le cadre d'une organisation interprofessionnelle reconnue a lieu pour une durée déterminée. Les principes de sécurité juridique et de confiance légitime ne sauraient imposer à l'administration de renouveler l'extension d'un accord si elle estime que ce dernier n'est pas compatible avec la législation de l'Union européenne. Par suite, le moyen tiré de ce que la décision litigieuse porterait atteinte aux principes de sécurité juridique et de confiance légitime ne paraît pas, en l'état de l'instruction, propre à créer un doute sérieux sur la décision contestée.<br/>
<br/>
              9. Il résulte de ce qui précède, sans qu'il besoin de se prononcer sur la condition d'urgence, que la requête ne peut qu'être rejetée, y compris les conclusions à fin d'injonction et celles présentées au titre de l'article L. 761-1 du code de justice administrative<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de l'association Interprofessionnelle des fruits et légumes frais est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à l'association Interprofessionnelle des fruits et légumes frais et au ministre de l'agriculture et de l'alimentation.<br/>
Copie en sera adressée au ministère de l'économie, des finances et de la relance.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
