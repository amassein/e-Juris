<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044087024</ID>
<ANCIEN_ID>JG_L_2021_08_000000455678</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/08/70/CETATEXT000044087024.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 20/08/2021, 455678, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-08-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>455678</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:455678.20210820</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 18 août 2021 au secrétariat du contentieux du Conseil d'Etat, M. B... A... et l'association Bonsens.org demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de transmettre au Conseil constitutionnel la question prioritaire de constitutionnalité portant sur l'article L. 3136-1 du code de la santé publique et le VIII de l'article 1er de la loi n° 2021-689 du 31 mai 2021 jointe à l'appui de sa requête ; <br/>
<br/>
              2°) de transmettre au Conseil constitutionnel la question prioritaire de constitutionnalité portant sur les articles L. 521-2 et L. 522-3 du code de justice administrative ;<br/>
<br/>
              3°) de ne pas surseoir à statuer dans l'attente de la décision du Conseil constitutionnel ; <br/>
<br/>
              4°) d'enjoindre au Premier ministre de produire dans un délai de trois jours suivant l'ordonnance à intervenir des " QR codes " de " passes sanitaires " conformes aux exigences du B du II de l'article 1er de la loi n° 2021-689 du 31 mai 2021 modifiée, c'est-à-dire ne permettant pas de connaître la nature du document et de savoir s'il s'agit d'une attestation de test virologique, d'une vaccination ou d'un certificat de rétablissement, sous astreinte de 500 euros par jour de retard au profit de M. A... et de 1 000 000 euros par jour de retard au profit de l'association Bonsens.org ; <br/>
<br/>
              5°) de suspendre l'exécution du chapitre 2 du titre Ier, notamment de son article 2-3 et de l'article 47-1 du décret n° 2021-699 du 1er juin 2021 modifié, dans l'attente de la réémission des certificats avec un " QR code " répondant aux exigences du B du II de l'article 1er de la loi n° 2021-689 du 31 mai 2021 modifiée, c'est-à-dire ne comprenant pas la nature du document mais uniquement les informations nécessaires, à savoir le nom, les prénoms, la date de naissance et la date limite de validité du certificat ; <br/>
<br/>
              6°) à titre subsidiaire, d'interdire temporairement certaines activités exposant particulièrement à un risque de contamination si la suspension de l'exécution du " passe sanitaire " est considérée comme faisant courir des risques sanitaires importants ;<br/>
<br/>
              7°) de mettre à la charge de l'Etat le versement de la somme de 500 euros à M. A... au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
              - ils justifient d'un intérêt à agir dès lors que, d'une part, l'obligation de se munir d'un " passe sanitaire " restreint les libertés fondamentales de M. A... en ce qu'elle l'empêche de se rendre notamment dans les restaurants, hypermarchés et à la piscine municipale pour participer à son club de plongée et, d'autre part, l'association Bonsens.org a pour objet statutaire la sauvegarde de la santé et la défense des libertés, notamment dans le cadre de la crise sanitaire ;<br/>
              - la condition d'urgence est satisfaite dès lors, en premier lieu, que les dispositions contestées affectent gravement la vie quotidienne de millions de Français, dont la sienne et celle des adhérents de l'association et restreignent plusieurs libertés fondamentales dont celle de l'accès aux soins, ou à la nourriture et la liberté d'aller et venir, en deuxième lieu, qu'elles mettent dans l'impossibilité les citoyens respectueux des lois de se conformer à celles-ci et les exposent à une amende au titre de l'article L. 3136 du code de la santé publique s'ils utilisent des " QR codes " comportant des informations illégales sur la nature du document et, en dernier lieu, que le dispositif en cause les expose à des atteintes au secret médical ; <br/>
              - le juge des référés n'est pas tenu, de maintenir l'exécution des dispositions relatives au " passe sanitaire ", en opposant la nécessité d'ordre public consistant à assurer la protection de la population contre les risques sanitaires importants résultant de la circulation d'une forme très contagieuse du covid-19, dès lors que le Gouvernement dispose d'alternatives en recourant, comme il l'a déjà fait, à la proclamation de l'état d'urgence sanitaire et qu'il est loisible au juge des référés d'interdire temporairement certaines activités exposant à un risque particulier de contamination ; <br/>
              - il est porté une atteinte grave et manifestement illégale aux libertés fondamentales ; <br/>
              - les dispositions contestées méconnaissent les dispositions de la loi n° 2021-689 du 31 mai 2021 modifiée, dès lors que, en premier lieu, les " QR codes délivrés " sont susceptibles de permettre de connaître la nature du document et de savoir s'il s'agit d'une attestation de test virologique, d'une vaccination ou d'un certificat de rétablissement, en deuxième lieu, la Commission nationale de l'informatique et des libertés (CNIL) a alerté sur la possibilité d'avoir accès à des données protégées par le secret médical notamment sur l'application " TousAntiCovid " et, en troisième lieu, les possesseurs d'un " QR code " sont exposés à une amende au titre de l'article L. 3136 du code de la santé publique s'ils utilisent des " QR codes " comportant des informations sur la nature du document et, en dernier lieu, il les expose à des atteintes au secret médical ;<br/>
              - l'article 47-1 du décret du 1er juin 2021 viole manifestement les dispositions du B du II de l'article 1er de la loi n° 2021-689 du 31 mai 2021 modifiée qui prévoient que la présentation des documents doit être faite sous une forme ne permettant pas aux personnes autorisées à en assurer le contrôle d'en connaître la nature.<br/>
<br/>
              Par deux mémoires distincts, enregistrés le 18 août 2021, présenté en application de l'article 23-5 de l'ordonnance du 7 novembre 1958, M. A... demande au juge des référés du Conseil d'Etat de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution, d'une part, de l'article L. 3136-1 du code de la santé publique et des dispositions du VIII de l'article 1er de la loi n° 2021-689 du 31 mai 2021 et, d'autre part, des articles L. 521-2 et L. 522-3 du code de justice administrative. Il soutient que ces dispositions sont applicables au litige qu'elles n'ont pas été déclarées conformes à la Constitution et qu'elles posent une question nouvelle et sérieuse.<br/>
<br/>
<br/>
              Vu les pièces du dossier.<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - la loi n° 2021-689 du 31 mai 2021 ;<br/>
              - la loi n° 2021-1040 du 5 août 2021 ;<br/>
              - le décret n° 2021-699 du 1er juin 2021 ;<br/>
              - le décret n° 2021-1059 du 7 août 2021 ;<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Il résulte de la combinaison des dispositions de l'ordonnance n° 58-1067 du 7 novembre 1958 avec celles du livre V du code de justice administrative qu'une question prioritaire de constitutionnalité peut être soulevée devant le juge administratif des référés statuant sur le fondement de l'article L. 521-2 de ce même code. Le juge des référés peut en toute hypothèse, y compris lorsqu'une question prioritaire de constitutionnalité est soulevée devant lui, rejeter une requête qui lui est soumise pour incompétence de la juridiction administrative, irrecevabilité ou défaut d'urgence.<br/>
<br/>
              3. Les requérants demandent en substance au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au Premier ministre de produire des codes de " passes sanitaires " ne permettant pas de connaître la nature du document et de savoir s'il s'agit d'une attestation de test virologique, d'une vaccination ou d'un certificat de rétablissement et, dans l'attente, de suspendre l'exécution du chapitre 2 (" Passe sanitaire ") du titre Ier, notamment de l'article 2-3 ainsi que les dispositions de l'article 47-1 du titre 4 (" Dispositions concernant les établissements et activités ") du décret du 1er juin 2021 modifié.<br/>
<br/>
              4. Pour justifier de l'urgence à ce qu'il soit fait droit à leurs conclusions, les requérants soutiennent qu'il serait possible, à l'aide de dispositifs accessibles anonymement sur Internet, de détourner le dispositif du " QR code " mis en place dans le cadre du " passe sanitaire " afin d'accéder illégalement aux informations médicales sensibles qu'il contiendrait. <br/>
<br/>
              5. Eu égard cependant, d'une part, à l'état de la situation sanitaire au vu de laquelle le législateur a adopté la loi du 5 août 2021 relative à la gestion de la sortie de crise sanitaire, laquelle permet de subordonner l'accès à certains lieux, établissements, services ou événements à la présentation d'un " passe sanitaire " et, d'autre part, à l'intérêt public qui s'attache à l'exécution de ces mesures, compte tenu de la persistance de l'épidémie et des délais nécessaires au déploiement de la vaccination, la condition d'urgence particulière requise par l'article L. 521-2 du code de justice administrative, qui doit s'apprécier en l'espèce au regard des dispositions relatives à la gestion de la sortie de crise sanitaire et non, contrairement à ce qui est soutenu, par rapport à celles non retenues de proclamation d'une nouvelle période d'état d'urgence sanitaire, n'est pas remplie.<br/>
<br/>
              6. Dès lors, et sans qu'il soit besoin de se prononcer sur la transmission au Conseil constitutionnel des questions prioritaires de constitutionnalité soulevées, la requête de M. A... et autre doit être rejetée, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, selon la procédure prévue à l'article L. 522-3 du même code.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A... et autre est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B... A..., premier requérant dénommé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
