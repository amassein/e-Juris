<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033473448</ID>
<ANCIEN_ID>JG_L_2016_11_000000404952</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/47/34/CETATEXT000033473448.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 22/11/2016, 404952, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-11-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404952</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2016:404952.20161122</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...C..., agissant en qualité de représentant légal de son fils mineur, M. B...C..., a demandé au juge des référés du tribunal administratif de Toulouse, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au préfet de la Haute-Garonne de procéder à l'enregistrement de la demande d'asile de son fils dans le délai de quarante-huit heures à compter du prononcé de l'ordonnance à intervenir.<br/>
              Par une ordonnance n° 1604762 du 27 octobre 2016, le juge des référés du tribunal administratif de Toulouse a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 9 novembre 2016 au secrétariat du contentieux du Conseil d'État, Mme C...demande au juge des référés du Conseil d'État, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) de l'admettre au bénéfice de l'aide juridictionnelle provisoire et de mettre à la charge de l'Etat la somme de 3 000 euros sur le fondement des dispositions des articles L. 761-1 du code de justice administrative et 37, alinéa 2 de la loi du 10 juillet 1991 relative à l'aide juridictionnelle.<br/>
<br/>
<br/>
             Elle soutient que :  <br/>
             - le refus du préfet de délivrance et d'enregistrement d'un dossier de demande d'asile pour son fils porte atteinte au droit d'asile de celui-ci, et son corollaire, le droit de solliciter l'asile ;<br/>
             - ce refus méconnaît, par la suite, l'article 22 de la Convention internationale relative aux droits de l'enfant et la directive 2013/32/UE du 26 juin 2013 ;<br/>
             - à supposer que le préfet ait entendu désigner la Pologne comme l'Etat membre responsable de la demande d'asile de son fils, celui-ci bénéficie d'un droit de se maintenir sur le territoire jusqu'à la notification d'une décision de transfert ;<br/>
             - l'ordonnance du 27 octobre 2016 a méconnu le principe de l'intérêt supérieur de l'enfant dès lors que le juge des référés du tribunal administratif de Toulouse a considéré qu'il était dans l'intérêt supérieur de son fils de demeurer auprès de sa mère en Pologne.<br/>
<br/>
<br/>
             Vu les autres pièces du dossier ;<br/>
<br/>
             Vu :<br/>
             - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
             - la convention internationale relative aux droits de l'enfant du 26 janvier 1990 ;<br/>
             - le règlement UE n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
             - la directive 2013/32/UE du 26 juin 2013 <br/>
             - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
             - la loi n° 91-647 du 10 juillet 1991 <br/>
             - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
             Considérant ce qui suit :<br/>
<br/>
             1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. Il appartient au juge des référés saisi en appel de porter son appréciation sur ce point au regard de l'ensemble des pièces du dossier, et notamment des éléments recueillis par le juge de première instance dans le cadre de la procédure écrite et orale qu'il a diligentée.<br/>
<br/>
             2. Le droit constitutionnel d'asile, qui a le caractère d'une liberté fondamentale, a pour corollaire le droit de solliciter le statut de réfugié. S'il implique que l'étranger qui sollicite la reconnaissance de la qualité de réfugié soit en principe autorisé à demeurer sur le territoire jusqu'à ce qu'il ait été statué sur sa demande, ce droit s'exerce dans les conditions définies par l'article L. 741-1 du code de l'entrée et du séjour des étrangers et du droit d'asile qui met en oeuvre les dispositions du règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013, qui prévoit que l'autorité compétente enregistre la demande présentée par un demandeur d'asile présent sur le territoire national et procède à la détermination de l'Etat responsable de son examen par application des règles du droit de l'Union.<br/>
<br/>
             3. MmeC..., de nationalité russe et d'origine tchétchène, est entrée irrégulièrement en France le 29 février 2016, accompagnée de son fils mineur, M. B... C.... Elle a sollicité l'asile auprès des services de la préfecture de la Haute-Garonne le 10 mars 2016. Le fichier Eurodac ayant fait apparaître qu'elle avait antérieurement déposé une demande d'asile en Pologne, le préfet du Tarn a, par deux arrêtés du 19 mai 2016, décidé de la réadmission de Mme C... en Pologne et l'a assignée à résidence pour une durée de 45 jours. Par un jugement du 23 mai 2016, le président du tribunal administratif de Toulouse a rejeté le recours formé par Mme C...contre les arrêtés du 19 mai 2016. La requérante s'est présentée, le 29 septembre 2016, avec son fils mineur M. B... C..., au guichet du service des étrangers de la préfecture de la Haute-Garonne pour déposer une demande d'asile au bénéfice de ce dernier. Elle s'est vue opposer un refus de guichet. Par un arrêté du 19 octobre 2016, le préfet du Tarn a placé en rétention administrative Mme C... avec son fils. Par une ordonnance du 24 octobre 2016, le juge des libertés et de la détention du tribunal de grande instance de Toulouse a ordonné la prolongation de la rétention de Mme C... et de son fils. Mme C... relève appel de l'ordonnance n° 1604762 du 27 octobre 2016, par laquelle le juge des référés du tribunal administratif de Toulouse a rejeté sa requête tendant à ce qu'il soit enjoint au préfet de la Haute-Garonne d'enregistrer la demande d'asile de son fils M. B... C....<br/>
<br/>
             4. Mme C...soutient que le refus du préfet d'enregistrer la demande de son fils mineur âgé de quatorze ans méconnaît son droit d'asile, et son corollaire, le droit de solliciter l'asile. Toutefois, d'une part, le préfet n'avait pas à faire application de la procédure d'enregistrement autonome de la demande d'asile pour les mineurs non accompagnés, prévue par les dispositions des articles L. 741-3 et L. 741-4 du code de l'entrée et du séjour des étrangers et du droit d'asile, dès lors qu'il est constant que M.B... C... est entré en France avec sa mère, MmeC.... D'autre part, il ressort de l'instruction diligentée par le juge des référés du tribunal administratif de Toulouse, que les autorités polonaises ont accepté de prendre en charge le fils de la requérante comme mineur accompagnant, rien ne faisant obstacle à ce que  la demande d'asile qu'il pourrait alors formuler soit examinée en Pologne, sans qu'il soit en rien nécessaire que le préfet, qui ne s'est nullement fondé sur la qualification de la Pologne comme Etat responsable de l'examen de cette éventuelle demande pour apprécier l'intérêt supérieur de l'enfant,  s'en assure au préalable. Enfin, ainsi que l'a constaté à bon droit le juge des référés du tribunal administratif de Toulouse, il est dans l'intérêt supérieur de son fils de demeurer auprès de sa mère et donc de l'accompagner  en Pologne, sans que la circonstance qu'il ait été scolarisé ou soigné lors de son séjour en France contrarie cette appréciation. Mme C...ne peut donc soutenir qu'il a été porté une atteinte grave et manifestement illégale aux droits de son fils mineur ni au regard de l'asile, ni au regard de son intérêt ou de sa vie privée, Mme C...n'est par suite  pas fondée à soutenir que c'est à tort que le juge des référés du tribunal administratif de Toulouse a estimé que le refus d'enregistrer la demande d'asile de M. B... C..., mineur non isolé, accompagné de sa mère, ne portait pas une atteinte grave et manifestement illégale à une liberté fondamentale.<br/>
<br/>
             5. Il résulte de ce qui précède que l'appel de Mme C...ne peut être accueilli. Sans qu'il y ait lieu d'accorder à l'intéressée l'aide juridictionnelle à titre provisoire, sa requête, y compris les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991, doit en conséquence être rejetée selon la procédure prévue par l'article L. 522-3 de ce code.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de Mme C...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme A...C....<br/>
Copie en sera adressée, pour information, au préfet de la Haute-Garonne.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
