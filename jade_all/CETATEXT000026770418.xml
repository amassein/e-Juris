<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026770418</ID>
<ANCIEN_ID>JG_L_2012_12_000000342713</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/77/04/CETATEXT000026770418.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  8ème sous-section jugeant seule, 14/12/2012, 342713, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-12-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>342713</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 8ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BERTRAND</AVOCATS>
<RAPPORTEUR>Mme Maryline Saleix</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:342713.20121214</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi, enregistré le 24 août 2010 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre du budget, des comptes publics et de la fonction publique ; il demande au Conseil d'Etat d'annuler les articles 3 et 4 du jugement n° 0615671 du 1er juin 2010 en tant que par ces articles le tribunal administratif de Paris a prononcé la décharge de l'obligation de payer les sommes correspondant à des cotisations de taxe d'habitation dues par Mme A au titre des années 1994, 1995 et 1997 à 2000 ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Maryline Saleix, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de Me Bertrand, avocat de Mme A,<br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Bertrand, avocat de Mme A ;<br/>
<br/>
<br/>
<br/>
<br/>
              Sans qu'il soit besoin d'examiner les moyens présentés par le ministre ;<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, le 27 juin 2006, les trésoriers des 2e et 4e divisions du 16e arrondissement de Paris ont fait délivrer des procès-verbaux d'opposition sur saisie antérieure à l'encontre de M. et Mme A pour obtenir le paiement, notamment, des cotisations de taxe d'habitation dont ils étaient redevables au titre des années 1994, 1995 et 1997 à 2000 ; que Mme A a contesté l'obligation de payer en résultant par lettres du 4 juillet 2006 ; que le receveur général des finances, trésorier payeur général de la région Ile-de-France a rejeté son opposition aux actes de poursuite par décision du 31 août 2006 ; que par les articles 3 et 4 du jugement n° 0615671 du 1er juin 2010, contre lesquels le ministre du budget, des comptes publics et de la réforme de l'Etat se pourvoit en cassation, le tribunal administratif de Paris a déchargé Mme A de l'obligation de payer les sommes résultant de ces procès-verbaux ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 277 du livre des procédures fiscales : " Le contribuable qui conteste le bien-fondé ou le montant des impositions mises à sa charge peut, s'il en a expressément formulé la demande dans sa réclamation et précisé le montant ou les bases du dégrèvement auquel il estime avoir droit, être autorisé à différer le paiement de la partie contestée de ces impositions et des pénalités y afférentes. Le sursis de paiement ne peut être refusé au contribuable que s'il n'a pas constitué auprès du comptable les garanties propres à assurer le recouvrement de la créance du Trésor. (...) L'exigibilité de la créance et la prescription de l'action en recouvrement sont suspendues jusqu'à ce qu'une décision définitive ait été prise sur la réclamation soit par l'administration, soit par le tribunal compétent. " ; qu'il résulte de ces dispositions que les impositions contestées par un contribuable qui a formé une réclamation assortie d'une demande de sursis de paiement cessent d'être exigibles à compter de la date à laquelle le sursis a été accordé ; que, par suite, les actes de poursuite notifiés antérieurement au contribuable deviennent caducs à compter de cette date ; qu'il appartient au contribuable, si les impositions redeviennent exigibles, d'engager une nouvelle procédure afin de poursuivre la contestation du recouvrement de celles-ci ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme A a présenté le 15 septembre 2006 des réclamations, dont l'administration a accusé réception le 18 septembre suivant, par lesquelles elle demandait la décharge des impositions qui ont fait l'objet des actes de poursuite délivrés à son encontre le 27 juin 2006 ; que ces réclamations étaient assorties de demandes de sursis de paiement qui ont eu pour effet immédiat de suspendre l'exigibilité des impositions en litige ; que Mme A soutient devant le Conseil d'Etat que, par le même jugement devenu définitif sur ce point en ce qui concerne les impositions autres que celles en litige à la suite du rejet par la cour administrative d'appel de Paris du recours du ministre chargé du budget, le tribunal administratif a jugé que les réclamations adressées au service d'assiette ne pouvaient être regardées comme régulières de sorte qu'elles ne pouvaient avoir eu pour effet de lui ouvrir le bénéfice du sursis de paiement ; qu'elle en déduit qu'il n'appartenait pas au tribunal de soulever d'office, en raison de la caducité des actes de poursuite, l'irrecevabilité de sa demande tendant à la décharge de l'obligation de payer les cotisations de taxe d'habitation ; que, toutefois, il ressort des pièces du dossier soumis aux juges du fond qu'elle se prévalait, sans être contredite, de ce que les impositions en litige avaient cessé d'être exigibles du fait de la présentation de ces réclamations d'assiette assorties d'une demande de sursis de paiement ; qu'elle ne soutient pas devant le juge de cassation que ce sursis ne lui aurait pas été accordé ; que, par suite, les procès-verbaux délivrés antérieurement à l'octroi de ce sursis étaient devenus caducs avant même que Mme A ne présente au tribunal administratif, le 25 octobre 2006, une demande tendant à la décharge de l'obligation de payer les sommes dues, procédant de ces actes de poursuite ; que, dès lors, en faisant partiellement droit aux conclusions de cette demande alors qu'étant dépourvues d'objet du fait de la caducité des poursuites, elles étaient irrecevables, le tribunal administratif a commis une erreur de droit ; que, par suite, le ministre du budget, des comptes publics et de la réforme de l'Etat est fondé à demander l'annulation des articles 3 et 4 du jugement attaqué; que la demande présentée à ce titre par Mme A n'étant pas recevable, il n'y a pas lieu à renvoi ;<br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Les articles 3 et 4 du jugement n° 0615671 du tribunal administratif de Paris du 1er juin 2010, en tant qu'ils déchargent Mme A de l'obligation de payer les sommes dues au titre des cotisations de taxe d'habitation des années 1994, 1995 et 1997 à 2000 résultant des procès-verbaux d'opposition sur saisie du 27 juin 2006 délivrés par les trésoriers des 2e et 4e divisions du 16e arrondissement de Paris sont annulés. <br/>
<br/>
Article 2 : Les conclusions présentées par Mme A tendant à la décharge de l'obligation de payer les sommes dues au titre des cotisations de taxe d'habitation des années 1994, 1995 et 1997 à 2000 sont rejetées.<br/>
<br/>
Article 3 : Les conclusions présentées par Mme A devant le Conseil d'Etat au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée au ministre de l'économie et des finances et à Mme Catherine A.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
