<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027138983</ID>
<ANCIEN_ID>JG_L_2013_03_000000334085</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/13/89/CETATEXT000027138983.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 01/03/2013, 334085, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-03-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>334085</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Samuel Gillis</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:334085.20130301</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 24 novembre 2009 au secrétariat du contentieux du Conseil d'Etat, présenté par le procureur général près la Cour des comptes, qui demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 56111 du 21 septembre 2009 par lequel la Cour des comptes, statuant toutes chambres réunies, a décidé qu'il n'y avait pas lieu de déclarer gestionnaires de fait MmeA..., MM.C..., H..., I..., D..., L..., F..., E..., K..., J..., B...et la Société de mathématiques appliquées et de sciences humaines (SMASH), à raison d'opérations exécutées en application de subventions de recherche versées par le ministère chargé de l'environnement à la SMASH ;<br/>
<br/>
              2°) de renvoyer l'affaire devant la Cour des comptes ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 12 février 2013, présentée pour Mme A...;<br/>
<br/>
              Vu le code des juridictions financières ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Samuel Gillis, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Waquet, Farge, Hazan, avocat de MmeA...,<br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Waquet, Farge, Hazan, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que par trois arrêts du 22 octobre 1997, la Cour des comptes a déclaré provisoirement, conjointement et solidairement, comptables de fait des deniers de l'Etat, à raison d'opérations exécutées en application de subventions de recherche versées par le ministère de l'environnement à la société de mathématique appliquées et de sciences humaines (SMASH) cette société, Mme A...et dix autres personnes ; que par trois arrêts du 18 février 1999, elle a déclaré à titre définitif, solidairement et conjointement, comptables de fait des deniers de l'Etat la SMASH, Mme A...et six des dix autres personnes ; que par une décision du 13 février 2002, le Conseil d'Etat statuant au contentieux a annulé les trois arrêts du 18 février 1999 en tant qu'ils déclarent définitivement MmeA..., conjointement et solidairement avec d'autres personnes, comptable de fait des deniers de l'Etat, en raison de la participation au délibéré de la formation de jugement du rapporteur qui avait été chargé de la vérification de la gestion de l'organisme dont les deniers étaient en cause ; que, par cette décision, le Conseil d'Etat a renvoyé les affaires à la Cour des comptes ; que, par l'arrêt attaqué du 21 septembre 2009, celle-ci a rejeté les conclusions de Mme A...à fin d'annulation des arrêts provisoires du 22 octobre 1997, écarté sa demande tendant à ce qu'elle constate la prescription de l'action publique et prononcé un non-lieu à déclaration de gestion de fait à l'égard de Mme A...et des dix autres personnes qui avaient été déclarées comptables de fait des deniers de l'Etat par les arrêts provisoires du 22 octobre 2007 ; <br/>
<br/>
              2. Considérant que le principe d'impartialité applicable à toutes les juridictions administratives fait obstacle à ce que le rapporteur de la Cour des comptes participe au jugement de comptes dont il a eu à connaître à l'occasion d'une vérification de gestion ; qu'il s'ensuit que la participation au délibéré de la formation de jugement chargée de se prononcer sur une déclaration de gestion de fait du rapporteur auquel avait été confiée la vérification de la gestion de l'organisme dont les deniers sont en cause entache d'irrégularité la composition de cette formation de jugement ;<br/>
<br/>
              3. Considérant que le Conseil d'Etat a jugé dans la décision du 13 février 2002, après avoir relevé qu'un auditeur à la Cour des comptes avait procédé à la vérification des comptes d'emploi des concours financiers publics attribués à la SMASH durant les exercices 1990 à 1996 et qu'à la suite de son rapport une procédure juridictionnelle avait été engagée à l'encontre, notamment, de MmeA..., que la composition de la formation de jugement ayant déclaré Mme A...conjointement et solidairement avec d'autres personnes comptable de fait des deniers de l'Etat avait été irrégulière, ce même auditeur à la Cour des comptes ayant en effet, en tant que magistrat, occupé les fonctions de rapporteur devant la quatrième section de la deuxième chambre chargée de se prononcer sur les opérations présumées constitutives de gestion de fait des deniers de l'Etat ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui a ainsi été jugé que la composition de la formation de jugement ayant rendu les arrêts provisoires du 22 octobre 1997 était de la même façon irrégulière ; que cette circonstance entache d'irrégularité l'ensemble de la procédure suivie ; qu'il suit de là que l'arrêt attaqué, par lequel la Cour des comptes a statué à titre définitif sur la déclaration de gestion de fait en se fondant sur ces trois arrêts provisoires, est également entaché d'irrégularité ; qu'il doit dès lors, et sans qu'il soit besoin d'examiner les moyens du pourvoi, être annulé ; que les dispositions alors en vigueur du code des juridictions financières, précisant que seuls les arrêts définitifs rendus par la Cour des comptes étaient susceptibles de cassation, ne font pas obstacle à ce que le Conseil d'Etat, saisi en cassation d'un arrêt définitif, annule le cas échéant d'office les arrêts provisoires au vu desquels ils seraient intervenus et dont l'irrégularité entacherait tout ou partie de la procédure ; que, par suite, il y a lieu d'annuler également les trois arrêts provisoires du 22 octobre 1997 ;<br/>
<br/>
              5. Considérant qu'aucune question ne reste à juger ; qu'il n'y a, dès lors, pas lieu de statuer au fond en application du second alinéa de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la Cour des comptes du 21 septembre 2009 et ses arrêts provisoires du 22 octobre 1997 sont annulés.<br/>
<br/>
Article 2 : La présente décision sera notifiée au procureur général près la Cour des comptes et à Mme G...A....<br/>
      Copie en sera adressée au ministre de l'économie et des finances.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
