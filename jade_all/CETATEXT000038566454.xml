<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038566454</ID>
<ANCIEN_ID>JG_L_2019_06_000000426516</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/56/64/CETATEXT000038566454.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 07/06/2019, 426516, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-06-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426516</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Nicolas Agnoux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:426516.20190607</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire complémentaire et trois nouveaux mémoires enregistrés le 21 décembre 2018 et les 3 janvier, 7 et 14 février et 13 mai 2019 au secrétariat du contentieux du Conseil d'Etat, la société Fioul 83 et la société Boudret SAS demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet née du silence gardé par le Premier ministre sur la demande qu'il a reçue le 18 octobre 2018 tendant à l'abrogation du 2° de l'article R. 221-3 du code de l'énergie, dans sa rédaction issue de l'article 3 du décret n° 2018-401 du 29 mai 2018 relatif aux certificats d'économies d'énergie et aux obligations d'économies d'énergie auxquelles sont soumises les personnes mettant à la consommation du fioul domestique, en tant qu'il diminue de 7 000 à 1 000 m³ le seuil d'assujettissement aux obligations d'économies d'énergie applicable à compter de 2019 aux entreprises qui mettent à la consommation des carburants autres que le gaz de pétrole liquéfié ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros à verser à chacune au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'énergie ;<br/>
              - la loi n° 2005-781 du 13 juillet 2005 ;<br/>
              - la loi n° 2010-788 du 12 juillet 2010 ;<br/>
              - la loi n° 2015-992 du 17 août 2015 ;<br/>
              - le décret n°2018-401 du 29 mai 2018 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Agnoux, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les articles L. 221-1 à L. 222-9 du code de l'énergie instituent un dispositif soumettant les fournisseurs d'énergie dont les ventes excèdent un certain seuil à des obligations d'économies d'énergie, dont ils s'acquittent par la détention, à la fin de chaque période de référence, de certificats d'économies d'énergie. Ces obligations d'économies d'énergie sont fixées par décret en fonction du type d'énergie considéré, des catégories de clients du fournisseur et du volume de son activité. Elles comportent, en application de l'article L. 221-1-1 du code de l'énergie issu de la loi du 17 août 2015 relative à la transition énergétique pour la croissance verte, des obligations d'économies d'énergie spécifiques à réaliser au bénéfice des ménages en situation de précarité énergétique. Les fournisseurs d'énergie peuvent réunir les certificats soit en réalisant eux-mêmes des économies d'énergie, soit en obtenant de leurs clients qu'ils en réalisent, soit en les acquérant auprès d'un autre fournisseur d'énergie ou d'une personne morale qui, en application de l'article L. 221-7 du code de l'énergie, est susceptible d'obtenir des certificats en contrepartie de mesures d'économies d'énergie réalisées volontairement. A défaut de justifier du respect de leurs obligations à l'issue de la période considérée, les fournisseurs d'énergie sont tenus d'opérer au profit du Trésor public un versement qui, aux termes de l'article L. 221-4 de ce code, " est calculé sur la base d'une pénalité maximale de 0,02 euro par kilowattheure ". <br/>
<br/>
              2. Par une lettre du 16 octobre 2018, les sociétés Fioul 83 et Boudret SAS ont demandé au Premier ministre d'abroger le b du 2° de l'article R. 221-3 du code de l'énergie dans sa rédaction issue de l'article 3 du décret du 29 mai 2018 relatif aux certificats d'économies d'énergie et aux obligations d'économies d'énergie auxquelles sont soumises les personnes mettant à la consommation du fioul domestique, qui abaisse, à compter de l'année 2019, de 7 000 à 1 000 mètres cubes le volume annuel de carburants autres que le gaz de pétrole liquéfié (GPL) au-delà duquel les personnes qui mettent à la consommation des carburants automobiles sont soumises à des obligations d'économies d'énergie. Leur demande a fait l'objet d'une décision implicite de rejet, dont elles demandent l'annulation pour excès de pouvoir. <br/>
<br/>
              3. Aux termes de l'article L. 221-1 du code de l'énergie, dans sa rédaction applicable à compter du 1er janvier 2019 : " Sont soumises à des obligations d'économies d'énergie : / 1° Les personnes morales qui mettent à la consommation des carburants automobiles ou du fioul domestique et dont les ventes annuelles sont supérieures à un seuil défini par décret en Conseil d'Etat./ 2° Les personnes qui vendent de l'électricité, du gaz, de la chaleur ou du froid aux consommateurs finals et dont les ventes annuelles sont supérieures à un seuil défini par décret en Conseil d'Etat. / Les personnes mentionnées aux 1° et 2° peuvent se libérer de ces obligations soit en réalisant, directement ou indirectement, des économies d'énergie, soit en acquérant des certificats d'économies d'énergie. " Aux termes de l'article R. 221-3 du code de l'énergie, tel que modifié par le décret du 29 mai 2018 : " Pour chaque année civile des périodes mentionnées à l'article R. 221-1, sont soumises à des obligations d'économies d'énergie les personnes pour lesquelles au moins l'une des quantités définies à l'article R. 221-2 est supérieure, la même année, aux seuils suivants :  (...) / 2° Pour la quantité de carburants autres que le gaz de pétrole liquéfié : a) 7 000 mètres cubes pour les années civiles 2015 à 2018 ; b) 1 000 mètres cubes pour les années suivantes ; (...) ". <br/>
<br/>
              4. Il résulte de l'économie générale de la loi du 13 juillet 2005 de programme fixant les orientations de la politique énergétique qui a institué le dispositif des certificats d'économie d'énergie, éclairée par les travaux parlementaires, que les seuils de vente à partir desquels les fournisseurs sont soumis aux obligations d'économies d'énergie doivent être fixés type d'énergie par type d'énergie de façon que les principaux opérateurs de chacun des secteurs concernés contribuent à la réalisation de l'objectif national d'économies d'énergie. Il ressort en outre des travaux préparatoires de l'article 78 de la loi du 12 juillet 2010 portant engagement national pour l'environnement, qui a étendu le dispositif des certificats d'économies d'énergie aux personnes mettant à la consommation des carburants autres que le GPL, que l'intention du législateur était d'exempter des obligations d'économies d'énergie les opérateurs du secteur ne disposant pas d'une masse critique suffisante.<br/>
<br/>
              5. Il ressort des pièces du dossier que, pour l'année 2018, parmi les quatre-vingt-six opérateurs mettant à la consommation des carburants autres que le GPL, trente-huit entreprises ont mis à la consommation un volume supérieur à 7 000 mètres cubes, vingt entreprises un volume compris entre 1 000 et 7 000 mètres cubes et vingt-huit entreprises un volume inférieur à 1 000 mètres cubes. Ces trois catégories représentent respectivement 99,86 %, 0,13 % et 0,01 % du volume total de carburant mis à la consommation, correspondant à 54,7 millions de mètres cubes. En abaissant à 1 000 mètres cubes le seuil d'assujettissement aux obligations d'économies d'énergie applicable à compter de l'année 2019 pour les entreprises qui mettent à la consommation des carburants autres que le GPL, le b du 2° de l'article R. 221-3 du code de l'énergie a pour effet, compte tenu de la structure du secteur, d'assujettir, non pas seulement les principaux opérateurs du secteur concerné, mais aussi des opérateurs représentant une part minime du volume total mis à la consommation, pour n'exonérer que des entreprises représentant ensemble 0,01 % du marché. Dès lors, le refus du Premier ministre d'abroger cette disposition est entaché d'une erreur manifeste d'appréciation. <br/>
<br/>
              6. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de la requête, les requérantes sont fondées à demander l'annulation de la décision implicite de refus d'abroger le b du 2° de l'article R. 221-3 du code de l'énergie ainsi que, au a du même 2°, les mots " pour les années civiles 2015 à 2018 ".<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement d'une somme de 2 000 euros à la société Fioul 83 et à la société Boudret SAS au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision implicite par laquelle le Premier ministre a refusé d'abroger le b du 2° de l'article R. 221-3 du code de l'énergie et, au a du même 2°, les mots " pour les années civiles 2015 à 2018 " est annulée.<br/>
Article 2 : L'Etat versera à la société Fioul 83 et à la société Boudret SAS la somme de 2 000 euros chacune au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Fioul 83, à la société Boudret SAS, au Premier ministre et au ministre d'Etat, ministre de la transition écologique et solidaire.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
