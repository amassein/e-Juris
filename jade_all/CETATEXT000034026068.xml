<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034026068</ID>
<ANCIEN_ID>JG_L_2017_02_000000391722</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/02/60/CETATEXT000034026068.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 10/02/2017, 391722</TITRE>
<DATE_DEC>2017-02-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391722</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL ; SCP BARADUC, DUHAMEL, RAMEIX ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>Mme Charline Nicolas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:391722.20170210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'Agence de maîtrise d'ouvrage des travaux du ministère de la justice, devenue l'Agence publique pour l'immobilier de la Justice, a demandé au tribunal administratif de Nice de condamner solidairement les membres du groupement de maîtrise d'oeuvre, les sociétés Atelier Christian de Portzamparc, Ingerop, Iosis Méditerranée et Atec, à lui verser une somme de 1 554 441,66 euros en réparation des surcoûts de l'opération de construction du palais de justice de Grasse liés notamment aux mouvements de sol.<br/>
<br/>
              Par un jugement n° 0801639 du 22 juin 2012, le tribunal administratif de Nice a condamné solidairement les sociétés Ingerop, Iosis Méditerranée, Atec et Atelier Christian de Portzamparc à verser à l'Agence publique pour l'immobilier de la Justice la somme de 1 457 686,78 euros TTC, puis, statuant sur les appels en garantie, a notamment laissé cette condamnation à la charge définitive des sociétés Campenon Bernard Côte d'Azur, venant aux droit des sociétés Sogea et Sogea Sud-Est, et Cari, à hauteur de 30 %, de Iosis Méditerranée à hauteur de 15 %, de Terrasol à hauteur de 7 %, de Fondasol à hauteur de 3 %, de l'Atelier Christian de Portzamparc à hauteur de 15 %, du Ceten Apave à hauteur de 5 % et de la société Ingerop à hauteur de 25 %, et rejeté le surplus des conclusions de l'Agence publique pour l'immobilier de la Justice. <br/>
<br/>
              Par un arrêt n°s 12MA03086, 12MA03611 du 12 mai 2015, la cour administrative d'appel de Marseille a réformé ce jugement en portant de 25 à 30 % la part de la condamnation solidaire laissée à la charge de la société Ingerop, ramené de 15% à 1 % et de 15% à 4,5 % les parts de la condamnation solidaire laissées respectivement à la charge des sociétés Atelier Christian de Portzamparc et Iosis Méditerranée, devenue Egis Bâtiment, et porté de 7% à 15 %, de 3% à 4,5 % et de 5% à 15 % les parts de la condamnation solidaire que les sociétés Terrasol et Fondasol et le GIE Ceten Apave ont été respectivement condamnés à garantir, puis a rejeté le surplus des conclusions des parties.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 13 juillet et 13 octobre 2015 au secrétariat du contentieux du Conseil d'Etat, la société Campenon Bernard Côte d'Azur, venant aux droits de la société Sogea, et la société Fayat Bâtiment, venant aux droits de la société Cari, demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge, solidairement, des sociétés Ingerop, Iosis Méditerranée, Atec et Atelier Christian de Portzamparc la somme de 8 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu :<br/>
              - le code civil ; <br/>
              - le code des marchés publics ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Charline Nicolas, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de la société Campenon Bernard Côte d'Azur et de la société Fayat Bâtiment, à la SCP Célice, Soltner, Texidor, Perier, avocat de la société Ingerop, à la SCP Baraduc, Duhamel, Rameix, avocat de la société Egis bâtiments Méditerranée et de la société Terrasol et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de l'agence publique pour l'immobilier de la justice.<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, le 11 mars 1994, le ministère de la justice a conclu, pour la construction d'une cité judiciaire à Grasse, un contrat de maîtrise d'oeuvre avec un groupement solidaire composé notamment de l'entreprise Atelier Christian de Portzamparc, architecte et mandataire du groupement, et de la société SEEE, aux droits de laquelle vient la société Ingerop Conseil et Ingénierie ; que, le 28 août 1995, le marché de travaux a été confié à un groupement solidaire composé notamment des sociétés Sogea Sud-est et Sogea SA, aux droits desquels vient la société Campenon Bernard Côte d'Azur ; que divers désordres ont été constatés, dus notamment à des mouvements de sol ; qu'à la suite d'une demande en référé expertise introduite par le maître d'ouvrage le 14 avril 1998, un rapport a été déposé le 5 février 2002 par l'expert désigné par le tribunal administratif ; que, le 19 mars 2008, l'Agence de maîtrise d'ouvrage des travaux du ministère de la justice, devenue l'Agence publique pour l'immobilier de la Justice, agissant pour le compte de l'Etat, a saisi le tribunal administratif de Nice d'une demande tendant à la condamnation solidaire des sociétés membres du groupement de maîtrise d'oeuvre à lui verser la somme de 1 554 441,66 euros en réparation du préjudice qu'il a subi en raison principalement du coût des travaux de confortement rendus nécessaires par les mouvements de sol ; que, par un jugement du 22 juin 2012, le tribunal administratif de Nice a condamné solidairement les sociétés Ingerop, Iosis Méditerranée, Atec et Atelier Christian de Portzamparc à verser à l'Agence publique pour l'immobilier de la Justice la somme de 1 457 686,78 euros toutes taxes comprises, avec intérêts au taux légal à compter du 3 mars 2008 et a, statuant sur les appels en garantie, notamment laissé cette condamnation à la charge définitive des sociétés Campenon Bernard Côte d'Azur et Cari à hauteur de 30 %, de Iosis Méditerranée à hauteur de 15 %, de Terrasol à hauteur de 7 %, de Fondasol à hauteur de 3 %, de l'Atelier Christian de Portzamparc à hauteur de 15 %, du Ceten Apave à hauteur de 5 % et de la société Ingerop à hauteur de 25 % ; que le pourvoi des sociétés Campenon Bernard Côte d'Azur et Fayat Bâtiment, venant aux droits de la société Cari, doit être regardé comme dirigé contre l'arrêt du 12 mai 2015 de la cour administrative d'appel de Marseille en tant qu'il a rejeté leur appel dirigé contre ce jugement ; <br/>
<br/>
              Sur le pourvoi principal des sociétés Campenon Bernard Côte d'Azur et Fayat Bâtiment :<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article 2270-1 du code civil alors applicable : " Les actions en responsabilité civile extracontractuelle se prescrivent par dix ans à compter de la manifestation du dommage ou de son aggravation " ; que cette disposition s'applique aux actions en garantie exercées par un constructeur contre un autre ; que le délai de prescription ne pouvant courir avant que la responsabilité de l'intéressé ait été recherchée par le maître d'ouvrage, la manifestation du dommage au sens de ces dispositions correspond à la date à laquelle le constructeur a reçu communication de la demande présentée par le maître d'ouvrage devant le tribunal administratif ; qu'une demande en référé expertise introduite par le maître d'ouvrage sur le fondement de l'article R. 532-1 du code de justice administrative ne peut être regardée comme constituant, à elle seule, une recherche de responsabilité des constructeurs par le maître d'ouvrage ; que, par suite, en jugeant que l'introduction d'une telle demande, si elle est susceptible d'interrompre le délai de prescription, n'est pas de nature à faire courir le délai de dix ans prévu par les dispositions précitées dès lors qu'elle ne présente pas le caractère d'une demande indemnitaire, la cour administrative d'appel de Marseille n'a pas entaché son arrêt d'erreur de droit ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'il ressort des énonciations de l'arrêt attaqué, d'une part, que le groupement attributaire de travaux avait proposé une variante concernant les techniques de soutènement afin d'améliorer la sécurité face au risque de glissement de terrain et qu'il n'a pas effectué, dans le cadre de cette solution variante, de reconnaissances de sol complémentaires ; d'autre part, qu'alors même qu'il disposait d'éléments d'information de son conseil sur l'éventuelle présence d'eau dans les sols, il a ignoré les indications soulignant les risques en résultant ; que compte-tenu de ces appréciations exemptes de dénaturation, la cour administrative d'appel de Marseille n'a pas commis d'erreur de droit ni d'erreur de qualification juridique des faits en jugeant qu'en s'abstenant de procéder, de leur propre chef, aux reconnaissances complémentaires nécessaires, les sociétés attributaires du marché de travaux ont commis une faute de nature à engager leur responsabilité partielle dans la survenance des préjudices subis par l'Agence publique pour l'immobilier de la Justice en raison des mouvements de terrain ; <br/>
<br/>
              4. Considérant, en dernier lieu, qu'il ressort des pièces du dossier soumis aux juges du fond, et en particulier de la page 67 du rapport d'expertise du 5 février 2002, que, contrairement à ce que soutiennent les sociétés requérantes, la cour a retenu un calcul parvenant au même résultat que l'expert ; qu'elle a suffisamment motivé son arrêt sur ce point et n'a pas dénaturé les pièces du dossier ; qu'elle n'a pas omis de prendre en compte l'indemnité versée au maître d'ouvrage par son assureur et n'a donc pas entaché son arrêt d'une erreur de droit sur ce point ; <br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que le pourvoi des sociétés Campenon Bernard Côte d'Azur et Fayat Bâtiment doit être rejeté, y compris leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Sur le pourvoi incident de la société Ingerop Conseil et Ingénierie : <br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond, en particulier du rapport d'expertise du 5 février 2002, que la société Ingerop Conseil et Ingénierie était chargée, au sein du groupement de maîtrise d'oeuvre, de la coordination technique des études, des études portant sur le génie civil, de l'élaboration du document de consultation des entreprises et d'une mission d'assistance à la passation des marchés de travaux ; que la cour a souverainement relevé, sans dénaturé les pièces du dossier, qu'elle était chargée d'établir le programme de reconnaissance de sol complémentaire et qu'elle n'a pas estimé nécessaire, au moment de la consultation des entreprises ou des études d'exécution confiées au groupement d'entreprises, de demander des sondages complémentaires dans la zone d'influence du terrassement projeté, alors même que les études préalables effectuées par la société Fondasol avaient mis en évidence des niveaux d'eau significatifs dans le sous-sol ; que, dans ces circonstances, la cour administrative d'appel de Marseille n'a pas entaché son arrêt d'erreur de qualification juridique en jugeant qu'elle avait commis une faute en s'abstenant de procéder à des sondages complémentaires ; que par suite, le pourvoi incident de cette société doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;  <br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge solidaire des sociétés Campenon Bernard Côte d'Azur et Fayat Bâtiment la somme de 1 500 euros à chacune des sociétés Egis Bâtiments Méditerranée et Terrasol, au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Campenon Bernard Côte d'Azur et de la société Fayat Bâtiment est rejeté.<br/>
Article 2 : Le pourvoi incident de la société Ingerop Conseil et Ingénierie est rejeté. <br/>
Article 3 : Les sociétés Campenon Bernard Côte-d'Azur et Fayat Bâtiment verseront solidairement une somme de 1500 euros à la société  Egis bâtiments Méditerranée et à la Terrasol au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société Campenon Bernard Côte d'Azur, à la société Fayat Bâtiment, à la société Ingerop Conseil et Ingénierie, à la société Egis Bâtiments Méditerranée et à la société Terrasol.<br/>
Copie en sera adressée à la société Atelier Christian de Portzamparc, à la société Atec, au GIE Ceten Apave, à la société Fondasol et à l'Agence publique pour l'immobilier de la Justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-05-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RECOURS OUVERTS AUX DÉBITEURS DE L'INDEMNITÉ, AUX ASSUREURS DE LA VICTIME ET AUX CAISSES DE SÉCURITÉ SOCIALE. ACTION EN GARANTIE. - ACTION EN GARANTIE D'UN CONSTRUCTEUR A MIS EN CAUSE PAR LE MAÎTRE DE L'OUVRAGE CONTRE UN AUTRE CONSTRUCTEUR B - DÉLAI DE PRESCRIPTION - DIX ANS - A) POINT DE DÉPART DU DÉLAI - MISE EN CAUSE DE LA RESPONSABILITÉ DU CONSTRUCTEUR A - B) DEMANDE DE RÉFÉRÉ-EXPERTISE PAR LE MAÎTRE DE L'OUVRAGE - CIRCONSTANCE DÉCLENCHANT LE DÉLAI DE PRESCRIPTION - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">67-05-01-01 TRAVAUX PUBLICS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INTRODUCTION DE L'INSTANCE. DÉLAIS. - ACTION EN GARANTIE D'UN CONSTRUCTEUR A MIS EN CAUSE PAR LE MAÎTRE DE L'OUVRAGE CONTRE UN AUTRE CONSTRUCTEUR B - DÉLAI DE PRESCRIPTION - DIX ANS - A) POINT DE DÉPART DU DÉLAI - MISE EN CAUSE DE LA RESPONSABILITÉ DU CONSTRUCTEUR A - B) DEMANDE DE RÉFÉRÉ-EXPERTISE PAR LE MAÎTRE DE L'OUVRAGE - CIRCONSTANCE DÉCLENCHANT LE DÉLAI DE PRESCRIPTION - ABSENCE.
</SCT>
<ANA ID="9A"> 60-05-01 Le délai de prescription de dix ans à compter de la manifestation du dommage prévu par l'article 2270-1 du code civil pour les actions en responsabilité civile extracontractuelle s'applique aux actions en garantie exercées par un constructeur contre un autre. Le délai de prescription ne pouvant courir avant que la responsabilité de l'intéressé ait été recherchée par le maître d'ouvrage, la manifestation du dommage au sens de ces dispositions correspond à la date à laquelle le constructeur a reçu communication de la demande présentée par le maître d'ouvrage devant le tribunal administratif.... ,,Une demande en référé-expertise introduite par le maître d'ouvrage sur le fondement de l'article R. 532-1 du code de justice administrative ne peut être regardée comme constituant, à elle seule, une recherche de responsabilité des constructeurs par le maître d'ouvrage.</ANA>
<ANA ID="9B"> 67-05-01-01 Le délai de prescription de dix ans à compter de la manifestation du dommage prévu par l'article 2270-1 du code civil pour les actions en responsabilité civile extracontractuelle s'applique aux actions en garantie exercées par un constructeur contre un autre. Le délai de prescription ne pouvant courir avant que la responsabilité de l'intéressé ait été recherchée par le maître d'ouvrage, la manifestation du dommage au sens de ces dispositions correspond à la date à laquelle le constructeur a reçu communication de la demande présentée par le maître d'ouvrage devant le tribunal administratif.... ,,Une demande en référé-expertise introduite par le maître d'ouvrage sur le fondement de l'article R. 532-1 du code de justice administrative ne peut être regardée comme constituant, à elle seule, une recherche de responsabilité des constructeurs par le maître d'ouvrage.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
