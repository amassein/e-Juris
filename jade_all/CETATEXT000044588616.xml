<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044588616</ID>
<ANCIEN_ID>JG_L_2021_12_000000457564</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/58/86/CETATEXT000044588616.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 27/12/2021, 457564, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>457564</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Charles-Emmanuel Airy</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:457564.20211227</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 15 octobre 2021 au secrétariat du contentieux du Conseil d'État, M. C... D... demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la circulaire du garde des sceaux, ministre de la justice, du 4 octobre 2021 relative à la lutte contre la fraude fiscale ;<br/>
<br/>
              2°) de mettre à la charge de l'État la somme de 3 000 euros au titre de l'article L 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la décision n° 445035 du 19 juillet 2021 du Conseil d'État, statuant au contentieux ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles-Emmanuel Airy, auditeur,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteure publique ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. L'article 1729 du code général des impôts dispose, dans sa version en vigueur depuis le 1er janvier 2009, que les inexactitudes ou omissions relevées dans une déclaration ou un acte comportant l'indication d'éléments à retenir pour l'assiette ou la liquidation de l'impôt ainsi que la restitution d'une créance de nature fiscale dont le versement a été indûment obtenu de l'État entraînent l'application d'une majoration de 40 % en cas de manquement délibéré, de 80 % en cas d'abus de droit au sens de l'article L. 64 du livre des procédures fiscales, sauf s'il n'est pas établi que le contribuable a eu l'initiative principale du ou des actes constitutifs de l'abus de droit ou en a été le principal bénéficiaire, et de 80 % en cas de manœuvres frauduleuses ou de dissimulation d'une partie du prix stipulé dans un contrat ou lorsqu'il est constaté une transmission dans une intention libérale de biens ou droits faisant l'objet d'un contrat de fiducie ou des fruits tirés de l'exploitation de ces biens ou droits. <br/>
<br/>
              2. L'article 1741 du même code punit, dans sa rédaction issue de la loi du 23 octobre 2018 relative à la lutte contre la fraude, d'un emprisonnement de cinq ans et d'une amende d'un montant de 500 000 euros le fait, pour quiconque, de se soustraire frauduleusement ou de tenter de se soustraire frauduleusement à l'établissement ou au paiement total ou partiels des impôts légalement dus, notamment quand il a volontairement dissimulé une part des sommes sujettes à l'impôt. Ces peines sont, dans certaines circonstances aggravantes que l'article énumère, portées à sept ans d'emprisonnement et à une amende de 3 millions d'euros, dont le montant peut être porté au double du produit tiré de l'infraction. Sont en outre prévues par le même article des peines complémentaires d'interdiction des droits civiques, civils et de famille, et d'affichage de la décision de condamnation.<br/>
<br/>
              3. M. D... demande l'annulation pour excès de pouvoir de la circulaire du garde des sceaux, ministre de la justice, du 4 octobre 2021 relative à la lutte contre la fraude fiscale, qui fixe les lignes directrices de la politique pénale en matière de fraude fiscale dans le cadre juridique issu de la loi du 23 octobre 2018. Eu égard au moyen qu'il invoque, tiré de ce que cette circulaire donne de la combinaison des dispositions des articles 1729 et 1741 du code général des impôts une interprétation contraire à la Constitution, il doit être regardé comme demandant l'annulation des seules énonciations du 1 du B du III de cette circulaire, intitulé " L'affirmation des possibilités de cumul des sanctions administratives et pénales ", qui rappellent les conditions dans lesquelles peuvent être appliquées de manière cumulée, sur le fondement respectif de chacun de ces deux articles, des sanctions fiscales et pénales en cas de fraude fiscale.  <br/>
<br/>
              4. Toutefois, par une décision n° 445035 du 19 juillet 2021, le Conseil d'État, statuant au contentieux a refusé d'admettre le pourvoi formé par le ministre de l'économie, des finances et de la relance contre l'arrêt de la cour administrative d'appel de Lyon du 6 août 2020 prononçant, pour un motif de régularité de la procédure d'imposition, la décharge des cotisations supplémentaires d'impôt sur le revenu auxquelles M. D... a été assujetti au titre des années 2012 et 2013 et des majorations dont ces cotisations avaient été assorties sur le fondement de l'article 1729 du code général des impôts. Il suit de là qu'à la date d'introduction de sa requête, M. D... n'était plus susceptible de faire l'objet, à ce titre, d'un cumul de sanctions administratives et pénales. Celui-ci, qui ne fait état d'aucune procédure fiscale ou répressive autre que celles diligentées à la suite du contrôle fiscal dont il a fait l'objet au titre des années 2012 et 2013, ne justifie donc pas d'un intérêt lui donnant qualité pour demander l'annulation pour excès de pouvoir des énonciations qu'il conteste. <br/>
<br/>
              5. Il résulte de ce qui précède que la requête de M. D... est irrecevable et doit, par suite, être rejetée, sans qu'il soit besoin pour le Conseil d'État de se prononcer sur la transmission au Conseil constitutionnel de la question prioritaire de constitutionnalité soulevée à l'appui de celle-ci. <br/>
<br/>
              6. L'article L. 761-1 du code de justice administrative fait obstacle à ce qu'une somme soit mise à ce titre à la charge de l'État, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. D... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. C... D..., au ministre de l'économie, des finances et de la relance et au garde des sceaux, ministre de la justice.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
              Délibéré à l'issue de la séance du 16 décembre 2021 où siégeaient : M. Pierre Collin, président de chambre, présidant ; M. Mathieu Herondart, conseiller d'Etat et M. F... A..., auditeur-rapporteur. <br/>
<br/>
              Rendu le 27 décembre 2021.<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Pierre Collin<br/>
 		Le rapporteur : <br/>
      Signé : M. Charles-Emmanuel Airy<br/>
                 La secrétaire :<br/>
                 Signé : Mme E... B...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
