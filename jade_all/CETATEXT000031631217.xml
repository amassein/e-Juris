<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031631217</ID>
<ANCIEN_ID>JG_L_2015_12_000000394993</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/63/12/CETATEXT000031631217.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section du Contentieux, 11/12/2015, 394993, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394993</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section du Contentieux</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>Mme Cécile Barrois de Sarigny</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2015:394993.20151211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au juge des référés du tribunal administratif de Rennes, sur le fondement de l'article L. 521-2 du code de justice administrative, de suspendre l'exécution de l'arrêté du 24 novembre 2015 par lequel le ministre de l'intérieur l'a astreinte à résider sur le territoire de la commune de Rennes jusqu'au 12 décembre 2015, avec obligation de présentation trois fois par jour à des horaires déterminés au commissariat de police de Rennes tous les jours de la semaine et de demeurer, tous les jours entre 20 heures et 6 heures, dans les locaux où elle réside. <br/>
<br/>
              Par une ordonnance n° 1505394 du 30 novembre 2015, le juge des référés a rejeté cette demande. <br/>
<br/>
              Par un pourvoi et deux mémoires complémentaires enregistrés les 2, 7 et 9 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
            Vu les autres pièces du dossier ;<br/>
<br/>
            Vu : <br/>
            - le code civil ;<br/>
            - la loi n° 55-385 du 3 avril 1955 ; <br/>
            - la loi n° 2000-321 du 12 avril 2000 ; <br/>
            - la loi n° 2015-1501 du 20 novembre 2015 ; <br/>
            - le décret n° 2015-1475 du 14 novembre 2015 ;<br/>
            - le décret n° 2015-1476 du 14 novembre 2015 ;<br/>
            - le décret n° 2015-1478 du 14 novembre 2015 ;<br/>
            - le décret n° 2005-850 du 27 juillet 2005 ;<br/>
            - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Barrois de Sarigny, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de Mme A... B...et à la SCP Spinosi, Sureau, avocat de la Ligue des droits de l'homme ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ; qu'aux termes de l'article L. 522-3 du même code : " Lorsque la demande ne présente pas un caractère d'urgence ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée, le juge des référés peut la rejeter par une ordonnance motivée sans qu'il y ait lieu d'appliquer les deux premiers alinéas de l'article L. 522-1 " ; <br/>
<br/>
              2. Considérant que Mme A... B...se pourvoit en cassation contre l'ordonnance du 30 novembre 2015 par laquelle le juge des référés du tribunal administratif de Rennes a rejeté, par application de l'article L. 522-3 du code de justice administrative, sa demande, présentée sur le fondement de l'article L. 521-2 du même code, tendant à la suspension des effets de la mesure d'assignation à résidence qui a été prise à son endroit par le ministre de l'intérieur le 24 novembre 2015 ;<br/>
<br/>
              Sur l'intervention : <br/>
<br/>
              3. Considérant que la Ligue des droits de l'homme, qui intervient au soutien des conclusions du pourvoi, justifie, eu égard à la nature et à l'objet du litige, d'un intérêt suffisant pour intervenir dans la présente instance ; que son intervention est, par suite, recevable ; <br/>
<br/>
              Sur les dispositions applicables :<br/>
<br/>
              4. Considérant qu'aux termes de l'article 1er de loi du 3 avril 1955 relative à l'état d'urgence : " L'état d'urgence peut être déclaré sur tout ou partie du territoire métropolitain, des départements d'outre-mer, des collectivités d'outre-mer régies par l'article 74 de la Constitution et en Nouvelle-Calédonie, soit en cas de péril imminent résultant d'atteintes graves à l'ordre public, soit en cas d'événements présentant, par leur nature et leur gravité, le caractère de calamité publique " ; qu'aux termes de l'article 2 de la même loi : " L'état d'urgence est déclaré par décret en Conseil des ministres. Ce décret détermine la ou les circonscriptions territoriales à l'intérieur desquelles il entre en vigueur. Dans la limite de ces circonscriptions, les zones où l'état d'urgence recevra application seront fixées par décret. La prorogation de l'état d'urgence au-delà de douze jours ne peut être autorisée que par la loi " ; <br/>
<br/>
              5. Considérant qu'après les attentats commis à Paris le 13 novembre 2015, l'état d'urgence a été déclaré sur le territoire métropolitain, y compris en Corse, par le décret délibéré en conseil des ministres n° 2015-1475 du 14 novembre 2015 ; que le décret n° 2015-1476 du même jour a décidé que les mesures d'assignation à résidence prévues à l'article 6 de la loi du 3 avril 1955 pouvaient être mises en oeuvre sur l'ensemble des communes d'Ile-de-France ; que ce périmètre a été étendu, à compter du 15 novembre à zéro heure, à l'ensemble du territoire métropolitain par le décret n° 2015-1478 du 14 novembre 2015 ; que l'état d'urgence a, en outre, été déclaré à compter du 19 novembre 2015, sur le territoire des collectivités de Guadeloupe, de la Guyane, de la Martinique, de la Réunion, de Mayotte, de Saint-Barthélemy et de Saint-Martin, par le décret délibéré en conseil des ministres n° 2015-1493 du 18 novembre 2015 ; <br/>
<br/>
              6. Considérant que la loi du 20 novembre 2015 prorogeant l'application de la loi n° 55-385 du 3 avril 1955 relative à l'état d'urgence et renforçant l'efficacité de ses dispositions a prorogé, pour une durée de trois mois à compter du 26 novembre 2015, l'état d'urgence déclaré par les décrets délibérés en conseil des ministres des 14 et 18 novembre 2015 ; que la loi du 20 novembre 2015 a modifié certaines des dispositions de la loi du 3 avril 1955, en particulier celles de l'article 6 de cette loi ; que les modifications résultant de cette loi sont applicables aux mesures prises après son entrée en vigueur, qui est intervenue, en vertu des dispositions particulières de son décret de promulgation, immédiatement à compter de sa publication le 21 novembre 2015 ; <br/>
<br/>
              7. Considérant qu'aux termes de l'article 6 de la loi du 3 avril 1955, dans sa rédaction résultant de la loi du 20 novembre 2015 : " Le ministre de l'intérieur peut prononcer l'assignation à résidence, dans le lieu qu'il fixe, de toute personne résidant dans la zone fixée par le décret mentionné à l'article 2 et à l'égard de laquelle il existe des raisons sérieuses de penser que son comportement constitue une menace pour la sécurité et l'ordre publics dans les circonscriptions territoriales mentionnées au même article 2. Le ministre de l'intérieur peut la faire conduire sur le lieu de l'assignation à résidence par les services de police ou les unités de gendarmerie. / La personne mentionnée au premier alinéa du présent article peut également être astreinte à demeurer dans le lieu d'habitation déterminé par le ministre de l'intérieur, pendant la plage horaire qu'il fixe, dans la limite de douze heures par vingt-quatre heures. / L'assignation à résidence doit permettre à ceux qui en sont l'objet de résider dans une agglomération ou à proximité immédiate d'une agglomération. / En aucun cas, l'assignation à résidence ne pourra avoir pour effet la création de camps où seraient détenues les personnes mentionnées au premier alinéa. / L'autorité administrative devra prendre toutes dispositions pour assurer la subsistance des personnes astreintes à résidence ainsi que celle de leur famille. / Le ministre de l'intérieur peut prescrire à la personne assignée à résidence : / 1° L'obligation de se présenter périodiquement aux services de police ou aux unités de gendarmerie, selon une fréquence qu'il détermine dans la limite de trois présentations par jour, en précisant si cette obligation s'applique y compris les dimanches et jours fériés ou chômés ; / 2° La remise à ces services de son passeport ou de tout document justificatif de son identité. Il lui est délivré en échange un récépissé, valant justification de son identité en application de l'article 1er de la loi n° 2012-410 du 27 mars 2012 relative à la protection de l'identité, sur lequel sont mentionnées la date de retenue et les modalités de restitution du document retenu. / La personne astreinte à résider dans le lieu qui lui est fixé en application du premier alinéa du présent article peut se voir interdire par le ministre de l'intérieur de se trouver en relation, directement ou indirectement, avec certaines personnes, nommément désignées, dont il existe des raisons sérieuses de penser que leur comportement constitue une menace pour la sécurité et l'ordre publics. Cette interdiction est levée dès qu'elle n'est plus nécessaire (...) " ;<br/>
<br/>
              8. Considérant que, ainsi que l'énonce l'article 14-1 de la loi du 3 avril 1955 telle que modifiée par la loi du 20 novembre 2015, les mesures prises sur le fondement de cette loi, à l'exception du prononcé des peines prévues à l'article 13, " sont soumises au contrôle du juge administratif dans les conditions fixées par le code de justice administrative, notamment son livre V " ; <br/>
<br/>
              Sur le pourvoi en cassation :<br/>
<br/>
              9. Considérant que, saisi sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative, le juge des référés du tribunal administratif de Rennes, faisant application de l'article L. 522-3, a rejeté la demande de suspension des effets de l'arrêté du ministre de l'intérieur du 24 novembre 2015 portant assignation à résidence de Mme B... sur le territoire de la commune de Rennes jusqu'au 12 décembre 2015, au motif que la condition d'urgence posée par l'article L. 521-2 ne pouvait, au regard des éléments avancés par l'intéressée, être considérée comme remplie ;<br/>
<br/>
              10. Considérant qu'eu égard à son objet et à ses effets, notamment aux restrictions apportées à la liberté d'aller et venir, une décision prononçant l'assignation à résidence d'une personne, prise par l'autorité administrative en application de l'article 6 de la loi du 3 avril 1955, porte, en principe et par elle-même, sauf à ce que l'administration fasse valoir des circonstances particulières, une atteinte grave et immédiate à la situation de cette personne, de nature à créer une situation d'urgence justifiant que le juge administratif des référés, saisi sur le fondement de l'article L. 521-2 du code de justice administrative, puisse prononcer dans de très brefs délais, si les autres conditions posées par cet article sont remplies, une mesure provisoire et conservatoire de sauvegarde ; <br/>
<br/>
              11. Considérant, par suite, que le juge des référés a commis une erreur de droit en refusant de retenir l'existence d'une situation d'urgence, au vu des éléments avancés par le demandeur et alors que le ministre de l'intérieur ne faisait valoir aucune circonstance particulière ; que, dès lors, Mme B... est fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de l'ordonnance qu'elle attaque ; <br/>
<br/>
              12. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée, en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Sur la demande en référé :<br/>
<br/>
              13. Considérant que, par l'arrêté du 24 novembre 2015 dont il est demandé en référé de suspendre les effets, le ministre de l'intérieur a astreint Mme A... B...à résider sur le territoire de la commune de Rennes jusqu'au 12 décembre 2015 inclus, avec obligation de se présenter trois fois par jour à 9 heures, 14 heures et 19 heures au commissariat de police de Rennes tous les jours de la semaine, y compris les jours fériés ou chômés, et lui a imposé de demeurer tous les jours, entre 20 heures et 6 heures, dans les locaux où elle réside ; que l'arrêté prévoit que Mme B... ne peut se déplacer en dehors de ces lieux d'assignation à résidence sans avoir obtenu préalablement une autorisation écrite du préfet d'Ille-et-Vilaine ; <br/>
<br/>
              14. Considérant que, pour prendre cette décision, le ministre de l'intérieur s'est fondé sur la gravité de la menace terroriste sur le territoire national et sur la nécessité de prendre des mesures afin d'assurer la sécurité de la conférence des Nations-Unies sur les changements climatiques, dite " COP 21 ", qui se déroule à Paris et au Bourget du 30 novembre au 11 décembre 2015 et à laquelle participent des représentants de très nombreux pays et un très grand nombre de chefs d'Etat et de gouvernement étrangers ; que le ministre a relevé qu'avaient été lancés des mots d'ordre appelant à des actions revendicatives violentes, aux abords de la conférence et de sites sensibles en Ile-de-France ; que le ministre a exposé, dans les motifs de sa décision, que la forte mobilisation des forces de l'ordre pour lutter contre la menace terroriste ne saurait être détournée, dans cette période, pour répondre aux risques d'ordre public liés à de telles actions ; <br/>
<br/>
              15. Considérant qu'une décision prononçant l'assignation à résidence d'une personne, prise par l'autorité administrative sur le fondement de l'article 6 de la loi du 3 avril 1955, porte atteinte à la liberté d'aller et venir, qui constitue une liberté fondamentale au sens de l'article L. 521-2 du code de justice administrative ; <br/>
<br/>
              16. Considérant que l'article 6 de la loi du 3 avril 1955, modifié par la loi du 20 novembre 2015, permet au ministre de l'intérieur, dans les zones territoriales où l'état d'urgence reçoit application, déterminées par le décret mentionné à l'article 2 de la loi, de prononcer l'assignation à résidence, dans le lieu qu'il fixe et selon les modalités qu'il retient parmi les sujétions susceptibles d'être prescrites en vertu de l'article 6, de " toute personne résidant dans la zone fixée par le décret mentionné à l'article 2 et à l'égard de laquelle il existe des raisons sérieuses de penser que son comportement constitue une menace pour la sécurité et l'ordre publics dans les circonscriptions territoriales mentionnées au même article 2 " ; que ces dispositions, dont la question de la conformité aux droits et libertés garantis par la Constitution a été renvoyée au Conseil constitutionnel par la décision du Conseil d'Etat statuant au contentieux rendue ce jour sous le n° 395009, de par leur lettre même, n'établissent pas de lien entre la nature du péril imminent ou de la calamité publique ayant conduit à ce que soit déclaré l'état d'urgence et la nature de la menace pour la sécurité et l'ordre publics susceptible de justifier une mesure d'assignation à résidence ; qu'elles doivent en l'état être comprises comme ne faisant pas obstacle à ce que le ministre de l'intérieur, tant que l'état d'urgence demeure en vigueur, puisse décider, sous l'entier contrôle du juge de l'excès de pouvoir, l'assignation à résidence de toute personne résidant dans la zone couverte par l'état d'urgence, dès lors que des raisons sérieuses donnent à penser que le comportement de cette personne constitue, compte tenu du péril imminent ou de la calamité publique ayant conduit à la déclaration de l'état d'urgence, une menace pour la sécurité et l'ordre publics ; <br/>
<br/>
              17. Considérant que, dans l'attente de la décision du Conseil constitutionnel sur la question prioritaire de constitutionnalité qui lui est renvoyée, la demande en référé doit être examinée par le Conseil d'Etat au regard et compte tenu des dispositions de l'article 6 de la loi du 3 avril 1955, telles qu'elles sont en vigueur à la date de la présente décision ; <br/>
<br/>
              18. Considérant qu'il appartient au Conseil d'Etat statuant en référé de s'assurer, en l'état de l'instruction devant lui, que l'autorité administrative, opérant la conciliation nécessaire entre le respect des libertés et la sauvegarde de l'ordre public, n'a pas porté d'atteinte grave et manifestement illégale à une liberté fondamentale, que ce soit dans son appréciation de la menace que constitue le comportement de l'intéressé, compte tenu de la situation ayant conduit à la déclaration de l'état d'urgence, ou dans la détermination des modalités de l'assignation à résidence ; que les juge des référés, s'il estime que les conditions définies à l'article L. 521-2 du code de justice administrative sont réunies, peut prendre toute mesure qu'il juge appropriée pour assurer la sauvegarde de la liberté fondamentale à laquelle il a été porté atteinte ; <br/>
<br/>
              19. Considérant qu'il résulte de l'instruction, et notamment des documents versés au dossier par le ministre de l'intérieur dans le cadre du débat contradictoire devant le Conseil d'Etat, que Mme B... a été interpellée, à Milan, le 28 avril 2015, peu de temps avant l'inauguration de l'exposition universelle devant se tenir dans cette ville à compter du 1er mai 2015, dans le cadre d'une opération menée par les forces de l'ordre italiennes dans des lieux qu'elle occupait avec d'autres activistes ; qu'ont été découverts dans ces lieux des barres de fer, casques, matraques et du matériel permettant de fabriquer des engins incendiaires ; que les manifestations qui se sont déroulées le 1er mai 2015 à l'occasion de l'inauguration de l'exposition universelle ont donné lieu à des incidents violents causés par des activistes ; qu'aucune disposition législative ni aucun principe ne s'oppose à ce que les faits relatés par les " notes blanches " produites par le ministre, qui ont été versées au débat contradictoire et ne sont pas sérieusement contestées par la requérante, soient susceptibles d'être pris en considération par le juge administratif ; <br/>
<br/>
              20. Considérant qu'il résulte également de l'instruction que les forces de l'ordre demeurent ...; <br/>
<br/>
              21. Considérant, dans ces conditions, qu'il n'apparaît pas, en l'état, qu'en prononçant l'assignation à résidence de Mme B... jusqu'à la fin de la conférence des Nations-Unies sur les changements climatiques au motif qu'il existait de sérieuses raisons de penser que son comportement constitue une menace pour la sécurité et l'ordre publics et en en fixant les modalités d'exécution, le ministre de l'intérieur, conciliant les différents intérêts en présence, aurait porté une atteinte grave et manifestement illégale à la liberté d'aller et venir ; <br/>
<br/>
              22. Considérant, enfin, qu'il résulte de l'instruction que le moyen tiré de ce que l'arrêté contesté n'aurait pas été signé par une autorité administrative régulièrement habilitée manque en fait ; qu'il résulte, en outre, tant des termes de l'article L. 521-2 du code de justice administrative que du but dans lequel la procédure qu'il instaure a été créée que doit exister un rapport direct entre l'illégalité relevée à l'encontre de l'autorité administrative et la gravité de ses effets au regard de l'exercice de la liberté fondamentale en cause ; que la circonstance que la décision attaquée aurait été prise sans que se déroule la procédure contradictoire prévue par l'article 24 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations, ne saurait, par elle-même, porter une atteinte grave à l'exercice de la liberté d'aller et venir, au sens de l'article L. 521-2 du code de justice administrative ; <br/>
<br/>
              23. Considérant qu'il résulte de tout ce qui précède que les conclusions présentées par Mme B... sur le fondement de l'article L. 521-2 du code de justice administrative doivent être rejetées ; que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
      Article 1er : L'intervention de la Ligue des droits de l'homme est admise. <br/>
<br/>
Article 2 : L'ordonnance du juge des référés du tribunal administratif de Rennes du 30 novembre 2015 est annulée.<br/>
<br/>
Article 3 : La demande présentée par Mme B... devant le juge des référés du tribunal administratif de Rennes et le surplus des conclusions de son pourvoi sont rejetés.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme A...B..., au ministre de l'intérieur et à la Ligue des droits de l'homme. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
