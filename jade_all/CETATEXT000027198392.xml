<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027198392</ID>
<ANCIEN_ID>JG_L_2013_03_000000339186</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/19/83/CETATEXT000027198392.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème sous-section jugeant seule, 20/03/2013, 339186, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-03-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>339186</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROGER, SEVAUX</AVOCATS>
<RAPPORTEUR>M. François Loloum</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:339186.20130320</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 3 mai 2010 au secrétariat du contentieux du Conseil d'Etat, présenté pour Mlle B...A..., demeurant...,; Mlle A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 634586 du 7 mai 2009 en tant que la Cour nationale du droit d'asile a rejeté sa demande tendant à l'annulation de la décision du 10 juillet 2008 du directeur général de l'Office français de protection des réfugiés et apatrides rejetant sa demande d'admission au statut de réfugié ;<br/>
<br/>
              2°) réglant l'affaire au fond, d'annuler la décision du directeur général de l'Office français de protection des réfugiés et apatrides et de faire droit à sa demande d'asile ;<br/>
<br/>
              3°) de mettre à la charge de l'Office français de protection des réfugiés et apatrides la somme de 3 000 euros à verser à la SCP Roger-Sevaux au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 21 février 2013, présentée pour Mlle A... ;<br/>
<br/>
              Vu la convention de Genève du 28 juillet 1951 et le protocole signé à New York le 31 janvier 1967 ;<br/>
<br/>
              Vu la directive européenne n° 2004-83 du 29 avril 2004 du Conseil concernant les normes minimales relatives aux conditions que doivent remplir les ressortissants des pays tiers ou les apatrides pour pouvoir prétendre au statut de réfugié ou les personnes qui, pour d'autres raisons, ont besoin d'une protection internationale, et relatives au contenu de ces statuts ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Loloum, Conseiller d'Etat,  <br/>
<br/>
              - les observations de la SCP Roger, Sevaux, avocat de MlleA...,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Roger, Sevaux, avocat de Mlle A... ;<br/>
<br/>
<br/>
<br/>
<br/>Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              1. Considérant qu'aux termes de l'article 1er-A-2° de la convention de Genève du 28 juillet 1951 et du protocole signé à New York le 31 janvier 1967, doit être considérée comme réfugiée : " Toute personne qui, craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut, ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays " ;<br/>
<br/>
              2. Considérant qu'un groupe social, au sens de ces stipulations et de la directive du 29 avril 2004 susvisée, est constitué de personnes partageant un caractère inné, une histoire commune ou une caractéristique essentielle à leur identité et à leur conscience, auxquels il ne peut leur être demandé de renoncer, ou une identité propre perçue comme étant différente par la société environnante ou par les institutions ; <br/>
<br/>
              3. Considérant que, pour refuser à Mlle A...la qualité de réfugié, la Cour nationale du droit d'asile a exclu en principe qu'elle puisse, du fait de son opposition à l'excision, appartenir à un groupe social au sens défini au point 2 ; qu'en s'abstenant de rechercher si les circonstances, dont se prévalait l'intéressée, selon lesquelles le Mali, dont elle est originaire, est marqué, particulièrement au sein de l'ethnie peuhle dont elle est issue, par une forte prévalence de l'excision et qu'elle avait manifesté son opposition à cette pratique, transgressant les normes coutumières de son pays et s'exposant de ce fait à des violences dirigées contre elle, étaient de nature à caractériser en l'espèce l'appartenance à un groupe social, la Cour nationale du droit d'asile a commis une erreur de droit ; que dès lors, son arrêt doit être annulé ;<br/>
<br/>
              Sur les conclusions tendant à l'application des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 :<br/>
<br/>
              4. Considérant que Mlle A...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Roger-Sevaux, avocat de MlleA..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Office français de protection des réfugiés et apatrides la somme de 1 500 euros à verser à la SCP Roger-Sevaux ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt de la Cour nationale du droit d'asile est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la Cour nationale du droit d'asile.<br/>
Article 3 : L'Office français de protection des réfugiés et apatrides versera à la SCP Roger-Sevaux, avocat de MlleA..., une somme de 1 500 euros en application des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat. <br/>
Article 4 : La présente décision sera notifiée à Mlle B...A...et à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
