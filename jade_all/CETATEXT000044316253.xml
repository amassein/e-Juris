<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044316253</ID>
<ANCIEN_ID>JG_L_2021_10_000000457027</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/31/62/CETATEXT000044316253.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 25/10/2021, 457027, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>457027</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:457027.20211025</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête et un nouveau mémoire, enregistrés les 27 septembre et 5 octobre 2021 au secrétariat du contentieux du Conseil d'Etat, M. D... A..., Mme C... B..., épouse A..., et l'association BonSens.org demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution du 6° de l'article 1er du décret n° 2021-1059 du 7 août 2021, en ce qu'il exempte les personnes mineures vaccinées contre la Covid-19 de l'obligation de présenter un test de dépistage négatif de cette maladie pour accéder aux lieux et services soumis à la présentation d'un passe sanitaire ; <br/>
<br/>
              2°) d'enjoindre au Premier ministre d'abroger cet article en tant qu'il ne soumet pas les personnes mineures vaccinées contre la Covid-19 à l'obligation de présenter un test de dépistage négatif de cette maladie pour accéder aux lieux et services soumis à la présentation d'un passe sanitaire ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
              - la condition d'urgence est satisfaite en ce que le fils de M. et Mme A... ne peut plus accéder depuis le 30 septembre 2021 à la maison des associations où il suit des cours de portugais ni aux complexes sportifs dans lesquels il pratique le football et la boxe ; <br/>
              - il est porté une atteinte grave et manifestement illégale à la liberté d'aller et venir, au droit à l'instruction, à la liberté de pratiquer un sport, au droit de ne pas être soumis à un traitement inhumain et dégradant et au droit au respect de la vie privée et familiale ; <br/>
              - ces atteintes ne sont pas proportionnées à l'objectif de protection de la santé publique dès lors que le vaccin n'empêche pas la transmission du virus, que le recours répété aux tests de dépistage contre la Covid-19 est douloureux et dangereux, que les mineurs sont peu contaminés par la Covid-19 et présentent peu de formes graves et que la létalité des vaccins peut être supérieure à la maladie elle-même dans cette tranche d'âge ;<br/>
              - l'exigence du passe sanitaire porte atteinte au principe d'égalité et aux dispositions règlement (UE) n° 2021/953 du Parlement européen et du Conseil du 14 juin 2021, en ce qu'elle traite différemment les mineurs vaccinés et ceux qui ne le sont pas, alors qu'ils sont potentiellement aussi contagieux les uns que les autres ;<br/>
              - le " passe sanitaire ", qui peut être diffusé sur internet, méconnaît l'exigence de sûreté posée par le règlement (UE) n° 2021/953.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le règlement (UE) n° 2021/953 du Parlement européen et du Conseil du 14 juin 2021 ;<br/>
              - la loi n° 2021-689 du 31 mai 2021 ;<br/>
              - la loi n° 2021-1040 du 5 août 2021 ;  <br/>
              - le décret n° 2021-699 du 1er juin 2021 ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Le requérant qui saisit le juge des référés sur le fondement des dispositions de l'article L. 521-2 doit justifier des circonstances particulières caractérisant la nécessité pour lui de bénéficier à très bref délai d'une mesure de la nature de celles qui peuvent être ordonnées sur le fondement de cet article.<br/>
<br/>
              3. M. A... et autres demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, de suspendre l'exécution des dispositions de l'article 47-1 du décret du 1er juillet 2021, issues du 6° de l'article 1er du décret du 7 août 2021, en ce qu'elles exemptent les personnes mineures vaccinées contre la Covid-19 de l'obligation présenter un test de dépistage négatif pour accéder aux lieux et services soumis à la présentation d'un passe sanitaire.<br/>
<br/>
              4. Pour justifier qu'il soit fait droit aux conclusions de leur demande en référé, les requérants se bornent à faire valoir que le fils mineur de M. et Mme A..., qui n'est pas vacciné, ne peut plus avoir accès à la maison des associations où il suit des cours de portugais ni aux complexes sportifs dans lesquels il pratique le football et la boxe sans présenter de tests de dépistage négatif. La mesure de suspension demandée est sans incidence sur cette situation. Par suite, les circonstances invoquées sont insusceptibles de caractériser la nécessité pour les requérants d'obtenir à très bref délai le prononcé de cette mesure.<br/>
<br/>
              5. Il résulte de ce qui précède que faute d'urgence, il est manifeste que la requête de M. A... et autres ne peut être accueillie. Elle doit, par suite, être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, selon la procédure prévue par l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. et Mme A... et de l'association BonSens.org est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. D... A..., premier requérant dénommé.<br/>
Fait à Paris, le 25 octobre 2021.<br/>
    Signé : Jean-Yves Ollier<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
