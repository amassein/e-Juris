<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038088232</ID>
<ANCIEN_ID>JG_L_2019_02_000000412137</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/08/82/CETATEXT000038088232.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 01/02/2019, 412137, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412137</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:412137.20190201</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société Nateva a demandé au tribunal administratif de Grenoble de prononcer la décharge des cotisations supplémentaires d'impôt sur les sociétés auxquelles elle a été assujettie au titre des exercices clos en 2008, 2009 et 2010 et des intérêts de retard correspondants. Par un jugement n° 1302740 du 9 juillet 2015, le tribunal administratif de Grenoble a rejeté sa demande. <br/>
<br/>
              Par un arrêt du n°15LY03023 du 2 mai 2017, la cour administrative d'appel de Lyon, après avoir annulé le jugement, a rejeté la demande de la société Nateva.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 4 juillet et 4 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, la société Nateva demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts ; <br/>
              - la décision du 26 avril 2018 par laquelle le Conseil d'Etat statuant au contentieux n'a pas renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société Nateva ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, au Cabinet Briard, avocat de la société Nateva ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à l'issue d'une vérification de comptabilité de la société Nateva, l'administration a remis en cause l'exonération prévue par l'article 44 sexies du code général des impôts dont cette société avait bénéficié au titre des exercices clos en 2008, 2009 et 2010. Par un jugement du 9 juillet 2015, le tribunal administratif de Grenoble a rejeté la demande de cette société tendant à la décharge des cotisations supplémentaires d'impôt sur les sociétés auxquels elle a été assujettie. Elle se pourvoit en cassation contre l'arrêt 2 mai 2017 par lequel la cour administrative d'appel de Lyon, après avoir annulé ce jugement, a rejeté sa demande. <br/>
<br/>
              2. Aux termes de l'article 44 sexies du code général des impôts, dans sa rédaction issue de la loi du 30 décembre 1999 de finances pour 2000 applicable au litige : " I. Les entreprises soumises de plein droit ou sur option à un régime réel d'imposition de leurs résultats et qui exercent une activité industrielle, commerciale ou artisanale au sens de l'article 34 sont exonérées d'impôt sur le revenu ou d'impôt sur les sociétés à raison des bénéfices réalisés (...) jusqu'au terme du vingt-troisième mois suivant celui de leur création (...). II. Le capital des sociétés nouvellement créées ne doit pas être détenu, directement ou indirectement, pour plus de 50 % par d'autres sociétés. / Pour l'application du premier alinéa, le capital d'une société nouvellement créée est détenu indirectement par d'autres sociétés lorsque l'une au moins des conditions suivantes est remplie : a - un associé exerce en droit ou en fait une fonction de direction ou d'encadrement dans une autre entreprise, lorsque l'activité de celle-ci est similaire à celle de l'entreprise nouvellement créée ou lui est complémentaire ". <br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond qu'au cours des exercices en litige, la société Nateva avait pour activité l'acquisition auprès de cueilleurs et de producteurs de plantes qu'elle valorisait en les transformant en extraits sous diverses formes. S'agissant des compléments alimentaires aqueux, qui représentaient près d'un tiers de son chiffre d'affaires, la société conditionnait elle-même une partie de sa production, le reste étant vendu à des clients qui réalisaient eux-mêmes le conditionnement. De son côté, la société Laboratoire Pasquier - dont il est constant que le directoire et 25 % du capital étaient, respectivement, présidé et détenus par deux associés détenant chacun un tiers du capital de la société Nateva - intervenait dans le domaine du façonnage de produits pharmaceutiques et de compléments alimentaires sous forme liquide. Si elle façonnait et conditionnait une partie des produits aqueux qu'elle commercialisait, elle en acquérait également une partie auprès de la société Nateva, dont elle était une des principales clientes. En se fondant sur ces éléments pour juger qu'une partie significative des activités des deux sociétés étaient soit similaires soit complémentaires, au sens des dispositions du II de l'article 44 sexies du code général des impôts, et en tenant pour inopérante, à cet égard, la circonstance que les deux sociétés n'avaient pas signé de contrat d'exclusivité ou de sous-traitance et étaient liées, en revanche, par un accord de non-concurrence, la cour n'a ni dénaturé les pièces du dossier ni commis d'erreur de droit. <br/>
<br/>
              4. Il résulte de ce qui précède que la société Nateva n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. Ses conclusions tendant au bénéfice des dispositions de l'article L. 761-1 du code de justice administrative doivent, par voie de conséquence, être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Nateva est rejeté. <br/>
Article 2 : La présente décision sera notifiée à la société Nateva et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
