<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028987562</ID>
<ANCIEN_ID>JG_L_2014_05_000000357934</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/98/75/CETATEXT000028987562.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 26/05/2014, 357934, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-05-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357934</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Marie Grosset</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:357934.20140526</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>VU LA PROCEDURE SUIVANTE :<br/>
<br/>
 Procédure contentieuse antérieure<br/>
<br/>
              La société civile immobilière David G a demandé au tribunal administratif d'Amiens d'annuler pour excès de pouvoir la délibération du 22 janvier 2009 par laquelle le conseil municipal de la commune de Servais (Aisne) a décidé d'instituer un droit de préemption urbain sur les parcelles cadastrées section AC n° 67 et 68 au lieu-dit " Le Jardin Navet " et le courrier du 12 mars 2009 par lequel le maire de la commune a décidé de préempter ces parcelles.<br/>
<br/>
              Par jugement n° 0900514 du 29 mars 2011, le tribunal administratif d'Amiens a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 11DA00777 du 26 janvier 2012, la cour administrative d'appel de Douai a, à la demande de la SCI David G, annulé le jugement du tribunal administratif d'Amiens du 29 mars 2011, la délibération du conseil municipal de Servais du 22 janvier 2009 et la décision du maire de la commune du 12 mars 2009. <br/>
<br/>
 Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 26 mars et 27 juin 2012 au secrétariat du contentieux du Conseil d'Etat, la commune de Servais demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt n° 11DA00777 de la cour administrative d'appel de Douai du 26 janvier 2012 ;<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la SCI David G ;  <br/>
<br/>
              3°) de mettre à la charge de la SCI David G la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Elle soutient que :<br/>
              - cet arrêt est irrégulier  faute de viser les dispositions des articles L. 210-1 et L. 300-1 du code de l'urbanisme dont il fait application ; <br/>
              - en exerçant un contrôle d'erreur manifeste d'appréciation sur l'institution du droit de préemption par la commune de Servais, la cour a commis une erreur de droit ;<br/>
              - en jugeant que la superficie incluse dans le périmètre du droit de préemption présentait un caractère disproportionné au regard de la taille de la commune, la cour a dénaturé les faits de l'espèce ; <br/>
              - en ne soulevant pas d'office l'irrecevabilité des conclusions tendant à l'annulation du courrier du maire de Servais du 12 mars 2009, alors qu'elles étaient à la fois tardives et nouvelles, la cour a commis une erreur de droit ; <br/>
              - en jugeant que cette lettre était une décision de préemption et non une simple proposition d'acquisition amiable, elle a dénaturé les pièces du dossier.<br/>
<br/>
              Par un mémoire en défense, enregistré le 3 décembre 2012, la SCI David G conclut au rejet du pourvoi et à ce que la somme de 5 000 euros soit mise à la charge de la commune de Servais au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Elle soutient que le moyen tiré de l'irrégularité de l'arrêt, faute d'avoir visé les  articles L 210-1 et L 300-1 du code de l'urbanisme, est inopérant et qu'aucun des moyens soulevés par le requérant n'est fondé.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ; <br/>
              - le code de l'urbanisme ; <br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Grosset, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune de Servais et à Me Balat, avocat de la SCI David G.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>CONSIDERANT CE QUI SUIT :<br/>
<br/>
              Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              1. En vertu du deuxième alinéa de l'article R. 741-2 du code de justice administrative, la décision contient " les visas des dispositions législatives ou réglementaires dont elle fait application ". L'arrêt attaqué, qui annule la délibération du 22 janvier 2009 par laquelle le conseil municipal de la commune de Servais a décidé d'instituer un droit de préemption urbain au motif qu'elle était entachée d'erreur manifeste d'appréciation compte tenu de l'importance du projet, vise le code de l'urbanisme et cite dans ses motifs les dispositions de son article L. 211-1, sur le fondement duquel la délibération a été adoptée. Il satisfait ainsi aux exigences des dispositions précitées de l'article R. 741-2 du code de justice administrative, alors même qu'il ne vise pas expressément les articles L. 210-1 et L. 300-1 du code de l'urbanisme. Par suite, la commune requérante n'est pas fondée à soutenir qu'il serait entaché d'irrégularité.<br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué, en ce qui concerne la légalité de la délibération du conseil municipal de Servais du 22 janvier 2009 :<br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 210-1 du code de l'urbanisme : " Les droits de préemption institués par le présent titre sont exercés en vue de la réalisation, dans l'intérêt général, des actions ou opérations répondant aux objets définis à l'article L. 300-1 (...) ". Aux termes du deuxième alinéa de l'article L 211-1 du même code : " Les conseils municipaux des communes dotées d'une carte communale approuvée peuvent, en vue de la réalisation d'un équipement ou d'une opération d'aménagement, instituer un droit de préemption dans un ou plusieurs périmètres délimités par la carte. La délibération précise, pour chaque périmètre, l'équipement ou l'opération projetée ". <br/>
<br/>
              3. En premier lieu, il appartient au juge de l'excès de pouvoir de vérifier si les projets d'équipement ou d'opération envisagés par le titulaire du droit de préemption sont de nature à justifier légalement l'exercice du droit de préemption sur le fondement des dispositions précitées de l'article L 211-1 du code de l'urbanisme et, notamment, répondent à un intérêt général suffisant. Il suit de là qu'en vérifiant si la délibération attaquée du 22 janvier 2009 reposait sur une erreur manifeste d'appréciation eu égard aux besoins de la commune, la cour administrative d'appel s'est méprise sur l'étendue du contrôle qu'il lui appartenait d'exercer. Toutefois, la cour ayant jugé que cette délibération était entachée d'erreur manifeste d'appréciation, l'erreur ainsi commise dans l'étendue de son contrôle est dépourvue de toute incidence sur l'appréciation qu'elle a portée sur la légalité de l'institution du droit de préemption par la commune et n'est, par suite, pas de nature à entraîner l'annulation de l'arrêt attaqué. <br/>
<br/>
              4. En second lieu, en jugeant, après avoir relevé que la commune se bornait à faire valoir que sa population, de 240 habitants, allait atteindre 300 habitants, que le projet de réalisation d'aires de jeux et de loisirs, d'un mini-terrain de football et d'un bassin à poissons, sur des parcelles d'une superficie de plus d'un hectare, présentait un caractère disproportionné, la cour a exactement qualifié les faits de l'espèce. <br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué, en ce qui concerne la recevabilité du recours dirigé contre le courrier du maire de Servais du 12 mars 2009 :<br/>
<br/>
              5. En premier lieu, l'article R. 421-5 du code de justice administrative dispose que : " Les délais de recours contre une décision administrative ne sont opposables qu'à la condition d'avoir été mentionnés, ainsi que les voies de recours, dans la notification de la décision ". <br/>
<br/>
              6. Il ressort des pièces du dossier soumis aux juges du fond que la SCI David G a été informée, par un courrier du maire du 12 mars 2009 qui ne mentionnait pas les voies et délais de recours, que la commune avait décidé d'exercer son droit de préemption sur des parcelles lui appartenant. La circonstance que la société aurait nécessairement eu connaissance de cette décision au plus tard le 20 mars 2009, date à laquelle elle a produit ce courrier devant le tribunal administratif d'Amiens dans l'instance qu'elle avait introduite à l'encontre de la délibération du conseil municipal du 22 janvier 2009 instituant le droit de préemption, n'était pas de nature à faire courir le délai de recours contentieux. Dès lors, ses conclusions tendant à l'annulation de la décision du 12 mars 2009, présentées devant ce même tribunal le 19 août 2009, n'étaient pas tardives. En outre, à supposer même que ces conclusions n'aient pas présenté un lien suffisant avec les conclusions de la requête introductive d'instance de la société, le tribunal administratif n'aurait pu les rejeter comme irrecevables sans avoir invité cette dernière à les régulariser par la présentation d'une requête distincte. Par suite, la commune requérante n'est pas fondée à soutenir que la cour aurait commis une erreur de droit en ne soulevant pas d'office l'irrecevabilité de ces conclusions.<br/>
<br/>
              7. En second lieu, il ressort des pièces du dossier soumis aux juges du fond que le courrier du 12 mars 2009 adressé par lettre recommandée avec demande d'avis de réception par le maire de la commune de Servais à la SCI David G mentionnait " que la commune de Servais a décidé d'exercer son droit de préemption sur les parcelles n° AC 67 et AC 68 - lieu dit " Le Jardin Navet " - d'une contenance totale de 11455 m2 " à la suite d'une " délibération du conseil municipal du 22 janvier 2009, légalisé par la préfecture en date du  27 janvier 2009 ". Par suite, la cour, qui n'avait pas, au stade de l'examen de la recevabilité de la requête, à tenir compte d'éventuelles irrégularités entachant cet acte, ne s'est pas méprise sur la nature de ce courrier en jugeant qu'il présentait le caractère d'une décision faisant grief.<br/>
<br/>
              8. Il résulte de tout ce qui précède que la commune de Servais n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. <br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la SCI David G, qui n'est pas, dans la présente instance, la partie perdante. En revanche, il y lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Servais une somme de 3 000 euros à verser à la SCI David G au titre des mêmes dispositions. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la commune de Servais est rejeté.<br/>
Article 2 : La commune de Servais versera une somme de 3 000 euros à la SCI David G au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la commune de Servais et à la société civile immobilière David G.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
