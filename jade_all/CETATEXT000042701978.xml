<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042701978</ID>
<ANCIEN_ID>JG_L_2020_12_000000424290</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/70/19/CETATEXT000042701978.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 18/12/2020, 424290, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424290</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>Mme Louise Cadin</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:424290.20201218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un nouveau mémoire et un mémoire en réplique, enregistrés les 17 septembre 2018, 17 décembre 2019 et 14 février 2020 au secrétariat du contentieux du Conseil d'Etat, l'association Forestiers du monde demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le décret n° 2018-254 du 6 avril 2018 relatif au régime spécial applicable dans les forêts de protection prévu à l'article L. 141-1 du code forestier, ainsi que les décisions rejetant leurs recours gracieux dirigés contre ce décret ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'environnement ;<br/>
              - le code forestier ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... A..., auditrice,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Texidor, Perier, avocat de l'association forestiers du Monde.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 141-1 du code forestier, relatif aux forêts de protection, dispose que : " Peuvent être classés comme forêts de protection, pour cause d'utilité publique, après enquête publique réalisée conformément aux dispositions du chapitre III du titre II du livre Ier du code de l'environnement : / 1° Les bois et forêts dont la conservation est reconnue nécessaire au maintien des terres sur les montagnes et sur les pentes, à la défense contre les avalanches, les érosions et les envahissements des eaux et des sables ; / 2° Les bois et forêts situés à la périphérie des grandes agglomérations ; / 3° Les bois et forêts situés dans les zones où leur maintien s'impose soit pour des raisons écologiques, soit pour le bien-être de la population ". L'article L. 141-2 du même code dispose que : " Le classement comme forêt de protection interdit tout changement d'affectation ou tout mode d'occupation du sol de nature à compromettre la conservation ou la protection des boisements ". Aux termes de l'article L. 141-3 : " Dès la notification au propriétaire de l'intention de classer une forêt en forêt de protection, aucune modification ne peut être apportée à l'état des lieux, aucune coupe ne peut être effectuée ni aucun droit d'usage créé pendant quinze mois à compter de la date de notification, sauf autorisation de l'autorité administrative compétente de l'Etat ". Enfin, l'article L. 141-4 dispose que : " Les forêts de protection sont soumises à un régime spécial, déterminé par décret en Conseil d'Etat, en ce qui concerne notamment (...) les fouilles et extractions de matériaux (...) ". Pour l'application de ces dernières dispositions, l'article R. 141-14 du même code dispose que : " Aucun défrichement, aucune fouille, aucune extraction de matériaux, aucune emprise d'infrastructure publique ou privée, aucun exhaussement du sol ou dépôt ne peuvent être réalisés dans une forêt de protection. / Par exception, le propriétaire peut procéder à des travaux qui ont pour but de créer les équipements indispensables à la mise en valeur et à la protection de la forêt (...) ".<br/>
<br/>
              2. L'association Forestiers du monde demande l'annulation pour excès de pouvoir du décret du 6 avril 2018 relatif au régime spécial applicable dans les forêts de protection prévu à l'article L. 141-4 du code forestier, qui modifie la partie réglementaire du code forestier relative à ce régime spécial pour autoriser dans les forêts de protection, sous réserve du respect des conditions qu'il définit, d'une part, la réalisation de fouilles et de sondages archéologiques et, d'autre part, la recherche et l'exploitation souterraine de gisements d'intérêt national de gypse. <br/>
<br/>
              Sur la légalité externe du décret attaqué :<br/>
<br/>
              3. Contrairement à ce que soutient l'association requérante, aucun texte ni aucun principe n'imposaient la consultation préalable de l'Office national des forêts. Par ailleurs, le moyen tiré de ce que d'autres organismes auraient dû être consultés n'est pas assorti des précisions permettant d'en apprécier le bien-fondé.<br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              4. En premier lieu, il résulte des dispositions de l'article L. 141-2 du code forestier citées au point 1 que le législateur n'a pas entendu interdire tout changement d'affectation ou tout mode d'occupation du sol dans les forêts de protection, mais seulement ceux qui sont de nature à compromettre la conservation ou la protection des boisements. En outre, il résulte des termes de l'article L. 141-4 du code forestier, cités au point 1, que les fouilles et extractions de matériaux peuvent être autorisées dans le cadre d'un régime spécial déterminé par décret en Conseil d'Etat.<br/>
<br/>
              5. Par suite, en prévoyant, d'une part, par l'introduction au code forestier des articles R. 141-38-1 à R.141-38-4  pour les fouilles et les sondages archéologiques et, d'autre part, par l'introduction au même code des articles R. 141-38-5 à R.141-38-9 pour la recherche ou  l'exploitation souterraine de gisements d'intérêt national de gypse, que de telles activités peuvent être autorisées par le préfet dans le périmètre d'une forêt de protection sous réserve, ainsi que le précisent respectivement les articles R.141-38-1 et R.141-38-5, qu'elles ne compromettent pas les exigences, fixées à l'article L. 141-2 du même code, de conservation et de protection des boisements et qu'elles ne " modifient pas fondamentalement la destination forestière des terrains ", le décret attaqué ne méconnaît pas, contrairement à ce que soutient l'association requérante, l'article L. 141-2 du code forestier. <br/>
<br/>
              6. En deuxième lieu, le septième alinéa du II l'article R. 141-38-5 du code forestier, introduit par le décret attaqué et relatif à la recherche ou à l'exploitation souterraine des gisements d'intérêt national de gypse dispose que : " Pour les équipements, constructions, aménagements et infrastructures indispensables à l'exploitation souterraine et à la sécurité de celle-ci, l'emprise correspondante ne peut pas dépasser six hectares de la surface de la forêt protégée (...) ".<br/>
<br/>
              7. L'autorisation ne pouvant être délivrée par le préfet que si les équipements, constructions, aménagements et infrastructures indispensables à l'exploitation souterraine et à la sécurité mentionnés ci-dessus respectent, outre cette limitation de surface, l'ensemble des autres conditions légales,  notamment, conformément au 1° et 2° du II du même article, les exigences de conservation et de protection des boisements ainsi que celles de conservation de l'écosystème forestier et de stabilité des sols, la fixation d'une surface maximale de six hectares n'est pas, contrairement à ce que soutient l'association requérante, entachée d'erreur manifeste d'appréciation.<br/>
<br/>
              8. En troisième lieu, le décret attaqué insère au code de l'environnement un article R. 181-33-1 qui dispose, pour l'exploitation souterraine d'une carrière de gypse située dans une forêt de protection, que la décision prise par le préfet sur la demande d'autorisation environnementale est soumise à un avis conforme du ministre chargé des forêts, dont le silence gardé pendant deux mois vaut avis favorable.<br/>
<br/>
              9. Ces dispositions ayant pour seul effet de créer une procédure d'avis conforme du ministre chargé des forêts, l'association requérante n'est en tout état de cause pas fondée à soutenir qu'elles méconnaissent le principe de non-régression posé par les dispositions de l'article L. 110-1 du code de l'environnement, la circonstance qu'elles prévoient un avis favorable en cas de silence gardé par le ministre chargé des forêts étant, à cet égard, sans incidence. <br/>
<br/>
              10. En quatrième lieu, si le premier alinéa de l'article R. 141-38-4 du code forestier, introduit par le décret attaqué, prévoit que les opérations de fouilles et de sondages archéologiques qui ont été autorisées dans une forêt avant son classement comme forêt de protection peuvent être poursuivies sans  qu'il soit nécessaire d'obtenir l'autorisation préfectorale prévue par le nouvel article R.141-38-1, il impose néanmoins que ces opérations fassent l'objet d'une information du préfet de région et d'une appréciation, par le préfet compétent, du risque d'atteinte à la conservation et à la protection des boisements ainsi qu'à la conservation des écosystèmes forestiers et à la stabilité des sols dans le périmètre de protection. En cas de risque d'atteinte à ces intérêts, le deuxième alinéa du même article R.141-38-4 prévoit que le préfet peut imposer les prescriptions qu'il estime nécessaires pour limiter les incidences des travaux. Par suite, en dispensant les opérations de fouilles et de sondages archéologiques engagées préalablement à l'entrée en vigueur du classement en forêt de protection de l'autorisation requise en principe pour de tels travaux, le décret attaqué ne méconnaît pas les dispositions de l'article L.141-2 du code forestier. Il ne porte pas non plus, sur ce point, atteinte au droit de propriété des propriétaires de parcelles classées comme forêt de protection et n'instaure aucune situation qui serait, à l'égard de ces propriétaires, contraire au principe d'égalité, alors même qu'en vertu de l'article L.141-3 du code cité ci-dessus, s'applique à ces derniers l'interdiction de toute modification de l'état des lieux dès que leur est notifiée une intention de classement. <br/>
<br/>
              11. Toutefois, ce même deuxième alinéa du nouvel article R. 141-38-4 du code forestier prévoit que les prescriptions que le préfet peut ainsi imposer à une opération de fouille ou de sondage autorisée avant le classement, lorsque ces prescriptions se révèlent nécessaires compte tenu de l'incidence de l'opération sur la stabilité des sols, la végétation forestière ou les écosystèmes forestiers, doivent être " proportionnées afin de ne pas compromettre l'opération ". En imposant ainsi une exigence de proportionnalité au regard des seuls besoins de l'opération de fouille ou de sondage, le décret attaqué méconnaît la nécessité de veiller aussi à ne pas compromettre la conservation ou la protection des boisements qui résulte des dispositions citées ci-dessus de l'article L. 141-2 du code forestier.<br/>
<br/>
              12. Par suite, l'association requérante est fondée à demander l'annulation des dispositions de la dernière phrase du deuxième alinéa de l'article R. 141-38-4 du code forestier, introduit par le décret attaqué, qui sont divisibles des autres dispositions.<br/>
<br/>
              13. Il résulte de tout ce qui précède que l'association Forestiers du monde n'est fondée à demander l'annulation du décret attaqué et des décisions du 18 juillet 2018 par lesquelles le Premier ministre et le ministre de l'agriculture et de l'alimentation ont rejeté ses recours gracieux que dans la mesure précisée au point 12. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 1 000 euros à verser à l'association requérante au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le décret du 6 avril 2018 est annulé en tant qu'il introduit au code forestier un article R. 141-38-4 dont le deuxième alinéa comporte la phrase : " Ces prescriptions sont proportionnées afin de ne pas compromettre l'opération ".<br/>
<br/>
Article 2 : Les décisions du 18 juillet 2018 par lesquelles le Premier ministre et le ministre de l'agriculture et de l'alimentation ont implicitement refusé de retirer le même décret sont annulées dans la mesure dite à l'article 1er.<br/>
<br/>
Article 3 : L'Etat versera à l'association Les Forestiers du monde une somme de 1 000 euros au titre de l'article L.761-1 du code de justice administrative.<br/>
<br/>
		Article 4 : Le surplus des conclusions de la requête est rejeté.<br/>
<br/>
Article 5 : La présente décision sera notifiée à l'association Forestiers du monde, au ministre de l'agriculture et de l'alimentation et au Premier ministre. <br/>
		Copie en sera adressée à la ministre de la culture.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
