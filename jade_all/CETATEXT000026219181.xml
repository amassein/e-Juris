<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026219181</ID>
<ANCIEN_ID>JG_L_2012_07_000000345225</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/21/91/CETATEXT000026219181.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 23/07/2012, 345225, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-07-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>345225</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jean-Pierre Jouguelet</PRESIDENT>
<AVOCATS>SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR>M. Olivier Gariazzo</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:345225.20120723</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et les mémoires complémentaires, enregistrés les 22 décembre 2010, 22 mars et 29 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SAS Relais Fnac, agissant tant pour elle-même que venant aux droits et obligations des sociétés Relais Fnac SNC et Relais Fnac Lyon SA, dont le siège est 18, place Henri Bergson à Paris (75008) ; la SAS Relais Fnac demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08PA05034 du 20 octobre 2010 de la cour administrative d'appel de Paris, en tant qu'après avoir réformé le jugement n° 0311999/1-3 du 25 juillet 2008 du tribunal administratif de Paris ayant rejeté sa demande et condamné l'Etat à lui verser une indemnité correspondant à la différence entre la rémunération calculée sur la base d'un taux d'intérêt équivalent à la moitié du taux applicable aux obligations assimilables du Trésor et celle calculée sur le fondement du taux d'intérêt de 0,1 %, majorée des intérêts au taux légal à compter du 31 décembre 2002, eux-mêmes capitalisés à la date du 1er janvier 2004 et à chaque échéance annuelle à compter de cette date pour produire eux-mêmes intérêts, il a rejeté le surplus de ses conclusions tendant au versement d'une somme de 2 138 413 euros assortie des intérêts au taux légal à compter du 1er janvier 2008 et de leur capitalisation ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à l'intégralité ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 8 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la sixième directive 77/388/CEE du Conseil des Communautés européennes du 17 mai 1977 en matière d'harmonisation des législations des Etats membres relatives aux taxes sur le chiffre d'affaires ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et le premier protocole additionnel à cette convention ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu la loi n° 68-1250 du 31 décembre 1968 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Gariazzo, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Bouzidi, Bouhanna, avocat de la SAS Relais Fnac,<br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Bouzidi, Bouhanna, avocat de la SAS Relais Fnac ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux " ;<br/>
<br/>
              2. Considérant que pour demander l'annulation de l'arrêt qu'elle attaque, la SAS Relais Fnac, agissant tant pour elle-même que venant aux droits et obligations des sociétés Relais Fnac SNC et Relais Fnac Lyon SA, soutient que la cour administrative d'appel de Paris a entaché son arrêt d'une insuffisance de motivation ; qu'elle a commis une erreur de droit en jugeant que le montant et le paiement de la créance litigieuse née de l'insuffisante rémunération de la créance sur le Trésor résultant du 3 de l'article 271 A du code général des impôts n'étaient pas liquide et exigible avant la publication du décret du 13 février 2002 relatif au remboursement par anticipation des créances sur le Trésor ; que le remboursement progressif de la créance par le paiement des échéances annuelles et le paiement anticipé du solde constituaient l'émission de moyens de règlement interruptive de la prescription quadriennale ; que, par suite, en lui opposant l'exception de prescription quadriennale invoquée par le ministre du budget pour les sommes réclamées au titre des années 1993 à 1997, la cour a méconnu les dispositions des articles 1er à 3 de la loi du 31 décembre 1968 relative à la prescription quadriennale des créances sur l'Etat ; qu'en accueillant cette exception de prescription, la cour a méconnu les stipulations de l'article 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et celles de l'article 1er du premier protocole additionnel à cette convention ; qu'en jugeant que les dispositions transitoires de l'article 271 A du code général des impôts n'étaient pas contraires aux articles 17 et 18 de la sixième directive 77/388/CEE du 17 mai 1977 en ce qu'elles réduisaient les effets de la disposition nationale dérogatoire antérieure, alors que ces dispositions avaient eu pour effet de reporter le paiement d'une fraction de ses droits à déduction sur une période allant jusqu'à l'année 2002, la cour a commis une erreur de droit ; qu'en jugeant d'une part, que le remboursement différé des seules créances sur le Trésor supérieures au montant fixé au 5 de l'article 271 A du code général des impôts n'instituait pas une discrimination prohibée entre les redevables à la taxe sur la valeur ajoutée selon le montant de leurs créances et, d'autre part, que le taux de rémunération des créances sur le Trésor résultant du 3 de l'article 271 A de ce code n'instituait pas une discrimination entre ces créanciers et les autres créanciers de l'Etat non justifiée par une différence de situation objective, la cour a méconnu les stipulations combinées de l'article 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et de l'article 1er du premier protocole additionnel à cette convention ; qu'en évaluant l'indemnisation de la société requérante sur la base d'un taux d'intérêt équivalent à la moitié du taux applicable aux obligations assimilables du Trésor, sans retenir un taux d'un montant supérieur au taux du marché, et en refusant de faire droit à la demande de capitalisation du différentiel annuel d'intérêt résultant de l'insuffisante rémunération de sa créance sur le Trésor, la cour a entaché son arrêt d'une insuffisance de motivation et commis une erreur de droit ; qu'en retenant qu'elle ne pouvait justifier d'aucun préjudice en tant qu'elle agissait pour elle-même et pour les sociétés Relais Fnac Avignon, Angers, Bordeaux, Clermont-Ferrand, Dijon, Marseille, Montpellier, Mulhouse, Nancy, Nice, Nîmes, Pau, Reims, Rouen, Toulouse, Strasbourg, Toulon, Troyes et Lyon, dès lors qu'elle avait été remboursée avant l'année 2002, alors qu'il importait de préciser la date des remboursements afin de vérifier la date à laquelle la prescription, qu'elle a retenue, était acquise, la cour a commis une erreur de droit ; qu'en jugeant, s'agissant de la demande d'une somme de 300 000 euros en réparation de son préjudice complémentaire, qu'elle n'en justifiait pas, la cour administrative d'appel de Paris a dénaturé ses écritures, entaché sa décision d'une erreur de droit et d'une insuffisance de motivation et violé l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              3. Considérant qu'eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi qui sont dirigées contre l'arrêt attaqué en tant qu'il s'est prononcé sur les droits à indemnité de la société Relais Fnac SAS à raison des créances sur le Trésor détenues, entre le 1er janvier 1998 et leur remboursement, par les sociétés Relais Fnac Avignon SNC, Relais Fnac Angers SNC, Relais Fnac Bordeaux SNC, Relais Fnac Clermont-Ferrand SNC, Relais Fnac Dijon SNC, Relais Fnac Marseille SNC, Relais Fnac Montpellier SNC, Relais Fnac Mulhouse SNC, Relais Fnac Pau SNC, Relais Fnac Reims SNC, Relais Fnac Rouen SNC, Relais Fnac Strasbourg SNC, Relais Fnac Toulon, Relais Fnac Toulouse SNC, Relais Fnac Troyes SNC, Fnac Lyon SA ; qu'en revanche, s'agissant des autres conclusions dirigées contre l'arrêt attaqué, aucun des moyens soulevés n'est de nature à permettre l'admission de ces conclusions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Les conclusions du pourvoi de la SAS Relais Fnac qui sont dirigées contre l'arrêt attaqué en tant qu'il n'est pas prononcé sur les droits à indemnité de la société Relais Fnac SAS à raison des créances sur le Trésor détenues, entre le 1er janvier 1998 et leur remboursement, par les sociétés Relais Fnac Avignon SNC, Relais Fnac Angers SNC, Relais Fnac Bordeaux SNC, Relais Fnac Clermont-Ferrand SNC, Relais Fnac Dijon SNC, Relais Fnac Marseille SNC, Relais Fnac Montpellier SNC, Relais Fnac Mulhouse SNC, Relais Fnac Pau SNC, Relais Fnac Reims SNC, Relais Fnac Rouen SNC, Relais Fnac Strasbourg SNC, Relais Fnac Toulon, Relais Fnac Toulouse SNC, Relais Fnac Troyes SNC, Fnac Lyon SA, sont admises.<br/>
Article 2 : Le surplus des conclusions du pourvoi de la SAS Relais Fnac n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à la SAS Relais Fnac.<br/>
Copie en sera adressée pour information au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
