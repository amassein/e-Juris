<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029985926</ID>
<ANCIEN_ID>JG_L_2014_12_000000364616</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/98/59/CETATEXT000029985926.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 23/12/2014, 364616, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364616</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY ; SCP HEMERY, THOMAS-RAQUIN</AVOCATS>
<RAPPORTEUR>Mme Pauline Pannier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:364616.20141223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 364616, le pourvoi sommaire et le mémoire complémentaire, enregistrés les 18 décembre 2012 et 18 mars 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A...B...demeurant ... ; M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 12PA00788 du 18 octobre 2012 par lequel la cour administrative d'appel de Paris a, d'une part, annulé le jugement nos 0900298/1 et 1001281/1 du 2 décembre 2011 par lequel le tribunal administratif de Melun a rejeté la demande de la société NC Numéricable tendant à l'annulation de la décision du 19 septembre 2008 par laquelle le ministre du travail, des relations sociales, de la famille et de la solidarité, après avoir annulé la décision de l'inspectrice du travail de Bagneux du 4 avril 2008, a refusé d'autoriser son licenciement et, d'autre part, annulé la décision du ministre ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société NC Numéricable ;<br/>
<br/>
              3°) de mettre à la charge de la société NC Numéricable la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu 2°, sous le n° 364633, le pourvoi sommaire et le mémoire complémentaire, enregistrés les 19 décembre 2012 et 18 mars 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A...B...demeurant ... ; M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le même arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société NC Numéricable ;<br/>
<br/>
              3°) de mettre à la charge de la société NC Numéricable la somme de 6 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ainsi que la somme de 35 euros correspondant à la contribution à l'aide juridique ;<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Pannier, auditeur,  <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. B...et à la SCP Hémery, Thomas-Raquin, avocat de la société NC Numéricable ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que les conclusions de M.B..., enregistrées sous deux numéros distincts, constituent un pourvoi unique, sur lequel il y a lieu de statuer par une seule décision ;<br/>
<br/>
              2. Considérant qu'en vertu des dispositions du code du travail, les salariés légalement investis de fonctions représentatives bénéficient, dans l'intérêt de l'ensemble des salariés qu'ils représentent, d'une protection exceptionnelle ; que, lorsque le licenciement d'un de ces salariés est envisagé, ce licenciement ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale de l'intéressé ; que, dans le cas où la demande de licenciement est motivée par un comportement fautif, il appartient à l'inspecteur du travail, et le cas échéant au ministre, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si les faits reprochés au salarié sont d'une gravité suffisante pour justifier son licenciement, compte tenu de l'ensemble des règles applicables au contrat de travail de l'intéressé et des exigences propres à l'exécution normale du mandat dont il est investi ;<br/>
<br/>
              3. Considérant qu'en l'absence de mention contractuelle du lieu de travail d'un salarié, la modification de ce lieu de travail constitue un simple changement des conditions de travail, dont le refus par le salarié est susceptible de caractériser une faute de nature à justifier son licenciement, lorsque le nouveau lieu de travail demeure à l'intérieur d'un même secteur géographique, lequel s'apprécie, eu égard à la nature de l'emploi de l'intéressé, de façon objective, en fonction de la distance entre l'ancien et le nouveau lieu de travail ainsi que des moyens de transport disponibles ; qu'en revanche, sous réserve de la mention au contrat de travail d'une clause de mobilité, tout déplacement du lieu de travail dans un secteur géographique différent du secteur initial constitue une modification du contrat de travail ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société NC Numéricable a demandé, le 25 janvier 2008, l'autorisation de licencier pour faute M.B..., chef de secteur rattaché au service distribution et ventes indirectes de la société, délégué syndical et candidat non élu aux élections du comité d'entreprise, auquel il était reproché de n'avoir effectué aucun travail depuis que l'entreprise, le 8 juin 2007, l'avait informé que son secteur géographique d'affectation, situé en Ile-de-France, avait été changé pour un secteur regroupant l'Yonne, la Côte-d'Or, la Nièvre, le Cher et le Doubs, et d'avoir eu de nombreuses absences injustifiées et prolongées depuis cette période ; que, par une décision du 19 septembre 2008, le ministre chargé du travail a, d'une part, annulé la décision du 4 avril 2008 par laquelle l'inspectrice du travail de Bagneux avait autorisé le licenciement de M. B...et, d'autre part, refusé d'autoriser le licenciement de l'intéressé au motif que les faits reprochés ne présentaient pas un caractère fautif dès lors que le changement de zone de prospection devait s'analyser comme une modification de son contrat de travail qui ne pouvait être imposée au salarié, et que la mesure de licenciement envisagée n'était pas dénuée de tout lien avec les mandats détenus par le salarié ; que le tribunal administratif de Melun, saisi par la société NC Numéricable, a confirmé cette décision par son jugement du 2 décembre 2011 ; que, par l'arrêt contre lequel se pourvoit M.B..., la cour administrative d'appel de Paris a annulé le jugement du tribunal administratif et la décision du ministre refusant d'autoriser le licenciement ; <br/>
<br/>
              5. Considérant que, pour juger que le changement de zone de prospection imposé à M. B...ne constituait pas une modification du contrat de travail de l'intéressé mais un simple changement dans ses conditions de travail dont le refus constituait une faute de nature à justifier son licenciement, la cour a retenu qu'aucune mention du secteur géographique de travail ne figurait dans son contrat de travail ni dans les avenants à celui-ci, et que, eu égard à la nature même de l'emploi de M.B..., le changement de secteur de prospection n'impliquait pas de changement de résidence de l'intéressé ni d'aggravation dans ses conditions de travail ; qu'en se fondant sur de tels éléments, alors qu'elle devait seulement rechercher si le nouveau lieu de travail de l'intéressé était situé dans un secteur géographique différent de l'ancien, la cour a commis une erreur de droit ; que, par suite et sans qu'il besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ; <br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M.B..., qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. B...au titre de ces mêmes dispositions et de celles de l'article R. 761-1 du même code  ; <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 18 octobre 2012 est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Paris.<br/>
Article 3 : Le surplus des conclusions du pourvoi de M. B...est rejeté.<br/>
Article 4 : Les conclusions de la société NC Numéricable présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. A...B...et à la société NC Numéricable.<br/>
Copie en sera adressée au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-07-01-04-02 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. CONDITIONS DE FOND DE L'AUTORISATION OU DU REFUS D'AUTORISATION. LICENCIEMENT POUR FAUTE. - LICENCIEMENT DISCIPLINAIRE EN CAS DE REFUS D'UNE MODIFICATION DES CONDITIONS DE TRAVAIL [RJ1] - MODIFICATION DU LIEU DE TRAVAIL À L'INTÉRIEUR DU MÊME SECTEUR GÉOGRAPHIQUE - NOTION DE SECTEUR GÉOGRAPHIQUE - APPRÉCIATION OBJECTIVE EN FONCTION DE LA DISTANCE ENTRE L'ANCIEN ET LE NOUVEAU LIEU DE TRAVAIL [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-07-01-04-035-04 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. CONDITIONS DE FOND DE L'AUTORISATION OU DU REFUS D'AUTORISATION. MOTIFS AUTRES QUE LA FAUTE OU LA SITUATION ÉCONOMIQUE. DIVERS. - MODIFICATION DU LIEU DE TRAVAIL ENTRAÎNANT UN CHANGEMENT DE SECTEUR GÉOGRAPHIQUE - MODIFICATION DU CONTRAT DE TRAVAIL DONT LE REFUS EST INSUSCEPTIBLE DE FONDER UN LICENCIEMENT DISCIPLINAIRE [RJ1] - APPRÉCIATION OBJECTIVE DU SECTEUR GÉOGRAPHIQUE [RJ2].
</SCT>
<ANA ID="9A"> 66-07-01-04-02 En l'absence de mention contractuelle du lieu de travail d'un salarié, la modification de ce lieu de travail constitue un simple changement des conditions de travail, dont le refus par le salarié est susceptible de caractériser une faute de nature à justifier son licenciement, lorsque le nouveau lieu de travail demeure à l'intérieur d'un même secteur géographique, lequel s'apprécie, eu égard à la nature de l'emploi de l'intéressé, de façon objective, en fonction de la distance entre l'ancien et le nouveau lieu de travail ainsi que des moyens de transport disponibles.... ,,En revanche, sous réserve de la mention au contrat de travail d'une clause de mobilité, tout déplacement du lieu de travail dans un secteur géographique différent du secteur initial constitue une modification du contrat de travail.</ANA>
<ANA ID="9B"> 66-07-01-04-035-04 En l'absence de mention contractuelle du lieu de travail d'un salarié, la modification de ce lieu de travail constitue un simple changement des conditions de travail, dont le refus par le salarié est susceptible de caractériser une faute de nature à justifier son licenciement, lorsque le nouveau lieu de travail demeure à l'intérieur d'un même secteur géographique, lequel s'apprécie, eu égard à la nature de l'emploi de l'intéressé, de façon objective, en fonction de la distance entre l'ancien et le nouveau lieu de travail ainsi que des moyens de transport disponibles.... ,,En revanche, sous réserve de la mention au contrat de travail d'une clause de mobilité, tout déplacement du lieu de travail dans un secteur géographique différent du secteur initial constitue une modification du contrat de travail.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf, sur la distinction entre la modification des conditions de travail qui peut justifier un licenciement disciplinaire en cas de refus et la modification du contrat de travail qui ne le peut pas, CE, 10 mars 1997, M. Vincent, n° 170114, p. 76., ,[RJ2]Ab. jur., en ce qui concerne les critères propres à la situation personnelle du salarié, CE, 27 décembre 2009, Société Autogrill Côte France, n° 301563, T. 979.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
