<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042545494</ID>
<ANCIEN_ID>JG_L_2020_11_000000443200</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/54/54/CETATEXT000042545494.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 20/11/2020, 443200, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>443200</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Cécile Viton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Céline  Guibé</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:443200.20201120</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée (SAS) Novartis Pharma a demandé au tribunal administratif de Montreuil de prononcer le remboursement d'un crédit de contribution sur la valeur ajoutée des entreprises acquittée au titre de l'année 2012 à hauteur de 1 276 454 euros et au titre de l'année 2013 à hauteur de 1 721 616 euros. Par un jugement nos 1505726, 1505728 du 22 juin 2017, le tribunal administratif de Montreuil a rejeté ses demandes.<br/>
<br/>
              Par un arrêt n° 17VE02790 du 28 janvier 2020, la cour administrative d'appel de Versailles a rejeté l'appel formé par la société Novartis Pharma contre ce jugement.<br/>
<br/>
              Par un pourvoi, enregistré le 21 août 2020 au secrétariat du contentieux du Conseil d'Etat, la société Novartis Pharma demande au Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner, avant dire droit, une mesure d'instruction sous la forme d'un avis technique sur le fondement de l'article R. 625-2 du code de justice administrative ou d'observations écrites sur le fondement de l'article R. 625-3 du même code, en invitant le Comité économique des produits de santé (CEPS) à produire un tel avis ou de telles observations sur la distinction entre les " remises conventionnelles " ou " remises produits " visées à l'article L. 162-18 du code de la sécurité sociale et la " contribution ONDAM " prévue à l'article <br/>
L. 138-10 du même code, y compris lorsque cette contribution est versée sous la forme de " remises quantitatives annuelles ", ainsi que sur la nature de " réductions de prix " que sont ces " remises conventionnelles " ou " remises produits " ;<br/>
<br/>
              2°) d'annuler l'arrêt du 21 août 2020 ;<br/>
<br/>
              3°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              4°) subsidiairement, de surseoir à statuer et renvoyer à la Cour de justice de l'Union européenne des questions préjudicielles relatives à l'interprétation des dispositions des articles 73 et 90 de la directive 2006/112/CE du Conseil du 28 novembre 2006 relative au système commun de taxe sur la valeur ajoutée ;<br/>
<br/>
              5°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par un mémoire distinct, enregistré le 24 août 2020, la société Novartis Pharma demande au Conseil d'Etat, à l'appui de ce pourvoi, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du 1 du I de l'article 1586 sexies du code général des impôts, telles qu'interprétées par le Conseil d'Etat. Elle soutient que ces dispositions, applicables au litige, méconnaissent les principes d'égalité devant la loi et d'égalité devant les charges publiques respectivement garantis par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son protocole additionnel ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des impôts ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Viton, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Céline Guibé, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la société Novartis Pharma ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Novartis Pharma a sollicité la restitution partielle des cotisations de contribution sur la valeur ajoutée des entreprises auxquelles elle a été assujettie au titre des années 2012 et 2013, correspondant à la déduction, pour le calcul de sa valeur ajoutée, des remises conventionnelles mentionnées à l'article L. 162-18 du code de la sécurité sociale. L'administration fiscale a rejeté ces demandes. La société Novartis Pharma se pourvoit en cassation contre l'arrêt du 28 janvier 2020 par lequel la cour administrative d'appel de Versailles a rejeté son appel contre le jugement du 22 juin 2017 du tribunal administratif de Montreuil rejetant sa demande de restitution partielle.<br/>
<br/>
              2. Aux termes du I de l'article 1586 sexies du code général des impôts, dans sa rédaction applicable aux années d'imposition en litige : " Pour la généralité des entreprises, à l'exception des entreprises visées aux II à VI : / 1. Le chiffre d'affaires est égal à la somme : / - des ventes de produits fabriqués, prestations de services et marchandises ; / - des redevances pour concessions, brevets, licences, marques, procédés, logiciels, droits et valeurs similaires ; / -des plus-values de cession d'immobilisations corporelles et incorporelles, lorsqu'elles se rapportent à une activité normale et courante ; / - des refacturations de frais inscrites au compte de transfert de charges. / 2. Le chiffre d'affaires des titulaires de bénéfices non commerciaux qui n'exercent pas l'option mentionnée à l'article 93 A s'entend du montant hors taxes des honoraires ou recettes encaissés en leur nom, diminué des rétrocessions, ainsi que des gains divers. / 3. Le chiffre d'affaires des personnes dont les revenus imposables à l'impôt sur le revenu relèvent de la catégorie des revenus fonciers définie à l'article 14 comprend les recettes brutes au sens de l'article 29. / 4. La valeur ajoutée est égale à la différence entre : / a) D'une part, le chiffre d'affaires tel qu'il est défini au 1, majoré : / - des autres produits de gestion courante (...) ; / - de la production immobilisée, à hauteur des seules charges qui ont concouru à sa formation et qui figurent parmi les charges déductibles de la valeur ajoutée ; (...) / - des subventions d'exploitation ; / - de la variation positive des stocks ; / - des transferts de charges déductibles de la valeur ajoutée, autres que ceux pris en compte dans le chiffre d'affaires ; / b) Et, d'autre part : / - les achats stockés de matières premières et autres approvisionnements, les achats d'études et prestations de services, les achats de matériel, équipements et travaux, les achats non stockés de matières et fournitures, les achats de marchandises et les frais accessoires d'achat ; / - diminués des rabais, remises et ristournes obtenus sur achats ; / - la variation négative des stocks ; / - les services extérieurs diminués des rabais, remises et ristournes obtenus (...) ; / - les taxes sur le chiffre d'affaires et assimilées, les contributions indirectes, la taxe intérieure de consommation sur les produits énergétiques ; - les autres charges de gestion courante (...) / 5. La valeur ajoutée des contribuables mentionnés au 2 est constituée par l'excédent du chiffre d'affaires défini au 2 sur les dépenses de même nature que les charges admises en déduction de la valeur ajoutée en application du 4, à l'exception de la taxe sur la valeur ajoutée déductible ou décaissée. / 6. La valeur ajoutée des contribuables mentionnés au 3 est égale à l'excédent du chiffre d'affaires défini au 3 diminué des charges de la propriété énumérées à l'article 31, à l'exception des charges énumérées aux c et d du 1° du I du même article 31 ".<br/>
<br/>
              3. Il résulte des dispositions de l'article L. 162-18 du code de la sécurité sociale que les entreprises qui exploitent une ou plusieurs spécialités pharmaceutiques remboursables peuvent s'engager à faire bénéficier l'assurance maladie de remises sur tout ou partie du chiffre d'affaires de ces spécialités réalisé en France. En particulier, ces entreprises peuvent conclure une convention avec le comité économique des produits de santé, qui comporte notamment des engagements portant sur leur chiffre d'affaires et dont le non-respect peut entraîner le versement de telles remises. Un tel conventionnement leur permet de ne pas être redevables de la contribution prévue par l'article L. 138-10 du même code lorsque leur chiffre d'affaires de l'année civile s'est accru, par rapport au chiffre d'affaires réalisé l'année précédente, d'un pourcentage excédant le taux de progression de l'objectif national de dépenses d'assurance maladie. Dans ces conditions, ces remises, qui sont directement versées à l'assurance maladie, ne constituent pas des avantages tarifaires consentis par les entreprises pour fidéliser leur clientèle, mais un mécanisme visant à réduire les dépenses d'assurance maladie. Par suite, elles ne sauraient venir en diminution des produits comptabilisés pour la détermination de la valeur ajoutée définie par l'article 1586 sexies du code général des impôts, interprétés à la lumière du compte 709 " rabais, remises, ristournes " du plan comptable général.<br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              4. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              5. La société Novartis Pharma soutient que les dispositions du 1 du I de l'article 1586 sexies du code général des impôts, telles qu'elles sont interprétées de manière constante par le Conseil d'Etat, statuant au contentieux, méconnaissent les principes d'égalité devant la loi et d'égalité devant les charges publiques respectivement garantis par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen, en ce que la valeur ajoutée prise en compte pour le calcul de la cotisation sur la valeur ajoutée des entreprises inclut les remises conventionnelles prévues à l'article L. 162-18 du code de la sécurité sociale.<br/>
<br/>
              6. Aux termes de l'article 6 de la Déclaration des droits de l'homme et du citoyen de 1789 : " La loi (...) doit être la même pour tous, soit qu'elle protège, soit qu'elle punisse ". Le principe d'égalité ne s'oppose ni à ce que le législateur règle de façon différente des situations différentes, ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général, pourvu que, dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la loi qui l'établit. Aux termes de l'article 13 de cette Déclaration : " Pour l'entretien de la force publique, et pour les dépenses d'administration, une contribution commune est indispensable : elle doit être également répartie entre tous les citoyens, en raison de leurs facultés ". En vertu de l'article 34 de la Constitution, il appartient au législateur de déterminer, dans le respect des principes constitutionnels et compte tenu des caractéristiques de chaque impôt, les règles selon lesquelles doivent être appréciées les facultés contributives. En particulier, pour assurer le respect du principe d'égalité, il doit fonder son appréciation sur des critères objectifs et rationnels en fonction des buts qu'il se propose. Cette appréciation ne doit cependant pas entraîner de rupture caractérisée de l'égalité devant les charges publiques.<br/>
<br/>
              7. En premier lieu, la société requérante fait valoir qu'une discrimination à rebours résulte du fait que les dispositions du 1 du I de l'article 1586 sexies du code général des impôts, qui définissent l'assiette de la cotisation sur la valeur ajoutée des entreprises, reposent sur une définition du chiffre d'affaires différente de celle qui est retenue pour déterminer l'assiette de la taxe sur la valeur ajoutée, régie par le droit de l'Union européenne. Toutefois, la base d'imposition de la contribution sur la valeur ajoutée des entreprises est distincte de celle de la taxe sur la valeur ajoutée et uniquement régie par la loi fiscale interne. La circonstance que, par un arrêt du 20 décembre 2017 Finanzamt Bingen-Alzey c/ Boehringer Ingelheim Pharma GmbH et Co. KG (aff. C-462/16), la Cour de justice de l'Union européenne ait dit pour droit que " l'article 90, paragraphe 1, de la directive 2006/112/CE du Conseil du 28 novembre 2006 relative au système commun de taxe sur la valeur ajoutée, doit être interprété en ce sens que la remise accordée, en vertu d'une loi nationale, par une entreprise pharmaceutique à une entreprise d'assurance maladie privée entraîne, au sens de cet article, une réduction de la base d'imposition en faveur de cette entreprise pharmaceutique, lorsque des livraisons de produits pharmaceutiques sont effectuées par l'intermédiaire de grossistes à des pharmacies qui effectuent ces livraisons à des personnes couvertes par une assurance maladie privée, laquelle rembourse à ses assurés le prix d'achat des produits pharmaceutiques ", n'est, en tout état de cause, pas de nature à créer une différence de traitement entre sociétés se trouvant, au regard de l'objet de la loi fiscale, dans la même situation.<br/>
<br/>
              8. En second lieu, la société requérante soutient que les dispositions du 1 du I de l'article 1586 sexies du code général des impôts méconnaissent le principe d'égalité devant les charges publiques garanti par l'article 13 de la Déclaration des droits de l'homme et du citoyen, dans la mesure où elles permettent que la valeur ajoutée prise en compte pour le calcul de la cotisation sur la valeur ajoutée des entreprises inclue les remises conventionnelles versées à l'assurance maladie, alors que les contribuables ne disposent pas de ces sommes. Toutefois, les remises conventionnelles constituent, non pas, comme le soutient la société, un revenu soumis à imposition alors que le contribuable n'en avait pas la disposition, mais des charges que doivent acquitter les contribuables concernés et dont le législateur a décidé qu'elles ne seraient pas déductibles de la valeur ajoutée.<br/>
<br/>
              9. Il résulte de tout ce qui précède que la question prioritaire de constitutionnalité soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Par suite, il n'y a pas lieu de la renvoyer au Conseil constitutionnel.<br/>
<br/>
              Sur les autres moyens du pourvoi :<br/>
<br/>
              10. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              11. Pour demander l'annulation de l'arrêt qu'elle attaque, la société Novartis Pharma soutient que la cour administrative d'appel de Versailles :<br/>
              - l'a entaché d'une insuffisance de motivation en ne répondant pas à l'ensemble de ses moyens et en n'expliquant pas en quoi les remises conventionnelles de l'article L. 162-18 du code de la sécurité sociale ne peuvent être regardées comme des " réductions sur ventes " au sens des dispositions de l'article 1586 sexies du code général des impôts alors d'ailleurs que ces dispositions ne font pas mention de " réductions sur ventes " ;<br/>
              - a commis une erreur de droit en appliquant les dispositions de l'article 1647 B du code général des impôts dans sa version antérieure à la loi du 30 décembre 2009 de finances pour 2010, qui ne sont pas applicables au litige ;<br/>
              - a commis une erreur de droit en assimilant à tort les remises conventionnelles aux " remises quantitatives de fin d'année " pour en déduire qu'elles ne peuvent pas venir en diminution des produits comptabilisés pour la détermination du chiffre d'affaires servant de base au calcul de la cotisation sur la valeur ajoutée des entreprises ;<br/>
              - a commis une erreur de droit en relevant que les remises conventionnelles sont dues par les entreprises pharmaceutiques en cas de non-respect des engagements souscrits à propos des modalités d'utilisation de ces produits ;<br/>
              - a commis une erreur de droit en se fondant seulement sur les normes comptables, sans rechercher si, eu égard à son modèle économique, les remises conventionnelles devaient être déduites du chiffre d'affaires servant de base au calcul de la cotisation sur la valeur ajoutée des entreprises ;<br/>
              - a commis une erreur de droit et une erreur de qualification juridique des faits en retenant que les remises conventionnelles de l'article L. 162-18 du code de la sécurité sociale ne constituaient pas des avantages tarifaires consentis par les entreprises pharmaceutiques pour fidéliser leur clientèle, au motif que ce mécanisme visait à réduire les dépenses d'assurance maladie, et en en déduisant que ces remises ne pouvaient pas venir, en tant que " rabais, remises, ristournes ", en diminution du chiffre d'affaires servant de base au calcul de la cotisation sur la valeur ajoutée des entreprises ;<br/>
              - a commis une erreur de droit et une erreur de qualification juridique des faits en écartant le moyen tiré de la méconnaissance de l'article 1er du protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, fondé sur le caractère confiscatoire de la cotisation sur la valeur ajoutée des entreprises, dont l'assiette est calculée en incluant les remises conventionnelles, qui viennent en diminution du chiffre d'affaires.<br/>
<br/>
              12. Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société Novartis Pharma.<br/>
Article 2 : Le pourvoi de la société Novartis Pharma n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à la société Novartis Pharma et au ministre de l'économie, des finances et de la relance.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre et au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
