<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032103910</ID>
<ANCIEN_ID>JG_L_2016_02_000000382688</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/10/39/CETATEXT000032103910.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 24/02/2016, 382688</TITRE>
<DATE_DEC>2016-02-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382688</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Marc Thoumelou</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:382688.20160224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Nantes d'annuler la décision du 25 octobre 2011 par laquelle la directrice de l'agence de Pôle emploi de Saumur Europe, confirmant sur recours gracieux sa décision du 10 octobre 2011, l'a radié de la liste des demandeurs d'emploi pour deux mois à compter du 8 septembre 2011. Par un jugement n° 1110948 du 18 avril 2014, le tribunal administratif de Nantes a rejeté sa demande.<br/>
<br/>
              Par un pourvoi, enregistré le 16 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à son avocat, la SCP Monod, Colin, Stoclet, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Thoumelou, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de M.B... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B...ne s'est pas présenté, le 5 septembre 2011, à un rendez-vous prévu dans le cadre d'une action d'aide à la recherche d'emploi dont il avait été informé le 11 août précédent ; que, par une décision du 10 octobre 2011, la directrice de l'agence de Pôle emploi de Saumur Europe a, pour ce motif, procédé à sa radiation de la liste des demandeurs d'emploi pour une durée de deux mois à compter du 8 septembre 2011 ; que cette radiation a été confirmée par cette même directrice par une décision du 25 octobre 2011, se substituant à sa précédente décision, à la suite du recours administratif préalable obligatoire formé par M.B... ; que ce dernier a saisi le tribunal administratif de Nantes d'un recours de plein contentieux contre cette radiation, qui revêt le caractère d'une sanction que l'administration inflige à un administré ; <br/>
<br/>
              2. Considérant que le 3° de l'article L. 5412-1 du code du travail prévoit notamment la radiation de la liste des demandeurs d'emploi de la personne qui, " sans motif légitime : (...) / b) Refuse de suivre une action de formation ou d'aide à la recherche d'emploi proposée par l'un des services ou organismes mentionnés à l'article L. 5311-2 et s'inscrivant dans le cadre du projet personnalisé d'accès à l'emploi ; / c) Refuse de répondre à toute convocation des services et organismes mentionnés à l'article L. 5311-2 ou mandatés par ces services et organismes (...) " ; que l'article R. 5412-1 du même code donne compétence au directeur régional de Pôle emploi pour procéder à cette radiation dont la durée est, selon le 1° de l'article R. 5412-5 de ce code, de quinze jours lorsque sont constatés pour la première fois les manquements mentionnés au b) du 3° de l'article L. 5412-1, pouvant, en cas de manquements répétés, être portée à une durée comprise entre un et six mois consécutifs, et, selon le 2° du même article R. 5412-5, de deux mois lorsque sont constatés pour la première fois les manquements mentionnés au c) du 3° de l'article L. 5412-1, pouvant, en cas de manquements répétés, être portée à une durée comprise entre deux et six mois consécutifs ;<br/>
<br/>
              3. Considérant, en premier lieu, qu'il résulte des dispositions du 3° de l'article L. 5412-1 du code du travail que le refus, sans motif légitime, de répondre à une convocation d'un organisme mandaté par Pôle emploi relève du c) de ce 3°, quand bien même la convocation avait pour objet le suivi d'une action de formation ou d'aide à la recherche d'emploi ; que, par suite, le tribunal administratif de Nantes n'a pas commis d'erreur de droit en jugeant que le défaut de présentation de M.B..., sans motif légitime, au rendez-vous de l'action d'aide à la recherche d'emploi auquel il avait été régulièrement convoqué relevait, à la différence d'un refus de suivre une telle action, du c du 3° de l'article L. 5412-1 du code du travail et, par suite, du 2° de l'article R. 5412-5 du même code, prévoyant, en cas de manquement constaté pour la première fois, une radiation de la liste des demandeurs d'emploi pour une durée de deux mois ;  <br/>
<br/>
              4. Considérant, en deuxième lieu, qu'il ressort des écritures de M. B...devant les juges du fond que celui-ci invoquait la panne de son scooter pour justifier son absence le 5 septembre 2011 au premier rendez-vous de l'action d'aide à la recherche d'emploi auquel il avait été convoqué ; que le tribunal n'a pas dénaturé les pièces du dossier qui lui était soumis en estimant que le requérant se bornait à alléguer, sans l'établir, la panne de son véhicule et n'apportait aucun élément de nature à établir l'absence de moyen de locomotion lui permettant de se présenter au rendez-vous auquel il était convoqué, pour suivre une action offerte par un organisme situé à quinze kilomètres de son domicile ; que le tribunal, qui a suffisamment motivé son jugement, en a exactement déduit que M. B...ne justifiait pas d'un motif légitime au sens du 3° de l'article L. 5412-1 cité ci-dessus ;<br/>
<br/>
              5. Considérant, en dernier lieu, que le moyen invoqué par M. B... et tiré de la disproportion de la sanction infligée n'a pas été soulevé devant le tribunal administratif de Nantes ; qu'il n'est pas d'ordre public ; que, par suite, il ne peut être utilement invoqué devant le juge de cassation ; qu'au surplus, la directrice de l'agence de Pôle emploi de Saumur Europe s'est bornée à faire application au manquement constaté de la sanction prévue par l'article R. 5412-5, dont les dispositions assurent la modulation des sanctions applicables en fonction de la gravité du comportement réprimé ;  <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que M. B...n'est pas fondé à demander l'annulation du jugement du tribunal administratif de Nantes qu'il attaque ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas partie à la présente instance ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et à Pôle emploi.<br/>
Copie en sera adressée à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-10-02 TRAVAIL ET EMPLOI. POLITIQUES DE L'EMPLOI. INDEMNISATION DES TRAVAILLEURS PRIVÉS D'EMPLOI. - SANCTION DE RADIATION DE LA LISTE DES DEMANDEURS D'EMPLOI [RJ1] - MANQUEMENTS JUSTIFIANT LA SANCTION - RADIATION DE DEUX MOIS EN CAS DE REFUS DE RÉPONDRE À UNE CONVOCATION (C DU 3° DE L'ART. L. 5412-1 DU CODE DU TRAVAIL) - INCLUSION - REFUS DE RÉPONDRE À UNE CONVOCATION RELATIVE À UNE ACTION DE FORMATION OU D'AIDE À LA RECHERCHE D'EMPLOI.
</SCT>
<ANA ID="9A"> 66-10-02 L'article L. 5412-1 du code du travail, qui prévoit la sanction de radiation de la liste des demandeurs d'emploi, distingue le refus de suivre une action de formation ou d'aide à la recherche d'emploi (b du 3° de cet article), qui peut donner lieu à une radiation de quinze jours, et le refus de répondre à une convocation (c du 3° de cet article), qui peut donner lieu à une radiation de deux mois. Le refus, sans motif légitime, de répondre à une convocation d'un organisme mandaté par Pôle emploi relève du c de ce 3°, quand bien même la convocation avait pour objet le suivi d'une action de formation ou d'aide à la recherche d'emploi.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf., sur la nature de sanction administrative, décision du même jour CE, 24 février 2016, Mme,, n° 378257, à mentionner aux tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
