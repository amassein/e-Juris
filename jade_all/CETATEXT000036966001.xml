<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036966001</ID>
<ANCIEN_ID>JG_L_2018_05_000000412964</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/96/60/CETATEXT000036966001.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 30/05/2018, 412964, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-05-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412964</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>Mme Céline  Guibé</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:412964.20180530</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société anonyme (SA) HighCo a demandé au tribunal administratif de Marseille de prononcer une réduction de la cotisation d'impôt sur les sociétés à laquelle elle a été assujettie au titre de l'exercice clos en 2010. Par un jugement n° 1305190 du 16 juin 2015, le tribunal administratif de Marseille a fait droit à sa demande.<br/>
<br/>
              Par un arrêt n° 15MA04108 du 1er juin 2017, la cour administrative d'appel de Marseille a, sur appel du ministre des finances et des comptes publics, annulé ce jugement et remis l'imposition en litige à la charge de la SA HighCo.<br/>
<br/>
              Par un pourvoi sommaire et deux mémoires complémentaires, enregistrés les 1er août 2017, 2 novembre 2017 et 28 mars 2018 au secrétariat du contentieux du Conseil d'Etat, la SA HighCo demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du ministre des finances et des comptes publics ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 7000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Par un mémoire distinct, enregistré le 28 mars 2018, la SA HighCo a demandé au Conseil d'Etat, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil constitutionnel une question prioritaire de constitutionnalité relative à la conformité aux droits et libertés garantis par la Constitution du a septies) du I de l'article 219 du code général des impôts.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son article 61-1 ;<br/>
              - le Traité sur le fonctionnement de l'Union européenne ;<br/>
              - la directive 2009/133/CE du Conseil du 19 octobre 2009 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - la loi n° 2010-1657 du 29 décembre 2010 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Céline Guibé, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de la société SA HighCo.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte des dispositions de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsque le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution est soulevé à l'occasion d'une instance devant le Conseil d'Etat, le Conseil constitutionnel est saisi de cette question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et qu'elle soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. En vertu du a septies du I de l'article 219 du code général des impôts, dans sa rédaction issue de l'article 13 de la loi du 29 décembre 2010 de finances pour 2011 : " Lorsqu'il existe des liens de dépendance entre l'entreprise cédante et l'entreprise cessionnaire au sens du 12 de l'article 39, l'imposition des plus-values et moins-values de cession de titres de participation définis au dix-huitième alinéa du 5° du 1 de l'article 39, autres que ceux mentionnés au a sexies-0 bis du présent article, et détenus depuis moins de deux ans, intervient à la première des dates suivantes : / a) La date à laquelle l'entreprise cédante cesse d'être soumise à l'impôt sur les sociétés ou est absorbée par une entreprise qui, à l'issue de l'absorption, n'est pas liée à l'entreprise détenant les titres cédés ; / b) La date à laquelle les titres cédés cessent d'être détenus par une entreprise liée à l'entreprise cédante, à l'exception du cas où la société dont les titres ont été cédés a été absorbée par une autre entreprise liée ou qui le devient à cette occasion et pour toute la période où elle demeure liée ; / c) La date correspondant à l'expiration d'un délai de deux ans, décompté à partir du jour où l'entreprise cédante a acquis les titres. / L'imposition est établie au nom de l'entreprise cédante ou, en cas d'absorption dans des conditions autres que celles mentionnées au a, de l'entreprise absorbante, selon le régime de plus-value ou moins-value qui aurait été applicable si l'entreprise avait cédé les titres à cette date et, le cas échéant, les avait détenus depuis la date d'acquisition par l'entreprise absorbée. / Toutefois, le présent a septies ne s'applique aux plus-values que si l'entreprise joint à sa déclaration de résultat au titre de chaque exercice concerné un état conforme au modèle fourni par l'administration, faisant apparaître les éléments nécessaires au calcul des plus-values et ceux relatifs à l'identification de l'entreprise qui détient les titres, explicitant les liens de dépendance qui les unissent. ". <br/>
<br/>
              3. En premier lieu, la société requérante soutient que les dispositions contestées portent atteinte au principe d'égalité devant les charges publiques en ne ménageant pas la possibilité pour le contribuable d'apporter la preuve que la cession de titres de participation qu'il détient depuis moins de deux ans, à l'occasion de laquelle est constatée une moins-value, ne poursuit pas un but exclusivement fiscal. <br/>
<br/>
              4. Il résulte des travaux préparatoires de la loi du 29 décembre 2010 que les dispositions du a septies du I de l'article 219 du code général des impôts ont pour objet de faire obstacle à une pratique d'optimisation fiscale consistant à céder à une filiale ou à une société soeur, dans les deux années de leur acquisition, des titres de participation ayant normalement vocation à être détenus sur le long terme, afin de constater des moins-values à court terme déductibles des résultats imposables à l'impôt sur les sociétés. A cette fin, elles soumettent au régime des plus-values et moins-values à long terme l'ensemble des cessions réalisées entre des sociétés liées, y compris celles qui interviennent dans un délai de deux ans après l'acquisition des titres, sous réserve de la survenance des événements mentionnés au a et au b de cet article. En adoptant les dispositions contestées, qui ne peuvent être regardées comme instituant une présomption de fraude ou d'évasion fiscale, le législateur a retenu des critères objectifs et rationnels en fonction du but poursuivi dès lors que la cession, dans les deux ans de leur acquisition, de titres de participation ayant subi une dépréciation était effectivement susceptible de caractériser une pratique d'optimisation fiscale au sein des groupes de sociétés. Par suite, le grief tiré de la méconnaissance du principe d'égalité devant les charges publiques ne présente pas un caractère sérieux.<br/>
<br/>
              5. En second lieu, la société requérante soutient que les dispositions contestées méconnaissent la liberté d'établissement garantie par l'article 49 du traité sur le fonctionnement de l'Union européenne ainsi que les objectifs de la directive 2009/133/CE du Conseil du 19 octobre 2009 concernant le régime fiscal commun applicable aux fusions, scissions, scissions partielles, apports d'actifs et échanges d'actions intéressant des sociétés d'États membres différents et qu'elles ne peuvent dès lors être légalement appliquées aux opérations de fusion-absorption entre des sociétés d'Etats membres différents, tandis qu'elles resteraient applicables aux opérations de fusion-absorption entre sociétés établies en France. Elle fait valoir qu'il en résulte une différence de traitement contraire aux principes d'égalité devant la loi et devant les charges publiques. <br/>
<br/>
              6. Lorsqu'est invoquée, à l'appui d'une question prioritaire de constitutionnalité, l'incompatibilité d'une disposition législative avec le droit de l'Union européenne, dont il résulterait une discrimination à rebours contraire aux principes d'égalité devant la loi et devant les charges publiques, il appartient au juge, pour apprécier le caractère sérieux de la question soulevée, d'examiner la compatibilité de la disposition nationale contestée avec celles du droit de l'Union européenne. <br/>
<br/>
              7. D'une part, si les dispositions contestées instituent une différence de traitement entre les cessions de titres de participation détenus depuis moins de deux ans intervenant entre des sociétés liées et celles qui interviennent entre des sociétés non liées, elles ne constituent pas une entrave à la liberté d'établissement protégée par l'article 49 du traité sur le fonctionnement de l'Union européenne en l'absence de toute différence de traitement entre les opérations conclues entre sociétés françaises et les opérations conclues entre sociétés d'Etats membres différents. <br/>
<br/>
              8. D'autre part, aux termes de l'article 8 de la directive du 19 octobre 2009 : " L'attribution, à l'occasion d'une fusion (...), de titres représentatifs du capital social de la société bénéficiaire ou acquérante à un associé de la société apporteuse ou acquise, en échange de titres représentatifs du capital social de cette dernière société, ne doit, par elle-même, entraîner aucune imposition sur le revenu, les bénéfices ou les plus-values de cet associé ". Les dispositions contestées du a septies du I de l'article 219 du code général des impôts n'ont en tout état de cause ni pour objet, ni pour effet d'entraîner l'imposition des plus-values constatées lors d'une fusion entre deux sociétés liées. Par suite, les dispositions contestées ne méconnaissent pas les objectifs de l'article 8 de la directive du 19 octobre 2009 doit être écarté.<br/>
<br/>
              9. Enfin, si le 1 de l'article 15 de la directive du 19 octobre 2009 dispose que " Un Etat membre peut refuser d'appliquer tout ou partie des dispositions des articles 4 à 14 ou d'en retirer le bénéfice lorsqu'une des opérations visées à l'article 1er : / a) a comme objectif ou comme un de ses objectifs principaux la fraude ou l'évasion fiscale (...) ", les dispositions contestées n'ont ni pour objet, ni pour effet de refuser le bénéfice des dispositions de la directive. Par suite, elles ne méconnaissent pas les objectifs de l'article 15 de la directive du 19 octobre 2009.<br/>
<br/>
              10. Il résulte de ce qui précède que les dispositions contestées, qui ne sont pas incompatibles avec les dispositions invoquées du droit de l'Union européenne, s'appliquent de la même façon aux opérations entre sociétés françaises et aux opérations conclues entre sociétés d'Etats membres différents. Par suite, le grief tiré de la méconnaissance des principes d'égalité devant la loi et devant les charges publiques ne présente pas un caractère sérieux.<br/>
<br/>
              11. Il résulte de tout ce qui précède que la question de constitutionnalité invoquée, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Dès lors, il n'y a pas lieu de la renvoyer au Conseil constitutionnel.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la SA HighCo.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la SA HighCo et au ministre de l'action et des comptes publics.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
