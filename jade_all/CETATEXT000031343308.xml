<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031343308</ID>
<ANCIEN_ID>JG_L_2015_10_000000385114</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/34/33/CETATEXT000031343308.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 16/10/2015, 385114</TITRE>
<DATE_DEC>2015-10-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385114</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP TIFFREAU, MARLANGE, DE LA BURGADE ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Cyrille Beaufils</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:385114.20151016</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme F...E..., M. A...D...et Mme B...C...ont demandé au tribunal administratif de Dijon d'annuler pour excès de pouvoir six arrêtés du 26 mai 2011 par lesquels le préfet de la Nièvre a accordé à la société Intervent six permis de construire en vue de l'implantation d'un parc de cinq éoliennes et d'un poste de livraison sur le territoire des communes de Bouhy et Dampierre-sous-Bouhy. Par un jugement n° 1102113 du 4 avril 2013, le tribunal administratif de Dijon a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 13LY01455 du 19 août 2014, la cour administrative d'appel de Lyon a, sur la requête de Mme E...et autres, en premier lieu, annulé ce jugement en tant qu'il avait rejeté leurs conclusions tendant à l'annulation des cinq arrêtés du préfet de la Nièvre en tant que, par leur article 2, ces arrêtés prescrivaient la plantation de haies, en deuxième lieu, annulé dans cette mesure les cinq arrêtés en cause, et, en troisième lieu, rejeté le surplus des conclusions de la requête.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 14 octobre 2014 et 13 janvier 2015 au secrétariat du contentieux du Conseil d'Etat, Mme E... et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - le code de l'environnement ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Cyrille Beaufils, auditeur,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Tiffreau, Marlange, de la Burgade, avocat de Mme E...et autres, et à la SCP Rocheteau, Uzan-Sarano, avocat de la société Intervent ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 21 septembre 2015, présentée par Mme E... et autres ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 21 septembre 2015, présentée par la société Intervent ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que Mme E...et autres se pourvoient en cassation contre un arrêt de la cour administrative d'appel de Lyon qui a annulé, en tant qu'ils prescrivaient la plantation de haies, cinq des six arrêtés du 26 mai 2011, par lesquels le préfet de la Nièvre a accordé à la société Intervent des permis de construire en vue de l'implantation d'un parc éolien de cinq machines hautes de 150 m et d'un poste de livraison sur le territoire des communes de Bouhy et Dampierre-sous-Bouhy, mais  rejeté le surplus de leurs conclusions ; <br/>
<br/>
              2. Considérant que, pour rejeter les conclusions tendant à l'annulation totale des arrêtés attaqués, la cour était tenue, même en cas d'annulation partielle, de répondre à l'ensemble des moyens soulevés, y compris au moyen tiré de ce que les arrêtés litigieux méconnaissaient les dispositions de l'article R. 111-14 du code de l'urbanisme, compte tenu de l'atteinte à l'activité agricole que le projet en cause était susceptible d'entraîner ; qu'elle ne s'est pas prononcée sur ce moyen, qui n'était pas inopérant ; que, par suite, les requérants sont fondés à demander l'annulation de l'arrêt qu'ils attaquent ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Sans qu'il soit besoin de statuer sur la fin de non recevoir opposée par le ministre de l'égalité des territoires et du logement ;<br/>
<br/>
              4. Considérant que le tribunal administratif s'est prononcé sur les différentes branches du moyen tiré de l'insuffisance de l'étude d'impact ; qu'il n'était pas tenu de répondre à l'ensemble des arguments des requérants relatifs à la méthode employée pour délimiter les aires d'étude rapprochée et éloignée ou le périmètre dans lequel ont été étudiées les questions autres que celle de l'impact du projet sur le paysage, en ce qui concerne les chiroptères et les grues ; que le tribunal a ainsi suffisamment motivé son jugement ;<br/>
<br/>
              5. Considérant qu'aux termes de l'article R. 122-3 du code de l'environnement, dans sa rédaction alors applicable : " I. - Le contenu de l'étude d'impact doit être en relation avec l'importance des travaux et aménagements projetés et avec leurs incidences prévisibles sur l'environnement. II. - L'étude d'impact présente successivement : 1° Une analyse de l'état initial du site et de son environnement, portant notamment sur les richesses naturelles et les espaces naturels agricoles, forestiers, maritimes ou de loisirs, affectés par les aménagements ou ouvrages ; 2° Une analyse des effets directs et indirects, temporaires et permanents du projet sur l'environnement, et en particulier sur la faune et la flore, les sites et paysages, le sol, l'eau, l'air, le climat, les milieux naturels et les équilibres biologiques, sur la protection des biens et du patrimoine culturel et, le cas échéant, sur la commodité du voisinage (bruits, vibrations, odeurs, émissions lumineuses) ou sur l'hygiène, la santé, la sécurité et la salubrité publique ; 3° Les raisons pour lesquelles, notamment du point de vue des préoccupations d'environnement, parmi les partis envisagés qui font l'objet d'une description, le projet présenté a été retenu ; 4° Les mesures envisagées par le maître de l'ouvrage ou le pétitionnaire pour supprimer, réduire et, si possible, compenser les conséquences dommageables du projet sur l'environnement et la santé, ainsi que l'estimation des dépenses correspondantes ; 5° Une analyse des méthodes utilisées pour évaluer les effets du projet sur l'environnement mentionnant les difficultés éventuelles de nature technique ou scientifique rencontrées pour établir cette évaluation ; (...) III. - Afin de faciliter la prise de connaissance par le public des informations contenues dans l'étude, celle-ci fait l'objet d'un résumé non technique (...). " ; que les inexactitudes, omissions ou insuffisances d'une étude d'impact ne sont susceptibles de vicier la procédure et donc d'entraîner l'illégalité de la décision prise au vu de cette étude que si elles ont pu avoir pour effet de nuire à l'information complète de la population ou si elles ont été de nature à exercer une influence sur la décision de l'autorité administrative ;<br/>
<br/>
              6. Considérant que les développements consacrés par l'étude d'impact aux chauves-souris reprennent les conclusions d'une étude sur les " Impacts potentiels sur les chauves-souris relatifs au projet d'implantation de la centrale éolienne de Bouhy et Dampierre-sous-Bouhy ", qui s'appuie en particulier sur un recensement réalisé par la société d'histoire naturelle d'Autun dans son étude de pré-diagnostic sur les espèces de chiroptères présentes à proximité du site et sur les observations réalisées au cours de trois soirées d'écoute en mai 2008 et avril et septembre 2009 ; qu'il ressort en particulier de l'étude d'impact que, lors des soirées d'écoute, aucune présence de chauve-souris n'a été constatée ; que, par ailleurs, il ne ressort pas des pièces du dossier que le projet, compte tenu de sa situation et des habitudes de comportement des chiroptères, en particulier des espèces Grand Murin ou Petit Rhinolophe, serait un obstacle majeur à leurs déplacements entre les gîtes d'été et d'hiver ; qu'en outre, contrairement à ce que prétendent les requérants, l'aire d'étude des chiroptères, qui a retenu trois périmètres d'étude au vu du diagnostic établi par une société savante, apparaît suffisamment large ; qu'enfin, l'étude d'impact comporte des développements consacrés aux habitats dans le secteur rapproché du site ; que les requérants ne sont par suite pas fondés à soutenir que la méthodologie de l'étude mise en oeuvre par la société Intervent et les conclusions qui ont pu en être tirées n'auraient pas permis d'apprécier exactement les incidences du projet sur les populations de chiroptères susceptibles d'être affectées par le projet ;<br/>
<br/>
              7. Considérant qu'il ressort des pièces du dossier que le département de la Nièvre est situé dans l'axe du couloir de migration des grues cendrées ; que si les enquêtes de terrain effectuées par l'auteur de l'expertise pendant la migration post-nuptiale et pendant la migration pré-nuptiale sont demeurées en nombre limité eu égard aux trois mois que durent ces migrations et n'ont eu lieu que de nuit, il ressort des études versées au dossier et des témoignages recueillis sur le terrain que les communes de Dampierre-sous-Bouhy et Bouhy se trouvent à l'écart des flux de passages principaux de ces oiseaux ; que, par suite, le moyen tiré de l'inexactitude de l'étude d'impact et de ses annexes en tant qu'elles relèvent que le site d'aérogénérateurs contesté n'est pas situé sur les principaux flux de passages des grues cendrées doit être écarté ; <br/>
<br/>
              8. Considérant qu'il ne ressort pas des pièces du dossier que le point de mesure du bruit PF 2 depuis le hameau de la Valotte aurait été placé à un endroit où le bruit résiduel en provenance de l'activité agricole aurait été particulièrement important ni qu'aurait ainsi été atténuée la perception des nuisances sonores en provenance du champ d'éoliennes ; que le moyen tiré de l'insuffisance des mesures acoustiques ne peut donc qu'être écarté ; <br/>
<br/>
              9. Considérant qu'aux termes de l'article R. 431-13 du code de l'urbanisme : " Lorsque le projet de construction porte sur une dépendance du domaine public, le dossier joint à la demande de permis de construire comporte une pièce exprimant l'accord du gestionnaire du domaine pour engager la procédure d'autorisation d'occupation temporaire du domaine public. " ; que le raccordement, à partir de son poste de livraison, d'une installation de production d'électricité au réseau électrique se rattache à une opération distincte de la construction de cette installation et est sans rapport avec la procédure de délivrance du permis de construire l'autorisant ; que la délivrance de ce permis n'est donc pas subordonnée, hors l'hypothèse où l'installation serait elle-même implantée, en tout ou en partie, sur le domaine public, à l'obtention préalable d'une autorisation d'occupation du domaine public ; que les requérants ne sauraient utilement invoquer l'absence aux dossiers des demandes de permis de construire en litige de pièces attestant de l'accord des autorités gestionnaires du domaine public routier et des ouvrages de distribution de l'électricité pour le passage des câbles électriques, dès lors que les éoliennes projetées ne sont implantées sur aucune parcelle appartenant au domaine public ou n'en surplombent pas et que leur raccordement au réseau électrique relève d'une opération distincte de leur construction et postérieure ;<br/>
<br/>
              10. Considérant qu'aux termes de l'article L. 424-4 du code de l'urbanisme : " Lorsque la décision autorise un projet soumis à étude d'impact, elle est accompagnée d'un document comportant les informations prévues à l'article L. 122-1 du code de l'environnement " ; qu'aux termes de l'article L. 122-1 du code de l'environnement, dans sa rédaction alors applicable : " Sans préjudice de l'application des dispositions des articles L. 11-1-1 du code de l'expropriation pour cause d'utilité publique et L. 126-1 du présent code relatives à la motivation des déclarations d'utilité publique et des déclarations de projet, lorsqu'une décision d'octroi ou de refus de l'autorisation concernant le projet soumis à l'étude d'impact a été prise, l'autorité compétente en informe le public et, sous réserve du secret de la défense nationale, met à sa disposition les informations suivantes : / - la teneur de la décision et les conditions dont celle-ci est le cas échéant assortie ; / - les motifs qui ont fondé la décision ; / - les lieux où peuvent être consultées l'étude d'impact ainsi que, le cas échéant, les principales mesures destinées à éviter, réduire et si possible compenser les effets négatifs importants du projet. " ; que ces dispositions, qui exigent que l'auteur de la décision, une fois cette dernière prise, porte à la connaissance du public une information supplémentaire explicitant les motifs et les considérations qui l'ont fondée, ne sauraient être interprétées comme imposant une motivation en la forme de la décision qui serait une condition de sa légalité ; que, par suite, la circonstance que les informations prévues par les dispositions précitées de l'article L. 122-1 du code de l'environnement n'ont pas été jointes aux arrêtés contestés est sans incidence sur leur légalité ; <br/>
<br/>
              11. Considérant qu'aux termes de l'article R. 111-15 du code de l'urbanisme : " Le permis ou la décision prise sur la déclaration préalable doit respecter les préoccupations d'environnement définies aux articles L. 110-1 et L. 110-2 du code de l'environnement. Le projet peut n'être accepté que sous réserve de l'observation de prescriptions spéciales si, par son importance, sa situation ou sa destination, il est de nature à avoir des conséquences dommageables pour l'environnement. " ; qu'en application de l'article L. 110-1 du code de l'environnement, dans sa version alors en vigueur, " I. - Les espaces, ressources et milieux naturels, les sites et paysages, la qualité de l'air, les espèces animales et végétales, la diversité et les équilibres biologiques auxquels ils participent font partie du patrimoine commun de la nation. II. - Leur protection, leur mise en valeur, leur restauration, leur remise en état et leur gestion sont d'intérêt général et concourent à l'objectif de développement durable qui vise à satisfaire les besoins de développement et la santé des générations présentes sans compromettre la capacité des générations futures à répondre aux leurs (...) " ; qu'il ressort des pièces du dossier que le projet en cause, qui est situé à l'écart des principaux couloirs de migration des grues, porte sur l'implantation de cinq éoliennes sur une distance d'environ mille mètres ; que, malgré une orientation perpendiculaire par rapport à l'axe de progression de ces oiseaux, il n'apparaît pas que la présence de ces machines présenterait pour ces derniers des dangers particuliers, compte tenu de la hauteur de vol des oiseaux et de la configuration du secteur d'implantation du projet, qui est très ouvert et ne présente pas de reliefs particuliers, de sorte que les risques de collision, selon les informations disponibles sur le sujet, demeurent rares; que, par suite, en n'assortissant pas les permis de construire en litige de prescriptions spéciales destinées à protéger l'avifaune, le préfet n'a pas commis d'erreur manifeste d'appréciation ; <br/>
<br/>
              12. Considérant qu'aux termes de l'article R. 111-14 du code de l'urbanisme : " En dehors des parties urbanisées des communes, le projet peut être refusé ou n'être accepté que sous réserve de l'observation de prescriptions spéciales s'il est de nature, par sa localisation ou sa destination : / a) A favoriser une urbanisation dispersée incompatible avec la vocation des espaces naturels environnants, en particulier lorsque ceux-ci sont peu équipés ; / b) A compromettre les activités agricoles ou forestières, notamment en raison de la valeur agronomique des sols, des structures agricoles, de l'existence de terrains faisant l'objet d'une délimitation au titre d'une appellation d'origine contrôlée ou d'une indication géographique protégée ou comportant des équipements spéciaux importants, ainsi que de périmètres d'aménagements fonciers et hydrauliques (...) " ; que si les requérants soutiennent que les arrêtés litigieux seraient contraires à ces dispositions, ils n'assortissent pas ce moyen des précisions suffisantes permettant d'en apprécier la portée et le bien-fondé ; qu'en tout état de cause, il ne ressort pas des pièces du dossier que les permis litigieux seraient entachés d'erreur manifeste dans l'appréciation du risque de favoriser une urbanisation dispersée et de porter atteinte à l'activité agricole qui pourrait résulter de l'implantation du parc éolien en cause ;<br/>
<br/>
              13. Considérant qu'aux termes de l'article R. 111-21 du code de l'urbanisme : " Le projet peut être refusé ou n'être accepté que sous réserve de l'observation de prescriptions spéciales si les constructions, par leur situation, leur architecture, leurs dimensions ou l'aspect extérieur des bâtiments ou ouvrages à édifier ou à modifier, sont de nature à porter atteinte au caractère ou à l'intérêt des lieux avoisinants, aux sites, aux paysages naturels ou urbains ainsi qu'à la conservation des perspectives monumentales. " ; que s'il est vrai que le projet sera totalement ou partiellement visible depuis les bourgs de Bouhy et de Dampierre-sous-Bouhy, à partir notamment de la place située devant l'église de ce dernier bourg, classée monument historique, l'atteinte que ce projet est susceptible de porter au paysage ou à l'environnement visuel demeure limitée ; que, dès lors, l'appréciation à laquelle s'est livré le préfet pour délivrer les permis de construire attaqués ne procède d'aucune erreur manifeste d'appréciation sur ce point ; <br/>
<br/>
              14. Considérant que, par cinq des six arrêtés contestés, le préfet de la Nièvre a autorisé la construction d'éoliennes sous réserve, pour la société Intervent, de respecter les prescriptions d'ordre technique, indivisibles du reste des permis, figurant à l'article 2 de ces arrêtés qui imposent, au titre de l'environnement, sur des parcelles appartenant à des propriétaires privés, la plantation de haies à la sortie du hameau de La Valotte et le long de la route départementale 957 ; <br/>
<br/>
              15. Considérant que si les requérants soutiennent que le préfet de la Nièvre a commis une erreur de droit en prescrivant la plantation de haies sur des parcelles privées, sans s'assurer de l'accord de leurs propriétaires, cette circonstance, à supposer que les propriétaires concernés n'aient pas donné leur accord à la date de délivrance des permis attaqués, n'est pas de nature à entacher d'illégalité ces derniers, qui ont été délivrés sous réserve des droits des tiers ; que la construction du parc d'éoliennes ne pourra, au demeurant, être légalement réalisée conformément aux permis délivrés qu'à la condition que les haies aient pu être plantées ; que, par suite, le moyen tiré de ce que ces prescriptions seraient entachées d'illégalité sur ce point doit être écarté ;<br/>
<br/>
              16. Considérant qu'il résulte de tout ce qui précède que Mme E...et autres ne sont pas fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Dijon a rejeté leur demande d'annulation des cinq arrêtés du préfet de la Nièvre en date du 26 mai 2011 ; <br/>
<br/>
              17. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de Mme E...et autres la somme globale de 1 000 euros à verser à la société Intervent au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 19 août 2014 est annulé.<br/>
<br/>
Article 2 : La requête présentée devant cette cour par Mme E...et autres est rejetée.<br/>
<br/>
Article 3 : Mme E...et autres verseront à la société Intervent une somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions de Mme E...et autres présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 5 : La présente décision sera notifiée à Mme F...E..., M. A...D..., Mme B...C..., à la ministre du logement, de l'égalité des territoires et de la ruralité et à la société Intervent.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-03-025-02-02-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. NATURE DE LA DÉCISION. OCTROI DU PERMIS. PERMIS ASSORTI DE RÉSERVES OU DE CONDITIONS. OBJET DES RÉSERVES OU CONDITIONS. - POSSIBILITÉ POUR UN PERMIS DE COMPORTER DES PRESCRIPTIONS RELATIVES À LA PLANTATION DE HAIES SUR DES PARCELLES PRIVÉES SANS L'ACCORD DES PROPRIÉTAIRES CONCERNÉS - EXISTENCE.
</SCT>
<ANA ID="9A"> 68-03-025-02-02-01 La circonstance qu'un préfet ait, en autorisant la construction d'un parc d'éoliennes, prescrit la plantation de haies sur des parcelles privées n'est pas, à supposer que les propriétaires concernés n'aient pas donné leur accord à la date de délivrance des permis attaqués, de nature à entacher d'illégalité ces derniers, qui ont été délivrés sous réserve des droits des tiers. La construction du parc d'éoliennes ne pourra, au demeurant, être légalement réalisée conformément aux permis délivrés qu'à la condition que les haies aient pu être plantées.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
