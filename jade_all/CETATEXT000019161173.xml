<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000019161173</ID>
<ANCIEN_ID>JG_L_2008_07_000000296439</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/19/16/11/CETATEXT000019161173.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 07/07/2008, 296439, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2008-07-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>296439</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin</PRESIDENT>
<AVOCATS>SCP LYON-CAEN, FABIANI, THIRIEZ ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Laure  Bédier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mlle Courrèges Anne</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 11 août et 11 décembre 2006 au secrétariat du contentieux du Conseil d'Etat, présentés pour la COMMUNAUTE DE COMMUNES DE VERDUN, dont le siège est 11, rue du Président Poincaré à Verdun (55107), représentée par son président ; la COMMUNAUTE DE COMMUNES DE VERDUN demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 1er juin 2006 par lequel la cour administrative d'appel de Nancy a annulé la délibération du conseil municipal de Verdun du 12 juillet 2000 approuvant la révision du plan d'occupation des sols de la commune, en tant qu'elle classe la parcelle AH 107 en secteur ULa et crée un emplacement réservé sur celle-ci ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel présenté par la SCI Victor Schleiter et M. Dominique A ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laure Bédier, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Piwnica, Molinié, avocat de la COMMUNAUTE DES COMMUNES DE VERDUN et de la SCP Lyon-Caen, Fabiani, Thiriez, avocat de la SCI Victor Schleiter et M. A, <br/>
<br/>
              - les conclusions de Mlle Anne Courrèges, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes de l'article L. 600-4-1 du code de l'urbanisme : « Lorsqu'elle annule pour excès de pouvoir un acte intervenu en matière d'urbanisme ou en ordonne la suspension, la juridiction administrative se prononce sur l'ensemble des moyens de la requête qu'elle estime susceptibles de fonder l'annulation ou la suspension, en l'état du dossier » ; qu'il résulte de ces dispositions que le juge administratif, lorsqu'il annule un acte d'urbanisme, ne peut se dispenser d'examiner l'ensemble des moyens de la requête ; qu'ainsi, en retenant le moyen tiré du détournement de pouvoir pour annuler, « sans qu'il soit besoin d'examiner les autres moyens de la requête », la délibération du conseil municipal de Verdun du 12 juillet 2000 révisant le plan d'occupation des sols de cette commune en tant qu'elle classe la parcelle AH 107 en secteur ULa et crée un emplacement réservé sur celle-ci, la cour administrative d'appel de Nancy a méconnu son office ; que son arrêt doit, par suite, être annulé ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Sur la régularité de la procédure d'adoption du plan d'occupation des sols :<br/>
<br/>
              Considérant, en premier lieu, que les dispositions de l'article L. 300-2 du code de l'urbanisme qui, dans leur rédaction applicable à la date de la délibération du 12 juillet 2000 portant révision du plan d'occupation des sols, prévoient qu'une concertation mise en place par le conseil municipal doit associer les habitants, les associations et les autres personnes concernées lorsque la révision du plan d'occupation des sols ouvre à l'urbanisation tout ou partie d'une zone d'urbanisation future, ne peuvent utilement être invoquées à l'encontre de la délibération litigieuse en tant qu'elle classe la parcelle AH 107 en secteur ULa et crée un emplacement réservé, dès lors que cette parcelle était déjà ouverte à l'urbanisation ;<br/>
<br/>
              Considérant, en deuxième lieu, que si l'avis des personnes publiques associées à la révision du plan d'occupation des sols n'était pas annexé au plan d'occupation des sols lors de l'ouverture de l'enquête publique, comme l'imposent les dispositions de l'article R. 123-10 du code de l'urbanisme, alors en vigueur, cette omission a été réparée le jour même à 14 heures, ainsi qu'en atteste le rapport du commissaire enquêteur, et n'a, dès lors, pas affecté la régularité de la procédure ;<br/>
<br/>
              Considérant, en troisième lieu, que la circonstance qu'une seule réunion publique a été organisée n'est pas par elle-même, et en l'absence de précisions apportées par les requérants, de nature à entacher d'irrégularité la procédure de révision du plan d'occupation des sols, non plus que le caractère « ancien et imprécis » de certains plans produits durant l'enquête publique ;<br/>
<br/>
              Considérant, en quatrième lieu, que si le maire de Verdun a adressé au commissaire enquêteur, avant la fin de l'enquête publique, une lettre lui faisant part de certaines modifications qui pourraient être apportées au projet, il ne s'en déduit pas pour autant que le dossier de révision du plan d'occupation des sols soumis à enquête publique ait été irrégulièrement modifié au cours de celle-ci ;<br/>
<br/>
              Considérant, en cinquième lieu, qu'il ressort des pièces du dossier que le commissaire enquêteur a rendu un avis favorable à la révision du plan d'occupation des sols, sous certaines réserves clairement exprimées, conformément aux dispositions de l'article R. 123-11 du code de l'urbanisme, alors en vigueur ; qu'ainsi, les moyens tirés de l'incohérence et du caractère défavorable de cet avis doivent être écartés ;<br/>
<br/>
              Considérant, enfin, qu'aux termes de l'article R. 123-12 du code de l'urbanisme, alors en vigueur  : « Le plan d'occupation des sols, éventuellement modifié pour tenir compte des résultats de l'enquête publique et des propositions de la commission de conciliation, donne lieu, dans les conditions fixées au premier alinéa de l'article R. 123-9, à la consultation des services de l'Etat et des personnes publiques associées si le maire estime que la nature et l'importance des modifications envisagées justifient cette consultation.  Le plan (...) est ensuite transmis au conseil municipal, qui l'approuve par délibération » ; qu'en l'espèce, si des modifications ont été apportées au projet de révision après l'enquête publique, il ressort des pièces du dossier qu'eu égard à l'importance de la commune et à l'étendue limitée des secteurs affectés par les modifications apportées au projet initial, celles-ci, qui tiennent compte des résultats de l'enquête publique et n'affectent pas l'économie générale du plan d'occupation des sols, n'étaient pas de nature à justifier une nouvelle consultation des personnes publiques associées ni l'engagement d'une nouvelle procédure d'enquête publique ;<br/>
<br/>
              Sur la légalité interne du plan d'occupation des sols en ce qui concerne la parcelle AH 107 :<br/>
<br/>
              Considérant, en premier lieu, qu'il ressort des pièces du dossier, notamment du rapport de présentation de la révision du plan d'occupation des sols, qu'en décidant de classer la parcelle AH 107, propriété de M. A, en zone ULa, ce qui limite sa constructibilité, et en l'inscrivant comme emplacement réservé, le conseil municipal de Verdun a souhaité protéger et mettre en valeur le site sur lequel se trouve cette parcelle, située à la pointe du confluent de la Meuse et du canal des Augustins, à proximité du centre historique de la ville et en co-visibilité de plusieurs monuments, afin de permettre l'aménagement d'une promenade le long de la Meuse et la création d'un espace vert et d'un équipement affecté à l'usage du public ; que ces choix, fondés sur la situation particulière de la parcelle et sur des motifs d'urbanisme, ne sont pas entachés d'une erreur manifeste d'appréciation et répondent à un intérêt général ; qu'ainsi, alors même que ces décisions auraient pour effet de faire obstacle aux projets immobiliers des requérants et que la juridiction administrative a précédemment annulé les refus opposés par le maire à des demandes de permis de construire sur cette parcelle ainsi qu'une modification du plan d'occupation des sols la concernant, le détournement de pouvoir allégué n'est pas établi ;<br/>
<br/>
              Considérant, en deuxième lieu, qu'aux termes de l'article L. 123-1 du code de l'urbanisme, dans sa rédaction alors applicable : « (...) Les plans d'occupation des sols doivent (...) : (...) / 8° fixer les emplacements réservés aux voies et ouvrages publics, aux installations d'intérêt général ainsi qu'aux espaces verts (...) » ; qu'il résulte de ces dispositions que la commune n'avait pas à justifier, pour décider la création d'un emplacement réservé sur la parcelle AH 107 dans le cadre de la révision du plan d'occupation des sols, d'un projet précis et déjà élaboré de voie ou d'ouvrage publics, d'équipement d'intérêt général ou d'espace vert ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que M. A et la SCI Victor Schleiter ne sont pas fondés à soutenir que c'est à tort que le tribunal administratif de Nancy a rejeté leur demande tendant à l'annulation de la délibération du conseil municipal de Verdun du 12 juillet 2000 en ce qu'elle classe la parcelle AH 107 en zone ULa et crée un emplacement réservé ; que leurs conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative doivent, en conséquence, être rejetées ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à leur charge le versement au profit de la COMMUNAUTE DE COMMUNES DE VERDUN de la somme de 750 euros chacun au titre des frais exposés par elle et non compris dans les dépens en cause d'appel ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 1er juin 2006 est annulé.<br/>
Article 2 : La requête d'appel de la SCI Victor Schleiter et de M. A et leurs conclusions présentées devant le Conseil d'Etat en application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La SCI Victor Schleiter et M. A verseront chacun à la COMMUNAUTE DE COMMUNES DE VERDUN une somme de 750 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la COMMUNAUTE DE COMMUNES DE VERDUN, à la SCI Victor Schleiter et à M. Dominique A.<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
