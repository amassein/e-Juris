<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000008258183</ID>
<ANCIEN_ID>JG_L_2006_11_000000285749</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/08/25/81/CETATEXT000008258183.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 15/11/2006, 285749</TITRE>
<DATE_DEC>2006-11-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>285749</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>Mme Hagelsteen</PRESIDENT>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN</AVOCATS>
<RAPPORTEUR>M. Marc  Lambron</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olson</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 4 octobre 2005 au secrétariat du contentieux du Conseil d'Etat, présentée pour l'association ZALEA TV, dont le siège est 47, rue d'Aubervilliers à Paris (75018), représentée par son président en exercice ; l'association ZALEA TV demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 19 juillet 2005 par laquelle le Conseil supérieur de l'audiovisuel (CSA) a rejeté sa candidature pour l'édition de services de télévision à vocation nationale diffusés par voie hertzienne terrestre en mode numérique ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu la loi n° 86-1067 du 30 septembre 1986 modifiée ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Lambron, Conseiller d'Etat,   <br/>
<br/>
              - les observations de la SCP Masse-Dessen, Thouvenin, avocat de l'association ZALEA TV, <br/>
<br/>
              - les conclusions de M. Terry Olson, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant que l'association ZALEA TV demande l'annulation de la décision du 19 juillet 2005 par laquelle le Conseil supérieur de l'audiovisuel a rejeté sa candidature pour l'édition de services de télévision à vocation nationale diffusés par voie hertzienne terrestre en mode numérique ;<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier que la décision attaquée par l'association ZALEA TV a été prise par le Conseil supérieur de l'audiovisuel délibérant de manière collégiale, et non par le seul président de cette autorité, qui s'est borné à la porter à la connaissance de l'association ; que le moyen tiré de ce que cette décision aurait été prise par une autorité incompétente doit être écarté ;<br/>
<br/>
              Considérant que le Conseil supérieur de l'audiovisuel n'était pas tenu d'énumérer dans sa décision, qui est suffisamment motivée, l'ensemble des critères dont la loi du 30 septembre 1986 relative à la liberté de communication audiovisuelle lui prescrit de tenir compte pour opérer son choix entre les différentes candidatures dont il est saisi ;<br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              Considérant qu'aux termes de l'article 29 de la loi du 30 septembre 1986 : « Le conseil accorde les autorisations en appréciant l'intérêt de chaque projet pour le public, au regard des impératifs prioritaires que sont la sauvegarde du pluralisme des courants d'expression socio-culturels.... Il tient également compte :.... 9°) du financement et des perspectives d'exploitation du service... » ; que le Conseil supérieur de l'audiovisuel peut légalement se fonder sur ce qu'un projet n'est pas assorti de garanties suffisantes permettant d'assurer de façon durable le financement et l'exploitation du service proposé pour refuser de délivrer l'autorisation sollicitée ;<br/>
<br/>
              Considérant que pour écarter la candidature de l'association ZALEA TV à l'exploitation d'un service payant de télévision à vocation nationale diffusé par voie hertzienne terrestre en mode numérique, le Conseil supérieur de l'audiovisuel s'est fondé sur ce que le financement du projet ne lui apparaissait pas assuré compte tenu, d'une part, de l'absence d'engagements fermes de la part des prêteurs alors que le dossier évoquait le recours à deux emprunts et, d'autre part, sur les incertitudes que comportait le projet en ce qui concerne le mode et les conditions de distribution du service ;<br/>
<br/>
              Considérant que la circonstance que le Conseil supérieur de l'audiovisuel dans son appel à candidature ait invité les candidats à produire à l'appui de leur dossier des « ...lettres d'intention d'établissements financiers en cas de recours à l'emprunt » ne faisait pas obstacle à ce qu'il apprécie le degré d'engagement exprimé dans de tels courriers par leurs signataires ; qu'en estimant que le recours à l'emprunt pendant les deux premières années d'exploitation constituait un élément déterminant pour la viabilité du projet de ZALEA TV et que les courriers bancaires produits par celle-ci, qui présentaient un caractère conditionnel, ne pouvaient être considérés comme des engagements suffisamment fermes et qu'il en résultait que le financement du projet était incertain, le Conseil supérieur de l'audiovisuel n'a pas commis d'erreur d'appréciation ;<br/>
<br/>
              Considérant que le Conseil supérieur de l'audiovisuel n'a pas davantage commis d'erreur de droit en tenant compte dans son appréciation des mérites de la candidature de ZALEA TV, de l'état d'avancement des négociations de cette dernière avec les éventuels distributeurs du service au cas où il serait autorisé, alors même qu'à la date de sa décision, le décret relatif au régime déclaratif des distributeurs des services de communication audiovisuelle n'était pas entré en vigueur ; qu'il ne résulte pas des pièces du dossier que le Conseil supérieur de l'audiovisuel, en estimant que les conditions de distribution du service étaient incertaines compte tenu du taux élevé de rémunération par abonné attendu des distributeurs, ait commis une autre erreur d'appréciation ;<br/>
<br/>
              Considérant qu'il ne ressort pas des pièces du dossier qu'en rejetant la candidature de l'association ZALEA TV le Conseil supérieur de l'audiovisuel ait méconnu le principe de pluralisme des courants d'expression socio-culturels énoncé à l'article 29 de la loi du 30 septembre 1986 ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que l'association ZALEA TV n'est pas fondée à demander l'annulation de la décision susvisée du Conseil supérieur de l'audiovisuel en date du 19 juillet 2005 ;<br/>
<br/>
              Sur l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que ces dispositions font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas dans la présente instance la partie perdante, la somme que l'association ZALEA TV demande au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
		Article 1er : La requête de l'association ZALEA TV est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à l'association ZALEA TV, au Conseil supérieur de l'audiovisuel, au Premier ministre et au ministre de la culture et de la communication.<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">56-04-03-02-01-01 RADIODIFFUSION SONORE ET TÉLÉVISION. SERVICES PRIVÉS DE RADIODIFFUSION SONORE ET DE TÉLÉVISION. SERVICES DE TÉLÉVISION. SERVICES AUTORISÉS. SERVICES DE TÉLÉVISION PAR VOIE HERTIENNE. OCTROI DES AUTORISATIONS. - MOTIFS DE REFUS - DÉFAUT DANS LE FINANCEMENT ET LES PERSPECTIVES D'EXPLOITATION DU SERVICE (ART. 29-9° DE LA LOI DU 30 SEPTEMBRE 1986) - NOTION - INCLUSION - ABSENCE D'ENGAGEMENT FERME DE LA PART DES PRÊTEURS PRESSENTIS - INCERTITUDES DU PROJET EN CE QUI CONCERNE LE MODE ET LES CONDITIONS DE DISTRIBUTION DU SERVICE.
</SCT>
<ANA ID="9A"> 56-04-03-02-01-01 Le Conseil supérieur de l'audiovisuel peut, en application du 9° de l'article 29 de la loi du 30 septembre 1986, légalement se fonder sur ce qu'un projet n'est pas assorti de garanties suffisantes permettant d'assurer de façon durable le financement et l'exploitation du service proposé pour refuser de délivrer l'autorisation sollicitée. Constituent dans ce cadre des motifs légaux de refus des motifs tirés, d'une part, de l'absence d'engagements fermes de la part des prêteurs alors que le dossier évoque le recours à l'emprunt et, d'autre part, des incertitudes que comporte le projet en ce qui concerne le mode et les conditions de distribution du service.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
