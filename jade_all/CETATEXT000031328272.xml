<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031328272</ID>
<ANCIEN_ID>JG_L_2015_10_000000384650</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/32/82/CETATEXT000031328272.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 16/10/2015, 384650, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-10-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384650</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sophie-Justine Lieber</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:384650.20151016</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 22 septembre 2014 et 15 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, la Fédération Allier Nature demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite par laquelle le ministre de l'écologie, du développement durable et de l'énergie, le ministre de l'intérieur et le ministre des droits des femmes, de la ville, de la jeunesse et des sports ont refusé de prendre l'arrêté interministériel d'application du décret n° 2011-269 du 15 mars 2011 pris pour l'application du deuxième alinéa de l'article L. 362-3 du code de l'environnement et relatif aux épreuves et compétitions de sports motorisés sur les voies non ouvertes à la circulation publique ;<br/>
<br/>
              2°) d'enjoindre à ces ministres de prendre cet arrêté, dans un délai de quatre mois à compter de la notification de la décision du Conseil d'Etat, sous astreinte de 500 euros par jour de retard à l'expiration de ce délai et jusqu'à la publication de cet arrêté ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - Vu le code de l'environnement, notamment son article L. 362-3 ;<br/>
              - Vu le code du sport ; <br/>
              - Vu le décret n° 2011-269 du 15 mars 2011 ;<br/>
              - Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Justine Lieber, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que l'exercice du pouvoir réglementaire comporte non seulement le droit mais aussi l'obligation de prendre dans un délai raisonnable les mesures qu'implique nécessairement l'application de la loi, hors le cas où le respect des engagements internationaux de la France y ferait obstacle ; que lorsqu'un décret pris pour l'application d'une loi renvoie lui-même à un arrêté la détermination de certaines mesures nécessaires à son application, cet arrêté doit également intervenir dans un délai raisonnable ;<br/>
<br/>
              2. Considérant qu'aux termes du troisième alinéa de l'article L. 362-3 du code de l'environnement : " Les épreuves et compétitions de sports motorisés sont autorisées, dans des conditions définies par décret en Conseil d'Etat, par le préfet " ; que le décret du 15 mars 2011 pris pour l'application de l'article L. 362-3 du code de l'environnement et relatif aux épreuves et compétitions de sports motorisés sur les voies non ouvertes à la circulation publique a introduit plusieurs dispositions dans le code de l'environnement et dans le code du sport ; qu'ainsi, l'article R. 362-1 du code de l'environnement prévoit que : " Les autorisations prévues au deuxième alinéa de l'article L. 362-3 sont délivrées dans les conditions fixées par les articles R. 331-18 et suivants du code du sport " ; que l'article R. 331-18 du code du sport dispose que : " (...) Les manifestations comportant la participation de véhicules terrestres à moteur qui se déroulent sur des circuits, terrains ou parcours, tels que définis à l'article R. 331-21 sont soumises à autorisation " ; que les modalités de délivrance de l'autorisation sont définies aux articles R. 331-23 à R. 331-28 du code du sport ; que, s'agissant des manifestations se déroulant sur des terrains ou des parcours fermés à la circulation publique et non homologués, l'article R. 331-24-1 du code du sport prévoit que : " Lorsque la demande d'autorisation porte sur l'organisation d'une épreuve ou d'une compétition de sports motorisés se déroulant sur des terrains ou des parcours fermés de manière permanente à la circulation publique et non soumis à la procédure prévue à l'article L. 421-2 du code de l'urbanisme, un arrêté conjoint du ministre de l'intérieur, du ministre chargé des sports et du ministre chargé de l'environnement détermine également, en fonction de l'importance de la manifestation, la nature des documents d'évaluation des incidences sur l'environnement et des mesures préventives et correctives que le dossier de la demande doit comprendre " ; <br/>
<br/>
              3. Considérant que l'arrêté auquel renvoie l'article R. 331-24-1 du code du sport n'a pas été pris ; que si, ainsi que le soutient le ministre, en vertu de l'article R. 414-19 du code de l'environnement, les manifestations sportives qui entrent dans le champ d'application du 24° de son I doivent faire l'objet d'une évaluation de leurs éventuelles incidences sur l'environnement, ces dispositions ne visent que les incidences sur les sites Natura 2000 ; que l'arrêté prévu par l'article R. 331-24-1 du code du sport est nécessaire à la mise en oeuvre de l'obligation d'évaluation des incidences sur l'environnement et de définition des mesures préventives et correctrices qu'il prévoit ; qu'aux dates des 3 et 9 septembre 2014, auxquelles sont nées les décisions de refus implicite des ministres de prendre l'arrêté, ceux-ci avaient disposé d'un temps suffisant pour s'acquitter de leur obligation ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que la Fédération Allier Nature est fondée à demander l'annulation de la décision implicite par laquelle les ministres chargés de l'environnement, du sport et de l'intérieur ont rejeté sa demande tendant à ce que l'arrêté interministériel prévu par l'article R. 331-24-1 du code du sport soit pris ; <br/>
<br/>
              5. Considérant qu'aux termes de l'article L. 911 1 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution " ; que l'annulation de la décision litigieuse implique nécessairement que soit pris l'arrêté prévu par l'article R. 331-24-1 du code du sport ; qu'il y a lieu, dès lors, pour le Conseil d'Etat d'enjoindre aux ministres concernés de prendre un tel arrêté dans un délai de six mois ; que, dans les circonstances de l'espèce, il n'y a pas lieu d'assortir cette injonction de l'astreinte demandée par la Fédération Allier Nature ;<br/>
<br/>
              6. Considérant, enfin, qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat, au titre de l'article L. 761-1 du code de justice administrative, une somme de 1 000 euros à verser à la Fédération Allier Nature ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les décisions implicites du ministre de l'écologie, du développement durable et de l'énergie, du ministre de la ville, de la jeunesse et des sports et du ministre de l'intérieur rejetant la demande de la Fédération Allier Nature de prendre l'arrêté prévu par l'article R. 331-24-1 du code du sport sont annulées.<br/>
<br/>
Article 2 : Il est enjoint au ministre de l'écologie, du développement durable et de l'énergie, au ministre de la ville, de la jeunesse et des sports et au ministre de l'intérieur de prendre, dans un délai de six mois à compter de la notification de la présente décision, l'arrêté interministériel prévu par l'article R. 331-24-1 du code du sport.<br/>
<br/>
Article 3 : L'Etat versera à Fédération Allier Nature la somme de 1 000 euros  au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la Fédération Allier Nature, à la ministre de l'écologie, du développement durable et de l'énergie, au ministre de la ville, de la jeunesse et des sports et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
