<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035048516</ID>
<ANCIEN_ID>JG_L_2017_03_000000408930</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/04/85/CETATEXT000035048516.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 29/03/2017, 408930, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408930</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:408930.20170329</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. D...A...a demandé au juge des référés du tribunal administratif de Lille, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner la suspension de l'exécution de l'arrêté du 20 décembre 2016 par lequel le ministre de l'intérieur a prolongé son assignation à résidence sur le territoire de la commune de Roubaix avec obligation de se présenter trois fois par jour au commissariat de police, à 8 heures, 12 heures et 19 heures y compris les jours fériés et chômés et de demeurer tous les jours de 20 heures à 6 heures à son domicile, et avec interdiction de se déplacer en dehors de son lieu d'assignation de résidence sans autorisation préalable de l'autorité administrative et de se trouver en relation avec M. B... G...F...et lui a fait obligation de remettre son passeport ou tout document justificatif de son identité au même commissariat. Par une ordonnance n° 1701413 du 17 février 2017, le juge des référés du tribunal administratif de Lille a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 15 mars 2017 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Matuchansky, Poupot, Valdelièvre au titre de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative ; <br/>
              4°) de mettre à la charge de l'Etat la somme de 1 200 euros à verser à Me E... au titre de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative pour la procédure devant le tribunal administratif de Lille. <br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence est remplie ; <br/>
              - l'ordonnance contestée est entachée d'une erreur de droit en ce que le juge des référés du tribunal administratif de Lille s'est fondé sur des éléments tenant à l'influence exercée sur lui par M.F..., alors que le juge des référés du Conseil d'Etat avait considéré que ces éléments ne sauraient être tenus pour établis dans son ordonnance n° 404824 du 16 novembre 2016 et qu'ils n'avaient pas été repris par le ministre dans son arrêté du 20 décembre 2016 pour justifier sa prolongation d'assignation à résidence ; <br/>
              - c'est au prix d'une erreur de droit et d'une inexacte appréciation des faits que le juge des référés a jugé que l'atteinte grave et manifestement illégale à sa liberté d'aller et venir n'était pas établie, dès lors qu'il ressort des procès-verbaux établis dans le cadre de l'enquête judiciaire que seules six connexions de courte durée ont été relevées en 2015 et 2016, que les plus récentes datent de juillet 2016 et que les vidéos consultées à cette occasion sur Internet ne contiennent aucun élément en relation avec l'apologie du terrorisme ou le djihadisme, éléments qui ne sauraient suffire à caractériser l'existence d'une menace à la sécurité et l'ordre publics ;<br/>
              - l'ordonnance du juge des référés est insuffisamment motivée, en ce qu'elle ne répond pas au moyen tiré du caractère peu circonstancié des éléments justifiant de l'existence d'une telle menace ;<br/>
              - l'arrêté contesté porte une atteinte grave et manifestement illégale à sa liberté d'aller et venir dès lors que, d'une part, le faible nombre de connexions et la durée très courte de visionnage des vidéos intitulées " 19HH " ne sont pas susceptibles, à eux seuls, d'attester l'existence d'un comportement d'auto-radicalisation, d'autre part, les messages datant de 2015 relevés sur son téléphone traduisent uniquement un intérêt d'ordre religieux et, enfin, il justifie d'un environnement familial stable et d'une volonté de retrouver un emploi.<br/>
              Par un mémoire en défense, enregistré le 22 mars 2017, le ministre de l'intérieur conclut au rejet de la requête. Il fait valoir que les moyens soulevés par le requérant ne sont pas fondés.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 55-385 du 3 avril 1955 ;<br/>
              - la loi n° 2015-1501 du 20 novembre 2015 ;<br/>
              - la loi n° 2016-162 du 19 février 2016 ;<br/>
              - la loi n° 2016-629 du 20 mai 2016 ;<br/>
              - la loi n° 2016-987 du 21 juillet 2016 ;<br/>
              - la loi n° 2016-1767 du 19 décembre 2016 ;<br/>
              - le décret n° 2015-1475 du 14 novembre 2015 ;<br/>
              - le décret n° 2015-1476 du 14 novembre 2015 ;<br/>
              - le décret n° 2015-1478 du 14 novembre 2015 ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. A..., d'autre part, le ministre de l'intérieur ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du vendredi 24 mars 2017 à 15 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Poupot, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. A...;<br/>
<br/>
              - M.A... ;<br/>
<br/>
              - la représentante de M. A...;<br/>
- le représentant du ministre de l'intérieur ; <br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ".<br/>
<br/>
              2. En application de la loi du 3 avril 1955, l'état d'urgence a été déclaré par le décret n° 2015-1475 du 14 novembre 2015, à compter du même jour à zéro heure, sur le territoire métropolitain et prorogé pour une durée de trois mois, à compter du 26 novembre 2015, par l'article 1er de la loi du 20 novembre 2015, puis prorogé à nouveau pour une durée de trois mois à compter du 26 février 2016 par l'article unique de la loi du 19 février 2016, pour une durée de deux mois à compter du 26 mai 2016 par l'article unique de la loi du 20 mai 2016, pour une durée de six mois à compter du 21 juillet 2016 par l'article 1er de la loi du 21 juillet 2016, et jusqu'au 15 juillet 2017 par l'article 1er de la loi du 19 décembre 2016.<br/>
<br/>
              3. Aux termes de l'article 6 de la loi du 3 avril 1955, dans sa rédaction issue de la loi du 19 décembre 2016 : " Le ministre de l'intérieur peut prononcer l'assignation à résidence, dans le lieu qu'il fixe, de toute personne résidant dans la zone fixée par le décret mentionné à l'article 2 et à l'égard de laquelle il existe des raisons sérieuses de penser que son comportement constitue une menace pour la sécurité et l'ordre publics dans les circonscriptions territoriales mentionnées au même article 2. (...) / La personne mentionnée au premier alinéa du présent article peut également être astreinte à demeurer dans le lieu d'habitation déterminé par le ministre de l'intérieur, pendant la plage horaire qu'il fixe, dans la limite de douze heures par vingt-quatre heures. / L'assignation à résidence doit permettre à ceux qui en sont l'objet de résider dans une agglomération ou à proximité immédiate d'une agglomération. (...) / Le ministre de l'intérieur peut prescrire à la personne assignée à résidence : / 1° L'obligation de se présenter périodiquement aux services de police ou aux unités de gendarmerie, selon une fréquence qu'il détermine dans la limite de trois présentations par jour, en précisant si cette obligation s'applique y compris les dimanches et jours fériés ou chômés (...). / La personne astreinte à résider dans le lieu qui lui est fixé en application du premier alinéa du présent article peut se voir interdire par le ministre de l'intérieur de se trouver en relation, directement ou indirectement, avec certaines personnes, nommément désignées, dont il existe des raisons sérieuses de penser que leur comportement constitue une menace pour la sécurité et l'ordre publics. Cette interdiction est levée dès qu'elle n'est plus nécessaire. (...) ".<br/>
              4. Il résulte de l'instruction que M. D... A..., né en 1987, marié et père de deux enfants, a vu son domicile perquisitionné le 27 juillet 2016, en application de l'article 11 de la loi du 3 avril 1955. Le 30 juillet 2016, l'intéressé a fait l'objet d'un premier arrêté d'assignation à résidence, fondé sur l'existence de liens qu'il aurait noués lorsqu'il vivait à la Réunion, par l'intermédiaire de l'application de vidéo-conversation " Skype ", avec M.C..., qualifié de " jeune illuminé dunkerquois convaincu d'être le compagnon du Mahdi et s'improvisant référent religieux ", sur l'existence de liens noués par M.A..., après son installation en 2015 dans la métropole lilloise, avec M. B...F..., connu des services de police pour son attitude de " prosélytisme agressif ", membre d'un groupuscule belge prônant l'instauration de la loi islamique en Belgique dissous en 2012, interpellé en avril 2015 dans le cadre d'une procédure diligentée du chef d'association de malfaiteurs terroristes, faisant lui-même l'objet d'une mesure d'assignation à résidence et qui serait son " guide spirituel ", sur la circonstance que M. A... serait un salafiste au caractère influençable ayant adopté des positions radicales, cultivant une haine de l'Occident et un discours hostile à la France ainsi qu'un comportement centré sur la religion et, enfin, sur la saisie, à son domicile, au cours de la perquisition administrative réalisée le 27 juillet 2016, d'impressions de documents relatifs à des recherches sur des lunettes à vision nocturne destinées à équiper des armes et un scanner de recherches de points de chaleur sur les véhicules. Saisi en appel de l'ordonnance par laquelle le juge des référés du tribunal administratif de Lille avait rejeté la requête de M. A...tendant à la suspension de l'exécution de cet arrêté d'assignation à résidence sur le fondement de l'article L. 521-2 du code de justice administrative, le juge des référés du Conseil d'Etat a, par une ordonnance n° 404824 du 16 novembre 2016, jugé que le ministre n'apportait aucun élément de nature à contredire l'affirmation de M. A...selon laquelle il n'était plus en relation depuis 2015 avec M.C..., dont l'administration ne précisait pas, en dépit d'un supplément d'instruction, pour quels faits il aurait lui-même fait l'objet de mesures administratives. Par cette ordonnance, le juge des référés du Conseil d'Etat a jugé que la note blanche produite par le ministre n'était assortie d'aucun élément précis et circonstancié ni sur l'influence qu'exercerait M. F... sur M.A..., ni sur les positions radicales et hostiles à la France et à l'Occident que ce dernier aurait prises. Enfin, le juge des référés du Conseil d'Etat a relevé que le ministre indiquait lui-même que les documents relatifs à du matériel technique saisis au domicile de M. A... dans le cadre de la perquisition s'étaient, après enquête, avérés avoir été édités par son frère, et adressés au requérant par l'un de ses cousins, dans le but de l'alerter sur les troubles mentaux dont souffrait ce frère. Au cours de l'instruction, le ministre avait toutefois ajouté que l'exploitation des matériels informatiques saisis au cours de la perquisition du 27 juillet 2016 avait mis en évidence la consultation sur Internet par M. A...de " vidéos de propagande djihadiste et anti-occidentale ". Le juge des référés du Conseil d'Etat a estimé qu'en l'état de l'instruction, et alors que l'enquête diligentée par les services de police judiciaire à raison des éléments résultant de l'exploitation des matériels informatiques de l'intéressé était toujours en cours, il n'apparaissait pas qu'en assignant M. A...à résidence, le ministre de l'intérieur ait porté une atteinte grave et manifestement illégale à sa liberté d'aller et de venir.<br/>
<br/>
              5. Par un arrêté du 20 décembre 2016, le ministre de l'intérieur a prolongé jusqu'à la fin de l'état d'urgence l'assignation à résidence de M.A.isolés et ne traduisent pas, en tout état de cause, un comportement de radicalisation du requérant M. A... relève appel de l'ordonnance du 17 février 2017 par laquelle le juge des référés du tribunal administratif de Lille, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté sa demande tendant à la suspension de l'exécution de cet arrêté. <br/>
<br/>
              Sur l'urgence :<br/>
<br/>
              6. Aux termes du second alinéa de l'article 14-1 de la loi du 3 avril 1955 : " La condition d'urgence est présumée satisfaite pour le recours juridictionnel en référé formé contre une mesure d'assignation à résidence ". Le ministre de l'intérieur n'a apporté aucun élément, dans ses écritures ou au cours de l'audience publique, de nature à remettre en cause, au cas d'espèce, la présomption d'urgence ainsi instituée par la loi.<br/>
<br/>
              Sur l'atteinte grave et manifestement illégale à une liberté fondamentale :<br/>
<br/>
              7. Il appartient au juge des référés de s'assurer, en l'état de l'instruction devant lui, que l'autorité administrative, opérant la conciliation nécessaire entre le respect des libertés et la sauvegarde de l'ordre public, n'a pas porté d'atteinte grave et manifestement illégale à une liberté fondamentale, que ce soit dans son appréciation de la menace que constitue le comportement de l'intéressé, compte tenu de la situation ayant conduit à la déclaration de l'état d'urgence, ou dans la détermination des modalités de l'assignation à résidence. Le juge des référés, s'il estime que les conditions définies à l'article L. 521-2 du code de justice administrative sont réunies, peut prendre toute mesure qu'il juge appropriée pour assurer la sauvegarde de la liberté fondamentale à laquelle il a été porté atteinte. <br/>
<br/>
              8. Le ministre de l'intérieur s'est uniquement fondé, pour prendre l'arrêté dont la suspension est demandée, sur les résultats de l'exploitation de divers supports numériques à la suite de la perquisition effectuée au domicile de M. A...le 27 juillet 2016, ayant mis en évidence " la présence de plusieurs dizaines de vidéos de propagande djihadiste et anti-occidentale ". De la lecture du procès-verbal dressé le 22 août 2016 par la direction interrégionale de la police judiciaire de Lille dans le cadre de l'enquête préliminaire aux fins d'éventuelles poursuites pour consultation habituelle de sites mettant à disposition des messages, images ou représentations provoquant directement à la commission d'actes de terrorisme, ou faisant l'apologie de ces actes, il ressort toutefois que l'exploitation de l'ordinateur portable de M. A...a fait apparaître que cet ordinateur ne contenait aucune vidéo ou photographie. Cependant, au cours de l'instruction, le ministre a expliqué que la prolongation de l'assignation à résidence de M. A... était justifiée par la consultation habituelle par l'intéressé de vidéos de cette nature sur Internet. <br/>
<br/>
              9. Il ressort de l'examen du procès-verbal dressé le 22 août 2016 que les services de police ont relevé sur l'ordinateur portable de M. A...de nombreuses connexions à YouTube, dont dix-huit en tout donnaient accès à des vidéos intitulées " 19HH ". Un second procès-verbal dressé le même jour, consacré spécifiquement à la présentation de ce que sont les vidéos " 19HH ", précise qu'elles sont qualifiées par le centre de prévention contre les dérives sectaires liées à l'Islam comme le principal point d'entrée dans l'autoradicalisation en ligne et que leur auteur est Omar Diaby, dit Omar Omsen, franco-sénégalais considéré comme l'un des principaux recruteurs de djihadistes français en Syrie. Le procès-verbal indique que ces vidéos " alternent prêches, mises en scène hollywoodiennes, films, documentaires, journaux ", et sont consacrées à Oussama Ben Laden, aux origines de l'univers selon le Coran, à la question palestinienne ou à la politique des puissances occidentales. Il est précisé que, dans ceux des documentaires qui en traitent, le recours au djihad y est présenté aux spectateurs comme le seul moyen de liberté, le sacrifice y étant glorifié. <br/>
<br/>
              10. D'une part, il ressort du premier procès-verbal dressé le 22 août 2016 qu'une connexion à YouTube donnant accès à une vidéo " 19HH " a été relevée le 30 mai 2015 sur l'ordinateur portable de M.A.isolés et ne traduisent pas, en tout état de cause, un comportement de radicalisation du requérant Si cinq autres connexions ont eu lieu deux semaines plus tard, le 10 juin 2015, c'est entre 20 heures 05 et 20 heures 11, durée qui rendait matériellement impossible le visionnage de plusieurs vidéos, dont la durée minimale est de quinze minutes, ainsi qu'il a été exposé au cours de l'audience. Le caractère ancien de ces connexions, isolées et qui n'ont pu donner lieu qu'à des consultations nécessairement partielles, ne permet pas d'établir l'existence de raisons sérieuses de penser que le comportement de M. A...constituait une menace grave pour la sécurité et l'ordre publics justifiant le renouvellement de son assignation à résidence par l'arrêté contesté du 20 décembre 2016.<br/>
<br/>
              11. D'autre part, si les services n'ont plus relevé aucune consultation de vidéos " 19HH " pendant plus d'un an, douze connexions à YouTube donnant accès à des vidéos de ce type ont eu lieu à partir du 12 juin 2016. Questionné au cours de l'audience publique sur la raison de ces consultations, M. A...a expliqué les avoir opérées à la suite de la diffusion par France 2, le 2 juin 2016, d'un numéro de l'émission " Complément d'enquête " consacré à Omar Diaby, dont une séquence portait sur les vidéos " 19HH " que ce dernier a réalisées. Le procès-verbal du 22 août 2016 fait apparaître que quatre connexions ont eu lieu le 12 juin 2016 entre 20 heures 36 et 20 heures 49, que quatre autres connexions ont eu lieu le 25 juin 2016 entre 20 heures 20 et 20 heures 23, que deux connexions ont eu lieu le 5 juillet 2016 à 20 heures 56 et que les deux dernières connexions ont eu lieu le 21 juillet 2016 à 20 heures 44. Le caractère très rapproché de ces connexions excluant, ici encore, le visionnage de plusieurs de ces vidéos, dont la durée minimale est de quinze minutes, il ressort de ces éléments que M. A... n'a pu, sur la période du 12 juin au 21 juillet 2016, procéder qu'à des consultations isolées de vidéos " 19HH ", consultations qui ne sauraient suffire à établir l'existence d'une radicalisation du requérant, dès lors que les services de police soulignent dans le procès-verbal susmentionné qu'il est impossible d'identifier quelles vidéos ont été consultées et que le requérant soutient, sans être utilement contredit, qu'il s'est borné à consulter des documentaires à caractère religieux. Si l'enquête a également révélé l'existence de plusieurs consultations de vidéos YouTube en rapport avec l'attentat commis à Nice le 14 juillet 2016, les services de police ont précisé dans leur procès-verbal que ces consultations portaient exclusivement sur des vidéos à caractère informatif et ne présentant pas de caractère illégal. Un troisième procès-verbal, dressé le 18 août 2016, révèle enfin que l'exploitation de la mémoire interne et de la carte SIM du téléphone portable de M. A...n'a permis la découverte d'aucun élément susceptible d'être en lien avec les faits pour lesquels l'enquête des services de police était réalisée, et que sur 932 discussions sauvegardées entre le 1er avril 2015 et le 8 janvier 2016, les échanges à caractère religieux demeurent.isolés et ne traduisent pas, en tout état de cause, un comportement de radicalisation du requérant Par suite, ces éléments, tout comme ceux relevés au point 10, ne permettent pas d'établir l'existence de raisons sérieuses de penser que le comportement de M. A...constituait une menace grave pour la sécurité et l'ordre publics justifiant le renouvellement de son assignation à résidence par l'arrêté contesté du 20 décembre 2016.<br/>
<br/>
              12. Si le ministre souligne en défense qu'il existe des raisons sérieuses de penser que le comportement de M. F...constitue une menace pour la sécurité et l'ordre publics, il ressort des termes mêmes de l'arrêté contesté que cet élément ne fonde pas la mesure d'assignation à résidence de M.A..., mais la mesure accessoire à cette assignation, par laquelle il lui est interdit de se trouver en relation, directement ou indirectement, avec M. F..., lui-même assigné à résidence. Le ministre a, au demeurant, confirmé au cours de l'audience publique que les motifs de la prolongation de l'assignation à résidence de M. A...s'attachaient à se conformer, sur ce point, à la position exprimée par le juge des référés du Conseil d'Etat dans son ordonnance du 16 novembre 2016.<br/>
<br/>
              13. Par suite, il apparaît qu'en l'état de l'instruction, la prolongation de l'assignation à résidence de M. A...a porté et continue de porter une atteinte grave et manifestement illégale à sa liberté d'aller et venir, qui constitue une liberté fondamentale au sens des dispositions de l'article L. 521-2 du code de justice administrative.<br/>
<br/>
              14. Il résulte de tout ce qui précède que M. A...est fondé à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Lille a rejeté ses conclusions tendant à la suspension de l'exécution de l'arrêté du 20 décembre 2016.<br/>
<br/>
              15. M. A...a obtenu le bénéfice de l'aide juridictionnelle. Par suite, ses avocats peuvent se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que les avocats de M. A...renoncent à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat, à leur profit, la somme de 2 000 euros au titre de la procédure devant le Conseil d'Etat et la somme de 1 200 euros au titre de la procédure devant le tribunal administratif de Lille.<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Lille du 17 février 2017 est annulée.<br/>
Article 2 : L'exécution de l'arrêté du 20 décembre 2016 du ministre de l'intérieur est suspendue.<br/>
Article 3 : L'Etat versera à la SCP Matuchansky, Poupot, Valdelièvre et à Me E...les sommes respectives de 2 000 euros et 1 200 euros au titre des dispositions de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative, sous réserve qu'ils renoncent à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente ordonnance sera notifiée à M. D...A...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
