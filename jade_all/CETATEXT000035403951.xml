<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035403951</ID>
<ANCIEN_ID>JG_L_2017_08_000000399320</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/40/39/CETATEXT000035403951.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 10/08/2017, 399320, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-08-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>399320</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LEVIS</AVOCATS>
<RAPPORTEUR>M. Vincent Uher</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:399320.20170810</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Batipro Logements Intermédiaires (BLI) a demandé au tribunal administratif de La Réunion de prononcer la décharge de la cotisation de taxe foncière sur les propriétés bâties restant en litige à laquelle elle a été assujettie, au titre de l'année 2012, à raison d'un immeuble d'habitation " Résidence Paradisier " dont elle est propriétaire dans la commune de Sainte-Marie (La Réunion). Par un jugement n° 1400821 du 28 janvier 2016, le tribunal a rejeté sa demande. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 29 avril et 29 juillet 2016 au secrétariat du contentieux du Conseil d'Etat, la société BLI demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 2014-1655 du 29 décembre 2014 ;  <br/>
              - la décision n° 2015-525 QPC du Conseil constitutionnel du 2 mars 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Uher, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lévis, avocat de la société Batipro Logements Intermédiaires.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que la société BLI a été assujettie, au titre de l'année 2012, à la taxe foncière sur les propriétés bâties à raison d'un immeuble d'habitation " Résidence Paradisier " dont elle est propriétaire dans la commune de Sainte-Marie (La Réunion). Elle se pourvoit en cassation contre le jugement du 28 janvier 2016 par lequel le tribunal administratif de La Réunion a rejeté sa demande tendant à la décharge de l'imposition restant en litige. <br/>
<br/>
              2. Aux termes de l'article 1496 du code général des impôts : " I. La valeur locative des locaux affectés à l'habitation ou servant à l'exercice d'une activité salariée à domicile est déterminée par comparaison avec celle de locaux de référence choisis, dans la commune, pour chaque nature et catégorie de locaux. /  II. La valeur locative des locaux de référence est déterminée d'après un tarif fixé, par commune ou secteur de commune, pour chaque nature et catégorie de locaux, en fonction du loyer des locaux loués librement à des conditions de prix normales et de manière à assurer l'homogénéité des évaluations dans la commune et de commune à commune. / Le tarif est appliqué à la surface pondérée du local de référence, déterminée en affectant la surface réelle de correctifs fixés par décret et destinés à tenir compte de la nature des différentes parties du local, ainsi que de sa situation, de son importance, de son état et de son équipement (...) ". <br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que la société BLI contestait le choix de l'administration, pour faire application des dispositions précitées, de retenir comme terme de comparaison le local de référence n° 30 au motif que sa valeur avait été déterminée par comparaison du local de référence n° 21, alors que celui-ci avait fait l'objet d'un changement d'affectation constaté en 2005. <br/>
<br/>
              4. Pour écarter le moyen tiré de l'irrégularité du choix du local de référence retenu par l'administration, le tribunal administratif s'est fondé sur les dispositions du III de l'article 32 de la loi du 29 décembre 2014 de finances rectificative pour 2014 aux termes duquel : " Sous réserve des décisions de justice passées en force de chose jugée, pour la détermination de la valeur locative des locaux mentionnés à l'article 1496 du code général des impôts et de ceux évalués en application du 2° de l'article 1498 du même code, sont validées les évaluations réalisées avant le 1er janvier 2015 en tant que leur légalité serait contestée au motif que, selon le cas, le local de référence ou le local-type ayant servi de terme de comparaison, soit directement, soit indirectement, a été détruit ou a changé de consistance, d'affectation ou de caractéristiques physiques ". <br/>
<br/>
              5. Toutefois, le Conseil constitutionnel a déclaré ces dispositions contraires à la Constitution par sa décision n° 2015-525 QPC du 2 mars 2016, dont le point 12 précise que cette déclaration d'inconstitutionnalité prend effet à compter de la date de la publication de la décision et qu'elle peut être invoquée dans toutes les instances introduites à cette date et non jugées définitivement. Doivent être entendues comme de telles instances, pour l'application des décisions du Conseil constitutionnel qui déterminent les modalités d'application dans le temps des déclarations d'inconstitutionnalité qu'il prononce, celles qui n'ont pas donné lieu à des décisions devenus irrévocables. Il s'ensuit que la société requérante peut se prévaloir, dans la présence instance, y compris devant le Conseil d'Etat, juge de cassation, de la déclaration d'inconstitutionnalité prononcée par la décision du Conseil constitutionnel du 2 mars 2016.<br/>
<br/>
              6. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la société BLI est fondée à demander l'annulation du jugement qu'elle attaque. <br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement de la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>            D E C I D E :<br/>
                           --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de La Réunion du 28 janvier 2016 est annulé. <br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de La Réunion. <br/>
<br/>
Article 3 : L'Etat versera à la société Batipro Logements Intermédiaires une somme de 1 000 euros, au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société Batipro Logements Intermédiaires et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
