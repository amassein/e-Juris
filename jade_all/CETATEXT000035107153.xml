<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035107153</ID>
<ANCIEN_ID>JG_L_2017_07_000000396430</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/10/71/CETATEXT000035107153.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 05/07/2017, 396430</TITRE>
<DATE_DEC>2017-07-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396430</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CAPRON ; SCP THOUIN-PALAT, BOUCARD ; SCP BOULLOCHE ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:396430.20170705</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Eurovia Champagne-Ardenne et la société SCREG Est ont demandé au tribunal administratif de Châlons-en-Champagne de condamner le centre hospitalier Geneviève de Gaulle-Anthonioz de Saint-Dizier à leur verser la somme de 2 305 467,73 euros TTC en règlement du lot n° 1 " terrassement, VRD, espaces verts et aménagements paysagers " du marché passé en vue de la construction d'un nouvel hôpital. Elles ont d'autre part demandé la condamnation des sociétés Barbosa Vivier, maître d'oeuvre, et Artelia Bâtiment Industrie, titulaire de la mission " ordonnancement - pilotage - coordination ", à les indemniser des préjudices relatifs à la moins-value assainissement, à l'allongement des délais d'exécution, au problème d'entretien du chantier et au décalage des prestations. Par des conclusions reconventionnelles, le centre hospitalier a demandé au même tribunal de condamner le groupement de sociétés à l'indemniser des préjudices subis en raison des dysfonctionnements affectant l'ouvrage, en réservant le chiffrage aux résultats d'une expertise. Par un jugement n° 1100383 du 17 décembre 2013, le tribunal administratif de Châlons-en-Champagne a rectifié le solde du décompte général de ce marché en le portant à la somme de 114 107,29 euros TTC au crédit du groupement et rejeté les autres conclusions des parties.<br/>
<br/>
              Par un arrêt n°s 14NC00353, 14NC00435 du 26 novembre 2015, la cour administrative d'appel de Nancy a, sur appels du centre hospitalier et des sociétés Eurovia Champagne-Ardenne et SCREG Est, fixé le solde du marché à la somme de 109 549,96 euros TTC et rejeté le surplus des conclusions des parties.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 26 janvier et 27 avril 2016 au secrétariat du contentieux du Conseil d'Etat, les sociétés Eurovia Champagne-Ardenne et Colas Est, venue aux droits de la société SCREG Est, demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il a rejeté comme irrecevables leurs demandes dirigées contre la société Barbosa Vivier Architectes et la société Artelia Bâtiment et Industrie ;<br/>
<br/>
              2°) de mettre à la charge des sociétés Barbosa Vivier Architectes et Artelia Bâtiment et Industrie la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la société Eurovia Champagne-Ardenne et de la société Colas Est, à la SCP Boulloche, avocat de la société Barbosa vivier, à la SCP Capron, avocat de la société Crédit agricole immobilier promotion et à la SCP Thouin-Palat, Boucard, avocat de la société Edeis anciennement dénommée SNC-Lavalin.<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que le centre hospitalier Geneviève de Gaulle-Anthonioz de Saint-Dizier a décidé en 2003 de procéder à la construction d'un nouvel hôpital ; qu'à cette fin, il a passé un marché de travaux à lots dont le lot n° 1 " Terrassement, VRD, espaces verts, aménagements " a été attribué à un groupement composé des sociétés Eurovia Champagne-Ardenne et SCREG Est ; que la maîtrise d'oeuvre de l'opération a été confiée à un groupement solidaire composé notamment de la société Barbosa Vivier Architectes et du cabinet Trouvin, aux droits duquel vient la société SAS EDEIS ; que la mission " ordonnancement - pilotage - coordination " (OPC) est revenue à la société GPCI, aux droits de laquelle vient la société Artelia Bâtiment et Industrie ; qu'un litige s'étant élevé sur le décompte général, la société Eurovia Champagne-Ardenne et la société SCREG Est ont demandé au tribunal administratif de Châlons-en-Champagne de condamner le centre hospitalier de Saint-Dizier à leur verser la somme de 2 305 467,73 euros TTC au titre du règlement de leur marché ou, à titre subsidiaire, de condamner les sociétés Barbosa Vivier, maître d'oeuvre, et Artelia Bâtiment et Industrie, titulaire de la mission " ordonnancement - pilotage - coordination " ; que, par un jugement du 17 décembre 2013, le tribunal administratif de Châlons-en-Champagne a rectifié le solde du décompte général de ce marché et rejeté les autres conclusions des parties ; que, par un arrêt du 26 novembre 2015, la cour administrative d'appel de Nancy a réformé ce jugement en réduisant le solde du marché et rejeté le surplus des conclusions des parties ; que le société Eurovia Champagne-Ardenne et la société Colas Est, venue aux droits de la société SCREG Est, se pourvoient en cassation contre cet arrêt en tant qu'il juge irrecevables leurs conclusions dirigées contre les sociétés Barbosa Vivier Architectes et Artelia Bâtiment et Industrie ;<br/>
<br/>
              2. Considérant que, dans le cadre d'un contentieux tendant au règlement d'un marché relatif à des travaux publics, le titulaire du marché peut rechercher, outre la responsabilité contractuelle du maître d'ouvrage, la responsabilité quasi-délictuelle des autres participants à la même opération de construction avec lesquels il n'est lié par aucun contrat de droit privé ;<br/>
<br/>
              3. Considérant que, pour rejeter comme irrecevables les conclusions indemnitaires que, en plus de leur demande, dirigée contre la maître d'ouvrage et tendant, sur le terrain contractuel, au règlement du marché, les sociétés Eurovia Champagne-Ardenne et SCREG Est ont présentées en première instance contre le maître d'oeuvre et le titulaire de la mission " ordonnancement - pilotage - coordination ", la cour administrative d'appel de Nancy a relevé que si, dans le cadre d'un contentieux tendant au règlement d'un marché relatif à des travaux publics, le titulaire du marché peut appeler en garantie des tiers au contrat, il n'est pas recevable à demander à titre principal la condamnation de tiers, sur le terrain quasi-délictuel, à réparer les préjudices qu'ils lui ont causés, sans tenir compte du fait que ces tiers participaient à la même opération de travaux ; qu'il résulte de ce qui a été dit au point 2 ci-dessus qu'elle a ainsi, en jugeant que les différentes conclusions des sociétés Eurovia Champagne-Ardenne et SCREG Est n'entretenaient pas entre elles un lien suffisant, commis une erreur de droit et une erreur de qualification juridique ; que, dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé en tant qu'il rejette comme irrecevables les conclusions indemnitaires dirigées par le groupement des entrepreneurs contre les sociétés Barbosa Vivier Architectes et Artelia Bâtiment et Industrie ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge des sociétés Barbosa Vivier Architectes et Artelia Bâtiment et Industrie la somme globale de 1 500 euros à verser chacune aux sociétés Eurovia Champagne-Ardenne et Colas Est, au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, les mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge des sociétés Eurovia Champagne-Ardenne et Colas Est qui ne sont pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 26 novembre 2015 est annulé en tant qu'il rejette comme irrecevables les conclusions indemnitaires présentées par les sociétés Eurovia Champagne-Ardenne et Colas Est contre les sociétés Barbosa Vivier Architectes et Artelia Bâtiment et Industrie.<br/>
Article 2 : L'affaire est renvoyée, dans la limite de la cassation ainsi prononcée, à la cour administrative d'appel de Nancy.<br/>
Article 3 : Les sociétés Barbosa Vivier Architectes et Artelia Bâtiment et Industrie verseront chacune aux sociétés Eurovia Champagne-Ardenne et Colas Est la somme globale de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la SAS EDEIS et de la société Barbosa Vivier Architectes présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée aux sociétés Eurovia Champagne-Ardenne et Colas Est et aux sociétés Barbosa Vivier Architectes, SAS EDEIS et Crédit agricole immobilier promotion.<br/>
Copie en sera adressée au centre hospitalier Geneviève de Gaulle-Anthonioz de Saint-Dizier et à la société Artélia Bâtiment et Industrie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-06 MARCHÉS ET CONTRATS ADMINISTRATIFS. RAPPORTS ENTRE L'ARCHITECTE, L'ENTREPRENEUR ET LE MAÎTRE DE L'OUVRAGE. - POSSIBILITÉ POUR L'ENTREPRENEUR DE RECHERCHER LA RESPONSABILITÉ QUASI-DÉLICTUELLE DU MAÎTRE D'OEUVRE AVEC LEQUEL IL N'EST PAS LIÉ PAR CONTRAT DANS LE CADRE D'UN CONTENTIEUX TENDANT AU RÈGLEMENT D'UN MARCHÉ RELATIF À DES TRAVAUX PUBLICS - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-08-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. RECEVABILITÉ. - CONCLUSIONS DU TITULAIRE D'UN MARCHÉ RELATIF À DES TRAVAUX PUBLICS TENDANT À L'ENGAGEMENT DE LA RESPONSABILITÉ QUASI-DÉLICTUELLE DES AUTRES PARTICIPANTS AUX OPÉRATIONS DE TRAVAUX AVEC LEQUEL IL N'EST PAS LIÉ PAR CONTRAT, PRÉSENTÉES DANS LE CADRE D'UN CONTENTIEUX TENDANT AU RÈGLEMENT DU MARCHÉ - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 39-06 Dans le cadre d'un contentieux tendant au règlement d'un marché relatif à des travaux publics, le titulaire du marché peut rechercher, outre la responsabilité contractuelle du maître d'ouvrage, la responsabilité quasi-délictuelle des autres participants à la même opération de construction avec lesquels il n'est lié par aucun contrat de droit privé.</ANA>
<ANA ID="9B"> 39-08-01 Dans le cadre d'un contentieux tendant au règlement d'un marché relatif à des travaux publics, le titulaire du marché peut rechercher, outre la responsabilité contractuelle du maître d'ouvrage, la responsabilité quasi-délictuelle des autres participants à la même opération de construction avec lesquels il n'est lié par aucun contrat de droit privé.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, 24 juillet 1981, Société générale d'entreprise, n° 13519, T. pp. 815-816-819.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
