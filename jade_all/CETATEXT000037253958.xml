<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037253958</ID>
<ANCIEN_ID>JG_L_2018_07_000000408806</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/25/39/CETATEXT000037253958.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 26/07/2018, 408806</TITRE>
<DATE_DEC>2018-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408806</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:408806.20180726</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme D...F..., agissant en son nom personnel et en qualité de représentant légal de sa fille mineureA..., M. B...F..., Mme E...F...et Mme C... F...ont saisi le tribunal administratif de Clermont-Ferrand d'une demande tendant à la condamnation du centre hospitalier du Puy-en-Velay à réparer les conséquences dommageables de l'infirmité présentée par A...F.... Par un jugement n° 1101540 du 16 décembre 2014, le tribunal administratif de Clermont-Ferrand a condamné le centre hospitalier du Puy-en-Velay à verser, d'une part, à Mme D...F..., en qualité de représentant légal de A...F..., la somme de 93 900 euros ainsi que, pour les périodes passées au domicile familial jusqu'à la majorité de l'enfant au prorata du nombre de nuits que l'enfant aura passées au domicile familial, une rente calculée sur la base d'un taux quotidien fixé à 108 euros versée par trimestres échus et, d'autre part, à Mme D...F...au titre de son préjudice propre la somme de 36 000 euros, à M. B...et Mme E...F...et à Mme C... F...la somme de 5 000 euros chacun.<br/>
<br/>
              Par un arrêt n° 15LTY00519 du 10 janvier 2017, la cour administrative d'appel de Lyon a, sur appel du centre hospitalier du Puy-en-Velay et appel incident des consortsF..., porté à 468 504 euros la somme due à Mme F... au titre de l'assistance d'une tierce personne à domicile, pour la période comprise entre le 24 février 2001 et le 31 décembre 2016, à 30 % du montant représentatif de la prise en charge au domicile de A...déterminé sur la base d'un taux quotidien fixé à 280 euros l'indemnité due à Mme F...au titre des frais liés au handicap à compter du 1er janvier 2017 et jusqu'à la majorité de l'enfant, au prorata du nombre de jours passés au domicile familial, et ramené à 12 000 euros la somme mise à la charge du centre hospitalier du Puy-en-Velay en réparation des préjudices personnels de Mme F....<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 10 mars 2017, 12 juin 2017 et 31 mai 2018 au secrétariat du contentieux du Conseil d'Etat, Mme F...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge du centre hospitalier du Puy-en-Velay la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de Mme F...et à Me Le Prado, avocat du centre hospitalier du Puy-en-Velay.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que A...F...est née le 24 janvier 2001 au centre hospitalier du Puy-en-Velay ; qu'elle est depuis lors atteinte de troubles neurologiques sévères, de troubles visuels, cognitifs et intellectuels et d'un important retard staturo-pondéral en rapport avec une leucomalacie périventriculaire ; que Mme D...F..., sa mère, imputant ces dommages à des fautes du centre hospitalier, a recherché la responsabilité de l'établissement, en son nom propre et au nom de sa fille, devant le tribunal administratif de Clermont-Ferrand ; que le tribunal administratif, par un jugement du 16 décembre 2014, a retenu l'existence de fautes du centre hospitalier du Puy-en-Velay dans la prise en charge de la grossesse de Mme F..., à l'origine d'une perte de chance pour son enfant d'échapper à son infirmité ; qu'ayant évalué cette perte de chance à 30 %, il a condamné l'établissement à verser à Mme F..., en sa qualité de représentant légal de son enfant et en son nom propre, des indemnités et une rente réparant 30 % des préjudices subis ; que, sur appel du centre hospitalier du Puy-en-Velay et appel incident de MmeF..., la cour administrative d'appel de Lyon a, par un arrêt du 10 janvier 2017, également retenu l'existence de fautes ayant entraîné une perte de chance de 30 %, modifié les montants des indemnités et de la rente mises à la charge du centre hospitalier et prévu que les sommes dues à l'enfant seraient versées sous déduction des sommes perçues au titre de l'allocation pour l'éducation de l'enfant handicapé ; que Mme F...se pourvoit en cassation contre cet arrêt ; que le centre hospitalier du Puy-en-Velay forme un pourvoi incident ;<br/>
<br/>
              Sur le pourvoi incident :<br/>
<br/>
              2. Considérant, en premier lieu, que la cour administrative d'appel a relevé, sans dénaturer les pièces du dossier qui lui était soumis, qu'il résultait, notamment, du rapport d'expertise établi le 15 février 2013 par les docteurs Rudigoz et Labaune, d'une part, que des examens échographiques pratiqués le 8 décembre 2000 avaient mis en évidence une croissance foetale limite pouvant laisser suspecter une hypotrophie foetale, suspicion qui justifiait des échographies ultérieures plus poussées, et, d'autre part, que si les manoeuvres de version pratiquées le 15 janvier 2001 avaient été effectuées conformément aux règles de l'art, l'enregistrement du rythme cardiaque foetal, qui avait mis en évidence une bradycardie, avait été interrompu à 10 h 47 alors que ce rythme n'était pas redevenu strictement normal ; qu'en jugeant, au vu de ces éléments, que le centre hospitalier du Puy-en-Velay avait commis, dans la prise en charge de la grossesse de Mme F..., des manquements fautifs tenant à l'absence d'examens échographiques après l'examen pratiqué le 8 décembre 2000 et à l'arrêt précoce de l'enregistrement du rythme cardiaque foetal lors des manoeuvres de version, la cour a exactement qualifié les faits de l'espèce ; <br/>
<br/>
              3. Considérant, en deuxième lieu, que la cour a retenu, pour estimer que ces manquements étaient pour A...F...directement à l'origine d'une perte de chance, qu'elle a évaluée à 30 %, d'obtenir une amélioration de son état de santé ou d'échapper à son aggravation, que, s'il n'était pas certain que le dommage ne serait pas advenu si des examens échographiques avaient été pratiqués après le 8 décembre 2000 et avaient conduit à une naissance plus précoce, ni qu'il ne serait pas advenu en l'absence de version par manoeuvres externes ou en cas de prise en charge adaptée de la grossesse à la faveur d'une surveillance régulière du rythme cardiaque foetal après ces manoeuvres, il n'était pas établi que les lésions cérébrales étaient irréversiblement acquises dans toute leur ampleur à la date de l'échographie ou au moment de la tentative de version ; que l'arrêt attaqué, qui est suffisamment motivé sur ce point, ne procède pas d'une dénaturation des pièces du dossier et n'est entaché ni d'une erreur de qualification juridique ni d'une erreur de droit en ce qui concerne les règles gouvernant l'indemnisation d'une perte de chance ;<br/>
<br/>
              4. Considérant, en troisième lieu, qu'il ressort des pièces du dossier soumis aux juges du fond que les experts qu'ils ont désignés ont constaté que le déficit fonctionnel permanent dont A...F...demeure atteinte ne saurait être inférieur à 80 % et nécessitera l'assistance d'une tierce personne sa vie durant ; que, compte tenu de la nature de son handicap, la cour administrative d'appel de Lyon n'a pas dénaturé les faits de l'espèce et n'a pas commis d'erreur de droit en retenant, par une motivation suffisante, qu'il y avait lieu d'indemniser le préjudice résultant des besoins en assistance de la victime à compter de la fin de son hospitalisation, le 24 février 2001 ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que le pourvoi incident présenté par le centre hospitalier du Puy-en-Velay doit être rejeté ;<br/>
<br/>
              Sur le pourvoi principal :<br/>
<br/>
              6. Considérant qu'en vertu des principes qui régissent l'indemnisation par une personne publique des victimes d'un dommage dont elle doit répondre, il y a lieu de déduire de l'indemnisation allouée à la victime d'un dommage corporel au titre des frais d'assistance par une tierce personne le montant des prestations dont elle bénéficie par ailleurs et qui ont pour objet la prise en charge de tels frais ; qu'il en est ainsi alors même que les dispositions en vigueur n'ouvrent pas à l'organisme qui sert ces prestations un recours subrogatoire contre l'auteur du dommage ; que la déduction n'a toutefois pas lieu d'être lorsqu'une disposition particulière permet à l'organisme qui a versé la prestation d'en réclamer le remboursement au bénéficiaire s'il revient à meilleure fortune ;<br/>
<br/>
              7. Considérant que les règles rappelées au point précédent ne trouvent à s'appliquer que dans la mesure requise pour éviter une double indemnisation de la victime ; que, par suite, lorsque la personne publique responsable n'est tenue de réparer qu'une fraction du dommage corporel, notamment parce que la faute qui lui est imputable n'a entraîné qu'une perte de chance d'éviter ce dommage, la déduction ne se justifie, le cas échéant, que dans la mesure nécessaire pour éviter que le montant cumulé de l'indemnisation et des prestations excède le montant total des frais d'assistance par une tierce personne ;<br/>
<br/>
              8. Considérant, d'une part, qu'aux termes de l'article L. 541-1 du code de la sécurité sociale : " Toute personne qui assume la charge d'un enfant handicapé a droit à une allocation d'éducation de l'enfant handicapé, si l'incapacité permanente de l'enfant est au moins égale à un taux déterminé. / Un complément d'allocation est accordé pour l'enfant atteint d'un handicap dont la nature ou la gravité exige des dépenses particulièrement coûteuses ou nécessite le recours fréquent à l'aide d'une tierce personne. Son montant varie suivant l'importance des dépenses supplémentaires engagées ou la permanence de l'aide nécessaire. / (...) L'allocation d'éducation de l'enfant handicapé n'est pas due lorsque l'enfant est placé en internat avec prise en charge intégrale des frais de séjour par l'assurance maladie, l'Etat ou l'aide sociale, sauf pour les périodes de congés ou de suspension de la prise en charge " ; qu'il résulte de ces dispositions que l'allocation d'éducation de l'enfant handicapé est destinée à compenser les frais de toute nature liés au handicap et qu'elle peut faire l'objet d'un complément lorsque ces frais sont particulièrement élevés ou que l'état de l'enfant nécessite l'assistance fréquente d'une tierce personne ; <br/>
<br/>
              9. Considérant, d'autre part, qu'aucune disposition législative ou réglementaire ne prévoit la récupération de l'allocation d'éducation de l'enfant handicapé en cas de retour de son bénéficiaire à meilleure fortune ; <br/>
<br/>
              10. Considérant qu'il suit de là que le montant de l'allocation d'éducation de l'enfant handicapé et de son complément éventuel peut être déduit d'une rente ou indemnité allouée au titre de l'assistance par tierce personne ; qu'ainsi qu'il a été dit au point 7 ci-dessus, lorsque l'auteur de la faute n'est tenu de réparer qu'une fraction du dommage corporel, cette déduction n'a lieu d'être que lorsque le montant cumulé de l'indemnisation incombant normalement au responsable et de l'allocation et de son complément excéderait le montant total des frais d'assistance par une tierce personne ; que l'indemnisation doit alors être diminuée du montant de cet excédent ;<br/>
<br/>
              11. Considérant qu'en décidant que les sommes perçues par Mme F...au titre de l'allocation d'éducation de l'enfant handicapé et de son complément seraient déduites de l'indemnité qu'elle a allouée à son enfant au titre des frais passés d'assistance par une tierce personne et de la rente qu'elle lui a accordée au titre des frais futurs, sans vérifier si le montant cumulé de ces prestations et de l'indemnisation ainsi mise à la charge du centre hospitalier excédait le montant total des frais d'assistance, la cour administrative d'appel de Lyon n'a pas mis en oeuvre les principes rappelés aux points 6 à 10 ci-dessus et a ainsi entaché son arrêt d'une erreur de droit ;<br/>
<br/>
              12. Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin d'examiner les moyens du pourvoi principal, que l'arrêt de la cour administrative d'appel de Lyon doit être annulé en tant qu'il statue sur l'évaluation des préjudices subis par A...F...au titre de l'assistance par une tierce personne ;<br/>
<br/>
              13. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier du Puy-en-Velay une somme de 3 000 euros à verser à Mme F...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 10 janvier 2017 est annulé en tant qu'il statue sur l'évaluation des préjudices subis par A...F...au titre de l'assistance par une tierce personne.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon dans la mesure de la cassation prononcée.<br/>
<br/>
Article 3 : Le centre hospitalier du Puy-en-Velay versera à Mme F...une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
		Article 4 : Le surplus des conclusions de Mme F...est rejeté.<br/>
<br/>
		Article 5 : Le pourvoi du centre hospitalier du Puy-en-Velay est rejeté.<br/>
Article 6 : La présente décision sera notifiée à Mme D...F...et au centre hospitalier du Puy-en-Velay.<br/>
Copie en sera adressée à la ministre des solidarités et de la santé et à la caisse primaire d'assurance maladie du Puy-de-Dôme.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-04-03-07 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. ÉVALUATION DU PRÉJUDICE. MODALITÉS DE FIXATION DES INDEMNITÉS. - INDEMNITÉ ALLOUÉE AU TITRE DES FRAIS D'ASSISTANCE PAR UNE TIERCE PERSONNE - MONTANT - DÉDUCTION DES PRESTATIONS PRENANT EN CHARGE LES MÊMES FRAIS - 1) PRINCIPES - EXISTENCE - EXCEPTION - CAS OÙ L'ORGANISME DÉBITEUR DE LA PRESTATION PEUT EN RÉCLAMER LE REMBOURSEMENT EN CAS DE RETOUR À UNE MEILLEURE FORTUNE [RJ1] - 2) CAS DANS LEQUEL LA PERSONNE PUBLIQUE RESPONSABLE N'EST TENUE DE RÉPARER QU'UNE FRACTION DU DOMMAGE CORPOREL - ECRÊTEMENT DANS LA MESURE NÉCESSAIRE POUR ÉVITER QUE LE MONTANT CUMULÉ DE L'INDEMNISATION EXCÈDE LE MONTANT TOTAL DES FRAIS D'ASSISTANCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-04-04-02-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. MODALITÉS DE LA RÉPARATION. FORMES DE L'INDEMNITÉ. RENTE. - INDEMNITÉ ALLOUÉE AU TITRE DES FRAIS D'ASSISTANCE PAR UNE TIERCE PERSONNE - MONTANT - DÉDUCTION DES PRESTATIONS PRENANT EN CHARGE LES MÊMES FRAIS - 1) PRINCIPES - EXISTENCE - EXCEPTION - CAS OÙ L'ORGANISME DÉBITEUR DE LA PRESTATION PEUT EN RÉCLAMER LE REMBOURSEMENT EN CAS DE RETOUR À UNE MEILLEURE FORTUNE [RJ1] - 2) CAS DANS LEQUEL LA PERSONNE PUBLIQUE RESPONSABLE N'EST TENUE DE RÉPARER QU'UNE FRACTION DU DOMMAGE CORPOREL - ECRÊTEMENT DANS LA MESURE NÉCESSAIRE POUR ÉVITER QUE LE MONTANT CUMULÉ DE L'INDEMNISATION EXCÈDE LE MONTANT TOTAL DES FRAIS D'ASSISTANCE.
</SCT>
<ANA ID="9A"> 60-04-03-07 1) En vertu des principes qui régissent l'indemnisation par une personne publique des victimes des dommages dont elle doit répondre, il y a lieu de déduire de l'indemnisation allouée à la victime d'un dommage corporel au titre des frais d'assistance par une tierce personne le montant des prestations dont elle bénéficie par ailleurs et qui ont pour objet la prise en charge de tels frais. Il en est ainsi alors même que les dispositions en vigueur n'ouvrent pas à l'organisme qui sert ces prestations un recours subrogatoire contre l'auteur du dommage. La déduction n'a toutefois pas lieu d'être lorsqu'une disposition particulière permet à l'organisme qui a versé la prestation d'en réclamer le remboursement au bénéficiaire s'il revient à meilleure fortune.... ,,2) Les règles rappelées ci-dessus ne trouvent à s'appliquer que dans la mesure requise pour éviter une double indemnisation de la victime. Par suite, lorsque la personne publique responsable n'est tenue de réparer qu'une fraction du dommage corporel, notamment parce que la faute qui lui est imputable n'a entraîné qu'une perte de chance d'éviter ce dommage, la déduction ne se justifie, le cas échéant, que dans la mesure nécessaire pour éviter que le montant cumulé de l'indemnisation et des prestations excède le montant total des frais d'assistance par une tierce personne.</ANA>
<ANA ID="9B"> 60-04-04-02-01 1) En vertu des principes qui régissent l'indemnisation par une personne publique des victimes des dommages dont elle doit répondre, il y a lieu de déduire de l'indemnisation allouée à la victime d'un dommage corporel au titre des frais d'assistance par une tierce personne le montant des prestations dont elle bénéficie par ailleurs et qui ont pour objet la prise en charge de tels frais. Il en est ainsi alors même que les dispositions en vigueur n'ouvrent pas à l'organisme qui sert ces prestations un recours subrogatoire contre l'auteur du dommage. La déduction n'a toutefois pas lieu d'être lorsqu'une disposition particulière permet à l'organisme qui a versé la prestation d'en réclamer le remboursement au bénéficiaire s'il revient à meilleure fortune.... ,,2) Les règles rappelées ci-dessus ne trouvent à s'appliquer que dans la mesure requise pour éviter une double indemnisation de la victime. Par suite, lorsque la personne publique responsable n'est tenue de réparer qu'une fraction du dommage corporel, notamment parce que la faute qui lui est imputable n'a entraîné qu'une perte de chance d'éviter ce dommage, la déduction ne se justifie, le cas échéant, que dans la mesure nécessaire pour éviter que le montant cumulé de l'indemnisation et des prestations excède le montant total des frais d'assistance par une tierce personne.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 23 septembre 2013, Centre hospitalier de Saint-Etienne, n° 350799, T. pp. 432-839-840.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
