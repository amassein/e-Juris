<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042499870</ID>
<ANCIEN_ID>JG_L_2020_11_000000437697</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/49/98/CETATEXT000042499870.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 04/11/2020, 437697, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437697</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>Mme Pauline Berne</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:437697.20201104</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Toulouse, d'une part, d'annuler la décision du 1er juin 2015 par laquelle la rectrice de l'académie de Toulouse a considéré qu'il ne remplissait pas les conditions pour faire valoir ses droits à la retraite et, d'autre part, d'enjoindre à cette autorité de réexaminer sa situation dans un délai d'un mois à compter de la notification de la décision à intervenir, sous astreinte de 100 euros par jour de retard.<br/>
<br/>
              Par un jugement n° 1503983 du 20 décembre 2017, le tribunal administratif de Toulouse a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 18BX00448 du 16 janvier 2020, enregistré le même jour au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Bordeaux a transmis au Conseil d'Etat le pourvoi formé par M. B... contre ce jugement, en application de l'article R. 811-1 du code de justice administrative.<br/>
<br/>
              Par ce pourvoi, ainsi que par un mémoire complémentaire et un mémoire en réplique, enregistrés les 23 mars et 29 septembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Toulouse du 20 décembre 2017 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - la loi n° 2010-1330 du 9 novembre 2010 ;<br/>
              - le décret n° 2011-2103 du 30 décembre 2011 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Berne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A... B..., qui appartenait alors au corps des contrôleurs de la Poste, a intégré par la voie du détachement une école normale primaire le 3 octobre 1983, avant d'être titularisé en qualité d'instituteur le 1er septembre 1986 puis en qualité de professeur des écoles à compter du 1er septembre 1999. En réponse à sa demande tendant à connaître la date à compter de laquelle il pourrait faire valoir ses droits à la retraite, la rectrice de l'académie de Toulouse l'a informé, par un courrier du 19 mars 2019, que ses droits à pension seraient ouverts en 2021. Par un courrier du 9 mai 2015, M. B... a contesté cette réponse en faisant valoir qu'il bénéficiait du régime applicable aux fonctionnaires totalisant plus de quinze années de services effectués dans un emploi classé dans la catégorie active, en tenant compte des années passées à l'école normale primaire. Par un second courrier du 1er juin 2015, la rectrice a maintenu sa position. M. B... a demandé au tribunal administratif de Toulouse d'annuler ce courrier. Par un jugement du 20 décembre 2017, le tribunal administratif a rejeté cette demande. Par un arrêt du 16 janvier 2020, la cour administrative d'appel de Bordeaux a transmis au Conseil d'Etat le pourvoi de M. B... dirigé contre ce jugement. <br/>
<br/>
              Sur la fin de non-recevoir opposée par le ministre de l'éducation nationale et de la jeunesse :<br/>
<br/>
              2. Un fonctionnaire qui sollicite le bénéfice d'un départ anticipé à la retraite au titre des services actifs est en droit de demander à l'autorité administrative, préalablement à cette échéance, de se prononcer sur sa demande à partir du moment où sa situation au regard de ses droits à la retraite peut être utilement appréciée. En statuant sur une telle demande, l'autorité administrative ne se borne pas à donner un avis, mais prend une décision faisant grief, bien que celle-ci soit prise sous la condition que les éléments de droit et de fait sur lesquels elle se fonde n'auront pas changé à sa date d'effet. Le ministre de l'éducation nationale et de la jeunesse n'est, dès lors, pas fondé à soutenir que M. B... n'était pas recevable à demander l'annulation de la décision de la rectrice de l'académie de Toulouse du 1er juin 2015.<br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              3. Aux termes de l'article L. 24 du code des pensions civiles et militaires de retraite, dans sa rédaction applicable au litige : " I. - La liquidation de la pension intervient : / 1° Lorsque le fonctionnaire civil est radié des cadres par limite d'âge, ou s'il a atteint, à la date de l'admission à la retraite, l'âge mentionné à l'article L. 161-17-2 du code de la sécurité sociale, ou de cinquante-sept ans s'il a accompli au moins dix-sept ans de services dans des emplois classés dans la catégorie active. / Sont classés dans la catégorie active les emplois présentant un risque particulier ou des fatigues exceptionnelles. La nomenclature en est établie par décret en Conseil d'Etat ; (...) ". <br/>
<br/>
              4. Aux termes de l'article 35 de la loi du 9 novembre 2010 portant réforme des retraites : " I. - Les durées de services effectifs prévues au 1° du I (...) de l'article L. 24 du code des pensions civiles et militaires de retraite (...) dans leur rédaction antérieure à l'entrée en vigueur de la présente loi, pour la liquidation de la pension des fonctionnaires et des militaires sont fixées, à compter du 1er janvier 2016 : / (...) 2° A dix-sept ans lorsque cette durée était fixée antérieurement à quinze ans ; / (...) III. - Par dérogation, les I et II ne sont pas applicables aux fonctionnaires et aux militaires qui, après avoir effectué les durées de services effectifs mentionnées au I avant l'entrée en vigueur de la présente loi, soit ont été intégrés dans un corps ou un cadre d'emploi dont les emplois ne sont pas classés en catégorie active, soit ont été radiés des cadres ".<br/>
<br/>
              5. Il résulte des dispositions mentionnées au point 4 que l'allongement de quinze à dix-sept ans de la durée des services accomplis dans des emplois classés dans la catégorie active nécessaire pour pouvoir bénéficier d'un départ à la retraite anticipé en application de l'article L. 24 du code des pensions civiles et militaires de retraite ne s'applique pas, notamment, aux fonctionnaires qui, après avoir accompli la durée de quinze années de services effectifs qui était prévue avant l'entrée en vigueur de cette loi, ont été intégrés dans un corps dont les emplois ne sont pas classés en catégorie active. Pour rejeter la demande de M. B..., le tribunal administratif de Toulouse a jugé qu'à supposer qu'il pût se prévaloir d'une ancienneté de quinze années, dix mois et vingt-sept jours dans un emploi de la catégorie active, comme il le soutenait, il ne comptabilisait pas les dix-sept années exigées par les dispositions de l'article L. 24 du code des pensions civiles et militaires de retraite. En statuant ainsi, sans tenir compte de la dérogation prévue par le III de l'article 35 de la loi du 9 novembre 2010 portant réforme des retraites et sans rechercher si M. B... remplissait les conditions posées par cette dernière disposition, le tribunal administratif de Toulouse a commis une erreur de droit. <br/>
<br/>
              6. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, M. B... est fondé à demander l'annulation du jugement du tribunal administratif de Toulouse qu'il attaque.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à M. B... d'une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement n° 1503983 du tribunal administratif de Toulouse du 20 décembre 2017 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Toulouse. <br/>
<br/>
Article 3 : L'Etat versera à M. B... une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A... B... et au ministre de l'éducation nationale, de la jeunesse et des sports.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
