<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033132165</ID>
<ANCIEN_ID>JG_L_2016_09_000000384197</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/13/21/CETATEXT000033132165.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 19/09/2016, 384197</TITRE>
<DATE_DEC>2016-09-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384197</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUTET, HOURDEAUX</AVOCATS>
<RAPPORTEUR>M. Etienne de Lageneste</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:384197.20160919</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme A...B...ont demandé au tribunal administratif de Cergy-Pontoise de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles ils ont été assujettis au titre des années 2003 et 2004. Par un jugement n° 0903201 du 30 mai 2012, le tribunal administratif, après avoir prononcé un non-lieu à statuer à hauteur des dégrèvements accordés en cours d'instance et sur les conclusions relatives au sursis de paiement, a rejeté le surplus de la demande.<br/>
<br/>
              Par un arrêt n° 12VE02836 du 1er juillet 2014, la cour administrative d'appel de Versailles s'est déclarée incompétente s'agissant des conclusions de M. et Mme B...tendant à la communication de pièces saisies par l'autorité judiciaire, a partiellement fait droit à leur requête et a rejeté le surplus de leurs conclusions.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 4 septembre et 28 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, M. et Mme B... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt en tant qu'il n'a pas fait droit à l'intégralité de leurs conclusions ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire entièrement droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code pénal ; <br/>
              - le code de procédure pénale ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Etienne de Lageneste, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boutet, Hourdeaux, avocat de M. et  Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Man Sécurité a fait l'objet d'une vérification de comptabilité et que M.B..., gérant de la société, a fait l'objet d'un examen de sa situation fiscale personnelle ; qu'à l'issue de ces contrôles, l'administration a établi des cotisations supplémentaires d'impôt sur le revenu et de cotisations sociales au titre des années 2003 et 2004 ; que M. et Mme B... ont saisi le tribunal administratif de Cergy-Pontoise qui, après avoir constaté un non-lieu à statuer à hauteur des dégrèvements prononcés en cours d'instance, a rejeté le surplus de leur demande ; qu'ils demandent l'annulation de l'arrêt du 1er juillet 2014 en tant que la cour administrative d'appel de Versailles, qui a partiellement fait droit à leur requête, a rejeté leurs conclusions tendant à la communication de pièces saisies par l'autorité judiciaire comme portées devant une juridiction incompétente pour en connaître ainsi que le surplus de leurs conclusions tendant à la décharge des impositions ;<br/>
<br/>
              Sur la compétence du juge administratif :<br/>
<br/>
              2. Considérant qu'aux termes de l'article 478 du code de procédure pénale : " Le prévenu, la partie civile ou la personne civilement responsable, peut réclamer au tribunal saisi de la poursuite la restitution des objets placés sous la main de la justice. / Le tribunal peut ordonner d'office cette restitution " ; qu'aux termes de l'article 482 du même code : " Le jugement qui rejette une demande de restitution est susceptible d'appel de la part de la personne qui a formé cette demande (...) " ; qu'aux termes de l'article 131-21 du code pénal : " La peine complémentaire de confiscation est encourue dans les cas prévus par la loi ou le règlement (...). La confiscation porte sur tous les biens meubles ou immeubles, quelle qu'en soit la nature, divis ou indivis, ayant servi à commettre l'infraction ou qui étaient destinés à la commettre, et dont le condamné est propriétaire ou, sous réserve des droits du propriétaire de bonne foi, dont il a la libre disposition " ; qu'aux termes de l'article 710 du code de procédure pénale : " Tous incidents contentieux relatifs à l'exécution sont portés devant le tribunal ou la cour qui a prononcé la sentence " ; qu'en application de ces dispositions, le juge pénal statue sur la restitution ou la confiscation des pièces, documents ou objets saisis et placés sous scellé au cours de la procédure pénale ; qu'il ne relève pas de la compétence du juge administratif d'enjoindre au procureur de la République de restituer à un requérant les pièces, documents ou objets confisqués par le juge pénal ni même de les lui communiquer ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un jugement du 13 mai 2009, le tribunal correctionnel de Pontoise a condamné M. B... pour abus de biens sociaux, faux, usage de faux et exécution de travail dissimulé et a ordonné la confiscation des scellés de la procédure, à l'exception du scellé n° 19, qui, à sa demande, a été restitué à M.B... ; que la cour n'a pas commis d'erreur de droit en rejetant, comme portées devant une juridiction incompétente pour en connaître, les conclusions tendant à ce qu'elle enjoigne au procureur de la République de communiquer les pièces confisquées par le juge pénal ;<br/>
<br/>
              Sur la régularité de l'arrêt :<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu' après l'expiration du délai d'appel, les requérants ont soulevé pour la première fois un moyen tiré de l'irrégularité du jugement du tribunal administratif ; qu'en jugeant que ce moyen, qui n'est pas d'ordre public, se rattachait à une cause juridique distincte de celle dont relevaient les moyens soulevés avant l'expiration du délai d'appel, portant sur le bien-fondé des impositions, et, par suite, devait être rejeté comme irrecevable, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              Sur le bien-fondé de l'arrêt s'agissant de la régularité de la procédure d'imposition :<br/>
<br/>
              5. Considérant qu'aux termes de l'article L. 16 du livre des procédures fiscales : " En vue de l'établissement de l'impôt sur le revenu, l'administration peut demander au contribuable des éclaircissements. (...) Elle peut également lui demander des justifications lorsqu'elle a réuni des éléments permettant d'établir que le contribuable peut avoir des revenus plus importants que ceux qu'il a déclarés " ;<br/>
<br/>
              6. Considérant, en premier lieu, que l'administration ne peut régulièrement demander au contribuable de lui apporter des justifications sur le fondement de ces dispositions sans lui restituer tous documents utiles à cet effet et qui lui auraient été antérieurement remis par l'intéressé ; que, toutefois, si la cour a relevé que l'administration fiscale ne justifiait pas avoir restitué des attestations de gains du casino d'Enghien-les-Bains et des bulletins de paie remis lors d'un entretien avec le vérificateur le 26 avril 2006, elle a, par un arrêt suffisamment motivé et exempt de dénaturation, souverainement apprécié les faits qui lui étaient soumis en ne regardant pas ces pièces comme utiles aux requérants pour faire valoir leurs droits; qu'en relevant qu'il n'était pas allégué que les tableaux de synthèse remis au vérificateur par les requérants auraient été des documents uniques dont les intéressés n'étaient pas en mesure de disposer pour répondre à la demande faite sur le fondement de l'article L. 16 du livre des procédures fiscales, la cour n'a pas méconnu les règles gouvernant l'attribution de la charge de la preuve ;<br/>
<br/>
              7. Considérant, en deuxième lieu, que si l'article L. 16 du livre des procédures fiscales, qui permet à l'administration de comparer les crédits figurant sur les comptes d'un contribuable au montant brut de ses revenus déclarés pour établir l'existence d'indices de revenus dissimulés l'autorisant à demander des justifications, ne l'oblige pas à procéder à un examen critique préalable de ces crédits, ni, quand elle l'a fait, à se référer comme terme de comparaison aux seuls crédits dont l'origine n'est pas justifiée après le premier examen, elles ne la dispensent pas de neutraliser, afin de déterminer le montant total des crédits à prendre en compte pour procéder à cette comparaison, les virements de compte à compte de l'intéressé ; que la cour a relevé que, pour déterminer le montant des crédits figurant sur les comptes de M. et MmeB..., l'administration avait tenu compte d'une somme de 75 780 euros portée au crédit du compte courant d'associé de M. B...dans les écritures de la société Man Sécurité, alors même qu'une prime de bilan, d'un montant équivalent, avait été comptabilisée au titre des salaires de l'intéressé ; qu'en jugeant que l'administration n'était pas tenue, dans sa recherche d'indices de revenus dissimulés, de neutraliser cette somme du montant total des crédits figurant sur les comptes de M. et MmeB..., la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              8. Considérant, en dernier lieu, qu'en jugeant que les requérants n'assortissaient pas de précisions suffisantes permettant d'apprécier le bien-fondé du moyen tiré de ce que l'administration ne pouvait adresser une demande de justification en induisant les contribuables en erreur sur l'étendue de leurs obligations, la cour, par un arrêt suffisamment motivé, n'a pas commis d'erreur de droit ;<br/>
<br/>
              Sur le bien-fondé de l'arrêt s'agissant du bien-fondé des impositions :<br/>
<br/>
              9. Considérant, en premier lieu, que la cour a relevé que la vérification de comptabilité de la société Man Sécurité et la consultation du dossier pénal de M. B...avaient permis à l'administration de constater que M. et Mme B...avaient encaissé sur leurs comptes bancaires personnels des chèques établis par des clients de la société ainsi que des chèques tirés sur les comptes bancaires de la société Man Sécurité ; que les sommes ainsi encaissées ont été regardées par l'administration comme des revenus distribués, desquels ont été déduits des débits correspondants à des charges, notamment salariales, incombant à la société et acquittées par les requérants ; que, devant la cour, les requérants soutenaient que certaines sommes auraient dû être admises en déduction au titre des charges de la société dès lors qu'elles  avaient un caractère professionnel et produisaient, à l'appui de leurs allégations, plusieurs pièces, notamment des talons de chéquiers et des tableaux élaborés à partir du dépouillement des chéquiers ; qu'en estimant que ces pièces ne permettaient pas d'établir l'identité réelle des bénéficiaires des chèques en cause et, par suite, le caractère professionnel de la dépense, la cour n'a pas dénaturé les pièces du dossier qui lui était soumis ;<br/>
<br/>
              10. Considérant, en deuxième lieu, qu'en jugeant que, s'agissant des chèques libellés au nom de personnes morales, l'administration pouvait estimer, en l'absence d'autres éléments, notamment de factures, que la seule identification du bénéficiaire ne permettait pas d'établir le caractère professionnel de la dépense en cause, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              11. Considérant, en troisième lieu, que, devant la cour, les requérants faisaient valoir que les sommes de 6 300 euros et 4 700 euros, imposées au titre de revenus d'origine indéterminée respectivement des années 2003 et 2004 correspondaient à des gains de casino ; qu'en jugeant, par une appréciation souveraine non arguée de dénaturation, que ni les retraits en espèce effectués parfois plusieurs semaines avant la date des crédits en litige, ni la production de tickets d'entrée au casino d'Enghien-les-Bains n'étaient suffisants pour établir l'origine et la nature de ces crédits, la cour, qui a statué de manière suffisamment motivé, n'a pas commis d'erreur de droit ;<br/>
<br/>
              12. Considérant, en dernier lieu, en estimant, par une appréciation souveraine non arguée de dénaturation, d'une part, que l'attestation produite par M. B... ne permettait pas, à elle seule, de justifier de l'origine et de la nature du crédit de 10 000 euros crédité sur son compte le 9 décembre 2004, d'autre part, que les relevés de compte bancaire de son père ne justifiaient pas davantage de l'origine de la somme, la cour n'a ni méconnu les règles gouvernant l'attribution de la charge de la preuve, ni commis d'erreur de droit ;<br/>
<br/>
              13. Considérant qu'il résulte de tout ce qui précède que les requérants ne sont pas fondés à demander l'annulation de l'arrêt qu'ils attaquent en tant qu'il n'a pas fait droit à l'intégralité de leurs conclusions ; que, par suite, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. et Mme B...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. et Mme A...B...et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03-01-02-05 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR DES TEXTES SPÉCIAUX. ATTRIBUTIONS LÉGALES DE COMPÉTENCE AU PROFIT DES JURIDICTIONS JUDICIAIRES. DIVERS CAS D`ATTRIBUTIONS LÉGALES DE COMPÉTENCE AU PROFIT DES JURIDICTIONS JUDICIAIRES. - CONFISCATION DE PIÈCES PAR LE JUGE PÉNAL - COMPÉTENCE DU JUGE ADMINISTRATIF POUR ENJOINDRE AU PROCUREUR DE RESTITUER OU COMMUNIQUER CES PIÈCES - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-03-02-07-05-02 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. PROBLÈMES PARTICULIERS POSÉS PAR CERTAINES CATÉGORIES DE SERVICES PUBLICS. SERVICE PUBLIC JUDICIAIRE. FONCTIONNEMENT. - CONFISCATION DE PIÈCES PAR LE JUGE PÉNAL - COMPÉTENCE DU JUGE ADMINISTRATIF POUR ENJOINDRE AU PROCUREUR DE RESTITUER OU COMMUNIQUER CES PIÈCES - ABSENCE.
</SCT>
<ANA ID="9A"> 17-03-01-02-05 En application des articles 478, 482 et 710 du code de procédure pénale ainsi que de l'article 131-21 du code pénal, le juge pénal statue sur la restitution ou la confiscation des pièces, documents ou objets saisis et placés sous scellé au cours de la procédure pénale. Il ne relève pas de la compétence du juge administratif d'enjoindre au procureur de la République de restituer à un requérant les pièces, documents ou objets confisqués par le juge pénal ni même de les lui communiquer.</ANA>
<ANA ID="9B"> 17-03-02-07-05-02 En application des articles 478, 482 et 710 du code de procédure pénale ainsi que de l'article 131-21 du code pénal, le juge pénal statue sur la restitution ou la confiscation des pièces, documents ou objets saisis et placés sous scellé au cours de la procédure pénale. Il ne relève pas de la compétence du juge administratif d'enjoindre au procureur de la République de restituer à un requérant les pièces, documents ou objets confisqués par le juge pénal ni même de les lui communiquer.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
