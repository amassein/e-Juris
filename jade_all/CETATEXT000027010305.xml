<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027010305</ID>
<ANCIEN_ID>JG_L_2013_01_000000352206</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/01/03/CETATEXT000027010305.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 30/01/2013, 352206, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-01-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352206</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Matthieu Schlesinger</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:352206.20130130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 26 août 2011 au secrétariat du contentieux du Conseil d'Etat, présentée pour l'Association nationale des opérateurs détaillants en énergie, dont le siège est 1 boulevard Malesherbes c/o Athenor Public Affairs à Paris (75008) ; l'Association nationale des opérateurs détaillants en énergie demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 27 juin 2011 de la ministre de l'économie, des finances et de l'industrie et du ministre auprès de la ministre de l'économie, des finances, et de l'industrie, chargé de l'industrie, de l'énergie, et de l'économie numérique relatif aux tarifs réglementés de vente du gaz naturel fourni à partir des réseaux publics de distribution de GDF Suez ;<br/>
<br/>
              2°) d'enjoindre aux ministres compétents d'adopter un nouvel arrêté conforme aux règles applicables et couvrant les coûts supportés par la société GDF Suez pour la fourniture du gaz ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'énergie ;<br/>
<br/>
              Vu le décret n° 2009-1603 du 18 décembre 2009 ; <br/>
<br/>
              Vu l'arrêté du 9 décembre 2010 relatif aux tarifs réglementés de vente du gaz naturel fourni à partir des réseaux publics de distribution de GDF Suez ;<br/>
<br/>
              Vu l'arrêté du 1er août 2012 relatif aux tarifs réglementés de vente du gaz naturel fourni à partir des réseaux publics de distribution de GDF Suez ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matthieu Schlesinger, Auditeur,  <br/>
<br/>
              - les observations de la SCP Lyon-Caen, Thiriez, avocat de l'Association nationale des opérateurs détaillants en énergie,<br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Lyon-Caen, Thiriez, avocat de l'Association nationale des opérateurs détaillants en énergie ;<br/>
<br/>
<br/>
<br/>Sur les conclusions aux fins d'annulation de l'arrêté attaqué :<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 445-3 du code de l'énergie, reprenant le II de l'article 7 de la loi du 3 janvier 2003 relative aux marchés du gaz et de l'électricité et au service public de l'énergie : " Les tarifs réglementés de vente du gaz naturel sont définis en fonction des caractéristiques intrinsèques des fournitures et des coûts liés à ces fournitures. Ils couvrent l'ensemble de ces coûts à l'exclusion de toute subvention en faveur des clients qui ont exercé leur droit prévu à l'article L. 441-1 (...) " ; que selon ce dernier article : " Tout client qui consomme le gaz qu'il achète ou qui achète du gaz pour le revendre a le droit, le cas échéant, par l'intermédiaire de son mandataire, de choisir son fournisseur de gaz naturel " ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 3 du décret du 18 décembre 2009 relatif aux tarifs réglementés de vente de gaz naturel : " les tarifs réglementés de vente du gaz naturel couvrent les coûts d'approvisionnement en gaz naturel et les coûts hors approvisionnement " ; qu'aux termes de l'article 4 de ce décret : " Pour chaque fournisseur, une formule tarifaire traduit la totalité des coûts d'approvisionnement en gaz naturel et des coûts hors approvisionnement et permet de déterminer le coût moyen de fourniture du gaz naturel, à partir duquel sont fixés les tarifs réglementés de vente de celui-ci, en fonction des modalités de desserte des clients concernés (...) / La formule tarifaire est fixée par les ministres chargés de l'économie et de l'énergie, après avis de la Commission de régulation de l'énergie, à partir, le cas échéant, des propositions faites par le fournisseur (...) " ; qu'aux termes de l'article 5 du décret : " Pour chaque fournisseur, un arrêté des ministres chargés de l'économie et de l'énergie pris après avis de la Commission de régulation de l'énergie fixe les barèmes des tarifs réglementés à partir, le cas échéant, des propositions du fournisseur. / Ces barèmes sont réexaminés au moins une fois par an et révisés s'il y a lieu en fonction de l'évolution de la formule tarifaire et compte tenu des modifications intervenues à l'initiative du fournisseur en application de l'article 6 du présent décret (...) " ; que l'article 6 du décret permet au fournisseur, sous le contrôle de la Commission de régulation de l'énergie et sauf disposition contraire prise par l'arrêté mentionné à l'article 5, de modifier, à titre conservatoire et jusqu'à l'intervention d'un nouvel arrêté tarifaire, les barèmes de ses tarifs réglementés en y répercutant les variations des coûts d'approvisionnement en gaz naturel, telles qu'elles résultent de l'application de sa formule tarifaire ;<br/>
<br/>
              3. Considérant que, par un arrêté du 9 décembre 2010, la ministre de l'économie, des finances et de l'industrie et le ministre chargé de l'industrie, de l'énergie et de l'économie numérique ont, en application de l'article 4 du décret du 18 décembre 2009, fixé la formule tarifaire, en fonction de laquelle sont déterminés les tarifs réglementés de vente hors taxes du gaz naturel fourni à partir des réseaux publics de distribution de la société GDF Suez, à partir d'un calcul de ses coûts d'approvisionnement en gaz naturel ;<br/>
<br/>
              4. Considérant qu'il résulte de la combinaison de ces dispositions que, s'il appartient aux ministres chargés de l'économie et de l'énergie, après avis de la Commission de régulation de l'énergie, de modifier la formule tarifaire prévue par l'article 4 du décret du 18 décembre 2009, dès lors qu'elle ne traduit plus correctement les coûts du fournisseur, et notamment ses coûts d'approvisionnement en gaz naturel, il leur incombe en revanche, lorsqu'ils révisent les barèmes des tarifs réglementés de vente du gaz naturel, en application de l'article 5 du même décret, de s'assurer que le niveau des tarifs qui en résulte permet de couvrir le coût moyen complet de fourniture du gaz naturel, tel qu'il est déterminé par l'application de la formule fixée par arrêté et, le cas échéant, de compenser l'écart, s'il est significatif, qui s'est produit entre tarifs et coût, au moins au cours de l'année écoulée, en vérifiant en outre s'il y a lieu de prendre en compte une estimation de l'évolution de ce coût sur l'année à venir, en fonction des éléments dont ils disposent à la date de leur décision ;<br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier, et notamment de l'avis rendu par la Commission de régulation de l'énergie le 23 juin 2011, que l'application de la formule tarifaire, fixée par l'arrêté du 9 décembre 2010, faisait apparaître une augmentation du coût d'approvisionnement en gaz naturel qui aurait dû conduire, au 1er juillet 2011, à une hausse des tarifs globale moyenne, à structure tarifaire inchangée, de 7,1 % ; qu'il suit de là que les auteurs de l'arrêté du 27 juin 2011, en maintenant au niveau fixé depuis le 1er avril 2011 les tarifs réglementés applicables aux clients résidentiels et aux petits clients professionnels et en augmentant en moyenne de 3,2 % seulement les tarifs réglementés applicables aux autres clients, sans que la différence entre cette évolution des tarifs et celle des coûts ne soit justifiée par une surévaluation initiale des tarifs ou par la baisse prévisible des coûts, et au lieu, s'ils l'estimaient nécessaire au regard des évolutions constatées des coûts d'approvisionnement, de modifier préalablement la formule tarifaire, ont entaché leur décision d'une erreur de droit ; que, sans qu'il soit besoin d'examiner les autres moyens de la requête, l'arrêté en litige doit dès lors être annulé ;<br/>
<br/>
              Sur les conséquences de l'illégalité de l'arrêté attaqué :<br/>
<br/>
              6. Considérant qu'il ne ressort pas des pièces du dossier, eu égard notamment à la faible durée d'application de l'arrêté annulé du 27 juin 2011, qui a été remplacé par d'autres dispositions trois mois après son entrée en vigueur, que son annulation soit de nature à emporter des conséquences manifestement excessives en raison tant des effets que cet acte a produits que des situations qui ont pu se constituer lorsqu'il était en vigueur ; qu'ainsi, il n'y a pas lieu, dans les circonstances de l'espèce, de limiter les effets de l'annulation de cet acte ;<br/>
<br/>
              Sur les conclusions aux fins d'injonction :<br/>
<br/>
              7. Considérant qu'aux termes de l'article L. 911-1 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution " ;<br/>
<br/>
              8. Considérant que la présente décision implique nécessairement que soit pris un nouvel arrêté relatif aux tarifs réglementés de vente du gaz naturel fourni à partir des réseaux publics de distribution de GDF Suez pour la période du 1er juillet 2011 au 1er octobre 2011, date d'entrée en vigueur des tarifs fixés par l'arrêté adopté le 1er août 2012 ; qu'il suit de là qu'il y a lieu de prescrire aux ministres chargés de l'économie et de l'énergie de prendre, dans le délai d'un mois à compter de la notification de la présente décision, un nouvel arrêté fixant une évolution des tarifs conforme aux principes énoncés par la présente décision ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              9. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros à l'Association nationale des opérateurs détaillants en énergie sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêté du 27 juin 2011 relatif aux tarifs réglementés de vente du gaz naturel fourni à partir des réseaux publics de distribution de GDF Suez est annulé.<br/>
Article 2 : Il est enjoint au ministre de l'économie et des finances et à la ministre de l'écologie, du développement durable et de l'énergie, de prendre dans le délai d'un mois à compter de la notification de la présente décision, un nouvel arrêté fixant une évolution des tarifs conforme aux principes énoncés par la présente décision.<br/>
Article 3 : L'Etat versera à l'Association nationale des opérateurs détaillants en énergie une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à l'Association nationale des opérateurs détaillants en énergie, au ministre de l'économie et des finances et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
Copie en sera adressée pour information à la Commission de régulation de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
