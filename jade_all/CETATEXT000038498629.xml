<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038498629</ID>
<ANCIEN_ID>JG_L_2019_05_000000420780</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/49/86/CETATEXT000038498629.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 22/05/2019, 420780</TITRE>
<DATE_DEC>2019-05-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420780</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>Mme Sara-Lou Gerber</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:420780.20190522</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Le comité d'entreprise de la société British Airways France a demandé au tribunal administratif de Melun d'annuler pour excès de pouvoir la décision du 7 juillet 2017 par laquelle le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi d'Ile-de-France a homologué le document unilatéral fixant le contenu du plan de sauvegarde de l'emploi de cette société. Par un jugement n° 1707064 du 24 novembre 2017, le tribunal administratif a annulé cette décision. <br/>
<br/>
              Par un arrêt n° 17PA03921, 18PA00281 du 21 mars 2018, la cour administrative d'appel de Paris a, sur appels de la société British Airways France et de la ministre du travail, d'une part, annulé ce jugement et, d'autre part, rejeté la demande de première instance du comité d'entreprise de la société British Airways France.   <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 22 mai et 10 août 2018 et le 4 avril 2019 au secrétariat du contentieux du Conseil d'Etat, le comité d'entreprise de la société British Airways France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les appels de la société British Airways France et de la ministre du travail ;<br/>
<br/>
              3°) de mettre à la charge de la société British Airways France et de la ministre du travail la somme de 3000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier.<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - la loi n° 2013-504 du 14 juin 2013 ;<br/>
                          - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sara-Lou Gerber, auditeur,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat du Comité d'entreprise de la société British Airways France et à la SCP Gatineau, Fattaccini, avocat de la société British Airways France.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société British Airways France, appartenant au groupe IAG, a décidé en novembre 2016, en conséquence de la réorganisation de ce groupe, de supprimer 41 de ses 73 emplois. Par une décision du 7 juillet 2017, le directeur régional des entreprises, de la consommation, de la concurrence, du travail et de l'emploi d'Ile-de-France a homologué la décision unilatérale de l'employeur fixant le contenu d'un plan de sauvegarde de l'emploi. Par un jugement du 24 novembre 2017, le tribunal administratif de Melun, saisi par le comité d'entreprise de la société, a annulé cette décision d'homologation.<br/>
<br/>
              2. Le comité d'entreprise de la société British Airways France se pourvoit en cassation contre l'arrêt du 21 mars 2018 par lequel la cour administrative d'appel de Paris a annulé ce jugement du 24 novembre 2017 et rejeté sa demande de première instance. <br/>
<br/>
              Sur l'arrêt en tant qu'il statue sur le déroulement de la procédure d'information et de consultation du comité d'entreprise : <br/>
<br/>
              3. Lorsqu'elle est saisie par un employeur d'une demande d'homologation d'un document élaboré en application de l'article L. 1233-24-4 du code du travail et fixant le contenu d'un plan de sauvegarde de l'emploi, il appartient à l'administration de s'assurer, sous le contrôle du juge de l'excès de pouvoir, que la procédure d'information et de consultation du comité d'entreprise, ou du comité social et économique, a été régulière. Elle ne peut légalement accorder l'homologation demandée que si le comité a été mis à même d'émettre régulièrement un avis, d'une part sur l'opération projetée et ses modalités d'application et, d'autre part, sur le projet de licenciement collectif et le plan de sauvegarde de l'emploi. A ce titre, il appartient à l'administration de s'assurer que l'employeur a adressé au comité tous les éléments utiles pour qu'il formule ses deux avis en toute connaissance de cause, dans des conditions qui ne sont pas susceptibles d'avoir faussé sa consultation.<br/>
<br/>
              4. Sous réserve, pour les entreprises qui sont en redressement ou en liquidation judiciaire, des dispositions de l'article L. 1233-58 du code du travail, l'information et la consultation du comité d'entreprise, ou, désormais, du comité social et économique, sur un licenciement collectif donnant lieu à l'établissement d'un plan de sauvegarde de l'emploi, sont régies par l'article L. 1233-30 du même code. Celui-ci dispose, dans sa version applicable à l'espèce, que : " Le comité d'entreprise rend ses deux avis (...) dans un délai qui ne peut être supérieur, à compter de la date de sa première réunion au cours de laquelle il est consulté sur les 1° et 2° du I, à : 1° Deux mois lorsque le nombre des licenciements est inférieur à cent. / 2° Trois mois lorsque le nombre des licenciements est au moins égal à cent et inférieur à deux cent cinquante. / 3° Quatre mois lorsque le nombre des licenciements est au moins égal à deux cent cinquante. / Une convention ou un accord collectif de travail peut prévoir des délais différents. En l'absence d'avis du comité d'entreprise dans ces délais, celui-ci est réputé avoir été consulté (...) ". L'administration ne peut ainsi être régulièrement saisie d'une demande d'homologation d'un document unilatéral fixant le contenu d'un plan de sauvegarde de l'emploi que si cette demande est accompagnée des avis rendus par le comité d'entreprise, ou, en l'absence de ces avis, si le comité d'entreprise est réputé avoir été consulté. <br/>
<br/>
              5. Lorsque la demande est accompagnée des avis rendus par le comité d'entreprise, il résulte des dispositions citées ci-dessus, éclairées par les travaux préparatoires de la loi du 14 juin 2013 de sécurisation de l'emploi de laquelle elles sont issues, que la circonstance que le comité d'entreprise ou, désormais, le comité social et économique ait rendu ses avis au-delà des délais qu'elles prévoient est par elle-même sans incidence sur la régularité de la procédure d'information et de consultation du comité.<br/>
<br/>
              6. En l'absence d'avis du comité d'entreprise ou, désormais, du comité social et économique, l'administration ne peut légalement homologuer ou valider le plan de sauvegarde de l'emploi qui lui est transmis que si, d'une part, le comité a été mis à même, avant cette transmission,  de rendre ses deux avis en toute connaissance de cause dans des conditions qui ne sont pas susceptibles d'avoir faussé sa consultation et que, d'autre part, le délai prévu par ces dispositions est échu à la date de cette transmission.<br/>
<br/>
              7. Enfin, si des modalités d'information et de consultation différentes ont été fixées par un accord conclu sur le fondement de l'article L. 1233-21 ou de l'article L. 1233-24-1 du code du travail, il appartient à l'administration de s'assurer, au regard de ces modalités, que le comité d'entreprise ou, désormais, le comité social et économique a été mis à même de rendre ses deux avis en toute connaissance de cause dans des conditions qui ne sont pas susceptibles d'avoir faussé sa consultation. <br/>
<br/>
              8. Il ressort des pièces du dossier soumis à la cour administrative d'appel que le délai applicable à l'opération litigieuse était, en vertu des dispositions citées ci-dessus de l'article L. 1233-30 du code du travail et en l'absence d'accord y ayant dérogé, de deux mois, à compter du 7 décembre 2016, date de la première réunion du comité d'entreprise de la société British Airways France. Il résulte de ce qui a été dit ci-dessus que la cour administrative d'appel n'a pas commis d'erreur de droit en jugeant que la circonstance que les avis du comité d'entreprise avaient été rendus le 9 juin 2017, soit au-delà de ce délai de deux mois, était par elle-même sans incidence sur la régularité de la procédure d'information et de consultation. Par ailleurs, alors qu'il ressortait du dossier qui lui était soumis que la poursuite de la consultation du comité d'entreprise au-delà du délai de deux mois n'avait pas, en l'espèce, empêché le comité de rendre ses avis en toute connaissance de cause ou faussé les conditions de sa consultation, la cour, en ne se prononçant pas expressément sur ce point, n'a pas commis d'erreur de droit et, eu égard à l'argumentation dont elle était saisie, n'a pas entaché son arrêt d'insuffisance de motivation.<br/>
<br/>
              Sur l'arrêt en tant qu'il statue sur la motivation de la décision d'homologation litigieuse :<br/>
<br/>
              9. Aux termes de l'article L. 1233-57-4 du code du travail : " L'autorité administrative notifie à l'employeur (...) la décision d'homologation dans un délai de vingt et un jours (...). / Elle la notifie, dans les mêmes délais, au comité d'entreprise (...). La décision prise par l'autorité administrative est motivée ". En vertu de ces dispositions, la décision expresse par laquelle l'administration homologue un document fixant le contenu d'un plan de sauvegarde de l'emploi doit énoncer les éléments de droit et de fait qui en constituent le fondement, de sorte que les personnes auxquelles cette décision est notifiée puissent à sa seule lecture en connaître les motifs. Doivent ainsi, notamment, y figurer les éléments relatifs à la régularité de la procédure d'information et de consultation des instances représentatives du personnel ainsi que, le cas échéant, tout élément sur lequel l'administration aurait été, en raison des circonstances propres à l'espèce, spécifiquement amenée à porter une appréciation. <br/>
<br/>
              10. En estimant, alors qu'il ressortait du dossier qui lui était soumis que la décision d'homologation litigieuse faisait mention de l'ensemble des réunions du comité d'entreprise, des conditions de désignation de l'expert comptable et de présentation de son rapport ainsi que de la date à laquelle le comité avait rendu ses deux avis, que cette décision était suffisamment motivée sur la régularité de la procédure d'information et de consultation, la cour administrative d'appel a porté une appréciation souveraine qui, alors même qu'aucune mention de la décision litigieuse ne faisait expressément état du dépassement du délai prévu par l'article L. 1233-30 du code du travail, n'est pas entachée de dénaturation.<br/>
<br/>
              11. Par ailleurs, contrairement à ce que soutient le requérant, la cour n'a pas entaché son arrêt de dénaturation des pièces du dossier en estimant que la décision d'homologation litigieuse était suffisamment motivée, alors même qu'elle ne mentionnait, notamment, ni le secteur d'activité dont relève la société British Airways France au sein du groupe IAG ni le montant global de l'enveloppe allouée au financement du plan de reclassement.<br/>
<br/>
              Sur les autres moyens du pourvoi :<br/>
<br/>
              12. S'il incombe à l'employeur d'adresser au comité d'entreprise, avec la convocation à sa première réunion, l'ensemble des renseignements utiles prévus notamment  par les dispositions des articles L. 1233-31 et L. 1233-32 du code du travail, il appartient à l'administration de s'assurer que le comité a été mis à même de rendre ses deux avis en toute connaissance de cause au vu, non seulement de ces premiers éléments d'information, mais aussi de tous ceux dont le comité a, le cas échéant, été destinataire au cours de la procédure d'information et de consultation. Par suite, la cour n'a pas commis d'erreur de droit en se fondant, pour estimer que le comité avait pu rendre ses deux avis en toute connaissance de cause, sur le caractère suffisant de l'information dont il disposait à la date de ces avis.<br/>
<br/>
              13. Enfin, il ressort des pièces du dossier soumis aux juges du fond, qui établissent notamment le grand nombre d'informations transmises au comité d'entreprise par l'employeur, lesquelles incluaient des informations financières relatives au groupe IAG auquel appartient la société British Airways France, qu'en estimant que les conditions dans lesquelles l'expert désigné par le comité d'entreprise avait exercé sa mission avaient permis au comité de formuler ses avis en toute connaissance de cause, la cour administrative d'appel n'a, alors même que l'expert n'aurait pas disposé d'un " business plan " confidentiel du groupe IAG, pas dénaturé les pièces du dossier. La circonstance que la cour a, à titre surabondant, relevé que l'existence d'un tel " business plan " n'était pas établie ne saurait, par ailleurs, entacher son arrêt d'inexactitude matérielle.<br/>
<br/>
              14. Il résulte de tout ce qui précède que le comité d'entreprise de la société British Airways France n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque. Par suite, son pourvoi doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              15. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge du comité d'entreprise de la société British Airways France les sommes demandées par la société British Airways France au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du comité d'entreprise de la société British Airways France est rejeté. <br/>
Article 2 : Les conclusions de la société British Airways France présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée au comité d'entreprise de la société British Airways France, à la société British Airways France et à la ministre du travail.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-07 TRAVAIL ET EMPLOI. - LICENCIEMENTS. - VALIDATION OU HOMOLOGATION ADMINISTRATIVE DES PSE - CONSULTATION PRÉALABLE DU COMITÉ D'ENTREPRISE OU DU COMITÉ SOCIAL ET ÉCONOMIQUE (L. 1233-30 DU CODE DU TRAVAIL) [RJ1] - 1) CIRCONSTANCE QUE LE COMITÉ A RENDU SES AVIS AU-DELÀ DES DÉLAIS PRÉVUS À L'ARTICLE L. 1233-30 - ABSENCE D'INCIDENCE SUR LA RÉGULARITÉ DE LA PROCÉDURE D'INFORMATION ET DE CONSULTATION DE CE COMITÉ - 2) POSSIBILITÉ POUR L'ADMINISTRATION, EN L'ABSENCE D'AVIS DU COMITÉ, D'HOMOLOGUER OU DE VALIDER LE PSE QUI LUI EST TRANSMIS - EXISTENCE SOUS CONDITIONS, D'UNE PART, QUE LE COMITÉ AIT ÉTÉ MIS À MÊME DE RENDRE SES AVIS EN TOUTE CONNAISSANCE DE CAUSE AVANT CETTE TRANSMISSION, LE CAS ÉCHÉANT AU REGARD DES EXIGENCES SPÉCIFIQUES PRÉVUES PAR UN ACCORD COLLECTIF [RJ2] ET, D'AUTRE PART, QUE LE DÉLAI PRÉVU PAR L'ARTICLE L. 1233-30 SOIT ÉCHU À LA DATE DE CETTE TRANSMISSION.
</SCT>
<ANA ID="9A"> 66-07 Sous réserve, pour les entreprises qui sont en redressement ou en liquidation judiciaire, des dispositions de l'article L.1233-58 du code du travail, l'information et la consultation du comité d'entreprise, ou, désormais, du comité social et économique, sur un licenciement collectif donnant lieu à l'établissement d'un plan de sauvegarde de l'emploi (PSE), est régie par l'article L. 1233-30 du même code. L'administration ne peut ainsi être régulièrement saisie d'une demande d'homologation d'un document unilatéral fixant le contenu d'un PSE que si cette demande est accompagnée des avis rendus par le comité d'entreprise, ou, en l'absence de ces avis, si le comité d'entreprise est réputé avoir été consulté. ......1) Lorsque la demande est accompagnée des avis rendus par le comité d'entreprise, il résulte des dispositions de l'article L. 1233-30, éclairées par les travaux préparatoires de la loi n° 2013-504 du 14 juin 2013 de laquelle elles sont issues, que la circonstance que le comité d'entreprise ou, désormais, le comité social et économique ait rendu ses avis au-delà des délais qu'elles prévoient est par elle-même sans incidence sur la régularité de la procédure d'information et de consultation du comité.......2) En l'absence d'avis du comité d'entreprise ou, désormais, du comité social et économique, l'administration ne peut légalement homologuer ou valider le PSE qui lui est transmis que si, d'une part, le comité a été mis à même, avant cette transmission, de rendre ses deux avis en toute connaissance de cause dans des conditions qui ne sont pas susceptibles d'avoir faussé sa consultation et que, d'autre part, le délai prévu par ces dispositions est échu à la date de cette transmission....Enfin, si des modalités d'information et de consultation différentes ont été fixées par un accord conclu sur le fondement de l'article L 1233-21 ou de l'article L 1233-24-1 du code du travail, il appartient à l'administration de s'assurer, au regard de ses modalités, que le comité d'entreprise ou, désormais, le comité social et économique a été mis à même de rendre ses deux avis en toute connaissance de cause dans des conditions qui ne sont pas susceptibles d'avoir faussé sa consultation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur l'obligation de l'administration de s’assurer, sous le contrôle du juge de l’excès de pouvoir, que la procédure d’information et de consultation du comité d’entreprise a été régulière, CE, Assemblée, 22 juillet 2015, Ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social c/ Comité central d'entreprise HJ Heinz France, n° 385816, p. 261....[RJ2] Cf. CE, 4 juillet 2018, Association des cités du secours catholique, n° 410904, T. p. 945 ; CE, décision du même jour, Société Véron International, n° 397059, T. p. 945.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
