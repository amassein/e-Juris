<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042844851</ID>
<ANCIEN_ID>JG_L_2020_12_000000426098</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/84/48/CETATEXT000042844851.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 30/12/2020, 426098</TITRE>
<DATE_DEC>2020-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426098</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX ; SCP OHL, VEXLIARD</AVOCATS>
<RAPPORTEUR>Mme Fanélie Ducloz</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:426098.20201230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La commune de Louvie-Juzon a demandé au tribunal administratif de Pau d'annuler pour excès de pouvoir l'arrêté du préfet des Pyrénées-Atlantiques du 27 novembre 2012 portant autorisation de dérivation et d'utilisation de l'eau pour la consommation humaine, déclaration d'utilité publique d'instauration des périmètres de protection autour de la source d'Aygue-Blanque et autorisation au titre du code de l'environnement, ainsi que la décision par laquelle le même préfet a implicitement rejeté son recours gracieux. Par un jugement n° 1300809 du 1er décembre 2015, le tribunal administratif a annulé la déclaration d'utilité publique du 27 novembre 2012 et mis à la charge de l'Etat et du syndicat mixte d'alimentation en eau potable du Nord-Est de Pau les frais de l'expertise, taxés et liquidés à la somme de 6 413,26 euros TTC.<br/>
<br/>
              Par un arrêt n° 16BX00405, 16BX00469 du 9 octobre 2018, la cour administrative d'appel de Bordeaux a, sur appel du syndicat mixte d'alimentation en eau potable du Nord-Est de Pau, annulé ce jugement, rejeté la demande présentée par la commune de Louvie-Juzon devant le tribunal administratif de Pau, et mis à la charge de la commune les frais d'expertise, taxés et liquidés à la somme de 6 413,26 euros TTC.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 7 décembre 2018, 26 février 2019 et 8 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, la commune de Louvie-Juzon demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du syndicat mixte d'alimentation en eau potable du Nord-Est de Pau ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat et du syndicat mixte d'alimentation en eau potable du Nord-Est de Pau la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le code de l'environnement ;<br/>
              - le code de l'expropriation pour cause d'utilité publique;<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative et le décret n° 2020-146 du 18 novembre 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... A..., maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de la commune de Louvie-Juzon et à la SCP Ohl, Vexliard, avocat du syndicat mixte d'alimentation en eau potable du Nord-Est de Pau;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 1321-2 du code de la santé publique dans sa rédaction alors applicable : " En vue d'assurer la protection de la qualité des eaux, l'acte portant déclaration d'utilité publique des travaux de prélèvement d'eau destinée à l'alimentation des collectivités humaines mentionné à l'article L. 215-13 du code de l'environnement détermine autour du point de prélèvement un périmètre de protection immédiate dont les terrains sont à acquérir en pleine propriété, un périmètre de protection rapprochée à l'intérieur duquel peuvent être interdits ou réglementés toutes sortes d'installations, travaux, activités, dépôts, ouvrages, aménagement ou occupation des sols de nature à nuire directement ou indirectement à la qualité des eaux et, le cas échéant, un périmètre de protection éloignée à l'intérieur duquel peuvent être réglementés les installations, travaux, activités, dépôts, ouvrages, aménagement ou occupation des sols et dépôts ci-dessus mentionnés. (...) Lorsque des terrains situés dans un périmètre de protection immédiate appartiennent à une collectivité publique, il peut être dérogé à l'obligation d'acquérir les terrains visés au premier alinéa par l'établissement d'une convention de gestion entre la ou les collectivités publiques propriétaires et l'établissement public de coopération intercommunale ou la collectivité publique responsable du captage. (...) ". Aux termes du premier alinéa de l'article L. 1321-3 du même code : " Les indemnités qui peuvent être dues aux propriétaires ou occupants de terrains compris dans un périmètre de protection de prélèvement d'eau destinée à l'alimentation des collectivités humaines, à la suite de mesures prises pour assurer la protection de cette eau, sont fixées selon les règles applicables en matière d'expropriation pour cause d'utilité publique. ". Aux termes de l'article L. 215-13 du code de l'environnement : " La dérivation des eaux d'un cours d'eau non domanial, d'une source ou d'eaux souterraines, entreprise dans un but d'intérêt général par une collectivité publique ou son concessionnaire, par une association syndicale ou par tout autre établissement public, est autorisée par un acte déclarant d'utilité publique les travaux. ". <br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que le préfet des Pyrénées-Atlantiques a, sur le fondement des dispositions qui viennent d'être rappelées, pris le 27 novembre 2012, au bénéfice du syndicat mixte d'alimentation en eau potable du nord-est de Pau, un arrêté déclarant d'utilité publique les travaux de dérivation des eaux de la source d'Aygue-Blanque située sur le territoire de la commune de Louvie-Juzon et instaurant des périmètres de protection immédiate et rapprochée. La commune de Louvie-Juzon est propriétaire d'une parcelle incluse dans le périmètre de protection immédiate et sur laquelle est située la résurgence de la source d'Aygue-Blanque. La commune, qui n'a pas conclu de convention de gestion de la source avec le syndicat mixte d'alimentation en eau potable du nord-est de Pau, a demandé au tribunal administratif de Pau l'annulation de l'arrêté préfectoral. Par un jugement avant-dire droit du 30 septembre 2014, le tribunal administratif a ordonné une mesure d'expertise aux fins d'évaluer la valeur vénale de la parcelle appartenant à la commune et, par un jugement du 1er décembre 2015, il a annulé l'arrêté attaqué et mis à la charge de l'Etat et du syndicat mixte d'alimentation en eau potable du nord-est de Pau les frais de l'expertise. Par un arrêt du 9 octobre 2018 contre lequel la commune de Louvie-Juzon se pourvoit en cassation, la cour administrative d'appel de Bordeaux a, sur appel du syndicat mixte d'alimentation en eau potable du nord-est de Pau, annulé le jugement du tribunal administratif de Pau, rejeté la demande en annulation de l'arrêté préfectoral du 27 novembre 2012 et mis les frais de l'expertise à la charge de la commune de Louvie-Juzon.<br/>
<br/>
              3. En l'absence de dispositions spécifiques définissant la procédure qui leur est applicable, les actes portant déclaration d'utilité publique des travaux de prélèvement d'eau destinée à l'alimentation des collectivités humaines pris sur le fondement des dispositions de l'article L. 1321-2 du code de la santé publique rappelées ci-dessus sont régis par les dispositions du code de l'expropriation pour cause d'utilité publique, en vigueur à la date de l'arrêté attaqué. <br/>
<br/>
              4. D'une part, aux termes de l'article 552 du code civil : " La propriété du sol emporte la propriété du dessus et du dessous. (...) ". Aux termes de l'article L. 13-13, devenu L. 321-1, du code de l'expropriation pour cause d'utilité publique : " Les indemnités allouées doivent couvrir l'intégralité du préjudice direct, matériel et certain, causé par l'expropriation. ". Aux termes de l'article L. 13-14, devenu L. 322-1, du même code : " La juridiction fixe le montant des indemnités d'après la consistance des biens à la date de l'ordonnance portant transfert de propriété. (...) ". <br/>
<br/>
              5. D'autre part, aux termes de l'article R. 11-3 du code de l'expropriation pour cause d'utilité publique, alors applicable : " L'expropriant adresse au préfet pour être soumis à l'enquête un dossier qui comprend obligatoirement : / I.- Lorsque la déclaration d'utilité publique est demandée en vue de la réalisation de travaux ou d'ouvrages (...) / 5° L'appréciation sommaire des dépenses (...) ". L'obligation ainsi faite à l'autorité publique qui poursuit la déclaration d'utilité publique de travaux ou d'ouvrages a pour but de permettre à tous les intéressés de s'assurer que ces travaux ou ouvrages, compte tenu de leur coût total réel, tel qu'il peut être raisonnablement apprécié à l'époque de l'enquête, ont un caractère d'utilité publique. Toutefois, la seule circonstance que certaines dépenses auraient été omises n'est pas, par elle-même, de nature à entacher d'irrégularité la procédure si, compte tenu de leur nature, leur montant apparaît limité au regard du coût global de l'opération et ne peut être effectivement apprécié qu'au vu d'études complémentaires postérieures, rendant ainsi incertaine leur estimation au moment de l'enquête.<br/>
<br/>
              6. Il résulte des dispositions citées au point 4 que le tréfonds fait partie de la consistance du bien et, qu'en conséquence, l'indemnité d'expropriation d'un terrain doit tenir compte de la plus-value apportée à ce terrain par le caractère exploitable, par le propriétaire ou à son profit, à la date de l'ordonnance de transfert de propriété, d'une ressource située dans son tréfonds. Par suite, en application des dispositions citées aux points 1 et 5, lorsqu'une source est située dans le tréfonds d'une parcelle se trouvant dans le périmètre de protection immédiat déterminé par l'acte portant déclaration d'utilité publique des travaux de prélèvement de son eau et est exploitable par le propriétaire de la parcelle ou à son profit à la date d'ouverture de l'enquête publique, son caractère exploitable est susceptible de conférer à cette parcelle une plus-value, compte tenu le cas échéant des dépenses nécessaires à la mise en exploitation, qui doit être prise en compte dans le coût de son acquisition et, par suite, dans l'appréciation sommaire des dépenses figurant dans le dossier d'enquête publique.<br/>
<br/>
              7. En se fondant, pour juger que le dossier soumis à enquête publique répondait aux exigences des dispositions rappelées précédemment, sur la seule circonstance que la commune de Louvie-Juzon n'avait jamais exploité à son profit la source d'Aygue-Blanque située dans le tréfonds du terrain lui appartenant, sans rechercher si, à la date d'ouverture de l'enquête publique, cette source était exploitable par la commune ou à son profit, ce qui était susceptible de conférer au terrain une plus-value devant être prise en compte dans le coût de son acquisition pour apprécier la régularité du dossier soumis à enquête publique, la cour a commis une erreur de droit.<br/>
<br/>
              8. Il résulte de ce qui précède que la commune de Louvie-Juzon est fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat et du syndicat mixte d'alimentation en eau potable du nord-est de Pau la somme de 1 500 euros chacun à verser à la commune de Louvie-Juzon au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à la charge de la commune qui n'est pas partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative de Bordeaux du 9 octobre 2018 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux.<br/>
Article 3 : L'Etat et le syndicat mixte d'alimentation en eau potable du Nord-Est de Pau verseront chacun la somme de 1 500 euros à la commune de Louvie-Juzon au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions du syndicat mixte d'alimentation en eau potable du Nord-Est de Pau au titre des dispositions de l'article L 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la commune de Louvie-Juzon, à la ministre de la transition écologique, au ministre des solidarités et de la santé et au syndicat mixte d'alimentation en eau potable du Nord-Est de Pau.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">27-03-01 EAUX. TRAVAUX. CAPTAGE DES EAUX DE SOURCE. - DÉCLARATION D'UTILITÉ PUBLIQUE DES TRAVAUX - DOSSIER D'ENQUÊTE PUBLIQUE - APPRÉCIATION SOMMAIRE DES DÉPENSES - PRISE EN COMPTE DE LA PLUS-VALUE QUE CONSTITUE LE CARACTÈRE EXPLOITABLE DU TRÉFONDS - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">34-02-01-01-01-03 EXPROPRIATION POUR CAUSE D'UTILITÉ PUBLIQUE. RÈGLES GÉNÉRALES DE LA PROCÉDURE NORMALE. ENQUÊTES. ENQUÊTE PRÉALABLE. DOSSIER D'ENQUÊTE. APPRÉCIATION SOMMAIRE DES DÉPENSES. - INDEMNITÉ D'EXPROPRIATION - 1) INCLUSION DE LA PLUS-VALUE QUE CONSTITUE LE CARACTÈRE EXPLOITABLE DU TRÉFONDS - EXISTENCE [RJ1] - 2) ILLUSTRATION - DÉCLARATION D'UTILITÉ PUBLIQUE DES TRAVAUX DE PRÉLÈVEMENT D'EAU D'UNE SOURCE.
</SCT>
<ANA ID="9A"> 27-03-01 En application des articles L. 1321-2 et L. 1321-3 du code de la santé publique (CSP), L. 215-13 du code de l'environnement et R. 11-3 du code de l'expropriation pour cause d'utilité publique (CECUP), lorsqu'une source est située dans le tréfonds d'une parcelle se trouvant dans le périmètre de protection immédiat déterminé par l'acte portant déclaration d'utilité publique des travaux de prélèvement de son eau et est exploitable par le propriétaire de la parcelle ou à son profit à la date d'ouverture de l'enquête publique, son caractère exploitable est susceptible de conférer à cette parcelle une plus-value, compte tenu le cas échéant des dépenses nécessaires à la mise en exploitation, qui doit être prise en compte dans le coût de son acquisition et, par suite, dans l'appréciation sommaire des dépenses figurant dans le dossier d'enquête publique.</ANA>
<ANA ID="9B"> 34-02-01-01-01-03 1) Il résulte de l'article 552 du code civil et des articles L. 13-13, devenu L. 321-1, et L. 13-14, devenu L. 322-1, du code de l'expropriation pour cause d'utilité publique que le tréfonds fait partie de la consistance du bien et, qu'en conséquence, l'indemnité d'expropriation d'un terrain doit tenir compte de la plus-value apportée à ce terrain par le caractère exploitable, par le propriétaire ou à son profit, à la date de l'ordonnance de transfert de propriété, d'une ressource située dans son tréfonds.... ,,2) Par suite, en application des articles L. 1321-2 et L. 1321-3 du code de la santé publique (CSP), L. 215-13 du code de l'environnement et R. 11-3 du code de l'expropriation pour cause d'utilité publique, lorsqu'une source est située dans le tréfonds d'une parcelle se trouvant dans le périmètre de protection immédiat déterminé par l'acte portant déclaration d'utilité publique des travaux de prélèvement de son eau et est exploitable par le propriétaire de la parcelle ou à son profit à la date d'ouverture de l'enquête publique, son caractère exploitable est susceptible de conférer à cette parcelle une plus-value, compte tenu le cas échéant des dépenses nécessaires à la mise en exploitation, qui doit être prise en compte dans le coût de son acquisition et, par suite, dans l'appréciation sommaire des dépenses figurant dans le dossier d'enquête publique.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. Cass. civ. 3e, 13 novembre 1969, Consorts X&#133; c/ Etat français, n° 68-70.137, Bull. n° 728.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
