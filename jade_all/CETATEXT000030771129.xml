<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030771129</ID>
<ANCIEN_ID>JG_L_2015_06_000000373379</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/77/11/CETATEXT000030771129.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 19/06/2015, 373379, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-06-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373379</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Guillaume Odinet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:373379.20150619</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure <br/>
<br/>
              Le département des Bouches-du-Rhône a demandé au tribunal administratif de Marseille, d'une part, d'annuler la décision implicite par laquelle le préfet des Bouches-du-Rhône a refusé de procéder au mandatement d'office de la somme de 29 483 798,18 euros à l'encontre de la commune de Marseille, d'autre part, de condamner l'Etat à lui verser la somme de 29 483 798,18 euros, assortie des intérêts au taux légal et de la capitalisation des intérêts, au titre du préjudice qu'il affirme avoir subi en raison de cette décision. Par un jugement n° 0806041-0806121 du 5 avril 2011, le tribunal administratif de Marseille a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 11MA02112 du 20 septembre 2013, la cour administrative d'appel de Marseille a rejeté l'appel formé par le département contre ce jugement.<br/>
<br/>
Procédure devant le Conseil d'Etat <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 20 novembre 2013, 20 février 2014 et le 27 mai 2015, au secrétariat du contentieux du Conseil d'Etat, le département des Bouches-du-Rhône demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11MA02112 du 20 septembre 2013 de la cour administrative d'appel de Marseille ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 83-8 du 7 janvier 1983 ;<br/>
              - la loi n° 99-641 du 27 juillet 1999 ;<br/>
              - le décret n° 87-1146 du 31 décembre 1987 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Odinet, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat du département des Bouches du Rhône ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant, d'une part, qu'aux termes du premier alinéa de l'article L. 1612-15 du code général des collectivités territoriales : " Ne sont obligatoires pour les collectivités territoriales que les dépenses nécessaires à l'acquittement des dettes exigibles et les dépenses pour lesquelles la loi l'a expressément décidé. " ; qu'il résulte de ces dispositions que seules présentent un caractère obligatoire les dettes échues, certaines, liquides, non sérieusement contestées dans leur principe et dans leur montant et découlant de la loi, d'un contrat, d'un délit, d'un quasi-délit ou de toute autre source d'obligations ; <br/>
<br/>
              2. Considérant, d'autre part, qu'en vertu des dispositions alors en vigueur du quatrième alinéa de l'article 93 de la loi du 7 janvier 1983 relative à la répartition de compétences entre les communes, les départements, les régions et l'Etat, les communes devaient participer aux dépenses des départements en matière d'aide sociale et de santé, dans des conditions et selon des critères fixés par décrets en Conseil d'Etat ; qu'en vertu des dispositions alors en vigueur du décret du 31 décembre 1987 relatif à la participation des communes aux dépenses d'aide sociale et de santé des départements pris en application de ces dispositions, cette participation prenait la forme d'une contribution globale annuelle arrêtée par le conseil général ; que la loi du 27 juillet 1999 portant création d'une couverture maladie universelle a supprimé cette contribution ; que le X de son article 13 dispose : " Les sommes restant dues par les communes aux départements en application de l'article 93 de la loi n° 83-8 du 7 janvier 1983 précitée au titre des exercices antérieurs à 2000 sont acquittées selon un échéancier arrêté par convention entre le département et la commune " ; qu'il ressort des pièces du dossier soumis aux juges du fond que la commune de Marseille et le département des Bouches du Rhône ont conclu, le 13 janvier 2000, un contrat intitulé " accord de partenariat " fixant notamment l'échéancier de paiement, par la commune, des sommes restant dues au titre de sa contribution aux dépenses d'aide sociale et de santé du département ;<br/>
<br/>
              3. Considérant qu'il ressort des termes de l'arrêt attaqué que, pour juger que l'Etat n'avait commis aucune faute du fait de l'abstention du préfet de procéder d'office au mandatement des sommes réclamées par le département des Bouches du Rhône à la commune de Marseille, la cour administrative d'appel de Marseille s'est fondée sur la circonstance que les dettes de la commune devaient être regardées comme sérieusement contestées dans leur principe ou dans leur montant en raison du litige dont l'exécution de la convention du 13 janvier 2000 faisait l'objet entre les parties ; qu'en statuant ainsi, alors que ni le principe de ces dettes, qui résultait de l'article 93 de la loi du 7 janvier 1983, ni leur montant, qui résultait de décisions prises par le département en application de cet article et du décret du 31 décembre 1987, n'étaient susceptibles d'être affectés par la convention, qui, conformément aux dispositions précitées de la loi du 27 juillet 1999, ne pouvait avoir pour objet, s'agissant des dettes en cause, que de fixer l'échéancier de leur règlement, elle a commis une erreur de droit ; que son arrêt doit être annulé  pour ce motif, sans qu'il soit besoin d'examiner les autres moyens du pourvoi;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser au département des Bouches-du-Rhône au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 20 septembre 2013 de la cour administrative d'appel de Marseille est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : L'Etat versera au département des Bouches-du-Rhône la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée au département des Bouches-du-Rhône et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
