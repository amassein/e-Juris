<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038815881</ID>
<ANCIEN_ID>JG_L_2019_07_000000430121</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/81/58/CETATEXT000038815881.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 24/07/2019, 430121, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430121</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Marie Grosset</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:430121.20190724</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire et un nouveau mémoire, enregistrés les 23  mai et 27 juin 2019 au secrétariat du contentieux du Conseil d'Etat, présentés en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, l'association Union nationale des étudiants en droit, gestion, AES, sciences économiques, politiques et sociales, l'association Bureau national des élèves ingénieurs et la Fédération nationale des étudiants en psychologie, demandent au Conseil d'Etat, à l'appui de leur requête tendant à l'annulation de l'arrêté du 19 avril 2019 du ministre de l'enseignement supérieur, de la recherche et de l'innovation, du ministre de l'action et des comptes publics et de la ministre des outre-mer, relatif aux droits d'inscription dans les établissements publics d'enseignement supérieur relevant du ministre chargé de l'enseignement supérieur, de renvoyer au Conseil  constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution du troisième alinéa de l'article 48 de la loi de finances n° 51-598 du 24 mai 1951.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de l'éducation ;<br/>
              - la loi de finances pour l'exercice 1951 n°51-598 du 24 mai 1951 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Grosset, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de cet article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Le troisième alinéa de l'article 48 de la loi de finances pour l'exercice 1951 du 24 mai 1951 prévoit que des arrêtés du ministre intéressé et du ministre du budget fixent : " Les taux et modalités de perception des droits d'inscription, de scolarité, d'examen, de concours et de diplôme dans les établissements de l'Etat ". Sur le fondement de ces dispositions, le ministre de l'enseignement supérieur, de la recherche et de l'innovation et le ministre de l'action et des comptes publics et la ministre des outre-mer ont, par l'arrêté du 19 avril 2019 dont l'Union nationale des étudiants en droit, gestion, AES, sciences économiques, politiques et sociales (UNEDESEP) et autres ont demandé l'annulation, fixé les droits d'inscription dans les établissements publics d'enseignement supérieur relevant du ministre chargé de l'enseignement supérieur à compter de l'année universitaire 2019-2020.<br/>
<br/>
              3. Les dispositions citées ci-dessus de la loi du 24 mai 1951 sont applicables au litige et n'ont pas été déclarées conformes à la Constitution par le Conseil Constitutionnel. Elles fondent, conjointement aux dispositions de l'article L. 719-4 du code de l'éducation, la faculté pour les établissements d'enseignement supérieur de percevoir des droits d'inscription. A ce titre, le moyen tiré de ce qu'elles méconnaissent les droits constitutionnellement protégés par le treizième alinéa du Préambule de la Constitution de 1946, aux termes duquel : " La Nation garantit l'égal accès de l'enfant et de l'adulte à l'instruction, à la formation professionnelle et à la culture. L'organisation de l'enseignement public gratuit et laïque à tous les degrés est un devoir de l'Etat ", présente un caractère sérieux.<br/>
<br/>
              4. Par suite, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée par l'UNEDESEP et autres.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité de la Constitution du troisième alinéa de l'article 48 de la loi du 24 mai 1951 est renvoyée au Conseil constitutionnel.<br/>
Article 2 : Il est sursis à statuer sur la requête de l'UNEDESEP et autres jusqu'à ce que le Conseil Constitutionnel ait tranché la question de constitutionnalité ainsi soulevée.<br/>
Article 3 : La présente décision sera notifiée à l'association Union nationale des étudiants en droit, gestion, AES, sciences économiques, politiques et sociales, premier requérant dénommé, au ministre de l'enseignement supérieur, de la recherche et de l'innovation, au ministre de l'action et des comptes publics et au ministre des outre-mer.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
