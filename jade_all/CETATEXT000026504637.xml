<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026504637</ID>
<ANCIEN_ID>JG_L_2012_10_000000352499</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/50/46/CETATEXT000026504637.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème sous-section jugeant seule, 17/10/2012, 352499, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-10-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352499</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Rémy Schwartz</PRESIDENT>
<AVOCATS>SCP BARADUC, DUHAMEL</AVOCATS>
<RAPPORTEUR>M. Fabrice Aubert</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:352499.20121017</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 7 septembre et 6 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'agence pour l'enseignement français à l'étranger (AEFE), dont le siège est 1, allée Baco, BP 21509 à Nantes cedex 1 (44015), représentée par sa directrice en exercice domiciliée en cette qualité audit siège ; l'agence pour l'enseignement français à l'étranger demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement n° 0802819 du 29 juin 2011 par lequel le tribunal administratif de Nantes a annulé sa décision de rejet de la demande de Mme Françoise A tendant à ce que soit rétabli le montant de son indemnité de résidence pour les périodes correspondant à ses congés de maladie ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la requête de Mme A devant le tribunal administratif de Nantes ; <br/>
<br/>
              3°) de mettre à la charge de Mme A la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ; <br/>
<br/>
              Vu le décret n° 67-290 du 28 mars 1967 ; <br/>
<br/>
              Vu le décret n° 2002-22 du 4 janvier 2002 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabrice Aubert, Auditeur,  <br/>
<br/>
              - les observations de la SCP Baraduc, Duhamel, avocat de l'agence pour l'enseignement français à l'étranger,<br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Baraduc, Duhamel, avocat de l'agence pour l'enseignement français à l'étranger ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'en vertu des dispositions de l'article 34 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, le fonctionnaire en activité perçoit, au cours des trois premiers mois de congés de maladie, l'intégralité de son traitement et conserve ses droits à la totalité du supplément familial de traitement et de l'indemnité de résidence ; que toutefois ces dispositions régissent les droits à congés des fonctionnaires en activité de service dans leur corps d'origine et ne sont pas applicables à la situation des fonctionnaires détachés dans leur emploi de détachement ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme A, professeur certifié de sciences de la vie et de la terre en position de détachement auprès de l'agence pour l'enseignement français à l'étranger (AEFE), avec le statut de résident à Londres, a bénéficié de congés de maladie pour les périodes allant du 11 au 18 décembre 2006, puis du 10 au 15 janvier 2007 ; que, par un recours gracieux du 20 novembre 2007, rejeté explicitement le 20 décembre 2007, elle a demandé à la directrice de l'AEFE de lui rembourser la somme de 355,95 euros correspondant aux retenues effectuées sur son salaire au titre de ces congés de maladie en application de l'article 15 du décret du 4 janvier 2002 relatif à la situation administrative et financière des personnels des établissements d'enseignement français à l'étranger ; que, par un jugement du 29 juin 2011 contre lequel l'AEFE se pourvoit en cassation, le tribunal administratif de Nantes a annulé la décision du 20 décembre 2007 et a enjoint à l'AEFE de verser à Mme A la somme demandée, majorée des intérêts au taux légal à compter du 23 novembre 2007 ; <br/>
<br/>
              3. Considérant que, pour annuler la décision du 20 décembre 2007 de la directrice de l'AEFE, le tribunal administratif de Nantes s'est fondé sur les dispositions de l'article 34 de la loi du 11 janvier 1984 alors qu'en l'espèce elles n'étaient pas applicables à la situation de Mme A, qui n'était pas en position d'activité mais de détachement ; qu'il a ainsi commis une erreur de droit ; que, par suite, l'AEFE est fondée à demander l'annulation du jugement attaqué ;<br/>
<br/>
              4. Considérant que, dans les circonstances de l'espèce, il y a lieu de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant qu'ainsi qu'il a été dit, les dispositions de l'article 34 de la loi du 11 janvier 1984 ne sont pas applicables à la situation des fonctionnaires détachés dans leur emploi de détachement ; que, dès lors, Mme A ne saurait soutenir que la décision dont elle demande l'annulation serait illégale au motif qu'elle méconnaîtrait ces dispositions ; qu'elle ne saurait davantage se prévaloir de celles du décret du 28 mars 1967 fixant les modalités de calcul des émoluments des personnels de l'Etat et des établissements publics de l'Etat à caractère administratif en service à l'étranger dont l'article 1er exclut de son champ d'application les personnels régis par le décret n° 2002-22 du 4 janvier 2002 modifié relatif à la situation administrative et financière des personnels des établissements d'enseignement français à l'étranger ; <br/>
<br/>
              6. Considérant qu'aux termes de l'article 15 de ce dernier décret : " Les émoluments de l'agent autorisé à bénéficier à l'étranger d'un congé de maladie comprennent, pendant les quatre-vingt-dix jours qui suivent la date à laquelle la maladie a été constatée par le chef de mission diplomatique ou de poste consulaire, le traitement, les indemnités et avantages statutaires  prévus par la réglementation en vigueur dans les établissements relevant en France du ministère de l'éducation nationale, dès lors qu'ils sont applicables à l'étranger. S'y ajoutent : - (...) s'il est résident : 50 % de l'indemnité spécifique de vie locale, 50 % de l'indemnité différentielle ainsi que l'avantage familial si l'agent peut y prétendre (...) / En outre, au-delà de quatre-vingt-dix jours (...), le traitement est réduit de moitié " ;<br/>
<br/>
              7. Considérant qu'il résulte de l'instruction que Mme A avait, à la date de ses congés de maladie, le statut de résident à Londres ; que, dès lors, c'est à bon droit qu'en application des dispositions précitées de l'article 15 du décret du 4 janvier 2002, la directrice de l'AEFE a, par la décision attaquée du 20 décembre 2007, rejeté sa demande de restitution des retenues effectuées à hauteur de 50 % sur le montant de son indemnité spécifique de vie locale et de son indemnité différentielle pendant les périodes correspondant à ses congés de maladie ; que, par suite, Mme A n'est pas fondée à demander l'annulation de cette décision ; que, par voie de conséquence, ses conclusions à fin d'injonction doivent elles aussi être rejetées ; <br/>
<br/>
              8. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme A la somme demandée par l'AEFE au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 29 juin 2011 du tribunal administratif de Nantes est annulé.<br/>
Article 2 : La demande de Mme A devant le tribunal administratif de Nantes est rejetée.<br/>
Article 3 : Les conclusions de l'AEFE présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à l'agence pour l'enseignement français à l'étranger et à Mme Françoise A.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
