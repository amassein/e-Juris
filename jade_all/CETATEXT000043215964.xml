<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043215964</ID>
<ANCIEN_ID>JG_L_2021_02_000000449266</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/21/59/CETATEXT000043215964.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 23/02/2021, 449266, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>449266</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:449266.20210223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. D... C... et Mme A... B... ont demandé au juge des référés du tribunal administratif de la Guyane, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, de suspendre la décision implicite de rejet née du silence gardé par le préfet de la Guyane sur la demande de passeport déposée le 8 juillet 2020 pour leur enfant mineure E... C... et, d'autre part, d'enjoindre au préfet de la Guyane de délivrer le passeport sollicité, sous astreinte de 50 euros par jour de retard dans un délai de huit jours à compter de la notification de l'ordonnance à intervenir. Par une ordonnance n° 2100040 du 18 janvier 2021, le juge des référés du tribunal administratif de la Guyane a rejeté leur demande.<br/>
<br/>
              Par une requête, enregistrée le 1er février 2021 au secrétariat du contentieux du Conseil d'Etat, M. C... et Mme B... demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à leur demande de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
              - l'ordonnance est entachée d'une erreur d'appréciation des faits dès lors qu'elle considère, d'une part, que le délai de traitement excessif, invoqué à l'appui de la condition d'urgence, est justifié par la crise sanitaire et, d'autre part, que les requérants se seraient placés eux-mêmes dans une situation d'urgence en ne répondant pas aux demandes de pièces complémentaires de l'administration ; <br/>
              - la condition d'urgence est satisfaite dès lors que le délai d'instruction de la demande de passeport revêt un caractère anormalement long ;<br/>
              - il est porté une atteinte grave et manifestement illégale à plusieurs libertés fondamentales ; <br/>
              - la décision contestée méconnaît la liberté d'aller et venir de leur enfant dès lors que le refus de lui délivrer un passeport l'empêche de quitter la Guyane pour rejoindre la métropole ; <br/>
              - elle méconnaît le droit de leur enfant de mener une vie privée et familiale normale dès lors que le défaut de passeport, d'une part, l'empêche de rejoindre son père en métropole, où ce dernier vit, a ses autres enfants et exerce son activité professionnelle et, d'autre part, empêche sa mère de faire enregistrer sa demande de titre de séjour, nécessaire pour rejoindre son concubin en métropole ; <br/>
              - elle méconnaît les principes d'égalité et de non-discrimination dès lors qu'elle crée une différence de traitement entre les enfants ayant un parent de nationalité étrangère et les autres enfants ; <br/>
              - elle méconnaît l'intérêt supérieur de leur enfant dès lors que ce dernier est privé de tout document d'identité. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention internationale du 26 janvier 1990 relative aux droits de l'enfant ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code des relations entre le public et l'administration :<br/>
              - le décret n° 2005-1726 du 30 décembre 2005 ;<br/>
              - le décret n° 2014-1292 du 23 octobre 2014 ;<br/>
              - le code de justice administrative ;  <br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. D'une part, aux termes de l'article 4 du décret du 30 décembre 2005 visé ci-dessus : " Le passeport électronique est délivré, sans condition d'âge, à tout Français qui en fait la demande. / Il a une durée de validité de dix ans. Lorsqu'il est délivré à un mineur, sa durée de validité est de cinq ans. " Aux termes du premier alinéa de l'article 8 du même décret : " La demande de passeport faite au nom d'un mineur est présentée par une personne exerçant l'autorité parentale. " Aux termes de l'article 5 : " Le passeport électronique est délivré ou renouvelé sur production de la copie intégrale d'un des actes de l'état civil figurant sur une liste déterminée par arrêté du ministre de l'intérieur. / La preuve de la nationalité française du demandeur est établie à partir de l'un des actes de l'état civil visés à l'alinéa précédent, portant le cas échéant, en marge, l'une des mentions prévues à l'article 28 du code civil. / Lorsque les actes de l'état civil visés au deuxième alinéa ne suffisent pas à établir la qualité de Français du demandeur, celle-ci peut être établie par la production de l'une des pièces justificatives de la nationalité française mentionnées aux articles 34 et 52 du décret du 30 décembre 1993 susvisé ou d'un certificat de nationalité française. (...) " Aux termes du premier alinéa de l'article 22 : " Pour l'instruction des demandes de passeport, il est vérifié, par la consultation du fichier des personnes recherchées, qu'aucune décision judiciaire ni aucune circonstance particulière ne s'oppose à sa délivrance. " Pour l'application de ces dispositions, il appartient aux autorités administratives de s'assurer, sous le contrôle du juge, que les pièces produites à l'appui d'une demande de passeport sont de nature à établir l'identité et la nationalité du demandeur, seul un doute suffisant à cet égard pouvant justifier le refus de délivrance ou de renouvellement de passeport.<br/>
<br/>
              3. D'autre part, aux termes de l'article L. 231-1 du code des relations entre le public et l'administration : " Le silence gardé pendant deux mois par l'administration sur une demande vaut décision d'acceptation. " Aux termes de l'article L. 231-5 du même code : " Eu égard à l'objet de certaines décisions ou pour des motifs de bonne administration, l'application de l'article L. 231-1 peut être écartée par décret en Conseil d'Etat et en conseil des ministres. " Aux termes de l'article 1er du décret du 23 octobre 2014 visé ci-dessus : " En application des articles L. 231-5 et L. 231-6 du code des relations entre le public et l'administration, le silence gardé pendant deux mois par l'administration vaut décision de rejet pour les demandes dont la liste figure en annexe du présent décret. " Selon l'article 2 du même décret : " Pour les demandes mentionnées à l'article 1er du présent décret, l'annexe du présent décret fixe, lorsqu'il est différent du délai de deux mois, le délai à l'expiration duquel, en application des articles L. 231-5 et L. 231-6 du code des relations entre le public et l'administration, la décision de rejet est acquise. " Il résulte de l'annexe de ce décret que le silence gardé par l'administration pendant 4 mois sur une demande de délivrance d'un passeport fait naître une décision implicite de rejet.<br/>
<br/>
              4. En l'espèce, M. D... C..., ressortissant français né en 1976, résidant habituellement à Châtenay-Malabry (Hauts-de-Seine), a sollicité la délivrance d'un passeport pour le compte d'une enfant prénommée E..., née en Guyane le 22 juin 2020 de Mme A... B..., ressortissante haïtienne née en 1995 dont il est constant qu'elle se maintient en situation irrégulière sur le territoire français. M. C... avait reconnu l'enfant avant sa naissance par une déclaration effectuée à la mairie de Châtenay-Malabry le 7 février 2020. La demande de passeport a été déposée à la mairie de Kourou le 8 juillet 2020. Le 4 septembre 2020, la préfecture de la Guyane a demandé à M. C..., par lettre recommandée avec avis de réception, des éléments complémentaires afin de vérifier que la demande ne présentait pas un caractère frauduleux. L'intéressé n'a pas répondu à ce courrier qui, bien que l'adresse à laquelle il a été envoyé soit la même que celle qui figure dans la requête dont le Conseil d'Etat est saisi, a été retourné avec la mention : " destinataire inconnu à l'adresse ". Eu égard à l'ensemble de ces circonstances, et au vu des dispositions rappelées ci-dessus, la décision implicite par laquelle l'administration a rejeté la demande de passeport ne peut être regardée comme ayant porté une atteinte manifestement illégale à une liberté fondamentale. Par suite, et sans qu'il soit besoin de statuer sur l'urgence, les requérants ne sont pas fondés à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de la Guyane a rejeté leur demande tendant à la suspension de cette décision.<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. C... et Mme B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. D... C... et à Mme A... B....<br/>
Copie en sera adressée au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
