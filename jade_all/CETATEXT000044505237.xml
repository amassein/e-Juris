<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044505237</ID>
<ANCIEN_ID>JG_L_2021_12_000000439941</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/50/52/CETATEXT000044505237.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 15/12/2021, 439941, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439941</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP DE CHAISEMARTIN, DOUMIC-SEILLER</AVOCATS>
<RAPPORTEUR>M. Bertrand Mathieu</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:439941.20211215</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... D... a demandé à la Cour nationale du droit d'asile d'annuler la décision du 27 septembre 2019 par laquelle le directeur général de l'Office français de protection des réfugiés et apatrides (OFPRA) a rejeté sa demande d'asile et de lui reconnaître la qualité de réfugié ou, à défaut, de lui accorder le bénéfice de la protection subsidiaire. <br/>
<br/>
              Par une décision n° 19049620 du 31 décembre 2019, la Cour nationale du droit d'asile a rejeté sa demande. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 3 et 28 avril 2020 et le 5 août 2021, au secrétariat du contentieux du Conseil d'Etat, M. D... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de lui accorder le statut de réfugié, à défaut, le bénéfice de la protection subsidiaire ;<br/>
<br/>
              3°) de mettre à la charge de l'OFPRA la somme de 3 000 euros à verser à la SCP de Chaisemartin, Doumic-Seiller, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention de Genève du 28 juillet 1951 et le protocole signé à New-York le 31 janvier 1967 relatifs au statut des réfugiés ;<br/>
              - la directive 2011/95/UE du Parlement européen et du Conseil du 13 décembre 2011 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bertrand Mathieu, conseiller d'Etat en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteure publique,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP de Chaisemartin, Doumic-Seiller, avocat de M. D..., et à la SCP Foussard, Froger, avocat de l'Office français de protection des réfugiés et apatrides ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 27 septembre 2019, le directeur général de l'Office français de protection des réfugiés et apatrides (OFPRA) a refusé de faire droit à la demande de M. D..., de nationalité turque, tendant à ce que lui soit reconnu le statut de réfugié ou, à défaut, accordé le bénéfice de la protection subsidiaire. Par une décision du 31 décembre 2019, contre laquelle M. D... se pourvoit en cassation, la Cour nationale du droit d'asile a rejeté la demande de l'intéressé contestant le refus opposé par l'OFPRA. <br/>
<br/>
              2.	D'une part, aux termes des stipulations de l'article 1er, A, 2 de la convention de Genève du 28 juillet 1951 et du protocole signé à New York le 31 janvier 1967, doit être considérée comme réfugiée toute personne qui " craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut, ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays ". Aux termes de l'article L. 712-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, devenu l'article L. 512-1 : " Le bénéfice de la protection subsidiaire est accordé à toute personne qui ne remplit pas les conditions pour se voir reconnaître la qualité de réfugié et pour laquelle il existe des motifs sérieux et avérés de croire qu'elle courrait dans son pays un risque réel de subir l'une des atteintes graves suivantes : / (...) b) la torture ou des peines ou traitements inhumains ou dégradants (...) ".<br/>
<br/>
              3.	D'autre part, aux termes de l'article 9 de la directive 2011/95/UE du Parlement européen et du Conseil du 13 décembre 2011, les actes de persécution au sens de l'article 1er, section A, de la convention de Genève, " peuvent notamment prendre les formes suivantes : / (...) b) les mesures légales, administratives, de police et/ou judiciaires qui sont discriminatoires en soi ou mises en œuvre d'une manière discriminatoire ; / c) les poursuites ou sanctions qui sont disproportionnées ou discriminatoires ; / (...) e) les poursuites ou sanctions pour refus d'effectuer le service militaire en cas de conflit lorsque le service militaire supposerait de commettre des crimes ou d'accomplir des actes relevant du champ d'application des motifs d'exclusion visés à l'article 12, paragraphe 2 (...) ". Il résulte de l'arrêt de la Cour de justice de l'Union européenne du 19 novembre 2020, EZ c/ Bundesrepublik Deutschland (C 238/19), que, pour apprécier les risques de persécution à raison du refus d'effectuer son service militaire, cet article doit être interprété en ce sens qu'il ne s'oppose pas, lorsque le droit de l'État d'origine ne prévoit pas la possibilité de refuser d'effectuer le service militaire, à ce que ce refus soit constaté dans le cas où la personne concernée n'a pas formalisé son refus selon une procédure donnée et a fui son pays d'origine sans se présenter aux autorités militaires. Pour établir la réalité de ce refus, il convient de tenir compte de tous les faits pertinents concernant le pays d'origine au moment de statuer sur la demande, des informations et des documents pertinents présentés par le demandeur ainsi que de son statut individuel et de sa situation personnelle.<br/>
<br/>
              4.	Il ressort des énonciations de la décision attaquée que M. D... faisait valoir le risque qu'il encourait d'être exposé à des persécutions à raison de son refus d'effectuer son service militaire en Turquie. En se bornant, pour écarter l'existence d'un tel risque, à constater que l'intéressé, qui devait se présenter aux bureaux responsables de la conscription entre le 1er juillet et le 31 octobre 2020, année de ses vingt ans, n'avait pas pu, à la date de l'audience, être matériellement convoqué au service militaire, exprimer son objection de conscience et effectuer les démarches nécessaires, le cas échéant, à l'obtention d'un report ou d'une dispense, alors que la seule circonstance qu'il ne s'était pas présenté aux autorités militaires n'était pas de nature à faire obstacle au constat de son refus de servir, la Cour a commis une erreur de droit. <br/>
<br/>
              5.	Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que M. D... est fondé à demander l'annulation de la décision qu'il attaque.<br/>
<br/>
              6.	M. D... a obtenu le bénéfice de l'aide juridictionnelle. Dès lors, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Office français de protection des réfugiés et apatrides le versement à la SCP de Chaisemartin, Doumic-Seiller, avocat de M. D..., d'une somme de 3 000 euros, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'État.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La décision de la Cour nationale du droit d'asile du 31 décembre 2019 est annulée.<br/>
Article 2 : L'affaire est renvoyée devant la Cour nationale du droit d'asile.<br/>
Article 3 : L'Office français de protection des réfugiés et apatrides versera, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, une somme de 3 000 euros à la SCP de Chaisemartin, Doumic-Seiller, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à M. B... D... et à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>
              Délibéré à l'issue de la séance du 22 novembre 2021 où siégeaient : M. Jacques-Henri Stahl, président adjoint de la section du contentieux, présidant ; M. H... K..., M. Olivier Japiot, présidents de chambre ; M. I... M..., M. E... G..., Mme A... L..., M. F... N..., M. Jean-Yves Ollier, conseillers d'Etat et M. Bertrand Mathieu, conseiller d'Etat-rapporteur.<br/>
<br/>
              Rendu le 15 décembre 2021.<br/>
<br/>
<br/>
                 Le Président : <br/>
                 Signé : M. Jacques-Henri Stahl<br/>
 		Le rapporteur : <br/>
      Signé : M. Bertrand Mathieu<br/>
                 La secrétaire :<br/>
                 Signé : Mme J... C...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
