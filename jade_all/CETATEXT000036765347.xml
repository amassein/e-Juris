<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036765347</ID>
<ANCIEN_ID>JG_L_2018_03_000000419059</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/76/53/CETATEXT000036765347.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 22/03/2018, 419059, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-03-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419059</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:419059.20180322</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif d'Orléans, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au président du conseil départemental d'Indre-et-Loire de le faire bénéficier d'un accueil provisoire d'urgence dans le délai de 24 heures à compter de la notification de l'ordonnance à intervenir, sous astreinte de 100 euros par jour de retard. Par une ordonnance n° 1800996 du 16 mars 2018, le juge des référés du tribunal administratif d'Orléans a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 16 mars 2018 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de l'admettre au bénéfice de l'aide juridictionnelle provisoire ;<br/>
<br/>
              2°) d'annuler cette ordonnance ;<br/>
<br/>
              3°) d'enjoindre au président du conseil départemental de lui faire bénéficier d'un accueil provisoire d'urgence dans un délai de 24 heures à compter de la notification de la présente ordonnance, sous astreinte de 100 euros par jour de retard, et de réaliser à cette occasion une évaluation de sa minorité conformément aux dispositions de l'article R. 221-11 du code de l'action sociale et des familles et de l'arrêté du 17 novembre 2016 ;<br/>
<br/>
              4°) de mettre à la charge du conseil départemental d'Indre-et-Loire la somme de 1 500 euros à verser à son avocat au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve qu'il renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la requête est recevable ;<br/>
              - la condition d'urgence est remplie dès lors qu'il est, compte tenu de sa qualité de mineur isolé sans hébergement ni ressources, maintenu dans une situation de précarité, de danger et de vulnérabilité ;<br/>
              - il est porté une atteinte grave et manifestement illégale à son droit à l'hébergement d'urgence dès lors que le président du conseil départemental d'Indre-et-Loire, par une décision du 26 février 2018, lui a refusé le bénéfice de la prise en charge au titre de la protection de l'enfance par la plateforme d'accueil des mineurs non accompagnés conformément à l'article 6 de l'arrêté du 17 novembre 2016 pris en application du décret du 24 juin 2016 relatif aux modalités de l'évaluation des mineurs privés temporairement ou définitivement de la protection de leur famille.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code civil ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - l'arrêté du 17 novembre 2016 pris en application du décret n° 2016-840 du 24 juin 2016 relatif aux modalités de l'évaluation des mineurs privés temporairement ou définitivement de la protection de leur famille ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. A cet égard, il appartient au juge d'appel de prendre en considération les éléments recueillis par le juge du premier degré dans le cadre de la procédure écrite et orale qu'il a diligentée.<br/>
<br/>
              2. Aux termes du deuxième et du quatrième alinéa de l'article L. 223-2 du code de l'action sociale et des familles : " En cas d'urgence et lorsque le représentant légal du mineur est dans l'impossibilité de donner son accord, l'enfant est recueilli provisoirement par le service qui en avise immédiatement le procureur de la République. / Si, dans le cas prévu au deuxième alinéa du présent article, l'enfant n'a pas pu être remis à sa famille ou le représentant légal n'a pas pu ou a refusé de donner son accord dans un délai de cinq jours, le service saisit également l'autorité judiciaire en vue de l'application de l'article 375-5 du code civil ". L'article 375-5 du code civil dispose que dans cette situation, le procureur de la République ou le juge des enfants auquel la situation d'un mineur isolé a été signalée décide de l'orientation du mineur concerné, laquelle peut consister en application de l'article 375-3 du même code en son admission à l'aide sociale à l'enfance. En revanche, si le département qui a recueilli la personne refuse de saisir l'autorité judiciaire, notamment parce qu'il estime que cette personne a atteint la majorité, cette personne peut saisir elle-même le juge des enfants en application de l'article 375 du code civil afin qu'il soit décidé de son orientation.<br/>
<br/>
              3. Aux termes de l'article R. 221-11 du code de l'action sociale et des familles : " I.-Le président du conseil départemental du lieu où se trouve une personne se déclarant mineure et privée temporairement ou définitivement de la protection de sa famille met en place un accueil provisoire d'urgence d'une durée de cinq jours, à compter du premier jour de sa prise en charge, selon les conditions prévues aux deuxième et quatrième alinéas de l'article L. 223-2. / II.- Au cours de la période d'accueil provisoire d'urgence, le président du conseil départemental procède aux investigations nécessaires en vue d'évaluer la situation de cette personne au regard notamment de ses déclarations sur son identité, son âge, sa famille d'origine, sa nationalité et son état d'isolement. Cette évaluation s'appuie essentiellement sur : 1° Des entretiens conduits par des professionnels justifiant d'une formation ou d'une expérience définies par un arrêté des ministres mentionnés au III dans le cadre d'une approche pluridisciplinaire et se déroulant dans une langue comprise par l'intéressé (...) / III.-L'évaluation est réalisée par les services du département, ou par toute structure du secteur public ou du secteur associatif à laquelle la mission d'évaluation a été déléguée par le président du conseil départemental. L'évaluation est conduite selon les modalités précisées dans un référentiel national fixé par arrêté interministériel du ministre de la justice, du ministre de l'intérieur, du ministre chargé de la famille et du ministre chargé de l'outre-mer. IV.- Au terme du délai mentionné au I, ou avant l'expiration de ce délai si l'évaluation a été conduite avant son terme, le président du conseil départemental saisit le procureur de la République en vertu du quatrième alinéa de l'article L. 223-2 et du second alinéa de l'article 375-5 du code civil. En ce cas, l'accueil provisoire d'urgence mentionné au I se prolonge tant que n'intervient pas une décision de l'autorité judiciaire. / S'il estime que la situation de la personne mentionnée au présent article ne justifie pas la saisine de l'autorité judiciaire, il notifie à cette personne une décision de refus de prise en charge (...). En ce cas, l'accueil provisoire d'urgence mentionné au I prend fin ". Le même article dispose que les décisions de refus de prise en charge sont motivées et mentionnent les voies et délais de recours. Il renvoie, en outre, à un arrêté interministériel le soin de définir les modalités d'évaluation de la situation de la personne. Cet arrêté, en date du 17 novembre 2016, prévoit, à son article 6, que l'entretien d'évaluation porte au minimum sur six éléments qu'il détermine.<br/>
<br/>
              4. Il appartient aux autorités de l'Etat de mettre en oeuvre le droit à l'hébergement d'urgence reconnu par la loi à toute personne sans abri qui se trouve en situation de détresse médicale, psychique et sociale. Une obligation particulière pèse, en ce domaine, sur les autorités du département en faveur de tout mineur dont la santé, la sécurité ou la moralité sont en danger. Une carence caractérisée des autorités de l'Etat dans l'accomplissement de cette mission peut faire apparaître une atteinte manifestement illégale à une liberté fondamentale lorsqu'elle entraîne des conséquences graves pour la personne intéressée. Il incombe au juge des référés d'apprécier, dans chaque cas, les diligences accomplies par l'administration, en tenant compte des moyens dont elle dispose, ainsi que de l'âge, de l'état de santé et de la situation de famille de la personne intéressée.<br/>
<br/>
              5. M.A..., ressortissant algérien, soutient qu'il n'a pas effectivement bénéficié de l'hébergement de cinq jours prévu par les dispositions de l'article R. 221-11 du code de l'action sociale et des familles. S'il fait valoir que les services du département ont procédé, avant même sa prise en charge, à une évaluation de sa situation fondée sur un seul entretien, en méconnaissance présumée des dispositions précitées, il résulte de l'instruction diligentée par le juge des référés du tribunal administratif d'Orléans, d'une part, que la note d'évaluation établie le 26 février 2018 à la suite d'un entretien avec le requérant porte sur l'ensemble des six critères prévus par l'arrêté du 17 novembre 2016 et conclut à la majorité de l'intéressé et, d'autre part, que ni les dispositions du II de l'article R. 221-11 du code de l'action sociale et des familles, ni celles de l'arrêté du 17 novembre 2016 n'imposent que l'évaluation fasse nécessairement l'objet de plusieurs entretiens. L'instruction n'a ainsi pas établi que la note d'évaluation était insuffisante pour apprécier la situation du requérant. Le juge des référés du tribunal administratif d'Orléans a considéré que si, d'une part, le requérant n'avait pas bénéficié de l'hébergement de cinq jours prévu par les dispositions de l'article R. 221-11 du code de l'action sociale et des familles et si, d'autre part, les services départementaux n'avaient pas établi le rapport d'évaluation prévu à l'article 7 de l'arrêté du 17 novembre 2016, qui n'est d'ailleurs qu'une synthèse des entretiens, il devait être regardé, eu égard aux garanties apportées, tenant à un accueil immédiat en entretien, à une durée globale d'entretien suffisante, à l'examen des éléments d'évaluation visés par l'arrêté du 17 novembre 2016 et à l'absence d'erreur manifeste dans l'appréciation de l'absence de minorité du requérant, comme ayant bénéficié de l'évaluation prévue par l'article R. 221-11 dans des circonstances ne faisant pas apparaître une atteinte grave et manifestement illégale à une liberté fondamentale. Le juge des référés du tribunal administratif a également relevé que l'intéressé était célibataire et sans enfant. Il a jugé qu'il ne résultait ainsi, de la part du département, aucune carence caractérisée, constitutive d'une illégalité manifestement grave et illégale aux libertés fondamentales invoquées par le requérant. <br/>
<br/>
              6. M. A... n'apporte, en appel, aucun élément de nature à infirmer l'appréciation ainsi portée par le juge des référés du tribunal administratif d'Orléans.<br/>
<br/>
              7. Il résulte de tout ce qui précède, sans qu'il y ait lieu d'admettre l'intéressé au bénéfice de l'aide juridictionnelle provisoire, qu'il est manifeste que l'appel de M. A...ne peut être accueilli. La requête, y compris les conclusions présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, doit être rejetée selon la procédure prévue par l'article L. 522-3 du même code.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B...A....<br/>
Copie en sera adressée pour information au département d'Indre-et-Loire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
