<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029800091</ID>
<ANCIEN_ID>JG_L_2014_10_000000366784</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/80/00/CETATEXT000029800091.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème SSJS, 15/10/2014, 366784, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-10-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366784</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUTET, HOURDEAUX ; SCP LEVIS</AVOCATS>
<RAPPORTEUR>M. Frédéric Dieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:366784.20141015</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu, avec les pièces qui y sont visées, la décision en date du 10 février 2014 par laquelle le Conseil d'Etat statuant au contentieux, saisi du pourvoi présenté pour M. B...A..., tendant, en premier lieu, à l'annulation du jugement n° 1100905 du 16 octobre 2012 du tribunal administratif de Nancy rejetant sa demande tendant à la condamnation du département de Meurthe-et-Moselle à lui verser la somme de 1 530 euros en réparation du manquement à son obligation légale et contractuelle de l'informer avant sa parution de la publication de la brochure " Culture et sport, cultiver ses passions " en juillet 2007, contenant dix photographies dont il est l'auteur, en second lieu, à ce qu'il soit fait droit à sa demande, a sursis à statuer jusqu'à ce que le Tribunal des conflits ait tranché la question de savoir quel est l'ordre de juridiction compétent pour statuer sur le litige ; <br/>
<br/>
              Vu la décision n° 3955 du 7 juillet 2014 du Tribunal des conflits ; <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Dieu, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boutet, Hourdeaux, avocat de M. A...et à la SCP Lévis, avocat du département de Meurthe-et-Moselle ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A...a signé en 2004 avec le département de Meurthe-et-Moselle un contrat portant sur la cession des droits de reproduction et de diffusion des photos prises par lui pour le compte du département en 2002, 2003 et au premier trimestre 2004, pour une durée de six années ; que le tribunal administratif de Nancy a rejeté sa demande tendant à la condamnation du département pour avoir utilisé certaines de ses photos sans l'informer au préalable du support de publication ; que, sur pourvoi en cassation formé contre ce jugement, le Conseil d'Etat statuant au contentieux, par décision du 10 février 2014, a renvoyé au Tribunal des conflits, par application de l'article 35 du décret du 26 octobre 1849, le soin de décider sur la question de compétence ;<br/>
<br/>
              2. Considérant que, par décision du 7 juillet 2014, le Tribunal des conflits a jugé que, par dérogation à la règle énoncée par l'article 2 de la loi du 11 décembre 2001 portant mesures urgentes de réformes à caractère économique et financier selon laquelle les marchés publics ont le caractère de contrats administratifs de sorte que les litiges nés de leur exécution ou de leur rupture relèvent de la compétence du juge administratif, la recherche de la responsabilité contractuelle des personnes morales de droit public en matière de propriété littéraire et artistique relève, depuis l'entrée en vigueur de la loi du 17 mai 2011 de simplification et d'amélioration de la qualité du droit, de la compétence des juridictions de l'ordre judiciaire et que, par voie de conséquence, les litiges nés des actions dirigées par M. A...contre le département de Meurthe-et-Moselle au titre de son droit d'auteur relèvent de la compétence de la juridiction de l'ordre judiciaire ;<br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède que le tribunal administratif de Nancy a commis une erreur de droit en admettant la compétence de la juridiction administrative pour connaître du litige opposant M. A...au département de Meurthe-et-Moselle ; que son jugement doit, dès lors, être annulé ;<br/>
<br/>
              4. Considérant qu'il y a lieu d'évoquer et de statuer immédiatement sur la demande présentée par M. A...; que celle-ci doit être rejetée en tant qu'elle est portée devant un ordre de juridiction incompétent pour en connaître ;<br/>
<br/>
              5. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le département de Meurthe-et-Moselle en application des dispositions de l'article L. 761-1 du code de justice administrative ;  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 16 octobre 2012 du tribunal administratif de Nancy est annulé.<br/>
Article 2 : La demande présentée par M. A...devant le tribunal administratif de Nancy est rejetée comme portée devant un ordre de juridiction incompétent pour en connaître. <br/>
Article 3 : Les conclusions du département de Meurthe-et-Moselle présentées devant le Conseil d'Etat et le tribunal administratif de Nancy en application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à M. B...A...et au département de Meurthe-et-Moselle. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
