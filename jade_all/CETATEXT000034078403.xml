<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034078403</ID>
<ANCIEN_ID>JG_L_2017_02_000000407349</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/07/84/CETATEXT000034078403.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 06/02/2017, 407349, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-02-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407349</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:407349.20170206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 31 décembre 2016 et le 6 février 2017 au secrétariat du contentieux du Conseil d'Etat, la Ligue pour la protection des oiseaux (LPO) demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision de la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat, ordonnant à l'administration chargée de la police de la chasse de ne pas verbaliser les chasseurs tirant les oies cendrées jusqu'au 12 février 2017 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              L'association requérante soutient que : <br/>
              - le Conseil d'Etat est compétent en premier et dernier ressort pour statuer sur sa demande ;<br/>
              - la requête est recevable dès lors que, d'une part, l'intervention de la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat, est une décision orale susceptible d'un recours pour excès de pouvoir, d'autre part, l'association a qualité et intérêt pour agir et, enfin, un recours en annulation a été enregistré au secrétariat du contentieux du Conseil d'Etat ;<br/>
              - la condition d'urgence est remplie dès lors que la décision contestée, en favorisant jusqu'au 10 février 2017 le braconnage d'espèces protégées ou fermées à la chasse depuis le 31 janvier 2017, préjudicie de manière grave et immédiate aux intérêts défendus par l'association ;<br/>
              - il existe un doute sérieux quant à la légalité de la décision contestée ;<br/>
              - la décision est entachée d'une erreur de droit en ce qu'elle méconnaît les dispositions de l'arrêté du 19 janvier 2009 relatif aux dates de fermeture de la chasse aux oiseaux de passage et au gibier d'eau, pris en application de l'article L. 424-2 du code de l'environnement et fixant la date de la fermeture de la chasse aux oies au 31 janvier de chaque année ;<br/>
              - elle facilite la réalisation d'actes de chasse pénalement réprimés ;<br/>
              - la décision, identique à l'instruction du 28 janvier 2015 de la ministre de l'écologie, du développement durable et de l'énergie, est entachée de la même illégalité que les précédents arrêtés annulés par le Conseil d'Etat ;<br/>
<br/>
              Par un mémoire en défense, enregistré le 3 février 2017, la ministre de l'écologie, du développement durable et de l'énergie, chargée des relations internationales sur le climat, conclut au rejet de la requête. Elle soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par la société requérante ne sont pas fondés ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - la directive 2009/147/CE du Parlement européen et du Conseil du 30 novembre 2009 ;<br/>
              - l'arrêté du 19 janvier 2009 du ministère de l'écologie, de l'énergie, du développement durable et de l'aménagement du territoire relatif aux dates de fermeture de la chasse aux oiseaux de passage et au gibier d'eau ;<br/>
              - le code de l'environnement ; <br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la ligue de protection des oiseaux, d'autre part, la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du lundi 6 février 2017 à 11 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Stoclet, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la Ligue de protection des animaux ;<br/>
<br/>
                          - les représentants de la Ligue de protection des oiseaux ;<br/>
<br/>
              - les représentants de la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              2. Aux termes de l'article L. 424-2 alinéa 2 du code de l'environnement règlement : " Nul ne peut chasser en dehors des périodes d'ouverture de la chasse fixées par l'autorité administrative selon des conditions déterminées par décret en Conseil d'Etat. Les oiseaux ne peuvent être chassés ni pendant la période nidicole ni pendant les différents stades de reproduction et de dépendance. Les oiseaux migrateurs ne peuvent en outre être chassés pendant leur trajet de retour vers leur lieu de nidation ". Aux termes de l'article 1er de l'arrêté du 19 janvier 2009 du ministre de l'écologie, de l'énergie, du développement durable et de l'aménagement du territoire relatif aux dates de fermeture de la chasse aux oiseaux de passage et au gibier d'eau,  la fermeture de la chasse aux oies est fixée au 31 janvier de chaque année. <br/>
<br/>
              3. Par la présente requête, la Ligue de protection des oiseaux demande au juge des référés du Conseil d'Etat la suspension de l'exécution de la décision du 25 janvier 2017 par laquelle la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat, ordonne à l'administration chargée de la police de la chasse de ne pas verbaliser les chasseurs tirant les oies cendrées jusqu'au 12 février 2017. Contrairement à ce qui est soutenu en défense, il résulte clairement des déclarations par lesquelles l'existence de la décision et son contenu ont été rendus publics par la réponse de la ministre à une question parlementaire orale à l'assemblée nationale le 25 janvier 2017 que la décision n'invite pas les agents à privilégier la pédagogie plutôt que la répression, mais leur prescrit sans aucune exception de ne pas sanctionner les actes illégaux qu'ils pourraient constater.<br/>
<br/>
              4. En dépit de la modicité alléguée des populations d'oies susceptibles d'être chassées durant les quatre jours séparant la date à laquelle il est statué en référé sur la requête de la Ligue de protection des oiseaux de l'expiration de l'instruction attaquée, la possibilité créée par l'ordre donné de ne pas sanctionner l'interdiction de tirer ces espèces sur l'ensemble du territoire national en méconnaissance des dates de fermeture de chasse fixées en application du droit de l'Union européenne pour assurer la protection de ces oiseaux ne repose sur aucune justification d'intérêt général. La volonté déclarée d'apaiser l'hostilité de certains chasseurs au droit en vigueur ne saurait d'évidence constituer une telle justification. L'instruction crée donc, notamment par le trouble à l'ordre public qu'elle constitue, et en raison tant des prélèvements pouvant être opérés illégalement que des troubles créés pour l'ensemble de la faune en zone humide,  une situation d'urgence au sens de l'article L 521-1.<br/>
<br/>
              		5. S'il était loisible à la ministre d'édicter des lignes directrices gouvernant l'exercice du pouvoir répressif, et notamment d'inviter les agents chargés de la répression des actes de chasse illégaux à privilégier dans une certaine mesure des admonestations pédagogiques envers les contrevenants à la réglementation, sans exclure absolument toute action répressive selon les circonstances, le moyen tiré de ce que l'instruction hiérarchique donnée de ne pas appliquer la loi et de ne pas sanctionner les infractions constatées aux dates de fermeture de la chasse aux oies  est dénuée de toute base légale et entachée d'erreur de droit apparaît, en l'état de l'instruction, sérieux et de nature à entraîner la suspension de la décision attaquée.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat, le versement à la ligue de protection des oiseaux d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La décision de la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat, ordonnant à l'administration chargée de la police de la chasse de ne pas verbaliser les chasseurs tirant les oies cendrées jusqu'au 12 février 2017 est suspendue. <br/>
<br/>
Article 2 : La ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat versera à la Ligue de protection des oiseaux une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente ordonnance sera notifiée à la Ligue de protection des oiseaux et à la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
