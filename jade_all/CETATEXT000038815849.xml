<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038815849</ID>
<ANCIEN_ID>JG_L_2019_07_000000426468</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/81/58/CETATEXT000038815849.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 24/07/2019, 426468, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426468</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Françoise Tomé</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:426468.20190724</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le préfet de la Haute-Savoie a déféré au tribunal administratif de Grenoble les deux délibérations n° Del.2018-V-108 et n° Del.2018-V-109 du 26 juin 2018 du conseil municipal de la commune nouvelle de Faverges-Seythenex, prenant acte des installations de M. B... G...et de Mme E...F...comme conseillers municipaux, à la suite des démissions de M. A...D...et de Mme H...C.... <br/>
<br/>
              Le tribunal administratif de Grenoble n'ayant pas statué dans le délai de deux mois qui lui était imparti pour se prononcer sur le déféré du préfet de la Haute-Savoie, ce dernier a saisi le Conseil d'Etat. <br/>
<br/>
              Par une requête, un mémoire en réplique et un mémoire en duplique, enregistrés les 20 décembre 2018 et les 3 et 21 mai 2019 au secrétariat du contentieux du Conseil d'Etat, le préfet de la Haute-Savoie demande au Conseil d'Etat d'annuler ces deux délibérations du 26 juin 2018 du conseil municipal de Faverges-Seythenex.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ; <br/>
              - le code général des collectivités territoriales ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Françoise Tomé, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il résulte de l'instruction que la commune de Faverges-Seythenex est une commune nouvelle, créée par arrêté préfectoral du 30 septembre 2015, issue de la fusion de Faverges, commune de plus de 1 000 habitants, et de Seythenex, commune de moins de 1 000 habitants. Le conseil municipal de cette commune nouvelle est formé de l'ensemble des membres en exercice des conseils municipaux des deux anciennes communes, conformément à l'article L. 2113-7 du code général des collectivités territoriales. A la suite des démissions de M. A... D...et de Mme H...C..., conseillers municipaux élus dans l'ancienne commune de Faverges, le conseil municipal de la commune nouvelle de Faverges-Seythenex a approuvé, le 26 juin 2018, deux délibérations procédant à l'installation des suivants de liste, M. B... G...et Mme E...F.... Par un déféré du 17 juillet 2018, le préfet de la Haute-Savoie a demandé au tribunal administratif de Grenoble l'annulation de ces deux délibérations. Par courrier du 21 novembre 2018, le tribunal administratif de Grenoble a informé le préfet de la Haute-Savoie que, faute d'avoir statué dans le délai de deux mois prévus par l'article R. 120 du code électoral, il était dessaisi. Par une requête enregistrée le 20 décembre 2018, le préfet de la Haute-Savoie a saisi le Conseil d'Etat. <br/>
<br/>
              Sur les mémoires en défense de la commune de Faverges-Seythenex :<br/>
<br/>
              2. Aux termes de l'article L. 248 du code électoral : " Tout électeur et tout éligible a le droit d'arguer de nullité les opérations électorales de la commune devant le tribunal administratif. / Le préfet, s'il estime que les conditions et les formes légalement prescrites n'ont pas été remplies, peut également déférer les opérations électorales au tribunal administratif ". Aux termes de l'article L. 249 : " Le tribunal administratif statue, sauf recours au Conseil d'Etat ". Aux termes de l'article L. 250 du même code : " Le recours au Conseil d'Etat contre la décision du tribunal administratif est ouvert soit au préfet, soit aux parties intéressées ". Il résulte de ces dispositions que, dans un contentieux électoral, faute de justifier d'un intérêt propre, une commune ne peut avoir, quand bien même elle aurait été mise en cause dans l'instance, ni la qualité de partie, ni, d'ailleurs, celle d'intervenant. Par suite, il y a lieu d'écarter les mémoires de la commune de Faverges-Seythenex. <br/>
<br/>
              Au fond : <br/>
<br/>
              3. D'une part, en vertu de l'article L. 2113-1 du code général des collectivités territoriales, les communes nouvelles sont soumises aux règles applicables aux communes, sous réserve des dispositions du chapitre III du titre Ier du livre Ier de la deuxième partie de ce code et des autres dispositions législatives qui leur sont propres. L'article L. 2113-7 du même code dispose que : " I- Jusqu'au prochain renouvellement suivant la création de la commune nouvelle, le conseil municipal est composé : / 1° De l'ensemble des membres en exercice des conseils municipaux des anciennes communes, si les conseils municipaux des communes concernées le décident par délibérations concordantes prises avant la création de la commune nouvelle (...) ". <br/>
<br/>
              4. D'autre part, aux termes de l'article L. 270 du code électoral, applicable aux communes de plus de 1 000 habitants, dans lesquelles l'élection des conseillers municipaux a lieu au scrutin de liste : " Le candidat venant sur une liste immédiatement après le dernier élu est appelé à remplacer le conseiller municipal élu sur cette liste dont le siège devient vacant pour quelque cause que ce soit. La constatation, par la juridiction administrative, de l'inéligibilité d'un ou plusieurs candidats n'entraîne l'annulation de l'élection que du ou des élus inéligibles. La juridiction saisie proclame en conséquence l'élection du ou des suivants de liste. / (...) / Lorsque les dispositions des alinéas précédents ne peuvent plus être appliquées, il est procédé au renouvellement du conseil municipal : / 1° Dans les trois mois de la dernière vacance, si le conseil municipal a perdu le tiers de ses membres, et sous réserve de l'application du deuxième alinéa de l'article L. 258 ; / 2° Dans les conditions prévues aux articles L. 2122-8 et L. 2122-14 du code général des collectivités territoriales, s'il est nécessaire de compléter le conseil avant l'élection d'un nouveau maire ". Par ailleurs, pour sa part, le premier alinéa de l'article L. 258 du même code, applicable aux communes de moins de 1 000 habitants, dans lesquelles l'élection des conseillers municipaux a lieu au scrutin majoritaire, dispose que : " Lorsque le conseil municipal a perdu, par l'effet des vacances survenues, le tiers de ses membres, il est, dans le délai de trois mois à dater de la dernière vacance, procédé à des élections complémentaires (...) ". <br/>
<br/>
              5. Il résulte des dispositions de l'article L. 2113-7 du code des collectivités territoriales que, si les anciens conseils municipaux l'ont décidé par délibérations concordantes, le conseil municipal d'une commune nouvelle issue de la fusion de plusieurs communes est composé, à titre transitoire jusqu'au premier renouvellement suivant la création de la commune nouvelle, des seuls conseillers municipaux en exercice lors de la fusion. Ces dispositions font obstacle, pendant la période allant de la création de la commune nouvelle au premier renouvellement du conseil municipal suivant cette création, à l'application des dispositions de l'article L. 270 du code électoral permettant, pour les communes de plus de 1 000 habitants, le remplacement des conseillers municipaux dont le siège devient vacant par les suivants de liste.<br/>
<br/>
              6. Il résulte de ce qui précède que lorsqu'un siège de conseiller municipal devient vacant après la création d'une commune nouvelle et avant le premier renouvellement du conseil municipal suivant cette création, il ne peut être pourvu au remplacement par le suivant de liste. Le préfet de la Haute-Savoie est, dès lors, fondé à demander l'annulation des deux délibérations du 26 juin 2018 par lesquelles le conseil municipal de Faverges-Seythenex, a à la suite des démissions de M. A...D...et Mme H...C..., conseillers municipaux élus dans l'ancienne commune de Faverges, installé M. B... G...et Mme E...F...en qualité de conseillers municipaux. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1 : Les délibérations n° Del.2018-V-108 et n° Del.2018-V-109 du 26 juin 2018 du conseil municipal de Faverges-Seythenex sont annulées. <br/>
Article 2 : Il y a lieu de constater la vacance des sièges de conseillers municipaux de Faverges-Seythenex précédemment occupés par M. A...D...et par Mme H...C.... <br/>
Article 3 : La présente décision sera notifiée au préfet de la Haute-Savoie, à M. B... G..., à Mme E...F...et à la commune de Faverges-Seythenex.<br/>
Copie en sera adressée à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
