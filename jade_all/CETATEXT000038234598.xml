<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038234598</ID>
<ANCIEN_ID>JG_L_2019_03_000000421716</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/23/45/CETATEXT000038234598.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 13/03/2019, 421716, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-03-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421716</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY ; SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:421716.20190313</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B... A...a demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir la décision du 5 mars 2015 par laquelle l'inspecteur du travail de l'unité territoriale de Paris a autorisé la société Triomphe sécurité à le licencier, ainsi que la décision implicite de la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social rejetant son recours hiérarchique contre cette décision. Par un jugement n° 1515744 du 2 mars 2016, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 16PA01531 du 24 avril 2018, la cour administrative d'appel de Paris a rejeté l'appel formé par M. A... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 25 juin et 25 septembre 2018 et le 15 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat et de la société Triomphe sécurité la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de M. A...et à la SCP Baraduc, Duhamel, Rameix, avocat de la société Triomphe sécurité ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.  Il ressort des pièces du dossier soumis aux juges du fond que M.A..., salarié de la société Triomphe sécurité ayant la qualité de salarié protégé, a demandé au tribunal administratif de Paris l'annulation de la décision du 5 mars 2015 par laquelle l'inspecteur du travail compétent a autorisé son licenciement, ainsi que la décision implicite du ministre chargé du travail rejetant son recours hiérarchique. Il se pourvoit en cassation contre l'arrêt du 24 avril 2018 par lequel la cour administrative d'appel de Paris a rejeté son appel dirigé contre le jugement du 2 mars 2016 du tribunal administratif de Paris ayant rejeté sa demande.<br/>
<br/>
              2.  En premier lieu, aux termes du deuxième alinéa de l'article R. 611-1 du code de justice administrative : " La requête, le mémoire complémentaire annoncé dans la requête et le premier mémoire de chaque défendeur sont communiqués aux parties (...) ". Aux termes du premier alinéa de l'article R. 613-2 du même code : " Si le président de la formation de jugement n'a pas pris d'ordonnance de clôture, l'instruction est close trois jours francs avant la date de l'audience indiquée dans l'avis d'audience prévu à l'article R. 711-2. Cet avis le mentionne ".<br/>
<br/>
              3.  Si M. A...soutient qu'il n'a pas disposé d'un délai suffisant pour répondre au premier mémoire en défense de la ministre du travail, il ressort des pièces du dossier soumis aux juges du fond que la teneur de ce mémoire de l'administration n'était pas substantiellement différente de celle des mémoires en défense de la société Triomphe sécurité qui avaient été communiqués antérieurement à M.A.... Celui-ci n'est, par suite, pas fondé à soutenir que l'arrêt qu'il attaque est entaché d'irrégularité.<br/>
<br/>
              4.  En second lieu, en estimant que, d'une part, dans le cadre de la procédure d'examen par l'administration de la demande d'autorisation de licenciement présentée par la société Triomphe sécurité, M. A... avait été informé dès le 25 février 2015 de la liste des pièces justifiant les faits qui lui étaient reprochés et, d'autre part, que la réorganisation de l'entreprise invoquée par l'employeur ne dissimulait pas une manoeuvre, la cour administrative d'appel de Paris, dont l'arrêt n'est pas entaché d'insuffisance de motivation sur ces points, a porté sur les pièces du dossier qui lui était soumis une appréciation souveraine, exempte de dénaturation.<br/>
<br/>
              5.  Il résulte de tout ce qui précède que le pourvoi de M. A... doit être rejeté, y compris, par voie de conséquence, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. Dans les circonstances de l'espèce, il n'y a pas lieu de mettre à la charge de M. A... la somme que demande la société Triomphe sécurité au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
Article 2 : Le surplus des conclusions de la société Triomphe sécurité, présenté au titre de l'article L. 761-1 du code de justice administrative, est rejeté. <br/>
Article 3 : La présente décision sera notifiée à M. C...A...et à  la société Triomphe Sécurité.<br/>
Copie en sera adressée à la  ministre du  travail.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
