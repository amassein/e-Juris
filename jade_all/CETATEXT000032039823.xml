<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032039823</ID>
<ANCIEN_ID>JG_L_2016_01_000000396066</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/03/98/CETATEXT000032039823.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 18/01/2016, 396066, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-01-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396066</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2016:396066.20160118</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au juge des référés du tribunal administratif de Pau, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner la suspension de l'exécution de l'arrêté du 4 décembre 2015 par lequel le ministre de l'intérieur l'a assigné à résidence sur le territoire de la commune d'Ancizan. Par une ordonnance n° 1502670 du 30 décembre 2015, le juge des référés du tribunal administratif de Pau n'a que partiellement fait droit à sa demande en enjoignant au ministre de l'intérieur de définir, dans un délai de 7 jours, la durée pendant laquelle les mesures prévues par l'arrêté du 4 décembre 2015 seraient susceptibles d'être appliquées.<br/>
              Par une requête enregistrée le 12 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) de l'admettre au bénéfice de l'aide juridictionnelle à titre provisoire ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence est remplie ; <br/>
              - l'arrêté dont la suspension de l'exécution est demandée, modifié par un arrêté du 31 décembre 2015 du ministre de l'intérieur, porte une atteinte grave et manifestement illégale à la liberté d'aller et venir et à son droit à la sûreté ;<br/>
              - il méconnaît les dispositions de l'article 6 de la loi du 3 avril 1955 relative à l'état d'urgence, d'une part, en ne lui permettant pas de résider dans une agglomération ou à proximité immédiate d'une agglomération, d'autre part, en ne prévoyant pas les dispositions nécessaires pour assurer sa subsistance et celle de sa famille ;<br/>
              - l'arrêté contesté est entaché d'une erreur d'appréciation dès lors qu'il ne peut pas être regardé comme représentant une menace grave pour la sécurité et l'ordre publics et eu égard à la sévérité des mesures mises en oeuvre.<br/>
<br/>
              Par un mémoire en défense, enregistré le 14 janvier 2016, le ministre de l'intérieur conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par M. B...ne sont pas fondés.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la loi n° 55-385 du 3 avril 1955 ;<br/>
              - la loi n° 2015-1501 du 20 novembre 2015 ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 2015-1475 du 14 novembre 2015 ;<br/>
              - le décret n° 2015-1476 du 14 novembre 2015 ;<br/>
              - le décret n° 2015-1478 du 14 novembre 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. B..., d'autre part, le ministre de l'intérieur ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 15 janvier 2016 à 11 heures 15 au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Spinosi, avocat au Conseil d'Etat et à la Cour de Cassation, avocat de M. B... ;<br/>
<br/>
              - la représentante du ministre de l'intérieur ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. " ;<br/>
<br/>
              2. Considérant qu'en application de la loi du 3 avril 1955, l'état d'urgence a été déclaré par le décret n° 2015-1475 du 14 novembre 2015, à compter du même jour à zéro heure, sur le territoire métropolitain et en Corse et prorogé pour une durée de trois mois, à compter du 26 novembre 2015, par l'article 1er de la loi du 20 novembre 2015 ; qu'aux termes de l'article 6 de la loi du 3 avril 1955, dans sa rédaction issue de la loi du 20 novembre 2015 : " Le ministre de l'intérieur peut prononcer l'assignation à résidence, dans le lieu qu'il fixe, de toute personne résidant dans la zone fixée par le décret mentionné à l'article 2 et à l'égard de laquelle il existe des raisons sérieuses de penser que son comportement constitue une menace pour la sécurité et l'ordre publics dans les circonscriptions territoriales mentionnées au même article 2. (...) / La personne mentionnée au premier alinéa du présent article peut également être astreinte à demeurer dans le lieu d'habitation déterminé par le ministre de l'intérieur, pendant la plage horaire qu'il fixe, dans la limite de douze heures par vingt-quatre heures. / L'assignation à résidence doit permettre à ceux qui en sont l'objet de résider dans une agglomération ou à proximité immédiate d'une agglomération. (...) / L'autorité administrative devra prendre toutes dispositions pour assurer la subsistance des personnes astreintes à résidence ainsi que celle de leur famille. / Le ministre de l'intérieur peut prescrire à la personne assignée à résidence : / 1° L'obligation de se présenter périodiquement aux services de police ou aux unités de gendarmerie, selon une fréquence qu'il détermine dans la limite de trois présentations par jour, en précisant si cette obligation s'applique y compris les dimanches et jours fériés ou chômés (...) " ; qu'il résulte de l'article 1er du décret n° 2015-1476 du 14 novembre 2015, modifié par le décret n° 2015-1478 du même jour, que les mesures d'assignation à résidence sont applicables à l'ensemble du territoire métropolitain et de la Corse à compter du 15 novembre à minuit ;<br/>
<br/>
              3. Considérant qu'il résulte de l'instruction que, par un arrêté du 4 décembre 2015, le ministre de l'intérieur a astreint M. B...à résider sur le territoire de la commune d'Ancizan (Hautes-Pyrénées), avec obligation de se présenter une fois par jour, à 11 heures 30, à la brigade territoriale de gendarmerie d'Arreau les lundi, jeudi et samedi, et à la brigade de gendarmerie de Vignec les mardi, mercredi, vendredi et dimanche, y compris les jours fériés ou chômés, et lui a imposé de demeurer tous les jours de 20 heures à 6 heures dans les locaux où il réside ; que cet arrêté prévoit que M. B... ne peut se déplacer en dehors de son lieu d'assignation à résidence sans avoir obtenu préalablement une autorisation écrite établie par le préfet des Hautes-Pyrénées ; que, par une requête enregistrée le 28 décembre 2015, M. B...a demandé au juge des référés du tribunal administratif de Pau d'ordonner la suspension de l'exécution de l'arrêté du 4 décembre 2015 ; que, par une ordonnance du 30 décembre 2015, le juge des référés du tribunal administratif de Pau a enjoint au ministre de l'intérieur de définir, dans un délai de 7 jours, la durée pendant laquelle les mesures prévues par l'arrêté du 4 décembre 2015 seraient susceptibles d'être appliquées et a rejeté le surplus des conclusions de M.B... ; que, par un arrêté du 31 décembre 2015, le ministre de l'intérieur a modifié les conditions de l'assignation à résidence de M. B... en l'assignant à résider sur le territoire de la commune d'Aragnouet et en lui imposant de demeurer tous les jours de 20 heures à 6 heures dans ses nouveaux locaux de résidence, sans préjudice des dispositions de l'arrêté initial concernant notamment les lieux et horaires de pointage, et en fixant la fin de l'état d'urgence comme terme à son arrêté ; que M. B...relève appel de l'ordonnance du 30 décembre 2015 du juge des référés du tribunal administratif de Pau en tant qu'il a rejeté sa demande tendant à la suspension de l'exécution de l'arrêté du ministre de l'intérieur l'assignant à résidence ;<br/>
<br/>
              En ce qui concerne la condition d'urgence :<br/>
<br/>
              4. Considérant qu'eu égard à son objet et à ses effets, notamment aux restrictions apportées à la liberté d'aller et venir, une décision prononçant l'assignation à résidence d'une personne, prise par l'autorité administrative en application de l'article 6 de la loi du 3 avril 1955, porte, en principe et par elle-même, sauf à ce que l'administration fasse valoir des circonstances particulières, une atteinte grave et immédiate à la situation de cette personne, de nature à créer une situation d'urgence justifiant que le juge administratif des référés, saisi sur le fondement de l'article L. 521-2 du code de justice administrative, puisse prononcer dans de très brefs délais, si les autres conditions posées par cet article sont remplies, une mesure provisoire et conservatoire de sauvegarde ; qu'aucun des éléments que le ministre de l'intérieur a fait valoir, dans ses écritures et au cours de l'audience publique, ne conduit à remettre en cause, au cas d'espèce, l'existence d'une situation d'urgence caractérisée de nature à justifier l'intervention du juge des référés dans les conditions d'urgence particulière prévues par l'article L. 521-2 du code de justice administrative ;<br/>
<br/>
              En ce qui concerne la condition tenant à l'atteinte grave et manifestement illégale à une liberté fondamentale :<br/>
<br/>
              5. Considérant qu'il appartient au juge des référés de s'assurer, en l'état de l'instruction devant lui, que l'autorité administrative, opérant la conciliation nécessaire entre le respect des libertés et la sauvegarde de l'ordre public, n'a pas porté d'atteinte grave et manifestement illégale à une liberté fondamentale, que ce soit dans son appréciation de la menace que constitue le comportement de l'intéressé, compte tenu de la situation ayant conduit à la déclaration de l'état d'urgence, ou dans la détermination des modalités de l'assignation à résidence ; que le juge des référés, s'il estime que les conditions définies à l'article L. 521-2 du code de justice administrative sont réunies, peut prendre toute mesure qu'il juge appropriée pour assurer la sauvegarde de la liberté fondamentale à laquelle il a été porté atteinte ;<br/>
<br/>
              6. Considérant qu'il résulte de l'instruction et notamment de deux " notes blanches " des services de renseignement comportant des éléments précis et circonstanciés, que M. B...est membre de l'association " Hayat ", dirigée par un prédicateur radical prônant le repli identitaire et le départ vers une terre d'islam, et dont plusieurs des membres projettent de se rendre à la frontière turco-syrienne dans le cadre d'un projet qu'ils présentent comme humanitaire ; qu'à l'occasion d'entretiens administratifs, et notamment en juillet 2014, M. B...et sa compagne ont fait part de leur intention de se rendre en Syrie ; que, le 12 novembre 2015, M. B... a embarqué à Toulouse, avec sa compagne enceinte de cinq mois ainsi que leur enfant âgé de dix mois, sur un vol à destination d'Istanbul, sans avoir prévenu les services de police, alors qu'il s'y était engagé, ni même sa famille ; que, refoulé de Turquie, le couple a été entendu par les services de police français le 13 novembre 2015 ; qu'à cette occasion, M. B... a indiqué entretenir des liens suivis avec plusieurs individus suspectés d'appartenir à un réseau d'acheminement de combattants vers la Syrie, dont l'un est détenu préventivement en Belgique ; qu'une fouille des bagages du couple réalisée par les services de douane a permis d'y découvrir une somme totale de 13 000 euros en billets de 200 euros ; qu'à l'issue de cet entretien, une interdiction de sortie du territoire national a été notifiée au couple ; qu'il est par ailleurs constant que M.B... a déclaré adhérer à l'idéologie du groupe " Ahrar al-Sham ", membre de la coalition armée " Ansar al-Charia ", engagée dans les combats en Syrie ;<br/>
              7. Considérant qu'eu égard à l'ensemble des éléments ainsi recueillis au cours des échanges écrits et oraux, il n'apparaît pas, en l'état de l'instruction, qu'en prononçant l'assignation à résidence de M. B... au motif qu'il existait de sérieuses raisons de penser que son comportement constitue une menace grave pour la sécurité et l'ordre publics et en fixant les modalités d'exécution, le ministre de l'intérieur ait porté une atteinte grave et manifestement illégale à une liberté fondamentale ; <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que M. B...n'est pas fondé à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Pau a rejeté sa demande ; que son appel ne peut donc être accueilli ; que, par suite, ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative ne peuvent, par voie de conséquence, qu'être rejetées ; qu'enfin, il y a lieu d'admettre M. B...au bénéfice de l'aide juridictionnelle à titre provisoire ;<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : M. B...est admis au bénéfice de l'aide juridictionnelle à titre provisoire.<br/>
Article 2 : La requête de M. B...est rejetée.<br/>
Article 3 : La présente ordonnance sera notifiée à M. A...B...et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
