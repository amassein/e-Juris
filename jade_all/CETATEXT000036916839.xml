<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036916839</ID>
<ANCIEN_ID>JG_L_2018_05_000000401901</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/91/68/CETATEXT000036916839.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème chambre jugeant seule, 16/05/2018, 401901, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-05-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401901</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème chambre jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>Mme Céline  Guibé</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Yohann Bénard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:401901.20180516</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Paris de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu auxquelles il a été assujetti au titre de l'année 2009, ainsi que des pénalités correspondantes. Par un jugement n° 1402052 du 24 avril 2015, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15PA01796 du 27 mai 2016, la cour administrative d'appel de Paris a rejeté l'appel formé par M. A...contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 27 juillet et 27 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Céline Guibé, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Yohann Bénard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de M.A....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M.A..., associé de la société en participation Cofina 01409, a imputé sur le montant de son impôt sur le revenu au titre de l'année 2009, sur le fondement des dispositions de l'article 199 undecies B du code général des impôts, une réduction d'impôt à raison d'investissements réalisés notamment en Guadeloupe et en Nouvelle-Calédonie par l'intermédiaire de cette société. M. A...se pourvoit en cassation contre l'arrêt du 27 mai 2016 par lequel la cour administrative d'appel de Paris a rejeté l'appel qu'il avait formé contre le jugement du 24 avril 2015 du tribunal administratif de Paris rejetant sa demande tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu auxquelles il a été assujetti au titre de l'année 2009.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article 199 undecies B du code général des impôts, dans sa rédaction applicable au litige : " Les contribuables domiciliés en France au sens de l'article 4 B peuvent bénéficier d'une réduction d'impôt sur le revenu à raison des investissements productifs neufs qu'ils réalisent dans les départements d'outre-mer (...) dans le cadre d'une entreprise exerçant une activité agricole ou une activité industrielle, commerciale ou artisanale relevant de l'article 34 ". Aux termes du vingtième alinéa du même article : " La réduction d'impôt prévue au premier alinéa est pratiquée au titre de l'année au cours de laquelle l'investissement est réalisé. ". Aux termes de l'article 95 Q de l'annexe II à ce code, dans sa rédaction alors applicable : " La réduction d'impôt prévue au I de l'article 199 undecies B du code général des impôts est pratiquée au titre de l'année au cours de laquelle l'immobilisation est créée par l'entreprise ou lui est livrée ou est mise à sa disposition dans le cadre d'un contrat de crédit-bail. ". Il résulte de la combinaison de ces dispositions que le fait générateur de la réduction d'impôt prévue à l'article 199 undecies B est la date de la création de l'immobilisation au titre de laquelle l'investissement productif a été réalisé ou de sa livraison effective dans le département d'outre-mer. Dans ce dernier cas, la date à retenir est celle à laquelle l'entreprise, disposant matériellement de l'investissement productif, peut commencer son exploitation effective et, dès lors, en retirer des revenus.<br/>
<br/>
              S'agissant de l'investissement réalisé en Guadeloupe :<br/>
<br/>
              3. Il ressort des énonciations de l'arrêt attaqué que la cour a relevé que M. A... ne produisait aucun élément d'ordre financier ni aucun document susceptible d'établir que la SEP Cofina 01409 avait réellement acquis la structure ombrière à raison de laquelle il entendait bénéficier de la réduction d'impôt prévue par les dispositions de l'article 199 undecies B du code général des impôts. En statuant ainsi, par une appréciation souveraine des faits exempte de dénaturation, la cour, qui a suffisamment motivé son arrêt, n'a, contrairement à ce que soutient le requérant, pas érigé le paiement effectif de la structure en condition légale de cette réduction d'impôt et n'a, dès lors, pas commis l'erreur de droit qui lui est reprochée.<br/>
<br/>
              S'agissant de l'investissement réalisé en Nouvelle-Calédonie :<br/>
<br/>
              4. Pour soutenir que la procédure suivie devant la cour aurait méconnu le principe du contradictoire, M. A...fait valoir que la cour s'est fondée sur une attestation, établie par un représentant de la société Solairinox, dans laquelle celui-ci mentionne qu'il a reçu le paiement partiel d'une facture n° 668, qui n'a été produite par l'administration ni en première instance, ni en appel. Toutefois, il ressort des énonciations de l'arrêt attaqué que la cour n'a mentionné cette attestation, sur laquelle elle n'a pas fondé sa solution, qu'en citant les termes de la proposition de rectification de la société en participation Cofina 01409 en date du 25 juillet 2012, qui était annexée à la proposition de rectification adressée à M.A.... En s'abstenant de prescrire la production de cette attestation, la cour s'est livrée à une appréciation souveraine qui n'est pas susceptible d'être discutée devant le juge de cassation.<br/>
<br/>
              5. Il ressort des énonciations de l'arrêt attaqué que la cour a relevé qu'une facture du 1er septembre 2009 faisait état de la vente de vingt chauffe-eau solaires par la société Solairinox à la société Solairinox Services pour un montant de 66 972,40 euros, qu'une facture du même jour mentionnait la vente de vingt chauffe-eau solaires par la société Solairinox Services à la société en participation Cofina 01409 pour le même montant et qu'un contrat de location établi le même jour mentionnait la location de vingt chauffe-eau solaires par cette société en participation à la société Solairinox Services. En jugeant, sur la base de ces éléments, que la société Solairinox Services était à la fois le fournisseur du matériel et le locataire de celui-ci, la cour a porté sur les faits qui lui étaient soumis une appréciation souveraine exempte de dénaturation. <br/>
<br/>
              6. Il résulte de tout ce qui précède que M. A...n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
