<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028245482</ID>
<ANCIEN_ID>JG_L_2013_11_000000359573</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/24/54/CETATEXT000028245482.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  2ème sous-section jugeant seule, 27/11/2013, 359573, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-11-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359573</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 2ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Marc Perrin de Brichambaut</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:359573.20131127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 22 mai et 20 août 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A...B..., demeurant ...; M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 12PA00459 du 10 février 2012 par laquelle le président de la 2ème chambre de la cour administrative d'appel de Paris a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 1109715/3-1 du 27 décembre 2011 par lequel le tribunal administratif de Paris a rejeté sa demande tendant à l'annulation pour excès de pouvoir de l'arrêté du préfet de police du 3 mai 2011 lui refusant la délivrance d'un titre de séjour, l'obligeant à quitter le territoire français et fixant son pays de destination, d'autre part, à l'annulation pour excès de pouvoir de cet arrêté et, enfin, à ce qu'il soit enjoint au préfet de lui délivrer un titre de séjour dans un délai d'un mois à compter de la notification de l'arrêt à intervenir, sous astreinte de 150 euros par jour de retard et de lui délivrer, durant cet examen, une autorisation provisoire de séjour ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu l'accord franco-algérien du 27 décembre 1968 relatif à la circulation, à l'emploi et au séjour des ressortissants algériens et de leurs familles ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu la loi n° 2011-672 du 16 juin 2011 ;<br/>
<br/>
              Vu le décret n° 2011-819 du 8 juillet 2011 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Perrin de Brichambaut, Conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, Rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de M. B...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 3 mai 2011, le préfet de police a refusé de délivrer à M. B...un titre de séjour, l'a obligé à quitter le territoire français et a fixé le pays de destination ; que, par un jugement du 27 décembre 2011, le tribunal administratif de Paris a rejeté la demande de l'intéressé tendant à l'annulation pour excès de pouvoir de cet arrêté ; que, par une ordonnance du 10 février 2012, le président de la 2ème chambre de la cour administrative d'appel de Paris, statuant en application des articles R. 222-33 et R. 222-34 du code de justice administrative, a rejeté l'appel formé par M. B...contre ce jugement ;<br/>
<br/>
              Considérant, en premier lieu, que si les articles R. 222-33 et R. 222-34 du code de justice administrative ont été abrogés par le II de l'article 1er du décret du 8 juillet 2011 pris pour l'application de la loi du 16 juin 2011 relative à l'immigration, à l'intégration et à la nationalité, les dispositions de l'article 4 de ce décret ont expressément prévu que les dispositions du code de justice administrative abrogées par le décret demeureraient applicables à la contestation des décisions prises avant l'entrée en vigueur du décret ; que, par suite, en statuant en application des articles R. 222-33 et R. 222-34 du code de justice administrative sur le litige qui lui était soumis, relatif à un arrêté pris le 3 mai 2011, soit avant l'entrée en vigueur, le 18 juillet 2011, du décret du 8 juillet 2011, l'auteur de l'ordonnance attaquée n'a pas commis d'erreur de droit ; <br/>
<br/>
              Considérant, en second lieu, qu'en présence d'une formulation différente d'un moyen examiné par le tribunal administratif, le juge d'appel peut se prononcer sur ce moyen par adoption des motifs des premiers juges sans méconnaître le principe de motivation des jugements, rappelé à l'article L. 9 du code de justice administrative, dès lors que la réponse du tribunal à ce moyen était elle-même suffisante et n'appelait pas de nouvelles précisions en appel ; <br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis à la cour administrative d'appel de Paris que M. B...n'avait soulevé aucun moyen nouveau par rapport à ceux qu'il avait invoqués en première instance à l'encontre de l'arrêté du 3 mai 2011 attaqué ; qu'à l'appui du moyen tiré de la méconnaissance des stipulations du point 7 de l'article 6 de l'accord franco-algérien du 27 décembre 1968, l'intéressé s'est borné à expliciter les arguments qu'il avait déjà présentés en première instance et que les premiers juges avaient écartés par des motifs suffisants ; que, par suite, l'auteur de l'ordonnance attaquée, qui n'a pas méconnu la portée des écritures de l'appelant, a pu écarter l'argumentation dont il était saisi par adoption des motifs retenus par le tribunal administratif de Paris sans entacher sa décision d'une insuffisance de motivation ; <br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que M. B...n'est pas fondé à demander l'annulation de l'ordonnance attaquée, qui a été régulièrement prise en vertu des articles R. 222-33 et R. 222-34 du code de justice administrative applicables aux requêtes qui ne sont manifestement pas susceptibles d'entraîner l'infirmation de la décision attaquée ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de M. B...est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
