<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036945721</ID>
<ANCIEN_ID>JG_L_2018_05_000000401760</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/94/57/CETATEXT000036945721.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  8ème chambre jugeant seule, 25/05/2018, 401760, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-05-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401760</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 8ème chambre jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAMARRE, JEHANNIN ; SCP LEVIS</AVOCATS>
<RAPPORTEUR>Mme Emmanuelle Petitdemange</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:401760.20180525</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au juge des référés du tribunal administratif de Melun d'enjoindre, sur le fondement de l'article L. 521-3 du code de justice administrative, à la commune de Joinville-le-Pont et au centre communal d'action sociale de cette commune, de procéder à une évaluation du degré de contamination par l'amiante du logement qu'elle occupe dans la résidence Wilson, de lui en communiquer les résultats, de réaliser des travaux de réfection des sols et des murs, de suspendre le paiement de sa redevance d'occupation jusqu'à son retour dans les lieux après une nouvelle évaluation du degré de contamination à l'amiante, de prendre en charge les frais de déménagement et de garde de ses biens, de lui attribuer un logement temporaire à titre gracieux pendant la durée des travaux, ainsi que de condamner la commune et le centre communal d'action sociale à lui verser  une provision de 3 000 euros ou à titre subsidiaire de 1 500 euros au titre de la réparation du trouble de jouissance subi. <br/>
<br/>
              Par une ordonnance n° 1605348 du 21 juillet 2016, le juge des référés du tribunal administratif de Melun a enjoint au centre communal d'action sociale de faire réaliser les travaux d'enlèvement des dalles recouvrant le sol du logement de Mme B...et contenant de l'amiante par une entreprise agréée ou certifiée pour effectuer des travaux d'amiante et de remise en état du sol, dans un délai de trois mois à compter de la notification de son ordonnance, et à l'issue de ces travaux, de faire réaliser un diagnostic de l'air ambiant du logement. Il a également enjoint au centre communal d'action sociale de prendre toutes dispositions utiles pour reloger Mme B...le temps des travaux et assurer le déménagement ainsi que le retour de Mme B...dans le logement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 25 juillet et 2 août 2016 au secrétariat du contentieux du Conseil d'Etat, la commune de Joinville-le-Pont et le centre communal d'action sociale demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de MmeB... ;<br/>
<br/>
              3°) de mettre à la charge de Mme B... la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Emmanuelle Petitdemange, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delamarre, Jéhannin, avocat de la commune de Joinville-le-Pont et du centre communal d'action sociale de Joinville-le-Pont et à la SCP Lévis, avocat de Mme B...;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 7 mai 2018 présentée pour la commune de Joinville-le-Pont et le centre communal d'action sociale ; <br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que Mme B...a conclu, le 24 décembre 2013, un contrat de résident avec le centre communal d'action sociale de Joinville-le-Pont pour l'attribution d'un appartement dans la résidence pour personnes âgées dénommée " Résidence Wilson " située à Joinville-le-Pont. Après avoir fait constater, au mois de novembre 2014, que les dalles de sols, qui se décollaient dans plusieurs pièces de son logement, ainsi que l'enduit et la colle utilisés contenaient de l'amiante, Mme B... a sollicité la réalisation de travaux de retrait et de remplacement des éléments contenant de l'amiante. Le 22 juin 2016, Mme B...a demandé au juge des référés du tribunal administratif de Melun d'enjoindre, sur le fondement de l'article L. 521-3 du code de justice administrative, à la commune de Joinville-le-Pont et au centre communal d'action sociale de cette commune, de procéder à une évaluation du degré de contamination par l'amiante du logement qu'elle occupe, de lui en communiquer les résultats, de réaliser des travaux de réfection des sols et des murs, de suspendre le paiement de sa redevance d'occupation jusqu'à son retour dans les lieux après une nouvelle évaluation du degré de contamination à l'amiante, de prendre en charge les frais de déménagement et de garde de ses biens, de lui attribuer un logement temporaire à titre gracieux pendant la durée des travaux, ainsi que de condamner la commune et le centre communal d'action sociale à lui verser une provision de 3 000 euros ou à titre subsidiaire de 1 500 euros au titre de la réparation du trouble de jouissance subi. La commune de Joinville-le-Pont et le centre communal d'action sociale de Joinville-le-Pont se pourvoient en cassation contre l'ordonnance du 21 juillet 2016 par laquelle le juge des référés du tribunal administratif de Melun a enjoint au centre communal d'action sociale de faire réaliser les travaux d'enlèvement des dalles recouvrant le sol du logement de Mme B...et contenant de l'amiante par une entreprise agréée ou certifiée pour effectuer des travaux d'amiante et de remise en état du sol, dans un délai de trois mois à compter de la notification de son ordonnance, et à l'issue de ces travaux, de faire réaliser un diagnostic de l'air ambiant du logement, de prendre toutes dispositions utiles pour reloger Mme B...le temps des travaux et assurer le déménagement ainsi que le retour de l'intéressée dans le logement.<br/>
<br/>
              Sur la recevabilité du pourvoi en cassation :<br/>
<br/>
              2. Par l'ordonnance attaquée, le juge des référés du tribunal administratif de Melun a ordonné au seul centre communal d'action sociale de Joinville-le-Pont la réalisation de travaux et la prise en charge de la requérante pendant la durée de ceux-ci. La commune de Joinville-le-Pont, à l'encontre de laquelle aucune mesure n'a été prononcée, n'est ainsi pas recevable à se pourvoir en cassation contre cette ordonnance. Le pourvoi doit, par suite, être rejeté en tant qu'il est présenté par la commune de Joinville-le-Pont.<br/>
<br/>
              Sur le bien-fondé de l'ordonnance attaquée :<br/>
<br/>
              3. Aux termes de l'article L. 521-3 du code de justice administrative : " En cas d'urgence et sur simple requête qui sera recevable même en l'absence de décision administrative préalable, le juge des référés peut ordonner toutes autres mesures utiles sans faire obstacle à l'exécution d'aucune décision administrative ". <br/>
<br/>
              4. Saisi sur le fondement de ces dispositions d'une demande qui n'est pas manifestement insusceptible de se rattacher à un litige relevant de la compétence du juge administratif, le juge des référés peut prescrire, à des fins conservatoires ou à titre provisoire, toutes mesures que l'urgence justifie, notamment sous forme d'injonctions, à la condition que ces mesures soient utiles et ne se heurtent à aucune contestation sérieuse. <br/>
<br/>
              5. En premier lieu, les personnes qui sont hébergées dans un foyer-logement relevant d'une personne morale de droit public sont des usagers d'un service public administratif, à l'égard duquel elles se situent dans un rapport de droit public. Les litiges susceptibles de s'élever entre ces établissements et les personnes qui y résident relèvent, en conséquence, de la juridiction administrative. La demande de MmeB..., hébergée dans la " Résidence Wilson " à Joinville-le-Pont, qui est un foyer-logement géré par le centre communal d'action sociale de cette commune, n'est ainsi pas manifestement insusceptible de se rattacher à un litige relevant de la compétence du juge administratif. Ce motif, qui n'appelle l'appréciation d'aucune circonstance de fait, doit être substitué à celui retenu par l'ordonnance attaquée, dont il justifie légalement le dispositif.<br/>
<br/>
              6. En deuxième lieu, en relevant le danger que constituait la présence d'amiante pour la sécurité des occupants du logement formant la résidence de Mme B...et en estimant par suite que la condition d'urgence était satisfaite, malgré les circonstances que le centre communal d'action sociale de Joinville-le-Pont avait tenté de faire réaliser les travaux demandés et que Mme B...s'était opposée, compte tenu des modalités alors envisagées, à leur réalisation, le juge des référés s'est livré à une appréciation souveraine des faits de l'espèce qui n'est arguée ni de dénaturation, ni d'insuffisance de motivation et qui n'est pas entachée d'erreur de droit.<br/>
<br/>
              7. En troisième lieu, en se fondant, pour regarder comme utiles les mesures demandées, notamment l'injonction au centre communal d'action sociale de faire réaliser les travaux par une entreprise certifiée ou agréée et d'assurer le déménagement de Mme B..., que son état de santé ne lui permettait pas d'effectuer seule et auxquelles ce centre avait opposé un refus ou n'avait pas acquiescé, sur les circonstances que la présence d'amiante était établie dans le logement de Mme B...et constituait un danger pour la santé des occupants et en écartant comme sans incidence sur l'utilité de ces mesures le fait que le centre communal d'action sociale avait déjà tenté de faire réaliser l'enlèvement des dalles dès lors qu'il n'était pas établi que l'entreprise qui devait intervenir était agréée ou certifiée pour assurer ce type de travaux, le juge des référés, qui a porté sur l'ensemble des faits en cause une appréciation souveraine exempte de dénaturation, n'a pas commis d'erreur de droit.<br/>
<br/>
              8. En quatrième lieu, le juge des référés, dont l'ordonnance est suffisamment motivée sur ce point, a pu, sans commettre d'erreur de droit, juger que la circonstance qu'aucune disposition ne ferait poser une telle obligation sur le gestionnaire d'un logement-foyer était sans incidence sur le caractère d'utilité du relogement de Mme B...le temps des travaux d'enlèvement des dalles et de réfection des sols de son logement.<br/>
<br/>
              10. Enfin, en jugeant que ne constituait pas une contestation sérieuse, dès lors que les travaux en litige ne présentaient pas le caractère de grosses réparations, la circonstance que le centre communal d'action sociale de Joinville-le-Pont n'était que le gestionnaire de la résidence Wilson, propriété de l'office public d'habitat de Joinville-le-Pont, et en lui ordonnant par suite de les réaliser, le juge des référés n'a pas commis d'erreur de droit.<br/>
<br/>
              11. Il résulte de tout ce qui précède que le centre communal d'action sociale de Joinville-le-Pont n'est pas fondé à demander l'annulation de l'ordonnance qu'il attaque.<br/>
<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              12. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de Mme B...qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées sur le fondement des dispositions de l'article 37 de la loi du 10 juillet 1991 par la SCP Marc Lévis, avocat de MmeB....<br/>
<br/>
<br/>
<br/>                  D E C I D E :<br/>
                                 --------------<br/>
<br/>
Article 1er : Le pourvoi de la commune de Joinville-le-Pont et du centre communal d'action sociale de Joinville-le-Pont est rejeté.<br/>
<br/>
Article 2 : Les conclusions présentées sur le fondement des dispositions de l'article 37 de la loi du 10 juillet 1991 par la SCP Marc Lévis, avocat de MmeB..., sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la commune de Joinville-le-Pont, au centre communal d'action sociale de Joinville-le-Pont et à Mme A...B....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
