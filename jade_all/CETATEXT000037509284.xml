<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037509284</ID>
<ANCIEN_ID>JG_L_2018_10_000000414159</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/50/92/CETATEXT000037509284.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 19/10/2018, 414159, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-10-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414159</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD ; SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR>Mme Pauline Berne</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:414159.20181019</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Versailles de condamner la commune du Vésinet à lui payer, à titre principal, la somme de 32 000 euros, augmentée du montant des intérêts légaux, à titre subsidiaire, la somme de 22 800 euros, augmentée du montant des intérêts légaux et, dans tous les cas, la somme de 10 000 euros au titre du préjudice moral.<br/>
<br/>
              Par un jugement n° 1501345 du 14 avril 2017, le tribunal administratif de Versailles a rejeté cette demande.<br/>
<br/>
              Par une ordonnance n° 17VE01896 du 11 juillet 2017, le président de la 6ème chambre de la cour administrative d'appel de Versailles a rejeté l'appel formé par M. B...contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 11 septembre et 11 décembre 2017, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune du Vésinet la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
               - la Constitution, et notamment son Préambule ; <br/>
              - la convention de sauvegarde des droits de l'homme et des libertés fondamentales et son premier protocole additionnel;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Berne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de M. B...et à la SCP Gadiou, Chevallier, avocat de la Commune du Vésinet ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un jugement du 14 avril 2017, le tribunal administratif de Versailles a rejeté la demande de M. B... tendant à la condamnation de la commune du Vésinet à lui verser diverses sommes. Par une ordonnance du 11 juillet 2017, le président de la 6ème chambre de la cour administrative d'appel de Versailles a rejeté la requête de M. B...pour irrecevabilité manifeste en l'absence de régularisation de la présentation des pièces jointes au moyen de l'application informatique mentionnée à l'article R. 414-1 du code de justice administrative, dénommée Télérecours, au motif que les vingt-deux fichiers contenant une pièce chacune n'étaient ni numérotés ni nommés conformément à l'inventaire. Le requérant se pourvoit en cassation contre cette ordonnance.  <br/>
<br/>
              2. Aux termes de l'article R. 222-1 du code de justice administrative : " (...) les présidents de formation de jugement des tribunaux et des cours (...) peuvent, par ordonnance : / (...) 4° Rejeter les requêtes manifestement irrecevables, lorsque la juridiction n'est pas tenue d'inviter leur auteur à les régulariser ou qu'elles n'ont pas été régularisées à l'expiration du délai imparti par une demande en ce sens ; (...) Les présidents des formations de jugement des cours peuvent (...), par ordonnance, rejeter (...), les requêtes dirigées contre des ordonnances prises en application des 1° à 5° du présent article (...) ". Aux termes de l'article R. 612-1 du même code : " Lorsque des conclusions sont entachées d'une irrecevabilité susceptible d'être couverte après l'expiration du délai de recours, la juridiction ne peut les rejeter en relevant d'office cette irrecevabilité qu'après avoir invité leur auteur à les régulariser. / (...) / La demande de régularisation mentionne que, à défaut de régularisation, les conclusions pourront être rejetées comme irrecevables dès l'expiration du délai imparti qui, sauf urgence, ne peut être inférieur à quinze jours. La demande de régularisation tient lieu de l'information prévue à l'article R. 611-7 ".  <br/>
<br/>
              3. Aux termes de l'article R. 412-2 du code de justice administrative : " Lorsque les parties joignent des pièces à l'appui de leurs requêtes et mémoires, elles en établissent simultanément un inventaire détaillé (...) ". L'article R. 414-1 du même code dispose : " Lorsqu'elle est présentée par un avocat, un avocat au Conseil d'Etat et à la Cour de cassation, une personne morale de droit public autre qu'une commune de moins de 3 500 habitants ou un organisme de droit privé chargé de la gestion permanente d'un service public, la requête doit, à peine d'irrecevabilité, être adressée à la juridiction par voie électronique au moyen d'une application informatique dédiée accessible par le réseau internet. La même obligation est applicable aux autres mémoires du requérant (...) ". Aux termes des dispositions de l'article R. 414-3 du même code : " Par dérogation aux dispositions des articles R. 411-3, R. 411-4, R. 412-1 et R. 412-2, les requérants sont dispensés de produire des copies de leur requête et des pièces qui sont jointes à celle-ci et à leurs mémoires. / Les pièces jointes sont présentées conformément à l'inventaire qui en est dressé. / Lorsque le requérant transmet, à l'appui de sa requête, un fichier unique comprenant plusieurs pièces, chacune d'entre elles doit être répertoriée par un signet la désignant conformément à l'inventaire mentionné ci-dessus. S'il transmet un fichier par pièce, l'intitulé de chacun d'entre eux doit être conforme à cet inventaire. Le respect de ces obligations est prescrit à peine d'irrecevabilité de la requête. / Les mêmes obligations sont applicables aux autres mémoires du requérant, sous peine pour celui-ci, après invitation à régulariser non suivie d'effet, de voir ses écritures écartées des débats. / Si les caractéristiques de certaines pièces font obstacle à leur communication par voie électronique, ces pièces sont transmises sur support papier, dans les conditions prévues par l'article R. 412-2. L'inventaire des pièces transmis par voie électronique en fait mention ".<br/>
<br/>
              4. Il résulte des dispositions citées au point 3, organisant la transmission par voie électronique des pièces jointes à la requête à partir de leur inventaire détaillé, que cet inventaire doit s'entendre comme une présentation exhaustive des pièces par un intitulé comprenant, pour chacune d'elles, un numéro dans un ordre continu et croissant ainsi qu'un libellé suffisamment explicite.<br/>
<br/>
              5. Ces dispositions imposent également, dans l'intitulé du fichier qui ne comprend qu'une seule pièce, de désigner chaque pièce dans l'application Télérecours au moins par le numéro d'ordre qui lui est attribué par l'inventaire détaillé. Dès lors, la présentation des pièces jointes est conforme à leur inventaire détaillé lorsque l'intitulé de chaque fichier comprenant une seule pièce comporte au moins le même numéro d'ordre que celui affecté à la pièce par l'inventaire détaillé. En cas de non-respect de ces prescriptions, la requête est irrecevable si le requérant n'a pas donné suite à l'invitation à régulariser que la juridiction doit, en ce cas, lui adresser par un document indiquant précisément les modalités de régularisation de la requête.  <br/>
<br/>
              6. En premier lieu, il ressort des pièces du dossier soumis au juge du fond que l'avocat de M. B...a adressé à la cour administrative d'appel de Versailles, le 16 juin 2017, en utilisant l'application Télérecours, une requête accompagnée d'un inventaire détaillé mentionnant vingt-deux pièces ainsi que de trois fichiers globaux dans lesquels ces pièces étaient réparties sans y être répertoriées par des signets les indexant au sein du fichier global où elles étaient incluses. Le même jour, l'avocat du requérant a reçu une invitation à régulariser cette requête dans les quinze jours de la réception de cette invitation. En réponse, l'avocat de M. B... a transmis à la juridiction, par deux envois successifs du 20 juin 2017, les vingt-deux pièces de la requête en autant de fichiers, accompagnés d'un inventaire détaillé. Les intitulés de quinze des vingt-deux fichiers n'incluaient pas les numéros d'ordre affectés aux pièces par l'inventaire.  Dès lors, le président de la 6ème chambre de la cour administrative d'appel de Versailles n'a ni commis d'erreur de droit ni dénaturé les pièces qui lui étaient soumises en jugeant que la requête n'avait pas été régularisée par l'intermédiaire de l'application Télérecours dans le délai imparti. <br/>
<br/>
              6. En second lieu, il résulte des dispositions du 4° de l'article R. 222-1 du code de justice administrative, combinées à celles de l'article R. 612-1 du même code, qu'aucune ordonnance de rejet ne peut être prise sur leur fondement sans que le requérant n'ait été préalablement averti de la méconnaissance de la formalité prévue par l'article R. 414-3 du même code ainsi que du risque que soit prononcée une irrecevabilité manifeste et invité à régulariser sa requête.<br/>
<br/>
              7. D'une part, les règles de procédure applicables devant les juridictions administratives relèvent de la compétence du pouvoir réglementaire, dès lors qu'elles ne mettent en cause aucune des matières réservées au législateur par l'article 34 de la Constitution ou d'autres règles ou principes de valeur constitutionnelle. Or, les dispositions de l'article R. 222-1 du code de justice administrative, qui excluent qu'une requête soit déclarée irrecevable lorsqu'une régularisation préalable doit être demandée au requérant, ne portent pas atteinte au droit d'exercer un recours effectif devant une juridiction résultant des dispositions de l'article 16 de la Déclaration des droits de l'homme et du citoyen. Ainsi, le pouvoir réglementaire était compétent pour les adopter. D'autre part, ces dispositions ne méconnaissent ni les stipulations du paragraphe 1 de l'article 6 de la convention de sauvegarde des droits de l'homme et des libertés fondamentales ni celles de l'article 1er de son premier protocole additionnel qui garantissent le droit à un recours effectif et à un procès équitable et protègent les droits patrimoniaux. Par suite, le président de la 6ème chambre de la cour administrative d'appel de Versailles pouvait, sans commettre d'erreur de droit, faire application des dispositions du 4° de l'article R. 222-1 du code de justice administrative pour constater l'irrecevabilité manifeste d'une requête dont, après une invitation à régularisation, les pièces n'étaient toujours pas présentées conformément à leur inventaire.<br/>
<br/>
              8. Il résulte de ce qui précède que le pourvoi de M. B...doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et à la commune du Vésinet.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
