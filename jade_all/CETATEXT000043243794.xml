<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043243794</ID>
<ANCIEN_ID>JG_L_2021_03_000000436971</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/24/37/CETATEXT000043243794.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 11/03/2021, 436971, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436971</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Géraud Sajust de Bergues</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:436971.20210311</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au tribunal administratif d'Orléans d'annuler les décisions des 17 octobre 2017 et 27 février 2018 par lesquelles le maire du Malesherbois (Loiret) l'a informée que le compteur d'eau qu'elle avait fait poser pour son raccordement au réseau d'eau potable ne pouvait être pris en compte du fait de sa non-conformité au règlement du service d'eau. Par un jugement n° 1802041 du 16 avril 2019, le tribunal administratif d'Orléans a rejeté sa demande. Par une ordonnance n° 19NT02416 du 21 octobre 2019, le président de la 4ème chambre de la cour administrative d'appel de Nantes a rejeté l'appel formé par Mme A... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés le 20 décembre 2019 et le 27 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de la commune du Malesherbois une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Géraud Sajust de Bergues, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Zribi et Texier, avocat de Mme A... et à la SCP Waquet, Farge, Hazan, avocat de la commune de Malesherbois ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme B... A... a demandé au tribunal administratif d'Orléans d'annuler les décisions des 17 octobre 2017 et 27 février 2018 par lesquelles le maire du Malesherbois (Loiret) l'a informée que le compteur d'eau qu'elle avait fait poser pour son raccordement au réseau d'eau potable ne pouvait être pris en compte du fait de sa non-conformité au règlement du service d'eau. Par un jugement du 16 avril 2019, le tribunal administratif d'Orléans a rejeté sa demande. Mme A... se pourvoit en cassation contre l'ordonnance du 21 octobre 2019 par laquelle le président de la 4ème chambre de la cour administrative d'appel de Nantes a rejeté comme tardif l'appel qu'elle a formé contre ce jugement.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article R. 811-2 du code de justice administrative : " Sauf disposition contraire, le délai d'appel est de deux mois. Il court contre toute partie à l'instance à compter du jour où la notification a été faite à cette partie dans les conditions prévues aux articles R. 753 à R. 751-4-1 (...) ". En vertu des dispositions de l'article R. 751-3 du même code, sauf disposition contraire, les décisions sont notifiées le même jour à toutes les parties en cause par lettre recommandée avec demande d'avis de réception.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que l'avis de réception du pli contenant le jugement du 16 avril 2019 du tribunal administratif d'Orléans indique qu'il a été présenté à l'adresse de Mme A... le 19 avril 2019 mais n'indique ni date de distribution, ni date de réexpédition. Par suite, le président de la 4ème chambre de la cour administrative d'appel de Nantes a dénaturé les pièces du dossier en jugeant que Mme A... avait reçu notification du jugement attaqué du tribunal administratif d'Orléans le 19 avril 2019. L'ordonnance attaquée doit, par suite, être annulée.<br/>
<br/>
              4.  Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de Mme A..., qui n'est pas, dans la présente instance, la partie perdante. Dans les circonstances de l'espèce, il y a lieu de mettre à la charge de la commune du Malesherbois la somme de 2 000 euros à verser à Mme A... au même titre.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'ordonnance du 21 octobre 2019 du président de la 4ème chambre de la cour administrative d'appel de Nantes est annulée.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
Article 3 : La commune du Malesherbois versera à Mme A... la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Les conclusions de la commune du Malesherbois présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme B... A... et à la commune du Malesherbois.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
