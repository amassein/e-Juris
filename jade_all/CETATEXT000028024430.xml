<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028024430</ID>
<ANCIEN_ID>JG_L_2013_10_000000358789</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/02/44/CETATEXT000028024430.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  5ème sous-section jugeant seule, 02/10/2013, 358789, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-10-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>358789</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 5ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Dominique Chelle </RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:358789.20131002</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée le 20 mars 2011 au secrétariat du contentieux du Conseil d'Etat, présentée pour la SARL Radio Impact FM, dont le siège est 14 rue Pailleron à Lyon (69004), représentée par son gérant en exercice ; la SARL Radio Impact FM demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 27 septembre 2011, confirmée sur recours gracieux le 14 février 2012, par laquelle le Conseil supérieur de l'audiovisuel (CSA) a rejeté sa demande d'autorisation d'exploiter par voie hertzienne terrestre le service radiophonique Radio Impact FM dans la zone de Valence ; <br/>
<br/>
              2°) de mettre à la charge du Conseil supérieur de l'audiovisuel le versement d'une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 86-1067 du 30 septembre 1986 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dominique Chelle, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que, par décision n° 2010-244 du 13 avril 2010, le Conseil supérieur de l'audiovisuel a lancé un appel à candidatures en vue de l'attribution de fréquences radiophoniques dans le ressort du comité territorial de l'audiovisuel de Lyon ; que le conseil supérieur a délibéré le 23 avril 2010 de l'attribution des fréquences disponibles dans la zone de Valence ; que la société Radio Impact FM demande l'annulation pour excès de pouvoir de la décision du Conseil supérieur de l'audiovisuel du 27 septembre 2011 rejetant sa candidature dans la zone de Valence ;<br/>
<br/>
              Sur la légalité externe : <br/>
<br/>
              2. Considérant qu'aux termes du second alinéa de l'article 32 de la loi du 30 septembre 1986 relative à la liberté de communication : " Les refus d'autorisation sont motivés (...). Lorsqu'ils s'appliquent à un service de radio diffusé par voie hertzienne terrestre, ils peuvent être motivés par référence à un rapport de synthèse explicitant les choix du Conseil au regard des critères mentionnés aux articles 1er à 29 " ; que la décision attaquée, motivée par un rapport de synthèse commun à plusieurs candidatures comme le permettent ces dispositions, permet d'identifier ceux des critères énumérés aux articles 1er et 29 de la loi du 30 septembre 1986 sur lesquels le Conseil supérieur de l'audiovisuel s'est fondé pour refuser l'autorisation demandée par la société Radio Impact FM et précise les éléments de fait qu'il a retenus pour rejeter la candidature de celle-ci ; qu'ainsi, la décision attaquée satisfait à l'obligation faite au Conseil supérieur de l'audiovisuel par l'article 32 de la loi précitée de motiver les refus d'autorisation ;<br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              3. Considérant, d'une part, qu'aux termes de l'article 29 de la loi du 30 septembre 1986 : " Sous réserve des dispositions de l'article 26 de la présente loi, l'usage des fréquences pour la diffusion de services de radio par voie hertzienne terrestre est autorisé par le Conseil supérieur de l'audiovisuel dans les conditions prévues au présent article. (...) Le conseil accorde les autorisations en appréciant l'intérêt de chaque projet pour le public, au regard des impératifs prioritaires que sont la sauvegarde du pluralisme des courants d'expression socioculturels, la diversification des opérateurs, et la nécessité d'éviter les abus de position dominante ainsi que les pratiques entravant le libre exercice de la concurrence. Il tient également compte : 1° De l'expérience acquise par le candidat dans les activités de communication ; / 2° Du financement et des perspectives d'exploitation du service notamment en fonction des possibilités de partage des ressources publicitaires entre les entreprises de presse écrite et les services de communication audiovisuelle ; (...) Le Conseil veille également au juste équilibre entre les réseaux nationaux de radiodiffusion, d'une part, et les services locaux, régionaux et thématiques indépendants, d'autre part. Il s'assure que le public bénéficie de services dont les programmes contribuent à l'information politique et générale. (...) " ; <br/>
<br/>
              4. Considérant, d'autre part, que par ses communiqués n° 34 du 29 août 1989 et n° 281 du 10 novembre 1994, le Conseil supérieur de l'audiovisuel, faisant usage de la compétence que lui confère le deuxième alinéa de l'article 29 de la loi du 30 septembre 1986 de déterminer des catégories de service en vue de l'appel à candidature pour l'exploitation de services de radiodiffusion sonore par voie hertzienne terrestre, a déterminé cinq catégories ainsi définies : services associatifs éligibles au fond de soutien, mentionnés à l'article 80 (catégorie A), services locaux ou régionaux indépendants ne diffusant pas de programme national identifié (catégorie B), services locaux ou régionaux diffusant le programme d'un réseau thématique à vocation nationale (catégorie C), services thématiques à vocation nationale (D), et les services généralistes à vocation nationale (catégorie E) ;<br/>
<br/>
              5. Considérant qu'avant l'appel aux candidatures litigieux, étaient autorisés dans la zone de Valence les services MTI en catégorie B, Beur FM et Rire et Chansons en catégorie D ; que le Conseil supérieur de l'audiovisuel a attribué les seize fréquences disponibles à Radio A, Radio Bourg-lès-Valence, Radio Méga et RCF 26 en catégorie A, à NRJ Vallée du Rhône, Nostalgie Vallée du Rhône et Virgin Radio Valence en catégorie C, à Fun Radio, Radio Classique, RFM, Skyrock et TSF Jazz en catégorie D, à Europe 1, RMC, RTL et Sud Radio en catégorie E ; que si la société requérante fait valoir que le programme du service Radio Impact FM qu'elle exploite, qui s'adresse à un public adulte et senior, était  inédit dans la zone, il ressort des pièces du dossier que son projet ne comportait pas la diffusion de programmes spécifiques à Valence, alors que les services Radio Bourg-lès-Valence et Radio Méga, autorisés en catégorie A, ainsi que le service Nostalgie, autorisé en catégorie C, dont les programmes musicaux s'adressent à des publics recoupant partiellement celui auquel est destiné le service Radio Impact FM, comportent par ailleurs des rubriques et informations locales ; qu'il ne ressort pas des pièces du dossier que les choix opérés par le Conseil supérieur de l'audiovisuel résultent d'une erreur d'appréciation au regard des critères prévus à l'article 29 de la loi du 30 septembre 1986, notamment les impératifs prioritaires de sauvegarde du pluralisme des courants d'expression socioculturels et de diversification des opérateurs ainsi que la nécessité d'éviter les abus de position dominante et les pratiques entravant le libre exercice de la concurrence et le juste équilibre entre les réseaux nationaux de radiodiffusion et les services locaux, régionaux et thématiques indépendants ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la SARL Radio Impact FM n'est pas fondée à demander l'annulation de la décision attaquée ; que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne sauraient, par suite, être accueillies ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : La requête de la société Radio Impact FM est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société Radio Impact FM et au Conseil supérieur de l'audiovisuel. <br/>
Copie pour information en sera adressée au ministre de la culture et de la communication.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
