<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030286050</ID>
<ANCIEN_ID>JG_L_2015_02_000000364581</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/28/60/CETATEXT000030286050.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  8ème sous-section jugeant seule, 25/02/2015, 364581, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-02-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364581</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 8ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Esther de Moustier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:364581.20150225</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Après renvoi de l'affaire par le Conseil d'Etat à la suite de l'annulation d'un précédent jugement du tribunal administratif de Montpellier du 18 novembre 2009, la SCI Entrepôts Chaîne du Soleil a demandé à ce même tribunal de prononcer la décharge de la cotisation supplémentaire de taxe foncière sur les propriétés bâties à laquelle elle a été assujettie au titre de l'année 2006 à raison d'un immeuble dont elle est propriétaire à Mèze (Hérault) ;<br/>
<br/>
              Par un jugement n° 1103658 du 18 octobre 2012, le tribunal administratif de Montpellier a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 17 décembre 2012, 15 mars 2013 et 13 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, la SCI Entrepôts Chaîne du Soleil demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement du 18 octobre 2012 du tribunal administratif de Montpellier ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative et la somme de 35 euros au titre de la contribution à l'aide juridique prévue à l'article R. 761-1 du même code. <br/>
<br/>
              la SCI Entrepôts Chaîne du Soleil soutient que :<br/>
              - en se bornant à faire l'inventaire des matériels et outillages appartenant à la SA Bessière pour rejeter sa demande, alors que l'entrepôt en litige était donné nu en location à cette société, et en s'abstenant de caractériser l'importance des matériels et outillages effectivement mis en oeuvre pour cette activité de location, le tribunal administratif a entaché son jugement d'un défaut et d'une contradiction de motifs, a omis de répondre au moyen démontrant l'inapplicabilité de l'article 1499 du code général des impôts pour la détermination de la valeur locative de l'immeuble en litige, a insuffisamment motivé son jugement, n'a pas mis le juge de cassation en mesure d'exercer son contrôle et a commis une erreur de droit ;<br/>
              - en jugeant qu'elle présentait le caractère d'une entreprise industrielle ou commerciale au sens de l'article 1500 du code général des impôts, alors qu'elle avait fourni les éléments démontrant que seuls les terrains et bâtiments loués figuraient à son bilan, à l'exclusion des moyens techniques utilisés par son locataire, et établi ainsi que son activité se bornait à la location de locaux nus et présentait donc un caractère exclusivement civil, le tribunal administratif a dénaturé les faits de l'espèce, commis une erreur de qualification juridique et une erreur de droit, insuffisamment motivé son jugement et omis de répondre au moyen qu'elle soulevait ;<br/>
              - le mémoire en défense de l'administration est irrecevable faute d'habilitation et de signature de son auteur.<br/>
<br/>
              Par un mémoire en défense, enregistré le 16 septembre 2014, le ministre des finances et des comptes publics conclut au rejet du pourvoi. Il soutient que les moyens soulevés par la société ne sont pas fondés.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Esther de Moustier, auditeur,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la SCI Entrepôts Chaîne du Soleil.<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SCI Entrepôts Chaîne du soleil a donné à bail un terrain et des locaux à la SA Bessière qui y exerce une activité d'éleveur-embouteilleur et de négociant en vins ; qu'à la suite d'une vérification de comptabilité de la société Bessière, l'administration fiscale a estimé que la valeur locative cadastrale du terrain et des bâtiments appartenant à la SCI Entrepôts Chaîne du soleil devait être déterminée selon la méthode prévue à l'article 1499 du code général des impôts et non dans les conditions prévues à l'article 1498 du même code ; que la société a demandé au tribunal administratif de Montpellier la décharge de la cotisation supplémentaire de taxe foncière sur les propriétés bâties mise en conséquence à sa charge au titre de l'année 2006 ; qu'elle se pourvoit en cassation contre le jugement du 18 octobre 2012 par lequel le tribunal administratif, statuant sur renvoi de l'affaire après l'annulation d'un premier jugement du 18 novembre 2009 par décision du Conseil d'Etat statuant au contentieux du 26 juillet 2011, a rejeté sa demande ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 1499 du code général des impôts : " La valeur locative des immobilisations industrielles passibles de la taxe foncière sur les propriétés bâties est déterminée en appliquant au prix de revient de leurs différents éléments, revalorisé à l'aide des coefficients qui avaient été prévus pour la révision des bilans, des taux d'intérêt fixés par décret en Conseil d'Etat. (...) " ; qu'aux termes de l'article 1500 du même code, dans sa rédaction applicable au litige : " Par dérogation à l'article 1499, les bâtiments et terrains industriels qui ne figurent pas à l'actif d'une entreprise industrielle ou commerciale astreinte aux obligations définies à l'article 53 A, sont évalués dans les conditions prévues à l'article 1498 " ; qu'il résulte des dispositions précitées de l'article 1500 du code général des impôts que la dérogation qu'elles prévoient aux règles d'évaluation de droit commun des bâtiments et terrains industriels s'applique lorsque le propriétaire des biens à évaluer n'est pas une entreprise industrielle ou commerciale, ou qu'il n'est pas astreint aux obligations définies à l'article 53 A du même code, ou enfin qu'il n'a pas inscrit ces biens à l'actif de son bilan ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'ainsi que le précise le bail commercial du 1er avril 1997 conclu par la SCI Entrepôts Chaîne du soleil avec la société Bessière, modifié par avenant du 1er avril 1999, la location consentie à cette dernière société a porté, depuis la date du bail initial du 14 septembre 1987, sur un entrepôt couvert de 2 900 m² et un terrain de 14 997 m² ; qu'il ne ressort d'aucune des clauses des contrats produits devant les juges du fond que cet entrepôt était doté d'installations ou d'équipements techniques correspondant à l'activité viticole du preneur ; que, si le bailleur, par l'avenant de 1999, a mis à la disposition de la société Bessière des locaux supplémentaires qu'il a fait édifier, désignés comme un bâtiment climatisé destiné à un usage de " cellier de vieillissement " d'une superficie de 1 950 m² et un " hall de départ " réalisé en bardage de 450 m², et a ajusté en conséquence le montant du loyer,  il ne ressort pas des pièces du dossier soumis aux juges du fond que ces locaux aient été loués munis des moyens techniques nécessaires à leur exploitation ; que, d'ailleurs, à la différence des bâtiments eux-mêmes, ces moyens figurent à l'actif de la société Bessière et non à celui de la SCI Entrepôts Chaîne du soleil ; qu'ainsi, cette dernière est fondée à soutenir que le tribunal administratif de Montpellier a dénaturé les pièces du dossier en déduisant de ces contrats successifs et des augmentations de loyer mises à la charge du preneur que ces contrats avaient pour objet d'offrir à la location non seulement les locaux mais également d'importantes installations et moyens techniques dont la propriété restait au bailleur ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son jugement doit être annulé ; <br/>
<br/>
              4. Considérant que l'affaire faisant l'objet d'un second recours en cassation, il y a lieu de la régler au fond en application des dispositions du deuxième alinéa de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              5. Considérant, ainsi qu'il vient d'être dit, qu'il ne peut être déduit ni des clauses des contrats de bail, ni d'aucun autre élément du dossier que la SCI Entrepôts Chaîne du soleil aurait donné en location à la société Bessière des locaux munis des équipements et du matériel nécessaires à leur exploitation ; qu'il se déduit de ce qui précède que la SCI Entrepôts Chaîne du soleil exerce une activité de location de locaux nus de nature civile et ne constitue donc pas une entreprise industrielle ou commerciale au sens des dispositions précitées de l'article 1500 du code général des impôts, l'option qu'elle a exercée pour l'assujettissement à l'impôt sur les sociétés étant à cet égard sans incidence ; que, dès lors, la société entre dans les prévisions de la dérogation aux règles d'évaluation de droit commun des bâtiments et terrains industriels prévue par ces dispositions ; qu'elle est, par suite, fondée à demander la décharge de la cotisation supplémentaire de taxe foncière sur les propriétés bâties à laquelle elle a été assujettie au titre de l'année 2006 ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCI Entrepôts Chaîne du soleil au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>                  D E C I D E :<br/>
                                --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Montpellier du 18 octobre 2012 est annulé.<br/>
<br/>
Article 2 : La SCI Entrepôts Chaîne du soleil est déchargée de la cotisation supplémentaire de taxe foncière sur les propriétés bâties à laquelle elle a été assujettie au titre de l'année 2006.<br/>
<br/>
Article 3 : L'Etat versera à la SCI Entrepôts Chaîne du soleil la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la SCI Entrepôts Chaîne du soleil et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
