<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044205261</ID>
<ANCIEN_ID>JG_L_2021_10_000000427355</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/20/52/CETATEXT000044205261.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 13/10/2021, 427355, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427355</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI, REBEYROL ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Dorothée Pradines</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:427355.20211013</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le conseil départemental du Rhône de l'ordre des chirurgiens-dentistes a porté plainte contre M. B... A... devant la chambre disciplinaire de première instance de Rhône-Alpes de l'ordre des chirurgiens-dentistes. Par une décision du 2 mai 2017, la chambre disciplinaire de première instance a prononcé à son encontre la sanction de l'interdiction d'exercer la profession de chirurgien-dentiste pendant une durée de deux mois assortie du sursis.<br/>
<br/>
              Par une décision du 6 décembre 2018, la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes a rejeté l'appel de M. A... formé contre cette décision. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 25 janvier, 24 avril et 26 novembre 2019 au secrétariat du contentieux du Conseil d'État, M. A... demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler la décision attaquée ;<br/>
<br/>
              2°) réglant l'affaire au fond, faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge du conseil départemental du Rhône de l'ordre des chirurgiens-dentistes la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la directive 2000/31/CE du Parlement européen et du Conseil, du 8 juin 2000, relative à certains aspects juridiques des services de la société de l'information, et notamment du commerce électronique, dans le marché intérieur (" directive sur le commerce électronique ") ; <br/>
               - l'arrêt n° C-339/15 du 4 mai 2017 de la Cour de justice de l'Union européenne ;<br/>
              - l'ordonnance n° C-296/18 du 23 octobre 2018 de la Cour de justice de l'Union européenne ;<br/>
              - le code de la santé publique ; <br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dorothée Pradines, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Gatineau, Fattaccini, Rebeyrol, avocat de M.  A... et à la SCP Lyon-Caen, Thiriez, avocat du conseil départemental du Rhône de l'ordre des chirurgiens-dentistes ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, sur la plainte du conseil départemental du Rhône de l'ordre des chirurgiens-dentistes, la chambre disciplinaire de première instance de la région Rhône-Alpes de l'ordre des chirurgiens-dentistes a, par une décision du 2 mai 2017, infligé à M. A..., chirurgien-dentiste, la sanction de l'interdiction d'exercer sa profession pendant deux mois avec sursis. Par une décision du 6 décembre 2018, contre laquelle M. A... se pourvoit en cassation, la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes a rejeté l'appel qu'il avait formé contre la décision de la chambre disciplinaire de première instance. <br/>
<br/>
              2. Aux termes de l'article R. 4127-215 du code de la santé publique, dans sa rédaction applicable à la date des faits en cause : " La profession dentaire ne doit pas être pratiquée comme un commerce. / Sont notamment interdits : / 3° Tous procédés directs ou indirects de publicité ; (...) ". Aux termes de la deuxième phrase du premier alinéa de l'article R. 4127-225 du même code, dans sa rédaction applicable à la date des faits en cause : " Sont également interdites toute publicité, toute réclame personnelle ou intéressant un tiers ou une firme quelconque ". <br/>
<br/>
              3. Il résulte des stipulations de l'article 56 du traité sur le fonctionnement de l'Union européenne, telles qu'interprétées par la Cour de justice de l'Union européenne dans son arrêt rendu le 4 mai 2017 dans l'affaire C-339/15, ainsi que des dispositions de l'article 8 paragraphe 1 de la directive 2000/31/CE du Parlement européen et du Conseil, du 8 juin 2000, relative à certains aspects juridiques des services de la société de l'information, et notamment du commerce électronique, dans le marché intérieur (" directive sur le commerce électronique "), telles qu'interprétées par la Cour de justice de l'Union européenne dans son ordonnance rendue le 23 octobre 2018 dans l'affaire C-296/18, qu'elles s'opposent à des dispositions réglementaires qui interdisent de manière générale et absolue toute publicité et toute communication commerciale par voie électronique, telles que celles qui figurent au 3° de l'article R. 4127-215 du code de la santé publique. Par suite, en jugeant que les dispositions du 3° de l'article R. 4127-215 du code de la santé publique et de la deuxième phrase du premier alinéa de l'article R. 4127-225 du même code n'étaient pas incompatibles avec le droit de l'Union européenne et en retenant que M. A... avait commis un manquement en les méconnaissant, la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes a entaché sa décision d'erreur de droit.<br/>
<br/>
              4. Il résulte de tout ce qui précède que, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, M. A... est fondé à demander l'annulation de la décision attaquée.<br/>
<br/>
              5. Dans les circonstances de l'espèce, il n'y a pas lieu de faire droit à ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise à la charge de M. A... qui, dans la présente instance, n'est pas la partie perdante.<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du 6 décembre 2018 de la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes est annulée.<br/>
Article 2 : L'affaire est renvoyée à la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes.<br/>
Article 3 : Les conclusions présentées par M. A... et par le conseil départemental du Rhône de l'ordre des chirurgiens-dentistes au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à M. B... A... et au conseil départemental du Rhône de l'ordre des chirurgiens-dentistes.<br/>
Copie sera adressée au Conseil national de l'ordre des chirurgiens-dentistes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
