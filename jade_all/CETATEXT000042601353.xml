<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042601353</ID>
<ANCIEN_ID>JG_L_2020_11_000000439287</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/60/13/CETATEXT000042601353.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 30/11/2020, 439287, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439287</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:439287.20201130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 4 mars 2020 au secrétariat du contentieux du Conseil d'Etat, M. et Mme A... demandent au Conseil d'Etat d'annuler pour excès de pouvoir les paragraphes 190 et 210 des commentaires administratifs publiés le 3 juin 2015 au Bulletin officiel des finances publiques (BOFiP) - Impôts sous la référence BOI-RPPM-PVBMI-20-30-10.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 90/434/CE du Conseil du 23 juillet 1990 ; <br/>
              - la directive 2009/133/CE du Conseil du 19 octobre 2009 ;<br/>
              - le règlement (UE) n° 651/2014 de la Commission du 17 juin 2014 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. L'article 150-0 D du code général des impôts soumet les plus-values de cession de valeurs mobilières et de droits sociaux réalisées à compter du 1er janvier 2013, auparavant imposées à un taux forfaitaire, à une imposition au barème de l'impôt sur le revenu, tout en prévoyant un dispositif d'abattement, de droit commun ou renforcé dans certaines situations, sur le montant des gains nets résultant de ces cessions, variable selon la durée de détention de ces valeurs. Le 1 quater de cet article institue un abattement renforcé de 85 % lorsque, notamment, la société émettrice des droits cédés répond à la définition des micro, petites et moyennes entreprises donnée à l'annexe I au règlement (UE) n° 651/2014 de la Commission du 17 juin 2014 déclarant certaines catégories d'aides compatibles avec le marché intérieur. Le b) du 2° du B du 1 quater de l'article 150-0 D prévoit que le respect de cette condition est apprécié à la clôture du dernier exercice précédant la date de souscription ou d'acquisition de ces droits ou, à défaut d'exercice clos, à la date du premier exercice clos suivant la date de souscription ou d'acquisition de ces droits. L'avant dernier alinéa de ce même 2° précise que lorsque la société émettrice des droits cédés est une société holding animatrice de groupe, le respect des conditions mentionnées par ce 2°, notamment celle tenant à ce que la société émettrice des droits cédés réponde à la définition des micro, petites et moyennes entreprises, s'apprécie au niveau de la société émettrice et de chacune des sociétés dans lesquelles elle détient des participations.<br/>
<br/>
              2. M. et Mme A... demandent l'annulation pour excès de pouvoir des paragraphes 190 et 210 des commentaires administratifs publiés le 3 juin 2015 au Bulletin officiel des finances publiques (BOFiP) - Impôts sous la référence BOI-RPPM-PVBMI-20-30-10, par lesquels l'administration fiscale a fait connaître son interprétation des dispositions du 1 quater de l'article 150-0 D du code général des impôts. <br/>
<br/>
              Sur la recevabilité des conclusions de la requête :<br/>
<br/>
              3. L'intérêt dont se prévalent les requérants au soutien de leur demande tient à l'application qui leur a été faite des dispositions du 1 quater de l'article 150-0 D du code général des impôts à l'occasion d'une opération d'échange de titres suivie d'une cession des titres remis à l'échange entre des sociétés toutes deux établies en France. Cet intérêt ne leur donne qualité pour demander l'annulation des commentaires en litige qu'en tant qu'ils concernent les situations internes. En revanche, il ne leur donne pas qualité pour demander l'annulation de ces commentaires en tant qu'ils s'appliquent à des plus-values résultant d'opérations intéressant des sociétés d'Etats membres différents. Il en résulte que la requête n'est pas recevable en tant qu'elle est dirigée contre les commentaires attaqués dans la mesure où ceux-ci donnent une interprétation de la loi fiscale pour son application aux situations entrant dans le champ de la directive 90/434/CE du Conseil du 23 juillet 1990 concernant le régime fiscal commun applicable aux fusions, scissions, apports d'actifs et échanges d'actions des sociétés d'Etats membres différents, dont les dispositions ont été reprises par la directive 2009/133/CE du Conseil du 19 octobre 2009.  <br/>
<br/>
              Sur les moyens de la requête :<br/>
<br/>
              4. Le paragraphe 190 des commentaires administratifs contestés prévoit que " en cas de cession de titres ou droits d'une société holding animatrice de son groupe, tant cette société holding que chacune des sociétés dans lesquelles elle détient des participations doivent respecter toutes les conditions d'application de l'abattement pour durée de détention renforcé (...) / Il est précisé que les conditions tenant à l'âge de la société ainsi qu'au caractère de PME au sens du droit de l'Union européenne (...) des sociétés dans lesquelles la holding détient des participations sont appréciées : - à la date d'acquisition par le contribuable des titres ou droits de la holding quand celle-ci détient, à cette date, des participations dans la société considérée ; - à la date à laquelle la holding acquiert les titres de la société considérée quand cette acquisition intervient postérieurement à l'acquisition par le contribuable des titres ou droits de la holding (...) ". Le paragraphe 210 des mêmes commentaires prévoient que " Lorsque les titres ou droits cédés ont été reçus lors d'une opération d'échange qui a bénéficié du sursis d'imposition prévu à l'article 150-0 B du CGI, les conditions mentionnées du II-A au II-G § 30 à 200 sont appréciées au niveau de la société dont les titres ou droits ont été reçus lors de l'échange et qui font l'objet de la cession ". <br/>
<br/>
              5. En premier lieu, les dispositions du B du 1 quater de l'article 150-0 D du code général des impôts prévoient, ainsi qu'il a été dit au point 1, d'une part, que lorsque la société émettrice des droits cédés est une société holding animatrice de groupe, la condition, à laquelle est notamment subordonné le bénéfice de l'abattement renforcé, tenant à ce que la société en cause réponde à la définition des micro, petites et moyennes entreprises, s'applique à la fois à la société holding émettrice et à chacune des sociétés dans laquelle elle détient des participations et, d'autre part, que le respect de cette condition s'apprécie à la date de clôture du dernier exercice précédant la date de souscription ou d'acquisition des droits cédés ou, à défaut d'exercice clos, à la date du premier exercice clos suivant la date de souscription ou d'acquisition de ces droits. <br/>
<br/>
              6. Il en résulte que lorsque les droits cédés sont des parts d'une société holding animatrice de groupe, le respect de la condition tenant à ce que les sociétés dans lesquelles cette société holding détient des participations répondent à la définition des micro, petites et moyennes entreprises s'apprécie à la date d'acquisition par le contribuable des titres ou droits de la holding quand celle-ci détient, à cette date, des participations dans la société considérée ou à la date à laquelle la holding acquiert les titres de la société considérée quand cette acquisition intervient postérieurement à l'acquisition par le contribuable des titres ou droits de la holding. Il en va ainsi notamment lorsque les titres de la société holding ont été remis au contribuable en rémunération d'un apport de titres d'une autre société. Est à cet égard dépourvue d'incidence la circonstance que, dans cette hypothèse, l'opération d'échange n'ait, en vertu des dispositions de l'article 150-0 B du code général des impôts, donné lieu à aucune taxation immédiate, celle-ci n'intervenant, en application du 9 de l'article 150-0 D du même code, que lors de la vente ultérieure des titres de la société holding reçus en échange, le gain net étant alors calculé à partir du prix d'acquisition des titres remis à l'échange. Est également sans incidence sur la portée des dispositions en cause la circonstance que le 1 quinquies du même article, relatif à la durée de détention des actions, parts, droits ou titres cédés, précise qu'en cas de vente ultérieure d'actions, parts, droits ou titres reçus à l'occasion d'opérations mentionnées à l'article 150-0 B, la durée de détention est décomptée à partir de la date de souscription ou d'acquisition des actions, parts, droits ou titres remis à l'échange.<br/>
<br/>
              7. Compte tenu de la lettre claire de la loi rappelée au point 1, M. et Mme A... ne peuvent utilement soutenir que celle-ci devrait recevoir une interprétation différente, conforme à celle qui permettait d'assurer, dans le champ de cette directive, sa compatibilité avec les objectifs de l'article 8, paragraphe 6, de la directive 90/434/CE du Conseil du 23 juillet 1990, dont les dispositions ont été reprises à l'article 8 de la directive 2009/133/CE du Conseil du 19 octobre 2009, en vue de prévenir l'existence d'une différence illégale de traitement à l'égard des situations internes.<br/>
<br/>
              8.  Ainsi, en indiquant que le respect de la condition tenant au caractère de micro, petite ou moyenne entreprise, au sens du droit de l'Union européenne, des sociétés dans lesquelles la holding détient des participations s'apprécie à la date d'acquisition par le contribuable des titres ou droits de la holding quand celle-ci détient, à cette date, des participations dans la société considérée ou à la date à laquelle la holding acquiert les titres de la société considérée quand cette acquisition intervient postérieurement à l'acquisition par le contribuable des titres ou droits de la holding, les commentaires attaqués ne donnent pas des dispositions qu'ils ont pour objet d'éclairer une interprétation qui en méconnaît la portée. <br/>
<br/>
              9. En deuxième lieu, M. et Mme A... ne peuvent utilement se prévaloir, à l'appui des conclusions pour lesquelles ils sont recevables à agir telles que définies au point 3, de ce que les dispositions de la loi, telles que réitérées par les énonciations attaquées, seraient incompatibles avec les objectifs de l'article 8 paragraphe 6 de la directive 90/434/CE du Conseil du 23 juillet 1990, dont les dispositions ont été reprises à l'article 8 de la directive 2009/133/CE du Conseil du 19 octobre 2009.<br/>
<br/>
              10. En troisième et dernier lieu, le moyen invoqué à titre subsidiaire, tiré de ce qu'à supposer les énonciations attaquées conformes à la loi, les dispositions législatives commentées seraient contraires à la Constitution, ne peut être utilement soulevé qu'à l'appui d'une question prioritaire de constitutionnalité présentée dans les formes prescrites par l'article 23-5 de l'ordonnance du 7 novembre 1958 et l'article R. 771-13 du code de justice administrative. Faute d'être soulevé à l'appui d'une telle question présentée par mémoire distinct, ce moyen est irrecevable. <br/>
<br/>
              11. Il résulte de ce qui précède que la requête de M. et Mme A... doit être rejetée, y compris leurs conclusions présentées sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. et Mme A... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. et Mme B... A... et au ministre de l'économie, des finances et de la relance.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
