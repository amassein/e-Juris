<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033314162</ID>
<ANCIEN_ID>JG_L_2016_10_000000394226</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/31/41/CETATEXT000033314162.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème chambres réunies, 27/10/2016, 394226, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-10-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394226</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Simon Chassard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:394226.20161027</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 26 octobre 2015 et 11 février 2016 au secrétariat du contentieux du Conseil d'Etat, la Fédération nationale indépendante des mutuelles demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2015-513 du 7 mai 2015 pris pour l'application de l'ordonnance n° 2012-378 du 2 avril 2015 transposant la directive 2009/138/CE du Parlement européen et du Conseil sur l'accès aux activités de l'assurance et de la réassurance et leur exercice (solvabilité II), ainsi que les décisions implicites du ministre des finances et des comptes publics et du ministre des affaires sociales, de la santé et des droits des femmes refusant de faire droit à sa demande d'abrogation de ce décret ;<br/>
<br/>
              2°) à titre subsidiaire, de renvoyer à la Cour de justice de l'Union européenne une question préjudicielle relative à l'interprétation des articles 5 et 9 de la directive du 25 novembre 2009 relatifs à son champ d'application et de surseoir à statuer dans l'attente de son arrêt.<br/>
<br/>
<br/>
                        Vu les autres pièces du dossier ;<br/>
<br/>
                        Vu :<br/>
                        - la loi n° 79-587 du 11 juillet 1979 ;<br/>
                        - l'ordonnance n° 2015-378 du 2 avril 2015 ;<br/>
                        - le code de la mutualité ;<br/>
              - la directive 2009/138/CE du Parlement européen et du Conseil du 25 novembre 2009 ;<br/>
<br/>
                        - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Simon Chassard, auditeur,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              1. Le décret dont la Fédération nationale indépendante des mutuelles demande l'annulation, ainsi que les décisions implicites de rejet de ses demandes tendant à l'abrogation de ce décret, ont un caractère réglementaire. La fédération requérante ne saurait, par suite, utilement soutenir que ces  actes méconnaissent les dispositions de l'article 1er de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public, aujourd'hui reprises à l'article L. 211-2 du code des relations entre le public et l'administration, qui ne sont pas applicables aux actes réglementaires. <br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              2. Aux termes de l'article 2 de la directive 2009/138/CE du 25 novembre 2009 du Parlement européen et du Conseil sur l'accès aux activités de l'assurance et de la réassurance et leur exercice (solvabilité II) : " 1. La présente directive s'applique aux entreprises d'assurance directe vie et non-vie qui sont établies sur le territoire d'un Etat membre ou qui désirent s'y établir (...) ". Le 1 de l'article 13 de cette même directive définit l'entreprise d'assurance comme une " entreprise d'assurance directe vie ou non-vie ayant obtenu un agrément conformément à l'article 14 " de la directive. La liste des entreprises d'assurance qui doivent solliciter l'octroi d'un agrément figure à l'annexe III de la directive, qui comprend les mentions suivantes : "  Forme juridique des entreprises (...) / A. Formes des entreprises d'assurance non-vie : / (...) / 10) en ce qui concerne la République française : société anonyme, société d'assurance mutuelle, institution de prévoyance régie par le code de la sécurité sociale, institution de prévoyance régie par le code rural et mutuelle régie par le code de la mutualité ; / (...) ". Cette annexe reprend, pour la France, la même liste s'agissant des formes d'entreprise d'assurance vie. Enfin, les articles 5 et 9 de la directive prévoient que cette dernière ne s'applique pas, d'une part, pour l'assurance non-vie, aux " opérations des organismes de prévoyance et de secours dont les prestations varient d'après les ressources disponibles et dans lesquels la contribution des adhérents est déterminée forfaitairement ", et d'autre part, pour l'assurance vie, aux " opérations des organismes de prévoyance et de secours qui accordent des prestations variables selon les ressources disponibles et exigent de chacun de leurs adhérents une contribution appropriée ".<br/>
<br/>
              3. L'article 13 du décret attaqué fait application du titre III de l'ordonnance du 2 avril 2015 transposant la directive 2009/138/CE du Parlement européen et du Conseil sur l'accès aux activités de l'assurance et de la réassurance et leur exercice (solvabilité II), qui soumet les mutuelles régies par le code de la mutualité aux obligations découlant de cette directive. La Fédération nationale indépendante des mutuelles soutient que les dispositions de cet article méconnaissent le champ d'application de la directive en y incluant les mutuelles régies par le livre II du code de la mutualité, qui, selon elle, en sont exclues par les dispositions, citées au point 2 ci-dessus, des articles 5 et 9 de cette directive.<br/>
<br/>
              4. En premier lieu, les mutuelles régies par le code de la mutualité sont mentionnées à l'annexe III de la directive du 25 novembre 2009, qui reprend les dispositions du a) du 1 de l'article 8 de la directive 73/239/CEE du Conseil du 24 juillet 1973 portant coordination des dispositions législatives, réglementaires et administratives concernant l'activité de l'assurance directe autre que l'assurance sur la vie, et son exercice. Par un arrêt Commission c/ France du 16 décembre 1999 (affaire C-239/98), la Cour de justice des communautés européennes a déclaré que la République française avait manqué aux obligations lui incombant en vertu de cette dernière directive en ne prenant pas, s'agissant des mutuelles régies par le code de la mutualité, les dispositions législatives, réglementaires et administratives nécessaires pour s'y conformer de manière complète. Il résulte de cet arrêt que la mention des mutuelles régies par le code de la mutualité au a) du 1 de l'article 8 de la directive du 24 juillet 1973 emportait inclusion de ces dernières dans le champ d'application de cette directive. Il en résulte que la mention des mutuelles régies par le code de la mutualité à l'annexe III de la directive du 25 novembre 2009 emporte leur inclusion dans le champ d'application de cette même directive.<br/>
<br/>
              5. En second lieu, l'exemption prévue par les articles 5 et 9 de la directive du 25 novembre 2009 vise les organismes qui, en vertu de leur statut ou du régime juridique spécifique qui leur est applicable, sont en mesure de faire varier le montant des prestations qu'ils servent en fonction du niveau de leurs ressources disponibles sans s'engager au règlement intégral de leurs engagements, de telle sorte qu'ils ne sont exposés à aucun risque de solvabilité.<br/>
<br/>
              6. Si l'article L. 114-7 du code de la mutualité dispose que : " Les décisions régulièrement prises par l'assemblée générale d'une mutuelle, d'une union ou d'une fédération s'imposent à l'organisme et à ses membres sous réserve de leur conformité aux dispositions du présent code. / Les modifications des montants de cotisations ainsi que des prestations sont applicables dès qu'elles ont été notifiées aux adhérents ", il n'a pas pour objet ou pour effet de dispenser les mutuelles régies par le code de la mutualité de respecter les dispositions de l'article L. 211-2 du même code qui prévoient que les mutuelles " garantissent à leurs membres participants et aux ayants droit de ceux-ci le règlement intégral des engagements qu'elles contractent à leur égard ". Partant, les mutuelles ne peuvent être regardées comme des organismes offrant des prestations variables selon les ressources disponibles, au sens des articles 5 et 9 de la directive du 25 novembre 2009.<br/>
<br/>
              7. Par suite, le moyen tiré de ce que le décret du 7 mai 2015 méconnaîtrait les dispositions de la directive du 25 novembre 2009 doit être écarté, sans qu'il soit besoin de saisir la Cour de justice de l'Union européenne d'une question préjudicielle sur ce point.<br/>
<br/>
              8. Il résulte de tout ce qui précède que la Fédération nationale indépendante des mutuelles n'est pas fondée à demander l'annulation du décret du 7 mai 2015 qu'elle attaque ni des décisions implicites portant refus de l'abroger. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la Fédération nationale indépendante des mutuelles est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la Fédération nationale indépendante des mutuelles, au Premier ministre et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
