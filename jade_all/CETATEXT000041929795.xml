<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041929795</ID>
<ANCIEN_ID>JG_L_2020_05_000000440707</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/92/97/CETATEXT000041929795.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 25/05/2020, 440707, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-05-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440707</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:440707.20200525</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au juge des référés du tribunal administratif de Bordeaux, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, de suspendre la décision du 21 avril 2020 par laquelle la commune de Bordeaux a décidé d'interrompre son recrutement en qualité de chef de service au sein de la police municipale de Bordeaux, d'autre part, d'enjoindre à cette commune de poursuivre cette procédure de recrutement et de solliciter du préfet et du procureur de la République d'accorder les agréments nécessaires à l'exercice de ses fonctions au sein de la police municipale. Par une ordonnance n° 2001940 du 5 mai 2020, le juge des référés du tribunal administratif de Bordeaux a rejeté cette demande.<br/>
<br/>
              Par une requête, enregistrée le 19 mai 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... demande :<br/>
<br/>
              1°) d'annuler l'ordonnance du 5 mai 2020 ;<br/>
<br/>
              2°) de faire droit à ses conclusions de première instance, en assortissant les injonctions prononcées d'une astreinte de 100 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Bordeaux une somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - l'ordonnance qu'il attaque fait, de manière erronée, état d'une " décision refusant le détachement d'un fonctionnaire ", alors qu'il critique une décision d'interruption de son recrutement ;<br/>
              - l'ordonnance est insuffisamment motivée ;<br/>
              - le juge de première instance a omis, à tort, de se prononcer sur l'urgence qu'il y avait à faire droit à ses demandes ;<br/>
              - l'interruption de son recrutement porte atteinte à son droit à mener une vie familiale normale, puisque sa famille s'est installée à Bordeaux et que, faute d'être recruté au sein de la police municipale de la ville, il va être obligé de reprendre son poste au sein de la police nationale à Marseille dans la mesure où son détachement est désormais impossible et, ainsi, en être séparé ;<br/>
              - ce même retrait méconnaît son droit au travail et sa liberté contractuelle ;<br/>
              - il y a urgence à prendre les mesures qu'il réclame, dans la mesure où l'emploi qui lui était destiné est, de nouveau, offert au recrutement sur le portail en ligne de la fonction publique territoriale et où il a signé une promesse de vente de la maison dont il est propriétaire dans le Var ;<br/>
              - les motifs pour lesquels son recrutement a été interrompu méconnaissent les dispositions combinées des articles L. 511-2 du code de justice administrative et 10-1 du décret n° 2011-444 du 21 avril 2011, qui supposent que les personnes exerçant des fonctions d'agent de police municipale et détachées à cet effet soient en premier lieu nommées par le maire, puis agréées par le préfet et le procureur de la République et, enfin, postérieurement seulement, détachées dans un emploi de la police municipale.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de la sécurité intérieure ;<br/>
              - le décret n° 2011-444 du 21 avril 2020 ;<br/>
              - le code justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures. ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. Il appartient au juge des référés saisi en appel de porter son appréciation sur ce point au regard de l'ensemble des pièces du dossier, et notamment des éléments recueillis par le juge de première instance dans le cadre de la procédure écrite et orale qu'il a diligentée.<br/>
<br/>
              2. M. B..., fonctionnaire de la police nationale affecté à Marseille, a présenté sa candidature à un emploi de chef de service au sein de la police municipale de Bordeaux, puis a reçu de la directrice du " pilotage emploi et ressources humaines " de la direction générale des ressources humaines de la commune un courrier l'informant de l'intention de celle-ci de le recruter. Il a, alors, sollicité de son administration d'origine son détachement dans cet emploi. A la suite de divers échanges entre cette dernière administration et celle de la commune, une divergence d'interprétation des dispositions combinées de l'article L. 511-2 du code de la sécurité intérieure et de l'article 10-1 du décret du 21 avril 2011 susvisé est apparue, la direction générale de la police nationale conditionnant le détachement de M. B... à son agrément préalable par le préfet et le procureur de la République, la commune estimant, pour sa part, que le détachement devait être prononcé avant qu'elle puisse procéder à la demande d'agrément, puis à la nomination de M. B... dans l'emploi qui lui était destiné. Par un courrier du directeur général des ressources humaines de la commune de Bordeaux en date du 21 avril 2020, il a été annoncé à M. B... que, du fait de cette divergence d'appréciation, l'arrêté le nommant dans cet emploi ne pourrait être pris. M. B... relève appel de l'ordonnance du 5 mai 2020 par laquelle le juge des référés du tribunal administratif de Bordeaux, saisi sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté sa demande tendant à ce que cette décision de ne pas poursuivre son recrutement soit suspendue et qu'il soit enjoint à la commune de solliciter l'agrément du préfet et du procureur de la République prévu par l'article L. 511-2 du code de la sécurité intérieure.<br/>
<br/>
              3. Pour rejeter la demande de M. B..., l'ordonnance attaquée a relevé, d'une part, que la décision critiquée ne saurait, par elle-même, affecter l'exercice, par M. B..., des libertés fondamentales dont il se prévaut, d'autre part, que cette décision n'apparaissait pas manifestement illégale au sens des dispositions précitées de l'article L. 521-2 du code de justice administrative.<br/>
<br/>
              4. En premier lieu, si M. B..., dans sa requête d'appel, fait grief à cette ordonnance d'avoir statué sur une décision de refus de détachement, là où sa demande portait sur la suspension de la décision de la commune de Bordeaux d'interrompre la procédure de recrutement le concernant et pas sur la position de son administration d'origine quant à son détachement, il ressort clairement de cette ordonnance que le juge des référés du tribunal administratif de Bordeaux ne s'est pas mépris sur le sens de la demande qui lui était faite et que c'est par une erreur de plume, sans incidence sur le sens de sa décision, qu'il a indiqué que M. B... critiquait une décision de refus de détachement.<br/>
<br/>
              5. En deuxième lieu, si M. B... fait valoir que sa famille s'est installée à Bordeaux, où sa conjointe a trouvé un emploi et qu'il est obligé, faute que son détachement ait pu être prononcé du fait de la décision d'arrêter la procédure de recrutement le concernant, de continuer à travailler à Marseille, cette décision n'est, par elle-même, de nature ni à l'empêcher de mener une vie familiale normale, puisqu'elle n'interdit pas à sa famille de le rejoindre, ni à porter atteinte à sa liberté de travailler, ce qu'au demeurant confirme le fait qu'il puisse toujours exercer ses fonctions au sein de la police nationale. En conséquence, même si les difficultés auxquelles il est confronté ressortent des pièces du dossier, M. B... n'est pas fondé à soutenir que c'est à tort que le juge des référés du tribunal administratif de Bordeaux a considéré que la commune de Bordeaux n'avait porté aucune atteinte à ses libertés fondamentales au sens des dispositions précitées de l'article L. 521-2 du code de justice administrative.<br/>
<br/>
              6. Enfin, le motif de l'ordonnance attaquée par lequel le juge des référés du tribunal administratif de Bordeaux a relevé que la décision de la commune des Bordeaux n'était pas manifestement illégale est, en tout état de cause, surabondant, en l'absence d'atteinte, par cette dernière, à une liberté fondamentale et cette ordonnance, qui apparaît suffisamment motivée, n'avait pas, contrairement à ce que soutient M. B..., à se prononcer sur la condition d'urgence, faute d'une telle atteinte.<br/>
<br/>
              7. Il résulte de tout ce qui précède qu'il est manifeste que l'appel de M. B... ne peut être accueilli. Sa requête, y compris ses conclusions au titre de l'article L. 761-1 du code de justice administrative, ne peut dès lors qu'être rejetée, selon la procédure prévue par l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
