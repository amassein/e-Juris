<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039258869</ID>
<ANCIEN_ID>JG_L_2019_10_000000427045</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/25/88/CETATEXT000039258869.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 21/10/2019, 427045, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427045</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>Mme Françoise Tomé</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:427045.20191021</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Mme A... B... a demandé au juge des référés du tribunal administratif de Marseille d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de la décision de la ministre du travail, en date du 21 novembre 2018, procédant au retrait de la décision implicite de rejet du recours hiérarchique formé contre la décision de l'inspecteur du travail du 23 mars 2018 refusant à la société Allianz Vie l'autorisation de la licencier, et autorisant son licenciement. Par une ordonnance du 28 décembre 2018 n° 1809841, le juge des référés du tribunal a fait droit à sa demande.<br/>
<br/>
              1° Sous le n° 427045, par un pourvoi et un mémoire en réplique, enregistrés les 14 janvier et 10 avril 2019 au secrétariat du contentieux du Conseil d'Etat, la société Allianz Vie demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant au titre de la procédure de référé, de rejeter la demande de suspension ;<br/>
<br/>
              3°) de mettre à la charge de Mme A... B... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 427074, par un pourvoi, enregistré le 15 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, la ministre du travail demande au Conseil d'Etat d'annuler la même ordonnance.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Françoise Tomé, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de la société Allianz Vie et à la SCP Spinosi, Sureau, avocat de Mme  A... B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les pourvois de la société Allianz Vie et de la ministre du travail sont dirigés contre la même ordonnance, en date du 28 décembre 2018, du juge des référés du tribunal administratif de Marseille. Il y a donc lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. En premier lieu, aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Ces dispositions ne permettent au justiciable de demander la suspension d'une décision administrative qu'à la condition qu'une telle décision soit encore susceptible d'exécution. En second lieu, la rupture du contrat de travail prend effet à compter de l'envoi du courrier recommandé avec accusé de réception notifiant cette rupture au salarié. Il résulte de ce qui précède que la décision administrative qui autorise le licenciement d'un salarié protégé doit être regardée comme entièrement exécutée à compter de cet envoi.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Marseille qu'à la suite de la décision de la ministre du travail du 21 novembre 2018 autorisant la société Allianz Vie à licencier Mme A... B..., cette société a envoyé le 30 novembre 2018 à l'intéressée une lettre recommandée avec accusé de réception lui notifiant son licenciement, mettant ainsi fin au contrat de travail à compter de cette date. Dès lors, par l'envoi de cette lettre prononçant le licenciement de Mme A... B..., la décision administrative autorisant son licenciement a été entièrement exécutée à compter de cette date. Si le juge des référés du conseil de prud'hommes de Martigues a, par une ordonnance du 21 décembre 2018, suspendu les effets du licenciement, cette mesure qui ne pouvait qu'être provisoire et qui ne se prononçait pas sur la validité du licenciement lui-même, est restée sans conséquence sur l'entière exécution de la décision de la ministre du travail, du 21 novembre 2018, autorisant le licenciement de cette salariée. Par suite, en se fondant sur cette ordonnance pour juger qu'elle a eu pour effet d'empêcher la décision d'autorisation contestée d'être regardée comme entièrement exécutée, le juge des référés du tribunal administratif de Marseille a commis une erreur de droit.<br/>
<br/>
              4. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens des pourvois, la société Allianz Vie et la ministre du travail sont fondées à demander l'annulation de l'ordonnance du 28 décembre 2018 du juge des référés du tribunal administratif de Marseille suspendant l'exécution de la décision de la ministre du travail du 21 novembre 2018.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de statuer sur la demande de suspension en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              6. La décision du 21 novembre 2018 autorisant le licenciement de Mme A... B... et dont cette dernière a demandé la suspension le 29 novembre 2018 ayant été, ainsi qu'il a été indiqué ci-dessus, entièrement exécutée le 30 novembre 2018, la demande de Mme A... B... a perdu son objet. Il n'y a donc plus lieu d'y statuer.<br/>
<br/>
              7. Enfin, il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme A... B... la somme que demande la société Alianz Vie au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce que soit mises à la charge de l'Etat et de la société Alianz Vie les sommes que demande, à ce titre, Mme A... B....<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 28 décembre 2018 du juge des référés du tribunal administratif de Marseille est annulée.<br/>
Article 2 : Il n'y a pas lieu de statuer sur la demande de Mme A... B... tendant à la suspension de la décision du ministre du travail du 21 novembre 2018 autorisant son licenciement.<br/>
Article 3 : Les conclusions de la société Allianz Vie et de Mme A... B... présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la société Allianz Vie, à la ministre du travail et à Mme C... A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
