<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038477473</ID>
<ANCIEN_ID>JG_L_2019_05_000000412087</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/47/74/CETATEXT000038477473.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 15/05/2019, 412087, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-05-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412087</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>Mme Catherine Calothy</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:412087.20190515</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1°, sous le numéro 412087, par une requête sommaire et un mémoire complémentaire, enregistrés les 4 juillet 2017 et 26 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, la commune du Marin demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2017-784 du 5 mai 2017 portant création du parc naturel marin de la Martinique ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2°, sous le n° 412187, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 5 juillet et 5 octobre 2017 et le 4 février 2019 au secrétariat du contentieux du Conseil d'Etat, la commune du Robert demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2017-784 du 5 mai 2017 portant création du parc naturel marin de la Martinique ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - la convention des Nations Unies sur le droit de la mer conclue à Montego Bay le 10 décembre 1982 ;<br/>
              - le code de l'environnement ;<br/>
              - le décret n° 95-915 du 11 août 1995 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Calothy, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange, de la Burgade, avocat de la commune du Marin et au Cabinet Briard, avocat de la commune du Robert.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              1.	En application de l'article 22 de la Constitution, les actes du Premier ministre sont contresignés, le cas échéant, par les ministres chargés de leur exécution. Le décret attaqué portant création du parc naturel marin de Martinique, s'il prévoit notamment que le directeur de la jeunesse, des sports et de la cohésion sociale de la Martinique ainsi que le directeur de l'Agence régionale de santé de la Martinique sont membres de droit du conseil de gestion du parc naturel marin, n'implique pas nécessairement l'intervention de mesures réglementaires ou individuelles que le ministre chargé de la jeunesse et des sports ou le ministre chargé des affaires sociales et de la santé seraient compétents pour signer ou contresigner. Dans ces conditions, ces ministres n'étaient pas chargés de l'exécution du décret attaqué, qui n'avait pas, dès lors, à être soumis à leur contreseing.<br/>
<br/>
              2.	En vertu du 2ème alinéa de l'article L. 334-3 du code de l'environnement, " Le décret créant un parc naturel marin est pris après enquête publique réalisée conformément au chapitre III du titre II du livre Ier du présent code ". Aux termes de l'article L. 123-1 du code de l'environnement, " L'enquête publique a pour objet d'assurer l'information et la participation du public ainsi que la prise en compte des intérêts des tiers lors de l'élaboration des décisions susceptibles d'affecter l'environnement mentionnées à l'article L. 123-2. ". <br/>
<br/>
              3.	En vertu de l'article R. 123-8 du même code, outre " les pièces et avis exigés par les législations et réglementations applicables au projet, plan ou programme ", le dossier soumis à l'enquête publique comprend au moins les éléments qu'il énumère et notamment, à son 2°, en l'absence d'évaluation environnementale, " une note de présentation précisant les coordonnées du maître d'ouvrage ou de la personne publique responsable du projet, plan ou programme, l'objet de l'enquête, les caractéristiques les plus importantes du projet, plan ou programme et présentant un résumé des principales raisons pour lesquelles, notamment du point de vue de l'environnement, le projet, plan ou programme soumis à enquête a été retenu ". En vertu de l'article R. 334-28 du même code, applicable à la création d'un parc naturel marin, " Le dossier de création comprend : / 1° Un document indiquant les limites du parc naturel marin projeté ; / 2° Une synthèse de l'état du patrimoine marin et des usages du milieu marin ; / 3° Les propositions d'orientations de gestion en matière de connaissance, de conservation et d'usage du patrimoine et du milieu marin ; / 4° Le projet de composition du conseil de gestion du parc ". Ces dispositions n'imposent pas que le dossier d'enquête publique soit assorti, au-delà de la synthèse qu'il comporte, d'études scientifiques portant notamment sur l'état chimique des masses d'eaux ou la situation détaillée des barrières coralliennes. Le moyen tiré de l'insuffisance du dossier soumis à l'enquête publique doit, par suite, être écarté.<br/>
<br/>
              4.	Les communes requérantes soutiennent que l'enquête publique était d'une durée insuffisante et qu'elle a eu lieu pendant les préparatifs du carnaval annuel et dans une période précédant les élections professionnelles des comités de pêche peu propice à la consultation des professionnels de ce secteur d'activité. Il ressort toutefois des pièces du dossier que l'enquête publique s'est déroulée pendant une durée de 30 jours, qui n'est pas inférieure à la durée minimale prescrite par les dispositions de l'article L. 123-9 du code de l'environnement, et que les circonstances invoquées relatives à la période d'organisation de l'enquête publique n'ont pas été de nature à entacher cette dernière d'irrégularité. <br/>
<br/>
              5.	La commune du Robert soutient en outre que l'enquête publique était irrégulière en ce qu'elle n'a été organisée que dans 27 communes de Martinique situées sur le littoral de l'île, alors que de nombreuses personnes concernées par les activités de la pêche résident dans des communes non littorales et sans que les îles voisines aient été associées à la consultation. Il ressort du rapport de la commission d'enquête publique que si le dossier d'enquête publique a été mis à disposition des 27 communes du littoral désignées par l'arrêté en date du 7 décembre 2016 du préfet de région de la Martinique prescrivant l'ouverture et l'organisation d'une enquête publique préalable au décret de création du parc naturel marin de Martinique, il a également été mis en ligne sur le site internet de la préfecture et de la direction de l'environnement, de l'aménagement et du logement de la Martinique et des permanences ont été organisées dans 21 de ces 27 communes. Par suite, contrairement à ce que soutient la commune du Robert, l'enquête publique ne peut être regardée comme n'ayant pas assuré l'information et la participation du public, en méconnaissance des dispositions précitées de l'article L. 123-1 du code de l'environnement.<br/>
<br/>
              6.	Aux termes de l'article L. 123-12 du code de l'environnement, dans sa rédaction issue de l'ordonnance du 3 août 2017 portant réforme des procédures destinées à assurer l'information et la participation du public à l'élaboration de certaines décisions susceptibles d'avoir une incidence sur l'environnement, : " Un accès gratuit au dossier [d'enquête publique mis en ligne pendant toute la durée de l'enquête] est également garanti par un ou plusieurs postes informatiques dans un lieu ouvert au public ". Cependant, en vertu des dispositions transitoires du I de l'article 8 de la même ordonnance, " les dispositions qu'elle comporte ne sont applicables qu'aux décisions pour lesquelles une participation du public a été engagée postérieurement à cette date ", cette dernière étant, en vertu du II du même article combiné à l'article 19 du décret du 25 avril 2017 pris pour l'application de l'ordonnance, le 1er janvier 2017. Ces dispositions ne sont en conséquence pas applicables au décret attaqué, l'avis d'enquête publique ayant été publié le 7 décembre 2016. Le moyen tiré de la méconnaissance de ces dispositions ne peut donc qu'être écarté.<br/>
<br/>
              7.	En vertu de l'article R. 334-29 du code de l'environnement, " Le projet de création d'un parc naturel marin est, simultanément ou successivement : / 1° Soumis pour avis aux personnes et organismes directement intéressés par le projet, figurant sur une liste établie par les représentants de l'Etat chargés de conduire la procédure et choisies parmi les catégories suivantes : services et établissements publics de l'Etat, régions et départements, communes littorales et leurs groupements, chambres de commerce et d'industrie territoriales, comités régionaux et locaux des pêches maritimes et des élevages marins, comités régionaux de la conchyliculture, organismes de gestion d'espaces naturels au sens du livre troisième du présent code. A défaut de réponse dans le délai de deux mois à compter de la réception de la demande d'avis, celui-ci est réputé favorable ". Il résulte de ces dispositions que la consultation des personnes intéressées peut avoir lieu en même temps que l'enquête. Par suite, le moyen tiré de ce que les avis n'étaient pas connus à la date de lancement de l'enquête ne peut qu'être écarté. <br/>
<br/>
              8.	Si la commune du Marin soutient que la présentation du projet de création du parc à la population a été insuffisante, qu'en particulier aucune séance de " questions-réponses " n'a été organisée préalablement à l'enquête publique et que seules 200 personnes ont été consultées alors que le projet concernerait tout le territoire de la Martinique, ces circonstances ne sont pas, en tout état de cause, de nature à établir que les dispositions précitées de l'article L. 123-1 du code de l'environnement avaient été méconnues.<br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              En ce qui concerne la décision de créer le parc :<br/>
<br/>
              9.	Aux termes de l'article L. 334-3 du code de l'environnement : " Des parcs naturels marins peuvent être créés dans les eaux placées sous la souveraineté ou la juridiction de l'Etat, ainsi que sur les espaces appartenant au domaine public maritime ou au plateau continental, pour contribuer à la connaissance du patrimoine marin ainsi qu'à la protection et au développement durable du milieu marin. La création de parcs naturels marins situés en partie dans les eaux sous juridiction de l'Etat ou sur son plateau continental tient compte des dispositions de la convention des Nations unies sur le droit de la mer du 10 décembre 1982 ".<br/>
<br/>
              10.	En premier lieu, si la commune du Marin soutient que le projet de création du parc naturel marin de Martinique, auquel la commission d'enquête publique a émis un avis défavorable, d'une part, n'était pas nécessaire compte tenu notamment des modalités de gestion de l'espace maritime déjà assurées par l'autorité préfectorale et de l'existence du sanctuaire Agoa et, d'autre part, constituait un frein au développement économique de la Martinique, il ne ressort pas des pièces du dossier que la décision de créer le parc était entachée d'une erreur manifeste d'appréciation, eu égard à l'intérêt général qui s'attache à la protection du milieu marin de la Martinique que la commune requérante ne conteste d'ailleurs pas. <br/>
<br/>
              11.	En second lieu, les dispositions du décret attaqué n'emportent pas, par elles-mêmes, de mesures de conservation ou de gestion des ressources biologiques, telles que des mesures de restriction de la pêche. Dès lors, les moyens tirés de la méconnaissance, d'une part, de l'article 61 de la convention des Nations unies sur le droit de la mer du 10 décembre 1982 qui stipule que les mesures de conservation et de gestion des ressources biologiques doivent viser à maintenir ou rétablir les stocks des espèces eu égard notamment aux besoins économiques des collectivités côtières vivant de la pêche et compte tenu des méthodes employées en matière de pêche et, d'autre part, de l'article 63 de la même convention qui stipule que lorsqu'un même stock de poissons se trouve dans les zones économiques exclusives de plusieurs Etats côtiers ou à la fois dans la zone économique exclusive d'un Etat et dans un secteur adjacent à la zone économique exclusive d'un autre Etat, ces Etats doivent s'efforcer de s'entendre sur les mesures nécessaires à la conservation de ces stocks, ne peuvent qu'être en tout état de cause écartés.<br/>
<br/>
              En ce qui concerne la composition du conseil de gestion du parc :<br/>
<br/>
              12.	En vertu de l'article L. 334-4 du code de l'environnement, le conseil de gestion du parc naturel marin doit être " composé de représentants locaux de l'Etat de façon minoritaire, de représentants des collectivités territoriales intéressées et de leurs groupements compétents, du représentant du ou des parcs naturels régionaux intéressés, du représentant de l'organisme de gestion d'une aire marine protégée contiguë, de représentants d'organisations représentatives des professionnels, d'organisations d'usagers, d'associations de protection de l'environnement et de personnalités qualifiées ".<br/>
<br/>
              13.	En premier lieu, il résulte des dispositions de l'article 3 du décret attaqué que le conseil de gestion, composé de 53 membres, comporte 14 représentants des collectivités territoriales et de leurs groupements, dont 7 élus de la Collectivité territoriale de la Martinique, un élu de chacun des trois établissements publics de coopération intercommunale de la Martinique et 4 élus de quatre communes littorales de Martinique, ainsi que 8 professionnels de la pêche et des élevages marins. Par suite, le moyen tiré de l'erreur manifeste d'appréciation dont serait entachée la composition du conseil de gestion en ce que, d'une part, les collectivités territoriales et les organisations représentatives des pêcheurs professionnels seraient insuffisamment représentées et, d'autre part, la composition du comité de gestion du parc naturel marin de Martinique serait représentative des seuls intérêts des communes littorales, ne peut être qu'écarté. <br/>
<br/>
              14.	En second lieu, l'île de Sainte-Lucie constitue un Etat indépendant ne relevant pas de la souveraineté française. Par suite, les dispositions précitées de l'article L. 334-4 du code de l'environnement ne sauraient imposer la présence d'un représentant de l'organisme de gestion de l'aire marine protégée que comporte cette île.<br/>
<br/>
              15.	En troisième lieu, il ressort des pièces du dossier que la réserve naturelle des îlets de Sainte-Anne, créée par un décret n° 95-915 du 11 août 1995, et les zones de l'arrêté préfectoral de biotope du 22 octobre 2002 concernant l'île Loup Garou, ne comportent pas de parties maritimes et ne constituent ainsi pas des aires marines protégées en application des dispositions de l'article L. 334-1 du code de l'environnement. Par suite, la commune du Robert ne saurait utilement invoquer la méconnaissance des dispositions précitées de l'article L. 334-4 du code de l'environnement faute de représentant, au sein du conseil de gestion du parc naturel marin de Martinique, de l'organisme de gestion d'une aire maritime protégée contiguë.<br/>
<br/>
              16.	Il résulte de ce qui précède que les communes du Marin et du Robert ne sont pas fondées à demander l'annulation du décret attaqué.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Sur les frais de justice :<br/>
<br/>
              17.	Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes de la commune du Marin et de la commune du Robert sont rejetées.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la commune du Marin, à la commune du Robert et au ministre d'Etat, ministre de la transition écologique et solidaire.<br/>
      Copie en sera adressée au Premier ministre et à la ministre des outre-mer. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
