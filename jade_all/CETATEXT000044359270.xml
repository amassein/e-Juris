<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044359270</ID>
<ANCIEN_ID>JG_L_2021_11_000000433536</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/35/92/CETATEXT000044359270.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 22/11/2021, 433536, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433536</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI, REBEYROL ; SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>Mme Catherine Brouard-Gallet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:433536.20211122</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Le syndicat national de l'enseignement privé CFE-CGC (SYNEP CFE-CGC), le syndicat national de l'enseignement privé initial (SNEIP-CGT) et la Fédération de l'éducation, de la recherche et de la culture CGT (FERC-CGT) ont demandé à la cour administrative d'appel de Paris d'annuler pour excès de pouvoir l'arrêté du 22 décembre 2017 par lequel la ministre du travail a fixé la liste des organisations représentatives dans la convention collective nationale de l'enseignement agricole privé. <br/>
<br/>
              Par un arrêt n° 18PA00724 du 20 juin 2019, la cour administrative d'appel de Paris a annulé cet arrêté. <br/>
<br/>
              Par un pourvoi, enregistré le 12 août 2019 au secrétariat du contentieux du Conseil d'Etat, la ministre du travail demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Brouard-Gallet, conseillère d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de la Fédération CFDT de l'enseignement privé et à la SCP Gatineau, Fattaccini, Rebeyrol, avocat du syndicat national de l'enseignement privé CFE-CGC ;<br/>
<br/>
<br/>
              Vu la note en délibéré, enregistrée le 3 novembre 2021, présentée par la ministre du travail, de l'emploi et de l'insertion ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 2122-11 du code du travail : " Après avis du Haut Conseil du dialogue social, le ministre chargé du travail arrête la liste des organisations syndicales reconnues représentatives par branche professionnelle et des organisations syndicales reconnues représentatives au niveau national et interprofessionnel en application des articles L. 2122-5 à L. 2122-10 ". En application de ces dispositions, la ministre du travail a pris, le 22 décembre 2017, un arrêté " fixant la liste des organisations syndicales reconnues représentatives dans la convention collective nationale de l'enseignement agricole privé ". L'article 1er de l'arrêté dresse la liste des organisations syndicales représentatives dans le champ de cette convention, en l'espèce la Confédération française démocratique du travail (CFDT), la Confédération française des travailleurs chrétiens (CFTC), le syndicat professionnel de l'enseignement libre catholique (SPELC). Son article 2 définit, dans cette branche, pour la négociation des accords collectifs en application de l'article L. 2232-6 du code du travail, le poids de chacune des organisations syndicales représentatives. Par un arrêt du 20 juin 2019, la cour administrative d'appel de Paris, sur la requête du syndicat national de l'enseignement privé initial (SNEIP) CGT, du Syndicat national de l'enseignement privé CFE-CGC (SYNEP CFE-CGC) et de la Fédération de l'éducation, de la recherche et de la culture CGT (FERC CGT), a annulé cet arrêté. La ministre du travail se pourvoit en cassation contre cet arrêt.<br/>
<br/>
              2. D'une part, aux termes de l'article L. 2121-1 du code du travail : " La représentativité des organisations syndicales est déterminée d'après les critères cumulatifs suivants : / 1° Le respect des valeurs républicaines ; / 2° L'indépendance ; / 3° La transparence financière ; / 4° Une ancienneté minimale de deux ans dans le champ professionnel et géographique couvrant le niveau de négociation (...) ; / 5° L'audience établie selon les niveaux de négociation conformément aux articles L. 2122-1, L. 2122-5, L. 2122-6 et L. 2122-9 ; / 6° L'influence, prioritairement caractérisée par l'activité et l'expérience ; / 7° Les effectifs d'adhérents et les cotisations ". Aux termes de l'article L. 2122-5 du même code : " Dans les branches professionnelles, sont représentatives les organisations syndicales qui : / 1° Satisfont aux critères de l'article L. 2121-1 ; / 2° Disposent d'une implantation territoriale équilibrée au sein de la branche ; / 3° Ont recueilli au moins 8 % des suffrages exprimés résultant de l'addition au niveau de la branche, d'une part, des suffrages exprimés au premier tour des dernières élections des titulaires aux comités sociaux et économiques, quel que soit le nombre de votants, et, d'autre part, des suffrages exprimés au scrutin concernant les entreprises de moins de onze salariés dans les conditions prévues aux articles L. 2122-10-1 et suivants. La mesure de l'audience s'effectue tous les quatre ans ". Aux termes de l'article D. 2122-6 du code du travail : " Le système de centralisation des résultats des élections professionnelles mentionnées aux articles L. 2122-5 à L. 2122-10 afin de mesurer l'audience des organisations syndicales doit : / a) Garantir la confidentialité et l'intégrité des données recueillies et traitées ; / b) Permettre de s'assurer, par des contrôles réguliers, de la fiabilité et de l'exhaustivité des données recueillies et consolidées (...) ".<br/>
<br/>
              3. Il résulte des dispositions qui précèdent que, pour la mesure de l'audience des organisations syndicales dans une branche professionnelle, en application de l'article L. 2122-5 du code du travail cité au point 2, en vue de l'édiction de la liste des organisations syndicales reconnues représentatives dans le champ de cette branche en application de l'article L. 2122-11 du même code, cité au point 1, il appartient au ministre du travail de se fonder sur les suffrages exprimés à l'occasion des élections professionnelles grâce à un système de centralisation des résultats dont les caractéristiques sont fixées par l'article D. 2122-6 du code du travail, pour autant que ces suffrages lui permettent, avec la fiabilité et l'exhaustivité requises, d'apprécier l'audience des organisations syndicales dans le champ de la convention de branche considérée. <br/>
<br/>
              4. D'autre part, aux termes de l'article L. 813-8 du code rural et de la pêche maritime, dans sa rédaction applicable au litige : " Dans les établissements dont les formations sont dispensées dans les conditions prévues au premier alinéa de l'article L. 811-5, l'association ou l'organisme responsable, et lié à l'Etat par contrat, désigne le chef d'établissement qui doit détenir les titres et présenter les qualifications comparables à ceux requis dans l'enseignement agricole public. (...) / Les personnels enseignants et de documentation de ces établissements sont nommés par le ministre de l'agriculture, après vérification de leurs titres et de leurs qualifications, sur proposition du chef d'établissement. Ils sont liés par un contrat de droit public à l'Etat, qui les rémunère directement par référence aux échelles indiciaires des corps équivalents de la fonction publique exerçant des fonctions comparables et ayant les mêmes niveaux de formation. En leur qualité d'agent public, ils ne sont pas, au titre des fonctions pour lesquelles ils sont employés et rémunérés par l'Etat, liés par un contrat de travail à l'établissement au sein duquel l'enseignement leur est confié. / Nonobstant l'absence de contrat de travail avec l'établissement, les personnels enseignants mentionnés à l'alinéa précédent sont, pour l'application des articles L. 4523-11, L. 4523-14, L. 4523-15, L. 4523-16, L. 4523-17, L. 2311-2 et L. 2312-8 du code du travail, pris en compte dans le calcul des effectifs de l'établissement, tel que prévu à l'article L. 1111-2 du même code. Ils sont électeurs et éligibles pour les élections des membres du comité social et économique. Ils bénéficient de ces institutions dans les conditions prévues par le code du travail. (...) ".<br/>
<br/>
              5. L'article 1er de la convention collective des personnels des établissements agricoles privés relevant du Conseil national de l'enseignement agricole privé stipule que cette convention a pour objet de régler les rapports entre, d'une part, les associations ou organismes ayant qualité d'employeurs dans les établissements d'enseignement agricole privé relevant de l'article L. 813-8 du code rural et de la pêche maritime ainsi que les établissements de formation professionnelle continue et d'apprentissage, adhérents ou affiliés au Conseil national de l'enseignement agricole privé, et d'autre part, les seuls personnels de ces établissements dont la relation de travail est régie par un contrat de droit privé.<br/>
<br/>
              6. Il résulte de ce qui a été dit aux points précédents que si les personnels enseignants et de documentation mentionnés à l'article L. 813-8 du code rural et de la pêche maritime, qui sont des agents publics, bénéficient de la qualité d'électeur pour les élections des institutions représentatives du personnel dans les établissements privés d'enseignement agricole couverts par la convention collective précédemment mentionnée et sont éligibles, leurs votes ne peuvent être pris en compte pour la détermination de la représentativité des organisations syndicales au niveau de la branche professionnelle de l'enseignement privé agricole, laquelle est couverte par une convention collective qui ne régit que les relations entre les employeurs relevant de son champ et leurs salariés de droit privé. <br/>
<br/>
              7. Par suite, la cour administrative d'appel de Paris a commis une erreur de droit en annulant l'arrêté du 22 décembre 2017 au motif que la ministre du travail n'avait pas pris en compte, pour mesurer l'audience des organisations syndicales de la branche de l'enseignement privé agricole, les suffrages émis, lors des élections professionnelles, dans les urnes réservées aux agents publics mentionnés à l'article L. 813-4 du code rural et de la pêche maritime. La ministre du travail est donc fondée, sans qu'il soit besoin de se prononcer sur l'autre moyen de son pourvoi, à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              9. Il ressort des pièces du dossier que lors des élections professionnelles organisées au niveau de la branche de l'enseignement agricole privé, les suffrages des personnels de droit privé et des agents de droit public de l'enseignement agricole privé n'ont pas fait l'objet de décomptes séparés, faute de mise en place d'urnes distinctes lors des opérations électorales, sauf en ce qui concerne quatre établissements. Il ressort également des pièces du dossier que, pour la confection de l'arrêté attaqué, la ministre du travail a mesuré l'audience des organisations syndicales dans cette branche en prenant en compte l'ensemble des suffrages émis, à l'exception de ceux émis par les agents publics dans les quatre établissements qui avaient séparé le vote des personnels de droit privé et celui des agents publics. Il résulte de ce qui a été dit au point 3 qu'à défaut de la mise en place générale d'urnes spécifiquement dédiées au vote des agents de droit public pour les élections professionnelles organisées au sein de la branche de l'enseignement agricole privé, permettant de distraire leurs suffrages de ceux émis par les personnels de droit privé seuls régis par la convention collective nationale de l'enseignement agricole privé, les résultats pris en compte par la ministre du travail afin de fixer la liste des organisations syndicales reconnues comme représentatives dans le champ de cette convention ne satisfaisaient pas l'exigence de fiabilité requise. Par suite, sans qu'il soit besoin de se prononcer sur les autres moyens des requêtes, l'arrêté de la ministre du travail du 22 décembre 2017 doit être annulé pour excès de pouvoir. <br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement au syndicat national de l'enseignement privé CFE-CGC, au syndicat national de l'enseignement privé initial et à la Fédération de l'éducation, de la recherche et de la culture CGT d'une somme de 1 000 euros chacun au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'il soit fait droit aux conclusions présentées au même titre par le syndicat professionnel de l'enseignement libre catholique, par la Fédération CFDT de l'enseignement privé et par la Confédération française des travailleurs chrétiens. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 20 juin 2019 est annulé. <br/>
Article 2 : L'arrêté du 22 décembre 2017 de la ministre du travail est annulé.<br/>
Article 3 : L'Etat versera au syndicat national de l'enseignement privé CFE-CGC, au syndicat national de l'enseignement privé initial et à la Fédération de l'éducation, de la recherche et de la culture CGT une somme de 1 000 euros chacun au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Les conclusions présentées par le syndicat professionnel de l'enseignement libre catholique, par la Fédération CFDT de l'enseignement privé et par la Confédération française des travailleurs chrétiens au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à la ministre du travail, de l'emploi et de l'insertion, au syndicat national de l'enseignement privé CFE-CGC, au syndicat national de l'enseignement privé initial, à la Fédération de l'éducation, de la recherche et de la culture CGT, à la Fédération CFDT de l'enseignement privé, au syndicat professionnel de l'enseignement libre catholique, au syndicat professionnel de l'enseignement libre catholique de la CFTC, à la Confédération française des travailleurs chrétiens, à la Confédération française démocratique du travail et à la Fédération nationale des syndicats professionnels de l'enseignement libre catholique. <br/>
              Délibéré à l'issue de la séance du 22 octobre 2021 où siégeaient : M. Jacques-Henri Stahl, président adjoint de la section du contentieux, présidant ; Mme A... O..., Mme F... N..., présidentes de chambre ; M. B... M..., M. K... I..., Mme J... L..., Mme D... H..., M. Damien Botteghi, conseillers d'Etat et Mme Catherine Brouard-Gallet, conseillère d'Etat en service extraordinaire-rapporteure. <br/>
<br/>
              Rendu le 22 novembre 2021.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. P... C...<br/>
 		La rapporteure : <br/>
      Signé : Mme Catherine Brouard-Gallet<br/>
                 La secrétaire :<br/>
                 Signé : Mme E... G...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
