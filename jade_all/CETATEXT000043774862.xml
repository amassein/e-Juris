<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043774862</ID>
<ANCIEN_ID>JG_L_2021_06_000000452482</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/77/48/CETATEXT000043774862.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 10/06/2021, 452482, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>452482</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BUK LAMENT - ROBILLOT</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:452482.20210610</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 11 et 26 mai 2021 au secrétariat du contentieux du Conseil d'Etat, la société Imerys Aluminates demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision implicite de rejet née du silence gardé par la ministre de la transition écologique sur sa demande du 9 février 2021 tendant au maintien des ciments d'aluminates de calcium dans les référentiels de " clinker " de ciment gris (BM10) et blanc (BM11) pour la quatrième période du système d'échange de quotas d'émission de gaz à effet de serre ;<br/>
<br/>
              2°) d'enjoindre a` titre provisoire et dans un délai de quinze jours, à la ministre de la transition écologique d'inclure les ciments d'aluminates de calcium dans le référentiel de " clinker " de ciment gris et blanc ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - la condition d'urgence est satisfaite dès lors que, d'une part, la décision contestée affecte considérablement l'équilibre économique de son activité et met en cause la pérennité de ses trois sites industriels en France, dans un secteur mondialisé exposé à une forte concurrence par les prix, pour un préjudice financier qu'elle évalue à plus de 6,5 millions d'euros par an, et, d'autre part, l'allocation des quotas est actuellement en cours de finalisation, de sorte que la réintégration des ciments d'aluminates de calcium dans le référentiel ne sera plus possible après la publication des arrêtés ministériels relatifs à l'octroi de quotas à titre gratuit, privant d'utilité d'éventuels recours ultérieurs ; <br/>
              - il existe un doute sérieux quant à la légalité de la décision contestée, entachée d'une erreur de droit en ce qu'elle se fonde sur une position informelle de la Commission européenne qui est dépourvue de portée juridique et ne saurait lier la ministre ;<br/>
              - la ministre a entaché sa décision d'une erreur de qualification juridique en excluant les ciments d'aluminates de calcium des référentiels de " clinker " de ciment gris et blanc en dépit du règlement délégué n° 2019/331 du 19 décembre 2018 rattachant à ces référentiels tous les procédés liés à la production de " clinker " de ciment gris ou blanc ;<br/>
              - le changement de position sur le référentiel applicable méconnait les principes de confiance légitime et de sécurité juridique, dès lors qu'il ne correspond à aucun changement des textes, qu'il n'a été accompagné d'aucune mesure transitoire et qu'il lui a été notifié par courriel deux mois avant le début de la quatrième période du système d'échange de quotas d'émissions de gaz à effet de serre. <br/>
<br/>
              Par un mémoire en défense, enregistré le 20 mai 2021, la ministre de la transition écologique conclut au rejet de la requête. Elle soutient que la requête est prématurée dès lors que la ministre n'a encore pris aucune décision faisant grief, que la condition d'urgence n'est pas satisfaite et que les moyens soulevés ne sauraient faire naître un doute sérieux sur la légalité de la décision de ne pas inclure un produit dans le référentiel, qui relève en tout état de cause de la seule compétence de la Commission européenne. <br/>
<br/>
              Vu le nouveau mémoire, enregistré le 1er juin 2021, présenté par la société Imerys Aluminates, qui maintient ses conclusions et en précise la portée ; elle soutient que les nouvelles pièces produites les 28 et 31 mai 2021 relatives aux éléments d'information obtenus de la Commission européenne ne révèlent aucune prise de position des ministres, dans leurs échanges avec la Commission, en faveur du maintien des ciments d'aluminates de calcium dans les référentiels de " clinker " de ciment ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 2003/87/CE du Parlement européen et du Conseil du 13 octobre 2003 ;<br/>
              - le règlement délégué (UE) 2019/331 de la Commission du 19 décembre 2018 ; <br/>
              - le règlement d'exécution (UE) 2021/447 de la Commission du 12 mars 2021 ;<br/>
              - le code de l'environnement ;<br/>
              - l'ordonnance n° 2019-1034 du 9 octobre 2019 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la société Imerys Aluminates et, d'autre part, la ministre de la transition écologique ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 28 mai 2021, à 10 heures : <br/>
<br/>
              - Me Robillot, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la société Imerys Aluminates ;<br/>
<br/>
              - les représentants de la société Imerys Aluminates ; <br/>
<br/>
              - les représentants de la ministre de la transition écologique ; <br/>
<br/>
              à l'issue de laquelle le juge des référés a reporté la clôture de l'instruction au 1er juin 2021 à 12 heures. <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du même code : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              Sur le cadre juridique du litige :<br/>
<br/>
              2. Le système d'échange de quotas d'émission de gaz à effet de serre de l'Union européenne est organisé par la directive 2003/87/CE du Parlement européen et du Conseil du 13 octobre 2003 modifiée, transposée aux articles L. 229-5 et suivants du code de l'environnement. Pour l'application de ces dispositions, un quota d'émission constitue une unité de compte représentative de l'émission d'une tonne d'équivalent dioxyde de carbone. Aux termes de l'article L. 229-7 de ce code, dans sa rédaction issue de l'ordonnance du 9 octobre 2019 relative au système d'échange de quotas d'émissions de gaz à effet de serre : " I. La quantité de gaz à effet de serre émise au cours d'une année civile est calculée ou mesurée et exprimée en tonnes d'équivalent dioxyde de carbone. / II. A l'issue de chaque année civile, l'exploitant restitue à l'autorité administrative, sous peine des sanctions prévues à l'article L. 229-10, un nombre d'unités égal au total des émissions de gaz à effet de serre durant cette année civile de ses installations (...) ". Aux termes du I de l'article L. 229-15 du même code : " Des quotas d'émission de gaz à effet de serre sont délivrés gratuitement, sur demande, par l'autorité administrative aux exploitants des installations bénéficiant de l'autorisation mentionnée au premier alinéa de l'article L. 229-6 (...). / L'affectation a lieu au titre de périodes de cinq années civiles consécutives, la première commençant le 1er janvier 2021 ". Aux termes du III du même article : " (...) la quantité initiale de quotas pour les secteurs ou sous-secteurs exposés à un risque important de fuite de carbone est de 100 % de la quantité déterminée conformément aux mesures mentionnées au paragraphe 1 de l'article 10 bis de la directive 2003/87/ CE du 13 octobre 2003. (...) ". <br/>
<br/>
              3. Les mesures mentionnées au III de l'article L. 229-15 du code de l'environnement sont des actes délégués par lesquels la Commission européenne arrête " des règles pleinement harmonisées à l'échelle de l'Union relatives à l'allocation des quotas (...) ". En particulier, pour l'application du paragraphe 2 de l'article 10 bis de la directive 2003/87 du 13 octobre 2003 relatif à la détermination de référentiels pour l'allocation des quotas à titre gratuit, le règlement délégué (UE) 2019/331 de la Commission du 19 décembre 2018 prévoit 54 référentiels de produits, avec les valeurs de ces référentiels et les taux d'actualisation de ces valeurs pour la période courant de 2021 à 2030, de façon à garantir que les modalités d'allocation des quotas encouragent l'utilisation de techniques efficaces pour réduire les émissions de gaz à effet de serre.<br/>
<br/>
              4. S'agissant de la procédure d'instruction des demandes d'allocation à titre gratuit de quotas d'émission, il résulte, en premier lieu, de l'article 4 du règlement mentionné au point 3 que : " L'exploitant d'une installation remplissant les conditions d'allocation de quotas d'émission à titre gratuit (...) peut soumettre à l'autorité compétente une demande d'allocation à titre gratuit pour une période d'allocation. Cette demande est présentée avant le 30 mai 2019 pour la première période d'allocation (...) ". En deuxième lieu, l'article 11 de la directive 2003/87 du 13 octobre 2003 prévoit que " 1. Chaque Etat membre publie et présente à la Commission (...) la liste des installations (...) qui se trouvent sur son territoire, ainsi que les quotas gratuits alloués à chaque installation installée sur son territoire, calculés conformément aux règles visées à l'article 10 bis, paragraphe 1 et à l'article 10 quater. / La liste des installations couvertes par la présente directive pour la période de cinq ans débutant le 1er janvier 2021 est présentée le 30 septembre 2019 au plus tard (...) ". En troisième lieu, aux termes du paragraphe 3 de l'article 14 du règlement mentionné au point 3 : " Dès réception de la liste, la Commission examine l'inscription de chaque installation sur la liste ainsi que les données associées (...) ". Aux termes du paragraphe 4 du même article : " Si la Commission ne rejette pas l'inscription d'une installation sur cette liste, les données sont utilisées pour le calcul des valeurs révisées des référentiels visés à l'article 10 bis, paragraphe 2, de la directive 2003/87/CE ". En quatrième lieu, par une décision concernant les mesures nationales d'exécution, la Commission " notifi[e] les quantités annuelles provisoires de quotas alloués à titre gratuit pour la période d'allocation concernée ". Enfin, en vertu de l'article R. 229-8 du code de l'environnement et " après approbation par la Commission européenne de la liste des installations qui lui a été notifiée ", le ministre chargé de l'environnement édicte un arrêté fixant la liste des exploitants d'installations éligibles à l'affectation et la délivrance de quotas à titre gratuit.<br/>
<br/>
              Sur la demande en référé :<br/>
<br/>
              5. Il résulte de l'instruction que, par une demande du 30 mai 2019, la société Imerys Aluminates a sollicité l'allocation à titre gratuit de quotas d'émission de gaz à effet de serre pour la période 2021-2025, pour un volume calculé, conformément aux périodes précédentes, à partir du référentiel applicable au " clinker " de ciment gris pour la production de ciment d'aluminates de calcium de ses sites de Dunkerque et Fos-sur-Mer et au " clinker " de ciment blanc pour celle du site du Teil. Les services du ministère de la transition écologique ont tenu compte de cette demande pour établir la liste des installations sollicitant l'allocation à titre gratuit de quotas, qu'ils ont présentée en septembre 2019 à la Commission européenne. Toutefois, informée en octobre 2020 du refus de la Commission de maintenir la production de ciment d'aluminates de calcium dans ces référentiels, la société Imerys Aluminates a présenté, le 24 novembre 2020, une nouvelle demande de quotas correspondant à un volume calculé à partir d'un référentiel de repli, dit " avec émissions de procédé ". La société requérante fait valoir qu'elle n'a, ce faisant, nullement acquiescé à l'exclusion de la production de ciment d'aluminates des référentiels de " clinker " de ciment ni renoncé à obtenir l'affectation à titre gratuit d'un nombre de quotas correspondant à ces référentiels. <br/>
<br/>
              6. Par un courrier du 9 février 2021, la société Imerys Aluminates a demandé à la ministre de la transition écologique de maintenir la production de ciment d'aluminates de calcium de ses sites de Dunkerque et Fos-sur-Mer dans le référentiel de " clinker " de ciment gris et celle de son site du Teil dans le référentiel de " clinker " de ciment blanc. Le silence gardé par la ministre ayant fait naître une décision implicite de rejet de sa demande, la société requérante demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de cette décision et d'enjoindre à la ministre de la transition écologique d'inclure les ciments d'aluminates de calcium dans ces référentiels. Dans ses dernières écritures, la société requérante précise que sa demande tend ainsi à ce qu'il soit enjoint à la ministre de la transition écologique de faire valoir auprès de la Commission européenne que les ciments d'aluminates de calcium doivent être inclus dans les référentiels de " clinker " de ciment pour l'évaluation du nombre de quotas à lui allouer. <br/>
<br/>
              7. Toutefois, ainsi qu'il a été dit au point 5, la ministre de la transition écologique a transmis à la Commission européenne, dans les délais prévus par les textes mentionnés au point 4, une liste incluant la production de ciment d'aluminates de calcium dans les référentiels de " clinker " de ciment. La Commission ayant refusé de valider cette liste en ce qu'elle incluait dans ces référentiels la production de ciment d'aluminates de calcium, il résulte de l'instruction qu'une liste révisée comportant les données modifiées conformément à la nouvelle demande de la société Imerys Aluminates du 24 novembre 2020 a été transmise par la ministre à la Commission qui a édicté depuis, le 12 mars 2021, un règlement d'exécution déterminant les valeurs révisées des référentiels pour l'allocation de quotas à titre gratuit pour la période 2021-2025, dont il ressort que la valeur des référentiels de " clinker " de ciment gris et blanc a d'ores et déjà été arrêtée par elle en tenant compte de données n'incluant plus la production de ciment d'aluminates de calcium. Dans ces conditions, en l'absence de possibilité de soumettre à ce stade de la procédure une nouvelle demande d'allocation de quotas fondée sur la réintégration des installations de la société requérante dans les référentiels de " clinker " de ciment, la requête présentée par la société Imerys Aluminates ne peut qu'être rejetée, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de la société Imerys Aluminates est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à la société Imerys Aluminates et à la ministre de la transition écologique. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
