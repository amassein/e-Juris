<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032528107</ID>
<ANCIEN_ID>JG_L_2016_05_000000396323</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/52/81/CETATEXT000032528107.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 11/05/2016, 396323, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396323</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD</AVOCATS>
<RAPPORTEUR>Mme Anne Egerszegi</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:396323.20160511</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance n° 1521399 du 6 janvier 2016, le juge des référés du tribunal administratif de Paris a suspendu, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, l'exécution de l'arrêté du 18 décembre 2015 par lequel le préfet de la région d'Ile-de-France a fixé le nombre et la répartition des sièges au sein du conseil communautaire de la communauté d'agglomération " Roissy Pays de France ".<br/>
<br/>
              Par un pourvoi enregistré le 22 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les demandes de suspension des communes de Dammartin-en-Goële et de Garges-Lès-Gonesse.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 2014-58 du 27 janvier 2014 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Egerszegi, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, avocat de la commune de Garges-Lès-Gonesse ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Par un arrêté du 18 décembre 2015, le préfet de la région d'Ile-de-France a fixé le nombre et la répartition des sièges au sein du conseil communautaire de la communauté d'agglomération " Roissy Pays de France ", issue de la fusion des communautés d'agglomération " Roissy Porte de France " et " Val de France ", et de l'extension de périmètre à dix-sept communes de la communauté de communes " Plaines et Monts de France ". Les communes de Dammartin-en-Goële et de Garges-Lès-Gonesse ont alors demandé au juge des référés du tribunal administratif de Paris de suspendre, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, l'exécution de cet arrêté.  Elles ont, à l'appui de leur demande de suspension, soulevé une question prioritaire de constitutionnalité relative aux dispositions du c du 1° de l'article L. 5211-6-2 du code général des collectivités territoriales. Par une ordonnance n° 1521399 du 6 janvier 2016, le juge des référés du tribunal administratif de Paris a, d'une part, suspendu l'exécution de l'arrêté litigieux au seul motif que le moyen tiré de l'inconstitutionnalité des dispositions du c du 1° de l'article L. 5211-6-2 du code général des collectivités territoriales devait être regardé, en l'état de l'instruction, comme propre à créer un doute sérieux quant à la légalité de l'arrêté attaqué et, d'autre part, décidé de ne pas transmettre la question prioritaire soulevée devant lui par ces communes au Conseil d'Etat au motif que le Conseil d'Etat était déjà saisi d'une question mettant en cause, par les mêmes motifs, ces dispositions.<br/>
<br/>
              2. En premier lieu, par décision du 5 mars 2015 du directeur des libertés publiques et des affaires juridiques du ministère de l'intérieur portant délégation de signature et publiée au Journal officiel du 6 mars 2015, M. A...B..., signataire du pourvoi, a reçu délégation pour signer les recours devant les juridictions. Par suite, la fin de non-recevoir soulevée par les communes doit être écartée. <br/>
<br/>
              3. En second lieu, l'arrêté du préfet de la région d'Ile-de-France du 18 décembre 2015, qui a pour objet de fixer, selon les modalités prévues aux II et III de l'article L. 5211-6-1 du code général des collectivités territoriales, le nombre et la répartition des sièges au sein du conseil communautaire de la communauté d'agglomération " Roissy Pays de France " à compter du 1er janvier 2016, a été pris en application des dispositions du VI de l'article 11 de la loi du 27 janvier 2014 de modernisation de l'action publique territoriale et d'affirmation des métropoles. Les dispositions du c du 1° de l'article L. 5211-6-2 du code général de collectivités territoriales, qui ont pour objet de fixer les modalités par lesquelles les communes désignent leurs conseillers communautaires au sein de l'organe délibérant de l'établissement public de coopération intercommunale qu'elles rejoignent, lorsque le nombre de sièges qui leur est attribué est inférieur au nombre de conseillers communautaires élus à l'occasion du précédent renouvellement général des conseils municipaux, ne sont par conséquent  pas applicables au litige soulevé par les communes de Dammartin-en-Goële et de Garges-Lès-Gonesse. Il suit de là qu'en jugeant qu'était, en l'état de l'instruction, propre à créer un doute sérieux quant à la légalité de la l'arrêté attaqué, le moyen tiré de l'absence de conformité à la Constitution des dispositions du c du 1° de l'article L. 5211-6-2 du code général des collectivités territoriales, le juge des référés du tribunal administratif de Paris a commis une erreur de droit. Par suite, et sans qu'il soit besoin de statuer sur les autres moyens du pourvoi, l'ordonnance attaquée doit être annulée.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de statuer sur les demandes de suspension en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              5. En premier  lieu, ainsi qu'il a été dit au point 3, les dispositions de l'article L. 5211-6-2 du code général des collectivités territoriales ne sont pas applicables au litige soulevé par les communes de Dammartin-en-Goële et de Garges-Lès-Gonesse. Par suite, le moyen tiré de l'absence de conformité à la Constitution des dispositions du c du 1° de l'article L. 5211-6-2 du code général des collectivités territoriales n'est pas, en l'état de l'instruction, de nature à créer un doute sérieux quant à la légalité de l'arrêté attaqué.<br/>
<br/>
              6. En second lieu, en dehors des cas et conditions où il est saisi sur le fondement de l'article 61-1 de la Constitution, il n'appartient pas au Conseil d'Etat, statuant au contentieux, de se prononcer sur un moyen tiré de la non-conformité de la loi à une norme de valeur constitutionnelle. En l'espèce, le moyen soulevé par les requérantes tiré de ce que l'arrêté litigieux, en ce qu'il conduit à la cessation anticipée du mandat des conseillers communautaires, porterait atteinte au libre exercice du mandat des élus locaux ainsi qu'au droit de suffrage et à la sincérité du scrutin, tend à l'appréciation de la conformité à la Constitution des dispositions du VI de l'article 11 de la loi du 27 janvier 2014. Par suite, ce moyen ne peut qu'être écarté.<br/>
<br/>
              7. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur la condition d'urgence, que les requêtes des communes de Dammartin-en-Goële et de Garges-Lès-Gonesse doivent être rejetées. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 6 janvier 2016 du juge des référés du tribunal administratif de Paris est annulée.<br/>
Article 2 : Les demandes présentées par les communes de Dammartin-en-Goële et de Garges-Lès-Gonesse devant le tribunal administratif de Paris sont rejetées.<br/>
Article 3 : La présente décision sera notifiée aux communes de Dammartin-en-Goële et de Garges-Lès-Gonesse et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
