<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042504505</ID>
<ANCIEN_ID>JG_L_2020_11_000000442136</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/50/45/CETATEXT000042504505.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 05/11/2020, 442136, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>442136</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GOUZ-FITOUSSI</AVOCATS>
<RAPPORTEUR>M. François Charmont</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:442136.20201105</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              M. A... B... a demandé au juge des référés du tribunal administratif de Paris, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre la décision du 31 janvier 2020 par laquelle l'Agence nationale des titres sécurisés (ANTS) a refusé de lui délivrer le permis de conduire de catégorie " B " et la décision implicite de rejet de son recours gracieux dirigé contre cette décision, et d'enjoindre à l'ANTS et au ministre de l'intérieur de lui délivrer matériellement ce titre de conduire, sous astreinte de 150 euros par jour de retard. Par une ordonnance n° 2008860 du 10 juillet 2020, le juge des référés a suspendu l'exécution de la décision du 31 janvier 2020 et la décision implicite de rejet du recours gracieux présenté contre cette décision par M. B..., enjoint à l'ANTS d'éditer le permis de conduire de catégorie " B " de M. B... dans un délai de dix jours, et rejeté le surplus de ses conclusions.<br/>
<br/>
              I. Par un pourvoi et un mémoire complémentaire, enregistrés les 24 juillet et 4 août 2020 au secrétariat du contentieux du Conseil d'Etat sous le n° 442136, l'ANTS demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de M. B... ;<br/>
<br/>
              3°) de mettre à la charge de M. B... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              II. Par une requête et une requête rectificative, enregistrées les 5 août et 3 septembre 2020 au secrétariat du contentieux du Conseil d'Etat sous le n° 442480, l'ANTS demande en outre au Conseil d'Etat d'ordonner qu'il soit sursis à l'exécution de cette même ordonnance.<br/>
<br/>
              Il soutient que : <br/>
              - l'exécution de l'ordonnance entraînera des conséquences difficilement réparables ; <br/>
              - les moyens tirés de l'insuffisance de motivation de la décision du juge des référés et de l'erreur de droit qu'il a commise en lui enjoignant de délivrer le permis de conduire présentent un caractère sérieux.<br/>
<br/>
<br/>
<br/>
		..............................................................................<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de la route ;<br/>
              - le décret n° 2007-240 du 22 février 2007 ;<br/>
              - le décret n° 2007-255 du 27 février 2007 ;<br/>
              - l'arrêté du 20 avril 2012 fixant les conditions d'établissement, de délivrance et de validité du permis de conduire modifié ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Charmont, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gouz-Fitoussi, avocat de l'Agence Nationale Des Titres Sécurisés.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Le pourvoi et la requête à fin de sursis à exécution présentés par l'ANTS sont dirigés contre la même ordonnance du juge des référés du tribunal administratif de Paris. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              Sur le pourvoi dirigé contre l'ordonnance attaquée : <br/>
<br/>
              2. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              3. Pour demander l'annulation de l'ordonnance du juge des référés du tribunal administratif de Paris qu'elle attaque, l'ANTS soutient qu'elle est entachée :<br/>
              - d'insuffisance de motivation en ce qu'elle omet de répondre au moyen tiré de ce qu'il ne lui appartient pas de délivrer une autorisation de conduire ;<br/>
              - d'erreur de droit en ce qu'elle lui enjoint d'éditer un titre de conduite sans rechercher si l'intéressé disposait d'une autorisation de conduire délivrée par l'autorité préfectorale.<br/>
<br/>
              4. Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
              Sur la requête à fin de sursis à exécution :<br/>
<br/>
              5. Il résulte de ce qui précède que le pourvoi de l'ANTS contre l'ordonnance attaquée n'est pas admis. Par suite, ses conclusions à fin de sursis à l'exécution de cette ordonnance présentée sur le fondement de l'article R. 821-5 du code de justice administrative sont devenues sans objet.<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Le pourvoi de l'Agence nationale des titres sécurisés n'est pas admis.<br/>
<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions à fin de sursis à exécution. <br/>
<br/>
Article 3 : La présente décision sera notifiée l'Agence nationale des titres sécurisés.<br/>
Copie en sera adressée à M. A... B... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
