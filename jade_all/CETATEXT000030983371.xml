<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030983371</ID>
<ANCIEN_ID>JG_L_2015_07_000000374804</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/98/33/CETATEXT000030983371.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème SSJS, 31/07/2015, 374804, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374804</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Romain Godet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:374804.20150731</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C...A...B...a demandé à la Cour nationale du droit d'asile d'annuler la décision du 30 octobre 2012 par laquelle le directeur général de l'Office français de protection des réfugiés et apatrides a rejeté sa demande d'asile. La Cour nationale du droit d'asile a rejeté son recours par une décision n° 12036206 du 12 juin 2013.<br/>
<br/>
              Par un pourvoi et un mémoire complémentaire, enregistrés les 21 janvier et 22 avril 2014 au secrétariat du contentieux du Conseil d'Etat, Mme A...B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision n°12036206 du 12 juin 2013 de la Cour nationale du droit d'asile ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre des dispositions combinées des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n°91-647 du 10 juillet 1991 et le décret n°91-1266 du 19 décembre 1991 ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Romain Godet, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, Trichet, avocat de Mme A...B...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 731-2 du code de l'entrée et du séjour des étrangers et du droit d'asile prévoit que la Cour nationale du droit d'asile statue sur les recours formés contre les décisions de l'Office français de protection des réfugiés et apatrides prises notamment en matière de reconnaissance de la qualité de réfugié. La deuxième phrase du premier alinéa de cet article dispose : " A peine d'irrecevabilité, ces recours doivent être exercés dans le délai d'un mois à compter de la notification de la décision de l'office " ;<br/>
<br/>
              2. Pour rejeter le recours formé par Mme A...B...contre la décision de l'Office français de protection des réfugiés et apatrides du 30 octobre 2012 lui refusant la qualité de réfugiée, enregistré auprès d'elle le 28 décembre 2012, la Cour nationale du droit d'asile a jugé que cette décision lui avait été régulièrement notifiée le 20 novembre 2012 et que son recours était donc tardif et, par suite, irrecevable ;<br/>
<br/>
              3. Il ressort cependant des pièces du dossier soumis à la Cour nationale du droit d'asile que le pli contenant la décision de l'Office français de protection des réfugiés et apatrides du 30 octobre 2012 a été présenté au domicile de Mme A...B...le 19 novembre 2012 puis lui a été distribué à une date inconnue. Dans son recours adressé à la Cour nationale du droit d'asile comme dans un courrier adressé le 5 décembre 2012 au directeur général de l'Office, par lequel Mme A...B...demandait à être mise en relation avec deux personnes qu'elle présentait comme étant sa mère et sa soeur, l'intéressée a indiqué avoir reçu le 28 novembre 2012 la notification de la décision de l'office du 30 octobre 2012. L'avis de réception du pli contenant cette décision a également été réexpédiée à l'Office à cette même date. Dans ces circonstances, c'est à la date du 28 novembre 2012 que le délai d'un mois imparti à Mme A...B...pour exercer un recours contre la décision de l'Office français de protection des réfugiés et apatrides doit être regardé comme ayant commencé à courir ;<br/>
<br/>
              4. Il résulte de ce qui précède que la Cour nationale du droit d'asile a dénaturé les pièces du dossier qui lui était soumis en jugeant que le recours qui lui était soumis était tardif. Sa décision doit donc être annulée ;<br/>
<br/>
              5. Mme A...B...a obtenu le bénéfice de l'aide juridictionnelle totale ; par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Toutefois, ces dispositions font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas partie à la présente instance, la somme demandée par la SCP Delaporte-Briard-Trichet, avocat de Mme A...B..., au titre des frais exposés et non compris dans les dépens.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision n°12036206 du 12 juin 2013 de la Cour nationale du droit d'asile est annulée.<br/>
Article 2 : L'affaire est renvoyée devant la Cour nationale du droit d'asile.<br/>
Article 3 : Les conclusions présentées par la SCP Delaporte-Briard-Trichet, avocat de Mme A...B..., au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à Madame C...A...B...et à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
