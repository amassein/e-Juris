<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034833624</ID>
<ANCIEN_ID>JG_L_2017_05_000000404849</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/83/36/CETATEXT000034833624.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 31/05/2017, 404849, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-05-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404849</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER ; SCP BOULLEZ</AVOCATS>
<RAPPORTEUR>M. François Monteagle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:404849.20170531</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de La Réunion d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution des arrêtés du 9 septembre 2016 par lesquels le président de la communauté d'agglomération du sud (CASUD) l'a placé rétroactivement en congé maladie ordinaire à demi-traitement à compter du 1er juin 2016, puis sans traitement à compter du 20 juillet 2016, et d'enjoindre à la CASUD de lui verser son complet traitement à compter du 1er juin 2016. Par une ordonnance n° 1601064 du 19 octobre 2016, le juge des référés du tribunal administratif de La Réunion a fait droit à sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 3 et 18 novembre 2016 au secrétariat du contentieux du Conseil d'Etat, la CASUD demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au titre de la procédure de référé engagée, de rejeter la demande de M.A... ;<br/>
<br/>
              3°) de mettre à la charge de M. A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 87-602 du 30 juillet 1987 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Monteagle, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de la communauté d'agglomération du Sud et à la SCP Boullez, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que M.A..., ingénieur territorial principal au sein de la communauté d'agglomération du sud (CASUD), a été victime le 12 mars 2011 d'un accident reconnu comme accident de service, justifiant son placement en congé maladie du mois de mai 2011 au mois de mai 2014. Bénéficiant d'un mi-temps thérapeutique jusqu'au 12 novembre 2014, il a ensuite repris ses fonctions à temps plein. Il a fait l'objet, à compter du 27 juillet 2015, de quatorze arrêts de travail successifs. La CASUD lui a versé l'intégralité de son traitement jusqu'au 31 mai 2016. Par deux arrêtés du 9 septembre 2016, le président de la CASUD a, d'une part, placé rétroactivement l'intéressé en congé de maladie ordinaire à demi-traitement à compter du 1er juin 2016, puis sans traitement à compter du 1er juillet 2016, sur le fondement des dispositions de l'article 57 de la loi du 26janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale et a, d'autre part, rejeté sa demande d'imputabilité de la maladie au service. M. A...a saisi le juge des référés du tribunal administratif de La Réunion qui, par une ordonnance en date du 19 octobre 2016, a fait droit à sa demande de suspension de ces deux arrêtés et a enjoint à la CASUD de lui verser l'intégralité de ses traitements à compter du 1er juin 2016. La CASUD se pourvoit en cassation contre cette ordonnance.<br/>
<br/>
              Sur les conclusions aux fins de suspension : <br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              3. Il résulte des dispositions des 2ème et 3ème alinéas du 2° de l'article 57 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale et de l'article 16 du décret du 30 juillet 1987 pris pour son application et relatif à l'organisation des comités médicaux, aux conditions d'aptitude physique et au régime des congés de maladie des fonctionnaires territoriaux, que la commission de réforme est obligatoirement consultée lorsque le fonctionnaire demande à bénéficier du régime des accidents de service et maladies imputables au service, sauf lorsque l'imputabilité au service de l'accident ou de la maladie est reconnue par l'administration<br/>
<br/>
              4. Après avoir jugé, par une appréciation souveraine qui n'est pas arguée de dénaturation, qu'à la date des arrêtés litigieux, l'employeur de M. A...ne pouvait ignorer que ce dernier invoquait une reconnaissance d'imputabilité au service à l'égard de l'ensemble de ses arrêts de travail depuis juillet 2015, le juge des référés n'a pas commis d'erreur de droit en estimant qu'était de nature à créer un doute sérieux quant à la légalité de ces arrêtés, qui placent M. A...en congé de maladie ordinaire en mentionnant, dans leurs visas, que l'intéressé " ne relève d'aucune maladie contractée dans le service", le moyen tiré de ce qu'ils sont irrégulièrement intervenus en l'absence de saisine de la commission de réforme.<br/>
<br/>
              5. La CASUD n'est, par suite, pas fondée à demander l'annulation de l'article 1er de l'ordonnance attaquée qui a prononcé la suspension demandée par M.A....<br/>
<br/>
              Sur les conclusions aux fins d'injonction :<br/>
<br/>
              6. Aux termes du second alinéa de l'article L. 521-1 du code de justice administrative : " Lorsque la suspension est prononcée, il est statué sur la requête en annulation ou en réformation de la décision dans les meilleurs délais. La suspension prend fin au plus tard lorsqu'il est statué sur la requête en annulation ou en réformation de la décision ". Il résulte de ces dispositions que la suspension de l'exécution d'une décision administrative présente le caractère d'une mesure provisoire. Ainsi, elle n'emporte pas les mêmes conséquences qu'une annulation prononcée par le juge administratif, laquelle a une portée rétroactive. En particulier, elle ne prend effet qu'à la date à laquelle la décision juridictionnelle ordonnant la suspension est notifiée à l'auteur de la décision administrative contestée.<br/>
<br/>
              7. La mesure de suspension visée au point 5 implique, d'une part, que l'administration statue à nouveau sur la demande de reconnaissance de l'imputabilité au service de la maladie de M. A...et, d'autre part, qu'elle lui rétablisse provisoirement son plein traitement. Le juge des référés n'a, par suite, pas commis d'erreur de droit en enjoignant que soit, à titre provisoire, versé à M. A...son plein traitement. En revanche, il ne pouvait sans méconnaitre son office, prévoir que cette injonction prendrait effet, d'une part, dès le 1er juin 2016 et non à compter de la notification de son ordonnance, et d'autre part, jusqu'à ce qu'il soit statué sur la requête au fond et non jusqu'à cette date ou, si elle est intervient précédemment, à celle à laquelle l'administration aura régulièrement réexaminé sa demande de reconnaissance d'imputabilité au service de sa maladie. La CASUD est, par suite, fondée à demander, dans cette mesure l'annulation de l'article 2 de l'ordonnance attaquée.<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              8. Ces dispositions font obstacle à ce que la CASUD, qui n'est pas, dans la présente instance, la partie perdante, soit condamnée à verser à M. A...la somme qu'il demande au titre des frais exposés par elle et non compris dans les dépens. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la CASUD au même titre.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'article 2 de l'ordonnance du juge des référés du tribunal administratif de La Réunion en date du 19 octobre 2016 est annulé en tant qu'il n'a pas limité l'injonction adressée à l'administration de rétablir le plein traitement à M. A...à titre provisoire, d'une part, à compter de la date de notification de l'ordonnance, et d'autre part, jusqu'à la date à laquelle il sera statué sur la requête au fond ou, si elle est intervient avant, à la date à laquelle l'administration aura régulièrement réexaminé la demande de l'intéressé tendant à la reconnaissance d'imputabilité au service de sa maladie.<br/>
<br/>
Article 2 : Le surplus des conclusions du pourvoi de la communauté d'agglomération du sud ainsi que les conclusions présentées par M. A...en application de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la communauté d'agglomération du sud et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
