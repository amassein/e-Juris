<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043178816</ID>
<ANCIEN_ID>JG_L_2021_02_000000445072</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/17/88/CETATEXT000043178816.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 23/02/2021, 445072, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445072</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAMARRE, JEHANNIN</AVOCATS>
<RAPPORTEUR>M. Edouard Solier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:445072.20210223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A... B... a demandé au tribunal administratif de Nancy de suspendre, sur le fondement de l'article L. 521-1 du code de justice administrative, l'exécution de l'arrêté du 30 juin 2020 par lequel le ministre de l'éducation nationale et de la jeunesse lui a infligé la sanction de la révocation. Par une ordonnance n° 2002138 du 18 septembre 2020, le juge des référés a suspendu l'exécution de cet arrêté.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire enregistrés les 5 octobre et 8 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'éducation nationale, de la jeunesse et des sports demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de Mme B....<br/>
<br/>
              Le ministre de l'éducation nationale, de la jeunesse et des sports soutient que l'ordonnance attaquée est entachée :<br/>
              - d'insuffisance de motivation et d'erreur de droit, en ce qu'elle se borne à relever, pour estimer que la condition d'urgence est remplie, que l'arrêté litigieux préjudicie de manière suffisamment grave et immédiate à la situation financière et professionnelle de Mme B... sans répondre à son argumentation tirée de ce que, d'une part, la réintégration de l'intéressée risquait de compromettre le bon fonctionnement du service public et d'autre part, que Mme B... avait manqué de diligence en saisissant tardivement le juge des référés ;<br/>
              - d'erreur de droit et de dénaturation des pièces du dossier en ce que, pour caractériser l'urgence à suspendre l'exécution de la décision contestée, elle retient que la révocation de Mme B... préjudicie de manière grave et immédiate à sa situation financière alors que l'intéressée bénéficie de l'allocation de retour à l'emploi et que son conjoint contribue aux dépenses communes ;<br/>
              - d'erreur de droit et de dénaturation des pièces du dossier en ce qu'elle estime que le moyen tiré du caractère disproportionné de la sanction par rapport aux faits reprochés à Mme B... est de nature à faire naître un doute sérieux sur la légalité de l'arrêté du 30 juin 2020.<br/>
<br/>
              Par un mémoire en défense, enregistré le 19 novembre 2020, Mme B... conclut au rejet du pourvoi et à ce que la somme de 4 000 euros soit mise à la charge de l'Etat au titre de l'article L. 761-1 du code de justice administrative. Elle soutient que les moyens soulevés par le ministre de l'éducation nationale, de la jeunesse et des sports ne sont pas fondés.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ;<br/>
              - la loi n° 84-16 du 11 janvier 1984, notamment son article 66 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Edouard Solier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Delamarre, Jéhannin, avocat de Mme B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Nancy que, par un arrêté du 30 juin 2020, le ministre de l'éducation nationale et de la jeunesse a prononcé à l'encontre de Mme B..., personnel de direction d'établissement d'enseignement de classe normale, affectée en qualité de principale adjointe au collège René Cassin d'Eloyes (Vosges) depuis le 1er septembre 2014, la sanction de la révocation. Par une ordonnance du 18 septembre 2020, le juge des référés, saisi par Mme B... sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, a suspendu l'exécution de cette décision. Le ministre de l'éducation nationale, de la jeunesse et des sports se pourvoit en cassation contre cette ordonnance. <br/>
<br/>
              2. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision (...) ".<br/>
<br/>
              3. L'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. Il lui appartient également, l'urgence s'appréciant objectivement et compte tenu de l'ensemble des circonstances de chaque espèce, de faire apparaître dans sa décision tous les éléments qui, eu égard notamment à l'argumentation des parties, l'ont conduit à considérer que la suspension demandée revêtait un caractère d'urgence.<br/>
<br/>
              4. Il ressort des termes mêmes de l'ordonnance attaquée que pour estimer remplie la condition d'urgence, le juge des référés du tribunal administratif de Nancy a seulement relevé que l'arrêté litigieux prive Mme B... de ses revenus professionnels et que l'allocation de retour à l'emploi qu'elle perçoit et les faibles revenus de son conjoint sont insuffisants pour assumer les charges du foyer, sans répondre à l'argumentation du ministre en défense, qui n'était pas inopérante, relative aux troubles que la réintégration de l'intéressée occasionnerait dans le bon fonctionnement de l'établissement. Son ordonnance est ainsi entachée d'insuffisance de motivation et d'erreur de droit et doit, dès lors, être annulée.<br/>
<br/>
              5. Il y a eu lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée par Mme B..., en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              6. D'une part, il résulte de l'instruction que la décision prononçant la révocation de Mme B... a pour effet de priver celle-ci de son traitement et porte à sa situation financière une atteinte grave et immédiate, que ne suffisent pas à compenser l'allocation de retour à l'emploi qu'elle perçoit et les revenus de faible montant, au demeurant irréguliers, de son conjoint. En outre, contrairement à ce que soutient en défense le ministre chargé de l'éducation nationale, Mme B... n'a fait preuve d'aucun manque de diligence en saisissant le juge des référés. Enfin, si le ministre fait valoir que l'intérêt du service public de l'éducation s'oppose à ce que l'intéressée reprenne ses fonctions de principale adjointe au sein du collège René Cassin d'Eloyes, l'administration dispose d'un éventail de mesures autres que la révocation pour atteindre cet objectif. Dans ces conditions, la condition d'urgence fixée par l'article L. 521-1 du code de justice doit être regardée comme remplie.<br/>
<br/>
              7. D'autre part, en vertu de l'article 66 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, les sanctions susceptibles d'être infligées aux fonctionnaires de l'Etat sont réparties en quatre groupes : relèvent du premier groupe les sanction de l'avertissement et du blâme, du deuxième groupe celles de la radiation du tableau d'avancement, de l'abaissement d'échelon, de l'exclusion temporaire de fonctions pour une durée maximale de quinze jours et du déplacement d'office, du troisième groupe celles de la rétrogradation et de l'exclusion temporaire de fonctions pour une durée de trois mois à deux ans et, enfin, du quatrième groupe celles de la mise à la retraite d'office et de la révocation. Il résulte de l'instruction qu'il est reproché à Mme B... un comportement agressif à l'égard des chefs d'établissement successifs sous la responsabilité desquels elle a travaillé ainsi que des agissements répétés à caractère vexatoire à l'égard de personnels placés sous son autorité. Alors que l'autorité disciplinaire disposait d'un éventail de sanctions de nature et de portée différentes, le moyen tiré de ce que la sanction de révocation qui lui a été infligée est disproportionnée par rapport aux faits reprochés est, en l'état de l'instruction, de nature à faire naître un doute sérieux sur la légalité de la décision attaquée.<br/>
<br/>
              8. Il résulte de tout ce qui précède que, sans qu'il soit besoin de se prononcer sur les autres moyens de sa demande, Mme B... est fondée à demander la suspension de l'exécution de l'arrêté du ministre de l'éducation nationale et de la jeunesse du 30 juin 2020. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à Mme B... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 18 septembre 2020 du tribunal administratif de Nancy est annulée.<br/>
Article 2 : L'exécution de l'arrêté du ministre de l'éducation nationale et de la jeunesse du 30 juin 2020 est suspendue.<br/>
Article 3 : L'Etat versera à Mme B... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée au ministre de l'éducation nationale, de la jeunesse et des sports et à Mme A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
