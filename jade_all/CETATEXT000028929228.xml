<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028929228</ID>
<ANCIEN_ID>JG_L_2014_05_000000369569</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/92/92/CETATEXT000028929228.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème SSJS, 07/05/2014, 369569, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-05-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369569</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>FOUSSARD ; SCP POTIER DE LA VARDE, BUK LAMENT ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Eric Aubry</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne Von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:369569.20140507</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 21 juin et 5 juillet 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B...A...et Mme C...E..., demeurant ...; M. A...et Mme E...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1302731 du 7 juin 2013 par laquelle le juge des référés du tribunal administratif de Grenoble, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a suspendu l'exécution de l'arrêté du 22 février 2013 par lequel le maire de Saint-Bon-Tarentaise leur a délivré un permis de construire ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de suspension présentée par M. et Mme D...et par l'association de défense des intérêts du quartier des Chenus contre l'arrêté du 5 juillet 2013 ;<br/>
<br/>
              3°) de mettre à la charge de M. et Mme D...et de l'association de défense des intérêts du quartier des Chenus la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Eric Aubry, conseiller d'Etat, <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Foussard, avocat de M. A...et de MmeE..., à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. et Mme D...et à la SCP Potier de la Varde, Buk Lament, avocat de la commune de Saint-Bon-Tarentaise ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision (...)" ;<br/>
<br/>
              2. Considérant  qu'il résulte des pièces du dossier soumis au juge des référés que, par un arrêté du 22 février 2013, la SCI L'Isard, aux droits de laquelle sont venus M. A...et MmeE..., a obtenu du maire de Saint-Bon-Tarentaise un permis de construire en vue de la construction d'un immeuble d'habitation ; que, par une ordonnance du 7 juin 2013, le juge des référés du tribunal administratif de Grenoble a, à la demande de M. et Mme D...et de l'association de défense des intérêts du quartiers des Chenus, suspendu l'exécution de ce permis jusqu'à ce qu'il soit statué au fond sur sa légalité ; que M. A...et Mme E...se pourvoient en cassation contre cette ordonnance ;<br/>
<br/>
              3. Considérant, en premier lieu, que le moyen tiré de l'absence de signature de l'ordonnance attaquée par le juge des référés du tribunal administratif de Grenoble manque en fait ;<br/>
<br/>
              4. Considérant, en deuxième lieu que, contrairement à ce qui est soutenu, le juge des référés s'est prononcé sur la fin de non-recevoir soulevée devant lui et tirée du défaut d'intérêt à agir des demandeurs ;<br/>
<br/>
              5. Considérant, en troisième lieu, qu'en retenant que les moyens tirés de l'incompétence de l'auteur du permis de construire, de la méconnaissance par le permis de construire de l'article R. 431-10 du code de l'urbanisme, de l'article UC 7 et de l'article UC 9 du règlement du plan local d'urbanisme étaient propres, en l'état de l'instruction, à créer un doute sérieux sur la légalité de l'arrêté, le juge des référés s'est livré, sans erreur de droit ni dénaturation, à une appréciation souveraine qui n'est pas susceptible d'être discutée devant le juge de cassation ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que M. A...et Mme E...ne sont pas fondés à demander l'annulation de l'ordonnance attaquée ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soient mises à ce titre, à la charge de M. et MmeD..., qui ne sont pas, dans la présente instance, les parties perdantes la somme que demandent M. A...et MmeE... ; qu'il y a lieu en revanche, dans les circonstances de l'espèce, de mettre à la charge de M. A...et Mme E...une somme globale de 3 000 euros à verser à M. et MmeD..., au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...et Mme E...est rejeté.<br/>
Article 2 : M. A...et Mme E...verseront à M. et Mme D...une somme globale de 3.000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à M. B...A...et Mme C...E..., à la commune de Saint-Bon-Tarentaise, à M. et Mme D...et à l'association de défense des intérêts du quartier des Chenus.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
