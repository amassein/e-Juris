<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036945758</ID>
<ANCIEN_ID>JG_L_2018_05_000000410972</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/94/57/CETATEXT000036945758.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème chambres réunies, 25/05/2018, 410972</TITRE>
<DATE_DEC>2018-05-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410972</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET</AVOCATS>
<RAPPORTEUR>M. Marc Firoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:410972.20180525</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a saisi le tribunal administratif de Paris d'une demande tendant à l'annulation de la décision du 21 août 2014 du ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche rejetant sa demande tendant à la révision de son classement dans le corps des ingénieurs d'études du ministère chargé de l'enseignement supérieur. Par un jugement n° 1432321 du 28 octobre 2015, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15PA04828 du 28 mars 2017, la cour administrative d'appel de Paris a rejeté l'appel formé par M. B...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 29 mai, 29 août 2017 et 11 février 2018 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 600 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le décret n° 85-986 du 16 septembre 1985 ;<br/>
              - le décret n° 85-1534 du 31 décembre 1985 ;<br/>
              - le décret n° 89-750 du 18 octobre 1989 ;<br/>
              - le décret n° 2010-309 du 22 mars 2010 ;<br/>
              - l'arrêté du 12 août 1986 relatif à l'échelonnement indiciaire des corps d'ingénieurs et de personnels techniques et administratifs de recherche et de formation du ministère de l'éducation nationale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Firoud, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Nervo, Poupet, avocat de M.B....<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.B..., ingénieur d'études et de fabrications du ministère de la défense, a été placé, à compter du 1er juin 2014, en position de détachement dans le corps des ingénieurs d'études du ministère chargé de l'enseignement supérieur ; que, par un arrêté du 12 juin 2014, le ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche a prononcé son classement au 7ème échelon du grade d'ingénieur d'études de 2ème classe ; que, par un courrier du 23 juin 2014, M. B...a sollicité du ministre une révision de ce classement et demandé à être classé au 1er échelon du grade d'ingénieur d'études de 1ère classe ; que, par une décision du 21 août 2014, le ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche a refusé de faire droit à cette demande ; que M. B...a demandé au tribunal administratif de Paris d'annuler cette décision et de condamner l'Etat à lui verser une somme de 3 500,51 euros en réparation de la perte de traitement qu'il estimait avoir subie du fait de ce classement ainsi qu'une somme de 4 405,92 euros en réparation de la perte de primes ; que, par un jugement du 28 octobre 2015, le tribunal administratif de Paris a rejeté sa demande ; que, par un arrêt du 28 mars 2017, contre lequel M. B...se pourvoit en cassation, la cour administrative d'appel de Paris a rejeté sa requête ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 26-1 du décret du 16 septembre 1985 relatif au régime particulier de certaines positions des fonctionnaires de l'Etat, à la mise à disposition, à l'intégration et à la cessation définitive de fonctions : " Lorsque le détachement est prononcé dans un corps de fonctionnaires de l'Etat, il est prononcé à équivalence de grade et à l'échelon comportant un indice égal ou, à défaut, immédiatement supérieur à celui dont l'intéressé bénéficie dans son grade d'origine. / Lorsque le corps de détachement ne dispose pas d'un grade équivalent à celui détenu dans le corps ou cadre d'emplois d'origine, il est classé dans le grade dont l'indice sommital est le plus proche de l'indice sommital du grade d'origine et à l'échelon comportant un indice égal ou, à défaut, immédiatement supérieur à celui qu'il détenait dans son grade d'origine (...) " ; <br/>
<br/>
              3. Considérant que, pour apprécier si le grade détenu par l'intéressé dans son corps d'origine et celui dans lequel il a été classé lors de son détachement dans un autre corps sont équivalents au sens et pour l'application des dispositions du décret du 16 septembre 1985 citées au point précédent, il y a lieu de prendre en compte non seulement l'indice terminal des deux grades, mais aussi des éléments tels que, notamment, la place des grades dans les deux corps et leur échelonnement indiciaire ; que ni la circonstance que le grade dans lequel a été prononcé le détachement d'un fonctionnaire comporte un indice terminal inférieur à celui du grade détenu par l'intéressé dans son corps d'origine, ni celle que la structuration par grades du corps d'accueil du fonctionnaire détaché soit différente de celle de son corps d'origine ne font obstacle, par elles-mêmes, à ce que les deux grades soient regardés comme équivalents ;<br/>
<br/>
              4. Considérant qu'il ressort des énonciations de l'arrêt attaqué que, pour juger que le grade d'ingénieur d'études de 2ème classe du corps des ingénieurs d'études du ministère chargé de l'enseignement supérieur était équivalent au grade d'ingénieur d'études et de fabrications du corps des ingénieurs d'études et de fabrications du ministère de la défense alors même que leurs indices terminaux étaient respectivement de 750 et 801 et que le corps d'origine de M. B...comprenait deux grades tandis que son corps d'accueil en comprenait trois, la cour s'est fondée sur la place de ces deux grades dans le déroulement de la carrière des fonctionnaires relevant des statuts des deux corps en cause, le nombre d'échelons de chacun de ces deux grades ainsi que leurs échelonnements indiciaires respectifs ; qu'en statuant ainsi, elle n'a pas commis d'erreur de droit et n'a entaché son arrêt d'aucune erreur de qualification juridique des faits ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que le pourvoi de M. B...doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et à la ministre de l'enseignement supérieur, de la recherche et de l'innovation.<br/>
Copie en sera adressée au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
