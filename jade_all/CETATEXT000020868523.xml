<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000020868523</ID>
<ANCIEN_ID>JG_L_2009_04_000000297471</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/20/86/85/CETATEXT000020868523.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 27/04/2009, 297471, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2009-04-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>297471</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin</PRESIDENT>
<AVOCATS>SCP CAPRON, CAPRON</AVOCATS>
<RAPPORTEUR>Mme Fabienne  Lambolez</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Burguburu Julie</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 18 septembre et 18 décembre 2006 au secrétariat du contentieux du Conseil d'Etat, présentés pour Maître Bernard B, agissant ès qualité de mandataire judiciaire à la liquidation de M. Léon C, demeurant ..., et pour Mme Marie-Christine A divorcée C, demeurant ... ; les requérants demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 11 juillet 2006 par lequel la cour administrative d'appel de Versailles, faisant droit à l'appel du ministre de l'économie, des finances et de l'industrie dirigé contre le jugement du 6 mai 2003 du tribunal administratif de Versailles, a rétabli M. C et Mme A au rôle de l'impôt sur le revenu à hauteur des droits et pénalités correspondant à la réintégration dans les bases d'imposition des montants de 694 850 francs au titre de 1990, 580 691 francs au titre de 1991 et 1 090 496 francs au titre de 1992 ;<br/>
<br/>
              2°) statuant au fond, de rejeter l'appel du ministre de l'économie, des finances et de l'industrie ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
     	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
                          Vu le code général des impôts et le livre des procédures fiscales ; <br/>
<br/>
                          Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Fabienne Lambolez, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Capron, Capron, avocat de Me Bernard B et de Mme Marie-Christine A, <br/>
<br/>
              - les conclusions de Mme Julie Burguburu, Rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Capron, Capron, avocat de Me Bernard B et de Mme Marie-Christine A ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. C, agent d'assurances, a fait l'objet d'une vérification de comptabilité portant sur les exercices 1990 à 1992 et d'un examen de sa situation fiscale personnelle portant sur les mêmes années ; que, sur la base de renseignements obtenus auprès de l'autorité judiciaire par l'exercice de son droit de communication, l'administration a imposé dans la catégorie des bénéfices non commerciaux sur le fondement de l'article 92 du code général des impôts les sommes détournées au détriment de ses clients par M. C, qui a été déclaré coupable d'abus de confiance et d'exercice illégal de l'activité de banquier par un arrêt de la cour d'appel de Paris du 18 décembre 1998 ; que Me B, ès qualité de mandataire judiciaire à la liquidation de M. C, et Mme A divorcée C se pourvoient en cassation contre l'arrêt de la cour administrative d'appel de Versailles du 11 juillet 2006 ayant fait droit aux conclusions de l'administration tendant au rétablissement de M. et Mme C au rôle de l'impôt sur le revenu au titre des années 1990, 1991 et 1992 et des pénalités pour mauvaise foi correspondantes ; <br/>
<br/>
              Sur la régularité de l'arrêt :<br/>
<br/>
              Considérant en premier lieu, que pour justifier devant la cour administrative d'appel de la répartition des sommes réintégrées dans les bases imposables des contribuables au titre des trois années en litige, l'administration a produit des tableaux chiffrés établis à partir d'un rapport d'expertise sur lequel la cour d'appel de Paris s'est fondée pour établir le montant des sommes détournées par M. C ; que l'appréciation portée par la cour administrative d'appel de Versailles sur la nécessité d'ordonner la production de l'intégralité de ce rapport n'est pas susceptible d'être discutée devant le juge de cassation ; que doivent être rejetés, par voie de conséquence, les moyens tirés de la violation du principe du caractère contradictoire de la procédure et des stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Considérant en deuxième lieu, que le moyen tiré de ce que la cour aurait omis d'examiner deux des moyens dont elle était saisie par l'effet dévolutif de l'appel, tirés, respectivement, de l'absence de débat oral et contradictoire au cours de l'examen de l'ensemble de la situation fiscale personnelle et de ce que M. C était dans l'impossibilité d'assurer sa défense pendant son incarcération, manque en fait, les requérants n'ayant pas soulevé ces moyens devant le tribunal administratif de Versailles ; <br/>
<br/>
              Considérant en troisième lieu, que la cour a précisé que les sommes imposées correspondaient aux fonds détournés conservés par le contribuable en tenant compte de ceux qu'il avait restitués à ses clients ; qu'ainsi, il ne peut être fait grief à la cour d'avoir omis de se prononcer sur cette question, qu'elle a exactement qualifiée ;<br/>
<br/>
              Sur le bien-fondé de l'arrêt :<br/>
<br/>
              En ce qui concerne la régularité de la procédure d'imposition :<br/>
<br/>
              Considérant qu'aux termes de l'article L. 12 du livre des procédures fiscales alors applicable :  L'administration des impôts peut procéder à l'examen contradictoire de l'ensemble de la situation fiscale personnelle des personnes physiques au regard de l'impôt sur le revenu dans les conditions prévues au présent livre. A l'occasion de cet examen, l'administration peut contrôler la cohérence entre, d'une part, les revenus déclarés et, d'autre part, la situation patrimoniale, la situation de trésorerie et les éléments du train de vie des membres du foyer fiscal. / (...)  ; qu'aux termes de l'article L. 48 du même livre :  A l'issue d'un examen contradictoire de l'ensemble de la situation fiscale personnelle au regard de l'impôt sur le revenu (...), lorsque des redressements sont envisagés, l'administration doit indiquer, avant que le contribuable présente ses observations ou accepte les rehaussements proposés, dans la notification prévue à l'article L. 57, le montant des droits, taxes et pénalités résultant de ces redressements. (...  ; qu'il résulte de ces dispositions que si le contrôle de la cohérence entre les revenus déclarés et la situation d'ensemble du contribuable, qui constitue l'objet de l'examen contradictoire de l'ensemble de la situation fiscale personnelle, implique nécessairement que le contribuable ait déclaré son revenu global de l'année, la seule absence de souscription de déclarations de revenus catégoriels ne fait pas obstacle à la mise en oeuvre de l'examen contradictoire de l'ensemble de la situation fiscale personnelle du contribuable ; qu'il ressort des pièces du dossier soumis aux juges du fond qu'après des mises en demeure de produire tant la déclaration de revenu global pour 1992 que les déclarations de bénéfices non commerciaux pour 1991 et 1992, M. C s'est abstenu de souscrire ses déclarations de revenus catégoriels, ce qui a justifié le recours à la procédure d'évaluation d'office ; que dès lors qu'il n'était pas contesté que les époux C avaient obtempéré à la mise en demeure de souscrire leur déclaration de revenu global pour 1992, la cour administrative d'appel, en estimant que l'administration pouvait régulièrement procéder à un examen contradictoire de l'ensemble de la situation fiscale personnelle en l'absence de déclaration de revenus, n'a pu viser, dans les circonstances de l'espèce, que l'absence des déclarations de revenus catégoriels ; qu'ainsi, la cour administrative d'appel n'a pas commis d'erreur de droit dans l'application de l'article L. 12 du livre des procédures fiscales ;<br/>
<br/>
              En ce qui concerne le bien-fondé de l'imposition :<br/>
<br/>
              Considérant que, faisant droit aux demandes de l'administration, les juges d'appel se sont fondés sur les constatations figurant dans l'arrêt de la cour d'appel de Paris du 18 décembre 1998 pour déterminer le montant des bénéfices non commerciaux non professionnels à réintégrer dans les bases d'imposition qui correspondaient aux sommes détournées par M. C, nettes des  remboursements  effectués par ce dernier, et leur répartition dans chacune des années d'imposition en cause ; que les moyens tirés de ce que les juges d'appel auraient méconnu le principe de l'annualité de l'impôt et l'autorité de chose jugée par le juge pénal ne sont pas fondés ; <br/>
<br/>
              Considérant que les juges d'appel ont exactement qualifié les faits en jugeant que les dernières conclusions d'appel par lesquelles l'administration réduisait le montant des bases d'imposition dont elle demandait le rétablissement, ne pouvaient être regardées comme une demande de compensation au sens des dispositions de l'article L. 203 du livre des procédures fiscales ;<br/>
<br/>
              Considérant enfin que les juges d'appel n'ont pas commis d'erreur de droit en estimant que l'administration établissait la mauvaise foi de M. C en relevant l'importance, l'origine des sommes non déclarées et le caractère répété de ces dissimulations ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que Me B ès qualité de mandataire judiciaire à la liquidation de M. C et Mme A divorcée C ne sont pas fondés à demander l'annulation de l'arrêt de la cour administrative d'appel de Versailles du 11 juillet 2006 ; que doivent être rejetées, par voie de conséquence, leurs conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Me B, ès qualité de mandataire judiciaire à la liquidation de M. C, et de Mme A divorcée C est rejeté.<br/>
      Article 2 : La présente décision sera notifiée à Me Bernard B, à Mme Marie-Christine 	A et au ministre du budget, des comptes publics et de la fonction publique.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
