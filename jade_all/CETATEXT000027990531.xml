<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027990531</ID>
<ANCIEN_ID>JG_L_2013_09_000000370023</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/99/05/CETATEXT000027990531.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 17/09/2013, 370023, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-09-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370023</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Christophe Pourreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:370023.20130917</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 1106442 du 4 juillet 2013 par laquelle le président de la dixième chambre du tribunal administratif de Cergy-Pontoise, avant qu'il soit statué sur la demande de la commune de Frépillon, tendant, d'une part, à l'annulation de la décision du préfet du Val-d'Oise du 9 mai 2011 fixant à 395 819 euros le montant de sa dotation forfaitaire au titre de la dotation globale de fonctionnement pour 2011 et, d'autre part, à ce qu'il soit enjoint au préfet de prendre une nouvelle décision à ce sujet, a décidé, par application de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des articles L. 2334-3 et L. 2334-7 du code général des collectivités territoriales ;<br/>
<br/>
              Vu le mémoire, enregistré le 31 décembre 2012 au greffe du tribunal administratif de Cergy-Pontoise, présenté par la commune de Frépillon, représentée par son maire, en application de l'article 23-1 de l'ordonnance du 7 novembre 1958 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 11 septembre 2013, présentée par la commune de Frépillon ;<br/>
<br/>
              Vu la Constitution, notamment son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu la loi n° 2004-1484 du 30 décembre 2004 de finances pour 2005, notamment son article 47 ;<br/>
<br/>
              Vu la décision n° 2004-511 DC du Conseil constitutionnel du 29 décembre 2004 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, Maître des Requêtes,<br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              Sur l'intervention de l'association des maires ruraux de France :<br/>
<br/>
              2. Considérant que l'association des maires ruraux de France n'est pas recevable à intervenir devant le Conseil d'Etat à l'occasion de la question prioritaire de constitutionnalité transmise par le tribunal administratif de Cergy-Pontoise ;<br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              3. Considérant que la commune de Frépillon conteste la conformité à la Constitution, d'une part, de l'article L. 2334-3 du code général des collectivités territoriales, en vertu duquel les communes sont classées, pour le calcul de leur effort fiscal et de la fraction de dotation nationale de péréquation et de dotation de solidarité rurale qui leur est attribuée, en quinze groupes démographiques en fonction de l'importance de leur population, allant du groupe des communes de 0 à 499 habitants au groupe des communes de plus de 200 000 habitants, et, d'autre part, eu égard à l'argumentation développée dans ses mémoires, des seules dispositions du 1° de l'article L. 2334-7 du même code, en vertu desquelles la dotation forfaitaire d'une commune, qui constitue l'une des deux composantes de sa dotation globale de fonctionnement, comprend une dotation de base, destinée à tenir compte des charges liées à l'importance de la population, égale au produit de sa population par un montant par habitant variant " en fonction croissante " de sa population entre 64,46 euros et 128,93 euros, dans des conditions définies par décret en Conseil d'Etat ;<br/>
<br/>
              4. Considérant, en premier lieu, que la commune de Frépillon a posé la question prioritaire de constitutionnalité à l'occasion d'un litige relatif au montant de la dotation forfaitaire qui lui a été allouée au titre de la dotation globale de fonctionnement pour 2011 ; que le calcul de la dotation forfaitaire d'une commune n'est pas fonction du groupe démographique auquel elle appartient au sens des dispositions de l'article L. 2334-3 du code général des collectivités territoriales ; qu'au demeurant, dans sa rédaction applicable au litige dont est saisi le tribunal administratif de Cergy-Pontoise, cet article ne mentionnait pas l'article L. 2334-7 du même code parmi les dispositions qu'il énumère et pour l'application desquelles le classement des communes en groupes démographiques était utilisé ; que, par suite, les dispositions de l'article L. 2334-3 du code général des collectivités territoriales ne peuvent être regardées comme applicables au litige ;<br/>
<br/>
              5. Considérant, en second lieu, que les dispositions du 1° de l'article L. 2334-7 du code général des collectivités territoriales sont issues de l'article 47 de la loi du 30 décembre 2004 de finances pour 2005, que le Conseil constitutionnel, dans l'article 3 de sa décision n° 2004-511 DC du 29 décembre 2004, a déclaré conforme à la Constitution ; que, depuis lors, le législateur n'est intervenu que pour actualiser les montants en valeur absolue, exprimés en euros (64,46 euros au lieu de 60 euros et 124,93 euros au lieu de 120 euros), entre lesquels varie le montant par habitant alloué aux communes selon leur population au titre de la dotation de base, sans modifier le rapport de un à deux entre les valeurs inférieure et supérieure ni le principe d'une variation selon une " fonction croissante " entre ces valeurs ; que leur simple actualisation ne peut être tenue pour un changement de circonstances de droit affectant la portée des dispositions déclarées conformes à la Constitution ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que les articles L. 2334-3 et L. 2334-7 du code général des collectivités territoriales porteraient atteinte aux droits et libertés garantis par la Constitution doit être écarté ;<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de l'association des maires ruraux de France n'est pas admise.<br/>
Article 2 : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le tribunal administratif de Cergy-Pontoise.<br/>
Article 3 : La présente décision sera notifiée à la commune de Frépillon, à l'association des maires ruraux de France et au ministre de l'intérieur.<br/>
Copie en sera adressée pour information au Premier ministre, au Conseil constitutionnel et au tribunal administratif de Cergy-Pontoise.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
