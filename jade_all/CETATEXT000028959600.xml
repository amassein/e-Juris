<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028959600</ID>
<ANCIEN_ID>JG_L_2014_05_000000368468</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/95/96/CETATEXT000028959600.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère SSJS, 19/05/2014, 368468, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-05-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368468</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>Mme Marie Grosset</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:368468.20140519</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>VU LA PROCEDURE SUIVANTE :<br/>
<br/>
              Par une décision du 1er août 2013, le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi de la société Aubert et Duval dirigées contre l'arrêt n° 12LY01895 de la cour administrative d'appel de Lyon du 7 mai 2013 en tant que cet arrêt s'est prononcé sur l'inscription de l'établissement Aubert et Duval des Ancizes sur la liste des établissements ouvrant droit au dispositif de cessation anticipée d'activité des travailleurs de l'amiante pour la période allant de 1993 au 7 février 2005.<br/>
<br/>
              Par un nouveau mémoire et un mémoire en réplique, enregistrés les 31 octobre 2013 et 26 mars 2014 au secrétariat du contentieux du Conseil d'Etat, la société Aubert et Duval reprend les conclusions de son pourvoi et les mêmes moyens.<br/>
<br/>
              Par un mémoire en défense, enregistré le 24 février 2014, M. A...B...conclut au rejet du pourvoi et à ce que la somme de 3 500 euros soit mise à la charge de la société Aubert et Duval au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Il soutient que les moyens du pourvoi ne sont pas fondés.<br/>
<br/>
<br/>
<br/>
                          Vu :<br/>
	- les autres pièces du dossier ;<br/>
              - la loi n° 98-1194 du 23 décembre 1998 ;<br/>
	- le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Grosset, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, avocat de la société Aubert et Duval, et à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M.B....<br/>
<br/>
<br/>
<br/>
<br/>
<br/>CONSIDERANT CE QUI SUIT :<br/>
<br/>
              1. Aux termes du I de l'article 41 de la loi n° 98-1194 du 23 décembre 1998 de financement de la sécurité sociale pour 1999, dans sa rédaction applicable à la date de la décision attaquée : " Une allocation de cessation anticipée d'activité est versée aux salariés et anciens salariés des établissements de fabrication de matériaux contenant de l'amiante, des établissements de flocage et de calorifugeage à l'amiante ou de construction et de réparation navales, sous réserve qu'ils cessent toute activité professionnelle, lorsqu'ils remplissent les conditions suivantes : / 1° Travailler ou avoir travaillé dans un des établissements mentionnés ci-dessus et figurant sur une liste établie par arrêté des ministres chargés du travail, de la sécurité sociale et du budget, pendant la période où y étaient fabriqués ou traités l'amiante ou des matériaux contenant de l'amiante. L'exercice des activités de fabrication de matériaux contenant de l'amiante, de flocage et de calorifugeage à l'amiante de l'établissement doit présenter un caractère significatif ; / 2° Avoir atteint un âge déterminé, qui pourra varier en fonction de la durée du travail effectué dans les établissements visés au 1° sans pouvoir être inférieur à cinquante ans (...) ".<br/>
<br/>
              2. Par un arrêt du 18 décembre 2008, la cour administrative d'appel de Lyon a rejeté les appels formés par la société Aubert et Duval et par le ministre du travail, des relations sociales et de la solidarité à l'encontre du jugement du 24 novembre 2006 par lequel le tribunal administratif de Clermont-Ferrand avait annulé la décision du ministre du 7 février 2005 refusant de classer l'établissement Aubert et Duval situé aux Ancizes sur la liste des établissements ouvrant droit au dispositif de cessation anticipée d'activité des travailleurs de l'amiante. Son arrêt se fonde sur le motif, qui s'est rétroactivement substitué à celui qu'avaient retenu les premiers juges, " qu'au moins jusqu'en 1992 des salariés de cet établissement entretenaient et réfectionnaient le calorifugeage d'un nombre significatif d'installations à l'aide de produits à base d'amiante sous différentes formes, afin d'assurer la protection thermique de certains fours, ainsi que de certaines pièces et fluides ou effectuaient dans le cadre de la production, lors de chaque coulée liquide d'acier du secteur " aciérie ", des opérations de calorifugeage et décalorifugeage portant sur le remplacement des joints d'embase en amiante situés sous les lingotières " et qu'un nombre significatif de salariés avaient été exposés à l'amiante  au cours de ces opérations.<br/>
<br/>
              3. Par son arrêt attaqué du 7 mai 2013, la cour a jugé qu'en l'absence de circonstances de fait et de droit nouvelles, le ministre chargé du travail ne pouvait, sans méconnaître l'autorité absolue de chose jugée s'attachant au jugement et à l'arrêt annulant sa décision du 7 février 2005, rejeter la réclamation de M. B...tendant à l'inscription de l'aciérie des Ancizes sur la liste prévue par les dispositions du 1° du I de l'article 41 de la loi du 23 décembre 1998 pour la période allant jusqu'à la date de cette décision. En statuant ainsi, alors que les motifs qui sont le soutien nécessaire du dispositif du jugement d'annulation du 24 novembre 2006 ne prenaient pas partie sur la question de savoir si les conditions mises par la loi à l'inscription sur la liste des établissements ouvrant droit au dispositif de cessation anticipée d'activité des travailleurs de l'amiante étaient remplies au-delà de 1992, la cour s'est méprise sur la portée de la chose jugée par son arrêt du 18 décembre 2008.<br/>
<br/>
              4. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi dirigés contre cette partie de l'arrêt, que la société Aubert et Duval est fondée à demander l'annulation de l'arrêt du 7 mai 2013 en tant qu'il s'est prononcé sur les conclusions de l'appel de M. B...relatives à l'inscription de l'établissement des Ancizes sur la liste prévue par l'article 41 de la loi du 23 décembre 1998 pour la période allant de 1993 au 7 février 2005.<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Aubert et Duval. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 7 mai 2013 est annulé en tant qu'il annule le jugement du tribunal administratif de Clermont-Ferrand du 7 juin 2012 et la décision implicite de rejet de la réclamation présentée par M. B...le 13 septembre 2010 et enjoint au ministre chargé du travail de procéder à l'inscription de l'aciérie des Ancizes sur la liste prévue au 1° du I de l'article 41 de la loi du 23 décembre 1998 pour la période allant de 1993 au 7 février 2005.<br/>
Article 2 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Lyon.<br/>
Article 3 : Les conclusions de M. B...présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la société Aubert et Duval et à M. A...B....<br/>
Copie en sera adressée pour information au ministre du travail, de l'emploi et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
