<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042322410</ID>
<ANCIEN_ID>JG_L_2020_09_000000439520</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/32/24/CETATEXT000042322410.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 09/09/2020, 439520, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-09-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439520</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:439520.20200909</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Procédure contentieuse antérieure :<br/>
<br/>
              Mme C... D..., agissant pour son compte personnel et en qualité de représentante légale de ses enfants mineurs, A..., Souleyman et Hamza et Mme B... E..., sa mère, ont demandé au tribunal administratif de Paris d'annuler la décision du Président de la République, rapportée par la presse le 13 mars 2019, d'organiser " au cas par cas " le rapatriement des enfants français se trouvant dans les camps du nord-est de la Syrie. Par une ordonnance n° 1910042 du 12 août 2019, le président de la 6ème section du tribunal administratif de Paris a rejeté leur demande comme ne relevant manifestement pas de la compétence de la juridiction administrative. <br/>
<br/>
              Par une ordonnance n°19PA03083 du 24 janvier 2020, la présidente de la 1ère chambre de la cour administrative d'appel de Paris a rejeté l'appel formé par Mmes D... et E... contre cette ordonnance.<br/>
<br/>
              Procédure devant le Conseil d'Etat :<br/>
<br/>
              Par un pourvoi, enregistré le 13 mars 2020 au secrétariat du contentieux du Conseil d'Etat, Mmes D... et E... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) en tant que de besoin, d'adresser à la Cour européenne des droits de l'homme, sur le fondement de l'article 1er du protocole n° 16 à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, la demande d'avis consultatif sur la question suivante : " L'Etat français a-t-il, en vertu des articles 1er, 2 et 3 de la Convention, une obligation positive de mettre en oeuvre les moyens qui sont susceptibles de mettre un terme aux traitements inhumains et dégradants et à l'exposition à un risque de mort subis par les ressortissants français mineurs retenus dans les camps de réfugiés en Syrie, et les parents de ces ressortissants mineurs peuvent-il se prévaloir d'un grief défendable devant les juridictions internes devant lesquelles ils agissent pour contester le refus de porter une telle assistance, en présence duquel l'incompétence du juge administratif et judiciaire pour connaître des actes non détachables de relations internationales constitue une violation de l'article 13 de la Convention ' ".<br/>
<br/>
              3°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
- la Déclaration des droits de l'homme et du citoyen, notamment son article 16 ;<br/>
              - la Constitution, notamment son Préambule et ses articles 34 et 61-1 ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son protocole n° 16 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de justice administrative, notamment ses articles L. 211-1 et L. 211-2.		<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Sevaux, Mathonnet, avocat de Mme C... D... et de Mme B... E... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et qu'elle soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes de l'article L. 211-1 du code de justice administrative : " Les tribunaux administratifs sont, en premier ressort et sous réserve des compétences attribuées aux autres juridictions administratives, juges de droit commun du contentieux administratif ". L'article L. 211-2 du même code dispose que : " Les cours administratives d'appel connaissent des jugements rendus en premier ressort par les tribunaux administratifs, sous réserve des compétences attribuées au Conseil d'Etat en qualité de juge d'appel et de celles définies aux articles L. 552-1 et L. 552-2./ Elles connaissent en premier et dernier ressort des litiges dont la compétence leur est attribuée par décret en Conseil d'Etat à raison de leur objet ou de l'intérêt d'une bonne administration ".<br/>
<br/>
              3. Mmes D... et E... soutiennent que les dispositions des articles L. 211-1 et L. 211-2 du code de justice administrative, citées au point 2, méconnaissent le droit à un recours juridictionnel effectif garanti par l'article 16 de la Déclaration des droits de l'homme et du citoyen, ainsi que la compétence confiée au législateur par l'article 34 de la Constitution, en ce qu'elles limitent la compétence des tribunaux administratifs et des cours administratives d'appel au contentieux des actes administratifs sans étendre cette compétence au contentieux de l'annulation des actes non détachables de la conduite des relations extérieures de la France. <br/>
<br/>
              4. Toutefois, l'incompétence de toute juridiction pour connaître des actes qui ne sont pas détachables de la conduite des relations extérieures de la France ne procède pas des articles L. 211-1 et L. 211-2 du code de justice administrative, qui disposent que, sous certaines réserves, les tribunaux administratifs sont juges de droit commun en premier ressort du contentieux administratif et que les cours administratives d'appel connaissent en appel des jugements rendus par les tribunaux administratifs. Il en résulte que ces articles ne peuvent être regardés comme applicables au litige, au sens de l'article 23-5 de l'ordonnance du 7 novembre 1958. Il s'ensuit qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par Mmes D... et E....<br/>
<br/>
              Sur les autres moyens du pourvoi :<br/>
<br/>
              5. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              6. Pour demander l'annulation de l'ordonnance qu'elles attaquent, Mme D... et E... soutiennent en outre, que :<br/>
              - cette ordonnance doit être annulée par voie de conséquence de l'inconstitutionnalité des articles L. 211-1 et L. 211-2 du code de justice administrative ;<br/>
              - l'ordonnance attaquée est entachée d'une erreur de droit et d'une erreur de qualification juridique des faits pour avoir considéré que la décision du Président de la République d'examiner au cas par cas la question du rapatriement des enfants retenus dans des camps en Syrie n'était pas détachable des relations internationales de la France ;<br/>
              - l'ordonnance attaquée méconnaît les stipulations des articles 2, 3 et 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.<br/>
<br/>
              7. Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par Mmes D... et E....<br/>
Article 2 : Le pourvoi de Mmes D... et E... n'est pas admis.<br/>
<br/>
Article 3 : La présente décision sera notifiée à Mmes C... D... et B... E....<br/>
Copie en sera adressée au Conseil constitutionnel, au Président de la République, au Premier ministre et au ministre de l'Europe et des affaires étrangères.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
