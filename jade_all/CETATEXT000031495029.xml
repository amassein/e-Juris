<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031495029</ID>
<ANCIEN_ID>JG_L_2015_11_000000380461</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/49/50/CETATEXT000031495029.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 18/11/2015, 380461, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-11-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380461</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Vincent Villette</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:380461.20151118</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Châlons-en-Champagne, d'une part, d'annuler la décision du 6 août 2012 par laquelle la garde des sceaux, ministre de la justice a refusé de lui octroyer l'indemnité de 2 000 euros qu'il sollicitait en réparation du préjudice qu'il estime avoir subi en raison de l'illégalité de la décision du 1er juin 2012 de la commission de discipline de la maison centrale de Clairvaux, à Bayel, lui infligeant une sanction de placement en cellule disciplinaire de douze jours, retirée le 13 juin 2012 par le directeur interrégional des services pénitentiaires de Dijon et, d'autre part, de condamner l'Etat à lui verser la somme de 2 000 euros. Par un jugement n° 1201821 du 11 février 2014, le tribunal administratif de Châlons-en-Champagne a rejeté cette demande.<br/>
<br/>
              Par une ordonnance n° 14NC00730 du 13 mai 2014, enregistrée le 21 mai 2014 au secrétariat du contentieux du Conseil d'Etat, la présidente de la cour administrative d'appel de Nancy a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 25 avril 2014 au greffe de la cour administrative d'appel de Nancy, présenté par M. B.... Par ce pourvoi, un nouveau mémoire ainsi qu'un mémoire en réplique, enregistrés au secrétariat du contentieux les 30 juillet 2014 et 21 septembre 2015, M. B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Waquat-Farge-Hazan, avocat de M.B..., au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
	- le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Villette, auditeur,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M. A...B...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.B..., écroué à la maison centrale de Clairvaux, à Bayel, s'est vu infliger, par une décision de la commission de discipline de la maison centrale en date du 1er juin 2012, une sanction de douze jours de cellule disciplinaire, qui a été retirée, après avoir été exécutée, par le directeur interrégional des services pénitentiaires pour un motif d'irrégularité de procédure ; que M. B... a demandé à la garde des sceaux, ministre de la justice, l'indemnisation du préjudice qu'il estime avoir subi en raison de l'illégalité de cette décision ; qu'il se pourvoit en cassation contre le jugement par lequel le tribunal administratif de Châlons-en-Champagne a rejeté sa demande d'indemnisation ;<br/>
<br/>
              2. Considérant que lorsqu'une personne sollicite le versement d'une indemnité en réparation du préjudice subi du fait de l'illégalité, pour un vice de procédure, de la décision lui infligeant une sanction, il appartient au juge de plein contentieux, saisi de moyens en ce sens, de déterminer, en premier lieu, la nature de cette irrégularité procédurale puis, en second lieu, de rechercher, en forgeant sa conviction au vu de l'ensemble des éléments produits par les parties, si, compte tenu de la nature et de la gravité de cette irrégularité procédurale, la même décision aurait pu être légalement prise, s'agissant tant du principe même de la sanction que de son quantum, dans le cadre d'une procédure régulière ; <br/>
<br/>
              3. Considérant qu'il suit de là qu'en jugeant que M. B...n'était pas fondé à réclamer une indemnisation au titre de l'irrégularité de la procédure tenue devant la commission de discipline, sans avoir recherché au préalable ni la nature de cette irrégularité, ni si elle était susceptible d'avoir exercé une influence sur le principe ou le quantum de la sanction retenue, le tribunal administratif de Châlons-en-Champagne a entaché son jugement d'erreur de droit ; qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, M. B...est fondé à demander l'annulation du jugement qu'il attaque ; <br/>
<br/>
              4. Considérant que M. B...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Waquet-Farge-Hazan renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat le versement à cette SCP de la somme de 3 000 euros ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Châlons-en-Champagne du 11 février 2014 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Châlons-en-Champagne.<br/>
Article 3 : L'Etat versera à la SCP Waquet-Farge-Hazan, avocat de M.B..., une somme de 3 000 euros en applications des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991 sous réserve qu'elle renonce à percevoir la somme correspondant à la part contributive de l'Etat. <br/>
Article 4 : La présente décision sera notifiée à M. A...B...et à la garde des sceaux, ministre de la justice. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">59-02-02 RÉPRESSION. DOMAINE DE LA RÉPRESSION ADMINISTRATIVE RÉGIME DE LA SANCTION ADMINISTRATIVE. - ILLÉGALITÉ D'UNE SANCTION POUR VICE DE PROCÉDURE - MÉTHODE À SUIVRE POUR DÉTERMINER SI L'ILLÉGALITÉ A CAUSÉ UN PRÉJUDICE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-01-04 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. RESPONSABILITÉ ET ILLÉGALITÉ. - ILLÉGALITÉ D'UNE SANCTION POUR VICE DE PROCÉDURE - MÉTHODE À SUIVRE POUR DÉTERMINER SI L'ILLÉGALITÉ A CAUSÉ UN PRÉJUDICE [RJ1].
</SCT>
<ANA ID="9A"> 59-02-02 Lorsqu'une personne sollicite le versement d'une indemnité en réparation du préjudice subi du fait de l'illégalité, pour un vice de procédure, de la décision lui infligeant une sanction, il appartient au juge de plein contentieux, saisi de moyens en ce sens, de déterminer, en premier lieu, la nature de cette irrégularité procédurale puis, en second lieu, de rechercher, en forgeant sa conviction au vu de l'ensemble des éléments produits par les parties, si, compte tenu de la nature et de la gravité de cette irrégularité procédurale, la même décision aurait pu être légalement prise, s'agissant tant du principe même de la sanction que de son quantum, dans le cadre d'une procédure régulière.</ANA>
<ANA ID="9B"> 60-01-04 Lorsqu'une personne sollicite le versement d'une indemnité en réparation du préjudice subi du fait de l'illégalité, pour un vice de procédure, de la décision lui infligeant une sanction, il appartient au juge de plein contentieux, saisi de moyens en ce sens, de déterminer, en premier lieu, la nature de cette irrégularité procédurale puis, en second lieu, de rechercher, en forgeant sa conviction au vu de l'ensemble des éléments produits par les parties, si, compte tenu de la nature et de la gravité de cette irrégularité procédurale, la même décision aurait pu être légalement prise, s'agissant tant du principe même de la sanction que de son quantum, dans le cadre d'une procédure régulière.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 19 juin 1981, Carliez, n° 20619, p. 274 ; CE, 7 juin 2010, Bussiere, n° 312909, T. pp. 635-974.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
