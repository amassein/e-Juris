<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027666357</ID>
<ANCIEN_ID>JG_L_2013_07_000000359164</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/66/63/CETATEXT000027666357.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 05/07/2013, 359164, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-07-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359164</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Angélique Delorme</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:359164.20130705</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 7 mai 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par l'Association agréée des pêcheurs amateurs aux engins et filets " la maille landaise ", dont le siège est chez M. A...B..., Mye Maisoun - route de Guiche à Sames (64520), représentée par son président ; l'association demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 2 mars 2012 fixant le contenu du dossier de demande d'agrément prévu à l'article L. 434-3 du code de l'environnement et les statuts types des associations départementales de pêcheurs amateurs aux engins et aux filets sur les eaux du domaine public ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 200 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Angélique Delorme, Auditeur,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'en vertu de l'article L. 434-3 du code de l'environnement, les associations départementales de pêcheurs amateurs aux engins et aux filets, qui contribuent à la surveillance de la pêche, exploitent les droits de pêche qu'elles détiennent, participent à la protection du patrimoine piscicole et des milieux aquatiques et effectuent des opérations de gestion piscicole pour les lots où leurs membres sont autorisés à pêcher, doivent obtenir un agrément ; qu'aux termes des troisième et quatrième alinéas de ce même article : " Dans chaque département, les associations agréées de pêche et de protection du milieu aquatique et l'association agréée de pêcheurs amateurs aux engins et aux filets sur les eaux du domaine public sont obligatoirement regroupées en une fédération départementale des associations agréées de pêche et de protection du milieu aquatique./ Les décisions de chacune de ces fédérations, relatives à la pêche amateur aux engins et aux filets, sont prises, à peine de nullité, après avis d'une commission spécialisée créée en leur sein et composée majoritairement de représentants des pêcheurs amateurs aux engins et aux filets sur les eaux du domaine public. " ; qu'aux termes de l'article R. 434-26 du même code : " L'agrément prévu pour ces associations peut être accordé aux associations constituées et déclarées conformément à la loi du 1er juillet 1901 relative au contrat d'association ou aux articles 21 à 79 du code civil local maintenus en vigueur dans les départements du Haut-Rhin, du Bas-Rhin et de la Moselle par la loi du 1er juin 1924 mettant en vigueur la législation civile française dans ces départements, dont les statuts sont conformes à des statuts types pris par arrêté ministériel. " ; <br/>
<br/>
              2. Considérant que, sur le fondement de ces dispositions, le ministre de l'écologie, du développement durable, du transport et du logement a pris, le 2 mars 2012, un arrêté fixant le contenu du dossier de demande d'agrément prévu à l'article L. 434-3 du code de l'environnement et les statuts-types des associations départementales de pêcheurs amateurs aux engins et aux filets sur les eaux du domaine public ; que l'Association départementale agréée des pêcheurs amateurs aux engins et filets " la maille landaise " demande l'annulation pour excès de pouvoir de cet arrêté ;<br/>
<br/>
              3. Considérant, en premier lieu, qu'aux termes de l'article 11 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " 1. Toute personne a droit à la liberté de réunion pacifique et à la liberté d'association (...). / 2. L'exercice de ces droits ne peut faire l'objet d'autres restrictions que celles qui, prévues par la loi, constituent des mesures nécessaires, dans une société démocratique, à la sécurité nationale, à la sûreté publique, à la défense de l'ordre et à la prévention du crime, à la protection de la santé ou de la morale, ou à la protection des droits et libertés d'autrui. Le présent article n'interdit pas que des restrictions légitimes soient imposées à l'exercice de ces droits par les membres des forces armées, de la police ou de l'administration de l'Etat " ;<br/>
<br/>
              4. Considérant qu'il résulte des dispositions de l'article L. 434-3 du code de l'environnement que, par le regroupement des associations agréées de pêche et de protection du milieu aquatique et de l'association agréée de pêcheurs amateurs aux engins et aux filets sur les eaux du domaine public de chaque département au sein d'une unique fédération départementale des associations agréées de pêche et de protection du milieu aquatique, le législateur a entendu concilier différents objectifs d'intérêt général, notamment assurer la coexistence des différentes catégories de pêcheurs et des différents modes de pêche, éviter une pratique anarchique de la pêche, protéger les milieux naturels et permettre une gestion rationnelle du patrimoine piscicole, tendant notamment à la reconstitution des populations de certaines espèces migratoires en voie de disparition ; qu'en prévoyant qu'au sein de ces fédérations, lesquelles n'exercent aucun pouvoir de contrainte ou de tutelle vis-à-vis des leurs associations adhérentes, une commission spécialisée composée majoritairement de représentants des pêcheurs amateurs aux engins et aux filets sur les eaux du domaine public serait consultée pour avis, à peine de nullité, sur toute décision relative à cette catégorie de pêcheurs, le législateur a institué une garantie de prise en compte des intérêts propres de la pêche aux engins et aux filets au sein de ces fédérations ; qu'il ne ressort pas des pièces du dossier que cette garantie serait insuffisante dans le cadre de l'exercice, par les fédérations départementales des associations agréées de pêche et de protection du milieu aquatique, de compétences concernant la pratique de la pêche aux engins et aux filets ; qu'ainsi, l'atteinte portée par les dispositions législatives en cause à la liberté d'association de la requérante ne revêt pas un caractère disproportionné par rapport aux buts d'intérêt général poursuivis ; que doit, par suite, être écarté le moyen tiré de ce que l'article 12 des statuts-types annexés à l'arrêté attaqué, qui réitère l'obligation d'adhésion des associations agréées de pêcheurs amateurs aux engins et aux filets à la fédération départementale des associations agréées de pêche et de protection du milieu aquatique de leur département, aurait été pris sur le fondement de dispositions législatives incompatibles avec l'article 11 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              5. Considérant, en second lieu, qu'il résulte des termes mêmes du troisième alinéa de l'article L. 434-3 du code de l'environnement précité que le ressort géographique des associations agréées des pêcheurs aux engins et aux filets ne peut être que départemental ; que, par suite, le moyen tiré de ce que l'arrêté attaqué aurait, en méconnaissance de ces dispositions, interdit l'agrément d'associations interdépartementales ne peut qu'être écarté ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que l'Association agréée des pêcheurs amateurs aux engins et filets " la maille landaise " n'est pas fondée à demander l'annulation de l'arrêté qu'elle attaque ; que, par voie de conséquence, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'Association agréée des pêcheurs amateurs aux engins et filets " la maille landaise " est rejetée.<br/>
Article 2 : La présente décision sera notifiée à l'Association agréée des pêcheurs amateurs aux engins et filets " la maille landaise " et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
