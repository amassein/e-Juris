<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026664219</ID>
<ANCIEN_ID>JG_L_2012_11_000000336380</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/66/42/CETATEXT000026664219.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 21/11/2012, 336380, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-11-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>336380</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Marc Dandelot</PRESIDENT>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Jérôme Marchand-Arvier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:336380.20121121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 8 février 2010 au secrétariat du contentieux du Conseil d'Etat, présenté pour Mme Bandi B, demeurant chez M. Tamba B Bagadadji à Bamako (BP 2098), Mali ; Mme B demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 06/00064 du 1er février 2008 par lequel la cour régionale des pensions de Paris a infirmé le jugement n° 06/00003 du 6 novembre 2006 par lequel le tribunal départemental des pensions de Paris avait fait droit à sa demande de décristallisation de la pension de réversion dont elle est titulaire du chef de M. Kassoum B, son époux décédé  ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 500 euros à verser à la SCP Delaporte-Briard-Trichet, son avocat, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment ses articles 61-1 et 62 ;<br/>
<br/>
              Vu le code des pensions militaires d'invalidité et des victimes de la guerre ;<br/>
<br/>
              Vu le code de procédure civile ;<br/>
<br/>
              Vu la loi n° 59-1454 du 26 décembre 1959 ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu la loi n° 2002-1576 du 30 décembre 2002 ;<br/>
<br/>
              Vu la loi n° 2010-1657 du 29 décembre 2010 ; <br/>
<br/>
              Vu le décret n° 59-327 du 20 février 1959 ; <br/>
<br/>
              Vu le décret n° 91-1266 du 19 décembre 1991 ; <br/>
<br/>
              Vu la décision n° 2010-1 QPC du 28 mai 2010 du Conseil constitutionnel ;<br/>
<br/>
              Vu la décision n° 2010-108 QPC du 25 mars 2011 du Conseil constitutionnel ;<br/>
<br/>
              Vu le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jérôme Marchand-Arvier, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Delaporte, Briard, Trichet, avocat de Mme B,<br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Delaporte, Briard, Trichet, avocat de Mme B ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. Kassoum B, ressortissant malien ayant servi dans l'armée française, s'est vu concéder une pension militaire d'invalidité au taux de 100 % par arrêté du 1er août 1969 ; qu'à la suite de son décès le 20 avril 1970, Mme B, sa veuve, également de nationalité malienne, a sollicité, par lettre du 4 septembre 2002, le bénéfice d'une pension de réversion de son chef  ; que, par arrêté du 18 juillet 2005, le ministre de la défense lui a accordé, avec jouissance rétroactive à compter du 1er janvier 2002, une pension de réversion calculée conformément aux dispositions de l'article 68 de la loi du 30 décembre 2002 de finances rectificative pour 2002 ; que, par une demande enregistrée le 9 janvier 2006, Mme B a contesté cette décision devant le tribunal départemental des pensions de Paris ; que, par jugement du 6 novembre 2006, le tribunal a ordonné la décristallisation de sa pension de réversion et condamné l'Etat à lui verser les arrérages correspondant à la différence entre le montant de la pension à taux plein qu'elle était en droit de percevoir et le montant de la pension cristallisée qu'elle a perçue ; que Mme B demande l'annulation de l'arrêt du 1er février 2008 par lequel, sur l'appel du ministre de la défense, la cour régionale des pensions de Paris a infirmé ce jugement et rejeté sa demande ;<br/>
<br/>
              Sur la fin de non-recevoir opposée par le ministre de la défense :<br/>
<br/>
              2. Considérant, d'une part, qu'aux termes du premier alinéa de l'article R. 821-1 du code de justice administrative, que l'article 17 du décret du 20 février 1959 rend applicable au pourvoi formé contre les arrêts des cours régionales des pensions : " Sauf disposition contraire, le délai de recours en cassation est de deux mois. " ; qu'aux termes du premier alinéa de l'article R. 811-5 du même code, dans sa rédaction alors en vigueur, que l'article R. 821-2 rend applicable aux pourvois en cassation : " Les délais supplémentaires de distance prévus aux articles 643 et 644 du nouveau code de procédure civile s'ajoutent aux délais normalement impartis. " ; qu'il résulte des dispositions de l'article 643 du nouveau code de procédure civile, devenu le code de procédure civile, que lorsque la demande est portée devant une juridiction qui a son siège en France métropolitaine, le délai du pourvoi en cassation est augmenté de deux mois pour les personnes demeurant à l'étranger ;<br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes de l'article 39 du décret du 19 décembre 1991 portant application de la loi n° 91-647 du 10 juillet 1991 relative à l'aide juridique : " Lorsqu'une demande d'aide juridictionnelle en vue de se pourvoir en matière civile devant la Cour de cassation est adressée au bureau d'aide juridictionnelle établi près cette juridiction avant l'expiration du délai imparti pour le dépôt du pourvoi ou des mémoires, ce délai est interrompu. Un nouveau délai court à compter du jour de la réception par l'intéressé de la notification de la décision du bureau d'aide juridictionnelle ou, si elle est plus tardive, de la date à laquelle un auxiliaire de justice a été désigné (...) / Les délais de recours sont interrompus dans les mêmes conditions lorsque l'aide juridictionnelle est sollicitée à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'aux termes de l'article 40 du même décret : " Lorsqu'une demande d'aide juridictionnelle est adressée à un bureau par voie postale, sa date est celle de l'expédition de la lettre. La date d'expédition est celle qui figure sur le cachet du bureau de poste d'émission " ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier que l'arrêt du 1er février 2008 de la cour régionale des pensions de Paris a été notifié à Mme B à Bamako, non pas le 13 février 2008 comme le soutient le ministre de la défense, mais le 14 mars 2008 ; que, compte tenu du délai supplémentaire de distance dont bénéficiait l'intéressée eu égard à sa résidence au Mali, le délai qui lui était imparti pour former un pourvoi en cassation contre cet arrêt expirait normalement le 15 juillet 2008 ; que, cependant, Mme B a sollicité le bénéficie de l'aide juridictionnelle par une demande adressée par voie postale le 10 juillet 2008, ainsi qu'en fait foi le cachet du bureau de poste d'émission ; que cette demande, présentée avant l'expiration du délai de pourvoi en cassation, a eu pour effet d'interrompre ce délai ; que la décision, par laquelle l'intéressée a été admise au bénéfice de l'aide juridictionnelle, lui a été notifiée au plus tôt le 8 janvier 2010 ; qu'ainsi, son pourvoi, présenté dès le 8 février 2010, n'est pas tardif ; que, par suite, la fin de non-recevoir opposée par le ministre de la défense doit être écartée ;<br/>
<br/>
              Sur le pourvoi de Mme B :<br/>
<br/>
              5. Considérant qu'aux termes du premier alinéa de l'article 61-1 de la Constitution : " Lorsque, à l'occasion d'une instance en cours devant une juridiction, il est soutenu qu'une disposition législative porte atteinte aux droits et libertés que la Constitution garantit, le Conseil constitutionnel peut être saisi de cette question sur renvoi du Conseil d'Etat ou de la Cour de cassation " ; qu'aux termes du deuxième alinéa de son article 62 : " Une disposition déclarée inconstitutionnelle sur le fondement de l'article 61-1 est abrogée à compter de la publication de la décision du Conseil constitutionnel ou d'une date ultérieure fixée par cette décision. Le Conseil constitutionnel détermine les conditions et limites dans lesquelles les effets que la disposition a produits sont susceptibles d'être remis en cause " ; qu'enfin, aux termes du troisième alinéa du même article : " Les décisions du Conseil constitutionnel ne sont susceptibles d'aucun recours. Elles s'imposent aux pouvoirs publics et à toutes les autorités administratives et juridictionnelles " ; <br/>
<br/>
              6. Considérant qu'il résulte des dispositions précitées de l'article 62 de la Constitution qu'une disposition législative déclarée contraire à la Constitution sur le fondement de l'article 61-1 n'est pas annulée rétroactivement mais abrogée pour l'avenir à compter de la publication de la décision du Conseil constitutionnel ou d'une date ultérieure fixée par cette décision ; que, par sa décision n° 2010-108 QPC en date du 25 mars 2011, le Conseil constitutionnel a jugé que " si, en principe, la déclaration d'inconstitutionnalité doit bénéficier à l'auteur de la question prioritaire de constitutionnalité et la disposition déclarée contraire à la Constitution ne peut être appliquée dans les instances en cours à la date de la publication de la décision du Conseil constitutionnel, les dispositions de l'article 62 de la Constitution réservent à ce dernier le pouvoir tant de fixer la date de l'abrogation et reporter dans le temps ses effets que de prévoir la remise en cause des effets que la disposition a produits avant l'intervention de cette déclaration " ;<br/>
<br/>
              7. Considérant que, lorsque le Conseil constitutionnel, après avoir abrogé une disposition déclarée inconstitutionnelle, use du pouvoir que lui confèrent les dispositions précitées, soit de déterminer lui-même les conditions et limites dans lesquelles les effets que la disposition a produits sont susceptibles d'être remis en cause, soit de décider que le législateur aura à prévoir une application aux instances en cours des dispositions qu'il aura prises pour remédier à l'inconstitutionnalité constatée, il appartient au juge, saisi d'un litige relatif aux effets produits par la disposition déclarée inconstitutionnelle, de les remettre en cause en écartant, pour la solution de ce litige, le cas échéant d'office, cette disposition, dans les conditions et limites fixées par le Conseil constitutionnel ou le législateur ; <br/>
<br/>
              8. Considérant que, par sa décision n° 2010-1 QPC du 28 mai 2010, le Conseil constitutionnel a déclaré contraires à la Constitution les dispositions de l'article 68 de la loi du 30 décembre 2002 de finances rectificative pour 2002, à l'exception de celles de son paragraphe VII ; qu'il a jugé que : " afin de permettre au législateur de remédier à l'inconstitutionnalité constatée, l'abrogation des dispositions précitées prendra effet à compter du 1er janvier 2011 ; afin de préserver l'effet utile de la présente décision à la solution des instances actuellement en cours, il appartient, d'une part, aux juridictions de surseoir à statuer jusqu'au 1er janvier 2011 dans les instances dont l'issue dépend de l'application des dispositions déclarées inconstitutionnelles et, d'autre part, au législateur de prévoir une application des nouvelles dispositions à ces instances en cours à la date de la présente décision " ;<br/>
<br/>
              9. Considérant que, à la suite de cette décision, l'article 211 de la loi du 29 décembre 2010 de finances pour 2011 a défini de nouvelles dispositions pour le calcul des pensions militaires d'invalidité, des pensions civiles et militaires de retraite et des retraites du combattant servies aux ressortissants des pays ou territoires ayant appartenu à l'Union française ou à la Communauté ou ayant été placés sous le protectorat ou sous la tutelle de la France et abrogé plusieurs dispositions législatives, notamment celles de l'article 71 de la loi du 26 décembre 1959 portant loi de finances pour 1960 ; que, par ailleurs, son paragraphe VI prévoit que " le présent article est applicable aux instances en cours à la date du 28 mai 2010, la révision des pensions prenant effet à compter de la date de réception par l'administration de la demande qui est à l'origine de ces instances " ; qu'enfin, aux termes du XI du même article : " Le présent article entre en vigueur au 1er janvier 2011 " ;<br/>
<br/>
              10. Considérant que, comme il a été dit, le Conseil constitutionnel a jugé qu'il appartenait au législateur de prévoir une application aux instances en cours à la date de sa décision des dispositions qu'il adopterait en vue de remédier à l'inconstitutionnalité constatée ; que l'article 211 de la loi de finances pour 2011 ne se borne pas à déterminer les règles de calcul des pensions servies aux personnes qu'il mentionne, mais abroge aussi des dispositions qui définissent, notamment, les conditions dans lesquelles est ouvert le droit à une pension de réversion ; qu'ainsi, alors même qu'il mentionne seulement la " révision des pensions ", le paragraphe VI de l'article 211 précité doit être regardé comme s'appliquant aussi aux demandes de pension de réversion ;<br/>
<br/>
              11. Considérant que, pour statuer, par l'arrêt attaqué, sur le droit à pension de Mme B, la cour régionale des pensions de Paris s'est exclusivement fondée sur les dispositions de l'article 68 de la loi de finances rectificative pour 2002 ; qu'afin de préserver l'effet utile de la décision précitée du Conseil constitutionnel à la solution de l'instance ouverte par la demande de Mme B, en permettant au juge du fond de remettre en cause, dans les conditions et limites définies par le paragraphe VI de l'article 211 de la loi de finances pour 2011, les effets produits par les dispositions mentionnées ci-dessus, il incombe au juge de cassation d'annuler l'arrêt attaqué ;<br/>
<br/>
              Sur les conclusions présentées par la SCP Delaporte-Briard-Trichet au titre des dispositions de l'article 37 de la loi du 10 juillet 1991 : <br/>
<br/>
              12. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées sur le fondement des dispositions de l'article 37 de la loi du 10 juillet 1991 par la SCP Delaporte-Briard-Trichet, avocat de Mme B ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour régionale des pensions de Paris du 1er février 2008 est annulé. <br/>
Article 2 : L'affaire est renvoyée devant la cour régionale des pensions de Versailles. <br/>
Article 3 : Les conclusions de la SCP Delaporte-Briard-Trichet, avocat de Mme B, présentées au titre des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à Mme Bandi B et au ministre de la défense. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
