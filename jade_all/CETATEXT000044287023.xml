<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044287023</ID>
<ANCIEN_ID>JG_L_2017_12_000000415376</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/28/70/CETATEXT000044287023.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 07/12/2017, 415376, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415376</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:415376.20171207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
               Par une requête et un mémoire en réplique, enregistrés les 31 octobre et 29 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, la Confédération générale du travail demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
               1°) d'ordonner la suspension de l'exécution de l'ordonnance n° 2017-1386 du 22 septembre 2017 relative à la nouvelle organisation du dialogue social et économique dans l'entreprise et favorisant l'exercice et la valorisation des responsabilités syndicales ;<br/>
<br/>
               2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
               La Confédération générale du travail soutient que :<br/>
               - il y a urgence à en suspendre l'exécution, dès lors que, d'une part, la suppression du comité d'hygiène, de sécurité et des conditions de travail et la réduction des attributions et des moyens d'action et financiers des nouveaux comités sociaux et économiques auront pour effet la dégradation de la santé et de la sécurité des salariés, d'autre part, l'impossibilité de demander l'organisation d'élections professionnelles dans les six mois suivant l'établissement d'un procès-verbal de carence entraînera une carence de long terme dans la représentation salariale, enfin, la disparition d'institutions représentatives du personnel porte une atteinte grave et immédiate aux intérêts qu'elle entend défendre ;<br/>
               - il existe un doute sérieux quant à la légalité de l'ordonnance contestée ; <br/>
               - l'ordonnance est entachée d'un vice d'incompétence, dès lors que ses dispositions ne correspondent ni au texte du projet soumis par le Gouvernement au Conseil d'Etat, ni au texte résultant de l'avis émis par le Conseil d'Etat ;<br/>
               - à titre principal, la loi du 15 septembre 2017 méconnaît les stipulations des articles 2, 8 et 11 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et de l'article 7 du pacte international relatif aux droits économiques, sociaux et culturels en habilitant le Gouvernement, d'une part, à fusionner en une seule instance les délégués du personnel, le comité d'entreprise et le comité d'hygiène, de sécurité et des conditions de travail, sans prévoir la possibilité de maintenir par accord collectif ces trois institutions, et, d'autre part, à définir les conditions dans lesquelles une commission spécifique au sein de cette instance traitera les questions d'hygiène, de sécurité et des conditions de travail ;<br/>
               - à titre subsidiaire, l'ordonnance contestée méconnaît elle-même ces stipulations, pour les mêmes raisons ;<br/>
               - la loi d'habilitation méconnaît les dispositions des articles 10 et 11 de la directive 89/391/CEE du 12 juin 1989, dès lors que les membres du nouveau comité social et économique ou de son émanation ne seront, par nature, pas des représentants ayant une fonction spécifique en matière de protection de la sécurité et de la santé des travailleurs ;<br/>
               - la suppression du comité d'hygiène, de sécurité et des conditions de travail méconnaît le droit à la santé des salariés et leur droit de participation à la détermination collective des conditions de travail dans l'entreprise, protégés par les huitième et onzième alinéas du préambule de la Constitution de 1946, dès lors que cette suppression n'est pas assortie des garanties nécessaires dans le cadre de la création du comité social et économique ;<br/>
               - l'article 1er de l'ordonnance est entaché d'une incompétence négative et d'une méconnaissance du champ d'habilitation de la loi du 15 septembre 2017, dès lors qu'il ne renforce pas les conditions d'accès à la formation des représentants des salariés et n'améliore pas les outils de lutte contre les discriminations syndicales, en réduisant le champ d'application du droit d'alerte ;<br/>
               - le nouvel article L. 2312-5 du code du travail méconnaît le principe d'égalité, en ce qu'il institue une différence de traitement, en premier lieu, entre le comité social et économique des entreprises de plus de cinquante salariés et celui des entreprises de onze à quarante-neuf salariés tant au regard des prérogatives des membres de ces comités que de la personnalité civile de ces derniers, et, en deuxième lieu, entre les membres des comités sociaux et économiques chargés des attributions en matière de sécurité, de santé et de conditions de travail dans les entreprises de onze à deux cent quatre-vingt-dix-neuf salariés et les membres de la commission en charge de ces attributions au sein des comités sociaux et économiques des entreprises de plus de trois cents salariés ;<br/>
               - le nouvel article L. 2314-8 du code du travail porte atteinte au principe de participation des salariés à la détermination des conditions de travail énoncé au huitième alinéa du préambule de la Constitution de 1946 et à la liberté syndicale, garantie par le sixième alinéa de ce préambule et l'article 11 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, en ce qu'il prive les salariés et les organisations syndicales de la possibilité de demander l'organisation d'élections professionnelles dans les six mois suivant l'établissement d'un procès-verbal de carence ; <br/>
               - cet article méconnaît le principe d'intelligibilité et de clarté de la norme en ce qu'il est imprécis sur le terme de l'interdiction d'organiser des élections professionnelles après l'établissement d'un procès-verbal de carence ;<br/>
               - le nouvel article L. 2314-30 du code du travail, par l'exigence de parité qu'il énonce pour les élections professionnelles, porte à la liberté syndicale, garantie par le sixième alinéa de ce préambule et l'article 11 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, une atteinte disproportionnée au regard de l'objectif poursuivi ;<br/>
               - le nouvel article L. 2314-33 du code du travail méconnaît le principe de participation énoncé par le huitième alinéa du préambule de la Constitution de 1946, en ce qu'il limite à trois le nombre de mandats successifs des membres des comités sociaux et économiques, excepté dans les entreprises de moins de 50 salariés, sans possibilité d'y déroger par un accord collectif ;<br/>
               - cet article méconnaît la liberté syndicale, garantie par le sixième alinéa du préambule de la Constitution de 1946, l'article 11 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et l'article 3 de la convention n° 87 de l'Organisation internationale du travail, en ce que la limitation du nombre de mandats successifs dans les comités sociaux et économiques prive les organisations syndicales de la possibilité de choisir librement les représentants aptes à se présenter aux élections professionnelles ;<br/>
               - le nouvel article L. 2315-80 du code du travail porte atteinte au droit à la protection de la santé des salariés et au droit de participation des salariés à la détermination des conditions de travail dans l'entreprise, dès lors qu'il met à la charge des comités sociaux et économiques 20 % du coût de l'expertise décidée par l'institution représentative du personnel, s'agissant notamment du recours à l'expertise en cas de projet important modifiant les conditions de santé et de sécurité ou les conditions de travail prévu au 2° du nouvel article L. 2315-96 ;<br/>
               - cet article méconnaît le principe d'égalité en ce qu'il institue une différence injustifiée entre les salariés et leurs représentants d'une entreprise dont le comité économique et social bénéficie d'un budget suffisant pour participer au financement des expertises et ceux dont le comité économique et social ne le pourra pas ;<br/>
               - les nouveaux articles L. 2321-1 à L. 2321-10 du code du travail méconnaissent les principes d'intelligibilité et de clarté de la norme en ce qu'ils ne permettent pas de déterminer, d'une part, la composition des conseils d'entreprise, le mode de désignation de leurs membres et, d'autre part, le mode de calcul de la validité d'un accord collectif d'entreprise conclu par des membres du conseil d'entreprise si aucun d'eux ne dispose d'un score personnel ;<br/>
               - ces articles méconnaissent la liberté syndicale garantie par le sixième alinéa du préambule de la Constitution de 1946 et l'article 11 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales en ce que certaines organisations syndicales réunissant au moins 50 % des suffrages peuvent décider, par accord collectif, de confier l'exercice du droit à la négociation collective à une institution élue du personnel, le conseil d'entreprise, ce qui prive les organisations syndicales de ce droit, à l'exception des accords relatifs aux élections professionnelles et à un plan de sauvegarde de l'emploi.  <br/>
<br/>
               Par un mémoire en défense, enregistré le 27 novembre 2017, la ministre du travail conclut au rejet de la requête. Elle soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par la requérante ne sont pas de nature à faire naître un doute sérieux sur la légalité de l'ordonnance contestée.<br/>
<br/>
<br/>
               Après avoir convoqué à une audience publique, d'une part, la Confédération générale du travail, d'autre part, le Premier ministre et la ministre du travail ; <br/>
<br/>
               Vu le procès-verbal de l'audience publique du jeudi 30 novembre 2017 à 15 heures au cours de laquelle ont été entendus :<br/>
<br/>
               - les représentants de la Confédération générale du travail ;<br/>
<br/>
               - les représentants de la ministre du travail ;<br/>
<br/>
               et à l'issue de laquelle le juge des référés a reporté la clôture de l'instruction au vendredi 1er décembre 2017 à 18 heures ;<br/>
<br/>
<br/>
               Vu le mémoire, enregistré le 30 novembre 2017, par lequel la Confédération générale du travail persiste dans ses précédentes écritures ;<br/>
<br/>
               Vu le mémoire, enregistré le 1er décembre 2017, par lequel la ministre du travail persiste dans ses précédentes écritures ;<br/>
<br/>
               Vu les autres pièces du dossier ;<br/>
<br/>
               Vu : <br/>
               - la Constitution, notamment son article 38 ;<br/>
               - le pacte international relatif aux droits économiques, sociaux et culturels du 19 décembre 1966 ;<br/>
               - la convention n° 87 de l'Organisation internationale du travail ;<br/>
               - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
               - la directive 89/391/CEE du 12 juin 1989 ;<br/>
               - le code du travail ;<br/>
               - la loi n° 2017-1340 du 15 septembre 2017 ;<br/>
               - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
               Considérant ce qui suit :<br/>
               1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
               2. La Confédération générale du travail doit être regardée, ainsi que cela a été confirmé au cours de l'audience publique, comme demandant au juge des référés du Conseil d'Etat, statuant sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de l'article 1er de l'ordonnance du 22 septembre 2017 relative à la nouvelle organisation du dialogue social et économique, en ce qu'il introduit dans le code du travail des articles relatifs, d'une part, à la fusion des délégués du personnel, du comité d'entreprise et du comité d'hygiène, de sécurité et des conditions de travail en une instance de représentation du personnel unique dénommée comité social et économique, et, d'autre part, à la possibilité d'instituer par accord d'entreprise un conseil d'entreprise exerçant l'ensemble des attributions de ce comité et seul compétent pour négocier, conclure et réviser les conventions et accords d'entreprise ou d'établissement, à l'exception des accords qui sont soumis à des règles spécifiques de validité.<br/>
<br/>
               3. Toutefois, les dispositions de l'article 1er de l'ordonnance contestée relatives aux comités économiques et sociaux prévoient l'intervention de nombreux décrets en Conseil d'Etat et décrets simples pour préciser, notamment, la composition de ces comités, leurs attributions, leurs moyens et les règles de leur fonctionnement, ainsi que les modalités de leur mise en place. En l'absence de tels décrets, et nonobstant la circonstance que les dispositions du I de l'article 9 de l'ordonnance prévoient leur entrée en vigueur au plus tard le 1er janvier 2018, ces dispositions de l'article 1er ne sont pas applicables. Par suite, il n'apparaît pas, en l'état de l'instruction, qu'elles portent aux intérêts que la Confédération générale du travail entend défendre une atteinte suffisamment grave et immédiate pour que la condition d'urgence posée par l'article L. 521-1 du code de justice administrative soit regardée comme remplie.   <br/>
<br/>
               4. En second lieu, la Confédération générale du travail ne fait état d'aucun élément de nature à caractériser l'urgence qu'il y aurait à suspendre l'exécution des dispositions de l'article 1er de l'ordonnance contestée relatives au conseil d'entreprise.<br/>
<br/>
               5. Il résulte de tout ce qui précède que la condition d'urgence posée par les dispositions de l'article L. 521-1 du code de justice administrative ne peut être regardée comme satisfaite. Par suite, et sans qu'il soit besoin de statuer sur l'existence d'un doute sérieux quant à la légalité de l'ordonnance contestée, la requête présentée par la Confédération générale du travail doit être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la Confédération générale du travail est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à la Confédération générale du travail, au Premier ministre et à la ministre du travail.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
