<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028700152</ID>
<ANCIEN_ID>JG_L_2014_03_000000372688</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/70/01/CETATEXT000028700152.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème SSJS, 07/03/2014, 372688, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-03-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372688</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:372688.20140307</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 8 octobre 2013 et 6 janvier 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Union Technique du Bâtiment, dont le siège est 159 avenue Jean Lolive à Pantin Cedex (93695) ; la société Union Technique du Bâtiment demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10PA04469 du 31 juillet 2013 par lequel la cour administrative d'appel de Paris n'a fait que partiellement droit à sa requête tendant à la réformation du jugement n° 0700093/6-2 du 29 juin 2010 du tribunal administratif de Paris condamnant la société Paris Habitat-OPH à réparer son préjudice ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ; <br/>
<br/>
              3°) de mettre à la charge de la société Paris Habitat-OPH la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des marchés publics ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de la société Union Technique du Bâtiment  ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux " ;<br/>
<br/>
              2. Considérant que pour demander l'annulation de l'arrêt attaqué, la société Union Technique du Bâtiment soutient que la cour administrative d'appel de Paris a omis de statuer sur ses conclusions tendant à la capitalisation des intérêts ; que la cour a omis de statuer sur sa demande tendant à ce que le jugement du tribunal administratif de Paris soit infirmé en tant qu'il n'avait pas pris en considération la taxe sur la valeur ajoutée sur la somme due par Paris Habitat-OPH au titre du marché " Riquet-Tanger " ; que la cour a entaché son arrêt de contradiction de motifs en jugeant au point 27 qu'elle avait droit à une somme de 179 694,94 euros avant d'omettre d'inclure cette somme dans le montant total de l'indemnisation figurant au point 28 ; que la cour a commis une erreur de droit et dénaturé les faits en jugeant que des pénalités de retard avaient pu lui être infligées sans mise en demeure préalable ou rappel et alors que le planning d'exécution était devenu caduc ; que la cour a dénaturé les pièces du dossier et notamment les stipulations contractuelles et commis une erreur de droit en jugeant que des pénalités de retard pouvaient lui être infligées pour dépassement des délais d'exécution prévus au contrat alors que du fait d'un bouleversement de l'économie de ce contrat, les délais d'exécution qu'il prévoyait étaient devenus caducs ; que la cour n'a pas répondu au moyen opérant tiré de ce que des pénalités de retard ne pouvaient lui être infligées en l'absence de toute constatation d'un retard par le maître d'oeuvre et ainsi entaché son arrêt d'insuffisante motivation et, à titre subsidiaire, commis une erreur de droit et dénaturé les faits ; que la cour a commis une erreur de droit et dénaturé les stipulations contractuelles en jugeant que le retard d'exécution devait être évalué compte tenu du terme du délai d'exécution contractuel alors qu'il ne pouvait courir qu'à compter du constat du retard par le maître d'oeuvre ; que la cour a commis une erreur de droit et dénaturé les stipulations contractuelles en prenant en considération la date effective de réception pour calculer les pénalités ; que la cour a commis une erreur de droit en jugeant que des pénalités de retard puissent lui être infligées alors que les retards étaient partiellement imputables au maître d'ouvrage ; que la cour a entaché son arrêt d'insuffisante motivation, d'erreur de droit et de dénaturation à avoir jugé que les pénalités n'étaient pas d'un montant manifestement excessif ; que la cour a dénaturé les faits en jugeant que le préjudice subi du fait des retards de paiement n'était pas distinct de celui indemnisé par les intérêts moratoires ;<br/>
<br/>
              3. Considérant qu'eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi dirigées contre l'arrêt en tant qu'il a omis de statuer sur les demandes de capitalisation des intérêts dus au titre des deux marchés en litige ; qu'en revanche, s'agissant des autres conclusions du pourvoi, aucun de ces moyens n'est de nature à permettre leur admission ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions du pourvoi de la société Union Technique du Bâtiment dirigées contre l'arrêt en tant qu'il a omis de statuer sur les demandes de capitalisation des intérêts dus au titre des deux marchés en litige sont admises.<br/>
Article 2 : Le surplus des conclusions du pourvoi de la société Union Technique du Bâtiment n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à la société Union Technique du Bâtiment. <br/>
Copie en sera adressée pour information à la société Paris Habitat-OPH.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
