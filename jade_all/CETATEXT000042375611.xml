<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042375611</ID>
<ANCIEN_ID>JG_L_2020_09_000000423986</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/37/56/CETATEXT000042375611.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 28/09/2020, 423986</TITRE>
<DATE_DEC>2020-09-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423986</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD ; SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:423986.20200928</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Bastia de condamner le centre hospitalier de Bastia à lui verser la somme de 168 326 euros en réparation du préjudice qu'il estime avoir subi du fait de l'absence de versement de ses indemnités de fin de contrat. Par un jugement n° 1500323 du 7 juillet 2016, le tribunal administratif a condamné le centre hospitalier de Bastia à lui verser la somme de 44 236 euros. <br/>
<br/>
              Par un arrêt n° 16MA03423 du 10 juillet 2018, la cour administrative d'appel de Marseille a rejeté l'appel formé par M. A... contre ce jugement et l'appel incident du centre hospitalier de Bastia. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 10 septembre et 10 décembre 2018 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il rejette son appel ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier de Bastia la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code du travail ;<br/>
              - la loi n° 68-1250 du 31 décembre 1968 ;<br/>
              - la loi n° 2004-806 du 9 août 2004 ;<br/>
              - le décret n° 93-701 du 27 mars 1993 ;<br/>
              - le décret n° 2005-207 du 1er mars 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat de M. A... et à la SCP Baraduc, Duhamel, Rameix, avocat du centre hospitalier de Bastia.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A... a exercé les fonctions de médecin urgentiste au centre hospitalier de Bastia dans le cadre de contrats à durée déterminée régulièrement renouvelés entre 1998 et 2014, d'abord en qualité d'assistant des hôpitaux entre 1998 et 2002, puis, à compter du 1er décembre 2002, en qualité de praticien contractuel. Ayant atteint la limite d'âge le 10 octobre 2012, il a obtenu deux prolongations d'activité d'un an, jusqu'au 10 octobre 2014. Il a alors demandé au centre hospitalier de Bastia le paiement de toutes les indemnités de fin de contrat qu'il estimait lui être dues, au titre des contrats qui avaient été conclus avec le centre hospitalier depuis 1998. L'établissement ayant refusé de faire droit à sa demande, il a obtenu, par un jugement du 7 juillet 2016 du tribunal administratif de Bastia, la condamnation du centre hospitalier à lui verser la somme de 44 236 euros, correspondant aux indemnités de fin de contrat pour les contrats couvrant la période du 1er décembre 2008 au 10 octobre 2012. Il se pourvoit en cassation contre l'arrêt du 10 juillet 2018 par lequel la cour administrative d'appel de Marseille a rejeté son appel formé contre ce jugement en tant qu'il rejette le surplus de sa demande, tendant au versement des indemnités de fin de contrat pour les contrats couvrant la période antérieure au 1er décembre 2008 et la période postérieure au 10 octobre 2012.<br/>
<br/>
              Sur les droits de M. A... à bénéficier de l'indemnité de fin de contrat pour les différentes périodes en litige :<br/>
<br/>
              2. L'indemnité de fin de contrat est prévue par les dispositions de l'article L. 1243-8 du code du travail, aux termes desquelles : " Lorsque, à l'issue d'un contrat de travail à durée déterminée, les relations contractuelles de travail ne se poursuivent pas par un contrat à durée indéterminée, le salarié a droit, à titre de complément de salaire, à une indemnité de fin de contrat destinée à compenser la précarité de sa situation./ Cette indemnité est égale à 10 % de la rémunération totale brute versée au salarié./ Elle s'ajoute à la rémunération totale brute due au salarié. Elle est versée à l'issue du contrat en même temps que le dernier salaire et figure sur le bulletin de salaire correspondant ". Dans sa rédaction alors applicable, l'article R. 6152-418 du code de la santé publique dispose que : " Les dispositions du code du travail et celles du code de la sécurité sociale sont applicables aux praticiens contractuels en tant qu'elles sont relatives (...) à l'indemnité prévue à l'article L. 1243-8 du code du travail (...) ".<br/>
<br/>
              3. En premier lieu, il résulte des termes mêmes de l'article 9 du décret du 27 mars 1993 relatif aux praticiens contractuels des établissements publics de santé, dont les dispositions ont été reprises, à compter du 26 juillet 2005, à l'article R. 6152-418 du code de la santé publique, que les dispositions de l'article L. 1243-8 du code du travail ne sont pas applicables aux assistants des hôpitaux. M. A... ne pouvait donc prétendre au versement de l'indemnité de fin de contrat au titre des contrats qui l'ont lié au centre hospitalier de Bastia en qualité d'assistant des hôpitaux entre septembre 1998 et novembre 2002.<br/>
<br/>
              4. En deuxième lieu, aux termes de l'article 1er de la loi du 31 décembre 1968 : " Sont prescrites, au profit de l'Etat (..) toutes créances qui n'ont pas été payées dans un délai de quatre ans à partir du premier jour de l'année suivant celle au cours de laquelle les droits ont été acquis ". Lorsqu'un litige oppose un agent public à son administration sur le montant des rémunérations auxquelles il a droit, le fait générateur de la créance se trouve en principe dans les services accomplis par l'intéressé. Dans ce cas, le délai de prescription de la créance relative à ces services court à compter du 1er janvier de l'année suivant celle au titre de laquelle l'agent aurait dû être rémunéré. L'indemnité de fin de contrat devant être versée à la fin de chaque contrat, le délai de prescription des indemnités dues par le centre hospitalier de Bastia à M. A... au titre des contrats successivement conclus en qualité de praticien contractuel à compter du 1er décembre 2002 a couru à compter du 1er janvier suivant la date de fin de chacun de ces contrats. La demande indemnitaire de M. A... ayant été présentée en 2014, le centre hospitalier de Bastia était fondé à lui opposer, devant les juges du fond, l'exception de prescription quadriennale pour les indemnités se rapportant aux contrats couvrant la période du 1er décembre 2002 au 1er décembre 2008.<br/>
<br/>
              5. En troisième lieu, aux termes de l'article 135 de la loi du 9 août 2004 relative à la politique de santé publique : " A compter du 1er janvier 2004, les praticiens visés à l'article L. 6152-1 du code de la santé publique peuvent être autorisés à prolonger leur activité dans un établissement de santé après la limite d'âge qui leur est applicable, dans la limite de trente-six mois maximum, sous réserve d'aptitude médicale ". Aux termes de l'article 1er du décret du 1er mars 2005 relatif à la prolongation d'activité des personnels médicaux hospitaliers, pris pour l'application de ces dispositions : " Peuvent être autorisés, dans la limite maximum de trente-six mois, à prolonger leur activité au-delà de la limite d'âge qui leur est applicable les praticiens hospitaliers à temps plein, les praticiens des hôpitaux à temps partiel, les praticiens contractuels, les assistants des hôpitaux et les praticiens attachés régis respectivement par les sections 1, 2, 4, 5 et 6 du chapitre II du titre V du livre Ier de la sixième partie du code de la santé publique, les médecins et pharmaciens régis par le décret du 6 mai 1995 susvisé ". Enfin, l'article 3 de ce décret dispose que : " (...) La prolongation d'activité est accordée, au vu du certificat médical d'aptitude physique et mentale délivré par un médecin et produit par l'intéressé, par périodes de six mois minimum ou un an maximum par l'autorité investie du pouvoir de nomination (...) ".<br/>
<br/>
              6. L'indemnité de fin de contrat prévue à l'article L. 1243-8 du code du travail étant destinée à compenser la précarité de la situation du salarié dont les relations contractuelles avec son employeur ne se poursuivent pas, à l'issue d'un contrat à durée déterminée, par un contrat à durée indéterminée, elle ne saurait s'appliquer  aux contrats passés avec les personnels médicaux hospitaliers autorisés à prolonger leur activité au-delà de la limite d'âge, dès lors que de tels contrats sont, dès leur signature, en vertu des dispositions citées ci-dessus, insusceptibles de se poursuivre par un contrat à durée indéterminée.<br/>
<br/>
              7. M. A... ne pouvait donc prétendre au versement de l'indemnité de fin de contrat au titre des contrats qui l'ont lié au centre hospitalier de Bastia dans le cadre de sa prolongation d'activité, après le 10 octobre 2012 <br/>
<br/>
              Sur l'arrêt attaqué :<br/>
<br/>
              8. Pour rejeter l'appel de M. A..., la cour administrative d'appel a estimé que, tout en exécutant son dernier contrat jusqu'à son terme, le 10 octobre 2014, l'intéressé avait fait part au centre hospitalier de son refus de renouvellement de ce contrat dans des conditions qui équivalaient à une rupture anticipée du contrat. Elle en a déduit que cette rupture privait l'intéressé de tout droit au versement des indemnités de fin de contrat, pour l'ensemble des périodes pour lesquelles il les sollicitait.<br/>
<br/>
              9. Sans qu'il soit besoin de se prononcer sur le bien-fondé des moyens par lesquels M. A... conteste le raisonnement tenu par la cour administrative d'appel, les motifs mentionnés aux points 3 à 7, qui n'impliquent l'appréciation d'aucune circonstance de fait, doivent être substitués au motif retenu par la cour dans son arrêt, dont ils justifient légalement le dispositif.<br/>
<br/>
              10. Par suite, M. A... n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque. Son pourvoi doit ainsi être rejeté, y compris, par voie de conséquence, les conclusions qu'il présente au titre de l'article L. 761-1 du code de justice administrative. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées à ce même titre par le centre hospitalier de Bastia.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de M. A... est rejeté.<br/>
<br/>
Article 2 : Les conclusions présentées par le centre hospitalier de Bastia au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. B... A... et au centre hospitalier de Bastia.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">18-04-02-04 COMPTABILITÉ PUBLIQUE ET BUDGET. DETTES DES COLLECTIVITÉS PUBLIQUES - PRESCRIPTION QUADRIENNALE. RÉGIME DE LA LOI DU 31 DÉCEMBRE 1968. POINT DE DÉPART DU DÉLAI. - LITIGE RELATIF AUX RÉMUNÉRATIONS D'UN AGENT - 1) PRINCIPE [RJ1] - FAIT GÉNÉRATEUR DE LA CRÉANCE CONSTITUÉ PAR LES SERVICES ACCOMPLIS PAR L'INTÉRESSÉ - CONSÉQUENCE - DÉLAI COURANT À COMPTER DU 1ER JANVIER DE L'ANNÉE SUIVANT CELLE AU COURS DE LAQUELLE CES SERVICES AURAIENT DÛ ÊTRE RÉMUNÉRÉS - 2) APPLICATION AUX INDEMNITÉS DE FIN DE CONTRAT - CAS DE CDD RENOUVELÉS - DÉLAI DE PRESCRIPTION COURANT À COMPTER DU 1ER JANVIER SUIVANT LA DATE DE FIN DE CHACUN DES CONTRATS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-11-01-01 FONCTIONNAIRES ET AGENTS PUBLICS. DISPOSITIONS PROPRES AUX PERSONNELS HOSPITALIERS. PERSONNEL MÉDICAL. RÈGLES COMMUNES. - INDEMNITÉ DE FIN DE CONTRAT (ART. L. 1243-8 DU CODE DU TRAVAIL) - CHAMP D'APPLICATION - EXCLUSION - CONTRATS PASSÉS AVEC LES PERSONNELS MÉDICAUX HOSPITALIERS AUTORISÉS À PROLONGER LEUR ACTIVITÉ AU-DELÀ DE LA LIMITE D'ÂGE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">36-12-03 FONCTIONNAIRES ET AGENTS PUBLICS. AGENTS CONTRACTUELS ET TEMPORAIRES. FIN DU CONTRAT. - INDEMNITÉ DE FIN DE CONTRAT (ART. L. 1243-8 DU CODE DU TRAVAIL) - CHAMP D'APPLICATION - EXCLUSION - CONTRATS PASSÉS AVEC LES PERSONNELS MÉDICAUX HOSPITALIERS AUTORISÉS À PROLONGER LEUR ACTIVITÉ AU-DELÀ DE LA LIMITE D'ÂGE.
</SCT>
<ANA ID="9A"> 18-04-02-04 1) Lorsqu'un litige oppose un agent public à son administration sur le montant des rémunérations auxquelles il a droit, le fait générateur de la créance se trouve en principe dans les services accomplis par l'intéressé. Dans ce cas, le délai de prescription de la créance relative à ces services court à compter du 1er janvier de l'année suivant celle au titre de laquelle l'agent aurait dû être rémunéré.,,,2) L'indemnité de fin de contrat devant être versée à la fin de chaque contrat, le délai de prescription des indemnités dues par un centre hospitalier au titre des contrats successivement conclus avec un praticien contractuel à compter du 1er décembre 2002 a couru à compter du 1er janvier suivant la date de fin de chacun de ces contrats. La demande indemnitaire du requérant ayant été présentée en 2014, le centre hospitalier était fondé à lui opposer, devant les juges du fond, l'exception de prescription quadriennale pour les indemnités se rapportant aux contrats couvrant la période du 1er décembre 2002 au 1er décembre 2008.</ANA>
<ANA ID="9B"> 36-11-01-01 L'indemnité de fin de contrat prévue à l'article L. 1243-8 du code du travail étant destinée à compenser la précarité de la situation du salarié dont les relations contractuelles avec son employeur ne se poursuivent pas, à l'issue d'un contrat à durée déterminée (CDD), par un contrat à durée indéterminée (CDI), elle ne saurait s'appliquer aux contrats passés avec les personnels médicaux hospitaliers autorisés à prolonger leur activité au-delà de la limite d'âge, dès lors que de tels contrats sont, dès leur signature, en vertu de l'article 135 de la loi n° 2004-806 du 9 août 2004 et des articles 1er et 3 du décret n° 2005-207 du 1er mars 2005, insusceptibles de se poursuivre par un CDI.</ANA>
<ANA ID="9C"> 36-12-03 L'indemnité de fin de contrat prévue à l'article L. 1243-8 du code du travail étant destinée à compenser la précarité de la situation du salarié dont les relations contractuelles avec son employeur ne se poursuivent pas, à l'issue d'un contrat à durée déterminée (CDD), par un contrat à durée indéterminée (CDI), elle ne saurait s'appliquer aux contrats passés avec les personnels médicaux hospitaliers autorisés à prolonger leur activité au-delà de la limite d'âge, dès lors que de tels contrats sont, dès leur signature, en vertu de l'article 135 de la loi n° 2004-806 du 9 août 2004 et des articles 1er et 3 du décret n° 2005-207 du 1er mars 2005, insusceptibles de se poursuivre par un CDI.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 1er juillet 2019, Ministre de l'action et des comptes publics c/ M.,, n° 413995, p. 266.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
