<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033204546</ID>
<ANCIEN_ID>JG_L_2016_10_000000394957</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/20/45/CETATEXT000033204546.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 06/10/2016, 394957, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-10-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394957</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Paul Bernard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:394957.20161006</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...C...et Mme A...C...ont, le 27 avril 2012, demandé au tribunal administratif de Cergy-Pontoise, d'une part, d'annuler la délibération du conseil municipal de la commune de Butry-sur-Oise du 17 octobre 2011 approuvant le plan local d'urbanisme de la commune, en tant que cette délibération a classé le terrain leur appartenant, cadastré AD 64, en zone naturelle, d'autre part, de condamner la commune à leur verser une indemnité de 5 000 euros en réparation du préjudice qu'elles estiment avoir subi. Par un jugement n° 1206799 du 11 juin 2013, le tribunal administratif a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 13VE02698 du 1er octobre 2015, la cour administrative d'appel de Versailles a rejeté l'appel formé par Mmes C...contre ce jugement.<br/>
<br/>
              Par un pourvoi, un mémoire complémentaire et un mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat le 1er décembre 2015 et les 29 février et 12 septembre 2016, Mmes C...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Butry-sur-Oise la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Paul Bernard, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Xavier Domino, Rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de Mmes C...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant, en premier lieu, qu'il ressort des pièces du dossier des juges du fond que Mme B...C...et Mme A...C...ont demandé au tribunal administratif de Cergy-Pontoise, d'une part, d'annuler pour excès de pouvoir la délibération du 17 octobre 2011 approuvant le plan local d'urbanisme de la commune de Butry-sur-Oise, d'autre part, de condamner la commune à leur verser une indemnité de 5 000 euros en réparation du préjudice qu'elles estimaient avoir subi ; que par jugement du 11 juin 2013, le tribunal administratif a rejeté leur demande, en jugeant notamment que les conclusions indemnitaires étaient irrecevables faute de demande préalable ; que l'appel formé par Mmes C...devant la cour administrative d'appel de Versailles n'a pas saisi le juge d'appel de conclusions dirigées contre la partie du jugement de première instance qui avait rejeté les conclusions indemnitaires ; que la cour n'avait, dès lors, pas à statuer sur cet aspect du litige de première instance ; qu'il s'ensuit que le moyen tiré de ce que l'arrêt attaqué serait irrégulier faute de s'être prononcé sur ce point ne peut qu'être écarté ; <br/>
<br/>
              2.	Considérant, en deuxième lieu, qu'aux termes de l'article R. 421-1 du code de justice administrative : " Sauf en matière de travaux publics, la juridiction ne peut être saisie que par voie de recours formé contre une décision, et ce, dans les deux mois à partir de la notification ou de la publication de la décision attaquée " ; que la délibération attaquée approuvant le plan local d'urbanisme de la commune de Butry-sur-Oise présente le caractère d'un acte réglementaire ; que la cour n'a pas commis d'erreur de droit en jugeant que le délai de recours à son encontre courait à compter de sa publication, dans les conditions fixées par le code de l'urbanisme ;<br/>
<br/>
              3.	Considérant, en troisième lieu, qu'aux termes du premier alinéa de l'article 18 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations, applicable à la date de la délibération attaquée et désormais codifié à l'article L. 110-1 du code des relations entre le public et l'administration : " Sont considérées comme des demandes au sens du présent chapitre les demandes et les réclamations, y compris les recours gracieux ou hiérarchiques, adressées aux autorités administratives " ; qu'aux termes de l'article 19 de la même loi, désormais codifié aux articles L. 112-3 et L. 112-6 du code des relations entre le public et l'administration : " Toute demande adressée à une autorité administrative fait l'objet d'un accusé de réception délivré dans des conditions définies par décret en Conseil d'Etat. (...) Les délais de recours ne sont pas opposables à l'auteur d'une demande lorsque l'accusé de réception ne lui a pas été transmis ou ne comporte pas les indications prévues par le décret mentionné au premier alinéa (...) " ; que, toutefois, lorsque la publication d'un acte suffit à faire courir le délai de recours contre cet acte à l'égard des tiers, indépendamment de toute notification, ces dispositions n'ont ni pour objet ni pour effet de faire obstacle à ce que, en cas de recours gracieux formé par ces tiers contre l'acte en cause, le délai de recours contentieux recommence à courir à leur égard à compter de l'intervention de la décision explicite ou implicite de rejet du recours gracieux, même en l'absence de délivrance d'un accusé de réception mentionnant les voies et délais de recours ; que, dès lors, la cour n'a pas commis d'erreur de droit en jugeant que la circonstance que le recours gracieux formé par les requérantes contre la délibération attaquée n'ait pas fait l'objet d'un accusé de réception mentionnant les voies et délais de recours n'avait pas eu pour effet de rendre inopposable à leur égard le délai de recours contentieux de deux mois à compter de la décision implicite de rejet née du silence gardé par le maire sur leur recours gracieux ;<br/>
<br/>
              4.	Considérant qu'il résulte de tout ce qui précède que Mmes C...ne sont pas fondées à demander l'annulation de l'arrêt qu'elles attaquent ;<br/>
<br/>
              5.	Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la commune de Butry-sur-Oise, qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mmes C...le versement à la commune d'une somme au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de Mmes C...est rejeté.<br/>
<br/>
Article 2 : Les conclusions de la commune de Butry-sur-Oise présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à Mme B...C..., Mme A...C...et à la commune de Butry-sur-Oise.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
