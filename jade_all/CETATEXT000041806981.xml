<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041806981</ID>
<ANCIEN_ID>JG_L_2020_03_000000436932</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/80/69/CETATEXT000041806981.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 16/03/2020, 436932, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-03-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436932</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Laurent Domingo</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:436932.20200316</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée (SAS) Roanne Energie Naturelle, à l'appui de son appel contre le jugement rejetant sa demande tendant à la réduction de la cotisation foncière des entreprises à laquelle elle a été assujettie au titre des années 2012 et 2013 et des pénalités correspondantes, a produit un mémoire, enregistré le 9 mars 2018 au greffe de la cour administrative d'appel de Lyon, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lequel elle soulève une question prioritaire de constitutionnalité.<br/>
<br/>
              Par un arrêt n° 18LY00930 du 19 décembre 2019, enregistré le 20 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Lyon, avant qu'il soit statué sur l'appel de la société Roanne Energie Naturelle, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution de l'article 1501 du code général des impôts. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des impôts, notamment son article 1501 ;<br/>
              - la loi nº 68-108 du 2 février 1968 ;<br/>
              - la loi n° 74-645 du 18 juillet 1974 ;<br/>
              - la loi n° 89-936 du 29 décembre 1989 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Domingo, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la société Roanne Energie Naturelle ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'État a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux. <br/>
<br/>
              2. Le premier alinéa du I de l'article 1501 du code général des impôts habilite le pouvoir réglementaire à fixer, par décret en Conseil d'Etat, des modalités particulières d'évaluation pour des catégories de locaux, établissements ou installations de caractère industriel ou commercial, lorsqu'il existe dans différentes communes des biens de cette nature présentant des caractéristiques analogues. En application de cette disposition, l'article 310 M de l'annexe II au code général des impôts a fixé des modalités particulières d'évaluation de la valeur locative applicables, notamment, aux établissements ou installations de caractère industriel ou commercial affectés à la production ou à la distribution publique d'énergie électrique. Aux termes du second alinéa du I de l'article 1501 : " Ces modalités d'évaluation ne sont pas applicables aux immobilisations visées au premier alinéa qui sont acquises ou créées à compter du 1er janvier 1974. Ces dernières sont évaluées conformément au dernier alinéa du 1 du II de l'article 1517 ". Le dernier alinéa de l'article 31 de la loi du 29 décembre 1989 de finances rectificatives pour 1989, duquel est issue cette disposition, précise qu'elle revêt un caractère interprétatif.<br/>
<br/>
              3. La société Roanne Energie Naturelle soutient qu'en ce qu'il exclut les immobilisations acquises ou créées après le 1er janvier 1974 du bénéfice de la méthode particulière d'évaluation prévue à l'article 310 M de l'annexe II au code général des impôts, le second alinéa du I de l'article 1501 de ce code aurait pour effet de soumettre des contribuables placés dans une situation identique à un traitement fiscal différent, sans que cette différence de traitement ne soit justifiée par un motif d'intérêt général et méconnaîtrait ainsi les principes d'égalité devant la loi fiscale et d'égalité devant les charges publiques. <br/>
<br/>
              4. Toutefois, si l'article 9 de la loi du 2 février 1968 relative aux évaluations servant de base à certains impôts directs locaux, codifié au premier alinéa de l'article 1501 du code général des impôts, a prévu, par dérogation à l'article 6 de cette même loi prévoyant une méthode de droit commun, codifiée à l'article 1499 du code général des impôts, pour procéder à l'évaluation de la valeur locative des immobilisations industrielles passibles de la taxe foncière sur les propriétés bâties, que des modalités particulières d'évaluation de ces mêmes immobilisations pourraient être retenues en cas d'existence dans différentes communes de biens de cette nature présentant des caractéristiques analogues, le dernier alinéa du II de l'article 2 de la loi du 18 juillet 1974 sur la mise à jour périodique de valeurs locatives servant de base aux impositions directes locales, codifié au dernier alinéa du 1 du II de l'article 1517 de ce même code, a prévu que les immobilisations industrielles passibles de la taxe foncière sur les propriétés bâties seraient, quelle que soit la date de leur acquisition, évaluées par l'administration d'après leur prix de revient conformément aux dispositions aujourd'hui codifiées à l'article 1499. L'article 26 de la loi du 29 décembre 1989 de finances rectificative pour 1989 a précisé, par des dispositions auxquelles le législateur a conféré un caractère interprétatif, que les méthodes particulières d'évaluation mentionnées au premier alinéa de l'article 1501 cessaient d'être applicables aux immobilisations acquises ou créées à compter du 1er janvier 1974. <br/>
<br/>
              5. Il résulte de ce qui précède que la différence de traitement entre les contribuables, au regard des règles d'évaluation des immobilisations industrielles passibles de la taxe foncière entrant dans leurs bases d'imposition, selon que ces immobilisations ont été acquises ou créées avant ou après le 1er juillet 1974 est inhérente à la succession de régimes juridiques dans le temps et n'est pas, par elle-même, contraire aux principes d'égalité devant la loi fiscale et d'égalité devant les charges publiques. <br/>
<br/>
              6. Il en résulte que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Par suite, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par la cour administrative d'appel de Lyon. <br/>
Article 2 : La présente décision sera notifiée à la société par actions simplifiée Roanne Energie Naturelle et au ministre de l'action et des comptes publics.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
