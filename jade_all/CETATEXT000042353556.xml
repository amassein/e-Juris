<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042353556</ID>
<ANCIEN_ID>JG_L_2020_09_000000426859</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/35/35/CETATEXT000042353556.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 21/09/2020, 426859</TITRE>
<DATE_DEC>2020-09-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426859</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD</AVOCATS>
<RAPPORTEUR>M. Vincent Daumas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:426859.20200921</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La commune de Bois-Guillaume a demandé au tribunal administratif de Rouen d'annuler, d'une part, la décision du 29 septembre 2014 par laquelle le préfet de la  Seine-Maritime a décidé de ramener la somme attribuée à la commune au titre de la dotation forfaitaire de la dotation globale de fonctionnement pour l'année 2014 au montant de 1 327 081 euros, d'autre part, l'arrêté du 1er octobre 2014 par lequel le préfet a fixé le reversement dû en conséquence par la commune au montant de 493 000 euros et, enfin, de condamner l'Etat à lui verser cette somme en réparation du dommage subi. Par un jugement n° 1500401 du 19 juillet 2016, le tribunal administratif de Rouen a annulé ces décisions et condamné l'Etat à verser à la commune de Bois-Guillaume une somme de 493 000 euros en réparation du dommage subi.<br/>
<br/>
              Par un arrêt n° 16DA01664, 17DA01928 du 6 novembre 2018, la cour administrative d'appel de Douai a joint l'appel formé par le ministre de l'intérieur contre ce jugement et la demande présentée par la commune de Bois-Guillaume, sur le fondement des articles L. 911-4 et suivants du code de justice administrative, tendant à l'exécution de ce même jugement. Elle a annulé le jugement du 19 juillet 2016, rejeté les conclusions présentées devant le tribunal administratif par la commune de Bois-Guillaume et dit n'y avoir lieu à statuer sur sa demande tendant à l'exécution du jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 7 janvier, 5 avril et 26 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, la commune de Bois-Guillaume demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions de première et seconde instances ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 93-1436 du 31 décembre 1993 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Daumas, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, au cabinet Briard, avocat de la commune de Bois-Guillaume ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que par un arrêté du 29 août 2011, le préfet de la Seine-Maritime a créé, à compter du 1er janvier 2012, une commune nouvelle issue de la fusion des communes de Bois-Guillaume et de Bihorel. Cet arrêté a été annulé par un jugement du tribunal administratif de Rouen du 18 juin 2013, devenu définitif, qui a cependant décidé, d'une part, de ne prononcer cette annulation qu'à compter du 31 décembre 2013, d'autre part, de réputer définitifs les effets produits par l'arrêté annulé avant cette date. Par un arrêté du 2 juin 2014, le préfet de la Seine-Maritime a fixé à la somme de 1 820 081 euros le montant dû pour l'année 2014 à la commune de Bois-Guillaume au titre de la dotation forfaitaire de la dotation globale de fonctionnement, en faisant application des dispositions de l'article L. 2334-12 du code général des collectivités territoriales relatives au calcul de la dotation forfaitaire en cas de division de communes. Estimant avoir commis une erreur de droit, il a procédé à un nouveau calcul et, par une décision du 29 septembre 2014, a fixé le nouveau montant de la dotation forfaitaire attribué à la commune pour l'année 2014 à 1 327 081 euros, puis, par un arrêté du 1er octobre 2014, a rendu la commune redevable, en conséquence, de la somme de 493 000 euros. Par un jugement du 19 juillet 2016, le tribunal administratif de Rouen a fait droit à la demande d'annulation des arrêtés des 29 septembre et 1er octobre 2014 présentée par la commune de Bois-Guillaume et condamné l'Etat à lui verser la somme de 493 000 euros en réparation du préjudice subi. La commune de Bois-Guillaume se pourvoit en cassation contre l'arrêt du 6 novembre 2018 par lequel la cour administrative d'appel de Douai, d'une part, faisant droit à l'appel du ministre de l'intérieur contre le jugement du 19 juillet 2016, a annulé ce jugement et rejeté les demandes présentées par la commune en première instance et, d'autre part, dit n'y avoir lieu à statuer sur la demande de cette dernière tendant à l'exécution du jugement.<br/>
<br/>
              2. Aux termes, d'une part, de l'article L. 2334-7 du code général des collectivités territoriales, dans sa version applicable au litige : " I. - A compter de 2005, la dotation forfaitaire comprend : / 1° Une dotation de base destinée à tenir compte des charges liées à l'importance de sa population. / A compter de 2011, cette dotation de base est égale pour chaque commune au produit de sa population par un montant de 64,46 euros par habitant à 128,93 euros par habitant en fonction croissante de la population de la commune, dans des conditions définies par décret en Conseil d'Etat. / 2° Une dotation proportionnelle à la superficie, égale à 3,22 euros par hectare à compter de 2011 (...) ; / 3° Les montants correspondant aux montants antérieurement perçus au titre du I du D de l'article 44 de la loi de finances pour 1999 (n° 98-1266 du 30 décembre 1998) et du 2° bis du II de l'article 1648 B du code général des impôts dans sa rédaction antérieure à la loi de finances pour 2004 (n° 2003-1311 du 30 décembre 2003). (...) / 4° Une garantie. Cette garantie est versée en 2005, le cas échéant, lorsque le montant prévu au a ci-dessous est supérieur aux montants mentionnés au b. Elle est égale en 2005 à la différence entre : / a. Le montant de dotation forfaitaire perçue en 2004 et indexée selon un taux de 1 % hors montants des compensations mentionnées au 3° ; / b. Et la somme de la dotation de base et de la dotation proportionnelle à la superficie calculées en application des 1° et 2° (...) ". <br/>
<br/>
              3. Aux termes, d'autre part, de l'article L. 2334-12 du même code, dans sa version applicable au litige : " En cas de division de communes, la dotation de base et la dotation proportionnelle à la superficie revenant à chaque commune sont calculées conformément à l'article L. 2334-7 en retenant sa nouvelle population et sa superficie. Les montants mentionnés aux 3° et 4° du I de l'article L. 2334-7 sont calculés au prorata de la population de chaque commune ". Il résulte de ces dernières dispositions, éclairées par les travaux préparatoires de la loi du 31 décembre 1993 portant réforme de la dotation globale de fonctionnement et modifiant le code des communes et le code général des impôts dont elles sont issues, qu'elles ne s'appliquent qu'aux situations résultant de décisions administratives de division de communes et non à celles résultant de l'annulation juridictionnelle d'une décision de fusion de communes. Est sans incidence à cet égard la circonstance que le juge aurait décidé, tout en prononçant l'annulation de la décision de fusion de communes, de réputer définitifs tout ou partie des effets produits par cette décision de fusion avant son annulation. <br/>
<br/>
              4. Par suite, en jugeant que les dispositions de l'article L. 2334-12 du code général des collectivités territoriales ne s'appliquaient pas au calcul de la dotation forfaitaire de la dotation globale de fonctionnement due à la commune de Bois-Guillaume pour l'année 2014, nonobstant le fait que le tribunal administratif avait, dans son jugement du 18 juin 2013 annulant l'arrêté de fusion de cette commune avec celle de Bihorel, réputé définitifs les effets produits par cet arrêté de fusion avant le 31 décembre 2013, la cour administrative d'appel de Douai n'a pas commis d'erreur de droit. <br/>
<br/>
              5. Il résulte de ce qui précède que le pourvoi de la commune de Bois-Guillaume doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de la commune de Bois-Guillaume est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la commune de Bois-Guillaume et à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-01-01 COLLECTIVITÉS TERRITORIALES. COMMUNE. ORGANISATION DE LA COMMUNE. IDENTITÉ DE LA COMMUNE. - DISPOSITIONS RELATIVES À LA DGF ALLOUÉE AUX COMMUNES ISSUES D'UNE DIVISION (ART. L. 2334-12 DU CGCT) - APPLICABILITÉ EN CAS D'ANNULATION JURIDICTIONNELLE D'UNE DÉCISION DE FUSION DE COMMUNES - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">135-02-04-03-03-01 COLLECTIVITÉS TERRITORIALES. COMMUNE. FINANCES COMMUNALES. RECETTES. DOTATIONS. DOTATION GLOBALE DE FONCTIONNEMENT. - DISPOSITIONS RELATIVES À LA DGF ALLOUÉE AUX COMMUNES ISSUES D'UNE DIVISION (ART. L. 2334-12 DU CGCT) - APPLICABILITÉ EN CAS D'ANNULATION JURIDICTIONNELLE D'UNE DÉCISION DE FUSION DE COMMUNES - ABSENCE.
</SCT>
<ANA ID="9A"> 135-02-01-01 Il résulte de l'article L. 2334-12 du code général des collectivités territoriales (CGCT), éclairées par les travaux préparatoires de la loi n° 93-1436 du 31 décembre 1993 dont il est issu, qu'il ne s'applique qu'aux situations résultant de décisions administratives de division de communes et non à celles résultant de l'annulation juridictionnelle d'une décision de fusion de communes. Est sans incidence à cet égard la circonstance que le juge aurait décidé, tout en prononçant l'annulation de la décision de fusion de communes, de réputer définitifs tout ou partie des effets produits par cette décision de fusion avant son annulation.</ANA>
<ANA ID="9B"> 135-02-04-03-03-01 Il résulte de l'article L. 2334-12 du code général des collectivités territoriales (CGCT), éclairées par les travaux préparatoires de la loi n° 93-1436 du 31 décembre 1993 dont il est issu, qu'il ne s'applique qu'aux situations résultant de décisions administratives de division de communes et non à celles résultant de l'annulation juridictionnelle d'une décision de fusion de communes. Est sans incidence à cet égard la circonstance que le juge aurait décidé, tout en prononçant l'annulation de la décision de fusion de communes, de réputer définitifs tout ou partie des effets produits par cette décision de fusion avant son annulation.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
