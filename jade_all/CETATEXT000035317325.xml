<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035317325</ID>
<ANCIEN_ID>JG_L_2017_07_000000409431</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/31/73/CETATEXT000035317325.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 28/07/2017, 409431, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-07-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409431</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>Mme Sabine Monchambert</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:409431.20170728</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 31 mars 2017 au secrétariat du contentieux du Conseil d'Etat, la SARL J. Cortès France demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 1er février 2017 modifiant l'arrêté du 24 juin 2016 portant homologation des prix de vente au détail des tabacs manufacturés en France, à l'exclusion des départements d'outre-mer, en tant qu'il prévoit le retrait immédiat ou différé des produits commercialisés sous la marque " Exotic " ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts ;<br/>
              - le code de la santé publique ;<br/>
              - la décision du Conseil d'Etat statuant au contentieux nos 401536, 401561, 401611, 401632, 401668 du 10 mai 2017 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sabine Monchambert, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat de la SARL J. Cortès France.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier que, par une lettre conjointe du 22 novembre 2016, le ministre de l'économie et des finances et le ministre des affaires sociales et de la santé ont adressé aux fabricants et aux fournisseurs agréés de tabacs manufacturés le calendrier prévisionnel de la campagne d'homologation des prix des tabacs manufacturés, en fixant au 2 décembre 2016 la date limite de dépôt, par les fournisseurs agréés, de la liste des nouveaux produits, des produits dont le prix ou la dénomination commerciale sont modifiés et des produits retirés de la distribution, au vu des informations transmises par les fabricants de tabac au plus tard le 29 novembre 2016. La SARL J. Cortès France a, le 29 novembre 2016, transmis à son fournisseur agréé la liste des produits dont le prix ou la dénomination commerciale étaient modifiés, ce qui, en ce qui concerne les cigares de la marque " Neos Exotic ", recouvrait quatre références. Par un arrêté conjoint du 1er février 2017, le ministre des affaires sociales et de la santé et le secrétaire d'Etat chargé du budget et des comptes publics ont homologué les prix des produits de la marque " Neos Exotic " dont le prix ou la dénomination était modifié, en limitant la durée de cette homologation à un an ou, pour les cigares, à deux ans à compter de l'entrée en vigueur de l'arrêté le 20 février 2017. La SARL J. Cortès France demande l'annulation pour excès de pouvoir de cet arrêté en tant qu'il interdit la vente de ces produits sur le marché français à compter du terme de cette période.<br/>
<br/>
              2. En raison des effets qui s'y attachent, l'annulation pour excès de pouvoir d'un acte administratif, qu'il soit ou non réglementaire, emporte, lorsque le juge est saisi de conclusions recevables, l'annulation par voie de conséquence des décisions administratives consécutives qui n'auraient pu légalement être prises en l'absence de l'acte annulé ou qui sont en l'espèce intervenues en raison de l'acte annulé. Il en va ainsi, notamment, des décisions qui ont été prises en application de l'acte annulé et de celles dont l'acte annulé constitue la base légale. Il incombe au juge de l'excès de pouvoir, lorsqu'il est saisi de conclusions recevables dirigées contre de telles décisions consécutives, de prononcer leur annulation par voie de conséquence, le cas échéant en relevant d'office un tel moyen qui découle de l'autorité absolue de chose jugée qui s'attache à l'annulation du premier acte.<br/>
<br/>
              3. Aux termes du premier alinéa de l'article 572 du code général des impôts, dans sa rédaction issue de la loi du 26 janvier 2016 de modernisation de notre système de santé : " Le prix de détail de chaque produit exprimé aux 1 000 unités ou aux 1 000 grammes, est unique pour l'ensemble du territoire et librement déterminé par les fabricants et les fournisseurs agréés. Il est applicable après avoir été homologué par arrêté conjoint des ministres chargés de la santé et du budget, dans des conditions définies par décret en Conseil d'Etat. Il ne peut toutefois être homologué s'il est inférieur à la somme du prix de revient et de l'ensemble des taxes ". Si l'article 2 de l'ordonnance du 19 mai 2016 portant transposition de la directive 2014/40/UE sur la fabrication, la présentation et la vente des produits du tabac et des produits connexes a complété cet alinéa pour prévoir que : " Cet arrêté mentionne la marque et la dénomination commerciale des produits du tabac à la condition que ces dernières respectent les dispositions de l'article L. 3512-21 du code de la santé publique ", cet article a été annulé par une décision du Conseil d'Etat statuant au contentieux du 10 mai 2017.<br/>
<br/>
              4. Il ressort des pièces du dossier, notamment du courrier adressé le 21 décembre 2016 par le directeur général de la santé à la société requérante pour l'inviter à présenter ses observations, que les dispositions critiquées de l'arrêté du 1er février 2017 sont motivées par la circonstance que la marque commerciale des produits considérés revêtirait un caractère laudatif, en méconnaissance de l'article L. 3512-21 du code de la santé publique et de l'article R. 3512-30 du même code pris pour son application. Par suite, les décisions attaquées de limitation de la durée d'homologation des prix des produits en cause, qui trouvaient leur base légale dans les dispositions ajoutées à l'article 572 du code général des impôts par l'article 2 de l'ordonnance du 19 mai 2016, doivent être annulées par voie conséquence de l'annulation de cet article 2. Ce moyen suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens de la requête.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 2 000 euros à verser à la SARL J. Cortès France au titre de l'article L. 761-1 du code de justice administrative. Les dispositions de cet article font, en revanche, obstacle à ce qu'une somme soit mise à la charge de la société requérante, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêté du 1er février 2017 modifiant l'arrêté du 24 juin 2016 portant homologation des prix de vente au détail des tabacs manufacturés en France, à l'exclusion des départements d'outre-mer, est annulé en tant qu'il limite la durée de l'homologation des prix des cigares de la même marque " Neos Exotic " dont le prix ou la dénomination commerciale ont été modifiés.<br/>
Article 2 : L'Etat versera à la société J. Cortès France une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Les conclusions du ministre de l'action et des comptes publics présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la SARL J. Cortès France, à la ministre des solidarités et de la santé et au ministre de l'action et des comptes publics. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
