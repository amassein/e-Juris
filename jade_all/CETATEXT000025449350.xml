<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025449350</ID>
<ANCIEN_ID>JG_L_2012_03_000000344743</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/44/93/CETATEXT000025449350.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 01/03/2012, 344743</TITRE>
<DATE_DEC>2012-03-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>344743</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jacques Arrighi de Casanova</PRESIDENT>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. Laurent Cytermann</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:344743.20120301</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 6 décembre 2010 et 7 mars 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. François A, demeurant au ... ; M. A demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement n° 0804155 du 28 septembre 2010 par lequel le tribunal administratif de Bordeaux a rejeté sa demande tendant à l'annulation du brevet de pension qui lui a été accordé le 5 août 2008 par la Caisse des dépôts et consignations et à ce qu'il soit enjoint à celle-ci, sous astreinte, de lui délivrer un brevet de pension retenant une base de liquidation modifiée, avec intérêts au taux légal ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 59-1479 du 28 décembre 1959 ;<br/>
<br/>
              Vu le décret n° 2004-1056 du 5 octobre 2004 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Cytermann, chargé des fonctions de Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Waquet, Farge, Hazan, avocat de M. A et de la SCP Odent, Poulet, avocat de la Caisse des dépôts et consignations, <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Waquet, Farge, Hazan, avocat de M. A et à la SCP Odent, Poulet, avocat de la Caisse des dépôts et consignations ; <br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes du premier alinéa de l'article unique de la loi du 28 décembre 1959 ouvrant à certains fonctionnaires de l'ordre technique une option en faveur d'une pension au titre de la loi du 2 août 1949, lors de leur mise à la retraite : " Les fonctionnaires civils de l'ordre technique du ministère des armées, nommés dans un corps de fonctionnaires après avoir accompli au moins dix ans de services en qualité d'ouvriers affiliés au régime des pensions fixé par la loi n° 49-1097 du 2 août 1949, pourront, lors de leur mise à la retraite, opter pour une pension ouvrière liquidée en application de la loi susvisée, s'ils perçoivent encore à cette date une indemnité différentielle basée sur les rémunérations ouvrières. Les émoluments de base retenus pour la liquidation de la pension sont ceux correspondant au salaire maximum de la profession à laquelle appartenaient les intéressés lors de leur nomination en qualité de fonctionnaire. " ; <br/>
<br/>
              Considérant qu'il résulte de ces dispositions que le législateur a prescrit que la pension des bénéficiaires de l'option qu'il ouvrait fût calculée sur les émoluments correspondant au salaire le plus élevé pouvant être perçu, à la date de leur admission à la retraite, dans la profession qu'il ont exercée avant d'être promus fonctionnaires, sans qu'il y ait lieu de tenir compte de circonstances relatives aux conditions d'accès à la catégorie au titre de laquelle ce salaire est versé ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A, après avoir exercé pendant dix ans comme ouvrier de pyrotechnie du ministère de la défense, a été titularisé à effet du 1er septembre 1976 en qualité de technicien supérieur d'études et de fabrications ; que, lors de son admission à la retraite, il a demandé à bénéficier de l'option ouverte par les dispositions précitées ; que, pour rejeter par le jugement attaqué la demande de M. A dirigée contre la décision du 5 août 2008 qui lui a concédé, à compter du 1er octobre 2007, une pension ouvrière calculée sur la base du grade " hors catégorie 8 " de la profession d'ouvrier de pyrotechnie, au lieu du grade " hors catégorie B ", le tribunal administratif de Bordeaux s'est fondé sur la circonstance que ce dernier grade n'était accessible que par la voie d'un examen professionnel ; qu'il résulte de ce qui vient d'être dit que le tribunal a, ce faisant, commis une erreur de droit ; que par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son jugement doit être annulé ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Considérant qu'au 1er octobre 2007, date de la radiation des cadres de M. A, ancien ouvrier de pyrotechnie dont il n'est pas contesté qu'il percevait encore à cette date une indemnité différentielle, le grade le plus élevé de cette profession, résultant de l'instruction du 20 février 1995 relative à la nomenclature des professions ouvrières, était le grade " hors catégorie B " ; que la circonstance que ce grade n'est accessible que par la voie d'un examen professionnel étant, ainsi qu'il a été dit, sans incidence sur le calcul de sa pension, le requérant est fondé à demander l'annulation du brevet de pension du 5 août 2008 qui a établi les bases de liquidation de sa pension sans tenir compte de ce grade ; qu'il y a lieu d'enjoindre à l'administration de procéder, dans le délai de deux mois, à une nouvelle liquidation de la pension de M. A à compter du 1er octobre 2007, sur la base de la rémunération correspondant à ce grade ; qu'il n'y a pas lieu d'assortir ce délai d'une astreinte ; <br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat, en application de l'article L. 761-1 du code de justice administrative, le versement à M. A d'une somme de 3 000 euros ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Bordeaux du 28 septembre 2010 est annulé.<br/>
Article 2 : Le brevet de pension du 5 août 2008 est annulé.<br/>
Article 3 : M. A est renvoyé devant l'administration afin qu'il soit procédé, dans les deux mois suivant la notification de la présente décision, à une nouvelle liquidation de sa pension à compter du 1er octobre 2007, sur la base de la rémunération correspondant au grade d'ouvrier de pyrotechnie " hors catégorie B ".<br/>
Article 4 : L'Etat versera une somme de 3 000 euros à M. A au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Le surplus des conclusions de M. A est rejeté. <br/>
Article 6 : La présente décision sera notifiée à M. François A, au ministre de la défense et des anciens combattants et à la Caisse des dépôts et consignations.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">48-02-02-03 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. PENSIONS CIVILES. LIQUIDATION DE LA PENSION. - PENSION DES BÉNÉFICIAIRES DE L'OPTION OUVERTE PAR LA LOI DU 28 DÉCEMBRE 1959 (PENSION OUVRIÈRE) - BASES DE CALCUL - EMOLUMENTS CORRESPONDANT AU SALAIRE MAXIMUM DE LA PROFESSION À LAQUELLE ILS APPARTENAIENT LORS DE LEUR NOMINATION EN QUALITÉ DE FONCTIONNAIRE - 1) DATE D'APPRÉCIATION DU SALAIRE MAXIMUM - DATE DE L'ADMISSION À LA RETRAITE - 2) PRISE EN COMPTE DES CONDITIONS D'ACCÈS À LA CATÉGORIE AU TITRE DE LAQUELLE CE SALAIRE EST VERSÉ - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 48-02-02-03 L'article unique de la loi n° 59-1479 du 28 décembre 1959 permet aux fonctionnaires civils de l'ordre technique des armées ayant préalablement accompli dix ans de service en qualité d'ouvriers affiliés au régime des pensions ouvrières d'opter, lors de leur mise à la retraite, pour une telle pension lorsqu'ils perçoivent encore à cette date une indemnité différentielle basée sur les rémunérations ouvrières. Cette pension est calculée 1) sur la base du salaire le plus élevé pouvant être perçu, à la date de la mise en retraite du fonctionnaire, dans la profession qu'il a exercée avant d'être promu fonctionnaire, 2) sans qu'il y ait lieu de tenir compte de circonstances relatives aux conditions d'accès à la catégorie au titre de laquelle ce salaire est versé.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., CE, 16 novembre 2005, M. Guille, n°s 264404 264562, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
