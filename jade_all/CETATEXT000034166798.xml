<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034166798</ID>
<ANCIEN_ID>JG_L_2017_03_000000402570</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/16/67/CETATEXT000034166798.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 10/03/2017, 402570, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402570</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:402570.20170310</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Montreuil d'annuler les décisions du ministre de l'intérieur portant retraits de points de son permis de conduire à la suite d'infractions commises les 29 avril 2003, 14 avril 2004, 27 avril 2004, 8 janvier 2005, 7 février 2005 et 22 février 2006. Par un jugement n° 1502508 du 16 juin 2016, le tribunal administratif a fait droit à sa demande.<br/>
<br/>
              Par un pourvoi, enregistré le 19 août 2016 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;  <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M.A....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la route ; <br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que le principe de sécurité juridique, qui implique que ne puissent être remises en cause sans condition de délai des situations consolidées par l'effet du temps, fait obstacle à ce que puisse être contestée indéfiniment une décision administrative individuelle qui a été notifiée à son destinataire, ou dont il est établi, à défaut d'une telle notification, que celui-ci a eu connaissance ; qu'en une telle hypothèse, si le non-respect de l'obligation d'informer l'intéressé sur les voies et les délais de recours, ou l'absence de preuve qu'une telle information a bien été fournie, ne permet pas que lui soient opposés les délais de recours fixés par le code de justice administrative, le destinataire de la décision ne peut exercer de recours juridictionnel au-delà d'un délai raisonnable ; qu'en règle générale et sauf circonstances particulières dont se prévaudrait le requérant, ce délai ne saurait, sous réserve de l'exercice de recours administratifs pour lesquels les textes prévoient des délais particuliers, excéder un an à compter de la date à laquelle une décision expresse lui a été notifiée ou de la date à laquelle il est établi qu'il en a eu connaissance ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond, qu'en exécution d'une décision du 16 novembre 2007 du préfet des Hauts-de-Seine lui enjoignant de restituer son titre de conduite invalidé pour solde de points nul, M. A... a restitué ce titre le 27 août 2008 ; qu'une telle circonstance révèle qu'il a eu connaissance, au plus tard à cette date, de la décision antérieure constatant la perte de validité de son titre ; qu'alors même que cette décision, notifiée le 10 octobre 2007, ne comportait pas la mention des voies et délais de recours, elle était, faute d'avoir été contestée dans un délai raisonnable à compter du 27 août 2008, devenue définitive à la date du 19 mars 2015 à laquelle M. A...a saisi le tribunal administratif de Montreuil de conclusions tendant à l'annulation des six décisions de retraits de points qui y ont concouru ; que, dans ces conditions, ces conclusions étaient dépourvues d'objet  à la date à laquelle elles ont été présentées et étaient, par suite irrecevables ; que cette irrecevabilité ressortait, ainsi qu'il vient d'être dit, des pièces du dossier qui était soumis aux juges du fond ; qu'en ne l'opposant pas à M.A..., le tribunal administratif de Montreuil a commis une erreur de droit ; que son jugement doit, par suite, être annulé ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-1 du code de justice administrative ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que les conclusions de M. A... tendant à l'annulation des décisions portant retrait de points à la suite des infractions des 29 avril 2003, 14 avril 2004, 27 avril 2004, 8 janvier 2005, 7 février 2005 et 22 février 2006 étaient dépourvues d'objet à la date à laquelle elles ont été présentées, faute pour M. A...d'avoir demandé, dans un délai raisonnable à compter de la date du 27 août 2008 à laquelle il en a eu connaissance au plus tard, l'annulation de la décision constatant la perte de validité de son permis de conduire ; qu'elles ne sont, par suite, pas recevables et doivent, dès lors, être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 16 juin 2016 du tribunal administratif de Montreuil est annulé.<br/>
<br/>
Article 2 : Les conclusions présentées par M. A...devant le tribunal administratif de Montreuil sont rejetées. <br/>
<br/>
Article 3 : La présente décision sera notifiée à M. B...A...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
