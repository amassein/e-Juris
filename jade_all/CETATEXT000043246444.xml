<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043246444</ID>
<ANCIEN_ID>JG_L_2021_03_000000445717</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/24/64/CETATEXT000043246444.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 12/03/2021, 445717, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445717</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Lionel Ferreira</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHS:2021:445717.20210312</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. D... F... a demandé au tribunal administratif de Lille d'annuler les opérations électorales qui se sont déroulées le 15 mars 2020 pour l'élection des conseillers municipaux et communautaires de la commune de Templeuve-en-Pévèle (Nord). Par un jugement n° 2002532 du 28 septembre 2020, le tribunal administratif de Lille a rejeté sa protestation.<br/>
<br/>
              Par une requête et un mémoire en réplique enregistrés les 27 octobre 2020 et 6 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, M. F... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de faire droit à sa protestation.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de procédure civile ;<br/>
              - l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
              - le décret n° 2020-571 du 14 mai 2020 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Lionel Ferreira, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme C... B..., rapporteure publique ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. M. F... fait appel du jugement du 28 septembre 2020 par lequel le tribunal administratif de Lille a rejeté sa protestation tendant à l'annulation des opérations électorales qui se sont déroulées le 15 mars 2020 pour l'élection des conseillers municipaux et communautaires de la commune de Templeuve-en-Pévèle (Nord) à l'issue desquelles la liste conduite par M. E... a recueilli 1 301 voix, soit 50,05 % des suffrages exprimés, et la liste conduite par M. F... a recueilli 1 298 voix, soit 49,94 % des suffrages exprimés. <br/>
<br/>
              2. Aux termes de l'article R. 611-7 du code de justice administrative : " Lorsque la décision lui paraît susceptible d'être fondée sur un moyen relevé d'office, le président de la formation de jugement (...) en informe les parties avant la séance de jugement et fixe le délai dans lequel elles peuvent, sans qu'y fasse obstacle la clôture éventuelle de l'instruction, présenter leurs observations sur le moyen communiqué (...) ".<br/>
<br/>
              3. Il résulte de l'instruction que M. F... a soulevé, dans un mémoire en réplique enregistré le 1er juillet 2020 au greffe du tribunal, deux griefs tirés, d'une part, de l'existence d'un écart de douze voix entre les émargements et les suffrages exprimés et d'autre part, de l'irrégularité de trois signatures portées sur les listes d'émargement. Il soutenait que compte tenu de ces irrégularités, la liste conduite par M. E... avait recueilli un nombre de suffrages inférieur à la majorité absolue des suffrages exprimés. Le tribunal a écarté ces griefs comme irrecevables faute d'avoir été présentés dans le délai fixé par les dispositions combinées du 3° du paragraphe II de l'article 15 de l'ordonnance du 25 mars 2020, de l'article 1er du décret du 14 mai 2020 et du second alinéa de l'article 642 du code de procédure civile. En ne communiquant pas à M. F... son intention de soulever cette irrecevabilité, le tribunal a méconnu les dispositions de l'article R. 611-7 du code de justice administrative. Son jugement doit dès lors être annulé, sans qu'il soit besoin de se prononcer sur les autres griefs de la requête. <br/>
<br/>
              4. Il y a lieu d'évoquer et de statuer immédiatement sur la protestation présentée par M. F....<br/>
<br/>
              Sur les griefs tirés des irrégularités affectant la liste d'émargement :<br/>
<br/>
              5. Le grief tiré de ce que la liste conduite par M. E... aurait recueilli un nombre de suffrages inférieur à la majorité absolue des suffrages exprimés du fait des irrégularités affectant les listes d'émargements a été soulevé, ainsi qu'il a été dit au point 3 ci-dessus, pour la première fois devant le tribunal par un mémoire enregistré au-delà du délai prévu pour cette élection. Ce grief, repris en appel, n'est pas d'ordre public. Par suite, il est irrecevable. <br/>
<br/>
              6. Il en est de même, ainsi que le relève en défense M. E..., du grief soulevé pour la première fois devant le juge d'appel tiré de ce que la liste conduite par celui-ci aurait recueilli un nombre de suffrages inférieur au quart du nombre des électeurs inscrits du fait des irrégularités affectant les listes d'émargements. <br/>
<br/>
              Sur les effets de la crise sanitaire sur la sincérité du scrutin :<br/>
<br/>
              7. D'une part, l'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre des solidarités et de la santé à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Dans ce contexte, le Premier ministre a adressé à l'ensemble des maires le 7 mars 2020 une lettre présentant les mesures destinées à assurer le bon déroulement des élections municipales et communautaires prévues les 15 et 22 mars 2020. Ces mesures ont été précisées par une circulaire du ministre de l'intérieur du 9 mars 2020 relative à l'organisation des élections municipales des 15 et 22 mars 2020 en situation d'épidémie de coronavirus covid-19, formulant des recommandations relatives à l'aménagement des bureaux de vote et au respect des consignes sanitaires, et par une instruction de ce ministre, du même jour, destinée à faciliter l'exercice du droit de vote par procuration. Après consultation par le Gouvernement du conseil scientifique mis en place pour lui donner les informations scientifiques utiles à l'adoption des mesures nécessaires pour faire face à l'épidémie de covid-19, les 12 et 14 mars 2020, le premier tour des élections municipales a eu lieu comme prévu le 15 mars 2020.<br/>
<br/>
              8. D'autre part, aux termes de l'article L. 262 du code électoral, applicable aux communes de mille habitants et plus : " Au premier tour de scrutin, il est attribué à la liste qui a recueilli la majorité absolue des suffrages exprimés un nombre de sièges égal à la moitié du nombre des sièges à pourvoir, arrondi, le cas échéant, à l'entier supérieur lorsqu'il y a plus de quatre sièges à pourvoir et à l'entier inférieur lorsqu'il y a moins de quatre sièges à pourvoir. Cette attribution opérée, les autres sièges sont répartis entre toutes les listes à la représentation proportionnelle suivant la règle de la plus forte moyenne, sous réserve de l'application des dispositions du troisième alinéa ci-après. / Si aucune liste n'a recueilli la majorité absolue des suffrages exprimés au premier tour, il est procédé à un deuxième tour (...) ".<br/>
<br/>
              9. Ni par ces dispositions, ni par celles de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19, le législateur n'a subordonné à un taux de participation minimal la répartition des sièges au conseil municipal dans les communes de mille habitants et plus lorsqu'une liste a recueilli la majorité absolue des suffrages exprimés à l'issue du premier tour de scrutin. Le niveau de l'abstention n'est ainsi, par lui-même, pas de nature à remettre en cause les résultats du scrutin, s'il n'a pas altéré, dans les circonstances de l'espèce, sa sincérité.<br/>
<br/>
              10. A l'appui de ce grief, M. F... se borne à invoquer un taux d'abstention de 48,54 %, d'ailleurs moins élevé que celui qui a été observé au niveau national, sans invoquer de circonstance locale particulière permettant de conclure à l'altération de la sincérité du scrutin. Dans ces conditions, le grief ne peut qu'être écarté.<br/>
<br/>
              Sur le grief relatif aux inscription sur les listes électorales :<br/>
<br/>
              11. Le juge administratif n'est pas compétent, hors le cas de manoeuvres, pour apprécier le bien-fondé des inscriptions et radiations opérées sur les listes électorales. Par suite, dès lors que M. F... n'invoque l'existence d'aucune manoeuvre susceptible d'avoir altéré la sincérité du scrutin, le grief tiré de ce qu'une électrice, ressortissante allemande, n'a pu exercer son droit de suffrage au motif qu'elle n'était pas inscrite sur la liste électorale de la commune est inopérant au soutien de la contestation de la régularité des opérations électorales qui s'y sont déroulées le 15 mars 2020. <br/>
<br/>
              Sur les autres griefs :<br/>
<br/>
              12. Si M. F... soutient que les bulletins de vote de la liste conduite par M. E... étaient plus épais que ceux de la liste qu'il conduisait, que les procurations des pensionnaires de l'établissement d'hébergement pour personnes âgées de Templeuve-en-Pévèle ont été irrégulièrement établies, et qu'un bulletin comportant une tache de café n'a pas été déclaré nul et a été pris en compte dans le résultat du dépouillement, il n'apporte aucun commencement de preuve à l'appui de ces allégations permettant d'apprécier le bien-fondé de ces griefs, qui, par suite, doivent être écartés.<br/>
<br/>
              13. Il résulte de tout ce qui précède que la protestation de M. F... doit être rejetée.<br/>
<br/>
              14. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire application de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Lille du 28 septembre 2020 est annulé. <br/>
Article 2 : La protestation de M. F... est rejetée. <br/>
Article 3 : Les conclusions de M. E... présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. D... F..., à M. A... E.... <br/>
Copie en sera adressée au ministre de l'intérieur et au préfet du Nord.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
