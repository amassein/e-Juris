<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041485748</ID>
<ANCIEN_ID>JG_L_2020_01_000000432578</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/48/57/CETATEXT000041485748.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 27/01/2020, 432578, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-01-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432578</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>Mme Cécile Renault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:432578.20200127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. D... C... a demandé au tribunal administratif de La Réunion d'annuler la décision du 1er octobre 2014 par laquelle le ministre de la défense a rejeté sa demande d'indemnisation présentée sur le fondement des dispositions de la loi du 5 janvier 2010 relative à la reconnaissance et à l'indemnisation des victimes des essais nucléaires français et de condamner l'Etat à lui verser une indemnité d'un montant global de 286 314 euros, subsidiairement d'ordonner une expertise ou d'enjoindre au ministre de la défense de procéder à une évaluation des préjudices imputables à la maladie dont il est atteint. Par un jugement n° 1401263 du 22 décembre 2016, le tribunal administratif de La Réunion a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17BX00672 du 14 mai 2019, la cour administrative d'appel de Bordeaux a annulé ce jugement et la décision du ministre puis enjoint au comité d'indemnisation des victimes des essais nucléaires (CIVEN) de proposer une indemnisation à M. C....  <br/>
<br/>
              Par un pourvoi, enregistré le 12 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, la ministre des armées demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. C....<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2010-2 du 5 janvier 2010 ;<br/>
              - la loi n° 2018-1317 du 28 décembre 2018 ; <br/>
              - le décret n° 2014-1049 du 15 septembre 2014 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... B..., auditrice,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de M. C... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. C..., sous-officier du génie, a été affecté au centre d'expérimentations du Pacifique, à Mururoa, en qualité de conducteur de travaux pendant la période du 22 mai 1989 au 22 mai 1990, au cours de laquelle sept essais nucléaires souterrains ont été réalisés. M. C..., qui a contracté une leucémie à l'âge de 46 ans, a déposé une demande d'indemnisation sur le fondement de la loi du 5 janvier 2010 relative à la reconnaissance et à l'indemnisation des victimes des essais nucléaires français. Le ministre de la défense a rejeté cette demande par une décision du 1er octobre 2014, après avis du comité d'indemnisation des victimes des essais nucléaires (CIVEN), qui a estimé que le risque attribuable aux essais nucléaires dans la survenue de la maladie de l'intéressé pouvait être considéré comme négligeable. Par un jugement du 22 décembre 2016, le tribunal administratif de La Réunion a rejeté la demande de M. C... tendant à l'annulation de cette décision. La ministre des armées se pourvoit en cassation contre l'arrêt du 14 mai 2019 par lequel la cour administrative d'appel de Bordeaux a annulé ce jugement et sa décision du 1er octobre 2014, puis enjoint au CIVEN de proposer une indemnisation à M. C....<br/>
<br/>
              2. Aux termes de l'article 1er de la loi du 5 janvier 2010 relative à la reconnaissance et à l'indemnisation des victimes des essais nucléaires français : " I. Toute personne souffrant d'une maladie radio-induite résultant d'une exposition à des rayonnements ionisants dus aux essais nucléaires français et inscrite sur une liste fixée par décret en Conseil d'Etat conformément aux travaux reconnus par la communauté scientifique internationale peut obtenir réparation intégrale de son préjudice dans les conditions prévues par la présente loi./II. Si la personne est décédée, la demande de réparation peut être présentée par ses ayants droit. (...) ". Aux termes de l'article 2 de cette même loi : " La personne souffrant d'une pathologie radio-induite doit avoir résidé ou séjourné : / 1° Soit entre le 13 février 1960 et le 31 décembre 1967 au Centre saharien des expérimentations militaires, ou entre le 7 novembre 1961 et le 31 décembre 1967 au Centre d'expérimentations militaires des oasis ou dans les zones périphériques à ces centres ; / 2° Soit entre le 2 juillet 1966 et le 31 décembre 1998 en Polynésie française. / (...) ". Aux termes du I de l'article 4 de la même loi : " I. - Les demandes individuelles d'indemnisation sont soumises au comité d'indemnisation des victimes des essais nucléaires (...) " En vertu du V du même article 4, dans sa rédaction résultant de l'article 113 de la loi du 28 février 2017 de programmation relative à l'égalité réelle outre-mer et portant autres dispositions en matière sociale et économique, dont les dispositions sont applicables aux instances en cours à la date de son entrée en vigueur, soit le lendemain de la publication de la loi au Journal officiel de la République française : " V. - Ce comité examine si les conditions de l'indemnisation sont réunies. Lorsqu'elles le sont, l'intéressé bénéficie d'une présomption de causalité (...) ". Désormais, le V de l'article 4, dans sa rédaction issue de l'article 232 de la loi du 28 décembre 2018 de finances pour 2019, dispose : " Ce comité examine si les conditions sont réunies. Lorsqu'elles le sont, l'intéressé bénéficie d'une présomption de causalité, à moins qu'il ne soit établi que la dose annuelle de rayonnements ionisants dus aux essais nucléaires français reçue par l'intéressé a été inférieure à la limite de dose efficace pour l'exposition de la population à des rayonnements ionisants fixée dans les conditions prévues au 3° de l'article L. 1333-2 du code de la santé publique ". <br/>
<br/>
              3. En modifiant les dispositions du V de l'article 4 de la loi du 5 janvier 2010 issues de l'article 113 de la loi du 28 février 2017, l'article 232 de la loi du 28 décembre 2018 élargit la possibilité, pour l'administration, de combattre la présomption de causalité dont bénéficient les personnes qui demandent une indemnisation lorsque les conditions de celle-ci sont réunies. Il doit être regardé, en l'absence de dispositions transitoires, comme ne s'appliquant qu'aux demandes qui ont été déposées après son entrée en vigueur, intervenue le lendemain de la publication de la loi du 28 décembre 2018 au Journal officiel de la République française. <br/>
<br/>
              4. Il ressort des énonciations de l'arrêt attaqué que M. C... a déposé sa demande au CIVEN le 20 octobre 2011. Il résulte de ce qui a été dit aux points 2 et 3 ci-dessus qu'en faisant application des dispositions de la loi du 5 janvier 2010 dans leur rédaction issue de la loi du 28 décembre 2018, alors qu'étaient applicables les dispositions issues de la loi du 28 février 2017, la cour administrative d'appel de Bordeaux a méconnu le champ d'application de la loi. Dès lors, sans qu'il soit besoin d'examiner les moyens du pourvoi, l'arrêt attaqué doit être annulé. <br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que la somme que demande M. C... à ce titre soit mise à la charge de l'Etat.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 14 mai 2019 de la cour administrative de Bordeaux est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux. <br/>
Article 3 : Les conclusions de M. C... présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à la ministre des armées et à M. D... C....<br/>
Copie en sera adressée au comité d'indemnisation des victimes des essais nucléaires.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
