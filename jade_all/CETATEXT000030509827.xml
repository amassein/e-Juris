<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030509827</ID>
<ANCIEN_ID>JG_L_2015_04_000000383275</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/50/98/CETATEXT000030509827.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème SSJS, 17/04/2015, 383275, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-04-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383275</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:383275.20150417</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. Jean Jouandet a demandé au tribunal administratif de Montpellier d'annuler, d'une part, la délibération du 9 avril 2014 du conseil de la communauté de communes Sud-Roussillon proclamant l'élection de M. Thierry Del Poso en qualité de président de la communauté de communes et, d'autre part, les opérations électorales préalables qui se sont déroulées le même jour.<br/>
<br/>
              Par un jugement n° 1401839 du 11 juillet 2014, le tribunal administratif a rejeté sa protestation.<br/>
<br/>
              Par une requête, enregistrée le 29 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement du 11 juillet 2014 ;<br/>
<br/>
              2°) de faire droit à sa protestation.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
<br/>
              - le code électoral ;<br/>
<br/>
              - le code général des collectivités territoriales ;<br/>
<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que, par une délibération du 9 avril 2014, le conseil de la communauté de communes Sud-Roussillon a proclamé M. Thierry Del Poso président du conseil communautaire ; que M. Jean Jouandet, conseiller communautaire, a demandé au tribunal administratif de Montpellier d'annuler cette délibération ainsi que les opérations électorales ayant conduit à l'élection de M. Del Poso ; que par un jugement du 11 juillet 2014, dont M. A... relève appel devant le Conseil d'Etat, le tribunal administratif de Montpellier a rejeté la protestation de l'intéressé ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'il résulte des dispositions combinées de l'article R. 773-1 du code de justice administrative et des articles R. 119, R. 120 et R. 121 du code électoral relatifs aux délais impartis pour l'instruction et le jugement des protestations dirigées contre l'élection des conseillers municipaux, qui sont applicables aux élections relatives aux membres du bureau d'un établissement public de coopération intercommunale en vertu de l'article L. 5211-3 du code général des collectivités territoriales, que, par dérogation aux dispositions de l'article R. 611-5 du code de justice administrative, le tribunal administratif n'est tenu de communiquer à l'ensemble des parties que la protestation elle-même ; qu'il lui appartient seulement de tenir à leur disposition les pièces jointes à cette protestation ainsi que les mémoires produits ultérieurement par les parties, de sorte que celles-ci soient à même, si elles l'estiment utile, d'en prendre connaissance ; qu'en l'espèce, il n'est pas contesté que le mémoire en défense du 28 avril 2014 et le bordereau de pièce complémentaire du 10 juin 2014 de M. Del Poso ont été tenus à la disposition des parties au greffe du tribunal administratif ; qu'ainsi, M. A...n'est pas fondé à soutenir que le jugement attaqué aurait été rendu à l'issue d'une procédure irrégulière ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'il ne résulte pas de l'instruction que les inexactitudes matérielles ou omissions entachant certaines mentions de la délibération du 9 avril 2014 et relatives aux conditions dans lesquelles a été présidée la séance au cours de laquelle a été élu le président de la communauté de communes Sud-Roussillon, d'ailleurs corrigées par une délibération du 11 avril 2014,  auraient été par elles-mêmes de nature à entacher d'irrégularité les opérations électorales litigieuses ; que la circonstance, d'ailleurs liée à la rectification de ces inexactitudes, que les délibérations litigieuses auraient été affichées plus de vingt-quatre heures après la séance du 9 avril 2014, en méconnaissance des prescriptions des articles L. 2122-12 et R. 2122-1 du code général des collectivités territoriales, applicables en vertu de l'article                   L. 5211-2 du même code, n'a pas été non plus de nature à vicier ces opérations ; <br/>
<br/>
              4. Considérant, en troisième lieu, que l'article L. 2121-7 du code général des collectivités territoriales, applicable aux établissements publics de coopération intercommunale en vertu des dispositions de l'article L. 5211-2 du même code, prévoit que lors du renouvellement général des conseils municipaux, la première réunion se tient de plein droit au plus tôt le vendredi et au plus tard le dimanche suivant le tour de scrutin à l'issue duquel le conseil a été élu au complet ; que l'article L. 2121-10 du même code, applicable dans les mêmes conditions, prévoit que toute convocation est faite par le maire ; qu'en vertu de l'article                    L. 2122-15 du même code, également applicable aux établissements publics de coopération intercommunale, le maire et les adjoints continuent à exercer leurs fonctions jusqu'à l'installation de leurs successeurs ; qu'aux termes du premier alinéa de l'article L. 2122-8 du code général des collectivités territoriales, également applicable aux établissements publics de coopération intercommunale : " La séance au cours de laquelle il est procédé à l'élection du maire est présidée par le plus âgé des membres du conseil municipal." ; que par ailleurs, aux termes de l'article L. 5211-9 du même code : " A partir de l'installation de l'organe délibérant et jusqu'à l'élection du président, les fonctions de président sont assurées par le doyen d'âge " ;<br/>
<br/>
              5. Considérant qu'il résulte de ces dispositions combinées qu'il appartient au président sortant du conseil communautaire de convoquer, dans le délai prévu, les nouveaux élus à la première réunion du conseil communautaire ; que le doyen d'âge préside cette première séance, au cours de laquelle sont installés, à la suite de leur élection, les conseillers communautaires et où il est procédé à l'élection du président, jusqu'à ce que ce dernier soit élu ; qu'en conséquence, le fait que M. Del Poso, président sortant, ait ouvert la séance du conseil communautaire du 9 avril 2014 et déclaré ses membres installés dans leurs fonctions à la place du doyen d'âge, constitue une irrégularité de procédure ; que toutefois, dans les circonstances de l'espèce, une telle irrégularité ne caractérise pas une manoeuvre et, eu égard à sa nature n'a pas altéré la sincérité du scrutin ;<br/>
<br/>
              6. Considérant, en quatrième lieu, qu'en vertu des dispositions de l'article              L. 2122-7 du code général des collectivités territoriales, applicables à l'élection du président d'un établissement public de coopération intercommunale en vertu de l'article L. 5211-2 du même code, l'élection a lieu au scrutin secret ; qu'aucune disposition, ni aucun principe n'impose l'usage d'isoloirs lors des opérations de vote, ni n'interdit aux conseillers municipaux de rédiger eux-mêmes leurs bulletins de vote pour l'élection du président ; qu'il ne résulte pas de l'instruction qu'il aurait été, en l'espèce, porté atteinte au principe du caractère secret du scrutin, sans incidence étant par elle-même la circonstance que les membres du conseil communautaire ont siégé côte à côte ; que d'ailleurs, M. A...n'allègue pas que des conseillers se seraient plaints de ce que le sens de leur vote aurait été divulgué, en méconnaissance de ce principe ; qu'ainsi, le moyen tiré du non-respect du secret du scrutin ne peut qu'être écarté ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que M. A...n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Montpellier a rejeté sa protestation dirigée contre la délibération du 9 avril 2014 du conseil de la communauté de communes Sud-Roussillon proclamant l'élection de M. Del Poso en qualité de président et les opérations électorales préalables à cette élection ; <br/>
<br/>
              8. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. Del Poso au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : Les conclusions de M. Del Poso présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. Jean Jouandet et à M. Thierry Del Poso. <br/>
Copie en sera adressée pour information au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
