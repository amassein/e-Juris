<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033285497</ID>
<ANCIEN_ID>JG_L_2016_10_000000400574</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/28/54/CETATEXT000033285497.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 19/10/2016, 400574, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-10-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400574</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. François Monteagle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:400574.20161019</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              1° Sous le n° 400574, par un mémoire et un mémoire en réplique, enregistrés le 20 juillet et le 12 septembre 2016 au secrétariat du contentieux du Conseil d'État, la commune de Courbevoie demande au Conseil d'État, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de sa requête tendant à l'annulation du décret n° 2016-423 du 8 avril 2016 relatif aux dotations de l'Etat, aux collectivités territoriales et à la péréquation des ressources fiscales, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 5219-8 du code général des collectivités territoriales.<br/>
<br/>
<br/>
              2° Sous le n° 401676, par un mémoire, un mémoire en réplique et un nouveau mémoire, enregistrés le 21 juillet 2016 et le 28 septembre 2016 au secrétariat du contentieux du Conseil d'État, la commune de Courbevoie demande au Conseil d'État, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de sa requête tendant à l'annulation de la note du ministre de l'intérieur du 27 mai 2016 relative à la répartition du fonds national de péréquation des ressources intercommunales et communales (FPIC) pour l'exercice 2016 à destination de la métropole et des départements d'outre-mer, à l'exception de Mayotte, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 5219-8 du code général des collectivités territoriales.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et ses articles 72, 72-2 et 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
              - Vu le code général des collectivités territoriales ; <br/>
              - la loi n° 2015-1785 du 29 décembre 2015 ;<br/>
              - la décision du Conseil constitutionnel n° 2013-685 DC du 29 décembre 2013 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Monteagle, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de la commune de Courbevoie ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. La question soulevée dans les instances visées ci-dessus est unique. Il y a lieu de joindre ces instances et de statuer par une seule décision.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux. <br/>
<br/>
              3. Les articles L. 2336-1 à L. 2336-7 du code général des collectivités territoriales relatifs à la péréquation des ressources fixent les modalités d'alimentation du Fonds national de péréquation des ressources intercommunales et communales (FPIC). En application de ces articles, sont contributeurs au FPIC, sous condition de ressources, d'une part, les communes n'appartenant à aucun groupement à fiscalité propre et, d'autre part, les ensembles intercommunaux, c'est-à-dire les établissements publics de coopération intercommunale à fiscalité propre et leurs communes membres. En vertu des dispositions de l'article L. 2336-3 du code, le prélèvement à destination du FPIC calculé pour chaque ensemble intercommunal est réparti entre l'établissement public de coopération intercommunale à fiscalité propre et ses communes membres en fonction du coefficient d'intégration fiscale, puis, entre les communes membres, en fonction du potentiel financier par habitant de ces communes et de leur population. Les communes de la région Ile-de-France bénéficient dans ce cadre d'un mécanisme de minoration de leur prélèvement, à due concurrence des montants prélevés l'année précédente à destination du Fonds de solidarité des communes de la région Ile-de-France (FSRIF), les montants correspondant à ces minorations étant acquittés par l'établissement public de coopération intercommunale à fiscalité propre auquel appartiennent ces communes. Le 3° du I de l'article L. 2336-3 a prévu un mécanisme de plafonnement des contributions versées au FPIC par les ensembles intercommunaux et les communes de la région Ile-de-France, de sorte que les prélèvements opérés au titre du FPIC et du FSRIF ne puissent excéder 13 % des ressources prises en compte pour le calcul du potentiel fiscal.<br/>
<br/>
              4. L'article L. 5219-8 du code général des collectivités territoriales, dans sa rédaction issue de l'article 162 de la loi du 29 décembre 2015 de finances pour 2016, prévoit, pour la métropole du Grand Paris,  des modalités d'alimentation du FPIC qui dérogent, sur plusieurs points, aux articles L. 2336-1 à L. 2336-7 du code général des collectivités territoriales. Il exclut des contributeurs au FPIC la métropole du Grand Paris elle-même, qui est un établissement public de coopération intercommunale à fiscalité propre à statut particulier. Il prévoit en revanche que les établissements publics territoriaux, qui ne sont pas des établissements publics de coopération intercommunale à fiscalité propre mais sont soumis aux dispositions applicables aux syndicats de communes, constituent, avec les communes qui en sont membres, des ensembles intercommunaux pour l'application des articles L. 2336-1 à L. 2336-7. Ces établissements contribuent à ce titre au financement du FPIC et bénéficient du plafonnement prévu au 3° du I de l'article L. 2336-3 du code. L'article L. 5219-8 prévoit en outre que le prélèvement à destination du FPIC calculé pour chaque établissement public territorial est réparti entre cet établissement et ses communes membres de façon que le prélèvement supporté par l'établissement public territorial soit égal à la somme des prélèvements supportés en 2015 par les groupements à fiscalité propre qui lui préexistaient. Le reste du prélèvement de chaque ensemble intercommunal est ensuite réparti entre les communes membres de l'établissement public territorial en fonction des prélèvements des communes calculés en 2015, sans que soient pris en compte, dans les modalités de calcul de cette répartition finale, les éventuels plafonnements opérés dans les conditions mentionnées au point 3  ci-dessus.<br/>
<br/>
              5. La commune de Courbevoie soutient, en premier lieu, que les dispositions de l'article L. 5219-8 du code général des collectivités territoriales méconnaissent les principes constitutionnels d'égalité et d'égalité devant les charges publiques, garantis par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen. <br/>
<br/>
              6. Les principes constitutionnels d'égalité devant la loi et d'égalité devant les charges publiques ne s'opposent ni à ce que le législateur règle de façon différente des situations différentes ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général pourvu que, dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport avec l'objet de la loi qui l'établit. Pour assurer le respect du principe d'égalité, le législateur doit fonder son appréciation sur des critères objectifs et rationnels en fonction des buts qu'il se propose. Par ailleurs, cette appréciation ne doit pas entraîner de rupture caractérisée de l'égalité devant les charges publiques. <br/>
<br/>
              7. La commune de Courbevoie soutient, tout d'abord, que l'application des dispositions des articles L. 2336-1 à L. 2336-7 du code général des collectivités territoriales conduit à faire des métropoles et de leurs communes membres des ensembles intercommunaux contributeurs au FPIC et qu'en prévoyant, par dérogation à ces dispositions, que ce sont les établissements publics territoriaux de la métropole du Grand Paris et leurs communes membres qui contribuent au FPIC, l'article L. 5219-8 crée, entre la métropole du Grand Paris et les autres métropoles, une différence de traitement qui méconnaît les principes d'égalité et d'égalité devant les charges publiques. Toutefois, eu égard à l'importance de la population de la métropole du Grand Paris, l'application des règles générales de calcul des contributions au FPIC, fixées aux articles L. 2336-1 et suivants du code général des collectivités territoriales, aurait conduit à une diminution manifestement disproportionnée de la contribution versée par les collectivités de la métropole du Grand Paris et à un bouleversement de l'équilibre existant, au niveau national, entre contributeurs au FPIC. Le traitement dérogatoire prévu par l'article L. 5219-8 du code général des collectivités territoriales au titre du FPIC est ainsi fondé sur des critères objectifs et rationnels en rapport avec l'objectif de la loi. Dès lors, le moyen tiré de ce que l'article L. 5219-8 du code général des collectivités territoriales méconnaîtrait, sur ce point, les principes d'égalité et d'égalité devant les charges publiques ne peut être regardé comme sérieux.<br/>
<br/>
              8. La commune de Courbevoie soutient ensuite que les dispositions litigieuses créent une différence de traitement entre les communes de la métropole du Grand Paris qui appartenaient à un groupement à fiscalité propre en 2015 et les communes qui étaient, à cette date, isolées, qui méconnaît les principes d'égalité et d'égalité devant les charges publiques. Selon elle, en effet, ces dispositions excluraient, pour les premières et non pour les secondes, la prise en compte du dispositif de plafonnement mentionné au point 3 ci-dessus. Toutefois, il résulte des dispositions combinées des articles L. 2336-3 et L. 5219-8 du code général des collectivités territoriales, d'une part, que les communes membres de la métropole du Grand Paris bénéficient du mécanisme de plafonnement au niveau de chaque ensemble intercommunal, constitué par un établissement public territorial et ses communes membres, d'autre part, que les modalités de calcul du prélèvement destiné au FPIC restant à la charge des communes membres d'un même établissement public territorial sont identiques quelle que soit la situation antérieure de la commune, en ce qu'elles ne prennent pas en compte, à ce stade, les éventuels plafonnements opérés antérieurement. Dès lors, l'argumentation développée par la commune sur ce point ne peut être regardée comme sérieuse.<br/>
<br/>
              9. La commune de Courbevoie soutient en outre que les dispositions litigieuses créent, entre les communes appartenant à la métropole du Grand Paris et les autres communes d'Ile-de-France une différence de traitement qui méconnaît le principe d'égalité, dès lors que les communes membres de la métropole du Grand Paris seraient, selon elle, exclues du bénéfice du dispositif de minoration de leur prélèvement à destination du FPIC. Toutefois, dans le but de maintenir les contributions au FPIC des établissements publics territoriaux de la métropole du Grand Paris au niveau de celles que supportaient en 2015 les établissements publics auxquels ils ont succédé, le législateur a prévu, au a) du 2° de l'article L. 5219-8 du code général des collectivités territoriales, que les prélèvements supportés par les établissements publics territoriaux de la métropole du Grand Paris soient égaux à la somme des prélèvements supportés en 2015 par les groupements à fiscalité propre qui leur préexistaient. Dans ce cadre, ces prélèvements intègrent le montant des minorations de prélèvements précédemment opérées au bénéfice des communes, en application du dernier alinéa du II de l'article L. 2336-3 du code général des collectivités territoriales. Dès lors, l'argumentation développée par la commune sur ce point ne peut être regardée comme sérieuse.  <br/>
<br/>
              10. La commune de Courbevoie soutient, en second lieu,  que les dispositions litigieuses portent atteinte aux principes constitutionnels de libre-administration et d'autonomie financière des collectivités territoriales, garantis par les articles 72 et 72-2 de la Constitution, pour les mêmes motifs que ceux qui ont été analysés aux points 7 à 9 ci-dessus. Il résulte de ce qui a été dit à ces mêmes points que le moyen tiré de la méconnaissance la libre administration des collectivités territoriales et, en tout état de cause, de leur autonomie financière, ne peut être regardé comme sérieux.<br/>
<br/>
              11. Il résulte de ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que l'article L. 5219-8 du code général des collectivités territoriales porte atteinte aux droits et libertés garantis par la Constitution doit être écarté.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la commune de Courbevoie sous les nos 400574 et 401676.<br/>
Article 2 : La présente décision sera notifiée à la commune de Courbevoie et au ministre de l'intérieur.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre, au ministre de l'économie et des finances et au ministre de l'aménagement du territoire et de la ruralité. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
