<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035911940</ID>
<ANCIEN_ID>JG_L_2017_10_000000412907</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/91/19/CETATEXT000035911940.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 26/10/2017, 412907</TITRE>
<DATE_DEC>2017-10-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412907</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:412907.20171026</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire, enregistré le 9 juin 2017 au greffe du tribunal administratif de Paris, présenté en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, M. B...A...a, à l'appui de ses conclusions tendant à l'annulation de l'avis rendu par le collège de déontologie de la juridiction administrative le 10 mars 2017 ainsi que de la décision du même jour de rendre public cet avis, soulevé une question prioritaire de constitutionnalité relative à la conformité aux droits et libertés garantis par la Constitution de l'article L. 131-6 du code de justice administrative.<br/>
<br/>
              Par une ordonnance n° 1709594 du 28 juillet 2017, enregistrée le 31 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, le président de la 4ème section du tribunal administratif a, avant qu'il soit statué sur la demande de M.A..., décidé, par application des dispositions de l'article 23-2 de l'ordonnance du 7 novembre 1958, de transmettre au Conseil d'Etat cette question prioritaire de constitutionnalité.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'à l'appui de conclusions formées devant le tribunal administratif de Paris à l'occasion d'un recours portant sur la légalité d'avis du collège de déontologie de la juridiction administrative, M. A...a soulevé, en application de l'article 23-1 de l'ordonnance organique du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, le moyen tiré de ce que l'article L. 131-6 du code de justice administrative porte atteinte aux droits et libertés que la Constitution garantit ; que, par une ordonnance du 28 juillet 2017, le président de la 4ème section du tribunal administratif de Paris a transmis cette question au Conseil d'Etat en application de l'article 23-2 de cette même ordonnance ; <br/>
<br/>
              2. Considérant que, postérieurement à cette transmission, M. A...a déclaré devant le Conseil d'Etat qu'il se désistait de sa demande tendant à ce que le Conseil constitutionnel soit saisi de la question prioritaire de constitutionnalité qu'il avait soulevée devant le tribunal administratif de Paris ;<br/>
<br/>
              3. Considérant que le désistement de M. A...est pur et simple ; que rien ne s'oppose à ce qu'il en soit donné acte ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Il est donné acte du désistement de M. A...de sa demande tendant à ce que le Conseil d'Etat transmette au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 131-6 du code de justice administrative.<br/>
Article 2 : La présente décision sera notifiée à M. B...A..., au président du tribunal administratif de Paris et à la ministre de la justice.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-05-04-02 PROCÉDURE. INCIDENTS. DÉSISTEMENT. PORTÉE ET EFFETS. - DÉSISTEMENT DEVANT LE CONSEIL D'ETAT, POSTÉRIEUR À LA TRANSMISSION DE LA QPC AU CONSEIL D'ETAT PAR UNE JURIDICTION DU FOND, DE LA DEMANDE DE RENVOI DE LA QPC AU CONSEIL CONSTITUTIONNEL - CONSÉQUENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-10 PROCÉDURE. - QUESTION TRANSMISE AU CONSEIL D'ETAT PAR UNE JURIDICTION DU FOND - DÉSISTEMENT DEVANT LE CONSEIL D'ETAT, POSTÉRIEUR À LA TRANSMISSION, DE LA DEMANDE DE RENVOI DE LA QPC AU CONSEIL CONSTITUTIONNEL - CONSÉQUENCE [RJ1].
</SCT>
<ANA ID="9A"> 54-05-04-02 Il y a lieu pour le Conseil d'Etat de donner acte au requérant du désistement de sa demande, transmise par une juridiction de fond, tendant au renvoi au Conseil Constitutionnel d'une question prioritaire de constitutionnalité, lorsque celui-ci a annoncé devant le Conseil d'Etat se désister de cette demande.</ANA>
<ANA ID="9B"> 54-10 Il y a lieu pour le Conseil d'Etat de donner acte au requérant du désistement de sa demande, transmise par une juridiction de fond, tendant au renvoi au Conseil Constitutionnel d'une question prioritaire de constitutionnalité, lorsque celui-ci a annoncé devant le Conseil d'Etat se désister de cette demande.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 22 juillet 2015, Fondation pour l'école, Centre de promotion de la coiffure, n°s 387472, 390172, inédite au Recueil. Comp., pour le cas d'un désistement de l'instance devant les juges du fond après transmission d'une QPC au Conseil d'Etat, CE, 1er février 2012, Commune des Angles, n° 353945, T. pp. 923-924-956 et CE, 31 mars 2014, Commune de Saint-Germain-en-Laye, n° 374855, T. pp. 802-804-835.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
