<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033314151</ID>
<ANCIEN_ID>JG_L_2016_10_000000381824</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/31/41/CETATEXT000033314151.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème chambres réunies, 27/10/2016, 381824, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-10-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>381824</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:381824.20161027</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Kerviande a demandé au tribunal administratif de Rennes de prononcer la décharge de la taxe sur les achats de viande à laquelle elle a été assujettie au titre de la période du 1er septembre 2002 au 30 novembre 2003 et des intérêts moratoires correspondants. Par un jugement n° 0804446 du 8 septembre 2011, le tribunal administratif de Rennes a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 11NT02849 du 17 avril 2014, la cour administrative d'appel de Nantes a rejeté l'appel formé par la société contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 25 juin 2014, 19 septembre 2014 et 9 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, la société Kerviande demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'État la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le décret n° 2000-348 du 20 avril 2000 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de la société Kerviande;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par une décision du 17 septembre 2015, postérieure à l'introduction du pourvoi, l'administration a prononcé le dégrèvement des intérêts moratoires, d'un montant de 5 928 euros, qui avaient été mis à la charge de la société Kerviande. Par suite, il n'y a pas lieu de statuer sur les conclusions du pourvoi tendant à la décharge des intérêts moratoires.<br/>
<br/>
              2. Aux termes de l'article L. 256 du livre des procédures fiscales, dans sa rédaction applicable à la procédure en litige : " Un avis de mise en recouvrement est adressé par le comptable public à tout redevable des sommes, droits, taxes et redevances de toute nature dont le recouvrement lui incombe lorsque le paiement n'a pas été effectué à la date d'exigibilité. / Un avis de mise en recouvrement est également adressé par le comptable public pour la restitution des sommes, droits, taxes et redevances de toute nature mentionnés au premier alinéa et indûment versés par l'État. (...)  ". Aux termes de l'article R. 256-1 du même livre, dans sa rédaction alors applicable : " L'avis de mise en recouvrement prévu à l'article L. 256 indique pour chaque impôt ou taxe le montant global des droits, des pénalités et des intérêts de retard qui font l'objet de cet avis. / Lorsque l'avis de mise en recouvrement est consécutif à une procédure de rectification contradictoire, il fait référence soit à la proposition de rectification prévue à l'article L. 57 ou à la notification prévue à l'article L. 76 et, le cas échéant, au document adressé au contribuable l'informant d'une modification des droits, taxes et pénalités résultant des rectifications. / (...) L'avis de mise en recouvrement, dans le cas mentionné au deuxième alinéa de l'article L. 256, indique seulement le montant de la somme indûment versée, et la date de son versement ". Si, depuis l'entrée en vigueur du décret du 20 avril 2000 susvisé modifiant l'article R. 256-1 du livre des procédures fiscales définissant le contenu de l'avis de mise en recouvrement, la mention de la période d'imposition dans l'avis de mise en recouvrement consécutif à une procédure de rectification n'est plus prescrite, cet avis et la proposition de rectification à laquelle il doit continuer de faire référence ne sauraient induire en erreur le contribuable sur la période d'imposition en litige sans priver ce dernier d'une garantie.<br/>
<br/>
              3. En premier lieu, il ressort des pièces du dossier soumis aux juges du fond que l'administration a poursuivi une procédure de rectification contradictoire avant d'émettre un avis de mise en recouvrement pour obtenir la restitution de sommes indûment remboursées par l'État à la société Kerviande au titre de la taxe sur les achats de viande.  Dès lors que la procédure prévue par le deuxième alinéa de l'article L. 256 du livre des procédures fiscales n'avait pas été mise en oeuvre, la cour n'a pas commis d'erreur de droit en jugeant que l'administration n'était pas tenue d'indiquer sur cet avis le montant de la somme indûment versée et la date de son versement.<br/>
<br/>
              4. En second lieu, il ressort des énonciations de l'arrêt attaqué que l'avis de mise en recouvrement émis le 19 novembre 2007 comporte une mention erronée de la période au titre de laquelle la société Kerviande doit restituer la somme de 156 306 euros correspondant à la taxe sur les achats de viande et que la proposition de rectification du 20 décembre 2004, à laquelle cet avis fait référence, comporte également une mention erronée de la période d'imposition. Toutefois, après avoir estimé, par une appréciation souveraine dont il n'est pas argué qu'elle serait entachée de dénaturation, que ces erreurs n'ont pas empêché la société d'identifier sans aucune ambigüité les sommes dont l'État poursuivait le recouvrement et qu'elles n'ont donc pas pu l'induire en erreur sur la période d'imposition concernée, la cour n'a pas commis d'erreur de droit en jugeant que ces erreurs matérielles n'ont pas privé le contribuable d'une garantie.<br/>
<br/>
              5. Il résulte de ce qui précède que le surplus des conclusions du pourvoi de la société Kerviande doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions du pourvoi de la société Kerviande tendant à la décharge des intérêts moratoires.<br/>
Article 2 : Le surplus des conclusions du pourvoi de la société Kerviande est rejeté.<br/>
Article 3 : La présente décision sera notifiée à la société Kerviande et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
