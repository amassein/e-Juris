<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029709166</ID>
<ANCIEN_ID>JG_L_2014_11_000000365121</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/70/91/CETATEXT000029709166.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 05/11/2014, 365121</TITRE>
<DATE_DEC>2014-11-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365121</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE ; SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>M. Charles Touboul</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:365121.20141105</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 11 janvier et 10 avril 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Saint-Martin-de-Belleville, représentée par son maire ; la commune de Saint-Martin-de-Belleville demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 12LY00623 du 13 novembre 2012 par lequel la cour administrative d'appel de Lyon, après avoir annulé pour irrégularité le jugement n° 0905540 du tribunal administratif de Grenoble du 30 décembre 2011, a annulé l'arrêté du préfet de la région Provence-Alpes-Côte d'Azur du 14 octobre 2009 l'autorisant à créer une unité touristique nouvelle ayant pour objet l'aménagement sur les sites des Ménuires et de Val-Thorens de deux terrains pour l'utilisation de motos-neige à des fins de loisirs ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge des associations Mouvement Homme et Nature - Fédération Rhône-Alpes de protection de la nature - comité de la Savoie, dite FRAPNA Savoie, et Mountain Wilderness, demanderesses devant le tribunal administratif, le versement d'une somme de 1 000 euros chacune au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu la loi n° 91-2 du 3 janvier 1991;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles Touboul, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat de la commune de Saint-Martin-de-Belleville et à la SCP Rousseau, Tapie, avocat de l'association Mouvement Homme et Nature - Fédération Rhône-Alpes protection nature - comité de la Savoie et de l'association Mountain  Wilderness ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 14 octobre 2009, le préfet de la région Provence-Alpes-Côte d'Azur, préfet coordonnateur du massif des Alpes, a autorisé la création par la commune de Saint-Martin-de-Belleville (Savoie) d'une unité touristique nouvelle en vue de l'aménagement sur les sites des Ménuires et de Val-Thorens de " deux terrains de sports ou loisirs motorisés " ; que, à la demande de l'association Mouvement Homme et Nature - Fédération Rhône-Alpes de protection de la nature - comité de la Savoie, dite " FRAPNA Savoie ", et de l'association Mountain Wilderness, le tribunal administratif de Grenoble a annulé cet arrêté par un jugement du 30 décembre 2011 ; que, saisie par la commune de Saint-Martin-de-Belleville, la cour administrative d'appel de Lyon a, par l'arrêt attaqué du 13 novembre 2012, annulé ce jugement pour irrégularité puis, évoquant la demande des associations, annulé l'arrêté préfectoral litigieux ; <br/>
<br/>
              Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              2. Considérant que l'Etat n'a pas introduit d'appel contre le jugement mentionné ci-dessus du 30 décembre 2011, alors que, défendeur en première instance, il aurait été recevable à le faire ; que si la cour avait la faculté de le mettre en cause pour qu'il produise des observations sur l'appel formé par la commune, elle n'a, en s'abstenant de le faire, ni méconnu le caractère contradictoire de la procédure, ni porté atteinte aux droits de la défense ; que le moyen tiré de ce que, en l'absence d'une telle mise en cause, l'arrêt attaqué serait entaché d'irrégularité doit, en tout état de cause, être écarté ;<br/>
<br/>
              Sur le bien fondé de l'arrêt attaqué :<br/>
<br/>
              3. Considérant, d'une part, qu'aux termes de l'article  L. 145-9 du code de l'urbanisme : " Est considérée comme unité touristique nouvelle toute opération de développement touristique, en zone de montagne, ayant pour objet ou pour effet, en une ou plusieurs tranches (...) de réaliser des aménagements touristiques ne comprenant pas de surfaces de plancher dont la liste est fixée par décret en Conseil d'Etat " ; qu'aux termes de l'article R. 145-2 du même code : " Sont soumises à autorisation du préfet coordonnateur de massif, en application du I de l'article L. 145-11, les unités touristiques nouvelles ayant pour objet : (...) 3° Lorsqu'ils sont soumis à étude d'impact en application de l'article L. 122-1 du code de l'environnement : (...) c) L'aménagement de terrains pour la pratique de sports ou de loisirs motorisés " ; qu'en vertu des dispositions combinées des articles L. 122-1 et R. 122-8 du code de l'environnement, les projets visant à l'aménagement de " terrains " pour la pratique de sports ou loisirs motorisés d'une emprise totale supérieure à quatre hectares sont soumis à étude d'impact ; que de tels projets revêtent par suite, en zone de montagne, le caractère d'unités touristiques nouvelles devant faire l'objet d'une autorisation du préfet coordonnateur de massif ;<br/>
<br/>
              4. Considérant, d'autre part, qu'aux termes de l'article L. 362-1 du code de l'environnement : " En vue d'assurer la protection des espaces naturels, la circulation des véhicules à moteur est interdite en dehors des voies classées dans le domaine public routier de l'Etat, des départements et des communes, des chemins ruraux et des voies privées ouvertes à la circulation publique des véhicules à moteur " ; qu'aux termes de l'article L. 362-3 du même code : " L'ouverture de terrains pour la pratique de sports motorisés est soumise à l'autorisation prévue à l'article L. 421-2 du code de l'urbanisme./ (...) / L'utilisation, à des fins de loisirs, d'engins motorisés conçus pour la progression sur neige est interdite, sauf sur les terrains ouverts dans les conditions prévues au premier alinéa " ; qu'aux termes de l'article L. 421-2 du code de l'urbanisme : " Les travaux, installations et aménagements affectant l'utilisation des sols et figurant sur une liste arrêtée par décret en Conseil d'Etat doivent être précédés de la délivrance d'un permis d'aménager " ; qu'aux termes de l'article R*. 421-19 du même code : " Doivent être précédés d'un permis d'aménager : ... g) l'aménagement d'un terrain pour la pratique des sports ou loisirs motorisés " ;<br/>
<br/>
              5. Considérant qu'il résulte de ces dispositions, éclairées par les travaux parlementaires préalables à l'adoption de la loi du 3 janvier 1991 relative à la circulation des véhicules terrestres dans les espaces naturels et portant modification du code des communes, de laquelle sont issues les dispositions ultérieurement codifiées aux articles L. 362-1 et L. 362-3 précités du code de l'environnement, que le législateur a entendu encadrer strictement les conditions dans lesquelles peut être autorisé l'aménagement en zone de montagne de " terrains " pour la pratique de sports ou de loisirs motorisés en vue de l'utilisation, à des fins de loisirs, d'engins conçus pour la progression sur neige ; qu'il a, en particulier, entendu empêcher la création d'itinéraires, mêmes balisés, lesquels ne peuvent être regardés comme des " terrains " au sens de la loi ;<br/>
<br/>
              6. Considérant  que la cour a relevé que le projet d'unité touristique nouvelle litigieux consistait en des boucles de 9,5 et 8 kilomètres, autour d'espaces de 570 et 424 hectares, dans des zones demeurées essentiellement naturelles, empruntant des pistes situées sur le domaine skiable des Ménuires et de Val-Thorens ; qu'elle a pu en déduire, sans commettre d'erreur de droit ni dénaturer les faits qui lui étaient soumis, que ces circuits constituaient des itinéraires balisés et non des terrains et que le préfet coordonnateur de massif ne pouvait, dès lors, légalement autoriser une unité touristique nouvelle en application des dispositions précitées ; que son arrêt est suffisamment motivé sur ce point ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que le pourvoi de la commune de Saint-Martin-de-Belleville doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Saint-Martin-de-Belleville, au titre des mêmes dispositions, une somme globale de 3 000 euros à verser à l'association Mouvement Homme et Nature - Fédération Rhône-Alpes de protection de la nature - comité de la Savoie et à l'association Mountain Wilderness ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de la commune de Saint-Martin-de-Belleville est rejeté.<br/>
<br/>
Article 2 : La commune de Saint-Martin-de-Belleville versera à l'association Mouvement Homme et Nature - Fédération Rhône-Alpes de protection de la nature - comité de la Savoie, dite FRAPNA Savoie et à l'association Mountain Wilderness la somme globale de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la commune de Saint-Martin-de-Belleville, à l'association Mouvement Homme et Nature - Fédération Rhône-Alpes de protection de la nature - comité de la Savoie, dite FRAPNA Savoie et à l'association Mountain Wilderness. <br/>
Copie en sera adressée, pour information, à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">63-05-02 SPORTS ET JEUX. SPORTS. ÉQUIPEMENTS SPORTIFS. - AMÉNAGEMENT DE TERRAINS DESTINÉS À LA PRATIQUE DES MOTONEIGES EN APPLICATION DE L'ARTICLE L. 263-3 DU CODE DE L'URBANISME- NOTION - ITINÉRAIRE BALISÉ DANS LA MONTAGNE - EXCLUSION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-04-044 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. AUTORISATIONS D`UTILISATION DES SOLS DIVERSES. AUTORISATIONS RELATIVES AUX ÉQUIPEMENTS DE SKI. - AMÉNAGEMENT DE TERRAINS DESTINÉS À LA PRATIQUE DES MOTONEIGES EN APPLICATION DE L'ARTICLE L. 263-3 DU CODE DE L'URBANISME- NOTION - ITINÉRAIRE BALISÉ DANS LA MONTAGNE - EXCLUSION.
</SCT>
<ANA ID="9A"> 63-05-02 Il résulte des dispositions des articles L. 362-1 et L. 263-3 du code de l'environnement, éclairées par les travaux parlementaires préalables à l'adoption de la loi du 3 janvier 1991 à l'origine de ces dispositions, que le législateur a entendu encadrer strictement les conditions dans lesquelles peut être autorisé l'aménagement en zone de montagne de  terrains  pour la pratique de sports ou de loisirs motorisés en vue de l'utilisation, à des fins de loisirs, d'engins conçus pour la progression sur neige. Il a, en particulier, entendu empêcher la création d'itinéraires, mêmes balisés, lesquels ne peuvent être regardés comme des  terrains  au sens de la loi. L'aménagement de ces terrains nécessite l'obtention du permis d'aménager prévu à l'article L. 421-2 du code de l'urbanisme.</ANA>
<ANA ID="9B"> 68-04-044 Il résulte des dispositions des articles L. 362-1 et L. 263-3 du code de l'environnement, éclairées par les travaux parlementaires préalables à l'adoption de la loi du 3 janvier 1991 à l'origine de ces dispositions, que le législateur a entendu encadrer strictement les conditions dans lesquelles peut être autorisé l'aménagement en zone de montagne de  terrains  pour la pratique de sports ou de loisirs motorisés en vue de l'utilisation, à des fins de loisirs, d'engins conçus pour la progression sur neige. Il a, en particulier, entendu empêcher la création d'itinéraires, mêmes balisés, lesquels ne peuvent être regardés comme des  terrains  au sens de la loi. L'aménagement de ces terrains nécessite l'obtention du permis d'aménager prévu à l'article L. 421-2 du code de l'urbanisme.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
