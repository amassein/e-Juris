<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029288327</ID>
<ANCIEN_ID>JG_L_2014_07_000000379102</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/28/83/CETATEXT000029288327.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 23/07/2014, 379102, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>379102</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Christophe Pourreau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:379102.20140723</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 24 avril 2014 au secrétariat du contentieux du Conseil d'Etat, M. A...B...demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret n° 2014-233 du 24 février 2014 portant délimitation des cantons dans le département du Pas-de-Calais.<br/>
<br/>
              Il soutient que le décret attaqué :<br/>
<br/>
              - a été édicté au terme d'une procédure irrégulière dès lorsque ni les élus locaux, ni les établissements publics de coopération intercommunale du département n'ont été consultés ;<br/>
<br/>
              - est insuffisamment motivé ;<br/>
<br/>
              - a été pris sur la base de données démographiques erronées ;<br/>
<br/>
              - ne respecte ni le schéma départemental de coopération intercommunale, ni les limites des arrondissements et des circonscriptions, ni les limites des anciens cantons et émiette l'existant ne prend pas en compte le travail réalisé par les élus de trois anciennes intercommunalités ;<br/>
<br/>
              - est entaché d'une erreur manifeste d'appréciation au motif qu'il a désigné la commune d'Auxi-le-Château, et non celle d'Hesdin, en tant que bureau centralisateur.<br/>
<br/>
              La requête a été communiquée au Premier ministre et au ministre de l'intérieur, qui n'ont pas produit de mémoire.<br/>
<br/>
<br/>
	Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code électoral ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 79-587 du 11 juillet 1979 ;<br/>
              - le décret n° 2013-938 du 18 octobre 2013 ;<br/>
              - le décret n° 2012-1479 du 27 décembre 2012 ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 191-1 du code électoral, résultant de la loi du 17 mai 2013 relative à l'élection des conseillers départementaux, des conseillers municipaux et des conseillers communautaires et modifiant le calendrier électoral : " Le nombre de cantons dans lesquels seront élus les conseillers départementaux est égal, pour chaque département, à la moitié du nombre de cantons existant au 1er janvier 2013, arrondi à l'unité impaire supérieure si ce nombre n'est pas entier impair. / Le nombre de cantons dans chaque département comptant plus de 500 000 habitants ne peut être inférieur à dix-sept. Il ne peut être inférieur à treize dans chaque département comptant entre 150 000 et 500 000 habitants ". Aux termes de l'article L. 3113-2 du code général des collectivités territoriales, dans sa rédaction issue de la même loi, applicable à la date du décret attaqué : " I. - Les modifications des limites territoriales des cantons, les créations et suppressions de cantons et le transfert du siège de leur chef-lieu sont décidés par décret en Conseil d'Etat après consultation du conseil général qui se prononce dans un délai de six semaines à compter de sa saisine. A l'expiration de ce délai, son avis est réputé rendu. / (...) III. - La modification des limites territoriales des cantons effectuée en application du I est conforme aux règles suivantes : / a) Le territoire de chaque canton est défini sur des bases essentiellement démographiques ; / b) Le territoire de chaque canton est continu ; / c) Est entièrement comprise dans le même canton toute commune de moins de 3 500 habitants ; / IV. - Il n'est apporté aux règles énoncées au III que des exceptions de portée limitée, spécialement justifiées, au cas par cas, par des considérations géographiques ou par d'autres impératifs d'intérêt général. ".<br/>
<br/>
              2. Compte tenu de la réduction du nombre des cantons résultant de l'application de l'article L. 191-1 du code électoral, le décret attaqué a, sur le fondement de l'article L. 3113-2 du code général des collectivités territoriales, procédé à une nouvelle délimitation des cantons du département du Pas-de-Calais.<br/>
<br/>
              3. En premier lieu, ni l'article L. 3113-2 du code général des collectivités territoriales, ni aucun autre texte non plus qu'aucun principe n'imposaient de procéder, préalablement à l'intervention du décret attaqué, à la consultation des communes ou des établissements publics de coopération intercommunale du département ou de prendre en compte le travail effectué par les élus.<br/>
<br/>
              4. En deuxième lieu, le décret attaqué, qui a le caractère d'un acte réglementaire, n'est pas au nombre des décisions qui doivent être motivées en vertu des dispositions de l'article premier de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public.<br/>
<br/>
              5. En troisième lieu, aux termes de l'article 71 du décret du 18 octobre 2013, dans sa rédaction applicable à la date du décret attaqué et dont la légalité n'est pas contestée : "  (...) Pour la première délimitation générale des cantons opérée en application de l'article L. 3113-2  du code général des collectivités territoriales, dans sa rédaction résultant de l'article 46 de la loi n° 2013-403 du 17 mai 2013 relative à l'élection des conseillers départementaux, (...), le chiffre de la population municipale auquel il convient de se référer est celui authentifié par le décret n° 2012-1479 du 27 décembre 2012 authentifiant les chiffres des populations (...) ". Or, il est constant que les nouveaux cantons du département du Pas-de-Calais ont été délimités sur la base des données authentifiées par le décret du 27 décembre 2012. Par suite, le moyen tiré de ce que les données retenues pour le redécoupage des cantons de ce département ne correspondraient pas à la réalité démographique ne peut qu'être écarté.<br/>
<br/>
              6. En quatrième lieu, ni l'article L. 3113-2 du code général des collectivités territoriales, ni aucun autre texte non plus qu'aucun principe n'imposaient au Premier ministre de prévoir que les limites des cantons, qui sont des circonscriptions électorales, coïncident avec les limites des circonscriptions législatives, les périmètres des établissements publics de coopération intercommunale, le schéma départemental de coopération intercommunale, les périmètres des arrondissements ou les limites des " bassins de vie " définis par l'institut national de la statistique et des études économiques ou de retenir la proximité géographique des communes comme critère de délimitation des cantons.<br/>
<br/>
              7. Il résulte de tout ce qui précède que M. B...n'est pas fondé à demander l'annulation pour excès de pouvoir du décret attaqué.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à notifiée à M. A...B..., au Premier ministre et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
