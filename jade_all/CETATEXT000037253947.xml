<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037253947</ID>
<ANCIEN_ID>JG_L_2018_07_000000403332</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/25/39/CETATEXT000037253947.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 26/07/2018, 403332, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403332</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BRIARD</AVOCATS>
<RAPPORTEUR>Mme Marie-Gabrielle Merloz</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:403332.20180726</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Crédit industriel et commercial (CIC) a demandé au tribunal administratif de Paris de prononcer la restitution de sommes correspondant à une partie de l'impôt sur les sociétés qu'elle a acquitté au titre des exercices 2000 et 2001. Par deux ordonnances du 4 octobre 2013, le président de la 2ème chambre de la 1ère section du tribunal a rejeté ses demandes.<br/>
<br/>
              Par un arrêt du 8 juillet 2016, la cour administrative d'appel de Paris a annulé ces ordonnances et rejeté les demandes présentées par la société CIC devant le tribunal administratif de Paris ainsi que le surplus des conclusions de ses requêtes d'appel.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 7 septembre et 7 décembre 2016 et le 13 juin 2018 au secrétariat du contentieux du Conseil d'Etat, la société CIC demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire intégralement droit à ses requêtes d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 8 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le traité instituant la Communauté européenne ;<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - l'arrêt C-446/04 du 12 décembre 2006 de la Cour de justice des Communautés européennes et les arrêts C-310/09 du 15 septembre 2011 et C-35/11 du 13 novembre 2012 de la Cour de justice de l'Union européenne ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Gabrielle Merloz, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Briard, avocat de la société Crédit industriel et commercial.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Crédit industriel et commercial (CIC) Est, filiale du groupe intégré CIC et anciennement société CIAL, a perçu, au titre des exercices clos en 2000 et 2001, des dividendes, versés par ses filiales établies dans d'autres Etats membres de l'Union européenne, qui ne relevaient pas du régime fiscal des sociétés mères. Elle n'a pas pu bénéficier, à l'occasion de ces distributions, de l'avoir fiscal prévu par les dispositions alors applicables de l'article 158 bis du code général des impôts qui réservaient le bénéfice de ce crédit d'impôt aux seuls dividendes de source française. Par des réclamations du 22 décembre 2005, la société CIC, en sa qualité de tête du groupe fiscalement intégré, a sollicité la restitution de l'impôt sur les sociétés acquitté à raison de ces dividendes. L'administration fiscale lui ayant opposé un refus implicite, elle a saisi le tribunal administratif de Paris de deux demandes tendant à la restitution, pour la première, de la somme de 2 145 236,86 euros au titre de l'exercice clos en 2001 et, pour la seconde, des sommes de 624 809 et 1 049 948 euros au titre, respectivement, des exercices clos en 2000 et 2001. Par des ordonnances du 4 octobre 2013, le président de la 2ème chambre de la 1ère section de ce tribunal a rejeté ses demandes. Par l'arrêt attaqué du 8 juillet 2016, la cour administrative d'appel de Paris, après avoir annulé ces ordonnances, a rejeté les demandes présentées par la société CIC devant le tribunal administratif de Paris ainsi que le surplus de ses conclusions d'appel.<br/>
<br/>
              Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              2. Aux termes de l'article R. 611-1 du code de justice administrative : " La requête et les mémoires, ainsi que les pièces produites par les parties, sont déposés ou adressés au greffe. / La requête, le mémoire complémentaire annoncé dans la requête et le premier mémoire de chaque défendeur sont communiqués aux parties avec les pièces jointes dans les conditions prévues aux articles R. 611-3, R. 611-5 et R. 611-6. / Les répliques, autres mémoires et pièces sont communiqués s'ils contiennent des éléments nouveaux ". Il résulte de ces dispositions, destinées à garantir le caractère contradictoire de l'instruction, que la méconnaissance de l'obligation de communiquer un mémoire ou une pièce contenant des éléments nouveaux est en principe de nature à entacher la procédure d'irrégularité. Il n'en va autrement que dans le cas où il ressort des pièces du dossier que, dans les circonstances de l'espèce, cette méconnaissance n'a pu préjudicier aux droits des parties.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que la société CIC a produit devant la cour administrative d'appel, les 27 mai 2015, 10 février 2016, 8 avril 2016 et 27 mai 2016,  des mémoires et des pièces sur lesquels la cour s'est fondée pour rendre l'arrêt attaqué. Toutefois, dès lors que la cour a rejeté les conclusions de la société requérante tendant à la restitution des impositions et pénalités en litige, l'absence de communication à l'administration fiscale des mémoires et pièces en cause n'a pu préjudicier ni aux droits de cette dernière, ni à ceux de la société. Par suite, cette méconnaissance de l'obligation posée par l'article R. 611-1 du code de justice administrative ne saurait entacher la procédure d'irrégularité. Le moyen soulevé par la société CIC, tiré de l'atteinte au principe du caractère contradictoire de la procédure, doit dès lors être écarté.<br/>
<br/>
              Sur le bien fondé de l'arrêt attaqué :<br/>
<br/>
              4. La société requérante soutient que la cour a commis une erreur de droit en se fondant sur l'arrêt " Accor " de la Cour de justice de l'Union européenne du 15 septembre 2011 (aff. C-310/09) et en assimilant le contentieux afférent au précompte mobilier relatif à des distributions relevant du régime des sociétés mères à un contentieux afférent au seul avoir fiscal et concernant des participations très minoritaires, et qu'à supposer cette jurisprudence transposable, elle a manqué à son obligation de coopération loyale consacrée à l'article 4 du traité sur l'Union européenne en s'abstenant de surseoir à statuer, insuffisamment motivé son arrêt et méconnu le principe d'effectivité garanti par le droit de l'Union européenne en jugeant qu'elle ne pouvait être regardée comme ayant apporté les premiers éléments de vraisemblance quant au caractère quasiment impossible ou excessivement difficile de la preuve du paiement de l'impôt par les sociétés distributrices établies dans les autres Etats membres. La réponse à ces moyens dépend de la question de savoir si les décisions rendues le 10 décembre 2012 par le Conseil d'Etat, statuant au contentieux n° 317074, ministre du budget, des comptes publics et de la fonction publique c/ Sté Rhodia, et n° 317075, ministre du budget, des comptes publics et de la fonction publique c/ Sté Accor, donnent son plein effet à l'arrêt de la Cour de justice de l'Union européenne du 15 septembre 2011. La Cour de justice de l'Union européenne, saisie par la Commission dans le cadre de l'affaire C-416/17, est actuellement saisie de cette question, déterminante pour la solution du présent litige.<br/>
<br/>
              5. Il y a lieu, dès lors, de surseoir à statuer sur le pourvoi de la société CIC jusqu'à ce que la Cour se soit prononcée.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il est sursis à statuer sur le pourvoi de la société CIC jusqu'à ce que la Cour de justice de l'Union européenne ait statué sur le recours dont elle a été saisie dans l'affaire C-416/17.<br/>
Article 2 : La présente décision sera notifiée à la société Crédit industriel et commercial et au ministre de l'action et des comptes publics.<br/>
Copie en sera adressée au greffe de la Cour de justice de l'Union européenne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
