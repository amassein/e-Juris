<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030253284</ID>
<ANCIEN_ID>JG_L_2015_02_000000383113</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/25/32/CETATEXT000030253284.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 17/02/2015, 383113, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-02-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383113</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Florence Marguerite</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:383113.20150217</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              Mme D...A...a demandé au tribunal administratif de Versailles d'annuler la proclamation des résultats des opérations électorales qui se sont déroulées le 30 mars 2014 pour l'élection des conseillers municipaux et des conseillers communautaires de la commune d'Itteville (Essonne), de rectifier les résultats et de déclarer M. C...B...inéligible. <br/>
<br/>
              Par un jugement nos 1402397, 1402741 du 27 juin 2014, le tribunal administratif de Versailles, après avoir joint cette protestation et le déféré du préfet de l'Essonne tendant à l'annulation des opérations électorales, a annulé les opérations électorales qui se sont déroulées les 23 et 30 mars 2014 en vue de la désignation des conseillers municipaux et des conseillers communautaires de la commune d'Itteville et a rejeté le surplus des conclusions des parties.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par une requête, enregistrée le 25 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement du tribunal administratif de Versailles du 27 juin 2014 ; <br/>
<br/>
              2°) de rectifier les résultats, de proclamer élus conseillers municipaux les 22 premiers candidats de sa liste et les 7 premiers candidats de la liste de M.B..., de proclamer élus conseillers communautaires les 4 premiers candidats de sa liste et le premier candidat de la liste de M. B...et d'annuler l'élection du maire et des adjoints de la commune ; <br/>
<br/>
              3°) de mettre à la charge de M. B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier.<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Marguerite, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de M.B....<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
              1. L'article L. 260 du code électoral, applicable à l'élection des conseilleurs municipaux dans les communes de mille habitants et plus, dispose que : " Les conseillers municipaux sont élus au scrutin de liste à deux tours, avec dépôt de liste comportant autant de candidats que de sièges à pourvoir, sans adjonction ni suppression de noms et sans modification de l'ordre de présentation, sous réserve des dispositions prévues au deuxième alinéa de l'article L. 264 ". L'article L. 66 du même code, dans sa rédaction applicable au scrutin litigieux, prévoit que : " Les bulletins blancs, ceux ne contenant pas une désignation suffisante (...) n'entrent pas en compte dans le résultat du dépouillement " et l'article R. 66-2 de ce code, applicable aux communes de mille habitants et plus, précise que : " Sont nuls et n'entrent pas en compte dans le résultat du dépouillement : (...) 3° (...) les bulletins comportant un ou plusieurs noms autres que celui du ou des candidats ou de leurs remplaçants éventuels ; 4° Les bulletins comportant une modification de l'ordre de présentation des candidats (...) ". Il résulte de ces dispositions que doivent être regardés comme nuls les bulletins comportant des noms autres que ceux des candidats ou une modification de l'ordre de leur présentation, sauf si ces discordances ne résultent pas d'une manoeuvre et si les électeurs ont pu émettre, au moyen de ces bulletins, un vote contenant une désignation suffisante de la liste en faveur de laquelle ils ont entendu se prononcer.<br/>
<br/>
              2. Il résulte de l'instruction que le jour du second tour de scrutin qui s'est déroulé le 30 mars 2014 en vue de l'élection des conseillers municipaux et des conseillers communautaires de la commune d'Itteville, des bulletins de la liste conduite par Mme A...au premier tour ont été mis à disposition des électeurs dans le bureau de vote n° 4, en lieu et place des bulletins imprimés en vue du second tour, et n'ont été retirés que dans le courant de l'après-midi. Lors du dépouillement, 196 suffrages manifestés par l'utilisation de ces bulletins ont été déclarés nuls. En application des dispositions de l'article L. 262 du code électoral, vingt-deux candidats de la liste conduite par M.B..., qui avait recueilli 1 392 voix, ont été proclamés élus conseillers municipaux et quatre candidats conseillers communautaires, tandis que sept candidats de la liste conduite par MmeA..., qui avait recueilli 1 228 voix, ont été proclamés élus conseillers municipaux et un candidat conseiller communautaire.<br/>
<br/>
              3. Il résulte également de l'instruction qu'à la suite de la fusion de la liste " Agir pour Itteville ", conduite par Mme A...au premier tour, avec la liste " IDEES : Itteville Démocratie Environnement Et Solidarité " conduite par M.E..., Mme A...conduisait au second tour une liste " Agir pour Itteville, tous ensemble " comprenant, par rapport à la liste présente au premier tour, quatre candidats différents, figurant à partir de la 7ème place sur la liste des candidats au conseil municipal et de la 5ème place sur la liste des candidats au conseil communautaire, et comportant des modifications de l'ordre des autres candidats.<br/>
<br/>
              4. Par suite, les bulletins du premier tour utilisés le 30 mars 2014 par des électeurs du bureau de vote n° 4 ne prenaient pas en compte l'existence d'une fusion avec une autre liste et la présence de nouveaux candidats issus de cette liste initialement distincte. Ainsi, en dépit de la qualité de tête de liste de MmeA..., de la proximité dans la dénomination des listes qu'elle conduisait au premier et au second tours et du maintien au second tour d'un nombre important de ses colistiers du premier tour, les électeurs ayant utilisé ces bulletins ne peuvent être regardés comme ayant émis un vote contenant une désignation suffisante de la liste en faveur de laquelle ils entendaient se prononcer, conformément aux prescriptions de l'article L. 66 du code électoral, et comme ayant clairement manifesté leur intention de voter pour les candidats de cette liste. Dès lors, c'est à juste titre que ces bulletins ont été déclarés nuls au moment du dépouillement.<br/>
<br/>
              5. Il résulte de ce qui précède que Mme A...n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Versailles, estimant que la sincérité du scrutin avait été affectée sans que les résultats puissent être rectifiés, a annulé les opérations électorales qui s'étaient déroulées les 23 et 30 mars 2014 dans la commune d'Itteville.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M.B..., qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme A...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à Mme D...A..., à M. C...B...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
