<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031640799</ID>
<ANCIEN_ID>JG_L_2015_12_000000392979</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/64/07/CETATEXT000031640799.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème SSJS, 16/12/2015, 392979, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392979</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>Mme Anne Iljic</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:392979.20151216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Paris : <br/>
              - d'annuler pour excès de pouvoir les décisions par lesquelles le ministre de l'intérieur et le ministre de la défense ont refusé de lui communiquer les informations le concernant figurant dans les traitements automatisés de données de la direction centrale du renseignement intérieur, de la direction générale de la sécurité extérieure, de la direction de la protection de la sécurité et de la défense et de la direction du renseignement militaire ; <br/>
              - d'enjoindre au ministre de l'intérieur et au ministre de la défense de lui communiquer les informations demandées dans un délai de quinze jours, sous astreinte de 100 euros par jour de retard.<br/>
<br/>
              Par un jugement avant dire droit n° 1311650 du 21 novembre 2014, le tribunal administratif de Paris a enjoint au ministre de l'intérieur et au ministre de la défense, dans un délai de deux mois à compter de la notification de sa décision, de lui communiquer tous éléments utiles à la solution du litige et relatifs aux informations concernant l'intéressé contenues dans le traitement automatisé de la direction centrale du renseignement intérieur, de la direction générale de la sécurité extérieure, de la direction de la protection de la sécurité et de la défense et de la direction du renseignement militaire ou, le cas échéant, tous éléments d'information appropriés sur la nature des pièces écartées et les raisons de leur exclusion. <br/>
              Par un arrêt n° 15PA00265 ; 15PA00972 du 25 juin 2015, la cour administrative d'appel de Paris a rejeté les appels formés par le ministre de l'intérieur et le ministre de la défense contre ce jugement.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              1° Sous le numéro  392979, par un pourvoi, enregistré le 26 août 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat d'annuler cet arrêt n° 15PA00265 ; 15PA00972 du 25 juin 2015 de la cour administrative d'appel de Paris. <br/>
<br/>
<br/>
              2° Sous le numéro 393029, par un pourvoi, enregistré le 28 août 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre de la défense demande au Conseil d'Etat : <br/>
              1°) d'annuler cet arrêt n° 15PA00265 ; 15PA00972 du 25 juin 2015 de la cour administrative d'appel de Paris ; <br/>
              2°) réglant l'affaire en application de l'article L. 821-2 du code de justice administrative de faire droit à son appel. <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - la loi n° 78-17 du 6 janvier 1978 ; <br/>
              - le décret n° 2005-1309 du 20 octobre 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Iljic, auditeur,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. A...B...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les pourvois du ministre de l'intérieur et du ministre de la défense sont dirigés contre une même décision. Il y a donc lieu de les joindre pour statuer par une seule décision.  <br/>
<br/>
              2. Aux termes de l'article 41 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, dans sa rédaction issue de la loi n° 2004-801 du 6 août 2004 : " Par dérogation aux articles 39 et 40, lorsqu'un traitement intéresse la sûreté de l'Etat, la défense ou la sécurité publique, le droit d'accès s'exerce dans les conditions prévues par le présent article pour l'ensemble des informations qu'il contient. / La demande est adressée à la commission qui désigne l'un de ses membres appartenant ou ayant appartenu au Conseil d'Etat, à la Cour de cassation ou à la Cour des comptes pour mener les investigations utiles et faire procéder aux modifications nécessaires. Celui-ci peut se faire assister d'un agent de la commission. Il est notifié au requérant qu'il a été procédé aux vérifications. /Lorsque la commission constate, en accord avec le responsable du traitement, que la communication des données qui y sont contenues ne met pas en cause  ses finalités, la sûreté de l'Etat, la défense ou la sécurité publique, ces données peuvent être communiquées au requérant. / Lorsque le traitement est susceptible de comprendre des informations dont la communication ne mettrait pas en cause les fins qui lui sont assignées, l'acte réglementaire portant création du fichier peut prévoir que ces informations peuvent être communiquées au requérant par le gestionnaire du fichier directement saisi. ". Aux termes de l'article 88 du décret du 20 octobre 2005 pris pour l'application de cette loi : " Aux termes de ses investigations, la commission constate, en accord avec le responsable du traitement, celles des informations susceptibles d'être communiquées au demandeur dès lors que leur communication ne met pas en cause les finalités du traitement, la sûreté de l'Etat, la défense ou la sécurité publique. Elle transmet au demandeur ces informations (...) Lorsque le responsable du traitement s'oppose à la communication au demandeur de tout ou partie des informations le concernant, la commission l'informe qu'il a été procédé aux vérifications nécessaires. / La commission peut constater en accord avec le responsable du traitement, que les informations concernant le demandeur doivent être rectifiées ou supprimées et qu'il y a lieu de l'en informer. En cas d'opposition du responsable du traitement, la commission se borne à informer le demandeur qu'il a été procédé aux vérifications nécessaires. Lorsque le traitement ne contient aucune information concernant le demandeur, la commission informe celui-ci, avec l'accord du responsable du traitement. / En cas d'opposition du responsable du traitement, la commission se borne à informer le demandeur qu'il a été procédé aux vérifications nécessaires. / La réponse de la commission mentionne les voies et délais de recours ouverts au demandeur ".<br/>
<br/>
              3. Par jugement avant dire droit du 21 novembre 2014, le tribunal administratif de Paris, saisi des conclusions de M. B...tendant à l'annulation pour excès de pouvoir des décisions du ministre de l'intérieur et du ministre de la défense lui refusant la communication des informations le concernant figurant au sein des traitements automatisés de données à caractère personnel de la direction centrale du renseignement intérieur, de la direction générale de la sécurité extérieure, de la direction de la protection de la sécurité et de la défense et de la direction du renseignement militaire, dont l'existence avait été confirmée à l'intéressée dans le courrier par lequel la Commission nationale de l'informatique et des libertés l'avait informé qu'elle avait procédé aux vérifications demandées en application de l'article 41 de la loi du 6 janvier 1978, a ordonné au ministre de l'intérieur et au ministre de la défense de lui communiquer, pour versement au dossier de l'instruction écrite contradictoire, tous éléments utiles à la solution du litige et relatifs aux informations concernant l'inscription de M. B...dans ces fichiers. Par ce jugement avant dire droit, il a indiqué que, dans l'hypothèse où les ministres estimeraient que la communication de ces informations mettrait en cause les fins assignées à ces fichiers, et où ils estimeraient en conséquence devoir refuser leur communication, il leur appartiendrait néanmoins de verser au dossier de l'instruction écrite contradictoire tous éléments d'information appropriés sur la nature des pièces écartées et les raisons de leur exclusion, de façon à permettre au tribunal de se prononcer en connaissance de cause sans porter, directement ou indirectement, atteinte aux secrets imposés par des considérations tenant à la sûreté de l'Etat, à la défense et à la sécurité publique. Enfin, dans le cas où un refus, exprès ou implicite, serait opposé à une demande d'information formulée par lui, le tribunal administratif a jugé qu'il lui appartiendrait, conformément aux règles générales d'établissement des faits devant le juge administratif, de joindre, en vue du jugement à rendre, cet élément de décision à l'ensemble des données fournies par le dossier.<br/>
<br/>
              4. Pour rejeter les appels du ministre de l'intérieur et du ministre de la défense contre ce jugement avant dire droit du 21 novembre 2014, la cour administrative d'appel de Paris a notamment jugé que la circonstance que le gestionnaire d'un fichier dit " de souveraineté " serait autorisé par la loi et le décret, eu égard aux finalités de renseignement de ce fichier ou pour des motifs tenant à la sûreté de l'Etat, à la défense ou à la sécurité publique, à ne communiquer aucune information tenant à son contenu ou à l'existence même en son sein de données concernant un individu, ne faisait pas obstacle à la communication au juge, pour versement au dossier de l'instruction écrite contradictoire, des informations utiles à la solution du litige dans le cas où cette communication constituerait la seule voie lui permettant d'assurer l'effectivité du contrôle juridictionnel.<br/>
<br/>
              5. L'article 26 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés dispose que : "  I. Sont autorisés par arrêté du ou des ministres compétents, pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés, les traitements de données à caractère personnel mis en oeuvre pour le compte de l'Etat et : / 1° Qui intéressent la sûreté de l'Etat, la défense ou la sécurité publique ; (...) / L'avis de la commission est publié avec l'arrêté autorisant le traitement. / II. Ceux de ces traitements qui portent sur des données mentionnées au I de l'article 8 sont autorisés par décret en Conseil d'Etat pris après avis motivé et publié de la commission ; cet avis est publié avec le décret autorisant le traitement. / III. Certains traitements mentionnés au I et au II peuvent être dispensés, par décret en Conseil d'Etat, de la publication de l'acte réglementaire qui les autorise ; pour ces traitements, est publié, en même temps que le décret autorisant la dispense de publication de l'acte, le sens de l'avis émis par la commission (...) ".<br/>
<br/>
              6. Si le caractère contradictoire de la procédure fait en principe obstacle à ce qu'une décision juridictionnelle puisse être rendue sur la base de pièces dont une des parties n'aurait pu prendre connaissance, il en va nécessairement autrement, afin d'assurer l'effectivité du droit au recours, lorsque l'acte litigieux n'est pas publié en application de l'article 26 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés. Si une telle dispense de publication, que justifie la préservation des finalités des fichiers intéressant la sûreté de l'Etat, la défense ou la sécurité publique fait obstacle à la communication tant de l'acte réglementaire qui en a autorisé la création que des décisions prises pour leur mise en oeuvre aux parties autres que celle qui les détient, dès lors qu'une telle communication priverait d'effet la dispense de publication, elle ne peut, en revanche, empêcher leur communication au juge lorsque celle-ci est la seule voie lui permettant d'apprécier le bien-fondé d'un moyen. Il suit de là que, quand, dans le cadre de l'instruction d'un recours dirigé contre le refus de communiquer des informations relatives à une personne mentionnée dans un fichier intéressant la sûreté de l'Etat, la défense ou la sécurité publique dont l'acte de création a fait l'objet d'une dispense de publication, le ministre refuse la communication de ces informations au motif que celle-ci porterait atteinte aux finalités de ce fichier, il lui appartient néanmoins de verser au dossier de l'instruction écrite, à la demande du juge, ces informations ou tous éléments appropriés sur leur nature et les motifs fondant le refus de les communiquer de façon à lui permettre de se prononcer en connaissance de cause sur la légalité de ce dernier sans que ces éléments puissent être communiqués aux autres parties, auxquelles ils révèleraient les finalités du fichier qui ont fondé la non publication du décret l'autorisant. <br/>
<br/>
              7. Il résulte de ce qui précède qu'en jugeant que le ministre de l'intérieur et le ministre de la défense étaient tenus de communiquer au juge les informations demandées par M. B...sans prévoir que, dans l'hypothèse où l'exposé de ces éléments compromettrait les finalités des fichiers non publiés auxquelles elles se rapportent en fournissant aux autres parties des indications, en violation des exigences ayant justifié la dispense de publication des actes réglementaires ayant créé ces fichiers, ces éléments ne seraient pas versés à l'instruction contradictoire, la cour administrative d'appel a commis une erreur de droit. Il suit de là que son arrêt doit, pour ce motif, être annulé sans qu'il soit besoin d'examiner l'autre moyen du pourvoi numéro 393029.<br/>
<br/>
              8. Il y a lieu, par application des dispositions de l'article L. 821-2 du code de justice administrative, de statuer sur les appels du ministre de l'intérieur et du ministre de la défense. <br/>
<br/>
              9. Il résulte des motifs énoncés au point 6 de la présente décision que le tribunal administratif de Paris ne pouvait, sans erreur de droit, ordonner les mesures contestées par son jugement du 21 novembre 2014. Il suit de là que le ministre de l'intérieur et le ministre de la défense sont fondés à en demander l'annulation. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 25 juin 2015 de la cour administrative d'appel de Paris et le jugement du 21 novembre 2014 du tribunal administratif de Paris sont annulés. <br/>
Article 2 : La présente décision sera notifiée au ministre de l'intérieur, au ministre de la défense, à la Commission nationale de l'informatique et des libertés et à M. A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
