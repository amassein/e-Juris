<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028411882</ID>
<ANCIEN_ID>JG_L_2013_12_000000359940</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/41/18/CETATEXT000028411882.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 30/12/2013, 359940, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359940</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROGER, SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Samuel Gillis</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:359940.20131230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 4 juin 2012 au secrétariat du contentieux du Conseil d'Etat, présenté pour l'association des familles victimes du saturnisme, représentée par son président en exercice, dont le siège est 3, rue du Niger à Paris (75012) ; l'association demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10PA03181 du 9 février 2012 par lequel la cour administrative d'appel de Paris, statuant sur le recours du ministre d'Etat, ministre de l'écologie,  de l'énergie, du développement durable et de la mer, en charge des technologies vertes et des négociations sur le climat, d'une part, a annulé le jugement n° 71189005/7-1 du 29 avril 2010 par lequel le tribunal administratif de Paris a annulé l'arrêté du 4 octobre 2007 du préfet de la région d'Ile-de-France refusant l'agrément de l'association requérante au titre de l'article L. 141-1 du code de l'environnement et a enjoint au préfet de procéder au réexamen de la demande d'agrément de l'association, d'autre part, a rejeté la demande de l'association présentée devant le tribunal administratif et ses conclusions d'appel incident ; <br/>
<br/>
              2°) de faire droit à ses conclusions d'appel ; <br/>
<br/>
              3°) de lui attribuer l'agrément sollicité ;<br/>
<br/>
              4°) d'ordonner la publication de l'arrêt à intervenir au recueil des actes administratifs de la préfecture de la région d'Ile-de-France, sous astreinte de 100 euros par jour de retard à compter de la notification dudit arrêt ;<br/>
<br/>
              5°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre des dispositions des articles L.761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
<br/>
     	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Samuel Gillis, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Roger, Sevaux, Mathonnet, avocat de l'association des familles victimes du saturnisme ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que, par un arrêté du 4 octobre 2012, le préfet de la région d'Ile-de-France a rejeté la demande d'agrément formée par l'association des familles victimes du saturnisme au titre de l'article L. 141-1 du code de l'environnement ; que, par un jugement du 29 avril 2010, le tribunal administratif de Paris a annulé cet arrêté et enjoint au préfet de procéder au réexamen de la demande d'agrément ; que, l'association se pourvoit en cassation contre un arrêt du 9 février 2012 par lequel la cour administrative d'appel de Paris a, sur le recours du ministre de l'écologie, de l'énergie, du développement durable et de la mer, annulé le jugement du tribunal administratif et rejeté la demande d'annulation de l'arrêté litigieux ;<br/>
<br/>
              2. Considérant, d'une part, qu'aux termes du premier alinéa de l'article L. 141-1 du code de l'environnement : " Lorsqu'elles exercent leurs activités depuis au moins trois ans, les associations régulièrement déclarées et exerçant leurs activités statutaires dans le domaine de la protection de la nature et de la gestion de la faune sauvage, de l'amélioration du cadre de vie, de la protection de l'eau, de l'air, des sols, des sites et paysages, de l'urbanisme, ou ayant pour objet la lutte contre les pollutions et les nuisances et, d'une manière générale, oeuvrant principalement pour la protection de l'environnement, peuvent faire l'objet d'un agrément motivé de l'autorité administrative. " ; que ces associations sont dites, selon son quatrième alinéa, " associations agréées de protection de l'environnement " ; que le dernier alinéa du même article précise que les décisions prises en application de ces dispositions sont soumises à un contentieux de pleine juridiction ; qu'en vertu de l'article R. 141-2 du même code, une association peut être agréée si, à la date de la demande d'agrément, elle justifie depuis trois ans au moins à compter de sa déclaration d'un objet statutaire relevant d'un ou plusieurs domaines mentionnés à l'article L. 141-1 et de l'exercice dans ces domaines d'activités effectives et publiques ou de publications et travaux dont la nature et l'importance attestent qu'elle oeuvre à titre principal pour la protection de l'environnement ; qu'il résulte de ces dispositions que les associations sollicitant l'octroi d'un agrément sur leur fondement doivent justifier non seulement exercer effectivement leur activité dans l'un ou plusieurs des domaines mentionnés ci-dessus, mais aussi oeuvrer principalement pour la protection de l'environnement ; <br/>
<br/>
              3. Considérant qu'en relevant, après avoir analysé les statuts de l'association requérante et son activité telle qu'elle ressortait des pièces du dossier, que, si l'association contribuait indirectement à la protection de l'environnement par ses actions de lutte contre le saturnisme, elle intervenait à titre principal dans les domaines de la santé publique et de l'action sociale, la cour a porté une appréciation souveraine sur les pièces du dossier, qui est exempte de dénaturation ; qu'en déduisant de ces constatations qu'elle ne justifiait pas oeuvrer principalement pour la protection de l'environnement et que, par suite, le préfet de la région d'Ile-de-France n'avait pas fait une inexacte application des dispositions précitées en refusant, pour ce motif, de délivrer à ladite association l'agrément sollicité, la cour n'a pas entaché son arrêt, qui est suffisamment motivé sur ce point, d'une erreur de qualification juridique ou d'une erreur de droit ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que l'association requérante n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de l'association des familles victimes du saturnisme est rejeté.<br/>
Article 2 : La présente décision sera notifiée à l'association des familles victimes du saturnisme et au ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
