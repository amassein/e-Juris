<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039217425</ID>
<ANCIEN_ID>JG_L_2019_10_000000418106</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/21/74/CETATEXT000039217425.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 14/10/2019, 418106, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418106</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR>Mme Tiphaine Pinault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:418106.20191014</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association départementale pour le développement des actions de prévention 13 a demandé au tribunal administratif de Marseille d'annuler pour excès de pouvoir, d'une part, la décision du 24 octobre 2014 par laquelle l'inspectrice du travail a rejeté sa demande d'autorisation de licencier Mme A... B... et, d'autre part, la décision du 27 avril 2015 par laquelle le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a rejeté son recours hiérarchique. Par un jugement n° 1503889 du 11 juillet 2017, le tribunal administratif a annulé ces décisions.      <br/>
<br/>
              Par une ordonnance n° 17MA03927 du 9 octobre 2017, le président de la 7ème chambre de la cour administrative d'appel de Marseille a rejeté la requête de Mme B....  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire enregistrés les 12 février et 14 mai 2018 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Bouzidi-Bouhanna, son avocat, au titre des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Tiphaine Pinault, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bouzidi- Bouhanna, avocat de Mme B....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Mme B... se pourvoit en cassation contre l'ordonnance du 9 octobre 2017 par laquelle le président de la 7ème chambre de la cour administrative d'appel de Marseille a rejeté sa requête comme entachée d'une irrecevabilité manifeste au motif, relevé d'office, qu'elle était présentée tardivement pour avoir été enregistrée au greffe de la cour le 18 septembre 2017 alors que la décision attaquée lui avait été notifiée le 12 juillet 2017, suivant mention figurant sur l'accusé de réception du courrier de notification, soit plus de deux mois auparavant.<br/>
<br/>
              2. Aux termes de l'article R. 222-1 du code de justice administrative, " (...) les présidents de formation de jugement (...) des cours (...) peuvent, par ordonnance : / (...) 4° rejeter les requêtes manifestement irrecevables, lorsque la juridiction n'est pas tenue d'inviter leur auteur à les régulariser ou qu'elles n'ont pas été régularisées à l'expiration du délai imparti par une demande en ce sens (...) ". Il résulte de l'article R. 811-2 du même code que " sauf disposition contraire, le délai d'appel est de deux mois. Il court contre toute partie à l'instance à compter du jour où la notification a été faite à cette partie dans les conditions prévues aux articles R. 751-3 et R. 751-4 ". En outre, l'article R. 751-3 du même code dispose que " sauf disposition contraire, les décisions sont notifiées le même jour à toutes les parties en cause et adressées à leur domicile réel, par lettre recommandée avec demande d'avis de réception, sans préjudice du droit des parties de faire signifier ces décisions par acte d'huissier de justice ".<br/>
<br/>
              3. Il résulte tant des pièces du dossier soumis aux juges du fond que de l'historique du pli édité par la société La Poste et produit en cassation que, si le courrier recommandé de notification du jugement du 11 juillet 2017, dont Mme B... a fait appel, a été présenté à son domicile le 12 juillet 2017, celle-ci est allée le retirer au bureau de poste le 17 juillet 2017, soit dans le délai de quinze jours de conservation du pli. Par suite, en jugeant que la date figurant sur l'accusé de réception était la date de distribution, et non la date de présentation du pli de notification du jugement, le président de la 7ème chambre de la cour administrative d'appel de Bordeaux a dénaturé les pièces du dossier. Par suite, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, son ordonnance doit être annulée.  <br/>
<br/>
              4. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées sur le fondement des dispositions de l'article 37 de la loi du 10 juillet 1991 par la SCP Bouzidi-Bouhanna, avocat de Mme B....   <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance n° 17MA03927 du président de la 7ème chambre de la cour administrative d'appel de Marseille du 9 octobre 2017 est annulée.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille. <br/>
<br/>
Article 3 : Les conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à Mme A... B..., à l'Association départementale pour le développement des actions de prévention 13.<br/>
Copie en sera adressée à la ministre du travail.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
