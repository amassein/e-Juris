<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037599971</ID>
<ANCIEN_ID>JG_L_2018_11_000000412562</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/59/99/CETATEXT000037599971.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 09/11/2018, 412562</TITRE>
<DATE_DEC>2018-11-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412562</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:412562.20181109</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              La société Support-RGS a demandé au tribunal administratif de Paris, d'une part, d'annuler les décisions des 27 novembre 2014, 7 novembre 2014, 18 novembre 2014, 1er décembre 2014, 5 décembre 2014, 15 décembre 2014 et 21 novembre 2014 par lesquelles la chambre de commerce et d'industrie (CCI) de région Paris - Ile de France, la CCI de Montpellier, la CCI de Rennes, la CCI de Bordeaux, la CCI Grand Lille, la CCI Nice Côte d'Azur, la CCI de Strasbourg et du Bas-Rhin et la CCI de Toulouse ont rejeté ses demandes tendant à ce qu'elles cessent toute action de promotion et de commercialisation des certificats de signature électronique ChamberSign ainsi que les décisions implicites par lesquelles l'assemblée des chambres françaises de commerce et d'industrie (ACFCI), la CCI Côte d'Or, la CCI de Lyon, la CCI Marseille Provence, la CCI de Nantes Saint-Nazaire et la CCI Pau Béarn ont rejeté ses demandes ayant le même objet, d'autre part, d'enjoindre à chacune de ces chambres de cesser la promotion et la commercialisation, via l'association ChamberSign, des certificats de signature électronique ChamberSign. Par un jugement n° 1500260 du 3 mai 2016, le tribunal administratif de Paris a annulé les décisions attaquées et a enjoint à chacune de ces chambres de cesser toute action de promotion et de commercialisation des certificats de signature électronique ChamberSign dans un délai de huit mois à compter de sa notification.<br/>
<br/>
              Par un arrêt n°s 16PA02138, 16PA03036 du 18 mai 2017, la cour administrative d'appel de Paris a rejeté l'appel formé par CCI France, la CCI de région Paris - Ile-de-France, la CCI de Bordeaux, la CCI Côte d'Or, la CCI Grand Lille, la CCI de Lyon, la CCI Marseille Provence, la CCI de Montpellier, la CCI Nice Côte d'Azur, la CCI de Nantes Saint-Nazaire, la CCI Pau Béarn, la CCI de Rennes, la CCI de Strasbourg et du Bas-Rhin et la CCI de Toulouse contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux mémoires en réplique, enregistrés les 18 juillet et 17 octobre 2017 et les 21 juin et 28 septembre 2018, CCI France, la CCI de région Paris - Ile-de-France, la CCI Côte d'Or, la CCI Hauts-de-France, la CCI de Lyon, la CCI Marseille Provence, la CCI de l'Hérault, la CCI Nice Côte d'Azur, la CCI de Nantes Saint Nazaire, la CCI Pau Béarn, la CCI d'Ille-et-Vilaine et la CCI d'Alsace demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de la société Support RGS la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive 2006/123/CE du 12 décembre 2006 ;<br/>
              - le règlement 910/2014 du 23 juillet 2014 ;<br/>
              - le code de commerce ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Sirinelli, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la CCI d'Ile-et-Vilaine, de CCI France, de la CCI de région Paris - Ile-de-France, de la CCI Côte-d'Or, de la CCI Alsace Eurométropole et du Bas-Rhin, de la CCI Hauts-de-France, de la CCI Lyon Métropole Saint-Etienne Roanne, de la CCI Marseille Provence, de la CCI de l'Hérault, de la CCI Nice Côte d'Azur, de la CCI Nantes Saint-Nazaire, de la CCI Pau Bearn et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la société Support-RGS. <br/>
<br/>
              Vu la note en délibéré, enregistrée le 12 octobre 2018 présentée par la société Support-RGS.<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'assemblée des chambres françaises de commerce et d'industrie (ACFCI), renommée CCI France, ainsi que la chambre de commerce et d'industrie (CCI) de Bordeaux, la CCI de Chalon sur Saône-Autun-Louhans, devenue la CCI de Saône-et-Loire, la CCI de Lyon, dont les services ont été repris par la CCI métropolitaine Lyon Métropole - Saint-Etienne Roanne, le groupement interconsulaire de Loire-Atlantique, dont les services ont été repris par la CCI de Nantes Saint-Nazaire, la CCI de Nice-Côte d'Azur, la CCI de Paris, la CCI de Strasbourg et du Bas-Rhin, la CCI de Toulouse, la CCI de Touraine, le groupement consulaire CCI 29 regroupant la CCI de Brest, la CCI de Quimper et la CCI de Morlaix, ont créé le 20 septembre 2000, une association dénommée ChamberSign France, régie par la loi de 1901 et chargée d'établir un réseau de certification de signatures électroniques par les chambres de commerce et d'industrie et par les chambres régionales de commerce et d'industrie pour les entreprises et les acteurs économiques ; qu'estimant être victime de concurrence déloyale, la société Support-RGS, qui commercialise également des certificats de signature électronique, a demandé à ces chambres consulaires de cesser toute action de promotion et de commercialisation des certificats de signature électronique ChamberSign ; que, par un jugement du 3 mai 2016, le tribunal administratif de Paris a fait droit à la demande d'annulation des décisions de refus des chambres présentée par la société Support-RGS et a enjoint à chacune de ces chambres de cesser la promotion et la commercialisation, par l'intermédiaire de l'association ChamberSign, de certificats de signature électronique ; que, par un arrêt du 18 mai 2017 contre lequel les chambres se pourvoient en cassation, la cour administrative d'appel de Paris a rejeté l'appel de ces dernières contre ce jugement ; <br/>
<br/>
              2. Considérant que le principe de spécialité qui régit les établissements publics leur interdit d'exercer des activités étrangères à leur mission, sauf si ces activités en constituent le complément normal et si elles sont directement utiles à l'établissement ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 710-1 du code de commerce : " Le réseau (des chambres de commerce et d'industrie) et, en son sein, chaque établissement ou chambre départementale contribuent au développement économique, à l'attractivité et à l'aménagement des territoires ainsi qu'au soutien des entreprises et de leurs associations en remplissant, dans des conditions fixées par décret, toute mission de service public et toute mission d'intérêt général nécessaires à l'accomplissement de ces missions. A cet effet, chaque établissement ou chambre départementale du réseau peut assurer, dans le respect, le cas échéant, des schémas sectoriels qui lui sont applicables : (...) / 2° Les missions d'appui, d'accompagnement, de mise en relation et de conseil auprès des créateurs et repreneurs d'entreprises et des entreprises, dans le respect des dispositions législatives et réglementaires en vigueur en matière de droit de la concurrence (...) / ; 6° Les missions de nature marchande qui lui ont été confiées par une personne publique ou qui s'avèrent nécessaires pour l'accomplissement de ses autres missions (...) " ; qu'aux termes de l'article L. 711-3 du même code : " Dans le cadre des orientations données par la chambre de commerce et d'industrie de région compétente, les chambres de commerce et d'industrie territoriales et départementales d'Ile-de-France exercent toute mission de service auprès des entreprises industrielles, commerciales et de services de leur circonscription. A ce titre : 1° Elles créent et gèrent des centres de formalités des entreprises et y assurent, pour ce qui les concerne, les missions prévues par l'article 2 de la loi n° 94-126 du 11 février 1994 relative à l'initiative et à l'entreprise individuelle ; 2° Elles peuvent assurer, en conformité, s'il y a lieu, avec le schéma sectoriel applicable, la maîtrise d'ouvrage de tout projet d'infrastructure ou d'équipement ou gérer tout service concourant à l'exercice de leurs missions (...) " ; qu'aux termes de l'article D. 711-10 du même code : " Les chambres de commerce et d'industrie territoriales, les chambres de commerce et d'industrie départementales d'Ile-de-France et les chambres de commerce et d'industrie de région ont notamment une mission de service aux créateurs et repreneurs d'entreprises et aux entreprises industrielles, commerciales et de services de leur circonscription./ Pour l'exercice de cette mission, elles créent et gèrent des centres de formalités des entreprises et apportent à celles-ci toutes informations et tous conseils utiles pour leur développement./ Elles peuvent également créer et assurer directement d'autres dispositifs de conseil et d'assistance aux entreprises, dans le respect du droit de la concurrence et sous réserve de la tenue d'une comptabilité analytique " ;<br/>
<br/>
              4. Considérant qu'il ressort des énonciations de l'arrêt attaqué, que, pour rejeter la requête dont elle était saisie, la cour administrative d'appel de Paris a estimé, au terme de son appréciation souveraine, que les chambres de commerce et d'industrie membres de l'association ChamberSign France devaient être regardées non seulement comme exerçant une activité de promotion des certificats de signature électronique proposés par cette association et de " bureau d'enregistrement " chargé de contrôler l'identité des demandeurs de certification électronique et de vérifier les documents transmis, mais comme procédant elles-mêmes à la commercialisation de ces certificats ; qu'elle a jugé que les chambres de commerce et d'industrie ne pouvaient, sans méconnaître le principe de spécialité qui régit tout établissement public, ni assurer la promotion de certificats de signature électronique ou exercer l'activité dite de bureau d'enregistrement ni commercialiser de tels certificats ;<br/>
<br/>
              5. Considérant, en premier lieu, qu'il résulte des dispositions citées au point 3 que les chambres de commerce et d'industrie sont investies de compétences étendues pour soutenir et accompagner les entreprises ; que, dans un contexte de dématérialisation des procédures, l'utilisation, par les entreprises, du mécanisme de la signature électronique est susceptible de constituer une des conditions du maintien et du développement de leur compétitivité ; que, par suite, en jugeant que la promotion de l'utilisation des certificats de signature électronique et l'exercice de l'activité dite de bureau d'enregistrement ne relevaient pas des missions des chambres de commerce et d'industrie et méconnaissaient le principe de spécialité qui leur était applicable, la cour administrative d'appel a entaché son arrêt d'erreur de qualification juridique ; <br/>
<br/>
              6. Considérant, en second lieu, qu'en jugeant que l'activité de commercialisation des certificats de signature électronique ne pouvait constituer un complément normal des missions des chambres de commerce, alors que cette mission s'inscrit dans le prolongement de leur mission d'appui et d'accompagnement des entreprises et est utile à l'exercice de celle-ci, y compris lorsque la commercialisation est effectuée au profit de collectivités territoriales ou de professions réglementées, la cour a entaché son arrêt d'une erreur de qualification juridique ;  <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède, et sans qu'il y ait lieu d'examiner les autres moyens du pourvoi, que l'arrêt attaqué doit être annulé ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Support-RGS la somme totale de 3 000 euros à verser à CCI France, à la CCI de région Paris - Ile-de-France, à la CCI Côte d'Or, à la CCI Hauts-de-France, à la CCI de Lyon, à la CCI Marseille Provence, à la CCI de l'Hérault, à la CCI Nice Côte d'Azur, à la CCI de Nantes Saint-Nazaire, à la CCI Pau Béarn, à la CCI d'Ille-et-Vilaine et à la CCI d'Alsace au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que les mêmes dispositions font, en revanche, obstacle à ce qu'une somme soit mise à la charge de CCI France et de ces CCI qui ne sont pas, dans la présente instance, les parties perdantes ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 18 mai 2017 de la cour administrative d'appel de Paris est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : La société support-RGS versera à la chambre de commerce et d'industrie France, à la chambre de commerce et d'industrie de région Paris - Ile-de-France, à la chambre de commerce et d'industrie Côte d'Or, à la chambre de commerce et d'industrie Hauts-de-France, à la chambre de commerce et d'industrie de Lyon, à la chambre de commerce et d'industrie Marseille Provence, à la chambre de commerce et d'industrie de l'Hérault, à la chambre de commerce et d'industrie Nice Côte d'Azur, à la chambre de commerce et d'industrie de Nantes Saint-Nazaire, à la chambre de commerce et d'industrie Pau Béarn, à la chambre de commerce et d'industrie d'Ille-et-Vilaine et à la chambre de commerce et d'industrie d'Alsace une somme totale de 3 000 euros au titre de l'article L. 761 1 du code de justice administrative. <br/>
Article 4 : Les conclusions présentées par la société support-RGS au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à CCI France, représentant unique pour l'ensemble des requérants, ainsi qu'à la société support-RGS.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">14-06-01-02 COMMERCE, INDUSTRIE, INTERVENTION ÉCONOMIQUE DE LA PUISSANCE PUBLIQUE. ORGANISATION PROFESSIONNELLE DES ACTIVITÉS ÉCONOMIQUES. CHAMBRES DE COMMERCE ET D'INDUSTRIE. ATTRIBUTIONS. - PROMOTION DE L'UTILISATION DE CERTIFICATS DE SIGNATURE ÉLECTRONIQUE ET ACTIVITÉ DITE DE BUREAU D'ENREGISTREMENT - INCLUSION - COMMERCIALISATION DE CES CERTIFICATS - INCLUSION, Y COMPRIS LORSQU'ELLE EST EFFECTUÉE AU PROFIT DE COLLECTIVITÉS TERRITORIALES OU DE PROFESSIONS RÉGLEMENTÉES.
</SCT>
<ANA ID="9A"> 14-06-01-02 Il résulte des articles L. 710-1, L. 711-3 et D. 711-10 du code de commerce que les chambres de commerce et d'industrie sont investies de compétences étendues pour soutenir et accompagner les entreprises. Dans un contexte de dématérialisation des procédures, l'utilisation, par les entreprises, du mécanisme de la signature électronique est susceptible de constituer une des conditions du maintien et du développement de leur compétitivité. Par suite, commet une erreur de qualification juridique une cour administrative d'appel qui juge que la promotion de l'utilisation des certificats de signature électronique et l'exercice de l'activité dite de bureau d'enregistrement ne relèvent pas des missions des chambres de commerce et d'industrie et méconnaissent le principe de spécialité qui leur est applicable.,,Commet également une erreur de qualification juridique une cour administrative d'appel qui juge que l'activité de commercialisation des certificats de signature électronique ne peut constituer un complément normal des missions des chambres de commerce, alors que cette mission s'inscrit dans le prolongement de leur mission d'appui et d'accompagnement des entreprises et est utile à l'exercice de celle-ci, y compris lorsque la commercialisation est effectuée au profit de collectivités territoriales ou de professions réglementées.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
