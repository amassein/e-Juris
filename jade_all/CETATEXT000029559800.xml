<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029559800</ID>
<ANCIEN_ID>JG_L_2014_10_000000370644</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/55/98/CETATEXT000029559800.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 08/10/2014, 370644, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-10-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370644</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROGER, SEVAUX, MATHONNET ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>Mme Laurence Marion</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:370644.20141008</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 29 juillet et 30 octobre 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Grenke location, dont le siège est 11 rue de Lisbonne à Schiltigheim (67300) ; la société Grenke location demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 12NC01396 du 27 mai 2013 par lequel la cour administrative d'appel de Nancy, à la demande du ministre de la culture et de la communication, a, en premier lieu, annulé l'article 1er du jugement n° 0904852 du 31 mai 2012 du tribunal administratif de Strasbourg condamnant l'Etat à lui payer la somme de 101 042,39 euros majorée des intérêts au taux légal à compter du 25 mars 2009, en deuxième lieu, rejeté sa demande présentée devant le tribunal administratif de Strasbourg ainsi que ses conclusions d'appel incident ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du ministre de la culture et de la communication et de faire droit à ses conclusions ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laurence Marion, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Roger, Sevaux, Mathonnet, avocat de la société Grenke location, et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat du ministre de la culture et de la communication ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que le " Musée des civilisations de l'Europe et de la méditerranée " (Mucem), service à compétence nationale du ministère de la culture et de la communication, et la société Grenke location ont conclu, le 10 avril 2008, un contrat par lequel la société Grenke location s'engageait à acheter auprès d'un fournisseur désigné cinq photocopieurs pour les donner ensuite en location au Mucem pour une durée de soixante-trois mois moyennant un loyer trimestriel de 5 563 euros ; que le Mucem ayant cessé de régler les loyers trimestriels dès le 27 mai 2008, la société Grenke location a résilié ce contrat, en application de la clause prévue à cet effet, par une lettre du 17 mars 2009 et a demandé le versement de l'indemnité de résiliation contractuellement prévue ainsi que la restitution des matériels ; que la société Grenke location se pourvoit en cassation contre l'arrêt du 27 mai 2013 par lequel la cour administrative d'appel de Nancy a annulé l'article 1er du jugement du 31 mai 2012 du tribunal administratif de Strasbourg ayant condamné l'Etat à lui payer la somme de 101 042,39 euros, majorée des intérêts aux taux légaux, et rejeté la demande d'indemnisation présentée par la société Grenke location ; <br/>
<br/>
              2. Considérant que le cocontractant lié à une personne publique par un contrat administratif est tenu d'en assurer l'exécution, sauf en cas de force majeure, et ne peut notamment pas se prévaloir des manquements ou défaillances de l'administration pour se soustraire à ses propres obligations contractuelles ou prendre l'initiative de résilier unilatéralement le contrat ; qu'il est toutefois loisible aux parties de prévoir dans un contrat qui n'a pas pour objet l'exécution même du service public les conditions auxquelles le cocontractant de la personne publique peut résilier le contrat en cas de méconnaissance par cette dernière de ses obligations contractuelles ; que, cependant, le cocontractant ne peut procéder à la résiliation sans avoir mis à même, au préalable, la personne publique de s'opposer à la rupture des relations contractuelles pour un motif d'intérêt général, tiré notamment des exigences du service public ; que lorsqu'un motif d'intérêt général lui est opposé, le cocontractant doit poursuivre l'exécution du contrat ; qu'un manquement de sa part à cette obligation est de nature à entraîner la résiliation du contrat à ses torts exclusifs ; qu'il est toutefois loisible au cocontractant de contester devant le juge le motif d'intérêt général qui lui est opposé afin d'obtenir la résiliation du contrat ; que, par suite, en écartant, en raison de leur illégalité, l'application des clauses de l'article 12 des conditions générales annexées au contrat conclu entre le Mucem et la société Grenke location au seul motif qu'elles permettaient au cocontractant de l'administration de résilier unilatéralement le contrat en cas de retard de paiement des loyers, sans rechercher si ces clauses répondaient aux conditions rappelées ci-dessus, la cour a commis une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ; <br/>
<br/>
              3. Considérant que si le ministre de la culture et de la communication demande que le juge de cassation substitue au motif erroné en droit retenu par la cour, celui tiré de ce que les clauses litigieuses revêtiraient un caractère abusif au sens des dispositions de l'article L. 132-1 du code de la consommation, une telle substitution impose toutefois, en tout état de cause, l'appréciation de circonstances de fait  ; qu'il n'y a pas lieu, par suite, de procéder à la substitution demandée ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros au titre des frais exposés par la société Grenke location et non compris dans les dépens, en application des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font, en revanche, obstacle à ce que la somme demandée par l'Etat à ce titre soit mise à la charge de la société requérante, qui n'est pas partie perdante dans la présente instance ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 27 mai 2013 de la cour administrative d'appel de Nancy est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy.<br/>
Article 3 : L'Etat versera à la société Grenke location la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par l'Etat au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société Grenke location et au ministre de la culture et de la communication. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-03 MARCHÉS ET CONTRATS ADMINISTRATIFS. EXÉCUTION TECHNIQUE DU CONTRAT. - POSSIBILITÉ DE SOULEVER UNE EXCEPTION D'INEXÉCUTION DU CONTRAT ADMINISTRATIF DANS LE SILENCE DU CONTRAT - ABSENCE - POSSIBILITÉ DE PRÉVOIR CONTRACTUELLEMENT UN MÉCANISME DE RÉSILIATION EN CAS DE MÉCONNAISSANCE PAR LA PERSONNE PUBLIQUE DE SES OBLIGATIONS CONTRACTUELLES - EXISTENCE - CONDITIONS - MODALITÉS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-04-02-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. FIN DES CONTRATS. RÉSILIATION. MOTIFS. - POSSIBILITÉ DE SOULEVER UNE EXCEPTION D'INEXÉCUTION DU CONTRAT ADMINISTRATIF DANS LE SILENCE DU CONTRAT - ABSENCE - POSSIBILITÉ DE PRÉVOIR CONTRACTUELLEMENT UN MÉCANISME DE RÉSILIATION EN CAS DE MÉCONNAISSANCE PAR LA PERSONNE PUBLIQUE DE SES OBLIGATIONS CONTRACTUELLES - EXISTENCE - CONDITIONS - MODALITÉS.
</SCT>
<ANA ID="9A"> 39-03 Le cocontractant lié à une personne publique par un contrat administratif est tenu d'en assurer l'exécution, sauf en cas de force majeure, et ne peut notamment pas se prévaloir des manquements ou défaillances de l'administration pour se soustraire à ses propres obligations contractuelles ou prendre l'initiative de résilier unilatéralement le contrat.... ,,Il est toutefois loisible aux parties de prévoir, dans un contrat qui n'a pas pour objet l'exécution même du service public, les conditions auxquelles le cocontractant de la personne publique peut résilier le contrat en cas  de méconnaissance par cette dernière de ses obligations contractuelles.... ,,Cependant, dans ce cas, le cocontractant ne peut procéder à la résiliation sans avoir mis à même, au préalable, la personne publique de s'opposer à la rupture des relations contractuelles pour un motif d'intérêt général, tiré notamment des exigences du service public. Lorsqu'un motif d'intérêt général lui est opposé, le cocontractant doit poursuivre l'exécution du contrat. Un manquement de sa part à cette obligation est de nature à entraîner la résiliation du contrat à ses torts exclusifs. Il est toutefois loisible au cocontractant de contester devant le juge le motif d'intérêt général qui lui est opposé afin d'obtenir la résiliation du contrat.</ANA>
<ANA ID="9B"> 39-04-02-01 Le cocontractant lié à une personne publique par un contrat administratif est tenu d'en assurer l'exécution, sauf en cas de force majeure, et ne peut notamment pas se prévaloir des manquements ou défaillances de l'administration pour se soustraire à ses propres obligations contractuelles ou prendre l'initiative de résilier unilatéralement le contrat.... ,,Il est toutefois loisible aux parties de prévoir, dans un contrat qui n'a pas pour objet l'exécution même du service public, les conditions auxquelles le cocontractant de la personne publique peut résilier le contrat en cas  de méconnaissance par cette dernière de ses obligations contractuelles.... ,,Cependant, dans ce cas, le cocontractant ne peut procéder à la résiliation sans avoir mis à même, au préalable, la personne publique de s'opposer à la rupture des relations contractuelles pour un motif d'intérêt général, tiré notamment des exigences du service public. Lorsqu'un motif d'intérêt général lui est opposé, le cocontractant doit poursuivre l'exécution du contrat. Un manquement de sa part à cette obligation est de nature à entraîner la résiliation du contrat à ses torts exclusifs. Il est toutefois loisible au cocontractant de contester devant le juge le motif d'intérêt général qui lui est opposé afin d'obtenir la résiliation du contrat.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
