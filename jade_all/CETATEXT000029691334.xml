<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029691334</ID>
<ANCIEN_ID>JG_L_2014_11_000000377597</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/69/13/CETATEXT000029691334.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 03/11/2014, 377597</TITRE>
<DATE_DEC>2014-11-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>377597</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Yannick Faure</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:377597.20141103</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête, enregistrée le 14 avril 2014 au secrétariat du contentieux du Conseil d'Etat, Mme A...B...demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret n° 2014-147 du 13 février 2014 portant délimitation des cantons dans le département de l'Ain.<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ; <br/>
              - la loi n° 2013-403 du 17 mai 2013 ;<br/>
              - le code électoral ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yannick Faure, auditeur,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 191-1 du code électoral, résultant de la loi du 17 mai 2013 relative à l'élection des conseillers départementaux, des conseillers municipaux et des conseillers communautaires et modifiant le calendrier électoral : " Le nombre de cantons dans lesquels sont élus les conseillers départementaux est égal, pour chaque département, à la moitié du nombre de cantons existant au 1er janvier 2013, arrondi à l'unité impaire supérieure si ce nombre n'est pas entier impair. / Le nombre de cantons dans chaque département comptant plus de 500 000 habitants ne peut être inférieur à dix-sept. Il ne peut être inférieur à treize dans chaque département comptant entre 150 000 et 500 000 habitants " ; qu'aux termes de l'article L. 3113-2 du code général des collectivités territoriales, dans sa rédaction issue de la même loi, applicable à la date du décret attaqué : " I. - Les modifications des limites territoriales des cantons, les créations et suppressions de cantons et le transfert du siège de leur chef-lieu sont décidés par décret en Conseil d'Etat après consultation du conseil général qui se prononce dans un délai de six semaines à compter de sa saisine. A l'expiration de ce délai, son avis est réputé rendu. (...) / III. - La modification des limites territoriales des cantons effectuée en application du I est conforme aux règles suivantes : / a) Le territoire de chaque canton est défini sur des bases essentiellement démographiques ; / b) Le territoire de chaque canton est continu ; / c) Est entièrement comprise dans le même canton toute commune de moins de 3 500 habitants ; / IV. - Il n'est apporté aux règles énoncées au III que des exceptions de portée limitée, spécialement justifiées, au cas par cas, par des considérations géographiques (...) ou par d'autres impératifs d'intérêt général " ;<br/>
<br/>
              2. Considérant que le décret attaqué a, sur le fondement de l'article L. 3113-2 du code général des collectivités territoriales, procédé à une nouvelle délimitation des cantons du département de l'Ain, compte tenu de l'exigence de réduction du nombre des cantons de ce département de quarante-trois à vingt-trois résultant de l'article L. 191-1 du code électoral ;<br/>
<br/>
              Sur la légalité externe du décret attaqué :<br/>
<br/>
              3. Considérant, en premier lieu, que les dispositions de l'article L. 3113-2 du code général des collectivités territoriales se bornent à prévoir la consultation du conseil général du département concerné à l'occasion de l'opération de création et suppression de cantons ; que si les élus du département de l'Ain ont été cependant consultés par le préfet, la circonstance alléguée que seules les remarques de certains d'entre eux auraient été prises en compte n'est pas, en tout état de cause, de nature à entacher d'illégalité le décret attaqué ; <br/>
<br/>
              4. Considérant, en deuxième lieu, que la requérante soutient que la consultation du conseil général de l'Ain, requise par l'article L. 3113-2 du code général des collectivités territoriales, serait irrégulière, dès lors que le président de cette assemblée aurait dissocié le débat, organisé le 7 octobre 2013 lors d'une séance non publique réunissant l'ensemble des conseillers généraux, et le vote, intervenu le 15 octobre suivant en séance publique ; que, toutefois, d'une part, il ressort des pièces du dossier que les conseillers généraux ont été mis à même, le 15 octobre 2013, de s'exprimer à nouveau sur la nouvelle délimitation des cantons avant qu'il soit procédé au vote ; que, d'autre part, aucune disposition législative ou réglementaire ni aucun principe ne faisait obstacle à ce que la séance plénière du conseil général soit précédée d'une réunion de travail, fût-elle composée de l'ensemble des conseillers généraux et accessible à la presse ; que, par suite, le moyen tiré de l'irrégularité de la consultation du conseil général doit être écarté ; <br/>
<br/>
              Sur la légalité interne du décret :<br/>
<br/>
              5. Considérant que l'article 71 du décret du 18 octobre 2013, dans sa rédaction issue de l'article 8 du décret du 6 février 2014, dont la légalité n'est pas contestée, dispose que : "  (...) Pour la première délimitation générale des cantons opérée en application de l'article L. 3113-2  du code général des collectivités territoriales, dans sa rédaction résultant de l'article 46 de la loi n° 2013-403 du 17 mai 2013 relative à l'élection des conseillers départementaux, (...), le chiffre de la population municipale auquel il convient de se référer est celui authentifié par le décret n° 2012-1479 du 27 décembre 2012 authentifiant les chiffres des populations (...) " ; qu'il  est constant que les nouveaux cantons du département de l'Ain ont été délimités sur la base des données authentifiées par le décret du 27 décembre 2012 ; que, par suite, le moyen tiré de ce que les données retenues pour le redécoupage des cantons de ce département ne correspondraient pas aux données démographiques les plus récentes ne peut qu'être écarté ;<br/>
<br/>
              6. Considérant en deuxième lieu, qu'il résulte des dispositions de l'article L. 3113-2 du code général des collectivités territoriales citées au point 1 que le territoire de chaque canton doit être établi sur des bases essentiellement démographiques, qu'il doit être continu et que toute commune de moins de 3 500 habitants doit être entièrement comprise dans un même canton, seules des exceptions de portée limitée et spécialement justifiées pouvant être apportées à ces règles ; que ni ces dispositions, ni aucun autre texte non plus qu'aucun principe n'imposent au Premier ministre de prévoir que les limites des cantons, qui sont des circonscriptions électorales, coïncident avec les limites des circonscriptions législatives, des cartes des établissements publics de coopération intercommunale, des " bassins de vie " ou des  " îlots regroupés pour l'information statistique " définis par l'Institut national de la statistique et des études économiques ; que, de même, si l'article L. 192 du code électoral, relatif aux modalités de renouvellement des conseils généraux, faisait référence aux arrondissements, dans sa rédaction antérieure à l'intervention de la loi précitée du 17 mai 2013, aucun texte en vigueur à la date du décret contesté ne mentionne les arrondissements, circonscriptions administratives de l'Etat, pour la détermination des limites cantonales ; <br/>
<br/>
              7. Considérant, en dernier lieu, que le détournement de pouvoir allégué n'est pas établi ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que Mme B...n'est pas fondée à demander l'annulation du décret qu'elle attaque ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme B...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à Mme A...B...et au ministre de l'intérieur.<br/>
Copie en sera adressée pour information au Premier ministre et au département de l'Ain.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-03-01-01 ÉLECTIONS ET RÉFÉRENDUM. ÉLECTIONS AU CONSEIL GÉNÉRAL. OPÉRATIONS PRÉLIMINAIRES À L'ÉLECTION. REMODELAGE DES CIRCONSCRIPTIONS CANTONALES. - CONSULTATION PRÉALABLE DU CONSEIL GÉNÉRAL (ART. L. 3113-2 DU CGCT) - PROCÉDURE IRRÉGULIÈRE DU FAIT QUE LA SÉANCE PLÉNIÈRE DU CONSEIL GÉNÉRAL A ÉTÉ PRÉCÉDÉE D'UNE RÉUNION DE TRAVAIL COMPOSÉE DE L'ENSEMBLE DES CONSEILLERS GÉNÉRAUX ET ACCESSIBLE À LA PRESSE - ABSENCE.
</SCT>
<ANA ID="9A"> 28-03-01-01 Aucune disposition législative ou réglementaire ni aucun principe ne fait obstacle à ce que la séance plénière au cours de laquelle le conseil général est appelé à rendre son avis, requis par l'article L. 3113-2 du code général des collectivités territoriales (CGCT), sur le projet de remodelage des circonscriptions cantonales soit précédée d'une réunion de travail, fût-elle composée de l'ensemble des conseillers généraux et accessible à la presse.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
