<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030956504</ID>
<ANCIEN_ID>JG_L_2015_07_000000360813</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/95/65/CETATEXT000030956504.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 27/07/2015, 360813, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360813</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:360813.20150727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un  mémoire complémentaire et un mémoire en réplique, enregistrés le 6 juillet 2012, le 8 octobre 2012 et le 14 octobre 2013 au secrétariat du contentieux du Conseil d'Etat, l'association " Parti Pirate " demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 27 avril 2012 relatif au traitement automatisé de données à caractère personnel prévu à l'article R. 176-3 du code électoral ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - la Constitution ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le pacte international relatif aux droits civils et politiques ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code électoral ;<br/>
              - la loi n° 78-17 du 6 janvier 1978 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes de l'article L. 330-13 du code électoral, relatif à l'élection des députés par les Français établis hors de France : " Les électeurs votent dans les bureaux ouverts en application de l'article précédent. / Ils peuvent également, (...), voter par correspondance, soit sous pli fermé, soit par voie électronique au moyen de matériels et de logiciels permettant de respecter le secret du vote et la sincérité du scrutin. Un décret en Conseil d'Etat fixe les modalités d'application du présent alinéa (...) " ; qu'aux termes de l'article R. 176-3 du même code, dans sa rédaction issue du décret n° 2011-843 du 15 juillet 2011 : "  I.-Pour l'élection de députés par les Français établis hors de France, les électeurs mentionnés à l'article R. 172 peuvent voter par correspondance électronique. A cette fin, il est créé un traitement automatisé de données à caractère personnel, placé sous la responsabilité du ministre de l'intérieur et du ministre des affaires étrangères. / Ce traitement automatisé garantit la séparation, dans des fichiers distincts, des données relatives aux électeurs, d'une part, et aux votes, d'autre part. / Les droits d'accès et de rectification (...) s'exercent auprès de l'ambassadeur ou du chef de poste consulaire chargé d'organiser les opérations de vote (...) / II.-Préalablement à sa mise en place, ou à toute modification substantielle de sa conception, le système de vote électronique fait l'objet d'une expertise indépendante destinée à vérifier le respect des garanties prévues par la présente sous-section . / III.-Un arrêté conjoint du ministre de l'intérieur et du ministre des affaires étrangères précise les caractéristiques du traitement prévu au I (...) " ; que sur ce fondement a été pris l'arrêté du 27 avril 2012 dont l'association " Parti Pirate " demande l'annulation pour excès de pouvoir ;<br/>
<br/>
              3. Considérant, en premier lieu, qu'il ressort des pièces du dossier que la Commission nationale de l'informatique et des libertés (CNIL) a émis un avis sur le projet d'arrêté litigieux, par une délibération n° 2012-083 du 15 mars 2012 prise sur le fondement du 4° du II de l'article 27 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés ; que, par suite, le moyen tiré de ce que cet arrêté aurait été pris au terme d'une procédure irrégulière, faute pour la CNIL d'avoir été consultée, manque en fait ;<br/>
<br/>
              4. Considérant, en deuxième lieu, qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...). Le moyen est présenté, à peine d'irrecevabilité, dans un mémoire distinct et motivé (...) " ; que par une ordonnance du 12 novembre 2012, le président de la 10e sous-section de la section du contentieux du Conseil d'Etat a donné acte du désistement de l'association requérante de sa demande, qui avait été présentée par mémoire distinct, tendant à ce que le Conseil d'Etat renvoie au Conseil constitutionnel la question de la non-conformité à la Constitution du 2° de l'article L. 330-13 du code électoral, sur le fondement duquel ont été pris l'article R. 176-3 du même code et l'arrêté pris pour son application ; que ce moyen, qui demeure développé dans les écritures de l'association requérante tendant à l'annulation de l'arrêté attaqué, n'est par suite pas recevable ;<br/>
<br/>
              5. Considérant, en troisième lieu,  que le protocole additionnel du 20 mars 1952 à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales stipule, dans son article 3, que " Les Hautes Parties contractantes s'engagent à organiser, à des intervalles raisonnables, des élections libres au scrutin secret, dans les conditions qui assurent la libre expression de l'opinion du peuple sur le choix du corps législatif " ; que le b) de l'article 25 du pacte international relatif aux droits civils et politiques, conclu à New York le 16 décembre 1966, stipule que tout citoyen a le droit et la possibilité, sans aucune discrimination et sans restrictions déraisonnables de voter et d'être élu, " au cours d'élections périodiques, honnêtes, au suffrage universel et égal et au scrutin secret, assurant l'expression libre de la volonté des électeur " ; <br/>
<br/>
              6. Considérant que l'association requérante excipe, à l'appui de ses conclusions dirigées contre l'arrêté attaqué, de l'illégalité des dispositions précitées de l'article R. 176-3 du code électoral qui crée le traitement automatisé de données à caractère personnel nécessaire à la mise en oeuvre du vote par correspondance électronique pour l'élection de députés par les Français établis hors de France, dont elle soutient qu'il n'est conforme ni à l'article 25 du pacte international relatif aux droits civils et politiques, ni à l'article 3 du premier protocole additionnel de la convention européenne de sauvegarde des droits de l'homme, ni à l'article L. 330-13 du code électoral ; que toutefois ces dispositions, qui ont pour seul objet de rappeler le principe, fixé par la loi, du recours au vote électronique pour l'élection des députés par les Français établis hors de France et de fixer les obligations de sécurité et de confidentialité que doit présenter le traitement automatisé et qui renvoient la définition des caractéristiques à un arrêté conjoint du ministre de l'intérieur et du ministre des affaires étrangères, ne sauraient, par elles-mêmes, porter atteinte aux principes invoqués par l'association requérante ni, par voie de conséquence, méconnaître les dispositions de l'article L. 330-13 du code électoral ; qu'en tout état de cause, le recours au vote électronique ne peut être regardé comme constituant une modalité de vote qui, par nature, violerait les engagements internationaux dont se prévaut la requérante ; <br/>
<br/>
              7. Considérant qu'il résulte au demeurant des dispositions du II de l'article R. 176-3 du code électoral, dont l'objectif est de garantir la sincérité du scrutin par voie électronique, que le recours à un système de vote électronique est subordonné à la réalisation d'une expertise indépendante, lors de sa conception initiale, à chaque fois qu'il est procédé à une modification substantielle ainsi que préalablement à chaque scrutin ; qu'en outre, tant l'article R. 176-3-7 du code électoral que l'article 4 de l'arrêté attaqué prévoient que l'identifiant et l'authentifiant sont transmis à l'électeur par des modes d'acheminement différents, que l'authentifiant est renouvelé en cas de second tour de scrutin et qu'en cas de perte, seul l'identifiant peut être récupéré par l'électeur ; qu'enfin, ainsi que le rappelle l'article 2 de l'arrêté attaqué, le respect du secret du vote, de la sincérité du scrutin et de l'accessibilité au suffrage doivent être garantis au stade de la mise en oeuvre du traitement ; <br/>
<br/>
              7. Considérant, en dernier lieu, que l'association requérante ne saurait utilement invoquer, par la voie de l'exception, le moyen tiré de ce que l'article R. 177-7 du code électoral, qui autorise les bureaux de vote à se prononcer, faute de disposer en temps utile des procès-verbaux, sur la base des télégrammes, des télécopies ou courriers électroniques des présidents des bureaux de vote, des ambassadeurs et des chefs de poste consulaire transmettant les résultats du scrutin et contenant les contestations formulées avec l'indication de leurs motifs et de leurs auteurs, méconnaîtrait les dispositions de l'article L. 330-14 du même code, dès lors que l'arrêté attaqué n'est pris ni sur son fondement, ni pour son application ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que l'association " Parti Pirate " n'est pas fondée à demander l'annulation de l'arrêté qu'elle attaque ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'association " Parti Pirate " est rejetée.<br/>
Article 2 : La présente décision sera notifiée à l'association " Parti Pirate " et au ministre des affaires étrangères et du développement international.<br/>
Copie en sera adressée au Premier ministre et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
