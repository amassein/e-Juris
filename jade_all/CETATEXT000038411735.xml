<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038411735</ID>
<ANCIEN_ID>JG_L_2019_04_000000409691</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/41/17/CETATEXT000038411735.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 24/04/2019, 409691, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-04-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409691</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>Mme Ophélie Champeaux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:409691.20190424</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société civile immobilière (SCI) SF a demandé au tribunal administratif de Melun, à titre principal, de prononcer la décharge de la somme de 189 211 euros mise à sa charge au titre du versement pour dépassement du plafond légal de densité par un titre de recettes du 7 mai 2010 et, à titre subsidiaire, la réduction du montant de ce versement. Par un jugement n° 1107032 du 9 janvier 2014, le tribunal administratif de Melun a rejeté sa demande.<br/>
<br/>
              Par une décision n° 376439 du 5 octobre 2015, le Conseil d'Etat statuant au contentieux a annulé ce jugement et renvoyé l'affaire au tribunal administratif de Melun. <br/>
<br/>
              Par un jugement du 14 février 2017, le tribunal administratif de Melun, statuant sur renvoi, a prononcé la décharge de la somme en litige. <br/>
<br/>
              Par un pourvoi, enregistré le 10 avril 2017 au secrétariat du contentieux du Conseil d'Etat, la ministre du logement et de l'habitat durable demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de la société SF. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la construction et de l'habitat ;<br/>
              - le code de l'urbanisme ;<br/>
              - le livre des procédures fiscales ; <br/>
              - la loi n° 2000-1208 du 13 décembre 2000 ;<br/>
              - la loi n° 2009-323 du 25 mars 2009 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Ophélie Champeaux, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de la SCI SF ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 25 mai 2009, le maire de Vincennes a accordé un permis de construire à la SCI SF pour la réhabilitation de six logements et la construction de quatre logements supplémentaires. Un titre de recettes de 189 211 euros a été émis le 7 mai 2010 à l'encontre de la société, au titre du versement pour dépassement du plafond légal de densité.<br/>
<br/>
              2. Aux termes de l'article L. 255 A du livre des procédures fiscales en vigueur à la date d'émission du titre exécutoire contesté : " (...) les taxes mentionnées au 1° de l'article L. 332-6-1 du code de l'urbanisme sont assis, liquidés et recouvrés en vertu d'un titre de recette individuel ou collectif délivré par le directeur départemental de l'équipement (...) /. L'autorité précitée peut déléguer sa signature aux agents placés sous son autorité. / (...) ". Si le premier alinéa du 3° de l'article 46 de la loi du 13 décembre 2000 relative à la solidarité et au renouvellement urbains a abrogé le b du 1° de l'article L. 332-6-1 du code de l'urbanisme retirant ainsi, de la liste des contributions aux dépenses d'équipements publics dues par les bénéficiaires d'autorisations de construire, le versement pour dépassement du plafond de légal de densité prévu à l'article L. 112-2 du même code, le second alinéa de ce même 3° précise que cette abrogation " prend effet lors de la suppression du plafond légal de densité intervenue dans les conditions fixées au II de l'article 50 ". Le II de cet article 50 prévoit que l'article L. 112-2 du code de l'urbanisme demeure applicable, dans sa rédaction antérieure à l'entrée en vigueur de la loi, dans les communes où un plafond légal de densité était institué le 31 décembre 1999. Il précise que le plafond légal de densité peut être supprimé par délibération de l'organe délibérant compétent ou de plein droit en cas d'institution, sur le territoire concerné, de la nouvelle participation au financement des voies nouvelles et réseaux créée par la loi.<br/>
<br/>
              3. Pour retenir l'incompétence du signataire du titre de recettes du 7 mai 2010 et prononcer en conséquence la décharge de l'imposition litigieuse, le tribunal administratif de Melun s'est fondé sur ce que la délégation de signature accordée par arrêté du 25 juillet 2008 du directeur départemental de l'équipement du Val-de-Marne ne portait que sur les titres de recettes relatifs aux taxes mentionnées à l'article L. 255 A du livre des procédures fiscales et à l'article R. 520-6 du code de l'urbanisme au nombre desquelles ne figurait pas le versement pour dépassement du plafond légal de densité. En statuant ainsi sans prendre en compte les dispositions du II de l'article 50 de la loi du 13 décembre 2000 mentionnées au point 2 qui précisent les conditions de prise d'effet de la suppression du plafond légal de densité et, par suite, du versement pour dépassement de ce plafond, et sans rechercher si ce versement était demeuré applicable dans la commune de Vincennes, le tribunal a commis une erreur de droit. Ce moyen suffisant à justifier l'annulation du jugement attaqué, il n'est pas nécessaire de se prononcer sur l'autre moyen du pourvoi.  <br/>
<br/>
              4. L'affaire faisant l'objet d'un deuxième pourvoi en cassation, il y a lieu de la régler au fond en application des dispositions du deuxième alinéa de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              5. En premier lieu, à les supposer même établies, les irrégularités entachant le courrier du 27 février 2009 qui se borne à informer le maire de Vincennes du montant du versement pour dépassement du plafond légal de densité susceptible d'être réclamé à la SCI SF après la délivrance de son permis de construire, la décision du 18 juillet 2011 rejetant la réclamation d'assiette de la société requérante et enfin, l'avis d'imposition du 4 juin 2010 qui ne revêt qu'un caractère informatif, sont sans influence sur la régularité et le bien-fondé de l'imposition contestée.<br/>
<br/>
              6. En deuxième lieu, il résulte de l'instruction que la délibération du 23 mars 1987 par laquelle la commune de Vincennes a institué un plafond légal de densité n'a pas été abrogée après l'entrée en vigueur de la loi du 13 décembre 2000 précitée. Dans ces conditions, il résulte de ce qui a été dit aux points 2 et 3 que le b du 1° de l'article L. 332-6-1 du code de l'urbanisme qui mentionne le versement pour dépassement du plafond légal de densité au titre des contributions aux dépenses d'équipements publics dues par les bénéficiaires d'autorisations de construire demeurait applicable dans la commune de Vincennes à la date de signature du titre de recettes litigieux. Par suite, le chef du pôle fiscalité de la direction départementale de l'équipement du Val-de-Marne était régulièrement habilité, par arrêté du 25 juillet 2008, à signer des titres de recettes relatifs à cet impôt prélevé au bénéfice de la commune. Dès lors, le moyen tiré de l'incompétence du signataire du titre de recettes du 7 mai 2010 doit être écarté. <br/>
<br/>
              7. En dernier lieu, d'une part, aux termes des dispositions de l'article L. 127-1 du code de l'urbanisme, dans sa rédaction applicable au permis de construire délivré à la société requérante, compte tenu du V de l'article 40 de la loi du 25 mars 2009 de mobilisation pour le logement et la lutte contre l'exclusion : " Le dépassement de la norme résultant de l'application du coefficient d'occupation des sols est autorisé, dans la limite de 20 % de ladite norme (...) sous réserve : / - d'une part, que la partie de la construction en dépassement ait la destination de logements à usage locatif bénéficiant d'un concours financier de l'Etat au sens du 3° de l'article L. 351-2 du code de la construction et de l'habitation. / (...) La partie de la construction en dépassement n'est pas assujettie au versement résultant du dépassement du plafond légal de densité.  (...) ". Les logements à usage locatif bénéficiant d'un concours financier de l'Etat mentionnés au 3° de l'article L. 351-2 du code de la construction et de l'habitation sont " les logements à usage locatif construits, acquis ou améliorés à compter du 5 janvier 1977 au moyen de formes spécifiques d'aides de l'Etat ou de prêts dont les caractéristiques et les conditions d'octroi sont déterminées par décrets ". D'autre part, aux termes du premier alinéa du I de l'article L. 321-1 du code de la construction et de l'habitation dans sa rédaction applicable au litige : " L'Agence nationale de l'habitat a pour mission, dans le respect des objectifs définis à l'article L. 301-1, de promouvoir le développement et la qualité du parc existant de logements privés. A cet effet, elle encourage et facilite l'exécution de travaux de réparation, d'assainissement, d'amélioration et d'adaptation d'immeubles d'habitation, ainsi que l'exécution de travaux de transformation en logements de locaux non affectés à l'habitation, dès lors que ces logements sont utilisés à titre de résidence principale (...) ". Aux termes de l'article R. 321-1 du même code : " L'Agence nationale de l'habitat est un établissement public administratif de l'Etat doté de la personnalité morale et de l'autonomie financière (...) ". <br/>
<br/>
              8. Il résulte de l'instruction que, par décision du 20 novembre 2008, l'Agence nationale pour l'habitat a accordé à la SCI SF une subvention pour le projet faisant l'objet du permis de construire qui lui a été délivré le 25 mai 2009 par la commune de Vincennes. Toutefois, dès lors que l'Agence nationale pour l'habitat, qui a la nature d'un établissement public administratif, est ainsi dotée d'une personnalité morale distincte de l'Etat, les subventions qu'elle octroie ne sauraient être regardées comme une aide de l'Etat au sens du 3° de l'article L. 351-2 du code de la construction et de l'habitation. Par suite, les logements faisant l'objet du permis de construire en cause n'ayant pas bénéficié d'un concours financier de l'Etat au sens de la disposition précitée, c'est à bon droit que l'administration a refusé à la société SF le bénéfice de l'exonération partielle du versement pour dépassement du plafond légal de densité prévue par les dispositions de l'article L. 127-1 du code de l'urbanisme précité.<br/>
<br/>
              9. Il résulte de ce qui précède que la demande de la SCI SF doit être rejetée. <br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat qui n'est pas la partie perdante dans la présente instance. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Melun du 14 février 2017 est annulé.<br/>
<br/>
Article 2 : La demande de la SCI SF est rejetée. <br/>
Article 3 : Les conclusions présentées devant le Conseil d'Etat par la SCI SF au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales et à la SCI SF.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
