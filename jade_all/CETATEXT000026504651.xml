<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026504651</ID>
<ANCIEN_ID>JG_L_2012_10_000000358762</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/50/46/CETATEXT000026504651.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 17/10/2012, 358762</TITRE>
<DATE_DEC>2012-10-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>358762</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Thierry Carriol</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:358762.20121017</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°), sous le n° 358762, la requête enregistrée le 23 avril 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par Mme Marie-Hélène B, demeurant ... ; Mme B demande au Conseil d'Etat d'annuler l'article 2 du jugement n° 1200399 du 27 mars 2012 du tribunal administratif d'Amiens en tant qu'il rejette sa protestation tendant à ce que l'élection de Mme Elise A, le 5 février 2012, en qualité de conseiller municipal d'Ailly-sur-Noye soit annulée ;<br/>
<br/>
<br/>
              Vu 2°), sous le n° 359041, la requête enregistrée le 30 avril 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par M. Frédéric , demeurant ... ; M.  demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le même jugement en tant qu'il a annulé son élection en tant que conseiller municipal d'Ailly-sur-Noye ;<br/>
<br/>
              2°) de rejeter la protestation de Mme B ;<br/>
<br/>
              3°) de mettre à la charge de Mme B la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu le code électoral ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thierry Carriol, Maître des Requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'une élection municipale partielle s'est déroulée à Ailly-sur-Noye (Somme) le 5 février 2012 afin de procéder, en application des dispositions de l'article L. 258 du code électoral, au renouvellement de huit membres du conseil municipal ; qu'à l'issue du premier tour de scrutin, les huit sièges à pourvoir ont été attribués à la liste " Ailly autrement " conduite par M.  ; que le tribunal administratif d'Amiens, saisi d'une protestation électorale formée par Mme B, maire de la commune, tendant à l'annulation de l'élection municipale partielle et à ce que soient déclarés inéligibles deux candidats déclarés élus en la personne de M.  et de Mme A, a, par jugement du 27 mars 2012, annulé l'élection de M.  mais rejeté le surplus des conclusions de la protestation de Mme B ; que M.  et Mme B font appel de ce jugement devant le Conseil d'Etat ; qu'il y a lieu de joindre leurs requêtes pour statuer par une seule décision ;<br/>
<br/>
              Sur la fin de non-recevoir opposée par Mme B à la requête de M.  :<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 116 du code électoral : " Le recours contre la décision du tribunal administratif est ouvert soit au préfet, soit aux parties intéressées. Il doit, à peine d'irrecevabilité, être déposé au Conseil d'Etat, dans le délai d'un mois à partir de la notification de la décision qui leur est faite et qui comporte l'indication dudit délai. " ;<br/>
<br/>
              3. Considérant que la requête de M.  contre le jugement du tribunal administratif d'Amiens, dont il a reçu notification le 29 mars 2012, a été enregistrée au secrétariat du contentieux du Conseil d'Etat le 30 avril 2012, soit dans le délai d'un mois prévu à l'article R. 116 du code électoral, qui est un délai franc ; qu'elle a par ailleurs été déposée par le mandataire de M.  dûment habilité ; que, par suite, la fin de non-recevoir opposée par Mme B doit être écartée ;<br/>
<br/>
              Sur les griefs tirés de l'inéligibilité de M.  et de Mme A à la date de l'élection :<br/>
<br/>
              4. Considérant qu'aux termes de l'article L. 231 du code électoral : " (...) Ne peuvent être élus conseillers municipaux dans les communes situées dans le ressort où ils exercent ou ont exercé leurs fonctions depuis moins de six mois : (...) / 8° Les directeurs de cabinet du président du conseil général et du président du conseil régional, les directeurs généraux, les directeurs, les directeurs adjoints, chefs de service et chefs de bureau de conseil général et de conseil régional, (...) " ;<br/>
<br/>
              5. Considérant que s'il appartient au juge de l'élection, saisi d'un grief relatif à l'inéligibilité d'un candidat à une élection municipale, de rechercher, lorsque le poste que l'intéressé occupe au sein d'une collectivité territoriale n'est pas mentionné en tant que tel au 8°) de l'article L. 231, si la réalité des fonctions exercées ne confère pas à leur titulaire des responsabilités équivalentes à celles exercées par les personnes mentionnées par ces dispositions ; qu'à cet égard, la circonstance que les fonctions exercées soient purement internes à la collectivité ou, au contraire, en rapport avec les autres collectivités territoriales, est sans incidence sur l'appréciation de cette équivalence ;<br/>
<br/>
              En ce qui concerne M.  :<br/>
<br/>
              6. Considérant que M.  avait, à la date de l'élection, la qualité de chef de cabinet du président du conseil général de la Somme ; qu'eu égard aux activités qu'il était, en l'espèce, amené à exercer à ce titre, à sa place au sein de l'organigramme du conseil général et à son niveau de rémunération, ses fonctions doivent être regardées comme étant équivalentes à celles d'un chef de bureau visé au 8° de l'article L. 231 du code électoral, le rendant inéligible aux fonctions de conseiller municipal ; que, par suite, M.  n'est pas fondé à soutenir que c'est à tort que le tribunal administratif d'Amiens a annulé son élection en tant que conseiller municipal de la commune d'Ailly-sur-Noye ; que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, dès lors, qu'être rejetées ;<br/>
<br/>
              En ce qui concerne Mme Maillard-Choquet :<br/>
<br/>
              7. Considérant que Mme A exerçait, à la date de l'élection, les fonctions de responsable de la mission de la communication interne du conseil régional de Picardie ; qu'il résulte de l'instruction que, dans le cadre de ces fonctions, elle encadrait trois agents, disposait d'une délégation de signature, notamment à l'effet de signer des marchés et bons de commande et occupait dans l'organigramme du conseil régional une place identique à celle d'autres chefs de bureau ; qu'ainsi les fonctions qu'elle occupait étaient équivalentes à celles d'un chef de bureau visé au 8° de l'article L. 231 du code électoral, la rendant inéligible aux fonctions de conseillère municipale ; que, ainsi qu'il a été dit au point 5, la circonstance que les activités de Mme A soient purement internes à l'administration régionale et n'impliquent aucun lien avec les communes est sans incidence sur ce constat ; que, dès lors, Mme B est fondée à soutenir que c'est à tort que le tribunal administratif d'Amiens a rejeté les conclusions de sa protestation tendant à l'annulation de l'élection de Mme A ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'élection de Mme A en tant que conseillère municipale d'Ailly-sur-Noye est annulée.<br/>
<br/>
Article 2 : Le jugement du tribunal administratif d'Amiens du 27 mars 2012 est réformé en ce qu'il a de contraire à la présente décision.<br/>
<br/>
Article 3 : La requête de M.  est rejetée.<br/>
Article 4 : La présente décision sera notifiée à Mme Marie Hélène B, à M. Frédéric , à Mme Elise A et au ministre de l'intérieur.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-04-02-02-065 ÉLECTIONS ET RÉFÉRENDUM. ÉLECTIONS MUNICIPALES. ÉLIGIBILITÉ. INÉLIGIBILITÉS. AGENTS DU CONSEIL GÉNÉRAL ET DU CONSEIL RÉGIONAL. - CAS DANS LEQUEL LE POSTE OCCUPÉ PAR UN CANDIDAT N'EST PAS EN TANT QUE TEL MENTIONNÉ AU 8° DE L'ARTICLE L. 231 DU CODE ÉLECTORAL - 1) PRINCIPE - OFFICE DU JUGE - OBLIGATION DE RECHERCHER SI LA RÉALITÉ DES FONCTIONS EXERCÉES NE CONFÈRE PAS À LEUR TITULAIRE DES RESPONSABILITÉS ÉQUIVALENTES À CELLES EXERCÉES PAR LES PERSONNES MENTIONNÉES PAR CES DISPOSITIONS - EXISTENCE - CIRCONSTANCE QUE LES FONCTIONS EXERCÉES SOIENT PUREMENT INTERNES À LA COLLECTIVITÉ - INCIDENCE - ABSENCE - 2) APPLICATION EN L'ESPÈCE - CAS DE LA RESPONSABLE DE LA MISSION COMMUNICATION INTERNE D'UN CONSEIL RÉGIONAL [RJ1].
</SCT>
<ANA ID="9A"> 28-04-02-02-065 1) Il appartient au juge de l'élection, saisi d'un grief relatif à l'inéligibilité d'un candidat à une élection municipale, de rechercher, lorsque le poste que l'intéressé occupe au sein d'une collectivité territoriale n'est pas mentionné en tant que tel au 8°) de l'article L. 231 du code électoral, si la réalité des fonctions exercées ne confère pas à leur titulaire des responsabilités équivalentes à celles exercées par les personnes mentionnées par ces dispositions. A cet égard, la circonstance que les fonctions exercées soient purement internes à la collectivité ou, au contraire, en rapport avec les autres collectivités territoriales, est sans incidence sur l'appréciation de cette équivalence.,,2)  En l'espèce, la candidate exerçait, à la date de l'élection, les fonctions de responsable de la mission de la communication interne d'un conseil régional. Dans le cadre de ces fonctions, elle encadrait trois agents, disposait d'une délégation de signature, notamment à l'effet de signer des marchés et bons de commande et occupait dans l'organigramme du conseil régional une place identique à celle d'autres chefs de bureau. Les fonctions qu'elle occupait étaient ainsi équivalentes à celles d'un chef de bureau visé au 8° de l'article L. 231 du code électoral, la rendant inéligible aux fonctions de conseillère municipale.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., pour des fonctions de chargé de mission regardées comme équivalentes à celles d'un chef de bureau de conseil régional, CE, 18 mai 2010, Elections municipales d'Hénin-Beaumont (Pas-de-Calais), Briois, n° 335786, T. p. 785.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
