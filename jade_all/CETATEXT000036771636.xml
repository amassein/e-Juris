<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036771636</ID>
<ANCIEN_ID>JG_L_2018_04_000000402426</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/77/16/CETATEXT000036771636.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 04/04/2018, 402426, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-04-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402426</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Pierre Lombard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:402426.20180404</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement n° 1311406 du 21 novembre 2014, le tribunal administratif de Paris a rejeté la demande de la société Lofta tendant à la décharge des cotisations supplémentaires d'impôt sur les sociétés et de la cotisation minimale de taxe professionnelle auxquelles elle a été assujettie au titre des exercices clos en 2006, 2007 et 2008 ainsi que des pénalités correspondantes.<br/>
<br/>
              Par un arrêt n° 15PA00096 du 15 juin 2016, la cour administrative d'appel de Paris a, après avoir annulé le jugement du tribunal administratif, rejeté la demande de la société Lofta.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 16 août et 17 novembre 2016, et le 20 juin 2017 au secrétariat du contentieux du Conseil d'Etat, la société Lofta demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 2 de cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Lombard, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la société Lofta ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que la société de droit luxembourgeois Lofta, dont le siège est situé dans le Grand-Duché du Luxembourg, et qui a pour objet social l'acquisition, la vente, la détention et la location d'immeubles notamment en France, a fait l'objet d'une procédure de visite et de saisie, le 7 avril 2009, à la suite de laquelle elle a déposé, le 28 mai 2009, des déclarations de résultats pour un établissement stable en France en ce qui concerne les exercices clos en 2006, 2007 et 2008. A l'issue d'une vérification de comptabilité portant sur ces exercices, elle a été assujettie, au titre de la procédure de taxation d'office, à des cotisations supplémentaires d'impôt sur les sociétés pour les exercices clos en 2007 et 2008, et sur la base des résultats ainsi évalués, à un rappel de cotisation minimale de taxe professionnelle au titre de 2007. Par un jugement du 21 novembre 2014, le tribunal administratif de Paris a rejeté sa demande tendant à la décharge de ces impositions ainsi que des intérêts de retard et majorations dont elles étaient assorties. La société Lofta se pourvoit en cassation contre l'arrêt du 15 juin 2016 par lequel la cour administrative de Paris, après avoir annulé ce jugement, a rejeté cette demande.<br/>
<br/>
              2. Aux termes de l'article L. 66 du livre des procédures fiscales : " Sont taxés d'office : (...) 2° à l'impôt sur les sociétés, les personnes morales passibles de cet impôt qui n'ont pas déposé dans le délai légal leur déclaration, sous réserve de la procédure de régularisation prévue à l'article L. 68 ; / 3° aux taxes sur le chiffre d'affaires, les personnes qui n'ont pas déposé dans le délai légal les déclarations qu'elles sont tenues de souscrire en leur qualité de redevables des taxes ; / 4° aux droits d'enregistrement et aux taxes assimilées, les personnes qui n'ont pas déposé une déclaration ou qui n'ont pas présenté un acte à la formalité de l'enregistrement dans le délai légal, sous réserve de la procédure de régularisation prévue à l'article L. 67 ; / Le présent 4° s'applique aux personnes mentionnées à l'article 964 du code général des impôts qui n'ont pas indiqué la valeur nette taxable de leur patrimoine imposable dans la déclaration prévue à l'article 170 du même code ou sur les annexes mentionnées à l'article 982 dudit code ou qui n'y ont pas joint ces mêmes annexes. / 5° aux taxes assises sur les salaires ou les rémunérations les personnes assujetties à ces taxes qui n'ont pas déposé dans le délai légal les déclarations qu'elles sont tenues de souscrire, sous réserve de la procédure de régularisation prévue à l'article L. 68 ". Aux termes de l'article L. 68 du même livre, dans sa rédaction applicable en l'espèce : " La procédure de taxation d'office prévue aux 2° et 5° de l'article L. 66 n'est applicable que si le contribuable n'a pas régularisé sa situation dans les trente jours de la notification d'une mise en demeure. / Toutefois, il n'y a pas lieu de procéder à cette mise en demeure : (...) / 3° Si le contribuable ne s'est pas fait connaître d'un centre de formalités des entreprises ou du greffe du tribunal de commerce (...) ".<br/>
<br/>
              Sur l'arrêt en ce qui concerne l'impôt sur les sociétés et les pénalités correspondantes :<br/>
<br/>
              3. Il résulte de ces dispositions que, si elles autorisent l'administration à imposer selon la procédure de taxation d'office, au titre de l'impôt sur les sociétés, les résultats qu'une société a omis de déclarer sans adresser à celle-ci une mise en demeure de régulariser sa situation préalablement à la notification des rectifications dans les cas qu'elles énumèrent limitativement, elles ne la dispensent pas d'envoyer une telle mise en demeure au contribuable qui s'est fait connaître d'un centre de formalités des entreprises ou du greffe du tribunal de commerce ou d'un organisme consulaire antérieurement à la proposition de rectification dès lors que sa déclaration d'activité, quand bien même elle comporterait des erreurs ou des omissions, notamment en ce qui concerne la date à laquelle l'activité a débuté ou les conditions réelles dans lesquelles elle a été exercée, le cas échéant à partir d'un établissement stable, mentionne l'adresse à laquelle le contribuable peut être joint et l'activité industrielle, commerciale ou artisanale dont les bénéfices font l'objet du redressement.<br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que la société Lofta, dont il n'est plus contesté qu'elle a exercé en France une activité de marchand de biens par l'intermédiaire d'un établissement stable, n'a, dans les délais légaux, déposé aucune déclaration à l'impôt sur les sociétés au titre des exercices clos en 2006, 2007 et 2008. Toutefois, la société a déclaré le 10 juillet 2006 à la direction des résidents à l'étranger et des services généraux l'activité de marchand de biens qu'elle exerçait en France, en mentionnant l'adresse à laquelle elle pouvait être jointe. Ainsi, et alors même que la taxe sur la valeur ajoutée était alors déclarée à titre d'activité exercée depuis son siège social du Luxembourg, la société Lofta doit être regardée comme s'étant, antérieurement à la proposition de rectification du 30 novembre 2009, fait connaître à un centre de formalité des entreprises, au sens des dispositions de l'article L. 68 du livre des procédures fiscales. Dès lors, en jugeant que l'administration n'avait pas à adresser à la société une mise en demeure de régulariser sa situation préalablement à la mise en oeuvre de la procédure de taxation d'office, la cour a entaché son arrêt d'erreur de droit.<br/>
<br/>
              Sur l'arrêt en ce qui concerne la cotisation minimale de taxe professionnelle :<br/>
<br/>
              5. Si la cotisation minimale de taxe professionnelle peut faire l'objet d'une taxation d'office lorsqu'aucune déclaration n'a été souscrite, une telle procédure ne saurait, au regard des dispositions citées au point 2, être regardée comme engagée sur le fondement des 2° à 5° de l'article L. 66 du livre des procédures fiscales et, dès lors, relever de l'article L. 68 du livre des procédures fiscales qui n'est applicable qu'aux procédures engagées sur un tel fondement.<br/>
<br/>
              6. La société requérante soutenait devant la cour qu'il appartenait à l'administration de lui adresser une mise en demeure de régulariser sa situation, en application de l'article L. 68 du livre des procédures fiscales, avant de pouvoir mettre en oeuvre à son encontre la procédure de taxation d'office. Dès lors, toutefois, ainsi qu'il a été dit au point précédent, que les dispositions de cet article ne sont pas applicables à la cotisation minimale de taxe professionnelle, ce moyen était inopérant. Ce motif, qui n'emporte l'appréciation d'aucune circonstance de fait, doit être substitué au motif retenu par l'arrêt attaqué. Il en résulte que le moyen tiré de ce que la cour a entaché son arrêt d'erreur de droit en jugeant que l'administration fiscale pouvait recourir à la procédure de taxation d'office sans la mettre en demeure de régulariser sa situation est sans incidence sur le bien-fondé de l'arrêt attaqué.<br/>
<br/>
              Sur l'arrêt en ce qui concerne les pénalités liées à la cotisation minimale de taxe professionnelle :<br/>
<br/>
              7. Aux termes du 1 de l'article 1728 du code général des impôts, dans sa rédaction applicable au litige : " Le défaut de production dans les délais prescrits d'une déclaration ou d'un acte comportant l'indication d'éléments à retenir pour l'assiette ou la liquidation de l'impôt entraîne l'application, sur le montant des droits mis à la charge du contribuable ou résultant de la déclaration ou de l'acte déposé tardivement, d'une majoration de : / (...) b. 40 % lorsque la déclaration ou l'acte n'a pas été déposé dans les trente jours suivant la réception d'une mise en demeure, notifiée par pli recommandé, d'avoir à le produire dans ce délai ; / c. 80 % en cas de découverte d'une activité occulte ". Il résulte de ces dispositions, éclairées par les travaux parlementaires qui ont précédé l'adoption de la loi de laquelle elles sont issues, que dans le cas où un contribuable n'a ni déposé dans le délai légal les déclarations qu'il était tenu de souscrire, ni fait connaître son activité à un centre de formalités des entreprises ou au greffe du tribunal de commerce, l'administration doit être réputée apporter la preuve, qui lui incombe, de l'exercice occulte de l'activité professionnelle si le contribuable n'est pas lui-même en mesure d'établir qu'il a commis une erreur justifiant qu'il ne se soit acquitté d'aucune de ces obligations déclaratives.<br/>
<br/>
              8. Dans le mémoire en réplique qu'elle a présenté en première instance et mis au dossier du juge du fond, la société Lofta a soutenu que l'administration n'était pas fondée à demander une substitution de base légale consistant à regarder la majoration de 40% dont avait été assortie la cotisation minimale de taxe professionnelle due au titre de 2007, comme fondée non plus sur les dispositions du a. de l'article 1729 relatif aux manquements délibérés mais, dans la limite du montant correspondant, sur celles du c du 1 de l'article 1728 du code général des impôts, en se prévalant, au titre de l'article L. 80 A du livre des procédures fiscales, de l'instruction 13 N-3-00 du 24 mars 2000 en tant qu'elle prévoit que lorsque le contribuable a souscrit des déclarations de taxe sur la valeur ajoutée afférente à une activité professionnelle, celle-ci ne peut être regardée comme ayant un caractère occulte au sens des dispositions précitées. La cour a omis de répondre à ce moyen, qui n'était pas inopérant et dont elle était saisie, après annulation du jugement, par la voie de l'évocation. Le moyen tiré de ce que, dans cette mesure, l'arrêt est insuffisamment motivé est donc fondé et justifie, sans qu'il soit besoin d'examiner l'autre moyen relatif à cette partie de l'arrêt, son annulation sur ce point.<br/>
<br/>
              9. Il résulte de tout ce qui précède que l'arrêt attaqué doit être annulé en tant qu'il rejette les conclusions de la société Lofta relatives, d'une part, à l'impôt sur les sociétés ainsi qu'aux pénalités correspondantes et, d'autre part, aux pénalités correspondant à la cotisation minimale de taxe professionnelle. <br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 2 000 euros à verser à la société requérante au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 2 de l'arrêt de la cour administrative d'appel de Paris du 15 juin 2016 est annulé en tant qu'il rejette les conclusions de la société Lofta relatives, d'une part, à l'impôt sur les sociétés ainsi qu'aux pénalités correspondantes et, d'autre part, aux pénalités correspondantes à la cotisation minimale de taxe professionnelle.<br/>
<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Paris.<br/>
<br/>
Article 3 : L'Etat versera à la société Lofta la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Le surplus des conclusions du pourvoi de la société Lofta est rejeté.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la société Lofta et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
