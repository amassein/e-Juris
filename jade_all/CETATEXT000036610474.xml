<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036610474</ID>
<ANCIEN_ID>JG_L_2018_02_000000398703</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/61/04/CETATEXT000036610474.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 16/02/2018, 398703, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-02-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398703</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Vincent Uher</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:398703.20180216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée SPIE Ouest-Centre a demandé au tribunal administratif de Montreuil de lui accorder la décharge des rappels de taxe sur la valeur ajoutée mis à sa charge au titre de la période du 1er janvier au 31 décembre 2009, ainsi que des majorations correspondantes. Par un jugement n° 1301491 du 31 mars 2014, le tribunal administratif a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 14VE01574 du 11 février 2016, la cour administrative d'appel de Versailles a rejeté l'appel formé  par la société contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 11 avril et 11 juillet 2016 au secrétariat du contentieux du Conseil d'Etat, la société SPIE Ouest-Centre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Uher, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de la société SPIE Ouest-Centre ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société SPIE Ouest-Centre, qui exerce son activité dans le domaine du génie mécanique, des réseaux de communication et des travaux électriques, a fait l'objet d'une vérification de comptabilité à l'issue de laquelle l'administration lui a réclamé des rappels de taxe sur la valeur ajoutée au titre de la période comprise entre le 1er janvier et le 31 décembre 2009, assortis d'intérêts de retard et de la majoration pour manquement délibéré prévue à l'article 1729 du code général des impôts, pour un montant total de 662 455 euros. Par un jugement du 31 mars 2014, le tribunal administratif de Montreuil a rejeté sa demande tendant à la décharge de ces sommes. La société SPIE Ouest-Centre se pourvoit en cassation contre l'arrêt du 11 février 2016 par lequel la cour administrative d'appel de Versailles a rejeté l'appel qu'elle avait formé contre ce jugement. <br/>
<br/>
              Sur l'étendue du litige :<br/>
<br/>
              2. Par une décision du 31 août 2017, postérieure à l'introduction du pourvoi, l'administration a accordé à la société SPIE Ouest-Centre un dégrèvement d'un montant total de 436 356 euros, en droits et majorations, au titre des rappels litigieux. Ce dégrèvement porte, d'une part, sur la fraction des rappels qui procédait de la remise en cause de la déduction de la taxe sur la valeur ajoutée ayant grevé des prestations de services réglées au moyen de billets à ordre et, d'autre part, sur la fraction des intérêts de retard dont la société requérante soutenait qu'elle excédait le montant légalement dû et constituait un enrichissement sans cause pour l'administration. Par suite, les conclusions du pourvoi sont, dans cette mesure, devenues sans objet. Il ne reste en litige qu'une somme de 226 099 euros correspondant à la fraction de ces mêmes rappels qui procède du redressement afférent au droit à déduction de la taxe sur la valeur ajoutée ayant grevé des prestations de services réglées autrement que par des billets à ordre et aux pénalités correspondantes. <br/>
<br/>
              Sur le surplus des conclusions :<br/>
<br/>
              3. Aux termes du 2 de l'article 269 du code général des impôts : " La taxe est exigible : / (...) c) Pour les prestations de services, lors de l'encaissement des acomptes, du prix, de la rémunération ou, sur option du redevable, d'après les débits. (...) ". Aux termes du 2 de l'article 271 du même code : " Le droit à déduction prend naissance lorsque la taxe déductible devient exigible chez le redevable. ". <br/>
<br/>
              4. Devant la cour administrative d'appel, la société requérante faisait valoir, en ce qui concerne les factures réglées autrement que par des effets de commerce, que les rappels de taxe devaient être limités à la différence entre les montants déduits à tort de façon anticipée au titre de l'année 2009, d'une part, et les montants déduits également à tort de façon anticipée au titre de l'année 2008 et dont la déduction aurait normalement dû survenir au cours de l'année 2009, d'autre part. En écartant cette argumentation au motif que la société, qui précisait expressément ne pas formuler de demande de compensation à ce titre, ne produisait à l'appui de ses allégations aucun élément chiffré permettant de tenir pour établis les éléments sur lesquels elle fondait ses prétentions, la cour, qui n'a pas méconnu la portée de ses écritures et a souverainement apprécié les pièces du dossier qui lui était soumis sans les dénaturer, n'a pas méconnu les dispositions des articles 269 et 271 du code général des impôts.<br/>
<br/>
              5. Aux termes l'article 1729 du code général des impôts : " Les inexactitudes ou les omissions relevées dans une déclaration ou un acte comportant l'indication d'éléments à retenir pour l'assiette ou la liquidation de l'impôt ainsi que la restitution d'une créance de nature fiscale dont le versement a été indûment obtenu de l'Etat entraînent l'application d'une majoration de : / a. 40 % en cas de manquement délibéré ; (...) ". Aux termes de l'article L. 62 du livre des procédures fiscales : " Au cours d'une vérification de comptabilité et pour les impôts sur lesquels porte cette vérification, le contribuable peut régulariser les erreurs, inexactitudes, omissions ou insuffisances dans les déclarations souscrites dans les délais (...). Cette procédure de régularisation spontanée ne peut être appliquée que si : (...) / 2° La régularisation ne concerne pas une infraction exclusive de bonne foi (...) ".<br/>
<br/>
              6. La cour, qui a porté sur les faits une appréciation souveraine non arguée de dénaturation, a relevé que l'administration avait constaté le caractère systématique de la pratique par la société SPIE Ouest-Centre de déductions anticipées de taxe sur la valeur ajoutée et lui avait rappelé, dans le cadre d'une vérification de comptabilité ayant porté sur la période comprise entre le 1er janvier 2005 et le 31 décembre 2007, les modalités de déduction de la taxe afférente à des prestations de services, quel que soit le moyen de paiement employé. En jugeant sans incidence sur l'appréciation du caractère délibéré de cette pratique la circonstance que la société n'avait pas fait l'objet de redressements à la suite de cette vérification de comptabilité, dès lors qu'elle avait spontanément régularisé sa situation en application de l'article L. 62 du livre des procédures fiscales, la cour n'a pas commis d'erreur de droit. En déduisant des circonstances qu'elle a relevées que l'administration apportait la preuve du caractère délibéré du manquement de la société requérante, la cour n'a en outre pas entaché son arrêt d'erreur de qualification juridique des faits. <br/>
<br/>
              7. Il résulte de tout ce qui précède que le surplus des conclusions du pourvoi de la société SPIE Ouest-Centre doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>                    D E C I D E :<br/>
                                   --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions du pourvoi de la société SPIE Ouest-Centre dans la mesure du dégrèvement prononcé postérieurement à son introduction.<br/>
Article 2 : Le surplus des conclusions du pourvoi de la société SPIE Ouest-Centre est rejeté.<br/>
Article 3 : La présente décision sera notifiée à la société par actions simplifiée SPIE Ouest-Centre et au ministre de l'action et des comptes publics. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
