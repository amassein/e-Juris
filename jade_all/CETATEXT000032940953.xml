<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032940953</ID>
<ANCIEN_ID>JG_L_2016_07_000000391691</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/94/09/CETATEXT000032940953.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 27/07/2016, 391691, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391691</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUZIDI, BOUHANNA ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Frédéric Pacoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:391691.20160727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé à la commission départementale d'aide sociale de Vaucluse d'annuler la décision du 25 novembre 2011 par laquelle le président du conseil général de Vaucluse a refusé de lui accorder une remise gracieuse de dette à la suite de la décision de la caisse d'allocations familiales de Vaucluse de récupérer deux indus d'allocation de revenu minimum d'insertion, de 23 371 euros et de 400 euros, pour la période d'août 2005 à mai 2009. Par une décision du 6 mars 2012, la commission départementale d'aide sociale de Vaucluse a rejeté sa demande.<br/>
<br/>
              Par une décision n° 130029 du 17 avril 2015, la Commission centrale d'aide sociale a, sur demande de MmeB..., annulé la décision de la commission  départementale d'aide sociale de Vaucluse du 6 mars 2012 et la décision du président du conseil général de Vaucluse du 25 novembre 2011, renvoyé l'intéressée devant cette autorité pour un nouveau calcul de l'indu d'allocation de revenu minimum d'insertion, dans la limite de la prescription biennale, et rejeté le surplus des conclusions de l'appel de MmeB.... <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 10 juillet 2015, 30 septembre 2015 et 25 avril 2016 au secrétariat du contentieux du Conseil d'Etat, le département de Vaucluse demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision de la Commission centrale d'aide sociale du 17 avril 2015 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de MmeB... ;<br/>
<br/>
              3°) de mettre à la charge de Mme B...la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
- le code de l'action sociale et des familles ;<br/>
- la loi n° 2006-339 du 23 mars 2006 ;<br/>
- le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Pacoud, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat du département de Vaucluse, et à la SCP Bouzidi, Bouhanna, avocat de Mme B... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur la fin de non-recevoir opposée par MmeB... :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article R. 821-1 du code de justice administrative : " Sauf disposition contraire, le délai de recours en cassation est de deux mois ". Il ressort des pièces du dossier que la décision de la Commission centrale d'aide sociale du 17 avril 2015 dont le département de Vaucluse demande l'annulation a été notifiée par lettre recommandée avec demande d'avis de réception reçue par ce département le 11 mai 2015. Par suite, Mme B...n'est pas fondée à soutenir que le pourvoi du département, enregistré au secrétariat du contentieux du Conseil d'Etat le 10 juillet suivant, serait tardif. <br/>
<br/>
              Sur le bien-fondé de la Commission centrale d'aide sociale :<br/>
<br/>
              2. Aux termes de l'article L. 262-41 du code de l'action sociale et des familles, dans sa rédaction applicable jusqu'au 24 mars 2006 : " Tout paiement indu d'allocations est récupéré (...) / Toutefois, le bénéficiaire peut contester le caractère indu de la récupération devant la commission départementale d'aide sociale dans les conditions définies à l'article L. 262-39. / (...) / En cas de précarité de la situation du débiteur, la créance peut être remise ou réduite par le président du conseil général ". La loi du 23 mars 2006 pour le retour à l'emploi et sur les droits et les devoirs des bénéficiaires de minima sociaux a modifié le dernier alinéa de cet article pour prévoir que : " La créance peut être remise ou réduite par le président du conseil général en cas de précarité de la situation du débiteur, sauf en cas de manoeuvre frauduleuse ou de fausse déclaration ".<br/>
<br/>
              3. Il ressort des pièces du dossier soumis à la Commission centrale d'aide sociale que MmeB..., bénéficiaire du revenu minimum d'insertion, s'est vu réclamer un indu d'un montant total de 23 771 euros au titre des allocations perçues pour la période d'août 2005 à mai 2009. Après le rejet, le 10 août 2011, de son recours contestant le bien-fondé de ce trop-perçu, Mme B...a de nouveau saisi le président du conseil général de Vaucluse, qui, par une décision du 25 novembre 2011, a refusé de lui accorder une remise gracieuse de sa dette. L'intéressée a contesté cette dernière décision sans succès devant la commission départementale d'aide sociale de ce département, puis formé un appel devant la Commission centrale d'aide sociale qui, après avoir annulé pour irrégularité la décision de la commission départementale, a statué en se substituant à cette dernière.<br/>
<br/>
              4. Il résulte des termes mêmes de la décision de la Commission centrale d'aide sociale que celle-ci a estimé être saisie de conclusions dirigées contre la seule décision du président du conseil général de Vaucluse du 25 novembre 2011, qu'elle a regardée comme se bornant à refuser d'accorder une remise gracieuse à MmeB.... Il lui appartenait, dès lors, d'examiner si une remise gracieuse totale ou partielle était justifiée et de se prononcer elle-même sur la demande en recherchant si, au regard des circonstances de fait dont il était justifié par l'une et l'autre parties à la date de sa propre décision, la situation de précarité de l'intéressée et sa bonne foi justifiaient que lui soit accordée une remise ou une réduction de la somme mise à sa charge. En examinant le bien-fondé de l'indu réclamé à Mme B...et en annulant la décision du 25 novembre 2011 au motif que l'action en recouvrement était pour partie prescrite par application de l'article L. 262-40 du code de l'action sociale et des familles, la Commission centrale d'aide sociale a commis une erreur de droit.<br/>
<br/>
              5. Il suit de là que le département de Vaucluse est fondé à demander l'annulation de la décision de la Commission centrale d'aide sociale du 17 avril 2015. Le moyen d'erreur de droit retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens de son pourvoi.<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              6. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le département au titre des dispositions de l'article L. 761-1 du code de justice administrative. Les dispositions de cet article et de l'article 37 de la loi du 10 juillet 1991 font obstacle à ce qu'il soit fait droit aux conclusions présentées sur le fondement de ces dispositions par la SCP Bouzidi, Bouhanna, avocat de MmeB....<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision de la Commission centrale d'aide sociale du 17 avril 2015 est annulée.<br/>
Article 2 : L'affaire est renvoyée à la Commission centrale d'aide sociale.<br/>
Article 3 : Les conclusions du département de Vaucluse présentées au titre de l'article L. 761-1 et les conclusions de la SCP Bouzidi, Bouhanna présentées au titre des dispositions de cet article et de l'article 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au département de Vaucluse et à Mme A...B....<br/>
Copie en sera adressée à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
