<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000018007581</ID>
<ANCIEN_ID>JG_L_2007_11_000000279626</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/18/00/75/CETATEXT000018007581.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 21/11/2007, 279626, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2007-11-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>279626</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Delarue</PRESIDENT>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN</AVOCATS>
<RAPPORTEUR>M. Edouard  Geffray</RAPPORTEUR>
<COMMISSAIRE_GVT>Mlle Verot</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 14 avril et 14 juin 2005 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Alain A, demeurant ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 10 février 2005 par lequel la cour administrative d'appel de Nancy a rejeté son appel tendant d'une part à l'annulation du jugement du 15 juin 2004 du tribunal administratif de Nancy rejetant sa demande tendant à la réduction des cotisations supplémentaires à l'impôt sur le revenu auxquelles il a été assujetti au titre des années 1999 et 2000 ainsi qu'à la condamnation de l'Etat à lui rembourser les sommes représentant l'imposition des intérêts qu'il a perçus, augmentées elles-mêmes des intérêts capitalisés à compter de leurs dates de versement, d'autre part, à ce que soit prononcée la réduction demandée, enfin, à ce que l'Etat soit condamné à lui rembourser les sommes représentant la part fiscale acquittée sur les intérêts de retard, elle-même augmentée des intérêts de retard calculés au taux légal et capitalisés ;<br/>
<br/>
              2°) statuant au fond, de faire droit aux conclusions de sa requête devant la cour administrative d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 600 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu le code civil, notamment son article 1153 ;<br/>
<br/>
              Vu le code monétaire et financier, notamment son article L. 313-3 ;<br/>
<br/>
              Vu le code général des impôts ;<br/>
<br/>
              Vu le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Edouard Geffray, Auditeur,  <br/>
<br/>
              - les observations de la SCP Masse-Dessen, Thouvenin, avocat de M. A, <br/>
<br/>
              - les conclusions de Mlle Célia Verot, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes de l'article 1153 du code civil : « Dans les obligations qui se bornent au paiement d'une certaine somme, les dommages-intérêts résultant du retard dans l'exécution ne consistent jamais que dans la condamnation aux intérêts au taux légal, sauf les règles particulières au commerce et au cautionnement. / Ces dommages et intérêts sont dus sans que le créancier soit tenu de justifier d'aucune perte. / Ils ne sont dus que du jour de la sommation de payer ou d'un autre acte équivalent telle une lettre missive s'il en ressort une interpellation suffisante, excepté dans le cas où la loi les fait courir de plein droit. / Le créancier auquel son débiteur en retard a causé, par sa mauvaise foi, un préjudice indépendant de ce retard, peut obtenir des dommages et intérêts distincts des intérêts moratoires de la créance » ; qu'aux termes de l'article 3 de la loi du 11 juillet 1975, désormais codifié à l'article L. 313-3 du code monétaire et financier : « En cas de condamnation pécuniaire par décision de justice, le taux de l'intérêt légal est majoré de cinq points à l'expiration d'un délai de deux mois à compter du jour où la décision de justice est devenue exécutoire, fût-ce par provision. » ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A, agent contractuel recruté par le ministère de l'intérieur en 1967, a réclamé le bénéfice de l'indemnité de résidence, qui lui a été accordée à compter de la date de sa prise de fonction par un jugement du 28 décembre 1989 du tribunal administratif de Nancy ; que M. A a perçu à ce titre, en 1999, une indemnité de 22 210 F (3 385,59 euros) en principal assortie, pour un montant de 30 675 F (4 676,37 euros), des intérêts au taux légal majorés en application de l'article 3 de la loi du 11 juillet 1975 et, en 2000, une somme de 63 455 F (9 673,65 euros) représentative des mêmes intérêts majorés afférents à l'indemnité en principal qu'il avait perçue en 1998 ; qu'il a été assujetti, pour l'indemnité principale comme pour les intérêts moratoires et majorés, à l'impôt sur le revenu au titre des années 1999 et 2000 ; qu'il a saisi le tribunal administratif de Nancy d'une demande tendant à la réduction des cotisations d'impôt sur le revenu, au motif que les intérêts moratoires et majorés ne constituaient pas des revenus imposables ; que M. A demande l'annulation de l'arrêt du 10 février 2005 par lequel la cour administrative d'appel de Nancy a rejeté sa requête tendant à l'annulation du jugement du tribunal administratif de Nancy en date du 15 juin 2004 ayant rejeté sa demande ;<br/>
              Considérant qu'ainsi que l'a relevé la cour, les indemnités en principal perçues par M. A, qui avaient pour seul objet de compenser la perte de revenus résultant du refus illégal du ministre de l'intérieur de lui verser l'indemnité de résidence, présentaient le caractère de sommes soumises à l'impôt sur le revenu dans la catégorie des traitements et salaires ; que les intérêts moratoires afférents à ces indemnités dont ils ne sont que l'accessoire, doivent être soumis au même régime fiscal que ces dernières ; que les intérêts majorés prévus par l'article 3 de la loi du 11 juillet 1975 ne réparent pas un préjudice personnel mais résultent de l'augmentation forfaitaire du taux de l'intérêt légal lorsque le créancier n'a pas exécuté une décision de justice à l'expiration d'un délai de deux mois à compter du jour où celle-ci est devenue exécutoire ; qu'ils sont, dès lors, soumis au même régime fiscal que les intérêts moratoires qu'ils majorent ; que par suite, la cour administrative d'appel de Nancy n'a pas commis d'erreur de droit en considérant que les intérêts moratoires et majorés perçus par M. A étaient imposables comme les indemnités versées au principal auxquelles ils se rattachent ; <br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que M. A n'est pas fondé à demander l'annulation de l'arrêt attaqué ; que ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative doivent être en conséquence rejetées ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. Alain A et au ministre du budget, des comptes publics et de la fonction publique.<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
