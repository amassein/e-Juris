<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044314723</ID>
<ANCIEN_ID>JG_L_2021_11_000000450752</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/31/47/CETATEXT000044314723.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 09/11/2021, 450752</TITRE>
<DATE_DEC>2021-11-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450752</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH ; SCP LE GRIEL</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Philippe Ranquet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:450752.20211109</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 16 mars et 28 mai 2021 au secrétariat du contentieux du Conseil d'Etat, M. C... T... J..., M. P... T... J..., M. U... T... J..., M. H... T... J..., Mme E... Z... T... AA..., Mme W... T... Y... et Mme V... AB... T... AC... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le décret du 22 janvier 2021 en tant qu'il autorise Mme D... N... à changer son nom en " N... de J... " ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le décret n° 94-52 du 20 janvier 1994 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, conseillère d'Etat, <br/>
<br/>
              - les conclusions de M. Philippe Ranquet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Le Griel, avocat de M. T... J... et autres, et à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de Mme X... N... T... J... ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 61 du code civil : " Toute personne qui justifie d'un intérêt légitime peut demander à changer de nom. / La demande de changement de nom peut avoir pour objet d'éviter l'extinction du nom porté par un ascendant ou un collatéral du demandeur jusqu'au quatrième degré (...) ". En vertu du premier alinéa de l'article 61-1 du même code : " Tout intéressé peut faire opposition devant le Conseil d'Etat au décret portant changement de nom dans un délai de deux mois à compter de sa publication au Journal officiel ".<br/>
<br/>
              2. Par un décret du 22 janvier 2021, Mme D... N... a été autorisée, sur le fondement du second alinéa de l'article 61 du code civil, à changer son nom pour ajouter à son nom de famille celui de sa grand-mère paternelle, " de J... ", au motif d'éviter que ce nom s'éteigne. M. C... T... J... et plusieurs membres de la famille T... J... ont, en application des dispositions de l'article 61-1 du code civil, formé opposition à ce décret par la présente requête.<br/>
<br/>
              3. Le moyen tiré de ce que la demande de changement de nom introduite par Mme N... n'aurait pas fait l'objet d'une publication dans les conditions prévues par l'article 3 du décret du 20 janvier 1994 relatif à la procédure de changement de nom manque en fait.<br/>
<br/>
              4. Le relèvement d'un nom afin d'éviter son extinction suppose qu'il soit établi que le nom en cause a été légalement porté par un ascendant de celui qui demande à changer de nom ou par un collatéral jusqu'au quatrième degré. La réalité de l'extinction alléguée s'apprécie à l'intérieur de la famille du demandeur du nom à relever, dans le cadre ainsi défini. <br/>
<br/>
              5. Il résulte de l'instruction que le nom " de J... " que Mme N... a été autorisée à ajouter, par le décret attaqué, à son patronyme est celui de sa grand-mère paternelle et que ce nom est en voie d'extinction dans la famille de l'intéressée. Par suite, alors même qu'il existe dans la famille des requérants, différente de celle de Mme N..., des porteurs du nom revendiqué qui sont susceptibles de le transmettre, Mme N... justifiait d'un intérêt légitime à demander le changement de son nom.<br/>
<br/>
              6. Ainsi qu'il a été dit, Mme N... a été autorisée à adjoindre à son nom celui de sa grand-mère paternelle. Par ailleurs, le risque de confusion allégué par les requérants, dont le nom est distinct de celui que Mme N... souhaite porter, n'est pas établi. Dans ces conditions, en dépit de la rareté alléguée du nom de " de J... ", le préjudice invoqué par les requérants ne peut être regardé comme suffisant pour justifier leur opposition au décret attaqué.<br/>
<br/>
              7. Il résulte de ce qui précède que la requête de M. T... J... et autres doit être rejetée, y compris leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme N... le versement d'une somme au même titre. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. T... J... et autres est rejetée.<br/>
Article 2 : Les conclusions présentées par Mme N... T... J... au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. C... T... J..., premier requérant dénommé, à Mme D... N... T... J... et au garde des sceaux, ministre de la justice.  <br/>
              Délibéré à l'issue de la séance du 18 octobre 2021 où siégeaient : M. Rémy Schwartz, président adjoint de la section du contentieux, présidant ; M. K... O..., M. Olivier Japiot, présidents de chambre ; M. L... R..., Mme A... Q..., M. F... I..., M. G... S..., M. Jean-Yves Ollier, conseillers d'Etat et Mme Sophie-Caroline de Margerie, conseillère d'Etat-rapporteure.<br/>
<br/>
              Rendu le 9 novembre 2021.<br/>
<br/>
<br/>
                 Le Président : <br/>
                 Signé : M. Rémy Schwartz<br/>
 		La rapporteure : <br/>
      Signé : Mme Sophie-Caroline de Margerie<br/>
                 La secrétaire :<br/>
                 Signé : Mme M... B...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-01-03 DROITS CIVILS ET INDIVIDUELS. - ÉTAT DES PERSONNES. - CHANGEMENT DE NOM PATRONYMIQUE. - INTÉRÊT LÉGITIME AU CHANGEMENT DE NOM - EVITER L'EXTINCTION D'UN NOM - ECHELLE À LAQUELLE S'APPRÉCIE LE RISQUE D'EXTINCTION - FAMILLE DU DEMANDEUR [RJ1].
</SCT>
<ANA ID="9A"> 26-01-03 Le relèvement d'un nom afin d'éviter son extinction suppose qu'il soit établi que le nom en cause a été légalement porté par un ascendant de celui qui demande à changer de nom ou par un collatéral jusqu'au quatrième degré. ......La réalité de l'extinction alléguée s'apprécie à l'intérieur de la famille du demandeur du nom à relever, dans le cadre ainsi défini.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, décision du même jour, Garde des sceaux c / M. Josselin de Castro, n° 448719, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
