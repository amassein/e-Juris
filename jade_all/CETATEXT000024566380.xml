<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024566380</ID>
<ANCIEN_ID>JG_L_2011_09_000000348394</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/56/63/CETATEXT000024566380.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 14/09/2011, 348394, Publié au recueil Lebon</TITRE>
<DATE_DEC>2011-09-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>348394</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP HEMERY, THOMAS-RAQUIN</AVOCATS>
<RAPPORTEUR>Mme Anissia Morel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le mémoire, enregistré le 20 juin 2011 au secrétariat du contentieux du Conseil d'Etat, présenté pour M. Michel A, demeurant ..., en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 ; M. A demande au Conseil d'Etat, à l'appui de son pourvoi tendant à l'annulation de l'arrêt n° 09PA05289 du 10 février 2011 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant à l'annulation du jugement n° 00505716-5 du 19 mai 2009 par lequel le tribunal administratif de Melun a annulé pour excès de pouvoir la décision de la commission communale d'aménagement foncier de Chambry du 18 mai 2004 relative à ses comptes de propriété, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des articles L. 123-3 5° et L. 123-4 du code rural ;<br/>
<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code rural ;<br/>
<br/>
              Vu la loi n° 2006-11 du 5 janvier 2006 ;<br/>
<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anissia Morel, Auditeur,  <br/>
<br/>
              - les observations de la SCP Hémery, Thomas-Raquin, avocat de M. A, <br/>
<br/>
      - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Hémery, Thomas-Raquin, avocat de M. A ;<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ; <br/>
<br/>
              Considérant qu'aux termes de l'article L. 123-3 du code rural dans sa rédaction applicable au litige : " Doivent être réattribués à leurs propriétaires, sauf accord contraire, et ne subir que les modifications de limites indispensables à l'aménagement : / (...) 5° De façon générale, les immeubles dont les propriétaires ne peuvent bénéficier de l'opération de remembrement, en raison de l'utilisation spéciale desdits immeubles. " ; qu'aux termes de l'article L. 123-4 du même code dans sa rédaction applicable au litige antérieure à la loi du 5 janvier 2006 d'orientation agricole : " Chaque propriétaire doit recevoir, par la nouvelle distribution, une superficie globale équivalente, en valeur de productivité réelle, à celle des terrains qu'il a apportés, déduction faite de la surface nécessaire aux ouvrages collectifs mentionnés à l'article L. 123-8 et compte tenu des servitudes maintenues ou créées. / (...) / Sauf accord exprès des intéressés, l'équivalence en valeur de productivité réelle doit, en outre, être assurée par la commission communale dans chacune des natures de culture qu'elle aura déterminées. (...) / (...) Le paiement d'une soulte en espèces est autorisé lorsqu'il y a lieu d'indemniser le propriétaire du terrain cédé des plus-values transitoires qui s'y trouvent incorporées et qui sont définies par la commission. Le montant de la soulte n'est versé directement au bénéficiaire que si l'immeuble qu'il cède est libre de toute charge réelle, à l'exception des servitudes maintenues. La dépense engagée par le département au titre du remembrement de la commune comprend, dans la limite de 1 p. 100 de cette dépense, les soultes ainsi définies (...) " ;<br/>
<br/>
              Considérant que M. A soutient que les dispositions précitées méconnaissent le droit de propriété constitutionnellement garanti, notamment par l'article 2 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789, ainsi que les exigences de préservation et d'amélioration de l'environnement et le principe de conciliation posés aux articles 2 et 6 de la Charte de l'environnement, en ce que, en vertu de l'interprétation constante que leur a donnée la jurisprudence, elles excluent que des parcelles exploitées selon un mode de culture biologique présentent le caractère de " terrains à utilisation spéciale " devant, sauf accord contraire, être réattribuées à leurs propriétaires, en ce qu'elles ne permettent pas de qualifier l'exploitation selon un mode biologique de " nature de culture " pour l'appréciation de l'équivalence en valeur de productivité réelle entre les terres apportées et attribuées et en ce qu'elles n'ont pas envisagé, dans l'hypothèse où un propriétaire recevrait des terres exploitées selon un mode conventionnel en échange d'apport de terres exploitées selon un mode biologique, que ce dernier soit indemnisé ; <br/>
<br/>
              Considérant, en premier lieu, qu'il résulte de l'article 2 de la Déclaration des droits de l'homme et du citoyen que les limites apportées à l'exercice du droit de propriété doivent être justifiées par un motif d'intérêt général et proportionnées à l'objectif poursuivi ; que l'aménagement foncier agricole, applicable aux propriétés rurales non bâties, a principalement pour but, par une nouvelle distribution des parcelles morcelées et dispersées, la constitution d'exploitations rurales d'un seul tenant ou à grandes parcelles bien groupées, permettant d'améliorer l'exploitation agricole des biens qui y sont soumis ; qu'au regard de cet objectif, qui répond à un besoin d'intérêt général, la nouvelle distribution des terres reçues par chaque propriétaire à l'instant où il perd la jouissance de ses apports doit être équivalente en superficie et en valeur de productivité réelle à ceux-ci dans chacune des natures de culture qui ont été déterminées et doit améliorer ses conditions d'exploitation ; que les terres exploitées selon un mode de culture biologique sont affectées à un usage agricole et ne présentent ni une particularité leur conférant le caractère de terrains à utilisation spéciale imposant qu'elles soient réattribuées à leur propriétaire et soustraites à l'objectif de regroupement des parcelles, ni une spécificité culturale justifiant la constitution d'une catégorie particulière de nature de culture en fonction de laquelle la nouvelle distribution doive être réalisée ; que toutefois, il peut être tenu compte de ce mode d'exploitation et de la valeur culturale spécifique qui en résulte lors du classement des terres que la commission communale d'aménagement foncier, doit, sur le fondement de l'article R. 123-1 du code rural, effectuer à l'intérieur de chaque nature de culture ; que, par ailleurs, dans l'hypothèse où l'équivalence en valeur de productivité réelle n'a pu être obtenue, la commission communale peut décider d'indemniser, par l'attribution d'une soulte en espèces, le propriétaire des terrains apportés dans lesquels sont incorporées des plus-values transitoires, lesquelles peuvent, le cas échéant, résulter des investissements réalisés pour convertir les terres à l'exploitation selon des méthodes biologiques ; qu'enfin, les règles de fond applicables au remembrement imposent de tenir compte des particularités de l'exploitation en agriculture biologique pour apprécier le respect de l'objectif d'amélioration des conditions d'exploitation ; que les opérations d'aménagement foncier agricole se déroulent dans le cadre d'une procédure dont l'ensemble des étapes est placé sous le contrôle du juge ; qu'au regard de l'ensemble de ces garanties, ni le 5° de l'article L. 123-3 ni l'article L. 123-4 ne portent une atteinte excessive au droit de propriété ;<br/>
<br/>
              Considérant, en deuxième lieu, qu'en vertu de l'article 2 de la Charte de l'environnement, " toute personne a le devoir de prendre part à la préservation et à l'amélioration de l'environnement " ; qu'il résulte de ces dispositions que l'ensemble des personnes et notamment les pouvoirs publics et les autorités administratives sont tenus à une obligation de vigilance à l'égard des atteintes à l'environnement qui pourraient résulter de leur activité ; que, toutefois, les dispositions contestées n'ont ni pour objet ni pour effet de porter atteinte à l'environnement ; qu'il suit de là qu'elles ne méconnaissent pas l'article 2 de la Charte ;<br/>
<br/>
              Considérant, enfin, qu'il appartient au législateur de déterminer les modalités de mise en oeuvre du principe de conciliation, posé par l'article 6 de la Charte de l'environnement, entre la protection et la mise en valeur de l'environnement, le développement économique et le progrès social ; que la conciliation entre ces intérêts généraux n'impose pas au législateur d'aménager la règle de l'équivalence en valeur de productivité réelle en prévoyant un traitement différencié des parcelles selon qu'elles sont exploitées de manière biologique ou conventionnelle ; qu'au surplus le remembrement qui a pour objet, outre l'amélioration des conditions d'exploitation, l'aménagement rural du périmètre dans lequel il est mis en oeuvre, doit comme les autres modes d'aménagement foncier rural respecter, en application de l'article L. 121-1 du code rural, les objectifs mentionnés à l'article L. 111-1, lequel impose de tenir compte des fonctions économique, environnementale et sociale de l'espace agricole ; qu'ainsi, eu égard à son objet et ses modalités de mise en oeuvre, le remembrement tel qu'il est défini par la loi ne méconnaît pas les exigences de l'article 6 de la Charte de l'environnement ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la question de constitutionnalité soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; que, par suite, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question invoquée par M. A ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. A.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. Michel A, au ministre de l'agriculture, de l'alimentation, de la pêche, de la ruralité et de l'aménagement du territoire et au Premier ministre. <br/>
		Copie en sera adressée au Conseil constitutionnel.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">03-04-02 AGRICULTURE, CHASSE ET PÊCHE. REMEMBREMENT FONCIER AGRICOLE. ATTRIBUTIONS ET COMPOSITION DES LOTS. - 1) TERRES EXPLOITÉES SELON UN MODE DE CULTURE BIOLOGIQUE -  A) TERRAINS À UTILISATION SPÉCIALE - ABSENCE  [RJ1] - B) CATÉGORIE PARTICULIÈRE DE NATURE DE CULTURE - ABSENCE [RJ2] - 2) PRISE EN COMPTE DE CE MODE DE CULTURE POUR LE CLASSEMENT DES TERRES, LA RECONNAISSANCE ÉVENTUELLE DE PLUS-VALUES TRANSITOIRES ET LE RESPECT DE L'OBJECTIF D'AMÉLIORATION DES CONDITIONS D'EXPLOITATION [RJ3] - CONSÉQUENCE - ABSENCE D'ATTEINTE EXCESSIVE AU DROIT DE PROPRIÉTÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-10-05-04-02 PROCÉDURE. - QUESTION QUI N'EST PAS NOUVELLE ET NE PRÉSENTE PAS UN CARACTÈRE SÉRIEUX, COMPTE TENU DE L'INTERPRÉTATION DONNÉE PAR LA JURISPRUDENCE ADMINISTRATIVE DE LA DISPOSITION CONTESTÉE - POSSIBILITÉ POUR LE CONSEIL D'ETAT DE DONNER, À L'OCCASION DE L'EXAMEN D'UNE QPC, UNE INTERPRÉTATION COMPLÉMENTAIRE DE CETTE LOI - EXISTENCE [RJ4].
</SCT>
<ANA ID="9A"> 03-04-02 1) Les terres exploitées selon un mode de culture biologique sont affectées à un usage agricole et ne présentent :... ...- a) ni une particularité leur conférant le caractère de terrains à utilisation spéciale imposant qu'elles soient réattribuées à leur propriétaire et soustraites à l'objectif de regroupement des parcelles ;... ...- b) ni une spécificité culturale justifiant la constitution d'une catégorie particulière de nature de culture en fonction de laquelle la nouvelle distribution doive être réalisée.,,2) Toutefois, il peut être tenu compte de ce mode d'exploitation et de la valeur culturale spécifique qui en résulte lors du classement des terres à l' intérieur de chaque nature de culture. Par ailleurs, dans l'hypothèse où l'équivalence en valeur de productivité réelle n'a pu être obtenue, la commission communale peut décider d'indemniser, par l'attribution d'une soulte en espèces, le propriétaire des terrains apportés dans lesquels sont incorporées des plus-values transitoires, lesquelles peuvent, le cas échéant, résulter des investissements réalisés pour convertir les terres à l'exploitation selon des méthodes biologiques. Enfin, les règles de fond applicables au remembrement imposent de tenir compte des particularités de l'exploitation en agriculture biologique pour apprécier le respect de l'objectif d'amélioration des conditions d'exploitation. Les opérations d'aménagement foncier agricole se déroulent dans le cadre d'une procédure dont l'ensemble des étapes est placé sous le contrôle du juge. Au regard de l'ensemble de ces garanties, ne portent une atteinte excessive au droit de propriété ni le 5° de l'article L. 123-3 du code rural, ni l'article L. 123-4 de ce code, dans leur rédaction antérieure à la loi n° 2006-11 du 5 janvier 2006.</ANA>
<ANA ID="9B"> 54-10-05-04-02 Afin d'examiner le caractère sérieux de la question prioritaire de constitutionnalité (QPC) soulevée devant lui, le Conseil d'Etat, qui examine la loi telle qu'il l'a interprétée [RJ4], peut donner une interprétation complémentaire de cette loi.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 1er février 1993, Mme Briand, n° 82102, T. p. 600 ; CE, 23 juin 2004, Hinard, n° 221115, T. p. 580.,,[RJ2] Cf. CE, 23 juin 2004, Hinard, n° 221115, T. p. 580.,,[RJ3] Cf. CE, 21 septembre 2007, M. et Mme Gerbenne, n° 285062, T. p. 677.,,[RJ4] Rappr., a) s'agissant de la possibilité pour le Conseil d'Etat de procéder à une interprétation neutralisante de la disposition contestée, CE, 19 mai 2010, T&#133;, n° 331025, p. 168 ; b) s'agissant de celle de se référer à la loi telle qu'il l'a interprétée afin d'examiner une QPC, CE, 25 juin 2010, Mortagne, n° 326363, p. 217 ; CE, 16 juill. 2010, SCI La Saulaie, n° 334665, p. 315.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
