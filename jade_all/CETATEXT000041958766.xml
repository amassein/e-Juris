<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041958766</ID>
<ANCIEN_ID>JG_L_2020_06_000000421888</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/95/87/CETATEXT000041958766.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 03/06/2020, 421888</TITRE>
<DATE_DEC>2020-06-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421888</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>Mme Christelle Thomas</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Anne Iljic</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:421888.20200603</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé à la Cour nationale du droit d'asile d'annuler la décision du 31 juillet 2017 de l'Office français de protection des réfugiés et apatrides (OFPRA) qui a refusé de lui reconnaître la qualité de réfugié ou à défaut de lui accorder le bénéfice de la protection subsidiaire. Par une ordonnance n° 17035798 du 30 novembre 2017, la Cour nationale du droit d'asile a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 2 juillet et 3 août 2018 au secrétariat du contentieux du Conseil d'Etat, Mme B... A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de l'OFPRA une somme de 2 000 euros au profit de la SCP Coutard et Munier-Apaire au titre de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative sous réserve que la SCP renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New York le 31 janvier 1967 relatif au statut des réfugiés ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Christelle Thomas, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Anne Iljic, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat de Mme B... A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. D'une part, l'article L. 723-2 du code de l'entrée et du séjour des étrangers et du droit d'asile définit les cas dans lesquels l'Office français de protection des réfugiés et apatrides (OFPRA) statue ou peut statuer en procédure accélérée sur une demande d'asile. Le V de cet article précise que, sauf si la présence en France du demandeur constitue une menace grave pour l'ordre public, la sécurité publique ou la sûreté de l'Etat, l'OFPRA peut décider de ne pas statuer en procédure accélérée lorsque cela lui paraît nécessaire pour assurer un examen approprié de la demande, en particulier si le demandeur provenant d'un pays d'origine sûr inscrit sur la liste mentionnée au cinquième alinéa de l'article L. 722-1 du code de l'entrée et du séjour des étrangers et du droit d'asile invoque des raisons sérieuses de penser que son pays d'origine ne peut pas être considéré comme sûr en raison de sa situation personnelle et au regard des motifs de sa demande. L'article L. 723-3 du même code prévoit que : " Pendant toute la durée de la procédure d'examen de la demande, l'office peut définir les modalités particulières d'examen qu'il estime nécessaires pour l'exercice des droits d'un demandeur en raison de sa situation particulière ou de sa vulnérabilité. / Pour l'application du premier alinéa du présent article, l'office tient compte des informations sur la vulnérabilité qui lui sont transmises en application de l'article L. 744-6 et des éléments de vulnérabilité dont il peut seul avoir connaissance au vu de la demande ou des déclarations de l'intéressé " et précise que : " Lorsque l'office considère que le demandeur d'asile, en raison notamment des violences graves dont il a été victime ou de sa minorité, nécessite des garanties procédurales particulières qui ne sont pas compatibles avec l'examen de sa demande en procédure accélérée en application de l'article L. 723-2, il peut décider de ne pas statuer ainsi ". Le VI de l'article L. 723-2 dispose que la décision de l'Office de statuer en procédure accélérée comme le refus de ne pas statuer en procédure accélérée ne peuvent être contestés que dans le cadre du recours formé, en application de l'article L. 731-2, devant la Cour nationale du droit d'asile à l'encontre de la décision de l'Office.<br/>
<br/>
              2. D'autre part, l'article L. 731-2 du code de l'entrée et du séjour des étrangers et du droit d'asile, après avoir disposé que : " La Cour nationale du droit d'asile statue en formation collégiale ", prévoit que " sans préjudice de l'application de l'article L. 733-2, lorsque la décision de l'office a été prise en application des articles L. 723-2 ou L. 723-11, le président de la Cour nationale du droit d'asile ou le président de formation de jugement qu'il désigne à cette fin statue dans un délai de cinq semaines à compter de sa saisine ". Le même article précise que : " De sa propre initiative ou à la demande du requérant, le président de la cour ou le président de formation de jugement désigné à cette fin peut, à tout moment de la procédure, renvoyer à la formation collégiale la demande s'il estime que celle-ci ne relève pas de l'un des cas prévus aux articles L. 723-2 et L. 723-11 ou qu'elle soulève une difficulté sérieuse ". Aux termes de l'article L. 733-2 du même code : " Le président et les présidents de section, de chambre ou de formation de jugement peuvent, par ordonnance, régler les affaires dont la nature ne justifie pas l'intervention de l'une des formations prévues à l'article L. 731-2. / Un décret en Conseil d'Etat fixe les conditions d'application du présent article. Il précise les conditions dans lesquelles le président et les présidents de section, de chambre ou de formation de jugement peuvent, après instruction, statuer par ordonnance sur les demandes qui ne présentent aucun élément sérieux susceptible de remettre en cause la décision d'irrecevabilité ou de rejet du directeur général de l'office ". L'article R. 733-4 du même code précise que : " Le président de la cour et les présidents qu'il désigne à cet effet peuvent, par ordonnance motivée :  [...] 5° Rejeter les recours qui ne présentent aucun élément sérieux susceptible de remettre en cause la décision de l'Office français de protection des réfugiés et des apatrides ; dans ce cas, l'ordonnance ne peut être prise qu'après que le requérant a été mis en mesure de prendre connaissance des pièces du dossier et après examen de l'affaire par un rapporteur ".<br/>
<br/>
              3. Il résulte des dispositions citées au point précédent qu'il appartient au magistrat désigné pour statuer seul sur une demande d'asile, tant, lorsque l'OFPRA a statué en procédure accélérée sur le fondement de l'article L. 731-2 précité, que lorsqu'une affaire ne justifie pas, devant la Cour nationale du droit d'asile, l'intervention d'une formation collégiale conformément à l'article L. 733-2 précité, de renvoyer l'affaire à une formation collégiale notamment lorsqu'il estime que la demande d'asile ne relève pas de la procédure accélérée, en particulier en raison de la vulnérabilité du demandeur, ou soulève une difficulté sérieuse. Il appartient au Conseil d'Etat, statuant en cassation, de censurer la décision ou l'ordonnance qui lui est déférée dans le cas où il juge, au vu de l'ensemble des circonstances de l'espèce, qu'il a été fait un usage abusif de la faculté ouverte par l'article L. 731-2 ou l'article L. 733-2 du code de l'entrée et séjour des étrangers et du droit d'asile.<br/>
<br/>
              4. Il ressort des pièces de la procédure que Mme A..., de nationalité kosovare, a soutenu, à l'appui de sa requête dirigée contre la décision du 31 juillet 2017 de l'Office français de protection des réfugiés et apatrides, que sa situation personnelle n'était pas compatible avec un examen de sa demande d'asile en procédure accélérée.<br/>
<br/>
              5. En premier lieu, il ressort des énonciations de l'ordonnance attaquée que, contrairement à ce qui est soutenu, son auteur a indiqué, alors qu'il n'y était pas tenu, les motifs, tenant au caractère sommaire des allégations de la requérante relatives à la situation générale en Albanie, pays d'origine sûr, justifiant le recours à une ordonnance prise sur le fondement de l'article L. 733-2 du code de l'entrée et du séjour des étrangers et du droit d'asile et le refus d'un renvoi de l'affaire devant une formation collégiale.  <br/>
<br/>
              6. En second lieu, il ressort des pièces du dossier soumis aux juges du fond qu'au vu des allégations de la requête présentée devant lui relatives, d'une part, à la situation générale en Albanie et, d'autre part, à la vulnérabilité de Mme A... au sens de l'article L. 744-6 du code de l'entrée et du séjour des étrangers et du droit d'asile, le président désigné par la présidente de la Cour nationale du droit d'asile a pu, sans abus, faire usage de la faculté que lui reconnaissent les dispositions précitées de l'article L. 733-2 du même code pour rejeter par ordonnance la requête dont il était saisi.<br/>
<br/>
              7. Il résulte de ce qui précède que le pourvoi de Mme A... doit être rejeté y compris ses conclusions présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                          --------------<br/>
<br/>
Article 1er : Le pourvoi de Mme A... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme B... A... et à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-08-04-01 - MAGISTRAT DÉSIGNÉ POUR STATUER SEUL SUR UNE DEMANDE D'ASILE (ART. L. 731-2 ET L. 733-2 DU CESEDA) - OBLIGATION DE RENVOYER L'AFFAIRE À UNE FORMATION COLLÉGIALE - EXISTENCE, LORSQUE LA DEMANDE D'ASILE NE RELÈVE PAS DE LA PROCÉDURE ACCÉLÉRÉE OU SOULÈVE UNE DIFFICULTÉ SÉRIEUSE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">095-08-06-01 - FACULTÉ POUR LE MAGISTRAT DÉSIGNÉ DE STATUER SEUL SUR UNE DEMANDE D'ASILE (ART. L. 731-2 ET L. 733-2 DU CESEDA) - CONTRÔLE DES SEULS ABUS DE L'USAGE DE CETTE FACULTÉ [RJ1] - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-08-02-02-005-02 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. RÉGULARITÉ EXTERNE. PROCÉDURE SUIVIE. - FACULTÉ POUR LE MAGISTRAT DÉSIGNÉ DE LA CNDA DE STATUER SEUL SUR UNE DEMANDE D'ASILE (ART. L. 731-2 ET L. 733-2 DU CESEDA) - CONTRÔLE DES SEULS ABUS DE L'USAGE DE CETTE FACULTÉ [RJ1] - EXISTENCE.
</SCT>
<ANA ID="9A"> 095-08-04-01 Il résulte des articles L. 731-2, L. 733-2 et R. 733-4 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) qu'il appartient au magistrat désigné pour statuer seul sur une demande d'asile, tant, lorsque l'Office français de protection des réfugiés et apatrides (OFPRA) a statué en procédure accélérée, sur le fondement de l'article L. 731-2 que lorsqu'une affaire ne justifie pas, devant la Cour nationale du droit d'asile (CNDA), l'intervention d'une formation collégiale conformément à l'article L.733-2, de renvoyer l'affaire à une formation collégiale notamment lorsqu'il estime que la demande d'asile ne relève pas de la procédure accélérée, en particulier en raison de la vulnérabilité du demandeur, ou soulève une difficulté sérieuse.</ANA>
<ANA ID="9B"> 095-08-06-01 Il résulte des articles L. 731-2, L. 733-2 et R. 733-4 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) qu'il appartient au magistrat désigné pour statuer seul sur une demande d'asile, tant, lorsque l'Office français de protection des réfugiés et apatrides (OFPRA) a statué en procédure accélérée, sur le fondement de l'article L. 731-2 que lorsqu'une affaire ne justifie pas, devant la Cour nationale du droit d'asile (CNDA), l'intervention d'une formation collégiale conformément à l'article L.733-2, de renvoyer l'affaire à une formation collégiale notamment lorsqu'il estime que la demande d'asile ne relève pas de la procédure accélérée, en particulier en raison de la vulnérabilité du demandeur, ou soulève une difficulté sérieuse. Il appartient au Conseil d'Etat, statuant en cassation de censurer la décision ou l'ordonnance qui lui est déférée dans le cas où il juge, au vu de l'ensemble des circonstances de l'espèce, qu'il a été fait un usage abusif de la faculté ouverte par l'article L. 731-2 ou l'article L. 733-2 du CESEDA.</ANA>
<ANA ID="9C"> 54-08-02-02-005-02 Il résulte des articles L. 731-2, L. 733-2 et R. 733-4 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) qu'il appartient au magistrat désigné pour statuer seul sur une demande d'asile, tant, lorsque l'Office français de protection des réfugiés et apatrides (OFPRA) a statué en procédure accélérée, sur le fondement de l'article L. 731-2 que lorsqu'une affaire ne justifie pas, devant la Cour nationale du droit d'asile (CNDA), l'intervention d'une formation collégiale conformément à l'article L.733-2, de renvoyer l'affaire à une formation collégiale notamment lorsqu'il estime que la demande d'asile ne relève pas de la procédure accélérée, en particulier en raison de la vulnérabilité du demandeur, ou soulève une difficulté sérieuse. Il appartient au Conseil d'Etat, statuant en cassation de censurer la décision ou l'ordonnance qui lui est déférée dans le cas où il juge, au vu de l'ensemble des circonstances de l'espèce, qu'il a été fait un usage abusif de la faculté ouverte par l'article L. 731-2 ou l'article L. 733-2 du CESEDA.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de la nature du contrôle, CE, Section, 5 octobre 2018, SA Finamur, n° 412560, p. 370.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
