<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029835107</ID>
<ANCIEN_ID>JG_L_2014_12_000000368584</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/83/51/CETATEXT000029835107.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 03/12/2014, 368584, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-12-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368584</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR>M. David Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:368584.20141203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête et le mémoire complémentaire, enregistrés les 16 mai et 16 août 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SELARL centre médical Nobel, dont le siège est route de Saint-Leu à Villetaneuse (93430) ; la SELARL centre médical Nobel demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 223 du 20 février 2013 par laquelle le Conseil national de l'ordre des médecins, statuant en formation restreinte, a rejeté son recours hiérarchique dirigé contre la décision du 24 septembre 2012 par laquelle la formation restreinte du conseil régional de l'ordre des médecins d'Ile-de-France a rejeté sa demande tendant à l'annulation des décisions des 11 avril et 21 juin 2005 du conseil départemental de l'ordre des médecins de la Seine-Saint-Denis lui refusant l'autorisation d'exercice sur un site supplémentaire à Paris au 78 avenue des Ternes dans le 17ème arrondissement ; <br/>
<br/>
              2°) de mettre à la charge du Conseil national de l'ordre des médecins la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              3°) de mettre à la charge du Conseil national de l'ordre des médecins les entiers dépens et notamment les frais du timbre fiscal ; <br/>
<br/>
<br/>
              Vu le code de la santé publique ; <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. David Moreau, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, avocat de la SELARL centre médical Nobel et à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
              1. Considérant que par une décision du 20 février 2013, le Conseil national de l'ordre des médecins, statuant en formation restreinte, a rejeté le recours hiérarchique de la SELARL centre médical Nobel dirigé contre la décision de la formation restreinte du conseil régional de l'ordre des médecins d'Ile-de-France du 24 septembre 2012 rejetant sa demande tendant à l'annulation des décisions des 11 avril et 21 juin 2005 du conseil départemental de l'ordre des médecins de la Seine-Saint-Denis lui refusant l'autorisation d'exercice sur un site supplémentaire à Paris au 78 avenue des Ternes dans le 17ème arrondissement ; que la SELARL demande l'annulation pour excès de pouvoir de la décision du 20 février 2013 ; <br/>
<br/>
              Sur la légalité externe de la décision attaquée :<br/>
<br/>
              2. Considérant que, contrairement à ce que soutient la requérante, il ressort des pièces du dossier que la formation restreinte avait été légalement constituée et que ses membres avaient été régulièrement désignés ; que, par suite, le moyen tiré de l'incompétence de l'auteur de la décision doit être écarté ;<br/>
<br/>
              Sur la légalité interne de la décision attaquée :<br/>
<br/>
              3. Considérant qu'aux termes de l'article R. 4113-4 du code de la santé publique applicable aux sociétés d'exercice libéral : " l'inscription ne peut être refusée que si les statuts ne sont pas conformes aux dispositions législatives et règlementaires en vigueur " ; qu'aux termes de l'article R. 4113-23 du même code, dans sa version applicable à l'espèce : " L'activité d'une société d'exercice libéral de médecins ne peut s'effectuer que dans un lieu unique. Toutefois, par dérogation aux dispositions du code de déontologie médicale mentionnées à l'article R. 4127-85, la société peut exercer dans cinq lieux au maximum lorsque, d'une part, elle utilise des équipements implantés en des lieux différents ou met en oeuvre des techniques spécifiques et que, d'autre part, l'intérêt des malades le justifie " ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier que par une délibération du 27 janvier 2005 le conseil départemental de l'ordre des médecins de la Seine-Saint-Denis a prononcé l'inscription de la société d'exercice libéral centre médical Nobel au tableau de son ordre en vue de l'exercice de son activité à son siège social, mais ne s'est pas prononcée sur un tel exercice à Paris ; que les décisions du conseil départemental de l'ordre des médecins de Seine-Saint Denis des 11 avril et 21 juin 2005 se sont bornées à confirmer la décision du 27 janvier 2005 ; que le Conseil national de l'ordre des médecins, en estimant que la délibération du 27 janvier 2005 du conseil départemental de l'ordre des médecins de la Seine-Saint-Denis ne l'avait pas autorisée à exercer sur un site secondaire à Paris, ne s'est donc pas mépris sur la portée de la décision du 27 janvier 2005 ; que, par suite, le moyen tiré de ce que la décision attaquée aurait illégalement validé le retrait d'une précédente autorisation créatrice de droits ne peut qu'être écarté ; <br/>
<br/>
              5. Considérant qu'en estimant que l'activité de lipotomie anti-âge exercée par la société d'exercice libéral centre médical Nobel ne justifiait pas, dans l'intérêt des patients, l'autorisation d'exercice sur un site distinct à Paris, la formation restreinte du Conseil national de l'ordre des médecins a fait une exacte application de l'article R. 4113-23 du code de la santé publique ;<br/>
<br/>
              Sur l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              6. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à la charge du Conseil national de l'ordre des médecins qui n'est pas, dans la présente instance, la partie perdante ; qu'en revanche, dans les circonstances de l'espèce, il y a lieu de mettre à la charge de la SELARL centre médical Nobel une somme de 3 000 euros au titre des frais exposés par le Conseil national de l'ordre des médecins et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de la SELARL centre médical Nobel est rejetée.<br/>
Article 2 : La SELARL centre médical Nobel versera une somme de 3 000 euros au Conseil national de l'ordre des médecins au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la SELARL centre médical Nobel et au Conseil national de l'ordre des médecins.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
