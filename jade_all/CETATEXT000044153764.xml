<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044153764</ID>
<ANCIEN_ID>JG_L_2021_09_000000448647</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/15/37/CETATEXT000044153764.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 29/09/2021, 448647, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-09-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448647</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI, REBEYROL</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Marc Pichon de Vendeuil</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:448647.20210929</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et cinq nouveaux mémoires, enregistrés les 13 janvier, 15 mars, 12 avril, 9 mai et 13 et 15 septembre 2021, M. A... B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le paragraphe 2.2 de la circulaire du 22 décembre 2020 de la Caisse nationale d'assurance vieillesse (CNAV) intitulée " revalorisation du SMIC au 1er janvier 2021 et incidences en matière de législation vieillesse " ;<br/>
<br/>
              2°) à titre subsidiaire, de surseoir à statuer et de saisir la Cour de justice de l'Union européenne d'une question préjudicielle ;<br/>
<br/>
              3°) d'ordonner la tenue d'une médiation ; <br/>
<br/>
              4°) en tout état de cause, de mettre à la charge de la CNAV la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le traité sur le fonctionnement de l'Union européenne ; <br/>
              - le code de la sécurité sociale ; <br/>
              - le code de justice administrative ; <br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Marc Pichon de Vendeuil, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Gatineau, Fattaccini, Rebeyrol, avocat de la Caisse nationale d'assurance vieillesse ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. M. B... demande l'annulation pour excès de pouvoir du paragraphe 2.2 de la circulaire du 22 décembre 2020 de la Caisse nationale d'assurance vieillesse (CNAV) intitulée " revalorisation du SMIC au 1er janvier 2021 et incidences en matière de législation vieillesse ". <br/>
<br/>
              Sur les conclusions tendant à ce qu'il soit ordonné une médiation :<br/>
<br/>
              2. Aux termes de l'article L. 114-1 du code de justice administrative : " Lorsque le Conseil d'Etat est saisi d'un litige en premier et dernier ressort, il peut, après avoir obtenu l'accord des parties, ordonner une médiation pour tenter de parvenir à un accord entre celles-ci (...) ". Il n'y pas lieu pour le Conseil d'Etat, dans les circonstances de l'espèce, de proposer une médiation aux parties.<br/>
<br/>
              Sur les conclusions aux fins d'annulation :<br/>
<br/>
              3. En vertu d'une jurisprudence constante de la Cour de justice de l'Union européenne, le droit de l'Union européenne ne porte pas atteinte à la compétence des Etats membres pour aménager leur système de sécurité sociale. Il appartient ainsi, en l'absence d'une harmonisation au niveau européen, à la législation de chaque État membre de déterminer les conditions du droit ou de l'obligation de s'affilier à un régime de sécurité sociale et le mode de financement de ce régime. En particulier, le droit de l'Union européenne ne fait pas obligation à tout organisme chargé de la gestion d'un régime de sécurité sociale de mettre en œuvre le principe de solidarité mais impose seulement que ce critère soit rempli pour qu'un tel organisme ne soit pas regardé comme une entreprise au sens du traité sur le fonctionnement de l'Union européenne, qui serait alors soumise aux règles de concurrence qui résultent de ce traité. Par suite, le moyen tiré de ce que, par la circulaire attaquée, la Caisse nationale d'assurance vieillesse méconnaîtrait le droit de l'Union européenne au seul motif qu'elle ne mettrait pas en œuvre le principe de solidarité ne peut, en tout état de cause, qu'être écarté.<br/>
<br/>
              4. Il résulte de ce qui précède que, sans qu'il y ait lieu de saisir la Cour de justice de l'Union européenne à titre préjudiciel, M. B... n'est pas fondé à demander l'annulation du paragraphe 2.2 de la circulaire attaquée.<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la Caisse nationale d'assurance vieillesse qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu en revanche, dans les circonstances de l'espèce, de mettre à la charge de M. B... la somme de 1 500 euros à verser à la Caisse nationale d'assurance vieillesse au titre des mêmes dispositions. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : M. B... versera à la Caisse nationale d'assurance vieillesse une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à M. A... B..., à la Caisse nationale d'assurance vieillesse et au ministre des solidarités et de la santé. <br/>
Copie en sera adressée au ministre de l'économie, des finances et de la relance et à la ministre du travail, de l'emploi et de l'insertion. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
