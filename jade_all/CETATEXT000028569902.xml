<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028569902</ID>
<ANCIEN_ID>JG_L_2014_02_000000367093</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/56/99/CETATEXT000028569902.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 05/02/2014, 367093, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-02-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367093</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LE GRIEL</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Anton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:367093.20140205</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 22 mars 2013 au secrétariat du contentieux du Conseil d'Etat, présenté par la Commission nationale des comptes de campagne et des financements politiques, dont le siège est 34-36, rue du Louvre à Paris (75042 Cedex 01) ; elle demande au Conseil d'Etat d'annuler l'arrêt n° 11PA04152 du 21 janvier 2013 de la cour administrative d'appel de Paris, en tant qu'il a fixé le montant du remboursement dû par l'Etat au titre des dépenses de campagne de la liste conduite par M.B..., candidat aux élections au conseil régional de Poitou-Charentes, à la somme de 117 297 euros et a réformé le jugement n° 1100335 du 12 juillet 2011 du tribunal administratif de Paris et la décision du 18 octobre 2010 de la Commission nationale des comptes de campagne et des financements politiques pour admettre le caractère remboursable par l'Etat de la somme de 1350 euros correspondant à la facturation à M. B...de locaux loués par son parti politique ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code électoral, modifié notamment par l'ordonnance n° 2003-1165 du 8 décembre 2003 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Anton, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Le Griel, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la Commission nationale des comptes de campagne et des financements politiques a, par une décision en date du 18 octobre 2010, approuvé, après réformation, le compte de campagne déposé par M.B..., candidat tête de liste aux élections qui se sont déroulées les 14 et 21 mars 2010 en vue de l'élection des membres du conseil régional de Poitou-Charentes  ; que, par un jugement du 12 juillet 2011, le tribunal administratif de Paris a rejeté la demande de M. B...tendant à la réformation de cette décision en tant qu'elle maintenait l'exclusion de certaines dépenses du remboursement dû par l'Etat au titre de ces opérations électorales ; que la cour administrative d'appel de Paris, par un arrêt du 21 janvier 2013, a partiellement fait droit à sa requête en admettant le caractère remboursable par l'Etat de la somme de 1 350 euros correspondant aux frais de location de locaux mis à disposition de M. B...par son parti politique ; que la Commission nationale des comptes de campagne et des financements politiques se pourvoit en cassation contre cet arrêt en tant qu'il a admis le caractère remboursable par l'Etat de cette dépense ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 52-15 du code électoral, dans sa rédaction issue de l'ordonnance du 8 décembre 2003 portant simplifications administratives en matière électorale : "  La Commission nationale des comptes de campagne et des financements politiques approuve et, après procédure contradictoire, rejette ou réforme les comptes de campagne. Elle arrête le montant du remboursement forfaitaire prévu à l'article L. 52-11-1. (...) " ; qu'aux termes de l'article L. 52-4 de ce code : " Le mandataire recueille, pendant l'année précédant le premier jour du mois de l'élection et jusqu'à la date du dépôt du compte de campagne du candidat, les fonds destinés au financement de la campagne. / Il règle les dépenses engagées en vue de l'élection et antérieures à la date du tour de scrutin où elle a été acquise " ; qu'aux termes de l'article L. 52-11-1 du même code, dans sa rédaction issue de la même ordonnance : " Les dépenses électorales des candidats aux élections auxquelles l'article L. 52-4 est applicable font l'objet d'un remboursement forfaitaire de la part de l'Etat égal à 50 % de leur plafond de dépenses. Ce remboursement ne peut excéder le montant des dépenses réglées sur l'apport personnel des candidats et retracées dans leur compte de campagne (...) " ; que les dépenses électorales susceptibles de faire l'objet du remboursement forfaitaire par l'Etat sont définies à l'article L. 52-12 du même code comme l'ensemble des dépenses engagées ou effectuées en vue de l'élection hors celles de la campagne officielle par le candidat ou pour son compte au cours de la période mentionnée à l'article L. 52-4 de ce code ; <br/>
<br/>
              3. Considérant qu'aucune disposition ne fait obstacle à ce qu'un candidat soit remboursé, dans le cadre défini par l'article L. 52-11-1 du code électoral, de dépenses de campagne correspondant à des prestations assurées à titre onéreux par un parti ou groupement politique ; que si, ainsi que l'a rappelé la cour, il appartient à la Commission nationale des comptes de campagne et des financements politiques, en application de l'article L. 52-15 de ce code et sous le contrôle du juge, de relever les irrégularités éventuelles des dépenses facturées par les formations politiques tenant, notamment, à l'inexistence des prestations ou à leur surévaluation et de  réformer en conséquence les comptes de campagne dont elle est saisie, il lui incombe également, à ce titre, de vérifier si les dépenses relatives à ces prestations ont été exposées spécifiquement en vue de l'élection et si elles correspondent à des charges relevant du fonctionnement habituel de la formation politique, qui auraient été supportées par celle-ci en dehors de toute circonstance électorale ; qu'ainsi, des frais de location de locaux facturés à un candidat à une élection par sa formation politique ne peuvent ouvrir droit au remboursement par l'Etat que si les dépenses correspondantes ont été engagées par cette formation politique spécifiquement en vue  de cette élection ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède qu'en jugeant que les frais de location des locaux mis à la disposition de M. B...par son parti politique, dans le cadre d'une convention conclue le 30 septembre 2009, pour la durée de la campagne électorale, étaient susceptibles de bénéficier du remboursement forfaitaire de l'Etat, après avoir souverainement estimé que ces locaux étaient antérieurement et sont demeurés affectés au fonctionnement habituel de ce parti, au motif que la Commission nationale des comptes de campagne et des financements politiques n'établissait ni même n'alléguait que ces prestations auraient été inexistantes ou surévaluées, la cour a commis une erreur de droit ; que, dès lors, la Commission nationale des comptes de campagne et des financements politiques est fondée à demander, dans cette mesure, l'annulation de l'arrêt qu'elle attaque ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond, en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant qu'ainsi qu'il a été dit au point 4, les frais de location des locaux mis à la disposition de M. B...par son parti politique pour la durée de la campagne électorale, qui n'avaient pas été exposés spécifiquement pour cette élection, n'étaient pas susceptibles de bénéficier du remboursement forfaitaire de l'Etat  ; que, dès lors, M. B... n'est pas fondé à soutenir que c'est à tort que le tribunal administratif de Paris a jugé que la Commission nationale des comptes de campagne et des financements politiques avait à bon droit exclu du remboursement par l'Etat la somme de 1 350 euros correspondant à ces frais ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 21 janvier 2013 est annulé en tant qu'il a réformé le jugement du tribunal administratif de Paris du 12 juillet 2011 et la décision du 18 octobre 2010 de la Commission nationale des comptes de campagne et des financements politiques en ce qu'ils ont exclu de l'assiette des dépenses ouvrant droit au remboursement forfaitaire de l'Etat la somme de 1 350 euros exposée par M. B...pour la location de locaux.<br/>
Article 2 : Les conclusions de la requête présentées par M. B...devant la cour administrative d'appel de Paris tendant à admettre au remboursement par l'Etat la somme de 1 350 euros sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la Commission nationale des comptes de campagne et des financements politiques et à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
