<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027410924</ID>
<ANCIEN_ID>JG_L_2013_05_000000352230</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/41/09/CETATEXT000027410924.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 15/05/2013, 352230, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-05-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352230</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT ; SPINOSI</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:352230.20130515</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 29 août et 29 novembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'établissement national des produits de l'agriculture et de la mer (FRANCEAGRIMER), dont le siège est 12, rue Henri Rol-Tanguy, TSA 20002 à Montreuil-sous-Bois Cedex (93555) ; FRANCEAGRIMER demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09MA04281 du 23 juin 2011 par lequel la cour administrative d'appel de Marseille, faisant droit à la requête de l'EARL Les Saules, a, d'une part, annulé le jugement du 16 octobre 2009 par lequel le tribunal administratif de Montpellier a rejeté la demande de l'EARL tendant à l'annulation de l'ordre de reversement et des titres de recettes émis à son encontre par l'office national interprofessionnel des fruits, des légumes, des vins et de l'horticulture (VINIFLHOR) et, d'autre part, déchargé l'EARL de l'obligation de payer mise à sa charge et annulé les titres de recettes émis par VINIFLHOR ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'EARL Les Saules le versement d'une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le règlement (CEE) n° 3816/92 du Conseil du 28 décembre 1992 ;<br/>
<br/>
              Vu le code rural ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée avant et après les conclusions à Me Balat, avocat de l'établissement national des produits de l'agriculture et de la mer et à Me Spinosi, avocat de l'Earl Les Saules ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, le 2 septembre 1998, dans le cadre du programme d'actions des autorités françaises élaboré en application des dispositions de l'article 2 du règlement n° 3816/92 du Conseil du 28 décembre 1992 prévoyant, dans le secteur des fruits et légumes, la suppression du mécanisme de compensation dans les échanges entre l'Espagne et les autres Etats membres, ainsi que des mesures connexes, dont la mise en oeuvre, s'agissant des dépenses de modernisation des serres maraichères, a été précisée par une circulaire de son directeur du 9 août 1995, l'office national interprofessionnel des fruits, des légumes et de l'horticulture (ONIFHLOR) a attribué à l'EARL Les Saules une subvention pour la réalisation d'un projet de construction de serres destinées à la production de tomates ; que, le 2 septembre 2008, à la suite de contrôles effectués en 2000 et 2001 par l'agence centrale des organismes d'intervention dans le secteur agricole révélant que les demandes de subvention émanant de douze EARL, au nombre desquelles elle figurait, se rapportaient à un projet unique, l'office national interprofessionnel des fruits, des légumes, des vins et de l'horticulture (VINIFLHOR), venant aux droits de l'ONIFHLOR, a adressé à l'EARL Les Saules un ordre de reversement de la subvention qu'elle avait reçue au motif qu'elle lui avait été attribuée en méconnaissance des dispositions de l'article L. 341-3 du code rural du plafonnement de la subvention un projet unique ; que, les 22 et 23 septembre 2008, VINIFLHOR a émis à l'encontre de l'EARL deux titres de recettes pour le reversement de la subvention perçue ; que l'établissement national des produits de l'agriculture et de la mer (FRANCEAGRIMER), venant aux droits de VINIFLHOR, se pourvoit en cassation contre l'arrêt du 23 juin 2011 par lequel la cour administrative d'appel de Marseille, faisant droit à la requête de l'EARL Les Saules, a, d'une part, annulé le jugement du 16 octobre 2009 par lequel le tribunal administratif de Montpellier a rejeté la demande de l'EARL tendant à l'annulation de l'ordre de reversement et des titres de recettes émis à son encontre et, d'autre part, déchargé l'EARL de l'obligation de payer mise à sa charge et annulé les titres de recettes ;<br/>
<br/>
              2. Considérant qu'en relevant que FRANCEAGRIMER n'avait produit aucun élément afin d'établir que le montant total des subventions accordées aux douze EARL était supérieur au montant de la subvention dont elles auraient pu bénéficier au titre de leur projet unique commun alors que, dans le mémoire en défense qu'il a produit devant la cour administrative d'appel, l'organisme national d'intervention, d'une part, se référait au plafond de dépenses ouvrant droit à subvention par projet de serres fixé à 4 millions de francs au point 2.1 de la circulaire mentionnée ci-dessus du directeur de l'ONIFLHOR du 9 août 1995 et que, d'autre part, il soutenait sans être contesté que les douze EARL avaient perçu des subventions d'un montant cumulé de 13 695 622 francs, la cour administrative d'appel a dénaturé les pièces du dossier qui lui était soumis et insuffisamment motivé son arrêt ; que, dès lors, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, FRANCEAGRIMER est fondé, pour ces motifs, à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'EARL Les Saules la somme de 750 euros à verser à FRANCEAGRIMER au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de FRANCEAGRIMER, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 23 juin 2011 est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Marseille.<br/>
Article 3 : L'EARL Les Saules versera une somme de 750 euros à FRANCEAGRIMER au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de l'EARL Les Saules présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à l'établissement national des produits de l'agriculture et de la mer et à l'EARL Les Saules.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
