<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028105156</ID>
<ANCIEN_ID>JG_L_2013_10_000000367107</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/10/51/CETATEXT000028105156.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 21/10/2013, 367107</TITRE>
<DATE_DEC>2013-10-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367107</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:367107.20131021</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 1201748 du 5 mars 2013, enregistrée le 25 mars 2013 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président du tribunal administratif d'Orléans a transmis au Conseil d'Etat la requête de M. A...B..., en application de l'article R. 341-3 du code de justice administrative ;<br/>
<br/>
              Vu la requête, enregistrée le 16 mai 2012 au greffe du tribunal administratif d'Orléans, présentée par M. A...B..., demeurant..., par laquelle il demande, d'une part, l'annulation de la décision du 2 mars 2012, transmise par lettre du 8 mars 2012, de la chambre d'appel de la Fédération française de basket-ball lui infligeant une suspension de ses fonctions d'entraîneur de l'équipe de Blois pour le week-end sportif du 30 mars au 1er avril 2012, d'autre part de mettre à la charge de la Fédération française de basket-ball le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, et notamment son Préambule ;<br/>
<br/>
              Vu le code du sport ;<br/>
<br/>
              Vu les règlements généraux de la Fédération française de basket-ball ;<br/>
<br/>
              Vu le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, Conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, Rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier que M.B..., entraîneur de l'équipe de basket-ball du club de l'ADA Blois, a été sanctionné de trois " fautes techniques ", prononcées par les arbitres lors de rencontres disputées par son équipe les 11 octobre, 12 novembre et 6 décembre 2011 ; qu'en application des dispositions du a) du paragraphe 3 de l'article 613 des règlements généraux de la Fédération française de basket-ball, la chambre d'appel de la fédération, par une décision adoptée le 2 mars 2012, a suspendu M. B... pour le week-end sportif du 30 mars au 1er avril 2012 ; qu'après que l'intéressé a été sanctionné d'une quatrième " faute technique " lors d'une rencontre disputée le 21 avril 2012, la chambre d'appel de la fédération, par une nouvelle décision prise lors de sa séance du 5 octobre 2012, l'a suspendu, par application du b) du paragraphe 3 de l'article 613, pour les deux week-ends sportifs des 9 et 11 novembre 2012 et 16 au 18 novembre 2012 ;<br/>
<br/>
              2.	Considérant que M. B...a successivement présenté, devant le tribunal administratif d'Orléans, des conclusions tendant à l'annulation pour excès de pouvoir de la décision de la chambre d'appel de la fédération du 2 mars 2012, des conclusions tendant à l'annulation des dispositions du paragraphe 3 de l'article 613 des règlements généraux de la fédération et des conclusions dirigées contre la seconde décision de la chambre d'appel du 5 octobre 2012 ; que le président du tribunal administratif d'Orléans a renvoyé l'ensemble de ces conclusions au Conseil d'Etat ; <br/>
<br/>
              3.	Considérant que le paragraphe 3 de l'article 613 des règlements généraux de la Fédération française de basket-ball, inséré dans le titre VI consacré aux pénalités, sanctions et voies de recours, prévoit que : " a) Une suspension ferme de toute fonction d'un week-end sportif est prononcée à l'encontre de tout licencié qui aura été sanctionné de trois fautes techniques et/ou disqualifiantes sans rapport au cours de la même saison sportive et dans quelque compétition que ce soit. Le week-end sportif de suspension ferme est fixé par l'organisme disciplinaire compétent (...) et qui enregistre la 3ème faute technique et/ou disqualifiante. La suspension est planifiée de telle manière qu'elle comprenne une rencontre de la compétition du plus haut niveau au titre de laquelle le licencié a été sanctionné. (...) / b) Une suspension ferme de toutes fonctions de deux week-ends sportifs est prononcée à l'encontre de tout licencié qui aura été sanctionné d'une 4ème faute technique et/ou disqualifiante sans rapport, dans les conditions ci-dessus précisées (...) " ; <br/>
<br/>
              4.	Considérant qu'aux termes de l'article 8 de la Déclaration des droits de l'homme et du citoyen de 1789 : " La loi ne doit établir que des peines strictement et évidemment nécessaires, et nul ne peut être puni qu'en vertu d'une loi établie et promulguée antérieurement au délit, et légalement appliquée " ; que le principe d'individualisation des peines qui découle de cet article, s'il ne saurait interdire de fixer des règles assurant une répression effective des infractions, implique qu'une sanction administrative ayant le caractère d'une punition ne puisse être appliquée que si l'autorité compétente la prononce expressément en tenant compte des circonstances propres à chaque espèce ;<br/>
<br/>
              5.	Considérant que la suspension d'un licencié prévue par l'article 613 des règlements généraux de la Fédération française de basket-ball constitue une sanction ayant le caractère d'une punition au sens de l'article 8 de la Déclaration de 1789 ; que les dispositions du a) et du b) du paragraphe 3 de l'article 613 confèrent un caractère automatique à la suspension pour un ou deux week-ends sportifs pour tous les licenciés qui ont été sanctionnés de trois ou quatre fautes techniques au cours d'une même saison sportive, sans habiliter l'organe disciplinaire compétent à statuer sur l'imputabilité effective des fautes techniques ni lui permettre de tenir compte des circonstances propres à chaque espèce ; que, dans ces conditions, M. B...est fondé à soutenir que les dispositions du a) et du b) du paragraphe 3 de l'article 613 des règlements généraux de la fédération méconnaissent le principe d'individualisation des peines qui découle de l'article 8 de la Déclaration des droits de l'homme et du citoyen et sont, par suite, entachées d'excès de pouvoir ; que, par voie de conséquence, M. B... est fondé à demander l'annulation des décisions de la chambre d'appel de la Fédération  française de basket-ball  en date du 2 mars et du 5 octobre 2012 ;<br/>
<br/>
              6.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la Fédération française de basket-ball la somme de 2 000 euros à verser à M. B..., au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions  font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. B... qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Les dispositions des a) et b) du paragraphe 3 de l'article 613 des règlements généraux de la Fédération française de basket-ball et les décisions de la chambre d'appel de la Fédération française de basket-ball en date du 2 mars et du 5 octobre 2012 sont annulées.<br/>
<br/>
Article 2 : La Fédération française de basket-ball versera à M. B... une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : Les conclusions de la Fédération française de basket-ball tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et à la Fédération française de basket-ball. Copie en sera adressée à la ministre des sports, de la jeunesse, de l'éducation populaire et de la vie associative.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-005 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. CONSTITUTION ET PRINCIPES DE VALEUR CONSTITUTIONNELLE. - PRINCIPE D'INDIVIDUALISATION DES PEINES (ART. 8 DE LA DDHC) - EXERCICE DU POUVOIR DISCIPLINAIRE PAR LES FÉDÉRATIONS SPORTIVES - SUSPENSION DE TOUS LES LICENCIÉS SANCTIONNÉS POUR UN NOMBRE DONNÉ DE FAUTES TECHNIQUES - SANCTION AYANT LE CARACTÈRE D'UNE PUNITION - EXISTENCE - POSSIBILITÉ POUR L'ORGANE DISCIPLINAIRE DE STATUER SUR L'IMPUTABILITÉ EFFECTIVE DES FAUTES ET DE TENIR COMPTE DES CIRCONSTANCES DE L'ESPÈCE - ABSENCE - CONSÉQUENCE - CARACTÈRE AUTOMATIQUE MÉCONNAISSANT L'ARTICLE 8 DE LA DDHC - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">63-05-01-02 SPORTS ET JEUX. SPORTS. FÉDÉRATIONS SPORTIVES. EXERCICE DU POUVOIR DISCIPLINAIRE. - SUSPENSION DE TOUS LES LICENCIÉS SANCTIONNÉS POUR UN NOMBRE DONNÉ DE FAUTES TECHNIQUES - SANCTION AYANT LE CARACTÈRE D'UNE PUNITION - EXISTENCE - POSSIBILITÉ POUR L'ORGANE DISCIPLINAIRE DE STATUER SUR L'IMPUTABILITÉ EFFECTIVE DES FAUTES ET DE TENIR COMPTE DES CIRCONSTANCES DE L'ESPÈCE - ABSENCE - CONSÉQUENCE - CARACTÈRE AUTOMATIQUE MÉCONNAISSANT L'ARTICLE 8 DE LA DDHC - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-04-005 Les dispositions des règlements généraux de la Fédération française de basket-ball qui confèrent un caractère automatique à la suspension pour un ou deux week-ends sportifs de tous les licenciés qui ont été sanctionnés de trois ou quatre fautes techniques au cours d'une même saison sportive, sans habiliter l'organe disciplinaire compétent à statuer sur l'imputabilité effective des fautes techniques ni lui permettre de tenir compte des circonstances propres à chaque espèce, méconnaissent le principe d'individualisation des peines qui découle de l'article 8 de la Déclaration des droits de l'homme et du citoyen (DDHC).</ANA>
<ANA ID="9B"> 63-05-01-02 Les dispositions des règlements généraux de la Fédération française de basket-ball qui confèrent un caractère automatique à la suspension pour un ou deux week-ends sportifs de tous les licenciés qui ont été sanctionnés de trois ou quatre fautes techniques au cours d'une même saison sportive, sans habiliter l'organe disciplinaire compétent à statuer sur l'imputabilité effective des fautes techniques ni lui permettre de tenir compte des circonstances propres à chaque espèce, méconnaissent le principe d'individualisation des peines qui découle de l'article 8 de la Déclaration des droits de l'homme et du citoyen (DDHC).</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
