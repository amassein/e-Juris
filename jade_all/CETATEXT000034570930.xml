<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034570930</ID>
<ANCIEN_ID>JG_L_2017_02_000000395433</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/57/09/CETATEXT000034570930.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 10/02/2017, 395433, Publié au recueil Lebon</TITRE>
<DATE_DEC>2017-02-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395433</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLEZ ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:395433.20170210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Paris d'annuler, pour excès de pouvoir, d'une part, la délibération du Conseil de Paris des 22 et 23 avril 2013 portant approbation de la division en volumes de l'Institut des cultures d'Islam sis 56, rue Stephenson et 23, rue Doudeauville à Paris 18ème, de la conclusion d'un bail emphytéotique administratif sur les volumes destinés aux seuls locaux cultuels et de la cession à une association représentant le culte, dans le cadre d'une vente d'immeuble à construire, des constructions à vocation cultuelle devant être réalisées sur le site, ainsi que des caractéristiques juridiques, techniques et financières, essentielles et déterminantes nécessaires à la mise en oeuvre de ces opérations, et portant autorisation de constituer toutes les servitudes nécessaires à la poursuite de l'opération et de participer à toute association syndicale libre dont la ville de Paris sera membre, d'autre part, la conclusion du bail emphytéotique administratif consenti à la société des Habous et des Lieux Saints de l'Islam. Par un jugement n° 1308715/2-1 du 20 mai 2014, le tribunal administratif de Paris a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 14PA03125 du 26 octobre 2015, la cour administrative d'appel de Paris a, sur appel de M.B..., annulé le jugement du 20 mai 2014, la délibération du Conseil de Paris des 22 et 23 avril 2013 ainsi que la décision du maire de conclure le bail emphytéotique administratif. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 21 décembre 2015, 21 mars 2016, 17 et 23 janvier 2017 au secrétariat du contentieux du Conseil d'Etat, la ville de Paris demande au Conseil d'Etat :  <br/>
<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ; <br/>
<br/>
              3°) de mettre à la charge de M. B...une somme de 4 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ; <br/>
              - la loi du 1er juillet 1901 relative au contrat d'association ; <br/>
              - la loi du 9 décembre 1905 concernant la séparation des Eglises et de l'Etat ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la ville de Paris, et à la SCP Boullez, avocat de M.B....<br/>
<br/>
              Vu la note en délibéré, enregistrée le 23 janvier 2017, présentée par la ville de Paris ; <br/>
<br/>
              Vu la note en délibéré, enregistrée le 30 janvier 2017, présentée par M.B.... <br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que, par une délibération des 22 et 23 avril 2013, le Conseil de Paris a approuvé la division en volumes du site de l'Institut des cultures d'Islam (ICI) situé 56, rue Stephenson et 23, rue Doudeauville, dans le 18ème arrondissement, ainsi que la conclusion avec la société des Habous et des Lieux Saints de l'Islam d'un bail emphytéotique administratif sur les volumes destinés à servir d'assiette à des locaux cultuels pour une durée de quatre-vingt-dix-neuf ans moyennant un loyer capitalisé d'un euro, et la cession à cette association, dans le cadre d'une vente d'immeuble à construire, des constructions à vocation cultuelle devant être réalisées par la ville sur le site ; que, par cette même délibération, le Conseil de Paris a approuvé les caractéristiques juridiques, techniques et financières, essentielles et déterminantes, nécessaires à la mise en oeuvre de ces opérations, et a autorisé le maire à signer tous les actes nécessaires à cette mise en oeuvre, notamment à constituer toutes les servitudes nécessaires et à participer à toute association syndicale libre dont la ville de Paris sera membre ; que M. B...a saisi le tribunal administratif de Paris d'une demande tendant à l'annulation pour excès de pouvoir de cette délibération ; qu'il a demandé également l'annulation de la décision du maire de conclure le bail emphytéotique administratif consenti à la société des Habous et des Lieux Saints de l'Islam ; que par un jugement du 20 mai 2014, le tribunal administratif de Paris a rejeté cette demande ; que par un arrêt du 26 octobre 2015 contre lequel la ville de Paris se pourvoit en cassation, la cour administrative d'appel de Paris a, sur appel de M.B..., annulé le jugement du 20 mai 2014 ainsi que la délibération des 22 et 23 avril 2013 et la décision du maire de conclure le bail emphytéotique administratif ;<br/>
<br/>
              2. Considérant, en premier lieu, que par une appréciation souveraine exempte de dénaturation, la cour administrative d'appel de Paris a jugé que M.B..., qui contrairement à ce qui est soutenu, était recevable à justifier de la qualité lui conférant un intérêt pour agir pour la première fois en appel, établissait, à la date d'introduction de sa demande devant le tribunal administratif de Paris tendant à l'annulation de la délibération du Conseil de Paris des 22 et 23 avril 2013 ainsi que de la décision du maire de conclure le bail emphytéotique administratif litigieux, sa qualité de contribuable local ; qu'elle n'a pas, eu égard à la très longue durée de ce bail et au montant modique de la redevance prévue par le contrat, inexactement qualifié les faits en estimant que les décisions en litige emportaient nécessairement des conséquences financières sur le budget municipal ; qu'elle n'a pas davantage insuffisamment motivé son arrêt sur ce point ;  <br/>
<br/>
              3. Considérant, en deuxième lieu, qu'aux termes de l'article 1er de la loi du 9 décembre 1905 concernant la séparation des Eglises et de l'Etat : " La République assure la liberté de conscience. Elle garantit le libre exercice des cultes sous les seules restrictions édictées ci-après dans l'intérêt de l'ordre public " ; que l'article 2 de cette loi dispose : " La République ne reconnaît, ne salarie ni ne subventionne aucun culte. En conséquence, à partir du 1er janvier qui suivra la promulgation de la présente loi, seront supprimées des budgets de l'Etat, des départements et des communes, toutes dépenses relatives à l'exercice des cultes. " ; qu'aux termes de l'article 13 de la même loi : " Les édifices servant à l'exercice public du culte, ainsi que les objets mobiliers les garnissant, seront laissés gratuitement à la disposition des établissements publics du culte, puis des associations appelées à les remplacer auxquelles les biens de ces établissements auront été attribués par application des dispositions du titre II. La cessation de cette jouissance et, s'il y a lieu, son transfert seront prononcés par décret (...). L'Etat, les départements, les communes et les établissements publics de coopération intercommunale pourront engager les dépenses nécessaires pour l'entretien et la conservation des édifices du culte dont la propriété leur est reconnue par la présente loi. " ; qu'enfin, aux termes du dernier alinéa de l'article 19 de cette même loi, les associations formées pour subvenir aux frais, à l'entretien et à l'exercice d'un culte " ne pourront, sous quelque forme que ce soit, recevoir des subventions de l'Etat, des départements et des communes. Ne sont pas considérées comme subventions les sommes allouées pour réparations aux édifices affectés au culte public, qu'ils soient ou non classés monuments historiques. " ; <br/>
<br/>
              4. Considérant, par ailleurs, que l'article L. 451-1 du code rural dispose : " Le bail emphytéotique de biens immeubles confère au preneur un droit réel susceptible d'hypothèque ; ce droit peut être cédé et saisi dans les formes prescrites pour la saisie immobilière. / Ce bail doit être consenti pour plus de dix-huit années et ne peut dépasser quatre-vingt-dix-neuf ans ; il ne peut se prolonger par tacite reconduction. " ; qu'aux termes de l'article L. 1311-2 du code général des collectivités territoriales, dans sa rédaction en vigueur à la date des actes attaqués : " Un bien immobilier appartenant à une collectivité territoriale peut faire l'objet d'un bail emphytéotique prévu à l'article L. 451-1 du code rural, (...) en vue de l'affectation à une association cultuelle d'un édifice du culte ouvert au public (...) " ; <br/>
<br/>
              5. Considérant qu'il résulte des dispositions précitées de la loi du 9 décembre 1905 que les collectivités publiques peuvent seulement financer les dépenses d'entretien et de conservation des édifices servant à l'exercice public d'un culte dont elles sont demeurées ou devenues propriétaires lors de la séparation des Eglises et de l'Etat ou accorder des concours aux associations cultuelles pour des travaux de réparation d'édifices cultuels et qu'il leur est interdit d'apporter une aide à l'exercice d'un culte ; que les collectivités publiques ne peuvent donc, aux termes de ces dispositions, apporter aucune contribution directe ou indirecte à la construction de nouveaux édifices cultuels ;<br/>
<br/>
              6. Considérant, toutefois, que l'article L. 1311-2 du code général des collectivités territoriales a ouvert aux collectivités territoriales la faculté, dans le respect du principe de neutralité à l'égard des cultes et du principe d'égalité, d'autoriser un organisme qui entend construire un édifice du culte ouvert au public à occuper pour une longue durée une dépendance de leur domaine privé ou de leur domaine public, dans le cadre d'un bail emphytéotique, dénommé bail emphytéotique administratif et soumis aux conditions particulières posées par l'article L. 1311-3 du code général des collectivités territoriales ; que le législateur a ainsi permis aux collectivités territoriales de conclure un tel contrat en vue de la construction d'un nouvel édifice cultuel, avec pour contreparties, d'une part, le versement, par l'emphytéote, d'une redevance qui, eu égard à la nature du contrat et au fait que son titulaire n'exerce aucune activité à but lucratif, ne dépasse pas, en principe, un montant modique, d'autre part, l'incorporation dans leur patrimoine, à l'expiration du bail, de l'édifice construit, dont elles n'auront pas supporté les charges de conception, de construction, d'entretien ou de conservation ; qu'il a, ce faisant, dérogé aux dispositions précitées de la loi du 9 décembre 1905 ; que cependant, cette faculté n'est ouverte qu'à la condition que l'affectataire du lieu de culte édifié dans le cadre de ce bail soit, ainsi que l'impliquent les termes mêmes de l'article L. 1311-2 du code général des collectivités territoriales, une association cultuelle, c'est-à-dire une association satisfaisant aux prescriptions du titre IV de la loi du 9 décembre 1905 ; que, dans l'hypothèse où l'affectataire ne serait pas l'emphytéote, un tel bail n'est légal que s'il comporte une clause résolutoire garantissant l'affectation du lieu à une association cultuelle satisfaisant aux prescriptions du titre IV de la loi du 9 décembre 1905 ;<br/>
<br/>
              7. Considérant qu'en jugeant que le bail emphytéotique administratif conclu entre la ville de Paris et la société des Habous et des Lieux Saints de l'Islam sur les volumes de l'Institut des cultures d'Islam destinés à servir d'assiette à des locaux cultuels, dont la société des Habous et des Lieux Saints de l'Islam serait l'affectataire, méconnaissait l'article L. 1311-2 du code général des collectivités territoriales au motif que cette association, régie par les dispositions de la loi du 1er juillet 1901, ne satisfaisait pas aux prescriptions du titre IV de la loi du 9 décembre 1905, la cour administrative d'appel de Paris n'a pas commis d'erreur de droit ; que la circonstance, invoquée par la requérante, que la cour aurait inexactement relevé que la société des Habous et des Lieux Saints de l'Islam n'avait pas pour objet exclusif l'exercice d'un culte est sans incidence sur le bien fondé de l'arrêt dès lors, ainsi qu'il ressort des pièces du dossier soumis au juge du fond, que cette association n'est pas régie par les dispositions du titre IV la loi du 9 décembre 1905 relatives aux associations cultuelles ;<br/>
<br/>
              8. Considérant, en dernier lieu, que si la cour a également relevé que l'aménagement des locaux cultuels serait réalisé sous la maîtrise d'ouvrage de la ville de Paris et non par la société des Habous et des Lieux Saints de l'Islam elle-même, il ressort des énonciations de l'arrêt attaqué que ce motif présente un caractère surabondant ; que, par suite, le moyen d'erreur de droit dirigé contre lui est inopérant et ne peut qu'être écarté ;<br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que, sans préjudice de la possibilité pour les parties de régulariser le bail en y insérant une clause résolutoire garantissant l'affectation du lieu à une association cultuelle satisfaisant aux prescriptions du titre IV de la loi du 9 décembre 1905 afin de répondre aux exigences de l'article L. 1311-2 du code général des collectivités territoriales, la ville de Paris n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; que ses conclusions, y compris celles présentées sur le fondement de l'article L. 761-1 du code de justice administrative, doivent être rejetées ; <br/>
<br/>
              10. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la ville de Paris une somme de 3 000 euros à verser à M. B...en application de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la ville de Paris est rejeté. <br/>
Article 2 : La ville de Paris versera à M. B...une somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la ville de Paris et à M. A...B.... <br/>
Copie en sera adressée au ministre de l'intérieur et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-01 COLLECTIVITÉS TERRITORIALES. DISPOSITIONS GÉNÉRALES. - CONTRATS CONCLUS PAR LES COLLECTIVITÉS TERRITORIALES - BAUX EMPHYTÉOTIQUES ADMINISTRATIFS CONCLUS EN VUE DE LA CONSTRUCTION D'UN ÉDIFICE CULTUEL (ART. L. 1311-2 DU CGCT) - CONDITION - AFFECTATAIRE AYANT LE STATUT D'ASSOCIATION CULTUELLE AU SENS DE LA LOI DU 2 DÉCEMBRE 1905 [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">21-01-02 CULTES. EXERCICE DES CULTES. STATUT DES ÉDIFICES CULTUELS. - BAUX EMPHYTÉOTIQUES ADMINISTRATIFS CONCLUS EN VUE DE LA CONSTRUCTION D'UN ÉDIFICE CULTUEL (ART. L. 1311-2 DU CGCT) - CONDITION - AFFECTATAIRE AYANT LE STATUT D'ASSOCIATION CULTUELLE AU SENS DE LA LOI DU 2 DÉCEMBRE 1905 [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">39-01-03 MARCHÉS ET CONTRATS ADMINISTRATIFS. NOTION DE CONTRAT ADMINISTRATIF. DIVERSES SORTES DE CONTRATS. - BAUX EMPHYTÉOTIQUES ADMINISTRATIFS CONCLUS EN VUE DE LA CONSTRUCTION D'UN ÉDIFICE CULTUEL (ART. L. 1311-2 DU CGCT) - CONDITION - AFFECTATAIRE AYANT LE STATUT D'ASSOCIATION CULTUELLE AU SENS DE LA LOI DU 2 DÉCEMBRE 1905 [RJ1].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-01-04-02-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. INTÉRÊT POUR AGIR. EXISTENCE D'UN INTÉRÊT. INTÉRÊT LIÉ À UNE QUALITÉ PARTICULIÈRE. - INTÉRÊT POUR AGIR DU CONTRIBUABLE LOCAL - DÉLIBÉRATION AUTORISANT LA CONCLUSION D'UN BAIL EMPHYTÉOTIQUE ADMINISTRATIF (ART. L. 1311-2 DU CGCT) - EXISTENCE EN L'ESPÈCE.
</SCT>
<ANA ID="9A"> 135-01 La faculté ouverte par l'article L. 1311-2 du code général des collectivités territoriales (CGCT) de conclure un bail emphytéotique administratif en vue de la construction d'un édifice cultuel n'est ouverte qu'à la condition que l'affectataire du lieu de culte édifié dans le cadre de ce bail soit, ainsi que l'impliquent les termes mêmes de l'article L. 1311-2 du CGCT, une association cultuelle, c'est-à-dire une association satisfaisant aux prescriptions du titre IV de la loi du 9 décembre 1905. Dans l'hypothèse où l'affectataire ne serait pas l'emphytéote, un tel bail n'est légal que s'il comporte une clause résolutoire garantissant l'affectation du lieu à une association cultuelle satisfaisant aux prescriptions du titre IV de la loi du 9 décembre 1905.</ANA>
<ANA ID="9B"> 21-01-02 La faculté ouverte par l'article L. 1311-2 du code général des collectivités territoriales (CGCT) de conclure un bail emphytéotique administratif en vue de la construction d'un édifice cultuel n'est ouverte qu'à la condition que l'affectataire du lieu de culte édifié dans le cadre de ce bail soit, ainsi que l'impliquent les termes mêmes de l'article L. 1311-2 du CGCT, une association cultuelle, c'est-à-dire une association satisfaisant aux prescriptions du titre IV de la loi du 9 décembre 1905. Dans l'hypothèse où l'affectataire ne serait pas l'emphytéote, un tel bail n'est légal que s'il comporte une clause résolutoire garantissant l'affectation du lieu à une association cultuelle satisfaisant aux prescriptions du titre IV de la loi du 9 décembre 1905.</ANA>
<ANA ID="9C"> 39-01-03 La faculté ouverte par l'article L. 1311-2 du code général des collectivités territoriales (CGCT) de conclure un bail emphytéotique administratif en vue de la construction d'un édifice cultuel n'est ouverte qu'à la condition que l'affectataire du lieu de culte édifié dans le cadre de ce bail soit, ainsi que l'impliquent les termes mêmes de l'article L. 1311-2 du CGCT, une association cultuelle, c'est-à-dire une association satisfaisant aux prescriptions du titre IV de la loi du 9 décembre 1905. Dans l'hypothèse où l'affectataire ne serait pas l'emphytéote, un tel bail n'est légal que s'il comporte une clause résolutoire garantissant l'affectation du lieu à une association cultuelle satisfaisant aux prescriptions du titre IV de la loi du 9 décembre 1905.</ANA>
<ANA ID="9D"> 54-01-04-02-01 Un contribuable local a intérêt à contester la délibération de sa commune autorisant la conclusion d'un bail emphytéotique administratif eu égard, en l'espèce, aux conséquences financières de cette décision sur le budget municipal du fait de la très longue durée du bail et du montant modique de la redevance prévue par le contrat.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur le fait que le dispositif déroge à l'interdiction de subventionnement édictée par la loi du 9 décembre 1905, CE, Assemblée, 19 juillet 2011, Mme Vayssière, n° 320796, p. 395.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
