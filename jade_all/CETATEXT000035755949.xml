<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035755949</ID>
<ANCIEN_ID>JG_L_2017_10_000000396801</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/75/59/CETATEXT000035755949.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 09/10/2017, 396801</TITRE>
<DATE_DEC>2017-10-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396801</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Cyrille Beaufils</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:396801.20171009</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              1° Sous le n° 396801, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 5 février et 4 mai 2016 et le 28 avril 2017 au secrétariat du contentieux du Conseil d'Etat, l'association des exploitants de la plage de Pampelonne demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2015-1675 du 15 décembre 2015 portant approbation du schéma d'aménagement de la plage de Ramatuelle ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 403779, par une ordonnance nos 1402555, 1504009 du 22 septembre 2016, enregistrée le 26 septembre 2016 au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de Toulon a transmis au Conseil d'Etat, en application des articles R. 341-1 et R. 341-2 du code de justice administrative, la requête de l'association des exploitants de la plage de Pampelonne, enregistrée le 3 juillet 2014 au greffe de ce tribunal.<br/>
<br/>
              Par cette requête et deux mémoires en réplique, enregistrés les 3 mai et 30 août 2017 au secrétariat du contentieux du Conseil d'Etat, l'association des exploitants de la plage de Pampelonne demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la délibération n° 120/15 du 22 septembre 2015 par laquelle le conseil municipal de la commune de Ramatuelle a décidé de modifier, en vue de son approbation, le projet de schéma d'aménagement de la plage de Pampelonne, de charger le maire d'effectuer les formalités administratives nécessaires à son exécution et de procéder en tant que de besoin aux ajustements formels nécessaires ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Ramatuelle une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              3° Sous le n° 403791, par une ordonnance nos 1402555, 1504009 du 22 septembre 2016, enregistrée le 26 septembre 2016 au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de Toulon a transmis au Conseil d'Etat, en application des articles R. 341-1 et R. 341-2 du code de justice administrative, la requête de l'association des exploitants de la plage de Pampelonne, enregistrée le 23 novembre 2015 au greffe de ce tribunal.<br/>
<br/>
              Par cette requête et un mémoire en réplique, enregistré le 30 août 2017 au secrétariat du contentieux du Conseil d'Etat, l'association des exploitants de la plage de Pampelonne demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la délibération n° 1/14 du 30 janvier 2014 par laquelle le conseil municipal de la commune de Ramatuelle a décidé d'arrêter le projet de schéma d'aménagement de la plage de Pampelonne, de charger le maire d'effectuer les formalités administratives nécessaires à son exécution et de procéder en tant que de besoin aux ajustements formels nécessaires ainsi que la décision du maire de Ramatuelle du 6 mai 2014 rejetant son recours gracieux contre cette délibération ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Ramatuelle une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son premier protocole additionnel ;<br/>
              - le code de l'environnement ;<br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Cyrille Beaufils, auditeur,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de l'association des exploitants de la plage de Pampelonne et à la SCP Didier, Pinet, avocat de la commune de Ramatuelle ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 21 septembre 2017 sous le n° 396801, présentée par l'association des exploitants de la plage de Pampelonne ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces des dossiers que, par une délibération du 5 décembre 2012, le conseil municipal de Ramatuelle (Var) a arrêté un projet de schéma d'aménagement de la plage de Pampelonne ; que, après avis de l'autorité environnementale et de la commission départementale de la nature, des paysages et des sites, ce projet a été soumis à enquête publique entre le 17 juin et le 19 juillet 2013 ; que le conseil municipal de cette commune a adopté le projet modifié par une délibération du 30 janvier 2014 ; que ce projet a encore été modifié par une délibération du 22 septembre 2015 à la suite d'une première saisine du Conseil d'Etat par le Gouvernement du projet de décret approuvant ce schéma d'aménagement ; que, après une nouvelle saisine du Conseil d'Etat, le projet de schéma d'aménagement de la plage de Pampelonne résultant de ces délibérations a été approuvé par le décret du 15 décembre 2015 ; que, par une requête formée directement devant le Conseil d'Etat sous le n° 396801, et sous les nos 403791 et 403779, par des requêtes renvoyées au Conseil d'Etat par le président du tribunal administratif de Toulon, l'association des exploitants de la plage de Pampelonne demande respectivement l'annulation pour excès de pouvoir du décret du 15 décembre 2015 approuvant le schéma d'aménagement de la plage de Pampelonne à Ramatuelle (Var) et des délibérations du 30 janvier 2014 et du 22 septembre 2015 par lesquelles le conseil municipal de Ramatuelle a adopté et modifié le projet de schéma d'aménagement en cause ; que ces requêtes présentant à juger des questions semblables, il y a lieu de les joindre pour statuer par une même décision ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 146-6-1 du code de l'urbanisme, en vigueur à la date des délibérations et du décret attaqués, et désormais repris aux articles L. 121-28 à L. 121-30 du même code : " Afin de réduire les conséquences sur une plage et les espaces naturels qui lui sont proches de nuisances ou de dégradations sur ces espaces, liées à la présence d'équipements ou de constructions réalisés avant l'entrée en vigueur de la loi n° 86-2 du 3 janvier 1986 précitée, une commune ou, le cas échéant, un établissement public de coopération intercommunale compétent peut établir un schéma d'aménagement. / Ce schéma est approuvé, après enquête publique réalisée conformément au chapitre III du titre II du livre Ier du code de l'environnement, par décret en Conseil d'Etat, après avis de la commission départementale compétente en matière de nature, de paysages et de sites. / Afin de réduire les nuisances ou dégradations mentionnées au premier alinéa et d'améliorer les conditions d'accès au domaine public maritime, il peut, à titre dérogatoire, autoriser le maintien ou la reconstruction d'une partie des équipements ou constructions existants à l'intérieur de la bande des cent mètres définie par le III de l'article L. 146-4, dès lors que ceux-ci sont de nature à permettre de concilier les objectifs de préservation de l'environnement et d'organisation de la fréquentation touristique. / Les conditions d'application du présent article sont déterminées par décret en Conseil d'Etat " ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article R. 146-3 du code de l'urbanisme, alors applicable et désormais repris à l'article R. 121-7 du même code : " Le schéma d'aménagement mentionné à l'article L. 146-6-1 du code de l'urbanisme : / 1° Comporte, pour le territoire qu'il délimite, une analyse de l'état initial du site, portant notamment sur les paysages, les milieux naturels, les conditions d'accès au domaine public maritime et les équipements et constructions réalisés avant l'entrée en vigueur de la loi n° 86-2 du 3 janvier 1986 relative à l'aménagement, la protection et la mise en valeur du littoral ; / 2° Définit les conditions d'aménagement des plages et des espaces naturels qui leur sont proches ainsi que les modalités de desserte et de stationnement des véhicules. Il fixe les mesures permettant d'améliorer l'accès au domaine public maritime, de réduire les dégradations constatées et d'atténuer les nuisances ; / 3° Justifie les partis d'aménagement retenus et évalue leur incidence sur l'environnement, au regard des objectifs définis à l'article L. 146-6-1 ; / 4° Détermine, dans la bande des 100 mètres mentionnée au III de l'article L. 146-4, les équipements ou constructions dont le maintien ou la reconstruction peuvent être autorisés par dérogation aux articles L. 146-1 à L. 146-6, ainsi que leur implantation. Il indique ceux qui doivent être démolis et fixe les conditions de la remise en état du site. / Le schéma d'aménagement définit dans un chapitre distinct les prescriptions qui pourront être imposées aux bénéficiaires des autorisations prévues à l'alinéa précédent afin que ces équipements et constructions ne dénaturent pas le caractère du site et ne compromettent pas la préservation des paysages et des milieux naturels " ; qu'aux termes de l'article R. 146-4 du même code, alors applicable et désormais repris à l'article R. 121-8 de ce code : " Le projet de schéma est arrêté, selon le cas, par le conseil municipal ou l'organe délibérant de l'établissement public de coopération intercommunale compétent en matière de plan local d'urbanisme. / Le projet de schéma, auquel est joint l'avis de la commission départementale compétente en matière de nature, de paysages et de sites, est soumis à l'enquête publique par le préfet dans les conditions prévues par les articles R. 123-1 et suivants du code de l'environnement. / Le décret en Conseil d'Etat approuvant le schéma fait l'objet des mesures de publicité définies à l'article R. 123-25 du même code. / Le schéma approuvé est annexé au plan local d'urbanisme, s'il existe " ; <br/>
<br/>
              4. Considérant qu'aux termes des dispositions de l'article R. 2124-16 du code général de la propriété des personnes publiques, applicables aux concessions accordées sur les plages : " Un minimum de 80 % de la longueur du rivage, par plage, et de 80 % de la surface de la plage, dans les limites communales, doit rester libre de tout équipement et installation " ;<br/>
<br/>
              Sur les délibérations du conseil municipal arrêtant le projet de schéma d'aménagement de plage :<br/>
<br/>
              5. Considérant que les délibérations attaquées du conseil municipal de Ramatuelle ont eu pour seul objet, en application des dispositions citées ci-dessus des articles L. 146-6-1, R. 146-3 et R. 146-4 du code de l'urbanisme, d'arrêter le projet du schéma d'aménagement de la plage de Pampelonne soumis à une approbation par décret en Conseil d'Etat, sans emporter par elles-mêmes d'autre effet juridique que de permettre cette approbation, qui seule fait l'objet des mesures de publicité auxquelles sont subordonnés les effets juridiques du schéma d'aménagement de plage ; qu'elles revêtent, dès lors, le caractère de mesures préparatoires, insusceptibles de faire l'objet d'un recours pour excès de pouvoir ; que la commune de Ramatuelle est, dès lors, fondée à demander le rejet comme irrecevables des requêtes de l'association requérante tendant à leur annulation ; <br/>
<br/>
              Sur le décret du 15 décembre 2015 portant approbation du schéma d'aménagement de la plage de Pampelonne : <br/>
<br/>
              En ce qui concerne l'enquête publique et les modifications apportées à l'issue de l'enquête publique :<br/>
<br/>
              6. Considérant qu'aux termes du premier alinéa du II de l'article L. 123-10 du code de l'environnement, dans sa version alors en vigueur, applicable à l'enquête publique conduite préalablement à l'élaboration d'un schéma d'aménagement de plage conformément aux articles L. 146-6-1 et R. 146-4 du code de l'urbanisme : " L'information du public est assurée par tous moyens appropriés, selon l'importance et la nature du projet, plan ou programme, notamment par voie d'affichage sur les lieux concernés par l'enquête, par voie de publication locale ou par voie électronique " ; <br/>
<br/>
              7. Considérant qu'il ressort des pièces du dossier que l'avis d'enquête a été affiché sur le lieu de réalisation du projet et que le site internet de la mairie de Ramatuelle en a assuré la publicité ; qu'eu égard à ces modalités ainsi qu'à la participation du public constatée lors de l'enquête, l'association requérante n'est pas fondée à soutenir que la publicité de l'avis d'enquête publique aurait été insuffisante au regard des dispositions précitées de l'article L. 123-10 du code de l'environnement et de nature à entacher la régularité de la procédure ;<br/>
<br/>
              8. Considérant que, si l'association requérante critique l'insuffisance de l'évaluation environnementale jointe au dossier d'enquête publique, il ne ressort pas des pièces du dossier que celle-ci ait présenté les insuffisances alléguées ; <br/>
<br/>
              9. Considérant que l'association requérante soutient que la commune de Ramatuelle a remis aux commissaires enquêteurs pendant le déroulement de l'enquête publique des documents dont le versement tardif au dossier a été de nature à entacher la régularité de la procédure ; que les documents ainsi remis, qui sont, outre divers courriers et communications reçus par la commune, une étude socio-économique remise par la commune au troisième jour de l'enquête et des documents du groupe de travail " Natura 2000 ", ne sont toutefois pas au nombre des ceux qui doivent figurer au dossier en application des dispositions de l'article  R. 123-8 du code de l'environnement ; que, en tout état de cause, la communication de ces documents au cours de l'enquête publique n'a pas été de nature à nuire à l'information de l'ensemble des personnes intéressées par l'opération ni à exercer une influence sur les résultats de l'enquête et, par suite, sur la décision de l'autorité administrative ;<br/>
<br/>
              10. Considérant qu'aucune disposition applicable n'imposait de faire figurer au dossier soumis à enquête une estimation du coût des travaux de mise en oeuvre du schéma d'aménagement plus détaillée que l'estimation, de 11 millions d'euros toutes dépenses confondues, qui y figurait ; que la circonstance que la commission d'enquête a subordonné son avis favorable à ce que soit élaborés " une méthodologie et un phasage stricts et clairs d'application du schéma y compris sur le plan financier " est sans influence sur la régularité de la procédure, l'autorité compétente n'étant pas tenue de faire suite aux réserves de la commission d'enquête, alors même que l'auteur du schéma d'aménagement a, au demeurant, précisé les estimations relatives au coût de l'opération dans le rapport de présentation ;<br/>
<br/>
              11. Considérant que l'association requérante soutient, enfin, que les modifications substantielles apportées postérieurement à l'enquête publique au projet de schéma d'aménagement imposaient qu'il fût procédé à une nouvelle enquête publique ; que, cependant, il ne ressort pas des pièces du dossier que les modifications apportées au projet, notamment par l'adoption d'une photographie aérienne plus récente comme fond de carte du schéma d'aménagement, l'augmentation de 6007 à 6095 mètres carrés de la surface des lots susceptibles de faire l'objet de sous-conventions en zone Zp, l'ajout de précisions relatives aux mouillages des bateaux et, afin de tenir compte de l'avis de la commission d'enquête, au financement des travaux, l'ajout d'un tableau de synthèses des surfaces susceptibles d'être bâties sur le domaine public et d'un plan relatif au secteur de Bonne terrasse, ainsi que l'inclusion, dans le schéma d'aménagement, des parcs de stationnement des secteurs de l'Epi et des campings, aient constitué des modifications substantielles qui auraient porté atteinte à l'économie générale du projet et nécessité une nouvelle enquête publique ;<br/>
<br/>
              En ce qui concerne les autres moyens :<br/>
<br/>
              12. Considérant que le moyen tiré de ce que la délibération du 22 septembre 2015, par laquelle le conseil municipal de Ramatuelle a adopté le schéma d'aménagement approuvé par le décret attaqué, a été prise au terme d'une procédure irrégulière n'est pas assorti des précisions permettant d'en apprécier le bien fondé ;<br/>
<br/>
              13. Considérant que le schéma d'aménagement de plage approuvé par le décret attaqué comprend l'ensemble des informations et documents propres à assurer sa régularité, en particulier, en ce qui concerne la pertinence du périmètre retenu, l'analyse de l'état du site, notamment l'évolution du trait de côte, les dégradations auxquelles il entend remédier, l'impact des équipements et constructions existants et la justification des partis d'aménagement retenus, notamment la destruction des constructions et équipements installés sur le domaine public maritime ;<br/>
<br/>
              14. Considérant que le schéma de cohérence territoriale des cantons de Grimaud et Saint-Tropez, approuvé le 12 juillet 2006 et modifié le 22 décembre 2006, fait figurer la plage de Pampelonne au nombre des " espaces à enjeux de développement durable " ; que ce document indique, dans son orientation 1.2, que : " L'article L. 146-6-1  permet d'envisager le maintien et la réhabilitation des installations réalisées avant l'entrée en vigueur de la loi littoral, dans le but de concilier la préservation de l'environnement et l'accueil du public " en précisant que " cet objectif sera atteint dans le cadre d'un schéma d'aménagement de site " ; qu'il se borne ainsi à renvoyer aux choix à arrêter dans le cadre du schéma d'aménagement de plage, sans imposer le maintien ou la réhabilitation des installations existantes, contrairement à ce qui est soutenu ; que le moyen tiré de ce que le schéma d'aménagement de plage ne serait ainsi pas compatible avec les prescriptions du schéma de cohérence territoriale du golfe de Saint-Tropez doit, dès lors, être écarté ; <br/>
<br/>
              15. Considérant que, si les dispositions citées ci-dessus de l'article R. 146-3 du code de l'urbanisme imposent à l'auteur d'un schéma d'aménagement de justifier les partis d'aménagement retenus et d'évaluer leur incidence sur l'environnement, au regard des objectifs définis à l'article L. 146-6-1 du même code, elles ne subordonnent pas la légalité d'un tel schéma à la démonstration des nuisances et dégradations qui sont liées à la présence d'équipements ou de constructions antérieures à la loi du 3 janvier 1986 ni du lien de ces nuisances et dégradations avec ces équipements ou constructions ; que le moyen tiré de l'absence de telles démonstrations, tant pour le schéma d'aménagement pris dans son ensemble que pour chaque équipement ou construction pris individuellement, doit, dès lors, être écarté ; que le moyen tiré de ce que le schéma d'aménagement serait contraire aux dispositions de l'article L. 146-6-1 en ce qu'il ne démontrerait pas la contribution qu'il apporterait à la réduction des nuisances ou des dégradations constatées doit également, compte tenu tant des informations produites que des orientations retenues, être, en tout état de cause, écarté ;<br/>
<br/>
              16. Considérant que, contrairement à ce que soutient l'association requérante, les dispositions des articles L. 146-6-1 et de l'article R. 146-3 du code de l'urbanisme n'interdisent pas au schéma d'aménagement de prévoir la réalisation d'équipements ou de constructions au-delà de la bande des cent mètres définies par le III de l'article L. 146-4 du même code ; qu'il ne ressort pas des pièces du dossier que, compte tenu de leur emplacement et de leur surface ainsi que des caractéristiques des constructions qui seront autorisées, la délimitation des zones d'implantation des équipements et installations retenue par le schéma d'aménagement soit entachée d'une erreur manifeste d'appréciation ;<br/>
<br/>
              17. Considérant que, si les dispositions de l'article L. 146-6-1 du code de l'urbanisme permettent au schéma d'aménagement d'autoriser à titre dérogatoire le maintien ou la reconstruction d'une partie des équipements ou constructions existants à l'intérieur de la bande des cent mètres, il résulte de leurs termes mêmes qu'elles n'emportent pas d'obligation pour celui-ci d'autoriser le maintien ou la reconstruction de l'intégralité de la surface de ces équipements ou constructions ni ne font obstacle à ce qu'il fixe les surfaces maximales de ceux qu'il autorise ; que l'association requérante n'est pas fondée à soutenir que les dispositions citées ci-dessus de l'article R. 2124-16 du code général de la propriété des personnes publiques, selon lesquelles 80 % de la surface des constructions d'une plage concédée doit rester libre de tout équipement ou installation, seraient les seules dispositions permettant de prévoir une limitation de la surface des constructions autorisées sur une plage ;<br/>
<br/>
              18. Considérant, enfin, qu'aux termes de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " Toute personne physique ou morale a droit au respect de ses biens. Nul ne peut être privé de sa propriété que pour cause d'utilité publique et dans les conditions prévues par la loi et les principes généraux du droit international. Les dispositions précédentes ne portent pas atteinte au droit que possèdent les Etats de mettre en vigueur les lois qu'ils jugent nécessaires pour réglementer l'usage des biens conformément à l'intérêt général " ; qu'il résulte de ces stipulations qu'une ingérence dans le droit de propriété doit respecter le principe de légalité, poursuivre un but légitime et ménager un juste équilibre entre les exigences de l'intérêt général et la sauvegarde des droits individuels ;<br/>
<br/>
              19. Considérant que le schéma d'aménagement contesté prévoit notamment la démolition de constructions édifiées sur le domaine public maritime afin d'assurer la protection du rivage de la mer et la conciliation, recherchée par le législateur, entre les objectifs de préservation de l'environnement et d'organisation de la fréquentation touristique, répondant ainsi à un intérêt public légitime ; que l'association requérante soutient, toutefois, qu'elles méconnaîtraient l'espérance légitime des exploitants des plages dont elles prévoient la démolition ou la limitation des installations à se voir reconnaître la jouissance effective d'un droit de propriété ; que cependant les principes applicables au domaine public maritime impliquent, comme les exploitants ne pouvaient l'ignorer, que nul ne peut se prévaloir d'aucun droit réel sur une dépendance du domaine public maritime ; que les exploitants de plage, eu égard à la nature précaire et renouvelable annuellement des autorisations d'occupation domaniale dont ils étaient titulaires et malgré l'ancienneté de situations, pour certaines déjà constituées depuis plusieurs décennies, ne peuvent se voir reconnaître une telle espérance légitime ; qu'à supposer, comme cela est soutenu, que ces exploitants puissent prétendre à une indemnisation en raison de la démolition des installations existantes, il n'appartenait pas au schéma d'aménagement contesté de prévoir cette dernière ; qu'il suit de là que les dispositions contestées, qui s'inscrivent dans une redéfinition des principes d'aménagement qui permettra l'attribution de nouvelles sous-concessions de plages, auxquelles les exploitants de plage concernés par les démolitions pourront se porter candidats, réalisent un juste équilibre entre les exigences de l'intérêt général et la sauvegarde des droits individuels et ne constituent pas une mesure contraire au droit au respect des biens garanti par les stipulations invoqués ;<br/>
<br/>
              20. Considérant qu'il résulte de tout ce qui précède que l'association des exploitants de la plage de Pampelonne n'est pas fondée à demander l'annulation du décret attaqué ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative doivent, par voie de conséquence,  être également rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes de l'association des exploitants de la plage de Pampelonne sont rejetées.<br/>
<br/>
Article 2 : La présente décision sera notifiée à l'association des exploitants de la plage de Pampelonne, à la commune de Ramatuelle et au ministre de la cohésion des territoires.<br/>
      Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-01-02-02 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES NE CONSTITUANT PAS DES DÉCISIONS SUSCEPTIBLES DE RECOURS. MESURES PRÉPARATOIRES. - INCLUSION - DÉLIBÉRATION DU CONSEIL MUNICIPAL ARRÊTANT UN PROJET DE SCHÉMA D'AMÉNAGEMENT DE PLAGE (ART. L. 121-28 À L. 121-30 DU CODE DE L'URBANISME).
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. - SCHÉMA D'AMÉNAGEMENT DE PLAGE (ART. L. 121-28 À L. 121-30 CODE DE L'URBANISME) - 1) DÉLIBÉRATION DU CONSEIL MUNICIPAL ARRÊTANT UN PROJET DE SCHÉMA - MESURE PRÉPARATOIRE - EXISTENCE - 2) OBLIGATION DE COMPATIBILITÉ AVEC LES PRESCRIPTIONS DU SCOT - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-01-006-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. SCHÉMAS DE COHÉRENCE TERRITORIALE. EFFETS. - OBLIGATION DE COMPATIBILITÉ DU SCHÉMA D'AMÉNAGEMENT DE PLAGE (ART. L. 121-28 À L. 121-30 DU CODE DE L'URBANISME) AUX PRESCRIPTIONS DU SCOT - EXISTENCE.
</SCT>
<ANA ID="9A"> 54-01-01-02-02 La délibération d'un conseil municipal prise en application de l'article L. 146-6-1 du code de l'urbanisme, repris aux articles L. 121-28 à L. 121-30 de ce code, et des articles R. 146-3 et R. 146-4 du même code, a pour seul objet d'arrêter le projet de schéma d'aménagement d'une plage soumis à approbation par décret en Conseil d'Etat, sans emporter par elle-même d'autre effet juridique que de permettre cette approbation. Elle revêt, dès lors, le caractère d'une mesure préparatoire insusceptible de faire l'objet d'un recours pour excès de pouvoir.</ANA>
<ANA ID="9B"> 68-01 1) La délibération d'un conseil municipal prise en application de l'article L. 146-6-1 du code de l'urbanisme, repris aux articles L. 121-28 à L. 121-30 de ce code, et des articles R. 146-3 et R. 146-4 du même code, a pour seul objet d'arrêter le projet de schéma d'aménagement d'une plage soumis à approbation par décret en Conseil d'Etat, sans emporter par elle-même d'autre effet juridique que de permettre cette approbation. Elle revêt, dès lors, le caractère d'une mesure préparatoire insusceptible de faire l'objet d'un recours pour excès de pouvoir....  ,,2) Le schéma d'aménagement de plage doit être compatible avec les prescriptions du schéma de cohérence territoriale (SCOT).</ANA>
<ANA ID="9C"> 68-01-006-02 Le schéma d'aménagement de plage régi par l'article L. 146-6-1 du code de l'urbanisme, repris aux articles L. 121-28 à L. 121-30 de ce code, doit être compatible avec les prescriptions du schéma de cohérence territoriale (SCOT).</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
