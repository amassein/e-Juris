<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043771321</ID>
<ANCIEN_ID>JG_L_2021_07_000000453505</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/77/13/CETATEXT000043771321.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 06/07/2021, 453505, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>453505</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BUK LAMENT - ROBILLOT</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:453505.20210706</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire, enregistrés les 11 et 14 juin 2021 au secrétariat du contentieux du Conseil d'Etat, l'association La Quadrature du Net demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution du dispositif intitulé " Passe sanitaire ", consistant en la présentation, numérique ou papier, d'une " preuve sanitaire ", en tant, d'une part, que ce dispositif exige le traitement dans le code en deux dimensions de données relatives à l'état civil et, d'autre part, que ce dispositif permet le traitement dans le code en deux dimensions de données de santé ;<br/>
<br/>
              2°) d'ordonner la suspension de l'exécution de la décision d'inclure dans les passes sanitaires des données relatives à l'état civil, ainsi que des justificatifs de statut vaccinal et des justificatifs de résultat de test virologique, révélée par la délivrance par le ministère des solidarités et de la santé de ces documents ;<br/>
<br/>
              3°) d'ordonner la suspension de l'exécution du décret n° 2021-724 du 7 juin 2021 ; <br/>
<br/>
              4°) d'enjoindre au ministre des solidarités et de la santé de cesser immédiatement, à compter du prononcé de l'ordonnance à intervenir, de délivrer des passes sanitaires qui contiendraient des codes en deux dimensions comportant des informations relatives à l'état civil des personnes ou des données de santé, sous astreinte de 1 024 euros par jour de retard ; <br/>
<br/>
              5°) de mettre à la charge de l'Etat la somme de 4 096 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - la requête est recevable ; <br/>
              - la condition d'urgence est satisfaite dès lors que, en premier lieu, la présence d'informations relatives à l'état civil et à la santé des personnes détentrices des passes sanitaires constitue une ingérence grave et manifestement illégale dans plusieurs libertés fondamentales, notamment le droit à la vie privée et le droit à la protection des données personnelles, en deuxième lieu, le dispositif de génération de passes sanitaires est contraire à la loi du 31 mai 2021 relative à la gestion de la sortie de crise sanitaire et au règlement du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement de données à caractère personnel et à la libre circulation de ces données (RGPD), en troisième lieu, le dispositif est actuellement utilisé pour délivrer des passes sanitaire et, en dernier lieu, en absence d'acte administratif exprès, le référé-liberté constitue la voie idoine de recours ; <br/>
              - il est porté une atteinte grave et manifestement illégale à plusieurs libertés fondamentales ; <br/>
              - le dispositif attaqué méconnaît le droit au respect de la vie privée et le droit à la protection des données personnelles dès lors qu'il constitue un traitement de données personnelles relatives à l'état civil et à l'état de santé des personnes qui peuvent être recoupées avec le lieu, la date et l'heure de lecture illicite du passe sanitaire ;<br/>
              - il méconnaît la liberté d'aller et venir en ce que l'étendue des données divulguées de manière obligatoire ne peut qu'inciter les personnes voulant se protéger contre cette atteinte à limiter leur droit d'aller et venir ; <br/>
              - il méconnaît la liberté de manifester et la liberté d'expression dès lors qu'il autorise la mise en oeuvre d'un traitement de données illégal et permet la délivrance de passes sanitaires contenant des informations, dont des données sensibles, sans que les personnes concernées ne puissent s'y opposer, de telle sorte que cette divulgation forcée de données personnelles rend vulnérable la population à des formes de surveillance et à la constitution de fichiers illicites ; <br/>
              - le dispositif litigieux présente un risque de détournement qui est intrinsèque à l'utilisation des codes à deux dimensions dès lors que des données personnelles qui y sont contenues, notamment des données relatives à l'état de civil et à leur état de santé, sont accessibles à n'importe quelle personne qui scanne ce code en deux dimensions ; <br/>
              - le décret du 7 juin 2021, en ce qu'il autorise la délivrance de passes qui mentionnent l'état civil de la personne concernée, méconnaît les dispositions de la loi du 31 mai 2021 ainsi que les dispositions du RGPD dès lors que, d'une part, la loi ne prévoit pas que l'identité civile puisse figurer dans le code en deux dimensions et, d'autre part, ces données ne sont pas nécessaires puisqu'elles ne permettent pas de vérifier l'authenticité des documents ; <br/>
              - la décision du ministre des solidarités et de la santé méconnaît les dispositions de la loi du 31 mai 2021, de la loi informatique et libertés du 6 janvier 1978, ainsi que les dispositions du RGPD dès lors que, d'une part, le dispositif attaqué intègre des codes en deux dimensions contenant des données manifestement interdites par la loi et manifestement non-nécessaires, en méconnaissance des principes de nécessité et de minimisation des données et, d'autre part, le dispositif prévoit la mise à disposition de tout tiers des données de santé sans qu'il soit nécessaire, pour pouvoir contrôler l'accès à un lieu, de traiter toutes les données actuellement accessibles dans les codes en deux dimensions ; <br/>
              - elle est illégale en ce qu'elle n'a été précédée d'aucune analyse d'impact sur la protection des données et qu'elle n'a donné lieu à aucune consultation préalable de l'autorité de contrôle.<br/>
<br/>
              Par un mémoire en défense, enregistré le 21 juin 2021, le ministre des solidarités et de la santé conclut au rejet de la requête. Il soutient qu'il n'est porté aucune atteinte grave et manifestement illégale aux libertés fondamentales invoquées.<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 22 juin 2021, présenté par la requérante, qui maintient ses conclusions et ses moyens ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son Préambule ; <br/>
              - le règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 ;<br/>
              - la loi n° 78-17 du 6 janvier 1978 ; <br/>
              - la loi n° 2021-689 du 31 mai 2021 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, l'association La Quadrature du Net, et d'autre part, le ministre des solidarités et de la santé ainsi que le Premier ministre et la Commission nationale de l'informatique et des libertés ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 22 juin 2021, à 14h30 : <br/>
<br/>
              - Me Robillot, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'association La Quadrature du Net ;<br/>
<br/>
              - les représentants de l'association La Quadrature du Net ;<br/>
<br/>
              - les représentants du ministre des solidarités et de la santé ; <br/>
<br/>
              à l'issue de laquelle le juge des référés a reporté la clôture de l'instruction au 25 juin 2021 à 15 heures ;<br/>
<br/>
              Vu les notes en délibéré, enregistrées les 25 et 29 juin 2021, présentées par la requérante ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ".<br/>
<br/>
              Sur le cadre juridique :<br/>
<br/>
              2. En raison de l'amélioration progressive de la situation sanitaire, les mesures de santé publique destinées à prévenir la circulation du virus de la Covid-19 prises dans le cadre de l'état d'urgence sanitaire régi par les articles L. 3131-12 et suivants du code de la santé publique ont été remplacées, dans le cadre de la loi du 31 mai 2021 relative à la gestion de la sortie de la crise sanitaire, par celles du décret du 1er juin 2021 modifié notamment par le décret du 7 juin 2021 qui les adapte à la situation sanitaire actuelle.<br/>
<br/>
              3. En vertu du A du II de l'article 1er de la loi du 31 mai 2021 relative à la gestion de la sortie de crise sanitaire, le Premier ministre peut, à compter du 2 juin 2021 et jusqu'au 30 septembre 2021, imposer la présentation du résultat d'un examen de dépistage virologique ne concluant pas à une contamination par la Covid-19, un justificatif de statut vaccinal concernant la Covid-19 ou un certificat de rétablissement à la suite d'une contamination par la Covid-19, d'une part, aux personnes souhaitant se déplacer à destination ou en provenance du territoire hexagonal, de la Corse ou de l'une des collectivités mentionnées à l'article 72-3 de la Constitution et, d'autre part, aux personnes souhaitant accéder à certains lieux, établissements ou événements impliquant de grands rassemblements de personnes pour des activités de loisirs ou des foires ou salons professionnels. Aux termes du B du II du même article : " B. - La présentation du résultat d'un examen de dépistage virologique ne concluant pas à une contamination par la covid-19, d'un justificatif de statut vaccinal concernant la covid-19 ou d'un certificat de rétablissement à la suite d'une contamination par la covid-19 dans les cas prévus au A du présent II peut se faire sur papier ou sous format numérique. / La présentation, sur papier ou sous format numérique, des documents mentionnés au premier alinéa du présent B est réalisée sous une forme ne permettant pas aux personnes habilitées ou aux services autorisés à en assurer le contrôle de connaître la nature du document ni les données qu'il contient. "<br/>
<br/>
              4. Pris en application de cette loi, le décret du 7 juin 2021 modifiant le décret du 1er juin 2021 prévoit les dispositions relatives au passe sanitaire et notamment les règles communes relatives à l'établissement et au contrôle du résultat d'un examen de dépistage virologique ne concluant pas à une contamination par la Covid-19, du justificatif de statut vaccinal concernant la covid-19 et du certificat de rétablissement à la suite d'une contamination par la Covid-19. L'article 2-3 du décret 1er juin 2021 dispose désormais que : " I.- Les justificatifs dont la présentation peut être exigée sont générés : / 1° Pour le résultat de l'examen de dépistage virologique ou le certificat de rétablissement, par le système d'information national de dépistage (" SI-DEP ") mis en oeuvre en application du décret n° 2020-551 du 12 mai 2020 relatif aux systèmes d'information mentionnés à l'article 11 de la loi n° 2020-546 du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ses dispositions ; 2° Pour le justificatif de statut vaccinal, par le traitement automatisé de données à caractère personnel " Vaccin Covid " mis en oeuvre en application du décret n° 2020-1690 du 25 décembre 2020 autorisant la création d'un traitement de données à caractère personnel relatif aux vaccinations contre la covid-19. / (...) Tout justificatif généré conformément au présent I comporte les noms, prénoms, date de naissance de la personne concernée et un code permettant sa vérification dans les conditions prévues au II. / Ces justificatifs peuvent être librement enregistrés par la personne concernée sur l'application mobile " TousAntiCovid ", comportant à cet effet la fonctionnalité " TAC Carnet ", mentionnée à l'article 1er du décret n° 2020-650 du 29 mai 2020 relatif au traitement de données dénommé " TousAntiCovid ", aux fins d'être conservées localement sur son téléphone mobile. / La personne concernée peut supprimer à tout moment les justificatifs enregistrés sur l'application mobile. / II.- Les justificatifs mentionnés au I peuvent être présentés sous format papier ou numérique, enregistré sur l'application mobile " TousAntiCovid " ou tout autre support numérique au choix de la personne concernée. / Sont autorisés à contrôler ces justificatifs, dans les seuls cas prévus au A du II de l'article 1er de la loi du 31 mai 2021 susvisée, et dans la limite de ce qui est nécessaire au contrôle des déplacements et de l'accès aux lieux, établissements ou évènements mentionnés par ce A : / (...). III.- La lecture des justificatifs par les personnes mentionnées au II est réalisée au moyen d'une application mobile dénommée " TousAntiCovid Vérif ", mise en oeuvre par le ministre chargé de la santé (direction générale de la santé). Elle permet à ces personnes de lire les noms, prénoms et date de naissance de la personne concernée par le justificatif, ainsi qu'un résultat positif ou négatif de détention d'un justificatif conforme, établi conformément aux dispositions de l'article 2-2. / Les données mentionnées à l'alinéa précédent ne sont pas conservées sur l'application " TousAntiCovid Vérif ". Elles ne sont traitées qu'une seule fois, lors de la lecture du justificatif. / (...). "<br/>
<br/>
              5. Aux termes de l'article 5 du règlement du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement de données à caractère personnel et à la libre circulation de ces données (RGPD) : " 1. Les données à caractère personnel doivent être : / (...) c) adéquates, pertinentes et limitées à ce qui est nécessaire au regard des finalités pour lesquelles elles sont traitées (minimisation des données) (...). " Aux termes de l'article 9 du même règlement : " 1. Le traitement des données (...) concernant la santé (...) d'une personne physique sont interdits. / 2. Le paragraphe 1 ne s'applique pas si l'une des conditions suivantes est remplie : / (...) g) le traitement est nécessaire pour des motifs d'intérêt public important, sur la base du droit de l'Union ou du droit d'un État membre qui doit être proportionné à l'objectif poursuivi, respecter l'essence du droit à la protection des données et prévoir des mesures appropriées et spécifiques pour la sauvegarde des droits fondamentaux et des intérêts de la personne concernée ; / (...) i) le traitement est nécessaire pour des motifs d'intérêt public dans le domaine de la santé publique (...) sur la base du droit de l'Union ou du droit de l'État membre qui prévoit des mesures appropriées et spécifiques pour la sauvegarde des droits et libertés de la personne concernée, notamment le secret professionnel (...) ". <br/>
<br/>
              6. Aux termes du 1 de l'article 35 du même règlement : " Lorsqu'un type de traitement, en particulier par le recours à de nouvelles technologies, et compte tenu de la nature, de la portée, du contexte et des finalités du traitement, est susceptible d'engendrer un risque élevé pour les droits et libertés des personnes physiques, le responsable du traitement effectue, avant le traitement, une analyse de l'impact des opérations de traitement envisagées sur la protection des données à caractère personnel (...) ". Aux termes du 1 l'article 36 du même règlement : " Le responsable du traitement consulte l'autorité de contrôle préalablement au traitement lorsqu'une analyse d'impact relative à la protection des données effectuée au titre de l'article 35 indique que le traitement présenterait un risque élevé si le responsable du traitement ne prenait pas de mesures pour atténuer le risque ". Aux termes de l'article 62 de la loi du 6 janvier 1978 : " Le responsable du traitement effectue préalablement à la mise en oeuvre du traitement une analyse d'impact des opérations de traitement envisagées sur la protection des données à caractère personnel dans les conditions prévues à l'article 35 du règlement (UE) 2016/679 du 27 avril 2016. " Enfin, aux termes de l'article 63 de la loi du 6 janvier 1978 : " Conformément à l'article 36 du règlement (UE) 2016/679du 27 avril 2016, le responsable du traitement est tenu de consulter la Commission nationale de l'informatique et des libertés préalablement à la mise en oeuvre du traitement lorsqu'il ressort de l'analyse d'impact prévue à l'article 62 que le traitement présenterait un risque élevé si le responsable du traitement ne prenait pas de mesures pour atténuer le risque ".<br/>
<br/>
              Sur la demande en référé :<br/>
<br/>
              7. L'association La Quadrature du Net demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, en premier lieu, d'ordonner la suspension de l'exécution du dispositif intitulé " Passe sanitaire ", en tant, d'une part, que ce dispositif exige le traitement dans le code en deux dimensions de données relatives à l'état civil et, d'autre part, que ce dispositif permet le traitement dans le code en deux dimensions de données de santé, en deuxième lieu, d'ordonner la suspension de l'exécution de la décision d'inclure dans les passes sanitaires des données relatives à l'état civil, ainsi que des justificatifs de statut vaccinal et des justificatifs de résultat de test virologique, en troisième lieu, d'ordonner la suspension de l'exécution du décret n° 2021-724 du 7 juin 2021 et, en dernier lieu, d'enjoindre au ministre des solidarités et de la santé de cesser immédiatement, à compter du prononcé de l'ordonnance à intervenir, de délivrer des passes sanitaires qui contiendraient des codes en deux dimensions comportant des informations relatives à l'état civil des personnes ou des données de santé, sous astreinte de 1 024 euros par jour de retard. <br/>
<br/>
              8. En premier lieu, l'association requérante soutient que le décret porte une atteinte grave et manifestement illégale au droit au respect de la vie privée et au droit à la protection des données personnelles en ce qu'il prévoit que le passe sanitaire contient les données d'identité civile telles que le nom, le prénom, la date de naissance et le genre de la personne alors même que la présence de ces données ne permettrait pas de vérifier l'authentification des documents. Elle fait valoir que le décret méconnaîtrait la loi du 31 mai 2021 et le principe de minimisation des données tel que prévu par l'article 5 du RGPD et l'article 4 de la loi du 6 janvier 1978. Toutefois, si les dispositions, citées au point 3, de second alinéa 2 du B du II de l'article 1er de la loi du 31 mai 2021, imposent que la forme des justificatifs exigibles pour certains déplacements ou pour l'accès à certains lieux, établissement ou évènements ne permette pas de connaître la nature du document ni les données qu'il contient, ces mêmes dispositions, qui ne visent qu'à empêcher les tiers de savoir si la personne est vaccinée, rétablie ou non-contaminée, n'interdisent nullement la présence dans le justificatif de données d'identité de la personne concernée. Il résulte en outre de l'instruction, et notamment des échanges lors de l'audience, que ces données d'identification sont nécessaires pour contrôler que le passe présenté est bien celui de la personne qui s'en prévaut. Au point 24 de son avis du 7 juin 2021, la Commission nationale informatique et liberté (CNIL) a d'ailleurs estimé que le dispositif du passe sanitaire tel que prévu par le décret " est de nature à assurer le respect du principe de minimisation des données, en limitant strictement la divulgation d'informations privées aux personnes habilitées à procéder aux vérifications ". Par suite, le moyen tiré de la méconnaissance de la loi du 31 mai 2021 et du principe de minimisation des données doit être rejeté. <br/>
<br/>
              9. En deuxième lieu, l'association requérante soutient que les justificatifs qui doivent être présentés pour générer le passe sanitaire et qui se présentent sous la forme d'un code en deux dimensions contiennent, sans base légale pour ce faire, des données de santé. Elle soutient également que si ces données ne sont pas lisibles par les personnes habilitées à contrôler le passe sanitaire, elles peuvent être détournées par des tiers. D'une part, il résulte des termes mêmes du I de l'article 2-3 du décret du 1er juin 2021 modifié, cités au point 4, que le résultat de l'examen de dépistage virologique, le certificat de rétablissement et le justificatif de statut vaccinal peuvent figurer dans le traitement. D'autre part, il résulte de l'instruction, et notamment des échanges lors de l'audience, que le choix de conserver ces données sensibles dans le module " Carnet " du traitement TousAntiCovid vient - outre de contraintes techniques tenant à l'urgence de mettre en oeuvre le passe sanitaire - du souhait d'éviter de créer, au niveau national, un traitement générant le passe sanitaire, en croisant les données issues du système d'information national de dépistage (" SI-DEP ") et celles contenues dans le traitement automatisé de données à caractère personnel " Vaccin Covid ". En outre, le risque de captation illégale des données de santé figurant sur le téléphone mobile, qui suppose que le QR code soit présenté par le propriétaire du téléphone à un individu doté d'un logiciel malveillant capable de lire les données de santé qui y figurent, semble peu élevé. En conséquence, le choix d'offrir un système décentralisé, limitant la constitution de traitements ou bases nationales de données de santé, au prix de la conservation, par la personne concernée, sur son propre téléphone mobile, de certaines de ses propres données de santé, remplit un motif d'intérêt public dans le domaine de la santé publique et n'est pas manifestement contraire au principe de minimisation. Par suite, à supposer même qu'il soit possible, comme le soutient le requérant, d'inscrire sur les téléphones mobiles, grâce à un traitement national, un certificat ne contenant pour qualifier l'état de santé de la personne qu'un feu rouge ou un feu vert, il résulte de tout ce qui a été dit que le Gouvernement n'a pas fait une appréciation erronée des exigences combinées des articles 5, paragraphe 1, et 9, paragraphe 2 sous i) du RGPD en autorisant l'inscription de ces données de santé dans le traitement.<br/>
<br/>
              10. En dernier lieu, il résulte des dispositions citées au point 6 que lorsqu'un traitement est susceptible d'engendrer un risque élevé pour les droits et libertés des personnes physiques, le responsable du traitement effectue, avant le traitement, une analyse de l'impact des opérations de traitement envisagées sur la protection des données à caractère personnelles. En outre, lorsqu'il ressort de cette analyse que les opérations de traitement des données comportent un risque élevé que le responsable du traitement ne peut atténuer en prenant des mesures appropriées compte tenu des techniques disponibles et des coûts liés à leur mise en oeuvre, il convient que l'autorité de contrôle soit consultée avant que le traitement n'ait lieu. S'il en résulte que l'absence de consultation de l'autorité de contrôle sur l'analyse d'impact d'un traitement présentant un risque résiduel fort est sans incidence sur la légalité de l'acte créant le traitement, il appartient en revanche au juge des référés de l'article L. 521-2 du code de justice administrative, saisi d'un moyen en ce sens, de vérifier s'il y avait lieu de saisir l'autorité de contrôle de l'analyse d'impact et, en cas de réponse positive, si la consultation a été faite préalablement au traitement. La violation d'une telle garantie est de nature à constituer une atteinte grave et manifestement illégale à la vie privée et à la protection des données personnelles.<br/>
<br/>
              11. Le ministre des solidarités et de la santé soutient que l'analyse d'impact du passe sanitaire, qui porte tant sur le module " Carnet " de l'application TousAntiCovid que sur l'application TousAntiCovid Vérif, n'a pas fait apparaître de risque résiduel, une fois les mesures de sécurité prises, et qu'il n'y avait par suite pas lieu de consulter la CNIL sur son contenu. Il souligne en outre avoir d'emblée associé la CNIL aux travaux de conception juridique et technique du traitement en cause. Il indique avoir transmis à la CNIL le 9 juin 2021 une analyse d'impact datée du même jour, qu'il produit.<br/>
<br/>
              12. Il résulte de l'instruction et il n'est d'ailleurs pas contesté que le traitement TousAntiCovid Vérif effectivement mis en oeuvre par le ministre de solidarités et de la santé repose sur un contrôle local des données contenues par les justificatifs (" mode off-line "), et que le Gouvernement a renoncé à tout échange de données avec le serveur central de la société prestataire lors de la vérification des justificatifs présentés sur le téléphone mobile de la personne entendant se prévaloir du passe sanitaire. Il résulte des termes mêmes de cette étude d'impact, et il n'est d'ailleurs pas contesté par les requérants, que cette modification limite significativement la vraisemblance des risques d'accès illégitime, de modification non désirée ou de disparition des données concernées. Cette modification est d'ailleurs conforme à la proposition faite par la CNIL au point 33 de son avis du 7 juin 2021. Par suite, le choix du Gouvernement de ne pas saisir la CNIL de l'analyse d'impact préalable à la mise en oeuvre du traitement - qui montrait l'existence d'un risque résiduel faible - n'entache la mise en oeuvre du passe sanitaire d'aucune illégalité manifeste.<br/>
<br/>
              13. Enfin, le passe sanitaire est de nature à permettre, par la limitation des flux et croisements de personne qu'il implique, de réduire la circulation du virus de la Covid-19 dans le pays. Son usage a été restreint au déplacements avec l'étranger, la Corse et l'outre-mer, d'une part, et à l'accès à des lieux de loisirs, d'autre part, sans que soient concernées les activités quotidiennes ou l'exercice des libertés de culte, de réunion ou de manifestation. En outre, l'usage de l'application TousAntiCovid demeure facultatif, les justificatifs pouvant être produits par voie papier ou sur tout autre support numérique, au choix de la personne concernée.<br/>
<br/>
              14. Il résulte de tout ce qui précède, et sans qu'il soit besoin de se prononcer sur la condition d'urgence, que la mise en oeuvre du passe sanitaire n'est pas, à la date de la présente ordonnance, manifestement illégale. Il y a lieu, par suite, de rejeter la requête de l'association La Quadrature du Net, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de l'association La Quadrature du Net est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à l'association La Quadrature du Net et au ministre des solidarités et de la santé. <br/>
Copie en sera adressée au Premier ministre et à la Commission nationale de l'informatique et des libertés.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
