<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026502056</ID>
<ANCIEN_ID>JG_L_2012_10_000000349665</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/50/20/CETATEXT000026502056.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  2ème sous-section jugeant seule, 16/10/2012, 349665, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-10-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349665</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 2ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jacques-Henri Stahl</PRESIDENT>
<AVOCATS>SCP LE BRET-DESACHE</AVOCATS>
<RAPPORTEUR>M. Camille Pascal</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:349665.20121016</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 27 mai et 25 août 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Jean-Claude B, demeurant ... ; M. B demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09/00119 du 16 septembre 2010 par lequel la cour régionale des pensions d'Aix-en-Provence a confirmé le jugement du 25 août 2009 par lequel le tribunal départemental des pensions du Var a rejeté sa demande de revalorisation de sa pension en fonction de l'indice de grade équivalent pratiqué pour les personnels de la marine nationale ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 500 euros à verser à la SCP Claire Le Bret Desaché, son avocat, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des pensions militaires d'invalidité et des victimes de la guerre ; <br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ; <br/>
<br/>
              Vu la loi n° 2000-321 du 12 avril 2000 ;<br/>
<br/>
              Vu le décret n° 56-913 du 5 septembre 1956 ; <br/>
<br/>
              Vu le décret n° 59-327 du 20 février 1959 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Camille Pascal, Conseiller d'Etat,<br/>
<br/>
              - les observations de la SCP Le Bret-Desaché, avocat de M. B, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, Rapporteur public,<br/>
<br/>
              La parole ayant à nouveau été donnée à la SCP Le Bret-Desaché, avocat de M. B ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes du premier alinéa de l'article L. 24 du code des pensions militaires d'invalidité et des victimes de la guerre, alors en vigueur : " Les pensions militaires prévues par le présent code sont liquidées et concédées (...) par le ministre des anciens combattants et des victimes de guerre ou par les fonctionnaires qu'il délègue à cet effet. Les décisions de rejet des demandes de pension sont prises dans la même forme " ; qu'en vertu de l'article 5 du décret du 20 février 1959 relatif aux juridictions des pensions, l'intéressé dispose d'un délai de six mois pour contester, devant le tribunal départemental des pensions, la décision prise sur ce fondement ; qu'enfin, aux termes de l'article 21 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations : " Sauf dans les cas où un régime de décision implicite d'acceptation est institué dans les conditions prévues à l'article 22, le silence gardé pendant plus de deux mois par l'autorité administrative sur une demande vaut décision de rejet. " ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B a demandé le 28 août 2006 au ministre de la défense de recalculer la pension militaire d'invalidité qui lui avait été concédée à titre définitif par arrêté du 6 décembre 1984 en fonction de l'indice du grade équivalent, plus favorable, pratiqué pour les personnels de la marine nationale ; que, par une lettre du 21 septembre 2006, le ministre lui a indiqué qu'il recherchait les moyens de donner une suite à sa demande ; que M. B a saisi le tribunal départemental des pensions du Var le 27 octobre 2007 d'un recours dirigé contre le rejet de sa demande ;<br/>
<br/>
              Considérant que, pour juger que la demande adressée par M. B au tribunal départemental des pensions était tardive, la cour régionale des pensions d'Aix-en-Provence s'est fondée sur la seule circonstance que cette demande avait été formée plus de six mois après l'intervention de la décision du 21 septembre 2006 ; qu'en statuant ainsi, sans que le ministre ait apporté la preuve, qui lui incombe, de la régularité de la notification de l'arrêté de concession de pension, la cour régionale des pensions a commis une erreur de droit ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. B est fondé à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              Considérant que M. B a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Le Bret Desaché, avocat de M. B, renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 2 000 euros à verser à la SCP Le Bret Desaché ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour régionale des pensions d'Aix-en-Provence du 16 septembre 2010 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour régionale des pensions de Nîmes.<br/>
<br/>
Article 3 : L'Etat versera à la SCP Claire Le Bret Desaché, avocat de M. B, une somme de 2 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. Jean-Claude B et au ministre de la défense.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
