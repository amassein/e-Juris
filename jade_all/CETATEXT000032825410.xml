<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032825410</ID>
<ANCIEN_ID>JG_L_2016_06_000000400816</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/82/54/CETATEXT000032825410.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 27/06/2016, 400816, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-06-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400816</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2016:400816.20160627</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A...B...C...a demandé au juge des référés du tribunal administratif de Mayotte, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au préfet de Mayotte d'organiser le retour de M. E... A...B..., son fils mineur, éloigné aux Comores,  dans un délai de quinze jours, sous astreinte de 100 euros par jour de retard. Par une ordonnance n° 1600463 du 6 juin 2016, le juge des référés du tribunal administratif de Mayotte a rejeté sa demande.<br/>
<br/>
              Par une requête enregistrée le 20 juin 2016 au secrétariat du contentieux du Conseil d'Etat, M. A...B...C...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) d'enjoindre au ministre de l'intérieur ainsi qu'aux autorités consulaires à Anjouan de prendre toutes mesures nécessaires au retour de M. D... B...à Mayotte, dans les plus brefs délais, sous astreinte de 100 euros par jour de retard ou à défaut, toute mesure qu'il estimera utile afin que soit organisé son retour effectif ;<br/>
<br/>
              3°) de l'admettre au bénéfice de l'aide juridictionnelle à titre provisoire ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence est remplie, dès lors que la mesure d'éloignement, bien qu'exécutée, continue de produire des effets d'une particulière gravité ; qu'elle fait obstacle à la possibilité pour M. E...A...B...de rejoindre les membres de sa famille installés à Mayotte ;<br/>
              - le préfet de Mayotte a porté une atteinte grave et manifestement illégale à l'intérêt supérieur de l'enfant et aux droits issus de l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - il n'a pas accompli les diligences particulières qui doivent entourer l'éloignement forcé d'un étranger mineur, notamment celles découlant de l'article L. 553-1 du code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la mesure d'éloignement litigieuse résulte d'une méconnaissance de l'article L. 511-4 du code de l'entrée et du séjour des étrangers et du droit d'asile dès lors qu'aucune vérification d'identité n'a été effectuée.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - la convention internationale relative aux droits de l'enfant ; <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ; qu'en vertu de l'article L. 522-3 du même code, le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée ; qu'à cet égard, il appartient au juge d'appel de prendre en considération les éléments recueillis par le juge du premier degré dans le cadre de la procédure écrite et orale qu'il a diligentée ;<br/>
<br/>
              2. Considérant que l'article L. 511-4 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) dispose que : " Ne peuvent faire l'objet d'une obligation de quitter le territoire français ou d'une mesure de reconduite à la frontière en application du présent chapitre : / 1° L'étranger mineur de dix-huit ans (...) " ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier du juge de première instance que, le 2 juin 2016, M. E...A...B..., a été interpellé lors d'une opération de police et a fait l'objet le même jour d'une obligation de quitter le territoire français sans délai et d'un placement en rétention en vue de son éloignement aux Comores, qui a eu lieu le 2 juin ; que, le 3 juin 2016, M. A...B...C..., se disant le père d'Abdou AbouB..., a saisi le juge des référés du tribunal administratif de Mayotte sur le fondement de l'article L. 521-2 du code de justice administrative afin qu'il soit enjoint au préfet d'organiser le retour de M. E...A...B... ; que, par une ordonnance du 6 juin 2016, le juge des référés du tribunal administratif de Mayotte a rejeté cette demande ; que M. A... B...C...relève appel de cette ordonnance ;<br/>
<br/>
              4. Considérant que M. A...B...C...soutient que la mesure prise porte une atteinte grave et manifestement illégale à plusieurs libertés, compte tenu de l'impossibilité de procéder à l'éloignement d'un mineur, des garanties qui doivent s'attacher à la mise en oeuvre d'une telle mesure et de la méconnaissance de l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales qu'entraîne nécessairement cette mesure en l'espèce ; <br/>
<br/>
              5. Considérant toutefois que, selon le procès-verbal de police établi le 2 juin 2016 et dûment signé par M. E...A...B...lors de son interpellation, celui-ci, démuni de tout document d'identité ou de voyage, a déclaré être né en 1997, n'a pu donner le nom de son père et n'a pas souhaité qu'un membre de sa famille soit avisé de son interpellation ; qu'ainsi que l'a estimé à bon droit le juge de première instance, il n'est donc pas établi que l'administration ait eu des raisons de douter des affirmations de M. E...A...B...en particulier sur son âge ; que, par ailleurs, comme l'a également relevé le premier juge, M. E...A...B... a été informé de la faculté de déposer un recours contre les arrêtés litigieux ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la requête de M. A...B...C...doit être rejetée, y compris les conclusions à fin d'injonction et celles présentées au titre de l'article L. 761-1 du code de justice administrative, selon la procédure prévue par l'article L. 522-3 de ce code, sans qu'il y ait lieu d'admettre le requérant à titre provisoire au bénéfice de l'aide juridictionnelle ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : Il n'y a pas lieu d'admettre, à titre provisoire, M. C...au bénéfice de l'aide juridictionnelle.<br/>
Article 2 : La requête de M. C...est rejetée.<br/>
Article 3 : La présente ordonnance sera notifiée à M. A...B...C....<br/>
Copie en sera adressée, pour information, au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
