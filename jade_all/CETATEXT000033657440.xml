<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033657440</ID>
<ANCIEN_ID>JG_L_2016_12_000000397934</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/65/74/CETATEXT000033657440.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 16/12/2016, 397934, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397934</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:397934.20161216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Versailles d'annuler les décisions des 23 octobre 2013 et 17 janvier 2014 par lesquelles le ministre de l'intérieur a constaté la perte de validité de son permis de conduire pour solde de points nul. Par un jugement n°1401343 du 12 janvier 2016, le tribunal administratif a constaté le retrait de la décision du 23 octobre 2013 et annulé la décision du 17 janvier 2014 en tant qu'elle constatait l'invalidité du permis de conduire de l'intéressé. <br/>
<br/>
              Par un pourvoi, enregistré le 15 mars 2016 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M. B....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la route ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant, d'une part, qu'aux termes de l'article L. 223-1 du code de la route, dans sa rédaction issue de la loi du 12 juin 2003, applicable au litige porté devant le juge du fond eu égard à la date de la délivrance du permis de conduire de M.B... : " Le permis de conduire est affecté d'un nombre de points. Celui-ci est réduit de plein droit si le titulaire du permis a commis une infraction pour laquelle cette réduction est prévue. / A la date d'obtention du permis de conduire, celui-ci est affecté, pendant un délai probatoire de trois ans, de la moitié du nombre maximal de points. Ce délai probatoire est réduit à deux ans lorsque le titulaire du permis de conduire a suivi un apprentissage anticipé de la conduite. A l'issue de ce délai probatoire, le permis de conduire est affecté du nombre maximal de points, si aucune infraction ayant donné lieu au retrait de points n'a été commise (...) " ; qu'aux termes des deux premiers alinéas de l'article R. 223-1 du même code : "  I. - Le permis de conduire est affecté d'un nombre maximal de douze points./ II. - A la date d'obtention du permis, celui-ci est affecté d'un nombre initial de six points " ;<br/>
<br/>
              2. Considérant, d'autre part, qu'aux termes du quatrième alinéa de l'article L. 223-6 du même code : " (...) Le titulaire du permis de conduire qui a commis une infraction ayant donné lieu à retrait de points peut obtenir une récupération de points s'il suit un stage de sensibilisation à la sécurité routière (...) " ; qu'aux termes du II de l'article R. 223-8 : " L'attestation délivrée à l'issue du stage effectué en application des dispositions du quatrième alinéa de l'article L. 223-6 donne droit à la récupération de quatre points dans la limite du plafond affecté au permis de conduire de son titulaire " ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que M. B...a obtenu le 19 mai 2005 un permis de conduire doté de six points en application des dispositions citées au point 1 ; que du fait d'une infraction commise le 13 avril 2006 et ayant entraîné le retrait d'un point, son capital de points n'a pas été porté au maximum de douze points à l'issue de la période probatoire qui expirait le 12 mai 2007 mais est demeuré fixé à cinq points ; que l'intéressé a, entre 2008 et 2013, commis cinq infractions ayant entraîné le retrait d'un total de dix-huit points et bénéficié de l'ajout de treize points à la suite, d'un part, de la restitution d'un point et, d'autre part, de l'accomplissement, en 2011, 2012 et 2013, de trois stages de sensibilisation à la sécurité routière ; qu'ainsi, le solde de points était nul lorsque le ministre de l'intérieur a, par sa décision du 17 janvier 2014, constaté la perte de validité du permis de conduire ; qu'il suit de là qu'en retenant qu'à cette date, le permis était affecté de quatre points à la suite du stage que l'intéressé avait effectué les 13 et 14 novembre 2013 et en annulant pour ce motif la décision du ministre, le tribunal administratif a entaché son jugement d'erreur de droit ; que son jugement doit, par suite, être annulé ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Versailles du 12 janvier 2016 est annulé.  <br/>
<br/>
		Article 2 : L'affaire est renvoyée au tribunal administratif de Versailles. <br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
