<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042570079</ID>
<ANCIEN_ID>JG_L_2020_11_000000442573</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/57/00/CETATEXT000042570079.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 25/11/2020, 442573, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>442573</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Laurent Roulaud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:442573.20201125</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le préfet du Territoire de Belfort a demandé au tribunal administratif de Besançon de procéder à la rectification de l'élection des conseillers municipaux et communautaires de la commune de Chatenois-les-Forges à l'issue du scrutin organisé le 15 mars 2020 par l'annulation de l'élection de M. A... D... en qualité de conseiller municipal et de Mme E... B... en qualité de conseiller communautaire.<br/>
<br/>
              Par un jugement n° 2000469 du 29 juillet 2020, le tribunal administratif de Besançon a annulé les opérations électorales qui se sont déroulées le 15 mars 2020 pour l'élection des conseillers municipaux et communautaires dans la commune de Chatenois-les-Forges.<br/>
<br/>
              1° Sous le n° 442573, par une requête et deux mémoires complémentaires, enregistrés les 7 août 2020, 3 septembre 2020 et 7 septembre 2020 au secrétariat du contentieux du Conseil d'Etat, Mme C... F... et l'ensemble des membres de la liste " Avenir et intérêt communal " demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) d'annuler l'élection de M. A... D... en qualité de conseiller municipal et l'élection de Mme E... B... en qualité de conseiller communautaire ;<br/>
<br/>
              3°) d'ordonner la rectification de la feuille de proclamation des résultats.<br/>
<br/>
              2° Sous le n° 443103, par une requête enregistrée le 20 août 2020 au secrétariat du Conseil d'Etat et régularisée le 24 août 2020, le préfet du Territoire de Belfort demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) d'annuler l'élection de M. A... D... en qualité de conseiller municipal et l'élection de Mme E... B... en qualité de conseiller communautaire ;<br/>
<br/>
              3°) d'ordonner la rectification de la feuille de proclamation des résultats.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Roulaud, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Les requêtes n° 442573 et 443103 tendent à l'annulation du même jugement du tribunal administratif de Besançon du 29 juillet 2020. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              Sur l'élection des conseillers municipaux :<br/>
<br/>
              2. L'article L. 225 du code électoral dispose que : " Le nombre des conseillers municipaux est, sauf en ce qui concerne Paris, fixé par l'article L. 2121-2 du code général des collectivités territoriales ". Conformément à l'article L.2121-2 du code général des collectivités territoriales, le nombre des membres du conseil municipal des communes de 2 500 à 3 499 habitants est de 23. Aux termes de l'article L. 260 du code électoral : " Les conseillers municipaux sont élus au scrutin de liste à deux tours, avec dépôt de listes comportant au moins autant de candidats que de sièges à pourvoir, et au plus deux candidats supplémentaires, sans adjonction ni suppression de noms et sans modification de l'ordre de présentation, sous réserve de l'application des dispositions prévues au deuxième alinéa de l'article L. 264. ". Enfin, en application de l'article L. 270 du même code : " Le candidat venant sur une liste immédiatement après le dernier élu est appelé à remplacer le conseiller municipal élu sur cette liste dont le siège devient vacant pour quelque cause que ce soit. (...) ". <br/>
<br/>
              3. Il résulte de ce qui précède que, alors même que l'article L. 260 du code électoral dispose que la liste des candidats aux sièges de conseiller municipal comporte un nombre de candidats égal au nombre de sièges à pourvoir, augmenté au plus de deux candidats supplémentaires, le nombre de candidats aux sièges de conseiller municipal proclamés élus à l'issue du scrutin ne peut être supérieur à celui fixé en application de l'article L. 2121-2 du code général des collectivités territoriales. Ainsi, la proclamation de l'élection d'un candidat supplémentaire, désigné en application de l'article L. 260 précité, ne peut qu'être annulée par le juge de l'élection.<br/>
<br/>
              4. Il résulte de l'instruction que, conformément à l'article L. 2121-2 du code général des collectivités territoriales précité, à Chatenois-les-Forges, commune qui compte 2 750 habitants, vingt-trois sièges de conseillers municipaux devaient être pourvus. Toutefois, à l'issue du premier tour de scrutin du 15 mars 2020, vingt-quatre noms, issus de la seule liste en présence " Avenir et intérêt communal ", figuraient en qualité de conseillers municipaux sur la feuille de proclamation des résultats annexée au procès-verbal des opérations électorales, M. A... D..., candidat supplémentaire désigné en application des dispositions susmentionnées de l'article L. 260 du code électoral, ayant également été proclamé élu. Si son élection devait être annulée par le juge de l'élection, une telle irrégularité ne saurait être considérée en l'espèce comme ayant vicié l'ensemble des opérations électorales. Il s'ensuit que les requérants sont fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Besançon a annulé l'ensemble des opérations qui se sont déroulées pour les élections municipales dans la commune de Chatenois-les-Forges le 15 mars 2020. Il appartient par suite au Conseil d'Etat, saisi du litige par l'effet dévolutif de l'appel, d'annuler uniquement l'élection du candidat supplémentaire, M. A... D....<br/>
<br/>
              Sur l'élection des conseillers communautaires :<br/>
<br/>
              5. L'article L. 273-1 du code électoral dispose que : " Le nombre de conseillers communautaires composant l'organe délibérant (...) des communautés d'agglomération, (...) et leur répartition entre les communes membres sont fixés dans les conditions prévues aux articles L. 5211-6-1 et L. 5211-6-2 du code général des collectivités territoriales. " L'article L. 5211-6-1 du code général des collectivités territoriales fixe notamment les modalités selon lesquelles le préfet arrête le nombre et la répartition des sièges de chaque commune membre aux conseils communautaires des communautés d'agglomération. Aux termes de l'article L. 273-9 du code électoral : " (...) La liste des candidats aux sièges de conseiller communautaire comporte un nombre de candidats égal au nombre de sièges à pourvoir, augmenté d'un candidat supplémentaire si ce nombre est inférieur à cinq et de deux dans le cas inverse (...). " Conformément à l'article L. 273-10 du même code : " (...) Lorsque le siège d'un conseiller communautaire devient vacant, pour quelque cause que ce soit, il est pourvu par le candidat de même sexe élu conseiller municipal ou conseiller d'arrondissement suivant sur la liste des candidats aux sièges de conseiller communautaire sur laquelle le conseiller à remplacer a été élu. Toutefois, lorsque la commune ne dispose que d'un siège de conseiller communautaire, ce siège est pourvu par le candidat supplémentaire mentionné au 1° du I de l'article L. 273-9(...). "<br/>
<br/>
              6. Il résulte de ce qui précède que, alors même que l'article L. 273-9 du code électoral dispose que la liste des candidats aux sièges de conseiller communautaire comporte un nombre de candidats égal au nombre de sièges à pourvoir, augmenté d'un candidat supplémentaire si ce nombre est inférieur à cinq, le nombre de candidats aux sièges de conseiller communautaire proclamés élus à l'issue du scrutin ne peut être supérieur à celui fixé par le préfet. Ainsi la proclamation de l'élection d'un candidat supplémentaire, désigné en application de l'article L. 273-9 précité, ne peut qu'être annulée par le juge de l'élection.<br/>
<br/>
              7. Il ressort de l'instruction qu'en application de l'arrêté du 17 septembre 2019 du préfet du Territoire de Belfort, les électeurs de la commune de Chatenois-les-Forges devaient élire deux conseillers communautaires au sein de la communauté d'agglomération Grand-Belfort. Toutefois, à l'issue du premier tour de scrutin du 15 mars 2020, trois noms, issus de la seule liste en présence " Avenir et intérêt communal ", figuraient en qualité de conseillers communautaires sur la feuille de proclamation des résultats annexée au procès-verbal des opérations électorales, Mme E... B..., candidate supplémentaire désignée en application des dispositions susmentionnées de l'article L. 273-9 du code électoral ayant également été proclamée élue. Si le juge de l'élection devait annuler son élection, une telle irrégularité ne saurait être considérée en l'espèce comme ayant vicié l'ensemble des opérations électorales. Il s'ensuit que les requérants sont fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Besançon a annulé l'ensemble des opérations qui se sont déroulées pour les élections communautaires dans la commune de Rougemont-le-Château le 15 mars 2020. Il appartient par suite au Conseil d'Etat, saisi du litige par l'effet dévolutif de l'appel, d'annuler uniquement l'élection de Mme E... B... en qualité de conseiller communautaire.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'élection de M. A... D..., en qualité de conseiller municipal de la commune de Chatenois-les-Forges, est annulée.<br/>
Article 2 : L'élection de Mme E... B..., en qualité de conseiller communautaire au sein de la communauté d'agglomération Grand Belfort, est annulée.<br/>
Article 3: Le jugement du tribunal administratif de Besançon du 29 juillet 2020 est réformé en ce qu'il a de contraire à la présente décision.<br/>
Article 4 : La présente décision sera notifiée au préfet du Territoire de Belfort, à Mme C... F..., première dénommée pour l'ensemble des requérants, à Mme E... B..., à M. A... D... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
