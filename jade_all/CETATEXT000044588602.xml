<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044588602</ID>
<ANCIEN_ID>JG_L_2021_12_000000447455</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/58/86/CETATEXT000044588602.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 27/12/2021, 447455, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>447455</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS</AVOCATS>
<RAPPORTEUR>M. Charles-Emmanuel Airy</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:447455.20211227</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme D... C... a demandé au tribunal administratif de Marseille de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles elle a été assujettie au titre de l'année 2010 ainsi que des pénalités correspondantes. Par un jugement n° 1604713 du 9 janvier 2019, ce tribunal a rejeté sa demande.  <br/>
<br/>
              Par un arrêt n° 19MA01093 du 22 octobre 2020, la cour administrative d'appel de Marseille a, sur appel de Mme C..., annulé ce jugement et prononcé la décharge des impositions en litige.<br/>
<br/>
              Par un pourvoi enregistré le 10 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'économie, des finances et de la relance demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel formé par Mme C....<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles-Emmanuel Airy, auditeur,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à Me Haas, avocat de Mme C... ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces soumis au juge du fond que Mme C... est associée à 50 % de la société civile immobilière (SCI) Tardi. Cette société a, le 6 mai 2010, cédé un appartement situé au 40, avenue de la Jarre à Marseille (Bouches-du-Rhône). A l'issue d'une vérification de la comptabilité de la société Tardi au titre des années 2008 à 2010, l'administration a estimé que cet appartement ne constituait pas, contrairement à ce qui était allégué, la résidence principale de Mme C... et a remis en cause le bénéfice de l'exonération de la plus-value de cession dont celle-ci s'était prévalue. Mme C... a saisi le tribunal administratif de Marseille d'une demande tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles elle a en conséquence été assujettie au titre de l'année 2010 ainsi que des pénalités correspondantes. Par un jugement du 9 janvier 2019, ce tribunal a rejeté sa demande. Par un arrêt du 22 octobre 2020, la cour administrative d'appel de Marseille, faisant droit à l'appel formé par Mme C..., a prononcé la décharge des impositions supplémentaires en litige. Le ministre de l'économie, des finances et de la relance se pourvoit en cassation contre cet arrêt.<br/>
<br/>
              2. Aux termes de l'article L. 256 du livre des procédures fiscales : " Un avis de mise en recouvrement est adressé par le comptable public à tout redevable des sommes, droits, taxes et redevances de toute nature dont le recouvrement lui incombe lorsque le paiement n'a pas été effectué à la date d'exigibilité ". Aux termes de l'article R. 256-6 du même code : " La notification de l'avis de mise en recouvrement comporte l'envoi au redevable, soit au lieu de son domicile, de sa résidence ou de son siège, soit à l'adresse qu'il a lui-même fait connaître au service compétent de la direction générale des finances publiques ou au service des douanes et droits indirects compétent, de la copie de l'avis de mise en recouvrement (...) ". <br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que l'administration fiscale avait joint au mémoire en réplique qu'elle a produit le 10 décembre 2018 devant le tribunal administratif de Marseille la copie de l'accusé de réception du pli contenant l'avis de mise en recouvrement adressé à Mme C... au " 40, avenue de la Jarre " à Marseille, qui lui a été retourné avec la mention " Destinataire inconnu à l'adresse ". Dès lors, et quand bien même l'administration fiscale n'a pas de nouveau produit en appel cet accusé de réception à la suite d'une mesure supplémentaire d'instruction, la cour administrative d'appel de Marseille a dénaturé les pièces du dossier qui lui était soumis en estimant que l'administration n'apportait pas la preuve d'une notification de l'avis de mise en recouvrement en cause au " 40, avenue de la Jarre ". Par suite, le ministre de l'économie, des finances et de la relance est fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              4. Les dispositions de l'article L. 761-1 du code de justice administrative et du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991 relative à l'aide juridique font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 22 octobre 2020 de la cour administrative d'appel de Marseille est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille. <br/>
Article 3 : Les conclusions présentées par Mme C... au titre de l'article L. 761-1 du code de justice administrative et du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991 relative à l'aide juridique sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'économie, des finances et de la relance et à Mme D... C....<br/>
              Délibéré à l'issue de la séance du 16 décembre 2021 où siégeaient : M. Pierre Collin, président de chambre, présidant ; M. Mathieu Herondart, conseiller d'Etat et M. F... A..., auditeur-rapporteur. <br/>
<br/>
              Rendu le 27 décembre 2021.<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Pierre Collin<br/>
 		Le rapporteur : <br/>
      Signé : M. Charles-Emmanuel Airy<br/>
                 La secrétaire :<br/>
                 Signé : Mme E... B...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
