<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027198418</ID>
<ANCIEN_ID>JG_L_2013_03_000000347882</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/19/84/CETATEXT000027198418.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 20/03/2013, 347882, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-03-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>347882</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR>Mme Eliane Chemla</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:347882.20130320</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 28 mars et 27 juin 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. et Mme  B...A..., demeurant... ; ils demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 09LY01052 du 25 janvier 2011 par lequel  la cour administrative d'appel de Lyon a rejeté leur requête tendant à l'annulation du jugement n° 0404575 du 12 mars 2009 par lequel le tribunal administratif de Grenoble a rejeté leur demande tendant à la décharge des cotisations d'impôt sur le revenu, de contribution sociale généralisée, de contribution au remboursement de la dette sociale et de prélèvement social auxquelles ils ont été assujettis au titre de l'année 2001 et au remboursement des sommes correspondantes assorties du paiement des intérêts moratoires ;  <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;  <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Anton, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Bouzidi, Bouhanna, avocat de M. et Mme A..., <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Bouzidi, Bouhanna, avocat de M. et Mme A...;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 29 juin 2004, l'administration fiscale a rejeté la réclamation de M. et Mme A... tendant à ce que la perte de 1 380 153 euros constatée en 2001 à l'occasion du rachat partiel d'un contrat d'assurance-vie par capitalisation soit imputée sur leurs revenus de capitaux mobiliers et à ce que la partie non imputable sur ces revenus soit déduite de leur revenu global ; que M. et Mme A...  demandent l'annulation de l'arrêt du 25 janvier 2011 par lequel la cour administrative d'appel de Lyon a confirmé le jugement du 12 mars 2009 du tribunal administratif de Grenoble rejetant leur demande tendant à la décharge des cotisations d'impôt sur le revenu et de contributions sociales auxquelles ils ont été assujettis au titre de l'année 2001 ;  <br/>
<br/>
              2. Considérant, d'une part, qu'aux termes de l'article 125-0 A du code général des impôts dans sa rédaction applicable à l'année 2001 : " Les produits attachés aux bons ou contrats de capitalisation ainsi qu'aux placements de même nature sont, lors du dénouement du contrat, soumis à l'impôt sur le revenu (...) Les produits en cause sont constitués par la différence entre les sommes remboursées au bénéficiaire et le montant des primes versées " ; <br/>
<br/>
<br/>
              3. Considérant qu'en jugeant que ces dispositions se bornaient à préciser les modalités d'imposition des produits attachés aux contrats de capitalisation et aux contrats de même nature lors du dénouement du contrat et qu'elles ne prévoyaient pas que la perte née de la différence entre le montant des versements ou cotisations versées au jour du rachat, qu'il soit partiel ou total, et la valeur du contrat lors de son rachat, laquelle présente le caractère d'une perte en capital, pût être déduite des revenus de capitaux mobiliers du contribuable à l'occasion d'une telle opération, la cour, à qui il n'appartenait pas d'examiner la conformité de l'article 125-0 A du code général des impôts au principe d'égalité devant l'impôt et les charges publiques, faute d'avoir été saisie d'une question prioritaire de constitutionnalité, et qui n'en a pas méconnu la portée, n'a pas commis d'erreur de droit ;<br/>
<br/>
              4. Considérant, d'autre part, qu'aux termes du premier alinéa de l'article 1 du  code général des impôts, dans sa rédaction applicable à l'année 2001 : " Il est établi un impôt annuel unique sur le  revenu des personnes physiques désigné sous le nom d'impôt sur le revenu. Cet impôt frappe le revenu net global du contribuable déterminé  conformément aux dispositions des articles 156 à 168 (... )" ; qu'aux termes du 1 de l'article 13 du même code :  " Le bénéfice ou revenu imposable est constitué par l'excédent du produit brut, y compris la valeur des profits et avantages en nature, sur les dépenses effectuées en vue de l'acquisition et de la conservation du revenu " ;  que le II de l'article 156 du même code énumère les charges qui peuvent être déduites du revenu brut pour la détermination du revenu net global du contribuable, lorsqu'elles n'entrent pas en compte pour l'évaluation des revenus des différentes catégories ; qu'il résulte de ces dernières dispositions que les pertes résultant du rachat d'un contrat de capitalisation ne sont pas au nombre de ces charges ;<br/>
<br/>
              5. Considérant qu'en jugeant qu'aucune de ces dispositions du code général des impôts ne permettait, pour le calcul de l'impôt sur le revenu, la prise en compte de la perte subie par les contribuables lors du dénouement de leur contrat de capitalisation en raison de sa dévaluation, laquelle ne saurait être regardée ni comme une dépense effectuée en vue de l'acquisition et de la conservation de ces revenus ni comme un déficit devant être pris en compte pour la détermination du revenu net global, la cour, qui a suffisamment motivé son arrêt, n'a pas commis d'erreur de droit ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que le pourvoi de M. et Mme A... doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>		D E C I D E :<br/>
              		--------------<br/>
<br/>
Article 1er : Le pourvoi de M. et Mme  A...est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. et Mme B... A...et au ministre de l'économie et des finances. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
