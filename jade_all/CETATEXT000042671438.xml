<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042671438</ID>
<ANCIEN_ID>JG_L_2020_12_000000432899</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/67/14/CETATEXT000042671438.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 11/12/2020, 432899, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432899</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>GALY</AVOCATS>
<RAPPORTEUR>M. François Charmont</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:432899.20201211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Strasbourg d'annuler pour excès de pouvoir la décision du 12 mars 2019 par laquelle le préfet du Bas-Rhin a refusé de procéder à l'échange de son permis de conduire arménien contre un permis de conduire français. Par un jugement n° 1902243 du 7 juin 2019, le tribunal administratif a annulé cette décision.<br/>
<br/>
              Par un pourvoi, enregistré le 23 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M. B....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la route ;<br/>
              - l'arrêté du 12 janvier 2012 fixant les conditions de reconnaissance et d'échange des permis de conduire délivrés par les Etats n'appartenant ni à l'Union européenne ni à l'espace économique européen ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Charmont, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à Me Galy Isabelle, avocat de M. A... B....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B..., ressortissant arménien, a obtenu le 12 février 2015 un permis de conduire délivré par les autorités de la République d'Arménie en échange du permis dont il était jusque-là titulaire, qui lui avait été délivré le 9 juillet 1991 par les autorités de l'Union des républiques socialistes soviétiques (URSS). Après son entrée sur le territoire français, il a demandé l'échange de ce permis arménien contre un permis de conduire français. Le préfet du Bas-Rhin a refusé cet échange par une décision du 12 mars 2019. Le ministre de l'intérieur se pourvoit en cassation contre le jugement du 7 juin 2019 par lequel le tribunal administratif de Strasbourg a, à la demande de M. B..., annulé ce refus.<br/>
<br/>
              2. Aux termes de l'article R. 222-3 du code la route : " Tout permis de conduire national, en cours de validité, délivré par un Etat ni membre de l'Union européenne, ni partie à l'accord sur l'Espace économique européen, peut être reconnu en France jusqu'à l'expiration d'un délai d'un an après l'acquisition de la résidence normale de son titulaire. Pendant ce délai, il peut être échangé contre le permis français, sans que son titulaire soit tenu de subir les examens prévus au premier alinéa de l'article D. 221-3 Les conditions de cette reconnaissance et de cet échange sont définies par arrêté du ministre chargé de la sécurité routière, après avis du ministre de la justice et du ministre chargé des affaires étrangères (...) " Aux termes de l'article 5 de l'arrêté du 12 janvier 2012 fixant les conditions de reconnaissance et d'échange des permis de conduire délivrés par les Etats n'appartenant ni à l'Union européenne, ni à l'Espace économique européen : " I. - Pour être échangé contre un titre français, tout permis de conduire délivré par un Etat n'appartenant ni à l'Union européenne, ni à l'Espace économique européen doit répondre aux conditions suivantes :/ A. Avoir été délivré au nom de l'Etat dans le ressort duquel le conducteur avait alors sa résidence normale, sous réserve qu'il existe un accord de réciprocité entre la France et cet Etat conformément à l'article R.222-1 du code de la route (...) ". Il résulte de ces dispositions que, pour déterminer si un permis de conduire délivré par un Etat n'appartenant ni à l'Union européenne, ni à l'espace économique européen est susceptible d'être échangé contre un permis français, il y a seulement lieu de vérifier si cet Etat est lié à la France par un accord de réciprocité en matière d'échange de permis de conduire.<br/>
<br/>
              3. Il ressort des termes du jugement attaqué que, pour annuler la décision litigieuse, le tribunal administratif s'est fondé sur ce que l'intéressé était encore titulaire du permis délivré par l'URSS en 1991 et que sa demande devait, dès lors, être examinée en considération de l'accord de réciprocité qui avait existé entre la France et l'URSS.<br/>
<br/>
              4. D'une part, en se fondant sur la possession par M. B... d'un permis de conduire délivré par l'URSS, dont la validité n'était au demeurant pas établie, alors que le titre dont l'intéressé demandait l'échange était le permis de conduire qui lui avait été délivré le 12 février 2015 par les autorités de la République d'Arménie, le tribunal administratif a entaché son jugement d'une erreur de droit. <br/>
<br/>
              5. D'autre part, en jugeant que la demande d'échange du permis arménien de M. B... devait être appréciée au regard d'un accord de réciprocité avec la Fédération de Russie, le tribunal, auquel il revenait de rechercher si la République d'Arménie était, à cette date, liée à la France par un accord de réciprocité en matière d'échange de permis de conduire a commis une autre erreur de droit.<br/>
<br/>
              6. Il résulte de ce qui précède que le ministre de l'intérieur est fondé à demander l'annulation du jugement qu'il attaque.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-1 du code de justice administrative.<br/>
<br/>
              8. Contrairement à ce que soutient M. B..., la circonstance que le permis de conduire dont il demandait l'échange lui avait été délivré, par les autorités arméniennes, en échange d'un permis antérieurement obtenu des autorités de l'URSS n'est de nature à lui rendre applicable, ni les règles d'échanges qui valaient avec l'URSS avant la disparition de cet Etat, ni celles qui valent avec la Fédération de Russie qui en est l'Etat continuateur. Il est par ailleurs constant qu'aucun accord de réciprocité n'existe entre la France et l'Arménie en matière d'échange de permis de conduire. Dès lors, le préfet du Bas-Rhin était tenu, en application des dispositions citées ci-dessus de l'article 5 de l'arrêté du 12 janvier 2012, de refuser l'échange demandé.<br/>
<br/>
              9. Il résulte de ce qui précède que les autres moyens d'annulation présentés par M. B..., tant devant le tribunal administratif que devant le Conseil d'Etat, sont inopérants. M. B... n'est, par suite, pas fondé à demander l'annulation de la décision du 12 mars 2019 du préfet du Bas-Rhin.<br/>
<br/>
              10. Les dispositions de l'article L.761-1 du code de justice administrative font obstacle à ce que soient mises à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance, les sommes que M. B... demande à ce titre devant le Conseil d'Etat.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              				D E C I D E :<br/>
              				  --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Strasbourg du 7 juin 2019 est annulé.<br/>
<br/>
Article 2 : La demande présentée par M. B... devant le tribunal administratif de Strasbourg est rejetée.<br/>
<br/>
Article 3 : Les conclusions présentées par M. B... devant le Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée au ministre de l'intérieur et à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
