<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029691311</ID>
<ANCIEN_ID>JG_L_2014_11_000000371115</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/69/13/CETATEXT000029691311.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 03/11/2014, 371115</TITRE>
<DATE_DEC>2014-11-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>371115</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Stéphane Bouchard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:371115.20141103</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 12 août et 12 novembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A...B..., demeurant ... ; M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0902731 du 10 juin 2013 par lequel le tribunal administratif de Strasbourg a rejeté sa demande tendant, en premier lieu, à l'annulation pour excès de pouvoir de l'arrêté du 7 janvier 2009 par lequel le maire de Thionville a mis fin, à compter du 1er mars 2009, à son détachement sur l'emploi fonctionnel de directeur général adjoint des services de la commune et, en second lieu, à l'annulation de la décision du 2 avril 2009 de rejet de son recours gracieux contre cet arrêté ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Thionville une somme 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984 ;<br/>
<br/>
              Vu le décret n° 86-68 du 13 janvier 1986 ;<br/>
<br/>
              Vu le décret n° 87-1101 du 30 décembre 1987 ;<br/>
<br/>
              Vu le décret n° 89-229 du 17 avril 1989 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Stéphane Bouchard, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de M. B...et à Me Le Prado, avocat de la commune de Thionville ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.B..., ingénieur territorial, a été détaché, par un arrêté du maire de la commune de Thionville du 28 octobre 2005, sur un emploi fonctionnel de directeur général adjoint des services au sein de cette commune ; que, par un arrêté du 7 janvier 2009, le maire a mis fin de manière anticipée au détachement de M. B...à compter du 1er mars 2009 ; que M. B...se pourvoit en cassation contre le jugement du tribunal administratif de Strasbourg du 10 juin 2013 rejetant sa demande d'annulation pour excès de pouvoir de cet arrêté et du rejet de son recours gracieux contre cette décision ; <br/>
<br/>
              Sur la régularité du jugement attaqué :<br/>
<br/>
              2. Considérant que, contrairement à ce que soutient le requérant, la minute du jugement attaqué comporte les signatures du magistrat ayant statué seul et du greffier d'audience ; qu'ainsi, le jugement n'est pas entaché d'irrégularité au regard des dispositions du second alinéa de l'article R. 741-8 du code de justice administrative ;<br/>
<br/>
              Sur le bien-fondé du jugement :<br/>
<br/>
              3. Considérant qu'aux termes de l'article 53 de loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " Lorsqu'il est mis fin au détachement d'un fonctionnaire occupant un emploi fonctionnel mentionné aux alinéas ci-dessous et que la collectivité ou l'établissement ne peut lui offrir un emploi correspondant à son grade, celui-ci peut demander à la collectivité ou l'établissement dans lequel il occupait l'emploi fonctionnel soit à être reclassé dans les conditions prévues aux articles 97 et 97 bis, soit à bénéficier, de droit, du congé spécial mentionné à l'article 99, soit à percevoir une indemnité de licenciement dans les conditions prévues à l'article 98. / Ces dispositions s'appliquent aux emplois : / (...) - de directeur général des services, de directeur général adjoint des services des communes de plus de 2 000 habitants ; (...) /. Il ne peut être mis fin aux fonctions des agents occupant les emplois mentionnés ci-dessus, sauf s'ils ont été recrutés directement en application de l'article 47, qu'après un délai de six mois suivant soit leur nomination dans l'emploi, soit la désignation de l'autorité territoriale. La fin des fonctions de ces agents est précédée d'un entretien de l'autorité territoriale avec les intéressés et fait l'objet d'une information de l'assemblée délibérante et du Centre national de la fonction publique territoriale ; elle prend effet le premier jour du troisième mois suivant l'information de l'assemblée délibérante " ; qu'aux termes de l'article 30 de la même loi : " Les commissions administratives paritaires connaissent des refus de titularisation. Elles connaissent des questions d'ordre individuel résultant de l'application, notamment, de l'article 25 du titre Ier du statut général des fonctionnaires de l'Etat et des collectivités territoriales, de l'article 87 de la loi n° 93-122 du 29 janvier 1993 relative à la prévention de la corruption et à la transparence de la vie économique et des procédures publiques et des articles (...) 67 (...) de la présente loi. " ; qu'aux termes de l'article 67 de la même loi : " (...) A l'expiration d'un détachement de longue durée, le fonctionnaire est réintégré dans son corps ou cadre d'emplois et réaffecté à la première vacance ou création d'emploi dans un emploi correspondant à son grade relevant de sa collectivité ou de son établissement d'origine. Lorsqu'il refuse cet emploi, il ne peut être nommé à l'emploi auquel il peut prétendre ou à un emploi équivalent que lorsqu'une vacance est ouverte ou un poste créé. Il est, en attendant, placé en position de disponibilité d'office. / (...) Le fonctionnaire détaché qui est remis à la disposition de sa collectivité ou de son établissement d'origine avant l'expiration normale de la période de détachement pour une cause autre qu'une faute commise dans l'exercice de ses fonctions et qui ne peut être réintégré dans son corps ou cadre d'emplois d'origine faute d'emploi vacant continue d'être rémunéré par l'organisme de détachement au plus tard jusqu'à la date à laquelle le détachement devait prendre fin. " ; <br/>
<br/>
              4. Considérant, en premier lieu,  que les dispositions précitées de l'article 53 de la loi du 26 janvier 1984 régissent entièrement la procédure que doit suivre l'autorité territoriale lorsqu'elle entend mettre fin au détachement d'un agent sur un des emplois fonctionnels qu'elles mentionnent ; qu'ainsi, la consultation de la commission administrative paritaire n'est pas requise avant qu'il ne soit mis fin de manière anticipée au détachement d'un agent occupant un tel emploi ; que, par suite, le tribunal administratif n'a pas commis d'erreur de droit en jugeant que la décision du maire de Thionville mettant fin avant son terme au détachement de M. B... sur l'emploi fonctionnel de directeur général adjoint des services n'avait pas à être précédée de la consultation de la commission administrative paritaire ;<br/>
<br/>
              5. Considérant, en deuxième lieu, que le tribunal administratif n'a commis aucune erreur de droit et s'est livré à une appréciation souveraine des faits exempte de dénaturation pour juger que M. B...avait manifesté son intention de quitter ses fonctions après l'élection d'un nouveau maire et qu'il avait fait preuve d'insuffisance professionnelle, compte tenu des responsabilités incombant à un directeur général adjoint des services ;<br/>
<br/>
              6. Considérant, en troisième lieu, qu'il ressort des énonciations du jugement attaqué que le tribunal administratif n'a pas fondé son appréciation de la perte de confiance du maire de la commune de Thionville en M. B...sur la seule circonstance que celui-ci avait repoussé à deux reprises l'entretien auquel le maire l'avait convoqué ; <br/>
<br/>
              7. Considérant, enfin, que le tribunal a, par une appréciation souveraine exempte de dénaturation, estimé que la décision contestée n'était entachée ni d'erreur manifeste d'appréciation ni de détournement de pouvoir ; <br/>
<br/>
              8. Considérant qu'il résulte de toute ce qui précède que le pourvoi de M. B... doit être rejeté ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Thionville qui n'est pas, dans la présente instance, la partie perdante ; que, dans les circonstances de l'espèce, il n'y a pas lieu de mettre à la charge de M. B...la somme demandée par la commune de Thionville au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B...est rejeté.  <br/>
Article 2 : Les conclusions de la commune de Thionville présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à M. A...B...et à la commune de Thionville.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-07-01-03 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. STATUT GÉNÉRAL DES FONCTIONNAIRES DE L'ÉTAT ET DES COLLECTIVITÉS LOCALES. DISPOSITIONS STATUTAIRES RELATIVES À LA FONCTION PUBLIQUE TERRITORIALE (LOI DU 26 JANVIER 1984). - EMPLOIS FONCTIONNELS - FIN DU DÉTACHEMENT SUR UN TEL EMPLOI - PROCÉDURE ENTIÈREMENT RÉGIE PAR L'ARTICLE 53 DE LA LOI DU 26 JANVIER 1984 - EXISTENCE - CONSÉQUENCE - CONSULTATION DE LA CAP NON REQUISE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-10-10 FONCTIONNAIRES ET AGENTS PUBLICS. CESSATION DE FONCTIONS. DIVERS. - FONCTION PUBLIQUE TERRITORIALE - FIN DU DÉTACHEMENT SUR UN EMPLOI FONCTIONNEL - PROCÉDURE ENTIÈREMENT RÉGIE PAR L'ARTICLE 53 DE LA LOI DU 26 JANVIER 1984 - EXISTENCE - CONSÉQUENCE - CONSULTATION DE LA CAP NON REQUISE.
</SCT>
<ANA ID="9A"> 36-07-01-03 Les dispositions de l'article 53 de la loi n° 85-53 du 26 janvier 1984 régissent entièrement la procédure que doit suivre l'autorité territoriale lorsqu'elle entend mettre fin au détachement d'un agent sur un des emplois fonctionnels qu'elles mentionnent. Ainsi, la consultation de la commission administrative paritaire (CAP) n'est pas requise avant qu'il ne soit mis fin de manière anticipée au détachement d'un agent occupant un tel emploi.</ANA>
<ANA ID="9B"> 36-10-10 Les dispositions de l'article 53 de la loi n° 85-53 du 26 janvier 1984 régissent entièrement la procédure que doit suivre l'autorité territoriale lorsqu'elle entend mettre fin au détachement d'un agent sur un des emplois fonctionnels qu'elles mentionnent. Ainsi, la consultation de la commission administrative paritaire (CAP) n'est pas requise avant qu'il ne soit mis fin de manière anticipée au détachement d'un agent occupant un tel emploi.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
