<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028077620</ID>
<ANCIEN_ID>JG_L_2013_10_000000345704</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/07/76/CETATEXT000028077620.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 16/10/2013, 345704</TITRE>
<DATE_DEC>2013-10-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>345704</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>OCCHIPINTI</AVOCATS>
<RAPPORTEUR>M. Nicolas Labrune</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:345704.20131016</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 345704, le pourvoi, enregistré le 10 janvier 2011 au secrétariat du contentieux du Conseil d'Etat, présenté pour M. C...B..., demeurant... ; M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 09/4020 du 9 décembre 2010 par laquelle la présidente de la Cour nationale du droit d'asile a fixé à deux unités de valeur le montant de l'indemnité à lui verser par l'Etat au titre de sa désignation dans le cadre de l'aide juridictionnelle en vue de soutenir l'action de M. A...D...tendant à l'annulation de la décision du 25 mai 2009 du directeur général de l'Office français de protection des réfugiés et apatrides rejetant sa demande d'asile ; <br/>
<br/>
              2°) réglant l'affaire au fond, de dire que le montant de la rétribution qui lui est due est fixé à huit unités de valeur et d'enjoindre à l'Etat de régler le montant de la rétribution correspondante en incluant les intérêts de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu, 2°, sous le n° 345705, la requête, enregistrée le 10 janvier 2011 au secrétariat du contentieux du Conseil d'Etat, présenté pour M. C...B..., demeurant... ; M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 09/4020 du 9 décembre 2010 par laquelle la présidente de la Cour nationale du droit d'asile a fixé à deux unités de valeur le montant de l'indemnité à lui verser par l'Etat au titre de sa désignation dans le cadre de l'aide juridictionnelle en vue de soutenir l'action de M. A...D...tendant à l'annulation de la décision du 25 mai 2009 du directeur général de l'Office français de protection des réfugiés et apatrides rejetant sa demande d'asile ; <br/>
<br/>
              2°) de dire que le montant de la rétribution qui lui est due est fixé à huit unités de valeur et d'enjoindre à l'Etat de régler le montant de la rétribution correspondante en incluant les intérêts de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le décret n° 91-1266 du 19 décembre 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Labrune, Auditeur,  <br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Occhipinti, avocat de M. C...B...;<br/>
<br/>
<br/>
<br/>1. Considérant que les recours visés ci-dessus sont dirigés contre la même décision ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant que, par une ordonnance du 9 décembre 2010, la présidente de la Cour nationale du droit d'asile a décidé que le montant de l'indemnité à verser à M. B...par l'Etat, en rétribution du concours qu'il avait prêté au titre de l'aide juridictionnelle à M. D..., dans l'action intentée par ce dernier devant la cour, était limité à deux unités de valeur ; que M. B...conteste cette ordonnance par un premier recours, par lequel il se pourvoit en cassation, dans l'hypothèse où cette décision présenterait un caractère juridictionnel, et par un second recours, qui tend à l'annulation de cette décision, dans l'hypothèse où elle serait de nature administrative ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article 27 de la loi du 10 juillet 1991 relative à l'aide juridique : " L'avocat qui prête son concours au bénéficiaire de l'aide juridictionnelle perçoit une rétribution. " ; qu'aux termes de l'article 90 du décret du 19 décembre 1991 portant application de la loi n° 91-647 du 10 juillet 1991 relative à l'aide juridique : " La contribution de l'Etat à la rétribution des avocats qui prêtent leur concours au bénéficiaire de l'aide juridictionnelle totale est déterminée en fonction du produit de l'unité de valeur prévue par la loi de finances (UV) et des coefficients ci-après " ; qu'il résulte du tableau figurant sous cet article que les recours présentés devant la Cour nationale du droit d'asile sont affectés d'un coefficient de base de huit unités de valeur ; qu'en application des articles 109 à 112 de ce décret, dans leur rédaction applicables à la date de la décision attaquée, le président de la juridiction saisie ou son délégué peuvent soit diminuer le montant de la rétribution lorsque l'avocat défend plusieurs personnes dans des litiges reposant sur les mêmes faits et comportant des prétentions ayant un objet similaire, soit attribuer une rétribution à l'avocat désigné au titre de l'aide juridictionnelle lorsque l'instance s'est terminée autrement que par une transaction ou un jugement ; <br/>
<br/>
              4. Considérant que les décisions prises par le président de la juridiction saisie en application des dispositions mentionnées au point 3 relatives à la rétribution de l'avocat désigné au titre de l'aide juridictionnelle ont le caractère de décisions administratives et sont, à ce titre, susceptibles de recours ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui a été dit au point 4, que M. B...n'est pas recevable à contester devant le Conseil d'Etat la décision du 9 décembre 2010 de la présidente de la Cour nationale du droit d'asile par la voie du pourvoi en cassation ; que son recours enregistré sous le n° 345704 doit dès lors être rejeté, y compris ses conclusions présentées sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              6. Considérant, enfin, que ni les dispositions de l'article R. 311-1 du code de justice administrative ni aucune autre disposition ne confère au Conseil d'Etat compétence pour connaître en premier et dernier ressort des recours dirigés contre les décisions prises par un président de juridiction et relatives à la rétribution de l'avocat désigné au titre de l'aide juridictionnelle ; que ces recours ressortissent en conséquence à la compétence du tribunal administratif territorialement compétent pour en connaître ; que, la Cour nationale du droit d'asile ayant son siège à Montreuil, le recours en annulation, enregistré sous le n° 345705, dirigé par M. B... contre la décision du 9 décembre 2010 de la présidente de la Cour nationale du droit d'asile, relève donc de la compétence du tribunal administratif de Montreuil ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le recours n° 345704 de M. B...est rejeté.<br/>
Article 2 : Le jugement du recours n° 345705 de M. B...est attribué au tribunal administratif de Montreuil.<br/>
Article 3 : La présente décision sera notifiée à M. C... B..., à la garde des sceaux, ministre de la justice, à la présidente de la Cour nationale du droit d'asile et au président du tribunal administratif de Montreuil.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-05-01-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - NOTION. ACTES À CARACTÈRE ADMINISTRATIF. ACTES PRÉSENTANT CE CARACTÈRE. - DÉCISIONS PRISES PAR LES PRÉSIDENTS DE JURIDICTION RELATIVES À LA RÉTRIBUTION DE L'AVOCAT DÉSIGNÉ AU TITRE DE L'AIDE JURIDICTIONNELLE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-05-01-01-01 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. COMPÉTENCE MATÉRIELLE. ACTES NON RÉGLEMENTAIRES. - DÉCISIONS PRISES PAR LES PRÉSIDENTS DE JURIDICTION RELATIVES À LA RÉTRIBUTION DE L'AVOCAT DÉSIGNÉ AU TITRE DE L'AIDE JURIDICTIONNELLE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">17-05-01-02 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. COMPÉTENCE TERRITORIALE. - RECOURS CONTRE LES DÉCISIONS PRISES PAR LES PRÉSIDENTS DE JURIDICTION RELATIVES À LA RÉTRIBUTION DE L'AVOCAT DÉSIGNÉ AU TITRE DE L'AIDE JURIDICTIONNELLE - TRIBUNAL ADMINISTRATIF DANS LE RESSORT DUQUEL SE TROUVE LE SIÈGE DE LA JURIDICTION EN CAUSE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">37-04-04-01 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. MAGISTRATS ET AUXILIAIRES DE LA JUSTICE. AUXILIAIRES DE LA JUSTICE. AVOCATS. - AVOCATS DÉSIGNÉS AU TITRE DE L'AIDE JURIDICTIONNELLE - DÉCISIONS PRISES PAR LES PRÉSIDENTS DE JURIDICTION RELATIVES À LEUR RÉTRIBUTION - 1) DÉCISIONS ADMINISTRATIVES SUSCEPTIBLES DE RECOURS - INCLUSION - 2) RECOURS - COMPÉTENCE POUR EN CONNAÎTRE EN PREMIER RESSORT - TRIBUNAL ADMINISTRATIF DANS LE RESSORT DUQUEL SE TROUVE LE SIÈGE DE LA JURIDICTION EN CAUSE.
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">54-01-01-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES CONSTITUANT DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - DÉCISIONS PRISES PAR LES PRÉSIDENTS DE JURIDICTION RELATIVES À LA RÉTRIBUTION DE L'AVOCAT DÉSIGNÉ AU TITRE DE L'AIDE JURIDICTIONNELLE.
</SCT>
<ANA ID="9A"> 01-01-05-01-01 Les décisions prises par le président de la juridiction saisie en application des articles 109 à 112 du décret n° 91-1266 du 19 décembre 1991, relatives à la rétribution de l'avocat désigné au titre de l'aide juridictionnelle, ont le caractère de décisions administratives et sont, à ce titre, susceptibles de recours.</ANA>
<ANA ID="9B"> 17-05-01-01-01 Ni les dispositions de l'article R. 311-1 du code de justice administrative ni aucune autre disposition ne confèrent au Conseil d'Etat compétence pour connaître en premier et dernier ressort des recours dirigés contre les décisions administratives prises par un président de juridiction et relatives à la rétribution de l'avocat désigné au titre de l'aide juridictionnelle. Ces recours ressortissent en conséquence à la compétence du tribunal administratif territorialement compétent pour en connaître, à savoir le tribunal administratif dans le ressort duquel se trouve le siège de la juridiction en cause.</ANA>
<ANA ID="9C"> 17-05-01-02 Les recours dirigés contre les décisions administratives prises par un président de juridiction et relatives à la rétribution de l'avocat désigné au titre de l'aide juridictionnelle ressortissent à la compétence du tribunal administratif dans le ressort duquel se trouve le siège de la juridiction en cause.</ANA>
<ANA ID="9D"> 37-04-04-01 1) Les décisions prises par le président de la juridiction saisie en application des articles 109 à 112 du décret n° 91-1266 du 19 décembre 1991, relatives à la rétribution de l'avocat désigné au titre de l'aide juridictionnelle, ont le caractère de décisions administratives et sont, à ce titre, susceptibles de recours.,,,2) Ni les dispositions de l'article R. 311-1 du code de justice administrative ni aucune autre disposition ne confèrent au Conseil d'Etat compétence pour connaître en premier et dernier ressort des recours dirigés contre les décisions administratives prises par un président de juridiction et relatives à la rétribution de l'avocat désigné au titre de l'aide juridictionnelle. Ces recours ressortissent en conséquence à la compétence du tribunal administratif territorialement compétent pour en connaître, à savoir le tribunal administratif dans le ressort duquel se trouve le siège de la juridiction en cause.</ANA>
<ANA ID="9E"> 54-01-01-01 Les décisions prises par le président de la juridiction saisie en application des articles 109 à 112 du décret n° 91-1266 du 19 décembre 1991, relatives à la rétribution de l'avocat désigné au titre de l'aide juridictionnelle, ont le caractère de décisions administratives et sont, à ce titre, susceptibles de recours.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
