<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033657450</ID>
<ANCIEN_ID>JG_L_2016_12_000000403738</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/65/74/CETATEXT000033657450.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 16/12/2016, 403738</TITRE>
<DATE_DEC>2016-12-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403738</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:403738.20161216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              La commune de Mantes-la-Jolie, en défense à la demande de M. B...A...tendant à l'annulation de la décision du 22 juillet 2016 par laquelle le maire de Mantes-la-Jolie a refusé de lui délivrer l'autorisation d'inhumer son fils dans cette commune et à ce qu'il soit enjoint au maire de Mantes-la-Jolie de lui délivrer cette autorisation, a produit deux mémoires, enregistrés les 11 août et 24 août 2016 au greffe du tribunal administratif, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lesquels elle soulève une question prioritaire de constitutionnalité.<br/>
<br/>
              Par un jugement n° 1605633 du 22 septembre 2016, enregistré le 23 septembre 2016 au secrétariat du contentieux du Conseil d'Etat, le tribunal administratif de Versailles, avant qu'il ne soit statué sur la demande de M.A..., a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des articles L. 2213-9 et L. 2223-3 du code général des collectivités territoriales.<br/>
<br/>
              Dans la question prioritaire de constitutionnalité transmise, la commune de Mantes-la-Jolie soutient que les dispositions des articles L. 2213-9 et L. 2223-3 du code général des collectivités territoriales, applicables au litige, méconnaissent le principe de libre administration des collectivités territoriales et le principe de liberté contractuelle des collectivités territoriales, garantis par l'article 72 de la Constitution, le droit de propriété, garanti par les articles 2 et 17 de la Déclaration des droits de l'homme et du citoyen de 1789 et l'objectif à valeur constitutionnelle de sauvegarde de l'ordre public.<br/>
<br/>
              Par un mémoire, enregistré le 3 novembre 2016, le ministre de l'intérieur soutient que les conditions posées par l'article 23-4 de l'ordonnance du 7 novembre 1958 ne sont pas remplies et, en particulier que la question soulevée n'est ni nouvelle ni sérieuse.<br/>
<br/>
              Par un nouveau mémoire, enregistré le 14 novembre 2016 au secrétariat du contentieux du Conseil d'Etat, la commune de Mantes-la-Jolie reprend ses précédentes conclusions, par les mêmes moyens.<br/>
<br/>
              La question prioritaire de constitutionnalité a été communiquée au Premier ministre, qui n'a pas produit de mémoire.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu : <br/>
<br/>
              - la Constitution, notamment son article 61-1 ;  <br/>
<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              - les articles L. 2213-9 et L. 2223-3 du code général des collectivités territoriales ; <br/>
<br/>
              - le code de justice administrative ; <br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes.<br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 2212-1 du code général des collectivités territoriales : " Le maire est chargé, sous le contrôle administratif du représentant de l'Etat dans le département, de la police municipale... " ; qu'aux termes de l'article L. 2212-2 du même code : " La police municipale a pour objet d'assurer le bon ordre, la sûreté, la sécurité et la salubrité publiques. Elle comprend notamment : / (...) 2° Le soin de réprimer les atteintes à la tranquillité publique telles que les rixes et disputes accompagnées d'ameutement dans les rues, le tumulte excité dans les lieux d'assemblée publique, les attroupements, les bruits, les troubles de voisinage, les rassemblements nocturnes qui troublent le repos des habitants et tous actes de nature à compromettre la tranquillité publique ; / 3° Le maintien du bon ordre dans les endroits où il se fait de grands rassemblements d'hommes... " ; qu'aux termes de l'article L. 2213-8 : " Le maire assure la police des funérailles et des cimetières " ; qu'aux termes de l'article L. 2213-9 : " Sont soumis au pouvoir de police du maire le mode de transport des personnes décédées, le maintien de l'ordre et de la décence dans les cimetières, les inhumations et les exhumations, sans qu'il soit permis d'établir des distinctions ou des prescriptions particulières à raison des croyances ou du culte du défunt ou des circonstances qui ont accompagné sa mort " ; qu'enfin, aux termes de l'article L. 2223-3 : " La sépulture dans un cimetière d'une commune est due : / 1° Aux personnes décédées sur son territoire, quel que soit leur domicile ; / 2° Aux personnes domiciliées sur son territoire, alors même qu'elles seraient décédées dans une autre commune ; / 3° Aux personnes non domiciliées dans la commune mais qui y ont droit à une sépulture de famille ; / 4° Aux Français établis hors de France n'ayant pas une sépulture de famille dans la commune et qui sont inscrits sur la liste électorale de celle-ci " ; <br/>
<br/>
              3. Considérant que les dispositions de l'article L. 2223-3 cité ci-dessus fixent les catégories de personnes auxquelles la sépulture est due dans les cimetières de la commune ; que les dispositions de l'article L. 2213-9, qui confient au maire la police des funérailles, lui interdisent d'établir des distinctions ou des prescriptions particulières en fonction, notamment, des circonstances de la mort ; que, selon la commune de Mantes-la-Jolie, ces dispositions, en ce qu'elles sont susceptibles de contraindre le maire à autoriser l'inhumation dans  un cimetière de la commune d'une personne qui a perpétré des actes de terrorisme ayant affecté cette collectivité, y compris lorsqu'elle est décédée à cette occasion, portent atteinte à la libre administration des collectivités territoriales, à la liberté contractuelle des communes et à leur droit de propriété, ainsi qu'à l'objectif constitutionnel de sauvegarde de l'ordre public ;<br/>
<br/>
              4. Considérant, toutefois, que les pouvoirs de police générale et spéciale que le maire tient des dispositions des articles L. 2212-1, L. 2212-2, L. 2213-8 et L. 2213-9 du code général des collectivités territoriales lui permettent de prendre les mesures nécessaires pour prévenir les troubles à l'ordre public que pourrait susciter l'inhumation dans un cimetière de la commune d'une personne qui a commis des actes d'une particulière gravité ayant affecté cette collectivité ; que la circonstance que ces actes sont à l'origine du décès de l'intéressé est sans incidence sur la possibilité de prendre de telles mesures ; qu'il appartient au maire, lorsqu'il constate un risque de troubles, de fixer des modalités d'inhumation de nature à préserver l'ordre public ; qu'en présence d'un risque de troubles tel que, dans les circonstances de l'espèce, aucune autre mesure ne serait de nature à le prévenir, le maire peut légalement refuser l'autorisation d'inhumation, sans qu'y fassent obstacle les dispositions de l'article L. 2223-3 du code, qui doivent être conciliées avec celles qui confient au maire des pouvoirs de police ; qu'ainsi, contrairement à ce que soutient la commune de Mantes-la-Jolie, le maire n'est pas contraint, quelles que puissent être les circonstances, d'autoriser une inhumation dans un cimetière communal ; qu'il suit de là que la question prioritaire de constitutionnalité soulevée par la commune de Mantes-la-Jolie, qui n'est pas nouvelle, ne présente par un caractère sérieux ; qu'il n'y a dès lors pas lieu de la renvoyer au Conseil constitutionnel ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le tribunal administratif de Versailles.<br/>
Article 2 : La présente décision sera notifiée à la commune de Mantes-la-Jolie et à M. B...A.... <br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre, au ministre de l'intérieur ainsi qu'au tribunal administratif de Versailles.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-03-02-05 COLLECTIVITÉS TERRITORIALES. COMMUNE. ATTRIBUTIONS. POLICE. POLICE DES CIMETIÈRES. - PERSONNES AUXQUELLES LA SÉPULTURE EST DUE DANS LES CIMETIÈRES DE LA COMMUNE (ART. L. 2223-3 DU CGCT) - FACULTÉ POUR LE MAIRE DE REFUSER L'INHUMATION POUR UN MOTIF D'ORDRE PUBLIC - EXISTENCE, LORSQU'AUCUNE AUTRE MESURE N'EST DE NATURE À PRÉVENIR LES TROUBLES À L'ORDRE PUBLIC [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">49-05-08 POLICE. POLICES SPÉCIALES. POLICE DES CIMETIÈRES. - PERSONNES AUXQUELLES LA SÉPULTURE EST DUE DANS LES CIMETIÈRES DE LA COMMUNE (ART. L. 2223-3 DU CGCT) - FACULTÉ POUR LE MAIRE DE REFUSER L'INHUMATION POUR UN MOTIF D'ORDRE PUBLIC - EXISTENCE, LORSQU'AUCUNE AUTRE MESURE N'EST DE NATURE À PRÉVENIR LES TROUBLES À L'ORDRE PUBLIC [RJ1].
</SCT>
<ANA ID="9A"> 135-02-03-02-05 L'article L. 2223-3  du code général des collectivités territoriales (CGCT) fixe les catégories de personnes auxquelles la sépulture est due dans les cimetières de la commune. Les dispositions de l'article L. 2213-9, qui confient au maire la police des funérailles, lui interdisent d'établir des distinctions ou des prescriptions particulières en fonction, notamment, des circonstances de la mort.,,,Toutefois, les pouvoirs de police générale et spéciale que le maire tient des articles L. 2212-1, L. 2212-2, L. 2213-8 et L. 2213-9 du CGCT lui permettent de prendre les mesures nécessaires pour prévenir les troubles à l'ordre public que pourrait susciter l'inhumation dans un cimetière de la commune d'une personne qui a commis des actes d'une particulière gravité ayant affecté cette collectivité. La circonstance que ces actes sont à l'origine du décès de l'intéressé est sans incidence sur la possibilité de prendre de telles mesures. Il appartient au maire, lorsqu'il constate un risque de troubles, de fixer des modalités d'inhumation de nature à préserver l'ordre public. En présence d'un risque de troubles tel que, dans les circonstances de l'espèce, aucune autre mesure ne serait de nature à le prévenir, le maire peut légalement refuser l'autorisation d'inhumation, sans qu'y fassent obstacle les dispositions de l'article L. 2223-3 du code, qui doivent être conciliées avec celles qui confient au maire des pouvoirs de police.</ANA>
<ANA ID="9B"> 49-05-08 L'article L. 2223-3  du code général des collectivités territoriales (CGCT) fixe les catégories de personnes auxquelles la sépulture est due dans les cimetières de la commune. Les dispositions de l'article L. 2213-9, qui confient au maire la police des funérailles, lui interdisent d'établir des distinctions ou des prescriptions particulières en fonction, notamment, des circonstances de la mort.,,,Toutefois, les pouvoirs de police générale et spéciale que le maire tient des articles L. 2212-1, L. 2212-2, L. 2213-8 et L. 2213-9 du CGCT lui permettent de prendre les mesures nécessaires pour prévenir les troubles à l'ordre public que pourrait susciter l'inhumation dans un cimetière de la commune d'une personne qui a commis des actes d'une particulière gravité ayant affecté cette collectivité. La circonstance que ces actes sont à l'origine du décès de l'intéressé est sans incidence sur la possibilité de prendre de telles mesures. Il appartient au maire, lorsqu'il constate un risque de troubles, de fixer des modalités d'inhumation de nature à préserver l'ordre public. En présence d'un risque de troubles tel que, dans les circonstances de l'espèce, aucune autre mesure ne serait de nature à le prévenir, le maire peut légalement refuser l'autorisation d'inhumation, sans qu'y fassent obstacle les dispositions de l'article L. 2223-3 du code, qui doivent être conciliées avec celles qui confient au maire des pouvoirs de police.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, Assemblée, 4 mars 1949, Dame Nemironsky, p. 104 ; CE, 12 mai 2004, Association du Vajra triomphant et,, n°s 253341 e. a., T. pp. 690-793.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
