<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043032344</ID>
<ANCIEN_ID>JG_L_2021_01_000000448487</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/03/23/CETATEXT000043032344.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 14/01/2021, 448487, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-01-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448487</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:448487.20210114</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au juge des référés du tribunal administratif de Nice, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre à l'Office français de l'immigration et de l'intégration (OFII) de rétablir immédiatement ses droits à l'allocation pour demandeur d'asile à compter de la notification de l'ordonnance sous astreinte de 50 euros par jour de retard. Par une ordonnance n° 2005210 du 21 décembre 2020, le juge des référés du tribunal administratif de Nice a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 7 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de l'admettre au bénéfice de l'aide juridictionnelle provisoire ;<br/>
<br/>
              2°) d'annuler cette ordonnance ;<br/>
<br/>
              3°) de faire droit à ses conclusions de première instance ; <br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 2 500 euros à verser à son conseil au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est satisfaite en ce que la privation des conditions matérielles d'accueil a des conséquences graves sur sa situation, dès lors qu'il est sans domicile fixe depuis plus d'un mois et ne peut subvenir à ses besoins minimaux ;<br/>
              - il est porté une atteinte grave et manifestement illégale au droit d'asile ;<br/>
              - il ne peut être retenu le fait qu'il ait été considéré comme " en fuite " dès lors, d'une part, que sa demande d'asile a été enregistrée au terme du délai de six mois en procédure normale et, d'autre part, que sa soustraction volontaire aux obligations prévues à l'article L. 766-8 du code de l'entrée et du séjour des étrangers et du droit d'asile n'a pas été démontrée ;<br/>
              - l'OFII a méconnu l'article L. 744-8 du code de l'entrée et du séjour des étrangers et du droit d'asile dès lors que, saisi d'une demande de rétablissement des conditions matérielles d'accueil, il n'a pas pris en compte la situation de vulnérabilité du requérant.  <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - la directive 2013/32/UE du Parlement européen et du Conseil du 26 juin 2013 ; <br/>
              - la directive 2013/33/UE du Parlement européen et du Conseil du 26 juin 2013 ; <br/>
              - la loi n° 91-647 du 10 juillet 1991 relative à l'aide juridique ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 511-1 du code de justice administrative dispose que : " Le juge des référés statue par des mesures qui présentent un caractère provisoire. Il n'est pas saisi du principal et se prononce dans les meilleurs délais ". Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". Enfin, en vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Il ressort des pièces du dossier que M. B..., ressortissant sierra-léonais né le 7 juillet 1998, a présenté une demande d'asile enregistrée le 18 septembre 2017 et a accepté le même jour les conditions matérielles d'accueil proposées par l'Office français de l'immigration et de l'intégration (OFII). Le préfet de l'Yonne a décidé son transfert aux autorités italiennes, responsables de l'examen de sa demande d'asile, et lui a enjoint de se présenter à l'aéroport en vue d'embarquer à destination de Naples le 1er mars 2018. M. B... ne s'étant pas présenté à l'embarquement, une nouvelle convocation à l'aéroport en vue de son transfert vers l'Italie le 16 avril 2018 lui a été adressée, à laquelle il n'a pas davantage déféré. Ayant été déclaré en fuite pour ce motif, l'OFII lui a retiré le bénéfice des conditions matérielles d'accueil par décision du 4 juin 2018. A l'expiration du délai de transfert vers l'Italie, M. B... a fait valoir que la France était devenue responsable de l'examen de sa demande d'asile, qui a été requalifiée en procédure normale le 14 novembre 2019. Par une ordonnance du 21 décembre 2020 dont M. B... relève appel, le juge des référés du tribunal administratif de Nice, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté sa demande tendant à ce qu'il soit enjoint au directeur de l'OFII de rétablir à son profit le bénéfice des conditions matérielles d'accueil et de procéder au versement de l'allocation pour demandeur d'asile.<br/>
<br/>
              3. Aux termes de l'article L. 744-8 du code de l'entrée et du séjour des étrangers et du droit d'asile dans sa rédaction résultant de la loi du 29 juillet 2015 relative à la réforme du droit d'asile : " Le bénéfice des conditions matérielles d'accueil peut être : / 1° Suspendu si, sans motif légitime, le demandeur d'asile a abandonné son lieu d'hébergement déterminé en application de l'article L. 744-7, n'a pas respecté l'obligation de se présenter aux autorités, n'a pas répondu aux demandes d'informations ou ne s'est pas rendu aux entretiens personnels concernant la procédure d'asile (...) ". Il résulte de ces dispositions que les conditions matérielles d'accueil sont proposées au demandeur d'asile par l'Office français de l'immigration et de l'intégration après l'enregistrement de la demande d'asile auquel il est procédé en application de l'article L. 741-1 du code de l'entrée et du séjour des étrangers et du droit d'asile. Si, par la suite, les conditions matérielles proposées et acceptées initialement peuvent être modifiées, en fonction notamment de l'évolution de la situation du demandeur ou de son comportement, la circonstance que, postérieurement à l'enregistrement de sa demande, l'examen de celle-ci devienne de la compétence de la France n'emporte pas l'obligation pour l'Office de réexaminer, d'office et de plein droit, les conditions matérielles d'accueil qui avaient été proposées et acceptées initialement par le demandeur. Dans le cas où les conditions matérielles d'accueil ont été suspendues sur le fondement de l'article L. 744-8, dans sa rédaction issue de la loi du 29 juillet 2015, le demandeur peut, notamment dans l'hypothèse où la France est devenue responsable de l'examen de sa demande d'asile, en demander le rétablissement. Il appartient alors à l'Office français de l'immigration et de l'intégration, pour statuer sur une telle demande de rétablissement, d'apprécier la situation particulière du demandeur à la date de la demande de rétablissement au regard notamment de sa vulnérabilité, de ses besoins en matière d'accueil ainsi que, le cas échéant, des raisons pour lesquelles il n'a pas respecté les obligations auxquelles il avait consenti au moment de l'acceptation initiale des conditions matérielles d'accueil.<br/>
<br/>
              4. Si la privation du bénéfice des mesures prévues par la loi afin de garantir aux demandeurs d'asile des conditions matérielles d'accueil décentes, jusqu'à ce qu'il ait été statué sur leur demande, est susceptible de constituer une atteinte grave et manifestement illégale à la liberté fondamentale que constitue le droit d'asile, le caractère grave et manifestement illégal d'une telle atteinte s'apprécie en tenant compte des moyens dont dispose l'autorité administrative compétente et de la situation du demandeur. Ainsi, le juge des référés ne peut faire usage des pouvoirs qu'il tient de l'article L. 521-2 du code de justice administrative en adressant une injonction à l'administration que dans le cas où, d'une part, le comportement de celle-ci fait apparaître une méconnaissance manifeste des exigences qui découlent du droit d'asile et où, d'autre part, il résulte de ce comportement des conséquences graves pour le demandeur d'asile, compte tenu notamment de son âge, de son état de santé ou de sa situation familiale. Il incombe au juge des référés d'apprécier, dans chaque situation, les diligences accomplies par l'administration en tenant compte des moyens dont elle dispose ainsi que de l'âge, de l'état de santé et de la situation familiale de la personne intéressée.<br/>
<br/>
              5. Il ressort des pièces du dossier que l'intéressé, célibataire, âgé de 22 ans, s'est placé en situation de fuite en refusant à deux reprises, après le rejet du recours qu'il avait formé contre la décision de le remettre aux autorités italiennes pour qu'elles traitent sa demande d'asile, de se présenter aux convocations à l'aéroport en vue de son transfert vers l'Italie, qui lui avaient été adressées en mars et avril 2018. Il n'a pas contesté le retrait du bénéfice des conditions matérielles d'accueil prononcé le 4 juin 2018 et n'en a demandé le rétablissement que plusieurs mois plus tard. Dans un tel cas, il appartient à l'OFII, pour statuer sur la demande de rétablissement, d'apprécier la situation particulière du demandeur à la date de la demande de rétablissement au regard notamment de sa vulnérabilité, de ses besoins en matière d'accueil ainsi que, le cas échéant, des raisons pour lesquelles il n'a pas respecté les obligations auxquelles il avait consenti au moment de l'acceptation initiale des conditions matérielles d'accueil.<br/>
<br/>
              6. D'une part, M. B..., qui se borne à soutenir, au demeurant à tort, que l'ordonnance du juge du référé du tribunal administratif du 8 novembre 2019 ordonnant l'enregistrement de sa demande aurait considéré qu'il n'avait pas été en fuite, n'invoque aucune raison justifiant son refus réitéré de se présenter à l'aéroport en vue de son transfert vers l'Italie. D'autre part, s'il fait valoir que l'OFII n'aurait pas procédé à une évaluation de sa vulnérabilité et qu'il se trouve à présent dans une situation de grande vulnérabilité, il n'apporte à l'appui de ces allégations aucun élément nouveau de nature à remettre en cause l'appréciation du juge des référés du tribunal administratif de Nice selon laquelle il ne résulte pas de l'instruction que le refus de rétablir les conditions matérielles d'accueil à l'intéressé porterait une atteinte grave et manifestement illégale au droit d'asile à laquelle il appartiendrait au juge des référés, statuant sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative, de mettre fin.<br/>
<br/>
              7. C'est, dès lors, à bon droit que le juge des référés du tribunal administratif de Nice a rejeté la demande dont il était saisi. Par suite, l'appel formé par M. B..., y compris ses conclusions tendant à l'application des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative, doit être rejeté selon la procédure prévue par l'article L. 522-3 de ce code, sans qu'il y ait lieu de l'admettre au bénéfice de l'aide juridictionnelle provisoire.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. A... B....<br/>
Copie en sera adressée à l'Office français de l'immigration et de l'intégration. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
