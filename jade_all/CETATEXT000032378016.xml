<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032378016</ID>
<ANCIEN_ID>JG_L_2016_04_000000393690</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/37/80/CETATEXT000032378016.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 07/04/2016, 393690, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-04-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393690</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Pauline Jolivet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:393690.20160407</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme D...K..., M. F...A..., Mme J...B..., M. L...H..., M. G...I...et Mme E...C...di Borgo ont demandé au tribunal administratif de Bastia d'annuler trois délibérations du 25 avril 2015 du conseil municipal de l'Ile-Rousse (Haute-Corse) relatives à l'élection sous le n° 36/2015 des membres du conseil d'administration du centre d'action sociale, sous le n° 37/2015 des membres de la commission d'appel d'offres et sous le n° 38/2015 des membres de la commission de délégation de service public. Par une ordonnance n° 1500784 du 24 août 2015, le premier conseiller du tribunal administratif de Bastia a rejeté leur protestation en tant qu'elle concernait les délibérations nos 37/2015 et 38/2015.<br/>
<br/>
              Par une requête et deux nouveaux mémoires, enregistrés le 23 septembre 2015, le 30 décembre 2015 et le 7 mars 2016 au secrétariat du contentieux du Conseil d'Etat, M. H...et autres demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de prendre acte de leur désistement de leurs conclusions tendant à l'annulation des délibérations nos 37/2015 et 38/2015 ;<br/>
<br/>
              3°) d'annuler la délibération n° 36/2015 ;<br/>
<br/>
              4°) de mettre à la charge de la commune de l'Ile-Rousse la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier :<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code électoral ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Jolivet, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que le désistement de M. H...et autres de leurs conclusions dirigées contre les délibérations nos 37/2015 et 38/2015 du 25 avril 2015 du conseil municipal de l'Ile-Rousse relatives à l'élection des membres de la commission d'appel d'offres et de la commission de délégation de service public est pur et simple ; que rien ne s'oppose à ce qu'il en soit donné acte ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 2121-33 du code général des collectivités territoriales : " Le conseil municipal procède à la désignation de ses membres ou de délégués pour siéger au sein d'organismes extérieurs dans les cas et conditions prévus par les dispositions du présent code et des textes régissant ces organismes (...) " ; qu'aux termes de l'article L. 2122-13 du même code : " L'élection du maire et des adjoints peut être arguée de nullité dans les conditions, formes et délais prescrits pour les réclamations contre les élections du conseil municipal " ; qu'en vertu de l'article L. 5211-2 de ce code, les dispositions du chapitre II du titre II du livre premier de la deuxième partie relatives aux maires et aux adjoints, dont font partie les deux articles précités, " sont applicables au président et aux membres de l'organe délibérant des établissements publics de coopération intercommunale, en tant qu'elles ne sont pas contraires aux dispositions du présent titre " ; qu'aux termes de l'article L. 123-1 du code de l'action sociale et des familles : " Le centre d'action sociale est un établissement public administratif communal (...)/Outre son président, le conseil d'administration comprend, pour le centre communal d'action sociale, des membres élus en son sein à la représentation proportionnelle par le conseil municipal (...) " ;<br/>
<br/>
              3. Considérant qu'il ne résulte ni des dispositions du code général des collectivités territoriales ni des textes régissant les centres communaux d'action sociale, qu'un litige relatif à la désignation par une commune de ses représentants dans les organes délibérants d'un tel organisme ressortisse en appel à la compétence du Conseil d'Etat ; que, par suite, le Conseil d'Etat n'est pas compétent pour statuer sur la requête de M. H...et autres tendant à l'annulation de l'ordonnance n° 150074 du 24 août 2015 du premier conseiller du tribunal administratif de Bastia, en tant qu'elle ne statue pas sur leur demande d'annulation de la délibération n° 36/2015 du 25 avril 2015 par laquelle le conseil municipal de l'Ile-Rousse a désigné les membres du conseil d'administration du centre d'action sociale ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède, qu'en vertu de la compétence d'appel de droit commun instituée par l'article L. 321-1 du code de justice administrative au bénéfice des cours administratives d'appel, il y a lieu d'attribuer à la cour administrative d'appel de Marseille le jugement des conclusions de la requête de M. H...et autres tendant à l'annulation de l'ordonnance attaquée en tant qu'elle n'a pas statué sur leurs conclusions tendant à l'annulation de la délibération n° 36/2015 du 25 avril 2015 du conseil municipal de l'Ile-Rousse ;<br/>
<br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de l'Ile-Rousse qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. H...et autres est renvoyée à la cour administrative d'appel de Marseille en tant qu'elle concerne la délibération n° 36/2015 du 25 avril 2015 du conseil municipal de l'Ile-Rousse relative à l'élection des membres du conseil d'administration du centre d'action sociale.<br/>
Article 2 : Il est donné acte du désistement de M. H...et autres de leurs conclusions de première instance dirigées contre les délibérations nos 37/2015 et 38/2015 du 25 avril 2015 du conseil municipal de l'Ile-Rousse relatives à l'élection des membres de la commission d'appel d'offres et de la commission de délégation de service public.<br/>
Article 3 : Les conclusions de M. H...et autres relatives à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. H...et à la commune de l'Ile-Rousse.<br/>
Copie en sera adressée au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
