<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026845838</ID>
<ANCIEN_ID>JG_L_2012_12_000000340419</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/84/58/CETATEXT000026845838.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 26/12/2012, 340419, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-12-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>340419</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP TIFFREAU, CORLAY, MARLANGE</AVOCATS>
<RAPPORTEUR>M. Olivier Gariazzo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:340419.20121226</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 10 juin 2010 au secrétariat du contentieux du Conseil d'État, présenté pour la société BNP Paribas, dont le siège est 16, boulevard des Italiens à Paris (75009), représentée par son président-directeur général ; la société BNP Paribas demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08PA03816 du 2 avril 2010 par lequel la cour administrative d'appel de Paris a rejeté les conclusions de sa requête tendant à l'annulation du jugement n° 0304607/1-2 du 20 mai 2008 du tribunal administratif de Paris rejetant sa demande tendant à la décharge des cotisations supplémentaires d'impôt sur les sociétés et de contributions additionnelles sur cet impôt mises à sa charge au titre des années 1995, 1996 et 1997, ainsi que des pénalités correspondantes, lui accordant seulement la réduction des sommes en cause à raison de l'impôt acquitté localement par la société Paribas Suisse Guernesey Ltd, conformément aux motifs de son arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à l'intégralité de son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'État le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Gariazzo, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Tiffreau, Corlay, Marlange, avocat de la Societe BNP Paribas,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Tiffreau, Corlay, Marlange, avocat de la Societe BNP Paribas ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces des dossiers soumis aux juges du fond qu'à l'issue d'une vérification de comptabilité, la SA BNP Paribas, venant aux droits de la SA Paribas, a été assujettie, au titre des exercices clos au cours des années 1995 à 1997, à des cotisations supplémentaires d'impôt sur les sociétés et de contributions additionnelles sur cet impôt ainsi qu'aux pénalités correspondantes, à raison de l'inclusion dans son bénéfice taxable, en application des dispositions de l'article 209 B du code général des impôts d'une fraction des bénéfices de la Société Paribas Suisse Guernesey Ltd, établie à Guernesey ; qu'après avoir vainement réclamé auprès de l'administration contre les impositions supplémentaires ainsi mises à sa charge, elle a saisi le tribunal administratif de Paris qui, par un jugement du 20 mai 2008, a rejeté sa demande ; que la cour administrative d'appel de Paris a, par un arrêt du 2 avril 2010, contre lequel elle se pourvoit en cassation, rejeté son appel contre ce jugement, lui accordant seulement la réduction des impositions et pénalités litigieuses, à hauteur d'un montant qu'elle a considéré être celui de l'impôt acquitté localement par la filiale ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 209 B du code général des impôts, dans sa rédaction applicable aux années d'imposition en litige : " I. Lorsqu'une entreprise passible de l'impôt sur les sociétés détient directement ou indirectement 25 % au moins des actions ou parts d'une société établie dans un État étranger ou un territoire situé hors de France dont le régime fiscal est privilégié au sens mentionné à l'article 238 A, cette entreprise est soumise à l'impôt sur les sociétés sur les résultats bénéficiaires de la société étrangère dans la proportion des droits sociaux qu'elle y détient. / Ces bénéfices font l'objet d'une imposition séparée. (...) / II. Les dispositions du I ne s'appliquent pas si l'entreprise établit que les opérations de la société étrangère n'ont pas principalement pour effet de permettre la localisation de bénéfices dans un État ou territoire où elle est soumise à un régime fiscal privilégié. Cette condition est réputée remplie notamment : / - lorsque la société étrangère a principalement une activité industrielle ou commerciale effective ; / et qu'elle réalise ses opérations de façon prépondérante sur le marché local " ; qu'aux termes de l'article 238 A du même code alors applicable : " (...) les personnes sont regardées comme soumises à un régime fiscal privilégié dans l'État ou le territoire considéré si elles n'y sont pas imposables ou si elles y sont assujetties à des impôts sur les bénéfices ou les revenus notablement moins élevés qu'en France " ;<br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions, éclairées par leurs travaux préparatoires, que le législateur a entendu dissuader les entreprises passibles en France de l'impôt sur les sociétés de localiser, pour des raisons principalement fiscales, une partie de leurs bénéfices, au travers de filiales, créées par elles ou par une de leurs filiales dans des pays ou territoires à régime fiscal privilégié au sens de l'article 238 A du même code ; qu'à cette fin, le premier alinéa du I de l'article 209 B du code général des impôts prévoit, dans sa rédaction applicable aux années d'imposition en litige, que dans le cas où une entreprise passible en France de l'impôt sur les sociétés détient, directement ou indirectement, au moins 25 % des actions ou parts d'une société implantée dans un État ou territoire à régime fiscal privilégié, elle est normalement soumise à l'impôt sur les sociétés sur les bénéfices de cette société, à proportion des droits qu'elle y détient ; qu'il n'en va autrement, de manière dérogatoire, que si l'entreprise démontre, ainsi que le prévoit le premier alinéa du II de l'article 209 B, que l'implantation de la filiale, détenue directement ou indirectement, dans un pays à régime fiscal privilégié n'a pas, pour la société mère, principalement pour objet d'échapper à l'impôt français ; que si une telle situation peut être présumée, c'est à la double condition qu'il soit démontré, d'une part, que la société étrangère exerce une activité industrielle ou commerciale effective, et, d'autre part, que les opérations qu'elle réalise dans le cadre de cette activité sont effectuées de manière prépondérante sur le marché local, cette dernière notion devant, en tant qu'elle autorise une dérogation à la règle de droit commun, être interprétée strictement ;<br/>
<br/>
              4. Considérant que si, pour juger qu'il n'était pas établi que la société Paribas Suisse Guernesey Ltd aurait exercé une activité industrielle ou commerciale effective réalisée de façon prépondérante sur le marché local, la cour administrative d'appel de Paris a étayé sa réfutation du caractère effectif de l'activité au moyen de plusieurs éléments non argués de dénaturation, en revanche, en se bornant à juger que la société requérante n'apportait aucun autre élément permettant d'établir que les opérations de cette filiale n'avaient pas principalement pour objet de permettre la localisation de bénéfices dans un État où elle était soumise à un régime fiscal privilégié, la cour a entaché son arrêt d'insuffisance de motivation ;<br/>
<br/>
              5. Considérant qu'il suit de là que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société BNP Paribas est fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans l'intérêt d'une bonne administration de la justice, de renvoyer l'affaire à la cour administrative d'appel de Versailles ;<br/>
<br/>
              7. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la société BNP Paribas au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 2 avril 2010 de la cour administrative d'appel de Paris est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : Le surplus des conclusions de la société BNP Paribas est rejeté.<br/>
Article 4 : La présente décision sera notifiée à la société BNP Paribas et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
