<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027362525</ID>
<ANCIEN_ID>JG_L_2013_04_000000355941</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/36/25/CETATEXT000027362525.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 26/04/2013, 355941, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-04-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>355941</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; HAAS</AVOCATS>
<RAPPORTEUR>Mme Agnès Martinel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:355941.20130426</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 18 janvier et 18 avril 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour le Comité interprofessionnel du vin de Champagne dont le siège social est situé 5, rue Henri-Martin, BP 135, à Epernay (51204 cedex) ; le comité demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 14 novembre 2011 relatif à l'indication géographique protégée " Coteaux de Coiffy " ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le règlement (CE) n° 1234/2007 du Conseil du 22 octobre 2007 ;<br/>
<br/>
              Vu le règlement (CE) n° 607/2009 de la Commission du 14 juillet 2009 ;<br/>
<br/>
              Vu le code rural et de la pêche maritime ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Agnès Martinel, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Waquet, Farge, Hazan, avocat du Comité Interprofessionnel du Vin de Champagne (CIVC) et de Me Haas, avocat de l'Institut national de l'origine et de la qualité, <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Waquet, Farge, Hazan, avocat du Comité Interprofessionnel du Vin de Champagne (CIVC) et de Me Haas, avocat de l'Institut national de l'origine et de la qualité ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du second alinéa de l'article R. 641-17 du code rural et de la pêche maritime : " L'arrêté homologuant le cahier des charges d'une indication géographique protégée relevant du règlement (CE) n° 1234 / 2007 du Conseil du 22 octobre 2007 portant organisation commune des marchés dans le secteur agricole et dispositions spécifiques en ce qui concerne certains produits de ce secteur (règlement " OCM unique ") est pris par les ministres chargés, respectivement, de l'agriculture, de la consommation et du budget. Il est fait mention de ces arrêtés au Journal officiel de la République française. " ;<br/>
<br/>
              2. Considérant que le b) du paragraphe 1 de l'article 118 ter du règlement auquel ces dispositions se réfèrent définit l'indication géographique comme : " une indication renvoyant à une région, à un lieu déterminé ou, dans des cas exceptionnels, à un pays, qui sert à désigner un produit (...) : / i) possédant une qualité, une réputation ou d'autres caractéristiques particulières attribuables à cette origine géographique (...) " ; que, selon le 2 de l'article 118 quater du même règlement : " Le cahier des charges permet aux parties intéressées de vérifier le respect des conditions de production associées à l'appellation d'origine ou à l'indication géographique./ Il comporte au minimum les éléments suivants : / (...) g) les éléments qui corroborent le lien visé (...) à l'article 118 ter, paragraphe 1, point b) i) " ; que, l'article 7 du règlement de la Commission du 14 juillet 2009 fixant certaines modalités d'application du règlement (CE) n° 479/2008 du Conseil en ce qui concerne les appellations d'origine protégées et les indications géographiques protégées, les mentions traditionnelles, l'étiquetage et la présentation de certains produits du secteur vitivinicole, dispose que : " 1. Les éléments qui corroborent le lien géographique (...) expliquent dans quelle mesure les caractéristiques de la zone géographique délimitée influent sur le produit final. (...) 3. Pour une indication géographique, le cahier des charges contient : a) des informations détaillées sur la zone géographique contribuant au lien ; / b) des informations détaillées sur la qualité, la réputation ou d'autres caractéristiques spécifiques du produit découlant de son origine géographique ; / c) une description de l'interaction causale entre les éléments visés au point a) et ceux visés au point b). / 4. Pour une indication géographique, le cahier des charges précise si l'indication se fonde sur une qualité ou une réputation spécifique ou sur d'autres caractéristiques liées à l'origine géographique. " ;<br/>
<br/>
              3. Considération que, par l'arrêté attaqué du 14 novembre 2011, les ministres compétents ont homologué le nouveau cahier des charges de l'indication géographique protégée " Côteaux de Coiffy ", proposé par l'Institut national de l'origine et de la qualité ; que si le Comité interprofessionnel du vin de Champagne demande au Conseil d'Etat l'annulation pour excès de pouvoir de cet arrêté, il ressort des termes de sa requête et notamment des moyens développés qu'il entend, en réalité, attaquer cet arrêté en tant seulement qu'il élargit aux " vins mousseux de qualité, rouges, rosés ou blancs " la possibilité de se prévaloir de l'indication géographique protégée " Côteaux de Coiffy " ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier que la production de vins mousseux n'est pas attestée pour une période antérieure à 1894 dans la zone géographique délimitée par le cahier des charges litigieux, mais l'est seulement à Soyers, commune du canton de Laferté-sur-Amance qui n'appartient pas à cette zone géographique ; qu'elle n'est davantage attestée ni dans cette zone géographique, ni dans la zone de proximité immédiate également définie par le cahier des charges litigieux pour la période postérieure à 1894 et à la crise du phylloxéra ; qu'ainsi l'existence d'un lien géographique, au sens des différentes dispositions rappelées plus haut et notamment de l'article 118 ter du règlement " OCM unique ", entre l'indication protégée " Côteaux de Coiffy " et les " vins mousseux de qualité, rouges, rosés ou blancs " n'est pas corroborée par les éléments de ce cahier des charges ; que, dans ces conditions, les ministres ont entaché leur décision d'une erreur manifeste d'appréciation en estimant que l'existence d'un lien géographique pouvait être établi entre l'aire géographique des " Côteaux de Coiffy " et la production de " vins mousseux de qualité, rouges, rosés ou blancs " ; qu'il suit de là que, sans qu'il soit besoin d'examiner les autres moyens de la requête, le Comité interprofessionnel du vin de Champagne est fondé à demander l'annulation de  l'arrêté attaqué en tant qu'il homologue les dispositions du cahier des charges litigieux qui autorisent les vins mousseux issus des zones qu'il définit à se prévaloir de cette indication protégée ;  <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser au Comité interprofessionnel du vin de Champagne, au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêté du 14 novembre 2011 relatif à l'indication géographique protégée " Coteaux de Coiffy " est annulé en tant qu'il homologue celles des dispositions du cahier des charges de cette indication géographique protégée relatives aux " vins mousseux de qualité, rouges, rosés ou blancs ".<br/>
Article 2 : L'Etat versera une somme de 3 000 euros au Comité interprofessionnel du vin de Champagne au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3: La présente décision sera notifiée au Comité interprofessionnel du vin de Champagne,  au ministre de l'économie et des finances, au ministre de l'agriculture, de l'agroalimentaire et de la forêt et à l'Institut national de l'origine et de la qualité. <br/>
Copie en sera adressée au syndicat des viticulteurs pour la défense des indications géographiques protégées " Franche-Comté ", " Haute-Marne " et " Côteaux de Coiffy ".<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
