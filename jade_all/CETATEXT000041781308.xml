<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041781308</ID>
<ANCIEN_ID>JG_L_2020_03_000000423693</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/78/13/CETATEXT000041781308.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 25/03/2020, 423693, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-03-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423693</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Paul Bernard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:423693.20200325</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Gaz réseau Distribution France (GRDF), devenue Engie, a demandé au tribunal administratif de Nantes de condamner la société Eurovia Atlantique à lui verser une somme de 9 354,16 euros, assortie des intérêts au taux légal et des intérêts capitalisés à compter de la date de réception de la mise en demeure de payer cette somme, en réparation du préjudice subi résultant des dommages causés par cette société à ses installations à l'occasion de travaux publics exécutés les 18 février 2013 et 19 juin 2014.<br/>
<br/>
              Par un jugement n° 1507560 du 26 juin 2018, le tribunal administratif a rejeté sa demande ainsi que les conclusions reconventionnelles présentées par la société Eurovia Atlantique.<br/>
<br/>
              Par un pourvoi et un mémoire complémentaire, enregistrés les 28 août et 26 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, la société Engie demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il a rejeté ses conclusions indemnitaires ; <br/>
<br/>
              2°) de mettre à la charge de la société Eurovia Atlantique la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Paul Bernard, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Celice, Texidor, Périer, avocat de la société Engie, et à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la société Eurovia Atlantique ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le 18 février 2013, alors qu'elle déposait une bordure de trottoir rue Pitre Chevalier, à Nantes, la société Eurovia Atlantique a provoqué la rupture d'une canalisation qui assurait la desserte en gaz d'un immeuble. Le 19 juin 2014, à l'occasion de travaux de terrassement boulevard Victor Hugo, à Château-Gontier, la même société a endommagé une canalisation de gaz souterraine. La société Gaz réseau Distribution France (GRDF), devenue la société Engie, a demandé en vain à la société Eurovia Atlantique l'indemnisation des préjudices subis du fait de ces dommages de travaux publics. La société Engie se pourvoit en cassation contre le jugement du 26 juin 2018 par lequel le tribunal administratif de Nantes a rejeté sa demande indemnitaire.<br/>
<br/>
              2. Après avoir reconnu l'existence d'un lien de causalité direct entre les dommages subis par la société GRDF et les travaux exécutés par la société Eurovia Atlantique et avoir admis l'engagement de la responsabilité de cette dernière, le tribunal a relevé, sans mettre en doute l'existence des préjudices ainsi causés, que la production de factures établies par la société GRDF ne permettait pas de justifier de leur montant et a rejeté pour ce seul motif les conclusions indemnitaires présentées devant lui. En statuant ainsi, alors que les factures litigieuses détaillaient les prestations fournies et que si cette modalité d'évaluation était contestée, il lui appartenait d'apprécier lui-même l'importance des préjudices indemnisables, en faisant usage, le cas échéant, de ses pouvoirs d'instruction, il a méconnu son office et commis une erreur de droit.<br/>
<br/>
              3. Il résulte de ce qui précède, et sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que la société Engie est fondée à demander l'annulation du jugement attaqué.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Eurovia Atlantique une somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce que soit mise à la charge de la société Engie, qui n'est pas, dans la présente instance, la partie perdante, la somme demandée par la société Eurovia Atlantique au même titre.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Nantes du 26 juin 2018 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Nantes. <br/>
Article 3 : La société Eurovia Atlantique versera à la société Engie une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la société Eurovia Atlantique au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société Engie et à la société Eurovia Atlantique.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
