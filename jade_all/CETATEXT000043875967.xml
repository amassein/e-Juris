<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043875967</ID>
<ANCIEN_ID>JG_L_2021_07_000000454493</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/87/59/CETATEXT000043875967.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 16/07/2021, 454493, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>454493</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:454493.20210716</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... et Mme D... C... épouse B... ont demandé au juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, d'enjoindre au ministre de l'intérieur et au consul général de France à Dakar de renouveler le passeport français de leur fille Gaëlle B..., dans un délai de deux jours à compter de l'ordonnance à intervenir, sous astreinte de 150 euros par jour de retard et, d'autre part, d'enjoindre à l'administration de produire au tribunal comme à leur conseil la preuve d'une telle délivrance dans le délai requis. Par une ordonnance n° 2114426 du 9 juillet 2021, le juge des référés du tribunal administratif de Paris a rejeté leur demande. <br/>
<br/>
              Par une requête, enregistrée le 13 juillet 2021 au secrétariat du contentieux du Conseil d'Etat, M. et Mme B... demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) d'annuler la décision du consul général de France en date du 3 mai 2021 refusant le renouvellement du passeport français de leur fille Gaëlle B... ;<br/>
<br/>
              3°) d'enjoindre au ministre de l'Europe et des affaires étrangères, dans un délai de deux jours à compter de la décision à intervenir, de renouveler le passeport français de leur fille, ou à défaut de lui délivrer un document de voyage lui permettant de se déplacer vers la France ;<br/>
<br/>
              4°) de fixer une astreinte de 150 euros par jour de retard pour l'exécution de cette injonction ; <br/>
<br/>
              5°) d'enjoindre à l'administration de produire au tribunal comme à leur conseil la preuve d'une telle délivrance dans le délai requis ;<br/>
<br/>
              6°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
              7°) de mettre à la charge de l'Etat les entiers dépens.  <br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
              - la condition d'urgence est satisfaite dès lors que, en premier lieu, la prise en charge de leur fille les 20 et 21 juillet 2021 à Montpellier par les médecins qui la suivent depuis 2011 pour le traitement d'une épilepsie pharmaco résistante constitue une urgence vitale, en deuxième lieu, la consultation par ces médecins tous les six mois est nécessaire et indispensable au suivi du traitement, en troisième lieu, son état de santé s'est dégradé dernièrement et, en dernier lieu, elle ne possède pas de passeport sénégalais et, si cela était possible, il n'y a aucune garantie que le consulat de France lui délivre de visa permettant de faire arriver l'enfant  à temps sur le territoire français ;<br/>
              - il est porté une atteinte grave et manifestement illégale à plusieurs libertés fondamentales ; <br/>
              - la décision de refus de renouvellement du passeport méconnaît la liberté d'aller et venir dès lors que, en premier lieu, leur fille est née française et bénéficie d'un passeport français depuis quinze ans, en deuxième lieu, elle possède une carte consulaire valable jusqu'au 12 avril 2023 et, en dernier lieu, le refus de délivrance de certificat n'est pas motivé par un doute sur la nationalité française mais par la non-production des pièces complémentaires de la part de ses parents ;<br/>
              - elle porte atteinte à l'intérêt supérieur de l'enfant dès lors que, en premier lieu, son état de santé nécessite des traitements appropriés, en deuxième lieu, il nécessite un déplacement en France au moins une fois tous les six mois pour le suivi, en troisième lieu, son dernier déplacement vers la France remonte au mois de février 2020 à raison de la situation pandémique et, en dernier lieu, son état de santé se dégrade ; <br/>
              - cette décision porte atteinte au droit au respect de la vie privée et familiale dès lors que la famille a choisi de vivre au Sénégal en tenant compte de la possibilité offerte par les médecins de Montpellier de dispenser un traitement approprié à leur fille à la condition d'examens médicaux pratiqués en France tous les six mois. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Déclaration des droits de l'homme et du citoyen ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - la convention internationale des droits de l'enfant ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. M et Mme B..., demeurant au Sénégal, demandent l'annulation de l'ordonnance en date du 9 juillet 2021 par laquelle le juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administratif, a rejeté leur demande tendant à ce que soit renouvelé le passeport français de leur fille Gaëlle née en 2004 ou à ce que lui soit octroyé un titre de voyage équivalent, afin de lui permettre de se rendre à divers rendez-vous médicaux prévus dans l'hôpital montpelliérain où elle est suivie pour épilepsie pharmaco-résistante depuis 2011.<br/>
<br/>
              3. Le juge des référés du tribunal administratif de Paris s'est fondé, pour rejeter leur demande, sur l'absence d'une situation d'urgence imminente compte tenu, en premier lieu, de ce que M. et Mme B... n'avaient fait diligence ni pour répondre aux demandes des tribunaux français pour établir la nationalité de leur fille, ni pour solliciter, avant son expiration, le renouvellement du passeport de Gaëlle, si bien que le certificat de nationalité française de celle-ci a été refusé par une décision du 14 février 2020 qu'ils n'ont pas contestée et le renouvellement de son passeport également refusé en juin 2021. Le juge des référés a tenu compte, en second lieu, de ce que les rendez-vous médicaux délivrés pour le 20 et le 21 juillet 2021 pouvaient être remplacés par une téléconsultation, ainsi que le médecin l'avait indiqué aux parents et comme ces rendez-vous périodiques l'avaient déjà été à plusieurs reprises. <br/>
<br/>
              4. M. et Mme B... pour attester de l'urgence de leur demande, contestent la réalité de leur manque de diligence à faire établir les documents attestant de la nationalité de leur fille et permettre l'octroi d'un passeport et font valoir que le rendez-vous médical du 21 juillet dans le service de neurochirurgie est destiné à " prévoir une intervention chirurgicale qui consiste en la pose d'une VNS ( vagal nerve stimulation ) qui ne se fait pas au Sénégal ", selon les termes d'une attestation établie, à leur demande, par le médecin traitant de leur fille le 12 juillet 2021. Il ressort cependant, en premier lieu, des pièces du dossier, notamment de la lettre adressée par Mme B... le 23 juin 2021 au garde des sceaux, ministre de la justice, qu'elle reconnaît ne pas avoir répondu à la demande de pièces que lui ont adressée en octobre 2018 les services chargés de la délivrance du certificat de nationalité française et il n'est pas, par ailleurs, contesté que la demande de renouvellement du passeport de la jeune fille a été faite après son expiration. Aucun document des services hospitaliers montpelliérains n'est, en second lieu, produit, de nature à établir que le rendez-vous en neurochirurgie fixé au 21 juillet ait pour objet non une simple consultation mais une opération immédiate, ni davantage qu'une telle opération revête une urgence vitale imminente. <br/>
<br/>
              5. Il résulte de ce qui précède que M. et Mme B... n'apportent aucun élément probant de nature à remettre en cause l'appréciation du juge des référés du tribunal administratif de Paris aux termes de laquelle les circonstances qu'ils invoquent ne sont pas constitutives d'une situation d'urgence imminente de nature à justifier l'application des dispositions de l'article L. 521-2 du code de justice administrative. Il s'ensuit qu'il est manifeste que l'appel de M. et Mme B... ne peut, dès lors, qu'être rejeté, y compris, par suite, les conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative, selon la procédure prévue par l'article L. 522-3 du même code. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. B... et Mme C... épouse B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. A... B..., premier requérant dénommé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
