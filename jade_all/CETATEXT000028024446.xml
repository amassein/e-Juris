<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028024446</ID>
<ANCIEN_ID>JG_L_2013_10_000000366884</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/02/44/CETATEXT000028024446.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 02/10/2013, 366884</TITRE>
<DATE_DEC>2013-10-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366884</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>FOUSSARD</AVOCATS>
<RAPPORTEUR>M. Gaël Raimbault</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2013:366884.20131002</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'arrêt n° 12DA00218 du 7 mars 2013, enregistré le 18 mars 2013 au secrétariat du contentieux du Conseil d'Etat, par lequel la cour administrative d'appel de Douai, avant de statuer sur l'appel de l'association chrétienne de réadaptation tendant, en premier lieu, à l'annulation du jugement n° 0804191 du 7 décembre 2011 par lequel le tribunal administratif de Lille a rejeté sa demande tendant à l'annulation de l'arrêté du 7 avril 2008 par lequel le préfet du Nord lui a ordonné de verser à l'association d'action éducative et sociale et à l'association des Flandres pour l'éducation, la formation des jeunes et l'insertion sociale et professionnelles les sommes respectives de 192 011,95 euros et de 90 358,57 euros correspondant aux amortissements cumulés au 31 octobre 2006 et aux réserves de trésorerie et de compensation et, en second lieu, à l'annulation de cet arrêté dans la mesure où il ordonne le reversement des sommes correspondant aux amortissements cumulés, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen les questions suivantes :<br/>
<br/>
              1°) Les dispositions du premier alinéa de l'article R. 314-97 du code de l'action sociale et des familles prévoyant le reversement des amortissements cumulés en cas de fermeture d'un établissement ou d'un service trouvent-elles leur fondement dans celles de l'article L. 313-19 du même code déterminant les sommes dont le reversement est exigé dans une telle hypothèse ou bien dans d'autres dispositions législatives de ce code, et notamment celles des articles L. 314-3 à L. 314-13 relatives aux règles budgétaires et de financement '<br/>
<br/>
              2°) En cas de réponse négative, le pouvoir réglementaire était-il compétent pour prévoir le reversement des amortissements cumulés '<br/>
<br/>
              3°) En cas de réponse positive à la question précédente, le reversement des amortissements cumulés est-il de nature à porter nécessairement atteinte au droit de propriété '<br/>
              Vu les observations, enregistrées le 30 mai 2013, présentées pour l'association chrétienne de réadaptation ;<br/>
<br/>
              Vu les observations, enregistrées le 7 juin 2013, présentées par le ministre des affaires sociales et de la santé ;<br/>
<br/>
              Vu les nouvelles observations, enregistrées le 1er juillet 2013, présentées pour l'association chrétienne de réadaptation ;<br/>
<br/>
              Vu les nouvelles observations, enregistrées le 22 juillet 2013, présentées par le ministre des affaires sociales et de la santé ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu la Constitution, notamment son Préambule et son article 34 ;<br/>
<br/>
              Vu le code de l'action sociale et des familles ;<br/>
<br/>
              Vu le code de justice administrative, notamment son article L. 113-1 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Gaël Raimbault, Maître des Requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              - La parole ayant été donnée, avant et après les conclusions, à Me Foussard, avocat de l'association chrétienne de réadaptation ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>REND L'AVIS SUIVANT :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article R. 314-97 du code de l'action sociale et des familles : " En cas de fermeture ou de cessation d'activité totale ou partielle d'un établissement ou d'un service, si les frais financiers, les dotations aux comptes d'amortissement et aux comptes de provisions, les dotations au compte de réserve de trésorerie et les annuités d'emprunt contractées en vue de la constitution d'un fonds de roulement ont été pris en compte dans la fixation des tarifs, l'organisme gestionnaire reverse à un établissement ou service poursuivant un but similaire les montants des amortissements cumulés des biens, des provisions non utilisées et des réserves de trésorerie apparaissant au bilan de clôture ".<br/>
<br/>
              2. En premier lieu, aux termes de l'article L. 313-19 du code de l'action sociale et des familles : " En cas de fermeture définitive d'un établissement ou d'un service géré par une personne morale de droit public ou de droit privé celle-ci reverse à une collectivité publique ou à un établissement privé poursuivant un but similaire les sommes affectées à l'établissement ou service fermé, apportées par l'Etat, les collectivités territoriales et leurs établissements publics ou par les organismes de sécurité sociale, énumérées ci-après : / 1° Les subventions d'investissement non amortissables, grevées de droits, ayant permis le financement de l'actif immobilisé de l'établissement ou du service. Ces subventions sont revalorisées selon des modalités fixées par décret ; / 2° Les réserves de trésorerie de l'établissement ou du service constituées par majoration des produits de tarification et affectation des excédents d'exploitation réalisés avec les produits de la tarification ; / 3° Des excédents d'exploitation provenant de la tarification affectés à l'investissement de l'établissement ou du service, revalorisés dans les conditions prévues au 1° ; / 4° Les provisions pour risques et charges, les provisions réglementées et les provisions pour dépréciation de l'actif circulant constituées grâce aux produits de la tarification et non employées le jour de la fermeture. / La collectivité publique ou l'établissement privé attributaire des sommes précitées peut être : / a) Choisi par le gestionnaire de l'établissement ou du service fermé, avec l'accord du préfet du département du lieu d'implantation de cet établissement ou service ; / b) Désigné par le préfet du département, en cas d'absence de choix du gestionnaire ou de refus par le préfet du choix mentionné au a. / L'organisme gestionnaire de l'établissement ou du service fermé peut, avec l'accord de l'autorité de tarification concernée, s'acquitter des obligations prévues aux 1° et 3° en procédant à la dévolution de l'actif net immobilisé de l'établissement ou du service ".<br/>
<br/>
              3. En tant qu'il prévoit qu'en cas de fermeture ou de cessation d'activité d'un établissement ou service, si les dotations aux comptes d'amortissement ont été prises en compte dans la fixation des tarifs, l'organisme gestionnaire de ce dernier doit reverser à un autre établissement ou service les montants des amortissements cumulés des biens tels qu'ils apparaissent au bilan de clôture, l'article R. 314-97 du code de l'action sociale et des familles ne peut être regardé comme pris sur le fondement de l'article L. 313-19 du même code, qui ne mentionne pas les amortissements cumulés parmi les sommes devant être reversées en cas de fermeture définitive d'un établissement ou d'un service.<br/>
<br/>
              4. En second lieu, il résulte des dispositions des articles L. 314-1 et suivants du code de l'action sociale et des familles relatives à la tarification des établissements et services sociaux et médico-sociaux que cette tarification vise à assurer le financement par l'Etat, les collectivités territoriales ou les organismes de sécurité sociale de certaines des prestations fournies par ces établissements et services. Eu égard à l'objet de la tarification, le pouvoir réglementaire a compétence pour prévoir les conditions dans lesquelles, en cas de fermeture ou de cessation d'activité, les sommes procurées par la tarification qui n'auraient pas été utilisées pour la fourniture des prestations en vue desquelles elles avaient été allouées doivent être reversées à un établissement ou un service poursuivant un but similaire. <br/>
<br/>
              5. Toutefois, les amortissements ont pour objet, d'une part, de constater la dépréciation des biens immobilisés et, d'autre part, de répartir le coût de ces biens sur leur durée probable d'utilisation. Par suite, la prise en compte des dotations aux comptes d'amortissement dans la fixation des tarifs d'un établissement ou d'un service correspond, en principe, à l'intégration du coût des immobilisations dans le calcul du coût des prestations fournies par cet établissement ou ce service, sans qu'il en résulte, en règle générale, d'accroissement du patrimoine de l'organisme qui en assure la gestion. Dès lors, le pouvoir réglementaire n'avait pas compétence pour prévoir qu'en cas de fermeture ou de cessation d'activité d'un établissement ou service, l'organisme gestionnaire de ce dernier doit reverser l'intégralité des montants des amortissements cumulés des biens tels qu'ils apparaissent au bilan de clôture si les dotations aux comptes d'amortissement ont été prises en compte dans la fixation des tarifs.<br/>
<br/>
              6. Compte tenu de la réponse apportée aux deux premières questions posées par la cour administrative d'appel de Douai, il n'y a pas lieu de répondre à sa troisième question.<br/>
<br/>
              Le présent avis sera notifié à la cour administrative d'appel de Douai, à l'association chrétienne de réadaptation et à la ministre des affaires sociales et de la santé.<br/>
<br/>
              Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-03-01 AIDE SOCIALE. INSTITUTIONS SOCIALES ET MÉDICO-SOCIALES. ÉTABLISSEMENTS - QUESTIONS COMMUNES. - FERMETURE OU CESSATION D'ACTIVITÉ - INCOMPÉTENCE DU POUVOIR RÉGLEMENTAIRE POUR PRÉVOIR QUE L'ORGANISME GESTIONNAIRE DE L'ÉTABLISSEMENT DOIT REVERSER L'INTÉGRALITÉ DES MONTANTS DES AMORTISSEMENTS CUMULÉS DES BIENS TELS QU'ILS APPARAISSENT AU BILAN DE CLÔTURE SI LES DOTATIONS AUX COMPTES D'AMORTISSEMENT ONT ÉTÉ PRISES EN COMPTE DANS LA FIXATION DES TARIFS (PREMIER ALINÉA DE L'ARTICLE R. 314-97 DU CASF).
</SCT>
<ANA ID="9A"> 04-03-01 Dès lors que les amortissements ont pour objet, d'une part, de constater la dépréciation des biens immobilisés et, d'autre part, de répartir le coût de ces biens sur leur durée probable d'utilisation, la prise en compte des dotations aux comptes d'amortissement dans la fixation des tarifs d'un établissement ou d'un service correspond, en principe, à l'intégration du coût des immobilisations dans le calcul du coût des prestations fournies par cet établissement ou ce service, sans qu'il en résulte, en règle générale, d'accroissement du patrimoine de l'organisme qui en assure la gestion. Dès lors, le pouvoir réglementaire n'avait pas compétence pour prévoir, au premier alinéa de l'article R. 314-97 du code de l'action sociale et des familles (CASF), qu'en cas de fermeture ou de cessation d'activité d'un établissement ou service, l'organisme gestionnaire de ce dernier doit reverser l'intégralité des montants des amortissements cumulés des biens tels qu'ils apparaissent au bilan de clôture si les dotations aux comptes d'amortissement ont été prises en compte dans la fixation des tarifs.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
