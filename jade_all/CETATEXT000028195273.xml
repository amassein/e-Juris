<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028195273</ID>
<ANCIEN_ID>JG_L_2013_11_000000360783</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/19/52/CETATEXT000028195273.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 13/11/2013, 360783, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-11-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360783</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:360783.20131113</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 5 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par M. A...B..., demeurant ... ; M. B...demande au Conseil d'Etat d'annuler pour excès de pouvoir l'arrêté du 26 juin 2012 relatif à l'autorisation d'animer les stages de sensibilisation à la sécurité routière, en tant qu'il ne prévoit pas l'intégration du " DESS / Master de management de la sécurité routière " dans la liste des diplômes donnant accès à la formation initiale des animateurs chargés des stages de sensibilisation à la sécurité routière ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la route ;<br/>
<br/>
              Vu la loi n° 2007-797 du 5 mars 2007 ; <br/>
<br/>
              Vu le décret n° 2009-1678 du 29 décembre 2009 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, Auditeur,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 212-2 du code de la route, issu de l'article 23 de la loi du 5 mars 2007 relative à la prévention de la délinquance : " Nul ne peut être autorisé à animer des stages de sensibilisation à la sécurité routière s'il ne satisfait aux conditions suivantes : / (...) / 2° Remplir des conditions précisées par décret en Conseil d'Etat, relatives à la détention d'un permis de conduire, à l'âge, à l'aptitude physique et aux formations suivies " ; qu'en application de ces dispositions, le décret du 29 décembre 2009 relatif à l'enseignement de la conduite et à l'animation de stages de sensibilisation à la sécurité routière a modifié l'article R. 212-2 du même code afin, notamment, de préciser les conditions de titres et de diplômes imposées aux demandeurs d'une autorisation d'animer les stages de sensibilisation à la sécurité routière ; que, dans sa rédaction en vigueur à la date de l'arrêté attaqué, l'article        R. 212-2 prévoit que : " I.- L'autorisation d'enseigner la conduite et la sécurité routière est délivrée aux personnes remplissant les conditions suivantes : / 1° Etre titulaire d'un des titres ou diplômes mentionnés à l'article R. 212-3 (...) / II.- L'autorisation d'animer les stages de sensibilisation à la sécurité routière est délivrée aux personnes remplissant les conditions suivantes : / - soit être titulaire de l'autorisation d'enseigner mentionnée au I du présent article et d'un diplôme complémentaire dans le domaine de la formation à la sécurité routière figurant sur une liste fixée par arrêté du ministre chargé de la sécurité routière ; / - soit être titulaire d'un diplôme permettant de faire usage du titre de psychologue et du permis de conduire dont le délai probatoire fixé à l'article L. 223-1 est expiré ; / - et, dans les deux cas, être âgé d'au moins vingt-cinq ans et être titulaire d'une attestation de suivi de formation initiale à l'animation de stages de sensibilisation à la sécurité routière délivrée dans les conditions fixées par arrêté du ministre chargé de la sécurité routière. / III.- Un arrêté du ministre chargé de la sécurité routière définit les conditions d'application du présent article. " ; que M. B...demande l'annulation pour excès de pouvoir de l'arrêté du 26 juin 2012 relatif à l'autorisation d'animer les stages de sensibilisation à la sécurité routière, en tant qu'il ne prévoit pas l'intégration du " DESS / Master de management de la sécurité routière " dans la liste des diplômes qui, aux termes de l'annexe I de l'arrêté, donnent accès à la formation initiale des animateurs chargés des stages de sensibilisation à la sécurité routière ;<br/>
<br/>
              2. Considérant, en premier lieu, que lorsque l'autorité compétente demande, sans y être légalement tenue, l'avis d'un organisme consultatif sur un projet de texte, elle doit procéder à cette consultation dans des conditions régulières ; que néanmoins, elle conserve, dans cette hypothèse, la faculté d'apporter au projet, après consultation, toutes les modifications qui lui paraissent utiles, quelle qu'en soit l'importance, sans être dans l'obligation de saisir à nouveau cet organisme ; qu'il ressort des pièces du dossier que le projet d'arrêté a été soumis, sans qu'aucun texte ne le requière, au comité de suivi des stages de formation des conducteurs infractionnistes ; que si, dans le cadre des discussions menées au sein de cette instance, avait été envisagée l'inclusion du " DESS/Master management de la sécurité routière " dans la liste établie en annexe I du projet d'arrêté, le requérant n'est en tout état de cause pas fondé à se prévaloir de ce que des modifications ont été apportées à ce projet après la consultation de cette instance pour soutenir que les dispositions litigieuses seraient intervenues à la suite d'une procédure irrégulière, dès lors que cette consultation n'avait pas un caractère obligatoire ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'en vertu des dispositions rappelées au point 1, le ministre chargé de la sécurité routière était notamment compétent pour fixer par arrêté la liste des diplômes complémentaires dans le domaine de la formation à la sécurité routière requis par le deuxième alinéa du II de l'article R. 212-2 du code de la route ; que cette habilitation n'impliquait toutefois pas, contrairement à ce qui est soutenu, que l'auteur de l'arrêté attaqué fût tenu d'inscrire sur cette liste tous les diplômes délivrés en matière de sécurité routière ; <br/>
<br/>
              4. Considérant, en dernier lieu, qu'il ressort des pièces du dossier que le pouvoir réglementaire a cherché à renforcer les qualifications attendues des personnes exerçant les fonctions d'animateurs de stage de sensibilisation à la sécurité routière, de sorte que celles-ci disposent notamment de compétences pédagogiques suffisantes pour assurer les formations concernées ; qu'eu égard au contenu du " DESS/Master de management de la sécurité routière ", qui est principalement consacré à la gestion des politiques publiques en matière de sécurité routière et à la gestion de projets, et non à l'animation de stages à destination des conducteurs de véhicules à moteur, le ministre chargé de la sécurité routière n'a pas commis d'erreur manifeste d'appréciation ni méconnu le principe d'égalité en ne retenant pas cette formation sur la liste des diplômes complémentaires dans le domaine de la formation à la sécurité routière au sens de l'article R. 212-2 du code de la route ;<br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin de statuer sur la fin de non-recevoir présentée par le ministre de l'intérieur, que la requête de M. B... doit être rejetée ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
