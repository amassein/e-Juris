<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044806256</ID>
<ANCIEN_ID>JG_L_2021_12_000000453463</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/80/62/CETATEXT000044806256.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 30/12/2021, 453463, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>453463</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Thomas Janicot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:453463.20211230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              1° Par une protestation, enregistrée le 9 juin 2021 au secrétariat du contentieux du Conseil d'Etat sous le n° 453463, M.  N... L... demande au Conseil d'Etat d'annuler les opérations électorales qui se sont tenues le 30 mai 2021 en vue de l'élection des conseillers des Français de l'étranger pour la circonscription du Portugal.<br/>
<br/>
              M. M... L... soutient que le refus, irrégulièrement opposé par le ministre de l'Europe et des affaires étrangères, de diffuser aux électeurs sa circulaire électorale a altéré la sincérité du scrutin. <br/>
<br/>
              Par un mémoire en défense, enregistré le 11 août 2021, M. H... E... conclut au rejet de la protestation de M. M... L.... Il soutient que les griefs invoqués par M. M... L... ne sont pas fondés. <br/>
<br/>
              Par un mémoire en défense, enregistré le 23 août 2021, M. C... D... conclut au rejet de la protestation de M. M... L.... Il soutient que les griefs invoqués par M. M... L... ne sont pas fondés. <br/>
              Par un mémoire en défense, enregistré le 7 septembre 2021, M. F... J... conclut au rejet de la protestation de M. M... L.... Il soutient que les griefs invoqués par M. M... L... ne sont pas fondés. <br/>
<br/>
              Le ministre de l'Europe et des affaires étrangères a produit des observations, enregistrées le 13 juillet 2021. <br/>
<br/>
              La protestation a été communiquée à M. K... G... et à M. A... I..., qui n'ont pas produit d'observations.<br/>
<br/>
<br/>
<br/>
              2° Par une protestation, enregistrée le 9 juin 2021 au secrétariat du contentieux du Conseil d'Etat sous le n° 453468, M. A... I... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'élection de M. F... J... en qualité de conseiller des Français de l'étranger acquise à la suite des opérations électorales qui se sont tenues le 30 mai 2021 en vue de l'élection des conseillers des Français de l'étranger pour la circonscription du Portugal ; <br/>
<br/>
              2°) à titre subsidiaire, d'annuler ces opérations électorales.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              3° Par une ordonnance n° 2112261 du 10 juin 2021, enregistrée le 11 juin 2021 au secrétariat du contentieux du Conseil d'Etat sous le n° 453536, le président du tribunal administratif de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la protestation, enregistrée le 8 juin 2021 au greffe de ce tribunal, présentée par M. K... G....<br/>
<br/>
              Par cette protestation et un nouveau mémoire, enregistré le 26 juillet 2021, M. G... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les opérations électorales qui se sont tenues le 30 mai 2021 en vue de l'élection des conseillers des Français de l'étranger pour la circonscription du Portugal ; <br/>
<br/>
              2°) d'enjoindre qu'il soit organisé un nouveau scrutin dans les plus brefs délais. <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la loi organique n°76-97 du 31 janvier 1976 ; <br/>
              - le code électoral ;<br/>
              - la loi n° 2013-659 du 22 juillet 2013 ;<br/>
              - le décret n° 2014-290 du 4 mars 2014 ;<br/>
              - le décret n° 2021-231 du 26 février 2021 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Janicot, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Les protestations de M. N... L..., de M. A... I... et de M. K... G... sont dirigées contre les opérations électorales qui se sont déroulées le 30 mai 2021 en vue de l'élection des conseillers des Français de l'étranger dans la circonscription électorale du Portugal. Il y a lieu de les joindre pour statuer par une seule décision. <br/>
<br/>
              2. A l'issue de ces opérations électorales, la liste " Français du Portugal - Union de centre-droit. Une équipe pour vous servir du Nord au Sud du Portugal. Grâce à notre expérience nous défendons mieux vos intérêts. Nous sommes impliqués de longue date au Portugal avec les principales associations ", conduite par M. H... E..., a recueilli 1 234 voix et obtenu deux sièges de conseiller des Français de l'étranger. La liste " Rassemblement national Portugal ", conduite par M. C... D..., a recueilli 493 voix et obtenu un siège de conseiller des Français de l'étranger. La liste " CAP - Citoyens au Portugal ", conduite par M. F... J... a recueilli 455 voix et obtenu un siège de conseiller des Français de l'étranger. La liste " Français, j'ai choisi le Portugal ! ", conduite par M. A... I..., la liste " Agissons ensemble (Mouvement citoyen pour l'écologie et la solidarité) soutenu par EELV, pour les Français du Portugal ", conduite par M. K... G..., et la liste " Français du monde, Citoyens solidaires au Portugal ", conduite par M. N... L..., ont recueilli respectivement 455, 452 et 334 voix et n'ont obtenu aucun siège de conseiller des Français de l'étranger.<br/>
<br/>
              Sur les conclusions tendant à l'annulation des opérations électorales et à l'annulation de l'élection de M. F... J... :<br/>
<br/>
              En ce qui concerne les règles relatives à la propagande électorale :<br/>
<br/>
              3. En premier lieu, aux termes du 2° du I de l'article 21 de la loi du 22 juillet 2013 relative à la représentation des Français établis hors de France : " Chaque candidat ou liste de candidats peut transmettre au ministre des affaires étrangères une circulaire électorale afin qu'elle soit mise à disposition et transmise aux électeurs sous une forme dématérialisée ". Aux termes de l'article 4 du décret du 4 mars 2014 portant dispositions électorales relatives à la représentation des Français établis hors de France : " Les circulaires dématérialisées prévues au I de l'article 21 de la loi du 22 juillet 2013 susvisée sont transmises au ministre des affaires étrangères au plus tard le quatrième lundi qui précède le jour de l'élection. Un arrêté du ministre des affaires étrangères fixe les caractéristiques techniques auxquelles elles doivent se conformer ainsi que leurs modalités de transmission. / A compter de la date d'ouverture de la campagne électorale, elles sont mises en ligne sur les sites internet des ambassades et des postes consulaires et téléchargeables depuis un lien envoyé aux électeurs à l'adresse électronique qu'ils ont communiquée à l'administration. / Les circulaires transmises postérieurement à la date prévue au premier alinéa ou qui ne respectent pas les caractéristiques techniques arrêtées en application du même alinéa ne sont pas mises à disposition des électeurs et ne leur sont pas transmises ". <br/>
<br/>
              4. Il résulte de l'instruction, qu'en application de ces dispositions, chaque candidat ou liste de candidats de la circonscription du Portugal avait jusqu'au 3 mai 2021, 18 heures, heure de Paris, pour transmettre par courriel au ministre de l'Europe et des affaires étrangères, sur une adresse électronique dédiée, la circulaire dématérialisée mentionnée au I de l'article 23 de la loi du 22 juillet 2013. La liste " Français du monde, Citoyens solidaires au Portugal " n'ayant pas transmis sa circulaire dématérialisée au ministre de l'Europe et des affaires étrangères dans les délais impartis, cette même autorité a pu, conformément aux dispositions de l'article 4 du décret du 4 mars 2014 citées au point 6, refuser de transmettre et de mettre à disposition des électeurs la circulaire électorale de cette liste, M. M... L... ne pouvant utilement se prévaloir de ce que l'envoi tardif de sa circulaire au ministère de l'Europe et des affaires étrangères procèderait d'une erreur d'adressage électronique.<br/>
<br/>
              5. En deuxième lieu, si la liste " Français du monde, Citoyens solidaires au Portugal " a adressé un courriel à tous les électeurs de la circonscription le vendredi 28 mai 2021 à 21h, dont un passage indiquait que " le vote par urne était clos " alors que ce dernier était ouvert le dimanche 30 mai 2021, cette seule mention n'a pas pu induire les électeurs en erreur sur la date du vote à l'urne, alors qu'il résulte de l'instruction, d'une part, que le même courriel rappelait à plusieurs reprises que le vote à l'urne était ouvert le 30 mai 2021 et, d'autre part, que cette date figurait sur les convocations aux élections adressées aux électeurs par les autorités diplomatiques et consulaires françaises au Portugal ainsi que sur l'ensemble des supports de communication déployés par celles-ci dans le cadre des élections des conseillers des Français de l'étranger. Par suite, cette mention, qui a le caractère d'une erreur matérielle, n'a pas été de nature à altérer la sincérité du scrutin.<br/>
<br/>
              6. En troisième lieu, aux termes de l'article L. 49 du code électoral, rendu applicable à l'élection des conseillers des Français de l'étranger par l'article 15 de la loi du 22 juillet 2013 relative à la représentation des Français établis hors de France : " A partir de la veille du scrutin à zéro heure, il est interdit de : 1° Distribuer ou faire distribuer des bulletins, circulaires et autres documents ; 2° Diffuser ou faire diffuser par tout moyen de communication au public par voie électronique tout message ayant le caractère de propagande électorale (...) ". La date du scrutin à prendre en considération pour l'application de ces dispositions aux élections des conseillers des Français de l'étranger est celle fixée pour le scrutin à l'urne.<br/>
<br/>
              7. D'une part, M. G... soutient que la liste " Français du monde, Citoyens solidaires au Portugal " a méconnu les dispositions de l'article L. 49 du code électoral en adressant à une électrice, le samedi 29 mai 2021 à 0h50, soit la veille du vote à l'urne, un courriel intitulé " 6 bonnes raisons de voter Français du monde, citoyens solidaires au Portugal ". Toutefois, eu égard à la nature du courriel en cause et à son contenu, exempt de tout élément nouveau de polémique électorale, sa réception par une seule électrice après la fin de la campagne électorale n'a pas été de nature à altérer les résultats du scrutin.<br/>
<br/>
              8. D'autre part, si M. I... soutient que la liste " CAP-Citoyens au Portugal " a méconnu les dispositions de l'article L. 49 du code électoral en diffusant, la veille du vote à l'urne, sur le réseau social Facebook une interview donnée au média " Le Petit Journal " par sa tête de liste, M. F... J..., il n'apporte aucun élément démontrant que la publication litigieuse aurait été diffusée après la fin de la campagne électorale.<br/>
<br/>
              En ce qui concerne les listes électorales consulaires : <br/>
<br/>
              9. Aux termes de l'article 6 de la loi organique du 31 janvier 1976 relative aux listes électorales consulaires et au vote des Français établis hors de France pour l'élection du Président de la République : " Les listes électorales consulaires sont permanentes. Les demandes d'inscription sur ces listes, en vue de participer à un scrutin, sont déposées au plus tard le sixième vendredi précédant ce scrutin dans la circonscription consulaire dans laquelle est établi le demandeur ". Aux termes du II de l'article 9 de la même loi : " Toute personne qui prétend avoir été omise de la liste électorale consulaire en raison d'une erreur purement matérielle ou avoir été radiée en méconnaissance de l'article 7 peut saisir le tribunal judiciaire de Paris, qui a compétence pour statuer jusqu'au jour du scrutin (...) ". Aux termes de l'article L. 30 du code électoral, rendu applicable aux élections des conseillers des Français de l'étranger par l'article 9-1 de la loi organique du 31 janvier 1976 relative aux listes électorales consulaires et au vote des Français établis hors de France pour l'élection du Président de la République : " Par dérogation à l'article L. 17, peuvent demander à être inscrits sur la liste électorale de la commune entre le sixième vendredi précédant le scrutin et le dixième jour précédant ce scrutin : 1° Les fonctionnaires et agents des administrations publiques mutés ou admis à faire valoir leurs droits à la retraite après la clôture des délais d'inscription ainsi que les membres de leur famille domiciliés avec eux à la date de la mutation ou de la mise à la retraite ; 2° Les militaires renvoyés dans leurs foyers après avoir satisfait à leurs obligations légales d'activité, libérés d'un rappel de classe ou démobilisés après la clôture des délais d'inscription, ainsi que ceux ayant changé de domicile lors de leur retour à la vie civile ; 2° bis Les personnes qui établissent leur domicile dans une autre commune pour un motif professionnel autre que ceux visés aux 1° et 2° après la clôture des délais d'inscription, ainsi que les membres de leur famille domiciliés avec elles à la date du changement de domicile ; 3° Les Français et Françaises remplissant la condition d'âge exigée pour être électeur, après la clôture des délais d'inscription ; 4° Les Français et Françaises qui ont acquis la nationalité française par déclaration ou manifestation expresse de volonté et été naturalisés après la clôture des délais d'inscription ; 5° Les Français et les Françaises ayant recouvré l'exercice du droit de vote dont ils avaient été privés par l'effet d'une décision de justice ".<br/>
<br/>
              10. En premier lieu, il ne résulte pas de l'instruction que la variation entre le nombre d'inscrits sur les listes électorales consulaires ayant servi au scrutin et, d'une part, le nombre des inscrits figurant sur les listes communiquées par l'ambassade de France au Portugal et, d'autre part, le nombre des inscrits figurant sur le procès-verbal de vote électronique, critiquée par M. I..., ait constitué une manœuvre destinée à fausser les résultats du scrutin. Par suite, M. I... ne saurait utilement soutenir que les listes électorales consulaires ayant servi au scrutin seraient entachées d'irrégularité. <br/>
<br/>
              11. En second lieu, si M. I... soutient qu'un électeur, M. B..., n'a pas pu participer au scrutin, alors pourtant qu'il avait reçu une convocation à cette fin par les services de l'ambassade de France au Portugal, cette circonstance n'est pas constitutive d'une irrégularité dès lors que cet électeur, qui a demandé son inscription sur les listes électorales trois jours après le délai de clôture des inscriptions et ne justifiait d'aucun motif prévu par l'article L. 30 du code électoral lui permettant de s'inscrire après ce délai, n'était pas inscrit sur les listes électorales et ne pouvait donc pas prendre part au scrutin.<br/>
<br/>
              En ce qui concerne le déroulement du scrutin : <br/>
<br/>
              12. En premier lieu, s'il résulte de l'instruction qu'au moins cinq électeurs ont rencontré des difficultés pour participer au vote par voie électronique organisé entre le 21 et le 26 mai 2021, M. I... n'établit toutefois pas, qu'elles seraient imputables à des dysfonctionnements touchant l'organisation du scrutin, alors notamment que le ministre de l'Europe et des affaires étrangères fait valoir, sans être contredit, qu'une plateforme d'assistance sept jours sur sept au vote électronique ayant traité près de dix mille demandes a été mise en place par le ministère et qu'une campagne de communication destinée à expliquer les modalités du vote électronique, notamment vis-à-vis des jeunes majeurs, a été conduite dans le cadre des élections des conseillers des Français de l'étranger.<br/>
<br/>
              13. En deuxième lieu, si M. I... soutient que le consulat honoraire de Tavira a induit en erreur certains électeurs et les a empêchés de voter en indiquant que le bureau de vote de Faro était situé au sein de l'Alliance Française, alors que le bureau de vote se situait au Business Center de la ville, il n'assortit ses allégations d'aucun élément probant. <br/>
<br/>
              14. En troisième lieu, si les sites internet du ministère de l'Europe et des affaires étrangères et des postes diplomatiques au Portugal ne communiquaient pas aux électeurs, pour des raisons de sécurité, les adresses de chaque bureau de vote, il résulte de l'instruction, d'une part, que ces adresses figuraient sur les convocations aux élections des conseillers des Français de l'étranger envoyées en format papier et électronique par les postes diplomatiques français au Portugal à tous les électeurs, et, d'autre part, que chaque électeur disposait de la possibilité d'interroger directement les autorités diplomatiques françaises pour connaitre l'adresse de son bureau de vote. Par suite, la circonstance que l'adresse des bureaux de vote de Faro et de Porto n'aurait pas été diffusée aux électeurs sur le site internet des postes diplomatiques français au Portugal ne constitue pas une irrégularité de nature à altérer les résultats du scrutin.<br/>
<br/>
              15. En quatrième lieu, aux termes du troisième alinéa de l'article L. 65 du code électoral : " (...) Si une enveloppe contient plusieurs bulletins, le vote est nul quand les bulletins portent des listes et des noms différents. Les bulletins multiples ne comptent que pour un seul quand ils désignent la même liste, le même binôme de candidats ou le même candidat (...) ". <br/>
<br/>
              16. Si M. I... fait valoir qu'une enveloppe comportant plusieurs bulletins de la même liste a été considérée comme un vote valablement émis par un bureau de vote, cette circonstance ne constitue pas une irrégularité dès lors qu'il résulte des dispositions citées au point 15 que les bulletins multiples comptent pour un seul quand ils désignent la même liste. <br/>
<br/>
              17. En dernier lieu, si M. I... soutient qu'un nombre significatif d'électeurs a voté sans passage par l'isoloir, que des représentants de listes de candidats ont poursuivi irrégulièrement la campagne à l'entrée de bureaux de vote et que certains électeurs ont été placés dans l'impossibilité d'obtenir une procuration, il n'assortit ses allégations d'aucun élément probant. <br/>
<br/>
              18. Il résulte de tout ce qui précède que les conclusions de M. M... L..., de M. I... et de M. G... tendant à ce que le Conseil d'Etat prononce l'annulation totale ou partielle des opérations électorales qui se sont tenues le 30 mai 2021 en vue de l'élection des conseillers des Français de l'étranger pour la circonscription du Portugal doivent être rejetées, ainsi que, par voie de conséquence, les conclusions aux fins d'injonction présentées par M. G.... <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les protestations de MM. M... L..., I... et G... sont rejetées. <br/>
Article 2 : La présente décision sera notifiée à M. N... L..., à M. A... I..., à M. K... G..., à M. F... J..., à M. C... D..., à M. H... E... et au ministre de l'Europe et des affaires étrangères. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
