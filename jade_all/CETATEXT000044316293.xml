<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044316293</ID>
<ANCIEN_ID>JG_L_2021_11_000000447293</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/31/62/CETATEXT000044316293.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 10/11/2021, 447293</TITRE>
<DATE_DEC>2021-11-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>447293</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Arno Klarsfeld</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Domingo</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:447293.20211110</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
<br/>
              M. J... M... a demandé à la Cour nationale du droit d'asile d'annuler la décision de l'Office français de protection des réfugiés et apatrides (OFPRA) du 31 décembre 2019 qui a rejeté sa demande de réexamen de sa demande d'asile. Par une ordonnance n° 20013190 du 27 mai 2020, la Cour nationale du droit d'asile a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés le 7 décembre 2020 et le 8 mars 2021 au secrétariat du contentieux du Conseil d'Etat, M. M... demande au Conseil d'Etat :<br/>
<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de lui reconnaitre la qualité de réfugié ou, à défaut, de lui accorder le bénéfice de la protection subsidiaire ;<br/>
<br/>
              3°) de mettre à la charge de l'OFPRA la somme de 2.000 euros au profit de la SCP Celice - Texidor - Périer, son avocat, au titre des dispositions de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
                     Vu les autres pièces du dossier ;<br/>
<br/>
                     Vu :<br/>
                     - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New York le 31 janvier 1967 relatif au statut des réfugiés ;<br/>
                     - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
                     - la loi n° 91-647 du 10 juillet 1991 ;<br/>
                     - l'ordonnance n° 2020-305 du 25 mars 2020, modifiée par l'ordonnance n° 2020-558 du 13 mai 2020 ;<br/>
                     - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arno Klarsfeld, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Laurent Domingo, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Célice, Texidor, Perier, avocat de M. J... M... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes des dispositions de l'article R. 733-5 du code de l'entrée et du séjour des étrangers et du droit d'asile dans leur rédaction applicable en l'espèce : " Le recours formé par un demandeur d'asile doit contenir les nom, prénoms, date et lieu de naissance, nationalité et domicile du requérant. Il mentionne l'objet de la demande et l'exposé des circonstances de fait et de droit invoquées à son appui. (...) ". Il résulte de l'article R. 733-10 du même code, dans sa version applicable au présent litige, que le recours est communiqué à l'Office français de protection des réfugiés et des apatrides, lequel transmet sans délai le dossier du requérant à la cour qui le tient à disposition de ce dernier.<br/>
<br/>
              2. L'article L. 733-2 du même code, applicable à la date de l'ordonnance attaquée, prévoit que : " Le président et les présidents de section, de chambre ou de formation de jugement à la Cour nationale du droit d'asile peuvent, par ordonnance, régler les affaires dont la nature ne justifie pas l'intervention de l'une des formations prévues à l'article L. 731-2. / Un décret en Conseil d'Etat fixe les conditions d'application du présent article. Il précise les conditions dans lesquelles le président et les présidents de section, de chambre ou de formation de jugement peuvent, après instruction, statuer par ordonnance sur les demandes qui ne présentent aucun élément sérieux susceptible de remettre en cause la décision d'irrecevabilité ou de rejet du directeur général de l'office ". Aux termes de l'article R. 733-4 de ce code, alors applicable : " Le président de la cour et les présidents qu'il désigne à cet effet peuvent, par ordonnance motivée : / (...) 5° Rejeter les recours qui ne présentent aucun élément sérieux susceptible de remettre en cause la décision de l'Office français de protection des réfugiés et des apatrides ; dans ce cas, l'ordonnance ne peut être prise qu'après que le requérant a été mis en mesure de prendre connaissance des pièces du dossier et après examen de l'affaire par un rapporteur ".<br/>
<br/>
              3. Il résulte des dispositions de l'article L. 731-2 du code de l'entrée et du séjour des étrangers et du droit d'asile alors en vigueur que la Cour nationale du droit d'asile, qui doit en principe être saisie, à peine d'irrecevabilité, des recours contre les décisions de l'Office français de protection des réfugiés et apatrides (OFPRA) dans un délai d'un mois à compter de leur notification, statue en formation collégiale, dans un délai de cinq mois à compter de sa saisine. Toutefois, lorsque l'OFPRA a statué dans le cadre de la procédure accélérée prévue à l'article L. 723-2 du même code ou a pris une décision d'irrecevabilité sur le fondement de l'article L. 723-11 de ce code, le président de la cour ou le président de formation de jugement qu'il désigne à cette fin statue dans un délai de cinq semaines à compter de sa saisine.<br/>
<br/>
              4. Il résulte des dispositions citées aux points précédents que, lorsque le demandeur d'asile qui introduit un recours devant la Cour nationale du droit d'asile contre une décision de l'OFPRA annonce son intention de produire des observations complémentaires, la cour, à qui il appartient de statuer dans les délais prévus à l'article L. 731-2 sur les recours dont elle est saisie, peut, après avoir mis en mesure le requérant de prendre connaissance des pièces du dossier et après examen de l'affaire par un rapporteur, rejeter ce recours par ordonnance s'il ne présente aucun élément sérieux susceptible de remettre en cause la décision de l'OFPRA, sans attendre la production des observations annoncées ni avoir imparti au requérant de les produire dans un délai déterminé et attendu l'expiration de ce délai. <br/>
<br/>
              5. Il résulte de ce qui vient d'être dit qu'en rejetant, par une ordonnance fondée sur le 5° de l'article R. 733-4 du code de l'entrée et du séjour des étrangers et du droit d'asile alors applicable, le recours présenté par M. M... contre la décision de l'OFPRA refusant de faire droit à sa demande de réexamen de sa demande d'asile, sans attendre la production par le requérant des observations complémentaires qu'il avait annoncées dans ce recours ni invité préalablement celui-ci à les produire, la Cour nationale du droit d'asile, qui l'a mis en mesure de prendre connaissance des pièces du dossier et qui n'avait pas à attendre l'expiration du délai de recours contentieux, n'a commis aucune irrégularité.<br/>
<br/>
              9. Il résulte de ce qui précède que le pourvoi de M. M... doit être rejeté. <br/>
<br/>
<br/>
<br/>
<br/>
     D E C I D E :<br/>
     --------------<br/>
<br/>
Article 1er : Le pourvoi de M. M... est rejeté. <br/>
Article 2 : La présente décision sera notifiée à M. J... M... et à l'Office français de protection des réfugiés et apatrides.<br/>
              Délibéré à l'issue de la séance du 13 octobre 2021 où siégeaient : Mme Christine Maugüé, présidente adjointe de la section du contentieux, présidant ; M. G... F..., M. Frédéric Aladjidi, présidents de chambre ; Mme I... B..., M. K... C..., Mme A... L..., M. D... E..., M. François Weil conseillers d'Etat et M. Arno Klarsfeld, conseiller d'Etat-rapporteur. <br/>
<br/>
              Rendu le 10 novembre 2021.<br/>
<br/>
                 La présidente : <br/>
                 Signé : Mme Christine Maugüé<br/>
 		Le rapporteur : <br/>
      Signé : M. Arno Klarsfeld<br/>
                 La secrétaire :<br/>
                 Signé : Mme H... N...<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-08-04 - PROCÉDURE DE JUGEMENT PAR ORDONNANCE (ART. R. 733-4 DU CESEDA) - REJET FONDÉ SUR L'ABSENCE D'ÉLÉMENT SÉRIEUX (5°) [RJ1] - OBLIGATION D'ATTENDRE LA PRODUCTION DES OBSERVATIONS COMPLÉMENTAIRES ANNONCÉES PAR LE REQUÉRANT - ABSENCE [RJ2].
</SCT>
<ANA ID="9A"> 095-08-04 Il résulte des articles L. 723-2, L. 723-11, L. 731-2, L. 733-2 et R. 733-4 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) que, lorsque le demandeur d'asile qui introduit un recours devant la Cour nationale du droit d'asile (CNDA) contre une décision de l'Office français de protection des réfugiés et des apatrides (OFPRA) annonce son intention de produire des observations complémentaires, la cour, à qui il appartient de statuer dans les délais prévus à l'article L. 731-2 sur les recours dont elle est saisie, peut, après avoir mis en mesure le requérant de prendre connaissance des pièces du dossier et après examen de l'affaire par un rapporteur, rejeter par ordonnance ce recours s'il ne présente aucun élément sérieux susceptible de remettre en cause la décision de l'OFPRA, sans attendre la production des observations annoncées ni avoir imparti au requérant de le produire dans un délai déterminé et attendu l'expiration de ce délai.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur l'obligation de mettre le requérant en mesure de prendre connaissance du dossier dans ce cas, CE, 10 décembre 2008, M. Islam, n° 284159, T. p. 775 ; CE, 9 juillet 2014, M. Faizi, n° 360162, T. p. 526....[RJ2] Comp., pour l'application du 9e alinéa de l'article R. 222-1 du CJA, CE, 10 juin 2020, M. Brunel, n° 427806, T. pp. 946-951.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
