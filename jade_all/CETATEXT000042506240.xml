<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042506240</ID>
<ANCIEN_ID>JG_L_2020_11_000000434022</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/50/62/CETATEXT000042506240.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 06/11/2020, 434022, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434022</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Charles-Emmanuel Airy</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:434022.20201106</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société à responsabilité limitée (SARL) TK Immobilier a demandé au tribunal administratif de Grenoble de prononcer la décharge des rappels de taxe sur la valeur ajoutée mis à sa charge au titre de la période du 1er janvier 2011 au 31 décembre 2012 et des pénalités correspondantes. Par un jugement n° 1700782 du 21 février 2019, ce tribunal a prononcé la décharge de ces rappels.<br/>
<br/>
              Par une ordonnance n° 19LY02023 du 4 juillet 2019, le président de la 2ème chambre de la cour administrative d'appel de Lyon a rejeté l'appel formé par le ministre de l'action et des comptes publics contre ce jugement.  <br/>
<br/>
              Par un pourvoi, enregistré le 28 août 2019 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'action et des comptes publics demande au Conseil d'Etat d'annuler cette ordonnance.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 2006/112/CE du 28 novembre 2006 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles-Emmanuel Airy, auditeur,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la société TK immobilier ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société TK Immobilier, qui exerce une activité de marchand de biens, a acquis le 30 janvier 2012 auprès d'un particulier une maison et le terrain y attenant de 1 647 m² en vue de diviser la parcelle initiale pour céder séparément la maison et un terrain à bâtir. La société a fait l'objet d'une vérification de comptabilité à l'issue de laquelle des rappels de taxe sur la valeur ajoutée lui ont été notifiés au titre de la période du 1er janvier au 31 décembre 2012, selon la procédure contradictoire, procédant de la remise en cause du bénéfice du régime de la taxe sur la valeur ajoutée sur la marge sous lequel elle avait placé l'opération de cession du terrain à bâtir issu de la division parcellaire. La société a contesté ces rappels devant le tribunal administratif de Grenoble qui, par jugement du 21 février 2019, a prononcé la décharge de ces impositions. Par une ordonnance du 4 juillet 2019, le président de la 2ème chambre de la cour administrative d'appel de Lyon a rejeté l'appel formé par le ministre de l'action et des comptes publics contre ce jugement. Le ministre de l'action et des comptes publics se pourvoit en cassation contre cet arrêt.<br/>
<br/>
              2. Le I de l'article 257 du code général des impôts dans sa rédaction applicable, issue de l'article 16 de la loi du 9 mars 2010 de finances rectificative pour 2010, prévoit que les opérations concourant à la production ou à la livraison d'immeubles, lesquelles comprennent les livraisons à titre onéreux de terrains à bâtir, sont soumises à la taxe sur la valeur ajoutée. En vertu du 2 du b de l'article 266 du même code, l'assiette de la taxe est en principe constituée par le prix de cession.<br/>
<br/>
              3. L'article 392 de la directive du Conseil du 28 novembre 2006 relative au système commun de taxe sur la valeur ajoutée dispose toutefois que : " Les États membres peuvent prévoir que, pour les livraisons de bâtiments et de terrains à bâtir achetés en vue de la revente par un assujetti qui n'a pas eu droit à déduction à l'occasion de l'acquisition, la base d'imposition est constituée par la différence entre le prix de vente et le prix d'achat ". L'article 268 du code général des impôts, pris pour la transposition de ces dispositions, prévoit, dans sa rédaction également issue de l'article 16 de la loi du 9 mars 2010 de finances rectificative pour 2010, que : " S'agissant de la livraison d'un terrain à bâtir (...), si l'acquisition par le cédant n'a pas ouvert droit à déduction de la taxe sur la valeur ajoutée, la base d'imposition est constituée par la différence entre : / 1° D'une part, le prix exprimé et les charges qui s'y ajoutent ; / 2° D'autre part, selon le cas : / - soit les sommes que le cédant a versées, à quelque titre que ce soit, pour l'acquisition du terrain(...); / - soit la valeur nominale des actions ou parts reçues en contrepartie des apports en nature qu'il a effectués ".<br/>
<br/>
              4. Il résulte de ces dernières dispositions, lues à la lumière de celles de la directive dont elles ont pour objet d'assurer la transposition, que les règles de calcul dérogatoires de la taxe sur la valeur ajoutée qu'elles prévoient s'appliquent aux opérations de cession de terrains à bâtir qui ont été acquis en vue de leur revente et ne s'appliquent donc pas à une cession de terrains à bâtir qui, lors de leur acquisition, avaient le caractère d'un terrain bâti, notamment quand le bâtiment qui y était édifié a fait l'objet d'une démolition de la part de l'acheteur-revendeur ou quand le bien acquis a fait l'objet d'une division parcellaire en vue d'en céder séparément des parties ne constituant pas le terrain d'assiette du bâtiment.<br/>
<br/>
              5. Il suit de là que le président de la 2ème chambre de la cour administrative d'appel a commis une erreur de droit en jugeant qu'il résultait des dispositions des articles 268 du code général des impôts et 392 de la directive du 28 novembre 2006 que le bénéfice du régime de la taxe sur la valeur ajoutée sur la marge était subordonné à la seule condition que l'acquisition du bien cédé n'ait pas ouvert droit à déduction de la taxe et en jugeant sans incidence sur sa mise en oeuvre la circonstance que la qualification juridique du bien en cause ait été modifiée entre son acquisition et sa vente.<br/>
<br/>
              6. Par suite, le ministre de l'action et des comptes publics est fondé à demander l'annulation de l'ordonnance qu'il attaque.  <br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 4 juillet 2019 du président de la 2ème chambre de la cour administrative d'appel de Lyon est annulée.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
Article 3 : Les conclusions de la société TK Immobilier présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'économie, des finances et de la relance et à la société à responsabilité limitée TK Immobilier. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
