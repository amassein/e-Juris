<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033820428</ID>
<ANCIEN_ID>JG_L_2016_12_000000389373</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/82/04/CETATEXT000033820428.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 30/12/2016, 389373, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389373</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Philippe Mochon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:389373.20161230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              La société Sogelink a demandé à la ministre de l'écologie, du développement durable et de l'énergie, par une demande reçue le 9 décembre 2014, d'abroger les points f) et g) du I de l'article 3 de l'arrêté du 22 décembre 2010 fixant les modalités de fonctionnement du guichet unique prévu à l'article L. 554-2 du code de l'environnement et, par une demande reçue le 2 février 2015, d'abroger les mots " les formulaires de déclaration complètement préremplis " au 2° de l'article R. 554-4 du code de l'environnement. Par des décisions implicites nées du silence gardé sur ces demandes, la ministre les a rejetées. Par une décision explicite du 12 février 2015, la ministre a confirmé le rejet de la demande d'abrogation des points f) et g) du I de l'article 3 de l'arrêté du 22 décembre 2010. <br/>
<br/>
              1° Sous le n° 389373, par une requête, enregistrée le 10 avril 2015 au secrétariat du contentieux du Conseil d'Etat, la société Sogelink demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite née du silence gardé par la ministre de l'écologie, du développement durable et de l'énergie sur sa demande d'abrogation des points f) et g) du I de l'article 3 de l'arrêté du 22 décembre 2010 ainsi que la décision exprès de la ministre en date du 12 février 2015 ; <br/>
<br/>
              2°) d'annuler pour excès de pouvoir la décision implicite née du silence gardé par la ministre de l'écologie, du développement durable et de l'énergie sur sa demande d'abrogation des mots " les formulaires de déclaration complètement préremplis " au 2° de l'article R. 554-4 du code de l'environnement ;<br/>
<br/>
              3°) d'enjoindre à la ministre de l'écologie, du développement durable et de l'énergie, sur le fondement de l'article L. 911-1 du code de justice administrative, d'abroger les points f) et g) du I de l'article 3 de l'arrêté du 22 décembre 2010, sous astreinte de 1 500 euros par jour de retard ;<br/>
<br/>
              4°) d'enjoindre à la ministre de l'écologie, du développement durable et de l'énergie, sur le fondement de l'article L. 911-1 du code de justice administrative, de supprimer les mots " les formulaires de déclaration complètement préremplis " au 2° de l'article R. 554-4 du code de l'environnement, sous astreinte de 1 500 euros par jour de retard ;<br/>
<br/>
              5°) de mettre à la charge de l'Etat la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 395163, sur le renvoi, par une ordonnance n° 1505874/7-2 du 7 décembre 2015, du président du tribunal administratif de Paris, la société Sogelink, par une requête enregistrée le 8 avril 2015 au greffe de ce tribunal, présente les mêmes demandes que celles analysées sous le n° 389373.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - le code de l'environnement ; <br/>
              - la loi n° 79-587 du 11 juillet 1979 ;<br/>
              - le décret n° 2005-850 du 27 juillet 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Philippe Mochon, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public.<br/>
<br/>
<br/>
<br/>1. Considérant, d'une part, qu'afin de prévenir les dommages accidentels aux réseaux de transport et de distribution à proximité desquels des travaux sont réalisés, l'article L. 554-1 du code de l'environnement, dans sa rédaction alors en vigueur, dispose : " I. - Les travaux réalisés à proximité des réseaux souterrains, aériens ou subaquatiques de transport ou de distribution sont effectués dans des conditions qui ne sont pas susceptibles de porter atteinte à la continuité de fonctionnement de ces réseaux, à l'environnement, à la sécurité des travailleurs et des populations situées à proximité du chantier ou à la vie économique. Il en va de même pour les travaux réalisés à proximité des ouvrages construits en vue de prévenir les inondations et les submersions, lesquels bénéficient des dispositions prévues au présent chapitre au profit des réseaux précités. / II. - Lorsque des travaux sont réalisés à proximité d'un réseau mentionné au I, des dispositions techniques et organisationnelles sont mises en oeuvre, dès le début du projet et jusqu'à son achèvement, sous leur responsabilité et à leurs frais, par le responsable du projet de travaux, par les exploitants des réseaux et par les entreprises exécutant les travaux. / Lorsque la position des réseaux n'est pas connue avec une précision suffisante pour mettre en oeuvre l'alinéa précédent, des dispositions particulières sont appliquées par le responsable du projet de travaux pour respecter l'objectif prévu au I. / III. - Des mesures contractuelles sont prises par les responsables de projet de travaux pour que les entreprises exécutant les travaux ne subissent pas de préjudice lié au respect des obligations prévues au II, notamment en cas de découverte fortuite d'un réseau durant le chantier ou en cas d'écart notable entre les informations relatives au positionnement des réseaux communiquées avant le chantier par le responsable du projet de travaux et la situation constatée au cours du chantier. / Le responsable du projet de travaux supporte toutes les charges induites par la mise en oeuvre de ces mesures, y compris en ce qui concerne le déroulement du chantier et sauf en ce qui concerne les dispositions du second alinéa du II qui sont appliquées conformément au IV. / IV. - Un décret en Conseil d'Etat précise les modalités de mise en oeuvre du présent article, et notamment : / 1° Les catégories de réseaux, y compris les équipements qui leur sont fonctionnellement associés, auxquelles s'applique le présent chapitre, ainsi que la sensibilité de ces réseaux ; / 2° Les dispositions techniques et organisationnelles mises en oeuvre par le responsable du projet de travaux, les exploitants de réseaux et les entreprises exécutant les travaux en relation, le cas échéant, avec le guichet unique mentionné à l'article L. 554-2 ; / 3° Les dispositions particulières mentionnées au second alinéa du II ; / 4° Les modalités de répartition, entre le responsable du projet de travaux et les exploitants des réseaux, des coûts associés à la mise en oeuvre des dispositions du second alinéa du II ; / 5° Les dispositions qui sont portées dans le contrat qui lie le responsable du projet de travaux et les entreprises de travaux pour l'application du présent article ; / 6° Les adaptations nécessaires à l'application du présent chapitre aux ouvrages construits en vue de prévenir les inondations et les submersions. " : qu'aux termes de l'article L. 554-2 du code de l'environnement, dans sa rédaction en vigueur à la date des décisions attaquées : " Il est instauré, au sein de l'Institut national de l'environnement industriel et des risques, dans le cadre d'une mission de service public qui lui est confiée pour contribuer à la préservation de la sécurité des réseaux, un guichet unique rassemblant les éléments nécessaires à l'identification des exploitants des réseaux mentionnés au I de l'article L. 554-1. " ; <br/>
<br/>
              2. Considérant, d'autre part, qu'aux termes de l'article R. 554-4 du même code : " Pour la gestion du guichet unique, qui est accessible par voie électronique, l'Institut national de l'environnement industriel et des risques est chargé, dans les conditions prévues au présent chapitre et par les arrêtés du ministre chargé de la sécurité des réseaux de transport et de distribution pris pour son application : 1° De recueillir, enregistrer et mettre à jour les coordonnées des exploitants des ouvrages mentionnés à l'article R. 554-2 et les zones d'implantation de ces ouvrages dans une base de données nationale unique comportant un outil cartographique ; / 2° De mettre gratuitement à la disposition des responsables de projets et des particuliers ou des entreprises exécutant ou prévoyant l'exécution de travaux à proximité des ouvrages mentionnés à l'article R. 554-2 les informations et les formulaires de déclaration complètement préremplis leur permettant de remplir les obligations prévues par le présent chapitre, soit directement, soit par l'intermédiaire de prestataires bénéficiant d'un accès spécifique aux informations gérées par le guichet unique ; / 3° De mettre à la disposition des services de l'Etat, des collectivités territoriales ou de leurs groupements les informations gérées par le guichet unique, le cas échéant en liaison avec les prestataires mentionnés au 2°, nécessaires à l'exercice de leurs missions respectives de service public ; / 4° D'inviter les exploitants n'ayant pas rempli les obligations qui leur incombent à l'égard du guichet unique en vertu du présent chapitre à y remédier et de signaler au ministre chargé de la sécurité des réseaux de transport et de distribution les cas d'absence de mise en conformité au-delà d'un délai de deux mois à compter de cette invitation ; / 5° De mettre à la disposition des particuliers ou entreprises exécutant des travaux les prescriptions techniques que ceux-ci doivent respecter afin de prévenir tout endommagement des ouvrages présents à proximité. (...) " ; <br/>
<br/>
              3. Considérant, enfin, qu'en vertu de l'article 3 de l'arrêté du 22 décembre 2010 du ministre de l'écologie, du développement durable, des transports et du logement fixant les modalités de fonctionnement du guichet unique prévu à l'article L. 554-2 du code de l'environnement le téléservice met gratuitement à disposition des déclarants un ensemble de services accessibles par internet leur permettant notamment : " (...) f) A des fins d'établissement de leurs déclarations de projet de travaux, de disposer sous format électronique des formulaires de déclaration complètement préremplis avec les informations qu'ils lui ont communiquées sous leur seule responsabilité, ainsi que de fichiers électroniques normalisés comprenant l'ensemble des données des formulaires et de la consultation et autorisant leur traitement automatisé ; / g) A des fins d'établissement de leurs déclarations d'intention de commencement de travaux, de disposer sous format électronique des formulaires de déclaration complètement préremplis avec les informations qu'ils lui ont communiquées sous leur seule responsabilité, ainsi que de fichiers électroniques normalisés comprenant l'ensemble des données des formulaires et de la consultation et autorisant leur traitement automatisé ; (...) " ; <br/>
<br/>
              4. Considérant que, sous les nos 389373 et 395163, la société Sogelink demande l'annulation pour excès de pouvoir des décisions par lesquelles le ministre chargé de l'écologie, du développement durable et de l'énergie a refusé d'abroger, d'une part, les mots " les formulaires de déclaration complètement préremplis " au 2° de l'article R. 554-4 du code de l'environnement et, d'autre part, les points f) et g) du I de l'article 3 de l'arrêté du 22 décembre 2010, dont les dispositions ont été rappelées aux points 2 et 3, en tant qu'ils confient à l'INERIS la mission de mettre à la disposition des utilisateurs des formulaires de déclaration complètement préremplis sur la base des informations que ceux-ci lui ont communiquées ; que ces requêtes sont dirigées contre les mêmes décisions refusant de faire droit à ces demandes ; qu'il y a lieu de les joindre pour statuer par une seule décision ; <br/>
<br/>
              Sur la légalité externe des décisions attaquées :<br/>
<br/>
              En ce qui concerne la décision du 12 février 2015 refusant d'abroger partiellement l'article 3 de l'arrêté du 22 décembre 2010 : <br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier que le directeur général de la prévention des risques, dont il n'est pas contesté qu'il avait été régulièrement nommé en cette qualité,  avait compétence, en application de l'article 1er du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement, pour signer, au nom du ministre dont il relevait et par délégation, l'ensemble des actes, à l'exception des décrets, relatifs aux affaires des services placés sous son autorité ; que le signataire de la décision du 12 février 2015, M.A..., ingénieur général des ponts, des eaux et forêts, a, par un arrêté du 12 décembre 2014 du directeur général de la prévention des risques, publié au Journal Officiel du 18 décembre 2014, reçu délégation, en application du 1° de l'article 3 du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement, pour signer au nom du ministre chargé de l'écologie, du développement durable et de l'énergie, tous actes, arrêtés et décisions, à l'exclusion des décrets, dans la limite des attributions de la direction générale ; que le moyen tiré de l'incompétence du signataire de cette décision ne peut, dès lors, qu'être écarté ; <br/>
<br/>
              En ce qui concerne la décision implicite refusant d'abroger partiellement le 2° de l'article R. 554-4 du code de l'environnement : <br/>
<br/>
              6. Considérant que ni les dispositions de l'article 1er de la loi du 11 juillet 1979 relative à la motivation des actes  administratifs et à l'amélioration des relations entre l'administration et le public, alors en vigueur, ni aucune autre disposition ou principe n'imposaient de motiver la décision par laquelle le ministre chargé de l'écologie, du développement durable et de l'énergie a rejeté la demande de la société Sogelink tendant à la suppression des mots " les formulaires de déclaration complètement préremplis " au 2° de l'article R. 554-4 du code de l'environnement ; que le moyen tiré de ce que cette décision serait irrégulière, faute d'être motivée, ne peut, par suite,  qu'être écarté ; <br/>
<br/>
              Sur la légalité interne des décisions attaquées : <br/>
<br/>
              7. Considérant qu'il résulte des dispositions citées au point 1 que l'INERIS a pour mission la gestion d'un guichet unique rassemblant les éléments nécessaires à l'identification des exploitants des réseaux de transport et de distribution à proximité desquels des travaux sont susceptibles d'être exécutés ; que, d'une part, en le chargeant, dans le cadre de sa mission de service public, de mettre gratuitement à la disposition des responsables de projets et des personnes entreprenant des travaux, afin de leur permettre de satisfaire à leurs obligations, des formulaires de déclaration complètement préremplis sur la base des informations qu'ils ont préalablement communiquées, les dispositions contestées de l'article R. 554-4 du code de l'environnement et de l'arrêté du 22 décembre 2010, qui, contrairement à ce que soutient la société requérante, n'ont pas méconnu la portée des articles L. 554-1 et L. 554-2 du code de l'environnement en étendant illégalement la mission confiée par le législateur à l'INERIS, ne lui confient aucune attribution qui emporte intervention sur un marché ; que, d'autre part, ces dispositions ne font pas obstacle à ce que des personnes privées offrent, à titre onéreux, des prestations de services à des personnes qui entreprennent des travaux à proximité de réseaux de transport ou de distribution, pour les accompagner et les conseiller, notamment, dans l'accomplissement de leurs démarches administratives ; qu'à cette fin, elles peuvent solliciter, dans le cadre prévu par les articles L. 554-3 et R. 554-6 du code de l'environnement, l'accès aux données enregistrées et mises à jour dans le guichet unique par l'INERIS ; qu'il résulte de ce qui précède que la société Sogelink n'est pas fondée à soutenir que les dispositions contestées placeraient l'INERIS en situation d'abuser d'une position dominante, en méconnaissance de l'article 102 du traité sur le fonctionnement de l'Union européenne et des règles de concurrence, et porteraient illégalement atteinte à la liberté d'entreprendre ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que la société Sogelink n'est pas fondée à demander l'annulation des décisions qu'elle attaque ; que ses conclusions présentées sur le fondement de l'article L. 911-1 doivent, par suite, être rejetées ; qu'il en va de même de ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
      Article 1er : Les requêtes de la société Sogelink sont rejetées. <br/>
<br/>
Article 2 : La présente décision sera notifiée à la société Sogelink et à la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
