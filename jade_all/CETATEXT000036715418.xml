<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036715418</ID>
<ANCIEN_ID>JG_L_2018_03_000000418347</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/71/54/CETATEXT000036715418.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 14/03/2018, 418347, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-03-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418347</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:418347.20180314</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Samad a demandé au juge des référés du tribunal administratif de Caen, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, à titre principal, d'enjoindre au maire de Vire Normandie d'assurer sans délai l'exécution de l'arrêt n° 16NT03217 du 14 juin 2017 de la cour administrative d'appel de Nantes, en prononçant le retrait du permis de construire délivré le 30 novembre 2017 à la société Lidl, en prenant un arrêté interruptif de travaux et en interdisant au maire de délivrer à cette société l'arrêté d'ouverture au public du bâtiment édifié sur ce terrain et, à titre subsidiaire, d'ordonner toutes mesures utiles susceptibles de mettre fin aux atteintes graves et manifestement illégales au droit au recours effectif, portées par l'abstention du maire de Vire Normandie et du préfet du Calvados à assurer l'exécution de l'arrêt de la cour administrative d'appel de Nantes.<br/>
<br/>
              Par une ordonnance n° 1800327 du 16 février 2018, le juge des référés du tribunal administratif de Caen a enjoint au maire de Vire Normandie de faire contrôler, sous sa responsabilité, l'absence de toute reprise des travaux sur le chantier de la société Lidl situé rue de Caen, par une visite quotidienne des lieux, tous les jours ouvrables et à des horaires variables, à compter du lundi 19 février 2018 et jusqu'à la notification des arrêts de la cour administrative d'appel de Nantes intervenant sur les recours en annulation dirigés contre les permis de construire délivrés le 30 novembre et le 1er décembre 2017, ou d'une décision du Conseil d'Etat annulant l'arrêt du 14 juin 2017 de cette cour, en précisant que le maire ferait de ces visites inopinées un rapport hebdomadaire au préfet du Calvados, en lui indiquant, dans les heures qui suivent la constatation d'une reprise des travaux, dans quel délai il prendrait un arrêté interruptif de travaux.<br/>
<br/>
              Par une requête et un mémoire, enregistrés les 20 et 28 février 2018 au secrétariat du contentieux du Conseil d'Etat, la société Lidl demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de rejeter la demande de la société Samad ;<br/>
<br/>
              3°) de mettre à la charge de la société Samad la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - du fait de l'interruption des travaux depuis le 9 février 2018, ni l'urgence, ni l'atteinte grave et manifestement illégale à une liberté fondamentale, au sens de l'article L. 521-2 du code de justice administrative, ne sont caractérisées ;<br/>
              - le juge des référés a entaché son ordonnance d'une erreur de droit en jugeant la condition d'urgence établie sur la base d'un simple risque de poursuite des travaux ;<br/>
              - le juge des référés a commis une autre erreur de droit en fixant pour terme de l'injonction prononcée à l'encontre du maire de Vire Normandie l'intervention des arrêts rendus par la cour administrative d'appel de Nantes sur les recours en annulation dirigés contre les permis de construire qui lui ont été délivrés les 30 novembre et 1er décembre 2017, ou d'une décision du Conseil d'Etat annulant l'arrêt du 14 juin 2017 de cette cour, ce qui fait obstacle à ce qu'elle obtienne un nouveau permis susceptible de régulariser la situation et d'autoriser la reprise des travaux.<br/>
<br/>
              Par un mémoire en défense, enregistré le 7 mars 2018, la société Samad conclut au rejet de la requête et à ce qu'une somme de 4 000 euros soit mise à la charge de la société Lidl au titre de l'article L. 761-1 du code de justice administrative. Elle fait valoir que les moyens soulevés par la société requérante ne sont pas fondés.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de commerce ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la société Lidl, d'autre part, la société Samad ;<br/>
              Vu le procès-verbal de l'audience publique du 8 mars 2018 à 15 heures au cours de laquelle ont été entendus : <br/>
              - Me François Molinié, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la société Lidl ;<br/>
<br/>
              - les représentants de la société Lidl ;<br/>
<br/>
              - le représentant de la société Samad ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ".<br/>
              2. Il résulte de l'instruction qu'une première demande de permis de construire valant autorisation d'exploitation commerciale portant sur le terrain cadastré section BB, numéros 11, 14, 28 et 32, situé rue de Caen à Vire Normandie a été déposée le 29 juillet 2015 par la société Lidl, pour une surface de vente de 1 420 m2. Après l'avis favorable rendu sur ce projet par la commission départementale d'aménagement commercial, la société Samad, qui exploite dans la commune un supermarché, a saisi la commission nationale d'aménagement commercial, qui a rendu un avis défavorable le 6 avril 2016. La société Lidl ayant revu son projet à la suite de cet avis, le maire de la commune de Vire Normandie lui a accordé, par un arrêté du 21 juillet 2016, un permis de construire sur le même terrain un supermarché d'une surface de plancher de 2 415,03 m2 comprenant une surface de vente de 999 m2. Cet arrêté indiquait que le permis valait autorisation d'exploitation commerciale, laquelle n'est pourtant requise par les dispositions de l'article L. 752-1 du code de commerce que pour les magasins de commerce de détail d'une surface de vente supérieure à 1 000 m2. Saisie par la société Samad, la cour administrative d'appel de Nantes a, par un arrêt du 14 juin 2017, relevé que la surface de vente annoncée de 999 m2 n'était séparée, sur les plans, d'un local " non affecté ", d'une surface de 417 m2, que par une cloison légère aisément démontable composée de plaques de plâtre d'une épaisseur de vingt-cinq millimètres. La cour en a déduit que le maire ne pouvait que s'estimer saisi d'une demande de permis de construire portant sur la création d'une surface de vente excédant 1 000 m2 et qu'elle était, ainsi, compétente pour statuer sur la requête de la société Samad sur le fondement de l'article L. 600-10 du code de l'urbanisme. Faute pour cette demande d'avoir été soumise à la commission départementale d'aménagement commercial, la cour a jugé que le permis litigieux ne pouvait, en application des dispositions de l'article L. 425-4 du code de l'urbanisme, valoir autorisation d'exploitation commerciale et a annulé, dans cette mesure, l'arrêté du 21 juillet 2016 du maire de Vire Normandie. Le préfet du Calvados a pris le 11 août 2017 un arrêté interruptif de travaux. Le maire de la commune de Vire Normandie a, le 30 novembre 2017, délivré à la société Lidl un permis modificatif, puis, le 1er décembre 2017, un nouveau permis, qui ont conduit le préfet du Calvados à lever, le 11 décembre 2017, l'interruption des travaux, lesquels ont alors repris. Toutefois, le maire a, le 29 janvier 2018, retiré l'arrêté du 1er décembre 2017 portant permis de construire. La société Lidl relève appel de l'ordonnance du 16 février 2018 par laquelle le juge des référés du tribunal administratif de Caen a, à la demande de la société Samad, enjoint au maire de Vire Normandie de faire contrôler, sous sa responsabilité, l'absence de toute reprise des travaux sur le chantier de la société Lidl situé rue de Caen, par une visite quotidienne des lieux, tous les jours ouvrables et à des horaires variables, à compter du lundi 19 février 2018 et jusqu'à la notification des arrêts de la cour administrative d'appel de Nantes intervenant sur les recours en annulation dirigés contre les permis de construire délivrés le 30 novembre et le 1er décembre 2017, ou d'une décision du Conseil d'Etat annulant l'arrêt du 14 juin 2017 de cette cour, en précisant que le maire ferait de ces visites inopinées un rapport hebdomadaire au préfet du Calvados, en lui indiquant, dans les heures qui suivent la constatation d'une reprise des travaux, dans quel délai il prendrait un arrêté interruptif de travaux.<br/>
<br/>
              3. Le juge des référés du tribunal administratif de Caen a jugé que la délivrance de plusieurs permis de construire au profit de la société Lidl par le maire de Vire Normandie agissant au nom de la commune, en dépit de l'avis de la commission nationale d'aménagement commercial, et l'attitude du maire pris en sa qualité d'agent de l'Etat, qui s'abstient de faire contrôler que la société Lidl a mis fin aux travaux de construction alors qu'il ne peut ignorer que celle-ci poursuit son projet pour le rendre irréversible, en essayant de contourner la législation sur l'urbanisme commercial, sont de nature à porter une atteinte grave et manifestement illégale au droit qu'a la société Samad de bénéficier d'une exécution effective de l'arrêt du 14 juin 2017 par lequel la cour administrative d'appel de Nantes a annulé l'arrêté du 21 juillet 2016 en tant qu'il tient lieu d'autorisation d'exploitation commerciale. Pour caractériser l'urgence qu'il y avait à faire usage des pouvoirs qu'il tire de l'article L. 521-2 du code de justice administrative, le juge des référés a relevé qu'à plusieurs reprises la société Lidl avait provisoirement arrêté le chantier de construction, puis décidé la reprise des travaux, de telle sorte que l'immeuble est en voie d'achèvement, et jugé que les pièces versées au dossier, en particulier les ordres de service datés du 9 février 2018, n'établissaient pas l'interruption des travaux, si bien qu'il apparaissait manifeste que la société Lidl n'avait pas l'intention de respecter les décisions de justice en arrêtant spontanément le chantier.<br/>
<br/>
              4. Toutefois, il ne résulte pas de l'instruction que des travaux aient eu lieu sur le chantier entre l'arrêt du 14 juin 2017 par lequel la cour administrative d'appel de Nantes a annulé l'arrêté du 21 juillet 2016 en tant qu'il tient lieu d'autorisation d'exploitation commerciale, ce qui faisait obstacle à la réalisation du projet, et le nouveau permis de construire délivré 1er décembre 2017 par le maire de Vire Normandie. Dans la mesure où, d'une part, ce nouveau permis autorisait un projet de construction s'attachant à remédier au motif d'illégalité retenu par cette cour, en prévoyant le remplacement de la cloison légère aisément démontable séparant la surface de vente annoncée, de 993 m2, du local précédemment " non affecté ", par un mur en béton dépourvu d'ouverture, et où, d'autre part, le préfet du Calvados avait en conséquence, par son arrêté du 11 décembre 2017, levé l'interruption des travaux, la société Lidl était autorisée à procéder à la reprise du chantier, sans que cette reprise puisse être regardée comme prouvant l'intention de la société de ne pas respecter l'arrêt du 14 juin 2017 de la cour administrative d'appel de Nantes, qui annulait partiellement un précédent permis de construire. A la suite de son arrêté du 1er décembre 2017 et de l'arrêté préfectoral du 11 décembre 2017, le maire de la commune de Vire Normandie n'avait donc pas à faire contrôler que la société avait mis fin à des travaux de construction qu'elle était autorisée à entreprendre.<br/>
<br/>
              5. Cependant, par un arrêté du 29 janvier 2018, le maire de la commune a retiré le nouveau permis de construire qu'il avait délivré le 1er décembre 2017 à la société Lidl. Il résulte de l'instruction, notamment des dix ordres de service à nouveau produits devant le juge des référés du Conseil d'Etat, qu'après avoir pris connaissance de cet arrêté, la société Lidl a notifié un arrêt du chantier à compter du 9 février 2018 aux entreprises chargées des différents lots, en précisant que la reprise des travaux ne pourrait intervenir qu'après notification d'un ordre de service en ce sens, et que chacun de ces dix documents est signé par le maître d'ouvrage, le maître d'oeuvre et l'entreprise concernée. Si, ainsi que l'avait relevé le juge des référés du tribunal administratif de Caen, certains de ces ordres de service concernent des lots qui, compte tenu de l'état d'avancement du chantier, sont déjà terminés, la majorité d'entre eux porte sur des travaux indispensables à l'achèvement du bâtiment et sans lesquels ce dernier est, en l'état, inexploitable. Ces documents attestent donc de l'arrêt effectif des travaux depuis le 9 février 2018. Dans ces circonstances, l'urgence qu'il y a à faire usage des pouvoirs que le juge des référés tire de l'article L. 521-2 du code de justice administrative n'est pas caractérisée. <br/>
<br/>
              6. Il résulte de tout ce qui précède que la société Lidl est fondée à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Caen a prononcé l'injonction contestée, sans qu'il soit besoin d'examiner si, en tout état de cause, l'abstention du maire de la commune de Vire-Normandie à faire contrôler, depuis son arrêté du 29 janvier 2018, que la société a interrompu les travaux litigieux porte au droit au recours effectif dont se prévaut la société Samad une atteinte grave et manifestement illégale.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Samad une somme de 3 000 euros à verser à la société Lidl, en application des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font en revanche obstacle à ce qu'une somme soit mise à la charge de la société Lidl, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'ordonnance du 16 février 2018 du juge des référés du tribunal administratif de Caen est annulée.<br/>
Article 2 : La demande présentée par la société Samad au juge des référés du tribunal administratif de Caen est rejetée.<br/>
Article 3 : La société Samad versera à la société Lidl une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la société Samad au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
		Article 5 : La présente ordonnance sera notifiée à la société Lidl et à la société Samad. <br/>
Copie en sera adressée à la commune de Vire Normandie, au préfet du Calvados et au ministre de la cohésion des territoires.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
