<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030445661</ID>
<ANCIEN_ID>JG_L_2015_03_000000374234</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/44/56/CETATEXT000030445661.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 27/03/2015, 374234, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-03-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374234</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Mireille Le Corre</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:374234.20150327</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 24 décembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentée par M. B...A..., demeurant ... ; M. A...demande au Conseil d'Etat d'annuler pour excès de pouvoir l'article 2 du décret n° 2013-958 du 25 octobre 2013 portant application des dispositions de l'article préliminaire et de l'article 803-5 du code de procédure pénale relatives au droit à l'interprétation et à la traduction en tant qu'il insère dans ce code une section 3 " Désignation de l'interprète ou du traducteur ", comportant un article unique D. 594-11 ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la directive 2010/64/UE du Parlement européen et du Conseil du 20 octobre 2010 ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu le code de procédure pénale ;<br/>
<br/>
              Vu la loi n° 71-498 du 29 juin 1971 ;<br/>
<br/>
              Vu la loi n° 2013-711 du 5 août 2013 ;<br/>
<br/>
              Vu le décret n° 2004-1463 du 23 décembre 2004 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Mireille Le Corre, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant, d'une part, qu'aux termes du paragraphe 8 de l'article 2 de la directive 2010/64/UE du Parlement et du Conseil du 20 octobre 2010 relative au droit à l'interprétation et à la traduction dans le cadre des procédures pénales : " L'interprétation prévue par le présent article est d'une qualité suffisante pour garantir le caractère équitable de la procédure, notamment en veillant à ce que les suspects ou les personnes poursuivies aient connaissance des faits qui leur sont reprochés et soient en mesure d'exercer leurs droits de la défense " ; qu'aux termes du paragraphe 9 de l'article 3 de la directive : " La traduction prévue par le présent article est d'une qualité suffisante pour garantir le caractère équitable de la procédure, notamment en veillant à ce que les suspects ou les personnes poursuivies aient connaissance des faits qui leur sont reprochés et soient en mesure d'exercer leurs droits de la défense " ; qu'aux termes du paragraphe 2 de l'article 5 de la directive : " Afin de disposer de services d'interprétation et de traduction adéquats et de faciliter un accès à ceux-ci, les Etats-membres s'efforcent de dresser un ou plusieurs registres de traducteurs et d'interprètes indépendants possédant les qualifications requises. Une fois établis, ces registres sont, le cas échéant, mis à la disposition des conseils juridiques et des autorités concernées " ; qu'aux termes de l'article 8 de la directive : " Nulle disposition de la présente directive ne saurait être interprétée comme limitant ou dérogeant aux droits et garanties procédurales accordés en vertu de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, de la charte des droits fondamentaux de l'Union européenne, de toute autre disposition pertinente du droit international ou du droit d'un Etat membre procurant un niveau de protection supérieur " ;<br/>
<br/>
              2. Considérant, d'autre part, qu'aux termes du III de l'article préliminaire du code de procédure pénale, dans sa rédaction issue de la loi du 5 août 2013 portant diverses dispositions d'adaptation dans le domaine de la justice en application du droit de l'Union européenne et des engagements internationaux de la France, qui transpose notamment la directive 2010/64/UE précitée : " Si la personne suspectée ou poursuivie ne comprend pas la langue française, elle a droit, dans une langue qu'elle comprend et jusqu'au terme de la procédure, à l'assistance d'un interprète, y compris pour les entretiens avec son avocat ayant un lien direct avec tout interrogatoire ou toute audience, et, sauf renonciation expresse et éclairée de sa part, à la traduction des pièces essentielles à l'exercice de sa défense et à la garantie du caractère équitable du procès qui doivent, à ce titre, lui être remises ou notifiées en application du présent code " ; qu'aux termes de l'article 803-5 du même code, dans sa rédaction issue de la même loi : " Pour l'application du droit d'une personne suspectée ou poursuivie, prévu par le III de l'article préliminaire, à un interprète ou à une traduction, il est fait application du présent article. / S'il existe un doute sur la capacité de la personne suspectée ou poursuivie à comprendre la langue française, l'autorité qui procède à son audition ou devant laquelle cette personne comparaît vérifie que la personne parle et comprend cette langue. / A titre exceptionnel, il peut être effectué une traduction orale ou un résumé oral des pièces essentielles qui doivent lui être remises ou notifiées en application du présent code. / Les modalités d'application du présent article sont précisées par décret, qui définit notamment les pièces essentielles devant faire l'objet d'une traduction " ; <br/>
<br/>
              3. Considérant, enfin, qu'aux termes de l'article D. 594-11 du code de procédure pénale, inséré dans ce code par l'article 2 du décret attaqué et pris pour l'application des dispositions citées ci-dessus : " Lorsqu'en application des dispositions du présent code un interprète ou un traducteur est requis ou désigné par l'autorité judiciaire compétente, celui-ci est choisi : / 1° Sur la liste nationale des experts judiciaires dressée par le bureau de la Cour de cassation, ou sur la liste des experts judiciaires dressée par chaque cour d'appel ; / 2° A défaut, sur la liste des interprètes traducteurs prévue par l'article R. 111-1 du code de l'entrée et du séjour des étrangers et du droit d'asile ; / 3° En cas de nécessité, il peut être désigné une personne majeure ne figurant sur aucune de ces listes, dès lors que l'interprète ou le traducteur n'est pas choisi parmi les enquêteurs, les magistrats ou les greffiers chargés du dossier, les parties ou les témoins. / Les interprètes ou les traducteurs ne figurant sur aucune des listes mentionnées au 1° ou au 2° prêtent, chaque fois qu'ils sont commis, le serment d'apporter leur concours à la justice en leur honneur et leur conscience. Leur serment est alors consigné par procès-verbal. / Les interprètes et les traducteurs sont tenus de respecter la confidentialité de l'interprétation et des traductions fournies " ; que M. A...demande l'annulation pour excès de pouvoir de ces dispositions ;<br/>
<br/>
              Sur les moyens tirés de la méconnaissance de la directive 2010/64/UE du 20 octobre 2010 :<br/>
<br/>
              4. Considérant que les dispositions réglementaires attaquées traitent des seules modalités de désignation des traducteurs interprètes ; qu'eu égard à leur objet, le requérant ne peut utilement se prévaloir d'une méconnaissance des dispositions des articles 2 et 3 de la directive citées au point 1, relatives à la qualité des prestations de traduction et d'interprétation ; que les dispositions du paragraphe 2 de l'article 5 de la directive laissent une large marge d'appréciation aux Etats membres pour en mettre en oeuvre les objectifs et n'imposent pas à ceux-ci de dresser un registre de traducteurs et interprètes indépendants ; qu'aucune disposition du décret ne méconnaît les dispositions de l'article 8 de la directive ; que les dispositions de la directive, ainsi qu'il a été rappelé ci-dessus, ont été transposées en droit interne par la loi du 5 août 2013 précitée, dont la conformité à la directive n'est pas contestée ; que les divers moyens tirés de la méconnaissance de la directive ne peuvent, dès lors, qu'être écartés ;<br/>
<br/>
              Sur les moyens tirés de la méconnaissance du droit interne :<br/>
<br/>
              5. Considérant que les dispositions de l'article préliminaire et de l'article 803-5 du code de procédure pénale consacrent le droit des personnes suspectées et poursuivies à l'assistance d'un interprète dans une langue qu'elles comprennent et à la traduction des pièces essentielles à l'exercice des droits de la défense et à la garantie du caractère équitable du procès ; que ces dispositions renvoient à un décret le soin d'en déterminer les modalités de mise en oeuvre ; <br/>
<br/>
              6. Considérant, en premier lieu, que les dispositions réglementaires attaquées prévoient qu'il est fait appel en priorité, pour assurer les prestations de traduction et d'interprétariat mentionnées au point 5, aux experts judiciaires figurant sur les listes nationale et locales d'experts judiciaires dressées par les juridictions ; qu'il n'est recouru qu'à défaut à la liste des interprètes traducteurs prévue par l'article R. 111-1 du code de l'entrée et du séjour des étrangers et du droit d'asile ; que ce n'est, en outre, qu'en cas de nécessité et à titre subsidiaire que peuvent être désignées d'autres personnes pour réaliser des prestations d'interprétariat ou de traduction ; que ces dispositions tendent à mieux garantir le droit des personnes suspectées ou poursuivies, en toutes circonstances, à accéder à un interprète et à bénéficier d'une traduction des pièces essentielles de la procédure ;<br/>
<br/>
              7. Considérant, d'une part, que s'agissant des experts judiciaires figurant sur les listes nationale et locales d'experts judiciaires dressées par les juridictions, la loi du 29 juin 1971 relative aux experts judiciaires et son décret d'application du 23 décembre 2004 prévoient des dispositions apportant des garanties quant aux conditions et aux modalités d'inscription d'une personne sur une telle liste, en particulier l'exigence que les demandes d'inscription sur les listes des experts judiciaires soient examinées en tenant compte notamment des qualifications et de l'expérience professionnelle des candidats ; que, d'autre part, l'établissement de la liste des interprètes traducteurs réalisée en application de l'article R. 111-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, est soumis à un ensemble de conditions destinées à assurer la compétence des personnes concernées en matière de traduction et d'interprétation, notamment par la justification d'un diplôme ou d'une expérience acquis en cette matière, ainsi que leur impartialité ; qu'enfin, le recours aux traducteurs interprètes ne relevant pas des listes mentionnées aux 1° et 2° de l'article D. 594-11 du code de procédure pénale ne peut concerner que des personnes majeures, ne pouvant être choisies parmi les enquêteurs, les magistrats ou les greffiers chargés du dossier, les parties ou les témoins et devant, chaque fois qu'elles sont commises, prêter le serment d'apporter leur concours à la justice en leur honneur et leur conscience ; que, comme tous les auxiliaires du service public de la justice, les intéressés sont tenus à une obligation d'impartialité ; qu'eu égard aux exigences rappelées ci-dessus, les dispositions contestées ne peuvent être regardées comme étant de nature à permettre la fourniture de prestations d'une qualité insuffisante pour assurer une mise en oeuvre effective du droit garanti par l'article préliminaire du code de procédure pénale ou, en tout état de cause, à porter atteinte à l'indépendance des traducteurs interprètes ; que, par suite, les moyens tirés de ce que les dispositions contestées méconnaîtraient l'article préliminaire du code de procédure pénale, les droits de la défense, le caractère équitable du procès et l'indépendance des traducteurs interprètes doivent être écartés ;  <br/>
<br/>
              8. Considérant, en deuxième lieu, qu'eu égard à l'objet et à la portée des dispositions attaquées, qui se bornent à fixer les modalités de désignation des traducteurs interprètes devant les tribunaux, le requérant ne peut utilement invoquer une méconnaissance des dispositions régissant les experts judiciaires et notamment les dénominations dont ils peuvent faire état ; que le moyen tiré de l'illégalité des dispositions réglementaires définissant les conditions d'exercice des experts judiciaires est également inopérant ; <br/>
<br/>
              9. Considérant, en dernier lieu, que M. A...ne peut, en tout état de cause, utilement invoquer la méconnaissance par le décret attaqué de la circulaire du Premier ministre du 25 avril 2013 relative à l'emploi de la langue française, qui est dépourvue de portée normative ; <br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin d'examiner la fin de non-recevoir opposée par le garde des sceaux, ministre de la justice, que M. A... n'est pas fondé à demander l'annulation des dispositions réglementaires qu'il attaque ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B...A..., au Premier ministre et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
