<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033157847</ID>
<ANCIEN_ID>JG_L_2016_09_000000391143</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/15/78/CETATEXT000033157847.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 23/09/2016, 391143, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-09-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391143</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Angélique Delorme</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:391143.20160923</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif de Nantes, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, de suspendre la décision implicite de rejet résultant du silence gardé par le directeur général de l'Institut national de la santé et de la recherche médicale (Inserm) sur sa demande, notifiée le 29 janvier 2015, tendant à la transformation de son contrat de travail à durée déterminée en un contrat à durée indéterminée avec l'Inserm et d'enjoindre au directeur général de l'Inserm de réexaminer sa demande.<br/>
<br/>
              Par une ordonnance n° 1504148 du 1er juin 2015, le juge des référés du tribunal administratif de Nantes a fait droit à la demande de M. A... de suspension de la décision implicite de rejet de sa demande de transformation de son contrat de recrutement à durée déterminée en un contrat à durée indéterminée, a enjoint au directeur général de l'Inserm de procéder au réexamen de la demande de l'intéressé dans un délai de deux mois à compter de la notification de l'ordonnance et a rejeté le surplus des conclusions de celui-ci.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 18 juin et 2 juillet 2015 et le 15 mars 2016 au secrétariat du contentieux du Conseil d'Etat, l'Inserm demande au Conseil d'Etat :<br/>
<br/>
               1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter les demandes de M. A...;<br/>
<br/>
              3°) de mettre à la charge de M. A...la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - la loi n° 2012-347 du 12 mars 2012 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Angélique Delorme, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de l'Institut national de la santé et de la recherche médicale (Inserm)  ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que M. A... a été recruté le 31 août 2006 en vertu d'un contrat à durée déterminée par l'Institut national de la santé et de la recherche médicale (Inserm) en qualité de chercheur et affecté à compter du 1er septembre 2006 au sein d'une unité de recherche. Il a été employé comme chercheur jusqu'au 31 janvier 2015, en vertu de plusieurs contrats à durée déterminée conclus avec divers employeurs de droit public ou de droit privé, en dernier lieu, à compter du 1er août 2013, par le centre hospitalier universitaire de Nantes dans une unité mixte de recherche située dans les locaux de ce centre hospitalier. <br/>
<br/>
              2. Par un courrier notifié au directeur général de l'Inserm le 29 janvier 2015, M. A...a demandé à ce dernier la transformation de son dernier contrat de recrutement à durée déterminée en contrat à durée indéterminée sur le fondement des dispositions des articles 6 et 6 bis de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, dans leur rédaction issue de l'entrée en vigueur de la loi du 12 mars 2012 relative à l'accès à l'emploi titulaire et à l'amélioration des conditions d'emploi des agents contractuels dans la fonction publique, à la lutte contre les discriminations et portant diverses dispositions relatives à la fonction publique. Une décision implicite de rejet est née du silence gardé par l'Inserm pendant plus de deux mois sur cette demande. L'Inserm se pourvoit en cassation contre l'ordonnance du 1er juin 2015 par laquelle le juge des référés du tribunal administratif de Nantes, saisi sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, a suspendu cette décision et a enjoint au directeur général de l'Inserm de procéder au réexamen de la demande de l'intéressé dans un délai de deux mois à compter de la notification de l'ordonnance.<br/>
<br/>
              3. D'une part, aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              4. D'autre part, aux termes de l'article 3 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires : " Sauf dérogation prévue par une disposition législative, les emplois civils permanents de l'Etat (...) et de leurs établissements publics à caractère administratif sont (...) occupés (...) par des fonctionnaires régis par le présent titre (...) ". Aux termes de l'article 4 de la loi du 11 janvier 1984 précitée : " Par dérogation au principe énoncé à l'article 3 du titre Ier du statut général, des agents contractuels peuvent être recrutés dans les cas suivants : / 1° Lorsqu'il n'existe pas de corps de fonctionnaires susceptibles d'assurer les fonctions correspondantes ; / 2° Pour les emplois du niveau de la catégorie A et, dans les représentations de l'Etat à l'étranger, des autres catégories, lorsque la nature des fonctions ou les besoins des services le justifient ". Aux termes du premier alinéa de l'article 6 de la même loi : " Les fonctions qui, correspondant à un besoin permanent, impliquent un service à temps incomplet d'une durée n'excédant pas 70 % d'un service à temps complet, sont assurées par des agents contractuels ". Enfin, aux termes de l'article 6 bis de cette loi, dans sa rédaction issue de l'article 37 de la loi du 12 mars 2012 précitée : " (...). / Tout contrat conclu ou renouvelé en application des (...) articles 4 et 6 avec un agent qui justifie d'une durée de services publics effectifs de six ans dans des fonctions relevant de la même catégorie hiérarchique est conclu, par une décision expresse, pour une durée indéterminée. / La durée de six ans mentionnée au deuxième alinéa du présent article est comptabilisée au titre de l'ensemble des services effectués dans des emplois occupés en application des articles 4, 6, 6 quater, 6 quinquies et 6 sexies. Elle doit avoir été accomplie dans sa totalité auprès du même département ministériel, de la même autorité publique ou du même établissement public. (...). / (...). / Lorsqu'un agent atteint l'ancienneté mentionnée aux deuxième à quatrième alinéas du présent article avant l'échéance de son contrat en cours, celui-ci est réputé être conclu à durée indéterminée. L'autorité d'emploi lui adresse une proposition d'avenant confirmant cette nouvelle nature du contrat (...) ". Il résulte de ces dispositions que la transformation en contrat à durée indéterminée d'un contrat de recrutement à durée déterminée conclu par un agent avec l'Etat ou l'un de ses établissements publics ne peut être constatée que si un tel contrat à durée déterminée est en cours à la date à laquelle l'agent concerné remplit la condition d'ancienneté de six ans. <br/>
<br/>
              5. Pour faire droit à la demande de suspension présentée par M. A..., le juge des référés a jugé que, quand bien même M. A...aurait été employé et rémunéré par d'autres personnes morales de droit public que l'Inserm au cours de la période du 2 janvier 2012 au 31 janvier 2015, le moyen tiré de l'erreur de droit qu'aurait commise l'Inserm en refusant de se reconnaître la qualité d'employeur ou de co-employeur de M. A...au cours d'une période de plus de six années et, à ce titre, de faire droit à sa demande présentée sur le fondement des articles 6 et 6 bis de la loi du 11 janvier 1984 modifiée était de nature à faire naître un doute sérieux quant à la légalité de la décision contestée. En décidant de ne tenir aucun compte de ce qu'il ressortait des pièces du dossier que M. A... était lié, à la date de sa demande, par un contrat de droit public, au centre hospitalier universitaire de Nantes et non à l'Inserm, le juge des référés a violé les dispositions citées au point 4. Par suite, l'Inserm est fondé à demander l'annulation de l'ordonnance attaquée.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de faire application de l'article L. 821-2 du code de justice administrative et de statuer sur la demande de suspension présentée par M.A....<br/>
<br/>
              7. M. A...fait valoir qu'il a exercé pendant plus de six années ses fonctions au sein d'unités des recherches rattachées à l'Inserm. Il ajoute qu'il est sans importance que ce fût en vertu de contrats passés avec divers employeurs, dans le cadre de transferts successifs entre diverses autorités publiques ou associations ou fondations de recherche dont le seul objet est de permettre la recherche de dons et l'allocation de diverses sources de financement à un même projet scientifique. Selon lui, l'Inserm aurait été, soit le bénéficiaire exclusif, soit le co-bénéficiaire des résultats de ses travaux. <br/>
<br/>
              8. Il résulte toutefois de ce qui a été dit ci-dessus qu'eu égard au contrat de droit public qui le liait au centre hospitalier universitaire de Nantes à la date de l'introduction de sa demande, laquelle tendait en définitive, ainsi qu'il vient d'être dit, à la transformation d'un contrat de travail à durée déterminée conclu avec le centre hospitalier universitaire de Nantes en contrat de travail à durée indéterminée avec l'Inserm, M. A...ne fait état d'aucun moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision contestée. Dès lors, sa demande doit être rejetée.<br/>
<br/>
              9. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A...une somme à verser à l'Inserm au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Inserm qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Nantes du 1er juin 2015 est annulée.<br/>
Article 2 : La demande de suspension présentée par M. A...au juge des référés du tribunal administratif de Nantes est rejetée.<br/>
Article 3 : Le surplus des conclusions du pourvoi de l'Inserm est rejeté.<br/>
Article 4 : Les conclusions de M. A...présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à l'Institut national de la santé et de la recherche médicale et à M. B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
