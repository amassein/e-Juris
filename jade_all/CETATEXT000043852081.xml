<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043852081</ID>
<ANCIEN_ID>JG_L_2021_07_000000439915</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/85/20/CETATEXT000043852081.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 22/07/2021, 439915, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439915</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Clément Tonon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:439915.20210722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 2 avril 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° CS 2020-04 du 8 janvier 2020 de la commission des sanctions de l'Agence française de lutte contre le dopage (AFLD) qui, d'une part, lui interdit, pour une durée de quatre ans à compter de sa notification, premièrement, de participer directement ou indirectement à l'organisation et au déroulement de toute manifestation sportive donnant lieu à une remise de prix en argent ou en nature, deuxièmement de participer directement ou indirectement à l'organisation et au déroulement de toute manifestation sportive organisée ou autorisée par une fédération sportive française délégataire ou agréée ainsi qu'aux entraînements y préparant organisés par une fédération agréée ou l'un des membres de celle-ci, troisièmement, d'exercer les fonctions définies à l'article L. 212-1 du code du sport, enfin, d'exercer toute fonction d'encadrement au sein d'une fédération agréée ou d'un groupement ou d'une association affilié à une telle fédération et, d'autre part, ordonne la publication du résumé de sa décision sur le site internet de l'AFLD pendant toute la durée de l'interdiction prononcée ;<br/>
<br/>
              2°) de prononcer à son encontre un avertissement ou, à titre subsidiaire, une interdiction de deux ans avec déduction d'une durée correspondant à celle de la suspension provisoire ordonnée le 11 avril 2019 ;<br/>
<br/>
              3°) de mettre à la charge de l'AFLD la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
                      Vu les autres pièces du dossier ;<br/>
<br/>
                      Vu :<br/>
                      - le code du sport ;<br/>
                      - le décret n° 2018-6 du 4 janvier 2018 ;<br/>
                      - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Clément Tonon, auditeur,  <br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M. B... et à la SCP Matuchansky, Poupot, Valdelièvre, avocat de l'Agence française de lutte contre le dopage ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	M. A... B..., titulaire d'une licence délivrée par la fédération française de rugby à XIII, a fait l'objet d'un contrôle antidopage, le 15 novembre 2018, à l'occasion d'un entraînement. L'analyse effectuée a fait ressortir la présence dans ses urines de sept substances prohibées. Par une décision du 11 avril 2019 prise en application de l'article 232-23-4 du code du sport, la présidente de l'Agence française de lutte contre le dopage (AFLD) a prononcé à l'encontre de M. B... une suspension provisoire à titre conservatoire. Par une décision du 8 janvier 2020, la commission des sanctions de l'AFLD a prononcé une sanction comportant notamment l'interdiction pour l'intéressé de pratiquer diverses activités dans le domaine sportif pendant une durée de quatre ans, l'article 2 de la décision précisant qu'il n'y a pas lieu de déduire de cette durée la période pendant laquelle la suspension provisoire aurait dû produire ses effets, et a ordonné la publication du résumé de cette décision sur le site internet de l'AFLD pendant toute la durée de l'interdiction. M. B... demande l'annulation de cette décision.<br/>
<br/>
              Sur la procédure de contrôle :<br/>
<br/>
              2.	En premier lieu, aux termes du premier alinéa de l'article L. 232-12 du code du sport : " Les opérations de contrôle sont diligentées par le directeur du département des contrôles de l'Agence française de lutte contre le dopage qui peut donner délégation aux agents placés sous son autorité hiérarchique (...) ". Il résulte de l'instruction, notamment de l'ordre de mission de contrôle antidopage du 8 novembre 2018, que le moyen tiré de ce que le contrôle effectué le 15 novembre 2018 serait irrégulier faute d'avoir été autorisé par le directeur du département des contrôles de l'AFLD manque en fait.<br/>
<br/>
              3.	En deuxième lieu, aux termes de l'article L. 232-13-1 du code du sport : " Les contrôles peuvent être réalisés : / 1° Dans tout lieu où se déroule un entraînement ou une manifestation mentionnés à l'article L. 230-3 (...) ". En vertu des dispositions de l'article L. 230-3 du même code : " Est un sportif au sens du présent titre toute personne qui participe ou se prépare : / 1° Soit à une manifestation sportive organisée par une fédération agréée ou autorisée par une fédération délégataire (...) ". Il résulte de ces dispositions qu'un contrôle antidopage peut être diligenté à l'encontre de toute personne prenant part à un entraînement préparant à des manifestations sportives organisées par une fédération agréée ou autorisée par une fédération délégataire, sans qu'ait d'incidence la circonstance que l'intéressé ne soit pas licencié au sein du club dans le cadre duquel l'entraînement a lieu. <br/>
<br/>
              4.	Si M. B... fait valoir que le contrôle antidopage du 15 novembre 2018 a été diligenté dans le cadre d'un entraînement du " FC Lézignan XIII ", alors qu'il n'était pas licencié au sein de ce club mais était affilié, en qualité d'amateur, auprès de " l'Union sportive Ferralaise XIII ", il résulte de ce qui précède que cette circonstance n'était pas de nature à faire obstacle à son contrôle, dès lors qu'il était titulaire d'une licence délivrée par la fédération française de rugby à XIII et participait à un entraînement le préparant à des rencontres organisées par cette fédération sportive délégataire. Au demeurant, il résulte de l'instruction que le club auquel il était affilié constitue la réserve de l'équipe première du " FC Lézignan XIII " et qu'à ce titre, l'intéressé était amené à participer à des entraînements et à des rencontres officielles de cette équipe. Par suite, le moyen tiré de l'irrégularité du contrôle antidopage dont a fait l'objet M. B... à l'occasion de l'entraînement du 15 novembre 2018 au regard des dispositions des articles L. 232-13-1 et L. 230-3 du code du sport ne peut qu'être écarté.<br/>
<br/>
              Sur la sanction :<br/>
<br/>
              5.	En premier lieu, aux termes de l'article L. 232-23 du code du sport dans sa rédaction alors applicable : " I. - La commission des sanctions de l'Agence française de lutte contre le dopage peut prononcer à l'encontre des personnes ayant enfreint les dispositions des articles L. 232-9, L. 232-9-1, L. 232-9-2, L. 232-9-3, L. 232-10, L. 232-14-5 ou L. 232-17 : / 1° Un avertissement ; / 2° Une interdiction temporaire ou définitive : / a) De participer directement ou indirectement à l'organisation et au déroulement de toute manifestation sportive donnant lieu à une remise de prix en argent ou en nature, et des manifestations sportives autorisées par une fédération délégataire ou organisées par une fédération agréée ou par une ligue sportive professionnelle ainsi qu'aux entraînements y préparant organisés par une fédération agréée ou une ligue professionnelle ou l'un des membres de celles-ci ; / b) D'exercer les fonctions définies à l'article L. 212-1 ; / c) D'exercer les fonctions de personnel d'encadrement ou toute activité administrative au sein d'une fédération agréée ou d'une ligue professionnelle, ou de l'un de leurs membres ; / d) Et de prendre part à toute autre activité organisée par une fédération sportive, une ligue professionnelle ou l'un de leurs membres, ou le comité national olympique et sportif français, ainsi qu'aux activités sportives impliquant des sportifs de niveau national ou international et financées par une personne publique, à moins que ces activités ne s'inscrivent dans des programmes ayant pour objet la prévention du dopage ". L'article L. 232-23-3-3 du même code, dans sa rédaction alors en vigueur, dispose que : " La durée des mesures d'interdiction mentionnées au 1° du I de l'article L. 232-23 à raison d'un manquement à l'article L. 232-9 : / a) Est de quatre ans lorsque ce manquement est consécutif à l'usage ou à la détention d'une substance non spécifiée. Cette durée est ramenée à deux ans lorsque le sportif démontre qu'il n'a pas eu l'intention de commettre ce manquement ; / b) Est de deux ans lorsque ce manquement est consécutif à l'usage ou à la détention d'une substance spécifiée. Cette durée est portée à quatre ans lorsqu'il est démontré que le sportif a eu l'intention de commettre ce manquement ". Aux termes de l'article L. 232-23-3-10 du même code : " La durée des mesures d'interdiction prévues aux articles L. 232-23-3-3 à L. 232-23-3-8 peut être réduite par une décision spécialement motivée lorsque les circonstances particulières de l'affaire le justifient au regard du principe de proportionnalité ". <br/>
<br/>
              6.	Il résulte de l'instruction que l'analyse effectuée à la suite du contrôle antidopage réalisé le 15 novembre 2018 a fait ressortir la présence dans les urines de M. B... de sept substances figurant sur la liste des substances interdites en permanence annexée au décret n° 2018 6 du 4 janvier 2018, dont six appartiennent à la classe S1 des agents anabolisants et sont répertoriées parmi les substances dites " non spécifiées ". Si l'intéressé soutient que la quantité des substances prohibées détectée dans ses urines est très faible et que leur présence ne pourrait provenir, pour les anabolisants, que d'une ingestion involontaire résultant de l'absorption d'un complément alimentaire et, pour la septième substance " spécifiée", de la confusion accidentelle avec un médicament prescrit à sa compagne, il n'apporte pas d'éléments permettant d'établir la présence des substances en cause dans le complément alimentaire qu'il allègue avoir absorbé ni justifiant de son prétendu manque de vigilance quant aux produits consommés, alors même qu'il pratique son sport en amateur. Dans les circonstances de l'espèce, eu égard à la nature et au nombre des substances mises en évidence et à la gravité du manquement constaté, la durée de quatre ans de la sanction prononcée par la commission des sanctions de l'AFLD n'est pas disproportionnée.<br/>
<br/>
              7.	En second lieu, aux termes du dernier alinéa de l'article L. 232-23-4 du code du sport : " La durée de la suspension provisoire est déduite de la durée de l'interdiction de participer aux manifestations sportives que la commission des sanctions peut ultérieurement prononcer ". Pour l'application de ces dispositions, il n'y a toutefois lieu de déduire la durée de la suspension provisoire de la durée de la sanction d'interdiction que si l'intéressé a effectivement suspendu son activité durant la période couverte par la mesure provisoire. Tel n'est pas le cas lorsque l'intéressé n'a pas respecté la mesure de suspension provisoire prononcée à son encontre, ce comportement étant alors de nature à justifier que la période de suspension, pour l'ensemble de sa durée, ne soit pas déduite de la durée de l'interdiction prononcée à titre de sanction.<br/>
<br/>
              8.	Il résulte de l'instruction, notamment de feuilles de match des 20 et 27 avril 2019, que M. B... a participé à des manifestations sportives organisées par la fédération française de rugby à XIII. Il a ainsi méconnu l'interdiction qui lui était faite, par la mesure de suspension provisoire prononcée par la présidente de l'AFLD le 11 avril 2019, de participer à de telles manifestations. Dans ces conditions, le moyen tiré de l'illégalité de l'absence de déduction de la période pendant laquelle la suspension provisoire aurait dû produire ses effets ne peut qu'être écarté.<br/>
<br/>
              9.	Il résulte de tout ce qui précède que M. B... n'est pas fondé à demander l'annulation de la décision qu'il attaque.<br/>
<br/>
              10.	Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Agence française de lutte contre le dopage qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B... une somme de 1 500 euros à verser à l'Agence au même titre.<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : M. B... versera à l'Agence française de lutte contre le dopage la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à M. A... B... et à l'Agence française de lutte contre le dopage.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
