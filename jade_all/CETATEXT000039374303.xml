<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039374303</ID>
<ANCIEN_ID>JG_L_2019_11_000000427436</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/37/43/CETATEXT000039374303.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 13/11/2019, 427436, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-11-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427436</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:427436.20191113</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La communauté de communes Artois-Lys a demandé au tribunal administratif de Lille de condamner solidairement M. C... D..., M. B... A..., la société Euro Ingénierie, la société Contracts Middle East, venue aux droits de la société Rabot Dutilleul Entreprises, la Mutuelle des Architectes Français et la société Lloyd's à lui verser une somme de 43 549,95 euros à titre de réparation des conséquences dommageables des désordres apparus après la réception des travaux d'aménagement et d'extension d'un bâtiment lui appartenant, situé à Lillers. Par un jugement n° 1203282 du 7 juillet 2015, le tribunal administratif de Lille a, après avoir mis hors de cause la société Contracts Middle East, rejeté la demande de la communauté de communes Artois-Lys et mis à sa charge les frais et honoraires de l'expertise.<br/>
<br/>
              Par un arrêt n° 15DA01475 du 22 novembre 2018, la cour administrative d'appel de Douai a, sur appel de la communauté d'agglomération de Béthune-Bruay-Artois-Lys Romane, venant aux droits de la communauté de communes Artois-Lys, notamment annulé le jugement du tribunal administratif de Lille en tant qu'il mettait hors de cause la société Contracts Middle East et condamné cette société à verser à la communauté d'agglomération la somme de 40 607,79 euros au titre de la garantie décennale et 4 647,14 euros au titre des frais d'expertise, puis mis à la charge solidaire de la société Contracts Middle East et de la société Rabot Dutilleul Construction la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 28 janvier et 29 avril 2019 au secrétariat du contentieux du Conseil d'Etat, la société Rabot Dutilleul Construction demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la communauté d'agglomération de Béthune-Bruay-Artois-Lys Romane la somme de 1 600 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Sirinelli, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gadiou, Chevallier, avocat de la société Rabot Dutilleul Construction ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la communauté de communes Artois-Lys a décidé d'engager des travaux d'aménagement et d'extension d'un bâtiment lui appartenant, situé à Lillers, afin d'y implanter son siège. Par un acte d'engagement signé le 24 novembre 2000, l'exécution des travaux correspondant au lot n° 1 a été confiée à la société Rabot Dutilleul Entreprises. A la suite de l'apparition de désordres, la communauté d'agglomération Artois-Lys a demandé au tribunal administratif de Lille de condamner solidairement les maîtres d'oeuvre ainsi que la société Contracts Middle East, venue aux droits de la société Rabot Dutilleul Entreprises, la Mutuelle des Architectes Français et la société Lloyd's à lui verser une somme de 43 549,95 euros à titre de réparation des conséquences dommageables de ces désordres. Par un jugement du 7 juillet 2015, le tribunal administratif de Lille a rejeté sa demande, après avoir mis hors de cause la société Contracts Middle East, et mis à sa charge les frais et honoraires de l'expertise. La société Rabot Dutilleul Constructions se pourvoit en cassation contre l'arrêt du 22 novembre 2018 par lequel la cour administrative d'appel de Douai a, sur appel de la communauté d'agglomération de Béthune-Bruay-Artois-Lys Romane, venant aux droits de la communauté de communes Artois-Lys, notamment annulé le jugement du tribunal administratif de Lille en tant qu'il mettait hors de cause la société Contracts Middle East et condamné cette société à verser à la communauté d'agglomération la somme de 40 607,79 euros au titre de la garantie décennale et 4 647,14 euros au titre des frais d'expertise, puis mis à la charge solidaire de la société Contracts Middle East et de la société Rabot Dutilleul Construction la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. Son pourvoi doit être regardé, eu égard aux moyens invoqués, comme tendant seulement à l'annulation de cet arrêt en tant qu'il a mis solidairement à sa charge une somme au titre de ces dispositions.<br/>
<br/>
              2. Aux termes de l'article L. 761-1 du code de justice administrative : " Dans toutes les instances, le juge condamne la partie tenue aux dépens ou, à défaut, la partie perdante, à payer à l'autre partie la somme qu'il détermine, au titre des frais exposés et non compris dans les dépens. Le juge tient compte de l'équité ou de la situation économique de la partie condamnée. Il peut, même d'office, pour des raisons tirées des mêmes considérations, dire qu'il n'y a pas lieu à cette condamnation ".<br/>
<br/>
              3. Il ressort des énonciations de l'arrêt attaqué que la cour administrative d'appel de Douai a relevé qu'en raison d'un apport partiel d'actifs de la société Rabot Dutilleul Construction à la société Contracts Middle East, seule cette dernière devait répondre des désordres en litige au titre de la garantie décennale des constructeurs. La cour a, par suite, condamné cette seule société à indemniser le maître d'ouvrage et à supporter les frais d'expertise. Elle a également mis à sa charge la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. Par suite, elle a entaché son arrêt d'une contradiction de motifs et d'une erreur de droit en mettant cette somme également à la charge solidaire de la société Rabot Dutilleul Construction, alors qu'elle n'avait prononcé aucune condamnation à l'encontre de celle-ci, qu'elle n'avait pas considérée comme la partie perdante. Il suit de là que la société Rabot Dutilleul Construction est fondée, sans qu'il soit besoin d'examiner l'autre moyen de son pourvoi, à demander l'annulation de l'arrêt qu'elle attaque en tant qu'il a mis à sa charge solidaire une somme au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond en application de dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Rabot Dutilleul Construction qui n'est pas la partie perdante en appel. En revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la communauté d'agglomération de Béthune-Bruay-Artois-Lys Romane une somme de 1 600 euros à verser à la société Rabot Dutilleul Construction au titre des frais exposés et non compris dans les dépens devant le Conseil d'Etat.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 5 de l'arrêt de la cour administrative d'appel de Douai du 22 novembre 2018 est annulé en tant qu'il a mis à la charge solidaire de la société Rabot Dutilleul Construction la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 2 : Les conclusions de la communauté d'agglomération de Béthune-Bruay-Artois-Lys Romane présentées devant la cour administrative d'appel de Douai à l'encontre de la société Rabot Dutilleul Construction au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La communauté d'agglomération de Béthune-Bruay-Artois-Lys Romane versera à la société Rabot Dutilleul Construction une somme de 1 600 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société Rabot Dutilleul Construction.<br/>
Copie en sera adressée à la communauté d'agglomération de Béthune-Bruay-Artois-Lys Romane et à la société Contracts Middle East. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
