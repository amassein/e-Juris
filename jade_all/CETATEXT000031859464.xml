<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031859464</ID>
<ANCIEN_ID>J5_L_2015_12_000001500802</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/85/94/CETATEXT000031859464.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de NANCY, 1ère chambre - formation à 3, 17/12/2015, 15NC00802, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-17</DATE_DEC>
<JURIDICTION>CAA de NANCY</JURIDICTION>
<NUMERO>15NC00802</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre - formation à 3</FORMATION>
<TYPE_REC>excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>Mme MONCHAMBERT</PRESIDENT>
<AVOCATS>THABET</AVOCATS>
<RAPPORTEUR>Mme Colette  STEFANSKI</RAPPORTEUR>
<COMMISSAIRE_GVT>M. FAVRET</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
       Procédure contentieuse antérieure :<br/>
<br/>
       M. B...a demandé au tribunal administratif de Strasbourg d'annuler, d'une part une décision implicite de rejet d'une délivrance d'une carte de séjour de dix ans qui aurait été révélée par la délivrance, par le préfet du Bas-Rhin d'un titre de séjour d'un an, d'autre part, de l'arrêté du 6 novembre 2014 par lequel le préfet du Bas-Rhin a refusé de lui délivrer un titre de séjour, lui a fait obligation de quitter le territoire et a fixé le pays à destination duquel il pourra être reconduit d'office.<br/>
<br/>
       Par un jugement nos 1402287 et 1406590 du 16 avril 2015, le tribunal administratif de Strasbourg a rejeté ses demandes.<br/>
<br/>
       Procédure devant la cour :<br/>
<br/>
       Par une requête enregistrée le 30 avril 2015, M.B..., représenté par MeC..., demande à la cour :<br/>
<br/>
       1°) d'annuler le jugement n° 1402287 et 1406590 du 16 avril 2015 du tribunal administratif de Strasbourg ;<br/>
<br/>
       2°) d'annuler les décisions contestées du préfet du Bas-Rhin ;<br/>
<br/>
       3°) de mettre à la charge de l'Etat une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
       Il soutient que :<br/>
<br/>
       - le préfet devait lui accorder d'office un titre de séjour de dix ans en application de l'article 10 a de l'accord franco-tunisien, sans rechercher s'il l'avait demandé, alors au surplus qu'il avait sollicité verbalement un tel titre auprès d'un agent de la préfecture ;<br/>
       - la rupture de vie commune est sans incidence sur le titre de séjour délivré à un conjoint en application d'une convention bilatérale.<br/>
<br/>
<br/>
       Par un mémoire en défense, enregistré le 9 octobre 2015, le préfet du Bas-Rhin conclut au rejet de la requête. <br/>
<br/>
       Il soutient que faute de contester le jugement, la requête est irrecevable et qu'à titre subsidiaire les moyens soulevés par le requérant ne sont pas fondés.<br/>
<br/>
<br/>
       M. B...a été admis au bénéfice de l'aide juridictionnelle totale par une décision du 10 septembre 2015. <br/>
<br/>
<br/>
       Vu les autres pièces du dossier.<br/>
<br/>
       Vu :<br/>
       - l'accord franco-tunisien du 17 mars 1988 modifié ; <br/>
       - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
       - le code de justice administrative. <br/>
<br/>
<br/>
       Le président de la formation de jugement a dispensé le rapporteur public, sur sa proposition, de prononcer des conclusions à l'audience.<br/>
<br/>
       Les parties ont été régulièrement averties du jour de l'audience.<br/>
<br/>
       Le rapport de Mme Stefanski, président, a été entendu au cours de l'audience publique.<br/>
<br/>
<br/>
       Considérant ce qui suit :<br/>
<br/>
       1. M.B..., de nationalité tunisienne, a épousé en France une ressortissante française le 26 juin 2012. Il est entré sur le territoire national le 23 novembre 2012 sous couvert d'un passeport revêtu d'un visa de long séjour portant la mention ''vie privée et familiale'' en raison de son mariage et valant titre de séjour jusqu'au 9 novembre 2013. Le 23 septembre 2013, le requérant a demandé un premier titre de séjour et s'est présenté à la préfecture accompagné de son épouse et a présenté des documents de nature à établir la réalité de la communauté de vie. Le préfet du Bas-Rhin lui a délivré, sur le fondement de l'article L. 313-10 du code de l'entrée et du séjour des étrangers et du droit d'asile, une carte de séjour temporaire valable du 10 novembre 2013 au 9 novembre 2014. Le 22 avril 2014, M B...a présenté un recours gracieux demandant au préfet de retirer le refus implicite de lui délivrer un titre de séjour de 10 ans sur le fondement de l'accord franco-tunisien en soutenant que cette décision avait été révélée par l'attribution de la carte de séjour temporaire d'un an. En l'absence de réponse du préfet, il a saisi le 17 avril 2014, le tribunal administratif d'un recours pour excès de pouvoir dirigé contre le rejet implicite de son recours gracieux né du silence de l'administration. Par ailleurs, le 9 septembre 2014 M. B...s'est présenté à la préfecture pour demander le renouvellement de sa carte de séjour temporaire d'un an et alors indiqué aux services qu'il était séparé de son épouse qui avait demandé le divorce en fournissant une convocation du 12 avril 2013 pour une tentative de conciliation du 26 septembre 2013 devant le juge des affaires familiales. Constatant la rupture de la vie commune, le préfet du Bas-Rhin a refusé le renouvellement du titre de séjour par arrêté du 6 novembre 2014. M. B...interjette appel du jugement du tribunal administratif de Strasbourg qui a rejeté sa demande dirigée contre le rejet de son recours gracieux ainsi que celle dirigée contre le refus de renouvellement du 6 novembre 2014.<br/>
<br/>
       2. Aux termes de l'article 10 de l'accord franco tunisien 17 mars 1988 modifié : " Un titre de séjour d'une durée de dix ans, ouvrant droit à l'exercice d'une activité professionnelle, est délivré de plein droit, sous réserve de la régularité du séjour sur le territoire français : / a) Au conjoint tunisien d'un ressortissant français, marié depuis au moins un an, à condition que la communauté de vie entre époux n'ait pas cessé, que le conjoint ait conservé sa nationalité française et, lorsque le mariage a été célébré à l'étranger, qu'il ait été transcrit préalablement sur les registres de l'état civil français. ". Aux termes de l'article 11 du même accord : " Les dispositions du présent accord ne font pas obstacle à l'application de la législation des deux États sur le séjour des étrangers sur tous les points non traités par l'accord. Chaque État délivre notamment aux ressortissants de l'autre État tous titres de séjours autres que ceux visés au présent accord, dans les conditions prévues par sa législation. ". <br/>
<br/>
       3. Ces stipulations, qui régissent de manière complète les conditions dans lesquelles un titre de séjour est délivré de plein droit aux ressortissants tunisiens mariés à un ressortissant français, font obstacle à l'application, pour ces ressortissants, des dispositions du 4° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile relatif à l'attribution d'une carte de séjour temporaire d'un an aux conjoints de français.<br/>
<br/>
       4. Dès lors que M.B..., ressortissant tunisien avait le 23 septembre 2013 sollicité un titre de séjour en qualité de conjoint d'une française, il appartenait au préfet, alors même que l'intéressé ne l'avait pas demandé explicitement, de statuer sur le fondement des seules stipulations de l'accord franco-tunisien qui prévoient l'attribution à certaines conditions d'un titre de séjour de dix ans et non comme il l'a fait, en appliquant le 4° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile prévoyant l'attribution d'une carte de séjour temporaire d'un an. Toutefois, M. B...n'a pas contesté, dans le délai du recours contentieux, la carte de séjour temporaire accordée par le préfet. Contrairement à ce que fait valoir le requérant la décision de délivrance de cette carte, alors même qu'elle était entachée d'erreur de droit en ce que le préfet n'avait pas statué sur le fondement de l'accord franco-tunisien, ne peut être regardée comme ayant révélé une autre décision divisible d'elle et consistant en refus implicite de statuer en appliquant cet accord, pour laquelle le délai de recours contentieux n'aurait pas encore commencé à courir quand l'intéressé a introduit un recours gracieux contre elle le 22 avril 2014. Dans ces conditions, la demande de première instance était irrecevable faute d'être dirigée contre une décision. <br/>
<br/>
       5. Pour contester le refus de renouvellement de sa carte de séjour temporaire d'un an, qui avait, en tout état de cause, été accordée au regard d'informations inexactes données par M. B...et son épouse sur la réalité de leur vie commune à la date de sa délivrance, le requérant ne soulève aucun moyen propre à ce refus et doit être regardé comme se bornant à faire valoir qu'il est illégal en raison de l'absence d'application de l'accord franco-tunisien à la première demande de carte de séjour temporaire. Dans ces conditions et compte tenu de ce que l'intéressé n'a pas attaqué dans le délai de recours contentieux la décision appliquant le code de l'entrée et du séjour des étrangers et du droit d'asile, ainsi qu'il est dit ci-dessus, les conclusions tendant à l'annulation de ce refus de renouvellement ne peuvent qu'être rejetées. <br/>
<br/>
       6. Il résulte de ce qui précède que M. B...n'est pas fondé à se plaindre de ce que, par le jugement attaqué, le tribunal administratif de Strasbourg a rejeté ses demandes. Ses conclusions à fin d'application de l'article L. 761-1 du CJA ne peuvent, en conséquence, qu'être écartées.<br/>
<br/>
<br/>
D E C I D E :<br/>
<br/>
Article 1er : La requête de M. B...est rejetée. <br/>
Article 2 : Le présent arrêt sera notifié à M. B...et au ministre de l'intérieur.<br/>
Copie en sera adressée au préfet du Bas-Rhin.<br/>
<br/>
''<br/>
''<br/>
''<br/>
''<br/>
2<br/>
N° 15NC00802<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-01 Étrangers. Séjour des étrangers.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
