<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034971135</ID>
<ANCIEN_ID>JG_L_2017_06_000000394823</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/97/11/CETATEXT000034971135.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 19/06/2017, 394823, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394823</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>DELAMARRE</AVOCATS>
<RAPPORTEUR>M. Julien Anfruns</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:394823.20170619</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1. M.  E...F...a demandé au tribunal administratif de Paris, à titre principal d'annuler la décision en date du 27 juin 2012 par laquelle la ministre de la culture et de la communication l'a mis à la retraite pour atteinte de la limite d'âge et, à titre subsidiaire, d'une part, d'annuler cette décision en tant qu'elle accorde cette mise à la retraite en qualité de vérificateur des monuments historiques et non de technicien des services culturels et des Bâtiments de France, d'autre part, en tant qu'elle lui refuse une pension de retraite et, enfin, d'enjoindre au ministre de mettre en oeuvre les dispositions des articles L. 161-17, D. 161-2-1-2 à D. 161-2-1-8-3 du code de la sécurité sociale relatives au droit à l'information en matière de retraite. Par un jugement n° 1215559 du 10 juin 2013, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13PA03243 du 19 novembre 2015, enregistré le 25 novembre 2015 au secrétariat du contentieux du Conseil d'Etat sous le n° 394823, la cour administrative d'appel de Paris a transmis au Conseil d'État, en application du 3° de l'article R. 222-12 du code de justice administrative, le pourvoi présenté à cette cour par M. F...le 9 août 2013 ainsi que les mémoires complémentaires enregistrés les 28 mars, 14 mai et 22 juillet 2014.<br/>
<br/>
              M. F...demande :<br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Paris en tant qu'il a statué en matière de pension ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Par un arrêt n°13PA03244 du 19 novembre 2015, enregistré le 25 novembre 2015 au secrétariat du contentieux du Conseil d'Etat sous le n° 394831, la cour administrative d'appel de Paris a transmis au Conseil d'Etat, en application du 3° de l'article R. 222-12 du code de justice administrative, le pourvoi présenté à cette cour par M. D...le 9 août 2013 ainsi que les mémoires complémentaires enregistrés les 27 mars et 14 mai 2014.<br/>
<br/>
              M. D...demande :<br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Paris en tant qu'il a statué en matière de pension ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Par un arrêt n°13PA03242 du 19 novembre 2015, enregistré le 26 novembre 2015 au secrétariat du contentieux du Conseil d'Etat sous le n° 394855, la cour administrative d'appel de Paris a transmis au Conseil d'Etat, en application du 3° de l'article R. 222-12 du code de justice administrative, le pourvoi présenté à cette cour par M. C...le 9 août 2013 ainsi que les mémoires complémentaires enregistrés les 27 mars et 14 mai 2014.<br/>
<br/>
              M. C...demande :<br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Paris en tant qu'il a statué en matière de pension ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ; <br/>
              -la loi n° 84-834 du 13 septembre 1984 ; <br/>
              - le décret du 22 mars 1908 ;<br/>
              - le décret n° 2012-229 du 16 février 2012 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Julien Anfruns, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Delamarre, avocat de MM.F..., D...et C...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les pourvois de MM.F..., D...et C...sont dirigés contre des jugements du tribunal administratif de Paris qui présentent à juger les mêmes questions. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Il ressort des pièces des dossiers soumis aux juges du fond que MM.F..., D...et C...ont été nommés, par arrêtés ministériels, dans le corps des vérificateurs du service d'architecture des bâtiments civils et des palais nationaux, dits " vérificateurs des monuments historiques ", régi par les dispositions du décret du 22 mars 1908 relatif à l'organisation du service d'architecture des bâtiments civils et des palais nationaux, alors en vigueur. Le Premier ministre a, par décret du 16 février 2012 portant statut particulier du corps des techniciens des services culturels et des Bâtiments de France, procédé à l'abrogation du décret du 22 mars 1908 et prévu l'intégration des vérificateurs des monuments historiques dans le corps des techniciens de service culturel et des Bâtiments de France. Toutefois, MM. F..., D...et C...ayant atteint l'âge de soixante-cinq ans, et ainsi dépassé la limite d'âge du corps des vérificateurs des monuments historiques, prévue par le décret du 22 mars 1908, avant l'entrée en vigueur du décret du 16 février 2012, le ministre chargé de la culture a, par trois arrêtés du 27 juin 2012, prononcé rétroactivement la cessation de leurs fonctions de vérificateur des monuments historiques. Par lettres du même jour, il leur a indiqué qu'eu égard au mode de rémunération par honoraires des vérificateurs des monuments historiques, ils ne pouvaient prétendre à aucune pension civile de l'Etat. Par trois jugements du 10 juin 2013, le tribunal administratif de Paris a rejeté, d'une part, leurs demandes tendant, à titre principal, à l'annulation des arrêtés par lesquels le ministre les a mis à la retraite pour atteinte de la limite d'âge, à titre subsidiaire, à leur annulation en tant qu'ils leur accordent cette mise à la retraite en tant que vérificateurs des monuments historiques et non en tant que technicien des services culturels et des Bâtiments de France et, d'autre part, à l'annulation des décisions refusant de leur attribuer une pension de retraite. Par trois arrêts du 19 novembre 2015, la cour administrative d'appel de Paris a transmis au Conseil d'Etat, en application du 3° de l'article R. 222-12 du code de justice administrative, les conclusions de MM.F..., D...et C...tendant à l'annulation de ces jugements en tant qu'ils rejettent leurs demandes de pension de retraite et a rejeté le surplus de leurs appels.<br/>
<br/>
              3. En premier lieu, aux termes de l'article L. 2 du code des pensions civiles et militaires de retraite : " Ont droit au bénéfice des dispositions du présent code: 1° Les fonctionnaires civils / (...) ". Aux termes de l'article L. 4 de ce code : " Le droit à pension est acquis : / 1° aux fonctionnaires après une durée fixée par décret en Conseil d'Etat .... ". Aux termes de l'article L. 13 du même code : " I.-La durée des services et bonifications admissibles en liquidation s'exprime en trimestres. Le nombre de trimestres nécessaires pour obtenir le pourcentage maximum de la pension civile ou militaire est fixé à cent soixante trimestres. / Ce pourcentage maximum est fixé à 75 % du traitement ou de la solde mentionné à l'article L. 15. ". Aux termes, enfin, de l'article L. 15 du même code : " Aux fins de liquidation de la pension, le montant de celle-ci est calculé en multipliant le pourcentage de liquidation tel qu'il résulte de l'application de l'article L. 13 par le traitement ou la solde soumis à retenue afférents à l'indice correspondant à l'emploi, grade, classe et échelon effectivement détenus depuis six mois au moins par le fonctionnaire ou militaire au moment de la cessation des services valables pour la retraite (...) ".<br/>
<br/>
              4. Il ressort des énonciations des jugements attaqués que le tribunal, après avoir constaté qu'à la date où les requérants avaient atteint la limite d'âge de soixante-cinq ans qui leur était applicable, leur statut était fixé par le décret du 22 mars 1908 relatif à l'organisation du service d'architecture des bâtiments civils et des palais nationaux et qu'ils étaient, dès lors, en vertu de l'article 10 de ce décret, rémunérés par le versement d'honoraires, a jugé qu'en conséquence, faute de traitement indiciaire soumis à retenue, ils ne pouvaient avoir droit à une pension régie par le code des pensions civiles et militaires de retraite. En statuant ainsi, le tribunal, qui s'est borné à faire application des dispositions des articles L. 13 et L. 15 du code des pensions civiles et militaires de retraite, n'a pas méconnu le droit à pension reconnu aux fonctionnaires notamment par l'article L. 2 de ce code.<br/>
<br/>
              5. En deuxième lieu, les requérants ne pouvaient utilement se prévaloir devant les juges du fond de l'indice qui aurait été le leur dans le corps des techniciens des services culturels et des Bâtiments de France régi par le décret du 16 février 2012 portant statut particulier de ce corps, dès lors que, comme cela a été dit ci-dessus, ils avaient atteint la limite d'âge de 65 ans qui leur était applicable pour l'exercice de leurs fonctions publiques, en vertu du décret du 22 mars 1908, avant l'entrée en vigueur des dispositions de ce décret du 16 février 2012 au titre desquelles les vérificateurs des monuments historiques ont été intégrés dans le corps des techniciens de service culturel et des Bâtiments de France. <br/>
<br/>
              6. En troisième lieu, le tribunal n'a pas, compte tenu de ce qui est dit au point 4 et en tout état de cause, commis d'erreur de droit en ne tenant pas compte de la situation de fonctionnaires de fait qui aurait, selon leurs écritures, été celle des requérants au moment où ils ont atteint la limite d'âge qui leur était applicable. <br/>
<br/>
              7. Enfin, le tribunal a également jugé que le caractère accessoire de l'exercice des fonctions publiques des vérificateurs des monuments historiques faisait obstacle à l'application des dispositions des articles L. 13 et L. 15 du code des pensions civiles et militaires de retraite. Si les requérants font valoir qu'il aurait ainsi dénaturé les pièces des dossiers qui lui était soumis et commis une erreur de droit dans la mesure où la part publique de leurs activités serait, en réalité, majoritaire, le motif ainsi critiqué des jugements attaqués est, compte tenu de ce qui est dit au point 4, surabondant. Le moyen ne peut, en conséquence, qu'être écarté. <br/>
<br/>
              8. Il résulte de tout ce qui précède que les pourvois de MM.F..., D...et C...doivent être rejetés. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les pourvois de M. F..., de M. D...et de M. C...sont rejetés.<br/>
Article 2 : La présente décision sera notifiée à M. E...F..., à M. A...D...et à M. B... C...et à la ministre de la culture.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
