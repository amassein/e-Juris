<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039230812</ID>
<ANCIEN_ID>JG_L_2019_10_000000425936</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/23/08/CETATEXT000039230812.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 16/10/2019, 425936, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425936</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Laurent Roulaud</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Anne Iljic</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:425936.20191016</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés le 3 décembre 2018 et le 25 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, l'Association nationale d'assistance aux frontières pour les étrangers (ANAFE) et le Groupe d'information et de soutien des immigré.e.s (GISTI) demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision de rétablir des contrôles aux frontières intérieures terrestres avec la Belgique, le Luxembourg, l'Allemagne, la Confédération suisse, l'Italie et l'Espagne ainsi qu'aux frontières aériennes et maritimes, du 1er novembre 2018 au 30 avril 2019, révélée par la note des autorités françaises au secrétaire général du Conseil de l'Union européenne du 2 octobre 2018 ;<br/>
<br/>
              2°) de surseoir à statuer pour poser une question à la Cour de justice de l'Union européenne portant sur la conformité de la décision attaquée au règlement (UE) 2016/399 du Parlement européen et du Conseil du 9 mars 2016 concernant un code de l'Union relatif au régime de franchissement des frontières par les personnes et aux articles 18 et 45 de la charte des droits fondamentaux de l'Union européenne ;<br/>
<br/>
              3°) d'enjoindre aux ministres intéressés de prendre toutes les mesures nécessaires pour assurer l'absence de contrôles frontaliers aux frontières intérieures de l'espace Schengen dès le prononcé de la décision ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le traité sur l'Union européenne ;<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la charte des droits fondamentaux de l'Union européenne ;<br/>
              - le règlement (UE) 2016/399 du Parlement européen et du Conseil du 9 mars 2016 concernant un code de l'Union relatif au régime de franchissement des frontières par les personnes ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de procédure pénale ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Roulaud, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Anne Iljic, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier que, par une note du 2 octobre 2018, les autorités françaises ont notifié au secrétaire général du Conseil de l'Union européenne leur intention, à compter du 1er novembre 2018 et jusqu'au 30 avril 2019, de réintroduire temporairement un contrôle aux frontières intérieures de l'espace Schengen, sur le fondement des articles 25 et 27 du règlement (UE) 2016/399 du Parlement européen et du Conseil du 9 mars 2016 concernant un code de l'Union relatif au régime de franchissement des frontières par les personnes, dit " code frontières Schengen ". Les conclusions de la requête doivent être regardées comme dirigées contre la décision de la France de réintroduire le contrôle aux frontières intérieures révélée par cette notification. Contrairement à ce que soutient le ministre de l'intérieur, la circonstance que la décision attaquée ait cessé de produire des effets depuis le 1er mai 2019 ne prive pas d'objet le recours pour excès de pouvoir qui a été formé contre cette décision.<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              2. Il appartient au Premier ministre, détenteur du pouvoir réglementaire en vertu des articles 21 et 37 de la Constitution, de prendre les mesures de police générale applicables à l'ensemble du territoire et justifiées par les nécessités de l'ordre public. La circonstance que la décision attaquée prise par le Premier ministre au titre de son pouvoir de police générale n'a pas fait l'objet d'une publication au Journal officiel de la République française est, contrairement à ce que soutiennent les associations requérantes, sans incidence sur la légalité de la décision attaquée. <br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              3. Les articles 25 à 27 du règlement (UE) 2016/399 du Parlement européen et du Conseil du 9 mars 2016 déterminent un cadre général de procédure pour la réintroduction temporaire du contrôle aux frontières intérieures des Etats appartenant à l'espace Schengen. Aux termes de l'article 25 de ce règlement : " 1. En cas de menace grave pour l'ordre public ou la sécurité intérieure d'un État membre dans l'espace sans contrôle aux frontières intérieures, cet État membre peut exceptionnellement réintroduire le contrôle aux frontières sur tous les tronçons ou sur certains tronçons spécifiques de ses frontières intérieures pendant une période limitée d'une durée maximale de trente jours ou pour la durée prévisible de la menace grave si elle est supérieure à trente jours. La portée et la durée de la réintroduction temporaire du contrôle aux frontières intérieures ne doivent pas excéder ce qui est strictement nécessaire pour répondre à la menace grave. 2. Le contrôle aux frontières intérieures n'est réintroduit qu'en dernier recours et conformément aux articles 27, 28 et 29. Les critères visés, respectivement, aux articles 26 et 30 sont pris en considération chaque fois qu'une décision de réintroduire le contrôle aux frontières intérieures est envisagée en vertu de l'article 27, 28 ou 29, respectivement. 3. Lorsque la menace grave pour l'ordre public ou la sécurité intérieure dans l'État membre concerné persiste au-delà de la durée prévue au paragraphe 1 du présent article, ledit État membre peut prolonger le contrôle à ses frontières intérieures, en tenant compte des critères visés à l'article 26 et conformément à l'article 27, pour les mêmes raisons que celles visées au paragraphe 1 du présent article et, en tenant compte d'éventuels éléments nouveaux, pour des périodes renouvelables ne dépassant pas trente jours. 4. La durée totale de la réintroduction du contrôle aux frontières intérieures, y compris toute prolongation prévue au titre du paragraphe 3 du présent article, ne peut excéder six mois (...) ". Aux termes de l'article 26 de ce même règlement : " Lorsqu'un État membre décide, en dernier recours, la réintroduction temporaire du contrôle à une ou plusieurs de ses frontières intérieures ou sur des tronçons de celles-ci ou décide de prolonger ladite réintroduction, conformément à l'article 25 ou à l'article 28, paragraphe 1, il évalue la mesure dans laquelle cette réintroduction est susceptible de remédier correctement à la menace pour l'ordre public ou la sécurité intérieure et évalue la proportionnalité de la mesure par rapport à cette menace. Lors de cette évaluation, l'État membre tient compte, en particulier, de ce qui suit : a) l'incidence probable de toute menace pour son ordre public ou sa sécurité intérieure, y compris du fait d'incidents ou de menaces terroristes, dont celles que représente la criminalité organisée ". Aux termes de son article 27 : " 1. Lorsqu'un État membre prévoit de réintroduire le contrôle aux frontières intérieures au titre de l'article 25, il notifie son intention aux autres États membres et à la Commission au plus tard quatre semaines avant la réintroduction prévue, ou dans un délai plus court lorsque les circonstances étant à l'origine de la nécessité de réintroduire le contrôle aux frontières intérieures sont connues moins de quatre semaines avant la date de réintroduction prévue (...). 4. À la suite de la notification par un État membre au titre du paragraphe 1, et en vue de la consultation prévue au paragraphe 5, la Commission ou tout autre État membre peut, sans préjudice de l'article 72 du traité sur le fonctionnement de l'Union européenne, émettre un avis. Si, sur la base des informations figurant dans la notification ou de toute information complémentaire qu'elle a reçue, la Commission a des doutes quant à la nécessité ou la proportionnalité de la réintroduction prévue du contrôle aux frontières intérieures, ou si elle estime qu'une consultation sur certains aspects de la notification serait appropriée, elle émet un avis en ce sens. 5. Les informations visées au paragraphe 1, ainsi que tout avis éventuel émis par la Commission ou un État membre au titre du paragraphe 4, font l'objet d'une consultation, y compris, le cas échéant, de réunions conjointes entre l'État membre prévoyant de réintroduire le contrôle aux frontières intérieures, les autres États membres, en particulier ceux directement concernés par de telles mesures, et la Commission, afin d'organiser, le cas échéant, une coopération mutuelle entre les États membres et d'examiner la proportionnalité des mesures par rapport aux événements qui sont à l'origine de la réintroduction du contrôle aux frontières ainsi qu'à la menace pour l'ordre public ou la sécurité intérieure. 6. La consultation visée au paragraphe 5 a lieu au moins dix jours avant la date prévue pour la réintroduction du contrôle aux frontières ".<br/>
<br/>
              En ce qui concerne la proportionnalité de la mesure :<br/>
<br/>
              4. Il ressort des pièces du dossier que la décision attaquée, qui n'est pas entachée du détournement de pouvoir allégué, a été prise en raison de l'actualité de la menace terroriste, notamment illustrée par la préparation et la réalisation d'attentats survenus en France et aux Pays-Bas, en mars, mai et août 2018 ainsi que par l'évolution de la situation dans la zone de conflit armé en Syrie susceptible d'entraîner un retour sur le territoire français de personnes potentiellement dangereuses. Au vu de la nature de ce risque et de la nécessité, pour le prévenir efficacement, de contrôler l'identité et la provenance des personnes désireuses d'entrer en France, la décision attaquée est proportionnée à la gravité de la menace. Alors même que les dispositions précitées des articles 25 et 26 du règlement du 9 mars 2016 prévoient que les Etats ne peuvent décider de leur mise en oeuvre qu'en dernier recours, il ne ressort pas des pièces du dossier que d'autres mesures moins restrictives à la libre circulation des personnes, tels que les contrôles d'identité effectués, notamment en zone frontalière, sur le fondement de l'article 78-2 du code de procédure pénale ou du I de l'article L. 611-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, seraient de nature à prévenir le risque terroriste dans des conditions équivalentes.<br/>
<br/>
              En ce qui concerne la durée de la mesure au regard de l'article 25 du " code frontières Schengen " :<br/>
<br/>
              5. D'une part, l'article 25 du règlement du 9 mars 2016 dispose qu'en cas de menace grave pour l'ordre public ou la sécurité intérieure, un Etat membre peut, pour une durée totale n'excédant pas six mois, réintroduire le contrôle aux frontières intérieures pour une durée de trente jours maximum renouvelable ou correspondant à la durée prévisible de la menace si elle est supérieure à trente jours. Il en résulte clairement que, dans l'hypothèse où la menace justifiant le rétablissement des contrôles aux frontières intérieures est d'une durée prévisible supérieure à trente jours sans que son terme puisse raisonnablement être fixé, l'Etat peut décider de rétablir d'emblée le contrôle temporaire pour la durée maximale de six mois.<br/>
<br/>
              6. D'autre part, si l'article 25 limite la durée maximale de la réintroduction d'un contrôle aux frontières intérieures à six mois, il ne fait pas obstacle, en cas de nouvelle menace ou de menace renouvelée pour l'ordre public ou la sécurité intérieure, à la mise en place à nouveau d'un contrôle aux frontières pour une autre période d'une durée maximale de 6 mois, ainsi d'ailleurs que le relève expressément la Commission dans sa recommandation du 3 octobre 2017 sur la mise en oeuvre des dispositions du " code frontières Schengen " relatives à la réintroduction temporaire du contrôle aux frontières intérieures de l'espace Schengen.<br/>
<br/>
              7. Il ressort des pièces du dossier que le Premier ministre, pour prendre la décision attaquée réintroduisant une nouvelle fois le contrôle aux frontières intérieures pour une période allant du 1er novembre 2018 au 30 avril 2019, s'est fondé sur l'actualité et le niveau élevé de la menace terroriste prévalant en France à la date de sa décision. Cette menace renouvelée constitue un motif de nature à justifier une nouvelle mise en oeuvre de la faculté prévue à l'article 25. Dès lors que la durée prévisible de cette menace excède trente jours sans que son terme puisse être fixé, le Premier ministre a pu légalement décider, pour y parer le plus efficacement possible, de réintroduire le contrôle aux frontières pour une durée fixée d'emblée à six mois. Il suit de là que le moyen tiré de la méconnaissance de l'article 25 du règlement du 9 mars 2016 doit être écarté.<br/>
<br/>
              En ce qui concerne le contenu de la notification préalable de la mesure au regard de l'article 27 du " code frontières Schengen " : <br/>
<br/>
              8. Aux termes du paragraphe 1 de l'article 27 du règlement du 9 mars 2006 : " Lorsqu'un État membre prévoit de réintroduire le contrôle aux frontières intérieures au titre de l'article 25, il notifie son intention aux autres États membres et à la Commission au plus tard quatre semaines avant la réintroduction prévue, ou dans un délai plus court lorsque les circonstances étant à l'origine de la nécessité de réintroduire le contrôle aux frontières intérieures sont connues moins de quatre semaines avant la date de réintroduction prévue ". Les conditions dans lesquelles un Etat membre procède, aux seules fins d'information préalable de la Commission et des autres Etats-membres, à la notification de sa décision de réintroduire temporairement le contrôle aux frontières intérieures sont sans incidence sur la légalité d'une telle mesure. Il suit de là que les associations requérantes ne sauraient utilement invoquer la méconnaissance de l'article 27 précité. <br/>
<br/>
              En ce qui concerne la transmission du rapport sur la réintroduction du contrôle aux frontières intérieures en application de l'article 33 du " code frontières Schengen " : <br/>
<br/>
              9. Aux termes du paragraphe 1 de l'article 33 du règlement du 9 mars 2016 : " Dans les quatre semaines de la levée du contrôle aux frontières intérieures, l'État membre qui a réalisé un contrôle aux frontières intérieures présente un rapport au Parlement européen, au Conseil et à la Commission sur la réintroduction du contrôle aux frontières intérieures, qui donne notamment un aperçu de l'évaluation initiale et du respect des critères visés aux articles 26, 28 et 30, de la mise en oeuvre des vérifications, de la coopération concrète avec les États membres voisins, de l'incidence sur la libre circulation des personnes qui en résulte et de l'efficacité de la réintroduction du contrôle aux frontières intérieures, y compris une évaluation ex post de la proportionnalité de cette réintroduction ". La circonstance, à la supposer établie, qu'un tel rapport n'ait pas été présenté au Parlement européen à la suite des précédentes périodes de rétablissement des contrôles aux frontières intérieures, est sans incidence sur la légalité de la décision attaquée. <br/>
<br/>
              En ce qui concerne le principe de libre-circulation :<br/>
<br/>
              10. En vertu de l'article 72 du traité sur le fonctionnement de l'Union européenne, le principe de libre-circulation, et son corollaire, l'absence de contrôles des personnes aux frontières intérieures, ne portent pas atteinte à l'exercice des responsabilités qui incombent aux États membres pour le maintien de l'ordre public et la sauvegarde de la sécurité intérieure. Il résulte des motifs énoncés aux points précédents que la décision attaquée ne porte à la libre circulation des personnes qu'une atteinte proportionnée au but qu'elle poursuit.<br/>
<br/>
              11. Il résulte de tout ce qui précède que, sans qu'il y ait lieu de saisir la Cour de justice de l'Union européenne d'une question préjudicielle, les conclusions de l'ANAFE et du GISTI doivent être rejetées, y compris leurs conclusions à fin d'injonction et celles présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                            --------------<br/>
<br/>
Article 1er : La requête de l'ANAFE et du GISTI est rejetée.<br/>
Article 2 : La présente décision sera notifiée à l'Association nationale d'assistance aux frontières pour les étrangers, au Groupe d'information et de soutien des immigré.e.s et au ministre de l'intérieur.<br/>
Copie en sera adressée, pour information, au ministre de l'Europe et des affaires étrangères et au Premier ministre.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
