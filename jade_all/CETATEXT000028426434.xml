<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028426434</ID>
<ANCIEN_ID>JG_L_2013_12_000000371602</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/42/64/CETATEXT000028426434.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème sous-section jugeant seule, 30/12/2013, 371602, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>371602</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>Mme Natacha Chicot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:371602.20131230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 26 août et 18 octobre 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B... A..., demeurant... ; M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 13PA00808 du 12 avril 2013 par laquelle le président de la 2ème chambre de la cour administrative d'appel de Paris a rejeté sa requête tendant, en premier lieu, à l'annulation du jugement n° 1202414/6 du 5 juillet 2012 du tribunal administratif de Melun rejetant sa demande tendant à l'annulation de l'arrêté du préfet du Val-de-Marne du 22 septembre 2011 lui refusant le renouvellement d'un titre de séjour, l'obligeant à quitter le territoire français et fixant son pays de destination, en deuxième lieu, à l'annulation dudit arrêté et, en troisième lieu, à ce qu'il soit enjoint au préfet de lui délivrer un titre de séjour dans un délai de quinze jours à compter de la notification de l'arrêt, sous astreinte de 100 euros par jour de retard ou, à défaut, de réexaminer sa situation administrative et de lui délivrer, dans l'attente, une autorisation provisoire de séjour, sous les mêmes conditions de délai et d'astreinte ; <br/>
<br/>
              2°) de renvoyer l'affaire devant la cour administrative d'appel de Paris ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 2 000 euros au titre du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le décret n° 91-1266 du 19 décembre 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Natacha Chicot, Auditeur,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat de M. A...;<br/>
<br/>
<br/>
<br/>1. Considérant, d'une part, qu'aux termes de l'article R. 222-1 du code de justice administrative : " Les présidents de tribunal administratif et de cour administrative d'appel, le vice-président du tribunal administratif de Paris et les présidents de formation de jugement des tribunaux et des cours peuvent, par ordonnance : (...) / 4° Rejeter les requêtes manifestement irrecevables, lorsque la juridiction n'est pas tenue d'inviter leur auteur à les régulariser ou qu'elles n'ont pas été régularisées à l'expiration du délai imparti par une demande en ce sens ; " ; que l'article R. 612-1 du même code précise que : " Lorsque des conclusions sont entachées d'une irrecevabilité susceptible d'être couverte après l'expiration du délai de recours, la juridiction ne peut les rejeter en relevant d'office cette irrecevabilité qu'après avoir invité leur auteur à les régulariser. / Toutefois, la juridiction d'appel ou de cassation peut rejeter de telles conclusions sans demande de régularisation préalable pour les cas d'irrecevabilité tirés de la méconnaissance d'une obligation mentionnée dans la notification de la décision attaquée conformément à l'article R. 751-5. / La demande de régularisation mentionne que, à défaut de régularisation, les conclusions pourront être rejetées comme irrecevables dès l'expiration du délai imparti qui, sauf urgence, ne peut être inférieur à quinze jours. La demande de régularisation tient lieu de l'information prévue à l'article R. 611-7 " ; que l'article R. 811-7 du même code dispose que : " Les appels ainsi que les mémoires déposés devant la cour administrative d'appel doivent être présentés, à peine d'irrecevabilité, par l'un des mandataires mentionnés à l'article R. 431-2. / Lorsque la notification de la décision soumise à la cour administrative d'appel ne comporte pas la mention prévue au troisième alinéa de l'article R. 751-5, le requérant est invité par la cour à régulariser sa requête dans les conditions fixées aux articles R. 612-1 et R. 612-2 (...) " ;<br/>
<br/>
              2. Considérant, d'autre part, que la loi du 10 juillet 1991 relative à l'aide juridique prévoit, en son article 2, que les personnes physiques dont les ressources sont insuffisantes pour faire valoir leurs droits en justice peuvent bénéficier d'une aide juridictionnelle et, en son article 25, que le bénéficiaire de l'aide juridictionnelle a droit à l'assistance d'un avocat choisi par lui ou, à défaut, désigné par le bâtonnier de l'ordre des avocats ; qu'il résulte des articles 76 et 77 du décret du 19 décembre 1991 que si la personne qui demande l'aide juridictionnelle ne produit pas de document attestant l'acceptation d'un avocat choisi par elle, l'avocat peut être désigné sur-le-champ par le représentant de la profession qui siège au bureau d'aide juridictionnelle, à condition qu'il ait reçu délégation du bâtonnier à cet effet ;<br/>
<br/>
              3. Considérant qu'il résulte de l'ensemble de ces dispositions que les cours administratives d'appel peuvent rejeter les requêtes entachées de défaut de ministère d'avocat, sans demande de régularisation préalable, si le requérant a été averti dans la notification du jugement attaqué que l'obligation du ministère d'avocat s'imposait à lui en l'espèce ; que toutefois, si ce requérant a obtenu la désignation d'un avocat au titre de l'aide juridictionnelle et si cet avocat n'a pas produit de mémoire, le juge d'appel ne peut, afin d'assurer au requérant le bénéfice effectif du droit qu'il tire de la loi du 10 juillet 1991, rejeter la requête sans avoir préalablement mis l'avocat désigné en demeure d'accomplir, dans un délai qu'il détermine, les diligences qui lui incombent et porté cette carence à la connaissance du requérant, afin de le mettre en mesure, le cas échéant, de choisir un autre représentant ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces de la procédure d'appel que M. A..., qui avait été régulièrement informé par la lettre de notification du jugement attaqué de l'obligation de recourir au ministère d'avocat pour faire appel de ce jugement, a présenté une demande tendant au bénéfice de l'aide juridictionnelle totale auprès du bureau d'aide juridictionnelle près le tribunal de grande instance de Paris ; que le bureau d'aide juridictionnelle près le tribunal de grande instance de Paris a fait droit à sa demande et désigné un avocat pour le représenter ; que cet avocat a présenté une requête devant la cour administrative d'appel de Paris ne comportant ni moyen ni conclusion après sa désignation au titre de l'aide juridictionnelle ; que par un courrier du 12 mars 2013 le greffe de la cour administrative d'appel de Paris a accusé réception de la requête présentée par l'avocat de M. A... et lui a rappelé l'obligation de régulariser sa requête par la production d'un mémoire comportant des moyens et des conclusions avant l'expiration du délai d'appel ; que, toutefois, en s'abstenant de porter à la connaissance de M. A...la carence de son avocat avant de lui opposer sur le fondement de l'article R. 222-1 précité une irrecevabilité tirée de l'absence de motivation de la requête d'appel, le président de la 2ème chambre de la cour administrative d'appel de Paris a commis une erreur de droit ; que, dès lors, son ordonnance doit être annulée ;<br/>
<br/>
              5. Considérant que M. A...a obtenu devant le Conseil d'Etat le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 2 000 euros à verser à la SCP Coutard et Munier-Apaire, sous réserve qu'elle renonce à percevoir la somme correspondant à la part contributive de l'Etat ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du président de la 2ème chambre de la cour administrative d'appel de Paris du 22 avril 2013 est annulée.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : L'Etat versera à la SCP Coutard et Munier-Apaire, avocat de M.A..., une somme de 2 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à M. B...A...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
