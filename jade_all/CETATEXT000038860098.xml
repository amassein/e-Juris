<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038860098</ID>
<ANCIEN_ID>JG_L_2019_07_000000431646</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/86/00/CETATEXT000038860098.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 23/07/2019, 431646, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431646</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:431646.20190723</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 13 juin 2019 au secrétariat du contentieux du Conseil d'Etat, le syndicat national des personnels des affaires sanitaires et sociales - Force ouvrière (SNPASS-FO) demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, d'ordonner la suspension de l'exécution de l'arrêté du 8 mars 2019 par lequel la ministre des solidarités et de la santé, le ministre de l'éducation nationale et de la jeunesse et la ministre des sports ont fixé la liste des organisations syndicales habilitées à désigner des représentants au sein du comité d'hygiène, de sécurité et des conditions de travail ministériel unique placé auprès des ministres chargés des affaires sociales, de la santé, de la jeunesse et des sports. <br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence est remplie en ce que, d'une part, l'arrêté du 8 mars 2019 ne respecte pas les résultats d'un scrutin professionnel, le privant de représentation au sein du comité d'hygiène, de sécurité et des conditions de travail (CHSCT) ministériel unique et, d'autre part, il exclut les agents des agences régionales de santé du périmètre de compétence du CHSCT ministériel unique ;<br/>
              - il existe un doute sérieux quant à la légalité de l'arrêté contesté ;<br/>
              - l'arrêté du 8 mars 2019 est entaché d'une erreur de droit en ce qu'il exclut le personnel des agences régionales de santé du périmètre de compétence du CHSCT ministériel unique ;<br/>
              - il est entaché d'une seconde erreur de droit en ce que la représentation des organisations syndicales qu'il fixe est contraire aux dispositions réglementaires relatives à l'hygiène, la sécurité du travail ainsi que la prévention médicale dans la fonction publique dès lors que les voix exprimées par les agents des agences régionales de santé ont été écartées du calcul des sièges dévolus à chaque organisation syndicale au sein du CHSCT ministériel unique.<br/>
<br/>
              Par un mémoire en défense, enregistré le 1er juillet 2019, la ministre des solidarités et de la santé, le ministre de l'éducation nationale et de la jeunesse et la ministre des sports concluent au rejet de la requête. Ils soutiennent que la condition d'urgence n'est pas remplie et que les moyens soulevés par le syndicat requérant ne sont pas fondés.<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, le syndicat national des personnels des affaires sanitaires et sociales - Force ouvrière (SNPASS-FO) et, d'autre part, la ministre des solidarités et de la santé, le ministre de l'éducation nationale et de la jeunesse et la ministre des sports ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 8 juillet 2019 à 11 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - les représentants du syndicat national des personnels des affaires sanitaires et sociales - Force ouvrière (SNPASS-FO) ;<br/>
<br/>
              - les représentants de la ministre des solidarités et de la santé, du ministre de l'éducation nationale et de la jeunesse et de la ministre des sports ;<br/>
              et à l'issue de laquelle le juge des référés a prolongé l'instruction jusqu'au 9 juillet à 12 heures ; <br/>
<br/>
<br/>
              Vu les nouveaux mémoires, enregistrés les 8 et 16 juillet 2019, présentés par la ministre des solidarités et de la santé, le ministre de l'éducation nationale et de la jeunesse et la ministre des sports dans lesquels ils maintiennent leurs conclusions à fin de rejet de la requête par les mêmes moyens ;<br/>
<br/>
              Après avoir rouvert l'instruction et convoqué à une seconde audience publique, d'une part, le syndicat national des personnels des affaires sanitaires et sociales - Force ouvrière (SNPASS-FO) et, d'autre part, la ministre des solidarités et de la santé, le ministre de l'éducation nationale et de la jeunesse, la ministre des sports ainsi que les organisations syndicales UNSA santé cohésion sociale, Fédération CFDT - Interco et CGT-SMAST-CGT - SNASS-CGT ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 17 juillet 2019 à 11 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - les représentants du syndicat national des personnels des affaires sanitaires et sociales - Force ouvrière (SNPASS-FO) ;<br/>
<br/>
              - les représentants de la ministre des solidarités et de la santé, du ministre de l'éducation nationale et de la jeunesse et de la ministre des sports ;<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ; <br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le décret n° 82-453 du 28 mai 1982 ;<br/>
              - le décret n° 2018-406 du 29 mai 2018 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              2. Par un arrêté du 8 mars 2019, la ministre des solidarités et de la santé, le ministre de l'éducation nationale et de la jeunesse et la ministre des sports ont fixé la liste des organisations syndicales habilitées à désigner des représentants au sein du CHSCT ministériel unique placé auprès des ministres chargés des affaires sociales, de la santé, de la jeunesse et des sports. Le syndicat national des personnels des affaires sanitaires et sociales - Force ouvrière (SNPASS-FO) demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de cet arrêté dont il a, par une requête distincte, sollicité l'annulation pour excès de pouvoir.<br/>
<br/>
              3. Le deuxième alinéa de l'article 42 du décret du 28 mai 1982 relatif à l'hygiène et à la sécurité du travail ainsi qu'à la prévention médicale dans la fonction publique prévoit que, pour chaque département ministériel, direction, service ou établissement public appelé à être doté d'un CHSCT, une liste des organisations syndicales habilitées à désigner des représentants ainsi que le nombre de sièges auxquels elles ont droit est arrêtée, proportionnellement au nombre de voix obtenues lors de l'élection ou de la désignation des représentants du personnel dans les comités techniques. Les quatre alinéas suivants prévoient des modalités dérogatoires de détermination de ces organisations et du nombre en l'absence de comité technique au niveau où est créé le CHSCT. En l'absence de circonstances particulières, d'éventuelles erreurs dans la répartition des sièges entre les organisations syndicales opérée en vue de constituer un CHSCT ne sont pas constitutives d'une situation d'urgence. <br/>
<br/>
              4. Pour soutenir que la suspension de l'arrêté attaqué du 8 mars 2019, qui habilite trois organisations syndicales à désigner des représentants du personnel au sein du CHSCT ministériel unique placé auprès des ministres chargés des affaires sociales, de la santé, de la jeunesse et des sports et précise le nombre de sièges affectés à chacune d'elles, le syndicat national des personnels des affaires sanitaires et sociales - Force ouvrière (SNPASS-FO), qui n'est pas au nombre des syndicats ainsi habilités, fait valoir que non seulement cet arrêté l'empêche illégalement de désigner un représentant au sein de cette instance mais qu'en excluant pour la première fois de prendre en compte les voix des agents de droit public travaillant dans les agences régionales de santé (ARS), ils privent ces agents de toute représentation au sein de ce CHSCT ministériel unique et plus largement au sein d'un CHSCT compétent pour examiner les questions de santé et de conditions de travail communes à toutes les ARS. Il soutient qu'il s'agit d'une circonstance particulière de nature à en justifier la suspension. Toutefois, la suspension demandée, en paralysant la désignation des membres de ce CHSCT, n'assurerait pas davantage cette représentation mais aurait en outre pour effet de priver tous les agents des ministères concernés d'un CHSCT ministériel unique dans l'attente du jugement de la requête au fond, susceptible de n'intervenir qu'au début de l'année 2019. L'intérêt général qui s'attache à la possibilité de réunir ce CHSCT ministériel unique, combiné avec la circonstance que le comité national de concertation prévu à l'article L. 1432-11 du code de la santé publique, sans constituer l'équivalent d'un CHSCT commun aux ARS, est néanmoins compétent pour être informé des questions communes relatives aux conditions de travail, d'hygiène, de sécurité et d'emploi et en débattre, fait dès lors, et en tout état de cause, obstacle à ce que cette suspension soit prononcée.<br/>
<br/>
              5. Il résulte de ce qui précède, et sans qu'il soit besoin d'examiner s'il existe un moyen de nature à faire naître un doute sérieux sur la légalité de l'arrêté attaqué, que la requête du syndicat national des personnels des affaires sanitaires et sociales - Force ouvrière (SNPASS-FO) doit être rejetée.<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête du syndicat national des personnels des affaires sanitaires et sociales - Force ouvrière (SNPASS-FO) est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée au syndicat national des personnels des affaires sanitaires et sociales - Force ouvrière (SNPASS-FO), à la ministre des solidarités et de la santé, au ministre de l'éducation nationale, à la ministre des sports ainsi qu'aux organisations syndicales UNSA santé cohésion sociale, Fédération CFDT - Interco et CGT-SMAST-CGT - SNASS-CGT. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
