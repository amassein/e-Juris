<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039195601</ID>
<ANCIEN_ID>JG_L_2019_09_000000425458</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/19/56/CETATEXT000039195601.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 26/09/2019, 425458, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-09-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425458</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>M. Géraud Sajust de Bergues</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:425458.20190926</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme A... B... a demandé au tribunal administratif de Dijon d'annuler la décision du 24 novembre 2015 par laquelle le maire de Montceau-les-Mines (Saône-et-Loire) a refusé de modifier l'attestation qu'il a établie à l'attention de Pôle Emploi, d'enjoindre à cette autorité de modifier cette attestation dans un délai de quinze jours ou, subsidiairement, de prendre une nouvelle décision dans le même délai et de condamner la commune de Montceau-les-Mines à lui payer une somme de 2 173 euros au titre des frais du litige.<br/>
<br/>
              Par un jugement n° 1600054 du 3 mai 2018, le tribunal administratif de Dijon a rejeté sa demande. <br/>
<br/>
              Par une ordonnance n° 18LY02582 du 17 septembre 2018, le président de la 3ème chambre de la cour administrative d'appel de Lyon a rejeté l'appel formé par Mme B... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés le 19 novembre 2018 et le 19 février 2019 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Montceau-les-Mines la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Géraud Sajust de Bergues, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de Mme B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du dernier alinéa de l'article R. 222-1 du code de justice administrative : " Les présidents des cours administratives d'appel, les premiers vice-présidents des cours et les présidents des formations de jugement des cours, ainsi que les autres magistrats ayant le grade de président désignés à cet effet par le président de la cour peuvent, en outre, par ordonnance, rejeter les conclusions à fin de sursis à exécution d'une décision juridictionnelle frappée d'appel, les requêtes dirigées contre des ordonnances prises en application des 1° à 5° du présent article ainsi que, après l'expiration du délai de recours ou, lorsqu'un mémoire complémentaire a été annoncé, après la production de ce mémoire les requêtes d'appel manifestement dépourvues de fondement (...) ". Aux termes de l'article R. 612-5 du même code : " Devant les tribunaux administratifs et les cours administratives d'appel, si le demandeur, malgré la mise en demeure qui lui a été adressée, n'a pas produit le mémoire complémentaire dont il avait expressément annoncé l'envoi ou, dans les cas mentionnés au second alinéa de l'article R. 611-6, n'a pas rétabli le dossier, il est réputé s'être désisté ".<br/>
<br/>
              2. Il résulte de ces dispositions que les présidents des cours administratives d'appel, les premiers vice-présidents des cours et les présidents des formations de jugement des cours, ainsi que les autres magistrats ayant le grade de président désignés à cet effet par le président de la cour ne peuvent rejeter par ordonnance une requête manifestement dépourvue de fondement, lorsque la production d'un mémoire complémentaire a été annoncée, qu'après la production de ce mémoire.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis au juge du fond que dans sa requête du 11 juillet 2018 tendant à l'annulation du jugement du tribunal administratif de Dijon du 3 mai 2018 qui a rejeté sa demande d'annulation du refus implicite du maire de Montceau-les-Mines de modifier l'attestation qu'il a établie à l'attention de Pôle emploi, Mme B... annonçait la production ultérieure d'un mémoire complémentaire. Dès lors, en rejetant par une ordonnance du 17 septembre 2018 la requête de Mme B... au motif qu'elle était manifestement dépourvue de fondement, sans attendre la production du mémoire complémentaire annoncé et sans même avoir mis en demeure l'intéressée de produire ce mémoire, le président de la 3ème chambre de la cour administrative d'appel de Lyon a méconnu le dernier alinéa de l'article R. 222-1 du code de justice administrative.  <br/>
<br/>
              4. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur l'autre moyen du pourvoi, que Mme B... est fondée à demander l'annulation de l'ordonnance qu'elle attaque.  <br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Montceau-les-Mines la somme de 3 000 euros à verser à Mme B... au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance n° 18LY02582 du 17 septembre 2018 du président de la 3ème chambre de la cour administrative d'appel de Lyon est annulée.   <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon. <br/>
Article 3 : La commune de Montceau-les-Mines versera une somme de 3 000 euros à Mme B... au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme A... B... et à la commune de Montceau-les-Mines.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
