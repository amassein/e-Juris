<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043466294</ID>
<ANCIEN_ID>JG_L_2021_04_000000448269</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/46/62/CETATEXT000043466294.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 30/04/2021, 448269, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-04-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448269</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD</AVOCATS>
<RAPPORTEUR>M. Edouard Solier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:448269.20210430</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu les procédures suivantes :<br/>
<br/>
              Mme B... A... a porté plainte contre M. D... C... devant la chambre disciplinaire de première instance de Basse-Normandie de l'ordre des médecins, devenue chambre disciplinaire de première instance de Normandie de l'ordre des médecins. Par une décision du 20 juillet 2018, la chambre disciplinaire de première instance a infligé à M. C... la sanction de l'interdiction d'exercer la médecine pendant une durée de six mois, dont cinq mois assortis du sursis.<br/>
<br/>
              Par une décision du 16 novembre 2020, la chambre disciplinaire nationale de l'ordre des médecins a, sur les appels de Mme A... et de M. C..., infligé à M. C... la sanction de l'interdiction d'exercer la médecine pendant une durée de six mois. <br/>
<br/>
              1° Sous le n° 448269, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 30 décembre 2020 et 17 février 2021 au secrétariat du contentieux du Conseil d'État, M. C... demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler cette décision ; <br/>
<br/>
              2°) de mettre à la charge de Mme A... la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              2° Sous le n° 448438, par une requête et un mémoire, enregistrés les 6 janvier et 22 février 2021 au secrétariat du contentieux du Conseil d'Etat, M. C... demande au Conseil d'Etat, en application de l'article R. 821-5 du code de justice administrative, qu'il soit sursis à l'exécution de la décision du 16 novembre 2020.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la santé publique ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Edouard Solier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Richard, avocat de M. C... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Le pourvoi par lequel M. C... demande l'annulation de la décision du 16 novembre 2020 de la chambre disciplinaire nationale de l'ordre des médecins et la requête par laquelle il demande qu'il soit sursis à l'exécution de cette décision présentent à juger les mêmes questions. Il y a lieu d'y statuer par une seule décision.<br/>
<br/>
              2. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'État fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              3. Pour demander l'annulation de la décision de la chambre disciplinaire nationale de l'ordre des médecins qu'il attaque, M. C... soutient qu'elle est entachée :<br/>
              - d'une contradiction de motifs en ce qu'elle se fonde sur deux dates différentes d'apparition de symptômes déterminants dans l'établissement du diagnostic de la maladie du patient ;<br/>
              - de dénaturation des pièces du dossier en ce qu'elle juge qu'il avait connaissance avant le mois d'août 2016 des douleurs inguinales et pelviennes du patient ;<br/>
              - d'inexacte qualification des faits en ce qu'elle juge qu'il a commis une faute en prescrivant une photovaporisation prostatique ;<br/>
              - d'insuffisance de motivation en ce qu'elle se fonde sur un motif hypothétique en jugeant que la photovaporisation a pu donner un " coup de fouet " au cancer du patient. <br/>
<br/>
              Il soutient, en outre, que la décision qu'il attaque lui inflige une sanction hors de proportion avec la gravité des faits reprochés. <br/>
<br/>
              Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
              4. Le pourvoi formé par M. C... contre la décision de la chambre disciplinaire nationale de l'ordre des médecins n'étant pas admis, les conclusions qu'il présente aux fins de sursis à exécution de cette décision sont devenues sans objet. Il n'y a donc plus lieu d'y statuer.<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de Mme A... qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. C... n'est pas admis.<br/>
Article 2 : Il n'y a pas lieu de statuer sur la requête aux fins de sursis à exécution de M. C....<br/>
Article 3 : Les conclusions de la requête de M. C... présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. D... C... et à Mme B... A....<br/>
Copie en sera adressée au Conseil national de l'ordre des médecins.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
