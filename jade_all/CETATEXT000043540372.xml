<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043540372</ID>
<ANCIEN_ID>JG_L_2021_05_000000446066</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/54/03/CETATEXT000043540372.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 26/05/2021, 446066, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-05-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>446066</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>OCCHIPINTI</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Prévoteau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:446066.20210526</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme D... B... a demandé au tribunal administratif de Lille d'annuler les opérations électorales qui se sont déroulées les 15 mars et 28 juin 2020 en vue de la désignation des conseillers municipaux et communautaires de la commune de La Sentinelle (Nord). Par un jugement n° 2004481 du 27 octobre 2020, le tribunal administratif de Lille a annulé ces opérations.<br/>
<br/>
              Par une requête, un mémoire complémentaire et un mémoire en réplique, enregistrés les 8 novembre et 8 décembre 2020 et le 23 avril 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) de rejeter la protestation électorale présentée par Mme B... ;<br/>
<br/>
              3°) de mettre à la charge de Mme B... la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Prévoteau, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à Me Occhipinti, avocat de M. C... ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Lors des opérations électorales organisées en vue de la désignation des conseillers municipaux de la commune de La Sentinelle (Nord), qui se sont déroulées les 15 mars et 28 juin 2020, la liste " La Sentinelle, la ville des possibles ", conduite par M. C..., a recueilli, au second tour de scrutin, 587 voix, soit 51,45 % des suffrages exprimés, tandis que la liste " La Sentinelle, continuons ensemble ! ", conduite par Mme B... a obtenu 554 voix, soit 48,55 % des suffrages exprimés. M. C... relève appel du jugement du 27 octobre 2020 par lequel le tribunal administratif de Lille a annulé les opérations électorales.<br/>
<br/>
              2. Aux termes de l'article L 48-2 du code électoral : " Il est interdit à tout candidat de porter à la connaissance du public un élément nouveau de polémique électorale à un moment tel que ses adversaires n'aient pas la possibilité d'y répondre utilement avant la fin de la campagne électorale ". Aux termes de l'article L. 49 du même code : " A partir de la veille du scrutin à zéro heure, il est interdit de distribuer ou faire distribuer des bulletins, circulaires et autres documents. / A partir de la veille du scrutin à zéro heure, il est également interdit de diffuser ou de faire diffuser par tout moyen de communication au public par voie électronique tout message ayant le caractère de propagande électorale ".  <br/>
<br/>
              3. Il résulte de l'instruction que, dans la soirée du vendredi 26 juin 2020, la liste " La Sentinelle, la ville des possibles ", conduite par M. C..., a distribué dans les boites aux lettres des habitants de la commune un tract comportant, notamment, les mentions suivantes : " Vous avez constaté que nos tracts distribués n'ont jamais fait acte de la gestion de Bernadette B..., alors que nous avions matière à le faire ", " Un dernier point sur lequel nous souhaitons revenir : Mme B... se vante à qui veut l'entendre qu'elle a réussi ce tour de force d'avoir assaini les comptes de la commune... quel mensonge alors que la dette par habitant a explosé ces dernières années. Hormis les courbes ci-dessous, nous vous invitons toutes et tous à consulter les chiffres sur internet. Pour cela il vous suffit de taper dans le moteur de recherche " endettement ville de La Sentinelle " et vous jugerez par vous-même " et " Evolution de la dette : En 2007, la dette était de 1 469 546 euros soit 387 euros / habitant. En 2019, la dette devrait être à 7 173 108 euros soit 2 290 euros / habitant. Mme B... a donc réussi ce tour de force à multiplier par 6 l'endettement de la commune ". <br/>
<br/>
              4. S'il est constant que la diffusion de ce tract avait pour objet de répondre aux éléments contenus dans un tract distribué la veille par la liste " La Sentinelle, continuons ensemble ! ", conduite par Mme B..., il ne résulte toutefois pas de l'instruction et notamment pas des mentions de ce dernier tract qui évoquait seulement le montant des dépenses d'investissement engagées et des subventions obtenues à ce titre et faisait état de l'obtention d'un prêt par la commune, cette circonstance étant présentée comme de nature à démontrer que la gestion financière n'était pas " en difficulté ", que la question du niveau d'endettement de la commune aurait été préalablement évoquée dans le cadre de la campagne électorale. Par suite, l'introduction d'un élément nouveau de polémique électorale sur un sujet sensible comme celui de l'endettement de la commune, juste avant le scrutin, à un moment où la liste conduite par Mme B... ne pouvait y répondre utilement, a été de nature à affecter la sincérité du scrutin, compte tenu de l'écart de seulement 33 voix sur 1 141 suffrages exprimés entre la liste " La Sentinelle, la ville des possibles " et la liste " La Sentinelle, continuons ensemble ! ".<br/>
<br/>
              5. Il résulte de ce qui précède que M. C... n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Lille a annulé les opérations électorales qui se sont déroulées les 15 mars et 28 juin 2020 pour l'élection des conseillers municipaux et communautaire de la commune de La Sentinelle. <br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de Mme B... qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. C... une somme au titre de ces mêmes dispositions. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de M. C... est rejetée.<br/>
Article 2 : Les conclusions de Mme B... au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. A... C..., à Mme D... B... et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
