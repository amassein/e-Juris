<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044806263</ID>
<ANCIEN_ID>JG_L_2021_12_000000455334</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/80/62/CETATEXT000044806263.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 30/12/2021, 455334, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>455334</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI ; SCP CELICE, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>Mme Pearl Nguyên Duy</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:455334.20211230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Le directeur général de l'agence régionale de santé de Bourgogne-Franche-Comté a porté plainte contre M. D... C... et contre la société Pharmacie du drapeau devant la chambre de discipline du conseil régional de Bourgogne de l'ordre des pharmaciens. Par une décision du 4 mars 2019, la chambre de discipline a prononcé à l'encontre de M. C... la sanction de l'interdiction d'exercer la pharmacie pour une durée de deux ans et, à l'encontre de la société Pharmacie du drapeau, la sanction de l'interdiction d'exercer la pharmacie pour une durée d'un an. <br/>
<br/>
              Par une décision du 18 juin 2021, la chambre de discipline du Conseil national de l'ordre des pharmaciens a rejeté l'appel formé par M. C... et par la société Pharmacie du drapeau contre cette décision et a fixé les dates d'exécution de ces sanctions du 1er novembre 2021 au 31 octobre 2023 et du 1er novembre 2021 au 31 octobre 2022. <br/>
<br/>
              1° Sous le n° 455334, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 9 août et 27 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, la société Pharmacie du drapeau et M. C... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de l'agence régionale de santé de Bourgogne-Franche-Comté la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 457024, par une requête, enregistrée le 27 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, la société Pharmacie du drapeau et M. C... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner qu'il soit sursis à l'exécution de la décision de la chambre de discipline du Conseil national de l'ordre des pharmaciens du 18 juin 2021 ;<br/>
              2°) de mettre à la charge de l'agence régionale de santé de Bourgogne-Franche-Comté la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - l'arrêté du 28 novembre 2016 relatif aux bonnes pratiques de dispensation des médicaments dans les pharmacies d'officine, les pharmacies mutualistes et les pharmacies de secours minières, mentionnées à l'article R. 5121-5 du code de la santé publique ;<br/>
              - l'arrêté du 1er août 1991 relatif au nombre de pharmaciens dont les titulaires d'officine doivent se faire assister en raison de l'importance de leur chiffre d'affaires ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pearl Nguyên Duy, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Spinosi, avocat de la société Pharmacie du drapeau et de M. C....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Le pourvoi par lequel la société Pharmacie du drapeau et M. C... demandent l'annulation de la décision du 18 juin 2021 par laquelle  la chambre de discipline du Conseil national de l'ordre des pharmaciens a rejeté l'appel qu'ils ont formé contre la décision du 4 mars 2019 par laquelle la chambre de discipline du conseil régional de Bourgogne de l'ordre des pharmaciens a prononcé à leur encontre la sanction de l'interdiction d'exercer la pharmacie pour une durée, respectivement, d'un an et de deux ans, et la requête par laquelle ils demandent qu'il soit sursis à l'exécution de cette décision présentent à juger les mêmes questions. Il y a lieu d'y statuer par une seule décision.<br/>
<br/>
              2. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ". <br/>
<br/>
              3. Pour demander l'annulation de la décision de la chambre de discipline du Conseil national de l'ordre des pharmaciens du 18 juin 2021 qu'ils attaquent, la société Pharmacie du drapeau et M. C... soutiennent qu'elle est entachée :<br/>
              - d'irrégularité, faute d'analyser leur mémoire enregistré le 2 mai 2021 ;<br/>
              - d'erreur de droit en ce qu'elle juge inopérant le moyen tiré de l'absence de communication des procès-verbaux de l'inspection de l'agence régionale de santé ; <br/>
              - d'inexacte qualification juridique des faits en ce qu'elle retient le grief tiré de l'insuffisance du nombre de pharmaciens adjoints ;<br/>
              - d'erreur de droit en ce qu'elle juge que la gestion de fait de la parapharmacie de Champagnole par M. C... caractérise une situation de cumul d'activités contraire aux dispositions des articles L. 5125-2 et R. 4235-4 du code de la santé publique ;<br/>
              - d'inexacte qualification juridique des faits en ce qu'elle retient le grief tiré de la complicité d'exercice illégal de la pharmacie ;<br/>
              - d'erreur de droit, d'inexacte qualification juridique des faits, de dénaturation des pièces du dossier et d'insuffisance de motivation en ce qu'elle retient le grief tiré de l'exercice d'une activité de grossiste-répartiteur non autorisée ;<br/>
              - d'irrégularité, faute de les avoir mis au préalable à même de s'expliquer sur le grief tiré de l'exercice d'une activité de grossiste-répartiteur non autorisée ;<br/>
              - d'erreur de droit en ce qu'elle juge qu'ils ne pouvaient légalement sous-traiter à une autre officine de pharmacie le contrôle des préparations magistrales ;<br/>
              - d'erreur de droit et d'inexacte qualification juridique des faits en ce qu'elle juge qu'ils ne pouvaient légalement mettre les médicaments commandés sur le site internet de l'officine à la disposition des patients dans un " point relais " ;<br/>
              - d'erreur de droit, d'inexacte qualification juridique des faits et de dénaturation des pièces du dossier en ce qu'elle estime qu'ils n'ont pas prévu de dispositif d'alerte ou de blocage lorsque les quantités de médicaments commandés sur le site internet de l'officine conduisent à un dépassement de la dose d'exonération autorisée.<br/>
              Ils soutiennent en outre qu'elle leur inflige une sanction hors de proportion avec la gravité des fautes retenues.<br/>
<br/>
              3. Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
              4. Le pourvoi formé par la société Pharmacie du drapeau et par M. C... contre la décision de la chambre de discipline du Conseil national de l'ordre des pharmaciens du 18 juin 2021 n'étant pas admis, les conclusions de leur requête tendant à ce qu'il soit sursis à l'exécution de cette décision sont devenues sans objet. Il n'y a donc pas lieu d'y statuer. <br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'agence régionale de santé de Bourgogne-Franche-Comté, qui n'est pas la partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Pharmacie du drapeau et de M. C... n'est pas admis.<br/>
<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions de la requête de la société Pharmacie du drapeau et de M. C... tendant à ce qu'il soit sursis à l'exécution de la décision de la chambre de discipline du Conseil national de l'ordre des pharmaciens du 18 juin 2021.<br/>
<br/>
Article 3 : Les conclusions de la requête de la société Pharmacie du drapeau et de M. C... présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société Pharmacie du drapeau et à M. D... C....<br/>
Copie en sera adressée à l'agence régionale de santé de Bourgogne-Franche-Comté et au Conseil national de l'ordre des pharmaciens.<br/>
              Délibéré à l'issue de la séance du 14 décembre 2021 où siégeaient : M. Jean-Philippe Mochon, assesseur, présidant ; M. Olivier Yeznikian, conseiller d'Etat et Mme Pearl Nguyên Duy, maître des requêtes-rapporteure. <br/>
<br/>
              Rendu le 30 décembre 2021.<br/>
<br/>
                 Le président : <br/>
                 Signé : M. F... A...<br/>
 		La rapporteure : <br/>
      Signé : Mme Pearl Nguyên Duy<br/>
                 Le secrétaire :<br/>
                 Signé : M. B... E...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
