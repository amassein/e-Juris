<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034230338</ID>
<ANCIEN_ID>JG_L_2017_03_000000393320</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/23/03/CETATEXT000034230338.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 20/03/2017, 393320</TITRE>
<DATE_DEC>2017-03-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393320</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Géraud Sajust de Bergues</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:393320.20170320</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B...a demandé au tribunal administratif de Besançon d'annuler la décision du 29 octobre 2012 par laquelle le président du centre de gestion de la fonction publique territoriale du Territoire de Belfort a prononcé son licenciement à titre disciplinaire. Par un jugement n° 1300069 du 6 mai 2014, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14NC01247 du 2 juillet 2015, la cour administrative d'appel de Nancy, faisant droit à l'appel de M.B..., a annulé ce jugement et la décision litigieuse.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 9 septembre et 27 novembre 2015 au secrétariat du contentieux du Conseil d'Etat, le centre de gestion de la fonction publique territoriale du Territoire de Belfort demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. B...;<br/>
<br/>
              3°) de mettre à la charge de M. B...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 88-145 du 15 février 1988 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Géraud Sajust de Bergues, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat du centre de gestion de la fonction publique territoriale du Territoire de Belfort ;<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le centre de gestion de la fonction publique territoriale du Territoire de Belfort a recruté par contrat M. A...B..., à compter du 4 juin 2010. M. B...a été mis à la disposition de la commune de Belfort pour y exercer, au titre d'un remplacement, les fonctions d'adjoint technique au sein de la police municipale. A la suite d'un rapport établi par le maire de Belfort, le centre de gestion a engagé une procédure qui a conduit au licenciement à titre disciplinaire de M. B..., cette sanction prenant effet le 19 novembre 2012. Cette mesure a été prononcée au motif que l'intéressé avait méconnu ses obligations professionnelles en divulguant, sur divers réseaux sociaux accessibles via Internet, des photographies et informations relatives à l'organisation de la police municipale, et notamment du système de vidéosurveillance en service dans cette commune. M. B...a saisi le tribunal administratif de Besançon de conclusions dirigées contre ce licenciement, sa demande ayant toutefois été rejetée par un jugement du 6 mai 2014. Par un arrêt du 2 juillet 2015, la cour administrative d'appel de Nancy, faisant droit à l'appel de M. B..., a annulé ce jugement ainsi que la décision litigieuse. Le centre de gestion de la fonction publique territoriale du Territoire de Belfort se pourvoit en cassation contre cet arrêt.<br/>
<br/>
              2. Aux termes de l'article 26 de la loi du 13 juillet 1983 portant droit et obligations des fonctionnaires, applicable aux agents non titulaires de la fonction publique territoriale en vertu de l'article 136 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " (...) Les fonctionnaires doivent faire preuve de discrétion professionnelle pour tous les faits, informations ou documents dont ils ont connaissance dans l'exercice ou à l'occasion de l'exercice de leurs fonctions (...) ". Il ressort des pièces du dossier soumis aux juges du fond que M. B...a divulgué sur Internet, au moyen d'un " blog " personnel et de comptes ouverts à son nom dans trois réseaux sociaux, des éléments détaillés et précis sur les domaines d'activité de la police municipale dans lesquels il intervenait, en faisant, en outre, systématiquement usage de l'écusson de la police municipale. Les éléments ainsi diffusés par M. B...étaient de nature à donner accès à des informations relatives à l'organisation du service de la police municipale, en particulier des dispositifs de vidéosurveillance et de vidéoverbalisation mis en oeuvre dans la commune. Eu égard à ces circonstances, la cour a inexactement qualifié les faits soumis à son appréciation en jugeant que M. B...n'avait pas commis de manquement à son obligation de discrétion professionnelle.<br/>
<br/>
              3. Il résulte de tout ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que l'arrêt attaqué doit être annulé.<br/>
<br/>
              4. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B...la somme que le centre de gestion de la fonction publique territoriale du Territoire de Belfort demande au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 2 juillet 2015 est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy.<br/>
Article 3 : Les conclusions présentées par le centre de gestion de la fonction publique territoriale du Territoire de Belfort tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée au centre de gestion de la fonction publique territoriale du Territoire de Belfort et à M. A... B....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-07-11 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. OBLIGATIONS DES FONCTIONNAIRES. - OBLIGATION DE DISCRÉTION PROFESSIONNELLE (ART. 26 DE LA LOI DU 13 JUILLET 1983) - MÉCONNAISSANCE - AGENT DIFFUSANT SUR INTERNET DES ÉLÉMENTS DÉTAILLÉS ET PRÉCIS SUR L'ORGANISATION DU SERVICE DE LA POLICE MUNICIPALE DONT IL RELÈVE.
</SCT>
<ANA ID="9A"> 36-07-11 Agent ayant divulgué sur Internet, au moyen d'un blog personnel et de comptes ouverts à son nom dans trois réseaux sociaux, des éléments détaillés et précis sur les domaines d'activité de la police municipale dans lesquels il intervenait, en faisant, en outre, systématiquement usage de l'écusson de la police municipale. Les éléments ainsi diffusés par l'intéressé étaient de nature à donner accès à des informations relatives à l'organisation du service de la police municipale, en particulier des dispositifs de vidéosurveillance et de vidéoverbalisation mis en oeuvre dans la commune. Eu égard à ces circonstances, il a commis un manquement à son obligation de discrétion professionnelle.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
