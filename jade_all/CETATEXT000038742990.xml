<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038742990</ID>
<ANCIEN_ID>JG_L_2019_07_000000431306</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/74/29/CETATEXT000038742990.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 01/07/2019, 431306, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431306</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET ; SCP MEIER-BOURDEAU, LECUYER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:431306.20190701</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 3 et 20 juin 2019 au secrétariat du contentieux du Conseil d'Etat, la société LVN Limited demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution des actes des 2 et 24 avril 2019 de l'Autorité de régulation des jeux en ligne relatifs à la fourniture de conseils payants en matière de jeux en ligne ; <br/>
<br/>
              2°) d'enjoindre à l'Autorité de régulation des jeux en ligne d'informer les opérateurs de paris sportifs en ligne de leur droit à renouer des partenariats commerciaux avec des sites de fourniture de conseils en paris sportifs, sans risquer une sanction de sa part, jusqu'à l'intervention de la décision du Conseil d'Etat au fond ;<br/>
<br/>
              3°) de mettre à la charge de l'Autorité de régulation des jeux en ligne la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - sa requête est recevable, dès lors que les actes de l'Autorité de régulation des jeux en ligne sont constitutifs de prises de position de nature à produire des effets notables sur sa situation économique ;<br/>
              - la condition de l'urgence est remplie, dès lors que les actes de l'Autorité de régulation des jeux en ligne, en demandant aux opérateurs agréés de paris en ligne de cesser toutes relations commerciales avec les pronostiqueurs, ont eu pour effet une baisse significative de son chiffre d'affaires du fait de la rupture par les opérateurs agréés de paris en ligne de ses contrats d'affiliation ;<br/>
              - il existe un doute sérieux quant à la légalité des actes contestés ; en effet :<br/>
              - les actes litigieux ont été pris en méconnaissance du principe du caractère contradictoire de la procédure, dès lors qu'ils ont été édictés alors qu'ils sont constitutifs d'une sanction, sans débat contradictoire préalable, en méconnaissance de l'article 43 de la loi du 12 mai 2010 ;<br/>
              - ils portent une atteinte disproportionnée à la liberté du commerce et de l'industrie, dès lors qu'ils entravent leur activité de pronostiqueurs laquelle est légale et n'est pas  soumise à autorisation ;<br/>
              - ils sont entachés d'erreurs de qualification juridique, dès lors que l'Autorité de régulation des jeux en ligne a considéré à tort que, d'une part, l'activité des pronostiqueurs était susceptible d'être qualifiée de publicité trompeuse au sens du code de la consommation et d'escroquerie au sens du code pénal et, d'autre part, que les paris sportifs constituaient des " jeux de hasard " au sens de la directive européenne n° 2005/29/CE du Parlement européen et du Conseil du 11 mai 2005 relative aux pratiques commerciales déloyales des entreprises vis-à-vis des consommateurs dans le marché intérieur.<br/>
<br/>
<br/>
              Par un mémoire distinct, enregistré le 20 juin 2019, présenté en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, la société LVN Limited demande au juge des référés du Conseil d'Etat de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 121-4 du code de la consommation. Elle soutient que cet article est applicable au litige, qu'il n'a jamais été déclaré conforme à la Constitution et qu'il porterait atteinte au principe de légalité des peines et délits, au principe selon lequel la loi n'a le droit de défendre que les actions nuisibles à la société, à la liberté d'entreprendre et à la liberté contractuelle.<br/>
<br/>
              Par un mémoire en défense, enregistré le 19 juin 2019, l'Autorité de régulation des jeux en ligne conclut au rejet de la requête et à ce que soit mis à la charge de la société LVN Limited le versement d'une somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. Elle soutient que la requête est irrecevable, que la condition d'urgence n'est pas remplie et, enfin, que les moyens soulevés par la requérante ne sont pas fondés.<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la société LVN Limited et, d'autre part, l'Autorité de régulation des jeux en ligne ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du vendredi 21 juin 2019 à 10 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Poupet, avocate au Conseil d'Etat et à la Cour de cassation, avocat de la société LVN Limited ;<br/>
<br/>
              - les représentants de la société LVN Limited ;<br/>
<br/>
              - Me Lécuyer, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'Autorité de régulation des jeux en ligne ;<br/>
<br/>
              - les représentants de l'Autorité de régulation des jeux en ligne ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a différé la clôture de l'instruction au 21 juin à 18 heures 30 puis au 24 juin 2019 à 15 heures ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 21 juin 2019, présenté par la société LVN Limited qui reprend les conclusions et moyens présentés à l'appui de sa requête ;<br/>
<br/>
              Vu les observations complémentaires en défense, enregistrées le 24 juin 2019, présentées par l'Autorité de régulation des jeux en ligne avant la clôture de l'instruction, par lesquelles l'Autorité de régulation des jeux en ligne, d'une part, maintient ses conclusions et moyens et, d'autre part, fait valoir qu'il n'y a pas lieu de statuer sur la question prioritaire de constitutionnalité soulevée par la société LVN Limited ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la directive européenne n° 2005/29/CE du Parlement européen et du Conseil du 11 mai 2005 relative aux pratiques commerciales déloyales des entreprises vis-à-vis des consommateurs dans le marché intérieur ;<br/>
              - la loi n° 2010-476 du 12 mai 2010 relative à l'ouverture à la concurrence et à la régulation des jeux d'argent et de hasard en ligne ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Il résulte de ces dispositions que le prononcé de la suspension d'un acte administratif est subordonné notamment à une condition d'urgence. L'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celui-ci porte atteinte de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue.<br/>
<br/>
              2. Par deux lettres en date des 2 avril et 24 mai 2019, l'Autorité de régulation des jeux en ligne a mis en garde les représentants légaux des opérateurs agrées de jeux et de paris en ligne sur les pratiques illicites des fournisseurs de conseils payants en matière de paris sportifs et les a incités à mettre fin à leurs relations contractuelles avec ces fournisseurs. L'Autorité de régulation des jeux en ligne a précisé que la persistance de telles relations, contraires à l'objet de la loi, pourraient justifier, le cas échéant, une sanction ou la suspension, voire le retrait de l'agrément. <br/>
<br/>
              3. Pour caractériser l'urgence qu'il y a à suspendre les mises en garde de l'Autorité de régulation des jeux en ligne, la société LVN Limited, société de droit maltais exerçant une activité de marketing en ligne et de conseils en paris sportifs, expose que son modèle économique repose, à la fois, sur le produit des abonnements proposés à ses clients pour bénéficier de ses pronostics sportifs et sur les commissions engendrées par les contrats commerciaux qu'elle perçoit des sites de paris en ligne auxquels ses clients sont incités à se reporter. Les trois opérateurs agrées de jeux et de paris en ligne avec lesquels elle avait conclu des relations contractuelles les ayant rompues à la suite des mises en garde de l'Autorité de régulation des jeux en ligne, la société requérante fait valoir que son chiffre d'affaires en a été immédiatement et significativement affecté et devrait décroître de 45% dans les prochains mois, si la situation devait perdurer.<br/>
<br/>
              4. Cependant, la société LVN Limited n'a produit à l'appui de cette affirmation qu'une unique attestation d'un expert-comptable. Elle n'a pas, ni dans ses écrits en réponse aux observations de l'Autorité de régulation des jeux en ligne soulignant le caractère insuffisamment circonstancié de cette attestation, ni lors du débat contradictoire à l'audience, précisé son chiffre d'affaires et le nombre de ses clients. Elle n'a pas davantage explicité la répartition de son chiffre d'affaires entre les recettes issues des abonnements de ses clients et celles nées des commissions versées par les opérateurs de jeu, alors même que l'Autorité de régulation des jeux en ligne faisait valoir que la part prépondérante des recettes d'opérateurs tels que la société requérante est, en général, issue des abonnements. <br/>
<br/>
              5. Il résulte de ce qui précède que la condition d'urgence ne peut être, en l'espèce, regardée comme établie. Dès lors, et sans qu'il soit besoin de statuer sur la recevabilité de la requête, ni davantage sur l'existence d'un doute sérieux quant à la légalité des mises en garde contestées, la requête présentée par la société LVN Limited doit être rejetée.<br/>
<br/>
              6. La présente ordonnance rejette les conclusions à fin de suspension pour défaut d'urgence ; il n'y a pas lieu, par suite, de statuer sur la demande de renvoi au Conseil constitutionnel de la question prioritaire de constitutionnalité soulevée.<br/>
<br/>
              7. L'Autorité de régulation des jeux en ligne n'étant pas la partie perdante, les conclusions de la société LVN Limited tendant à ce que cette autorité soit condamnée à lui verser une somme sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative doivent être rejetées. Il n'y a pas lieu, dans les circonstances de l'espèce, de condamner la société LVN Limited à verser à l'Autorité de régulation des jeux en ligne les conclusions présentées par cette dernière sur le même fondement. <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société LVN Limited.<br/>
Article 2 : La requête de la société LVN Limited est rejetée. <br/>
Article 3 : Les conclusions de l'Autorité de régulation des jeux en ligne présentées sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.  <br/>
Article 4 : La présente ordonnance sera notifiée à la société LVN Limited et à l'Autorité de régulation des jeux en ligne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
