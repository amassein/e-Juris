<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036586701</ID>
<ANCIEN_ID>JG_L_2018_02_000000413486</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/58/67/CETATEXT000036586701.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 07/02/2018, 413486, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-02-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413486</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI ; SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:413486.20180207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Gascogne et la société Gascogne Flexible ont demandé au juge des référés du tribunal administratif de Châlons-en-Champagne, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution d'un arrêté de péril imminent pris par le maire de la commune de Givet le 19 mai 2017 en tant qu'il ordonne à la société Gascogne de prendre avant le 30 septembre 2017 des mesures visant à mettre en sécurité définitive ses deux propriétés, sises 27 rue du Luxembourg. Par une ordonnance n° 1701401 du 2 août 2017, le juge des référés a rejeté leur demande. <br/>
<br/>
              Par un pourvoi, deux mémoires complémentaires, un mémoire en réplique et un nouveau mémoire, enregistrés les 17 et 31 août, 22 septembre 2017, 9 et 30 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, la société Gascogne et la société Gascogne Flexible demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) statuant en référé, de suspendre la décision du maire de Givet du 19 mai 2017 dans la mesure demandée ; <br/>
<br/>
              3°) de mettre à la charge de la commune la somme de 4 500 euros sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la construction et de l'habitation ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de la société Gascogne Flexible et de la société Gascogne et à la SCP Célice, Soltner, Texidor, Perier, avocat de la commune de Givet.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-1 du code de justice administrative :  " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision (...) " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que, par arrêté du 19 mai 2017, le maire de Givet a ordonné à la société Gascogne, actionnaire unique de la société Gascogne Flexible, d'une part, de prendre immédiatement diverses mesures provisoires destinées à mettre en sécurité deux bâtiments sis 27 rue du Luxembourg, appartenant à la société Gascogne Flexible, fragilisés à la suite d'un incendie, d'autre part, avant le 30 septembre 2017, de " prendre toutes dispositions à même de mettre cet ensemble en sécurité définitive " en procédant à une " réhabilitation de l'immeuble " comportant à tout le moins " la réfection globale de la couverture, l'obturation de toutes les baies extérieures, la recréation des structures horizontales intérieures participant au contreventement de murs, la consolidation des souches de cheminées " ; que les sociétés Gascogne et Gascogne Flexible ont demandé au juge des référés du tribunal administratif de Châlons-en-Champagne, sur le fondement de l'article L. 521-1 précité du code de justice administrative, de suspendre l'exécution de cet arrêté en tant qu'il prescrivait les mesures de mise en sécurité définitive ; que, par l'ordonnance attaquée, le juge des référés a rejeté cette demande ;<br/>
<br/>
              3. Considérant que, pour estimer que n'était pas remplie la condition d'urgence à laquelle les dispositions de l'article L. 521-1 subordonnent la possibilité pour le juge d'ordonner, en référé, la suspension de l'exécution d'une décision administrative, l'auteur de l'ordonnance attaquée a notamment relevé que les mesures prescrites par l'arrêté contesté du maire de Givet présentaient " un caractère provisoire " ; qu'en se prononçant ainsi, alors que les mesures contestées, rappelées ci-dessus, ont pour objet de remédier définitivement au péril par des travaux portant sur la structure des bâtiments, il a dénaturé les éléments qui lui étaient soumis ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'ordonnance attaquée doit être annulée ;<br/>
<br/>
              4. Considérant qu'il y a lieu pour le Conseil d'Etat de régler l'affaire au titre de l'article L. 521-1 du code de justice administrative par application de l'article L. 821-1 du même code ;<br/>
<br/>
              5. Considérant que l'urgence justifie que soit prononcée la suspension d'une décision administrative lorsque l'exécution de celle-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de la décision litigieuse sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de cette décision soit suspendue ; que l'urgence doit être appréciée objectivement compte tenu de l'ensemble des circonstances de l'affaire ;<br/>
<br/>
              6. Considérant qu'il est constant que les sociétés requérantes ont mis en oeuvre les mesures provisoires prescrites par l'arrêté litigieux du maire de Givet ; que ces mesures assurent dans l'immédiat la sécurité des personnes ; qu'il ressort des pièces du dossier que l'exécution des mesures litigieuses, visant à assurer une mise en sécurité définitive, implique des travaux importants et coûteux ; que, dans ces conditions, la condition d'urgence prévue à l'article L. 521-1 du code de justice administrative doit être regardée comme remplie ;<br/>
<br/>
              7. Considérant qu'aux termes de l'article L. 511-3 du code de la construction et de l'habitation : " En cas de péril imminent, le maire, après avertissement adressé au propriétaire, demande à la juridiction administrative compétente la nomination d'un expert qui, dans les vingt-quatre heures qui suivent sa nomination, examine les bâtiments, dresse constat de l'état des bâtiments mitoyens et propose des mesures de nature à mettre fin à l'imminence du péril s'il la constate. / Si le rapport de l'expert conclut à l'existence d'un péril grave et imminent, le maire ordonne les mesures provisoires nécessaires pour garantir la sécurité, notamment, l'évacuation de l'immeuble. / Dans le cas où ces mesures n'auraient pas été exécutées dans le délai imparti, le maire les fait exécuter d'office. En ce cas, le maire agit en lieu et place des propriétaires, pour leur compte et à leurs frais. / Si les mesures ont à la fois conjuré l'imminence du danger et mis fin durablement au péril, le maire, sur le rapport d'un homme de l'art, prend acte de leur réalisation et de leur date d'achèvement. / Si elles n'ont pas mis fin durablement au péril, le maire poursuit la procédure dans les conditions prévues à l'article L. 511-2 " ;<br/>
<br/>
              8. Considérant que le moyen tiré de ce que les mesures prescrites par l'arrêté litigieux et dont la suspension est demandée ne présentent pas un caractère provisoire et ne pouvaient, par suite, être ordonnées dans le cadre de la procédure prévue par les dispositions citées ci-dessus de l'article L. 511-3 du code de la construction et de l'habitation est de nature, en l'état de l'instruction, à créer un doute sérieux quant à la légalité de cet arrêté ;<br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède qu'il y a lieu, pour le Conseil d'Etat, d'ordonner la suspension de l'arrêté du maire de Givet du 19 mai 2017 en tant qu'il ordonne des mesures visant à mettre en sécurité définitive des immeubles appartenant à la société Gascogne Flexible ;<br/>
<br/>
              10. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Givet la somme de 3 000 euros à verser aux sociétés requérantes au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font en revanche obstacle à ce qu'une somme soit mise, à ce titre, à la charge des sociétés requérantes qui ne sont pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Châlons-en-Champagne du 2 août 2017 est annulée.<br/>
<br/>
Article 2 : L'exécution de l'arrêté du maire de Givet du 19 mai 2017 est suspendue en tant qu'il ordonne à la société Gascogne de " prendre toutes dispositions à même de mettre (les immeubles concernés) en sécurité définitive " et notamment " la réfection globale de la couverture, l'obturation de toutes les baies extérieures, la recréation des structures horizontales intérieures participant au contreventement de murs, la consolidation des souches de cheminées ".<br/>
<br/>
Article 3 : La commune de Givet versera aux sociétés Gascogne Flexible et Gascogne une somme globale de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées par la commune de Givet au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la société Gascogne Flexible, à la société Gascogne et à la commune de Givet.<br/>
		Copie en sera adressée au ministre de la cohésion des territoires.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
