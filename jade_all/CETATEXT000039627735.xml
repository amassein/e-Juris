<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039627735</ID>
<ANCIEN_ID>JG_L_2019_12_000000397134</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/62/77/CETATEXT000039627735.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 16/12/2019, 397134, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-12-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397134</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUTET-HOURDEAUX</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Nevache</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:397134.20191216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 17 mars 2017, le Conseil d'Etat, statuant au contentieux, saisi de la requête de la Fédération française des sociétés d'assurance (FFSA) tendant à l'annulation pour excès de pouvoir de l'arrêté du 11 décembre 2015 du ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social portant extension d'un accord conclu dans le cadre de la convention collective de la librairie (n° 3013), a sursis à statuer sur ces conclusions en tant que cet arrêté étend les stipulations des articles 8.1, 9.3, 10, 10.1 et 10.2 de l'accord du 2 juillet 2015 relatives au fonds de solidarité de la branche, jusqu'à ce que le tribunal de grande instance de Paris se soit prononcé sur le point de savoir si l'exercice par les parties à l'accord du 1er avril 2015 de la liberté contractuelle leur permettait, en l'absence de disposition législative, de prévoir la mutualisation du financement et de la gestion de certaines prestations et notamment leur financement par un prélèvement de 2 % sur les cotisations versées à l'organisme recommandé, ou un prélèvement équivalent à cette somme exigible auprès des entreprises qui n'adhèrent pas à l'organisme recommandé.<br/>
<br/>
              Par un jugement n° 17/06346 du 20 février 2018, le tribunal de grande instance de Paris s'est prononcé sur cette question.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier, y compris celles visées par la décision du Conseil d'Etat du 17 mars 2017 ;<br/>
<br/>
              Vu :<br/>
              - le code de la sécurité sociale ;<br/>
              - le code du travail ;<br/>
              - la loi n° 2013-1203 du 23 décembre 2013 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Nevache, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boutet-Hourdeaux, avocat de la SLF, de la FS CFDT, de la CDS CGT, de la SNPELAC CFTC, de la FEC FO et de la FCCS CFE-CGC ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 911-1 du code de la sécurité sociale prévoit qu'à moins qu'elles ne soient instituées par des dispositions législatives ou réglementaires, les garanties collectives dont bénéficient les salariés, anciens salariés et ayants droit en complément de celles qui résultent de l'organisation de la sécurité sociale sont déterminées, notamment, par voie de conventions ou d'accords collectifs. Le I de l'article L. 912-1 du même code, dans sa rédaction issue de la loi du 23 décembre 2013, dispose que : " Les accords professionnels ou interprofessionnels mentionnés à l'article L. 911-1 peuvent, dans des conditions fixées par décret en Conseil d'Etat, prévoir l'institution de garanties collectives présentant un degré élevé de solidarité et comprenant à ce titre des prestations à caractère non directement contributif, pouvant notamment prendre la forme d'une prise en charge partielle ou totale de la cotisation pour certains salariés ou anciens salariés, d'une politique de prévention ou de prestations d'action sociale. / Dans ce cas, les accords peuvent organiser la couverture des risques concernés en recommandant un ou plusieurs organismes mentionnés à l'article 1er de la loi n° 89-1009 du 31 décembre 1989 renforçant les garanties offertes aux personnes assurées contre certains risques ou une ou plusieurs institutions mentionnées à l'article L. 370-1 du code des assurances (...) ", sous réserve du respect des conditions de mise en concurrence et d'égalité définies au II du même article.<br/>
<br/>
              2. En application du premier alinéa de l'article L. 2261-15 du code du travail, aux termes duquel : " Les stipulations d'une convention de branche ou d'un accord professionnel ou interprofessionnel (...) peuvent être rendues obligatoires pour tous les salariés et employeurs compris dans le champ d'application de cette convention ou de cet accord, par arrêté du ministre chargé du travail, après avis motivé de la Commission nationale de la négociation collective ", le ministre chargé du travail a, par un arrêté du 11 décembre 2015, étendu les stipulations de l'accord du 2 juillet 2015 relatif à l'instauration d'un régime professionnel de santé, conclu dans le cadre de la convention collective nationale de la librairie. L'accord ainsi étendu procède notamment, en application de l'article L. 912-1 du code de la sécurité sociale, à la recommandation de l'institution de prévoyance IPSEC pour l'assurance des garanties frais de santé ainsi que pour la gestion de l'ensemble des garanties et pour procéder, y compris auprès des entreprises non adhérentes à cette institution au titre du régime conventionnel de prévoyance, à l'appel des cotisations relatives au fonds de solidarité de la branche qu'il institue. La Fédération française des sociétés d'assurance (FFSA), devenue Fédération française de l'assurance, demande au Conseil d'Etat d'annuler pour excès de pouvoir cet arrêté.<br/>
<br/>
              3. Par une décision du 17 mars 2017, le Conseil d'Etat, statuant au contentieux a sursis à statuer sur les conclusions de la requête tendant à l'annulation de l'arrêté du 11 décembre 2015 en tant qu'il étend les stipulations des articles 8.1, 9.3, 10, 10.1 et 10.2 de l'accord du 2 juillet 2015 relatives au fonds de solidarité de la branche, jusqu'à ce que le tribunal de grande instance de Paris se soit prononcé sur le point de savoir si l'exercice par les parties à l'accord de la liberté contractuelle leur permettait, en l'absence de disposition législative, de prévoir la mutualisation du financement et de la gestion de certaines prestations et notamment leur financement par un prélèvement de 2 % sur les cotisations versées à l'organisme recommandé, ou un prélèvement équivalent à cette somme exigible auprès des entreprises qui n'adhèrent pas à l'organisme recommandé.<br/>
<br/>
              Sur l'arrêté attaqué, en tant qu'il étend les stipulations des articles 8.1, 9.3, 10, 10.1 et 10.2 de l'accord du 2 juillet 2015 relatives au fonds de solidarité de la branche :<br/>
<br/>
              4. Le IV de l'article L. 912-1 du code de la sécurité sociale dispose que : " Les accords mentionnés au I peuvent prévoir que certaines des prestations nécessitant la prise en compte d'éléments relatifs à la situation des salariés ou sans lien direct avec le contrat de travail les liant à leur employeur sont financées et gérées de façon mutualisée, selon des modalités fixées par décret en Conseil d'Etat, pour l'ensemble des entreprises entrant dans leur champ d'application ". Toutefois, ainsi que le Conseil d'Etat l'a jugé par sa décision du 17 mars 2017, ces dispositions n'étaient pas entrées en vigueur à la date de la conclusion de l'accord litigieux faute que soit intervenu, à cette date, le décret nécessaire à cette entrée en vigueur, prévoyant ces modalités de gestion et de financement mutualisé de certaines prestations.<br/>
<br/>
              5. Le tribunal de grande instance de Paris, par un jugement du 20 février 2018, a jugé qu'en l'absence de dispositions législatives, les signataires de 1'accord du 2 juillet 2015 relatif à l'instauration du régime professionnel de santé ne pouvaient prévoir la mutualisation du financement et de la gestion de certaines prestations et notamment leur financement par un prélèvement de 2 % sur les cotisations versées à l'organisme recommandé, ou un prélèvement équivalent à cette somme exigible des entreprises qui n'adhèrent pas à l'organisme recommandé. Ce jugement n'a pas fait l'objet d'un pourvoi et est devenu définitif. En raison de l'autorité qui s'y attache, la réponse ainsi faite par le tribunal à la question qui lui a été renvoyée s'impose au Conseil d'Etat, sans que les parties défenderesses puissent utilement faire valoir que la Cour de cassation, saisie de pourvois en cassation contre des jugements analogues rendus le même jour par le même tribunal dans des affaires similaires, a statué dans un sens différent par des arrêts du 9 octobre 2019, qui sont dépourvus de toute autorité de chose jugée dans la présente affaire. <br/>
<br/>
              6. Il en résulte que la fédération requérante est fondée à soutenir que l'arrêté attaqué ne pouvait légalement étendre les clauses du cinquième alinéa de l'article 8.1, de l'article 9.3, du troisième alinéa de l'article 10 et des articles 10.1 et 10.2 de l'accord du 2 juillet 2015, relatives au fonds de solidarité de la branche, prévoyant les prestations qui seraient financées et gérées de façon mutualisée ainsi que les modalités de leur financement, par un prélèvement à la charge de toutes les entreprises, et de leur gestion mutualisés, et à demander, pour ce motif, l'annulation de l'arrêté attaqué, en tant seulement qu'il n'a pas exclu de l'extension ces clauses, qui sont divisibles.<br/>
<br/>
              Sur les conséquences de l'illégalité de l'arrêté attaqué :<br/>
<br/>
              7. Il ressort des pièces du dossier que la disparition rétroactive, dans la mesure de l'annulation prononcée, des dispositions de l'arrêté du 11 décembre 2015 aurait en l'espèce, eu égard tant à l'absence d'autre moyen fondé qu'à l'objet des clauses considérées, des conséquences manifestement excessives pour les salariés et employeurs auxquels l'arrêté d'extension a rendu l'accord obligatoire, de nature à justifier une limitation dans le temps des effets de son annulation. Dans ces conditions, sous réserve des actions contentieuses engagées à la date de la présente décision contre les actes pris sur ce fondement, devront être regardés comme définitifs, dans la mesure de l'annulation prononcée, les effets produits par l'arrêté attaqué antérieurement au 12 mai 2018, date d'entrée en vigueur de l'arrêté du 9 mai 2018 par lequel le ministre chargé du travail a étendu l'avenant du 19 septembre 2017 qui a repris l'accord du 2 juillet 2015.<br/>
<br/>
               Sur les frais de l'instance :<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas la partie perdante pour l'essentiel, la somme que demande la Fédération française de l'assurance à ce titre. Il y a lieu, en revanche, de mettre à la charge de la fédération requérante la somme de 500 euros à verser au Syndicat de la librairie française, à la SNPELAC CFTC, à la FEC FO, à la FS CFDT et à la CDS CGT au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêté du 11 décembre 2015 du ministre du travail de l'emploi, de la formation professionnelle et du dialogue social est annulé en tant qu'il étend les stipulations du cinquième alinéa de l'article 8.1, de l'article 9.3, du troisième alinéa de l'article 10 et des articles 10.1 et 10.2 de l'accord du 2 juillet 2015, relatives au fonds de solidarité de la branche, conclu dans le cadre de la convention collective nationale de la librairie (n° 3013).<br/>
Article 2 : Sous réserve des actions contentieuses engagées à la date de la présente décision contre les actes pris sur son fondement, les effets produits, dans la mesure de l'annulation prononcée à l'article 1er, antérieurement au 12 mai 2018, par l'arrêté du 11 décembre 2015 sont regardés comme définitifs.<br/>
Article 3 : Les conclusions présentées par la Fédération française de l'assurance au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La Fédération française de l'assurance versera une somme 500 euros au Syndicat de la librairie française, à la SNPELAC CFTC, à la FEC FO, à la FS CFDT et à la CDS CGT au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à la Fédération française de l'assurance, à la ministre du travail, au Syndicat de la librairie française, premier dénommé, pour l'ensemble des défendeurs ayant présenté un mémoire commun avec ce syndicat, à la FNECS CFE-CGC,  à la FCCS CFE-CGC et à l'institution de prévoyance IPSEC.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
