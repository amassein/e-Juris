<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044213868</ID>
<ANCIEN_ID>JG_L_2021_10_000000436725</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/21/38/CETATEXT000044213868.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 15/10/2021, 436725</TITRE>
<DATE_DEC>2021-10-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436725</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH ; CARBONNIER</AVOCATS>
<RAPPORTEUR>Mme Flavie Le Tallec</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:436725.20211015</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              La société 2AB a demandé au tribunal administratif de Montpellier de condamner la commune de Pézenas à lui verser la somme de 340 023,82 euros en réparation des préjudices qu'elle estime avoir subis du fait de la fermeture des établissements commerciaux exploités dans l'immeuble dont elle est propriétaire. Par un jugement n° 1601307 du 17 octobre 2017, le tribunal administratif de Montpellier a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17MA04569 du 14 octobre 2019, la cour administrative d'appel de Marseille a, sur appel de la société 2AB, annulé ce jugement et condamné la commune de Pézenas à lui verser la somme de 56 685 euros.<br/>
<br/>
<br/>
              1° Sous le n° 436725, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 16 décembre 2019 et 16 mars 2020 au secrétariat du contentieux du Conseil d'Etat, la société 2AB demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il rejette le surplus de ses conclusions ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Pézenas la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 436746, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 16 décembre 2019 et 11 mars 2020 au secrétariat du contentieux du Conseil d'Etat, la commune de Pézenas demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le même arrêt en tant qu'il l'a condamnée à verser une indemnité à la société 2AB ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société 2AB ;<br/>
<br/>
              3°) de mettre à la charge de la société 2AB la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code de la construction et de l'habitation ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Flavie Le Tallec, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de la société 2AB, et à Me Carbonnier, avocat de la commune de Pézenas ;<br/>
<br/>
<br/>
              Vu les notes en délibéré, présentées par la société 2AB, enregistrées le 14 octobre 2021 ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société 2AB est propriétaire d'un bâtiment situé à Pézenas (Hérault), qui était initialement destiné à abriter des bureaux, un hall d'exposition, un garage et un entrepôt. Ce bâtiment a été divisé en 2013 en trois locaux commerciaux, pris à bail par trois locataires. La société 2AB a déposé l'année suivante une demande de permis de construire visant à régulariser les travaux qu'elle avait effectués lors du changement d'affectation des locaux. A la suite d'un avis défavorable de la commission de sécurité territorialement compétente, le maire de Pézenas a ordonné la fermeture au public de deux des trois établissements commerciaux installés sur le site, " Couleur nature " et " Area gym ", par deux arrêtés du 24 septembre 2014. Après un nouvel avis de la commission de sécurité, le maire de Pézenas a toutefois délivré à la société 2AB, par un arrêté du 4 décembre 2014, le permis de construire demandé et a abrogé, par un arrêté du 5 décembre 2014, l'arrêté de fermeture visant l'établissement " Area gym ". La société 2AB ayant recherché devant le juge administratif la condamnation de la commune de Pézenas à l'indemniser des préjudices qu'elle estimait avoir subi du fait des deux arrêtés du 24 septembre 2014, la cour administrative d'appel de Marseille a, par un arrêt du 14 octobre 2019 rendu sur appel d'un jugement du tribunal administratif de Montpellier du 17 octobre 2017, condamné la commune de Pézenas à verser à la société 2AB la somme de 56 685 euros. Par deux pourvois qu'il y a lieu de joindre pour statuer par une seule décision, la commune de Pézenas demande l'annulation de cet arrêt et la société 2AB demande son annulation en tant qu'il ne fait pas droit à l'intégralité de sa demande indemnitaire.<br/>
<br/>
              Sur le pourvoi de la commune de Pézenas :<br/>
<br/>
              En ce qui concerne la responsabilité de la commune :<br/>
<br/>
              2. Aux termes de l'article R. 123-52 du code de la construction et de l'habitation : " Sans préjudice de l'exercice par les autorités de police de leurs pouvoirs généraux, la fermeture des établissements exploités en infraction aux dispositions du présent chapitre peut être ordonnée par le maire, ou par le représentant de l'Etat dans le département dans des conditions fixées aux articles R. 123-27 et R. 123-28./ La décision est prise par arrêté après avis de la commission de sécurité compétente./ L'arrêté fixe, le cas échéant, la nature des aménagements et travaux à réaliser ainsi que les délais d'exécution. " Aux termes de l'article R. 123-14 du même code : " Les établissements dans lesquels l'effectif du public n'atteint pas le chiffre fixé par le règlement de sécurité pour chaque type d'établissement sont assujettis à des dispositions particulières déterminées dans le règlement de sécurité./ Le maire, après consultation de la commission de sécurité compétente, peut faire procéder à des visites de contrôle dans les conditions fixées aux articles R. 123-45 et R. 123-48 à R. 123-50 afin de vérifier si les règles de sécurité sont respectées./ Lorsque ces établissements disposent de locaux d'hébergement pour le public, les travaux qui conduisent à leur création, à leur aménagement ou à leur modification ne peuvent être exécutés qu'après délivrance de l'autorisation prévue aux articles L. 111-8 et suivants et après avis de la commission de sécurité compétente. Ils sont par ailleurs soumis aux dispositions des articles R. 111-19-14 et R. 123-22 ainsi qu'aux articles R. 123-43 à R. 123-52. " Il résulte de ces dispositions que, sous réserve du cas des établissements disposant de locaux d'hébergement pour le public, les dispositions du code de la construction et de l'habitation relatives à la protection contre les risques d'incendie et de panique dans les établissements recevant du public, autres que celles des articles R. 123-45 et R. 123-48 à R. 123-50, ne sont pas applicables aux établissements dits de cinquième catégorie, dans lesquels l'effectif du public n'atteint pas le chiffre fixé par le règlement de sécurité. En particulier, lorsque l'obtention d'un permis de construire est nécessaire pour un de ces établissements, il peut être délivré sans consultation préalable de la commission de sécurité compétente.<br/>
<br/>
              3. Il s'ensuit que la cour administrative d'appel a exactement qualifié les faits qui lui étaient soumis en jugeant que, dès lors qu'il ressortait des pièces du dossier qui lui était soumis et n'était d'ailleurs pas contesté, que l'effectif du public accueilli dans chacun des deux établissements " Couleur nature " et " Area gym " les faisaient relever, l'un et l'autre, de la cinquième catégorie, leur ouverture au public n'était pas soumise à l'autorisation préalable prévue par les dispositions citées ci-dessus et que, par suite, leur fermeture ordonnée en raison de ce qu'ils n'étaient pas titulaires de cette autorisation était illégale et donc fautive et de nature à engager la responsabilité de la commune de Pézenas. A cet égard la cour a pu, sans erreur de droit, juger qu'était sans incidence sur la responsabilité de la commune la circonstance que les arrêtés en cause avaient été précédés d'un avis défavorable de la commission de sécurité. Enfin si, sur ce dernier point, la commune de Pézenas soutient que la cour a dénaturé les pièces du dossier en estimant que la commission de sécurité avait rendu un avis défavorable au vu d'éléments portés à sa connaissance par la commune, ce moyen, qui met en cause une considération que la cour a, à bon droit, regardée comme inopérante, est lui-même inopérant.<br/>
<br/>
              En ce qui concerne les préjudices indemnisables :<br/>
<br/>
              4. Il résulte des termes de l'arrêt attaqué que, pour condamner la commune de Pézenas à verser à la société 2AB une partie des sommes que cette dernière a elle-même été condamnée à payer à Mme B..., exploitante de la société " Couleur nature ", dans le cadre de la procédure engagée par celle-ci devant le juge judiciaire, la cour administrative d'appel a jugé que la fermeture au public de cet établissement, prononcée par l'arrêté du 24 septembre 2014, avait été la cause directe de la décision de Mme B... de cesser définitivement son activité et de solliciter la résiliation judiciaire de son bail. <br/>
<br/>
              5. Or il ressort des pièces du dossier soumis aux juges du fond et, notamment, des termes de l'arrêt du 27 juin 2017 par lequel la cour d'appel de Montpellier a prononcé la résiliation judiciaire du bail commercial conclu entre la société 2AB et Mme B..., que cette résiliation, prononcée aux torts du bailleur, avait pour motif que ce dernier avait méconnu son obligation de délivrance conforme en ne mettant pas à disposition de Mme B... un local en état de servir à l'usage qui avait été contractuellement stipulé. Par suite, en jugeant que les préjudices résultant, pour la société 2AB, de la résiliation du bail qui la liait à Mme B... présentaient un lien direct et certain avec la faute qu'avait commise la commune de Pézenas en ordonnant la fermeture de l'établissement " Couleur nature ", la cour administrative d'appel a inexactement qualifié les faits de l'espèce. <br/>
<br/>
              6. Il résulte de ce qui précède que la commune de Pézenas est fondée à demander l'annulation de l'arrêt attaqué en tant, d'une part, qu'il annule le jugement du tribunal administratif en tant que celui-ci rejette la demande indemnitaire de la société 2AB liée à la résiliation du bail qui la liait à Mme B... et, d'autre part, qu'il la condamne à indemniser, à hauteur de 56 685 euros, la société 2AB des préjudices ayant résulté, pour cette société, de la résiliation de ce bail. <br/>
<br/>
              Sur le pourvoi de la société 2AB :<br/>
<br/>
              En ce qui concerne les conclusions relatives au préjudice ayant résulté de la résiliation du bail de Mme B... :<br/>
<br/>
              7. Il résulte de ce qui est jugé au point 6 que la société 2AB n'est pas fondée à demander l'annulation de l'arrêt attaqué en tant que, par son article 4, il a rejeté le surplus de sa demande indemnitaire de première instance relatif à ce chef de préjudice.<br/>
<br/>
              En ce qui concerne les conclusions relatives aux autres préjudices :<br/>
<br/>
              8. En premier lieu, aux termes de l'article L. 761-1 du code de justice administrative : " Dans toutes les instances, le juge condamne la partie tenue aux dépens ou, à défaut, la partie perdante, à payer à l'autre partie la somme qu'il détermine, au titre des frais exposés et non compris dans les dépens (...) ". <br/>
<br/>
              9. Les frais de justice exposés devant le juge administratif en conséquence directe d'une faute de l'administration sont susceptibles d'être pris en compte dans le préjudice résultant de la faute imputable à celle-ci. Toutefois, lorsque l'intéressé avait qualité de partie à l'instance, la part de son préjudice correspondant à des frais non compris dans les dépens est réputée intégralement réparée par la décision que prend le juge dans l'instance en cause.<br/>
<br/>
              10. Il résulte des termes de l'arrêt attaqué que, pour rejeter la demande présentée par la société 2AB tendant à ce que la commune de Pézenas soit condamnée à l'indemniser du montant des frais d'avocat exposés dans le cadre d'instances engagées devant le juge administratif pour contester, en excès de pouvoir, l'arrêté du 24 septembre 2014 du maire de Pézenas, la cour administrative d'appel s'est fondée sur le fait que la société avait pu, dans les instances en question, légalement bénéficier des dispositions de l'article L. 761-1 du code de justice administrative. Il résulte de ce qui a été dit précédemment qu'elle n'a, ce faisant, pas commis d'erreur de droit.<br/>
<br/>
              11. En deuxième lieu, en jugeant que les frais d'architecte relatifs à la mise en conformité de l'accessibilité du bâtiment étaient sans lien avec le litige, la cour administrative d'appel a porté sur les faits de l'espèce une appréciation souveraine, exempte de dénaturation.<br/>
<br/>
              12. Enfin, en jugeant, pour rejeter la demande d'indemnisation du préjudice lié au départ du troisième locataire, la société AMV, dont le local n'avait pas été visé par l'arrêté du 24 septembre 2014 du maire de Pézenas, que la société 2AB se bornait à invoquer l'illégalité de cet arrêté et que celle-ci était sans lien avec la résiliation du bail commercial conclu avec la société AMV, la cour ne s'est pas méprise sur la portée des écritures de la requérante et a suffisamment motivé son arrêt. <br/>
<br/>
              13. Il résulte de tout ce qui précède que doivent être annulés l'article 1er de l'arrêt attaqué, en tant qu'il annule le jugement du tribunal administratif en tant que celui-ci rejette la demande indemnitaire, son article 2 condamnant la commune de Pézenas à verser à la société 2AB une indemnité de 56 685 euros en réparation du préjudice résultant de la résiliation du bail de Mme B... et, la commune de Pézenas ne pouvant en conséquence être regardée comme la partie perdante en appel, son article 3 qui met à la charge de la commune le paiement d'une somme au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              14. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative, dans la mesure de la cassation prononcée.<br/>
<br/>
              15. Il résulte de ce qui a été dit au point 6 que la société 2AB n'est pas fondée à soutenir que c'est à tort que, par son jugement du 17 octobre 2017, le tribunal administratif de Montpellier a rejeté sa demande de condamnation de la commune de Pézenas à l'indemniser du préjudice qu'elle a subi du fait de la résiliation du bail commercial conclu avec Mme B....<br/>
<br/>
              16. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société 2AB la somme de 5 000 euros à verser à la commune de Pézenas, sur le fondement de l'article L. 761-1 du code de justice administrative, au titre des frais exposés par la commune devant la cour administrative d'appel et dans le cadre des instances engagées devant le Conseil d'Etat. Les dispositions de l'article L. 761-1 font, en revanche, obstacle à ce qu'une somme soit mise, au même titre, à la charge de la commune de Pézenas. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'article 1er de l'arrêt de la cour administrative d'appel de Marseille du 14 octobre 2019, en tant qu'il annule le jugement du tribunal administratif en tant que celui-ci rejette la demande indemnitaire liée à la résiliation du bail de Mme B..., ainsi que les articles 2 et 3 du même arrêt sont annulés.<br/>
<br/>
Article 2 : Les conclusions du pourvoi de la société 2AB et son appel contre le jugement du 17 octobre 2017 sont rejetés.<br/>
<br/>
Article 3 : La société 2AB versera à la commune de Pézenas une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société 2AB et à la commune de Pézenas.<br/>
              Délibéré à l'issue de la séance du 27 septembre 2021 où siégeaient : M. Jacques-Henri Stahl, président adjoint de la section du contentieux, Présidant ; M. A... F..., M. Fabien Raynaud, présidents de chambre ; Mme L... J..., M. D... H..., M. K... C..., M. D... I..., M. Cyril Roger-Lacan, conseillers d'Etat et Mme Flavie Le Tallec, maître des requêtes en service extraordinaire-rapporteure. <br/>
<br/>
              Rendu le 15 octobre 2021.<br/>
                 Le Président : <br/>
                 Signé : M. Jacques-Henri Stahl<br/>
 		La rapporteure : <br/>
      Signé : Mme Flavie Le Tallec<br/>
                 La secrétaire :<br/>
                 Signé : Mme G... E...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-06-05-11 PROCÉDURE. - JUGEMENTS. - FRAIS ET DÉPENS. - REMBOURSEMENT DES FRAIS NON COMPRIS DANS LES DÉPENS. - FRAIS DE JUSTICE - 1) CARACTÈRE INDEMNISABLE - EXISTENCE, LORSQU'ILS SONT LA CONSÉQUENCE DIRECTE D'UNE FAUTE [RJ1] - 2) EXCEPTION - VICTIME AYANT LA QUALITÉ DE PARTIE À L'INSTANCE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-04-03-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. - RÉPARATION. - ÉVALUATION DU PRÉJUDICE. - PRÉJUDICE MATÉRIEL. - FRAIS DE JUSTICE - 1) CARACTÈRE INDEMNISABLE - EXISTENCE, LORSQU'ILS SONT LA CONSÉQUENCE DIRECTE D'UNE FAUTE [RJ1] - 2) EXCEPTION - VICTIME AYANT LA QUALITÉ DE PARTIE À L'INSTANCE [RJ2].
</SCT>
<ANA ID="9A"> 54-06-05-11 1) Les frais de justice exposés devant le juge administratif en conséquence directe d'une faute de l'administration sont susceptibles d'être pris en compte dans le préjudice résultant de la faute imputable à celle-ci.......2) Toutefois, lorsque l'intéressé avait qualité de partie à l'instance, la part de son préjudice correspondant à des frais non compris dans les dépens est réputée intégralement réparée par la décision que prend le juge dans l'instance en cause sur le fondement de l'article L. 761-1 du code de justice administrative (CJA).</ANA>
<ANA ID="9B"> 60-04-03-02 1) Les frais de justice exposés devant le juge administratif en conséquence directe d'une faute de l'administration sont susceptibles d'être pris en compte dans le préjudice résultant de la faute imputable à celle-ci.......2) Toutefois, lorsque l'intéressé avait qualité de partie à l'instance, la part de son préjudice correspondant à des frais non compris dans les dépens est réputée intégralement réparée par la décision que prend le juge dans l'instance en cause sur le fondement de l'article L. 761-1 du code de justice administrative (CJA).</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 16 avril 2012, M. et Mme Afeian, n° 311308, T. pp. 826-876-928-990-991-992....[RJ2] Ab. jur., sur ce point, CE, 16 avril 2012, M. et Mme Afeian, n° 311308, T. pp. 826-876-928-990-991-992.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
