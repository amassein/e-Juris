<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031537087</ID>
<ANCIEN_ID>JG_L_2015_11_000000381826</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/53/70/CETATEXT000031537087.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème SSJS, 27/11/2015, 381826, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-11-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>381826</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Stéphane Decubber</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:381826.20151127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête sommaire et deux mémoires complémentaires, enregistrés le 25 juin et les 25 et 29 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, la commune de Saint-Leu demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet née du silence gardé par le Premier ministre sur sa demande du 19 février 2014 tendant à l'abrogation du décret n° 2007-236 du 21 février 2007 portant création de la réserve naturelle nationale marine de La Réunion ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre d'abroger ce décret ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code de l'environnement ; <br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - le décret n° 2007-236 du 21 février 2007 ;<br/>
              - la décision du 19 décembre 2014 par laquelle le Conseil d'Etat, statuant au contentieux, n'a pas renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la commune de Saint-Leu ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Stéphane Decubber, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la commune de Saint-leu ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 332-1 du code de l'environnement : "  I. - Des parties du territoire d'une ou de plusieurs communes peuvent être classées en réserve naturelle lorsque la conservation de la faune, de la flore, du sol, des eaux, des gisements de minéraux et de fossiles et, en général, du milieu naturel présente une importance particulière ou qu'il convient de les soustraire à toute intervention artificielle susceptible de les dégrader. Le classement peut affecter le domaine public maritime et les eaux territoriales françaises. / (...) " ; qu'aux termes de l'article L. 332-3 du même code, dans sa rédaction applicable lors de l'édiction du décret susvisé du 21 février 2007 portant création de la réserve naturelle nationale marine de La Réunion : " L'acte de classement d'une réserve naturelle nationale peut soumettre à un régime particulier et, le cas échéant, interdire à l'intérieur de la réserve toute action susceptible de nuire au développement naturel de la faune et de la flore et, plus généralement, d'altérer le caractère de ladite réserve, notamment la chasse et la pêche, les activités agricoles, forestières et pastorales, industrielles, minières et commerciales, l'exécution de travaux publics ou privés, l'extraction de matériaux concessibles ou non, l'utilisation des eaux, la circulation du public, quel que soit le moyen employé, la divagation des animaux domestiques et le survol de la réserve " ; qu'aux termes de l'article L. 332-17 du même code : " A l'intérieur des périmètres de protection, des prescriptions peuvent soumettre à un régime particulier ou interdire toute action susceptible d'altérer le caractère ou de porter atteinte à la réserve naturelle. Les prescriptions concernent tout ou partie des actions énumérées à l'article L. 332-3 " ; <br/>
<br/>
              2. Considérant, en premier lieu, que le législateur a institué, à l'article L. 332-10 du code de l'environnement, une procédure spécifique encadrant le déclassement total ou partiel d'un territoire classé en réserve naturelle ; qu'il suit de là qu'une demande tendant à l'abrogation d'un décret de classement ne peut être regardée que comme tendant au déclassement d'un territoire classé en réserve naturelle ; que si l'administration peut proposer, le cas échéant, le déclassement d'une réserve dont les prescriptions ne se justifieraient plus, elle n'a l'obligation d'engager une telle procédure que dans le cas où le changement qui s'est produit dans les circonstances de fait a transformé les caractéristiques du site à un point tel qu'il a eu pour effet de retirer son fondement au classement initial ; que, par suite, la demande présentée par la commune de Saint-Leu au Premier ministre doit être regardée comme tendant au déclassement de la réserve naturelle nationale marine de la Réunion ; que la commune ne présente pas, à l'appui de sa demande, d'éléments attestant d'un changement de circonstances de fait tel que le classement initial aurait perdu son fondement ; que, dès lors, elle n'est pas fondée à soutenir que le refus du Premier ministre de prendre une décision de déclassement de la réserve naturelle nationale marine de La Réunion serait entaché d'illégalité ;<br/>
<br/>
              3. Considérant, en second lieu et au demeurant, que si le décret du 21 février 2007 portant création de la réserve naturelle nationale marine de La Réunion a notamment pour objet d'interdire en principe toute atteinte aux animaux d'espèces non domestiques, d'interdire ou de réglementer la pêche professionnelle, de loisir et la pêche sous-marine et d'encadrer la circulation humaine et de véhicules nautiques, des autorisations dérogeant à l'ensemble de ces mesures peuvent être délivrées par le préfet en vue d'assurer la gestion de la réserve ; qu'en vertu du 2° de l'article 2 du décret précité, le préfet peut également prendre toutes mesures pour limiter les espèces surabondantes ou éliminer les espèces envahissantes ; qu'ainsi, le préfet peut notamment autoriser, à des fins de protection de la sécurité des personnes, des mesures de prélèvement des espèces dangereuses ; qu'il n'est, par ailleurs, pas établi que les mesures de protection prévues par le décret du 21 février 2007 favoriseraient, par elles-mêmes, la prolifération et la sédimentation des requins à l'abord des côtes de l'île de La Réunion ; que, par suite, la commune requérante n'est, en tout état de cause, pas fondée à soutenir que le décret litigieux dont elle a demandé l'abrogation porterait par lui-même une atteinte excessive à la sécurité publique et que pour ce motif la décision implicite attaquée serait entachée d'une erreur d'appréciation et méconnaîtrait l'article 16-1 de la loi du 12 avril 2000 relative aux relations des citoyens avec les administrations et le droit à la vie, garanti par l'article 2 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              4. Considérant qu'il résulte de tout de qui précède que la commune de Saint-Leu n'est pas fondée à demander l'annulation de la décision qu'elle attaque ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la commune de Saint-Leu est rejetée. <br/>
Article 2 : La présente décision sera notifiée à la commune de Saint-Leu, à la ministre de l'écologie, du développement durable et de l'énergie et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
