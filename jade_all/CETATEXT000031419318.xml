<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031419318</ID>
<ANCIEN_ID>JG_L_2015_11_000000366763</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/41/93/CETATEXT000031419318.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème SSJS, 02/11/2015, 366763, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-11-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366763</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Etienne de Lageneste</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:366763.20151102</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société civile immobilière Pantin Bobigny (IPB) a demandé au tribunal administratif de Montreuil de prononcer la réduction des cotisations de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre des années 2009 et 2010 à raison d'un ensemble immobilier dénommé "le Citrail" dont elle est propriétaire 110 B avenue du Général Leclerc à Pantin (93). Par un jugement avant-dire-droit n° 1107015 du 10 janvier 2013, le tribunal administratif a décidé de procéder à une mesure d'instruction. <br/>
<br/>
              Par un jugement au fond n° 1107015 du 5 juillet 2013, le tribunal administratif a prononcé la décharge des cotisations de taxe foncière sur les propriétés bâties auxquelles la société a été assujettie au titre des années 2009 et 2010 à concurrence de la réduction de sa base imposable résultant de la différence entre la valeur locative de 14,64 euros/m2 pour certains locaux évalués distinctement et celle de 13,42 euros par m2 pour certains locaux évalués distinctement et a rejeté le surplus des conclusions de sa demande.<br/>
<br/>
              1° Sous le n° 366763, par un pourvoi et deux nouveaux mémoires, enregistrés les 12 mars 2013 et les 3 et 30 juillet 2015, la société IPB demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement avant-dire-droit du 10 janvier 2013 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le numéro n° 371901, par un pourvoi sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 4 septembre et 3 décembre 2013 et les 3 mars et 9 juillet 2015, la société IPB demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du 5 juillet 2013 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Etienne de Lageneste, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société civile immobilière Pantin Bobigny (IPB) ;<br/>
<br/>
<br/>
<br/>1. Considérant que les pourvois de la société IPB visés ci-dessus ont trait au même litige ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              Sur les moyens dirigés contre le jugement du 10 janvier 2013 :<br/>
<br/>
              2. Considérant qu'alors que l'administration proposait, pour évaluer l'ensemble immobilier de la société IPB situé dans la commune de Pantin, de retenir comme terme de comparaison le local-type n° 80 du procès-verbal des opérations de révision foncière de la commune d'Aulnay-sous-Bois, la société IPB soutenait notamment que ces deux communes ne présentaient pas une situation économique analogue ; que le tribunal administratif ne s'est pas prononcé sur ce moyen qui, contrairement à ce que soutient le ministre des finances et des comptes publics, ne peut être regardé comme inopérant au motif que ce local-type aurait finalement été écarté par le tribunal administratif dans son jugement au fond du 5 juillet 2013 ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société IPB est fondée à demander l'annulation du jugement avant-dire-droit du 10 janvier 2013 du tribunal administratif de Montreuil ;<br/>
<br/>
              Sur les moyens dirigés contre le jugement du 5 juillet 2013 :<br/>
<br/>
              3. Considérant qu'aux termes de l'article 1498 du code général des impôts : " La valeur locative de tous les biens autres que les locaux visés au I de l'article 1496 et que les établissements industriels visés à l'article 1499 est déterminée au moyen de l'une des méthodes indiquées ci-après : / 1° Pour les biens donnés en location à des conditions de prix normales, la valeur locative est celle qui ressort de cette location ; / 2° a. Pour les biens loués à des conditions de prix anormales ou occupés par leur propriétaire (...), la valeur locative est déterminée par comparaison. / Les termes de comparaison sont choisis dans la commune. Ils peuvent être choisis hors de la commune pour procéder à l'évaluation des immeubles d'un caractère particulier ou exceptionnel ; / b. La valeur locative des termes de comparaison est arrêtée : / Soit en partant du bail en cours à la date de référence de la révision lorsque l'immeuble type était loué normalement à cette date, / Soit, dans le cas contraire, par comparaison avec des immeubles similaires situés dans la commune ou dans une localité présentant, du point de vue économique, une situation analogue à celle de la commune en cause et qui faisaient l'objet à cette date de locations consenties à des conditions de prix normales ; / 3° A défaut de ces bases, la valeur locative est déterminée par voie d'appréciation directe " ; qu'aux termes de l'article 324 Z de l'annexe III au même code : " I. L'évaluation par comparaison consiste à attribuer à un immeuble ou à un local donné une valeur locative proportionnelle à celle qui a été adoptée pour d'autres biens de même nature pris comme types. II. Les types dont il s'agit doivent correspondre aux catégories dans lesquelles peuvent être rangés les biens de la commune visés aux articles 234 Y à 324 AC, au regard de l'affectation, de la situation, de la nature de la construction, de son importance, de son état d'entretien et de son aménagement. / Ils sont inscrits au procès-verbal des opérations de la révision " ; qu'aux termes de l'article 324 AA de l'annexe III au même code : " La valeur locative cadastrale des biens loués à des conditions anormales ou occupés par leur propriétaire (...) est obtenue en appliquant aux données relatives à leur consistance telles que superficie réelle, nombre d'éléments, les valeurs unitaires arrêtées pour le type de la catégorie. Cette valeur est ensuite ajustée pour tenir compte des différences qui peuvent exister entre le type considéré et l'immeuble à évaluer, notamment du point de vue de la situation, de la nature de la construction, de son état d'entretien, de son aménagement ainsi que de l'importance plus ou moins grande de ses dépendances bâties et non bâties, si ces éléments n'ont pas été pris en considération lors de l'appréciation de la consistance " ;<br/>
<br/>
              4. Considérant qu'il résulte de ces dispositions que la valeur locative d'un immeuble retenu comme local-type dans un procès-verbal des opérations de révision des évaluations foncières d'une commune peut être déterminée par comparaison avec celle d'un autre local-type situé dans la commune ou dans une localité présentant, du point de vue économique, une situation analogue à celle de la commune en cause en appliquant le coefficient prévu à l'article 324 AA de l'annexe III au code général des impôts afin de tenir compte des différences entre l'immeuble concerné et le terme de comparaison ; qu'en se fondant uniquement, pour juger qu'un local-type figurant sur le procès-verbal des opérations de révision des évaluations foncières de la commune de Créteil ne pouvait être retenu comme terme de comparaison, sur la circonstance que sa valeur locative avait été déterminée en appliquant une majoration de 20 % à la valeur locative d'un autre local-type figurant sur le procès-verbal des opérations de révision des évaluations foncières de la commune de Charenton-le-Pont, le tribunal administratif a commis une erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société IPB est fondée, en tout état de cause, à demander l'annulation du jugement du 5 juillet 2013 du tribunal administratif de Montreuil ; <br/>
<br/>
              5.  Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la société IPB au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                          --------------<br/>
<br/>
Article 1er : Les jugements du 10 janvier 2013 et du 5 juillet 2013 du tribunal administratif de Montreuil sont annulés.<br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Montreuil.<br/>
<br/>
Article 3 : L'Etat versera une somme de 3 000 euros à la société IPB au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société civile immobilière Pantin Bobigny et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
