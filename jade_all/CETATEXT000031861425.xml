<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031861425</ID>
<ANCIEN_ID>JG_L_2016_01_000000377112</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/86/14/CETATEXT000031861425.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème SSJS, 06/01/2016, 377112, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-01-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>377112</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE CHAISEMARTIN, COURJON ; SCP BORE, SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>Mme Emmanuelle Petitdemange</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:377112.20160106</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>       Vu la procédure suivante :<br/>
<br/>
              	Mme B...A...a demandé au tribunal administratif de Lille que soit reconnue la responsabilité du grand port maritime de Dunkerque dans la chute dont elle a été victime, qu'un expert médical soit désigné afin d'évaluer le préjudice et que lui soit allouée une indemnité provisionnelle de 3 000 euros. Par un jugement n° 1005240 du 24 avril 2012, le tribunal administratif a rejeté la demande.<br/>
<br/>
              	Par un arrêt n° 13DA00311 du 4 février 2014, la cour administrative d'appel de Douai a rejeté l'appel formé par Mme A...contre ce jugement. <br/>
<br/>
              	Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 4 avril et 27 juin 2014 et le 16 janvier 2015 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              	1°) d'annuler cet arrêt ;<br/>
<br/>
              	2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              	3°) de mettre à la charge du grand port maritime de Dunkerque la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Emmanuelle Petitdemange, auditeur,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Chaisemartin, Courjon, avocat de Mme A...et à la SCP Boré, Salve de Bruneton, avocat du  Grand port maritime de Dunkerque ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que, le 4 février 2008 vers 23 heures, Mme A...a été victime d'une chute au niveau d'un regard d'égout dont il manquait la plaque de protection, alors qu'elle circulait à pied sur le domaine public du grand port maritime de Dunkerque ; que la cour administrative d'appel de Douai a relevé qu'après avoir stationné leur véhicule sur une place de parking aménagée située à proximité immédiate du bâtiment abritant l'Hôtel des technologies, M. et Mme A...ont contourné par le quai un bâtiment désaffecté, en suivant un cheminement non éclairé et non balisé, plutôt que d'emprunter la voie d'accès au parking qui était éclairée ; que la cour a également relevé que les dangers potentiels de la zone portuaire étaient  signalés par un panneau situé à l'entrée du port ; qu'elle a enfin relevé que la zone où a eu lieu l'accident appartenait à la catégorie des " ouvrages et terres pleins de service " dont l'accès était interdit à toutes personnes n'ayant pas de motif d'y pénétrer pour l'exécution des travaux ou pour les besoins de l'activité et de l'exploitation du port ; qu'il ressort des pièces du dossier soumis au juge du fond que cette interdiction, qui figurait à l'article 28 du règlement particulier de police du port, n'était toutefois pas rappelée sur les lieux, qui, ainsi que l'a mentionné la cour, étaient libres d'accès ; que, par ailleurs, il ressort de ces mêmes pièces que Mme A...a été exposée à un danger qui n'était pas signalé par le port et qui n'était pas prévisible ;  que, par suite, en se fondant sur les éléments rappelés ci-dessus pour juger que Mme A...avait commis une imprudence de nature à exonérer en totalité le grand port maritime de Dunkerque de sa responsabilité au titre du dommage de travaux publics, la cour a inexactement qualifié les faits qui lui étaient soumis ; que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, Mme A...est fondée à demander l'annulation de l'arrêt qu'elle attaque ; <br/>
<br/>
              2. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de Mme A...qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstance de l'espèce, de mettre à la charge du grand port maritime de Dunkerque le versement au même titre à Mme A... de la somme de 3 000 euros ;<br/>
<br/>
<br/>
<br/>            D E C I D E :<br/>
                         --------------<br/>
<br/>
Article 1er : L'arrêt du 4 février 2014 de la cour administrative d'appel de Douai est annulé. <br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Douai.<br/>
<br/>
Article 3 : Le grand port maritime de Dunkerque versera à Mme A...la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées par le grand port maritime de Dunkerque au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à Mme B...A...et au grand port maritime de Dunkerque. <br/>
Copie en sera adressée, pour information, à la caisse primaire d'assurance maladie des Flandres.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
