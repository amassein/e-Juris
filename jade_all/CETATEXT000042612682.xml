<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042612682</ID>
<ANCIEN_ID>JG_L_2020_12_000000419361</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/61/26/CETATEXT000042612682.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 03/12/2020, 419361</TITRE>
<DATE_DEC>2020-12-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419361</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI, REBEYROL ; SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>Mme Yaël Treille</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:419361.20201203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et deux mémoires en réplique, enregistrés les 28 mars et 28 juin 2018 et les 29 mai et 12 juin 2020 au secrétariat du contentieux du Conseil d'Etat, le syndicat national de la plasturgie, des composites, des bioplastiques et de la fabrication additive (Plastalliance) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté de la ministre du travail du 15 février 2018 portant extension de l'avenant n°1 à l'accord du 20 juin 2012 relatif au financement et au fonctionnement du paritarisme conclu dans le cadre de la convention collective nationale de la plasturgie ;<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ;<br/>
              - la convention internationale du travail n° 87 de 1948 concernant la liberté syndicale et la protection du droit syndical adoptée à San Francisco lors de la trente-et-unième session de la conférence internationale du travail, ratifiée le 28 juin 1951 ;<br/>
              - la convention internationale du travail n° 98 de 1949 concernant l'application des principes du droit d'organisation et de négociation collective adoptée à Genève lors de la trente-deuxième session de la conférence internationale du travail, ratifiée le 26 octobre 1951 ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, notamment son protocole n°12 ;<br/>
              - la charte sociale européenne (révisée), signée à Strasbourg le 3 mai 1996 ;<br/>
              - la Charte des droits fondamentaux de l'Union européenne ;<br/>
              - le code du travail ;<br/>
              - la loi n° 2016-1088 du 8 août 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... B..., auditrice,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, Rebeyrol, avocat du syndicat national de la plasturgie, des composites, des bioplastiques et de la fabrication additive et à la SCP Thouvenin, Coudray, Grévy, avocat de la fédération chimie énergie CFDT ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier que la fédération de la plasturgie et des composites a conclu le 26 avril 2017 avec cinq organisations représentatives de salariés un avenant à l'accord du 20 juin 2012 relatif au financement et au fonctionnement du paritarisme, conclu dans le cadre de la convention collective nationale de la plasturgie. Cet avenant prévoit en particulier, en son article 2.1, une modification de l'article 6 de cet accord relatif à la composition des délégations syndicales et patronales aux réunions paritaires au sein de la branche. Le syndicat national de la plasturgie, des composites, des bioplastiques et de la fabrication additive (Plastalliance), qui est une organisation professionnelle d'employeurs, demande l'annulation pour excès de pouvoir de l'arrêté du 15 février 2018 portant extension de cet avenant.<br/>
<br/>
              2. En premier lieu, il ressort des pièces du dossier que, contrairement à ce que soutient Plastalliance, l'arrêté attaqué est revêtu de la signature du directeur général du travail. Par suite, le moyen tiré de ce que l'arrêté attaqué ne comporterait pas la signature de son auteur manque en fait.<br/>
<br/>
              3. En deuxième lieu, aux termes de l'article L. 2232-9 du code du travail, dans sa rédaction antérieure à la loi du 8 août 2016 relative au travail, à la modernisation du dialogue social et à la sécurisation des parcours professionnels : " Les conventions de branche et les accords professionnels instituent des commissions paritaires d'interprétation ". Aux termes de l'article L. 2261-19 du code du travail, dans sa rédaction antérieure à cette même loi : " Pour pouvoir être étendus, la convention de branche ou l'accord professionnel ou interprofessionnel, leurs avenants ou annexes, doivent avoir été négociés et conclus en commission paritaire. / Cette commission est composée de représentants des organisations syndicales d'employeurs et de salariés représentatives dans le champ d'application considéré. (...) ".<br/>
<br/>
              4.  Aux termes de l'article L. 2232-9 du code du travail, dans sa rédaction applicable au litige, issue de la loi du 8 août 2016 relative au travail, à la modernisation du dialogue social et à la sécurisation des parcours professionnels : " I. Une commission paritaire permanente de négociation et d'interprétation est mise en place par accord ou convention dans chaque branche. / II. La commission paritaire exerce les missions d'intérêt général suivantes : / 1° Elle représente la branche, notamment dans l'appui aux entreprises et vis-à-vis des pouvoirs publics ; / 2° Elle exerce un rôle de veille sur les conditions de travail et l'emploi ; / 3° Elle établit un rapport annuel d'activité (...) / Elle peut rendre un avis à la demande d'une juridiction sur l'interprétation d'une convention ou d'un accord collectif dans les conditions mentionnées à l'article L. 441-1 du code de l'organisation judiciaire. / Elle peut également exercer les missions de l'observatoire paritaire mentionné à l'article L. 2232-10 du présent code. / Un décret définit les conditions dans lesquelles les conventions et accords d'entreprise conclus dans le cadre du titre II, des chapitres Ier et III du titre III et des titres IV et V du livre Ier de la troisième partie du présent code sont transmis aux commissions mentionnées au I du présent article (...) ". Aux termes de l'article L. 2261-19 du code du travail, dans sa rédaction issue de cette même loi : " Pour pouvoir être étendus, la convention de branche ou l'accord professionnel ou interprofessionnel, leurs avenants ou annexes, doivent avoir été négociés et conclus au sein de la commission paritaire mentionnée à l'article L. 2232-9. / Cette commission est composée de représentants des organisations syndicales d'employeurs et de salariés représentatives dans le champ d'application considéré (...) ". <br/>
<br/>
              5. Il résulte des dispositions citées au point 4 qu'en l'absence de mesures transitoires prévues par la loi du 8 août 2016 relative au travail, à la modernisation du dialogue social et à la sécurisation des parcours professionnels concernant ces dispositions, le législateur a entendu laisser un délai raisonnable aux organisations syndicales de salariés et aux organisations professionnelles d'employeurs représentatives au sein de chaque branche pour négocier un accord en vue de mettre en place la commission paritaire permanente de négociation et d'interprétation. Lorsqu'une commission paritaire permanente de négociation et d'interprétation a été mise en place dans une branche, seul un accord de branche, ou un avenant à celui-ci, négocié et conclu au sein de cette commission peut être étendu. En revanche, lorsqu'une telle commission n'a pas encore été mise en place au sein de la branche et que le délai raisonnable laissé aux organisations syndicales de salariés et aux organisations professionnelles d'employeurs représentatives au sein de chaque branche pour mettre en place cette commission n'est pas écoulé, l'extension d'un accord de branche n'est pas subordonnée à la consultation de celle-ci et demeure subordonnée aux dispositions applicables antérieurement à la loi du 8 août 2016 et citées au point 3. <br/>
<br/>
              6. Si le syndicat requérant soutient que l'arrêté attaqué est entaché d'illégalité, en ce qu'il procède à l'extension d'un avenant qui n'a pas été négocié et conclu au sein d'une commission paritaire permanente de négociation et d'interprétation, il ressort des pièces du dossier que la commission paritaire permanente de négociation et d'interprétation n'avait pas été mise en place au sein de la branche de la plasturgie à la date de signature de l'avenant litigieux le 26 avril 2017. En outre, il ne ressort pas des pièces du dossier que le délai raisonnable pour mettre en place cette commission était à cette date écoulé et il n'est pas contesté que cet avenant a été négocié et conclu au sein des instances paritaires existantes de cette branche. Par suite, ce moyen doit être écarté. <br/>
<br/>
              7. En troisième lieu, Plastalliance soutient que la circonstance qu'il n'a pas été invité à négocier l'avenant du 26 avril 2017, alors qu'il est une organisation professionnelle d'employeurs représentative au niveau de la branche, entache d'illégalité l'arrêté attaqué dès lors qu'il étend un avenant qui n'a pas été négocié par l'ensemble des organisations professionnelles d'employeurs représentatives dans son champ d'application. Toutefois, le requérant se borne, au soutien de ce moyen, à se prévaloir de son audience au sein de la branche de la plasturgie à la date de la conclusion de cet avenant, alors que l'audience ne constitue que l'un des critères au regard desquels est appréciée la représentativité d'une organisation d'employeurs dans une branche, ainsi que du fait qu'il figure dans la liste des organisations professionnelles d'employeurs reconnues représentatives dans la convention collective nationale de la plasturgie dressée, postérieurement à l'avenant litigieux, par l'arrêté du ministre du travail du 29 novembre 2017. Ces seuls éléments ne suffisent pas à établir que ce syndicat aurait dû être regardé comme représentatif dans le champ de la branche de la plasturgie et à ce titre être invité à négocier l'avenant en litige. Il suit de là que le moyen tiré de ce que l'arrêté attaqué serait entaché d'illégalité au motif que l'avenant qu'il étend aurait été négocié et conclu sans que l'ensemble des organisations professionnelles d'employeurs représentatives dans la branche professionnelle de la plasturgie aient été invitées à sa négociation doit être écarté.<br/>
<br/>
              8. En quatrième lieu, l'article 2.1 de l'avenant étendu par l'arrêté attaqué modifie l'article 6 de l'accord du 20 juin 2012 en y insérant notamment un article 6.2.1 relatif au nombre de représentants qui stipule que " Le nombre de représentants de la délégation patronale est égal à la somme des représentants de l'ensemble des organisations syndicales de salariés représentatives dans la branche ", un article 6.2.2 qui stipule que " La répartition des postes au sein de la délégation patronale entre organisations professionnelles d'employeurs représentatives se fait selon les principes suivants : / - prise en compte de l'audience de chaque organisation reconnue comme représentative mesurée à partir des effectifs des salariés de ses entreprises adhérentes ; / - attribution de la moitié des postes (...) à l'organisation qui a la meilleure audience puis répartition des postes restant entre toutes les organisations représentatives à la représentation proportionnelle à la plus forte moyenne ; (...) / - dans le cas où une organisation reconnue comme représentative ne pourrait prétendre à l'obtention d'un poste en vertu de ce mode de calcul, il lui sera attribué un poste " et un article 6.2.3, relatif aux règles de vote et de décision au niveau de la délégation au sein des instances paritaires de la branche plasturgie aux termes duquel  : " Les décisions sont prises au sein entre les organisations professionnelles d'employeurs représentatives en fonction de l'audience (calculée sur le pourcentage des salariés des entreprises adhérentes) de chacune(s) et en application des règles relatives à la négociation au niveau de la branche, y compris la règle prévue à l'article L. 2261-19 du code du travail (...) ".  L'article 1er de l'arrêté attaqué dispose que les premiers et deuxième alinéas de l'article 6-2-3 tels qu'ils résultent de l'article 2-1 de l'avenant sont étendus sous réserve du respect des règles de négociation et de validité des conventions ou accords de branche ou professionnels prévues notamment aux articles L. 2231-1, L. 2232-6 et L. 2261-19 du code du travail. <br/>
<br/>
              9. D'une part, au regard de la différence de situation entre organisations professionnelles d'employeurs et organisations syndicales, la circonstance que les stipulations citées au point précédent prennent en compte, pour déterminer la composition de la délégation patronale, l'audience des organisations professionnelles d'employeurs représentatives, alors que les règles équivalentes pour la délégation syndicale, fixées à l'article 6.1 de l'accord du 20 juin 2012 tel que modifié par l'avenant du 26 avril 2017, ne reposent pas sur un tel critère, ne saurait entacher l'avenant étendu par l'arrêté attaqué d'une méconnaissance du principe d'égalité. Par suite, le moyen tiré de la méconnaissance du principe d'égalité garanti entre autres par la Constitution ne soulève pas une contestation sérieuse et ne peut qu'être écarté. <br/>
<br/>
              10. D'autre part, si Plastalliance soutient que les stipulations citées au point 8, dès lors qu'elles reposent sur un critère d'audience, sont de nature à dissuader les entreprises d'adhérer à une organisation professionnelle d'employeurs minoritaire, cette seule circonstance, à la supposer avérée, n'est pas de nature à entacher, par elle-même, ces stipulations de méconnaissance de la liberté syndicale garantie par la Constitution. <br/>
<br/>
              11. En cinquième lieu, le moyen tiré de ce que les stipulations de l'article 6.2.3 citées au point 8 fixant des règles de décision au sein de la délégation patronale qui seraient contraires aux règles présidant à la négociation collective, l'arrêté attaqué serait illégal en ce qu'il procède à leur extension ne peut qu'être écarté, dès lors que l'article 1er de cet arrêté précise qu'il procède à cette extension sous la réserve du respect des règles de négociation et de validité des conventions ou accords de branche ou professionnels prévues notamment aux articles L. 2231-1, L. 2232-6 et L. 2261-19 du code du travail. <br/>
<br/>
              12. En sixième lieu, en se bornant à invoquer des considérations d'intérêt général tirés de la " loyauté " et du " bon développement de la négociation collective et du dialogue social ", qui selon lui s'opposeraient à l'extension de l'avenant en litige, le syndicat requérant n'établit pas que l'arrêté attaqué serait entaché d'une erreur manifeste d'appréciation. <br/>
<br/>
              13. En septième et dernier lieu, aucun des autres moyens soulevés par le requérant n'est assorti des précisions nécessaires permettant d'en apprécier le bien-fondé.<br/>
<br/>
              14. Il résulte de tout ce qui précède que Plastalliance n'est pas fondé à demander l'annulation de l'arrêté attaqué.<br/>
<br/>
              15. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de Plastalliance une somme de 3 000 euros à verser à la fédération chimie énergie CFDT au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête du syndicat national de la plasturgie, des composites, des bioplastiques et de la fabrication additive (Plastalliance) est rejetée.<br/>
Article 2 : Le syndicat national de la plasturgie, des composites, des bioplastiques et de la fabrication additive versera à la fédération chimie énergie CFDT une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au syndicat national de la plasturgie, des composites, des bioplastiques et de la fabrication additive, à la fédération chimie énergie CFDT et à la ministre du travail, de l'emploi et de l'insertion.<br/>
Copie sera adressée à la fédération nationale de la chimie CGT-FO, à la fédération nationale du personnel d'encadrement de la chimie CFE-CGC, à la fédération nationale des industries chimiques CGT, à la fédération chimie, mines, textile, énergie CFTC, à la fédération de la plasturgie et des composites <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-08-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. APPLICATION DANS LE TEMPS. TEXTE APPLICABLE. - DISPOSITIONS RELATIVES À L'EXTENSION DES CONVENTIONS COLLECTIVES (ART. L. 2232-9 ET L. 2261-19 DU CODE DU TRAVAIL) - LOI DU 8 AOÛT 2016 PRÉVOYANT, SANS MESURES TRANSITOIRES, L'INTERVENTION DE LA CPPNI - 1) DÉLAI RAISONNABLE EN VUE DE LA MISE EN PLACE DE CETTE COMMISSION - EXISTENCE - 2) CONSÉQUENCE - PROCÉDURE APPLICABLE - A) EXISTENCE DE CETTE COMMISSION - PROCÉDURE ISSUE DE LA LOI DU 8 AOÛT 2016 - B) ABSENCE DE CETTE COMMISSION - PROCÉDURE ANTÉRIEURE, TANT QUE LE DÉLAI RAISONNABLE N'EST PAS ÉCOULÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-02-02-01 TRAVAIL ET EMPLOI. CONVENTIONS COLLECTIVES. EXTENSION DES CONVENTIONS COLLECTIVES. PROCÉDURE D'EXTENSION. - LOI DU 8 AOÛT 2016 PRÉVOYANT, SANS MESURES TRANSITOIRES, L'INTERVENTION DE LA CPPNI - 1) DÉLAI RAISONNABLE EN VUE DE LA MISE EN PLACE DE CETTE COMMISSION - EXISTENCE - 2) CONSÉQUENCE - PROCÉDURE APPLICABLE - A) EXISTENCE DE CETTE COMMISSION - PROCÉDURE ISSUE DE LA LOI DU 8 AOÛT 2016 - B) ABSENCE DE CETTE COMMISSION - PROCÉDURE ANTÉRIEURE, TANT QUE LE DÉLAI RAISONNABLE N'EST PAS ÉCOULÉ.
</SCT>
<ANA ID="9A"> 01-08-03 1) Il résulte des articles L. 2232-9 et L. 2261-19 du code du travail, dans leur rédaction issue de la loi n° 2016-1088 du 8 août 2016, qu'en l'absence de mesures transitoires prévues par cette loi, le législateur a entendu laisser un délai raisonnable aux organisations syndicales de salariés et aux organisations professionnelles d'employeurs représentatives au sein de chaque branche pour négocier un accord en vue de mettre en place la commission paritaire permanente de négociation et d'interprétation (CPPNI).... ,,2) a) Lorsqu'une CPPNI a été mise en place dans une branche, seul un accord de branche, ou un avenant à celui-ci, négocié et conclu au sein de cette commission peut être étendu.... ,,b) En revanche, lorsqu'une telle commission n'a pas encore été mise en place au sein de la branche et que le délai raisonnable laissé aux organisations syndicales de salariés et aux organisations professionnelles d'employeurs représentatives au sein de chaque branche pour mettre en place cette commission n'est pas écoulé, l'extension d'un accord de branche n'est pas subordonnée à la consultation de celle-ci et demeure subordonnée aux dispositions des mêmes articles applicables antérieurement à la loi du 8 août 2016.</ANA>
<ANA ID="9B"> 66-02-02-01 1) Il résulte des articles L. 2232-9 et L. 2261-19 du code du travail, dans leur rédaction issue de la loi n° 2016-1088 du 8 août 2016, qu'en l'absence de mesures transitoires prévues par cette loi, le législateur a entendu laisser un délai raisonnable aux organisations syndicales de salariés et aux organisations professionnelles d'employeurs représentatives au sein de chaque branche pour négocier un accord en vue de mettre en place la commission paritaire permanente de négociation et d'interprétation (CPPNI).... ,,2) a) Lorsqu'une CPPNI a été mise en place dans une branche, seul un accord de branche, ou un avenant à celui-ci, négocié et conclu au sein de cette commission peut être étendu.... ,,b) En revanche, lorsqu'une telle commission n'a pas encore été mise en place au sein de la branche et que le délai raisonnable laissé aux organisations syndicales de salariés et aux organisations professionnelles d'employeurs représentatives au sein de chaque branche pour mettre en place cette commission n'est pas écoulé, l'extension d'un accord de branche n'est pas subordonnée à la consultation de celle-ci et demeure subordonnée aux dispositions des mêmes articles applicables antérieurement à la loi du 8 août 2016.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
