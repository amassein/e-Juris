<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030926078</ID>
<ANCIEN_ID>JG_L_2015_07_000000376212</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/92/60/CETATEXT000030926078.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème SSJS, 22/07/2015, 376212, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>376212</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR>M. Patrick Quinqueton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:376212.20150722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Rennes de prononcer la restitution des droits de taxe sur la valeur ajoutée qu'il a acquittés au titre de la période allant du 1er janvier 2007 au 31 décembre 2008, en ordonnant, le cas échéant, une expertise. Par un jugement n° 1001300 du 18 octobre 2012, le tribunal administratif de Rennes a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 12NT03249 du 9 janvier 2014, la cour administrative d'appel de Nantes a rejeté sa requête. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 10 mars et 10 juin 2014 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat : <br/>
<br/>
               1°) d'annuler cet arrêt ;  <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 2000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la 6ème directive 77/388/CE du Conseil des communautés européennes du 17 mai 1977 relative au système commun de taxe sur la valeur ajoutée ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 2002-303 du 4 mars 2002 ;<br/>
              - le décret n° 2011-32 du 7 janvier 2011 ; <br/>
              - le décret n° 2011-1127 du 20 septembre 2011 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Patrick Quinqueton, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat de M. B...A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B...A...qui exerce l'activité de chiropracteur, a, par une réclamation du 24 décembre 2009, sollicité la restitution des droits de taxe sur la valeur ajoutée qu'il a spontanément acquittés au titre de la période du 1er janvier 2007 au 31 décembre 2008 pour un montant de 28 939 euros ; qu'il se pourvoit en cassation contre l'arrêt du 9 janvier 2014 par lequel la cour administrative d'appel de Nantes a rejeté sa requête tendant à l'annulation du jugement du 18 octobre 2012 du tribunal administratif de Rennes ayant rejeté sa demande de restitution ; <br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article R. 194-1 du livre des procédures fiscales : " Lorsque, ayant donné son accord à la rectification ou s'étant abstenu de répondre dans le délai légal à la proposition de rectification, le contribuable présente cependant une réclamation faisant suite à une procédure contradictoire de rectification, il peut obtenir la décharge ou la réduction de l'imposition, en démontrant son caractère exagéré. / Il en est de même lorsqu'une imposition a été établie d'après les bases indiquées dans la déclaration souscrite par un contribuable (...) " ; qu'il résulte de ces dispositions qu'un contribuable ne peut obtenir la restitution de droits de taxe sur la valeur ajoutée qu'il a déclarés et spontanément acquittés conformément à ses déclarations qu'à la condition d'établir le caractère non-fondé de cette taxation ; <br/>
<br/>
              3. Considérant, en deuxième lieu, qu'aux termes de l'article 13, A, paragraphe 1 de la sixième directive du Conseil du 17 mai 1977 en matière d'harmonisation des législations des Etats membres relatives aux taxes sur le chiffre d'affaires : " Sans préjudice d'autres dispositions communautaires, les Etats membres exonèrent, dans les conditions qu'ils fixent en vue d'assurer l'application correcte et simple des exonérations prévues ci-dessous et de prévenir toute fraude, évasion et abus éventuels : / (...) c) les prestations de soins à la personne effectuées dans le cadre de l'exercice des professions médicales et paramédicales telles qu'elles sont définies par l'Etat membre concerné (...) " ; qu'en vertu du 1° du 4 de l'article 261 du code général des impôts, dans sa rédaction applicable à la période d'imposition en litige, sont exonérés de la taxe sur la valeur ajoutée : " Les soins dispensés aux personnes par les membres des professions médicales et paramédicales réglementées (...) " ; qu'en limitant l'exonération qu'elles prévoient aux soins dispensés par les membres des professions médicales et paramédicales soumises à réglementation, ces dispositions ne méconnaissent pas l'objectif poursuivi par l'article 13, A, paragraphe 1, sous c) de la sixième directive, précité, qui est de garantir que l'exonération s'applique uniquement aux prestations de soins à la personne fournies par des prestataires possédant les qualifications professionnelles requises ; qu'en effet, la directive renvoie à la réglementation interne des Etats membres la définition de la notion de professions paramédicales, des qualifications requises pour exercer ces professions et des activités spécifiques de soins à la personne qui relèvent de telles professions ; que, toutefois, conformément à l'interprétation des dispositions de la sixième directive qui résulte de l'arrêt rendu le 27 avril 2006 par la Cour de justice des Communautés européennes dans les affaires C-443/04 et C-444/04, l'exclusion d'une profession ou d'une activité spécifique de soins à la personne de la définition des professions paramédicales retenue par la réglementation nationale aux fins de l'exonération de la taxe sur la valeur ajoutée prévue à l'article 13, A, paragraphe 1, sous c) de cette directive serait contraire au principe de neutralité fiscale inhérent au système commun de taxe sur la valeur ajoutée s'il pouvait être démontré que les personnes exerçant cette profession ou cette activité disposent, pour la fourniture de telles prestations de soins, de qualifications professionnelles propres à assurer à ces prestations un niveau de qualité équivalente à celles fournies par des personnes bénéficiant, en vertu de la réglementation nationale, de l'exonération ;<br/>
<br/>
              4. Considérant, en troisième lieu, qu'aux termes de l'article 75 de la loi du 4 mars 2002 relative aux droits des malades et à la qualité du système de santé, dans sa version applicable au présent litige : " L'usage professionnel du titre d'ostéopathe ou de chiropracteur est réservé aux personnes titulaires d'un diplôme sanctionnant une formation spécifique à l'ostéopathie ou à la chiropraxie délivrée par un établissement de formation agréé par le ministre chargé de la santé dans des conditions fixées par décret. Le programme et la durée des études préparatoires et des épreuves après lesquelles peut être délivré ce diplôme sont fixés par voie réglementaire. (...) / Les praticiens en exercice, à la date d'application de la présente loi, peuvent se voir reconnaître le titre d'ostéopathe ou de chiropracteur s'ils satisfont à des conditions de formation ou d'expérience professionnelle analogues à celles des titulaires du diplôme mentionné au premier alinéa. Ces conditions sont déterminées par décret. (...) / Un décret établit la liste des actes que les praticiens justifiant du titre d'ostéopathe ou de chiropracteur sont autorisés à effectuer, ainsi que les conditions dans lesquelles ils sont appelés à les accomplir (...) " ; que le décret du 7 janvier 2011 relatif aux actes et aux conditions d'exercice de la chiropraxie n'a été publié que le 9 janvier 2011 et que le décret du 20 septembre 2011 relatif à la formation des chiropracteurs et à l'agrément des établissements de formation en chiropraxie, ainsi que l'arrêté du même jour pris en application de ces deux décrets, n'ont été publiés que le 21 septembre 2011 ; que, durant la période allant du 1er janvier 2007 au 31 décembre 2008, les actes dits de chiropraxie ne pouvaient être pratiqués que par les docteurs en médecine et, le cas échéant, pour certains actes seulement et sur prescription médicale, par les autres professionnels de santé habilités à les réaliser ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que, pour statuer sur la restitution des droits de taxe sur la valeur ajoutée acquittés par M. A...sur ses prestations de chiropraxie au cours de cette période, la cour devait vérifier que celui-ci démontrait disposer, pour la fourniture de ces prestations, de qualifications professionnelles propres à leur assurer un niveau de qualité équivalente à celles fournies, selon le cas, par un médecin ou par un membre d'une profession de santé réglementée ; qu'une telle appréciation ne peut être portée qu'au vu de la nature des actes accomplis sous la dénomination d'actes de chiropraxie et, s'agissant des actes susceptibles de comporter des risques en cas de contre-indication médicale, en considération des conditions dans lesquelles ils ont été effectués ; qu'il appartenait, dès lors, à M.A..., pour mettre le juge à même de s'assurer que la condition tenant à la qualité des actes était remplie, de produire, d'une part, et sous réserve de l'occultation des noms des patients, des éléments relatifs à sa pratique permettant d'appréhender, sur une période significative, la nature des actes accomplis et les conditions dans lesquelles ils l'ont été et, d'autre part, tous éléments utiles relatifs à ses qualifications professionnelles ;<br/>
<br/>
              6. Considérant qu'après avoir estimé que M. A...devait être regardé comme ayant produit des éléments attestant de la qualité de la formation qu'il a suivie et du diplôme qu'il a obtenu, la cour a relevé que les copies de " fiches patients " avaient été éditées à une date postérieure à la période en cause,  que certaines mentionnaient des actes postérieurs à cette période et que les fiches étaient difficilement lisibles ; qu'elle en  a déduit qu'elles n'étaient pas de nature à établir que les actes de chiropraxie qu'il avait accomplis pouvaient être regardés comme d'une qualité équivalente à ceux qui, s'ils avaient été effectués par un médecin pratiquant la chiropraxie, auraient été exonérés de taxe sur la valeur ajoutée ; qu'en statuant ainsi, alors que ces 457 fiches, dont la date d'édition était indifférente, mentionnaient toutes des actes accomplis au cours de la période en litige et reflétaient les consultations assurées par M. A...par les mentions des signes symptomatiques de ses clients, de leurs antécédents médicaux, du résultat de ses examens physiques ainsi que des différents tests pratiqués, du diagnostic posé, des traitements " manipulatifs " effectués à des dates précisées et de leur évolution pathologique et alors qu'elles  étaient accompagnées d'une attestation d'un médecin spécialisé dans la chirurgie et la pathologie du rachis ayant suivi une formation en chiropraxie qui indiquait avoir pris connaissance de ces documents et de la pratique professionnelle du requérant et  certifiait que ses actes étaient conformes aux données acquises de la science et que les soins ainsi dispensés étaient d'une qualité au moins équivalente à ceux d'un médecin pratiquant des actes de chiropraxie, la cour a dénaturé les pièces qui lui étaient soumises ;  qu'en conséquence, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. A...est fondé à demander l'annulation de l'arrêt qu'il attaque ; <br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond  en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              8. Considérant qu'il résulte de ce qui a été dit aux points 1 à 5 que, pour obtenir la restitution des droits de taxe sur la valeur ajoutée qu'il a acquittés au titre de la période allant du 1er janvier 2007 au 31 décembre 2008 à raison de ses prestations de chiropraxie, M. A...doit, pour mettre le juge à même de s'assurer que la condition de l'exonération de taxe tenant à la qualité de ces prestations était remplie, produire, d'une part, et sous réserve de l'occultation des noms des patients, des éléments relatifs à sa pratique permettant d'appréhender, sur une période significative, la nature des actes accomplis et les conditions dans lesquelles ils l'ont été et, d'autre part, tous éléments utiles relatifs à ses qualifications professionnelles ;<br/>
<br/>
              9. Considérant qu'il résulte de ce qui a été dit au point 6 que M. A...a produit des éléments attestant, de manière suffisante, la qualité de la formation qu'il a suivie et du diplôme qu'il a obtenu et a produit 457 " fiches patients " relatant les consultations qu'il a assurées au cours de la période en litige qui étaient suffisamment précises et qui étaient accompagnées d'une attestation d'un médecin certifiant qu'il exerçait son art comme un médecin aurait pu le faire ; qu'ainsi, M. A...a établi, sans qu'il soit besoin d'ordonner une expertise, que les actes qu'il a accomplis devaient être exonérés de taxe sur la valeur ajoutée ;<br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède que M. A...est fondé à soutenir que c'est à tort que, par le jugement qu'il attaque, le tribunal administratif de Rennes a rejeté sa demande ; <br/>
<br/>
              11. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement de la somme de 4 000 euros à M. A...au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>                          D E C I D E :<br/>
                                        --------------<br/>
<br/>
Article 1er : L'arrêt du 9 janvier 2014 de la cour administrative d'appel de Nantes et le jugement du 18 octobre 2012 du tribunal administratif de Rennes sont annulés.<br/>
<br/>
Article 2 : M. A...est déchargé des droits de taxe sur la valeur ajoutée qu'il a  acquittés au titre de la période du 1er janvier 2007 au 31 décembre 2008 pour un montant de 28 939 euros.<br/>
<br/>
Article 3 : L'Etat versera à M. A...la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. B... A...et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
