<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027724297</ID>
<ANCIEN_ID>JG_L_2013_07_000000347719</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/72/42/CETATEXT000027724297.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  8ème sous-section jugeant seule, 17/07/2013, 347719, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-07-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>347719</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 8ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Maxime Boutron</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:347719.20130717</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le  pourvoi sommaire et le mémoire complémentaire, enregistrés les 22 mars et 22 juin 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société  Cofitem Cofimur, dont le siège est 4, rue Lasteyrie à Paris (75116) ; elle demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0610125 du 20 janvier 2011 par lequel le tribunal administratif de Cergy-Pontoise a rejeté sa demande tendant à la réduction de la cotisation de taxe foncière à laquelle elle a été assujettie au titre de l'année 2005 dans les rôles de la commune de Roissy-en-France, à raison d'un hôtel-restaurant exploité sous l'enseigne  " Campanile " ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Esther de Moustier, Auditeur, <br/>
<br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, Trichet, avocat de la société Cofitem Cofimur ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article 1498 du code général des impôts, la valeur locative des immeubles commerciaux " est déterminée au moyen de l'une des méthodes indiquées ci-après : 1° Pour les biens donnés en location à des conditions de prix normales, la valeur locative est celle qui ressort de cette location ; / 2° a. Pour les biens loués à des conditions de prix anormales ou... occupés par un tiers à un autre titre que la location... la valeur locative est déterminée par comparaison. Les termes de comparaison sont choisis dans la commune. Ils peuvent être choisis hors de la commune pour procéder à l'évaluation des immeubles d'un caractère particulier ou exceptionnel ; / b. La valeur locative des termes de comparaison est arrêtée : / Soit en partant du bail en cours à la date de référence de la révision, lorsque l'immeuble type était loué normalement à cette date, / Soit, dans le cas contraire, par comparaison avec des immeubles similaires situés dans la commune ou dans une localité présentant, du point de vue économique, une situation analogue à celle de la commune en cause et qui faisaient l'objet à cette date de locations consenties à des conditions de prix normales ; / 3° A défaut de ces bases, la valeur locative est déterminée par voie d'appréciation directe " ;<br/>
<br/>
              2. Considérant que les dispositions du 2° de l'article 1498 du code général des impôts ne permettent de se référer, pour déterminer par comparaison la valeur locative d'un local, qu'à des locaux loués à des conditions de prix normales ; que toutefois en écartant comme terme de comparaison, pour l'application des dispositions du 2° de l'article 1498 précité, le local-type n° 43 du procès-verbal des opérations de révision foncière  de la commune de Villejuif au motif que les stipulations de son bail en cours au 1er janvier 1970, qui mettaient à la charge du preneur des travaux importants d'aménagement incombant normalement au propriétaire, suffisaient à elles seules à faire regarder le bail comme conclu à des conditions de prix anormales, le tribunal administratif de Cergy-Pontoise a commis une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi,  la société Cofitem Cofimur est fondée à demander l'annulation du jugement du 20 janvier 2011 ;<br/>
<br/>
              3. Considérant que, dans les circonstances de l'espèce, il y a lieu pour le Conseil d'Etat, par application des dispositions de l'article L. 821-2  du code de justice administrative, de régler l'affaire au fond ;<br/>
<br/>
              Sur la méthode d'évaluation par comparaison :<br/>
<br/>
              4. Considérant que l'imposition de l'immeuble dont la société est propriétaire à  Roissy-en-France et qui est exploité sous l'enseigne Campanile a été initialement établie à partir de la valeur locative du local-type n° 1 du procès-verbal des opérations de révision foncière de cette commune ; que l'administration a admis, en cours d'instance, que ce local n'avait pas été régulièrement évalué et ne pouvait, par suite, être retenu comme terme de comparaison ; que l'administration et la société proposent chacune, comme elles sont en droit de le faire au cours de la procédure contentieuse, d'autres termes de comparaison ; <br/>
<br/>
              5. Considérant, en premier lieu, qu'il résulte de l'instruction que le local-type n° 90 du procès-verbal de la commune d'Issy-les-Moulineaux construit en 1926 est vétuste, que son état d'entretien était jugé passable en 1970 et qu'il n'est pas soutenu qu'il aurait fait l'objet de travaux depuis cette date de nature à le rendre comparable au local à évaluer ; que le local-type n° 5 du procès-verbal de la commune de Roissy-en-France, le local-type n° 48 du procès-verbal de la commune de Chelles et  le local-type n° 67 du procès-verbal de la commune de Livry-Gargan ne présentent pas, compte tenu de la date de leur construction, de leur nature d'hôtel traditionnel, de leur structure et de leur aménagement, de similitudes suffisantes avec l'hôtel à évaluer qui fait partie d'une chaîne d'hôtels restaurants de conception moderne ; qu'il n'est pas contesté que le local-type n°57 du procès-verbal de la commune de Boulogne et le local-type n° 33 de la commune de Saint-Mandé sont de construction et de conception ancienne ;  qu'aucun de ces locaux-types ne peut, par suite, être retenu comme terme de comparaison ; <br/>
<br/>
              6. Considérant, en deuxième lieu, qu'il résulte de l'instruction que le local-type n° 218 du procès-verbal de la commune de Versailles correspond à un hôtel qui a été évalué par rapport à un bail conclu le 19 août 1970 et non par référence à un bail en cours au 1er janvier 1970 ; qu'il a, par suite, été irrégulièrement évalué ; <br/>
<br/>
              7. Considérant, en troisième lieu, qu'il résulte des mentions du procès-verbal  complémentaire des opérations de révision foncière de la commune de Villeneuve-Saint-Georges que le  local-type n° 55  construit en 1991 a été évalué par référence au local-type n° 10 du procès-verbal de la commune de Chennevières-sur-Marne ; qu'il ressort d'une part de la déclaration souscrite en 1970 que ce dernier immeuble était loué au 1er janvier de cette année par son usufruitière pour un loyer symbolique à son fils nu-propriétaire et d'autre part des données de la fiche de calcul de cet immeuble qu'il a été évalué par la méthode d'appréciation directe à partir d'un loyer très largement supérieur au loyer réel ; que le local-type n° 56 de la commune de Villeneuve-Saint-Georges a été évalué par comparaison avec le local-type n° 11 de la commune de Chennevières-sur-Marne, lui-même évalué selon la méthode de l'appréciation directe ; que ces  locaux doivent donc être écartés ;<br/>
<br/>
              8. Considérant, en quatrième lieu, qu'il résulte de l'instruction que le procès-verbal complémentaire des opérations de révision foncière de la commune de Villejuif , établi en 1979, a modifié les données applicables au local-type n°43 ; que, par suite, il s'est substitué sur ce point au procès-verbal initial établi en 1973 ; que ce local a été évalué par comparaison avec le local-type n°4 du procès-verbal de la commune d'Evry, qui correspond à un hôtel exploité sous l'enseigne Novotel, lequel n'était pas loué au 1er janvier 1970 et n'a pas été évalué par comparaison avec un autre immeuble ; que, par suite, il ne peut être retenu comme terme de comparaison pour l'évaluation de la valeur locative du local-type n°43 qui doit donc être écarté ;<br/>
<br/>
              9. Considérant, en cinquième lieu, que le local-type n° 33 du procès-verbal de la commune de Morangis, qui correspond à un hôtel deux étoiles,  était occupé à la date de référence par son propriétaire, ainsi que l'administration le fait valoir sans être contredite, et qu'en conséquence, le tarif qui lui a été appliqué n'a pas été déterminé en fonction d'un bail en cours au 1er janvier 1970 ; que si la société requérante propose de retenir le local-type n° 1 du procès-verbal ME de la commune de Cergy correspondant à un hôtel deux étoiles construit en 1844, l'administration fait valoir, sans que cela soit contesté, qu'elle ne dispose pas de documents sur la  méthode retenue pour en évaluer la valeur locative ; qu'elle soutient également sans être contredite que le local-type n° 64 de la commune de Montrouge n'a été construit qu'en 1977 et qu'aucune indication du procès-verbal C ne fait apparaître les modalités d'évaluation de sa  valeur locative ; que, par suite, aucun de ces locaux- types ne peut être retenu comme terme de comparaison ;<br/>
<br/>
              10. Considérant, en sixième lieu, qu'il résulte de l'instruction et n'est pas contesté par la société que le local-type n° 119 du procès-verbal de la commune de Saint-Germain-en-Laye a été détruit en 1985 ; que le local-type n° 55 a été supprimé du procès-verbal initial de la commune de Courbevoie par un procès-verbal complémentaire établi en 1987 ; que, par suite, aucun de ces deux locaux ne peut être retenu comme terme de comparaison ;<br/>
<br/>
              Sur la méthode d'évaluation par appréciation directe :<br/>
<br/>
              11. Considérant qu'il ne résulte pas de l'instruction qu'il existerait un terme de comparaison plus approprié que ceux énumérés ci-dessus susceptible d'être retenu pour procéder à la détermination, par la méthode de comparaison, de la valeur locative de l'immeuble à évaluer ; que, toutefois, la constatation de l'impossibilité de déterminer la valeur locative d'un bien par comparaison, en application du 2° de cet article, n'implique pas nécessairement qu'il soit fait droit aux conclusions présentées par la société requérante à fin de décharge des impositions litigieuses dès lors que l'administration a la faculté de faire état, à tout moment de la procédure contentieuse, d'un mode de détermination de la valeur locative d'un local commercial conforme aux prescriptions de l'article 1498 précité ; que l'administration est ainsi fondée à proposer le recours à la méthode d'évaluation directe prévue par le 3° de l'article 1498 du code général des impôts ;<br/>
<br/>
              12. Considérant qu'aux termes de l'article 324 AB de l'annexe III au code général des impôts : " Lorsque les autres moyens font défaut, il est procédé à l'évaluation directe de l'immeuble en appliquant un taux d'intérêt à sa valeur vénale, telle qu'elle serait constatée à la date de référence si l'immeuble était libre de toute location ou occupation. / Le taux d'intérêt susvisé est fixé en fonction du taux des placements immobiliers constatés dans la région à la date de référence pour des immeubles similaires. " ; qu'aux termes de l'article 324 AC : " En l'absence d'acte et de toute autre donnée récente faisant apparaître une estimation de l'immeuble à évaluer susceptible d'être retenue sa valeur vénale à la date de référence est appréciée d'après la valeur vénale d'autres immeubles d'une nature comparable ayant fait l'objet de transactions récentes situés dans la commune même ou dans une localité présentant du point de vue économique une situation analogue à celle de la commune en cause. / La valeur vénale d'un immeuble peut également être obtenue en ajoutant à la valeur vénale du terrain, estimée par comparaison avec celle qui ressort de transactions récentes relatives à des terrains à bâtir situés dans une zone comparable, la valeur de reconstruction au 1er janvier 1970 dudit immeuble, réduite pour tenir compte, d'une part, de la dépréciation immédiate et, d'autre part, du degré de vétusté de l'immeuble et de son état d'entretien, ainsi que de la nature, de l'importance, de l'affectation et de la situation de ce bien. " ; <br/>
<br/>
              13. Considérant qu'en vertu des dispositions des articles 324 AB et AC de l'annexe III, la valeur vénale des immeubles évalués par voie d'appréciation directe doit d'abord être déterminée en utilisant les données figurant dans les différents actes constituant l'origine de la propriété de l'immeuble si ces données, qui peuvent résulter notamment d'actes de cession, de déclarations de succession, d'apport en société ou, s'agissant d'immeubles qui n'étaient pas construits en 1970, de leur valeur lors de leur première inscription au bilan, ont une date la plus proche possible de la date de référence du 1er janvier 1970 ; que si ces données ne peuvent être regardées comme pertinentes du fait qu'elles présenteraient une trop grande antériorité ou postériorité par rapport au 1er janvier 1970, il incombe à l'administration de proposer des évaluations fondées sur les deux autres méthodes prévues à l'article 324 AC, en retenant des transactions qui peuvent être postérieures ou antérieures aux actes ou aux bilans mentionnés ci-dessus dès lors qu'elles ont été conclues à une date plus proche du 1er janvier 1970 et non, comme la société le soutient, celle du 1er août 1939 ; que ce n'est que si l'administration n'est pas à même de proposer des éléments de calcul fondés sur l'une ou l'autre de ces méthodes et si le contribuable n'est pas davantage en mesure de fournir ces éléments de comparaison qu'il y a lieu de retenir, pour le calcul de la valeur locative, les données figurant dans les actes constituant l'origine de la propriété du bien ou, le cas échéant, dans son bilan ; <br/>
<br/>
              14. Considérant que si la société propose de retenir la cession d'un immeuble situé 6 avenue de Flandres à Bonneuil intervenue le 13 février 1973, l'administration fait à bon droit valoir que cette transaction portait sur un immeuble ne présentant pas des caractéristiques et ne proposant pas des prestations similaires à celles de l'établissement à évaluer ;<br/>
<br/>
               15. Considérant, dans ces conditions, que l'administration propose de déterminer la valeur locative de l'hôtel à partir de la valeur vénale moyenne résultant de cinq actes de cession concernant des hôtels situés à Bobigny, Cergy, Pantin, Osny et Pontoise et la valeur locative du restaurant à partir de la valeur vénale moyenne résultant de trois actes de cession concernant des restaurants situés à Saint-Ouen-l'Aumône, Pierrelaye et Cergy ; qu'il n'est pas contesté que ces cessions étaient les plus proches possible de la date de référence ; <br/>
<br/>
              16. Considérant que la société, qui se borne à renvoyer à un document établi en 1971 pour la commune de Gonesse et comportant un taux d'intérêt de 5 %, ne critique pas utilement le taux de 7 % appliqué à la valeur vénale de l'immeuble en vertu de l'article 324 AB de l'annexe III au code général des impôts ;  <br/>
<br/>
              17. Considérant que les éléments ainsi retenus conduisent à déterminer une valeur locative supérieure à celle retenue pour l'évaluation de la valeur locative ayant servi de base à l'imposition en litige ; <br/>
<br/>
              18. Considérant qu'il résulte de ce qui précède que la demande de la société Cofitem Cofimur doit être rejetée ainsi que ses conclusions fondées sur l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
 Article 1er : Le jugement du tribunal administratif de Cergy-Pontoise du 20 janvier 2011 est annulé.<br/>
<br/>
 Article 2 : La demande de la société Cofitem Cofimur présentée devant le tribunal administratif de Cergy-Pontoise et le surplus de ses conclusions présentées devant le Conseil d'Etat sont rejetés.<br/>
<br/>
 Article 3 : La présente décision sera notifiée à la société Cofitem Cofimur et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
