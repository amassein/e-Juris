<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039728695</ID>
<ANCIEN_ID>JG_L_2019_12_000000420025</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/72/86/CETATEXT000039728695.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 31/12/2019, 420025</TITRE>
<DATE_DEC>2019-12-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420025</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:420025.20191231</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Paris d'ordonner une expertise médicale en vue d'obtenir tous les éléments utiles sur les préjudices qu'il estime avoir subis du fait de la prise du Mediator et de condamner solidairement l'Etat et l'Agence nationale de sécurité du médicament et des produits de santé à lui verser une provision à valoir sur l'indemnisation de ses préjudices. <br/>
<br/>
              Par un jugement avant-dire droit n°1312676 du 7 août 2014, le tribunal administratif de Paris a rejeté les conclusions de la demande dirigées contre l'Agence nationale de sécurité du médicament et des produits de santé, a déclaré l'Etat responsable des conséquences dommageables éventuelles pour M. A... de la prise du Mediator à partir du 7 juillet 1999, a ordonné une expertise et a rejeté la demande de provision. Par un jugement n° 1312676 du 23 octobre 2015, le tribunal administratif de Paris a rejeté les conclusions indemnitaires de M. A.... <br/>
<br/>
              Par un arrêt n°s 14PA04079, 15PA04749 du 28 février 2018, la cour administrative d'appel de Paris a rejeté l'appel formé par M. A... contre le jugement du tribunal administratif de Paris du 23 octobre 2015 et prononcé un non-lieu à statuer sur l'appel formé par le ministre des affaires sociales, de la santé et des droits des femmes contre le jugement avant-dire droit du 7 août 2014.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 avril et 20 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat et de l'Agence nationale de sécurité du médicament et des produits de santé la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A... a demandé au tribunal administratif de Paris de condamner l'Etat et l'Agence nationale de sécurité du médicament et des produits de santé à l'indemniser des préjudices qu'il estime avoir subis du fait de son exposition au benfluorex, principe actif du Mediator, par lequel il a été traité de juin 2001 à janvier 2006. Par un jugement avant-dire droit du 7 août 2014, le tribunal a déclaré l'Etat responsable des conséquences dommageables éventuelles pour M. A... de la prise du Médiator et ordonné une expertise, à sa demande, en vue d'établir l'existence d'un lien de causalité entre la pathologie dont il souffre et son exposition au Mediator et d'apprécier l'étendue des préjudices subis. A l'invitation du vice-président du tribunal, l'expert qu'il avait désigné le 26 septembre 2014 a déposé un rapport de carence au vu duquel le tribunal administratif de Paris a rejeté la demande de M. A... par un jugement du 23 octobre 2015. Par un arrêt du 28 février 2018, la cour administrative d'appel de Paris a rejeté l'appel de M. A... contre le jugement du 23 octobre 2015 et prononcé un non-lieu à statuer sur l'appel formé par le ministre chargé de la santé contre le jugement du 7 août 2014. Par son pourvoi, M. A... doit être regardé comme demandant l'annulation de cet arrêt en tant que, par son article 1er, il rejette son appel.<br/>
<br/>
              2. Aux termes de l'article R. 621-1 du code de justice administrative : " La juridiction peut, soit d'office, soit sur la demande des parties ou de l'une d'elles, ordonner, avant dire droit, qu'il soit procédé à une expertise sur les points déterminés par sa décision (...) ". Aux termes de l'article R. 621-12 du même code : " Le président de la juridiction (...) peut (...) accorder aux experts et aux sapiteurs, sur leur demande, une allocation provisionnelle à valoir sur le montant de leurs honoraires et débours. / Il précise la ou les parties qui devront verser ces allocations. Sa décision ne peut faire l'objet d'aucun recours (...) ". Aux termes de l'article R. 621-12-1 du même code : " L'absence de versement, par la partie qui en a la charge, de l'allocation provisionnelle, dans le mois qui suit la notification de la décision mentionnée à l'article R. 621-12, peut donner lieu, à la demande de l'expert, à une mise en demeure signée du président de la juridiction. / Si le délai fixé par cette dernière n'est pas respecté, et si le rapport d'expertise n'a pas été déposé à cette date, l'expert est appelé par le président à déposer, avec sa note de frais et honoraires, un rapport se limitant au constat des diligences effectuées et de cette carence, dont la juridiction tire les conséquences, notamment pour l'application des dispositions du deuxième alinéa de l'article R. 761-1. / Le président peut toutefois, avant d'inviter l'expert à produire un rapport de carence, soumettre l'incident à la séance prévue à l'article R. 621-8-1 ". Enfin, en vertu de l'article R. 621-1-1 de ce code, le président de la juridiction peut désigner au sein de sa juridiction un magistrat chargé des questions d'expertise et du suivi des opérations d'expertise, en lui déléguant notamment les attributions mentionnées aux articles R. 621-12 et R. 621-12-1.<br/>
<br/>
              3. Il résulte de ces dispositions que lorsque la partie qui en a la charge ne verse pas à l'expert l'allocation provisionnelle accordée, le président de la juridiction ou le magistrat qu'il a désigné ne peut autoriser l'expert à déposer un rapport de carence, dont la juridiction tirera les conséquences, qu'après en avoir averti cette partie par une mise en demeure qui lui impartit un nouveau délai pour verser l'allocation.<br/>
<br/>
              4. Pour juger que le vice-président du tribunal administratif de Paris avait pu inviter l'expert qu'il avait désigné à déposer un rapport de carence sans mise en demeure préalable de M. A..., la cour administrative d'appel de Paris a retenu qu'aucune allocation provisionnelle n'avait été sollicitée par l'expert, de sorte que les dispositions de l'article R. 621-12-1 du code de justice administrative ne trouvaient pas à s'appliquer. Il ressort toutefois des pièces du dossier soumis aux juges du fond que le vice-président du tribunal administratif de Paris a, par une ordonnance du 17 octobre 2014 prise sur le fondement des dispositions de l'article R. 621-12 du même code, accordé à l'expert une allocation provisionnelle de 1 000 euros à valoir sur le montant de ses honoraires et débours et mis cette allocation à la charge de M. A.... Par suite, la cour a dénaturé les pièces du dossier qui lui était soumis.<br/>
<br/>
              5. Il résulte de ce qui précède que M. A... est fondé à demander l'annulation de l'article 1er de l'arrêt qu'il attaque. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens de son pourvoi.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à M. A... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'article 1er de l'arrêt de la cour administrative d'appel de Paris du 28 février 2018 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris dans la mesure de la cassation prononcée.<br/>
<br/>
Article 3 : L'Etat versera à M. A... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A... et à la ministre des solidarités et de la santé.<br/>
Copie en sera adressée à l'Agence nationale de sécurité du médicament et des produits de santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-04-02-02-01 PROCÉDURE. INSTRUCTION. MOYENS D'INVESTIGATION. EXPERTISE. RECOURS À L'EXPERTISE. - RAPPORT DE CARENCE EN CAS DE NON VERSEMENT DE L'ALLOCATION PROVISIONNELLE (ART. R. 621-12 ET R. 621-12-1 DU CJA) - EXIGENCE D'UNE MISE EN DEMEURE PRÉALABLE - EXISTENCE.
</SCT>
<ANA ID="9A"> 54-04-02-02-01 Il résulte des articles R. 621-1, R. 621-12, R. 621-12-1 et R. 621-1-1 du code de justice administrative (CJA) que lorsque la partie qui en a la charge ne verse pas à l'expert l'allocation provisionnelle accordée, le président de la juridiction ou le magistrat qu'il a désigné ne peut autoriser l'expert à déposer un rapport de carence, dont la juridiction tirera les conséquences, qu'après en avoir averti cette partie par une mise en demeure qui lui impartit un nouveau délai pour verser l'allocation.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
