<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037089169</ID>
<ANCIEN_ID>JG_L_2018_06_000000408299</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/08/91/CETATEXT000037089169.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 20/06/2018, 408299</TITRE>
<DATE_DEC>2018-06-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408299</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Marie Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:408299.20180620</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Montpellier d'annuler la décision du 1er août 2013 par laquelle le directeur général des finances publiques a décidé la cessation du versement de l'allocation d'aide au retour à l'emploi dont elle bénéficiait, et la récupération des montants versés au titre de cette même allocation pour les mois de mars et avril 2013. Par un jugement n° 1304676 du 20 février 2015, le tribunal administratif de Montpellier a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15MA01517 du 7 février 2017, enregistré le 23 février suivant au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Marseille a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 10 avril 2015 au greffe de cette cour, présenté par MmeA....<br/>
<br/>
              Par ce pourvoi et par un mémoire complémentaire et un mémoire rectificatif, enregistrés les 13 juillet et 17 août 2017 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Montpellier du 20 février 2015 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier.<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le décret n° 2003-1370 du 31 décembre 2003 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Sirinelli, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de MmeA....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. D'une part, aux termes de l'article L. 5422-1 du code du travail : " Ont droit à l'allocation d'assurance les travailleurs involontairement privés d'emploi (...), aptes au travail et recherchant un emploi qui satisfont à des conditions d'âge et d'activité antérieure ". Aux termes de l'article L. 5422-2 de ce code : " L'allocation d'assurance est accordée pour des durées limitées qui tiennent compte de l'âge des intéressés et de leurs conditions d'activité professionnelle antérieure. (...) ".<br/>
<br/>
              2. D'autre part, aux termes de l'article L. 5424-1 du même code : " Ont droit à une allocation d'assurance dans les conditions prévues aux articles L. 5422-2 et L. 5422-3 : / 1° Les agents fonctionnaires et non fonctionnaires de l'Etat et de ses établissements publics administratifs (...) ". Aux termes de l'article R. 5424-2 de ce code, dans sa rédaction applicable au litige : " Lorsque, au cours de la période retenue pour l'application de l'article L. 5422-2, la durée totale d'emploi accomplie pour le compte d'un ou plusieurs employeurs affiliés au régime d'assurance a été plus longue que l'ensemble des périodes d'emploi accomplies pour le compte d'un ou plusieurs employeurs relevant de l'article L. 5424-1, la charge de l'indemnisation incombe à l'institution mentionnée à l'article L. 5312-1 pour le compte de l'organisme mentionné à l'article L. 5427-1. / Dans le cas contraire, cette charge incombe à l'employeur relevant de l'article L. 5424-1, ou à celui des employeurs relevant de cet article qui a employé l'intéressé durant la période la plus longue ".<br/>
<br/>
              3. Pour l'application de ces dispositions, lorsqu'au cours de la période de référence retenue pour apprécier la condition d'activité professionnelle antérieure à laquelle est subordonné le versement de l'allocation d'assurance, la durée totale d'emploi a été accomplie par l'intéressé pour le compte de plusieurs employeurs publics relevant de l'article L. 5424-1 du code du travail, ou que la durée totale d'emploi accomplie pour le compte de tels employeurs a été plus longue que celle accomplie pour le compte d'un ou plusieurs employeurs affiliés au régime d'assurance, la charge de l'indemnisation incombe à celui de ces employeurs publics qui a employé l'intéressé durant la période la plus longue. <br/>
<br/>
              4. La circonstance que la situation de travailleur involontairement privé d'emploi et recherchant un emploi, au sens de l'article L. 5422-1, découlerait de l'absence de réintégration de l'intéressé à sa demande par un de ses employeurs publics, alors que cette réintégration à l'issue d'un congé pour convenances personnelles était de droit, est, à cet égard, sans incidence sur la détermination de l'employeur public auquel incombe la charge de l'indemnisation. <br/>
<br/>
              5. Il ressort des pièces du dossier soumis au juge du fond que MmeA..., agent de Pôle emploi sous contrat de droit public, a bénéficié d'un congé pour convenances personnelles du 1er mars 2010 au 28 février 2013, au cours duquel elle a été employée par la direction générale des finances publiques, jusqu'à ce que cette direction mette fin à ses fonctions, le 11 janvier 2013. A la suite de sa demande de réintégration, qui était de droit en vertu de l'article 26 du décret du 31 décembre 2003 fixant les dispositions applicables aux agents contractuels de droit public de Pôle emploi, cet établissement public ne l'a toutefois pas réintégrée à l'issue de son congé, mais placée en attente de réintégration, laquelle est intervenue au mois d'août 2013. Mme A...ayant ainsi relevé de deux employeurs publics, entrant tous deux dans le champ de l'article L. 5424-1 du code du travail, au cours de la période retenue pour l'application de l'article L. 5422-2, la charge de son indemnisation incombait à celui de ces employeurs publics qui l'avait employée durant la période la plus longue. Par le jugement attaqué, le tribunal a cependant jugé que, sa situation d'agent involontairement privé d'emploi résultant de son placement en " attente de réintégration " par Pôle emploi, il incombait à ce dernier, en qualité d'employeur de l'intéressée, de prendre en charge son indemnisation au titre de l'allocation d'aide au retour à l'emploi, sans qu'y fasse obstacle la circonstance qu'elle avait travaillé pendant sa période de congé sans rémunération pour convenances personnelles à la direction générale des finances publiques, entre le 1er mars 2010 et le 11 janvier 2013. Le tribunal a ce faisant commis une erreur de droit, dès lors qu'il n'a pas déterminé l'employeur public auquel incombe le versement de l'allocation d'assurance en fonction de la plus longue des durées d'emploi de l'intéressée auprès de ceux-ci.<br/>
<br/>
              6. Il résulte de ce qui précède que Mme A...est fondée à demander l'annulation du jugement attaqué. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire de se prononcer sur les autres moyens du pourvoi. <br/>
<br/>
              7. Enfin, il y a lieu de mettre à la charge de l'Etat, au titre des dispositions de l'article L. 761-1 du code de justice administrative, une somme de 3 000 euros à verser à Mme A....<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le jugement du 20 février 2015 du tribunal administratif de Montpellier est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Montpellier.<br/>
Article 3 : L'Etat versera à Mme A...une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à Mme B...A...et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-10-06-04 FONCTIONNAIRES ET AGENTS PUBLICS. CESSATION DE FONCTIONS. LICENCIEMENT. ALLOCATION POUR PERTE D'EMPLOI. - ALLOCATION D'ASSURANCE VERSÉE AUX AGENTS PUBLICS INVOLONTAIREMENT PRIVÉS D'EMPLOI - DÉTERMINATION DE L'EMPLOYEUR PUBLIC AUQUEL INCOMBE LA CHARGE DE L'INDEMNISATION - 1) CAS OÙ LA DURÉE TOTALE D'EMPLOI AU COURS DE LA PÉRIODE DE RÉFÉRENCE A ÉTÉ ACCOMPLIE POUR LE COMPTE DE PLUSIEURS EMPLOYEURS - EMPLOYEUR PUBLIC QUI A EMPLOYÉ L'INTÉRESSÉ DURANT LA PÉRIODE LA PLUS LONGUE - 2) AGENT PUBLIC INVOLONTAIREMENT PRIVÉ D'EMPLOI EN RAISON DE SON ABSENCE DE RÉINTÉGRATION À L'ISSUE D'UN CONGÉ POUR CONVENANCES PERSONNELLES - CIRCONSTANCE SANS INCIDENCE, LA RÉINTÉGRATION ÉTANT DE DROIT.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-10-02 TRAVAIL ET EMPLOI. POLITIQUES DE L'EMPLOI. INDEMNISATION DES TRAVAILLEURS PRIVÉS D'EMPLOI. - ALLOCATION D'ASSURANCE VERSÉE AUX AGENTS PUBLICS INVOLONTAIREMENT PRIVÉS D'EMPLOI - DÉTERMINATION DE L'EMPLOYEUR PUBLIC AUQUEL INCOMBE LA CHARGE DE L'INDEMNISATION - 1) CAS OÙ LA DURÉE TOTALE D'EMPLOI AU COURS DE LA PÉRIODE DE RÉFÉRENCE A ÉTÉ ACCOMPLIE POUR LE COMPTE DE PLUSIEURS EMPLOYEURS - EMPLOYEUR PUBLIC QUI A EMPLOYÉ L'INTÉRESSÉ DURANT LA PÉRIODE LA PLUS LONGUE - 2) AGENT PUBLIC INVOLONTAIREMENT PRIVÉ D'EMPLOI EN RAISON DE SON ABSENCE DE RÉINTÉGRATION À L'ISSUE D'UN CONGÉ POUR CONVENANCES PERSONNELLES - CIRCONSTANCE SANS INCIDENCE, LA RÉINTÉGRATION ÉTANT DE DROIT.
</SCT>
<ANA ID="9A"> 36-10-06-04 1) Pour l'application des articles L. 5422-1, L. 5422-2, L. 5424-1 et R. 5424-2 du code du travail, lorsqu'au cours de la période de référence retenue pour apprécier la condition d'activité professionnelle antérieure à laquelle est subordonné le versement de l'allocation d'assurance, la durée totale d'emploi a été accomplie par l'intéressé pour le compte de plusieurs employeurs publics relevant de l'article L. 5424-1 du code du travail, ou que la durée totale d'emploi accomplie pour le compte de tels employeurs a été plus longue que celle accomplie pour le compte d'un ou plusieurs employeurs affiliés au régime d'assurance, la charge de l'indemnisation incombe à celui de ces employeurs publics qui a employé l'intéressé durant la période la plus longue.... ,,2) La circonstance que la situation de travailleur involontairement privé d'emploi et recherchant un emploi, au sens de l'article L. 5422-1, découlerait de l'absence de réintégration de l'intéressé à sa demande par un de ses employeurs publics, alors que cette réintégration à l'issue d'un congé pour convenances personnelles était de droit, est, à cet égard, sans incidence sur la détermination de l'employeur public auquel incombe la charge de l'indemnisation.</ANA>
<ANA ID="9B"> 66-10-02 1) Pour l'application des articles L. 5422-1, L. 5422-2, L. 5424-1 et R. 5424-2 du code du travail, lorsqu'au cours de la période de référence retenue pour apprécier la condition d'activité professionnelle antérieure à laquelle est subordonné le versement de l'allocation d'assurance, la durée totale d'emploi a été accomplie par l'intéressé pour le compte de plusieurs employeurs publics relevant de l'article L. 5424-1 du code du travail, ou que la durée totale d'emploi accomplie pour le compte de tels employeurs a été plus longue que celle accomplie pour le compte d'un ou plusieurs employeurs affiliés au régime d'assurance, la charge de l'indemnisation incombe à celui de ces employeurs publics qui a employé l'intéressé durant la période la plus longue.... ,,2) La circonstance que la situation de travailleur involontairement privé d'emploi et recherchant un emploi, au sens de l'article L. 5422-1, découlerait de l'absence de réintégration de l'intéressé à sa demande par un de ses employeurs publics, alors que cette réintégration à l'issue d'un congé pour convenances personnelles était de droit, est, à cet égard, sans incidence sur la détermination de l'employeur public auquel incombe la charge de l'indemnisation.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
