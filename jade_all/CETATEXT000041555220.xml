<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041555220</ID>
<ANCIEN_ID>JG_L_2020_02_000000429343</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/55/52/CETATEXT000041555220.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 10/02/2020, 429343</TITRE>
<DATE_DEC>2020-02-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429343</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT</AVOCATS>
<RAPPORTEUR>Mme Cécile Renault</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:429343.20200210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. D... C... a demandé au tribunal administratif de Lille d'annuler l'arrêté du 14 janvier 2011 du ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat en tant qu'il le nomme dans le grade d'inspecteur départemental de première classe, 3ème échelon, à compter du 1er avril 2011 et l'arrêté ministériel du 22 août 2011 en tant qu'il le reclasse dans le grade d'inspecteur divisionnaire des finances publiques hors classe, 3ème échelon, à compter du 1er septembre 2011. Par une ordonnance n° 1403894 du 20 octobre 2016, la présidente de la 5ème chambre du tribunal administratif de Lille a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 16DA02456 du 31 janvier 2019, la cour administrative d'appel de Douai a rejeté l'appel formé par M. C... contre cette ordonnance.  <br/>
<br/>
              Par un pourvoi et un nouveau mémoire, enregistrés les 1er avril 2019 et 20 janvier 2020 au secrétariat du contentieux du Conseil d'Etat, M. C... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... B..., auditrice,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, Robillot, avocat de M. C... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat a, par un arrêté du 14 janvier 2011, nommé M. C... inspecteur départemental de 1ère classe des impôts et, par un arrêté du 20 août 2011, l'a reclassé dans le grade d'inspecteur divisionnaire hors classe, 3ème échelon, à compter du 1er septembre 2011. Par une demande enregistrée le 18 juin 2014, M. C... a saisi le tribunal administratif de Lille de conclusions tendant à l'annulation de ces deux arrêtés. Par une ordonnance du 20 octobre 2016, le président de la 5ème chambre du tribunal administratif de Lille a rejeté sa demande. Par un arrêt du 31 janvier 2019, contre lequel il se pourvoit en cassation, la cour administrative d'appel de Douai a rejeté l'appel formé par M. C... contre cette ordonnance.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article R. 421-1 du code de justice administrative dans sa rédaction applicable au présent litige : " Sauf en matière de travaux publics, la juridiction ne peut être saisie que par voie de recours formé contre une décision, et ce, dans les deux mois à partir de la notification ou de la publication de la décision attaquée ". Il résulte des dispositions de l'article R. 421-5 du même code que ce délai n'est opposable qu'à la condition d'avoir été mentionné, ainsi que les voies de recours, dans la notification de la décision. <br/>
<br/>
              3. Toutefois, le principe de sécurité juridique, qui implique que ne puissent être remises en cause sans condition de délai des situations consolidées par l'effet du temps, fait obstacle à ce que puisse être contestée indéfiniment une décision administrative individuelle qui a été notifiée à son destinataire, ou dont il est établi, à défaut d'une telle notification, que celui-ci a eu connaissance. En une telle hypothèse, si le non-respect de l'obligation d'informer l'intéressé sur les voies et les délais de recours, ou l'absence de preuve qu'une telle information a bien été fournie, ne permet pas que lui soient opposés les délais de recours fixés par le code de justice administrative, le destinataire de la décision ne peut exercer de recours juridictionnel au-delà d'un délai raisonnable. En règle générale et sauf circonstances particulières dont se prévaudrait le requérant, ce délai ne saurait, sous réserve de l'exercice de recours administratifs pour lesquels les textes prévoient des délais particuliers, excéder un an à compter de la date à laquelle une décision expresse lui a été notifiée ou de la date à laquelle il est établi qu'il en a eu connaissance.<br/>
<br/>
              4. Aux termes de l'article R. 222-1 du code de justice administrative : " Les présidents de tribunal administratif et de cour administrative d'appel, le vice-président du tribunal administratif de Paris et les présidents de formation de jugement des tribunaux et des cours peuvent, par ordonnance : / (...) 4° Rejeter les requêtes manifestement irrecevables, lorsque la juridiction n'est pas tenue d'inviter leur auteur à les régulariser ou qu'elles n'ont pas été régularisées à l'expiration du délai imparti par une demande en ce sens (...) ".<br/>
<br/>
              5. Lorsque, dans l'hypothèse où l'obligation d'informer l'intéressé sur les voies et les délais de recours n'a pas été respectée, ou en l'absence de preuve qu'une telle information a bien été fournie, le requérant entend contester devant le juge une décision administrative individuelle dont il a eu connaissance depuis plus d'un an, il lui appartient de faire valoir, le cas échéant, que, dans les circonstances de l'espèce, le délai raisonnable dont il disposait pour la contester devait être regardé comme supérieur à un an. En l'absence de tels éléments, et lorsqu'il ressort des pièces du dossier soumis aux juges que le requérant a eu connaissance de la décision depuis plus d'un an, la requête peut être rejetée par ordonnance comme manifestement irrecevable, sur le fondement de l'article R. 222-1 du code de justice administrative, sans que le requérant soit invité à justifier de sa recevabilité.<br/>
<br/>
              6. Pour rejeter par ordonnance comme manifestement irrecevable, sur le fondement de l'article R. 222-1 du code de justice administrative, la demande de M. C..., la présidente de la 5ème chambre du tribunal administratif de Lille a relevé que l'intéressé avait eu connaissance des arrêtés contestés, au plus tard, respectivement les 15 mars et 15 septembre 2011 et que, par suite, alors même que les voies et délais de recours ne lui auraient pas été notifiés, il n'avait pas exercé son recours juridictionnel dans un délai raisonnable en ne saisissant le tribunal que le 18 juin 2014.<br/>
<br/>
              7. En premier lieu, en jugeant que M. C... avait été informé du contenu précis des arrêtés des 14 janvier et 22 août 2011 par des notifications des 15 mars et 15 septembre 2011 et que, dès lors, il devait être regardé comme ayant eu connaissance de ces arrêtés au plus tard à ces dates, la cour administrative d'appel n'a entaché son arrêt ni d'erreur de droit, ni de dénaturation des pièces du dossier, ni d'insuffisance de motivation. <br/>
<br/>
              8. En deuxième lieu, il ressort des pièces du dossier soumis aux juges du fond que M. C... n'a pas avancé, dans le cadre de l'instance devant le tribunal administratif de Lille, de circonstances particulières susceptibles de justifier le délai entre les notifications, les 15 mars et 15 septembre 2011, des arrêtés des 14 janvier et 22 août 2011, et la saisine du tribunal administratif le 18 juin 2014. Dès lors, en jugeant que la présidente de la 5ème chambre du tribunal administratif avait pu rejeter la demande de M. C..., en application des dispositions du 4° de l'article R. 222-1 du code de justice administrative, sans l'inviter préalablement à justifier de sa recevabilité, la cour n'a pas commis d'erreur de droit. <br/>
<br/>
              9. En troisième lieu, si, aux termes du premier alinéa de l'article R. 611-7 du code de justice administrative, " lorsque la décision lui paraît susceptible d'être fondée sur un moyen relevé d'office, le président de la formation de jugement (...) en informe les parties avant la séance de jugement et fixe le délai dans lequel elles peuvent, sans qu'y fasse obstacle la clôture éventuelle de l'instruction, présenter leurs observations sur le moyen communiqué ", le second alinéa du même article prévoient que ces dispositions ne sont pas applicables lorsque le juge rejette une demande par ordonnance sur le fondement de l'article R. 222-1 du code de justice administrative, sans réserver le cas où cette ordonnance interviendrait alors que l'instruction a été ouverte. Par suite, la cour administrative d'appel de Douai n'a pas entaché son arrêt d'erreur de droit en jugeant qu'alors même que l'instruction avait été ouverte, le tribunal administratif pouvait rejeter par ordonnance la demande de M. C... sans informer celui-ci qu'il entendait se fonder sur la circonstance que sa demande n'avait pas été présentée dans un délai raisonnable.<br/>
<br/>
              10. En quatrième lieu, la cour administrative d'appel ne s'est fondée qu'à titre surabondant, pour écarter le moyen tiré de la méconnaissance de l'article R. 611-7 du code de justice administrative par le premier juge, sur la circonstance que le ministre de l'action et des comptes publics devait être regardé comme ayant soulevé lui-même une telle cause d'irrecevabilité dès lors qu'il avait produit la décision du Conseil d'Etat, statuant au contentieux, n° 387763 du 13 juillet 2016. Les moyens dirigés contre ce motif de l'arrêt sont donc inopérants.<br/>
<br/>
              11. Enfin, si M. C... soutenait devant elle qu'il avait cherché à obtenir un règlement amiable du litige l'opposant à l'administration par tout moyen avant d'agir en justice, y compris en saisissant le Défenseur des droits, et que, s'agissant d'un problème touchant de nombreux agents, il avait souhaité laisser place au dialogue, notamment par l'intermédiaire des associations et syndicats, la cour administrative d'appel n'a, en tout état de cause, pas entaché son arrêt de dénaturation des faits en relevant que de telles circonstances ne permettaient pas de déroger, en l'espèce, au délai d'un an mentionné au point 3.<br/>
<br/>
              12. Enfin, la règle énoncée au point 3, qui a pour seul objet de borner dans le temps les conséquences de la sanction attachée au défaut de mention des voies et délais de recours, ne porte pas atteinte à la substance du droit au recours, mais tend seulement à éviter que son exercice, au-delà d'un délai raisonnable, ne mette en péril la stabilité des situations juridiques et la bonne administration de la justice, en exposant les défendeurs potentiels à des recours excessivement tardifs. Il appartient dès lors au juge administratif d'en faire application au litige dont il est saisi, quelle que soit la date des faits qui lui ont donné naissance et la date à laquelle le recours a été introduit, sans qu'y fassent obstacle les stipulations des articles 6, paragraphe 1, et 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.<br/>
<br/>
              13. Par suite, M. C... n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              14. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. C... est rejeté. <br/>
Article 2 : La présente décision sera notifiée à M. D... C..., au ministre de l'économie et des finances et au ministre de l'action et des comptes publics. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-07 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉLAIS. - IMPOSSIBILITÉ D'EXERCER UN RECOURS JURIDICTIONNEL AU-DELÀ D'UN DÉLAI RAISONNABLE [RJ1] - POSSIBILITÉ DE REJETER PAR ORDONNANCE LES REQUÊTES MANIFESTEMENT IRRECEVABLES POUR CE MOTIF (4° DE L'ART. R. 222-1 DU CJA) - EXISTENCE - OBLIGATION POUR LE JUGE DE COMMUNIQUER UN MOYEN RELEVÉ D'OFFICE - ABSENCE, Y COMPRIS LORSQUE L'INSTRUCTION A ÉTÉ OUVERTE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-03 PROCÉDURE. JUGEMENTS. COMPOSITION DE LA JURIDICTION. - POSSIBILITÉ DE REJETER PAR ORDONNANCE LES REQUÊTES MANIFESTEMENT IRRECEVABLES (4° DE L'ART. R. 222-1 DU CJA) - CAS OÙ LE REQUÉRANT EXERCE UN RECOURS AU-DELÀ DU DÉLAI RAISONNABLE [RJ1] - EXISTENCE -  OBLIGATION POUR LE JUGE DE COMMUNIQUER UN MOYEN RELEVÉ D'OFFICE - ABSENCE, Y COMPRIS LORSQUE L'INSTRUCTION A ÉTÉ OUVERTE.
</SCT>
<ANA ID="9A"> 54-01-07 Lorsque, dans l'hypothèse où l'obligation d'informer l'intéressé sur les voies et les délais de recours n'a pas été respectée, ou en l'absence de preuve qu'une telle information a bien été fournie, le requérant entend contester devant le juge une décision administrative individuelle dont il a eu connaissance depuis plus d'un an, il lui appartient de faire valoir, le cas échéant, que, dans les circonstances de l'espèce, le délai raisonnable dont il disposait pour la contester devait être regardé comme supérieure à un an. En l'absence de tels éléments, et lorsqu'il ressort des pièces du dossier soumis aux juges que le requérant a eu connaissance de la décision depuis plus d'un an, la requête peut être rejetée par ordonnance comme manifestement irrecevable, sur le fondement de l'article R. 222-1 du code de justice administrative (CJA), sans que le requérant soit inviter à justifier de sa recevabilité.,,,Le second alinéa de l'article R. 611-7 du CJA prévoit que son premier alinéa, qui impose d'informer les parties lorsque la décision paraît susceptible d'être fondée sur un moyen relevé d'office, n'est pas applicable lorsque le juge rejette une demande par ordonnance sur le fondement de l'article R. 222-1 du même code, sans réserver le cas où cette ordonnance interviendrait alors que l'instruction a été ouverte. Par suite, ne commet pas d'erreur de droit la cour administrative d'appel qui juge qu'alors même que l'instruction avait été ouverte, le tribunal administratif pouvait rejeter par ordonnance la demande du requérant sans informer celui-ci qu'il entendait se fonder sur la circonstance que sa demande n'avait pas été présentée dans un délai raisonnable.</ANA>
<ANA ID="9B"> 54-06-03 Lorsque, dans l'hypothèse où l'obligation d'informer l'intéressé sur les voies et les délais de recours n'a pas été respectée, ou en l'absence de preuve qu'une telle information a bien été fournie, le requérant entend contester devant le juge une décision administrative individuelle dont il a eu connaissance depuis plus d'un an, il lui appartient de faire valoir, le cas échéant, que, dans les circonstances de l'espèce, le délai raisonnable dont il disposait pour la contester devait être regardé comme supérieure à un an. En l'absence de tels éléments, et lorsqu'il ressort des pièces du dossier soumis aux juges que le requérant a eu connaissance de la décision depuis plus d'un an, la requête peut être rejetée par ordonnance comme manifestement irrecevable, sur le fondement de l'article R. 222-1 du code de justice administrative (CJA), sans que le requérant soit inviter à justifier de sa recevabilité.,,,Le second alinéa de l'article R. 611-7 du CJA prévoit que son premier alinéa, qui impose d'informer les parties lorsque la décision paraît susceptible d'être fondée sur un moyen relevé d'office, n'est pas applicable lorsque le juge rejette une demande par ordonnance sur le fondement de l'article R. 222-1 du même code, sans réserver le cas où cette ordonnance interviendrait alors que l'instruction a été ouverte. Par suite, ne commet pas d'erreur de droit la cour administrative d'appel qui juge qu'alors même que l'instruction avait été ouverte, le tribunal administratif pouvait rejeter par ordonnance la demande du requérant sans informer celui-ci qu'il entendait se fonder sur la circonstance que sa demande n'avait pas été présentée dans un délai raisonnable.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 13 juillet 2016,,, n° 387763, p. 340.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
