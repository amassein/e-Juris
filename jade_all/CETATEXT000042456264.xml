<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042456264</ID>
<ANCIEN_ID>JG_L_2020_10_000000428480</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/45/62/CETATEXT000042456264.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 21/10/2020, 428480, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428480</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BUK LAMENT - ROBILLOT ; SCP GATINEAU, FATTACCINI, REBEYROL</AVOCATS>
<RAPPORTEUR>M. Charles-Emmanuel Airy</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:428480.20201021</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal des pensions de Saint-Denis de La Réunion d'annuler la décision du 9 mai 2012 par laquelle le ministre de la défense a rejeté sa demande tendant au bénéfice d'une pension militaire d'invalidité au taux de 20 % à raison de douleurs abdominales à type de brûlures épigastriques post prandiales avec troubles de transit intestinal et sensibilité abdominale. Par un jugement n° 12/00006 du 10 février 2015, ce tribunal a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 16/02 du 24 février 2016, la cour régionale des pensions de Saint-Denis de la Réunion a rejeté l'appel formé par M. A... contre ce jugement.<br/>
<br/>
              Par une décision n° 397796 du 17 novembre 2017, le Conseil d'Etat, statuant au contentieux, a annulé cet arrêt et renvoyé l'affaire à la cour régionale des pensions de Paris.<br/>
<br/>
              Par un arrêt n° RG 17/21878 du 14 décembre 2018, cette cour a annulé le jugement du 10 février 2015 du tribunal des pensions de Saint-Denis de La Réunion, annulé la décision du 9 mai 2012 par laquelle le ministre de la défense avait rejeté la demande de M. A..., et a accordé à ce dernier le bénéfice d'une pension militaire d'invalidité au taux de 20 % à raison de ses pathologies oesophagiques, gastriques et intestinales contractées au Tchad en opérations extérieures en 1986, 1987 et 1988.  <br/>
<br/>
<br/>
Par un pourvoi et un mémoire en réplique, enregistrés les 27 février et 31 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, la ministre des armées demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. A.... <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des pensions militaires d'invalidité et des victimes de la guerre ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alexandre Koutchouk, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, Rebeyrol, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A... a demandé le 16 février 2009 le bénéfice d'une pension militaire d'invalidité au taux de 20 % à raison de douleurs abdominales et de troubles du transit intestinal. Par une décision du 9 mai 2012, le ministre de la défense a rejeté sa demande au motif que ces infirmités n'étaient pas de nature à justifier un taux d'invalidité permettant d'atteindre le seuil minimum de 10 % requis par les articles L. 4 et L. 5 du code des pensions militaires d'invalidité et des victimes de la guerre pour l'octroi d'une pension. Par un jugement du 10 février 2015, le tribunal des pensions de Saint-Denis de La Réunion a rejeté la demande de M. A... tendant à l'annulation de cette décision et à l'octroi d'une pension. Par une décision du 17 novembre 2017, le Conseil d'Etat, statuant au contentieux, a annulé l'arrêt du 24 février 2016 par lequel la cour régionale des pensions de Saint-Denis de la Réunion avait rejeté l'appel formé par M. A... contre ce jugement et renvoyé l'affaire à cette cour. La ministre des armées se pourvoit en cassation contre l'arrêt du 14 décembre 2018 par lequel la cour régionale des pensions de Paris a annulé le jugement du tribunal départemental des pensions et la décision du 9 mai 2012 et a fixé à 20%, à compter du 16 février 2009, le taux d'invalidité résultant des pathologies oesophagiques, gastriques et intestinales contractées par M. A... au Tchad en opérations extérieures.<br/>
<br/>
              2. Il résulte de l'accusé de réception figurant dans les pièces du dossier de procédure que l'arrêt de la cour régionale des pensions de Paris du 14 décembre 2018 a été notifié par le greffe de cette cour à la ministre des armées le 26 décembre 2018. Le pourvoi de la ministre, enregistré le 27 février 2019 au secrétariat du contentieux du Conseil d'Etat, a donc été formé dans le délai de recours contentieux de deux mois à compter de la notification de l'arrêt, prévu par l'article R.733-1 du code des pensions militaires d'invalidité et des victimes de guerre. Il s'ensuit que la fin de non-recevoir opposée par M. A... et tirée de la tardiveté du pourvoi de la ministre doit être écartée. <br/>
<br/>
              3. Aux termes de l'article L. 2 du code des pensions militaires d'invalidité et des victimes de la guerre, dans sa version alors applicable : " Ouvrent droit à pension :/ (...). 2° Les infirmités résultant de maladies contractées par le fait ou à l'occasion du service ;/ (...) ". Aux termes de l'article L. 3 du même code : " Lorsqu'il n'est pas possible d'administrer ni la preuve que l'infirmité ou l'aggravation résulte d'une des causes prévues à l'article L. 2, ni la preuve contraire, la présomption d'imputabilité au service bénéficie à l'intéressé à condition:/(...) ;/ 2° S'il s'agit d'une maladie, qu'elle n'ait été constatée qu'après le quatre-vingt-dixième jour de service effectif et avant le trentième jour suivant le retour du militaire dans ses foyers ;/ 3° En tout état de cause, que soit établie, médicalement, la filiation entre la blessure ou la maladie ayant fait l'objet de la constatation et l'infirmité invoquée. En cas d'interruption de service d'une durée supérieure à quatre-vingt-dix jours, la présomption ne joue qu'après le quatre-vingt-dixième jour suivant la reprise du service actif. / La présomption définie au présent article s'applique exclusivement aux constatations faites, (...) au cours d'une expédition déclarée campagne de guerre (...) ". Il résulte de ces dispositions que, s'il ne peut prétendre au bénéfice de la présomption légale d'imputabilité, le demandeur d'une pension doit rapporter la preuve de l'existence d'un fait précis ou de circonstances particulières de service à l'origine de l'affection qu'il invoque. Cette preuve ne saurait résulter de la seule circonstance que l'infirmité soit apparue durant le service, ni d'une hypothèse médicale, ni d'une vraisemblance, ni d'une probabilité, aussi forte soit-elle.<br/>
<br/>
              4. Pour juger que M. A... apportait, contrairement à ce que soutenait la ministre des armées, la preuve, en application des dispositions précitées de l'article L. 2 du code des pensions militaires d'invalidité et des victimes de guerre, de l'imputabilité au service des pathologies dont il souffrait, la cour régionale des pensions de Paris s'est fondée sur les seules mentions portées en octobre 1987 sur le registre des constatations des blessures, infirmités et maladies survenant pendant le service selon lesquelles M. A..., alors qu'il se trouvait en poste isolé au cours d'une mission opérationnelle sur le territoire tchadien du 14 février au 5 mai 1986, se serait plaint à plusieurs reprises de violentes douleurs au ventre qui l'auraient conduit à consulter un médecin tchadien qui aurait constaté une gastrite le 25 avril 1986 et précisant que la maladie n'avait pu faire l'objet ni d'une constatation médicale, ni d'une inscription sur les pièces médicales de l'intéressé avant son retour. En statuant ainsi, sans rechercher si l'affection en cause était en relation avec un fait précis ou des circonstances particulières de service à l'origine de celle-ci, la cour régionale des pensions a méconnu les dispositions de l'article L. 2 du code des pensions militaires d'invalidité et des victimes de guerre.<br/>
<br/>
              5. La ministre des armées est, par suite, sans qu'il soit besoin de se prononcer sur l'autre moyen de son pourvoi, fondée à demander l'annulation de l'arrêt qu'elle attaque. <br/>
<br/>
              6. Aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire ". Le Conseil d'Etat étant saisi, en l'espèce, d'un second pourvoi en cassation, il lui incombe de régler l'affaire au fond.<br/>
<br/>
              7. En premier lieu, si, après avoir estimé que les affections dont souffrait M. A... ne conduisaient pas à des taux d'invalidité suffisants pour justifier l'octroi d'une pension militaire d'invalidité, le tribunal des pensions de Saint-Denis de la Réunion a ajouté " qu'au-delà de ces éléments ", il n'était pas établi que ces affections étaient en lien avec la mission effectuée au Tchad de février à mai 1986, il ressort du jugement qu'un tel motif revêtait un caractère surabondant. M. A... n'est, par suite et en tout état de cause, pas fondé à soutenir que le tribunal régional des pensions aurait entaché son jugement d'irrégularité en fondant sa décision sur le motif, non invoqué par le ministre et non soumis au débat contradictoire, tiré de l'absence d'imputabilité au service des affections dont il souffrait.<br/>
<br/>
              8. En deuxième lieu, il résulte de l'instruction qu'aucun diagnostic médical n'a conclu à l'imputabilité des pathologies en litige à un fait ou des circonstances particulières de service. En se bornant à faire état de ce que son isolement, au cours du service, ne lui avait permis de consulter que le 25 avril 1986 un médecin au Tchad, dont le diagnostic n'a pas été consigné par écrit, M. A... n'apporte pas la preuve, qui lui incombe en application des dispositions de l'article L. 2 du code des pensions militaires d'invalidité et des victimes de la guerre, que ses pathologies ont une cause certaine, directe et déterminante dans le service. <br/>
<br/>
              9. En troisième lieu, il résulte de l'instruction que la constatation officielle des affections en litige, ayant donné lieu à mention sur le registre des constatations, n'est intervenue que le 12 octobre 1987, à la suite d'un examen médical du 8 octobre 1987, la simple mention, sur le livret médical, lors d'une visite d'aptitude annuelle réalisée le 12 septembre 1986, d'une sensation de brûlure épigastrique après les repas ne pouvant en tenir lieu. M. A... ayant participé à des opérations extérieures au Tchad du 14 février au 5 mai 1986, puis en République Centrafricaine du 12 mai au 22 juillet 1986, puis à nouveau au Tchad du 28 novembre 1986 au 7 février 1987, cette constatation a ainsi été effectuée au-delà du délai de trente jours prévu par les dispositions précitées du 2° de l'article L. 3 du code des pensions militaires d'invalidité et des victimes de la guerre, dans sa rédaction applicable au litige. M. A... ne peut par suite bénéficier de la présomption d'imputabilité instituée par ces dispositions. <br/>
<br/>
              10. Il résulte de ce qui précède que le requérant n'est pas fondé à se plaindre de ce que, par le jugement attaqué, le tribunal des pensions a rejeté sa demande tendant à l'attribution d'une pension militaire d'invalidité à raison de douleurs abdominales à type de brûlures épigastriques post prandiales avec troubles de transit intestinal et sensibilité abdominale.<br/>
<br/>
              11. La requête de M. A... doit par suite être rejetée, y compris les conclusions présentées sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour régionale des pensions de Paris du 14 décembre 2018 est annulé.<br/>
Article 2 : La requête d'appel de M. A... est rejetée.<br/>
Article 3 : Les conclusions présentées devant le Conseil d'Etat sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la ministre des armées et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
