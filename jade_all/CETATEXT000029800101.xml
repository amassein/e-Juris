<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029800101</ID>
<ANCIEN_ID>JG_L_2014_10_000000372518</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/80/01/CETATEXT000029800101.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème SSJS, 15/10/2014, 372518, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-10-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372518</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP JEAN-PHILIPPE CASTON ; SCP BOULLOCHE</AVOCATS>
<RAPPORTEUR>Mme Charline Nicolas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:372518.20141015</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 1er octobre et 31 décembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la Mutuelle des architectes français, dont le siège est 9 rue de l'Amiral Hamelin à Paris cedex 16 (75783) ; la Mutuelle des architectes français demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 12NC00633 du 1er août 2013 par lequel la cour administrative d'appel de Nancy a rejeté sa requête tendant, en premier lieu, à l'annulation du jugement n° 0702151 du 22 mars 2012 du tribunal administratif de Strasbourg rejetant sa demande tendant à la condamnation, in solidum, des sociétés Sanichauf et Oth Est à lui verser la somme de 135 459,93 euros au titre de la réparation des désordres affectant le bâtiment des archives de la communauté urbaine de Strasbourg et, en second lieu, à la condamnation, in solidum, des sociétés Sanichauf et Oth Est à lui verser la somme de 135 459,93 euros, au titre de la réparation des désordres affectant le bâtiment des archives de la communauté urbaine de Strasbourg ; <br/>
<br/>
              2°) réglant l'affaire au fond, de condamner les sociétés Sanichauf et Oth Est à lui verser la somme de 135 459,93 euros, outre les intérêts à compter du 21 avril 2007 ainsi que leur capitalisation ; <br/>
<br/>
              3°) de mettre à la charge des sociétés Sanichauf et Oth Est le versement de la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative ainsi que la contribution pour l'aide juridique ; <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des assurances ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Charline Nicolas, auditeur,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boulloche, avocat de la Mutuelle des architectes français, à la SCP Lyon-Caen, Thiriez, avocat de la société Sanichauf, et à la SCP Jean-Philippe Caston, avocat de la société Iosis Grand Est, venant aux droits de la société Oth Est ;<br/>
<br/>
<br/>
<br/>1. Considérant que la communauté urbaine de Strasbourg a, d'une part, conclu un marché de travaux pour la construction d'un bâtiment destiné aux archives communautaires dont la conception technique a été confiée au bureau d'études Oth Est et le lot " chauffage, ventilation, climatisation " attribué à la société Sanichauf, et, d'autre part, souscrit auprès de la Mutuelle des architectes français (M.A.F.), le 15 novembre 2001, une police d'assurances dommages-ouvrages ; que le 26 avril 2005, la communauté urbaine de Strasbourg a adressé à la M.A.F. une déclaration de sinistre portant sur ce bâtiment ; que la M.A.F. se pourvoit contre l'arrêt par lequel la cour administrative d'appel de Nancy a confirmé le rejet, par le tribunal administratif de Strasbourg, de son action dirigée contre la société Sanichauf et la société Oth Est, en raison de l'absence de subrogation dans les droits de la communauté urbaine de Strasbourg ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 121-12 du code des assurances : " L'assureur qui a payé l'indemnité d'assurance est subrogé, jusqu'à concurrence de cette indemnité, dans les droits et actions de l'assuré contre les tiers qui, par leur fait, ont causé le dommage ayant donné lieu à la responsabilité de l'assureur ; qu'il incombe à l'assureur qui entend bénéficier de la subrogation prévue par l'article L. 121-12 précité du code des assurances d'apporter la preuve du versement de l'indemnité d'assurance à son assuré, et ce par tout moyen " ; qu'il résulte de ces dispositions que la subrogation légale ainsi instituée est subordonnée au seul paiement à l'assuré de l'indemnité d'assurance en exécution du contrat d'assurance et ce, dans la limite de la somme versée ; que la circonstance qu'une telle indemnité n'a été accordée qu'à titre provisionnel n'est pas, par elle-même, de nature à faire obstacle à la subrogation ; qu'il appartient seulement à l'assureur, pour en bénéficier, d'apporter par tout moyen la preuve du paiement de l'indemnité ;<br/>
<br/>
              3. Considérant qu'en estimant que ni les deux documents informatiques sur papier libre intitulés " extraits de compte bancaire " de la M.A.F., comportant l'indication du débit de plusieurs sommes et des numéros de chèque en date des 23 février et 28 juillet 2006 et 30 novembre 2011, ni la lettre d'acceptation d'une indemnité, en date du 10 février 2006, ne pouvaient à elles seules justifier du paiement des indemnités par la M.A.F. à la communauté urbaine de Strasbourg, alors que la lettre d'acceptation, par celle-ci, de l'indemnité proposée par la M.A.F. établissait, en tout état de cause, l'existence du versement par la requérante d'une provision de 50 871,68 euros au profit de la communauté urbaine, la cour administrative d'appel de Nancy a dénaturé les pièces du dossier ; que, dès lors, son arrêt doit être annulé ;<br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la M.A.F. qui n'est pas, dans la présente instance, la partie perdante ;  qu'il y a lieu, en revanche, de mettre à la charge des sociétés Sanichauf et Iosis Grand Est la somme de 1 518 euros à verser chacune à la M.A.F. au titre des dispositions des articles L. 761-1 et R. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 1er août 2013 est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy.<br/>
Article 3 : Les sociétés Sanichauf et Iosis Grand Est verseront chacune une somme de 1 518 euros à la Mutuelle des architectes français au titre des dispositions des articles L. 761-1 et R. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions des sociétés Sanichauf et Iosis Grand Est présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à la Mutuelle des architectes français, à la société Sanichauf et à la société Iosis Grand Est.<br/>
Copie en sera adressée pour information à la communauté urbaine de Strasbourg.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
