<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029918558</ID>
<ANCIEN_ID>JG_L_2014_12_000000372415</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/91/85/CETATEXT000029918558.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  8ème sous-section jugeant seule, 17/12/2014, 372415, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-12-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372415</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 8ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Anton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:372415.20141217</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi, enregistré le 26 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre délégué, chargé du budget ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 1102080 du 23 juillet 2013 par lequel le tribunal administratif de Rouen, statuant sur la demande de la société Oseo, l'a déchargée de la cotisation supplémentaire de taxe foncière sur les propriétés bâties à laquelle elle a été assujettie en 2010 dans les rôles de la commune de Lillebonne ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de cette société ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Anton, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, avocat de la société Bpifrance Financement ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que la société Oseo, crédit-bailleur, aux droits de laquelle vient la société Bpifrance Financement, a conclu avec la Compagnie française Eco Huile, crédit-preneur, un contrat de crédit-bail pour la construction et la mise en service de deux bacs métalliques destinés au recyclage d'huiles usagées ; que la construction de ces bacs a été autorisée par permis de construire ; que l'administration fiscale a estimé que ces bacs, les cuves de rétention au-dessus desquelles ils sont placés ainsi que l'ensemble des éléments faisant corps avec ces installations, tels que le système de détection d'incendie, les pompes de relevage, l'alimentation électrique, les tuyauteries et les études relatives à l'implantation de cet équipement, devaient être assujettis à la taxe foncière sur les propriétés bâties ; qu'elle a mis à ce titre des cotisations de taxe foncière sur les propriétés bâties à la charge de la société Oseo, en sa qualité de propriétaire des bacs et des cuves, et à la charge de la Compagnie française Eco Huile, à raison des autres éléments faisant corps avec ces bacs, inscrits pour leur part au bilan de cette société ; que le ministre délégué, chargé du budget se pourvoit en cassation contre le jugement du 23 juillet 2013 par lequel le tribunal administratif de Rouen a déchargé la société Oseo de la cotisation ainsi mise à sa charge ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 1380 du code général des impôts : " La taxe foncière est établie annuellement sur les propriétés bâties sises en France à l'exception de celles qui en sont expressément exonérées par les dispositions du présent code " ; qu'aux termes de l'article 1381 du même code : " Sont également soumis à la taxe foncière sur les propriétés bâties : 1° Les installations destinées à abriter des personnes ou des biens ou à stocker des produits ainsi que les ouvrages en maçonnerie présentant le caractère de véritables constructions tels que, notamment, les cheminées d'usine, les réfrigérants atmosphériques, les formes de radoub, les ouvrages servant de support aux moyens matériels d'exploitation (...) " ; qu'aux termes de l'article 1382 du même code : " Sont exonérés de la taxe foncière sur les propriétés bâties : (...) 11° Les outillages et autres installations et moyens matériels d'exploitation des établissements industriels à l'exclusion de ceux visés aux 1° et 2° de l'article 1381 (...) "  ; qu'il résulte notamment de ces dispositions que les installations  destinées à stocker des produits et les ouvrages en maçonnerie présentant le caractère de véritables constructions au sens du 1° de l'article 1381 ne peuvent être exonérés de cette taxe au titre du 11° de l'article 1382, alors même qu'ils participeraient directement à l'activité industrielle de l'établissement ; <br/>
<br/>
              3. Considérant, en premier lieu, qu'il résulte des énonciations du jugement attaqué, qu'après un premier tri, les bacs objets du présent litige servent à l'homogénéisation et à la décantation des huiles usagées, lesquelles sont ensuite envoyées en distillation atmosphérique ; qu'ils n'interviennent pas en amont du cycle de recyclage des huiles usagées, mais, au contraire, au même titre que les cuves de rétention qui sont placées en-dessous, dont le seul objet est d'en recueillir les écoulements accidentels, participent directement au processus de transformation des huiles usagées ; qu'en déduisant de ces faits, qu'il a souverainement appréciés, d'une part, que les bacs et les cuves en cause ainsi que les éléments faisant corps avec eux constituaient des outillages, installations et moyens matériels d'exploitation au sens du 11° de l'article 1382 précité du code général des impôts, d'autre part, qu'ils ne constituaient pas des installations de stockage au sens du 1° de l'article 1381, le tribunal n'a pas commis d'erreur de qualification juridique ; <br/>
<br/>
              4. Considérant, en second lieu, que si le ministre soutient que le tribunal était tenu de regarder l'ensemble formé par ces bacs et ces cuves, ainsi que les éléments faisant corps avec eux, comme de véritables constructions installées à perpétuelle demeure relevant du champ d'application du 1° de l'article 1381 précité du code général des impôts, il ressort des pièces du dossier soumis au juge du fond que l'administration fiscale s'était bornée à soutenir que ces bacs constituaient " dans leur ensemble, une construction passible de la taxe foncière " au seul motif qu'ils étaient des " réservoirs de travail assimilables à des installations de stockage " et que les " cuves de rétention sous-jacentes " en constituaient " des aménagements indissociables ", sans assortir ses écritures de la production d'éléments qui auraient permis au tribunal administratif de vérifier si les bacs et les cuves en litige formaient un ouvrage en maçonnerie présentant le caractère d'une véritable construction au sens de cet article ; que, dans ces conditions, compte tenu des éléments versés au dossier, le ministre n'est pas fondé à soutenir, en tout état de cause, que le tribunal aurait dénaturé les pièces du dossier, inexactement qualifié les faits soumis à son examen et commis une erreur de droit en ne regardant pas ces bacs et les cuves placées en-dessous et les éléments faisant corps avec eux, comme des installations assujetties à la taxe foncière sur les propriétés bâties en application du 1° de l'article 1381  au titre des ouvrages en maçonnerie présentant le caractère de véritables constructions ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que le pourvoi du ministre délégué, chargé du budget doit être rejeté ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions de la société Bpifrance Financement au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre délégué, chargé du budget est rejeté.<br/>
Article 2 : Les conclusions de la société Bpifrance Financement présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée au ministre des finances et des comptes publics et à la société Bpifrance Financement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
