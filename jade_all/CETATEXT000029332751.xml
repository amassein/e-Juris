<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029332751</ID>
<ANCIEN_ID>JG_L_2014_07_000000381551</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/33/27/CETATEXT000029332751.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 24/07/2014, 381551, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>381551</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2014:381551.20140724</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 20 juin 2014 au secrétariat du contentieux du Conseil d'Etat, présentée pour le Groupe d'information et de soutien des immigré-e-s (GISTI), dont le siège est 3, villa Marcès à Paris (75011), représenté par son président, l'association nationale d'assistance aux frontières pour les étrangers (ANAFE), dont le siège social est 21 ter rue Voltaire à Paris (75011), représentée par son co-président, l'association AIDES, dont le siège social est 14 rue Scandicci à Pantin (93508), représentée par son président, l'association de soutien aux Amoureux au Ban Public, dont le siège social est 46 boulevard des Batignolles à Paris (75017), représentée par son président, la CIMADE, Service oecuménique d'entraide, dont le siège social est situé 64, rue Clisson à Paris (75013), représentée par sa présidente, la Fédération des associations de solidarité avec les travailleur-euse-s immigré-e-s (FASTI), dont le siège est 58, rue des Amandiers à Paris (75020), représentée par son co-président, la Ligue des droits de l'homme (LDH), dont le siège est 138, rue Marcadet à Paris (75018), représentée par son président, l'association Médecins du monde, dont le siège social est situé 62, rue Marcadet à Paris (75018), représentée par son président, et le Syndicat de la Magistrature, dont le siège social est situé 12-14, rue Charles Fourrier à Paris (75013), représenté par sa présidente ; les requérants demandent au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution des articles 5 et 14 II de l'ordonnance n° 2014-464 du 7 mai 2014 portant extension et adaptation à Mayotte du code de l'entrée et du séjour des étrangers et du droit d'asile (partie législative) ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 1 000 euros par requérant au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              ils soutiennent que : <br/>
              - ils ont intérêt à agir ;<br/>
              - la condition d'urgence est remplie ;<br/>
              - les dispositions contestées portent une atteinte grave et immédiate à leurs intérêts, aux intérêts qu'ils défendent et à l'intérêt public ;<br/>
              - l'absence de recours suspensif de plein droit et de jour franc automatique porte une atteinte grave et immédiate aux droits et libertés fondamentaux des étrangers garantis par la convention européenne des droits de l'homme  ; <br/>
              - l'absence de jour franc accordé de plein droit empêche les étrangers entrés à Mayotte d'avoir connaissance de l'existence d'une procédure d'asile à la frontière régie par l'article R. 213-2 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), en méconnaissance des exigences découlant des directives " retour " et " procédures "  ; <br/>
              - l'absence de recours de plein droit suspensif contre les mesures d'éloignement méconnaît l'exigence de recours effectif posée par l'article 13 de la convention européenne de sauvegarde des droits de l'homme ; qu'en particulier, elle rend ineffectif le recours à la Cour nationale de droit d'asile dès lors que le préfet refuse le séjour au titre de l'article L. 741-4 du CESEDA ;<br/>
<br/>
<br/>
              Vu l'ordonnance dont la suspension de l'exécution est demandée ;<br/>
<br/>
                    Vu la copie de la requête aux fins d'annulation de l'ordonnance contestée ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 4 juillet 2014, présenté par le ministre de l'intérieur, qui conclut au rejet de la requête ; <br/>
              il soutient que :<br/>
              - quatre des huit requérants, l'association AIDES, l'association de soutien aux Amoureux au ban public, l'association Médecins du Monde et le Syndicat de la Magistrature, sont dépourvus d'intérêt à agir ; <br/>
              - la condition d'urgence n'est pas remplie ; que, d'une part, la préfecture de Mayotte ne faisant pas application de l'article L. 213-2 du CESDA, la non application de la règle dite du " jour franc " prévue par l'article 5 de l'ordonnance contestée n'est pas susceptible de porter une atteinte grave et immédiate aux intérêts invoqués par les requérants ; que, d'autre part, l'article 14 II de l'ordonnance litigieuse ne fait que maintenir l'état du droit qui prévalait antérieurement ; qu'en outre, n'étant pas en contradiction avec les garanties procédurales prévues par la Convention européenne des droits de l'homme, ses dispositions ne peuvent être regardée comme préjudiciant de manière suffisamment grave et immédiate à un intérêt public et aux intérêts des requérants ; <br/>
              - les dispositions litigieuses ne préjudicient pas de manière grave et immédiate au droit d'asile ; <br/>
              - le défaut d'urgence résulte également de l'intérêt public que représente la lutte contre l'immigration irrégulière dans le contexte particulier à Mayotte ; <br/>
              - les dérogations dans l'application du CESEDA à Mayotte et traduites dans les dispositions contestées sont conformes à la Constitution, à la Convention européenne des droits de l'homme et au droit de l'Union ;<br/>
<br/>
              Vu le mémoire en réplique, enregistré le 9 juillet 2014, présenté pour Gisti et autres, qui reprend les conclusions et les moyens de sa requête ; <br/>
<br/>
              il soutient que :<br/>
              - le moyen tiré du défaut d'intérêt à agir des quatre requérants susmentionnés ne saurait prospérer compte tenu de l'impact de l'exécution des dispositions contestées sur leur activité à Mayotte et, par suite, la requête doit être déclarée recevable ; <br/>
              - les obligations qui résultent de la convention européenne des droits de l'homme s'imposent face à l'intérêt public dont se prévaut le ministre pour justifier l'exécution des dispositions contestées ; <br/>
<br/>
              - les refus d'entrée opposés par les autorités françaises à Mayotte constituent une pratique qui entre dans le champ d'application de directive 2008/115/CE du 16 décembre 2008 et, dès lors, justifie l'urgence à appliquer le droit commun concernant le refus d'être rapatrié avant l'expiration du délai d'un jour franc ;  <br/>
<br/>
              - l'objection tirée de l'état constant du droit applicable à Mayotte en matière de recours contre les obligations de quitter le territoire doit céder devant l'atteinte grave et immédiate, d'une part, aux intérêts des requérants et, d'autre part, à l'intérêt public, en ce qu'il contredit notamment l'objectif poursuivi par le législateur de mettre en conformité le droit applicable à Mayotte avec le droit de l'Union ; <br/>
<br/>
              - l'article 5 de l'ordonnance contestée est entaché d'un doute sérieux quant à sa légalité au regard du droit de l'Union, en ce qu'il régit la situation d'étrangers qui ont fait l'objet d'un refus d'entrée et s'oppose à l'absence de jour franc accordé automatiquement ;<br/>
<br/>
              - le droit à un recours effectif que les autorités françaises sont tenues de garantir aux étrangers à l'encontre de toute mesure d'éloignement dont ils auraient fait l'objet doit s'entendre, au sens de la convention européenne des droits de l'homme telle qu'interprétée par la Cour chargée d'en assurer le respect, comme étant un recours suspensif de plein droit ;  <br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, le Gisti et autres, d'autre part, le ministre de l'intérieur et le ministre des outre-mer ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 10 juillet à 9 heures 30 au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Spinosi, avocat au Conseil d'Etat et à la Cour de cassation, avocat des requérants ;<br/>
<br/>
              - les représentants du GISTI ;<br/>
<br/>
              - le représentant de la CIMADE ;<br/>
<br/>
              - le représentant de la FASTI ;<br/>
<br/>
              - les représentants du ministre de l'intérieur ; <br/>
<br/>
              - la représentante du ministre des outre-mer ;<br/>
              et à l'issue de laquelle l'instruction a été prolongée jusqu'au mercredi  23 juillet 2014 ; <br/>
<br/>
              Vu les observations, enregistrées le 17 juillet 2014, présentées par le Défenseur des droits ; <br/>
<br/>
              Vu les nouveaux mémoires, enregistrés les 16 et 18 juillet 2014, présentés pour le Gisti et autres, qui reprend les conclusions de leur requête avec les mêmes moyens ; <br/>
<br/>
              Vu les nouveaux mémoires, enregistrés les 17 et 18 juillet 2014, présentés par le ministre de l'intérieur, qui reprend les conclusions de son précédent mémoire ; <br/>
                          Vu les autres pièces du dossier ;<br/>
              Vu la Constitution ; <br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
      Vu le traité sur l'Union européenne ;<br/>
      Vu la Charte des droits fondamentaux de l'Union européenne ;<br/>
      Vu la directive 2003/9/CE du Conseil du 27 janvier 2003 ;<br/>
<br/>
      Vu la directive 2005/85/CE du Conseil du 1er décembre 2005 ;<br/>
<br/>
              Vu la directive 2008/115/CE du Parlement européen et du Conseil du 16 décembre 2008 ;<br/>
      Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
      Vu la loi n° 2012-1270 du 20 novembre 2012 ;<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
              2. Considérant que les requérants demandent la suspension de l'exécution des articles 5 et 14 II de l'ordonnance du 7 mai 2014 portant extension et adaptation à Mayotte du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) ;  <br/>
<br/>
              3. Considérant que l'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celle-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ; que l'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire ;<br/>
<br/>
              4. Considérant, en premier lieu, que l'article 5 de l'ordonnance litigieuse a pour objet d'écarter, à Mayotte, l'application de la règle selon laquelle tout étranger faisant l'objet d'un refus d'entrée en France, sur le fondement de l'article L. 213-2 du CESEDA, peut refuser d'être rapatrié avant l'expiration du délai d'un jour franc ; qu'il ressort des indications fournies par le ministre de l'intérieur et confirmées par les requérants que les ressortissants de pays étrangers qui sont interceptés en mer ne font pas l'objet, à Mayotte, d'un refus d'entrée sur le fondement de l'article L. 213-2 du CESEDA mais d'une procédure d'éloignement forcé ; que, dans ces conditions, en l'absence, en l'état des pratiques des services préfectoraux, de mise en oeuvre du régime posé à cet article, l'absence d'application de la règle dite du " jour franc " n'est pas de nature à porter atteinte, de manière grave et immédiate, aux intérêts défendus par les requérants ;<br/>
<br/>
              5. Considérant, en second lieu, que l'article 14 II de l'ordonnance litigieuse étend à Mayotte le régime spécifique défini à l'article L. 514-1 du CESEDA, qui s'applique à la Guyane et à Saint-Martin, en vertu duquel le recours dirigé contre les obligations de quitter le territoire français est dépourvu de caractère suspensif contrairement à celui qui est prévu à l'article L. 512-1 du même code ; qu'il apparaît, d'une part, que l'édiction de ces dispositions ne modifie pas l'état du droit sur ce point, l'ordonnance du 26 avril 2000 relative aux conditions d'entrée et de séjour des étrangers à Mayotte qui était applicable jusqu'à l'intervention de l'ordonnance du 7 mai 2014 n'ayant pas organisé de recours suspensif contre les mesures d'éloignement forcé ; que, d'autre part, il ressort des pièces du dossier, en particulier des éléments versés au dossier lors de l'audience et postérieurement à celle-ci, que, depuis l'arrêt de la Cour européenne des droits de l'homme du 13 décembre 2012, la pratique de la préfecture de Mayotte consiste à différer la mise en oeuvre des mesures d'éloignement forcé dans les cas où l'étranger qui en fait l'objet a saisi le juge des référés du tribunal administratif et ce, jusqu'à ce que ce dernier se soit prononcé ; que, par une note du 3 avril 2013, le ministre de l'intérieur a prescrit au préfet de Mayotte de se conformer à cette pratique qu'implique d'ailleurs la mise en oeuvre des dispositions contestées dans le respect des exigences du droit au recours effectif ; qu'enfin, ainsi que l'avait annoncé le ministre de l'intérieur lors de l'audience, un projet de loi relatif au droit des étrangers en France a été examiné lors du conseil des ministres du 23 juillet 2014 qui prévoit qu'à Mayotte, l'obligation de quitter le territoire français ne peut faire l'objet d'une exécution d'office si l'étranger a saisi le tribunal administratif d'une demande sur le fondement de l'article L. 521-2 du code de justice administrative avant que le juge des référés n'ai informé les parties de la tenue ou non d'une audience ni avant, si les parties ont été informées de la tenue d'une telle audience, que le juge n'ait statué sur la demande ; qu'il résulte de l'ensemble de ces éléments que, contrairement à ce qui est soutenu, l'exécution de l'article 14 II de l'ordonnance litigieuse n'est pas de nature à porter atteinte, de manière grave et immédiate, aux intérêts défendus par les requérants ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que la condition d'urgence, requise par l'article L. 521-1 du code de justice administrative pour justifier la suspension immédiate des dispositions contestées, n'est pas caractérisée ; que, sans qu'il soit besoin de statuer ni sur la fin de non recevoir soulevée par le ministre de l'intérieur ni sur l'existence d'un doute sérieux quant à la légalité des dispositions contestés, la requête du GISTI et autres doit donc être rejetée, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête du GISTI et autres est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée au Groupe d'information et de soutien des immigré-e-s (GISTI), premier requérant dénommé, au ministre de l'intérieur et au ministre des outre-mer. <br/>
 Les autres requérants seront informés de la présente ordonnance par Me A...Spinosi, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
