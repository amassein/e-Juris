<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036378505</ID>
<ANCIEN_ID>JG_L_2017_12_000000416494</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/37/85/CETATEXT000036378505.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 22/12/2017, 416494, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416494</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:416494.20171222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
              Mme D...C...et M. A...B...ont demandé au juge des référés du tribunal administratif de Grenoble, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, d'ordonner la suspension de l'exécution de l'arrêté du 30 novembre 2017 par lequel le préfet de la Drôme a placé M. B...en rétention administrative, d'autre part, d'enjoindre au préfet de la Drôme de lui délivrer une autorisation provisoire de séjour et, enfin, d'ordonner la suspension de l'exécution de l'arrêté du 18 octobre 2017 par lequel le préfet de la Drôme a fait obligation à M. B...de quitter le territoire français sans délai et a fixé le pays à destination vers lequel il pourra être reconduit jusqu'à ce que la cour administrative d'appel de Lyon ait statué sur le bien-fondé de son appel tendant à l'annulation du jugement n° 1705869 du 23 octobre 2017 du tribunal administratif de Grenoble. Par une ordonnance n° 1706769 du 6 décembre 2017, le juge des référés du tribunal administratif de Grenoble a rejeté leur demande.<br/>
              Par une requête et un mémoire, enregistrés les 12 et 19 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, Mme C...et M. B...demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) d'ordonner la suspension de l'exécution de l'arrêté du 18 octobre 2017 ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
              - la condition d'urgence est remplie du fait de l'imminence de l'éloignement de M. B...en exécution de l'arrêté du préfet de la Drôme du 18 octobre 2017 lui faisant obligation de quitter le territoire français sans délai ;<br/>
              - il est porté une atteinte grave et manifestement illégale à la liberté fondamentale de se marier garantie à l'article 12 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - l'arrêté contesté du préfet de la Drôme qui n'a pour seul objet que d'empêcher leur mariage est entaché de détournement de pouvoir ; <br/>
              - la saisine du préfet de la Drôme par le maire de Buis-les-Baronnies de leur projet de mariage du seul fait de l'irrégularité de la situation administrative de M. B...est illégale ;<br/>
              - le juge des référés du tribunal administratif de Grenoble a méconnu son office en ce qu'il a considéré que les conditions du référé-liberté n'étaient pas remplies au seul motif que le juge du principal a, par un jugement du 23 octobre 2017, rejeté leur demande d'annulation de l'arrêté préfectoral contesté.<br/>
<br/>
              Par un mémoire en défense, enregistré le 18 décembre 2017, le ministre d'Etat, ministre de l'intérieur conclut, à titre principal, au rejet de la requête pour irrecevabilité dès lors que le juge administratif s'est prononcé sur la demande d'annulation de l'arrêté litigieux du 18 octobre 2017 sur le fondement des dispositions du III de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, en l'absence de changements dans les circonstances de droit ou de fait survenus depuis l'intervention de la mesure d'éloignement et de son jugement. Il conclut, à titre subsidiaire, au rejet de la requête en faisant valoir que la condition d'urgence n'est pas remplie et qu'il n'est porté aucune atteinte grave et manifestement illégale à la liberté de se marier des requérants.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, notamment ses articles 12 et 14 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, Mme C...et M. B..., d'autre part, le ministre d'Etat, ministre de l'intérieur ;<br/>
              Vu le procès-verbal de l'audience publique du mercredi 20 décembre 2017 à 10 heures au cours de laquelle ont été entendus : <br/>
              - Me Bouzidi, avocat au Conseil d'Etat et à la Cour de cassation, avocat de Mme C... et M. B... ;<br/>
<br/>
              - les représentantes du ministre d'Etat, ministre de l'intérieur ;<br/>
<br/>
              et à l'issue de laquelle les juges des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". <br/>
<br/>
              2. Aux termes de l'article L. 511-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " I. - L'autorité administrative peut obliger à quitter le territoire français un étranger non ressortissant d'un Etat membre de l'Union européenne, (...) lorsqu'il se trouve dans l'un des cas suivants : (...) 2° Si l'étranger s'est maintenu sur le territoire français au-delà de la durée de validité de son visa (...) ; II. - L'autorité administrative peut, par une décision motivée, décider que l'étranger est obligé de quitter sans délai le territoire français : (...) 3° S'il existe un risque que l'étranger se soustraie à cette obligation. Ce risque peut être regardé comme établi, sauf circonstance particulière, dans les cas suivants : (...) ; b) Si l'étranger s'est maintenu sur le territoire français au-delà de la durée de validité de son visa (...) ; ". Aux termes de l'article L. 512-1 du même code : " (...) III. - En cas de placement en rétention en application de l'article L. 551-1, l'étranger peut demander au président du tribunal administratif l'annulation de l'obligation de quitter le territoire français, de la décision refusant un délai de départ volontaire, (...), dans un délai de quarante-huit heures à compter de leur notification, lorsque ces décisions sont notifiées avec la décision de placement en rétention. (...) L'étranger faisant l'objet d'une décision d'assignation à résidence prise en application de l'article L. 561-2 peut, dans le même délai, demander au président du tribunal administratif l'annulation de cette décision. Les décisions mentionnées au premier alinéa du présent III peuvent être contestées dans le même recours lorsqu'elles sont notifiées avec la décision d'assignation. ".<br/>
<br/>
              3. Il ressort des dispositions du III de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile que le législateur a entendu organiser une procédure spéciale afin que le juge administratif statue rapidement sur la légalité des mesures relatives à l'éloignement des étrangers, hors la décision refusant le séjour et les mesures d'expulsion, lorsque ces derniers sont placés en rétention ou assignés à résidence. Cette procédure est applicable quelle que soit la mesure d'éloignement, autre qu'un arrêté d'expulsion, en vue de l'exécution de laquelle le placement en rétention ou l'assignation à résidence ont été pris, y compris en l'absence de contestation de cette mesure. Il en résulte qu'il appartient à l'étranger qui entend contester une obligation de quitter le territoire français lorsqu'elle est accompagnée d'un placement en rétention administrative ou d'une mesure d'assignation à résidence, de saisir le juge administratif sur le fondement des dispositions du III de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile d'une demande tendant à leur annulation, assortie le cas échéant de conclusions à fin d'injonction. Cette procédure particulière est exclusive de celles prévues par le livre V du code de justice administrative. Il n'en va autrement que dans le cas où les modalités selon lesquelles il est procédé à l'exécution d'une telle mesure relative à l'éloignement forcé d'un étranger emportent des effets qui, en raison de changements dans les circonstances de droit ou de fait survenus depuis l'intervention de cette mesure et après que le juge, saisi sur le fondement de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, a statué ou que le délai prévu pour le saisir a expiré, excèdent ceux qui s'attachent normalement à sa mise à exécution.<br/>
<br/>
              4. Il résulte de l'instruction que, par deux arrêtés du 18 octobre 2017, le préfet de la Drôme a, en premier lieu, fait obligation à M. B...de quitter le territoire français sans délai et a fixé le pays à destination duquel il pourra être reconduit et, en second lieu, l'a assigné à résidence dans le département de la Drôme. Saisi par M. B...sur le fondement du III de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, le tribunal administratif de Grenoble a, par un jugement du 23 octobre 2017, rejeté sa demande tendant à l'annulation de ces deux arrêtés. Par un arrêté du 30 novembre 2017, le préfet de la Drôme a placé l'intéressé en rétention administrative. Le 1er décembre 2017, M. B...et Mme C..., avec qui il a un projet de mariage, ont saisi le juge des référés du tribunal administratif de Grenoble d'une requête tendant à la suspension de l'exécution des arrêtés du 18 octobre et du 30 novembre 2017 et à ce qu'il soit enjoint au préfet de la Drôme de lui délivrer une autorisation provisoire de séjour, jusqu'à ce que la cour administrative d'appel de Lyon ait statué sur l'appel formé par M. B...contre le jugement du tribunal administratif de Grenoble du 23 octobre 2017. Par une ordonnance du 2 décembre 2017, le juge des libertés et de la détention près le tribunal de grande instance de Lyon, saisi par l'intéressé, a annulé la mesure de rétention administrative. Mme C...et M. B...relèvent appel de l'ordonnance du 3 octobre 2017 par laquelle le juge des référés du tribunal administratif de Melun, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, et après avoir relevé que les conclusions dirigées contre la mesure de rétention étaient devenues sans objet, a rejeté leur requête.<br/>
<br/>
              5. Les requérants font valoir que c'est en commettant une illégalité que le maire de Buis-les-Baronnies, saisi du projet de mariage en sa qualité d'officier de l'état civil, a saisi la préfecture de la Drôme de ce projet, et que cette circonstance a des effets anormaux justifiant l'intervention du juge du référé-liberté. Cependant, pour rejeter la demande d'annulation de la mesure d'éloignement prise à son encontre, le tribunal administratif de Grenoble statuant sur le fondement du III. de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile par le jugement du 23 octobre 2017, après avoir relevé que M. B...avait fait l'objet d'une enquête administrative au regard de son projet de mariage, a estimé qu'il ne ressortait pas des pièces du dossier qui lui a été soumis que les décisions attaquées auraient eu pour motif déterminant de faire obstacle à son projet de mariage et a écarté pour ce motif le moyen tiré du détournement de pouvoir. Dès lors il ne résulte ni de l'instruction ni des échanges au cours de l'audience publique que soit survenu, depuis cette date, un élément nouveau de nature à rendre recevables les conclusions présentées devant le juge des référés saisi sur le fondement de l'article L. 521-2 du code de justice administrative.<br/>
<br/>
              6. Il résulte de ce qui précède que Mme C...et M. B...ne sont pas fondés à se plaindre de ce que, par l'ordonnance du 23 octobre 2017, le juge des référés du tribunal administratif de Grenoble a rejeté leurs conclusions tendant à la suspension de l'exécution de l'arrêté du 18 octobre 2017. Par suite, leur requête doit être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de Mme C...et M. B...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme D...C...et M. A...B...et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
