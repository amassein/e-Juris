<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027592837</ID>
<ANCIEN_ID>JG_L_2013_06_000000350498</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/59/28/CETATEXT000027592837.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 19/06/2013, 350498, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-06-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>350498</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Benjamin de Maillard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:350498.20130619</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée le 30 juin 2011 au secrétariat du contentieux du Conseil d'Etat, présentée par le syndicat des dentistes solidaires et indépendants, dont le siège est 14 rue Vavin, à Paris (75006) ; le syndicat des dentistes solidaires et indépendants demande au Conseil d'Etat :<br/>
              1°) d'annuler pour excès de pouvoir la décision du Conseil national de l'ordre des chirurgiens-dentistes portant sur le montant de la cotisation ordinale 2011 ;<br/>
              2°) d'annuler les dépenses excédant les missions du Conseil national de l'ordre des chirurgiens-dentistes portant sur l'exercice 2011 ;<br/>
              3°) d'enjoindre au Conseil national de l'ordre des chirurgiens-dentistes, d'une part, de produire son bilan prévisionnel 2011 en justifiant toutes les sommes budgétées, d'autre part, de communiquer l'ensemble des documents comptables et fiscaux, détaillés poste par poste et dépense par dépense, de l'ordre national des chirurgiens-dentistes pour l'année 2010 ;<br/>
              4°) de procéder à la nomination d'un expert-comptable ;<br/>
              5°) de déclarer illégale la double cotisation imposée au praticien qui exerce sous forme de société d'exercice libéral (SEL) ou de société quelle que soit la forme de cette structure et d'annuler les appels de cotisation adressés aux SEL ;<br/>
              6°) de mettre à la charge du Conseil national de l'ordre des chirurgiens-dentistes une somme de 7 000 euros à verser au syndicat dentistes solidaires et indépendants au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Benjamin de Maillard, Auditeur,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat du Conseil national de l'ordre des chirurgiens-dentistes ;<br/>
<br/>
<br/>
<br/>
<br/>
              Sans qu'il soit besoin de statuer sur les fins de non-recevoir soulevées par le Conseil national de l'ordre des chirurgiens-dentistes ;<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 4121-1 du code de la santé publique : " L'ordre des médecins, celui des chirurgiens-dentistes et celui des sages-femmes veillent au maintien des principes de moralité, de probité, de compétence et de dévouement indispensables à l'exercice de la médecine, de l'art dentaire, ou de la profession de sage-femme et à l'observation, par tous leurs membres, des devoirs professionnels, ainsi que des règles édictées par le code de déontologie prévu à l'article L. 4127-1. / Ils assurent la défense de l'honneur et de l'indépendance de la profession médicale, de la profession de chirurgien-dentiste ou de celle de sage-femme. / Ils peuvent organiser toutes oeuvres d'entraide et de retraite au bénéfice de leurs membres et de leurs ayants droit. / Ils accomplissent leur mission par l'intermédiaire des conseils départementaux, des conseils régionaux ou interrégionaux et du conseil national de l'ordre. " ; qu'aux termes du premier alinéa de l'article L. 4122-2 du même code, dont les dispositions sont applicables à l'ensemble des ordres nationaux des professions médicales : " Le conseil national fixe le montant de la cotisation versée à chaque ordre par toute personne inscrite au tableau, qu'elle soit physique ou morale. " ; Que la cotisation ainsi levée a pour objet de procurer à l'ordre les ressources nécessaires à son fonctionnement comme à l'accomplissement des missions que le législateur lui a confiées ; <br/>
<br/>
              2. Considérant que, pour l'application des dispositions du premier alinéa de l'article L. 4122-2 du code de la santé publique, la décision fixant le montant des cotisations pour l'année 2011 a retenu, comme d'ailleurs les décisions des années antérieures, un montant forfaitaire unique pour toutes les personnes physiques ou morales inscrites au tableau de l'ordre ; qu'il en résulterait selon le syndicat requérant une différence de traitement, contraire au principe d'égalité, entre les chirurgiens-dentistes exerçant en leur nom propre et ceux qui, exerçant comme associés d'une société, doivent la cotisation en qualité de personne physique alors que leur société doit la même cotisation et acquittent, de ce fait, une contribution plus élevée aux charges de l'ordre ;<br/>
<br/>
              3. Considérant que l'assujettissement à cotisation des personnes physiques et des personnes morales inscrites au tableau de l'ordre est prescrit par les dispositions législatives mentionnées ci-dessus ; que si elles ne font pas obstacle à ce que le conseil national fixe des modalités de calcul de la cotisation tenant compte de ce qu'une personne physique inscrite au tableau est associée d'une société elle-même soumise à cotisation, elles n'interdisent pas non plus, par elles-mêmes, la fixation d'une cotisation d'un montant identique pour la société d'une part et chacun des associés d'autre part  ; que compte tenu des charges particulières que représente pour l'ordre l'exercice de ses missions à l'égard des sociétés inscrites à son tableau, les chirurgiens-dentistes exerçant en leur nom propre et ceux exerçant comme associés d'une société ne se trouvent pas dans la même situation ; que la différence de traitement qui résulte, entre eux, de ce que le Conseil national de l'ordre des chirurgiens-dentistes n'a pas prévu de modulation de la cotisation due par les personnes morales n'est pas manifestement disproportionnée au regard de cette différence de situation ; qu'ainsi, le moyen du syndicat requérant doit être écarté ;<br/>
<br/>
              4. Considérant que le moyen tiré de ce que diverses dépenses du Conseil national de l'ordre des chirurgiens-dentistes, que le syndicat requérant qualifie d'irrégulières ou d'abusives, seraient étrangères à l'accomplissement par l'ordre des missions que la loi lui confère n'est pas assorti des précisions suffisantes pour en apprécier le bien-fondé et ne peut qu'être écarté ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin d'ordonner la production des documents comptables qu'il demande ou de procéder à la nomination d'un expert-comptable, que le syndicat dentistes solidaires et indépendants n'est pas fondé à demander l'annulation de la décision attaquée ; que doivent être rejetées, par voie de conséquence, ses conclusions à fin d'injonction et tendant au bénéfice des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du syndicat des dentistes solidaires et indépendants la somme de 2 000 euros à verser au Conseil national de l'ordre des chirurgiens-dentistes, au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : La requête du syndicat des dentistes solidaires et indépendants est rejetée.<br/>
Article 2 : Le syndicat des dentistes solidaires et indépendants versera au Conseil national de l'ordre des chirurgiens-dentistes une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au syndicat des dentistes solidaires et indépendants et au Conseil national de l'ordre des chirurgiens-dentistes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
