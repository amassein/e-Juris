<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028115146</ID>
<ANCIEN_ID>JG_L_2013_10_000000328643</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/11/51/CETATEXT000028115146.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 23/10/2013, 328643, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-10-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>328643</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT ; SCP LESOURD</AVOCATS>
<RAPPORTEUR>Mme Agnès Martinel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:328643.20131023</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu, avec les pièces qui y sont visées, la décision du 28 novembre 2011 par laquelle le Conseil d'Etat, statuant au contentieux sur le pourvoi enregistré sous le numéro 328643, présenté pour l'établissement national des produits de l'agriculture et de la mer (FranceAgriMer) et tendant à l'annulation de l'arrêt n° 07MA03743 du 30 mars 2009 par lequel la cour administrative d'appel de Marseille a rejeté l'appel que l'office national interprofessionnel des fruits, des légumes, des vins et de l'horticulture (Viniflhor) a formé contre le jugement n° 0501594 du 5 juin 2007 par lequel le tribunal administratif de Marseille a annulé, à la demande de l'organisation de producteurs Les Cimes, le titre de recettes n° 2005/00010 du 5 janvier 2005 du directeur de l'office national interprofessionnel des fruits, des légumes et de l'horticulture (Oniflhor) mettant à la charge de cette organisation la somme de 253 338 euros au titre du reversement des aides du fonds opérationnel reçues pour l'année 1998, a sursis à statuer jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur la question de savoir comment la faculté, ouverte par le paragraphe 4 de l'article 2 du règlement (CEE) n° 4045/89 du Conseil du 21 décembre 1989 relatif aux contrôles, par les Etats membres, des opérations faisant partie du système de financement par le FEOGA, section " garantie ", d'étendre la période contrôlée " pour des périodes (...) précédant ou suivant la période de douze mois " qu'il définit, peut être mise en oeuvre par un Etat membre, eu égard, d'une part, aux exigences de protection des intérêts financiers des Communautés, et d'autre part, au principe de sécurité juridique et à la nécessité de ne pas laisser aux autorités de contrôle un pouvoir indéterminé, et, en particulier, sur les questions de savoir :<br/>
<br/>
              1°) si la période contrôlée doit, en toute hypothèse, sous peine que le contrôle soit entaché d'une irrégularité dont le contrôlé pourrait se prévaloir à l'encontre de la décision tirant les conséquences des résultats de ce contrôle, s'achever au cours de la période de douze mois qui précède la période, dite " de contrôle ", au cours de laquelle les opérations de contrôle sont effectuées ;<br/>
<br/>
              2°) en cas de réponse positive à la question précédente, comment la faculté, expressément prévue par le règlement, d'étendre la période contrôlée pour des périodes " suivant la période de douze mois " doit s'entendre ;<br/>
<br/>
              3°) en cas de réponse négative à la première question, si la période contrôlée doit néanmoins, sous peine que le contrôle soit entaché d'une irrégularité dont le contrôlé pourrait se prévaloir à l'encontre de la décision tirant les conséquences des résultats de ce contrôle, comporter une période de douze mois qui s'achève au cours de la période de contrôle précédant celle durant laquelle le contrôle a lieu, ou bien si le contrôle peut ne porter que sur une période qui s'achève avant le début de la période de contrôle précédente ; <br/>
<br/>
              Vu l'arrêt n° C-671/11 à C-676/11 du 13 juin 2013 par lequel la Cour de justice de l'Union européenne s'est prononcée sur ces questions ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le traité sur le fonctionnement de l'Union européenne ;<br/>
<br/>
              Vu le règlement (CEE) n° 4045/89 du Conseil du 21 décembre 1989 ;<br/>
<br/>
              Vu le règlement (CE) n° 3094/94 du Conseil du 12 décembre 1994 ;<br/>
<br/>
              Vu le règlement (CE, Euratom) n° 2988/95 du Conseil du 18 décembre 1995 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Agnès Martinel, Maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Balat, avocat de l'Etablissement national des produits de l'agriculture et de la mer ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à l'issue d'un contrôle intervenu en novembre 2000 et janvier 2001, réalisé par l'Agence centrale des organismes d'intervention dans le secteur agricole (ACOFA) sur le fondement du règlement (CEE) n° 4045/89 du Conseil du 21 décembre 1989 relatif aux contrôles, par les Etats membres, des opérations faisant partie du système de financement par le Fonds européen d'orientation et de garantie agricole (FEOGA), section " garantie ", le directeur de l'Office national interprofessionnel des fruits, des légumes et de l'horticulture (Oniflhor) a émis, le 5 janvier 2005, à l'encontre de l'organisation de producteurs Les Cimes, un titre de recettes d'un montant de 253 338 euros correspondant au reversement des aides perçues par cette organisation au titre du fonds opérationnel 1998 ; que, par un jugement du 5 juin 2007, le tribunal administratif de Marseille a, à la demande de l'organisation de producteurs Les Cimes, annulé ce titre de recettes ; que l'établissement national des produits de l'agriculture et de la mer (FranceAgriMer), venant aux droits de l'Office national interprofessionnel des fruits, des légumes, des vins et de l'horticulture (Viniflhor), lui-même venu aux droits de l'Oniflhor, se pourvoit en cassation contre l'arrêt du 30 mars 2009 par lequel la cour administrative d'appel de Marseille a rejeté l'appel que Viniflhor a interjeté de ce jugement ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 2 du règlement (CEE) n° 4045/89 du Conseil du 21 décembre 1989 relatif aux contrôles, par les Etats membres, des opérations faisant partie du système de financement par le FEOGA, section " garantie ", dans sa rédaction issue du règlement (CE) n° 3094/94 du Conseil du 12 décembre 1994 : " 1. Les Etats membres procèdent à des contrôles des documents commerciaux des entreprises (...). / 2. (...) Pour chaque période de contrôle (...), les Etats membres sélectionnent les entreprises à contrôler (...). / Les Etats membres soumettent à la Commission leur proposition relative à l'utilisation de l'analyse des risques. Cette proposition (...) est présentée au plus tard le 1er décembre de l'année précédant le début de la période de contrôle à laquelle elle doit s'appliquer (...). / 4. La période de contrôle se situe entre le 1er juillet et le 30 juin de l'année suivante. / Le contrôle porte sur une période d'au moins douze mois s'achevant au cours de la période de contrôle précédente ; il peut être étendu pour des périodes, à déterminer par l'Etat membre, précédant ou suivant la période de douze mois. " ;<br/>
<br/>
              3. Considérant que, dans l'arrêt du 13 juin 2013 par lequel elle s'est prononcée sur les questions dont le Conseil d'Etat, statuant au contentieux, l'avait saisie à titre préjudiciel, la Cour de justice de l'Union européenne a dit pour droit que les dispositions précitées du second alinéa du paragraphe 4 de l'article 2 du règlement (CE) n° 4045/89 doivent être interprétées en ce sens qu'elles ne permettent pas à un Etat membre de se limiter à contrôler une période qui s'achève avant le début de la période de contrôle précédant celle durant laquelle le contrôle a lieu ; que toutefois, dès lors que cet article ne confère pas aux opérateurs un droit leur permettant de s'opposer à des contrôles autres ou plus étendus que ceux qu'il prévoit, la seule circonstance qu'un contrôle porte uniquement sur une période s'achevant antérieurement à la période de contrôle précédant celle durant laquelle il est effectué n'est pas de nature à rendre ce contrôle irrégulier à l'égard des opérateurs contrôlés ;<br/>
<br/>
              4. Considérant qu'il suit de là qu'en jugeant, après avoir relevé que l'Organisation de producteurs Les Cimes avait fait l'objet, les 22, 23 et 24 novembre 2000 et les 24, 25 et 26 janvier 2001, de contrôles portant sur l'année 1998, que ce contrôle était irrégulier au motif qu'il avait porté sur une période s'achevant antérieurement à la période de contrôle précédant celle durant laquelle il a été effectué, la cour administrative d'appel de Marseille a commis une erreur de droit ; que son arrêt doit, pour ce motif, être annulé ; <br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de FranceAgriMer, qui n'est pas, dans la présente instance, la partie perdante ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'organisation de producteurs Les Cimes la somme de 1 000 euros à verser à FranceAgriMer au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 30 mars 2009 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : L'organisation de producteurs Les Cimes versera une somme de 1 000 euros à FranceAgriMer au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par l'organisation de producteurs Les Cimes au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à l'Etablissement national des produits de l'agriculture et de la mer (FranceAgriMer) et à l'organisation de producteurs Les Cimes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
