<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034946959</ID>
<ANCIEN_ID>JG_L_2017_06_000000410813</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/94/69/CETATEXT000034946959.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 13/06/2017, 410813, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410813</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CORLAY</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:410813.20170613</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...C...A...a demandé au juge des référés du tribunal administratif de Toulouse, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, de constater que son maintien en rétention administrative, sans qu'aucune décision n'ait été prise par l'autorité administrative à la suite de sa demande d'asile, porte une atteinte grave et manifestement illégale à sa liberté fondamentale d'aller et venir, à son droit à la sûreté ainsi qu'à son droit au maintien sur le territoire le temps de l'instruction de sa demande d'asile, d'autre part, d'enjoindre au préfet de la Haute-Garonne de la remettre en liberté et de lui délivrer l'attestation prévue à l'article L. 741-1 du code de l'entrée et du séjour des étrangers et du droit d'asile. Par une ordonnance n° 1702307 du 22 mai 2017, le juge des référés du tribunal administratif de Toulouse a rejeté sa demande. <br/>
<br/>
              Par une requête, enregistrée le 23 mai 2017 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de l'admettre au bénéfice de l'aide juridictionnelle provisoire ;<br/>
<br/>
              2°) d'annuler cette ordonnance ; <br/>
<br/>
              3°) de faire droit à sa demande de première instance ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 alinéa 2 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - la condition d'urgence est remplie, dès lors qu'elle est illégalement retenue au centre de rétention de Cornebarrieu et ainsi privée d'un recours suspensif contre le rejet éventuel de sa demande d'asile par l'Office français de protection des réfugiés et apatrides ; <br/>
              - le juge des référés du tribunal administratif de Toulouse a commis une erreur de droit en jugeant que son maintien en rétention avait été autorisé le 10 mai 2017 par le juge des libertés et de la détention du tribunal de grande instance de Toulouse en application de l'article L. 552-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, alors qu'à la suite de la formation de sa demande d'asile, ce maintien ne pouvait être décidé que conformément aux prévision de l'article L. 556-1 du même code ;<br/>
              - son maintien en détention hors de toute décision de l'autorité administrative, en méconnaissance des prescriptions de l'article L. 556-1 du code, porte une atteinte grave et manifestement illégale à son droit d'asile, dont le corollaire réside dans le droit au maintien sur le territoire pendant la durée d'examen de la demande d'asile, et à sa liberté d'aller et venir.<br/>
<br/>
<br/>
              Par un mémoire en intervention, enregistré le 6 juin 2017, l'association La Cimade demande au juge des référés du Conseil d'Etat de faire droit aux conclusions de la requérante.<br/>
<br/>
              Par un mémoire en défense, enregistré le 6 juin 2017, le ministre d'Etat, ministre de l'intérieur, conclut au rejet de la requête. Il fait valoir, à titre principal, que la requête ne relève pas de la compétence de la juridiction administrative et, à titre subsidiaire, que les moyens soulevés par la requérante ne sont pas fondés. <br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, Mme A...et l'association la Cimade, d'autre part, le ministre d'Etat, ministre de l'intérieur ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du mardi 6 juin 2017 à 16 heures 30 au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Rémy-Corlay, avocat au Conseil d'Etat et à la Cour de cassation, avocat de Mme A... ;<br/>
<br/>
              - le représentant de l'association La Cimade ; <br/>
              - les représentantes du ministre d'Etat, ministre de l'intérieur ;<br/>
et à l'issue de laquelle le juge des référés a différé la clôture de l'instruction jusqu'au jeudi 8 juin 2017 à 10h ;<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - la directive 2013/33/UE du Parlement Européen et du Conseil du 26 juin 2013 ;<br/>
              -  le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ; <br/>
              -  le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". <br/>
<br/>
              Sur l'intervention de la Cimade : <br/>
<br/>
              2. L'association La Cimade justifie, eu égard à son objet statutaire et aux questions soulevées par le litige, d'un intérêt suffisant pour intervenir au soutien des conclusions de la requérante. Son intervention est, par suite, recevable.<br/>
<br/>
              Sur le cadre juridique du litige : <br/>
              3. D'une part, aux termes de l'article L. 556-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Lorsqu'un étranger placé en rétention en application de l'article L. 551-1 présente une demande d'asile, l'autorité administrative peut, si elle estime, sur le fondement de critères objectifs, que cette demande est présentée dans le seul but de faire échec à l'exécution de la mesure d'éloignement, maintenir l'intéressé en rétention le temps strictement nécessaire à l'examen de sa demande d'asile par l'Office français de protection des réfugiés et apatrides et, en cas de décision de rejet ou d'irrecevabilité de celui-ci, dans l'attente de son départ. Cette décision de maintien en rétention n'affecte ni le contrôle du juge des libertés et de la détention exercé sur la décision de placement en rétention en application de l'article L. 512-1 ni sa compétence pour examiner la prolongation de la rétention en application du chapitre II du titre V du livre V. La décision de maintien en rétention est écrite et motivée. A défaut d'une telle décision, il est immédiatement mis fin à la rétention et l'autorité administrative compétente délivre à l'intéressé l'attestation mentionnée à l'article L. 741-1. / L'étranger peut demander au président du tribunal administratif l'annulation de la décision de maintien en rétention dans les quarante-huit heures suivant sa notification pour contester les motifs retenus par l'autorité administrative pour estimer que sa demande d'asile a été présentée dans le seul but de faire échec à l'exécution de la mesure d'éloignement. (...) / En cas d'annulation de la décision de maintien en rétention, il est immédiatement mis fin à la rétention et l'autorité administrative compétente délivre à l'intéressé l'attestation mentionnée à l'article L. 741-1. ". Il résulte de ces dispositions que, hors le cas particulier où il a été placé en rétention en vue de l'exécution d'une décision de transfert vers l'Etat responsable de l'examen de sa demande d'asile, prise en application de l'article L. 742-3 du code de l'entrée et du séjour des étrangers et du droit d'asile, il doit en principe être mis fin à la rétention administrative d'un étranger qui formule une demande d'asile. Toutefois, l'administration peut maintenir l'intéressé en rétention, par une décision écrite et motivée, dans le cas où elle estime que sa demande d'asile a été présentée dans le seul but de faire échec à l'exécution de la mesure d'éloignement prise à son encontre. La contestation de la légalité de la décision de maintenir le demandeur d'asile en rétention pour ce motif relève, en application du deuxième alinéa de l'article L. 556-1 du code, de la seule compétence du juge administratif. En cas d'annulation d'une telle décision, l'étranger doit immédiatement être mis en liberté et l'autorité administrative doit lui délivrer l'attestation de demande d'asile mentionnée à l'article L. 741-1 du code. Les dispositions de l'article L. 556-1 du code s'appliquent sans préjudice des pouvoirs reconnus par ailleurs à l'autorité administrative par les dispositions combinées des articles L. 551-1 et du 1° du I de l'article L. 561-2 du même code de placer en rétention l'étranger dont l'examen de la demande d'asile relève de la responsabilité d'un autre Etat membre de l'Union européenne, lorsque l'éloignement du demandeur vers cet Etat demeure une perspective raisonnable et que l'intéressé ne présente pas de garanties de représentation effectives propres à prévenir le risque qu'il se soustraie à cette mesure d'éloignement. <br/>
<br/>
              Sur l'appel de MmeA... : <br/>
<br/>
              4. Il résulte de l'instruction que Mme B...C...A..., ressortissante nigériane, a fait l'objet d'un contrôle par les services de police français alors qu'elle était à bord d'un bus de ligne assurant la liaison entre l'Italie et l'Espagne, à l'occasion duquel il est apparu qu'elle était munie d'un titre de séjour espagnol contrefait. Par un arrêté du 8 mai 2017, le préfet des Pyrénées-Orientales l'a, d'une part, obligée à quitter le territoire français sans délai en fixant le pays de destination et en interdisant son retour sur le territoire national pour une durée de trois ans et, d'autre part, placée en rétention administrative. Le 10 mai 2017, le juge des libertés et de la détention du tribunal de grande instance de Toulouse a ordonné la prolongation pour une durée de vingt-huit jours de sa rétention, à nouveau prolongée pour une durée de quinze jours par une ordonnance rendue par le même juge le 7 juin 2017. Par un jugement du 11 mai 2017, le tribunal administratif de Toulouse a rejeté sa demande tendant à l'annulation de l'arrêté préfectoral du 8 mai 2017. Le 11 mai 2017, Mme A...a déposé une demande d'asile alors qu'elle était ainsi retenue au centre de rétention de Cornebarrieu. Le relevé des empreintes digitales de Mme A... et la consultation de la base de données " Eurodac " ayant fait apparaître qu'elle avait précédemment demandé l'asile en Italie, le préfet de la Haute-Garonne a saisi, le 12 mai 2017, les autorités italiennes d'une requête aux fins de prise en charge en application du règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013, dit " règlement Dublin III ". Par l'ordonnance attaquée du 22 mai 2017, le juge des référés du tribunal administratif de Toulouse, saisi sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté la demande de Mme A...tendant à ce qu'il soit enjoint au préfet de la Haute-Garonne de mettre fin à sa rétention et de lui délivrer l'attestation prévue par l'article L. 741-1 du code de l'entrée et du séjour des étrangers et du droit d'asile.<br/>
<br/>
              5. En premier lieu, eu égard aux restrictions apportées à la liberté d'aller et venir de Mme A... par son maintien en rétention, la condition d'urgence doit être considérée comme remplie. <br/>
              6. En second lieu, il résulte de l'instruction qu'alors que la demande d'asile de Mme A...a été formée pendant sa rétention au centre de rétention de Cornebarrieu, le préfet de la Haute-Garonne n'a pris à son encontre aucune décision de maintien en rétention dans les conditions prévues par l'article L. 556-1 du code de l'entrée et du séjour des étrangers et du droit d'asile. Si le ministre de l'intérieur affirme que l'examen de la demande d'asile de la requérante, parce qu'elle avait été précédée d'une demande d'asile en Italie, relevait de la compétence des autorités de ce pays, il ressort des échanges au cours de l'audience publique qu'aucune décision de transfert vers l'Italie de Mme A...n'a été prise en application de l'article L. 742-3 du code et que les dispositions de l'article L. 556-1 du code étaient, ainsi, applicables. Par suite, et sans préjudice de son pouvoir de placer tout demandeur d'asile en rétention sur le fondement des dispositions combinées des articles L. 551-1 et du 1° du I de l'article L. 561-2 du même code, l'autorité administrative ne pouvait maintenir la requérante en rétention sans qu'une décision écrite et motivée en ce sens lui ait été notifiée, conformément aux prescriptions de l'article L. 556-1. En l'absence de toute décision administrative postérieure au dépôt de sa demande d'asile, le préfet de la Haute-Garonne a porté une atteinte grave et manifestement illégale à la liberté d'aller et venir de Mme A...ainsi qu'à son droit d'asile. <br/>
<br/>
              7. Il résulte de tout ce qui précède que Mme A...est fondée à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Toulouse a rejeté sa demande. Il y a lieu, par suite, d'enjoindre au préfet de la Haute-Garonne de réexaminer la situation de la requérante au regard des règles énoncées au point 3 et, en l'absence de nouvelle décision prise dans un délai de vingt-quatre heures à compter de la notification de la présente ordonnance, de mettre fin à la rétention de MmeA....<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, d'admettre Mme A...au bénéfice de l'aide juridictionnelle provisoire et de faire droit à ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative et 37 alinéa 2 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'intervention de l'association La Cimade est admise.<br/>
Article 2 : Mme A...est admise à l'aide juridictionnelle provisoire. <br/>
Article 3 : L'ordonnance n° 1702307 du 22 mai 2017 du juge des référés du tribunal administratif de Toulouse est annulée.<br/>
Article 4 : Il est enjoint au préfet de la Haute-Garonne de réexaminer la situation de Mme A... au regard des règles énoncées au point 3 et, en l'absence de nouvelle décision prise dans un délai de vingt-quatre heures à compter de la notification de la présente ordonnance, de mettre fin à sa rétention.<br/>
Article 5 : L'Etat versera la somme de 1 000 euros à Me Rémy-Corlay en application des dispositions de l'article L. 761-1 du code de justice administrative et 37 alinéa 2 de la loi du 10 juillet 1991, sous réserve qu'elle renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 6 : Le surplus des conclusions de la requête de Mme A...est rejeté.<br/>
Article 7 : La présente ordonnance sera notifiée à Mme B...C...A..., à l'association La Cimade et au ministre d'Etat, ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
