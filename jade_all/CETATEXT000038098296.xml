<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038098296</ID>
<ANCIEN_ID>JG_L_2019_01_000000427000</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/09/82/CETATEXT000038098296.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 29/01/2019, 427000, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-01-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427000</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:427000.20190129</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C...A...a demandé au juge des référés du tribunal administratif de Toulouse, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, de dire que la responsabilité des autorités espagnoles pour examiner sa demande d'asile a cessé et que l'instruction de sa demande incombe aux autorités françaises et, d'autre part, d'enjoindre au préfet de la région Occitanie, préfet de la Haute-Garonne, de lui délivrer un dossier de demande d'asile ainsi que l'attestation correspondante, dans un délai de 24 heures à compter de la notification de l'ordonnance à intervenir et sous astreinte de 200 euros par jour de retard. Par une ordonnance n° 1805991 du 28 décembre 2018, le juge des référés du tribunal administratif de Toulouse a rejeté sa demande. <br/>
<br/>
                Par une requête, enregistrée le 11 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
                1°) de l'admettre au bénéfice de l'aide juridictionnelle provisoire ;<br/>
<br/>
                2°) d'annuler cette ordonnance ;<br/>
<br/>
                3°) de faire droit à sa demande de première instance ;<br/>
<br/>
                4°) à titre subsidiaire, de suspendre l'exécution de la décision de transfert de M. A... vers l'Espagne dans l'attente de la justification par l'autorité administrative française de ce que le requérant pourra bénéficier en Espagne des soins qu'il reçoit en France, dans les mêmes conditions économiques et sociales qu'en France ;<br/>
<br/>
                5°) de mettre à la charge de l'Etat le versement à son conseil, MeB..., d'une somme de 2 000 euros au titre des dispositions combinées des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ou, dans l'hypothèse du rejet de sa demande d'aide juridictionnelle provisoire, de mettre à la charge de l'Etat le versement d'une somme de 2 000 euros à son profit au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est remplie ; <br/>
              - le refus d'examiner sa demande d'asile en procédure " normale " porte une atteinte grave et manifestement illégale à son droit de solliciter l'asile ;<br/>
              - l'illégalité de cette atteinte résulte de la méconnaissance de l'article 29 du règlement " Dublin III " dès lors que, le délai de six mois prévu par ces dispositions pour procéder à l'exécution de son transfert ayant expiré depuis le 12 septembre 2018, la France est devenue l'Etat membre responsable de l'examen de sa demande d'asile ; <br/>
              - il ne peut être regardé comme " en fuite " dès lors que, d'une part, il s'est conformé au contrôle de l'autorité préfectorale en respectant les termes de son assignation à résidence pendant toute la durée de la procédure et, d'autre part, il n'a jamais été convoqué par le préfet en vue de l'exécution de son transfert ;<br/>
              - l'illégalité de cette atteinte résulte également de la méconnaissance des dispositions de l'article 31 du règlement " Dublin III " dès lors que le préfet, en ne transmettant pas aux autorités espagnoles les informations relatives à son état de santé alors qu'une interruption du suivi de son traitement médical en France risquerait d'entraîner pour lui des conséquences d'une exceptionnelle gravité, ne s'est pas assuré que les autorités espagnoles étaient en mesure de répondre à ses besoins et de garantir sa prise en charge médicale.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le règlement (UE) n° 118/2014 de la Commission du 30 janvier 2014 modifiant le règlement (CE) n° 1560/2003 de la Commission du 2 septembre 2003 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction contradictoire ni audience publique lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. A cet égard, il appartient au juge d'appel de prendre en considération les éléments recueillis par le juge du premier degré dans le cadre de la procédure écrite et orale qu'il a diligentée.<br/>
<br/>
              2. Il résulte de l'instruction diligentée par le juge des référés du tribunal administratif de Toulouse que M.A..., de nationalité guinéenne, a déclaré être entré irrégulièrement sur le territoire français le 3 août 2017. Le 7 août 2017, il s'est présenté à la plateforme d'accueil des demandeurs d'asile, avant de déposer une demande d'asile à la préfecture de la Haute-Garonne le 31 août 2017. Il a alors été informé que le relevé de ses empreintes décadactylaires révélait qu'il avait été précédemment contrôlé par les autorités espagnoles, qui étaient désormais responsables de sa demande d'asile. Saisies d'une demande de prise en charge en application du règlement " Dublin III ", les autorités espagnoles ont explicitement accepté le transfert de l'intéressé le 10 octobre 2017. Par deux arrêtés du 19 février 2018, le préfet de la Haute-Garonne a décidé, d'une part, de son transfert aux autorités espagnoles et, d'autre part, de son assignation à résidence pour une durée de sept jours, du 6 au 13 mars 2018. M. A...a été informé le 6 mars 2018 des modalités de son départ contrôlé, prévu le 13 mars 2018 de l'aéroport Toulouse Blagnac. Par un jugement n° 1801040 du 12 mars 2018, confirmé par un arrêt n° 18BX01491 du 27 juillet 2018 de la cour administrative d'appel de Bordeaux, le tribunal administratif de Toulouse a rejeté sa demande tendant à l'annulation des deux arrêtés du 19 février 2018. Le 13 mars 2018, M. A...ne s'est pas présenté à l'embarquement de son vol à destination de Madrid. Le 14 mars 2018, avant l'expiration du délai de transfert de six mois, le préfet de la Haute-Garonne a informé les autorités espagnoles que, pour ce motif, il était considéré " en fuite " et que le transfert serait donc effectué au plus tard dans un délai de 18 mois à compter de leur accord, soit le 9 octobre 2019. Le 12 juin 2018, le requérant a déposé une demande de titre de séjour en tant qu'étranger malade, rejetée par le préfet par une décision du même jour. Les 17 septembre et 13 novembre 2018, M. A...a demandé au préfet d'enregistrer sa demande d'asile en procédure normale. Par une ordonnance n° 1805991 du 28 décembre 2018, le juge des référés du tribunal administratif de Toulouse, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté sa demande tendant à enjoindre au préfet de procéder à l'enregistrement de sa demande d'asile en procédure normale. M. A...relève appel de cette ordonnance.<br/>
<br/>
              3. Le juge des référés du tribunal administratif de Toulouse a rejeté la demande de M. A...en se fondant sur les deux considérations suivantes. D'une part, il a jugé que le préfet de la Haute-Garonne était fondé à prolonger le délai de transfert de l'intéressé à dix-huit mois en le regardant comme étant en fuite dès lors que, en premier lieu, l'administration a respecté les obligations qui sont les siennes dans l'organisation du départ contrôlé de M. A...par l'information le 6 mars 2018 des modalités de son départ dans une langue qu'il comprend et, en second lieu, M. A...s'est intentionnellement soustrait à l'exécution de son départ sans qu'il ait établi être dans l'impossibilité matérielle de se rendre lui-même à l'aéroport ni même qu'il aurait rencontré d'autres difficultés rendant impossible sa présentation à l'embarquement de son vol. D'autre part, il a considéré que le préfet de la Haute-Garonne n'avait pas porté une atteinte grave et manifestement illégale aux garanties que lui confère sa qualité de demandeur d'asile dès lors que, en premier lieu, la méconnaissance alléguée par M. A...du préfet de la Haute-Garonne des dispositions de l'article 31 du règlement du 26 juin 2013 portant sur les modalités d'exécution d'une décision de transfert était sans incidence sur le bien-fondé de la mesure de transfert et, en second lieu, l'intéressé n'établissait ni qu'il ne pourrait voyager sans risque vers l'Espagne ni qu'il lui serait impossible d'y bénéficier de soins appropriés, ni même qu'il aurait été effectivement privé des garanties instituées par les dispositions précitées du règlement du 26 juin 2013. Le requérant n'apporte en appel aucun élément nouveau susceptible d'infirmer la solution retenue par le juge de première instance. Au surplus, les conclusions présentées à titre subsidiaire, tendant à la suspension de l'exécution de la décision de transfert de M. A...vers l'Espagne et qui n'ont pas été présentées par le requérant devant le juge des référés de première instance, sont donc nouvelles en appel et, partant, irrecevables.<br/>
              4. Il résulte de ce qui précède qu'il est manifeste que l'appel de M. A...ne peut être accueilli. Il y a donc lieu de rejeter sa requête selon la procédure prévue par l'article L. 522-3 du code de justice administrative, y compris ses conclusions présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sans qu'il y ait lieu de l'admettre au bénéfice de l'aide juridictionnelle provisoire.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 3 : La présente ordonnance sera notifiée à M. C...A....<br/>
Copie en sera adressée au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
