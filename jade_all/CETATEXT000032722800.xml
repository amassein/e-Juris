<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032722800</ID>
<ANCIEN_ID>JG_L_2016_06_000000382004</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/72/28/CETATEXT000032722800.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 13/06/2016, 382004, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-06-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382004</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD</AVOCATS>
<RAPPORTEUR>M. Matias de Sainte Lorette</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:382004.20160613</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Viel et Compagnie a demandé au tribunal administratif de Paris de prononcer la décharge des cotisations supplémentaires d'impôt sur les sociétés et de contribution additionnelle à cet impôt auxquelles elle a été assujettie au titre de l'exercice clos en 2001. Par un jugement n° 0811919/1-1 du 14 mars 2012, le tribunal administratif de Paris a fait droit à sa demande.<br/>
<br/>
              Par un arrêt n° 12PA02294 du 29 avril 2014, la cour administrative d'appel de Paris a rejeté l'appel formé par le ministre des finances et des comptes publics contre ce jugement. <br/>
<br/>
              Par un pourvoi, enregistré le 30 juin 2014 au secrétariat du contentieux du Conseil d'Etat, le ministre des finances et des comptes publics demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel et de remettre à la charge de la société Viel et compagnie les impositions litigieuses.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matias de Sainte Lorette, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, avocat de la société Viel et Compagnie ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 27 mai 2016, présentée par la société Viel et Compagnie ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 38 du code général des impôts, applicable à l'impôt sur les sociétés en vertu de l'article 209 de ce code : " (...) 2. Le bénéfice net est constitué par la différence entre les valeurs de l'actif net à la clôture et à l'ouverture de la période dont les résultats doivent servir de base à l'impôt diminuée des suppléments d'apports et augmentée des prélèvements effectués au cours de cette période par l'exploitant ou par les associés. L'actif net s'entend de l'excédent des valeurs d'actif sur le total formé au passif par les créances des tiers, les amortissements et les provisions justifiés ". <br/>
<br/>
              2. Il résulte des pièces du dossier soumis aux juges du fond que la société Finacor, devenue ensuite société Rocani, a souhaité, dans le cadre d'une réorganisation, céder à la société Viel et Compagnie son activité d'intermédiation financière. Cette opération a été réalisée en plusieurs étapes. Par un traité d'apport partiel d'actif du 28 septembre 2000 complété par un avenant du 27 novembre 2000, la société Rocani a apporté cette branche d'activité à la société nouvelle Finacor, devenue ensuite société Finacor, constituée à cet effet. Cet apport a été placé sous le régime fiscal de droit commun des fusions. L'actif net apporté s'élevait à 19 056 125 euros. A la suite de cet apport, la société Finacor a comptabilité à son passif une provision pour risques de 22 378 651 euros. Par un traité de fusion du 14 mars 2001 avec effet rétroactif au 1er  janvier 2001, modifié par un avenant du 9 avril 2001, la société Viel et Compagnie a absorbé la société Finacor, dont elle détenait la totalité du capital. Cette opération a été placée sous le régime de faveur prévu à l'article 210 A du code général des impôts. L'actif net apporté s'élevait à 19 189 219 euros et tenait compte notamment d'une provision pour risques de 22 378 652 euros que la société Viel et Compagnie a reprise à son passif. Cette dernière a en outre comptabilisé les actions de la société Finacor à hauteur de 19 056 125 euros, constatant ainsi un bonus de fusion de 133 094 euros. Le 20 juin 2001, la société Viel et Compagnie a cédé, pour un prix très proche de la valeur d'apport, les participations ainsi acquises à une filiale suisse du groupe, la Compagnie financière tradition. Le 31 décembre 2001, la société Viel et Compagnie a procédé à la reprise de la provision pour risques de 22 378 652 euros, devenue sans objet, puis l'a déduite de manière extracomptable. A l'issue de la vérification de comptabilité dont la société Viel et Compagnie a fait l'objet, l'administration a remis en cause cette déduction. Par un jugement du 14 mars 2012, le tribunal administratif de Paris a prononcé la décharge des cotisations supplémentaires d'impôt sur les sociétés et de contribution additionnelle en résultant au titre de l'année 2001. Le ministre des finances et des comptes publics se pourvoit en cassation contre l'arrêt du 29 avril 2014 par lequel la cour administrative d'appel de Paris a rejeté le recours qu'il avait formé contre ce jugement. <br/>
<br/>
              3. Par une appréciation souveraine, la cour a estimé que la provision litigieuse devait être regardée comme ayant été prise en compte tant dans le calcul de l'actif net dont la société Finacor avait fait apport à la société Viel et Compagnie que dans celui de l'actif net dont la société Viel et Compagnie avait fait apport à la société Compagnie financière tradition. En déduisant de ces constatations que la société Viel et Compagnie pouvait procéder, par voie extracomptable, à la déduction fiscale de la reprise de cette provision, alors que la société Viel et Compagnie a cédé ses actifs pour leur valeur réelle, proche de leur valeur d'apport, elle-même calculée en tenant compte de l'existence de cette provision, et que cette déduction a dès lors eu pour effet de minorer indûment son bénéfice imposable, la cour a commis une erreur de droit.<br/>
<br/>
              4. Il résulte de ce qui précède que le ministre est fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : L'arrêt du 29 avril 2014 de la cour administrative d'appel de Paris est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Paris. <br/>
Article 3 : Les conclusions de la société Viel et Compagnie au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre des finances et des comptes publics et à la société Viel et Compagnie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
