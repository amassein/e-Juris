<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044176761</ID>
<ANCIEN_ID>JG_L_2021_10_000000437532</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/17/67/CETATEXT000044176761.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 07/10/2021, 437532, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437532</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Lionel Ferreira</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:437532.20211007</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Rennes de prononcer la décharge, en droits et pénalités, des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre de l'année 2012. Par un jugement n° 1604328 du 27 juin 2018, le tribunal administratif de Rennes a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 18NT03283 du 15 novembre 2019, la cour administrative d'appel de Nantes a rejeté l'appel formé par M. A... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 9 janvier, 9 avril et 30 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 89-462 du 6 juillet 1989 ;<br/>
              - la loi n° 2014-366 du 24 mars 2014 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Lionel Ferreira, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A... a acquis, le 1er septembre 2005, une maison en l'état futur d'achèvement à Concarneau. A l'issue d'un contrôle sur pièces, l'administration fiscale a, par une proposition de rectification du 1er décembre 2015, remis en cause l'application du bénéfice du dispositif prévu au h du 1° du I de l'article 31 du code général des impôts au motif que le locataire de cette maison à compter du 15 octobre 2012 ne l'utilisait pas comme habitation principale. M. A... a été en conséquence assujetti à des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales au titre de l'année 2012. Il se pourvoit en cassation contre l'arrêt du 15 novembre 2019 par lequel la cour administrative d'appel de Nantes a rejeté l'appel qu'il a formé contre le jugement du 27 juin 2018 du tribunal administratif de Rennes rejetant sa demande de décharge de ces impositions.<br/>
<br/>
              2. En premier lieu, aux termes de l'article L. 76 B du livre des procédures fiscales : " L'administration est tenue d'informer le contribuable de la teneur et de l'origine des renseignements et documents obtenus de tiers sur lesquels elle s'est fondée pour établir l'imposition faisant l'objet de la proposition prévue au premier alinéa de l'article L. 57 ou de la notification prévue à l'article L. 76. Elle communique, avant la mise en recouvrement, une copie des documents susmentionnés au contribuable qui en fait la demande ". Il résulte de ces dispositions que l'administration ne peut fonder le redressement des bases d'imposition d'un contribuable sur des renseignements ou documents qu'elle a obtenus de tiers sans l'avoir informé, avant la mise en recouvrement, de l'origine et de la teneur de ces renseignements. Cette obligation d'information ne se limite pas aux renseignements et documents obtenus de tiers par l'exercice du droit de communication. Si cette obligation ne s'étend pas aux éléments nécessairement détenus par les différents services de l'administration fiscale en application de dispositions législatives ou réglementaires, tel n'est pas le cas pour les informations fournies à titre déclaratif à l'administration par des contribuables tiers, dont elle tire les conséquences pour reconstituer la situation du contribuable vérifié.<br/>
<br/>
              3. Il ressort des énonciations de l'arrêt attaqué que, dans la réponse aux observations du contribuable du 2 mars 2016, l'administration fiscale a fait part à M. A... du fait que son locataire avait continué à déclarer son habitation principale à Pont-Audemer. C'est sans commettre d'erreur de droit ni de dénaturation des pièces du dossier que la cour, qui a suffisamment motivé sa décision sur ce point, a pu déduire de ces énonciations, au demeurant non contestées, que M. A... avait été informé de l'existence d'une déclaration par laquelle son locataire soutenait que le bien situé à Concarneau ne constituait pas sa résidence principale, et par voie de conséquence de la teneur et de l'origine des renseignements sur lesquels l'administration s'était fondée, sans qu'il était besoin de préciser que la déclaration faite par le locataire à l'administration fiscale l'avait été au titre de l'impôt sur le revenu.<br/>
<br/>
              4. En deuxième lieu, aux termes de l'article 31 du code général des impôts : " I. Les charges de la propriété déductibles pour la détermination du revenu net comprennent : / 1° Pour les propriété urbaines : (...) / h) Pour les logements situés en France, acquis neufs ou en l'état futur d'achèvement entre le 3 avril 2003 et le 31 décembre 2009, et à la demande du contribuable, une déduction au titre de l'amortissement égale à 6 % du prix d'acquisition du logement pour les sept premières années et à 4 % de ce prix pour les deux années suivantes. La période d'amortissement a pour point de départ le premier jour du mois de l'achèvement de l'immeuble ou de son acquisition si elle est postérieure. (...) Le bénéfice de la déduction est subordonné à une option qui doit être exercée lors du dépôt de la déclaration des revenus de l'année d'achèvement de l'immeuble ou de son acquisition si elle est postérieure. Cette option est irrévocable pour le logement considéré et comporte l'engagement du propriétaire de louer le logement nu pendant au moins neuf ans à usage d'habitation principale à une personne autre qu'un membre de son foyer fiscal. Cette location doit prendre effet dans les douze mois qui suivent la date d'achèvement de l'immeuble ou de son acquisition si elle est postérieure. Cet engagement prévoit, en outre, que le loyer ne doit pas excéder un plafond fixé par décret. (...) ". Il résulte de ces dispositions que le bénéfice de l'avantage fiscal qu'elles prévoient est subordonné à la condition que le locataire fasse effectivement de l'immeuble qui lui est loué par le contribuable son habitation principale. <br/>
<br/>
              5. C'est sans erreur de droit que la cour a jugé que M. A... ne pouvait utilement se prévaloir de la définition de la résidence principale introduite à l'article 2 de la loi du 6 juillet 1989 tendant à améliorer les rapports locatifs et portant modification de la loi n° 86-1290 du 23 décembre 1986 par la loi du 24 mars 2014 pour l'accès au logement et un urbanisme rénové, dès lors que la loi du 6 juillet 1989 vise seulement à régir les obligations réciproques entre bailleurs et locataires et est par suite sans incidence sur la définition de l'habitation principale au sens du h du 1° du I de l'article 31 du code général des impôts. Au demeurant, cette définition, issue de la modification introduite par la loi du 24 mars 2014 pour l'accès au logement et un urbanisme rénové, n'est entrée en vigueur que le 27 mars 2014.<br/>
<br/>
              6. En troisième lieu, la cour a relevé que le locataire de M. A... a indiqué, de façon continue, sur ses déclarations des revenus des années 2010 à 2015, être domicilié à l'adresse de Pont-Audemer, où il possède une maison. La cour a également relevé que si M. A... faisait valoir que son locataire a ultérieurement attesté avoir occupé la maison situé à Concarneau à partir du 15 octobre 2012 comme résidence permanente et qu'il travaillait à cette époque à Scaër et ne pouvait donc rentrer le soir à Pont-Audemer, il était constant que la résidence située à Pont-Audemer était, avant le 1er octobre 2012, l'habitation principale de M. A... et que son épouse a par ailleurs continué de résider à Pont-Audemer pendant la période en litige. La cour a relevé enfin, outre les déclarations à l'impôt sur le revenu faites par le locataire lui-même, qu'il n'est ni établi ni même allégué que son locataire aurait, après sa mission, déménagé de Pont-Audemer. En se fondant sur l'ensemble de ces éléments pour en déduire que le logement en litige n'était pas l'habitation principale du locataire de M. A..., la cour n'a ni commis erreur de droit ni entaché son arrêt de dénaturation.<br/>
<br/>
              7. Il résulte de tout ce qui précède que le pourvoi de M. A... ne peut qu'être rejeté, y compris ses conclusions au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. B... A... et au ministre de l'économie, des finances et de la relance.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
