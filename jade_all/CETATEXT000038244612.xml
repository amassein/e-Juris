<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038244612</ID>
<ANCIEN_ID>JG_L_2019_03_000000411462</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/24/46/CETATEXT000038244612.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 18/03/2019, 411462</TITRE>
<DATE_DEC>2019-03-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411462</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET ; SCP L. POULET, ODENT</AVOCATS>
<RAPPORTEUR>Mme Liza Bellulo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:411462.20190318</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société civile immobilière (SCI) des Cèdres a demandé au tribunal administratif de Grenoble, d'une part, de condamner la commune de Chambéry à lui verser la somme de 88 368,35 euros au titre de la réparation des préjudices subis sur sa propriété du fait de troncs de faux-acacias et d'un frêne implantés sur une dépendance du domaine public, d'autre part, d'enjoindre à cette commune de dévitaliser ces troncs, de traiter leurs rejets et drageons, et d'abattre ce frêne dans un délai d'un mois après la notification du jugement, sous astreinte de 100 euros par jour de retard. Par un jugement n° 1103564 du 20 novembre 2014, le tribunal a condamné la commune de Chambéry à verser à la société des Cèdres la somme de 10 996 euros au titre de la réparation de ses préjudices, déduction étant faite de la provision de 2 200 euros accordée à la société par une ordonnance du juge des référés de ce tribunal du 4 juin 2008, et rejeté le surplus des conclusions de la société des Cèdres.<br/>
<br/>
              Par un arrêt n° 15LY00195 du 13 avril 2017, rectifié par un arrêt n° 17LY02162 du 21 septembre 2017, la cour administrative d'appel de Lyon a, sur appel de la société des Cèdres et sur appel incident de la commune de Chambéry, porté la somme que la commune était condamnée à verser à la société des Cèdres à 40 394 euros, enjoint à la commune de Chambéry, dans un délai d'un an à compter de la notification de l'arrêt et sous astreinte de 100 euros par jour de retard passé ce délai, de dévitaliser le système racinaire contribuant à la présence de drageons et de rejets de faux-acacias sur le terrain de la société jusqu'à la cessation de tels rejets ainsi que d'abattre le frêne implanté sur le domaine public, et rejeté le surplus des conclusions d'appel de la société des Cèdres et de la commune de Chambéry.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 13 juin et 13 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, la commune de Chambéry demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société des Cèdres et de faire droit à son appel incident ;<br/>
<br/>
              3°) de mettre à la charge de la société des Cèdres le versement de la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Liza Bellulo, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de la commune de Chambéry et à la SCP L. Poulet, Odent, avocat de la société civile immobilière des Cèdres. <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que la société des Cèdres a acquis en juin 2002 une maison d'habitation et un terrain sur la parcelle cadastrée BV 299 dont le mur de clôture jouxtait partiellement une parcelle du domaine public appartenant à la commune de Chambéry, constituée d'une voie communale dite " chemin Chantemerle " et d'un talus en surplomb, sur lequel étaient implantés des faux-acacias (robiniers) et un frêne. Au cours de l'hiver 2002-2003, la commune a procédé, sur demande de la société des Cèdres, à l'abattage des faux acacias, afin de prévenir leur chute sur la propriété de cette société. Par un jugement du 20 novembre 2014, le tribunal administratif de Grenoble, faisant partiellement droit à la demande indemnitaire de la société des Cèdres, a condamné la commune de Chambéry à lui verser, après déduction de la somme de 2 200 euros qui lui avait été versée à titre de provision, une somme de 10 996 euros en réparation du préjudice grave et spécial résultant de la présence de drageons et de rejets de faux-acacias sur son terrain en provenance du domaine public et de la dégradation de son mur de clôture du fait du développement d'un frêne implanté sur le domaine public. Statuant sur l'appel de la société et l'appel incident de la commune, la cour administrative d'appel de Lyon a, par un arrêt du 13 avril 2017, rectifié le 21 septembre 2017, porté la somme que la commune était condamnée à verser à la société des Cèdres à 40 394 euros et enjoint à la commune de Chambéry, dans un délai d'un an et sous astreinte de 100 euros par jour de retard, de dévitaliser le système racinaire contribuant à la présence de drageons et de rejets de faux-acacias sur le terrain de la société jusqu'à la cessation de tels rejets ainsi que d'abattre le frêne, et rejeté le surplus des conclusions de la société des Cèdres et de la commune de Chambéry. La commune de Chambéry se pourvoit en cassation contre cet arrêt.<br/>
<br/>
              Sur le bien-fondé de l'arrêt en tant qu'il est relatif aux conclusions indemnitaires présentées par la SCI des Cèdres :<br/>
<br/>
              2. Le maître d'ouvrage est responsable, même en l'absence de faute, des dommages que les ouvrages publics dont il a la garde peuvent causer aux tiers tant en raison de leur existence que de leur fonctionnement. Il ne peut dégager sa responsabilité que s'il établit que ces dommages résultent de la faute de la victime ou d'un cas de force majeure. Dans le cas d'un dommage causé à un immeuble, la fragilité ou la vulnérabilité de celui-ci ne peuvent être prises en compte pour atténuer la responsabilité du maître de l'ouvrage, sauf lorsqu'elles sont elles-mêmes imputables à une faute de la victime. En dehors de cette hypothèse, de tels éléments ne peuvent être retenus que pour évaluer le montant du préjudice indemnisable.<br/>
<br/>
              3. La cour administrative d'appel, par des motifs non contestés de son arrêt, a estimé que les désordres subis par la propriété de la société des Cèdres trouvaient leur source dans l'abattage par la commune, au cours de l'hiver 2002/2003, de faux-acacias plantés sur le talus, dans l'orientation, par l'effet de travaux publics de voirie, de la pousse des rejets et drageons en résultant vers cette propriété, ainsi que dans la présence d'un frêne implanté sur le domaine public à proximité du mur de clôture qui avait entraîné des fissurations et un descellement de celui-ci.<br/>
<br/>
              4. En premier lieu, la commune n'est pas fondée à soutenir, au seul motif que les faux acacias étaient présents sur le domaine public à la date d'acquisition de la propriété par la société et que leur abattage par la commune serait intervenu sur sa demande, aux fins de prévenir un risque de chute de ces arbres, que la cour administrative d'appel aurait entaché son arrêt de dénaturation en jugeant qu'il ne résultait pas de l'instruction que la société aurait eu connaissance, à la date d'acquisition de sa propriété, des risques auxquels elle était exposée à raison du développement de rejets et drageons de faux acacias sur son terrain en provenance des souches persistant après l'abattage. <br/>
<br/>
              5. En deuxième lieu, en se fondant, pour écarter le moyen tiré de l'existence d'une faute de la société des Cèdres de nature à exonérer la commune de sa responsabilité, sur ce qu'il ne résultait pas de l'instruction que cette société aurait été défaillante dans l'entretien de son mur et de son jardin, la cour administrative d'appel s'est livrée à une appréciation souveraine non entachée de dénaturation.<br/>
<br/>
              6. En troisième lieu, en déduisant de l'ensemble de ces éléments que la commune devait être déclarée entièrement responsable du dommage, qu'elle a regardé comme revêtant un caractère grave et spécial, causé à la propriété de la société des Cèdres, la cour administrative d'appel n'a pas, contrairement à ce qui est soutenu, entaché son arrêt d'erreur de droit.<br/>
<br/>
              Sur le bien fondé de l'arrêt en tant qu'il est relatif aux conclusions aux fins d'injonction présentées par la société des Cèdres :<br/>
<br/>
              7. Lorsque le juge administratif statue sur un recours indemnitaire tendant à la réparation d'un préjudice imputable à un comportement fautif d'une personne publique et qu'il constate que ce comportement et ce préjudice perdurent à la date à laquelle il se prononce, il peut, en vertu de ses pouvoirs de pleine juridiction et lorsqu'il est saisi de conclusions en ce sens, enjoindre à la personne publique en cause de mettre fin à ce comportement ou d'en pallier les effets. Lorsqu'il met à la charge de la personne publique la réparation d'un préjudice grave et spécial imputable à la présence ou au fonctionnement d'un ouvrage public, il ne peut user d'un tel pouvoir d'injonction que si le requérant fait également état, à l'appui de ses conclusions à fin d'injonction, de ce que la poursuite de ce préjudice, ainsi réparé sur le terrain de la responsabilité sans faute du maître de l'ouvrage, trouve sa cause au moins pour partie dans une faute du propriétaire de l'ouvrage. Il peut alors enjoindre à la personne publique, dans cette seule mesure, de mettre fin à ce comportement fautif ou d'en pallier les effets.<br/>
<br/>
              8. Il résulte de ce qui vient d'être dit au point 7, qu'en ordonnant à la commune de Chambéry de dévitaliser le système racinaire contribuant à la présence de drageons et de rejets de faux-acacias sur le terrain de la société des Cèdres, jusqu'à cessation de tels rejets, et d'abattre le frêne implanté sur le domaine public au voisinage de la propriété de cette société, alors qu'elle avait engagé la responsabilité de la commune sur le terrain de la responsabilité sans faute du propriétaire de l'ouvrage publique à l'égard d'un tiers sans rechercher, d'une part, si la société requérante avait fondé ses conclusions à fin d'injonction sur une faute de la commune en cette qualité de propriétaire de l'ouvrage à l'origine d'une partie au moins des dommages et, d'autre part, si les mesures demandées tendaient uniquement à mettre fin à ce comportement fautif ou à en pallier les effets, la cour administrative d'appel a commis une erreur de droit.<br/>
<br/>
              9. Il résulte de l'ensemble de ce qui précède que la commune de Chambéry n'est fondée à demander l'annulation de l'arrêt attaqué qu'en tant seulement qu'il statue sur les conclusions aux fins d'injonction présentées par la société des Cèdres.<br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société des Cèdres le versement à la commune de Chambéry d'une somme de 600 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font en revanche obstacle à ce qu'une somme soit mise au même titre à la charge de la commune de Chambéry, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>                              D E C I D E :<br/>
                                            --------------<br/>
<br/>
Article 1er : L'arrêt du 13 avril 2017 de la cour administrative d'appel de Lyon est annulé en tant qu'il a statué sur les conclusions aux fins d'injonction présentées par la société des Cèdres.<br/>
<br/>
Article 2 : La société des Cèdres versera à la commune de Chambéry une somme de 600 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la commune de Chambéry et à la société civile immobilière des Cèdres.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-07-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. POUVOIRS DU JUGE DE PLEIN CONTENTIEUX. - RÉPARATION D'UN PRÉJUDICE - CAS OÙ LE PRÉJUDICE PERDURE À LA DATE À LAQUELLE SE PRONONCE LE JUGE - POUVOIRS D'INJONCTION - 1) HYPOTHÈSE D'UN COMPORTEMENT FAUTIF D'UNE PERSONNE PUBLIQUE - POSSIBILITÉ POUR LE JUGE D'ENJOINDRE À LA PERSONNE PUBLIQUE DE METTRE FIN À SON COMPORTEMENT FAUTIF OU D'EN PALLIER LES EFFETS [RJ1] - 2) HYPOTHÈSE D'UN PRÉJUDICE GRAVE ET SPÉCIAL CAUSÉ PAR LA PRÉSENCE OU LE FONCTIONNEMENT D'UN OUVRAGE PUBLIC - POUVOIR D'INJONCTION LIMITÉ AU CAS, ET DANS LA SEULE MESURE, OÙ LA PERSISTANCE DU DOMMAGE TROUVE SA CAUSE AU MOINS POUR PARTIE DANS UNE FAUTE DU PROPRIÉTAIRE DE L'OUVRAGE - 3) ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-01-02-01-03-01-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. FONDEMENT DE LA RESPONSABILITÉ. RESPONSABILITÉ SANS FAUTE. RESPONSABILITÉ ENCOURUE DU FAIT DE L'EXÉCUTION, DE L'EXISTENCE OU DU FONCTIONNEMENT DE TRAVAUX OU D'OUVRAGES PUBLICS. VICTIMES AUTRES QUE LES USAGERS DE L'OUVRAGE PUBLIC. TIERS. - RÉPARATION DU PRÉJUDICE - 1) PRINCIPE - PRÉJUDICE GRAVE ET SPÉCIAL CAUSÉ PAR LA PRÉSENCE OU LE FONCTIONNEMENT D'UN OUVRAGE PUBLIC - CAS OÙ LE PRÉJUDICE PERDURE À LA DATE À LAQUELLE SE PRONONCE LE JUGE - POUVOIR D'INJONCTION [RJ2] LIMITÉ AU CAS, ET DANS LA SEULE MESURE, OÙ LA PERSISTANCE DU DOMMAGE TROUVE SA CAUSE AU MOINS POUR PARTIE DANS UNE FAUTE DU PROPRIÉTAIRE DE L'OUVRAGE - 2) ESPÈCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">67-03-03 TRAVAUX PUBLICS. DIFFÉRENTES CATÉGORIES DE DOMMAGES. DOMMAGES CAUSÉS PAR L'EXISTENCE OU LE FONCTIONNEMENT D'OUVRAGES PUBLICS. - RÉPARATION DU PRÉJUDICE - 1) PRINCIPE - PRÉJUDICE GRAVE ET SPÉCIAL CAUSÉ PAR LA PRÉSENCE OU LE FONCTIONNEMENT D'UN OUVRAGE PUBLIC - CAS OÙ LE PRÉJUDICE PERDURE À LA DATE À LAQUELLE SE PRONONCE LE JUGE - POUVOIR D'INJONCTION [RJ2] LIMITÉ AU CAS, ET DANS LA SEULE MESURE, OÙ LA PERSISTANCE DU DOMMAGE TROUVE SA CAUSE AU MOINS POUR PARTIE DANS UNE FAUTE DU PROPRIÉTAIRE DE L'OUVRAGE - 2) ESPÈCE.
</SCT>
<ANA ID="9A"> 54-07-03 1) Lorsque le juge administratif statue sur un recours indemnitaire tendant à la réparation d'un préjudice imputable à un comportement fautif d'une personne publique et qu'il constate que ce comportement et ce préjudice perdurent à la date à laquelle il se prononce, il peut, en vertu de ses pouvoirs de pleine juridiction et lorsqu'il est saisi de conclusions en ce sens, enjoindre à la personne publique en cause de mettre fin à ce comportement ou d'en pallier les effets.... ...2) Lorsqu'il met à la charge de la personne publique la réparation d'un préjudice grave et spécial imputable à la présence ou au fonctionnement d'un ouvrage public, il ne peut user d'un tel pouvoir d'injonction que si le requérant fait également état, à l'appui de ses conclusions à fin d'injonction, de ce que la poursuite de ce préjudice, ainsi réparé sur le terrain de la responsabilité sans faute du maître de l'ouvrage, trouve sa cause au moins pour partie dans une faute du propriétaire de l'ouvrage. Il peut alors enjoindre à la personne publique, dans cette seule mesure, de mettre fin à ce comportement fautif ou d'en pallier les effets.,,3) En ordonnant à la commune de dévitaliser le système racinaire contribuant à la présence de drageons et de rejets de faux-acacias sur le terrain de la requérante, jusqu'à cessation de tels rejets, et d'abattre le frêne implanté sur le domaine public au voisinage de la propriété de cette société, alors qu'elle avait engagé la responsabilité de la commune sur le terrain de la responsabilité sans faute du propriétaire de l'ouvrage public à l'égard d'un tiers sans rechercher, d'une part, si la société requérante avait fondé ses conclusions à fin d'injonction sur une faute de la commune en cette qualité de propriétaire de l'ouvrage à l'origine d'une partie au moins des dommages et, d'autre part, si les mesures demandées tendaient uniquement à mettre fin à ce comportement fautif ou à en pallier les effets, une cour administrative d'appel commet une erreur de droit.</ANA>
<ANA ID="9B"> 60-01-02-01-03-01-01 1) Lorsque le juge administratif statue sur un recours indemnitaire tendant à la réparation d'un préjudice imputable à un comportement fautif d'une personne publique et qu'il constate que ce comportement et ce préjudice perdurent à la date à laquelle il se prononce, il peut, en vertu de ses pouvoirs de pleine juridiction et lorsqu'il est saisi de conclusions en ce sens, enjoindre à la personne publique en cause de mettre fin à ce comportement ou d'en pallier les effets.... ...Lorsqu'il met à la charge de la personne publique la réparation d'un préjudice grave et spécial imputable à la présence ou au fonctionnement d'un ouvrage public, il ne peut user d'un tel pouvoir d'injonction que si le requérant fait également état, à l'appui de ses conclusions à fin d'injonction, de ce que la poursuite de ce préjudice, ainsi réparé sur le terrain de la responsabilité sans faute du maître de l'ouvrage, trouve sa cause au moins pour partie dans une faute du propriétaire de l'ouvrage. Il peut alors enjoindre à la personne publique, dans cette seule mesure, de mettre fin à ce comportement fautif ou d'en pallier les effets.,,2) En ordonnant à la commune de dévitaliser le système racinaire contribuant à la présence de drageons et de rejets de faux-acacias sur le terrain de la requérante, jusqu'à cessation de tels rejets, et d'abattre le frêne implanté sur le domaine public au voisinage de la propriété de cette société, alors qu'elle avait engagé la responsabilité de la commune sur le terrain de la responsabilité sans faute du propriétaire de l'ouvrage public à l'égard d'un tiers sans rechercher, d'une part, si la société requérante avait fondé ses conclusions à fin d'injonction sur une faute de la commune en cette qualité de propriétaire de l'ouvrage à l'origine d'une partie au moins des dommages et, d'autre part, si les mesures demandées tendaient uniquement à mettre fin à ce comportement fautif ou à en pallier les effets, une cour administrative d'appel commet une erreur de droit.</ANA>
<ANA ID="9C"> 67-03-03 1) Lorsque le juge administratif statue sur un recours indemnitaire tendant à la réparation d'un préjudice imputable à un comportement fautif d'une personne publique et qu'il constate que ce comportement et ce préjudice perdurent à la date à laquelle il se prononce, il peut, en vertu de ses pouvoirs de pleine juridiction et lorsqu'il est saisi de conclusions en ce sens, enjoindre à la personne publique en cause de mettre fin à ce comportement ou d'en pallier les effets.... ...Lorsqu'il met à la charge de la personne publique la réparation d'un préjudice grave et spécial imputable à la présence ou au fonctionnement d'un ouvrage public, il ne peut user d'un tel pouvoir d'injonction que si le requérant fait également état, à l'appui de ses conclusions à fin d'injonction, de ce que la poursuite de ce préjudice, ainsi réparé sur le terrain de la responsabilité sans faute du maître de l'ouvrage, trouve sa cause au moins pour partie dans une faute du propriétaire de l'ouvrage. Il peut alors enjoindre à la personne publique, dans cette seule mesure, de mettre fin à ce comportement fautif ou d'en pallier les effets.,,2) En ordonnant à la commune de dévitaliser le système racinaire contribuant à la présence de drageons et de rejets de faux-acacias sur le terrain de la requérante, jusqu'à cessation de tels rejets, et d'abattre le frêne implanté sur le domaine public au voisinage de la propriété de cette société, alors qu'elle avait engagé la responsabilité de la commune sur le terrain de la responsabilité sans faute du propriétaire de l'ouvrage public à l'égard d'un tiers sans rechercher, d'une part, si la société requérante avait fondé ses conclusions à fin d'injonction sur une faute de la commune en cette qualité de propriétaire de l'ouvrage à l'origine d'une partie au moins des dommages et, d'autre part, si les mesures demandées tendaient uniquement à mettre fin à ce comportement fautif ou à en pallier les effets, une cour administrative d'appel commet une erreur de droit.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 27 juillet 2015,,, n° 367484, p. 285.,,[RJ2] Rappr., s'agissant de la responsabilité pour faute, CE, 27 juillet 2015,,, n° 367484, p. 285.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
