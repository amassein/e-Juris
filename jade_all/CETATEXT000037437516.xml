<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037437516</ID>
<ANCIEN_ID>JG_L_2018_09_000000401511</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/43/75/CETATEXT000037437516.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 26/09/2018, 401511, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-09-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401511</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY ; BOUTHORS</AVOCATS>
<RAPPORTEUR>M. Olivier  Fuchs</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:401511.20180926</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Forge France a demandé au tribunal administratif de Châlons-en-Champagne d'annuler pour excès de pouvoir la décision du 1er octobre 2013 par laquelle le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a, d'une part, annulé la décision du 1er février 2013 de l'inspecteur du travail de la 1ère section de l'unité territoriale des Ardennes l'autorisant à licencier M. B...A...et, d'autre part, refusé cette autorisation. Par un jugement n° 1302159 du 31 décembre 2014, le tribunal administratif de Châlons-en-Champagne a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15NC00337 du 12 mai 2016, la cour administrative d'appel de Nancy a, sur appel de la société Forge France, annulé ce jugement et la décision du ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social du 1er octobre 2013.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 13 juillet et 13 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société Forge France ;<br/>
<br/>
              3°) de mettre à la charge de la société Forge France la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Fuchs, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de M. B...A...et à Me Bouthors, avocat de la société Forge France ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Forge France a sollicité le 11 janvier 2013 l'autorisation de licencier pour motif économique M.A..., salarié protégé ; que, par une décision du 1er octobre 2013, le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a annulé la décision du 1er février 2013 par laquelle l'inspecteur du travail avait autorisé ce licenciement et a rejeté la demande de l'employeur au motif qu'elle ne précisait pas la nature de la cause économique du licenciement ; que, par un jugement du 31 décembre 2014, le tribunal administratif de Châlons-en-Champagne a rejeté la demande de la société Forge France tendant à l'annulation de cette décision du 1er octobre 2013 ; que M. A...se pourvoit en cassation contre l'arrêt du 12 mai 2016 par lequel la cour administrative d'appel de Nancy a, sur appel de la société, annulé ce jugement et cette décision ; <br/>
<br/>
              2. Considérant qu'en vertu des dispositions du code du travail, les salariés légalement investis de fonctions représentatives bénéficient, dans l'intérêt de l'ensemble des salariés qu'ils représentent, d'une protection exceptionnelle ; que, lorsque le licenciement d'un de ces salariés est envisagé, ce licenciement ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale de l'intéressé ; que, dans le cas où la demande d'autorisation de licenciement présentée par l'employeur est fondée sur un motif de caractère économique, il appartient à l'inspecteur du travail et, le cas échéant, au ministre, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si la situation de l'entreprise justifie le licenciement du salarié ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article R. 2421-10 du code du travail, " la demande d'autorisation de licenciement (...) énonce les motifs du licenciement envisagé " ; que lorsque l'employeur sollicite de l'inspecteur du travail l'autorisation de licencier un salarié protégé, il lui appartient de faire état avec précision, dans sa demande, ou le cas échéant dans un document joint à cet effet auquel renvoie sa demande, de la cause justifiant, selon lui, ce licenciement ; que si le licenciement a pour cause la réorganisation de l'entreprise, il appartient à l'employeur de préciser si cette réorganisation est justifiée par des difficultés économiques, par la nécessité de sauvegarder la compétitivité de l'entreprise ou encore par des mutations technologiques ;<br/>
<br/>
              4. Considérant que, pour juger que la demande de licenciement de M. A...présentée par la société Forge France permettait à l'autorité administrative d'appréhender la cause de ce licenciement, la cour administrative d'appel s'est fondée sur ce que cette demande, d'une part, faisait état de la réorganisation de l'entreprise et, d'autre part, comportait, parmi ses annexes, un document relatif à la présentation d'un projet de réorganisation par l'employeur au comité d'entreprise, dans lequel une phrase mentionnait l'intention de sauvegarder la compétitivité de l'entreprise ; qu'il résulte de ce qui a été dit au point 3 qu'en jugeant qu'une telle demande de l'employeur satisfaisait aux dispositions, citées ci-dessus, de l'article R. 2421-10 du code du travail, la cour administrative d'appel de Nancy a commis une erreur de droit ; que, dès lors, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Forge France une somme de 3 000 euros à verser à M. A...au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce que soit mise à la charge de M.A..., qui n'est pas, dans la présente instance, la partie perdante, la somme que demande, au même titre, la société Forge France ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy. <br/>
Article 3 : La société Forge France versera une somme de 3 000 euros à M. A... au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la société Forge France au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. B...A...et à la société Forge France.<br/>
Copie en sera adressée à la ministre du travail.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
