<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043328481</ID>
<ANCIEN_ID>JG_L_2021_04_000000427931</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/32/84/CETATEXT000043328481.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 02/04/2021, 427931</TITRE>
<DATE_DEC>2021-04-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427931</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:427931.20210402</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Serpe a demandé au tribunal administratif de Montpellier d'annuler pour excès de pouvoir la décision implicite par laquelle le maire de Mauguio (Hérault) a refusé de lui délivrer un certificat d'obtention d'un permis de construire tacite et d'enjoindre à l'administration de lui délivrer ce certificat. Par un jugement n° 1603466 du 14 septembre 2017, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17MA04408 du 11 décembre 2018, la cour administrative d'appel de Marseille a rejeté l'appel formé par la société Serpe contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 12 février et 13 mai 2019 et le 30 avril 2020 au secrétariat du contentieux du Conseil d'Etat, la société Serpe demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Mauguio la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code des relations entre le public et l'administration ;  <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Richard, avocat de la société Serpe et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune de Mauguio ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Forénergie et la société Serpe ont conjointement déposé le 12 février 2015 auprès du maire de Mauguio (Hérault) une demande de permis de construire sur laquelle la société Forénergie était désignée comme " demandeur " et la société Serpe comme " autre demandeur ". Avant le terme du délai d'instruction, le maire de Mauguio a refusé de délivrer le permis sollicité par une décision expresse du 9 mars 2015, notifiée à la seule société Forénergie. La société Serpe a alors, une fois expiré le délai d'instruction, sollicité la délivrance d'un certificat de permis tacite, qui a été implicitement refusé par le maire de Mauguio. Elle demande l'annulation de l'arrêt du 11 décembre 2018 par lequel la cour administrative d'appel de Marseille a rejeté son appel formé contre le jugement du 14 septembre 2017 par lequel le tribunal administratif de Montpellier a rejeté sa demande tendant à l'annulation du refus opposé par le maire de Mauguio à sa demande de certificat. <br/>
<br/>
              2. Aux termes de l'article L. 424-1 du code de l'urbanisme : " L'autorité compétente se prononce par arrêté sur la demande de permis ou, en cas d'opposition ou de prescriptions, sur la déclaration préalable (...) ". Aux termes de l'article L. 424-2 du même code : " Le permis est tacitement accordé si aucune décision n'est notifiée au demandeur à l'issue du délai d'instruction (...) ". Enfin, l'article R. 423-1 dispose que : " Les demandes de permis de construire, d'aménager ou de démolir et les déclarations préalables sont adressées par pli recommandé avec demande d'avis de réception ou déposées à la mairie de la commune dans laquelle les travaux sont envisagés : / a) Soit par le ou les propriétaires du ou des terrains, leur mandataire ou par une ou plusieurs personnes attestant être autorisées par eux à exécuter les travaux ; / b) Soit, en cas d'indivision, par un ou plusieurs coindivisaires ou leur mandataire ; / c) Soit par une personne ayant qualité pour bénéficier de l'expropriation pour cause d'utilité publique ". <br/>
<br/>
              3. Il résulte de ces dispositions que, lorsqu'une demande de permis de construire est présentée par plusieurs personnes et que l'autorité administrative compétente prend une décision de rejet fondée sur l'impossibilité de réaliser légalement la construction envisagée, la notification de ce refus exprès à l'un des demandeurs avant l'expiration du délai d'instruction fait obstacle à la naissance d'un permis de construire tacite au terme de ce délai, y compris à l'égard des demandeurs auxquels ce refus n'a pas été notifié avant l'expiration du délai. Il ne peut en aller autrement que lorsque la décision expresse de refus, notifiée avant l'expiration du délai d'instruction à l'un des demandeurs, ne rejette la demande de permis qu'en tant qu'elle émane de cette personne et pour des motifs propres à son projet de construction, notamment pour le motif qu'elle ne dispose pas d'un titre l'habilitant à construire, une telle décision ne faisant alors, par elle-même, pas obstacle à la naissance éventuelle d'un permis tacite à l'issue du délai d'instruction au profit des autres demandeurs pour leur propre projet de construction.<br/>
<br/>
              4. En premier lieu, il résulte de ce qui vient d'être dit qu'en jugeant, alors qu'il ressortait des pièces du dossier qui lui était soumis que le refus du 9 mars 2015 notifié à la société Forénergie était fondé sur le caractère inconstructible du terrain d'assiette du projet, que la notification de cette décision à la société Forénergie avant l'expiration du délai d'instruction avait fait obstacle à la naissance, au terme de ce délai, d'un permis de construire tacite au bénéfice de la société Serpe, la cour administrative d'appel n'a pas commis d'erreur de droit.<br/>
<br/>
              5. En second lieu, il résulte des termes mêmes de l'arrêt attaqué que, s'il relève que la société Forénergie et la société Serpe ont le même représentant légal, ce motif revêt un caractère surabondant. Par suite, le moyen tiré de ce qu'il serait entaché d'erreur de droit est inopérant.<br/>
<br/>
              6. Il résulte de tout ce qui précède que le pourvoi de la société Serpe doit être rejeté, ainsi que, par voie de conséquence, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Serpe la somme de 3 000 euros à verser à la commune de Mauguio au titre des frais exposés par elle et non compris dans les dépens.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de la société Serpe est rejeté.<br/>
<br/>
Article 2 : La société Serpe versera la somme de 3 000 euros à la commune de Mauguio au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société Serpe et à la commune de Mauguio.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-03-025-02-01-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. NATURE DE LA DÉCISION. OCTROI DU PERMIS. PERMIS TACITE. EXISTENCE OU ABSENCE D'UN PERMIS TACITE. - PERMIS DEMANDÉ PAR PLUSIEURS PERSONNES - REJET FONDÉ SUR L'IMPOSSIBILITÉ DE RÉALISER LÉGALEMENT LA CONSTRUCTION NOTIFIÉ À L'UN DES DEMANDEURS - CONSÉQUENCE - 1) PRINCIPE - ABSENCE DE NAISSANCE D'UN PERMIS TACITE À L'ÉGARD DE TOUS LES DEMANDEURS [RJ1] - 2) TEMPÉRAMENT - MOTIF DE REJET PROPRE AU SEUL DEMANDEUR AUQUEL IL A ÉTÉ NOTIFIÉ [RJ2].
</SCT>
<ANA ID="9A"> 68-03-025-02-01-02 1) Il résulte des articles L. 424-1, L. 424-2 et R. 423-1 du code de l'urbanisme que, lorsqu'une demande de permis de construire est présentée par plusieurs personnes et que l'autorité administrative compétente prend une décision de rejet fondée sur l'impossibilité de réaliser légalement la construction envisagée, la notification de ce refus exprès à l'un des demandeurs avant l'expiration du délai d'instruction fait obstacle à la naissance d'un permis de construire tacite au terme de ce délai, y compris à l'égard des demandeurs auxquels ce refus n'a pas été notifié avant l'expiration du délai.... ...2) Il ne peut en aller autrement que lorsque la décision expresse de refus, notifiée avant l'expiration du délai d'instruction à l'un des demandeurs, ne rejette la demande de permis qu'en tant qu'elle émane de cette personne et pour des motifs propres à son projet de construction, notamment pour le motif qu'elle ne dispose pas d'un titre l'habilitant à construire, une telle décision ne faisant alors, par elle-même, pas obstacle à la naissance éventuelle d'un permis tacite à l'issue du délai d'instruction au profit des autres demandeurs pour leur propre projet de construction.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., jugeant que le permis de construire n'est pas délivré en considération de la personne, CE, 10 décembre 1965, Syndicat des copropriétaires de l'immeuble Pharo-Pasteur, n°s 53773 60304, p. 684.,,[RJ2] Rappr., jugeant qu'en cas de pluralité de demandeurs, chacun doit justifier d'un titre l'habilitant à construire, CE, 14 octobre 2009, Commune de Messanges, n° 297727, T. p. 989.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
