<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035245540</ID>
<ANCIEN_ID>JG_L_2017_07_000000400424</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/24/55/CETATEXT000035245540.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 19/07/2017, 400424, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-07-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400424</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:400424.20170719</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 6 juin 2016 et 30 mars 2017 au secrétariat du contentieux du Conseil d'Etat, l'association France Nature Environnement demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2015-1782 du 28 décembre 2015 modifiant diverses dispositions de la partie réglementaire du code de l'urbanisme ; <br/>
<br/>
              2°) subsidiairement, de surseoir à statuer et de renvoyer à la Cour de justice de l'Union européenne une question préjudicielle portant sur l'interprétation de l'article 8 bis de la directive 2001/92/CE du 13 décembre 2001 dans sa rédaction issue de la directive 2014/52/UE du 16 avril 2014 ;<br/>
<br/>
              3°) de mettre à la charge de l'État la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 2001/42/CE du Parlement européen et du Conseil du 27 juin 2001 ;<br/>
              - la directive 2011/92/UE du Parlement européen et du Conseil du 13 décembre 2011 ;<br/>
              - la directive 2014/52/UE du Parlement européen et du Conseil du 16 avril 2014 ;<br/>
              - le code l'environnement ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que l'association France Nature Environnement demande l'annulation pour excès de pouvoir du décret  du 28 décembre 2015 modifiant diverses dispositions de la partie réglementaire du code de l'urbanisme ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes du 2 de l'article 2 de la directive du 13 décembre 2011 concernant l'évaluation des incidences de certains projets publics et privés sur l'environnement, telle que modifiée par la directive du 16 avril 2014 : "  L'évaluation des incidences sur l'environnement peut être intégrée dans les procédures existantes d'autorisation des projets dans les États membres ou, à défaut, dans d'autres procédures ou dans celles à établir pour répondre aux objectifs de la présente directive. " ; qu'en vertu du 1 de l'article 8 bis de cette directive, issu de la directive du 16 avril 2014, l'autorisation du projet doit comprendre notamment les éventuelles conditions environnementales jointes à la décision ainsi qu'une description de toutes les caractéristiques du projet ou mesures envisagées pour éviter, prévenir ou réduire et, si possible, compenser des incidences négatives notables sur l'environnement, ainsi que, le cas échéant, des mesures de suivi ; que l'article 2 de la directive du 16 avril 2014 a fixé au 16 mai 2017 la date limite de transposition de cette directive pour les États membres, sans préjudice des dispositions de l'article 3 qui retiennent également le 16 mai 2017 comme date à prendre en compte afin de déterminer pour chaque projet, en fonction de l'état de la mise en oeuvre des exigences résultant de la directive, si lui sont applicables les exigences modifiées par la directive du 16 avril 2014 ; <br/>
<br/>
              3. Considérant que le décret attaqué a modifié les articles R. 431-16, R. 441-5 et R. 443-5 du code de l'urbanisme afin de prévoir que l'obligation de joindre l'étude d'impact aux dossiers de demandes de permis de construire et de permis d'aménager ne concerne que les cas où l'étude d'impact est exigée au titre du permis de construire auquel est soumis le projet figurant dans l'énumération du tableau annexé à l'article R. 122-2 du code de l'environnement ; que l'association requérante soutient que ces dispositions, dans leur rédaction alors applicable, en ce qu'elles ne prévoyaient pas l'obligation de joindre l'étude d'impact au dossier de demande de permis de construire ou de permis d'aménager lorsque l'étude d'impact est exigée au titre de polices administratives autres que celle de l'urbanisme, ont sérieusement compromis l'atteinte du résultat recherché par l'article 8 bis de la directive du 13 décembre 2011 analysé au point 2      ci-dessus ; que, cependant, les dispositions citées ci-dessus de la directive laissent aux autorités nationales le choix de la prise en compte des incidences sur l'environnement dans les procédures existantes d'autorisations ou dans d'autres procédures ; que le parti alors retenu par le pouvoir règlementaire était de les intégrer aux autorisations prises au titre des polices autres que celle de l'urbanisme ; que les dispositions attaquées n'ont, par suite, et sans qu'il soit besoin de saisir la Cour des justice de l'Union européenne d'une question tendant à l'interprétation du droit de l'Union européenne, pas été de nature à compromettre sérieusement l'atteinte du résultat recherché par la directive ; <br/>
<br/>
              4. Considérant, en deuxième lieu, qu'aux termes du 4 de l'article 6 de la directive du 13 décembre 2011 concernant l'évaluation des incidences de certains projets publics et privés sur l'environnement : " A un stade précoce de la procédure, le public concerné se voit donner des possibilités effectives de participer au processus décisionnel en matière d'environnement visé à l'article 2, paragraphe 2, et, à cet effet, il est habilité à adresser des observations et des avis, lorsque toutes les options sont envisageables, à l'autorité ou aux autorités compétentes avant que la décision concernant la demande d'autorisation ne soit prise " ; qu'aux termes de l'article 8 de la même directive : " Le résultat des consultations et les informations recueillies conformément aux articles 5 à 7 sont dûment pris en compte dans le cadre de la procédure d'autorisation. " ;<br/>
<br/>
              5. Considérant que l'article L. 103-2 du code de l'urbanisme institue une procédure de concertation obligatoire pour certains projets d'aménagement et de construction  associant les habitants, associations et autres personnes concernées pendant la durée de l'élaboration du projet ; qu'en vertu du premier alinéa de l'article L. 300-2, les autres projets d'aménagement et de construction peuvent être soumis à titre facultatif à cette procédure ; que les articles R. 300-1 et R. 300-2 du code de l'urbanisme, dans leur rédaction issue du décret attaqué, précisent les modalités de prise en compte de la concertation organisée sur le fondement de l'article L. 300-2 du même code ;<br/>
<br/>
              6. Considérant que la soumission d'un projet à une procédure d'enquête publique doit être regardée comme une modalité d'information et de participation du public assurant la mise en oeuvre des objectifs fixés par la directive du 13 décembre 2011 dont le délai de transposition est expiré ; que le droit national, prévoyant l'organisation d'enquêtes publiques pour des projets d'aménagement et de construction, est ainsi conforme à ces objectifs ; qu'il s'ensuit que le moyen tiré de ce que les dispositions du II de l'article 9 du décret attaqué, définissant les conditions d'entrée en vigueur des articles R. 300-1 et R. 300-2 du code de l'urbanisme, seraient illégales au motif qu'elles conduiraient à maintenir en vigueur un état du droit national contraire aux objectifs des articles 6 et 8 de la directive du 13 décembre 2011 ne peut, dès lors, qu'être écarté ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que l'association France Nature Environnement n'est pas fondée à demander l'annulation du décret attaqué ; que, par suite, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative doivent également être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'association France Nature Environnement est rejetée.<br/>
Article 2 : La présente décision sera notifiée à l'association France Nature Environnement, au ministre d'Etat, ministre de la transition écologique et solidaire et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
