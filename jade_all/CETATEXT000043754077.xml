<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043754077</ID>
<ANCIEN_ID>JG_L_2021_07_000000448790</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/75/40/CETATEXT000043754077.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 02/07/2021, 448790, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448790</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:448790.20210702</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
               Procédure contentieuse antérieure<br/>
<br/>
               Le médecin conseil, chef de l'échelon local du service médical de la caisse primaire d'assurance-maladie (CPAM) des Bouches-du-Rhône a porté plainte à l'encontre de Mme A... B... devant la section des assurances sociales de la chambre disciplinaire de première instance du conseil régional des régions Provence-Alpes-Côte d'Azur et Corse de l'ordre des infirmiers. Par une décision du 20 décembre 2019, la section des assurances sociales a infligé à Mme B... la sanction de l'interdiction de donner des soins aux assurés sociaux pendant une durée de six mois avec sursis.<br/>
<br/>
               Par une décision du 18 novembre 2020, la section des assurances sociales du Conseil national de l'ordre des infirmiers a, sur appels de Mme B... et du médecin conseil, chef de l'échelon local du service médical de la CPAM des Bouches-du-Rhône, annulé cette décision et infligé à Mme B... la sanction de l'interdiction de donner des soins aux assurés sociaux pendant une durée de six mois, dont trois mois avec sursis.<br/>
<br/>
               Procédures devant le Conseil d'Etat<br/>
<br/>
               1° Sous le n° 448790, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 janvier et 24 février 2021 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat :<br/>
<br/>
               1°) d'annuler la décision de la section des assurances sociales du Conseil national de l'ordre des infirmiers ;<br/>
<br/>
               2°) de mettre à la charge de la CPAM des Bouches-du-Rhône la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
               2° Sous le n° 450118, par une requête, enregistrée le 24 février 2021 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat d'ordonner qu'il soit sursis à l'exécution de la même décision de la section des assurances sociales du Conseil national de l'ordre des infirmiers.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
               Vu les autres pièces des dossiers ;<br/>
<br/>
               Vu :<br/>
               - le code de la sécurité sociale ;<br/>
               - le code de la santé publique ;<br/>
               - l'arrêté du 27 mars 1972 fixant la nomenclature générale des actes professionnels des médecins, des chirurgiens-dentistes, des sages-femmes et des auxiliaires médicaux ;<br/>
               - le code de justice administrative ;<br/>
<br/>
<br/>
               Après avoir entendu en séance publique :<br/>
<br/>
               - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
               - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
               La parole ayant été donnée, après les conclusions, à Me Le Prado, avocat de Mme B... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
               1. Les requêtes visées ci-dessus de Mme B... présentent à juger des questions semblables, il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
               2. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
               3. Pour demander l'annulation de la décision de la section des assurances sociales du Conseil national de l'ordre des infirmiers qu'elle attaque, Mme B... soutient qu'elle est entachée :<br/>
               - d'insuffisance de motivation s'agissant de l'irrégularité du cumul d'actes AIS3 et AMI1 ;<br/>
               - d'erreur de droit et d'inexacte qualification juridique des faits en ce que qu'elle juge qu'est contraire à l'article L. 162-1-7 du code de la sécurité sociale et à l'article 10 du chapitre I du titre XVI de la nomenclature générale des actes professionnels la facturation d'actes de surveillance ou d'observation d'un patient lors de la mise en œuvre d'un traitement ;<br/>
               - de dénaturation des pièces du dossier en ce qu'elle estime qu'elle a facturé à tort des majorations de nuit pour la mise en œuvre d'une ordonnance prévoyant des contrôles de glycémie à jeun d'un patient diabétique ;<br/>
               - d'erreur de droit en ce qu'elle juge qu'elle n'a pas respecté la durée prévue par la nomenclature générale des actes professionnels pour les séances de soins infirmiers, en se fondant seulement sur les décomptes de la caisse de sécurité sociale ;<br/>
               - de disproportion entre la sanction infligée et les fautes reprochées.<br/>
<br/>
               4. Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
               5. La présente décision n'admettant pas le pourvoi en cassation de la requérante dirigé contre la décision de la section des assurances sociales du Conseil national de l'ordre des infirmiers, les conclusions de sa requête tendant à ce qu'il soit sursis à l'exécution de la même décision sont privées d'objet. Il n'y a plus lieu d'y statuer.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
               --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions de la requête n° 450118.<br/>
Article 2 : Le pourvoi n° 448790 de Mme B... n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à Mme A... B....<br/>
Copie en sera adressée à la caisse primaire d'assurance-maladie des Bouches-du-Rhône et au Conseil national de l'ordre des infirmiers.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
