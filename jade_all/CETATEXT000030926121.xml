<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030926121</ID>
<ANCIEN_ID>JG_L_2015_07_000000390808</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/92/61/CETATEXT000030926121.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème SSJS, 22/07/2015, 390808, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390808</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème SSJS</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Camille Pascal</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:390808.20150722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A..., à l'appui de sa demande contestant le rejet opposé le 18 décembre 2012 à sa demande de révision pour aggravation de la pension militaire d'invalidité dont il est titulaire, a produit un mémoire, enregistré le 5 mars 2015 au greffe du tribunal des pensions de Paris, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lequel il soulève une question prioritaire de constitutionnalité.<br/>
<br/>
              Par un jugement n° 15/00005 du 29 mai 2015, enregistré le 8 juin 2015 au secrétariat du contentieux du Conseil d'Etat, le tribunal des pensions de Paris, avant qu'il soit statué sur la requête de M.A..., a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité de l'article L. 29 du code des pensions militaires d'invalidité et des victimes de la guerre aux droits et libertés garantis par la Constitution.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code des pensions militaires d'invalidité et des victimes de la guerre ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Camille Pascal, conseiller d'Etat, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;  <br/>
<br/>
              2.	Considérant qu'aux termes du 3ème alinéa de l'article L. 29 du code des pensions militaires d'invalidité et des victimes de la guerre : " La pension ayant fait l'objet de la demande est révisée lorsque le degré d'invalidité résultant de l'infirmité ou de l'ensemble des infirmités est reconnu supérieur de 10 % au moins du pourcentage antérieur " ; <br/>
<br/>
              3.	Considérant que, pour demander au Conseil d'Etat de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de ces dispositions de l'article L. 29, M. A...soutient qu'en ce qu'elles ne permettent la révision de la pension que lorsque l'aggravation est supérieure à 10 % au moins du pourcentage d'invalidité antérieur, elles méconnaissent, d'une part, le principe de responsabilité et le droit à la réparation intégrale du préjudice qui découle de l'article 4 de la Déclaration des droits de l'homme et du citoyen, d'autre part, le principe d'égalité résultant des articles 1er et 6 de la Déclaration ; <br/>
<br/>
              4.	Considérant, en premier lieu, ainsi que le Conseil constitutionnel l'a jugé, que s'il résulte de l'article 4 de la Déclaration des droits de l'homme et du citoyen qu'en principe tout fait quelconque de l'homme qui cause à autrui un dommage oblige celui par la faute duquel il est arrivé à le réparer, le législateur peut toutefois, pour un motif d'intérêt général, apporter à ce principe des exclusions ou des limitations, à condition qu'il n'en résulte pas une atteinte disproportionnée aux droits des victimes d'actes fautifs ; que l'objectif d'intérêt général qui s'attache au droit à réparation due aux militaires, aux victimes de la guerre et aux victimes d'actes de terrorisme ainsi qu'à leurs ayants droit institué par le code des pensions militaires d'invalidité et des victimes de la guerre est de nature à justifier l'absence de prise en considération, en dessous d'un seuil défini par le législateur, de certaines aggravations des infirmités ou maladies pensionnées ; <br/>
<br/>
              5.	Considérant, en second lieu, que le principe d'égalité ne s'oppose ni à ce que le législateur règle de façon différente des situations différentes, ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général, pourvu que, dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la loi qui l'établit ; que la seule circonstance que, dans le régime des pensions militaires d'invalidité qui est propre aux militaires, aux victimes civiles de la guerre et aux victimes d'actes de terrorisme, il existe un seuil d'aggravation en deçà duquel la demande d'aggravation ne peut être accueillie ne suffit pas, au regard de l'objet de la loi, à caractériser une atteinte au principe d'égalité ; <br/>
<br/>
              6.	Considérant qu'il résulte de ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; qu'il n'y a, par suite, pas lieu de la renvoyer au Conseil constitutionnel ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le tribunal des pensions de Paris.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M.A..., au ministre de la défense et au tribunal des pensions de Paris. Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
