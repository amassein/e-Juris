<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036253329</ID>
<ANCIEN_ID>JG_L_2017_12_000000391696</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/25/33/CETATEXT000036253329.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème chambre jugeant seule, 22/12/2017, 391696, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391696</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème chambre jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>Mme Sara-Lou Gerber</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:391696.20171222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M.  B...A...a demandé au tribunal administratif de Melun d'annuler pour excès de pouvoir la décision du 16 janvier 2012 par laquelle l'inspectrice du travail de la 5ème section de la Seine-et-Marne a autorisé son licenciement, ainsi que la décision implicite par laquelle le ministre du travail, de l'emploi et de la santé a rejeté son recours hiérarchique. Par un jugement n° 1207912 du 2 avril 2014, le tribunal administratif a annulé ces décisions.<br/>
<br/>
              Par un arrêt n° 14PA02521, 14PA02439 du 4 juin 2015, la cour administrative d'appel de Paris a rejeté les appels formés contre ce jugement par la société Imprimerie Didier Mary, la société Garnier, C..., agissant en qualité de mandataire judiciaire de celle-ci et par la société H2D Didier Mary.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 13 juillet et 13 octobre 2015 et le 28 avril 2017 au secrétariat du contentieux du Conseil d'Etat, la société Imprimerie Didier Mary, la société H2D Didier Mary, la société Garnier, C..., agissant en qualité de liquidateur judiciaire de la société Imprimerie Didier Mary et de mandataire judiciaire de la société H2D Didier Mary et la société Cabooter, agissant en qualité d'administrateur judiciaire de la société H2D Didier Mary, demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de commerce ;<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sara-Lou Gerber, auditeur,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de la société Imprimerie Didier Mary et autres ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 18 décembre 2017, présentée par la société Imprimerie Didier Mary et autres.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un jugement du 6 octobre 2011, le tribunal de commerce de Meaux a arrêté le plan de cession totale des actifs de la société Imprimerie Didier Mary, entreprise du groupe Circle Printers, à la société H2D Didier Mary, entreprise du groupe H2D, qui prévoyait le transfert de 251 des 456 salariés de la société Imprimerie Didier Mary et le licenciement de l'ensemble des salariés non-transférés ; que la société Imprimerie Didier Mary a, ultérieurement, été placée en liquidation par un jugement du même tribunal du 26 octobre 2011 ; que l'inspectrice du travail de la 5ème section de la Seine-et-Marne a, par une décision du 16 janvier 2012, autorisé le licenciement de M.A..., salarié protégé ; que M. A...a toutefois obtenu l'annulation de cette décision, ainsi que de la décision du ministre chargé du travail ayant rejeté son recours hiérarchique, par un jugement du 2 avril 2014 du tribunal administratif de Melun ; que la société Imprimerie Didier Mary et autres demandent l'annulation de l'arrêt du 4 juin 2015 par lequel la cour administrative d'appel de Paris a rejeté leurs appels dirigés contre ce jugement du 2 avril 2014 ; <br/>
<br/>
              2. Considérant qu'en vertu des dispositions du code du travail, le licenciement des salariés légalement investis de fonctions représentatives, qui bénéficient d'une protection exceptionnelle dans l'intérêt de l'ensemble des travailleurs qu'ils représentent, ne peut intervenir que sur autorisation de l'inspecteur du travail ; que, lorsque le licenciement d'un de ces salariés est envisagé, ce licenciement ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale de l'intéressé ; <br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 642-5 du code de commerce dans sa rédaction alors en vigueur, relatif au jugement du tribunal de commerce arrêtant un plan de cession : " (...) Le plan précise notamment les licenciements qui doivent intervenir dans le délai d'un mois après le jugement. Dans ce délai, ces licenciements interviennent sur simple notification du liquidateur, ou de l'administrateur lorsqu'il en a été désigné, sous réserve des droits de préavis prévus par la loi, les conventions ou les accords collectifs du travail. / Lorsque le licenciement concerne un salarié bénéficiant d'une protection particulière en matière de licenciement, ce délai d'un mois après le jugement est celui dans lequel l'intention de rompre le contrat de travail doit être manifestée " ; qu'il résulte de ces dispositions que la réalité du motif économique du licenciement et la nécessité des suppressions de postes sont examinées par le juge de la procédure collective dans le cadre du plan de cession ; que, dès lors qu'un licenciement a été autorisé par un jugement arrêtant un plan de cession, ces éléments du motif de licenciement ne peuvent être contestés qu'en exerçant les voies de recours ouvertes contre ce jugement et ne peuvent être discutés devant l'administration ; que, dès lors, en se fondant, pour annuler la décision du 16 janvier 2012, sur la circonstance que l'administration n'avait pas contrôlé la réalité du motif économique invoqué par l'employeur, alors que la réalité de ce motif de licenciement résultait du jugement du tribunal de commerce du 6 octobre 2011 arrêtant le plan de cession, la cour a commis une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner l'autre moyen de leur pourvoi, les requérants sont fondés à demander l'annulation de l'arrêt qu'ils attaquent ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative et de statuer sur les appels formés par la société Imprimerie Didier Mary, la société Garnier, C...agissant en qualité de mandataire judiciaire de la société Imprimerie Didier Mary et la société H2D Didier Mary, contre le jugement du 2 avril 2014 du tribunal administratif de Melun ;<br/>
<br/>
              5. Considérant que, pour apprécier si l'employeur a satisfait à son obligation en matière de reclassement, l'autorité administrative doit s'assurer, sous le contrôle du juge de l'excès de pouvoir, qu'il a procédé à une recherche sérieuse des possibilités de reclassement du salarié, au sein de l'entreprise puis dans les entreprises du groupe auquel elle appartient, ce dernier étant entendu comme comportant les entreprises dont l'organisation, les activités ou le lieu d'exploitation permettent, en raison des relations qui existent avec elles, d'y effectuer la permutation de tout ou partie de son personnel ; que l'employeur doit s'efforcer de proposer au salarié des offres de reclassement écrites, précises et personnalisées, portant, si possible, sur un emploi équivalent ; que le contexte d'une cessation d'activité de l'entreprise ne dispense pas l'employeur de l'obligation qui lui incombe de rechercher des offres personnalisées de reclassement pour le salarié au sein du groupe ;<br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier que l'administrateur judiciaire de la société Imprimerie Didier Mary s'est borné à solliciter, par lettre circulaire, l'ensemble des entreprises du groupe auquel appartenait la société Imprimerie Didier Mary, aux fins qu'elles lui communiquent " les emplois disponibles au sein des autres divisions et entités du groupe " en leur demandant de se fonder sur " les informations dont vous disposez sur le personnel de la Direction France " ; qu'il a, par ailleurs, diffusé à l'ensemble des salariés dont le licenciement était envisagé une liste de postes disponibles au sein d'une des sociétés du groupe Circle Printers ainsi qu'au sein de l'entreprise cessionnaire ; que, dans ces conditions, faute d'avoir procédé à un examen spécifique des possibilités de reclassement de M.A..., et alors même que ce dernier aurait fait acte de candidature pour un départ volontaire ou refusé de se voir proposer des reclassements hors de France, l'employeur ne peut être regardé comme ayant satisfait à son obligation de reclassement ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que les requérants ne sont pas fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Melun a annulé la décision de l'inspectrice du travail du 16 janvier 2012 autorisant le licenciement de M. A...ainsi que la décision du ministre chargé du travail rejetant son recours hiérarchique ; que leurs requêtes doivent être rejetées, y compris leurs conclusions présentées, en appel, au titre de l'article L. 761-1 ;<br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées à ce titre en cassation par la société Imprimerie Didier Mary et autres ;<br/>
<br/>
<br/>
<br/>
              				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt du 4 juin 2015 de la cour administrative d'appel de Paris est annulé.<br/>
Article 2 : Les requêtes présentées devant la cour administrative d'appel de Paris par la société Imprimerie Didier Mary, Me C...et la société H2D Didier Mary sont rejetées.<br/>
Article 3 : Le surplus des conclusions du pourvoi de la société Imprimerie Didier Mary et autres présenté au titre de l'article L. 761-1 du code de justice administrative est rejeté.<br/>
Article 4 : La présente décision sera notifiée à la société Imprimerie Didier Mary, premier dénommé pour l'ensemble des sociétés requérantes et à M. B...A....<br/>
Copie en sera adressée à la ministre du travail.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
