<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037641067</ID>
<ANCIEN_ID>JG_L_2018_11_000000425530</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/64/10/CETATEXT000037641067.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 22/11/2018, 425530, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-11-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425530</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:425530.20181122</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
              L'association nationale des supporters (ANS) a demandé au juge des référés du tribunal administratif de Lyon, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, d'ordonner la suspension de l'exécution de l'arrêté du 9 novembre 2018 par lequel le préfet du Rhône a interdit, à toute personne se prévalant de la qualité de supporter de l'Association sportive de Saint-Etienne (ASSE) ou se comportant comme tel, l'accès au Groupama Stadium de Décines-Charpieu et à ses abords, le 23 novembre 2018 de 8 heures à 24 heures et, d'autre part, d'enjoindre au préfet du Rhône de réexaminer la situation afin de trouver une solution intermédiaire, notamment par le recours au dialogue et à la mobilisation de stadiers par le club de Saint-Etienne. Par une ordonnance n° 1808314 du 20 novembre 2018, le juge des référés du tribunal administratif de Lyon a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 20 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, l'ANS demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
              2°) de faire droit à sa demande de première instance ;<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - le juge des référés du tribunal administratif de Nantes a entaché son ordonnance d'irrégularités dès lors qu'il a retenu à tort, d'une part, qu'il n'existait pas de mesures moins contraignantes, d'autre part, que les forces de l'ordre n'étaient pas disponibles et, enfin, qu'il n'existait pas de mesure plus protectrice de l'ordre public ;<br/>
              - la condition d'urgence est remplie dès lors que, d'une part, la décision litigieuse porte une atteinte grave et immédiate à la situation des supporters et, d'autre part, le match a lieu dans trois jours ;<br/>
              - l'arrêté porte une atteinte grave et manifestement illégale à la liberté d'aller et venir et aux libertés d'association, de réunion et d'expression des supporters stéphanois ;<br/>
              - l'arrêté est entaché d'erreurs de droit et de fait ;<br/>
              - l'arrêté est entaché d'un défaut de caractérisation d'un risque de troubles inacceptables pour l'ordre public dans le cadre d'un déplacement organisé et encadré ;<br/>
              - l'arrêté est entaché d'un défaut de caractérisation des circonstances de temps et de lieu le justifiant dès lors que, premièrement, la mesure est systématique, deuxièmement, le caractère organisé d'un déplacement n'est pas pris en compte, troisièmement, le caractère encadré du déplacement n'est pas pris en compte, quatrièmement, le périmètre à sécuriser est entaché d'une erreur d'appréciation, cinquièmement, les forces de l'ordre ne sont pas indisponibles, sixièmement, la mesure prise augmente les risques ;<br/>
              - l'arrêté est entaché d'un défaut de caractérisation de la proportionnalité de la mesure dès lors qu'il ne donne aucun chiffre sur le nombre de personnels requis et sur le nombre de personnels disponibles ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - le code du sport ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ; qu'aux termes de l'article L. 522-3 du même code : " Lorsque la demande ne présente pas un caractère d'urgence ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée, le juge des référés peut la rejeter par une ordonnance motivée sans qu'il y ait lieu d'appliquer les deux premiers alinéas de l'article L. 522-1 " ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 332-16-2 du code du sport : " Le représentant de l'Etat dans le département ou, à Paris, le préfet de police peut, par arrêté, restreindre la liberté d'aller et de venir des personnes se prévalant de la qualité de supporter d'une équipe ou se comportant comme tel sur les lieux d'une manifestation sportive et dont la présence est susceptible d'occasionner des troubles graves pour l'ordre public. / L'arrêté énonce la durée, limitée dans le temps, de la mesure, les circonstances précises de fait et de lieu qui la motivent, ainsi que le territoire sur lequel elle s'applique. / Le fait pour les personnes concernées de ne pas se conformer à l'arrêté pris en application des deux premiers alinéas est puni de six mois d'emprisonnement et d'une amende de 30 000 &#128;. (...) " ; <br/>
<br/>
              3. Considérant qu'en application de ces dispositions, le préfet du Rhône a pris, le 9 novembre 2018, un arrêté portant interdiction d'accès au périmètre du Groupama Stadium de Décines à l'occasion du match de football du 23 novembre 2018 opposant l'Olympique Lyonnais à l'Association sportive de Saint-Etienne ; que cet arrêté interdit l'accès au Groupama Stadium de Décines et à ses abords, le vendredi 23 novembre 2018 de 8 heures à 24 heures à toute personne se prévalant de la qualité de supporter de l'Association sportive de Saint-Etienne ou se comportant comme tel, au motif que la présence de ces supporters impliquerait des risques sérieux pour la sécurité des personnes et des biens ; que l'Association nationale des supporters a saisi le juge des référés du tribunal administratif de Lyon, sur le fondement de l'article L. 521-2 du code de justice administrative, d'une demande tendant à ce que celui-ci ordonne la suspension de l'exécution de l'arrêté du 9 novembre 2018 et enjoigne au préfet du Rhône de réexaminer la situation afin de trouver une solution alternative permettant le déplacement des supporters de l'équipe de Saint-Etienne, notamment par le recours au dialogue et à la mobilisation de stadiers ; que, par une ordonnance en date du 20 novembre 2008 dont l'Association nationale des supporters relève appel, le juge des référés du tribunal administratif de Lyon a rejeté cette demande ;<br/>
<br/>
              4. Considérant que les interdictions que le représentant de l'Etat dans le département peut décider, sur le fondement des dispositions de l'article L. 332-16-2 du code du sport, présentent le caractère de mesure de police ; que l'existence d'une atteinte à l'ordre public de nature à justifier de telles interdictions doit être appréciée objectivement, indépendamment du comportement des personnes qu'elles visent dès lors que leur seule présence est susceptible d'occasionner des troubles graves pour l'ordre public ;<br/>
<br/>
              5. Considérant qu'il appartient aux autorités de l'Etat d'assurer la préservation de l'ordre public et sa conciliation avec les libertés fondamentales que sont notamment la liberté d'aller et venir, la liberté d'association, la liberté de réunion et la liberté d'expression ; qu'il incombe au juge des référés d'apprécier dans chaque cas les diligences accomplies par l'administration en tenant compte des moyens dont elle dispose ainsi que des circonstances particulières de l'espèce ;<br/>
<br/>
              6. Considérant qu'il résulte de l'instruction qui s'est déroulée devant le juge des référés du tribunal administratif de Lyon qu'en raison d'un antagonisme ancien entre les supporters des clubs de football de l'Olympique lyonnais et de l'Association sportive de Saint-Etienne, des incidents violents sont survenus, à de multiples reprises, depuis le début des années 2010, en marge ou à l'occasion de rencontres opposant les deux équipes, et notamment lors de la rencontre qui s'est tenue, le 5 novembre 2017, au stade Geoffroy-Guichard ; qu'en outre, au cours des douze derniers mois, des supporters stéphanois ont été impliqués dans des troubles graves à l'ordre public lors d'au moins cinq rencontres ; que l'association requérante n'apporte, au soutien de son appel, aucun élément de nature à faire porter sur la situation litigieuse une appréciation différente de celle retenue par le juge des référés du tribunal administratif de Lyon, dont l'ordonnance n'est pas, contrairement à ce qu'elle soutient, entachée d'irrégularité ; que, dès lors, et compte tenu de l'inimitié particulière qui règne entre les supporters des deux clubs, il n'apparaît pas manifeste que, dans les circonstances de l'espèce, des mesures moins contraignantes que celles édictées par l'arrêté litigieux seraient de nature à éviter la survenance des troubles graves à l'ordre public qu'il a pour objet de prévenir, ni que l'interdiction contestée, en tant qu'elle conduirait des supporters à se déplacer à titre individuel et sans signe distinctif, risquerait d'engendrer elle-même des troubles à l'ordre public d'une gravité plus grande encore ; qu'ainsi, cette interdiction ne peut être regardée comme entachée d'une disproportion qui lui conférerait le caractère d'une atteinte grave et manifestement illégale à la liberté d'aller et venir, à la liberté d'association et de réunion et à la liberté d'expression ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède qu'il est manifeste que l'appel de l'Association nationale des supporters ne peut être accueilli ; que la requête doit, en conséquence, être rejetée, selon la procédure prévue par l'article L. 522-3 du code de justice administrative, y compris les conclusions présentées au titre des dispositions de l'article L. 761-1 du même code ;<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de l'Association nationale des supporters est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à l'Association nationale des supporters et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
