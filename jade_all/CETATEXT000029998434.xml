<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029998434</ID>
<ANCIEN_ID>JG_L_2014_12_000000373162</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/99/84/CETATEXT000029998434.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 30/12/2014, 373162, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373162</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CAPRON ; SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Jean-Marie Deligne</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:373162.20141230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              Mme B...A...a demandé au juge des référés du tribunal administratif de Nîmes, d'une part, d'ordonner sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative la suspension de l'arrêté du 9 septembre 2013 par lequel le maire de la commune de Bouillargues l'a radiée des cadres du personnel communal, d'autre part, d'enjoindre à celle-ci, sous astreinte de 100 euros par jour, en application des articles L. 911-1 et suivants du code de justice administrative, de la réintégrer dans ses fonctions et de rétablir son traitement.<br/>
<br/>
              Par une ordonnance n° 1302697 du 23 octobre 2013, le juge des référés du tribunal administratif de Nîmes a, jusqu'à l'intervention du jugement se prononçant au fond sur la légalité de la décision contestée, suspendu l'arrêté du 9 septembre 2013 et enjoint à la commune de réintégrer provisoirement Mme A...et de rétablir son traitement.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 6 et 20 novembre 2013 au secrétariat du contentieux du Conseil d'Etat, la commune de Bouillargues demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1302697 du 23 octobre 2013 du juge des référés du tribunal administratif de Nîmes ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de suspension de MmeA... ;<br/>
<br/>
              3°) de mettre à la charge de Mme A...la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marie Deligne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de la commune de Bouillargues  et à la SCP Capron, avocat de Mme A... ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision (...) " ;<br/>
<br/>
              2. Considérant qu'une mesure de radiation des cadres pour abandon de poste ne peut être régulièrement prononcée que si l'agent concerné a, préalablement à cette décision, été mis en demeure de rejoindre son poste ou de reprendre son service dans un délai approprié qu'il appartient à l'administration de fixer ; qu'une telle mise en demeure doit prendre la forme d'un document écrit, notifié à l'intéressé, et l'informant du risque couru de radiation des cadres sans procédure disciplinaire préalable ; que lorsque l'agent ne s'est ni présenté ni n'a fait connaître à l'administration aucune intention avant l'expiration du délai fixé par la mise en demeure, et en l'absence de toute justification d'ordre matériel ou médical, présentée par cet agent, de nature à expliquer le retard qu'il aurait eu à manifester un lien avec le service, cette administration est en droit d'estimer que ce lien a été rompu du fait de l'intéressé ;<br/>
<br/>
              3. Considérant que, pour ordonner la suspension de la décision contestée, le juge des référés du tribunal administratif de Nîmes a relevé qu'il résultait de l'instruction, notamment des constats d'huissier versés au dossier, que la localisation du poste de travail de MmeA..., laquelle devait recevoir une affectation correspondant à son grade et bénéficier de conditions d'emploi normales en exécution d'un jugement de ce tribunal en date du 9 juin 2011, avait pour effet d'isoler cet agent des autres employés de la commune, tant physiquement que fonctionnellement, sans que la nature de ses fonctions administratives ou l'absence de disponibilité d'un autre poste de travail dans les locaux administratifs de la mairie le justifient ; qu'il en a déduit que le moyen tiré de ce que l'agent n'avait pas entendu rompre le lien avec le service, était, en l'état de l'instruction, de nature à faire naître un doute sérieux sur la légalité de la décision de radiation des cadres prise le 9 septembre 2013 ;<br/>
<br/>
              4. Considérant qu'en se fondant ainsi sur les seules conditions de travail de Mme A...pour apprécier si cette dernière avait entendu rompre le lien avec le service, le juge des référés, qui n'a, en tout état de cause, pas recherché si ces conditions de travail étaient de nature à révéler un harcèlement moral de la part de la commune, comme le soutenait également MmeA..., a commis une erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, la commune de Bouillargues est fondée à demander l'annulation de l'ordonnance qu'elle attaque ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée par la commune, en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant, en premier lieu, que l'arrêté litigieux a pour effet de priver MmeA..., adjoint administratif territorial, de son emploi et de tout revenu ; que la commune n'invoque, de son côté, aucun intérêt public susceptible de faire obstacle à la suspension demandée ; qu'ainsi, la condition d'urgence énoncée à l'article L. 521-1 du code de justice administrative doit être regardée comme remplie ;<br/>
<br/>
              7. Considérant, en second lieu, que, si Mme A...n'a pas repris son service à l'issue de la mise en demeure qui lui a été adressée le 6 juin 2013, elle s'est néanmoins présentée sur son lieu de travail, a fait constater les conditions de travail dégradées et d'isolement, physique comme fonctionnel, qui lui étaient imposées et a produit un nouveau certificat médical relatif à l'incompatibilité de son état de santé avec de telles conditions ; qu'ainsi, en l'état de l'instruction, le moyen tiré de ce que l'intéressée ne peut, être regardée comme ayant manifesté la volonté de rompre les liens qui l'unissaient avec le service est de nature à faire naître un doute sérieux sur la légalité de l'arrêté contesté ;<br/>
<br/>
              8. Considérant qu'il y a lieu, par suite, d'ordonner la suspension de l'exécution de l'arrêté du 9 septembre 2013 et d'enjoindre au maire de Bouillargues de réintégrer, à titre provisoire, Mme A...dans les effectifs de la commune ainsi que de reprendre le versement de son traitement, jusqu'à ce qu'il ait été statué au fond sur la demande présentée par Mme A...devant le tribunal administratif de Nîmes ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, d'assortir l'injonction d'une astreinte ;<br/>
<br/>
              9. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de Mme A...qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Bouillargues le versement des sommes demandées par Mme A...au titre des dispositions de l'article L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Nîmes du 23 octobre 2013 est annulée.<br/>
Article 2 : L'exécution de l'arrêté du 9 septembre 2009 du maire de la commune de Bouillargues est suspendue.<br/>
Article 3 : Il est enjoint au maire de la commune de Bouillargues de réintégrer, à titre provisoire, Mme A...dans les effectifs de la commune et de reprendre le versement de son traitement à compter de la notification de la présente décision, jusqu'à ce qu'il soit statué sur le fond de la demande présentée par Mme A...devant le tribunal administratif de Nîmes.<br/>
Article 4 : Les conclusions de la commune de Bouillargues présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : Les conclusions de Mme A...présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à la commune de Bouillargues et à Mme B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
