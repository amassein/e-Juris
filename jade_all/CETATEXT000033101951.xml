<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033101951</ID>
<ANCIEN_ID>JG_L_2016_09_000000394243</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/10/19/CETATEXT000033101951.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 07/09/2016, 394243, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-09-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394243</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:394243.20160907</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Nîmes d'annuler pour excès de pouvoir, d'une part, la décision du 6 janvier 2015 par laquelle le directeur régional des entreprises, de la concurrence, de la consommation du travail et de l'emploi de Provence-Alpes-Côte d'Azur a homologué le document unilatéral fixant le plan de sauvegarde de l'emploi de la société GSE et, d'autre part, ce plan de sauvegarde de l'emploi. Par un jugement n° 1500581 du 21 mai 2015, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15MA02165 du 26 août 2015, la cour administrative d'appel de Marseille a, sur appel de MmeA..., annulé ce jugement ainsi que la décision du 6 janvier 2015.<br/>
<br/>
              Par un pourvoi, un mémoire complémentaire et un mémoire en réplique, enregistrés les 26 octobre, 27 novembre 2015 et 9 mars 2016 au secrétariat du contentieux du Conseil d'Etat, la société GSE demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de Mme A... ;<br/>
<br/>
              3°) de mettre à la charge de Mme A...la somme de 4 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de la société GSE et à la SCP Rocheteau, Uzan-Sarano, avocat de Mme A... ;<br/>
<br/>
              Vu la note en délibéré, présentée par la société GSE, enregistrée le 8 juillet 2016 ;<br/>
<br/>
              Vu la note en délibéré, présentée par MmeA..., enregistrée le 17 août 2016 ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 6 janvier 2015, le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi de Provence-Alpes-Côte d'Azur a homologué le document unilatéral fixant le plan de sauvegarde de l'emploi de la société GSE SAS, devenue la société GSE ; que par un arrêt du 26 août 2015 contre lequel cette société se pourvoit en cassation, la cour administrative d'appel de Marseille a annulé cette décision, en se fondant, d'une part sur l'irrégularité de la procédure de consultation du comité d'hygiène, de sécurité et des conditions de travail (CHSCT) de la société et, d'autre part, sur l'insuffisance des mesures de reclassement prévues par le plan de sauvegarde de l'emploi ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article L. 1233-61 du code du travail : " Dans les entreprises d'au moins cinquante salariés, lorsque le projet de licenciement concerne au moins dix salariés dans une même période de trente jours, l'employeur établit et met en oeuvre un plan de sauvegarde de l'emploi pour éviter les licenciements ou en limiter le nombre (...) " ; que les articles L. 1233-24-1 et L. 1233-24-4 du même code prévoient que le contenu de ce plan de sauvegarde de l'emploi est déterminé par un accord collectif d'entreprise et que, lorsque cet accord n'a pas fixé le nombre des licenciements ou la pondération des critères d'ordre des licenciements, ces éléments sont précisés par un document élaboré unilatéralement par l'employeur ; qu'aux termes de l'article L. 1233-57-2 du même code : " L'autorité administrative valide l'accord collectif mentionné à l'article L. 1233-24-1 dès lors qu'elle s'est assurée de : (...) / 2° la régularité de la procédure d'information et de consultation du comité d'entreprise et, le cas échéant, du comité d'hygiène, de sécurité et des conditions de travail (...) " ; qu'aux termes de son article L. 1233-57-3 : " (...) l'autorité administrative homologue le document élaboré par l'employeur mentionné à l'article L. 1233-24-4, après avoir vérifié (...) la régularité de la procédure d'information et de consultation du comité d'entreprise et, le cas échéant, du comité d'hygiène, de sécurité et des conditions de travail (...) " ; qu'enfin, aux termes de l'article L. 4612-8 du même code, dans sa rédaction alors en vigueur : " Le comité d'hygiène, de sécurité et des conditions de travail est consulté avant toute décision d'aménagement important modifiant les conditions de santé et de sécurité ou les conditions de travail et, notamment, avant toute transformation importante des postes de travail découlant de la modification de l'outillage, d'un changement de produits ou de l'organisation du travail (...) " ; qu'il résulte de l'ensemble de ces dispositions que lorsque l'autorité administrative est saisie d'une demande de validation d'un accord collectif ou d'homologation d'un document unilatéral fixant le contenu d'un plan de sauvegarde de l'emploi pour une opération qui, parce qu'elle modifie de manière importante les conditions de santé et de sécurité ou les conditions de travail des salariés de l'entreprise, requiert la consultation du ou des comités d'hygiène, de sécurité et des conditions de travail concernés, elle ne peut légalement accorder la validation ou l'homologation demandée que si cette consultation a été régulière ;<br/>
<br/>
              3. Considérant qu'il résulte des termes mêmes de l'arrêt attaqué que, pour faire droit au moyen tiré du caractère irrégulier de la procédure de consultation du CHSCT de la société GSE, la cour administrative d'appel a écarté comme inopérant le moyen en défense, articulé devant elle par la société GSE, tiré de ce que la consultation du CHSCT n'était pas requise pour l'opération en cause au regard des conditions fixées par l'article L. 4612-8 du code du travail ; qu'il résulte de ce qui a été dit ci-dessus que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi relatifs à la consultation du CHSCT, l'arrêt attaqué est entaché sur ce point d'erreur de droit ; <br/>
<br/>
              4. Considérant toutefois que, pour annuler la décision du 6 janvier 2015, la cour administrative d'appel de Marseille s'est, ainsi qu'il a été dit ci-dessus, également fondée sur un second moyen de nature à justifier, à lui seul, cette annulation, tiré de ce que le plan de reclassement inclus dans le plan de sauvegarde de l'emploi n'était pas conforme aux dispositions des articles L. 1233-61 et L. 1233-62 du code du travail ;<br/>
<br/>
              5. Considérant, en effet, que, lorsqu'elle est saisie d'une demande d'homologation d'un document élaboré en application de l'article L. 1233-24-4 du code du travail, il appartient à l'administration, sous le contrôle du juge de l'excès de pouvoir, de vérifier la conformité de ce document et du plan de sauvegarde de l'emploi dont il fixe le contenu aux dispositions législatives et aux stipulations conventionnelles applicables, en s'assurant notamment du respect par le plan de sauvegarde de l'emploi des dispositions des articles L. 1233-61 à L. 1233-63 du même code ; qu'à ce titre, elle doit, au regard de l'importance du projet de licenciement, apprécier si les mesures contenues dans le plan sont précises et concrètes et si, à raison, pour chacune, de sa contribution aux objectifs de maintien dans l'emploi et le reclassement des salariés, elles sont, prises dans leur ensemble, propres à satisfaire à ces objectifs compte tenu, d'une part, des efforts de formation et d'adaptation déjà réalisés par l'employeur et, d'autre part, des moyens dont disposent l'entreprise et, le cas échéant, l'unité économique et sociale et le groupe ;<br/>
<br/>
              6. Considérant qu'à ce titre, il revient notamment à l'autorité administrative de s'assurer que le plan de reclassement intégré au plan de sauvegarde de l'emploi est de nature à faciliter le reclassement des salariés dont le licenciement ne pourrait être évité ; que l'employeur doit, à cette fin, avoir identifié dans le plan l'ensemble des possibilités de reclassement des salariés dans l'entreprise ; qu'en outre, lorsque l'entreprise appartient à un groupe, l'employeur, seul débiteur de l'obligation de reclassement, doit avoir procédé à une recherche sérieuse des postes disponibles pour un reclassement dans les autres entreprises du groupe ; que pour l'ensemble des postes de reclassement ainsi identifiés, l'employeur doit avoir indiqué dans le plan leur nombre, leur nature et leur localisation ;<br/>
<br/>
              7. Considérant qu'en se fondant, pour juger insuffisantes les mesures de reclassement prévues par le plan de sauvegarde de l'emploi de la société GSE, sur la circonstance que le plan de reclassement ne faisait état d'aucune recherche des postes disponibles pour un reclassement dans les autres entreprises du groupe et sur le fait que les six postes de reclassement qu'il prévoyait dans l'entreprise ne précisaient pas leur localisation, la cour administrative d'appel, qui devait statuer, non sur l'étendue des recherches de reclassement que l'employeur alléguait avoir effectuées, mais sur les mesures prévues dans le plan dont l'homologation lui était soumise, a porté sur les pièces du dossier qui lui était soumis une appréciation souveraine qui n'est pas entachée de dénaturation et n'a pas commis d'erreur de droit ; que contrairement à ce que soutient la société GSE, la cour a procédé à une appréciation globale du caractère suffisant des mesures de reclassement et n'a, par suite, pas non plus commis d'erreur de droit sur ce point ; <br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que la société GSE n'est pas fondée à demander l'annulation de l'arrêt de la cour administrative d'appel de Marseille du 26 août 2015 ; que son pourvoi doit être rejeté ;<br/>
<br/>
              9. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de Mme A...qui n'est pas la partie perdante dans la présente instance ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société GSE une somme de 3 000 euros à verser à Mme A... au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de la société GSE est rejeté.<br/>
Article 2 : La société GSE versera à Mme A...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société GSE et à Mme B...A....<br/>
Copie en sera adressée à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
