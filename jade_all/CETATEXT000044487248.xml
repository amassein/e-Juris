<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044487248</ID>
<ANCIEN_ID>JG_L_2021_12_000000453978</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/48/72/CETATEXT000044487248.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 09/12/2021, 453978, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>453978</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>M. Olivier Guiard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Céline  Guibé</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:453978.20211209</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire et un nouveau mémoire, enregistrés les 28 septembre et 27 octobre 2021 au secrétariat du contentieux du Conseil d'Etat, présentés en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, la société Eiffage Construction Nord Aquitaine demande au Conseil d'Etat, à l'appui de son pourvoi tendant à l'annulation de l'arrêt n° 19BX03253 du 27 avril 2021 de la cour administrative d'appel de Bordeaux, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du 9ème alinéa du I et du 1er alinéa du VI de l'article L. 441-6 du code de commerce.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de commerce ;<br/>
              - le code général des impôts ; <br/>
              - la loi n° 2014-344 du 17 mars 2014 ; <br/>
              - loi n° 2016-1691 du 9 décembre 2016 ; <br/>
              - la décision du Conseil constitutionnel n° 2014-690 DC du 13 mars 2014 ; <br/>
              - la décision du Conseil constitutionnel n° 2016-741 DC du 8 décembre 2016 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Guiard, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Céline Guibé, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, au cabinet Munier-Apaire, avocat de la société Eiffage Construction Nord Aquitaine ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement de circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes de l'article L. 441-6 du code de commerce, dans sa rédaction issue de l'article 123 de la loi du 17 mars 2014 relative à la consommation : " I. - Tout producteur, prestataire de services, grossiste ou importateur est tenu de communiquer ses conditions générales de vente à tout acheteur de produits ou tout demandeur de prestations de services qui en fait la demande pour une activité professionnelle. Elles comprennent : / - les conditions de vente ; / - le barème des prix unitaires ; / - les réductions de prix ; / - les conditions de règlement. / Les conditions générales de vente peuvent être différenciées selon les catégories d'acheteurs de produits ou de demandeurs de prestation de services. Dans ce cas, l'obligation de communication prescrite au premier alinéa porte sur les conditions générales de vente applicables aux acheteurs de produits ou aux demandeurs de prestation de services d'une même catégorie. / Les conditions générales de vente constituent le socle unique de la négociation commerciale. Dans le cadre de cette négociation, tout producteur, prestataire de services, grossiste ou importateur peut convenir avec un acheteur de produits ou demandeur de prestation de services de conditions particulières de vente qui ne sont pas soumises à l'obligation de communication prescrite au premier alinéa. / Sauf dispositions contraires figurant aux conditions de vente ou convenues entre les parties, le délai de règlement des sommes dues est fixé au trentième jour suivant la date de réception des marchandises ou d'exécution de la prestation demandée. / Le délai convenu entre les parties pour régler les sommes dues ne peut dépasser quarante-cinq jours fin de mois ou soixante jours à compter de la date d'émission de la facture. En cas de facture périodique, au sens du 3 du I de l'article 289 du code général des impôts, ce délai ne peut dépasser quarante-cinq jours à compter de la date d'émission de la facture. / (...) / VI. - Sont passibles d'une amende administrative dont le montant ne peut excéder 75 000 € pour une personne physique et 375 000 € pour une personne morale le fait de ne pas respecter les délais de paiement mentionnés aux neuvième alinéas du I du présent article, ainsi que le fait de ne pas respecter les modalités de computation des délais de paiement convenues entre les parties conformément au neuvième alinéa dudit I. L'amende est prononcée dans les conditions prévues à l'article L. 465-2. Le montant de l'amende encourue est doublé en cas de réitération du manquement dans un délai de deux ans à compter de la date à laquelle la première décision de sanction est devenue définitive. (...) ". <br/>
<br/>
              3. La société Eiffage Construction Nord Aquitaine soutient que les dispositions précitées du 9ème alinéa du I et du 1er alinéa du VI de l'article L. 441-6 du code de commerce méconnaissent les principes constitutionnellement garantis de légalité des délits et des peines, de proportionnalité et d'individualisation des peines, la liberté d'entreprendre, ainsi que les principes d'impartialité et d'égalité devant la loi.  <br/>
<br/>
              4. En premier lieu, toutefois, par la décision n° 2014-690 DC du 13 mars 2014, le Conseil constitutionnel a, dans ses motifs et son dispositif, déclaré le surplus du paragraphe VI de l'article L. 441-6 du code de commerce issu de l'article 123 de la loi du 17 mars 2014 relative à la consommation, conforme à la Constitution. Or ni la décision du Conseil d'Etat n° 430130 du 3 février 2021, ni le fait que depuis 2014 les sanctions administratives infligées pour des retards de paiement, ainsi que leur montant, ont augmenté conformément à ce qu'avait envisagé le législateur, ne constituent des changements de circonstances survenus depuis la décision du 13 mars 2014, de nature à justifier que la conformité à la Constitution de ces dispositions soit à nouveau examinée par le Conseil constitutionnel. Ainsi, et alors au demeurant que la décision n° 2016-741 DC du 8 décembre 2016 a admis la conformité à la Constitution du b) du 1° du I de l'article 123 de la loi du 9 décembre 2016 relative à la transparence, à la lutte contre la corruption et à la modernisation de la vie, qui a porté de 375 000 euros à deux millions d'euros le montant maximum de l'amende administrative prévue par le VI de l'article L. 441-6 du code de commerce, il n'a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée à l'encontre du VI de l'article L. 441-6 dans sa rédaction issue de l'article 123 de la loi du 17 mars 2014.  <br/>
<br/>
              5. En deuxième lieu, à l'appui de sa question prioritaire de constitutionnalité dirigée contre les dispositions du 9ème alinéa du I de l'article L. 441-6 du code de commerce, la société Eiffage Construction Nord Aquitaine ne peut utilement invoquer le principe de légalité des délits et des peines, ni les principes de proportionnalité et d'individualisation des peines garantis par l'article 8 de la Déclaration des droits de l'homme et du citoyen de 1789, dès lors que ces dispositions, qui n'instituent à elles seules aucune sanction, se bornent à prévoir que les professionnels ne peuvent convenir de délais de paiement excédant " quarante-cinq jours fin de mois ou soixante jours à compter de la date d'émission de la facture ". <br/>
<br/>
              6. En troisième lieu, en prévoyant un délai maximum de paiement de " soixante jours à compter de la date d'émission de la facture ", auquel les professionnels ont le loisir de se référer plutôt qu'au " délai de quarante-cinq jours fin de mois " également prévu par le 9ème alinéa de l'article L. 441-6 du code de commerce, les dispositions contestées, qui tendent au rééquilibrage des relations commerciales entre professionnels et en particulier entre les créanciers dont la trésorerie souffre de retards de paiement et les débiteurs qui sont susceptibles d'en tirer avantage, ne portent pas une atteinte disproportionnée à la liberté d'entreprendre, dès lors qu'en vertu de l'article L. 441-3 du code de commerce, devenu l'article L. 441-9, la facture, qui doit mentionner sa date d'émission conformément à l'article 242 nonies A du code général des impôts, est délivrée par le vendeur dès la réalisation de la vente ou de la prestation de service et doit, le cas échéant, être réclamée par l'acheteur, quand bien même le débiteur de l'obligation de payer ne fixe pas lui-même cette date d'émission et qu'il peut en être averti avec retard si les parties n'ont pas convenu de la mise en place d'un mécanisme d'information approprié. De même, cette circonstance n'est pas de nature à caractériser, entre les débiteurs et les créanciers professionnels, une atteinte au principe d'égalité devant la loi, dès lors qu'ils se trouvent dans une situation différente au regard de l'objet des dispositions en cause. Il suit de là que les griefs tirés de la méconnaissance des articles 4 et 6 de la Déclaration des droits de l'homme et du citoyen de 1789 ne peuvent qu'être écartés.  <br/>
<br/>
              7. Il résulte de tout ce qui précède qu'il y n'a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, qui n'est pas nouvelle et ne présente pas un caractère sérieux. <br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société Eiffage Construction Nord Aquitaine. <br/>
<br/>
Article 2 : La présente décision sera notifiée à la société Eiffage Construction Nord Aquitaine et au ministre de l'économie, des finances et de la relance.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.  <br/>
<br/>
Délibéré à l'issue de la séance du 25 novembre 2021 où siégeaient : M. Frédéric Aladjidi, président de chambre, présidant ; M. Thomas Andrieu, conseiller d'Etat et M. Olivier Guiard, maître des requêtes-rapporteur. <br/>
<br/>
              Rendu le 9 décembre 2021.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Frédéric Aladjidi<br/>
 		Le rapporteur : <br/>
      Signé : M. Olivier Guiard<br/>
                 La secrétaire :<br/>
                 Signé : Mme A... B...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
