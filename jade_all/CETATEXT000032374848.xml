<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032374848</ID>
<ANCIEN_ID>JG_L_2016_04_000000394900</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/37/48/CETATEXT000032374848.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème / 7ème SSR, 04/04/2016, 394900</TITRE>
<DATE_DEC>2016-04-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394900</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème / 7ème SSR</FORMATION>
<TYPE_REC>Recours en révision</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>M. Clément Malverti</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:394900.20160404</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Versailles de condamner l'Etat à lui verser une indemnité de 3 768,66 euros, augmentée des intérêts au taux légal, en réparation du préjudice qu'il estime avoir subi du fait d'erreurs affectant la comptabilisation et la rémunération de ses heures de travail, et d'enjoindre à l'administration pénitentiaire d'exécuter le jugement à intervenir sous une astreinte de 50 euros par jour de retard. <br/>
<br/>
              Par un jugement n° 0907496 du 12 avril 2012, le tribunal administratif a rejeté cette demande.<br/>
<br/>
              Par une ordonnance n° 13VE01287 du 8 septembre 2014, la présidente de la cour administrative d'appel de Versailles a transmis au Conseil d'Etat le pourvoi formé par M. A... contre ce jugement, enregistré le 23 avril 2013 au greffe de la cour.<br/>
<br/>
              Par une décision n° 384907 du 1er octobre 2015, le Conseil d'Etat statuant au contentieux a rejeté ce pourvoi.<br/>
<br/>
              Par une requête, enregistrée le 30 novembre 2015 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) de réviser la décision du 1er octobre 2015 par laquelle le Conseil d'Etat statuant au contentieux a rejeté son pourvoi ; <br/>
<br/>
              2°) de procéder à une instruction contradictoire du pourvoi avant d'y statuer à nouveau ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Clément Malverti, auditeur,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat de M. A...; <br/>
<br/>
<br/>
              Vu la note en délibéré enregistrée le 17 mars 2016 présentée par M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que M.A..., estimant avoir effectué, entre mars 2006 et mars 2008, 997 heures de travail qui ne lui auraient pas été rémunérées alors qu'il était incarcéré à ...; que le tribunal administratif a rejeté sa demande par un jugement du 12 avril 2012 ; que M. A...a entendu contester ce jugement devant la cour administrative d'appel de Versailles ; que la requête de M.A..., enregistrée le 23 avril 2013 au greffe de la cour, a été communiquée à la garde des sceaux, ministre de la justice, qui a produit un mémoire en défense le 27 août 2014 ; que, toutefois, le jugement du tribunal administratif ayant été, eu égard au montant de l'indemnité demandée, rendu en dernier ressort par application des dispositions de l'article R. 811-1 du code de justice administrative, la présidente de la cour administrative d'appel de Versailles a transmis au Conseil d'Etat, par ordonnance du 8 septembre 2014 prise en vertu de l'article R. 351-2 du code de justice administrative, la requête de M.A..., qui présentait en réalité le caractère d'un pourvoi en cassation relevant de la compétence du Conseil d'Etat ; que, par décision du 1er octobre 2015, le Conseil d'Etat statuant au contentieux a rejeté le pourvoi de M. A...; que, par la présente requête, M. A...forme un recours en révision contre cette décision du Conseil d'Etat ; <br/>
<br/>
              2.	Considérant qu'aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux " ; qu'aux termes de l'article R. 822-2 du même code : " S'il apparaît que l'admission du pourvoi peut être refusée, le président de la sous-section transmet le dossier au rapporteur public en vue de son inscription au rôle ; le requérant ou son mandataire est averti du jour de la séance. / Dans le cas contraire, le président de la sous-section décide qu'il sera procédé à l'instruction du pourvoi dans les conditions ordinaires ; le requérant ou son mandataire est avisé de cette décision " ; que, dans le cas où le Conseil d'Etat est saisi de la contestation d'un jugement présentant le caractère d'un pourvoi en cassation, mais qui a été initialement introduit devant une cour administrative d'appel et a commencé d'être instruit devant cette cour avant d'être renvoyé au Conseil d'Etat par application de l'article R. 351-2 du code, le Conseil d'Etat statue sur ce pourvoi sans mettre en oeuvre la procédure d'admission des pourvois en cassation prévue à l'article L. 822-1 du code de justice administrative ;<br/>
<br/>
              3.	Considérant, en l'espèce, que la contestation du jugement rendu en premier et dernier ressort par le tribunal administratif de Versailles présentait le caractère d'un pourvoi en cassation ; que ce pourvoi, introduit à tort devant la cour administrative d'appel de Versailles avait commencé d'être instruit par la cour, avant d'être renvoyé au Conseil d'Etat ; que la décision du 1er octobre 2015 par laquelle le Conseil d'Etat a statué sur cette contestation n'a pas refusé l'admission de ce pourvoi en cassation, mais l'a rejeté au vu de l'instruction contradictoire qui a été menée et qui a notamment conduit à ce que le mémoire en défense produit par le ministre de la justice soit communiqué à M. A...; que ce dernier forme toutefois un recours en révision contre cette décision, en faisant valoir que l'avis d'audience et le rôle de la séance publique ont porté la mention " procédure d'admission des pourvois en cassation " et que les deux mémoires que son avocat a produits devant le Conseil d'Etat n'ont pas été communiqués au ministre de la justice ;<br/>
<br/>
              4.	Mais considérant qu'aux termes de l'article R. 834-1 du code de justice administrative : " Le recours en révision contre une décision contradictoire du Conseil d'Etat ne peut être présenté que dans trois cas : / 1° Si elle a été rendue sur pièces fausses ; / 2° Si la partie a été condamnée faute d'avoir produit une pièce décisive qui était retenue par son adversaire ; / 3° Si la décision est intervenue sans qu'aient été observées les dispositions du présent code relatives à la composition de la formation de jugement, à la tenue des audiences ainsi qu'à la forme et au prononcé de la décision. " ;<br/>
<br/>
              5.	Considérant, d'une part, qu'aucune disposition du code de justice administrative ne prévoit que l'avis d'audience ou le rôle de la séance publique comporte de mention indiquant qu'un pourvoi est examiné dans le cadre de la procédure d'admission des pourvois en cassation ; que, par suite, l'erreur de mention ayant affecté ces documents n'est pas au nombre des cas d'ouverture du recours en révision limitativement énumérés par les dispositions de l'article R. 834-1 du code de justice administrative ; <br/>
<br/>
              6.	Considérant, d'autre part, que si M. A...soutient que les deux nouveaux mémoires produits par son avocat devant le Conseil d'Etat les 2 mars et 12 juin 2015 n'ont pas été communiqués au ministre de la justice, cette circonstance n'affecte pas le respect du caractère contradictoire de la procédure à son égard et ne saurait dès lors, et en tout état de cause, être utilement invoquée par lui ; <br/>
<br/>
              7.	Considérant qu'il résulte de ce qui précède que le recours en révision formé par M. A...ne peut qu'être rejeté ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au garde des sceaux, ministre de la justice.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-08-02-03-01 PROCÉDURE. VOIES DE RECOURS. CASSATION. POUVOIRS DU JUGE DE CASSATION. ADMISSION DES POURVOIS EN CASSATION. - POURVOI AYANT FAIT L'OBJET D'UN DÉBUT D'INSTRUCTION DEVANT UNE CAA AVANT D'ÊTRE RENVOYÉ AU CONSEIL D'ETAT - POSSIBILITÉ DE METTRE EN OEUVRE LA PAPC - ABSENCE.
</SCT>
<ANA ID="9A"> 54-08-02-03-01 Dans le cas où le Conseil d'Etat est saisi de la contestation d'un jugement présentant le caractère d'un pourvoi en cassation, mais qui a été initialement introduit devant une cour administrative d'appel (CAA) et a commencé d'être instruit devant cette cour avant d'être renvoyé au Conseil d'Etat par application de l'article R. 351-2 du code de justice administrative, le Conseil d'Etat statue sur ce pourvoi sans mettre en oeuvre la procédure d'admission des pourvois en cassation (PAPC) prévue à l'article L. 822-1 de ce code.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
