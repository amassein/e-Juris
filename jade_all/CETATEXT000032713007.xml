<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032713007</ID>
<ANCIEN_ID>JG_L_2016_06_000000386209</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/71/30/CETATEXT000032713007.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 15/06/2016, 386209, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-06-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386209</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Vincent Uher</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:386209.20160615</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Rennes de prononcer la décharge de la cotisation de taxe d'habitation à laquelle il a été assujetti au titre de l'année 2011 dans les rôles de la commune de Vannes (Morbihan). Par un jugement n° 1104995 du 21 octobre 2014, le tribunal administratif a fait droit à cette demande.<br/>
<br/>
              Par un pourvoi, enregistré le 4 décembre 2014 au secrétariat du contentieux du Conseil d'État, le ministre des finances et des comptes publics demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler les articles 1er et 3 de ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M.B....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la construction et de l'habitation ;<br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              -  le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Uher, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que M.B..., étudiant, a, par un bail conclu pour une durée de dix mois à compter du 27 août 2010, pris en location un appartement meublé situé à Vannes ; qu'il a sollicité la décharge de la cotisation de taxe d'habitation à laquelle il a été assujetti au titre de l'année 2011 à raison de cet appartement au motif qu'il s'agissait d'une location saisonnière d'une durée limitée et que la propriétaire s'en réservait la disposition pendant la période estivale ; que le ministre se pourvoit en cassation contre les articles 1er  et 3 du jugement du tribunal administratif de Rennes qui a fait droit à cette demande ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 1407 du code général des impôts : " I. La taxe d'habitation est due : / 1º Pour tous les locaux meublés affectés à l'habitation (...) " ; qu'aux termes de l'article 1408 de ce code : " I. La taxe est établie au nom des personnes qui ont, à quelque titre que ce soit, la disposition ou la jouissance des locaux imposables (...) " ; qu'aux termes de l'article 1415 du même code : " La taxe foncière sur les propriétés bâties, la taxe foncière sur les propriétés non bâties et la taxe d'habitation sont établies pour l'année entière d'après les faits existants au 1er janvier de l'année de l'imposition " ; qu'aux termes de l'article L. 632-1 du code de la construction et de l'habitation : " Toute personne qui loue un logement meublé, que la location s'accompagne ou non de prestations secondaires, bénéficie d'un contrat établi par écrit d'une durée d'un an dès lors que le logement loué constitue sa résidence principale. A l'expiration de ce contrat, le bail est tacitement reconduit pour un an sous réserve des dispositions suivantes. / Lorsque la location est consentie à un étudiant, la durée du bail peut être réduite à neuf mois. Dans ce cas, la clause de reconduction tacite prévue au premier alinéa est inapplicable " ;<br/>
<br/>
              3. Considérant qu'il résulte des dispositions précitées qu'est en principe redevable de la taxe d'habitation le locataire d'un local imposable au 1er janvier de l'année d'imposition ; que, par dérogation à ce principe, lorsqu'un logement meublé fait l'objet de locations saisonnières, le propriétaire du bien est redevable de la taxe d'habitation dès lors qu'au 1er janvier de l'année de l'imposition, il peut être regardé comme entendant en conserver la disposition ou la jouissance une partie de l'année ; que ne constitue pas une location saisonnière la location d'un logement meublé par bail conclu dans les conditions prévues à l'article L. 632-1 du code de la construction et de l'habitation, au regard des caractéristiques de cette location, consentie à titre de résidence principale pour une durée d'un an, qui peut être ramenée à neuf mois lorsqu'elle est consentie à un étudiant ; que dans le cadre d'un tel bail, le locataire qui occupe le logement au 1er  janvier en a la disposition, au sens de l'article 1408 du code général des impôts ; que, dès lors, en relevant, pour prononcer la décharge de l'imposition litigieuse, que le propriétaire de l'appartement en cause ne l'avait remis en location après le départ de M. B...le 28 mai 2011 qu'à compter de la fin de l'été 2011, sans rechercher si, au regard des caractéristiques du bail invoquées par le ministre, cette location était consentie dans les conditions prévues à l'article L. 632-1 du code de la construction et de l'habitation, le tribunal administratif a commis une erreur de droit ; que, dès lors, le ministre est fondé, à demander l'annulation du jugement qu'il attaque  ;<br/>
<br/>
<br/>
<br/>              D E C I D E :<br/>
                            --------------<br/>
<br/>
Article 1er : Les articles 1er et 3 du jugement du tribunal administratif de Rennes du 21 octobre 2014 sont annulés.<br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Rennes.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre des finances et des comptes  publics et à M. A...B....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
