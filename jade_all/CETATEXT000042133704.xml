<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042133704</ID>
<ANCIEN_ID>JG_L_2020_07_000000440333</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/13/37/CETATEXT000042133704.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 16/07/2020, 440333, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440333</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:440333.20200716</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'École nationale supérieure d'architecture de Paris-Val de Seine, à l'appui de sa demande tendant à ce que le tribunal administratif de Paris prononce la restitution de la taxe sur les bureaux, commerces et locaux de stockage et surfaces de stationnement et de la taxe additionnelle sur les surfaces de stationnement qu'elle a acquittées au titre des années 2015, 2016 et 2017, a produit un mémoire distinct, enregistré le 2 mars 2020 au greffe de ce tribunal, par lequel elle soulève une question prioritaire de constitutionnalité relative à la question de la conformité aux droits et libertés garantis par la Constitution des mots " du premier et du second degré " figurant au 2° bis du V de l'article 231 ter du code général des impôts issus de l'article 9 de la loi du 28 décembre 2001 de finances pour 2002.<br/>
<br/>
              Par une ordonnance n° 1820148 du 27 avril 2020, enregistrée le 29 avril 2020 au secrétariat du contentieux du Conseil d'État, le vice-président de la 2ème section du tribunal administratif de Paris, avant qu'il soit statué sur la demande de l'École nationale supérieure d'architecture de Paris-Val de Seine, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, de transmettre au Conseil d'État cette question prioritaire de constitutionnalité. <br/>
<br/>
              Par la question prioritaire de constitutionnalité ainsi transmise et par un nouveau mémoire enregistré au secrétariat du contentieux du Conseil d'État le 28 mai 2020, l'École nationale supérieure d'architecture de Paris-Val de Seine soutient que le 2° bis du V de l'article 231 ter du code général des impôts, dans sa rédaction issue de l'article 9 de la loi de finances pour 2002 applicable au litige, méconnaît le principe d'égalité devant la loi et le principe d'égalité devant les charges publiques garantis par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen de 1789, en tant qu'il institue une différence de traitement non justifiée par un motif d'intérêt général en rapport avec l'objet de la loi et non fondée sur un critère objectif et rationnel en rapport avec le but poursuivi par le législateur, entre les établissements d'enseignement du premier et du second degré qui bénéficient de l'exonération de la taxe sur les bureaux, commerces et locaux de stockage et surfaces de stationnement et les établissements d'enseignement supérieur qui ne bénéficient pas de cette exonération. <br/>
<br/>
              Par un mémoire, enregistré le 25 juin 2020, le ministre de l'action et des comptes publics conclut à ce que la question ne soit pas renvoyée au Conseil constitutionnel. Il soutient que les conditions posées par l'article 23-4 de l'ordonnance du 7 novembre 1958 ne sont pas remplies, et, en particulier, que la question ne présente pas un caractère sérieux et qu'elle n'est pas nouvelle. <br/>
<br/>
              La question a été communiquée au Premier ministre qui n'a pas produit d'observations. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de l'éducation ; <br/>
              - le code général des impôts, notamment son article 231 ter ;<br/>
              - la loi n° 2001-1275 du 31 décembre 2001 ; <br/>
              - la loi n° 2010-1658 du 29 décembre 2010 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de l'Ecole nationale supérieure d'architecture de Paris-Val de Seine ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes de l'article 231 ter du code général des impôts dans sa rédaction applicable aux impositions contestées, issue de l'article 31 de la loi du 29 décembre 2010 de finances rectificative pour 2010 : " I. - Une taxe annuelle sur les locaux à usage de bureaux, les locaux commerciaux, les locaux de stockage et les surfaces de stationnement annexées à ces catégories de locaux est perçue, dans les limites territoriales de la région d'Ile-de-France (...). / II. - Sont soumises à la taxe les personnes privées ou publiques qui sont propriétaires de locaux imposables ou titulaires d'un droit réel portant sur de tels locaux. (...) / III. - La taxe est due : / 1° Pour les locaux à usage de bureaux, qui s'entendent, d'une part, des bureaux proprement dits et de leurs dépendances immédiates et indispensables destinés à l'exercice d'une activité, de quelque nature que ce soit, par des personnes physiques ou morales privées, ou utilisés par l'Etat, les collectivités territoriales, les établissements ou organismes publics et les organismes professionnels, et, d'autre part, des locaux professionnels destinés à l'exercice d'activités libérales ou utilisés par des associations ou organismes privés poursuivant ou non un but lucratif ; / (...) V. - Sont exonérés de la taxe : / (...) 2° bis Les locaux administratifs et les surfaces de stationnement des établissements publics d'enseignement du premier et du second degré et des établissements privés sous contrat avec l'Etat au titre des articles L. 442-5 et L. 442-12 du code de l'éducation (...) ".<br/>
<br/>
              3. L'École nationale supérieure d'architecture de Paris-Val de Seine soutient que les termes " des établissements publics d'enseignement du premier et du second degré " figurant au 2° bis du V de l'article 231 ter du code général des impôts, en ce qu'ils ont pour effet d'exonérer les locaux administratifs et les surfaces de stationnement des établissements publics d'enseignement du premier et du second degré, à l'exclusion des locaux de même nature relevant des établissements d'enseignement supérieur, de la taxe sur les locaux à usage de bureaux, méconnaissent les principes d'égalité devant la loi et d'égalité devant les charges publiques qui résultent des articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen de 1789.  <br/>
<br/>
              4. Aux termes de l'article 6 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789 : " La loi (...) doit être la même pour tous, soit qu'elle protège, soit qu'elle punisse ". Le principe d'égalité ne s'oppose ni à ce que le législateur règle de façon différente des situations différentes, ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général, pourvu que, dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la loi qui l'établit. Aux termes de l'article 13 de cette Déclaration : " Pour l'entretien de la force publique, et pour les dépenses d'administration, une contribution commune est indispensable : elle doit être également répartie entre tous les citoyens, en raison de leurs facultés ". En vertu de l'article 34 de la Constitution, il appartient au législateur de déterminer, dans le respect des principes constitutionnels et compte tenu des caractéristiques de chaque impôt, les règles selon lesquelles doivent être appréciées les facultés contributives. En particulier, pour assurer le respect du principe d'égalité, il doit fonder son appréciation sur des critères objectifs et rationnels en fonction des buts qu'il se propose. Cette appréciation ne doit cependant pas entraîner de rupture caractérisée de l'égalité devant les charges publiques.<br/>
<br/>
              5. En premier lieu, eu égard au public qu'ils accueillent, soumis au principe de l'instruction obligatoire s'agissant des établissements du premier et du second degrés et qui échappe à celui-ci pour les établissements d'enseignement supérieur, et à leurs modalités distinctes de financement, caractérisées par la possibilité, pour les seuls établissement d'enseignement supérieur, de disposer de ressources propres et de percevoir des droits d'inscription, ces derniers établissements se trouvent dans une situation différente des établissements d'enseignement des premier et second degrés. Il était par conséquent loisible au législateur, eu égard aux objectifs qu'il poursuivait en instituant l'exonération de taxe sur les locaux à usage de bureaux et aux spécificités des établissements d'enseignement scolaire, de réserver à ces derniers le bénéfice de l'exonération de ces impositions. Par suite, ne peut être regardé comme revêtant un caractère sérieux le grief tiré de que les dispositions en litige, en ne prévoyant pas d'étendre aux établissements d'enseignement supérieur le bénéfice de l'exonération qu'elles prévoient, méconnaitraient le principe d'égalité devant la loi garanti par l'article 6 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789.<br/>
<br/>
              6. En second lieu, il résulte des dispositions du VI de l'article 231 ter précité que les tarifs applicables au titre des années en litige pour les locaux à usages de bureaux, étaient, respectivement, de 8,46 euros par mètre carré en 2015, 8,37 euros en 2016 et 8,56 euros en 2017 et que les tarifs applicables pour ces mêmes années pour les surfaces de stationnement annexées à ces locaux étaient, respectivement, de 2,27 euros par mètre carré, 2,25 euros et 2,30 euros. En soumettant à cette taxe les locaux administratifs et les surfaces de stationnement des établissements d'enseignement supérieur tout en exonérant de cette même taxe les établissements d'enseignement des premier et second degrés, le législateur a établi les impositions contestées selon des modalités qui reposent sur des critères objectifs et rationnels en rapport avec l'objet de la loi, eu égard aux différences mentionnées au point 5 ci-dessus, et dans le respect des capacités contributives des établissements concernés. Par suite, ne peut davantage être regardé comme sérieux le grief tiré de ce que les dispositions en litige méconnaîtraient le principe d'égalité devant les charges publiques garanti par l'article 13 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789.<br/>
<br/>
              7. Il résulte de tout ce qui précède que la question prioritaire de constitutionnalité soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Par suite, il n'y a pas lieu de la renvoyer au Conseil constitutionnel.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par l'École nationale supérieure d'architecture de Paris-Val de Seine.<br/>
Article 2 : La présente décision sera notifiée à l'École nationale supérieure d'architecture de Paris-Val de Seine, au ministre de l'économie, des finances et de la relance et au Premier ministre.<br/>
Copie en sera adressée au Conseil constitutionnel et au tribunal administratif de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
