<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043672609</ID>
<ANCIEN_ID>JG_L_2021_06_000000437273</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/67/26/CETATEXT000043672609.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 15/06/2021, 437273, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437273</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Edouard Solier</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHS:2021:437273.20210615</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 437273, par une requête et deux mémoires en réplique, enregistrés les 31 décembre 2019, 20 août 2020 et 29 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la délibération du 21 novembre 2019 par laquelle le conseil d'administration restreint de l'Institut national des sciences appliquées (INSA) de Lyon a annulé sa délibération du 5 juin 2018 portant sur l'avis du comité de sélection relatif au concours de recrutement pour le poste de professeur des universités " PR-4224 " et déclaré ce concours infructueux ; <br/>
<br/>
              2°) d'enjoindre au directeur de l'INSA de reprendre la procédure de nomination au stade de la communication au ministre chargé de l'enseignement supérieur en application de l'avant-dernier alinéa de l'article 9-2 du décret n° 84-431 du 6 juin 1984 en vue de sa nomination dans le corps des professeurs dans un délai de quinze jours à compter de la décision à intervenir, sous astreinte de 200 euros par jour de retard ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat et de l'INSA de Lyon la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 440742, par une demande et un mémoire en réplique, enregistrés les 17 mars et 20 août 2020 au secrétariat de la section du rapport et des études du Conseil d'Etat, M. B... demande au Conseil d'Etat d'assurer l'exécution de la décision n° 422962 du 18 septembre 2019 par laquelle le Conseil d'Etat, statuant au contentieux, a annulé la décision du directeur de l'INSA de Lyon du 2 juillet 2018 et enjoint au directeur de l'INSA de Lyon de reprendre la procédure de recrutement sur le poste de professeur des universités " PR-4224 "  au stade de la communication prévue à l'article 9-2 du décret du 6 juin 1984 dans un délai d'un mois. <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ;<br/>
              - le code des relations entre le public et l'administration ; <br/>
              - le décret n° 84-431 du 6 juin 1984 ;<br/>
              - le code de justice administrative;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Edouard Solier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rousseau, Tapie, avocat de M. B... ;<br/>
<br/>
<br/>
              Vu les notes en délibéré, enregistrées les 20 mai et 27 mai 2021, présentées par M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier que M. B..., maître de conférences en informatique, a présenté sa candidature à un concours de recrutement ouvert par l'Institut national des sciences appliquées (INSA) de Lyon pour un emploi de professeur des universités " PR-4224 " au sein de son département d'informatique. Par une délibération du 24 mai 2018, le comité de sélection institué pour ce recrutement a établi une liste de deux candidats, sur laquelle M. B... figurait en première position. Lors de sa séance du 31 mai 2018, le conseil d'administration de l'INSA de Lyon, siégeant en formation restreinte, a émis un avis favorable sur cette liste. Toutefois, par une décision du 2 juillet 2018, le directeur de l'INSA de Lyon a interrompu ce concours et l'a déclaré infructueux, au motif que la délibération du comité de sélection lui apparaissait irrégulière. Par une décision n° 422962 du 18 septembre 2019, le Conseil d'Etat statuant au contentieux a annulé la décision du 2 juillet 2018 et a enjoint à l'INSA de reprendre la procédure au stade de la communication prévue à l'article 9-2 du décret du 6 juin 1984 fixant les dispositions statutaires communes applicables aux enseignants-chercheurs et portant statut particulier du corps des professeurs des universités et du corps des maîtres de conférences  dans le délai d'un mois à compter de la notification de sa décision. Par courrier du 15 octobre 2019, l'administratrice provisoire de l'INSA de Lyon a demandé au conseil d'administration restreint de délibérer à nouveau sur l'avis motivé du comité de sélection. Par une délibération du 21 novembre 2019, le conseil d'administration restreint a annulé sa délibération du 31 mai 2018 et déclaré le concours infructueux. M. B... demande au Conseil d'Etat, d'une part, d'annuler pour excès de pouvoir la délibération du 21 novembre 2019, d'autre part, d'enjoindre à l'INSA de Lyon de prendre les mesures qu'implique l'exécution de la décision n° 422962 du 18 septembre 2019 du Conseil d'Etat. Ces requêtes présentent à juger des questions semblables. Il y a lieu de les joindre pour statuer par une seule décision. <br/>
<br/>
              Sur les conclusions aux fins d'annulation :<br/>
<br/>
              2. En premier lieu, aux termes de l'article L. 952-6-1 du code de l'éducation : " (...) lorsqu'un emploi d'enseignant-chercheur est créé ou déclaré vacant au sein de l'établissement, les candidatures (...) sont soumises à l'examen d'un comité de sélection créé par délibération du (...) conseil d'administration, siégeant en formation restreinte aux représentants élus des enseignants-chercheurs, des chercheurs et des personnels assimilés. / (...) Au vu de son avis motivé, le conseil d'administration, siégeant en formation restreinte (...), transmet au ministre compétent le nom du candidat dont il propose la nomination ou une liste de candidats classés par ordre de préférence (...) ".<br/>
<br/>
              Et aux termes de l'article 9-2 du décret du 6 juin 1984 : " (...) Sauf dans le cas où le conseil d'administration émet un avis défavorable motivé, le président ou directeur de l'établissement communique au ministre chargé de l'enseignement supérieur le nom du candidat sélectionné ou, le cas échéant, une liste de candidats classés par ordre de préférence. (...). " <br/>
<br/>
              3. D'une part, il résulte des dispositions de l'article 9-2 du décret du 6 juin 1984 citées au point 2 que la décision du conseil d'administration, eu égard à la nature et aux attributions de cet organisme dans le cadre de la procédure de recrutement des enseignants-chercheurs, doit être motivée lorsqu'il ne reprend pas les propositions du comité de sélection. Il ressort en l'espèce des termes mêmes de la délibération attaquée que pour retirer sa délibération du 31 mai 2018 par laquelle il avait émis un avis favorable sur la liste de candidats établie par le comité de sélection, et déclarer le concours infructueux, le conseil d'administration restreint a estimé que de graves irrégularités remettaient en cause l'impartialité du comité de sélection et l'égalité de traitement des candidats. Par suite, M. B... n'est pas fondé à soutenir que la délibération attaquée serait insuffisamment motivée. <br/>
<br/>
              4. D'autre part, il ressort des pièces du dossier qu'avant la réunion du comité de sélection, l'ensemble des candidats ont été reçus par le professeur qui avait été initialement pressenti pour présider le comité, et qui a dû être remplacé pour des motifs tenant à son état de santé. Bien que n'étant pas membre du comité de sélection, ce professeur a adressé à l'un des membres du jury un courriel faisant part, de manière circonstanciée, de son avis sur l'ensemble des candidats, et comportant un classement des candidatures. Une telle immixtion dans le processus d'examen des candidatures par le comité de sélection est susceptible d'avoir exercé une influence sur la délibération du conseil d'administration restreint, alors que l'objet de la procédure organisée pour le recrutement des enseignants-chercheurs est de pourvoir ces emplois, conformément à l'intérêt du service, dans le respect du principe d'impartialité du jury et du principe d'égalité entre les candidats. La procédure suivie en l'espèce a ainsi été entachée d'irrégularité. Par suite, M. B... n'est pas fondé à soutenir que la délibération attaquée procèderait d'une inexacte appréciation de la régularité de la procédure. <br/>
<br/>
              5. En deuxième lieu, ainsi que l'a jugé le Conseil d'Etat statuant au contentieux dans sa décision n° 422962 du 18 septembre 2019, il appartenait à l'INSA de Lyon de reprendre la procédure de recrutement pour l'emploi de professeur des universités " PR-4224 " au stade de la communication prévue à l'article 9-2 du décret du 6 juin 1984 dans un délai d'un mois à compter de la notification de cette décision. En application de ces dispositions, citées au point 2, il est loisible au président de l'université, ou pour les instituts et écoles ne faisant pas partie des universités au directeur de l'établissement, lorsqu'il estime que la procédure de recrutement d'un enseignant-chercheur est irrégulière, soit de demander au conseil d'administration de délibérer à nouveau sur l'avis motivé du comité de sélection, soit de faire part de ses observations sur la procédure au ministre chargé de l'enseignement supérieur à l'occasion de la transmission du nom du candidat ou de la liste arrêtée par le comité de sélection. <br/>
<br/>
              6. Il résulte de ce qui est dit aux points 4 et 5 qu'en délibérant à nouveau, à la demande de l'administratrice provisoire de l'INSA de Lyon, sur l'avis motivé émis par le comité de sélection le 24 mai 2018, le conseil d'administration restreint n'a nullement méconnu l'autorité de la chose jugée par le Conseil d'Etat dans sa décision du 18 septembre 2019. <br/>
<br/>
              7. En troisième lieu, aux termes de l'article L. 242-1 du code des relations entre le public et l'administration : " L'administration ne peut abroger ou retirer une décision créatrice de droits de sa propre initiative ou sur la demande d'un tiers que si elle est illégale et si l'abrogation ou le retrait intervient dans le délai de quatre mois suivant la prise de cette décision. ". D'une part, la délibération attaquée n'a eu ni pour objet, ni pour effet de retirer la délibération du comité de sélection du 24 mai 2018. D'autre part, si la délibération par laquelle un conseil d'administration émet un avis favorable sur la liste des candidats proposée par le comité de sélection est un acte créateur de droits, et ne peut dès lors en principe être abrogée ou retirée au-delà du délai de quatre mois suivant son intervention, la délibération attaquée a été prise pour l'exécution de la décision du Conseil d'Etat statuant au contentieux du 18 septembre 2019. Dès lors, le moyen tiré de ce que cette délibération méconnaîtrait les dispositions de l'article L. 242-1 du code des relations entre le public et l'administration ne peut qu'être écarté. <br/>
<br/>
              8. En dernier lieu, il résulte des points 4 à 6 que la délibération attaquée a été prise en raison de l'irrégularité entachant la procédure de recrutement sur le poste " PR-4224 ". Il s'ensuit que le moyen tiré de ce qu'elle serait entachée d'un détournement de pouvoir ne peut qu'être écarté. <br/>
<br/>
              Sur la demande d'exécution : <br/>
<br/>
              9. Aux termes de l'article L. 911-5 du code de justice administrative : " En cas d'inexécution d'une décision rendue par une juridiction administrative, le Conseil d'Etat peut, même d'office, prononcer une astreinte contre les personnes morales de droit public ou les organismes de droit privé chargés de la gestion d'un service public pour assurer l'exécution de cette décision ". Aux termes de l'article R. 931-2 du même code : " Les parties intéressées peuvent demander au Conseil d'Etat de prescrire les mesures nécessaires à l'exécution d'une de ses décisions ou d'une décision d'une juridiction administrative spéciale, en assortissant le cas échéant ces prescriptions d'une astreinte (...) ". <br/>
<br/>
              10. En reprenant les opérations de recrutement ainsi qu'il est dit au point 6, l'INSA de Lyon a complètement exécuté la décision n° 422962 du 18 septembre 2019 qui, contrairement à ce que soutient M. B..., n'impliquait pas nécessairement la transmission au ministre chargé de l'enseignement supérieur de la liste de candidats arrêtée par le comité de sélection. Dès lors, la demande de M. B... tendant à ce que le Conseil d'Etat enjoigne sous astreinte à l'INSA de Lyon d'exécuter sa décision du 18 septembre 2019 est devenue sans objet.<br/>
<br/>
              11. Il résulte de tout ce qui précède que les requêtes de M. B... doivent être rejetées, y compris ses conclusions aux fins d'injonction et celles qu'il présente au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête n° 437273 de M. B... est rejetée.  <br/>
Article 2 : Il n'y a pas lieu de statuer sur la demande n° 440742 de M. B.... <br/>
Article 3 : La présente décision sera notifiée à M. A... B... et à l'Institut national des sciences appliquées (INSA) de Lyon.<br/>
Copie en sera adressée à la ministre de l'enseignement supérieur, de la recherche et de l'innovation et à la section du rapport et des études du Conseil d'Etat.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
