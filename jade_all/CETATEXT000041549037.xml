<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041549037</ID>
<ANCIEN_ID>JG_L_2020_02_000000435854</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/54/90/CETATEXT000041549037.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 05/02/2020, 435854, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-02-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>435854</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Guillaume de LA TAILLE LOLAINVILLE</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:435854.20200205</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 8 novembre 2019 au secrétariat du contentieux du Conseil d'Etat, la société par actions simplifiée (SAS) Gaia Services et Prospective demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le paragraphe n° 100 des commentaires administratifs publiés au Bulletin officiel des finances publiques (BOFiP) - Impôts sous la référence BOI-BIC-RICI-10-160-40 dans ses rédactions successives en vigueur depuis le 8 juillet 2015 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - le code général des impôts ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume de La Taille Lolainville, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. La société Gaia Services et Prospective demande l'annulation pour excès de pouvoir du paragraphe n° 100 des commentaires administratifs publiés au Bulletin officiel des finances publiques (BOFiP) - Impôts, dans ses versions successives depuis le 8 juillet 2015, sous la référence BOI-BIC-RICI-10-160-40, qui a pour objet de commenter les dispositions du 3 du VIII de l'article 244 quater W du code général des impôts. <br/>
<br/>
              2. En vertu de l'article 244 quater W du code général des impôts, les entreprises imposées d'après leur bénéfice réel ou exonérées sous certaines conditions, exerçant une activité agricole ou une activité industrielle, commerciale ou artisanale relevant de l'article 34 de ce code, peuvent bénéficier d'un crédit d'impôt à raison des investissements productifs neufs qu'elles réalisent dans un département d'outre-mer pour l'exercice d'une activité relevant d'un secteur éligible. Le 3 du VIII de cet article dispose : " Le crédit d'impôt prévu au présent article est subordonné au respect par les entreprises exploitantes et par les organismes mentionnés au 4 du I de leurs obligations fiscales et sociales et de l'obligation de dépôt de leurs comptes annuels selon les modalités prévues aux articles L. 232-21 à L. 232-23 du code de commerce à la date de réalisation de l'investissement. (...) "<br/>
<br/>
              3. Pour justifier de son intérêt à demander l'annulation des commentaires administratifs relatifs à cette disposition, la requérante se borne à faire état de ce que l'administration lui a refusé, sur son fondement, le bénéfice du crédit d'impôt prévu à l'article 244 quater W du code général des impôts au titre de l'année 2016. L'intérêt dont elle se prévaut ainsi ne lui donne cependant qualité pour demander l'annulation des commentaires administratifs qu'elle conteste que dans leur rédaction applicable au 31 décembre 2016, laquelle a été publiée du 1er juin 2016 au 5 juillet 2017. Il en résulte que sa requête n'est recevable que dans cette mesure.<br/>
<br/>
              4. Eu égard, en outre, aux moyens qu'elle soulève, cette requête doit être regardée comme étant dirigée contre le paragraphe contesté des commentaires administratifs en litige seulement en tant que, réitérant la règle législative mentionnée au point 2, il énonce que l'octroi du crédit d'impôt prévu au I de l'article 244 quater W du code général des impôts " est subordonné au respect par les entreprises exploitantes et organismes mentionnés au 1 du I de l'article 244 quater X du CGI de l'obligation de dépôt de leurs comptes annuels selon les modalités prévues à l'article L. 232-21 du code de commerce, à l'article L. 232-22 du code de commerce et à l'article L. 232-23 du code de commerce, à la date de réalisation de l'investissement ". <br/>
<br/>
              5. Pour demander l'annulation de ces commentaires, la société Gaia Services et Prospective soutient que les dispositions législatives qu'ils réitèrent portent elles-mêmes atteinte aux droits et libertés garantis par les articles 6, 4, 8 et 13 de la Déclaration des droits de l'homme et des citoyens de 1789, l'alinéa 16 du Préambule de la Constitution du 27 octobre 1946 et l'article 73 de la Constitution. <br/>
<br/>
              6. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              7. En premier lieu, aux termes de l'article 6 de la Déclaration des droits de l'homme et du citoyen de 1789 : " La loi... doit être la même pour tous, soit qu'elle protège, soit qu'elle punisse ". Le principe d'égalité ne s'oppose ni à ce que le législateur règle de façon différente des situations différentes, ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général, pourvu que, dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la loi qui l'établit. Aux termes de l'article 13 de cette Déclaration : " Pour l'entretien de la force publique, et pour les dépenses d'administration, une contribution commune est indispensable : elle doit être également répartie entre tous les citoyens, en raison de leurs facultés ". Cette exigence ne serait pas respectée si l'impôt revêtait un caractère confiscatoire ou faisait peser sur une catégorie de contribuables une charge excessive au regard de leurs facultés contributives. En vertu de l'article 34 de la Constitution, il appartient au législateur de déterminer, dans le respect des principes constitutionnels et compte tenu des caractéristiques de chaque impôt, les règles selon lesquelles doivent être appréciées les facultés contributives. En particulier, pour assurer le respect du principe d'égalité, il doit fonder son appréciation sur des critères objectifs et rationnels en fonction des buts qu'il se propose. Cette appréciation ne doit pas entraîner de rupture caractérisée de l'égalité devant les charges publiques.<br/>
<br/>
              8. En subordonnant l'octroi de la réduction d'impôt prévue à l'article 244 quater W du code général des impôts au respect, par les entreprises réalisant l'investissement et, le cas échéant, les organismes mentionnés au 1 du I de l'article 244 quater X, de l'obligation de dépôt de leurs comptes annuels selon les modalités prévues aux articles L. 232-21 à L. 232-23 du code de commerce à la date de réalisation de l'investissement, le législateur a entendu imposer le respect de cette obligation aux seuls entreprises et organismes entrant dans le champ d'application de ces dispositions du code de commerce. Le législateur, qui s'est fixé comme objectif de renforcer la transparence des opérations de défiscalisation, s'est ainsi fondé sur un critère objectif et rationnel au regard du but poursuivi. S'il en résulte une différence de traitement entre les entreprises et organismes réalisant l'investissement selon qu'ils sont ou non soumis à l'obligation de dépôt des comptes annuels, celle-ci, qui résulte du champ d'application des dispositions du code de commerce, est justifiée par une différence de situation en rapport direct avec l'objet de la loi. Par suite, ne présentent pas un caractère sérieux les griefs tirés de ce que les dispositions contestées subordonneraient le bénéfice de la réduction d'impôt à une obligation de dépôt de comptes annuels dans des conditions méconnaissant les principes d'égalité devant la loi et d'égalité devant les charges publiques. <br/>
<br/>
              9. En deuxième lieu, les dispositions du 3 du VIII de l'article 244 quater W du code général des impôts n'ont pour objet ni l'accès à une profession ou à une activité économique, ni l'exercice d'une telle profession ou activité. Par suite, le grief tiré de ce qu'elles méconnaîtraient la liberté d'entreprendre ne présente pas un caractère sérieux. <br/>
<br/>
              10. En troisième lieu, ces dispositions, qui ne poursuivent aucune visée répressive, n'instituent ni une incrimination, ni une peine, ni une sanction. Dès lors, le grief tiré de ce qu'elles méconnaîtraient les principes issus de l'article 8 de la Déclaration de 1789 ne peut qu'être regardé comme dépourvu de sérieux.<br/>
<br/>
              11. Enfin, si la société Gaia Services et Prospective soutient que le 3 du VIII de l'article 244 quater W du code général des impôts porte atteinte au principe, énoncé au seizième alinéa du Préambule de la Constitution de 1946, selon lequel " La France forme avec les peuples d'outre-mer une Union fondée sur l'égalité des droits et des devoirs, sans distinction de race ni de religion ", et au principe, énoncé à l'article 73 de la Constitution, selon lequel " Dans les départements et les régions d'outre-mer, les lois et règlements sont applicables de plein droit ", ces principes ne sont pas au nombre, au sens et pour l'application de l'article 23-1 de l'ordonnance portant loi organique du 7 novembre 1958, des droits et libertés garantis par la Constitution.<br/>
<br/>
              12. La question soulevée, qui n'est pas nouvelle, ne présente ainsi pas un caractère sérieux. Il n'y a donc pas lieu de la renvoyer au Conseil constitutionnel. <br/>
<br/>
              13. Il en résulte que la société Gaia Services et Prospective n'est pas fondée à demander l'annulation des commentaires administratifs qu'elle attaque.<br/>
<br/>
              14. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présence instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société Gaia Services et Prospective.<br/>
Article 2 : La requête de la société Gaia Services et Prospective est rejetée.<br/>
Article 3 : La présente décision sera notifiée à la société par actions simplifiée Gaia Services et Prospective et au ministre de l'action et des comptes publics<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
