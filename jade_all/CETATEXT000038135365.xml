<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038135365</ID>
<ANCIEN_ID>JG_L_2019_02_000000404556</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/13/53/CETATEXT000038135365.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 13/02/2019, 404556</TITRE>
<DATE_DEC>2019-02-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404556</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Sara-Lou Gerber</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:404556.20190213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. CU... -CZ...Y..., Mme S...Y..., M. AB... AY..., M. A... G..., Mme BU...G..., M. CA... AZ..., Mme AV...CY..., M. BV... CE..., M. L... Z..., M. AE... CN..., M. E... CF..., M. C... BB..., M. CG... BC..., Mme CP...BD..., Mme AV...H..., M. CT... B..., M. CU... -CP...J..., Mme BE...BH..., Mme CX... BI..., M. L... CQ..., Mme AH...CQ..., M. AN... CQ..., M. CH... BJ..., M. BT... BK..., M. CB... AF..., M. CM... BL..., M. AI... BM..., M. CR... K..., M. AG... M..., M. AN... N..., M. AN... AJ..., M. CU... -F...BN..., M. BS... BO..., M. F... AK..., Mme CK...BP..., M. V... BQ..., M. CM... AL..., Mme CI...D..., M. AB... BR..., M. I... AM..., Mme O...CS..., M. CO... AO..., M. AN... AP..., M. BV... P..., M. CD... CL..., M. AA... AR..., M. AC... BX..., M. X... AS..., M. BG... BY..., M. BW... BZ..., Mme BU...AT..., M. CU... -DA...AU..., M. AQ... R..., M. Q... T..., M. CU... -CP...U..., M. CC... AW..., M. BA... AX..., M. AD... W..., et Mme CJ...CV...ont demandé au tribunal administratif de Rennes d'annuler pour excès de pouvoir la décision du 19 novembre 2015 par laquelle le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi de Bretagne a homologué le document unilatéral fixant le contenu du plan de sauvegarde de l'emploi de la société Hardy Roux développement (HRD). Par un jugement n° 1600197 du 1er avril 2016, le tribunal administratif a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 16NT01796 du 31 août 2016, la cour administrative d'appel de Nantes a rejeté l'appel formé par M. Y...et autres contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 octobre et 28 novembre 2016 au secrétariat du contentieux du Conseil d'Etat, M. Y... et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat et de MeBF..., en qualité de liquidateur judiciaire de la société Hardy Roux développement, la somme de 250 euros à verser à chacun des requérants au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de commerce ;<br/>
              - le code du travail ;<br/>
              - la loi n° 2015-990 du 6 août 2015, notamment son article 295 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sara-Lou Gerber, auditeur,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP  Didier, Pinet, avocat de M. Y...et autres, et à la SCP Piwnica, Molinié, avocat de Me BF... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un jugement du 27 juillet 2015, le tribunal de commerce de Rennes a placé la société Hardy Roux développement (HRD) en liquidation judiciaire avec poursuite d'activité jusqu'au 30 septembre 2015. Par un jugement du 16 septembre 2015, le même tribunal a prorogé cette poursuite d'activité jusqu'au 31 octobre 2015. A la demande de MeBF..., liquidateur judiciaire de la société, le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi de Bretagne a, par une décision du 19 novembre 2015, homologué le document unilatéral fixant le plan de sauvegarde de l'emploi de cette société. Par un jugement du 1er avril 2016, le tribunal administratif de Rennes a rejeté la demande de M. Y... et d'autres salariés de l'entreprise tendant à l'annulation de cette décision. M. Y... et les mêmes salariés se pourvoient en cassation contre l'arrêt du 31 août 2016 par lequel la cour administrative d'appel de Nantes a rejeté leur appel dirigé contre ce jugement.<br/>
<br/>
              Sur l'arrêt en tant qu'il statue sur le calendrier choisi par l'employeur :<br/>
<br/>
              2. L'article L. 641-10 du code de commerce dispose que : " Si la cession totale ou partielle de l'entreprise est envisageable ou si l'intérêt public ou celui des créanciers l'exige, le maintien de l'activité peut être autorisé par le tribunal pour une durée maximale fixée par décret en Conseil d'Etat. Elle peut être prolongée à la demande du ministère public pour une durée fixée par la même voie. (...) Le liquidateur administre l'entreprise. Dans les conditions prévues à l'article L. 631-17, il peut procéder aux licenciements ". Il résulte de ces dispositions que, lorsque le tribunal de commerce décide de la poursuite provisoire de l'activité d'une entreprise dans le cadre d'une procédure de liquidation judiciaire, le liquidateur judiciaire ne peut procéder à des licenciements pour motif économique qu'après autorisation du juge-commissaire désigné par le tribunal de commerce. En revanche, l'article L. 641-4 du code de commerce prévoit que " (...) Les licenciements auxquels procède le liquidateur en application de la décision ouvrant ou prononçant la liquidation, le cas échéant au terme du maintien provisoire de l'activité autorisé par le tribunal, sont soumis aux dispositions de l'article L. 1233-58 du code du travail (...) ". Il résulte de ces dernières dispositions que, lorsque les licenciements sont prononcés après la fin de la période de maintien d'activité, l'autorisation du juge-commissaire n'est pas requise. <br/>
<br/>
              3. En jugeant que le liquidateur de la société HDR avait pu, sans méconnaître les dispositions des articles L. 641-10 et L. 631-17 du code de commerce mentionnés ci-dessus, ne faire débuter la procédure d'information et de consultation des représentants du personnel sur le plan de sauvegarde de l'emploi qu'après la fin de la période de maintien de l'activité, sans que puisse être utilement invoquée la circonstance que, si les licenciements prévus par le plan litigieux avaient été opérés plus tôt, soit entre la date de limite de dépôt des offres de reprises et la fin de la période de maintien de l'activité, ils auraient été soumis, pour les motifs indiqués au point 2, à l'autorisation du juge-commissaire, la cour administrative d'appel, qui a suffisamment motivé son arrêt et n'a pas dénaturé les pièces du dossier, n'a pas commis d'erreur de droit. <br/>
<br/>
              Sur l'arrêt en tant qu'il statue sur la procédure d'information et de consultation :<br/>
<br/>
              4. En premier lieu, l'article L. 641-1 du code de commerce, dans sa rédaction applicable à l'espèce, dispose que : " (...) Lorsque la procédure est ouverte à l'égard d'un débiteur dont le nombre de salariés est au moins égal à un seuil fixé par décret en Conseil d'Etat, le tribunal sollicite les observations des institutions mentionnées à l'article L. 3253-14 du code de travail sur la désignation du liquidateur. / Un représentant des salariés est désigné dans les conditions prévues au deuxième alinéa de l'article L. 621-4 et à l'article L. 621-6. Il exerce la mission prévue à l'article L. 625-2. En l'absence de comité d'entreprise et de délégués du personnel, le représentant des salariés exerce les fonctions dévolues à ces institutions par les dispositions du présent titre (...) ". Ces dispositions fixent les conditions dans lesquelles le tribunal de commerce doit rendre le jugement arrêtant la liquidation. Par suite, la cour administrative d'appel de Nantes a pu, sans erreur de droit, juger que leur méconnaissance ne peut être utilement invoquée au soutien d'un recours dirigé contre une décision d'homologation d'un plan de sauvegarde de l'emploi.<br/>
<br/>
              5. En deuxième lieu, l'article L. 1233-57-3 du code du travail, dans sa rédaction applicable à l'espèce, dispose que : " (...) l'autorité administrative homologue le document élaboré par l'employeur mentionné à l'article L. 1233-24-4, après avoir vérifié (...) la régularité de la procédure d'information et de consultation du comité d'entreprise et, le cas échéant, du comité d'hygiène, de sécurité et des conditions de travail  (...) ". L'article L. 4612-8 du code du travail, applicable à l'espèce, dispose que : " Le comité d'hygiène, de sécurité et des conditions de travail est consulté avant toute décision d'aménagement important modifiant les conditions de santé et de sécurité ou les conditions de travail et, notamment, avant toute transformation importante des postes de travail découlant de la modification de l'outillage, d'un changement de produit ou de l'organisation du travail, avant toute modification des cadences et des normes de productivité liées ou non à la rémunération du travail ". Il résulte de l'ensemble de ces dispositions que, lorsque l'autorité administrative est saisie d'une demande d'homologation d'un document unilatéral fixant le contenu d'un plan de sauvegarde de l'emploi pour une opération qui, parce qu'elle modifie de manière importante les conditions de santé et de sécurité ou les conditions de travail des salariés de l'entreprise, requiert la consultation du ou des comités d'hygiène, de sécurité et des conditions de travail concernés, elle ne peut légalement accorder l'homologation demandée que si cette consultation a été régulière.<br/>
<br/>
              6. Il ressort des mentions de l'arrêt attaqué que la cour a relevé qu'en l'absence d'offre de reprise de l'activité de la société HRD, l'ensemble des postes de travail de cette entreprise devait être supprimé. En outre, il n'était pas soutenu devant elle qu'avant l'achèvement de cette opération, les conditions de santé et de sécurité dans l'entreprise ou les conditions de travail des salariés ayant vocation à être licenciés étaient susceptibles d'être affectées par l'opération projetée. Dès lors, la cour n'a pas commis d'erreur de droit en jugeant que les requérants ne pouvaient utilement soutenir que la procédure avait été irrégulière, faute de consultation du comité d'hygiène, de sécurité et des conditions de travail.<br/>
<br/>
              7. En troisième lieu, en estimant que les annotations portées par le liquidateur sur les procès-verbaux des réunions du comité d'entreprise avaient été sans incidence sur l'appréciation portée par l'administration sur le respect de la procédure d'information et de consultation, la cour administrative d'appel a porté sur les pièces du dossier qui lui était soumis une appréciation souveraine, qui n'est pas entachée de dénaturation.<br/>
<br/>
              8. Enfin, il résulte des dispositions de l'article L. 2322-4 du code du travail que l'existence d'une unité économique et sociale peut être reconnue par convention ou par décision de justice. A défaut de convention, il n'appartient qu'au juge judiciaire de se prononcer sur l'existence d'une unité économique et sociale au sens de ces dispositions. Dès lors, après avoir souverainement relevé, sans dénaturer les pièces du dossier qui lui était soumis, qu'aucune unité économique et sociale n'avait été reconnue au sein de la société HRD, la cour n'a pas commis d'erreur de droit en jugeant que le plan de sauvegarde de l'emploi et la procédure de consultation prévue par l'article L. 1233-30 du code du travail n'avaient pas à être mis en oeuvre au niveau d'une unité économique et sociale.<br/>
<br/>
              Sur l'arrêt en tant qu'il statue sur le contenu du plan de sauvegarde de l'emploi :<br/>
<br/>
              9. Lorsqu'elle est saisie d'une demande d'homologation d'un document élaboré en application de l'article L. 1233-24-4 du code du travail, il appartient à l'administration de contrôler la légalité des dispositions du plan de sauvegarde de l'emploi fixé par ce document et notamment le respect par ce plan des articles L. 1233-61 à L. 1233-63 du même code. Dans les entreprises en liquidation judiciaire, elle doit, en application des dispositions de l'article L. 1233-58 du code du travail dans leur rédaction applicable aux procédures de licenciement pour motif économique engagées à compter du 8 août 2015, apprécier si les mesures contenues dans le plan sont précises et concrètes et si, à raison, pour chacune, de sa contribution aux objectifs de maintien dans l'emploi et de reclassement des salariés, elles sont, prises dans leur ensemble, propres à satisfaire à ces objectifs compte tenu des moyens dont dispose l'entreprise.<br/>
<br/>
              10. Dans ce cadre, il revient notamment à l'autorité administrative de s'assurer que le plan de reclassement intégré au plan de sauvegarde de l'emploi est de nature à faciliter le reclassement des salariés. A cette fin, l'employeur doit avoir identifié dans le plan l'ensemble des possibilités de reclassement des salariés dans l'entreprise. En outre, lorsque l'entreprise appartient à un groupe, l'employeur, seul débiteur de l'obligation de reclassement, doit avoir procédé à une recherche sérieuse des postes disponibles pour un reclassement dans les autres entreprises du groupe et, pour l'ensemble des postes de reclassement ainsi identifiés, l'employeur doit avoir indiqué dans le plan leur nombre, leur nature et leur localisation.<br/>
<br/>
              11. Pour examiner, ainsi qu'il lui incombait, les mesures de reclassement du plan dont l'homologation était attaquée, la cour administrative d'appel, qui a souverainement apprécié les pièces du dossier soumis au juge du fond sans les dénaturer, a pu, sans commettre d'erreur de droit, prendre en considération le sérieux des démarches effectuées par le mandataire social de la société HRD auprès des autres sociétés du groupe auquel elle appartenait pour estimer que l'absence de mention, dans le plan de sauvegarde de l'emploi, de postes de reclassement dans les autres entreprises de ce groupe, se justifiait. En outre, en estimant que, compte tenu des moyens très limités de la société HRD, placée en liquidation judiciaire, les mesures prévues par le plan de sauvegarde de l'emploi, qui comportaient des dispositifs spécifiques au bénéfice des salariés âgés de plus de 50 ans et des salariés handicapés, des mesures d'aide au déménagement, à la création ou à la reprise d'activité, des prises en charge de frais de formation et de déplacement et représentaient une somme d'environ 1 000 euros par salarié, pouvaient être, alors même qu'elles ne comportaient aucun poste de reclassement, légalement regardées par l'administration comme étant, prises dans leur ensemble, propres à satisfaire aux objectifs mentionnés par les articles L. 1233-61 à L. 1233-63 du code du travail, la cour a porté une appréciation souveraine sur les pièces du dossier qui lui était soumis exempte de dénaturation.<br/>
<br/>
              12. Enfin, les requérants ne peuvent utilement soutenir, pour la première fois en cassation, que le délai dont a disposé l'administration pour homologuer le plan de sauvegarde de l'emploi était insuffisant.<br/>
<br/>
              13. Il résulte de tout ce qui précède que les requérants ne sont pas fondés à demander l'annulation de l'arrêt qu'ils attaquent. Leurs conclusions doivent, par suite, être rejetées, y compris, par voie de conséquence, celles qu'ils présentent au titre de l'article L. 761-1 du code de justice administrative. Par ailleurs, il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. Y...et des autres requérants les sommes que demande Me BF...au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. Y... et autres est rejeté.<br/>
Article 2 : Les conclusions de MeBF..., liquidateur judiciaire de la société Hardy Roux, présentées au titre de l'article L. 761-1 du code de justice administrative, sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. CU... -CZ...Y..., premier dénommé, pour l'ensemble des requérants, à Me CW...BF..., liquidateur judiciaire de la société Hardy Roux développement et à la ministre du travail.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-07 TRAVAIL ET EMPLOI. LICENCIEMENTS. - HOMOLOGATION ADMINISTRATIVE D'UN PSE (ART. L. 1233-24-4 DU CODE DU TRAVAIL) - INFORMATION ET CONSULTATION PRÉALABLES DU CHSCT (ART. L. 1233-57-3 DE CE CODE, DANS SA RÉDACTION ANTÉRIEURE À L'ORDONNANCE DU 22 SEPTEMBRE 2017) - 1) CONTRÔLE DE L'ADMINISTRATION - RÉGULARITÉ DE LA CONSULTATION [RJ1] - 2) ESPÈCE - OPÉRATION SUPPRIMANT L'ENSEMBLE DES POSTES - ABSENCE DE MODIFICATION, AVANT L'ACHÈVEMENT DE L'OPÉRATION, DES CONDITIONS DE SANTÉ ET DE SÉCURITÉ OU DES CONDITIONS DE TRAVAIL DES SALARIÉS AYANT VOCATION À ÊTRE LICENCIÉS - CONSÉQUENCE - ABSENCE DE CONSULTATION N'ENTACHANT PAS D'IRRÉGULARITÉ LA PROCÉDURE.
</SCT>
<ANA ID="9A"> 66-07 1) Il résulte des articles L. 1233-57-3 et L. 4612-8 du code du travail dans leur rédaction antérieure à l'ordonnance n° 2017-1386 du 22 septembre 2017 que, lorsque l'autorité administrative est saisie d'une demande d'homologation d'un document unilatéral fixant le contenu d'un plan de sauvegarde de l'emploi (PSE) pour une opération qui, parce qu'elle modifie de manière importante les conditions de santé et de sécurité ou les conditions de travail des salariés de l'entreprise, requiert la consultation du ou des comités d'hygiène, de sécurité et des conditions de travail (CHSCT) concernés, elle ne peut légalement accorder l'homologation demandée que si cette consultation a été régulière.,,2) Cour ayant relevé qu'en l'absence d'offre de reprise de l'activité d'une société faisant l'objet d'un PSE, placée en liquidation judiciaire avec poursuite d'activité, l'ensemble des postes de travail de cette entreprise devait être supprimés. Cour devant laquelle il n'était pas soutenu, en outre,  qu'avant l'achèvement de cette opération, les conditions de santé et de sécurité dans l'entreprise ou les conditions de travail des salariés ayant vocation à être licenciés étaient susceptibles d'être affectées par l'opération projetée.... ...La cour n'a pas commis d'erreur de droit en jugeant que les requérants ne pouvaient utilement soutenir que la procédure avait été irrégulière, faute de consultation du CHSCT.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 21 octobre 2015, Syndicat CGT SKF Montigny et autres, n° 386123, T. pp. 793-896 ; CE, 29 juin 2016, Société Astérion France et ministre du travail, de l'emploi et du dialogue social, n° 386581 386844, p. 265.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
