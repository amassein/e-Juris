<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028195296</ID>
<ANCIEN_ID>JG_L_2013_11_000000368764</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/19/52/CETATEXT000028195296.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  2ème sous-section jugeant seule, 13/11/2013, 368764, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-11-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368764</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 2ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT</AVOCATS>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:368764.20131113</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et les mémoires complémentaires enregistrés au secrétariat du contentieux du Conseil d'Etat les 23 mai, 12 septembre et 17 octobre 2013 , présentés par Mme A...B..., demeurant ... ; Mme A...B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la délibération du jury de l'examen d'aptitude à la profession d'avocat au Conseil d'Etat et à la Cour de cassation en date du 20 décembre 2011 en tant que cette délibération ne l'a pas déclarée admise à cet examen au titre de la session de l'année 2011 ainsi que la décision implicite du président de ce jury ayant rejeté le recours gracieux formé contre cette délibération ;<br/>
<br/>
              2°) d'ordonner la production de ses copies d'examen ainsi que des notes qui lui ont été attribuées aux épreuves écrites et orales ; <br/>
<br/>
              3°) d'enjoindre, sur le fondement des articles L. 911-2 et suivants du code de justice administrative, à la garde des sceaux, ministre de la justice, ou à toute autre autorité compétente d'organiser, dans un délai de trois mois à compter de la notification de la décision à intervenir et sous astreinte de deux cents euros par jour de retard, une nouvelle épreuve d'admission portant sur la réglementation professionnelle et la gestion d'un office d'avocat au Conseil d'Etat et à la Cour de cassation ;<br/>
<br/>
              4°) de surseoir à statuer et de renvoyer à la Cour de justice de l'Union européenne une question préjudicielle portant sur la question de savoir si la liberté d'établissement s'oppose à une législation nationale réservant l'exclusivité de la représentation des justiciables devant les cours suprêmes de l'Etat considéré à des officiers ministériels titulaires de leur charge et disposant du droit de présentation de leur successeur ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu l'ordonnance du 10 septembre 1817 ;<br/>
<br/>
              Vu le décret n° 91-1125 du 28 octobre 1991 ;<br/>
<br/>
              Vu le décret n° 2010-164 du 22 février 2010 ;<br/>
<br/>
              Vu l'arrêté du 2 août 2000 fixant le programme et les modalités de l'examen d'aptitude à la profession d'avocat au Conseil d'Etat et à la Cour de cassation ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, Conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Xavier Domino, Rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de La Varde, Buk Lament, avocat de l'Ordre des avocats au Conseil d'Etat et à la Cour de cassation ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant, d'une part, que les conditions d'accès à la profession d'avocat au Conseil d'Etat et à la Cour de cassation sont fixées, en vertu de l'article 3 de l'ordonnance du 10 septembre 1817, par décret en Conseil d'Etat ; qu'aux termes de l'article 1er du décret du 28 octobre 1991 relatif aux conditions d'accès à la profession d'avocat au Conseil d'Etat et à la Cour de cassation : " Nul ne peut accéder à la profession d'avocat au Conseil d'Etat et à la Cour de cassation s'il ne remplit les conditions suivantes :  (...) 5° Avoir subi avec succès l'examen d'aptitude à la profession d'avocat au Conseil d'Etat et à la Cour de cassation prévu au titre III, sous réserve des dispenses prévues aux articles 2, 3 et 5 (...) " ; que, selon l'article 18 du même décret, l'examen d'aptitude à la profession d'avocat au Conseil d'Etat et à la Cour de cassation est subi devant un jury, composé d'un conseiller d'Etat, d'un conseiller à la Cour de cassation, d'un professeur d'université chargé d'un enseignement juridique et de trois avocats au Conseil d'Etat et à la Cour de cassation ; <br/>
<br/>
              Considérant, d'autre part, qu'aux termes de l'article R. 311-1 du code de justice administrative, applicable à la date d'introduction de la requête : " Le Conseil d'Etat est compétent pour connaître en premier et dernier ressort : / 1° Des recours dirigés contre les ordonnances du Président de la République et les décrets ; / 2° Des recours dirigés contre les actes réglementaires des ministres et des autres autorités à compétence nationale et contre leurs circulaires et instructions de portée générale ; / 3° Des litiges concernant le recrutement et la discipline des agents publics nommés par décret du Président de la République en vertu des dispositions de l'article 13 (3e alinéa) de la Constitution et des articles 1er et 2 de l'ordonnance n° 58-1136 du 28 novembre 1958 portant loi organique concernant les nominations aux emplois civils et militaires de l'Etat ; / 4° Des recours dirigés contre les décisions prises par les organes des autorités suivantes, au titre de leur mission de contrôle ou de régulation : / -l'Agence française de lutte contre le dopage ; / -L'Autorité de contrôle prudentiel ; / -l'Autorité de la concurrence ; / -l'Autorité des marchés financiers ; / -l'Autorité de régulation des communications électroniques et des postes ; / -l'Autorité de régulation des jeux en ligne ; / -l'Autorité de régulation des transports ferroviaires ; / -l'Autorité de sûreté nucléaire ; / -la Commission de régulation de l'énergie ; / -le Conseil supérieur de l'audiovisuel ; / -la Commission nationale de l'informatique et des libertés ; / -la Commission nationale de contrôle des interceptions de sécurité ; / -la commission nationale d'aménagement commercial ; / 5° Des actions en responsabilité dirigées contre l'Etat pour durée excessive de la procédure devant la juridiction administrative ; / 6° Des recours en interprétation et des recours en appréciation de légalité des actes dont le contentieux relève en premier et dernier ressort du Conseil d'Etat ; / 7° Des recours dirigés contre les décisions ministérielles prises en matière de contrôle des concentrations économiques " ; <br/>
<br/>
              Considérant que la requête de Mme B...tend à l'annulation de la délibération du jury de l'examen d'aptitude à la profession d'avocat au Conseil d'Etat et à la Cour de cassation en date du 20 décembre 2011 en tant que cette délibération ne l'a pas déclarée admise à l'examen du certificat d'aptitude à la profession d'avocat au Conseil d'Etat et à la Cour de cassation au titre de la session de l'année 2011, ainsi que de la décision implicite du président de ce jury rejetant le recours gracieux formé contre cette délibération ; <br/>
<br/>
              Considérant que si l'article R. 311-1 du code de justice administrative, dans sa rédaction antérieure au décret du 22 février 2010, attribuait compétence au Conseil d'Etat pour connaître en premier et dernier ressort des recours dirigés contre les décisions administratives des organismes collégiaux à compétence nationale, cette attribution de compétence a été supprimée par l'effet des dispositions de l'article 1er du décret du 22 février 2010, applicables, en vertu de son article 55, aux requêtes enregistrées à compter du 1er avril 2010 ; que ni les dispositions applicables en l'espèce de l'article R. 311-1 du code de justice administrative, ni les dispositions du décret du 28 octobre 1991, ni aucune autre disposition législative ou réglementaire ne donnent directement compétence au Conseil d'Etat pour connaître en premier ressort des décisions dont Mme B...demande l'annulation ; qu'il y a lieu, dès lors, en application de l'article R. 351-1 du code de justice administrative, d'en attribuer le jugement au tribunal administratif de Paris, dans le ressort duquel a son siège le jury qui a pris la délibération attaquée, lequel est compétent pour en connaître en vertu de l'article R. 312-1 du même code ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement de la requête de Mme B...est attribué au tribunal administratif de Paris.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme A...B...et au président du tribunal administratif de Paris.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
