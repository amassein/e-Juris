<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023690725</ID>
<ANCIEN_ID>JG_L_2011_03_000000328870</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/69/07/CETATEXT000023690725.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 04/03/2011, 328870</TITRE>
<DATE_DEC>2011-03-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>328870</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN</AVOCATS>
<RAPPORTEUR>M. Frédéric  Desportes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Thiellay Jean-Philippe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:328870.20110304</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 15 juin et 14 septembre 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. D...A...représenté par son représentant légal, Mme B...C...demeurant... ; M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n°  0601430 du 25 septembre 2008 par lequel le tribunal administratif de Lyon a rejeté sa demande tendant, d'une part, à l'annulation de la décision implicite par laquelle le ministre des transports, de l'équipement, du tourisme et de la mer a rejeté le recours hiérarchique qu'il a formé le 25 octobre 2005 contre la décision implicite des services de ce ministère rejetant sa demande du 24 juin 2005 aux fins de liquidation de sa pension de retraite, ensemble la décision implicite de ces services et, d'autre part, en tant que de besoin, à l'annulation de la décision du 6 février 2006 par laquelle le ministre des transports, de l'équipement, du tourisme et de la mer a rejeté sa demande aux fins de liquidation de sa pension de retraite ;<br/>
<br/>
              2°) réglant l'affaire au fond au fond, d'annuler les décisions administratives attaquées et condamner l'Etat à verser entre les mains de son représentant légal l'intégralité des arrérages de sa pension de retraite dus à compter de sa demande assortis des intérêts légaux capitalisés ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu le code civil ;<br/>
<br/>
              Vu le code des pensions civiles et militaires de retraite ;<br/>
<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Desportes, chargé des fonctions de Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Masse-Dessen, Thouvenin, avocat de M. A..., <br/>
<br/>
              - les conclusions de M. Jean-Philippe Thiellay, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Masse-Dessen, Thouvenin, avocat de M.A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par le ministre d'Etat, ministre de l'écologie, de l'énergie, du développement  durable et de la mer ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un jugement du 24 février 2003, le juge des tutelles du tribunal d'instance d'Annecy a, par application des articles 112 et 113 du code civil, constaté la présomption d'absence de M. A..., professeur technique de classe normale de l'enseignement maritime, qui n'était pas reparu à son domicile et n'avait plus donné de ses nouvelles depuis le 25 août 2002, et désigné sa fille, Mme C..., pour le représenter et administrer ses biens ; que, par lettre du 24 juin 2005, Mme C...a demandé pour le compte de son père la liquidation de la pension de retraite de celui-ci ; que sa demande étant demeurée sans réponse, elle a formé, par lettre du 24 octobre 2005, un recours hiérarchique auprès du ministre chargé des transports qui ne s'est pas davantage prononcé sur les droits de M. A...; que par lettre du 6 février 2006, les services du ministère ont informé Mme C...du rejet de sa demande ; que celle-ci, agissant en sa qualité de représentante de M.A..., a demandé au tribunal administratif de Lyon d'annuler, d'une part, les décisions implicites de rejet résultant du silence gardé pendant plus de deux mois sur sa demande du 24 juin 2005 et son recours hiérarchique du 24 octobre 2005 et, d'autre part, la décision de rejet du 6 février 2006 ; que Mme C... se pourvoit en cassation contre le jugement du 25 septembre 2008 par lequel le tribunal administratif a rejeté sa demande ;<br/>
<br/>
              Considérant qu'aux termes de l'article L. 1 du code des pensions civiles et militaires de retraite : " La pension est une allocation pécuniaire  personnelle (...) accordée aux fonctionnaires (...) et, après leur décès,  à leurs ayants cause désignés par la loi (...) " ; qu'aux termes du premier alinéa de l'article L. 57 du même code : " Lorsqu'un bénéficiaire du présent code, titulaire d'une pension (...) a disparu de son domicile et que plus d'un an s'est écoulé sans qu'il ait réclamé les arrérages de sa pension (...), son conjoint et les enfants âgés de moins de vingt et un ans qu'il a laissés peuvent obtenir, à titre provisoire, la liquidation des droits à la pension qui leur seraient ouverts en cas de décès " ; qu'en vertu du deuxième alinéa du même article, une pension peut être également attribuée, à titre provisoire, au conjoint et aux enfants de moins de vingt et un ans du fonctionnaire disparu depuis plus d'un an, lorsqu'au jour de sa disparition celui-ci justifiait d'au moins quinze années de services effectifs ; qu'en son troisième alinéa, l'article L. 57 dispose que : " La pension provisoire est supprimée lorsque le décès est officiellement établi ou que l'absence a été déclarée par jugement passé en force de chose jugée et une pension définitive est alors attribuée aux ayants cause " ; qu'il résulte de ces dispositions, qui dérogent  pour le droit à pension aux articles 112 et suivants du code civil, que la disparition, depuis plus d'un an, d'un fonctionnaire civil ou militaire a pour effet de suspendre ses droits propres à pension et d'ouvrir, le cas échéant, à ses ayants cause la possibilité de se voir reconnaître à titre provisoire le bénéfice des droits à pension qu'ils détiendraient s'il était décédé; que l'état de présomption d'absence de M. A...faisait, dès lors, obstacle au versement, entre les mains de MmeC..., administratrice de ses biens, de la pension à laquelle il aurait pu prétendre ; que ce motif, qui ne comporte  l'appréciation d'aucune circonstance de fait, doit être substitué au motif  retenu par le jugement attaqué du tribunal administratif de Lyon, dont il justifie légalement le dispositif ; qu'il suit de là que le moyen  tiré de ce que ce jugement violerait les dispositions des articles 112 et suivants du  code civil, dont il résulte que la personne présumée absente doit être présumée en vie, ne peut être qu'écarté ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que M.  A...n'est pas fondé à demander l'annulation du jugement du tribunal administratif de Lyon du 25 septembre 2008 ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de M. A...est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme B...C...en qualité de représentant légal de M. D...A..., au ministre de l'écologie, du développement durable, des transports et du logement et au ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-01-04 DROITS CIVILS ET INDIVIDUELS. ÉTAT DES PERSONNES. QUESTIONS DIVERSES RELATIVES À L`ÉTAT DES PERSONNES. - ABSENCE AU SENS DES ART. 112 ET SUIVANTS DU CODE CIVIL - DISPOSITIONS INAPPLICABLES AU DROIT À PENSION, RÉGI PAR LES DISPOSITIONS DÉROGATOIRES DE L'ARTICLE L. 57 DU CPCMR [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">48-02-01-09 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. QUESTIONS COMMUNES. AYANTS-CAUSE. - DISPARITION DU PENSIONNÉ (ART. L. 57 DU CPCMR)[RJ1] - DISPOSITIONS DÉROGEANT AUX ARTICLES 112 ET SUIVANTS DU CODE CIVIL RELATIFS À L'ABSENCE.
</SCT>
<ANA ID="9A"> 26-01-04 Il résulte des dispositions de l'article L. 57 du code des pensions civiles et militaires de retraite (CPCMR), qui dérogent, pour le droit à pension, aux articles 112 et suivants du code civil, que la disparition, depuis plus d'un an, d'un fonctionnaire civil ou militaire a pour effet de suspendre ses droits propres à pension et d'ouvrir, le cas échéant, à ses ayants cause la possibilité de se voir reconnaître à titre provisoire le bénéfice des droits à pension qu'ils détiendraient s'il était décédé.</ANA>
<ANA ID="9B"> 48-02-01-09 Il résulte des dispositions de l'article L. 57 du code des pensions civiles et militaires de retraite (CPCMR), qui dérogent, pour le droit à pension, aux articles 112 et suivants du code civil, que la disparition, depuis plus d'un an, d'un fonctionnaire civil ou militaire a pour effet de suspendre ses droits propres à pension et d'ouvrir, le cas échéant, à ses ayants cause la possibilité de se voir reconnaître à titre provisoire le bénéfice des droits à pension qu'ils détiendraient s'il était décédé.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 5 janvier 1994, Mme Beucher, n° 88953, p. 4.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
