<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033685136</ID>
<ANCIEN_ID>JG_L_2016_12_000000399723</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/68/51/CETATEXT000033685136.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 23/12/2016, 399723, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>399723</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Clément Malverti</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:399723.20161223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 11 mai et 14 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, la région Aquitaine-Limousin-Poitou-Charentes demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'avis n° 2016-023 de l'Autorité de régulation des activités ferroviaires et routières du 8 mars 2016 relatif au projet de décision de la Région Aquitaine-Limousin-Poitou-Charentes d'interdiction du service déclaré par la société FlixBus France sur la liaison entre Niort et Poitiers ;<br/>
<br/>
              2°) de mettre à la charge de l'Autorité de régulation des activités ferroviaires et routières la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier :<br/>
<br/>
              Vu : <br/>
              - le code des transports ;<br/>
              - le décret n° 85-891 du 16 août 1985 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Clément Malverti, auditeur,<br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes de l'article L. 3111-17 du code des transports, issu de la loi du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques : " Les entreprises de transport public routier de personnes établies sur le territoire national peuvent assurer des services réguliers interurbains " ; qu'aux termes de l'article L. 3111-18 de ce code : " Tout service assurant une liaison dont deux arrêts sont distants de 100 kilomètres ou moins fait l'objet d'une déclaration auprès de l'Autorité de régulation des activités ferroviaires et routières, préalablement à son ouverture. L'autorité publie sans délai cette déclaration./ Une autorité organisatrice de transport peut, après avis conforme de l'Autorité de régulation des activités ferroviaires et routières, dans les conditions définies à l'article L. 3111-19, interdire ou limiter les services mentionnés au premier alinéa du présent article lorsqu'ils sont exécutés entre des arrêts dont la liaison est assurée sans correspondance par un service régulier de transport qu'elle organise et qu'ils portent, seuls ou dans leur ensemble, une atteinte substantielle à l'équilibre économique de la ligne ou des lignes de service public de transport susceptibles d'être concurrencées ou à l'équilibre économique du contrat de service public de transport concerné " ;<br/>
<br/>
              2.	Considérant que la société FlixBus France a déposé auprès de l'Autorité de régulation des activités ferroviaires et routières, le 9 novembre 2015, une déclaration portant sur un service régulier interurbain de transport par autocar entre Niort et Poitiers visant à réaliser deux dessertes par jour depuis chacune de ces villes ; que la région Aquitaine-Limousin-Poitou-Charentes a saisi l'Autorité d'un projet d'interdiction de ce service ; que, par un avis n° 2016-023 du 8 mars 2016, l'Autorité a estimé que le service envisagé ne portait pas une atteinte substantielle à l'équilibre économique de la ligne Poitiers - La Rochelle que la région organise au titre du service public des transports express régionaux Poitou-Charentes et s'est dès lors prononcée défavorablement sur le projet de la région ; que la région Aquitaine-Limousin-Poitou-Charentes demande l'annulation pour excès de pouvoir de cet avis ;<br/>
<br/>
              3.	Considérant, en premier lieu, que l'autorité organisatrice de transport peut, selon l'article L. 3111-18 du code des transports, interdire les services déclarés lorsqu'ils " portent, seuls ou dans leur ensemble, une atteinte substantielle à l'équilibre économique de la ligne ou des lignes de service public de transport susceptibles d'être concurrencées ou à l'équilibre économique du contrat de service public de transport concerné " ; que, pour apprécier le caractère substantiel de cette atteinte, l'Autorité a comparé la perte de recettes commerciales induite par le report de clientèle du service de transport organisé par la région vers le service déclaré par la société FlixBus France avec, d'une part, les recettes commerciales du service de transport organisé par la région, d'autre part, le montant de la compensation versée par la région au titre de ce service ; qu'en procédant ainsi,  l'Autorité n'a entaché son avis d'aucune erreur de droit, dès lors que l'équilibre du service de transport organisé par la région dépend, pour une part importante, des subventions publiques dont il bénéficie ;<br/>
<br/>
              4.	Considérant, en deuxième lieu, qu'il résulte des dispositions de l'article L. 3111-18 du code des transports citées ci-dessus qu'un nouveau service assurant une liaison entre deux points distants de 100 kilomètres ou moins ne peut être ouvert avant d'avoir fait l'objet d'une déclaration auprès de l'Autorité de régulation des activités ferroviaires ; qu'aux termes de l'article 31-15 du décret du 16 août 1985 relatif aux transports urbains de personnes et aux transports routiers non urbains de personnes, modifié par le décret du 13 octobre 2015 relatif aux services réguliers interurbains de transport public routier de personnes librement organisés : " Une décision d'interdiction ou de limitation prise par une autorité organisatrice en application de l'article L. 3111-18 du code des transports peut porter sur tout service routier librement organisé assurant une liaison intérieure de distance routière inférieure ou égale au seuil mentionné au premier alinéa de cet article déjà assurée par un service de l'autorité ou une liaison similaire./ Cette décision, si elle est prise à l'occasion de la déclaration d'un nouveau service, peut s'accompagner du réexamen de plein droit des décisions portant sur les services routiers antérieurement déclarés assurant la même liaison déjà assurée par un service de l'autorité ou une liaison similaire " ; <br/>
<br/>
              5.	Considérant qu'il résulte de ces dispositions que si l'Autorité doit, dans son analyse de l'atteinte à l'équilibre économique de la ligne de service public qui est concurrencée, tenir compte des effets cumulés de l'ensemble des services proposés qui sont susceptibles de la concurrencer, elle ne peut prendre en considération que les services qui ont fait l'objet d'une déclaration et non ceux qui, faute de déclaration, ne peuvent être commercialisés, la déclaration de ces derniers pouvant donner lieu, le cas échéant, à une nouvelle décision de l'autorité organisatrice des transports soumise à un nouvel avis conforme ; que, par suite, la région n'est pas fondée à soutenir que l'Autorité a commis une erreur de droit en ne prenant pas en compte la ligne que la société FlixBus France envisageait de mettre en service entre Niort et La Rochelle, qui n'avait pas, à la date de l'avis attaqué, fait l'objet d'une déclaration ;<br/>
<br/>
              6.	Considérant, en dernier lieu, qu'il ressort des pièces du dossier que l'Autorité, qui a pu, à juste titre, prendre en compte la circonstance que les liaisons déclarées par la société FlixBus France s'inscrivent dans le cadre de l'exploitation de lignes de longue distance, a évalué la perte de recettes maximale, pour le service public de transport en cause,  liée à la mise en place des liaisons de la société Flixbus à 340 000 euros, estimation d'ailleurs supérieure à celle présentée par la région ; qu'au regard du montant des subventions publiques versées par la région, qui finance à hauteur de 10,7 millions d'euros ce service public, dont les recettes commerciales s'élèvent à 4,7 millions d'euros, l'Autorité a pu, sans commettre d'erreur d'appréciation, estimer que les services déclarés par la société FlixBus France n'étaient pas susceptibles de porter une atteinte substantielle au service de transport organisé par la région requérante ;<br/>
<br/>
              7.	Considérant qu'il résulte de tout ce qui précède que la région Aquitaine-Limousin-Poitou-Charentes n'est pas fondée à demander l'annulation pour excès de pouvoir de l'avis attaqué ;<br/>
<br/>
              8.	Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Autorité de régulation des activités ferroviaires et routières, qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu de faire droit aux conclusions présentées au titre des mêmes dispositions par l'Autorité de régulation des activités ferroviaires et routières, qui ne justifie avoir exposé aucun frais pour sa défense dans la présente instance ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de la région Aquitaine-Limousin-Poitou-Charentes est rejetée.<br/>
<br/>
Article 2 : Les conclusions de l'Autorité de régulation des activités ferroviaires et routières présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la région Nouvelle-Aquitaine, à la société FlixBus France et à l'Autorité de régulation des activités ferroviaires et routières. Copie en sera adressée à la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
