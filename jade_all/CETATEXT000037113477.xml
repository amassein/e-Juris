<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037113477</ID>
<ANCIEN_ID>JG_L_2018_06_000000401333</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/11/34/CETATEXT000037113477.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 27/06/2018, 401333, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401333</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUTET-HOURDEAUX ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Matias de Sainte Lorette</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Yohann Bénard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:401333.20180627</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et deux nouveaux mémoires, enregistrés les 8 juillet et 23 décembre 2016 et les 15 janvier et 28 février 2018 au secrétariat du contentieux du Conseil d'Etat, la société Manufacture de tabacs Heintz Van Landewyck demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 24 juin 2016 portant homologation des prix de vente au détail des tabacs manufacturés en France, à l'exclusion des départements d'outre-mer, en tant qu'il n'a pas homologué les prix de certains de ces produits et, par voie de conséquence, en tant qu'il a abrogé l'arrêté du 4 février 2015 modifié qui incluait ces prix ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matias de Sainte Lorette, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Yohann Bénard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boutet-Hourdeaux, avocat de la société Manufacture de tabacs Heintz Van Landewyck et à la SCP Foussard, Froger, avocat du ministre de l'économie et des finances.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. La société Landewyck France a intérêt à l'annulation de l'arrêté attaqué en tant qu'il n'a pas homologué les prix de certains des produits de la société Manufacture de tabacs Heintz Van Landewyck. Par suite, son intervention est recevable.<br/>
<br/>
              2. Aux termes de l'article 572 du code général des impôts, dans sa rédaction applicable à la date de l'arrêté attaqué : " Le prix de détail de chaque produit exprimé aux 1 000 unités ou aux 1 000 grammes, est unique pour l'ensemble du territoire et librement déterminé par les fabricants et les fournisseurs agréés. Il est applicable après avoir été homologué par arrêté conjoint des ministres chargés de la santé et du budget, dans des conditions définies par décret en Conseil d'Etat. Il ne peut toutefois être homologué s'il est inférieur à la somme du prix de revient et de l'ensemble des taxes. / Les tabacs manufacturés vendus ou importés dans les départements de Corse sont ceux qui ont été homologués conformément aux dispositions du premier alinéa. Toutefois, le prix de vente au détail applicable à ces produits dans les départements de Corse est déterminé dans les conditions prévues à l'article 575 E bis. / En cas de changement de prix de vente, et sur instruction expresse de l'administration, les débitants de tabac sont tenus de déclarer, dans les cinq jours qui suivent la date d'entrée en vigueur des nouveaux prix, les quantités en leur possession à cette date. ". Aux termes de l'article 284 de l'annexe II à ce code dans sa rédaction applicable à cette même date : " A la demande de l'administration, les fournisseurs agréés communiquent, par voie dématérialisée auprès de la direction générale des douanes et droits indirects, les prix de vente au détail des tabacs manufacturés, pour chacun de leurs produits par marque et dénomination commerciale. / Les prix sont homologués par arrêté conjoint du ministre chargé du budget et du ministre chargé de la santé et publiés au Journal officiel de la République française. ".<br/>
<br/>
              3. Par une lettre du 6 avril 2016, la direction générale des douanes et des droits indirects a invité les fabricants de produits du tabac, dont la société Manufacture de tabacs Heintz van Landewyck, à déposer les prix qu'ils souhaitaient présenter à l'homologation. Cette lettre précise que les fabricants de produits du tabac n'ont pas accès à l'application dédiée dite " Soprano-Delphes " mais qu'ils communiquent leurs prix, soit directement à un fournisseur agréé qui les intègre ensuite dans cette application, soit à l'administration elle-même, par l'intermédiaire d'une adresse électronique fonctionnelle, à charge pour cette dernière de les transmettre au fournisseur agréé désigné par le fabricant. Au cours de la campagne en litige d'homologation des prix des produits du tabac, qui pouvaient être déposés par les fabricants jusqu'au 29 avril 2016 au plus tard, ces derniers n'ont, en conséquence, pas eu accès à l'application " Soprano-Delphes " et ont dû communiquer leurs prix, directement ou par l'intermédiaire de l'adresse électronique susmentionnée, à des fournisseurs agréés. La société requérante demande l'annulation de l'arrêté d'homologation des prix des produits du tabac du 24 juin 2016, adopté à l'issue de cette procédure, en tant qu'il n'a pas homologué les prix de certains des produits qu'elle distribuait antérieurement sur le marché français et, par voie de conséquence, en tant qu'il a abrogé l'arrêté du 4 février 2015 modifié qui incluait ces prix<br/>
<br/>
              4. Dans sa rédaction applicable à la date de l'arrêté attaqué, à laquelle s'apprécie la légalité de ce dernier, l'article 284 de l'annexe II au code général des impôts, cité au point 2, prévoyait que seuls les fournisseurs agréés communiquent à la direction générale des douanes et des droits indirects les prix de vente au détail des tabacs manufacturés, ce dont il résultait que les fabricants de ces produits devaient transmettre aux fournisseurs agréés les prix qu'ils souhaitaient présenter à l'homologation.<br/>
<br/>
              5. Par une décision nos 405705, 405767 du 7 février 2018, le Conseil d'Etat statuant au contentieux a jugé qu'eu égard aux liens capitalistiques qui unissent certains fournisseurs agréés à des fabricants de produits du tabac et à la nature des informations collectées sur les prix, qui révèlent la stratégie commerciale des fabricants de produits du tabac, les dispositions du décret du 7 juin 2016 modifiant l'article 284 de l'annexe II au code général des impôts portent une atteinte illégale au secret des affaires en tant qu'elles ne permettent pas aux fabricants de produits du tabac de communiquer directement leurs prix à l'administration. <br/>
<br/>
              6. En raison des effets qui s'y attachent, l'annulation pour excès de pouvoir d'un acte administratif, qu'il soit ou non réglementaire, emporte, lorsque le juge est saisi de conclusions recevables, l'annulation par voie de conséquence des décisions administratives consécutives qui n'auraient pu légalement être prises en l'absence de l'acte annulé ou qui sont en l'espèce intervenues en raison de l'acte annulé. Il en va ainsi, notamment, des décisions qui ont été prises en application de l'acte annulé et de celles dont l'acte annulé constitue la base légale. Il incombe au juge de l'excès de pouvoir, lorsqu'il est saisi de conclusions recevables dirigées contre de telles décisions consécutives, de prononcer leur annulation par voie de conséquence, le cas échéant en relevant d'office un tel moyen qui découle de l'autorité absolue de chose jugée qui s'attache à l'annulation du premier acte.<br/>
<br/>
              7. Le décret du 7 juin 2016 précité constitue, en tant qu'il a prévu que seuls les fournisseurs agréés communiquent à la direction générale des douanes et des droits indirects les prix de vente au détail des tabacs manufacturés, la base légale de la procédure suivie pour l'adoption de l'arrêté attaqué. Par suite, il y a lieu de prononcer l'annulation de cet arrêté, par voie de conséquence de la décision du 7 février 2018 mentionnée au point 5, en tant qu'il n'a pas homologué les références de certains des produits de la société requérante et en tant qu'il a, par son article 4, abrogé l'arrêté du 4 février 2015 dans ses dispositions relatives aux références en cause.<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la société Manufacture de tabacs Heintz van Landewyck au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de la société Landewyck France est admise.<br/>
Article 2 : L'arrêté du 24 juin 2016 portant homologation des prix de vente au détail des tabacs manufacturés en France, à l'exclusion des départements d'outre-mer, est annulé en tant qu'il n'a pas homologué les prix de certains des produits de la société Manufacture de tabacs Heintz van Landewyck et en tant qu'il a, par son article 4, abrogé l'arrêté du 4 février 2015 dans ses dispositions relatives aux prix en cause.<br/>
Article 3 : L'Etat versera la somme de 3 000 euros à la société Manufacture de tabacs Heintz van Landewyck au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société Manufacture de tabacs Heintz Van Landewyck, à la société Landewyck France, au ministre de l'économie et des finances et à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
