<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033191642</ID>
<ANCIEN_ID>JG_L_2016_09_000000403553</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/19/16/CETATEXT000033191642.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 22/09/2016, 403553, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-09-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403553</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2016:403553.20160922</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...C...a demandé au juge des référés du tribunal administratif de Nantes, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre à l'Office français de l'immigration et de l'intégration (OFII) de lui verser à compter du 16 février 2016 l'allocation pour demandeur d'asile qu'elle a sollicitée, en sa qualité de représentante légale, pour sa fille N'gama Keita, dans le délai de 48 heures à compter de la notification de l'ordonnance à intervenir, sous une astreinte de 250 euros par jour de retard ou, à défaut, en cas de doute sur les atteintes graves et manifestement illégales au droit d'asile invoquées, de saisir la Cour de justice de l'Union européenne, sur le fondement de l'article 267 du Traité sur le fonctionnement de l'Union européenne et, dans l'attente, de prendre toute mesure permettant à N'gama Keita de subvenir à ses besoins élémentaires et, subsidiairement, d'enjoindre à l'OFII d'examiner sa demande tendant à ce que sa fille bénéficie des conditions matérielles d'accueil offertes aux demandeurs d'asile, dans le délai de 24 heures à compter de la notification de l'ordonnance à intervenir, sous une astreinte de 250 euros par jour de retard. <br/>
              Par une ordonnance n° 1607453 du 8 septembre 2016, le juge des référés du tribunal administratif de Nantes a rejeté sa demande. <br/>
              Par une requête, enregistrée le 16 septembre 2016 au secrétariat du contentieux du Conseil d'Etat, Mme B...C...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de faire droit à ses demandes de première instance ;<br/>
              3°) de mettre à la charge de l'OFII la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - la condition d'urgence est remplie compte tenu de la vulnérabilité de sa fille et d'elle-même, dénuées de toutes ressources pour subvenir à leurs besoins élémentaires ;<br/>
              - l'ordonnance attaquée n'a pas correctement évalué l'atteinte grave et manifestement illégale portée par l'OFII au droit à des conditions d'accueil, corollaire du droit d'asile, dont doit bénéficier sa fille, en s'abstenant de prendre en considération sa vulnérabilité ;<br/>
              - les articles L. 744-9 et D. 744-18 du code de l'entrée et du séjour des étrangers et du droit d'asile méconnaissent le directive de 2013/33/UE du 26 juin 2013, les articles 1er et 24 de la charte des droits fondamentaux de l'Union européenne et l'article 3-1 de la  convention internationale des droits de l'enfant.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention internationale des droits de l'enfant ;<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la charte des droits fondamentaux de l'Union européenne ;<br/>
              - la directive 2013/33/UE du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. A cet égard, il appartient au juge d'appel de prendre en compte les éléments recueillis par le juge du premier degré dans le cadre de la procédure écrite et orale qu'il a diligentée. <br/>
<br/>
               2. Mme B...C..., ressortissante guinéenne entrée en France en mai 2013, a vu sa demande d'asile rejetée par une décision du 28 avril 2014 de l'Office français de la protection des étrangers et des demandeurs d'asile (OFPRA). Ce refus a été confirmé le 5 février 2016 par une décision devenue définitive de la Cour nationale du droit d'asile (CNDA). Le 16 février 2016, Mme C...a déposé une demande d'asile pour sa fille N'gama Keita, née le 7 avril 2011 et qui serait entrée en France en janvier 2016. Le 14 mars 2016, le préfet des Yvelines a délivré une attestation de demandeur d'asile pour la fille de MmeC.... Agissant en qualité de représentante légale, MmeC..., qui avait elle-même bénéficié du versement de l'allocation pour demandeur d'asile pendant la procédure d'examen de sa propre demande par l'OFPRA puis par la CNDA, a sollicité de l'OFII le versement au profit de sa fille de l'allocation pour demandeur d'asile. Par une décision du 29 août 2016, l'OFII a rejeté cette demande sur le fondement de l'article D. 744-18 du code de l'entrée et du séjour des étrangers et du droit d'asile, qui réserve le bénéfice de l'allocation pour demandeur d'asile aux personnes âgées de dix-huit ans révolus. Mme C...relève appel de l'ordonnance du 8 septembre 2016 par laquelle le juge des référés du tribunal administratif de Nantes a rejeté sa demande tendant à ce qu'il soit enjoint à l'OFII, sur le fondement de l'article L. 521-2 du code de justice administrative, de lui verser cette allocation, pour le compte de sa fille, à compter du 16 février 2016.<br/>
              3. Mme C...se borne à reprendre en appel l'argumentation qu'elle avait présentée en première instance et qui, compte tenu de l'ensemble des éléments du dossier, a été écartée à bon droit par le juge des référés du tribunal administratif de Nantes. Il est ainsi manifeste que, pour les motifs retenus par le juge de première instance et compte tenu tant de l'office du juge du référé-liberté que de la situation de la requérante, son appel ne peut être accueilli. Il y a lieu, par adoption des motifs retenus par le premier juge des référés, de rejeter la requête de MmeC..., y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, selon la procédure prévue par l'article L. 522-3 de ce code.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de Mme B...C...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme B...C....<br/>
Copie en sera en sera adressée à l'Office français de l'immigration et de l'intégration. <br/>
Copie en sera adressée pour information au ministre de l'intérieur.<br/>
Fait à Paris, le 22 septembre 2016<br/>
    Signé :  M. D...A...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
