<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034166783</ID>
<ANCIEN_ID>JG_L_2017_03_000000397942</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/16/67/CETATEXT000034166783.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 10/03/2017, 397942, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397942</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET ; BROUCHOT</AVOCATS>
<RAPPORTEUR>M. Thomas Odinot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:397942.20170310</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Groupe Vinet a demandé au tribunal administratif de Toulouse de condamner l'office public d'HLM Habitat Toulouse à lui verser la somme de 70 525,13 euros TTC, correspondant au solde du lot n° 16 " sols souples " du marché de construction d'un ensemble immobilier et une indemnité de 3 000 euros à titre de dommages et intérêts. Par un jugement n° 1002202 du 11 mars 2014, le tribunal administratif de Toulouse a condamné l'office à verser à la société requérante une somme de 10 786,85 euros augmentée des intérêts moratoires à compter du 5 février 2010 et de la capitalisation de ces intérêts à compter du 5 février 2011.<br/>
<br/>
              Par un arrêt n° 14BX01375 du 19 janvier 2016, la cour administrative d'appel de Bordeaux a, sur appel de l'office public d'HLM Habitat Toulouse et appel incident de la société Groupe Vinet, condamné celle-ci à verser à l'office la somme de 17 203,90 euros, assortie des intérêts au taux légal à compter de la date de réception par la société de la lettre du 9 novembre 2009 par laquelle l'office lui a notifié le décompte général, et rejeté le surplus des conclusions des parties.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 15 mars, 20 mai et 3 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, la société Groupe Vinet demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt, en tant qu'il lui fait grief ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'office public d'HLM Habitat Toulouse la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Odinot, auditeur,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de la société Groupe Vinet, et à Me Brouchot, avocat de l'office public d'HLM Habitat Toulouse.<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que par acte d'engagement signé le 18 décembre 2007, l'office public d'HLM (OPHLM) Habitat Toulouse a confié à la société Groupe Vinet le lot n° 16 " sols souples " d'un marché de construction pour un montant de 129 598,88 euros HT ; que, par un jugement du 11 mars 2014, le tribunal administratif de Toulouse a condamné l'OPHLM à verser à la société Groupe Vinet, au titre du solde du marché, la somme de 10 786,85 euros, assortie des intérêts moratoires et de la capitalisation des intérêts ; que, sur appel principal de l'OPHLM et appel incident de la société Groupe Vinet, la cour administrative d'appel de Bordeaux a, par un arrêt du 19 janvier 2016 contre lequel se pourvoit en cassation la société Groupe Vinet, condamné celle-ci à verser à l'OPHLM la somme de 17 203,90 euros au titre du solde du marché, assortie des intérêts moratoires ;<br/>
<br/>
              2. Considérant, d'une part, que l'analyse du mémoire présenté par la société requérante en réponse au moyen d'ordre public qui lui avait été communiqué par la cour est suffisante ;<br/>
<br/>
              3. Considérant, d'autre part, qu'il résulte des termes de l'arrêt attaqué que le motif par lequel la cour a jugé irrecevables les moyens invoqués par la société Groupe Vinet à l'appui de son appel incident, tirés de l'irrégularité du jugement du tribunal administratif de Toulouse, présente un caractère surabondant ; que, dès lors, les autres moyens du pourvoi, qui sont dirigés contre ce motif, sont inopérants ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que la société Groupe Vinet n'est pas fondée à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'office public d'HLM Habitat Toulouse qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce de mettre à la charge de la société Groupe Vinet la somme de 3 000 euros à verser à l'office public d'HLM Habitat Toulouse, au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Groupe Vinet est rejeté.<br/>
Article 2 : La société Groupe Vinet versera à l'office public d'HLM Habitat Toulouse la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Groupe Vinet et à l'office public d'HLM Habitat Toulouse.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
