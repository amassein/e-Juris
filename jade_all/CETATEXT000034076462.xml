<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034076462</ID>
<ANCIEN_ID>JG_L_2017_02_000000404007</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/07/64/CETATEXT000034076462.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 22/02/2017, 404007, Publié au recueil Lebon</TITRE>
<DATE_DEC>2017-02-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404007</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2017:404007.20170222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un arrêt n° 16PA00920 du 29 septembre 2016, enregistré le 4 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Paris, avant de statuer sur l'appel de Mme C...B...tendant à l'annulation de l'ordonnance du 4 décembre 2015 par laquelle le président du tribunal administratif de Nouvelle Calédonie a rejeté sa demande d'annulation pour excès de pouvoir de l'arrêté du 16 juin 2015 du maire de Nouméa accordant un permis de construire à M. D...A..., a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen les questions suivantes :<br/>
<br/>
              1°) Les dispositions de l'article R. 600-1 du code de l'urbanisme, qui ne s'appliquaient initialement pas en Nouvelle-Calédonie, y sont-elles devenues applicables et, dans l'affirmative, à compter de quelle date ' ;<br/>
<br/>
              2°) Dans l'hypothèse où les dispositions de cet article seraient devenues applicables en Nouvelle-Calédonie, y a-t-il lieu de tirer des conséquences, quant à la recevabilité d'une requête introduite sans que celles-ci aient été respectées, du fait qu'aucune publicité n'ait été donnée à ce changement de l'état du droit, ni aucun délai fixé pour l'entrée en vigueur de ces dispositions ' Le juge administratif peut-il notamment, ou même doit-il, afin d'assurer le respect du principe de sécurité juridique et du droit au recours, décider d'aménager ou de différer l'application de la règle nouvelle et le changement de jurisprudence qui en résulte '<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - la loi organique n° 99-209 du 19 mars 1999 ;<br/>
              - la loi organique n° 2009-969 du 3 août 2009 ;<br/>
              - le décret n° 2000-389 du 4 mai 2000 ;<br/>
              - le décret n° 2013-879 du 1er octobre 2013 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              le rapport de Mme Marie Gautier-Melleray, maître des requêtes, <br/>
<br/>
              les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              - La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la commune de Nouméa ;<br/>
<br/>
<br/>
<br/>
<br/>Rend l'avis suivant :<br/>
<br/>
              1. Aux termes de l'article R. 600-1 du code de l'urbanisme, dans sa rédaction issue du I de l'article 4 du décret du 4 mai 2000 relatif à la partie réglementaire du code de justice administrative : " En cas de déféré du préfet ou de recours contentieux à l'encontre d'un document d'urbanisme ou d'une décision relative à l'occupation ou l'utilisation du sol régie par le présent code, le préfet ou l'auteur du recours est tenu, à peine d'irrecevabilité, de notifier son recours à l'auteur de la décision et, s'il y a lieu, au titulaire de l'autorisation. Cette notification doit également être effectuée dans les mêmes conditions en cas de demande tendant à l'annulation ou à la réformation d'une décision juridictionnelle concernant un document d'urbanisme ou une décision relative à l'occupation ou l'utilisation du sol. L'auteur d'un recours administratif est également tenu de le notifier à peine d'irrecevabilité du recours contentieux qu'il pourrait intenter ultérieurement en cas de rejet du recours administratif./ La notification prévue au précédent alinéa doit intervenir par lettre recommandée avec accusé de réception, dans un délai de quinze jours francs à compter du dépôt du déféré ou du recours./ La notification du recours à l'auteur de la décision et, s'il y a lieu, au titulaire de l'autorisation est réputée accomplie à la date d'envoi de la lettre recommandée avec accusé de réception. Cette date est établie par le certificat de dépôt de la lettre recommandée auprès des services postaux ". <br/>
<br/>
              2. L'obligation de notification résultant de l'article R. 600-1 du code de l'urbanisme a le caractère d'une règle de procédure contentieuse.<br/>
<br/>
              3. L'article R. 600-1 du code de l'urbanisme, dans sa rédaction citée au point 1, ayant été créé par le décret du 4 mai 2000, était applicable en Nouvelle-Calédonie, à compter du 1er janvier 2001, date d'entrée en vigueur du décret, en vertu de l'article 6 de ce dernier, aux termes duquel : " Les dispositions du présent décret sont applicables en Nouvelle-Calédonie, en Polynésie française, dans les îles Wallis et Futuna, dans les Terres australes et antarctiques françaises et à Mayotte, à l'exception des dispositions du chapitre 6 du titre VII du livre VII du code de justice administrative, et sous réserve de l'applicabilité, dans ces collectivités, des textes cités en les reproduisant par le code de justice administrative ".<br/>
<br/>
              4. La loi organique du 3 août 2009 a inséré dans la loi organique du 19 mars 1999 relative à la Nouvelle-Calédonie un article 6-2, lequel prévoit que " [...] sont applicables de plein droit en Nouvelle-Calédonie, sans préjudice des dispositions les adaptant à son organisation particulière, les dispositions législatives et réglementaires qui sont relatives :/ (...) 6° A la procédure administrative contentieuse" et qui vaut tant pour les dispositions relatives à la procédure administrative contentieuse introduites après cette date que pour celles qui étaient alors en vigueur. La loi organique du 3 août 2009 n'a ainsi pas modifié l'état du droit applicable en Nouvelle-Calédonie quant à l'applicabilité dans ce territoire de l'article R. 600-1 du code de justice administrative.<br/>
<br/>
              5. Une publicité suffisante de cette règle de procédure contentieuse a, en tout état de cause, été assurée par la publication régulière de la loi organique du 3 août 2009, après l'entrée en vigueur de laquelle la demande de Mme B...a été présentée. Dans ces conditions, il n'y a pas lieu de différer dans le temps, afin de garantir l'exigence de sécurité juridique et le respect du droit au recours, l'application, par le juge, de cette règle de procédure contentieuse, qui n'est applicable qu'aux requêtes introduites après son entrée en vigueur.<br/>
<br/>
              6. Le présent avis sera notifié à la cour administrative d'appel de Paris, à Mme C...B..., à M. D...A..., à la commune de Nouméa et à la ministre du logement et de l'habitat durable. Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">46-01-03-02-02 OUTRE-MER. DROIT APPLICABLE. LOIS ET RÈGLEMENTS (HORS STATUTS DES COLLECTIVITÉS). COLLECTIVITÉS D'OUTRE-MER ET NOUVELLE-CALÉDONIE. NOUVELLE-CALÉDONIE. - APPLICABILITÉ DE L'ARTICLE R. 600-1 DU CODE DE L'URBANISME (OBLIGATION DE NOTIFICATION DES RECOURS) - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-06-01-04 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INTRODUCTION DE L'INSTANCE. OBLIGATION DE NOTIFICATION DU RECOURS. - APPLICABILITÉ EN NOUVELLE-CALÉDONIE - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 46-01-03-02-02 Article R. 600-1 du code de l'urbanisme, dans sa rédaction issue du I de l'article 4 du décret n° 2000-389 du 4 mai 2000.... ,,Cet article, ayant été créé par le décret du 4 mai 2000, était applicable en Nouvelle-Calédonie à compter du 1er janvier 2001, date d'entrée en vigueur de ce décret en vertu de son article 6.</ANA>
<ANA ID="9B"> 68-06-01-04 Article R. 600-1 du code de l'urbanisme, dans sa rédaction issue du I de l'article 4 du décret n° 2000-389 du 4 mai 2000.... ,,Cet article, ayant été créé par le décret du 4 mai 2000, était applicable en Nouvelle-Calédonie à compter du 1er janvier 2001, date d'entrée en vigueur de ce décret.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Ab. jur. CE, 27 avril 2011, SARL Altitude et SCI Tina-sur-Mer, n°s 312093 312166, T. pp. 1041-1200.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
