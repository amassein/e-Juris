<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037802652</ID>
<ANCIEN_ID>JG_L_2018_12_000000417244</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/80/26/CETATEXT000037802652.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 12/12/2018, 417244, Publié au recueil Lebon</TITRE>
<DATE_DEC>2018-12-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417244</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Richard Senghor</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:417244.20181212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un nouveau mémoire, enregistrés les 11 janvier et 21 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, la Section française de l'observatoire international des prisons demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet résultant du silence gardé par le Premier ministre, sur sa demande tendant à l'abrogation des articles R. 57-8-8, R. 57-8-13 à R. 57-8-17, D. 57 et D. 297 du code de procédure pénale ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre d'abroger ces dispositions ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - la Constitution ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de procédure pénale ; <br/>
              - la loi n° 2009-1436 du 24 novembre 2009 ;<br/>
              - la décision n° 2018-715 QPC du Conseil constitutionnel du 22 juin 2018 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Richard Senghor, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la Section francaise de l'observatoire international des prisons ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. La Section française de l'Observatoire international des prisons demande l'annulation pour excès de pouvoir de la décision du Premier ministre opposant un refus à la demande d'abrogation des articles R. 57-8-8, R. 57-8-13 à R. 57-8-17, D. 57 et D. 297 du code de procédure pénale.<br/>
<br/>
<br/>
              Sur les conclusions dirigées contre le refus d'abroger les dispositions relatives aux modalités d'organisation des visites des détenus en prévention  :<br/>
<br/>
              2. L'article 36 de la loi pénitentiaire du 24 novembre 2009 dispose que : " Les unités de vie familiale ou les parloirs familiaux implantés au sein des établissements pénitentiaires peuvent accueillir toute personne détenue. Toute personne détenue peut bénéficier à sa demande d'au moins une visite trimestrielle dans une unité de vie familiale ou un parloir familial, dont la durée est fixée en tenant compte de l'éloignement du visiteur. Pour les prévenus, ce droit s'exerce sous réserve de l'accord de l'autorité judiciaire compétente ". Aux termes de l'article 145-4 du code de procédure pénale : " Lorsque la personne mise en examen est placée en détention provisoire, le juge d'instruction peut prescrire à son encontre l'interdiction de communiquer pour une période de dix jours. Cette mesure peut être renouvelée, mais pour une nouvelle période de dix jours seulement. En aucun cas l'interdiction de communiquer ne s'applique à l'avocat de la personne mise en examen. /Sous réserve des dispositions qui précèdent, toute personne placée en détention provisoire peut, avec l'autorisation du juge d'instruction, recevoir des visites sur son lieu de détention ou téléphoner à un tiers. /A l'expiration d'un délai d'un mois à compter du placement en détention provisoire, le juge d'instruction ne peut refuser de délivrer un permis de visite ou d'autoriser l'usage du téléphone que par une décision écrite et spécialement motivée au regard des nécessités de l'instruction, du maintien du bon ordre et de la sécurité ou de la prévention des infractions. /Cette décision est notifiée par tout moyen et sans délai au demandeur. Ce dernier peut la déférer au président de la chambre de l'instruction, qui statue dans un délai de cinq jours par une décision écrite et motivée non susceptible de recours. Lorsqu'il infirme la décision du juge d'instruction, le président de la chambre de l'instruction délivre le permis de visite ou l'autorisation de téléphoner. /Après la clôture de l'instruction, les attributions du juge d'instruction sont exercées par le procureur de la République selon les formes et conditions prévues au présent article. Il en est de même dans tous les autres cas où une personne est placée en détention provisoire. /A défaut de réponse du juge d'instruction ou du procureur de la République à la demande de permis de visite ou de téléphoner dans un délai de vingt jours, la personne peut également saisir le président de la chambre de l'instruction. /Lorsque la procédure est en instance d'appel, les attributions du procureur de la République sont confiées au procureur général ". <br/>
<br/>
              3. Les articles R. 57-8-13 à R. 57-8-15 du code de procédure pénale définissent le régime particulier des visites qui sont effectuées au sein des parloirs familiaux et des unités de vie familiale. S'agissant des prévenus, le refus de faire droit à une demande de bénéficier d'un parloir familial ou d'une unité de vie familiale est opposé dans les conditions prévues par l'article R. 57-8-8 du code de procédure pénale  aux termes duquel : " Les permis de visite sont délivrés, refusés, suspendus ou retirés pour les personnes détenues prévenues par le magistrat saisi du dossier de la procédure dans les conditions prévues par l'article 145-4. Ce magistrat peut prescrire que les visites auront lieu dans un parloir avec dispositif de séparation (...) ".  Il s'ensuit que, contrairement à ce qui est soutenu par l'association requérante, de tels refus peuvent faire l'objet d'un recours devant le président de la chambre de l'instruction en application des dispositions combinées des articles 145-4 et R. 57-8-8 du code de procédure pénale, conformément aux exigences de l'article 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. <br/>
<br/>
<br/>
              Sur les conclusions dirigées contre le refus d'abroger les dispositions relatives à la correspondance des détenus en prévention :<br/>
<br/>
              4. L'article 40 de la loi pénitentiaire du 24 novembre 2009 dispose que : " Les personnes condamnées et, sous réserve que l'autorité judiciaire ne s'y oppose pas, les personnes prévenues peuvent correspondre par écrit avec toute personne de leur choix ". Aux termes de l'article R. 57-8-16 du code de procédure pénale : " Les personnes détenues peuvent correspondre par écrit tous les jours et sans limitation avec toute personne de leur choix. / Pour les personnes prévenues, le magistrat saisi du dossier de la procédure peut s'y opposer soit de façon générale soit à l'égard d'un ou plusieurs destinataires expressément mentionnés dans sa décision. / Les correspondances écrites par les prévenus ou à eux adressées sont, sauf décision contraire du magistrat, communiquées à celui-ci ". Aux termes de l'article R. 57-8-17 du  même code : " La décision refusant à une personne prévenue l'exercice du droit de correspondance lui est notifiée par tout moyen ".<br/>
<br/>
              5. Par sa décision n° 2018-715 QPC du 22 juin 2018, le Conseil constitutionnel a déclaré contraires à la Constitution les mots " sous réserve que l'autorité judiciaire ne s'y oppose pas " figurant au premier alinéa de l'article 40 de la loi du 24 novembre 2009, au motif que ces dispositions méconnaissent le droit à un recours juridictionnel effectif garanti par l'article 16 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789. L'article 2 du dispositif de cette décision précise que la déclaration d'inconstitutionnalité prend effet dans les conditions fixées aux paragraphes 9 et 10, qui prévoient que, d'une part, " afin de permettre au législateur de remédier à l'inconstitutionnalité constatée, il y a lieu de reporter au 1er mars 2019 la date de cette abrogation " et, d'autre part, " afin de faire cesser l'inconstitutionnalité constatée à compter de la publication de la présente décision, il y a lieu de juger que les décisions de refus prises après la date de cette publication peuvent être contestées devant le président de la chambre de l'instruction dans les conditions prévues par la deuxième phrase du quatrième alinéa de l'article 145-4 du code de procédure pénale ". <br/>
<br/>
              6. L'absence de prescriptions relatives à la remise en cause des effets produits par les dispositions déclarées contraires à la Constitution avant leur abrogation doit, en l'espèce, eu égard à la circonstance que le Conseil constitutionnel a décidé de reporter dans le temps les effets abrogatifs de sa décision, être regardée comme indiquant que celui-ci n'a pas entendu remettre en cause les effets que la disposition déclarée contraire à la Constitution avait produits avant la date de son abrogation. Par suite, et alors même que l'association requérante est l'auteur de la question prioritaire de constitutionnalité, la déclaration d'inconstitutionnalité des mots " sous réserve que l'autorité judiciaire ne s'y oppose pas " figurant au premier alinéa de l'article 40 de la loi du 24 novembre 2009 est, à la date de la présente décision, sans incidence sur l'issue du litige dirigé contre le refus d'abroger les articles R. 58-8-16 et R. 58-8-17 du code de procédure pénale. <br/>
<br/>
              7. Le Conseil constitutionnel ayant, afin de faire cesser immédiatement l'atteinte au droit au recours portée par les dispositions litigieuses, consacré l'existence d'une voie de recours permettant, dès la date de publication de sa décision, aux détenus prévenus de contester le refus par l'autorité judiciaire de les autoriser à exercer leur droit de correspondance avec des tiers dans les conditions prévues à l'article 145-4 du code de procédure pénale, le moyen tiré de la violation des stipulations de l'article 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ne peut qu'être écarté.<br/>
<br/>
<br/>
              Sur les conclusions dirigées contre le refus d'abroger les dispositions relatives aux translations judiciaires des détenus en prévention :<br/>
<br/>
              8. Pour demander l'annulation du refus d'abroger l'article D. 57 du code de procédure pénale relatif aux mesures d'extraction ou de translation des personnes prévenues en tant qu'il renvoie aux dispositions du premier alinéa de l'article D. 297 du même code qui prévoit que " les personnes détenues en prévention sont transférées sur la réquisition de l'autorité judiciaire compétente selon les règles édictées par le présent code " ainsi que ces dispositions, la Section française de l'Observatoire international des prisons soutient que les dispositions litigieuses méconnaissent l'article 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, en l'absence de voie de recours permettant de contester ces translations judiciaires. <br/>
<br/>
              9. En premier lieu, il résulte du régime de la détention provisoire prévu par les articles143-1 et suivants du code de procédure pénale que toutes les décisions affectant ses modalités d'exécution impliquent nécessairement l'intervention du magistrat judiciaire saisi du dossier de la procédure. Il s'ensuit que le pouvoir réglementaire était compétent pour définir les conditions dans lesquelles l'autorité judiciaire ordonne la translation judiciaire d'une personne détenue en prévention.<br/>
<br/>
              10. En second lieu, eu égard à leur nature et à leurs effets, afin de respecter les exigences fixées par l'article 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, les décisions de changement d'affectation entre établissements de même nature doivent pouvoir faire l'objet d'un recours, au moins lorsque la nouvelle affectation s'accompagne d'une modification du régime de détention entraînant une aggravation des conditions de détention ou, si tel n'est pas le cas, lorsque sont en cause des libertés et des droits fondamentaux des détenus. Il s'ensuit que le pouvoir réglementaire ne pouvait légalement intervenir tant que le législateur n'avait pas préalablement organisé, dans son champ de compétence relatif à la procédure pénale, une voie de recours effectif permettant de contester des mesures de translation judiciaire, à tout le moins dans les cas mentionnés précédemment. Par suite, en l'absence de la possibilité d'exercer un tel recours, le pouvoir réglementaire ne pouvait légalement édicter ni le premier alinéa de l'article D. 57 du code de procédure pénale en tant qu'il renvoie au premier alinéa de l'article D. 297 du même code, ni le premier alinéa de ce même article D. 297.<br/>
<br/>
              11. Il résulte de l'ensemble de ce qui précède que la Section française de l'Observatoire international des prisons est seulement fondée à demander l'annulation de la décision par laquelle le Premier ministre a refusé d'abroger le premier alinéa de l'article D. 57 du code de procédure pénale en tant qu'il renvoie au premier alinéa de l'article D. 297 du même code ainsi que le premier alinéa de ce même article D. 297.<br/>
<br/>
              12.  L'annulation du refus d'abroger le premier alinéa de l'article D. 57 du code de procédure pénale en tant qu'il renvoie à l'article D. 297 du même code implique nécessairement que le Premier ministre procède à cette abrogation. Il y a lieu, dans les circonstances de l'espèce, de lui enjoindre d'abroger ces dispositions dans un délai de douze mois à compter de la notification de la présente décision, à défaut d'intervention du législateur, dans ce délai, pour ouvrir une voie de recours à l'encontre des mesures de translations judiciaires qu'elles visent.<br/>
<br/>
              13. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 1500  euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le refus d'abroger le premier alinéa de l'article D. 57 du code de procédure pénale en tant qu'il renvoie au premier alinéa de l'article D. 297 du même code ainsi que le premier alinéa de cet article D. 297 est annulé.<br/>
Article 2 : Il est enjoint au Premier ministre d'abroger ces dispositions dans un délai de douze mois à compter de la notification de la présente décision, à défaut d'intervention du législateur, dans ce délai, pour ouvrir une voie de recours à l'encontre des mesures de translations judiciaires qu'elles visent.<br/>
Article 3 : L'Etat versera à la Section française de l'Observatoire international des prisons une somme de 1 500 euros au titre de l'article L. 761 1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 5 : La présente décision sera notifiée à la Section française de l'observatoire international des prisons, au Premier ministre et à la garde des sceaux, ministre de la justice. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-02-01-02-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. LOI ET RÈGLEMENT. ARTICLES 34 ET 37 DE LA CONSTITUTION - MESURES RELEVANT DU DOMAINE DE LA LOI. RÈGLES CONCERNANT L'ORGANISATION JURIDICTIONNELLE. - RÈGLES CONCERNANT LA PROCÉDURE PÉNALE - ORGANISATION D'UNE VOIE DE RECOURS PERMETTANT DE CONTESTER LES MESURES DE TRANSLATION JUDICIAIRE DES DÉTENUS EN PRÉVENTION [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-02-01-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. LOI ET RÈGLEMENT. ARTICLES 34 ET 37 DE LA CONSTITUTION - MESURES RELEVANT DU DOMAINE DU RÈGLEMENT. - 1) MESURES RELATIVES AUX CONDITIONS DANS LESQUELLES L'AUTORITÉ JUDICIAIRE ORDONNE LA TRANSLATION JUDICIAIRE D'UNE PERSONNE DÉTENUE EN PRÉVENTION - INCLUSION, COMPTE TENU DU RÉGIME LÉGISLATIF DE LA DÉTENTION PROVISOIRE (ART. L. 143-1 ET S. DU CPP) - 2) MESURES RELATIVES À L'ORGANISATION DES TRANSLATIONS JUDICIAIRES DES DÉTENUS EN PRÉVENTION - COMPÉTENCE SUBORDONNÉE À L'INTERVENTION DU LÉGISLATEUR [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">26-055-01-13 DROITS CIVILS ET INDIVIDUELS. CONVENTION EUROPÉENNE DES DROITS DE L'HOMME. DROITS GARANTIS PAR LA CONVENTION. DROIT À UN RECOURS EFFECTIF (ART. 13). - CONSÉQUENCE - MESURES DE TRANSLATION JUDICIAIRE - HYPOTHÈSES MINIMALES JUSTIFIANT L'ORGANISATION PAR LE LÉGISLATEUR D'UNE VOIE DE RECOURS EFFECTIVE CONTRE CES MESURES.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-10-09 PROCÉDURE. - QPC SOULEVÉE À L'OCCASION D'UN REP DIRIGÉ CONTRE UN REFUS D'ABROGER DES DISPOSITIONS RÉGLEMENTAIRES - DÉCLARATION D'INCONSTITUTIONNALITÉ DONNANT LIEU À UNE ABROGATION DIFFÉRÉE PAR LE CONSEIL CONSTITUTIONNEL, ET À LA CRÉATION D'UNE VOIE DE RECOURS TEMPORAIRE POUR REMÉDIER À L'ATTEINTE PORTÉE AU DROIT À UN RECOURS JURIDICTIONNEL EFFECTIF - CONSÉQUENCES SUR LE REP - 1) MOYEN TIRÉ DE L'INCONSTITUTIONNALITÉ DE LA BASE LÉGALE - OPÉRANCE - ABSENCE [RJ1] - 2) MOYEN TIRÉ DE LA MÉCONNAISSANCE DU DROIT AU RECOURS GARANTI PAR L'ARTICLE 13 CEDH - ABSENCE, COMPTE TENU DE CETTE VOIE DE RECOURS TEMPORAIRE.
</SCT>
<ANA ID="9A"> 01-02-01-02-01 Eu égard à leur nature et à leurs effets, afin de respecter les exigences fixées par l'article 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, les décisions de changement d'affectation entre établissements de même nature doivent pouvoir faire l'objet d'un recours, au moins lorsque la nouvelle affectation s'accompagne d'une modification du régime de détention entraînant une aggravation des conditions de détention ou, si tel n'est pas le cas, lorsque sont en cause des libertés et des droits fondamentaux des détenus. Il s'ensuit que le pouvoir réglementaire ne pouvait légalement intervenir tant que le législateur n'avait pas préalablement organisé, dans son champ de compétence relatif à la procédure pénale, une voie de recours effectif permettant de contester des mesures de translation judiciaire, à tout le moins dans les cas mentionnés précédemment.</ANA>
<ANA ID="9B"> 01-02-01-03 1) Il résulte du régime de la détention provisoire que toutes les décisions affectant ses modalités d'exécution impliquent nécessairement l'intervention du magistrat judiciaire saisi du dossier de la procédure conformément au régime de la détention provisoire prévu par les articles L. 143-1 et suivants du code de procédure pénale (CPP). Il s'ensuit que le pouvoir réglementaire était compétent pour définir les conditions dans lesquelles l'autorité judiciaire ordonne la translation judiciaire d'une personne détenue en prévention.,,2) Eu égard à leur nature et à leurs effets, afin de respecter les exigences fixées par l'article 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, les décisions de changement d'affectation entre établissements de même nature doivent pouvoir faire l'objet d'un recours, au moins lorsque la nouvelle affectation s'accompagne d'une modification du régime de détention entraînant une aggravation des conditions de détention ou, si tel n'est pas le cas, lorsque sont en cause des libertés et des droits fondamentaux des détenus. Il s'ensuit que le pouvoir réglementaire ne pouvait légalement intervenir tant que le législateur n'avait pas préalablement organisé, dans son champ de compétence relatif à la procédure pénale, une voie de recours effectif permettant de contester des mesures de translation judiciaire, à tout le moins dans les cas mentionnés précédemment.</ANA>
<ANA ID="9C"> 26-055-01-13 Eu égard à leur nature et à leurs effets, afin de respecter les exigences fixées par l'article 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, les décisions de changement d'affectation entre établissements de même nature doivent pouvoir faire l'objet d'un recours, au moins lorsque la nouvelle affectation s'accompagne d'une modification du régime de détention entraînant une aggravation des conditions de détention ou, si tel n'est pas le cas, lorsque sont en cause des libertés et des droits fondamentaux des détenus. Il s'ensuit que le pouvoir réglementaire ne pouvait légalement intervenir tant que le législateur n'avait pas préalablement organisé, dans son champ de compétence relatif à la procédure pénale, une voie de recours effectif permettant de contester des mesures de translation judiciaire, à tout le moins dans les cas mentionnés précédemment.</ANA>
<ANA ID="9D"> 54-10-09 Disposition législative déclarée contraire à la Constitution par le Conseil constitutionnel qui, statuant sur la question prioritaire de constitutionnalité (QPC) soulevée par la requérante à l'occasion d'un recours pour excès de pouvoir contre un refus d'abroger les articles réglementaires dont cette disposition constituait la base légale, en a prononcé l'abrogation avec effet différé, en créant une voie de recours temporaire destinée à pallier la méconnaissance du droit à un recours juridictionnel effectif ayant fondé la censure.... ...1) L'absence de prescriptions relatives à la remise en cause des effets produits par les dispositions déclarées contraires à la Constitution avant leur abrogation doit, en l'espèce, eu égard à la circonstance que le Conseil constitutionnel a décidé de reporter dans le temps les effets abrogatifs de sa décision, être regardée comme indiquant que celui-ci n'a pas entendu remettre en cause les effets que la disposition déclarée contraire à la Constitution avait produits avant la date de son abrogation. Par suite, et alors même que la requérante est l'auteur de la QPC, la déclaration d'inconstitutionnalité est, à la date de la présente décision, sans incidence sur l'issue du litige dirigé contre le refus d'abroger les articles réglementaires dont la disposition censurée constitue la base légale.... ...2) Le Conseil constitutionnel ayant, afin de faire cesser immédiatement l'atteinte au droit au recours portée par les dispositions litigieuses, consacré l'existence d'une voie de recours permettant, dès la date de publication de sa décision, aux détenus prévenus de contester le refus par l'autorité judiciaire de les autoriser à exercer leur droit de correspondance avec des tiers dans les conditions prévues à l'article 145-4 du code de procédure pénale (CPP), le moyen tiré de la violation des stipulations de l'article 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ne peut qu'être écarté.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 14 novembre 2012, Association France Nature Environnement Réseau Juridique, n° 340539, T. pp. 940-965. Cf, sol. contr., s'agissant d'une abrogation à effet immédiat, CE, 30 mai 2018, Mme Schreuer, n° 400912, à publier au Recueil., ,[RJ2] Rappr, s'agissant des mesures de mise à l'isolement en prison ordonnées par l'autorité judiciaire, CE, Section, 31 octobre 2008, Section française l'Observatoire international des prisons, n° 293785, p. 374.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
