<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026701790</ID>
<ANCIEN_ID>JG_L_2012_11_000000353652</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/70/17/CETATEXT000026701790.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 28/11/2012, 353652, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-11-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353652</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Alain Ménéménis</PRESIDENT>
<AVOCATS>SCP MONOD, COLIN</AVOCATS>
<RAPPORTEUR>M. Matthieu Schlesinger</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:353652.20121128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 26 octobre 2011 et 26 janvier 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SAS Eiffage TP, agissant en son nom et en tant qu'associée de la société en participation Tunnel Maurice Lemaire, ayant son siège 2 rue Hélène Boucher à Neuilly-sur-Marne (93330), représentée par son gérant en exercice ; la SAS Eiffage TP demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10VE03148 du 15 juillet 2011 par lequel la cour administrative d'appel de Versailles a rejeté sa requête tendant à l'annulation du jugement n° 0912953 du 13 juillet 2010 par lequel le tribunal administratif de Montreuil a rejeté sa demande tendant à la décharge de la cotisation minimale de taxe professionnelle à laquelle elle a été assujettie au titre de l'année 2006 ainsi que des pénalités correspondantes ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et le premier protocole additionnel à cette convention ; <br/>
<br/>
              Vu le code civil ; <br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matthieu Schlesinger, Auditeur,  <br/>
<br/>
              - les observations de la SCP Monod, Colin, avocat de la SAS Eiffage TP,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Monod, Colin, avocat de la SAS Eiffage TP ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SAS Eiffage TP est gérante et associée à hauteur de 50 % dans la société en participation Tunnel Maurice Lemaire, dont les statuts ont été enregistrés à la recette de Neuilly-sur-Marne le 3 décembre 2003 ; qu'à l'occasion d'un contrôle sur pièces, l'administration fiscale a constaté que les opérations effectuées dans le cadre de cette société en participation n'avaient donné lieu à aucune déclaration au titre de la cotisation minimale de taxe professionnelle prévue à l'article 1647 E du code général des impôts, alors que le chiffre d'affaires était supérieur au seuil de 7 600 000 euros ; qu'en conséquence, elle a établi une cotisation minimale de taxe professionnelle à raison de cette activité pour l'année 2006, qu'elle a libellée, à hauteur de la part de celle-ci dans la société en participation, au nom de la SAS Eiffage TP, associée gérante de cette société en participation ; que le tribunal administratif de Montreuil, a rejeté sa demande par un jugement du 13 juillet 2010 ; que la société a relevé appel de ce jugement en tant qu'il a rejeté ses conclusions, sous le n  0912953, relatives à la cotisation minimale de taxe professionnelle au titre de l'année 2006 due à raison de l'activité exercée dans le cadre de la société en participation Tunnel Maurice Lemaire ; qu'elle se pourvoit en cassation contre l'arrêt du 15 juillet 2011 par lequel la cour administrative d'appel de Versailles a rejeté sa requête ;<br/>
<br/>
              Sans qu'il soit besoin d'examiner les moyens du pourvoi ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 1447 du code général des impôts dans sa rédaction applicable aux impositions en litige : " I. La taxe professionnelle est due chaque année par les personnes physiques ou morales qui exercent à titre habituel une activité professionnelle non salariée (...) " ; qu'aux termes de l'article 1448 du même code, alors en vigueur : " La taxe professionnelle est établie suivant la capacité contributive des redevables, appréciée d'après des critères économiques en fonction de l'importance des activités exercées par eux (...) " ; qu'aux termes de l'article 1476, dans sa rédaction applicable aux impositions en litige : " La taxe professionnelle est établie au nom des personnes qui exercent l'activité imposable, dans les conditions prévues en matière de contributions directes (...) " ; qu'aux termes de l'article 1647 E, dans sa rédaction alors en vigueur : " I. La cotisation de taxe professionnelle des entreprises dont le chiffre d'affaires est supérieur à 7 600 000 euros est au moins égale à 1,5 % de la valeur ajoutée produite par l'entreprise, telle que définie au II de l'article 1647 B sexies. (...) II. Le supplément d'imposition, défini par différence entre la cotisation résultant des dispositions du I et la cotisation de taxe professionnelle déterminée selon les règles définies au III, est une recette du budget général de l'Etat (...) " ; qu'enfin, aux termes de l'article 310 HP de l'annexe II au code général des impôts dans sa version applicable aux impositions en litige : " L'imposition à la taxe professionnelle des sociétés de fait et des sociétés en participation est libellée au nom du ou des associés connus des tiers. " ;<br/>
<br/>
              3. Considérant qu'une société en participation, qui possède un bilan fiscal propre et qui doit, le cas échéant, acquitter la taxe sur la valeur ajoutée à raison des opérations qu'elle effectue, constitue, du point de vue fiscal, malgré son absence de personnalité juridique, une entité distincte de ses membres ; qu'il s'en déduit que lorsqu'elle exerce à titre habituel une activité professionnelle non salariée, elle est elle-même légalement redevable des cotisations de taxe professionnelle et, le cas échéant, de la cotisation minimale de taxe professionnelle dues à raison de cette activité, sans qu'y fasse obstacle la circonstance que ces cotisations, qui constituent une obligation fiscale propre de la société et non une obligation de ses membres à hauteur de leur participation, ne peuvent être libellées, en l'absence de personnalité morale de la société en participation, qu'au nom de ceux de ses associés qui sont connus de l'administration fiscale, ainsi que le prévoit, sans excéder sa compétence, l'article 310 HP de l'annexe II au code général des impôts précité ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que la cour administrative d'appel a entaché son arrêt d'erreur de droit en jugeant que, dans le cas où une activité professionnelle non salariée est exercée par une société en participation, les dispositions précitées du code général des impôts doivent être interprétées comme faisant naître, au titre de la taxe professionnelle et de la cotisation minimale de taxe professionnelle dues à raison de cette activité, une obligation fiscale dans le chef non de la société en participation elle-même mais des personnes physiques ou morales la constituant ; que la SAS Eiffage TP est, dès lors, fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              Sur les conclusions de la SAS Eiffage TP présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à la société requérante d'une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : L'Etat versera à la société requérante une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la SAS Eiffage TP et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
