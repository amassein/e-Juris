<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044468705</ID>
<ANCIEN_ID>JG_L_2021_12_000000436695</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/46/87/CETATEXT000044468705.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 09/12/2021, 436695, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436695</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Olivier Guiard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Céline  Guibé</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:436695.20211209</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 12 décembre 2019, 12 mars 2020 et le 4 mai 2021 au secrétariat du contentieux du Conseil d'Etat, la société Enedis demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la délibération de la Commission de régulation de l'énergie n° 2019-138 du 25 juin 2019 portant évolution au 1er août 2019 de la grille tarifaire des tarifs d'utilisation des réseaux publics d'électricité dans les domaines de haute tension A (HTA) et basse tension (BT), en tant que le point k) de son annexe 1 ne prévoit pas la couverture intégrale des sommes qu'elle a exposées au titre des contrats de prestations de services conclus avec les fournisseurs d'électricité afin d'assurer la gestion de clientèle des utilisateurs titulaires d'un contrat unique. <br/>
<br/>
              2°) d'annuler la décision du 10 octobre 2019 rejetant le recours gracieux qu'elle a formé contre cette délibération ; <br/>
<br/>
              3°) d'enjoindre à la Commission de régulation de l'énergie de prendre une nouvelle délibération prévoyant la couverture tarifaire des sommes versées aux fournisseurs d'électricité en contrepartie de leur gestion des clients titulaires d'un contrat unique et, en particulier, la couverture du montant de 2 658 540 euros qu'elle a exposé dans le cadre des contrats de prestations de services conclus avec les sociétés Enercoop, Eni et Total Spring ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'énergie ; <br/>
              - le code de la consommation ;  <br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Guiard, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Céline Guibé, rapporteure publique ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Afin de simplifier la souscription des contrats d'approvisionnement en électricité des petits consommateurs et de faciliter la mise en œuvre de leur droit de choisir leur fournisseur à la suite de la libéralisation du marché de détail de l'électricité, l'article L. 224-8 du code de la consommation et l'article L. 332-3 du code de l'énergie prévoient la faculté de conclure un contrat unique portant sur la fourniture et la distribution d'électricité. La souscription d'un tel contrat dispense les consommateurs de conclure directement un contrat d'accès au réseau avec le gestionnaire du réseau de distribution, parallèlement au contrat de fourniture conclu avec leur fournisseur. <br/>
<br/>
              2. Il ressort des pièces du dossier que, par une délibération n° 2018-148 du 28 juin 2018, la Commission de régulation de l'énergie a défini les nouveaux tarifs d'utilisation des réseaux publics d'électricité haute tension A (HTA) et basse tension (BT) dits " TURPE 5 bis HTA-BT " applicable à compter du 1er août 2018. Par une délibération n° 2019-138 du 25 juin 2019, la Commission de régulation de l'énergie a décidé de faire évoluer cette grille tarifaire à compter du 1er août 2019, d'une part en retenant une évolution moyenne à la hausse de 3,04 % et, d'autre part, en ajustant le montant moyen par client de la contrepartie versée aux fournisseurs par le gestionnaire de réseau au titre des prestations de gestion de clientèle que ces fournisseurs assurent pour le compte du gestionnaire au profit des utilisateurs titulaires d'un contrat unique. La société Enedis demande l'annulation de cette seconde délibération, en tant que le point k) de son annexe 1 ne prévoit pas la couverture intégrale des sommes exposées par elle au titre des contrats de prestations de services conclus avec différents fournisseurs d'électricité. Elle demande également l'annulation de la décision du 10 octobre 2019 rejetant son recours gracieux.<br/>
<br/>
              Sur la légalité externe des décisions attaquées :<br/>
<br/>
              3. Aux termes du 4ème alinéa de l'article L. 341-3 du code de l'énergie, dans sa version alors en vigueur : " (...) La Commission de régulation de l'énergie transmet à l'autorité administrative pour publication au Journal officiel de la République française, ses décisions motivées relatives aux évolutions, en niveau et en structure, des tarifs d'utilisation des réseaux publics de transport et de distribution d'électricité, aux évolutions des tarifs des prestations annexes réalisées à titre exclusif par les gestionnaires de réseaux et aux dates d'entrée en vigueur de ces tarifs. (...) ".<br/>
<br/>
              4. Si la société Enedis soutient que la délibération n° 2019-138 du 25 juin 2019 est insuffisamment motivée, il ressort des pièces du dossier que cette délibération de dix-huit pages accompagnées de quatre annexes comporte l'énoncé des considérations de droit et de fait qui en constituent le fondement. S'agissant spécifiquement des modalités de prise en compte des charges liées à la gestion de clientèle en contrat unique, la délibération attaquée précise que la délibération tarifaire n° 2018-148 du 28 juin 2018 a reconduit celles qui avaient été définies par la délibération n° 2017-239 du 26 octobre 2017 qu'elle met œuvre au point k) de son annexe 1. L'exigence de motivation prévue par l'article L. 341-3 précité du code de l'énergie n'imposait pas à la Commission de régulation de l'énergie, par ailleurs, de mentionner l'intégralité des éléments écartés au titre du solde du compte de régularisation des charges et des produits en fonction duquel, notamment, a été déterminée l'évolution des tarifs " TURPE 5 bis HTA-BT " à compter du 1er août 2019. Il suit de là que le moyen tiré du défaut de motivation de la délibération du 25 juin 2019 doit être écarté. Il en va de même du moyen d'insuffisance de motivation invoqué à l'encontre de la décision du 10 octobre 2019 rejetant le recours gracieux formé par la société Enedis contre cette délibération, dès lors que les vices propres dont cette décision serait entachée ne peuvent, en tout état de cause, pas être utilement invoqués. <br/>
<br/>
              Sur la légalité interne des décisions attaquées :<br/>
<br/>
              5. Aux termes du premier alinéa de l'article L. 341-2 du code de l'énergie : " Les tarifs d'utilisation du réseau public de transport et des réseaux publics de distribution sont calculés de manière transparente et non discriminatoire, afin de couvrir l'ensemble des coûts supportés par les gestionnaires de ces réseaux dans la mesure où ces coûts correspondent à ceux d'un gestionnaire de réseau efficace. " Il résulte de ces dispositions que la rémunération qu'un gestionnaire de réseau de transport d'électricité tire de l'application des tarifs d'utilisation du réseau public doit au moins couvrir ses charges d'exploitation et ses charges d'investissement, prises dans leur ensemble, sous réserve que ces coûts n'excèdent pas ceux d'un gestionnaire de réseau efficace compte tenu des gains de productivité attendus de lui.  <br/>
<br/>
              6. La société Enedis soutient que les décisions attaquées méconnaissent le principe de couverture tarifaire intégrale des coûts supportés par le gestionnaire du réseau de distribution d'électricité en violation des dispositions précitées de l'article L. 341-2 du code de l'énergie, au motif que la Commission de régulation de l'énergie, tout en actualisant la grille des tarifs d'utilisation des réseaux publics d'électricité dans les domaines de tension HTA et BT à compter du 1er aout 2019, aurait refusé de couvrir certaines sommes exposées par elle à défaut d'écart significatif et global entre ces sommes et son revenu autorisé.<br/>
<br/>
              7. En premier lieu, toutefois, il ressort des pièces du dossier et la Commission de régulation de l'énergie fait valoir, sans être sérieusement contestée sur ce point, que les sommes versées avant le 1er janvier 2017 par la société Enedis aux sociétés Enercoop et Total Spring en application des contrats de prestations de services qu'elle avait conclus avec ces fournisseurs, ont été totalement couvertes par les revenus que le gestionnaire du réseau public d'électricité a perçus durant la période au titre de laquelle se sont appliqués les tarifs dits " TURPE 4 ". La société Enedis n'est donc pas fondée à soutenir que ces sommes auraient également dû être prises en compte pour déterminer l'évolution de la grille tarifaire des tarifs " TURPE 5 bis HTA-BT " à compter du 1er août 2019. <br/>
<br/>
              8. En deuxième lieu, il ressort des termes du point k) de l'annexe 1 à la délibération attaquée du 25 juin 2019 que le montant de 228 millions d'euros, correspondant au revenu autorisé ex post, au titre de l'année 2018, pour couvrir les charges exposées par la société Enedis relatives à la contrepartie versée aux fournisseurs d'électricité pour la gestion des clients en contrat unique, est constitué, premièrement, de la somme des contreparties versées aux fournisseurs à ce titre en 2018, soit 224,1 millions d'euros, deuxièmement, du rattrapage des versements réalisés en 2017 non couverts par l'ajustement annuel du 1er août 2018, soit 3,7 millions d'euros, troisièmement, des montants versés en 2018 au titre de prestations de gestion de clientèle en contrat unique réalisées antérieurement au 1er janvier 2018, soit 0,2 millions d'euros. S'agissant de ces derniers versements, il ressort également du point k) de l'annexe 1 à la délibération attaquée que ceux-ci ont été couverts dans la limite d'un montant maximum de 2,42 euros par an et par point de comptage en application de la délibération tarifaire du 28 juin 2018, qui a elle-même reconduit, ainsi qu'il a été dit au point 4, les modalités de prise en compte des charges liées à la gestion de clientèle en contrat unique définies par la délibération n° 2017-239 du 26 octobre 2017. <br/>
<br/>
              9. Dès lors que par une délibération du 26 juillet 2012 portant communication relative à la gestion de clients en contrat unique la Commission de régulation de l'énergie avait indiqué que la rémunération versée par le gestionnaire du réseau public d'électricité aux fournisseurs pour la gestion de ces clients était de nature à entrer dans le périmètre des charges couvertes par les tarifs d'utilisation des réseaux publics d'électricité, il appartenait à la société requérante, lors de la signature des contrats de prestation de services avec les fournisseurs d'électricité, de ne pas s'engager, de manière définitive, à verser à ces derniers des rémunérations susceptibles d'excéder les coûts d'un gestionnaire de réseau efficace. Si la société Enedis fait valoir que l'ensemble des contrats de prestation de services qu'elle a conclus avec les fournisseurs d'électricité ont été transmis à la Commission de régulation de l'énergie, il ne ressort d'aucune pièce versée au dossier que le montant des rémunérations prévu par ces contrats en faveur des fournisseurs d'électricité aurait été repris à son compte par la commission, ni que celle-ci aurait décidé de prendre en compte ces sommes au titre du CRCP dans leur intégralité. Il suit de là que la société Enedis n'est pas fondée à soutenir que la totalité de la somme de 1 450 000 euros versée à la société ENI en 2018 en contrepartie de prestations de gestion de clientèle en contrat unique réalisées antérieurement au 1er janvier 2018, dont il n'est pas démontré qu'elle correspondrait à des coûts correspondant à ceux d'un gestionnaire efficace, aurait dû légalement être couverte par le CRCP. <br/>
<br/>
              10. En troisième et dernier lieu, en opposant à la société Enedis, dans sa décision du 10 octobre 2019 rejetant le recours gracieux qu'elle avait formé à l'encontre la délibération du 25 juin 2019, le fait que les arguments présentés à l'appui de ce recours étaient connus depuis le 31 janvier 2019, soit préalablement à l'adoption de la délibération contestée, ainsi que le fait que la société Enedis n'apportait aucun élément nouveau à l'appui de son recours, la Commission de régulation de l'énergie a entendu fonder sa décision de rejet sur les mêmes motifs que sa délibération du 25 juin 2019 qui, ainsi qu'il a été dit aux points 7 à 9 ci-dessus, ne sont pas entachés d'erreur de droit au regard de l'exigence de couverture des coûts prévue par l'article L. 341-2 du code de l'énergie. Il résulte de l'instruction que la Commission de régulation de l'énergie aurait pris la même décision si elle s'était fondée uniquement sur ce motif. Par suite, la société requérante ne peut utilement critiquer le motif par lequel la décision du 10 décembre 2019 relève, par ailleurs, que ni les éléments qu'elle avait présentés ni les informations détenues par la commission ne permettaient de constater, à la date de cette décision, qu'un écart significatif s'était produit ou serait susceptible de se produire entre le revenu autorisé et les coûts de la société. <br/>
<br/>
              11. Il résulte de tout ce qui précède que la société Enedis n'est fondée à demander l'annulation ni de la délibération de la Commission de régulation de l'énergie du 25 juin 2019, en tant que le point k) de son annexe 1 ne prévoirait pas la couverture intégrale des sommes qu'elle a exposées en contrepartie des prestations de gestion de clientèle en contrat unique réalisées par les fournisseurs d'électricité, ni de la décision rejetant son recours gracieux. Sa requête doit, par suite, être rejetée, y compris ses conclusions à fin d'injonction et celles tendant au bénéfice des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le requête de la société Enedis est rejetée. <br/>
Article 2 : La présente décision sera notifiée à la société Enedis et à la Commission de régulation de l'énergie.  <br/>
              Délibéré à l'issue de la séance du 25 novembre 2021 où siégeaient : M. Frédéric Aladjidi, président de chambre, présidant ; M. Thomas Andrieu, conseiller d'Etat et M. Olivier Guiard, maître des requêtes-rapporteur. <br/>
<br/>
              Rendu le 9 décembre 2021.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Frédéric Aladjidi<br/>
 		Le rapporteur : <br/>
      Signé : M. Olivier Guiard<br/>
                 La secrétaire :<br/>
                 Signé : Mme A... B...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
