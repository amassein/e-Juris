<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037265645</ID>
<ANCIEN_ID>JG_L_2018_07_000000422241</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/26/56/CETATEXT000037265645.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, formation collégiale, 27/07/2018, 422241</TITRE>
<DATE_DEC>2018-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422241</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés, formation collégiale</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. M. Guyomar</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:422241.20180727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au juge des référés du tribunal administratif de Montpellier, d'ordonner au centre hospitalier universitaire de Montpellier, sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, de procéder à son admission en urgence et de réaliser l'intervention chirurgicale concernant son escarre, en particulier à la date prévue du 11 juillet 2018, d'autre part, d'assurer sa prise en charge post-opératoire soit directement, soit dans un centre de soins et réadaptation dans la région de Montpellier, jusqu'à la guérison de l'escarre et cicatrisation et d'assortir chacune des mesures d'injonction sollicitées d'une astreinte de 300 000 euros par jour de retard. <br/>
<br/>
              Par une ordonnance n° 1803156 du 6 juillet 2018, le juge des référés du tribunal administratif de Montpellier a rejeté sa demande.<br/>
<br/>
              Par une requête enregistrée le 14 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, M.B..., demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) d'enjoindre au centre hospitalier universitaire de Montpellier de procéder à son admission dans un délai n'excédant pas cinq jours et de programmer l'intervention chirurgicale concernant son escarre dans un délai n'excédant pas huit jours, sous astreinte de 1 000 euros par jour de retard ;<br/>
<br/>
              3°) d'enjoindre au centre hospitalier universitaire de Montpellier d'assurer sa prise en charge post-opératoire dans ses services ou dans un centre extérieur adapté à ses soins ;<br/>
              4°) de mettre à la charge du centre hospitalier universitaire de Montpellier la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence est remplie dès lors que les douleurs causées par son escarre sont intolérables ;<br/>
              - l'opération est nécessaire et urgente ;<br/>
              - l'abstention d'administrer des soins en cas d'urgence est un refus de soins illicite constituant une discrimination en raison de sa tétraplégie et menaçant la liberté fondamentale d'accès aux soins ; <br/>
              - le choix de différer l'intervention chirurgicale constitue un traitement inhumain et dégradant au sens de l'article 3 de la Convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - les médecins sont tenus d'agir, la défaillance de la permanence ou de la continuité des soins constituant un refus de soins illicite ;<br/>
              - l'aggravation de son état de santé résulte d'une absence de prise en charge efficace et adaptée par l'équipe médicale du centre hospitalier universitaire de Montpellier.<br/>
              Par un mémoire en défense, enregistré le 19 juillet 2018, le CHU de Montpellier conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par M. B...ne sont pas fondés. <br/>
<br/>
              Par un mémoire en défense, enregistré le 19 juillet 2018, la ministre des solidarités et de la santé conclut au rejet de la requête. Elle soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par M. B...ne sont pas fondés.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir convoqué à une audience à huis-clos, d'une part, M. B...et, d'autre part, le CHU de Montpellier et la ministre des solidarités et de la santé ; <br/>
<br/>
              Vu le procès-verbal de l'audience à huis-clos du 25 juillet 2018 à 9 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Gaschignard, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. B... ;<br/>
<br/>
              - la représentante de M.B... ;<br/>
              - Me Le Prado, avocat au Conseil d'Etat et à la Cour de cassation, avocat du centre hospitalier universitaire de Montpellier ;<br/>
<br/>
              - les représentants du centre hospitalier universitaire de Montpellier ;<br/>
<br/>
              - les représentants de la ministre des solidarités et de la santé ;<br/>
<br/>
	et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ".<br/>
<br/>
              2. Il résulte de l'instruction que M. A...B..., né en 1994, a été victime, en 2015, d'un accident de la circulation qui l'a rendu tétraplégique. Depuis 2017, il est suivi par le centre hospitalier universitaire (CHU) de Montpellier pour une escarre ischiatique gauche. L'intervention chirurgicale programmée pour la traiter ayant été reportée à plusieurs reprises au cours de l'année 2018, M. B...a saisi, le 3 juillet 2018, le juge des référés du tribunal administratif de Montpellier, sur le fondement de l'article L. 521-2 du code de justice administrative, d'une demande tendant à ce qu'il soit enjoint au CHU de Montpellier de réaliser cette intervention chirurgicale et d'assurer sa prise en charge post opératoire. Il relève appel de l'ordonnance du 6 juillet 2018 par laquelle le juge des référés a rejeté cette demande.<br/>
<br/>
              3. Aux termes des dispositions de l'article L. 731-1 du code de justice administrative : " Par dérogation aux dispositions de l'article L. 6, le président de la formation de jugement peut, à titre exceptionnel, décider que l'audience aura lieu ou se poursuivra hors la présence du public, si la sauvegarde de l'ordre public ou le respect de l'intimité des personnes ou de secrets protégés par la loi l'exige. ". Dans les circonstances de l'espèce, compte tenu des informations couvertes par le secret médical échangées au cours de l'audience et du respect de l'intimité du requérant, il a été fait application de ces dispositions en tenant l'audience, devant une formation collégiale, hors la présence du public. <br/>
<br/>
              4. Aux termes de l'article L. 1111-4 du code de la santé publique : " Toute personne prend, avec le professionnel de santé et compte tenu des informations et des préconisations qu'il lui fournit, les décisions concernant sa santé. (...) / Aucun acte médical ni aucun traitement ne peut être pratiqué sans le consentement libre et éclairé de la personne et ce consentement peut être retiré à tout moment. (...) ". Aux termes de l'article L. 1110-5 du même code : " Toute personne a, compte tenu de son état de santé et de l'urgence des interventions que celui-ci requiert, le droit de recevoir, sur l'ensemble du territoire, les traitements et les soins les plus appropriés et de bénéficier des thérapeutiques dont l'efficacité est reconnue et qui garantissent la meilleure sécurité sanitaire et le meilleur apaisement possible de la souffrance au regard des connaissances médicales avérées. Les actes de prévention, d'investigation ou de traitements et de soins ne doivent pas, en l'état des connaissances médicales, lui faire courir de risques disproportionnés par rapport au bénéfice escompté (...). ". Il résulte de ces dispositions que toute personne a le droit de recevoir les traitements et les soins les plus appropriés à son état de santé sous réserve de son consentement libre et éclairé. En revanche, ces mêmes dispositions ni aucune autre ne consacrent, au profit du patient, un droit de choisir son traitement. <br/>
<br/>
              5. Il résulte de l'instruction que le litige porté devant le juge des référés concerne le choix d'administrer un traitement, plus particulièrement, les modalités et le calendrier fixés pour la réalisation d'une opération chirurgicale, au vu du bilan qu'il appartient aux médecins d'effectuer en tenant compte, d'une part, des risques encourus et, d'autre part, du bénéfice escompté.<br/>
<br/>
              6. Il résulte de l'instruction que M. B...est suivi, depuis 2017, au titre de l'escarre ischiatique gauche dont il souffre, par le CHU de Montpellier, en particulier le service " Plaies, cicatrisation, brûlés ". L'équipe médicale en charge de ce patient a décidé, en l'absence de cicatrisation de cette plaie, de réaliser une intervention chirurgicale consistant en une couverture tégumentaire par lambeau à laquelle M. B...a donné son consentement. Cette intervention chirurgicale a été programmée à trois reprises, les 9 mars, 12 avril et 11 juillet 2018 pour être, à chaque fois, finalement reportée. Il ressort, tant des pièces du dossier que des échanges au cours de l'audience, que les décisions de reporter l'opération sont fondées sur la circonstance que les conditions requises pour le succès de l'intervention chirurgicale n'apparaissent pas réunies. D'une part, l'état du patient, qui souffre d'une importante dénutrition et d'une addiction au tabac et au cannabis, est tel que les conditions pré-opératoires ne sont pas satisfaites. D'autre part, les refus de prendre en charge M. B...opposés par les centres de soins de suite et de réadaptation sollicités par le CHU de Montpellier, dans le cadre du parcours de soins, font obstacle à la mise en place du suivi post-opératoire spécifique qu'implique nécessairement la réalisation de l'intervention envisagée. Il s'ensuit qu'alors même que ces refus révèlent de regrettables dysfonctionnements, d'ordre administratif, qui ont affecté, en l'espèce, la  mise en oeuvre du parcours de soins, les décisions du CHU de Montpellier de reporter l'intervention chirurgicale de M. B...reposent sur des appréciations d'ordre médical portées dans le cadre du bilan qui doit être effectué entre le bénéfice escompté de l'opération et les risques encourus. Au vu de l'état du patient, tel qu'il a été évalué, en dernier lieu, dans le cadre de la consultation multidisciplinaire qui s'est tenue, au CHU de Montpellier, le 17 juillet 2018, l'équipe médicale a fixé à M. B...un nouveau rendez-vous, en consultation pré-opératoire, le 21 août 2018, afin d'apprécier si son état avait évolué conformément au " projet thérapeutique " mis en place à la réalisation duquel la soeur de l'intéressé apporte son appui. Par ailleurs, le CHU de Montpellier a indiqué que l'intervention chirurgicale qu'appellent à bref délai l'escarre de M. B... ainsi que les souffrances, physiques et morales, qu'elle provoque, serait réalisée à la condition que l'état nutritionnel et la diminution de la consommation de cannabis de l'intéressé le permettent, dans la première quinzaine du mois de septembre. Afin que les conditions requises pour le suivi post-opératoire soient réunies, le CHU de Montpellier s'est également engagé, si aucun centre de soins de suite et de réadaptation n'acceptait de le prendre en charge, à accueillir, à titre exceptionnel, M. B...le temps nécessaire à la cicatrisation de la plaie, au sein du service de soins de suite de médecine physique et de réadaptation de l'hôpital. Dans ces conditions, et dès lors qu'une prise en charge thérapeutique est assurée par l'hôpital selon les modalités décrites ci-dessus, il n'appartient pas au juge des référés, saisi sur le fondement de l'article L. 521-2 du code de justice administrative, de prescrire à l'équipe médicale que soit fixé un autre calendrier pour la réalisation de l'intervention chirurgicale que celui qu'elle a retenu à l'issue du bilan qu'il lui appartient d'effectuer.<br/>
<br/>
              7. Il résulte de ce qui précède que M. B...n'est pas fondé à se plaindre de ce que, par l'ordonnance du 6 juillet 2018, le juge des référés du tribunal administratif de Montpellier a rejeté sa demande. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à leur titre, à la charge du CHU de Montpellier.<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. A... B..., au centre hospitalier universitaire de Montpellier et à la ministre des solidarités et de la santé.<br/>
<br/>
              Délibéré à l'issue de la séance du 25 juillet 2018 où siégeaient : M. Jean-Denis Combrexelle, président de la section du contentieux, présidant ; M. D... C... et M. E...F..., conseillers d'Etat, juges des référés.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-035-03-04 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ TENDANT AU PRONONCÉ DE MESURES NÉCESSAIRES À LA SAUVEGARDE D'UNE LIBERTÉ FONDAMENTALE (ART. L. 521-2 DU CODE DE JUSTICE ADMINISTRATIVE). POUVOIRS ET DEVOIRS DU JUGE. - LITIGE PORTANT SUR LES MODALITÉS ET LE CALENDRIER FIXÉS POUR LA RÉALISATION D'UNE OPÉRATION CHIRURGICALE - OFFICE DU JUGE DU RÉFÉRÉ LIBERTÉ SAISI D'UNE DEMANDE TENDANT À CE QU'IL ORDONNE LA RÉALISATION D'UNE OPÉRATION CHIRURGICALE DANS UN DÉLAI DONNÉ - CONTRÔLE DE L'EXISTENCE D'UN BILAN EFFECTUÉ PAR L'ÉQUIPE MÉDICALE ENTRE LES BÉNÉFICES ESCOMPTÉS ET LES RISQUES ENCOURUS - INCLUSION - INJONCTION À L'ÉQUIPE MÉDICALE DE RETENIR UN AUTRE CALENDRIER QUE CELUI QU'ELLE A RETENU À L'ISSUE DE CE BILAN - EXCLUSION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">61-05 SANTÉ PUBLIQUE. BIOÉTHIQUE. - LITIGE PORTANT SUR LES MODALITÉS ET LE CALENDRIER FIXÉS POUR LA RÉALISATION D'UNE OPÉRATION CHIRURGICALE - OFFICE DU JUGE DU RÉFÉRÉ LIBERTÉ SAISI D'UNE DEMANDE TENDANT À CE QU'IL ORDONNE LA RÉALISATION D'UNE OPÉRATION CHIRURGICALE DANS UN DÉLAI DONNÉ - CONTRÔLE DE L'EXISTENCE D'UN BILAN EFFECTUÉ PAR L'ÉQUIPE MÉDICALE ENTRE LES BÉNÉFICES ESCOMPTÉS ET LES RISQUES ENCOURUS - INCLUSION - INJONCTION À L'ÉQUIPE MÉDICALE DE RETENIR UN AUTRE CALENDRIER QUE CELUI QU'ELLE A RETENU À L'ISSUE DE CE BILAN - EXCLUSION [RJ1].
</SCT>
<ANA ID="9A"> 54-035-03-04 Litige porté devant le juge des référés concernant le choix d'administrer un traitement, et plus particulièrement les modalités et le calendrier fixés pour la réalisation d'une opération chirurgicale, au vu du bilan qu'il appartient aux médecins d'effectuer en tenant compte, d'une part, des risques encourus, et, d'autre part, du bénéfice escompté.... ,,Le choix du traitement administré au patient résulte de l'appréciation comparée, par les médecins en charge, des bénéfices escomptés des stratégies thérapeutiques en débat ainsi que des risques qui y sont attachés. Dans ces conditions et dès lors qu'une prise en charge thérapeutique est assurée par l'hôpital, il n'appartient pas au juge des référés, saisi sur le fondement de l'article L. 521-2 du code de justice administrative (CJA), de prescrire à l'équipe médicale que soit fixé un autre calendrier pour la réalisation de l'intervention chirurgicale que celui qu'elle a retenu à l'issue du bilan qu'il lui appartient d'effectuer.</ANA>
<ANA ID="9B"> 61-05 Litige porté devant le juge des référés concernant le choix d'administrer un traitement, et plus particulièrement les modalités et le calendrier fixés pour la réalisation d'une opération chirurgicale, au vu du bilan qu'il appartient aux médecins d'effectuer en tenant compte, d'une part, des risques encourus, et, d'autre part, du bénéfice escompté.... ,,Le choix du traitement administré au patient résulte de l'appréciation comparée, par les médecins en charge, des bénéfices escomptés des stratégies thérapeutiques en débat ainsi que des risques qui y sont attachés. Dans ces conditions et dès lors qu'une prise en charge thérapeutique est assurée par l'hôpital, il n'appartient pas au juge des référés, saisi sur le fondement de l'article L. 521-2 du code de justice administrative (CJA), de prescrire à l'équipe médicale que soit fixé un autre calendrier pour la réalisation de l'intervention chirurgicale que celui qu'elle a retenu à l'issue du bilan qu'il lui appartient d'effectuer.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., en précisant s'agissant des modalités et du calendrier d'un traitement, CE, juge des référés, 26 juillet 2017,,, n° 412618, p. 279.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
