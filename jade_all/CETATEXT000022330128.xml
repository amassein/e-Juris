<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000022330128</ID>
<ANCIEN_ID>JG_L_2009_12_000000315838</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/22/33/01/CETATEXT000022330128.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 30/12/2009, 315838</TITRE>
<DATE_DEC>2009-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>315838</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin</PRESIDENT>
<AVOCATS>SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Alain  Boulanger</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Derepas Luc</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi et le mémoire complémentaire, enregistrés les 2 mai et 21 octobre 2008 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Jean A, demeurant ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 11 mars 2008 par lequel la cour régionale des pensions de Colmar, sur l'appel du ministre de la défense, a, d'une part, infirmé le jugement du 12 décembre 2005 du tribunal départemental des pensions du Bas-Rhin, rectifié par un jugement du 11 septembre 2006, d'autre part, débouté le requérant de sa demande de pension au titre de l'infirmité nouvelle résultant d'une affection intestinale, enfin, confirmé la décision ministérielle du 9 février 2004 de rejet de la prise en compte de cette infirmité pour le calcul de ses droits à pensions militaire d'invalidité ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du ministre de la défense ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Gatineau, Fattaccini, avocat de M. A, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des pensions militaires d'invalidité et des victimes de la guerre ;<br/>
<br/>
              Vu la loi n° 83-1109 du 21 décembre 1983 ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le décret n° 73-74 du 18 janvier 1973, modifié notamment par le décret n° 81-315 du 6 avril 1981 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Boulanger, chargé des fonctions de Maître des requêtes,<br/>
<br/>
              - les observations de la SCP Gatineau, Fattaccini, avocat de M. A, <br/>
<br/>
              - les conclusions de M. Luc Derepas, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Gatineau, Fattaccini, avocat de M. A ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'il résulte des énonciations de l'arrêt attaqué que M. A, interné au camp russe de Tambow en 1944, a sollicité la révision de sa pension en vue de la prise en compte de nouvelles infirmités dont celle résultant d'une colite ; que, par l'arrêt attaqué du 11 mars 2008, la cour régionale des pensions de Colmar a annulé le jugement par lequel le tribunal départemental des pensions du Bas-Rhin a fixé à 20 % le pourcentage d'invalidité de M. A résultant de son affection dite  colite vraie , retenu le lien au service de cette affection et fixé en conséquence à 75 % son taux d'invalidité totale ;<br/>
<br/>
              Considérant que l'article L. 3 du code des pensions militaires d'invalidité et des victimes de la guerre dispose que la présomption d'imputabilité au service d'une infirmité résultant d'une blessure ou d'une maladie ne bénéficie aux prisonniers de guerre et internés à l'étranger qu'à la condition que ces blessures ou maladies aient été régulièrement constatées dans certains délais qu'il fixe ; que, toutefois, les dispositions du guide-barème pour la classification des invalidités annexé à l'article D. 2 du même code par le décret du 18 janvier 1973 ont prévu pour certaines affections des délais de constatation supérieurs ; que le décret du 6 avril 1981, auquel la loi du 21 décembre 1983 a conféré valeur législative, a même supprimé tout délai de constatation pour certaines infirmités et maladies contractées au cours de la captivité subie dans certains camps ou lieux de détention ; qu'il en va notamment ainsi de l'affection dite  colite vraie  pour les anciens captifs du camp de Tambow ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que, par dérogation aux dispositions de l'article L. 3 du code des pensions militaires d'invalidité et des victimes de la guerre, la présomption d'imputabilité au service des  colites vraies  bénéficie aux anciens captifs du camp de Tambow sans que puisse leur être opposé un délai de constatation de cette affection ; que la cour régionale des pensions de Colmar, après avoir relevé que M. A avait été détenu au camp de Tambow, ne pouvait dès lors, sans erreur de droit, estimer qu'en raison du caractère récent du diagnostic de sa colite, M. A n'établissait pas la preuve de l'imputabilité de l'infirmité en résultant ; que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Considérant, en premier lieu, qu'il résulte de l'instruction, et notamment du rapport d'expertise établi à la demande du juge de première instance et non formellement contredit pas les documents produits en appel par le ministre de la défense, que M. A est atteint de l'affection dite  colite vraie  entraînant une invalidité dont le pourcentage doit être évalué à 20 % ; qu'ainsi qu'il a été dit ci-dessus, M. A bénéficie pour cette affection d'une présomption d'imputabilité au service ; que, par suite, le ministre de la défense n'est pas fondé à se plaindre de ce que, par le jugement attaqué, le tribunal départemental des pensions du Bas-Rhin a fixé à 20 % le pourcentage d'invalidité ouvrant droit à pension qu'entraîne l'affection de  colite vraie  ; <br/>
<br/>
              Considérant, en second lieu, qu'aux termes de l'article L. 14 du code des pensions militaires d'invalidité et des victimes de la guerre :  Dans le cas d'infirmités multiples dont aucune n'entraîne l'invalidité absolue, le taux d'invalidité est considéré intégralement pour l'infirmité la plus grave et pour chacune des infirmités supplémentaires, proportionnellement à la validité restante (...)/ Toutefois, quand l'infirmité principale est considérée comme entraînant une invalidité d'au moins 20 %, les degrés d'invalidité de chacune des infirmités supplémentaires sont élevés d'une, de deux ou de trois catégories, soit de 5, 10, 15 %, et ainsi de suite, suivant qu'elles occupent les deuxième, troisième, quatrième rangs dans la série décroissante de leur gravité (...)  ; que par application de ces dispositions, il résulte, tant du pourcentage d'invalidité qui doit être retenu pour l'affection de  colite vraie  que des termes non contestés de l'arrêté de pension du 9 février 2004, que le taux global de la pension de M. A doit être déterminé en retenant les infirmités et taux suivants : 1°) syndrome asthénique : 20 %, 2°) rhumatisme arthrosique du rachis cervical et lombaire : 20 % + 5 %, 3°) : colite vraie : 20 % + 10 %, 4°) larmoiement de l'oeil gauche : 10 % + 15 % ; que la prise en compte successive de ces infirmités aboutit à un taux de validité restant de 31,5 % arrondi à 30 % ; que, par suite, le taux d'invalidité global de M. A doit être fixé à 70 % ; que le ministre de la défense est ainsi fondé à soutenir que c'est à tort que, par le jugement contesté, le tribunal départemental des pensions du Bas-Rhin a jugé que M. A avait droit à une pension au taux de 75 % ; <br/>
<br/>
              Considérant, enfin, que M. A a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; que l'Etat devant être regardé, dans la présente instance, comme la partie perdante pour l'essentiel, il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Gatineau, Fattaccini, avocat de M. A, renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de ce dernier le versement à cette SCP de la somme de 3 000 euros demandée à ce titre ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour régionale des pensions de Colmar du 11 mars 2008 est annulé.<br/>
Article 2 : Le taux d'invalidité, ouvrant droit à pension, résultant de l'affection répertoriée sous le n° 8 dans l'arrêté de pension du 9 février 2004 de M. A et constituée par une  colite vraie  est fixé à 20 % à compter de la date de la demande de révision.<br/>
Article 3 : Le taux global d'invalidité à retenir pour la pension de M. A est fixé à  70 % à compter de cette même date.<br/>
Article 3 : Le jugement du 12 décembre 2005 du tribunal départemental des pensions du Bas-Rhin, rectifié par son jugement du 11 septembre 2006, est réformé en ce qu'il est contraire à la présente décision.<br/>
Article 4 : L'Etat versera à la SCP Gatineau, Fattaccini, avocat de M. A, la somme de 3 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 5 : La présente décision sera notifiée à M. Jean A et au ministre de la défense.<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">48-01-02-03-02 PENSIONS. PENSIONS MILITAIRES D'INVALIDITÉ ET DES VICTIMES DE GUERRE. CONDITIONS D'OCTROI D'UNE PENSION. IMPUTABILITÉ. LIEUX DE CAPTIVITÉ OU D'INTERNEMENT. - ANCIENS CAPTIFS DU CAMP DE TAMBOW - PRÉSOMPTION D'IMPUTABILITÉ AU SERVICE DES COLITES VRAIES - EXISTENCE - DÉLAI DE CONSTATATION - ABSENCE.
</SCT>
<ANA ID="9A"> 48-01-02-03-02 L'article L. 3 du code des pensions militaires d'invalidité et des victimes de la guerre dispose que la présomption d'imputabilité au service d'une infirmité résultant d'une blessure ou d'une maladie ne bénéficie aux prisonniers de guerre et internés à l'étranger qu'à la condition que ces blessures ou maladies aient été régulièrement constatées dans certains délais qu'il fixe. Toutefois, les dispositions du guide-barème pour la classification des invalidités, annexé à l'article D. 2 du même code par le décret n° 73-74 du 18 janvier 1973, ont prévu pour certaines affections des délais de constatation supérieurs. Le décret n° 81-315 du 6 avril 1981, auquel la loi n° 83-1109 du 21 décembre 1983 a conféré valeur législative, a même supprimé tout délai de constatation pour certaines infirmités et maladies contractées au cours de la captivité subie dans certains camps ou lieux de détention. Il en va notamment ainsi de l'affection dite « colite vraie » pour les anciens captifs du camp de Tambow. Il en résulte que, par dérogation aux dispositions de l'article L. 3 du code des pensions militaires d'invalidité et des victimes de la guerre, la présomption d'imputabilité au service des « colites vraies » bénéficie aux anciens captifs du camp de Tambow sans que puisse leur être opposé un délai de constatation de cette affection.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
