<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041569453</ID>
<ANCIEN_ID>JG_L_2020_02_000000434356</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/56/94/CETATEXT000041569453.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 12/02/2020, 434356, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-02-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434356</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Louise Cadin</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2020:434356.20200212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et deux nouveaux mémoires, enregistrés les 6 septembre, 23 octobre et 28 novembre 2019 au secrétariat du contentieux du Conseil d'Etat, la société Redcore demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 3 juillet 2019 de la ministre des armées relatif au classement de certaines armes et munitions en application de l'article R. 2331-2 du code de la défense, ainsi que le rejet de son recours gracieux ; <br/>
<br/>
              2°) d'annuler pour excès de pouvoir la décision implicite par laquelle le ministre de l'intérieur a rejeté sa demande tendant à ce qu'il classe le lanceur de balles de défense " KANN 44 CLR " et la munition " MAT 44/83 SP " en catégorie B3, ainsi que le rejet de son recours gracieux ;<br/>
<br/>
              3°) d'enjoindre au ministre de l'intérieur de procéder à un classement en catégorie B3 ou, à défaut, de réexaminer sa demande de classement dans un délai de quinze jours, sous astreinte de 500 euros par jours de retard ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la défense ;<br/>
              - le code de la sécurité intérieure ;<br/>
              - l'arrêté du 1er août 2017 relatif au classement des matériels de guerre de la catégorie A2 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... A..., auditrice,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 27 janvier 2020, présentée par la société Redcore ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. L'article L. 311-2 du code de la sécurité intérieure prévoit le classement des matériels de guerre, armes, munitions et leurs éléments dans les catégories suivantes : " (...) 1° Catégorie A : matériels de guerre et armes interdits à l'acquisition et à la détention, sous réserve des dispositions des articles L. 312-1 à L. 312-4-3 du code de la sécurité intérieure. / Cette catégorie comprend : - A1 : les armes et éléments d'armes interdits à l'acquisition et à la détention ; - A2 : les armes relevant des matériels de guerre, les matériels destinés à porter ou à utiliser au combat les armes à feu, les matériels de protection contre les gaz de combat ; / 2° Catégorie B : armes soumises à autorisation pour l'acquisition et la détention ; / (...) Un décret en Conseil d'Etat détermine les matériels de guerre, armes, munitions, éléments, accessoires et opérations industrielles compris dans chacune de ces catégories ainsi que les conditions de leur acquisition et de leur détention. (...) / En vue de préserver la sécurité et l'ordre publics, le classement prévu aux 1° à 4° est fondé sur la dangerosité des matériels de guerre et des armes (...) ". <br/>
<br/>
              2. En vertu du I de l'article R.311-2 du code de la sécurité intérieure, relèvent de la catégorie A2 : " (...) 4° [les] canons, obusiers, mortiers, lance-roquettes et lance-grenades, de tous calibres, lance-projectiles et systèmes de projection spécifiquement destinés à l'usage militaire ou au maintien de l'ordre, ainsi que leurs tourelles, affûts, bouches à feu, tubes de lancement, lanceurs à munition intégrée, culasses, traîneaux, freins et récupérateurs ( ...) " ainsi que " (...) 5° [les] munitions et éléments de munitions pour les armes énumérées au 4° (...) ". En vertu du II du même article, relèvent de la catégorie B3, notamment, les " armes à feu fabriquées pour tirer une balle ou plusieurs projectiles non métalliques et munitions classées dans cette catégorie par arrêté conjoint du ministre de l'intérieur et des ministres chargés des douanes et de l'industrie ". <br/>
<br/>
              3. Aux termes de l'article R.311-3 du code de la sécurité intérieure : " Les mesures de classement des armes dans les catégories définies à l'article R.311-2, autres que celles prévues par des arrêtés interministériels, sont prises par le ministre de l'intérieur, à l'exclusion de celles des matériels de guerre de la catégorie A2, prises par le ministre de la défense ". Aux termes du deuxième alinéa de l'article R. 311-3-1 du même code : " S'il s'avère que le matériel relève de la compétence du ministre de la défense, au titre de l'article R. 2332-1 du code de la défense, le ministre de l'intérieur lui transmet le dossier de classement dans les meilleurs délais ".<br/>
<br/>
              4. Sur le fondement de ces dispositions, la société Redcore a demandé au ministre de l'intérieur de classer le lanceur de balles de défense KANN 44 CLR et sa munition MAT 44/83 SP dans la catégorie B3. La société demande l'annulation pour excès de pouvoir du refus opposé par le ministre de l'intérieur ainsi que l'annulation de l'arrêté du 3 juillet 2019 par lequel la ministre des armées a classé cette arme et cette munition en catégorie A2. <br/>
<br/>
              5. L'acte par lequel le ministre de l'intérieur décide, en application des dispositions citées ci-dessus de l'article R. 311-3-1 du code de la sécurité intérieure, de transmettre au ministre de la défense le dossier de classement d'une arme ou d'une munition qu'il estime relever de la catégorie A2 étant insusceptible de recours, les conclusions par lesquelles la société Redcore demande l'annulation du refus de classer l'arme en cause en catégorie B3 doivent être regardées comme dirigées contre l'arrêté du 3 juillet 2019 en ce que, classant le lanceur de balles de défense KANN 44 CLR et la munition MAT 44/83 SP en catégorie A2, il rejette implicitement sa demande de classement en catégorie B3.<br/>
<br/>
              Sur la légalité externe de l'arrêté du 3 juillet 2019 :<br/>
<br/>
              6. L'article R. 2331-2 du code de la défense dispose que : " Les mesures de classement des matériels de guerre de la catégorie A2 sont prises par le ministre de la défense. Toute question relative au classement des matériels mentionnés au premier alinéa est soumise à une expertise du ministre de la défense, selon des modalités définies par arrêté du ministre de la défense. Il précise si le matériel en question relève de la catégorie A2 et notifie sa décision au demandeur (...) ". L'arrêté du 1er août 2017 relatif au classement des matériels de guerre de la catégorie A2, pris pour l'application de ces dispositions, prévoit que le ministre de la défense peut, avant de prononcer le classement d'un matériel dans la catégorie A2, solliciter l'avis d'une commission technique.<br/>
<br/>
              7. Contrairement à ce que soutient la société requérante, la circonstance que, lors de la réunion du 13 juin 2019 au cours de laquelle la commission technique a rendu son avis sur le classement litigieux, les représentants de la ministre des armées aient été absents, n'est pas de nature à entacher d'irrégularité la consultation de cette commission technique. Il en va de même de la circonstance que des représentants du ministre de l'intérieur auraient, ainsi que les y autorisaient les dispositions de l'article 5 de l'arrêté du 1er août 2017, assisté à la séance de la commission en qualité d'experts, dès lors qu'il ne ressort pas des pièces du dossier qu'ils auraient pris part à l'adoption de l'avis. <br/>
<br/>
              Sur la légalité interne de l'arrêté du 3 juillet 2019 :<br/>
<br/>
              En ce qui concerne la légalité du classement :<br/>
<br/>
              8. En premier lieu, il ne ressort pas des pièces du dossier que la ministre des armées se serait prononcée au vu d'un dossier comportant des inexactitudes quant aux performances du lanceur de balles de défense KANN 44 CLR ou à ses caractéristiques comparées aux modèles d'autres fabricants. Il n'est pas davantage établi, contrairement à ce qui est soutenu, que ce dossier aurait comporté des résultats d'essais balistiques réalisés dans des conditions inadaptées.<br/>
<br/>
              9. En deuxième lieu, il résulte des dispositions citées aux points 1 et 2 que le classement qu'elles prévoient est fondé sur la dangerosité des matériels de guerre, armes et munitions et peut légalement, à ce titre, se fonder non seulement sur la puissance de l'arme mais également sur les usages auxquelles celle-ci a vocation à servir. <br/>
<br/>
              10. Il ressort des pièces du dossier que, compte tenu de ses caractéristiques et de ses performances, notamment de sa puissance, ainsi que le montrent les valeurs énergétiques mesurées lors des tests de tir du lanceur KANN 44 CLR et de sa munition MAT 44/83SP, du fait que le lanceur est à canon rayé et de la distance potentielle de tir, l'arme doit être regardée comme spécifiquement destinée au maintien de l'ordre. En tenant compte de la puissance et la précision du lanceur KANN 44 CLR et sa munition MAT 44/83 SP ainsi que des conditions dans lesquelles ces matériels ont vocation à être utilisés, pour estimer qu'ils relèvent de la catégorie A2, la ministre des armées n'a pas méconnu les critères qu'il lui revenait d'appliquer et a fait une exacte application des dispositions de l'article R. 311-2 du code de la sécurité intérieure.<br/>
<br/>
              11. En troisième lieu, les dispositions qui précisent, en application de l'article L. 211-9 du code de sécurité intérieure, les conditions dans lesquelles les représentants de la force publique appelés en vue de dissiper un attroupement peuvent faire directement usage de la force, sont par elles-mêmes sans incidence sur le classement des armes qu'ils sont, dans ces circonstances, autorisés à employer. Par suite, la société requérante ne saurait utilement soutenir que la faculté donnée aux forces de l'ordre, par l'article D. 211-19 de ce code, d'utiliser notamment, lorsqu'ils sont amenés à dissiper des attroupements, des lanceurs de balles de défense de 44 millimètres ainsi que des munitions classées en catégorie B3, impose le classement dans cette dernière catégorie de tous les lanceurs de balles de défense d'un calibre de 44 millimètres et, notamment, du lanceur en litige.<br/>
<br/>
              12. Enfin, il ressort des pièces du dossier, notamment des courriers adressés par la société Redcore au service central des armes, que l'arme et la munition qu'elle commercialise et dont elle attaque le classement sont dénommées par elle " KANN 44 CLR " et " MAT 44/83 SP ". Si l'arrêté litigieux indique qu'il procède au classement du " lanceur de balle de défense KANN 44 de la société Redcore " et de " la munition MAT 44 de la société Redcore ", cette formulation qui s'écarte de la dénomination retenue par la société n'est pas de nature à créer un doute sur la portée du classement en litige et n'entache pas l'arrêté attaqué d'illégalité. <br/>
<br/>
              13. Il résulte de tout ce qui précède que la société Redcore n'est pas fondée à demander l'annulation pour excès de pouvoir de l'arrêté du 3 juillet 2019 classant le lanceur de balles de défense KANN 44 CLR et sa munition MAT 44/83 SP en catégorie A2 et rejetant, par suite, implicitement, sa demande de classement en catégorie B3. Pour les mêmes motifs, elle n'est pas fondée à demander l'annulation du rejet de son recours gracieux dirigé contre cette décision.<br/>
<br/>
              14. Sa requête doit, par suite, être rejetée, y compris par voie de conséquence ses conclusions à fin d'injonction ainsi que celles présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
         Article 1er : La requête de la société Redcore est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société Redcore, à la ministre des armées et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
