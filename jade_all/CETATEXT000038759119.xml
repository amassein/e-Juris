<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038759119</ID>
<ANCIEN_ID>JG_L_2019_07_000000430060</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/75/91/CETATEXT000038759119.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 12/07/2019, 430060, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430060</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Thomas Pez-Lavergne</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:430060.20190712</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...C...B...a demandé au juge des référés du tribunal administratif de Paris, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de la décision implicite par laquelle la ministre des armées a refusé de lui accorder la protection fonctionnelle, et de lui enjoindre de réexaminer sa demande dans un délai de huit jours à compter de la notification de l'ordonnance, sous astreinte de 500 euros par jour de retard.<br/>
<br/>
              Par une ordonnance n° 194075 du 19 mars 2019, le juge des référés du tribunal administratif de Paris a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 avril et 9 mai 2019 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat au profit de la SCP Lyon-Caen et Thiriez, avocat de M.B..., la somme de 5 000 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 91-647 du 31 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Pez-Lavergne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M.B....<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que la société B...Hewad Logestic et Construction Company, dont M.B..., ressortissant afghan, est le président, a effectué, entre le 6 septembre 2009 et le 10 juillet 2013, à Kaboul et dans les environs, de nombreux travaux d'infrastructures que l'économat des armées lui avaient commandés au profit des forces armées françaises stationnées en Afghanistan. Les autorités françaises ont annoncé au mois de mai 2012 le retrait des forces françaises d'Afghanistan à partir du mois de juillet. M.B..., qui vit à Kaboul avec son épouse, a sollicité en mars 2018, de la ministre des armées qu'elle lui accorde le bénéfice de la protection fonctionnelle pour lui-même et son épouse compte tenu des menaces dont ils feraient l'objet en Afghanistan à raison de sa collaboration avec les forces armées françaises. Cette demande a fait l'objet d'une décision implicite de rejet, confirmée par une décision expresse du 22 janvier 2019, dont M. B...a demandé au juge des référés du tribunal administratif de Paris, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension assortie d'une injonction tendant au réexamen de sa demande par la ministre des armées. M. B...se pourvoit en cassation contre l'ordonnance du 19 mars 2019 par laquelle le juge des référés de ce tribunal a rejeté sa demande.<br/>
<br/>
              2. Aux termes du IV de l'article 11 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires : " La collectivité publique est tenue de protéger le fonctionnaire contre les atteintes volontaires à l'intégrité de la personne, les violences, (...) les menaces, les injures, les diffamations ou les outrages dont il pourrait être victime sans qu'une faute personnelle puisse lui être imputée. Elle est tenue de réparer, le cas échéant, le préjudice qui en est résulté ".<br/>
<br/>
              3. Il résulte d'un principe général du droit que, lorsqu'un agent public est mis en cause par un tiers à raison de ses fonctions, il incombe à la collectivité dont il dépend de le couvrir des condamnations civiles prononcées contre lui, dans la mesure où une faute personnelle détachable du service ne lui est pas imputable, de lui accorder sa protection dans le cas où il fait l'objet de poursuites pénales, sauf s'il a commis une faute personnelle, et, à moins qu'un motif d'intérêt général ne s'y oppose, de le protéger contre les menaces, violences, voies de fait, injures, diffamations ou outrages dont il est l'objet. Ce principe général du droit s'étend aux agents non-titulaires de l'Etat recrutés à l'étranger, alors même que leur contrat est soumis au droit local. La juridiction administrative est compétente pour connaître des recours contre les décisions des autorités de l'Etat refusant aux intéressés le bénéfice de cette protection.<br/>
<br/>
              4. En premier lieu, il ressort des pièces du dossier soumis au juge des référés qu'en estimant qu'à supposer même que la réalité des menaces pesant sur le requérant soit établie, M.B..., qui n'était pas un agent non-titulaire de l'Etat, ne pouvait pas davantage être regardé comme un collaborateur occasionnel du service public au seul motif qu'il était l'un des dirigeants d'une entreprise ayant effectué des travaux d'infrastructures au profit des forces françaises stationnées en Afghanistan, et en en déduisant que n'était pas de nature à créer un doute sérieux sur la légalité de la décision attaquée de refus de protection fonctionnelle, le moyen tiré de la violation de l'article 11 de la loi du 13 juillet 1983 ou du principe général du droit dont il s'inspire, rappelé au point 3, le juge des référés du tribunal administratif de Paris n'a pas commis d'erreur de droit, ni dénaturé les pièces du dossier qui lui était soumis.<br/>
<br/>
              5. En second lieu, en jugeant que le moyen tiré d'un défaut d'examen sérieux de la situation individuelle de l'intéressé n'était pas de nature à créer un doute sérieux sur la légalité de la décision en litige, le juge des référés s'est livré, au regard des pièces qui lui étaient soumises, et notamment des éléments que la ministre des armées avait produits en défense, à une appréciation souveraine des faits exempte de dénaturation.<br/>
<br/>
              6. Il résulte de tout ce qui précède que M. B...n'est pas fondé à demander l'annulation de l'ordonnance qu'il attaque. Ses conclusions présentées sur le fondement des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ne peuvent, en conséquence, qu'être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. A...C...B...et à la ministre des armées.<br/>
Copie en sera adressée au ministre de l'intérieur et au ministre de l'Europe et des affaires étrangères.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
