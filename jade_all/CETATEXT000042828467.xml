<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042828467</ID>
<ANCIEN_ID>JG_L_2020_12_000000428115</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/82/84/CETATEXT000042828467.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 29/12/2020, 428115, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428115</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SARL DIDIER, PINET ; SCP SEVAUX, MATHONNET ; OCCHIPINTI ; SCP GATINEAU, FATTACCINI, REBEYROL</AVOCATS>
<RAPPORTEUR>Mme Pearl Nguyên Duy</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:428115.20201229</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes : <br/>
<br/>
              1° Sous le n° 428115, par une décision du 27 mars 2020, le Conseil d'Etat statuant au contentieux a prononcé l'admission des conclusions du pourvoi de l'Assistance publique - Hôpitaux de Paris (AP-HP) dirigées contre l'arrêt n° 14PA03561 du 6 mai 2015 de la cour administrative d'appel de Paris en tant que, sur appel de Mme A..., il se prononce sur les conclusions à fin d'indemnisation présentées par l'organisme de prévoyance APGIS.<br/>
<br/>
<br/>
              2° Sous le n° 428149, par une décision du 27 mars 2020, le Conseil d'Etat statuant au contentieux a prononcé l'admission des conclusions de Mme A... dirigées contre l'arrêt n° 14PA03561 du 6 mai 2015 de la cour administrative d'appel de Paris en tant qu'il se prononce sur les préjudices invoqués par Mme A... aux titres des pertes de primes et de rémunération variable et sur son préjudice de retraite.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ; <br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pearl Nguyên Duy, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SARL Didier, Pinet, avocat de l'Assistance Publique - Hôpitaux de Paris, à Me Occhipinti, avocat de Mme A... et à la SCP Gatineau, Fattaccini, Rebeyrol, avocat de l'organisme de prévoyance APGIS.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme A... a subi, à l'hôpital Saint Louis, une biopsie chirurgicale ayant entraîné une atteinte du sterno-cléido mastoïdien et du trapèze droit. Elle a demandé au tribunal administratif de Paris la condamnation de l'Assistance publique - hôpitaux de Paris (AP-HP) à l'indemniser des préjudices qu'elle avait subis du fait de la faute médicale commise, selon elle, au cours de cet examen. Par un jugement du 17 juin 2014, le tribunal administratif a rejeté sa demande. Sur appel de l'intéressée, la cour administrative d'appel de Paris a, par un arrêt avant-dire droit du 6 mai 2015, annulé ce jugement et, après avoir évoqué l'affaire, a ordonné la désignation d'un expert. Par l'arrêt attaqué du 18 décembre 2018, la cour administrative d'appel a condamné l'AP-HP à verser à Mme A... la somme de 134 190,15 euros et à l'organisme de prévoyance APGIS la somme de 304 559,17 euros. L'AP-HP demande l'annulation de cet arrêt en tant qu'il statue sur les conclusions de l'organisme de prévoyance APGIS. Par un pourvoi qu'il y a lieu de joindre pour y statuer par une même décision, Mme A... demande l'annulation du même arrêt en tant qu'il se prononce sur ses préjudices liés à la perte de ses primes et de sa rémunération variable et sur son préjudice de retraite.<br/>
<br/>
              Sur le pourvoi de l'AP-HP : <br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que, devant la cour administrative d'appel de Paris, l'AP-HP avait soulevé une fin de non-recevoir tirée de ce que les conclusions à fin d'indemnisation présentées par la société SAPIA, aux droits de laquelle est venu l'organisme de prévoyance APGIS, étaient présentées pour la première fois en appel et étaient, par suite, irrecevables. <br/>
<br/>
              3. En faisant droit à ces conclusions sans répondre à la fin de non-recevoir soulevée par l'AP-HP la cour administrative d'appel a entaché son arrêt d'une insuffisance de motivation, alors même que, ayant annulé le jugement du 17 juin 2014, elle était appelée à statuer sur le litige par la voie de l'évocation. L'AP-HP est, par suite, fondée à demander l'annulation de l'arrêt attaqué en tant qu'il la condamne à verser une somme à l'organisme de prévoyance APGIS. <br/>
<br/>
              Sur le pourvoi de Mme A... :<br/>
<br/>
              4. En premier lieu, en jugeant, pour rejeter les conclusions de Mme A... tendant à l'indemnisation de la perte de sa part variable de rémunération et de ses primes de participation et d'intéressement, que leur caractère d'éléments variables de rémunération faisait obstacle à ce que leur perte soit indemnisée au titre des pertes de gains professionnels, sans rechercher si le versement de tout ou partie de ces éléments de rémunération aurait présenté, en l'absence de l'arrêt de travail causé par la faute de l'établissement de santé, un caractère suffisamment certain, la cour a entaché son arrêt d'une erreur de droit. Mme A... est, par suite, fondée à demander l'annulation de l'arrêt attaqué en tant qu'il rejette ses conclusions tendant à l'indemnisation de sa perte de part variable de rémunération et de primes de participation et d'intéressement.<br/>
<br/>
              5. En second lieu, il résulte des termes mêmes de l'arrêt attaqué que, pour juger que le préjudice de retraite de Mme A... n'était pas établi, la cour s'est fondée sur ce qu'en raison des indemnités versées par son employeur et son organisme de prévoyance, l'intéressée n'avait pas subi de diminution de salaire, à l'exception de la perte de sa part variable de rémunération et de ses primes de participation et d'intéressement. Par suite, il résulte de ce qui a été dit ci-dessus que Mme A... est également fondée à demander l'annulation de l'arrêt attaqué en tant qu'il rejette ses conclusions tendant à l'indemnisation de son préjudice de retraite.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'AH-HP les sommes que demande, à ce titre, l'organisme de prévoyance APGIS et que soit mises à la charge de Mme A... les sommes que demande, au même titre, l'AP-HP.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'organisme de prévoyance APGIS une somme de 3 000 euros à verser l'AP-HP au titre de l'article L. 761-1 du code de justice administrative et de mettre à la charge de l'AP-HP une somme de 3 000 euros à verser, au même titre, à Mme A....<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 18 décembre 2018 de la cour administrative d'appel de Paris est annulé en tant qu'il statue sur les conclusions de Mme A... tendant à l'indemnisation des préjudices liés, d'une part aux pertes de part variable de rémunération et de primes de participation et d'intéressement et, d'autre part, de son préjudice de retraite, ainsi qu'en tant qu'il condamne l'AP-HP à verser une somme à l'organisme de prévoyance APGIS.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Paris.<br/>
Article 3 : L'organisme de prévoyance APGIS versera à l'AP-HP la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par l'organisme de prévoyance APGIS au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : L'Assistance publique-Hôpitaux de Paris versera à Mme A... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 6 : Les conclusions présentées par l'AP-HP au titre de l'article L. 761-1 du code de justice administrative, sous le n° 428149, sont rejetées.<br/>
Article 7 : La présente décision sera notifiée à l'Assistance publique - hôpitaux de Paris, à Mme B... A..., à l'organisme de prévoyance APGIS et à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
