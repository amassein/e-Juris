<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041688025</ID>
<ANCIEN_ID>JG_L_2020_03_000000426633</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/68/80/CETATEXT000041688025.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 04/03/2020, 426633, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-03-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426633</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Bénédicte Fauvarque-Cosson</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:426633.20200304</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 26 décembre 2018 au secrétariat du contentieux du Conseil d'Etat, la Ligue nationale pour la liberté des vaccinations demande au Conseil d'Etat d'annuler pour excès de pouvoir les dispositions du 4° de l'article 3 de l'arrêté de la ministre des solidarités et de la santé du 5 novembre 2018 relatif à la formation des assistants maternels et fixant le modèle de convention de stage prévu à l'article D. 421-44 du code de l'action sociale et des familles.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de la santé publique ;<br/>
              - le décret n° 2018-903 du 23 octobre 2018 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Bénédicte Fauvarque-Cosson, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. D'une part, l'article L. 421-14 du code de l'action sociale et des familles prévoit que : " Tout assistant maternel agréé doit suivre une formation dont les modalités de mise en oeuvre par le département, la durée, le contenu et les conditions de validation sont définis par décret. (...) ". Aux termes du I de l'article D. 421-44 du même code, dans sa rédaction issue du décret du 23 octobre 2018 relatif à la formation et au renouvellement d'agrément des assistants maternels, entré en vigueur le 1er janvier 2019 : " La formation de l'assistant maternel agréé prévue à l'article L. 421-14 est organisée et financée par le président du conseil départemental pour une durée totale d'au moins cent vingt heures, le cas échéant complétée de périodes de formation en milieu professionnel dans des conditions définies par arrêté du ministre chargé de la famille ".<br/>
<br/>
              2. D'autre part, aux termes du premier alinéa de l'article L. 3111-1 du code de la santé publique : " La politique de vaccination est élaborée par le ministre chargé de la santé qui fixe les conditions d'immunisation, énonce les recommandations nécessaires et rend public le calendrier des vaccinations après avis de la Haute Autorité de santé ". Aux termes de l'article L. 3111-4 du même code : " Une personne qui, dans un établissement ou organisme public ou privé de prévention de soins ou hébergeant des personnes âgées, exerce une activité professionnelle l'exposant ou exposant les personnes dont elle est chargée à des risques de contamination doit être immunisée contre l'hépatite B, la diphtérie, le tétanos, la poliomyélite et la grippe. / (...) / Un arrêté des ministres chargés de la santé et du travail, pris après avis de la Haute Autorité de santé, détermine les catégories d'établissements et organismes concernés (...) ".<br/>
<br/>
              3. Par un arrêté du 5 novembre 2018 relatif à la formation des assistants maternels et fixant le modèle de convention de stage prévu à l'article D. 421-44 du code de l'action sociale et des familles, entrant en vigueur le 1er janvier 2019, la ministre des solidarités et de la santé a, notamment, déterminé les conditions d'organisation de la période de formation en milieu professionnel prévue par les dispositions de cet article. Le 4° de l'article 3 de l'arrêté prévoit qu'une convention de stage est, préalablement au commencement de la période de formation en milieu professionnel, établie par la personne assurant la formation et signée par le stagiaire, la personne assurant la formation et la structure ou l'assistant maternel tuteur recevant le stagiaire. La Ligue nationale pour la liberté des vaccinations demande l'annulation pour excès de pouvoir de ces dispositions en tant qu'elles prévoient qu'" est annexé à cette convention un certificat médical attestant que le futur stagiaire est à jour de ses vaccinations obligatoires et recommandées pour les professionnels de la petite enfance selon le calendrier des vaccinations prévu à l'article L. 3111-1 du code de la santé publique ".<br/>
<br/>
              4. En premier lieu, il résulte des dispositions du 1° de l'article 3 de l'arrêté attaqué que la période de formation en milieu professionnel peut se dérouler dans un établissement d'accueil de jeunes enfants, une pouponnière à caractère social ou un centre maternel, au domicile privé d'un assistant maternel agréé, ou encore dans une maison d'assistants maternels ou un relais d'assistants maternels. En disposant qu'est annexé à la convention de stage un certificat médical attestant que le futur stagiaire est à jour de ses vaccinations obligatoires pour les professionnels de la petite enfance, la ministre des solidarités et de la santé a seulement imposé que soit vérifié, avant le début de la période de formation en milieu professionnel, le respect des obligations vaccinales applicables en fonction du lieu de stage choisi. En particulier, contrairement à ce que soutient l'association requérante, ces dispositions n'ont ni pour objet ni pour effet d'étendre à l'ensemble des assistants maternels les obligations vaccinales applicables en cas d'exercice professionnel dans certains établissements et organismes, en vertu de l'arrêté des ministres chargés de la santé et du travail prévu par l'article L. 3111-4 du code de la santé publique. <br/>
<br/>
              5. En second lieu, en revanche, la ministre des solidarités et de la santé ne tenait ni de l'article L. 421-14 du code de l'action sociale et des familles, ni de l'article L. 3111-1 du code de la santé publique, ni d'aucune autre disposition législative une habilitation lui conférant le pouvoir de soumettre les assistants maternels suivant une période de formation en milieu professionnel à des vaccinations non obligatoires. Elle a, par suite, méconnu sa compétence en exigeant du futur stagiaire qu'il produise, préalablement au commencement de sa période de formation en milieu professionnel, un certificat médical attestant qu'il est à jour de ses vaccinations recommandées, sans qu'ait d'incidence le fait que l'organisation et le financement de la formation des assistants maternels agréés relèvent des départements et que, dans certains départements, la période de formation en milieu professionnel puisse ne pas revêtir de caractère obligatoire. <br/>
<br/>
              6. Il résulte de tout ce qui précède que la Ligue nationale pour la liberté des vaccinations est fondée à demander l'annulation de l'arrêté attaqué en tant seulement qu'il impose au futur stagiaire, préalablement au commencement de sa période de formation en milieu professionnel, de produire un certificat médical attestant qu'il est à jour de ses vaccinations recommandées. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Les mots " et recommandées " au 4° de l'article 3 de l'arrêté du 5 novembre 2018 relatif à la formation des assistants maternels et fixant le modèle de convention de stage prévu à l'article D. 421-44 du code de l'action sociale et des familles sont annulés.<br/>
Article 2 : Le surplus des conclusions de la requête de la Ligue nationale pour la liberté des vaccinations est rejeté.<br/>
Article 3 : La présente décision sera notifiée à Ligue nationale pour la liberté des vaccinations et au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
