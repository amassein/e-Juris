<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044254917</ID>
<ANCIEN_ID>JG_L_2021_10_000000456973</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/25/49/CETATEXT000044254917.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 22/10/2021, 456973, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>456973</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:456973.20211022</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 23 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, la société Wari Pay demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution de la décision du 15 juin 2021 par laquelle l'Autorité de contrôle prudentiel et de résolution (ACPR) lui a interdit d'exercer à titre temporaire son activité d'émission de monnaie électronique, ainsi que de la décision du 7 juillet 2021 rejetant son recours gracieux ;<br/>
<br/>
              2°) de suspendre l'exécution de la décision de publier cette mesure d'interdiction temporaire ;<br/>
<br/>
              3°) d'enjoindre à l'ACPR de publier la décision du juge des référés du Conseil d'Etat sur le site internet de l'Autorité, dans l'hypothèse où il serait fait droit à sa demande de suspension ; <br/>
<br/>
              4°) de mettre à la charge de l'Etat une somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - la condition d'urgence est satisfaite dès lors que, en premier lieu, son chiffre d'affaire est presque nul depuis l'exécution des décisions contestées prises peu après la fin d'une procédure de redressement judiciaire, en deuxième lieu, elle a été contrainte d'effectuer quatre licenciements en août dernier, soit la moitié de son effectif, en troisième lieu, les relations contractuelles avec son réseau de partenaires sont interrompues et, en dernier lieu, le préjudice d'image et de réputation qu'elle subit est grave et immédiat en ce que la mesure est portée à la connaissance du public depuis le 5 juillet 2021 ;<br/>
              - il existe un doute sérieux quant à la légalité des décisions contestées ; <br/>
              - la décision d'interdiction temporaire de ses activités est entachée d'une erreur de qualification juridique au regard des dispositions de l'article L. 526-32 du code monétaire et financier en ce que le " ticket premium " ne constitue pas une monnaie électronique et que son rôle à ce titre ne peut être regardé comme celui d'un émetteur de monnaie électronique et d'une erreur de droit en ce que les fonds du public ne sont pas collectés par elle mais par les buralistes qui constituent les points de vente des " tickets premium " ; <br/>
              - cette décision est entachée d'erreur de qualification juridique au regard de l'article L. 612-33 du code monétaire et financier dès lors que l'obligation d'assurance prévue par le 2° de l'article L. 526-32 du code monétaire et financier ne constituait pas une condition nécessaire à la poursuite de son activité et que les intérêts de sa clientèle n'étaient pas susceptibles d'être compromis par l'arrêt des relations contractuelles avec la société d'assurance chargée du cautionnement des fonds collectés  ;<br/>
              - la mesure de police administrative contestée n'est pas légalement justifiée par le risque invoqué par l'ACPR du fait de l'interruption volontaire d'activité décidée dès le 1er juillet 2021 et est disproportionnée dès lors que, d'une part, ses perspectives de redressement dépendent de la poursuite de son activité " ticket premium " auprès de son réseau de distributeurs et des sites marchands, d'autre part, il n'est pas établi qu'elle ne serait pas en mesure d'honorer les engagements souscrits par ses clients et, enfin, qu'il est possible d'obtenir une assurance classique venant garantir le montant de l'encours des coupons non utilisés par les clients ou à tout le moins, de prévoir un cantonnement bancaire pour sécuriser ces fonds ou encore une exemption au titre de l'article L. 525-5 du code monétaire et financier ; <br/>
              - la décision de publier l'interdiction temporaire sur le site de l'Autorité, prise sur le fondement du IV de l'article L. 612-2 du code monétaire et financier, est dépourvue de base légale et est disproportionnée en ce qu'elle n'est pas nécessaire à l'accomplissement des missions de l'ACPR faute pour l'obligation de souscription d'une caution d'être légalement justifiée, en ce qu'elle ne préserve pas les intérêts des clients ayant acquis les " tickets premium " par les effets de dissuasion et de perte de confiance qu'elle comporte et en ce que l'interruption volontaire d'activité a pour effet de supprimer tout risque à compter du 1er juillet 2021 ;<br/>
              - les décision attaquées sont entachées de détournement de pouvoir. <br/>
<br/>
              Par un mémoire en défense, enregistré le 12 octobre 2021, l'Autorité de contrôle prudentiel et de résolution conclut au rejet de la requête et à ce qu'une somme de 3 500 euros soit mise à la charge de la société Wari Pay au titre de l'article L. 761-1 du code de justice administrative. Elle soutient que la condition d'urgence n'est pas satisfaite et que les moyens soulevés ne sont pas propres à créer, en l'état de l'instruction, un doute sérieux quant à la légalité des décisions contestées. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive 2007/64/CE du Parlement européen et du Conseil du 13 novembre 2007 ;<br/>
              - la directive 2009/110/CE du Parlement européen et du Conseil du 16 septembre 2009 ;<br/>
              - le code des assurances ;<br/>
              - le code de commerce ;<br/>
              - le code monétaire et financier ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la société Wari Pay, et d'autre part, l'Autorité de contrôle prudentiel et de résolution ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 15 octobre 2021, à 10 heures : <br/>
<br/>
              - Me Thiriez, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la société Wari Pay ;<br/>
<br/>
              - Me Rocheteau, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'Autorité de contrôle prudentiel et de résolution ;<br/>
<br/>
              - les représentants de la société Wari Pay ; <br/>
<br/>
              - les représentantes de l'Autorité de contrôle prudentiel et de résolution ; <br/>
<br/>
              à l'issue de laquelle le juge des référés a clos l'instruction. <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              2. Aux termes du I de l'article L. 612-1 du code monétaire et financier, l'Autorité de contrôle prudentiel et de résolution est une autorité administrative indépendante chargée de veiller " à la préservation de la stabilité du système financier et à la protection des clients, assurés, adhérents et bénéficiaires des personnes soumises à son contrôle. / L'Autorité contrôle le respect par ces personnes des dispositions européennes qui leur sont directement applicables, des dispositions du code monétaire et financier ainsi que des dispositions réglementaires prévues pour son application (...) ". Aux termes du A du I de l'article L. 612-2 du même code : " Relèvent de la compétence de l'Autorité de contrôle prudentiel et de résolution : / (...) 8° Les établissements de monnaie électronique (...) ". <br/>
<br/>
              3. Pour l'accomplissement de ses missions, l'Autorité de contrôle prudentiel et de résolution (ACPR) dispose de pouvoirs de police administrative déterminés par les articles L. 612-30 à L. 612-37 du code monétaire et financier. En vertu de l'article L. 612-33 de ce code : " Lorsque la solvabilité ou la liquidité d'une personne soumise au contrôle de l'Autorité ou lorsque les intérêts de ses clients, assurés, adhérents ou bénéficiaires, sont compromis ou susceptibles de l'être (...), l'Autorité de contrôle prudentiel et de résolution prend les mesures conservatoires nécessaires. / Elle peut, à ce titre : / (...) 3° Limiter ou interdire temporairement l'exercice de certaines opérations ou activités par cette personne, y compris l'acceptation de primes ou dépôts (...) ". Par ailleurs, en application du IV de l'article L. 612-1 du même code, elle peut " porter à la connaissance du public toute information qu'elle estime nécessaire à l'accomplissement de ses missions, sans que lui soit opposable le secret professionnel mentionné à l'article L. 612-17 ". <br/>
<br/>
              4. Il ressort du dossier que la société Wari Pay est un établissement agréé en tant qu'émetteur de monnaie électronique qui relève à ce titre de la compétence de l'ACPR. A la suite d'un contrôle dont cette société a fait l'objet entre le 30 avril et le 11 mai 2021, elle a été informée, par courrier du 18 mai 2021, que le collège de supervision de l'ACPR avait considéré, lors de la séance du 12 mai, que la cessation, à compter du 11 juillet 2021, du cautionnement apporté par un assureur pour protéger les fonds de ses clients collectés en contrepartie de l'émission de monnaie électronique, dans un contexte où l'établissement affichait une situation financière fortement dégradée, paraissait susceptible de compromettre les intérêts de ses clients et envisageait, pour ce motif, de lui interdire temporairement d'exercer ses activités d'émission de monnaie électronique. Par une décision du 11 juin 2021, notifiée par courrier du 15 juin et prenant effet le 5 juillet suivant, le collège de cette autorité a, en application des dispositions de l'article L. 612-33 du code monétaire et financière, prononcé à l'encontre de la société Wary Pay l'interdiction temporaire d'activité en cause jusqu'à ce qu'il soit justifié de la mise en place d'un dispositif de protection des fonds de ses clients conforme aux règles prudentielles applicables. L'ACPR a également décidé, en application des dispositions du IV de l'article L. 612-1 du même code, de porter à la connaissance du public le contenu de cette mesure, à sa date de prise d'effet. La société Wary Pay demande, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de ces deux décisions, ainsi que de celle portant rejet de son recours gracieux.<br/>
<br/>
              Sur la décision d'interdiction temporaire d'activité :<br/>
<br/>
              5. Aux termes du I de l'article L. 315-1 du code monétaire et financier, pris pour la transposition de l'article 2 de la directive 2009/110/CE du 16 septembre 2009 concernant l'accès à l'activité des établissements de monnaie électronique et son exercice ainsi que la surveillance prudentielle de ces établissements : " La monnaie électronique est une valeur monétaire qui est stockée sous une forme électronique, y compris magnétique, représentant une créance sur l'émetteur, qui est émise contre la remise de fonds aux fins d'opérations de paiement définies à l'article L. 133-3 et qui est acceptée par une personne physique ou morale autre que l'émetteur de monnaie électronique ". Selon l'article L. 526-32 du même code, relatif aux dispositions prudentielles applicables au établissements de monnaie électronique, qui assure la transposition de l'article 9 de la directive 2007/64/CE du 13 novembre 2007 et de l'article 7 de la directive 2009/110/CE : " Les fonds collectés en contrepartie de l'émission de monnaie électronique sont protégés conformément à l'une des deux méthodes suivantes : / 1° Les fonds collectés ne sont en aucun cas confondus avec les fonds de personnes physiques ou morales autres que les détenteurs de monnaie électronique. / Les espèces collectées en contrepartie de l'émission de la monnaie électronique sont déposées sur un compte distinct auprès d'un établissement de crédit habilité à recevoir des fonds à vue du public, au plus tard à la fin du jour ouvrable, au sens du d de l'article L. 133-4, suivant leur collecte. (...) / 2° Les fonds collectés en contrepartie de l'émission de monnaie électronique sont couverts, dans le respect des délais mentionnés au 1° du présent article, par un contrat d'assurance ou une autre garantie comparable d'une entreprise d'assurances, d'une société de financement ou d'un établissement de crédit n'appartenant pas au même groupe, au sens de l'article L. 233-3 du code de commerce, selon des modalités définies par arrêté du ministre chargé de l'économie qui assurent ou garantissent les détenteurs de monnaie électronique contre la défaillance de l'établissement de monnaie électronique dans l'exécution de ses obligations financières. / Le présent article s'applique aux fonds collectés par les personnes mentionnées à l'article L. 525-8, les délais mentionnés au 1° du présent article commençant à courir à partir de la collecte par lesdites personnes. / Le présent article s'applique aux personnes mentionnées à l'article L. 525-8 ou aux établissements de monnaie électronique dès que le détenteur a remis les fonds à l'un d'entre eux en vue de la création de la monnaie électronique. / Les fonds collectés sont protégés tant que la monnaie électronique émise est en circulation ".<br/>
<br/>
              6. Si la société Wari Pay soutient que le ticket Premium ne constituerait pas de la monnaie électronique au sens du I de l'article L. 315-1 du code monétaire et financier et qu'elle n'était pas soumise aux dispositions prudentielles de l'article 526-32, il n'est pas contesté que cette société dispose d'un agrément en qualité d'établissement de monnaie électronique. Il résulte par ailleurs de l'instruction qu'elle propose à ses clients d'acheter, par tout moyen habituel, dans un point de vente de son réseau, essentiellement composé de buralistes, un support ou ticket qui comporte un code PIN qu'elle émet et auquel est associé une ligne de valeur monétaire qui peut être consommée en ligne auprès des sites marchands, notamment de jeux et paris en ligne, acceptant ce mode de paiement. Si les fonds des clients ne sont pas collectés directement par la société Wari Pay, ils le sont pour son compte et lui sont reversés par ses distributeurs dans le cadre d'un réseau de distribution mandaté à cet effet, tout comme il lui revient de rembourser aux sites marchands le montant des tickets consommés et aux utilisateurs ceux non consommés, qui valent créance sur elle. Dans ces conditions et eu égard aux caractéristiques du ticket de paiement litigieux, telles qu'elles ressortent des écritures et des échanges à l'audience, le moyen tiré de l'inapplicabilité de l'article L. 526-32 du code monétaire et financier et de l'absence de base légale de la mesure contestée n'est pas de nature, en l'état de l'instruction, de créer un doute sérieux quant à la légalité de la décision attaquée. Il en va de même du moyen tiré de ce que l'obligation d'assurance prévue par le 2° de cet article ne constituerait pas une condition de la poursuite de l'activité de la société Wari Pay, dès lors qu'il n'est pas contesté que l'autre méthode de protection des fonds collectés en contrepartie de l'émission de monnaie électronique, dite de cantonnement, prévue par le 1°, n'est pas mise en œuvre par cette société. <br/>
<br/>
              7. Si la société Wari Pay soutient par ailleurs que l'ACPR aurait fait une inexacte application des dispositions de l'article L. 612-33 du code monétaire et financier en estimant que les intérêts de ses clients étaient susceptibles d'être compromis, il résulte de l'instruction qu'à supposer même que l'encours à couvrir corresponde aux seuls tickets non encore consommés, sans être périmés, la société Wari Pay ne disposait plus, à compter du 11 juillet dernier, d'aucune solution prudentielle. Il ressort par ailleurs du rapport de contrôle définitif de l'ACPR que sa situation financière est fortement dégradée et que le jugement du tribunal de commerce de Nanterre du 8 juin 2021 mettant fin à la procédure de redressement judiciaire de cette société constate que l'apport financier ayant permis de clôturer cette procédure ne la met en mesure de financer son activité que pour les trois prochains mois et que la poursuite de son activité au-delà de cette période dépend notamment de la mise en place d'une garantie financière après le 11 juillet. Si elle a fait valoir à l'audience qu'elle remplirait les conditions pour bénéficier d'une exemption des règles applicables aux émetteurs de monnaie électronique en application des dispositions de l'article L. 525-5 du code monétaire et financier, il lui est loisible, si elle s'y croit fondée, de faire une demande en ce sens à l'ACPR. En l'état de l'instruction, eu égard au caractère substantiel de la règle prudentielle en cause, qui constitue une exigence essentielle découlant du droit de l'Union européenne, prise dans l'intérêt des clients des émetteurs de monnaie électronique, et au risque auquel ceux-ci sont exposés, alors même que la société Wari Pay avait fait savoir son intention d'interrompre volontairement son activité à compter du 1er juillet 2021, les moyens tirés de ce que la mesure prise, qui ne revêt qu'un caractère temporaire dans l'attente de la justification de la mise en place d'un dispositif de protection des fonds de ses clients conforme aux règles prudentielles applicables, ne serait pas légalement justifiée et serait disproportionnée n'apparaissent pas propres à créer un doute sérieux quant à sa légalité. Le moyen tiré d'un détournement de pouvoir n'est pas davantage de nature à faire naître un tel doute.<br/>
<br/>
              Sur la décision de publication :<br/>
<br/>
              8. Par les dispositions, citées au point 3, du IV de l'article L. 612-1 du code monétaire et financier, le législateur a entendu que puissent être portées à la connaissance du public les informations nécessaires à l'exercice de ses missions et ainsi satisfaire aux exigences d'intérêt général relatives à la transparence et au bon fonctionnement du système financier et à la protection des clients des personnes soumises à son contrôle. Il résulte de l'instruction et des éléments échangés au cours de l'audience publique que la publication de l'interdiction temporaire d'activité d'émetteur de monnaie électronique visait à assurer l'effectivité de la mesure conservatoire adoptée et d'informer les clients, actuels ou potentiels, de la société Wari Pay de ce que leurs fonds risquaient de ne plus être protégés à compter du 11 juillet 2021, date de résiliation du contrat d'assurance, et ce alors que cette société pouvait à tout moment revenir sur son annonce d'une interruption volontaire de son activité dans l'attente de la mise en place d'un nouveau dispositif de protection des fonds de ses clients. Les moyens tirés de ce que cette décision de publication ne serait pas légalement justifiée et serait disproportionnée n'apparaissent pas propres à créer, en l'état de l'instruction, un doute sérieux quant à sa légalité. Il en va de même du moyen tiré d'un détournement de pouvoir.<br/>
<br/>
              9. Il résulte de ce qui précède que la requête de la société Wari Pay doit être rejetée, y compris les conclusions à fin d'injonction et celles présentées au titre de l'article L. 761-1 du code de justice administrative. Il n'y a pas lieu de faire droit aux conclusions présentées au même titre par l'Autorité de contrôle prudentiel et de résolution.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de la société Wari Pay est rejetée.<br/>
Article 2 : Les conclusions présentées par l'Autorité de contrôle prudentiel et de résolution au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente ordonnance sera notifiée à la société Wari Pay et à l'Autorité de contrôle prudentiel et de résolution. <br/>
Fait à Paris, le 22 octobre 2021.<br/>
    Signé : Anne Courrèges<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
