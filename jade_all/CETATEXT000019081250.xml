<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000019081250</ID>
<ANCIEN_ID>JG_L_2008_06_000000301343</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/19/08/12/CETATEXT000019081250.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 27/06/2008, 301343, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2008-06-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>301343</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLEZ</AVOCATS>
<RAPPORTEUR>Mme Caroline  Martin</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olléon Laurent</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2008:301343.20080627</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 7 février et 7 mai 2007 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SOCIETE SEGAME, dont le siège est 57, rue de Seine à Paris (75006), représentée par son liquidateur amiable M. Gérard A ; la SOCIETE SEGAME demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler les articles 4 et 5 de l'arrêt du 24 novembre 2006 par lesquels la cour administrative d'appel de Paris, ne faisant que partiellement droit à sa requête tendant à l'annulation du jugement du 4 novembre 2004 du tribunal administratif de Paris rejetant sa demande tendant à la décharge des rappels de taxe sur les métaux précieux, les bijoux, les objets d'art, de collection et d'antiquité qui lui ont été réclamés au titre de la période du 1er janvier 1991 au 31 juillet 1993, des intérêts de retard correspondants et de l'amende fiscale prévue à l'article 1788 ter devenu article 1770 octies du code général des impôts et, d'autre part, à la décharge de ces droits, intérêts et amende, a rejeté le surplus de ses conclusions ;<br/>
<br/>
              2°) réglant l'affaire au fond, de prononcer la décharge des droits, intérêts et amende restant en litige ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 3 500 euros sur le fondement de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son premier protocole additionnel ;<br/>
<br/>
              Vu la première directive 67/227/CEE  du 11 avril 1967 et  la sixième directive 77/388/CEE  du 17 mai 1977 ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu la loi n° 76-660 du 19 juillet 1976 portant imposition des plus-values et création d'une taxe forfaitaire sur les métaux précieux, les bijoux, les objets d'art, de collection et d'antiquité ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Caroline Martin, Conseiller d'Etat, <br/>
<br/>
              - les observations de la SCP Boullez, avocat de la SOCIETE SEGAME, <br/>
<br/>
              - les conclusions de M. Laurent Olléon, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant que la SOCIETE SEGAME, qui gérait une galerie de tableaux, a fait l'objet d'une vérification de comptabilité portant sur la période du 1er janvier 1990 au 8 novembre 1993 ; qu'à l'issue de ce contrôle, l'administration a émis le 24 janvier 1995 un avis de mise en recouvrement portant sur des rappels de taxe sur les métaux précieux, les bijoux, les objets d'art, de collection et d'antiquité en application des dispositions de l'article 302 bis A du code général des impôts alors applicables et a assorti ces rappels des intérêts de retard et de l'amende fiscale prévue à l'article 1788 ter du code général des impôts, devenu 1770 octies ; que la société demande l'annulation de l'article 4 de l'arrêt du 24 novembre 2006 par lequel la cour administrative d'appel de Paris, après avoir prononcé le non-lieu à statuer sur les conclusions de la requête à raison des dégrèvements prononcés par l'administration en ce qui concerne la taxe mise à sa charge au titre des trois années et l'amende fiscale et après avoir réduit très partiellement la base de la taxe au titre de l'année 1991, a rejeté le surplus de ses conclusions ;<br/>
<br/>
              Considérant, en premier lieu, que pour écarter comme inopérant le moyen, qu'elle a regardé comme ayant trait à la régularité de la procédure d'imposition, tiré de la documentation administrative de base, invoquée sur le fondement de l'article L. 80 A du livre des procédures fiscales, la cour s'est fondée sur ce que les extraits de la documentation administrative de base ainsi invoqués ne portaient que sur le champ d'application et que la définition du redevable de la taxe litigieuse ; que, par suite, le moyen tiré de ce qu'elle aurait ainsi commis une erreur de droit en jugeant que les extraits de la documentation administrative de base ne pouvaient, eu égard à leur objet, être invoqués sur le fondement de l'article L. 80 A du livre des procédures fiscales, manque en fait ;<br/>
<br/>
              Considérant, en deuxième lieu, qu'aux termes de l'article 376 de l'annexe II au code général des impôts, applicable en l'espèce : "... seuls les fonctionnaires titulaires ou stagiaires appartenant à des corps des catégories A et B peuvent, dans le ressort territorial du service auquel ils sont affectés, fixer les bases d'imposition ou notifier des redressements..." ; qu'après avoir relevé que l'inspecteur des impôts, signataire des notifications de redressements dont procèdent les impositions et pénalités en litige, était affecté à la direction des vérifications de la région Ile-de-France Est  et que le ressort territorial de cette direction s'étendait à la totalité de la région Ile-de-France, la cour a pu, sans commettre d'erreur de droit, juger que quelle que soit la brigade d'affectation de cet agent au sein de la direction, celui-ci pouvait fixer les bases d'imposition et notifier des redressements à la SOCIETE SEGAME, laquelle avait son siège et ses bureaux à Paris ;<br/>
<br/>
              Considérant, en  troisième lieu, qu'aux termes de l'article 302 bis A du code général des impôts, issu de la loi du 19 juillet 1976 portant imposition des plus-values et création d'une taxe forfaitaire sur les métaux précieux, les bijoux, les objets d'art, de collection et d'antiquité, dans sa rédaction en vigueur au 1er janvier  1991 : "I. Sous réserve des dispositions particulières qui sont propres aux bénéfices professionnels... les ventes de bijoux, d'objets d'art, de collection et d'antiquité sont soumises à une taxe de 7 % lorsque leur montant excède 20 000 F ; dans le cas où ce montant est compris entre 20 000 F et 30 000 F, la base d'imposition est réduite d'un montant égal à la différence entre 30 000 F et ledit montant" ; qu'en vertu de l'article 302 bis B du même code : "La taxe prévue à l'article 302 bis A est supportée par le vendeur. Elle est versée par l'intermédiaire participant à la transaction ou, à défaut, par l'acheteur... La taxe n'est pas perçue lorsque le vendeur fait commerce des biens concernés, à titre professionnel" ; qu'enfin, aux termes de l'article 302 bis E du même code : "Le vendeur des bijoux et objets mentionnés à l'article 302 bis A, I, deuxième alinéa, peut opter par une déclaration faite au moment de la vente, pour le régime défini aux articles 150 A à 150 T, sous réserve qu'il puisse justifier de la date et du prix d'acquisition" ;<br/>
<br/>
              Considérant qu'en jugeant que, dès lors que la taxe litigieuse constituait une modalité particulière d'imposition des plus-values, elle ne contrevenait pas aux dispositions de l'article 1er de la première directive  du 11 avril 1967 et de l'article 33 de la sixième directive du 17 mai 1977 interdisant aux Etats membres d'introduire ou de maintenir des impôts qui ont le caractère d'une taxe sur le chiffre d'affaires, sans rechercher si elle présentait le caractère d'une taxe sur la valeur ajoutée, la cour a commis une erreur de droit ; que, toutefois, ainsi que l'a fait valoir le ministre en appel, la taxe, qui n'est due que par les personnes qui vendent dans le cadre de la gestion de leur patrimoine privé des métaux précieux ou des objets d'art, n'a pas de caractère général, ne s'applique pas à chaque stade du processus de production et de distribution de ces objets et n'a pas une assiette limitée à la valeur ajoutée ; que ce motif, qui répond à un moyen  invoqué devant le juge du fond et ne comporte l'appréciation d'aucune  circonstance de fait, doit être substitué au motif juridiquement erroné retenu par l'arrêt attaqué, dont il justifie légalement le dispositif ;<br/>
<br/>
              Considérant, en quatrième lieu, que la cour qui a jugé que la société a acheté, le 30 septembre 1992, des tableaux à  et non aux galeries Valois et Lansberg et était de ce fait redevable de la taxe, en application des dispositions de l'article 302 bis A du code général des impôts, a suffisamment motivé son arrêt en déduisant des factures établies par ces galeries qu'elles sont antérieures aux achats mentionnés en comptabilité, qu'elles indiquent que les règlements ont été effectués par chèque émanant de  et que celui-ci a pu acquérir les tableaux aux galeries avant de les  revendre à la société en septembre 1992 ;<br/>
<br/>
              Considérant, en  cinquième lieu, que la cour, qui n'était pas tenue de rechercher si les vendeurs de tableaux avaient acquitté un impôt sur les plus-values réalisées à l'occasion des ventes, n'a ni dénaturé les pièces du dossier ni retenu un motif inopérant en estimant que ces vendeurs auxquels avait été facturée la taxe devaient être regardés comme des particuliers passibles de la taxe dès lors que les éléments produits étaient trop imprécis pour établir que ces personnes étaient des marchands de tableaux non assujettis à la taxe en litige ;<br/>
<br/>
              Considérant, en sixième lieu, que les dispositions de l'article 1761 du code général des impôts issu de l'article 17 de l'ordonnance du 7 décembre 2005, lequel abroge l'article 1770 octies fixant le taux de l'amende à 100 %, sanctionnent d'une amende égale à 25 % des droits éludés le défaut de déclaration ou l'insuffisance de déclaration de la taxe litigieuse ; que le moyen tiré de ce que la cour n'aurait pas appliqué la loi répressive plus douce issue de l'ordonnance du 7 décembre 2005 créant ce nouvel article 1761 du code général des impôts manque en fait dès lors qu'elle a pris acte du dégrèvement prononcé par l'administration sur ce fondement et que ce n'est que par une erreur de plume sans incidence sur la solution retenue qu'elle a mentionné dans les motifs de son arrêt répondant au moyen tiré de la non conformité de la pénalité aux stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales le taux de 100 % résultant de l'article 1788 ter dont il avait été fait application dans l'avis de mise en recouvrement ; que, par suite, l'arrêt n'est entaché ni d'une insuffisance de motivation ni d'une erreur de droit ; <br/>
<br/>
              Considérant, en dernier lieu, que, d'une part, pour proportionner le taux de l'amende à l'infraction, l'ordonnance du 7 décembre 2005 a fixé ce taux à 25 % ; que, d'autre part, le juge de l'impôt, après avoir exercé son plein contrôle sur les faits invoqués et la qualification retenue par l'administration, décide, dans chaque cas, selon les résultats de ce contrôle, soit de maintenir la majoration infligée par l'administration, soit d'en prononcer la décharge s'il estime que le contribuable n'a pas contrevenu aux règles applicables en matière d'objet d'art et qu'il dispose ainsi d'un pouvoir de pleine juridiction conforme aux stipulations du paragraphe 1 de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, lesquelles n'impliquent pas, alors même que le législateur a retenu un taux unique pour l'amende en cause, que le juge puisse en moduler l'application en lui substituant un taux inférieur à celui prévu par la loi ; qu'ainsi, en n'écartant pas au cas d'espèce l'application de l'amende prévue à l'article 1761 du code général des impôts, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              Considérant qu'il suit de là que le pourvoi doit être rejeté ainsi que, par voie de conséquence, les conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              		D E C I D E :<br/>
              		--------------<br/>
<br/>
Article 1er : Le pourvoi de la  SOCIETE SEGAME est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la SOCIETE SEGAME, représentée par son liquidateur amiable, M. Gérard A, et au ministre du budget, des comptes publics et de la fonction publique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
