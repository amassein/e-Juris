<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036739785</ID>
<ANCIEN_ID>JG_L_2018_03_000000406802</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/73/97/CETATEXT000036739785.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section, 23/03/2018, 406802, Publié au recueil Lebon</TITRE>
<DATE_DEC>2018-03-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>406802</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Vincent Villette</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2018:406802.20180323</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société SAS Patrice Parmentier automobiles a demandé au tribunal administratif d'Amiens de prononcer la décharge des rappels de taxe sur la valeur ajoutée ainsi que de l'amende prévue au 4 de l'article 1788 A du code général des impôts, mis à sa charge au titre de la période du 1er février 2006 au 31 octobre 2009, et des taxes sur les dépenses de publicité auxquelles elle a été assujettie au titre des exercices 2006 à 2008. Par un jugement n° 1400375 du 10 mars 2016, le tribunal a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 16DA00896 du 2 novembre 2016, le président de la 2ème chambre de la cour administrative d'appel de Douai a, sur le fondement de l'article R. 222-1 du code de justice administrative, rejeté l'appel formé par la société contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire et un autre mémoire, enregistrés les 12 janvier et 10 avril 2017 et le 5 mars 2018 au secrétariat du contentieux du Conseil d'Etat, la société Patrice Parmentier automobiles demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Villette, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de la société Patrice Parmentier Automobiles ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces de la procédure devant les juges du fond que l'avocat de la société Patrice Parmentier automobiles, après avoir introduit devant la cour administrative d'appel de Douai une requête d'appel contre le jugement du tribunal administratif d'Amiens du 10 mars 2016 rejetant la demande en décharge des rappels de taxes auxquels avait été assujettie la société, a informé la cour qu'il n'assurait plus la défense des intérêts de sa cliente. La société Patrice Parmentier automobiles n'ayant pas répondu à l'invitation envoyée par la cour tendant à ce qu'elle régularise, dans un délai d'un mois, sa requête en désignant un nouveau mandataire qui aurait qualité pour la représenter, le président de la 2ème chambre de cette cour a rejeté sa requête comme manifestement irrecevable, par une ordonnance en date du 2 novembre 2016 prise sur le fondement de l'article R. 222-1 du code de justice administrative. La société se pourvoit en cassation contre cette ordonnance.<br/>
<br/>
              2. Lorsqu'elle est exigée par les dispositions régissant la procédure applicable devant les juridictions administratives, l'obligation faite aux parties d'être représentées par un avocat, qui a pour objet tant d'assurer aux justiciables le concours d'un mandataire qualifié veillant à leurs intérêts que de contribuer à la bonne administration de la justice en faisant de ce mandataire l'interlocuteur de la juridiction comme des autres parties, revêt un caractère continu qui se poursuit jusqu'à la lecture de la décision.<br/>
<br/>
              3. Il résulte d'une règle générale de procédure que lorsque la représentation est obligatoire, la révocation d'un avocat par sa partie ou la décision d'un avocat de mettre fin à son mandat est sans effet sur le déroulement de la procédure juridictionnelle et ne met un terme aux obligations professionnelles incombant à cet avocat que lorsqu'un autre avocat s'est constitué pour le remplacer, le cas échéant après qu'une invitation à cette fin a été adressée à la partie concernée par la juridiction.<br/>
<br/>
              4. Il résulte de ce qui précède qu'en rejetant comme irrecevable la requête de la société requérante au seul motif qu'elle avait cessé, en cours d'instance, d'être régulièrement représentée et qu'elle n'avait pas donné suite à la demande de régularisation l'invitant à constituer un nouvel avocat, le président de la 2ème chambre de la cour administrative d'appel de Douai a entaché son ordonnance d'erreur de droit. La société requérante est donc fondée, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, à en demander l'annulation.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à la société Patrice Parmentier Automobiles au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'ordonnance du président de la 2ème chambre de la cour administrative d'appel de Douai du 2 novembre 2016 est annulée.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Douai.<br/>
Article 3 : L'Etat versera une somme de 3 000 euros à la société Patrice Parmentier automobiles au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société Patrice Parmentier automobiles et au ministre de l'action et des comptes publics. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-08-02-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. FORMES DE LA REQUÊTE. MINISTÈRE D'AVOCAT. OBLIGATION. - 1) PORTÉE - OBLIGATION CONTINUE, JUSQU'À LA LECTURE DE LA DÉCISION - 2) RÉVOCATION D'UN AVOCAT PAR SA PARTIE OU DÉCISION D'UN AVOCAT DE METTRE FIN À SON MANDAT EN COURS DE PROCÉDURE - INCIDENCE SUR LE DÉROULEMENT DE CETTE PROCÉDURE - ABSENCE.
</SCT>
<ANA ID="9A"> 54-01-08-02-01 1) Lorsqu'elle est exigée par les dispositions régissant la procédure applicable devant les juridictions administratives, l'obligation faite aux parties d'être représentées par un avocat, qui a pour objet tant d'assurer aux justiciables le concours d'un mandataire qualifié veillant à leurs intérêts que de contribuer à la bonne administration de la justice en faisant de ce mandataire l'interlocuteur de la juridiction comme des autres parties, revêt un caractère continu qui se poursuit jusqu'à la lecture de la décision.,,,2) Il résulte d'une règle générale de procédure que lorsque la représentation est obligatoire, la révocation d'un avocat par sa partie ou la décision d'un avocat de mettre fin à son mandat est sans effet sur le déroulement de la procédure juridictionnelle et ne met un terme aux obligations professionnelles incombant à cet avocat que lorsqu'un autre avocat s'est constitué pour le remplacer, le cas échéant après qu'une invitation à cette fin a été adressée à la partie concernée par la juridiction.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
