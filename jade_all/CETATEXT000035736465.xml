<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035736465</ID>
<ANCIEN_ID>JG_L_2017_10_000000412381</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/73/64/CETATEXT000035736465.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 04/10/2017, 412381, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-10-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412381</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Etienne de Lageneste</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:412381.20171004</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société civile immobilière Marlin, à l'appui de sa demande tendant à ce que le tribunal administratif de Paris prononce la décharge des cotisations de la taxe sur les bureaux, commerces et locaux de stockage et surfaces de stationnement, ainsi que des majorations et intérêts de retard auxquelles elle a été assujettie au titre des années 2012 à 2015, ainsi que des majorations et intérêts de retard correspondantes, a produit deux mémoires, enregistrés les 30 décembre 2016 et 3 février 2017 au greffe du tribunal administratif, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lesquels elle soulève une question prioritaire de constitutionnalité.<br/>
<br/>
              Par une ordonnance n° 1621534 du 10 juillet 2017, enregistrée le 11 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, le magistrat délégué par le président du tribunal administratif de Paris, avant qu'il soit statué sur la demande de la SCI Marlin, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution du 2° bis du V de l'article 231 ter du code général des impôts.<br/>
<br/>
              Dans la question prioritaire de constitutionnalité transmise et dans un nouveau mémoire enregistré le 18 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, la SCI Marlin soutient que le 2° bis du V de l'article 231 ter du code général des impôts, applicable au litige, méconnaît le principe d'égalité devant la loi et le principe d'égalité devant les charges publiques garantis par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen de 1789.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de l'éducation ;<br/>
              - le code général des impôts, notamment son article 231 ter ;<br/>
              - la loi n° 2001-1275 du 31 décembre 2001 ; <br/>
              - la loi n° 2010-1658 du 29 décembre 2010 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Etienne de Lageneste, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes de l'article 231 ter du code général des impôts dans sa rédaction applicable aux impositions contestées : " I. - Une taxe annuelle sur les locaux à usage de bureaux, les locaux commerciaux, les locaux de stockage et les surfaces de stationnement annexées à ces catégories de locaux est perçue, dans les limites territoriales de la région d'Ile-de-France (...). / II. - Sont soumises à la taxe les personnes privées ou publiques qui sont propriétaires de locaux imposables ou titulaires d'un droit réel portant sur de tels locaux. (...) / III. - La taxe est due : / 1° Pour les locaux à usage de bureaux, qui s'entendent, d'une part, des bureaux proprement dits et de leurs dépendances immédiates et indispensables destinés à l'exercice d'une activité, de quelque nature que ce soit, par des personnes physiques ou morales privées, ou utilisés par l'Etat, les collectivités territoriales, les établissements ou organismes publics et les organismes professionnels, et, d'autre part, des locaux professionnels destinés à l'exercice d'activités libérales ou utilisés par des associations ou organismes privés poursuivant ou non un but lucratif ; / (...) V. - Sont exonérés de la taxe : / (...) 2° Les locaux et les surfaces de stationnement appartenant aux fondations et aux associations, reconnues d'utilité publique, dans lesquels elles exercent leur activité, ainsi que les locaux spécialement aménagés pour l'archivage administratif et pour l'exercice d'activités de recherche ou à caractère sanitaire, social, éducatif ou culturel ; / 2° bis Les locaux administratifs et les surfaces de stationnement des établissements publics d'enseignement du premier et du second degré et des établissements privés sous contrat avec l'Etat au titre des articles L. 442-5 et L. 442-12 du code de l'éducation (...) ".<br/>
<br/>
              3. La SCI Marlin soutient que les mots : " sous contrat avec l'Etat au titre des articles L. 442-5 et L. 442-12 du code de l'éducation " figurant au 2° bis du V de l'article 231 ter du code général des impôts et issus de l'article 9 de la loi de finances pour 2002, cité au point 2 ci-dessus méconnaissent le principe d'égalité devant la loi et le principe d'égalité devant les charges publiques qui résultent des articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen de 1789, en tant qu'elles traitent différemment les établissements privés selon qu'ils ont ou non conclu un contrat avec l'Etat, dès lors qu'aucune raison d'intérêt général en rapport avec l'objet de la loi ne permet de justifier cette différence de traitement, qui n'est pas fondée sur un critère objectif et rationnel en rapport avec le but poursuivi par le législateur.<br/>
<br/>
              4. Le 2° bis du V de l'article 231 ter du code général des impôts est applicable au litige dont est saisi le tribunal administratif de Paris. Cette disposition n'a pas déjà été déclarée conforme à la Constitution par le Conseil constitutionnel. Le moyen tiré de ce que les dispositions citées au point 3 ci-dessus portent atteinte aux droits et libertés garantis par la Constitution soulève une question présentant un caractère sérieux. Ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité à la Constitution des mots : " sous contrat avec l'Etat au titre des articles L. 442-5 et L. 442-12 du code de l'éducation " figurant au 2° bis du V de l'article 231 ter du code général des impôts issus de l'article 9 de la loi du 28 décembre 2001 est renvoyée au Conseil constitutionnel.<br/>
Article 2 : La présente décision sera notifiée à la société civile immobilière Marlin, au Premier ministre et au ministre de l'action et des comptes publics. <br/>
Copie en sera adressée au tribunal administratif de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
