<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036411857</ID>
<ANCIEN_ID>JG_L_2017_12_000000403048</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/41/18/CETATEXT000036411857.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 28/12/2017, 403048, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403048</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:403048.20171228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'État les 1er septembre 2016 et 28 juin 2017, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite du garde des sceaux, ministre de la justice, refusant d'abroger les dispositions du paragraphe 1 du titre II de sa note du 24 février 2015 de présentation des dispositions du décret n° 2014-1502 du 12 décembre 2014 relatif aux demandes d'aide juridictionnelle en cas de prise en charge par un dispositif de protection juridique ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 91-1266 du 19 décembre 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que, par une lettre du 30 mai 2016, M. A...B...a saisi le garde des sceaux, ministre de la justice d'une demande tendant à l'abrogation des dispositions du paragraphe 1 du titre II de sa note du 24 février 2015 de présentation des dispositions du décret du 12 décembre 2014 relatif aux demandes d'aide juridictionnelle en cas de prise en charge par un dispositif de protection juridique ; que M. B...demande l'annulation pour excès de pouvoir de la décision du 1er juillet 2016 par laquelle le garde des sceaux, ministre de la justice a refusé de faire droit à cette demande ;<br/>
<br/>
              Sur la recevabilité du mémoire en défense du garde des sceaux, ministre de la justice :<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 612-6 du code de justice administrative : " Si, malgré une mise en demeure, la partie défenderesse n'a produit aucun mémoire, elle est réputée avoir acquiescé aux faits exposés dans les mémoires du requérant. " ; que, contrairement à ce que soutient le requérant, il ne résulte pas de cette disposition que le mémoire en défense du garde des sceaux, ministre de la justice devrait être écarté des débats au motif qu'il a été produit postérieurement au délai fixé par la mise en demeure qui lui a été adressée ;<br/>
<br/>
              Sur les conclusions tendant à l'annulation du refus opposé par le garde des sceaux à la demande d'abrogation du paragraphe 1 du titre II de la note du 24 février 2015 :<br/>
<br/>
              3. Considérant que l'interprétation que, par voie, notamment, de circulaires ou d'instructions, l'autorité administrative donne des lois et règlements qu'elle a pour mission de mettre en oeuvre n'est pas susceptible d'être déférée au juge de l'excès de pouvoir lorsque, étant dénuée de caractère impératif, elle ne saurait, quel qu'en soit le bien-fondé, faire grief ; qu'en revanche, les dispositions impératives à caractère général d'une circulaire ou d'une instruction doivent être regardées comme faisant grief ; que le recours formé à leur encontre doit être accueilli si ces dispositions fixent, dans le silence des textes, une règle nouvelle entachée d'incompétence ou si, alors même qu'elles ont été compétemment prises, il est soutenu à bon droit qu'elles sont illégales pour d'autres motifs ; qu'il en va de même s'il est soutenu à bon droit que l'interprétation qu'elles prescrivent d'adopter, soit méconnaît le sens et la portée des dispositions législatives ou réglementaires qu'elle entendait expliciter, soit réitère une règle contraire à une norme juridique supérieure ;<br/>
<br/>
              4. Considérant qu'aux termes du dernier alinéa de l'article 2 de la loi du 10 juillet 1991 relative à l'aide juridique : " L'aide juridictionnelle n'est pas accordée lorsque les frais couverts par cette aide sont pris en charge au titre d'un contrat d'assurance de protection juridique ou d'un système de protection. " ; qu'en vertu de l'article 33 du décret du 19 décembre 1991 pris pour l'application de cette loi : " La demande d'aide juridictionnelle est déposée ou adressée par l'intéressé ou par tout mandataire au bureau d'aide juridictionnelle. / (...) / En outre, le demandeur doit préciser : / a) S'il dispose d'un ou plusieurs contrats d'assurance de protection juridique ou d'un autre système de protection couvrant la rémunération des auxiliaires de justice et les frais afférents au différend pour lequel le bénéfice de l'aide est demandé ; / (...) " ; que l'article 34 du même décret précise que : " Le demandeur doit joindre à cette demande : / (...) / 9° S'il a déclaré disposer d'un contrat d'assurance de protection juridique ou d'un autre système de protection en application du a de l'article 33, l'attestation de non-prise en charge délivrée selon le cas par l'employeur ou l'assureur, lorsque ce dernier ne prend pas en charge le litige ou le différend. En cas de prise en charge partielle des frais de procédure, le demandeur doit joindre la justification fournie par l'employeur ou l'assureur précisant le montant des plafonds de garantie et de remboursement des frais, émoluments et honoraires couverts. / (...) " ; que l'article 42 du même décret dispose que : " Le bureau peut faire recueillir tous renseignements et faire procéder à toutes auditions. / Il peut entendre ou faire entendre les intéressés. / Si le requérant ne produit pas les pièces nécessaires, le bureau ou la section du bureau peut lui enjoindre de fournir, dans un délai qu'il fixe et qui ne saurait excéder deux mois à compter de la réception de la demande qui lui est faite, tout document mentionné à l'article 34, même en original, ou tout renseignement de nature à justifier qu'il satisfait aux conditions exigées pour bénéficier de l'aide juridictionnelle. À défaut de production dans ce délai, la demande d'aide est caduque. Il en est de même lorsque le requérant demeure hors de France ou est de nationalité étrangère, sous réserve des conventions internationales. / (...) " ;<br/>
<br/>
              5. Considérant que les dispositions de la note dont M. B...a demandé l'abrogation se bornent à attirer l'attention des bureaux d'aide juridictionnelle sur la faculté que leur confère l'article 42 du décret du 19 décembre 1991 de solliciter du demandeur de l'aide juridictionnelle des informations complémentaires ; qu'ainsi, elles ne peuvent être regardées comme impératives ; que, par suite, le refus de les abroger ne fait pas grief et le ministre est fondé à soutenir que les conclusions de M. B...tendant à l'annulation de ce refus ne sont pas recevables ; qu'il résulte de ce qui précède que la requête de M. B...doit être rejetée, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : La requête de M. B...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et à la garde des sceaux, ministre de la justice. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
