<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028161267</ID>
<ANCIEN_ID>JG_L_2013_11_000000363963</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/16/12/CETATEXT000028161267.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 06/11/2013, 363963, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-11-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>363963</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:363963.20131106</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 19 novembre 2012 et 19 février 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SARL Rapidépannage 62, dont le siège est 1 rue de la Libération à Haillicourt (62940), représentée par son gérant en exercice ; la société demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2012-1063 du 17 septembre 2012 approuvant le onzième avenant à la convention passée entre l'Etat et la Société des autoroutes du nord et de l'est de la France (SANEF) en tant qu'il modifie le paragraphe 13.1 de l'article 13 du cahier des charges annexé à cette convention ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 2 500 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ainsi que le versement du montant de la contribution pour l'aide juridique mentionnée à l'article R. 761-1 du même code ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution ;<br/>
<br/>
              Vu le traité sur l'Union européenne ;<br/>
<br/>
              Vu le traité sur le fonctionnement de l'Union européenne ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu le code de la route ;<br/>
<br/>
              Vu le code de la voirie routière ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, avocat de la Société Rapidepannage 62 ;<br/>
<br/>
<br/>
<br/>1. Considérant que le dernier alinéa du paragraphe 13.1 de l'article 13 de la convention passée entre l'Etat et la société des autoroutes du Nord et de l'Est de la France (SANEF), dans la rédaction que lui a donnée l'avenant approuvé par le décret du 17 septembre 2012 dont la société requérante demande, dans cette seule mesure, l'annulation, charge le concessionnaire d'assurer ou de faire assurer, sur l'ensemble du domaine concédé, le dépannage des véhicules en panne ou accidentés et prévoit, lorsqu'il décide de confier cette activité à un tiers, la publication d'un avis d'appel à candidatures et la mise à disposition des candidats un dossier de consultation ; qu'il résulte enfin de cette même stipulation que tout candidat retenu doit être soumis à l'agrément du représentant de l'Etat ; <br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article 22 de la Constitution : " Les actes du Premier ministre sont contresignés, le cas échéant, par les ministres chargés de leur exécution. " ; que le décret attaqué n'implique pas nécessairement l'intervention de mesures réglementaires ou individuelles que le ministre de l'intérieur serait compétent pour signer ou contresigner ; que, par suite, ce ministre n'étant pas chargé de l'exécution de ce décret, celui-ci n'avait pas à être soumis à son contreseing ;<br/>
<br/>
              3. Considérant, en deuxième lieu, que la convention de concession passée entre l'Etat et la SANEF a pour objet de confier à celle-ci la construction, l'entretien et l'exploitation de certaines autoroutes ; que, parmi les missions de service public qui lui sont ainsi déléguées, figure notamment le service de dépannage des véhicules en panne ou accidentés sur l'ensemble du domaine autoroutier concédé, y compris sur les aires de repos et de stationnement ; que la société concessionnaire peut décider de confier à un tiers cette activité de dépannage ; que l'agrément du représentant de l'Etat aux entreprises de dépannage sélectionnées par la société concessionnaire a principalement pour objet de s'assurer que ces entreprises seront en mesure de remplir leurs missions dans l'ensemble du périmètre de la concession et répond aux objectifs de la sécurité routière sur des voies où les conditions de circulation conjuguent vitesse élevée et importance du trafic ; qu'ainsi, la société requérante n'est pas fondée à soutenir que l'institution de cet agrément porterait une atteinte disproportionnée à la liberté du commerce et de l'industrie, en faisant obstacle à ce que des entreprises de dépannage puissent proposer librement leurs services sur le domaine public autoroutier concédé, à tout le moins sur les aires de repos et de stationnement qui en sont les installations annexes ; <br/>
<br/>
              4. Considérant, en troisième lieu, qu'en vertu des stipulations citées ci-dessus, lorsque le concessionnaire décide de confier à un tiers l'activité de dépannage, il est tenu de publier par voie de presse un avis d'appel à candidatures, de mettre à la disposition des candidats un dossier de consultation indiquant les modalités et les critères de sélection et d'indiquer aux candidats les motifs du rejet de leur candidature ; que cette procédure, qui permet notamment de garantir un degré de publicité suffisant, ne méconnaît pas, en tout état de cause, les règles fondamentales du traité sur l'Union européenne au nombre desquelles figure le principe de non discrimination en raison de la nationalité ;<br/>
<br/>
              5. Considérant, en quatrième lieu, qu'aux termes du paragraphe 2 de l'article 106 du traité sur le fonctionnement de l'Union européenne : " Les entreprises chargées de la gestion de services d'intérêt économique général ou présentant le caractère d'un monopole fiscal sont soumises aux règles des traités, notamment aux règles de concurrence, dans les limites où l'application de ces règles ne fait pas échec à l'accomplissement en droit ou en fait de la mission particulière qui leur a été impartie (...) " ; que la mission de service public de dépannage des véhicules, déléguée par la concession, implique que la société concessionnaire ou, le cas échéant, les entreprises sous-traitantes choisies par elle, aient la capacité d'effectuer leurs interventions conformément aux objectifs de la sécurité routière qui s'imposent sur l'ensemble du périmètre de la concession, y compris pour l'accès aux aires de repos et de stationnement et pour leur dégagement ; que le choix, par la société concessionnaire, des entreprises de dépannage habilitées à intervenir sur le domaine concédé doit être effectué selon des critères objectifs, en rapport avec la mission de service public déléguée de dépannage des véhicules et propres à garantir que ces entreprises auront la capacité de réaliser leurs interventions conformément à ces objectifs ; qu'ainsi qu'il a été dit au point 3, l'agrément du représentant de l'Etat aux entreprises de dépannage sélectionnées par la société concessionnaire a principalement pour objet de s'assurer que ces entreprises seront en mesure de remplir leurs missions dans l'ensemble du périmètre de la concession et répond aux objectifs de la sécurité routière ; qu'ainsi, le décret attaqué a pu, sans méconnaître les stipulations de l'article 106 du traité sur le fonctionnement de l'Union européenne, autoriser la société concessionnaire soit à assurer elle-même à titre exclusif le dépannage des véhicules en panne ou accidentés, soit à sélectionner des entreprises de dépannage, qui seront seules habilitées à exercer leur activité sur le périmètre de la concession et prévoir, dans ce cas, que tout candidat retenu serait soumis à l'agrément du représentant de l'Etat ; <br/>
<br/>
              6. Considérant, en cinquième lieu, qu'il ne résulte ni du principe d'égalité, ni d'aucune règle que la SANEF et les autres concessionnaires d'autoroute seraient tenus de sélectionner les candidats à l'attribution de la sous-concession des activités de dépannage sur une portion d'autoroute selon une procédure et des critères strictement identiques sur l'ensemble du territoire national ; qu'il leur appartient seulement, ainsi qu'il vient d'être dit, de retenir des critères objectifs, en rapport avec la mission de service public déléguée de dépannage des véhicules ; que, par suite, le moyen tiré de ce que les stipulations contestées donneraient la possibilité de choisir les entreprises chargées des activités de dépannage selon des critères et des procédures variables et différents pour chaque portion d'autoroute concédée par l'Etat, tant à la SANEF qu'à d'autres concessionnaires, méconnaîtraient le principe d'égalité ne peut qu'être écarté ; <br/>
<br/>
              7. Considérant, en dernier lieu, que le moyen tiré de ce que, du fait de l'intervention du décret, les sociétés de dépannages agréées exploiteraient de manière abusive leur position dominante, en méconnaissance de l'article L. 420-2 du code de commerce, n'est pas assorti de précisions suffisantes permettant d'en apprécier le bien-fondé ; qu'il ne peut, par suite, qu'être écarté ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin d'examiner la fin de non-recevoir opposée par la SANEF, la requérante n'est pas fondée à demander l'annulation du décret attaqué ni, par voie de conséquence, à ce qu'il soit fait droit à ses conclusions au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à sa charge la somme de 3 500 euros à verser à la SANEF au titre des frais exposés par elle et non compris dans le dépens ; que, dans les circonstances de l'espèce, il y a lieu de laisser la contribution à l'aide juridique à la charge de la SARL Rapidépannage 62 ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la SARL Rapidépannage 62 est rejetée.<br/>
Article 2 : La SARL Rapidépannage 62 versera la somme de 3 500 euros à la Société des autoroutes du Nord et de l'Est de la France au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la SARL Rapidépannage 62, à la Société des autoroutes du Nord et de l'Est de la France, au ministre de l'écologie, du développement durable et de l'énergie et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
