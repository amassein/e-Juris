<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028195224</ID>
<ANCIEN_ID>JG_L_2013_11_000000340304</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/19/52/CETATEXT000028195224.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 13/11/2013, 340304</TITRE>
<DATE_DEC>2013-11-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>340304</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Olivier Gariazzo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:340304.20131113</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'arrêt n° 09NT00744 du 12 mai 2010, enregistré le 7 juin 2010 au secrétariat du contentieux du Conseil d'État, par lequel la cour administrative d'appel de Nantes a transmis au Conseil d'État le pourvoi présenté à cette cour par la SAS Icade Capri ;<br/>
<br/>
              Vu le pourvoi, enregistré le 26 mars 2009 au greffe de la cour administrative d'appel de Nantes, présenté par la SAS Icade Capri, et le mémoire complémentaire, enregistré le 10 septembre 2010 au secrétariat du contentieux du Conseil d'État, présenté pour la SAS Icade promotion logement, venant aux droits de la SAS Icade Capri, dont le siège est 2, avenue Carnot, BP 12319 à Nantes Cedex 1 (44023) ; la société demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler le jugement nos 06-4456 et 07-3237 du 20 janvier 2009 par lequel le tribunal administratif de Nantes a rejeté ses demandes tendant à l'annulation des titres de recette émis par le président de l'association syndicale autorisée des propriétaires de l'avenue René-Bazin à Nantes correspondant aux redevances syndicales arrêtées par le bureau de cette association syndicale au titre des années 2006 et 2007 ;<br/>
<br/>
              2°) de mettre à la charge de l'État et de l'association syndicale autorisée (ASA) des propriétaires de l'avenue René-Bazin le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 2000-321 du 12 avril 2000 ;<br/>
<br/>
              Vu l'ordonnance n° 2004-632 du 1er juillet 2004 ;<br/>
<br/>
              Vu le décret n° 2006-504 du 3 mai 2006 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Gariazzo, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de la SAS Icade promotion logement et à la SCP Lyon-Caen, Thiriez, avocat de l'association syndicale autorisée des propriétaires de l'avenue René-Bazin ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à raison de la propriété d'un ensemble immobilier situé avenue René-Bazin à Nantes, des redevances syndicales ont été mises à la charge de la société Icade Capri par des titres émis par l'association syndicale autorisée des propriétaires de l'avenue René-Bazin, au titre des années 2006 et 2007 ; que la société Icade promotion logement, venant aux droits de la société Icade Capri, se pourvoit en cassation contre le jugement du 20 janvier 2009 par lequel le tribunal administratif de Nantes a rejeté sa demande dirigée contre les titres exécutoires des 10 juillet 2006 et 18 mai 2007 ;<br/>
<br/>
              2. Considérant, en premier lieu, que, contrairement à ce qu'elle prétend, la société requérante ne soutenait pas devant le tribunal administratif que la parcelle sur laquelle est érigé l'immeuble objet des redevances litigieuses n'était pas incluse dans le périmètre de l'association syndicale autorisée des propriétaires de l'avenue René-Bazin ; qu'il suit de là qu'elle n'est pas fondée à soutenir que le tribunal aurait omis de répondre à un tel moyen ; qu'en outre, ce moyen, qui n'est pas d'ordre public et n'est pas né du jugement attaqué, ne peut être utilement invoqué pour la première fois devant le juge de cassation ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'aux termes de l'article 3 de l'ordonnance du 1er juillet 2004 relative aux associations syndicales de propriétaires, applicable au litige : " Les droits et obligations qui dérivent de la constitution d'une association syndicale de propriétaires sont attachés aux immeubles compris dans le périmètre de l'association et les suivent, en quelque main qu'ils passent, jusqu'à la dissolution de l'association ou la réduction de son périmètre. / (...) / Lors de la mutation d'un bien compris dans le périmètre d'une association syndicale, avis de la mutation doit être donné, dans les conditions prévues à l'article 20 de la loi n° 65-557 du 10 juillet 1965 fixant le statut de la copropriété des immeubles bâtis, à l'association qui peut faire opposition (...) pour obtenir le paiement des sommes restant dues par l'ancien propriétaire " ; qu'aux termes de l'article 4 de la même ordonnance : " Le président de l'association syndicale de propriétaires tient à jour l'état nominatif des propriétaires des immeubles inclus dans le périmètre de celle-ci ainsi que le plan parcellaire. À cet effet, toute mutation de propriété d'un immeuble inclus dans le périmètre de l'association lui est notifiée par le notaire qui en fait le constat " ; qu'il résulte de ces dispositions que, si les propriétaires successifs sont redevables de plein droit des redevances établies à leur nom, quelle que soit la date à laquelle remontent les dépenses auxquelles les redevances doivent faire face, ces redevances n'en constituent pas moins, dès l'émission des rôles, des dettes personnelles de ceux au nom desquelles elles sont établies ; que, par suite, lorsqu'une association syndicale autorisée n'a pas été informée d'une mutation de propriété dans les conditions prévues par ces dispositions, l'ancien propriétaire inscrit sur les rôles demeure redevable, à ce titre, des redevances ainsi mises à sa charge ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède qu'après avoir estimé, par une appréciation souveraine, non arguée de dénaturation, des pièces qui lui étaient soumises, que l'association syndicale autorisée des propriétaires de l'avenue René-Bazin n'avait pas reçu notification, dans les conditions prévues par les dispositions précitées de l'ordonnance du 1er juillet 2004, de la mutation de propriété dont la société requérante se prévalait, le tribunal administratif  n'a pas commis d'erreur de droit en en déduisant que la société ne pouvait utilement se prévaloir de la circonstance qu'elle n'était plus propriétaire de l'ensemble immobilier en cause pour obtenir la décharge des redevances syndicales en litige ;<br/>
<br/>
              5. Considérant, en troisième lieu, qu'il résulte des termes mêmes du jugement attaqué que le motif par lequel le tribunal, qui n'était saisi que de conclusions dirigées contre les titres exécutoires des 10 juillet 2006 et 18 mai 2007, s'est prononcé sur la régularité des décisions de l'association syndicale autorisée des propriétaires de l'avenue René-Bazin des 11 avril 2006 et 20 avril 2007 arrêtant les rôles des redevances syndicales respectivement au titre des années 2006 et 2007, était surabondant ; que, dès lors, le moyen dirigé contre ce motif et tiré de l'article 4 de la loi du 12 avril 2000 est inopérant et ne peut, par suite, qu'être écarté ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que le pourvoi de la société Icade promotion logement doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu en revanche, dans les circonstances de l'espèce, de mettre à sa charge la somme de 3 000 euros à verser à l'association syndicale autorisée des propriétaires de l'avenue René-Bazin, au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Le pourvoi de la SAS Icade promotion logement est rejeté.<br/>
Article 2 : La SAS Icade promotion logement versera à l'association syndicale autorisée des propriétaires de l'avenue René-Bazin une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Le surplus des conclusions de l'association syndicale autorisée des propriétaires de l'avenue René-Bazin est rejeté.<br/>
Article 4 : La présente décision sera notifiée à la SAS Icade promotion logement, au ministre de l'économie et des finances et à l'association syndicale autorisée des propriétaires de l'avenue René-Bazin.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">11-01-03 ASSOCIATIONS SYNDICALES. QUESTIONS COMMUNES. RESSOURCES. - REDEVANCE SYNDICALE - NATURE - DETTE PERSONNELLE, DÈS L'ÉMISSION DES RÔLES, DE CELUI AU NOM DUQUEL ELLE EST ÉTABLIE - EXISTENCE - CONSÉQUENCE - CAS OÙ L'ASSOCIATION N'A PAS ÉTÉ INFORMÉE D'UNE MUTATION DE PROPRIÉTÉ - REDEVABLE - ANCIEN PROPRIÉTAIRE INSCRIT SUR LES RÔLES - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 11-01-03 Il résulte des dispositions des articles 3 et 4 de l'ordonnance n° 2004-632 du 1er juillet 2004 relative aux associations syndicales de propriétaires que, si les propriétaires successifs sont redevables de plein droit des redevances établies à leur nom, quelle que soit la date à laquelle remontent les dépenses auxquelles les redevances doivent faire face, ces redevances n'en constituent pas moins, dès l'émission des rôles, des dettes personnelles de ceux au nom desquelles elles sont établies. Par suite, lorsqu'une association syndicale autorisée n'a pas été informée d'une mutation de propriété dans les conditions prévues par ces dispositions, l'ancien propriétaire inscrit sur les rôles demeure redevable, à ce titre, des redevances ainsi mises à sa charge.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., dans l'état du droit antérieur à l'ordonnance du 1er juillet 2004, CE, 8 juillet 1998, Association syndicale autorisée de drainage de Damvillers, n° 169257, T. pp. 755-862.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
