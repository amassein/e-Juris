<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037882245</ID>
<ANCIEN_ID>JG_L_2018_12_000000402321</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/88/22/CETATEXT000037882245.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 28/12/2018, 402321</TITRE>
<DATE_DEC>2018-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402321</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP RICARD, BENDEL-VASSEUR, GHNASSIA</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT>Mme Anne Iljic</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEORD:2018:402321.20181228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association du Vajra Triomphant Mandarom Aumisme (VTMA) a demandé au tribunal administratif de Marseille d'annuler pour excès de pouvoir l'arrêté du 2 décembre 2010 par lequel le maire de Castellane (Alpes de Haute-Provence) a retiré un permis de construire tacite acquis le 3 septembre 2010 ainsi que la décision du 8 décembre 2010 refusant d'accorder le permis de construire demandé le 29 mai 2007 pour la construction d'un temple. Par un jugement n° 1100708 du 29 avril 2013, le tribunal administratif de Marseille a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 13MA02652 du 9 juin 2016, la cour administrative d'appel de Marseille a, sur appel de l'association, d'une part, déclaré nul et de nul effet l'arrêté du 2 décembre 2010 et, d'autre part, annulé ce jugement dans la mesure où il a rejeté les conclusions dirigées contre cet arrêté et rejeté le surplus des conclusions. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 10 août et 10 novembre 2016 au secrétariat du contentieux du Conseil d'Etat, l'association VTMA demande au Conseil d'Etat :  <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Castellane la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Richard Senghor, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Anne Iljic, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de l'association du Vajra Triomphant Mandarom Aumisme et à la SCP Ricard, Bendel-Vasseur, Ghnassia, avocat de la commune de Castellane ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 10 août 2007, le maire de Castellane (Alpes de Haute-Provence) a sursis à statuer sur la demande de permis de construire formée par l'association du Vajra Triomphant Mandarom Aumisme (VTMA) pour l'édification d'un temple, au motif qu'un plan local d'urbanisme était en cours d'élaboration. Par un jugement du 31 mai 2010, le tribunal administratif de Marseille a, à la demande de l'association, annulé pour excès de pouvoir cet arrêté et enjoint au maire de réexaminer la demande de permis dans un délai de trois mois à compter de la notification du jugement. En l'absence de nouvelle décision de la commune dans ce délai, l'association l'a informée, par un courrier daté du 13 septembre 2010, qu'elle estimait être titulaire d'un permis tacite à compter du 3 septembre 2010. A la suite d'un courrier du 6 octobre 2010 l'informant de l'intention de la commune de retirer ce permis tacite, l'association a, par lettre du 26 octobre 2010, confirmé sa demande de permis de construire. Par un jugement du 29 avril 2013, le tribunal administratif de Marseille a rejeté sa demande tendant à l'annulation pour excès de pouvoir des deux arrêtés, respectivement du 2 et du 8 décembre 2010, par lesquels la commune de Castellane a, en premier lieu, retiré le permis tacite et, en second lieu, expressément refusé d'octroyer le permis sollicité. L'association du Vajra Triomphant Mandarom Aumisme se pourvoit en cassation contre l'arrêt du 9 juin 2016 par lequel la cour administrative d'appel de Marseille a déclaré nul et de non effet l'arrêté du 2 décembre 2010, annulé le jugement du 29 avril 2013 dans la mesure où il a rejeté les conclusions dirigées contre cet arrêté et rejeté le surplus de ses conclusions.<br/>
<br/>
Sur le désistement partiel : <br/>
<br/>
              2. L'association requérante a déclaré se désister des conclusions de son pourvoi dirigées contre l'arrêt de la cour administrative d'appel mentionné au point 1 en tant qu'il déclare nul et de nul effet l'arrêté du 2 décembre 2010 du maire de la commune de Castellane et annule le jugement du 29 avril 2013 du tribunal administratif de Marseille en tant qu'il rejette la demande de l'association tendant à l'annulation pour excès de pouvoir de cet arrêté. Ce désistement est pur et simple. Rien ne s'oppose à ce qu'il en soit donné acte. <br/>
<br/>
Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              3. Aux termes de l'article R. 741-2 du code de justice administrative : " La décision (...) contient le nom des parties, l'analyse des conclusions et mémoires ainsi que les visas des dispositions législatives ou réglementaires dont elle fait application (...) ". Aux termes de l'article R. 611-7 du code de justice administrative : " Lorsque la décision lui paraît susceptible d'être fondée sur un moyen relevé d'office, le président de la formation de jugement (...) en informe les parties avant la séance de jugement et fixe le délai dans lequel elles peuvent, sans qu'y fasse obstacle la clôture éventuelle de l'instruction, présenter leurs observations sur le moyen communiqué (...) ". Il résulte de ces dispositions que lorsqu'une partie se borne à produire des observations sur des moyens relevés d'office, il appartient dans tous les cas au juge administratif d'en prendre connaissance avant l'audience publique et de les viser dans sa décision, sans être tenu de les analyser. <br/>
<br/>
              4. Il ressort des pièces de la procédure devant la cour administrative d'appel que l'association VTMA a répondu par des observations enregistrées le 17 mai 2016, visées dans l'arrêt attaqué, au moyen relevé d'office, qui lui avait été communiqué dans des termes suffisamment précis, tiré de ce que n'étant pas titulaire d'un permis de construire tacite, l'arrêté du 2 décembre 2010 retirant ce permis tacite devait être déclaré nul et non avenu. Il s'ensuit que, contrairement à ce qui est soutenu, l'arrêt attaqué n'est pas entaché d'irrégularité faute pour les observations de l'association requérante, en réponse à cette communication, d'avoir été analysées dans ses visas.<br/>
<br/>
Sur le bien fondé de l'arrêt attaqué en tant qu'il statue sur les conclusions dirigées contre l'arrêté du 8 décembre 2010 :<br/>
<br/>
En ce qui concerne les articles NB3c et NB11 du règlement du plan d'occupation des sols : <br/>
<br/>
              5. Dès lors que les dispositions du règlement d'un plan d'occupation des sols ou d'un plan local d'urbanisme ont le même objet que celles d'un article du code de l'urbanisme posant des règles nationales d'urbanisme et posent des exigences qui ne sont pas moindres, c'est par rapport aux dispositions du règlement du plan d'occupation des sols que doit être appréciée la légalité d'une décision délivrant ou refusant une autorisation d'urbanisme. <br/>
<br/>
              6. Aux termes de l'article NB3c du règlement du plan d'occupation des sols de la commune de Castellane, applicable à la date du refus attaqué : " Accès et voieries : (...) Les accès doivent être adaptés à l'opération et aménagés de façon à éviter tout danger pour la circulation publique. / (...) Les voies publiques ou privées devront posséder des caractéristiques adaptées aux opérations qu'elles desservent et aux trafics qu'elles supportent. De plus, elles devront permettre commodément l'approche des véhicules de services et de lutte contre l'incendie. Enfin, les impasses seront aménagées à leur extrémité pour faciliter aux véhicules le demi-tour ". Il s'ensuit que c'est sans erreur de droit que la cour administrative d'appel, qui ce faisant n'a ni soulevé un moyen d'office, ni procédé à une substitution de base légale, a apprécié la légalité de la décision attaquée au regard des dispositions de l'article NB3c du règlement du plan d'occupation des sols de la commune de Castellane, relatives aux accès et voiries, après avoir relevé qu'elles avaient le même objet que celles de l'article R. 111-4 du code de l'urbanisme et posaient des exigences qui ne sont pas moindres. En jugeant que le maire de Castellane n'avait pas commis d'erreur d'appréciation au regard des dispositions précitées en estimant que la voie d'accès au site ne présentait pas les garanties nécessaires exigées par ces dispositions, les juges d'appel ont porté sur les faits de l'espèce une appréciation souveraine exempte de dénaturation.<br/>
<br/>
              7. Aux termes de l'article NB11 du plan d'occupation des sols de la commune de Castellane, applicable à la date du refus attaqué : " Aspect extérieur  : Conformément à l'article R. 111-21 du code de l'urbanisme : " Le projet peut être refusé ou n'être accordé que sous réserve de l'observation de prescriptions spéciales si les constructions, par leur situation, leur architecture, leurs dimensions ou l'aspect extérieur des bâtiments ou ouvrages à édifier ou à modifier, sont de nature à porter atteinte au caractère ou à l'intérêt des lieux avoisinants, aux sites, aux paysages naturels ou urbains ainsi qu'à la conservation des perspectives monumentales. L'implantation, la volumétrie et l'aspect architectural devront être conçus de manière à respecter les caractéristiques naturelles du terrain (topographie, végétation) et du paysage. En particulier : 1/ Les terrassements seront réduits au strict minimum et le sol remodelé selon son profil naturel (...) ".<br/>
<br/>
              8. Il ressort des pièces du dossier soumis aux juges du fond que la cour administrative d'appel de Marseille a pu considérer que le projet litigieux portait atteinte aux lieux avoisinants et aux paysages naturels, sans procéder à une substitution de motifs par rapport aux éléments retenus par le maire dans l'arrêté litigieux pour justifier son refus de délivrer un permis de construire, sans commettre d'erreur de droit dans le maniement des critères retenus, tenant à la volumétrie et à l'importance du projet au regard de l'état naturel du site sur lequel il est implanté, sans dénaturer les faits de l'espèce, compte tenu en particulier de l'avis défavorable émis le 4 juillet 2007 par l'architecte des bâtiments de France, fondé notamment sur la circonstance que " le bâtiment projeté vient lourdement rompre le charme décalé de l'ensemble des constructions "  et sans entacher son arrêt d'insuffisance de motivation eu égard aux arguments développés devant elle par les parties.<br/>
<br/>
En ce qui concerne le détournement de pouvoir allégué :<br/>
<br/>
              9. En jugeant que, dès lors que l'arrêté litigieux reposait sur des motifs légaux tirés de la méconnaissance des règles d'urbanisme, le moyen tiré de ce qu'il serait entaché de détournement de pouvoir ne pouvait qu'être écarté, la cour administrative d'appel de Marseille n'a entaché son arrêt d'aucune erreur de droit.<br/>
<br/>
En ce qui concerne les articles 9 et 11 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et l'article 1er du premier protocole additionnel à cette convention :<br/>
<br/>
              10. En jugeant, sans dénaturer les pièces du dossier qui lui était soumis, que le refus attaqué ne portait pas, par lui-même, atteinte aux libertés de religion et d'association protégées par les articles 9 et 11 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, la cour administrative d'appel de Marseille n'a pas commis d'erreur de droit.<br/>
<br/>
              11. Si l'association requérante soutient que la cour a entaché son arrêt d'erreur de droit en jugeant que l'arrêté litigieux ne portait pas atteinte au droit au respect des biens garanti par l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, elle n'apporte au soutien de ce moyen aucun élément permettant d'en apprécier le bien-fondé. <br/>
<br/>
En ce qui concerne le refus du 8 décembre 2010 :<br/>
<br/>
              12. Aux termes de l'article L. 123-6 du code de l'urbanisme, dans sa rédaction applicable à la date de la décision attaquée : " (...) A compter de la publication de la délibération prescrivant l'élaboration d'un plan local d'urbanisme, l'autorité compétente peut décider de surseoir à statuer, dans les conditions et délai prévus à l'article L. 111-8, sur les demandes d'autorisation concernant des constructions, installations ou opérations qui seraient de nature à compromettre ou à rendre plus onéreuse l'exécution du futur plan ". Aux termes de l'article L. 600-2 du même code : " Lorsqu'un refus opposé à une demande d'autorisation d'occuper ou d'utiliser le sol ou l'opposition à une déclaration de travaux régies par le présent code a fait l'objet d'une annulation juridictionnelle, la demande d'autorisation ou la déclaration confirmée par l'intéressé ne peut faire l'objet d'un nouveau refus ou être assortie de prescriptions spéciales sur le fondement de dispositions d'urbanisme intervenues postérieurement à la date d'intervention de la décision annulée sous réserve que l'annulation soit devenue définitive et que la confirmation de la demande ou de la déclaration soit effectuée dans les six mois suivant la notification de l'annulation au pétitionnaire ". Selon l'article R. 423-23 : " Le délai d'instruction de droit commun est de :/ (...) c) Trois mois pour les autres demandes de permis de construire (...) ". Aux termes de l'article R. 424-1 du même code : " A défaut de notification d'une décision expresse dans le délai d'instruction déterminé comme il est dit à la section IV du chapitre III ci-dessus, le silence gardé par l'autorité compétente vaut, selon les cas :/ (...) b) Permis de construire, permis d'aménager ou permis de démolir tacite ". Aux termes de l'article L. 911-2 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne à nouveau une décision après une nouvelle instruction, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision juridictionnelle, que cette nouvelle décision doit intervenir dans un délai déterminé ".<br/>
<br/>
              13. Il résulte de ces dispositions que l'annulation par le juge de l'excès de pouvoir de la décision qui a refusé de délivrer un permis de construire, ou qui a sursis à statuer sur une demande de permis de construire, impose à l'administration, qui demeure saisie de la demande, de procéder à une nouvelle instruction de celle-ci, sans que le pétitionnaire ne soit tenu de la confirmer. En revanche, un nouveau délai de nature à faire naître une autorisation tacite ne commence à courir qu'à dater du jour de la confirmation de sa demande par l'intéressé. En vertu des dispositions, citées au point 12, de l'article R. 424-1 du code de l'urbanisme, la confirmation de la demande de permis de construire par l'intéressé fait courir un délai de trois mois, à l'expiration duquel le silence gardé par l'administration fait naître un permis de construire tacite.<br/>
<br/>
              14. Il ressort des pièces du dossier soumis aux juges du fond que, par son jugement du 31 mai 2010, le tribunal administratif de Marseille, après avoir annulé la décision du 10 août 2007 du maire de Castellane de surseoir à statuer sur la demande de permis de construire de l'association VTMA, a enjoint à ce dernier de statuer à nouveau sur la demande dans un délai de trois mois à compter de la notification du jugement. Il résulte de ce qui a été dit au point précédent qu'après avoir relevé que l'association VTMA n'avait pas confirmé sa demande initiale avant le 26 octobre 2010, la cour administrative d'appel de Marseille en a déduit, sans erreur de droit, que l'association requérante n'était titulaire d'aucun permis de construire tacite à la date du 8 décembre 2010 à laquelle a été pris le refus de permis attaqué.<br/>
<br/>
              15. Il résulte de tout ce qui précède que l'association VTMA n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              16. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'association VTMA la somme de 3 000 euros à verser à la commune de Castellane au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions font, en revanche, obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la commune de Castellane, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Il est donné acte du désistement des conclusions du pourvoi de l'association VTMA dirigées contre les articles 1er et 2 de l'arrêt de la cour administrative d'appel de Marseille du 9 juin 2016.<br/>
Article 2 : Le surplus des conclusions du pourvoi de l'association VTMA est rejeté.<br/>
Article 3 : L'association VTMA versera à la commune de Castellane une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à l'association du Vajra Triomphant Mandarom Aumisme et à la commune de Castellane.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
