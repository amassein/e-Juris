<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023604371</ID>
<ANCIEN_ID>JG_L_2011_02_000000316508</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/60/43/CETATEXT000023604371.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 11/02/2011, 316508, Publié au recueil Lebon</TITRE>
<DATE_DEC>2011-02-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>316508</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER ; SCP DE CHAISEMARTIN, COURJON</AVOCATS>
<RAPPORTEUR>Mlle Aurélie  Bretonneau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Roger-Lacan Cyril</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:316508.20110211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 26 mai et 26 août 2008 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SOCIETE GENERALE, dont le siège est Tour Société Générale, 17, cours Valmy à Paris La Défense Cedex (92987) ; la SOCIETE GENERALE demande au Conseil d'Etat d'annuler la décision du 7 février 2008 par laquelle la commission des sanctions de l'Autorité des marchés financiers (AMF) a prononcé à son encontre une sanction pécuniaire de 300 000 euros et a ordonné sa publication au Bulletin des annonces légales obligatoires, ainsi que sur le site internet et dans la revue de l'AMF ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code monétaire et financier ;<br/>
<br/>
              Vu le règlement général du Conseil des marchés financiers ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mlle Aurélie Bretonneau, Maître des Requêtes,<br/>
<br/>
              - les observations de la SCP Célice, Blancpain, Soltner, avocat de la SOCIÉTÉ GENERALE, et de la SCP de Chaisemartin, Courjon, avocat de l'Autorité des marchés financiers ;<br/>
<br/>
              - les conclusions de M. Cyril Roger-Lacan, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Célice, Blancpain, Soltner, avocat de la SOCIÉTÉ GENERALE, et à la SCP de Chaisemartin, Courjon, avocat de l'Autorité des marchés financiers ;<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes de l'article L. 533-4 du code des marchés financiers, dans sa rédaction applicable au litige : " Les prestataires de services d'investissement (...) sont tenus de respecter des règles de bonne conduite destinées à garantir la protection des investisseurs et la régularité des opérations./ Ces règles (...)/ obligent notamment à :/ (...) 6. S'efforcer d'éviter les conflits d'intérêts et, lorsque ces derniers ne peuvent être évités, veiller à ce que leurs clients soient traités équitablement ;/ 7. Se conformer à toutes les réglementations applicables à l'exercice de leurs activités de manière à promouvoir au mieux les intérêts de leurs clients et l'intégrité du marché " ; qu'aux termes de l'article 3-1-6 du règlement général du Conseil des marchés financiers applicable aux faits litigieux, le recueil de l'ensemble des dispositions déontologiques que doivent observer le prestataire habilité, les personnes agissant pour son compte ou son autorité et ses mandataires agissant dans le cadre du service d'investissement exercé par le prestataire : " comporte en particulier les procédures connues sous le nom de ''muraille de chine'', dont l'objet est de prévenir la circulation indue d'informations confidentielles, notamment des informations privilégiées au sens de la réglementation en vigueur (...) " ; que l'article 3-1-7 du même règlement dispose, dans sa version applicable, que : " Les dispositions citées à l'article 3-1-6 prévoient notamment :/ 1° l'organisation matérielle conduisant à la séparation des différentes activités susceptibles de générer des conflits d'intérêts dans les locaux du prestataire habilité (...) " ; que selon l'article 3-1-8 du même texte : " (...) le déontologue organise les conditions de surveillance des transactions sur instruments financiers effectuées par le prestataire habilité pour son compte propre ou leur interdiction. Il élabore et tient à jour une liste de surveillance ou une liste d'interdiction de transactions pour compte propre sur des instruments financiers déterminés " ; qu'en vertu de l'article 3-1-9 du même texte : " La liste de surveillance recense les instruments financiers sur lesquels le prestataire habilité dispose d'une information sensible rendant nécessaire une vigilance particulière de la part du déontologue. Le déontologue suit l'état des transactions sur les instruments financiers inscrits sur la liste de surveillance " ;<br/>
<br/>
              Considérant qu'il résulte de l'instruction qu'au cours des mois d'août à novembre 2003, le service de la surveillance des marchés de l'Autorité des marchés financiers a constaté que le fonds spéculatif Amber Fund, contrôlé par une filiale à 100 % de la SOCIETE GENERALE, avait procédé à des acquisitions massives sur le marché du titre Sophia, société foncière de droit français cotée sur le marché Euronext Paris ; que l'Autorité des marchés financiers a, le 19 janvier 2004, ouvert une enquête portant sur le marché du titre Sophia à compter du 1er août 2003 puis, le 5 septembre 2005, étendu cette enquête à l'étude des participations du fonds d'investissement Amber Fund à compter de 2002 ; que sur la base du rapport d'enquête, des griefs ont été notifiés, d'une part, au fonds Amber Fund et à son gérant, d'autre part, à la SOCIETE GENERALE et à son secrétaire général ; que par la décision du 7 février 2008 que la SOCIETE GENERALE attaque, la commission des sanctions de l'Autorité des marchés financiers a, après avoir mis hors de cause les autres personnes concernées, infligé à cette société une sanction pécuniaire d'un montant de 300 000 euros en raison de manquements à ces dispositions ;<br/>
<br/>
              Considérant, en premier lieu, que si l'obligation de motivation à laquelle sont assujetties les sanctions prononcées par la commission des sanctions implique que celles-ci comportent l'énoncé des considérations de droit et de fait qui en constituent le fondement, elle n'impose pas qu'il soit répondu à l'intégralité des arguments invoqués devant la commission ; que contrairement à ce que soutient la requérante, la décision qu'elle attaque, qui comporte l'énoncé des considérations de droit et de fait sur lesquelles elle se fonde, est suffisamment motivée ; qu'en particulier, la commission des sanctions n'a pas insuffisamment motivé sa décision en ne mentionnant pas, lorsqu'elle a abordé la question des bases de données mises à la disposition du déontologue, l'existence, invoquée par la SOCIETE GENERALE dans ses observations, d'une base de données recensant les activités des structures du groupe opérant sur la base d'informations publiques, dont la requérante n'établissait pas qu'elle serait de nature à prévenir les manquements retenus à son encontre ;<br/>
<br/>
              Considérant, en deuxième lieu, qu'ainsi que l'a affirmé la commission des sanctions de l'Autorité des marchés financiers dans la décision litigieuse, il découle de l'objet des dispositions de l'article L. 533-4 citées ci-dessus que les règles qu'elles prévoient, dans l'objectif de garantir la protection des investisseurs et la régularité des opérations, s'imposent au prestataire de services d'investissement pour l'ensemble des activités dont il a la maîtrise, y compris celles exercées pour son compte ou à son bénéfice par une structure qu'il contrôle dotée de la personnalité morale ; qu'ainsi, en affirmant que les dispositions du règlement du Conseil des marchés financiers alors applicables, prises en application de l'article L. 533-4 du code des marchés financiers, relatives à la prévention et au règlement des conflits d'intérêts s'imposaient à la SOCIETE GENERALE, y compris au titre des activités du fonds spéculatif Amber Fund dont il n'est pas contesté qu'elle le contrôlait, et en estimant en particulier que les dispositions des articles 3-1-8 et 3-1-9 de ce règlement s'imposaient aux transactions réalisées par le fonds Amber Fund pour son compte propre au bénéfice de la SOCIETE GENERALE, la commission des sanctions de l'Autorité des marchés financiers n'a pas commis d'erreur de droit ;<br/>
<br/>
              Considérant, en troisième lieu, que pour caractériser un manquement de la SOCIETE GENERALE aux dispositions des articles 3-1-6 et 3-1-7 du règlement général du Conseil des marchés financiers citées ci-dessus, la commission des sanctions de l'Autorité des marchés financiers a relevé que si la séparation fonctionnelle opérée entre les structures exerçant des activités sur la base d'informations publiques, d'une part, et celles ayant accès à des informations confidentielles, d'autre part, était de nature à éviter la transmission d'informations confidentielles par les secondes aux premières, et s'il existait un dispositif permettant d'identifier d'éventuelles situations de conflits d'intérêts résultant des différentes transactions opérées par les structures ayant accès à des informations confidentielles, aucun dispositif efficace permettant de prévenir d'éventuelles situations de conflits d'intérêts entre d'une part l'activité d'une structure exerçant son activité sur la base d'informations confidentielles et d'autre part l'activité d'une structure opérant sur la base d'informations publiques n'avait, en revanche, été mis en place ; qu'elle n'a, ce faisant, ni entaché sa décision de contradiction de motifs, ni commis d'erreur d'appréciation ;<br/>
<br/>
              Considérant enfin que les articles 3-1-8 et 3-1-9 du règlement général du Conseil des marchés financiers cités ci-dessus imposaient que le déontologue exerce un suivi effectif de l'état des transactions opérées sur l'ensemble des instruments financiers inscrits sur la " liste de surveillance " ; qu'ainsi la commission des sanctions de l'Autorité des marchés financiers était fondée à sanctionner, en vertu de ces articles, le manquement résultant de l'absence de surveillance effective par le déontologue des opérations réalisées par le fonds Amber Fund sur le titre Sophia dont il résulte de l'instruction qu'il avait pourtant été inscrit sur la " liste de surveillance " dès le mois de novembre 2002 ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la SOCIETE GENERALE n'est pas fondée à demander l'annulation de la décision qu'elle attaque ; qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à sa charge le versement à l'Autorité des marchés financiers de la somme de 3 000 euros au titre des frais exposés par cette dernière et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
		Article 1er : La requête de la SOCIETE GENERALE est rejetée.<br/>
<br/>
Article 2 : La SOCIETE GENERALE versera à l'Autorité des marchés financiers la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la SOCIETE GENERALE, à l'Autorité des marchés financiers et au ministre de l'économie, des finances et de l'industrie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-01-02-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. QUESTIONS GÉNÉRALES. MOTIVATION. MOTIVATION SUFFISANTE. EXISTENCE. - DÉCISION DE LA COMMISSION DES SANCTIONS DE L'AMF.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">13-01-02-01 CAPITAUX, MONNAIE, BANQUES. CAPITAUX. OPÉRATIONS DE BOURSE. AUTORITÉ DES MARCHÉS FINANCIERS. - 1) APPLICATION DES RÈGLES DE BONNE CONDUITE DES PRESTATAIRES FINANCIERS AUX STRUCTURES QU'ILS CONTRÔLENT (FILIALES) - EXISTENCE - CONSÉQUENCE - POSSIBILITÉ DE SANCTIONNER LES PRESTATAIRES À RAISON DE L'ACTIVITÉ DE CES STRUCTURES - EXISTENCE - 2) MANQUEMENT DE NATURE À JUSTIFIER UNE SANCTION - ABSENCE DE SURVEILLANCE EFFECTIVE, PAR LE DÉONTOLOGUE DE LA BANQUE, DES OPÉRATIONS RÉALISÉES PAR LE FONDS SPÉCULATIF QUE CETTE DERNIÈRE DÉTIENT SUR DES TITRES INSCRITS SUR LA LISTE DE SURVEILLANCE.
</SCT>
<ANA ID="9A"> 01-03-01-02-02-02 Si l'obligation de motivation à laquelle sont assujetties les sanctions prononcées par la commission des sanctions implique que celles-ci comportent l'énoncé des considérations de droit et de fait qui en constituent le fondement, elle n'impose pas qu'il soit répondu à l'intégralité des arguments invoqués devant la commission. En l'espèce, la commission des sanctions n'a pas insuffisamment motivé sa décision en ne mentionnant pas, lorsqu'elle a abordé la question des bases de données mises à la disposition du déontologue, l'existence, invoquée par la requérante dans ses observations, d'une base de données recensant les activités des structures du groupe opérant sur la base d'informations publiques, dont elle n'établissait pas qu'elle serait de nature à prévenir les manquements retenus à son encontre.</ANA>
<ANA ID="9B"> 13-01-02-01 1) Il découle de l'objet des dispositions de l'article L. 533-4 du code monétaire et financier (antérieurement au 1er novembre 2007) que les règles qu'elles prévoient dans l'objectif de garantir la protection des investisseurs et la régularité des opérations s'imposent au prestataire de services d'investissement pour l'ensemble des activités dont il a la maîtrise, y compris celles exercées pour son compte ou à son bénéfice par une structure qu'il contrôle dotée de la personnalité morale. Par suite, les dispositions du règlement du Conseil des marchés financiers pris en application de cet article, relatives à la prévention et au règlement des conflits d'intérêts, s'imposent à une banque, y compris au titre des activités d'un fonds spéculatif dont il n'est pas contesté qu'elle le contrôlait et qui réalisait des transactions pour son compte.,,2) Est de nature à justifier une sanction l'absence de surveillance, par le déontologue de la banque, des opérations réalisées par le fonds spéculatif dépendant de celle-ci, portant sur des titres inscrits sur la liste de surveillance prévue par le règlement général du Conseil des marchés financiers.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
