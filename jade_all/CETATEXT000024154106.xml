<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024154106</ID>
<ANCIEN_ID>JG_L_2011_06_000000340863</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/15/41/CETATEXT000024154106.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 08/06/2011, 340863, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2011-06-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>340863</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Christian Vigouroux</PRESIDENT>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD</AVOCATS>
<RAPPORTEUR>Mme Eliane Chemla</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 25 juin et 27 septembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SA BALSA, dont le siège est 101 route du Polygone à Strasbourg (67000), représentée par son président-directeur général en exercice ; la SA BALSA demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09NC00872 du 22 avril 2010 par lequel la cour administrative d'appel de Nancy a rejeté sa requête tendant à l'annulation du jugement n° 0702574 du 9 avril 2009 du tribunal administratif de Strasbourg rejetant sa demande tendant à la décharge des rappels de taxe sur les salaires auxquelles elle a été assujettie au titre des années 2002, 2003, 2004 et 2005 et des intérêts de retard correspondants ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa requête ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 500 euros en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 62 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu la sixième directive 77/388/CEE du Conseil des communautés européennes du 17 mai 1977 en matière d'harmonisation des législations des Etats membres relatives aux taxes sur le chiffre d'affaires ; <br/>
<br/>
              Vu le code de commerce ; <br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu la décision du Conseil constitutionnel n° 2010-28-QPC du 17 septembre 2010 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Eliane Chemla, Conseiller d'Etat, <br/>
<br/>
              - les observations de la SCP Barthélemy, Matuchansky, Vexliard, avocat de la SA BALSA, <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Barthélemy, Matuchansky, Vexliard, avocat de la SA BALSA ;<br/>
<br/>
<br/>
<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              Considérant que la cour administrative d'appel de Nancy a omis de répondre au moyen tiré de ce que l'article 231 du code général des impôts serait contraire à l'article 13 de la sixième directive du Conseil des communautés européennes du 17 mai 1977 ; que, par suite, la SA BALSA est fondée à demander l'annulation de l'arrêt du 22 avril 2010 ;<br/>
<br/>
              Considérant que, dans les circonstances de l'espèce, il y a lieu de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Considérant qu'aux termes du 1 de l'article 231 du code général des impôts, les employeurs doivent payer une taxe sur les salaires  lorsqu'ils ne sont pas assujettis à la taxe sur la valeur ajoutée ou ne l'ont pas été sur 90 % au moins de leur chiffre d'affaires au titre de l'année civile précédant celle du paiement desdites rémunérations. L'assiette de la taxe due par ces personnes ou organismes est constituée par une partie des rémunérations versées, déterminée en appliquant à l'ensemble de ces rémunérations le rapport existant, au titre de cette même année, entre le chiffre d'affaires qui n'a pas été passible de la taxe sur la valeur ajoutée et le chiffre d'affaires total (...) ;<br/>
<br/>
              Sur la conformité aux droits et libertés garantis par la Constitution de l'article 231 du code général des impôts : <br/>
<br/>
              Considérant que si la SA BALSA soutient que l'article 231 du code général des impôts porte atteinte aux droits et libertés garantis par la Constitution, le Conseil constitutionnel, par une décision n° 2010-28-QPC du 17 septembre 2010 a, dans ses motifs et son dispositif, déclaré cette disposition conforme à la Constitution ; qu'ainsi, et en tout état de cause, le moyen doit être écarté ;<br/>
<br/>
              Sur la compatibilité de l'article 231 du code général des impôts avec la sixième directive du Conseil des communautés européennes du 17 mai 1977 : <br/>
<br/>
              Considérant, en premier lieu, que la taxe sur les salaires, qui est assise sur les rémunérations ou une partie des rémunérations versées par ses redevables, n'est pas établie d'une manière générale sur la base des transactions réalisées par ceux-ci et portant sur des biens ou des services, ni calculée proportionnellement au prix acquitté par le client, ni perçue à chaque stade du processus de production et de distribution, après déduction des droits acquittés lors de la transaction précédente ; que, dans ces conditions, elle ne présente pas les caractéristiques essentielles de la taxe sur la valeur ajoutée ; que la circonstance que la taxe sur les salaires ne frappe que les entreprises exonérées de taxe sur la valeur ajoutée ou non soumises à cette taxe sur au moins 90 % de leur chiffre d'affaires n'a pas pour effet de lui conférer le caractère d'une taxe sur le chiffre d'affaires prohibée par l'article 33 de la sixième directive ; <br/>
<br/>
              Considérant, en deuxième lieu, que les dispositions de l'article 13 de la sixième directive sont relatives aux exonérations à l'assujettissement à la taxe sur la valeur ajoutée et à leurs conditions de mise en oeuvre par les Etats membres ; que si les dispositions de l'article 231 du code général des impôts fixent l'assiette de l'imposition à la taxe sur les salaires à proportion inverse du chiffre d'affaires réalisé dans le cadre d'opérations soumises à la taxe sur la valeur ajoutée, cette circonstance n'affecte pas par elle-même le régime des exonérations de la taxe sur la valeur ajoutée et ne limite pas la portée des options prévues pour l'imposition à cette taxe ; que, par suite, la société requérante n'est pas fondée à soutenir que l'assujettissement à la taxe sur les salaires priverait d'effet les exonérations de taxe sur la valeur ajoutée prévues à l'article 13 de la sixième directive ; <br/>
<br/>
              Sur l'assujettissement des rémunérations des dirigeants à la taxe sur les salaires :<br/>
<br/>
              Considérant, d'une part, que lorsque les activités d'une entreprise sont, pour l'exercice de ses droits à déduction de la taxe sur la valeur ajoutée, réparties en plusieurs secteurs distincts au sens de l'article 213 de l'annexe II au code général des impôts alors en vigueur, la taxe sur les salaires doit être déterminée par secteur, en appliquant aux rémunérations des salariés affectés spécifiquement à chaque secteur le rapport d'assujettissement propre à ce secteur ; que, toutefois, la taxe sur les salaires des personnels concurremment affectés à plusieurs secteurs doit être établie en appliquant à leurs rémunérations le rapport existant pour l'entreprise dans son ensemble entre le chiffre d'affaires qui n'a pas été passible de la taxe sur la valeur ajoutée et le chiffre d'affaires total ; <br/>
<br/>
              Considérant, d'autre part, que les fonctions de directeur général d'une société anonyme ou d'une société par actions simplifiée confèrent à leurs titulaires, en vertu de l'article L. 225-56 du code de commerce, les pouvoirs les plus étendus dans la direction de la société et que le président du conseil d'administration est investi, aux termes de l'article L. 225-51 du même code, d'une responsabilité générale ; que, s'agissant d'une société holding, ces pouvoirs s'étendent en principe au secteur financier, même si le suivi des activités est sous-traité à des tiers ou confié à des salariés spécialement affectés à ce secteur et si le nombre des opérations relevant de ce secteur est très faible ; que, toutefois, s'il résulte des éléments produits par l'entreprise que certains de ses dirigeants n'ont pas d'attribution dans le secteur financier, notamment lorsque, compte tenu de l'organisation adoptée, l'un d'entre eux est dépourvu de tout contrôle et responsabilité en la matière, la rémunération de ce dirigeant doit être regardée comme relevant entièrement des secteurs passibles de la taxe sur la valeur ajoutée et, par suite, comme placée hors du champ de la taxe sur les salaires ; <br/>
<br/>
              Considérant qu'il résulte de l'instruction que la SA BALSA, qui avait deux secteurs d'activité dont l'un, à caractère financier, comprenait l'acquisition et la souscription de parts ou d'actions, la gestion de ses participations et de la trésorerie du groupe et l'autre, à caractère administratif, avait pour objet la réalisation de prestations de services destinées aux filiales, était soumise au titre de ce second secteur à la taxe sur la valeur ajoutée au titre des années 2002 à 2005 dans des conditions telles qu'elle était passible de la taxe sur les salaires ; qu'elle était dirigée par le président du conseil d'administration et deux directeurs généraux ; qu'il ne résulte ni des conventions d'assistance et de refacturation conclues avec ses filiales pour les services rendus par la holding, ni de la refacturation intégrale aux filiales des rémunérations allouées aux dirigeants, ni des statuts de la société ou des contrats de travail des dirigeants ni d'aucun autre élément produit au cours de l'instruction qu'un ou plusieurs des dirigeants de la société n'auraient eu d'attributions que dans le secteur de la holding soumis à la taxe sur la valeur ajoutée ; que, par suite, c'est à bon droit que l'administration a inclus leurs rémunérations dans le champ de la taxe sur les salaires et les a imposées à proportion du rapport existant pour l'entreprise dans son ensemble entre le chiffre d'affaires qui n'a pas été passible de la taxe sur la valeur ajoutée et le chiffre d'affaires total ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la SA BALSA n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Strasbourg a rejeté sa demande ; que, par voie de conséquence, ses conclusions fondées sur les dispositions de l'article L. 761-1 du code de justice administratives doivent être rejetées ;<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'arrêt du 22 avril 2010 de la cour administrative d'appel de Nancy est annulé. <br/>
Article 2 : La requête de la SA BALSA et le surplus des conclusions de son pourvoi sont rejetés.<br/>
Article 3 : La présente décision sera notifiée à la SA BALSA, au ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat, porte-parole du Gouvernement et au Premier ministre.<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
