<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033194811</ID>
<ANCIEN_ID>JG_L_2016_10_000000385009</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/19/48/CETATEXT000033194811.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 05/10/2016, 385009, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-10-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385009</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS ; LE PRADO ; SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>Mme Angélique Delorme</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:385009.20161005</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société SCREG Ile-de-France Normandie a demandé au tribunal administratif de Melun de condamner solidairement le département de Seine-et-Marne et la commune de Champagne-sur-Seine à lui verser la somme de 345 000 euros en réparation du préjudice consécutif à l'accident survenu le 18 mai 2005, au cours duquel un véhicule lourd lui appartenant a été endommagé.<br/>
<br/>
              Par un jugement n° 1007313/2 du 28 mars 2013, le tribunal administratif de Melun a fait droit à sa demande. Le département de Seine-et-Marne et la commune de Champagne-sur-Seine ont relevé appel de ce jugement.<br/>
<br/>
              Par un arrêt n° 13PA02048, 13PA2055 du 31 juillet 2014, la cour administrative d'appel de Paris a jugé que la responsabilité du département de Seine-et-Marne n'était pas engagée et que la société avait elle-même concouru à la survenance du dommage à hauteur de 25 % de celui-ci. Elle a condamné la commune de Champagne-sur-Seine à verser à la société Colas Ile-de-France Normandie, venue aux droits de la société SCREG Ile-de-France Normandie, une somme de 258 750 euros en réparation du préjudice subi et a rejeté le surplus des conclusions de la requête de la commune.<br/>
<br/>
              Par un pourvoi et un mémoire complémentaire, enregistrés les 6 octobre 2014 et 6 janvier 2015 au secrétariat du contentieux du Conseil d'Etat, la commune de Champagne-sur-Seine demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, à titre principal, de rejeter les conclusions indemnitaires formées à son encontre par la société Colas Ile-de-France Normandie et, à titre subsidiaire, d'appliquer un partage de responsabilité entre elle-même et le département de Seine-et-Marne, maître d'ouvrage, et d'atténuer sa responsabilité pour tenir compte des causes étrangères exonératoires de celle-ci ;<br/>
<br/>
              3°) de mettre à la charge de la société Colas Ile-de-France Normandie la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Angélique Delorme, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de la commune de Champagne-sur-Seine, à la SCP Baraduc, Duhamel, Rameix, avocat du département de Seine-et-Marne et à Me Le Prado, avocat de la société Colas Ile-de-France ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la commune de Champagne-sur-Seine a passé en 2005 un marché public de travaux portant sur la réfection de la voirie communale avec la société SCREG Ile-de-France Normandie ; que, pour transporter les matériaux nécessaires à la conduite de ces travaux, un véhicule lourd de cette société, équipé d'une remorque, devait plusieurs fois par jour emprunter dans la commune un itinéraire incluant le franchissement d'un passage à niveau immédiatement suivi d'un virage à droite dans une voie communale, la rue de Sens, ce virage nécessitant une manoeuvre difficile ; que, le 18 mai 2005, un véhicule lourd de cette société, effectuant la manoeuvre décrite ci-dessus, a été immobilisé par un véhicule tiers empruntant la rue de Sens dans la direction opposée ; que le véhicule lourd dédié au transport de matériaux de chantier s'est trouvé de ce fait immobilisé, alors que sa remorque était encore engagée sur le passage à niveau, avant d'être percuté par un train de marchandises ; que la société SCREG Ile-de-France Normandie  a demandé au tribunal administratif de Melun de condamner solidairement le département de Seine-et-Marne et la commune de Champagne-sur-Seine à lui verser la somme de 345 000 euros en réparation des dommages causés à son véhicule ; que, par un jugement du 28 mars 2013, le tribunal administratif de Melun a fait droit à cette demande ; que, par un arrêt du 31 juillet 2014, la cour administrative d'appel de Paris a, d'une part, jugé que la responsabilité du département de Seine-et-Marne n'était pas engagée à l'égard de la société Colas Ile-de-France Normandie, venue aux droits de la société SCREG Ile-de-France Normandie, et a, d'autre part, condamné la commune de Champagne-sur-Seine à verser à la société Colas Ile-de-France Normandie une somme de 258 750 euros en réparation du préjudice subi par celle-ci ; <br/>
<br/>
              2. Considérant que la commune de Champagne-sur-Seine se pourvoit en cassation contre cet arrêt ; que la société Colas Ile-de-France Normandie, qui conclut au rejet de ce pourvoi, demande en outre que l'arrêt attaqué soit annulé en tant qu'il a écarté la responsabilité du département de Seine-et-Marne ; <br/>
<br/>
              Sur le pourvoi principal de la commune de Champagne-sur-Seine :<br/>
<br/>
              3. Considérant qu'en relevant, d'une part, que l'état de la route départementale ne faisait l'objet d'aucune critique, et, d'autre part, qu'il résultait des dispositions des articles L. 2213-1 et L. 3221-4 du code général des collectivités territoriales que le maire d'une commune est seul compétent, dans le cadre de ses pouvoirs de police de la circulation, pour décider de la mise en place de dispositifs de sécurité sur les routes et voies à l'intérieur de l'agglomération de sa commune, dès lors que ces dispositifs n'ont ni pour objet, ni pour effet, de modifier l'assiette des routes dont la commune n'est pas propriétaire, pour en déduire que l'absence de mise en oeuvre d'un système de signalisation, seul à même de permettre le franchissement du passage à niveau et l'engagement dans la rue communale dans des conditions de sécurité adéquates, devait être assimilé à un défaut d'entretien normal de l'ouvrage public et engageait la responsabilité de la seule commune de Champagne-sur-Seine, la cour administrative d'appel de Paris, qui a sur ce point exactement interprété les écritures des parties et a porté sur les faits de l'espèce une appréciation souveraine exempte de dénaturation, n'a pas commis d'erreur de droit ;  <br/>
<br/>
              4. Considérant qu'il ressort de l'arrêt attaqué que la cour a estimé que la responsabilité de la commune de Champagne-sur-Seine était engagée sur le terrain du défaut d'entretien normal de la voirie communale ; que si, à cet égard, les juges d'appel ont relevé l'insuffisance de la signalisation dont était affectée celle-ci, ils ont pu sans erreur de droit regarder cette insuffisance de la signalisation comme étant constitutive d'un défaut d'entretien normal de la voirie communale ; que, dès lors que la cour n'a pas retenu à l'encontre de la commune de Champagne-sur-Seine une quelconque faute dans l'exercice des pouvoirs de police du maire, le moyen tiré de ce que la cour aurait commis une erreur de droit en ne recherchant pas si pouvait être reprochée à la commune de Champagne-sur-Seine une faute lourde dans l'exercice de tels pouvoirs de police ne peut qu'être écarté ; <br/>
<br/>
              5. Considérant que le maître d'un ouvrage public ne peut invoquer, vis-à-vis d'une victime ayant la qualité de tiers par rapport à l'ouvrage, le fait d'un tiers que lorsqu'il se trouve privé de la possibilité d'exercer un recours en garantie contre ce dernier ; qu'en relevant que la commune de  Champagne-sur-Seine pouvait exercer une action récursoire contre la SNCF et contre le chauffeur du véhicule tiers ayant empêché le véhicule lourd de la société SCREG Ile-de-France Normandie d'achever sa manoeuvre, et en en déduisant que l'invocation de la responsabilité éventuellement encourue par des tiers n'était pas de nature à atténuer la responsabilité de la commune de  Champagne-sur-Seine, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que le pourvoi de la commune de Champagne-sur-Seine doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Sur le pourvoi provoqué de la société Colas Ile-de-France :<br/>
<br/>
              7. Considérant que le rejet, par la présente décision, du pourvoi principal de la commune de Champagne-sur-Seine n'entraine aucune aggravation de la situation de la société Colas Ile-de-France Normandie, dès lors que la commune demeure condamnée à lui verser l'intégralité de l'indemnité mise à la charge de celle-ci par la cour administrative d'appel de Paris ; que, dès lors, les conclusions du pourvoi provoqué par lequel la société Colas Ile-de-France Normandie demande l'annulation de l'arrêt attaqué, en tant que la cour a écarté la responsabilité du département de Seine-et-Marne, sont irrecevables ;  <br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative<br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Champagne-sur-Seine la somme de 2 000 euros à verser au département de Seine-et-Marne, au titre de ces dispositions ainsi qu'une somme de 2 000 euros à verser à la société Colas Ile-de-France Normandie ;<br/>
<br/>
              9. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Colas Ile-de-France Normandie le versement au département de Seine-et-Marne de la somme de 2 000 euros à verser au département de Seine-et-Marne ; que ces mêmes dispositions font obstacle à ce que la somme demandée au même titre par la société Colas Ile-de-France Normandie soit à la charge du département de Seine-et-Marne qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la commune de Champagne-sur-Seine est rejeté.<br/>
Article 2 : Le pourvoi provoqué de la société Colas Ile-de-France est rejeté.<br/>
Article 3 : La commune de Champagne-sur-Seine versera une somme de 2 000 euros au département de Seine-et-Marne au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La commune de Champagne-sur-Seine versera une somme de 2 000 euros à la société Colas Ile-de-France au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La société Colas Ile-de-France versera une somme de 2 000 euros au département de Seine-et-Marne au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 6 : La présente décision sera notifiée à la commune de Champagne-sur-Seine, à la société Colas Ile-de-France et au département de Seine-et-Marne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
