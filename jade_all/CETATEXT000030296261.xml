<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030296261</ID>
<ANCIEN_ID>JG_L_2015_02_000000376598</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/29/62/CETATEXT000030296261.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème SSR, 27/02/2015, 376598, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-02-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>376598</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Camille Pascal</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2015:376598.20150227</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 376598, le pourvoi sommaire et le mémoire complémentaire, enregistrés les 24 mars et 26 juin 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour La Poste, dont le siège est 44, boulevard de Vaugirard à Paris Cedex 15 (75757) ; La Poste demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt de la cour administrative d'appel de Marseille n° 11MA00325 du 17 janvier 2014 en tant qu'il a réformé le jugement n° 0808803 rendu par le tribunal administratif de Marseille le 25 novembre 2010 et annulé la décision du 14 octobre 2008 par laquelle le président du conseil d'administration de La Poste a prononcé, à l'encontre de M. A...B..., la sanction d'exclusion temporaire de fonctions pour une durée de deux ans ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. B...; <br/>
<br/>
              3°) de mettre à la charge de M. B...la somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu 2°, sous le n° 381828, la requête, enregistrée le 26 juin 2014 au secrétariat du contentieux du Conseil d'Etat, présenté pour La Poste, qui demande au Conseil d'Etat d'ordonner qu'il soit sursis à l'exécution de l'arrêt n° 11MA00325 du 17 janvier 2014 de la cour administrative d'appel de Marseille qui fait l'objet du pourvoi enregistré sous le n° 376598 ; <br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ; <br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu la loi n° 90-568 du 2 juillet 1990 ;<br/>
<br/>
              Vu le décret n° 82-447 du 28 mai 1982 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Camille Pascal, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de La Poste, et à la SCP Waquet, Farge, Hazan, avocat de M. B...; <br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier soumis au juge du fond que M.B..., agent de La Poste depuis 1993, facteur au centre courrier de Marseille, a fait l'objet, par décision du 14 octobre 2008 signée du président du conseil central de discipline de La Poste, d'une sanction disciplinaire d'exclusion temporaire de fonction pour une durée de deux ans pour des refus d'obéissance envers ses supérieurs hiérarchiques, agression à l'encontre de sa supérieure hiérarchique, dégradation d'une des portes d'accès au local de la direction et attitude dilatoire au cours de l'enquête interne ; que La Poste se pourvoit en cassation contre l'arrêt du 17 janvier 2014 par lequel la cour administrative d'appel de Marseille a, sur appel de M. B..., prononcé l'annulation pour excès de pouvoir de la décision du 14 octobre 2008 ; qu'elle présente par ailleurs une requête tendant à ce qu'il soit sursis à l'exécution de cet arrêt ; qu'il y a lieu de joindre ce pourvoi et cette requête pour statuer par une seule décision ;<br/>
<br/>
              2.	Considérant qu'il appartient au juge de l'excès de pouvoir, saisi de moyens en ce sens, de rechercher si les faits reprochés à un agent public ayant fait l'objet d'une sanction disciplinaire constituent des fautes de nature à justifier une sanction et si la sanction retenue est proportionnée à la gravité de ces fautes ; <br/>
<br/>
              3.	Considérant que la constatation et la caractérisation des faits reprochés à l'agent relèvent, dès lors qu'elles sont exemptes de dénaturation, du pouvoir souverain des juges du fond ; que le caractère fautif de ces faits est susceptible de faire l'objet d'un contrôle de qualification juridique de la part du juge de cassation ; que l'appréciation du caractère proportionné de la sanction au regard de la gravité des fautes commises relève, pour sa part, de l'appréciation des juges du fond et n'est susceptible d'être remise en cause par le juge de cassation que dans le cas où la solution qu'ils ont retenue quant au choix, par l'administration, de la sanction est hors de proportion avec les fautes commises ;<br/>
<br/>
              4.	Considérant que la cour administrative d'appel de Marseille, pour caractériser les faits reprochés à M. B...et juger que les fautes commises par l'intéressé n'étaient pas de nature à justifier le prononcé de la sanction qui lui a été infligée, a estimé que n'étaient établis " ni l'existence d'une agression physique du supérieur hiérarchique, ni le caractère récurrent au delà des trois dates retenues des refus d'obéissance, ni le caractère volontaire de la dégradation de la porte d'accès à la direction " ; qu'il ressort toutefois des pièces du dossier soumis à la cour que, le 7 mars 2008, alors que plusieurs facteurs du centre courrier de Marseille manifestaient leur refus de distribuer des plis électoraux à destination des électeurs de Marseille et tentaient de pénétrer par la force dans un local sécurisé et accessible aux seules personnes accréditées, M.B..., militant syndical, a bousculé et interpellé la responsable de ce service, en proférant des menaces à son encontre alors qu'elle tentait de s'opposer à l'intrusion dans ce local ; que, le 21 mai 2008, M. B...a pris la parole devant des agents non grévistes du centre de distribution du courrier, alors que le responsable du centre avait interdit les prises de paroles ; que, le 23 mai 2008 ce dernier a tenté, en compagnie d'autres manifestants, de pénétrer de force dans les locaux de la direction, provoquant une bousculade et des dégradations matérielles ; que rappelé à l'ordre à trois reprises par sa hiérarchie, M. B...a refusé d'obtempérer ; que, par suite, en estimant que n'étaient établis ni l'agression physique à l'égard de la supérieure hiérarchique de l'intéressée, ni le caractère récurrent des refus d'obéissance, ni le caractère volontaire des dégradations, la cour administrative d'appel a dénaturé les pièces du dossier qui lui était soumis ; <br/>
<br/>
              5.	Considérant que La Poste est, dès lors, fondée à demander l'annulation de l'arrêt qu'elle attaque, en tant qu'il a annulé la décision du 14 octobre 2008 et réformé en ce sens le jugement du tribunal administratif de Marseille du 25 novembre 2010 ; qu'il s'ensuit que ses conclusions tendant à ce qu'il soit sursis à l'exécution de cet arrêt deviennent sans objet et qu'il n'y a, dès lors, plus lieu d'y statuer ;<br/>
<br/>
              6.	Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de La Poste, qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B...la somme demandée par La Poste au titre des mêmes dispositions ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 17 janvier 2014 est annulé en tant qu'il a annulé la décision du 14 octobre 2008 du président du conseil d'administration de La Poste et réformé le jugement du tribunal administratif de Marseille du 25 novembre 2010.<br/>
<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : Il n'y a pas lieu de statuer sur la requête n° 381828.<br/>
<br/>
Article 4 : Les conclusions de La Poste présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : Les conclusions de M. B...présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à La Poste et à M. A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-13-01-03 FONCTIONNAIRES ET AGENTS PUBLICS. CONTENTIEUX DE LA FONCTION PUBLIQUE. CONTENTIEUX DE L'ANNULATION. POUVOIRS DU JUGE. - RECOURS POUR EXCÈS DE POUVOIR CONTRE UNE SANCTION DISCIPLINAIRE D'UN AGENT PUBLIC - NATURE DU CONTRÔLE DU JUGE DE CASSATION - 1) JUGE DU FOND - CARACTÈRE FAUTIF DES FAITS REPROCHÉS - CONTRÔLE ENTIER - PROPORTIONNALITÉ DE LA SANCTION - CONTRÔLE ENTIER - 2) JUGE DE CASSATION - A) SUR LA MATÉRIALITÉ DES FAITS REPROCHÉS À L'AGENT - DÉNATURATION - B) SUR LEUR CARACTÈRE FAUTIF - QUALIFICATION JURIDIQUE - C) SUR LA SANCTION PRONONCÉE - VÉRIFICATION DE CE QUE LA SOLUTION RETENUE PAR LES JUGES DU FOND QUANT AU CHOIX DE LA SANCTION N'EST PAS HORS DE PROPORTION AVEC LES FAUTES COMMISES [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-02-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE NORMAL. - QUALIFICATION JURIDIQUE DE FAUTE DES FAITS REPROCHÉS À UN AGENT PUBLIC - EXISTENCE - PROPORTIONNALITÉ DE LA SANCTION DISCIPLINAIRE - EXISTENCE [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-08-02-02-01 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. - JUGEMENT S'ÉTANT PRONONCÉ SUR UN RECOURS POUR EXCÈS DE POUVOIR CONTRE UNE SANCTION DISCIPLINAIRE D'UN AGENT PUBLIC - NATURE DU CONTRÔLE DU JUGE DE CASSATION - 1) SUR LA MATÉRIALITÉ DES FAITS - DÉNATURATION - 2) SUR LE CARACTÈRE FAUTIF DES FAITS REPROCHÉS - QUALIFICATION JURIDIQUE - 3) SUR LE CARACTÈRE PROPORTIONNÉ DE LA SANCTION - VÉRIFICATION DE CE QUE LA SOLUTION RETENUE PAR LES JUGES DU FOND QUANT AU CHOIX DE LA SANCTION N'EST PAS HORS DE PROPORTION AVEC LES FAUTES COMMISES [RJ1].
</SCT>
<ANA ID="9A"> 36-13-01-03 1) Il appartient au juge de l'excès de pouvoir, saisi de moyens en ce sens, de rechercher si les faits reprochés à un agent public ayant fait l'objet d'une sanction disciplinaire constituent des fautes de nature à justifier une sanction et si la sanction retenue est proportionnée à la gravité de ces fautes.... ,,2) a) La constatation et la caractérisation des faits reprochés à l'agent relèvent, dès lors qu'elles sont exemptes de dénaturation, du pouvoir souverain des juges du fond.... ,,b) Le caractère fautif de ces faits est susceptible de faire l'objet d'un contrôle de qualification juridique de la part du juge de cassation.... ,,c) L'appréciation du caractère proportionné de la sanction au regard de la gravité des fautes commises relève, pour sa part, de l'appréciation des juges du fond et n'est susceptible d'être remise en cause par le juge de cassation que dans le cas où la solution qu'ils ont retenue quant au choix, par l'administration, de la sanction est hors de proportion avec les fautes commises.</ANA>
<ANA ID="9B"> 54-07-02-03 Il appartient au juge de l'excès de pouvoir, saisi de moyens en ce sens, de rechercher si les faits reprochés à un agent public ayant fait l'objet d'une sanction disciplinaire constituent des fautes de nature à justifier une sanction et si la sanction retenue est proportionnée à la gravité de ces fautes.</ANA>
<ANA ID="9C"> 54-08-02-02-01 Il appartient au juge de l'excès de pouvoir, saisi de moyens en ce sens, de rechercher si les faits reprochés à un agent public ayant fait l'objet d'une sanction disciplinaire constituent des fautes de nature à justifier une sanction et si la sanction retenue est proportionnée à la gravité de ces fautes.... ,,1) La constatation et la caractérisation des faits reprochés à l'agent relèvent, dès lors qu'elles sont exemptes de dénaturation, du pouvoir souverain des juges du fond.... ,,2) Le caractère fautif de ces faits est susceptible de faire l'objet d'un contrôle de qualification juridique de la part du juge de cassation.... ,,3) L'appréciation du caractère proportionné de la sanction au regard de la gravité des fautes commises relève, pour sa part, de l'appréciation des juges du fond et n'est susceptible d'être remise en cause par le juge de cassation que dans le cas où la solution qu'ils ont retenue quant au choix, par l'administration, de la sanction est hors de proportion avec les fautes commises.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Rappr., s'agissant du contrôle en cassation d'une décision juridictionnelle prononçant une sanction, CE, Assemblée, 30 décembre 2014, M. Bonnemaison, n° 381245, p. 443., ,[RJ2]Cf. CE, Assemblée, 13 novembre 2013, M. Dahan, n° 347704, p. 279.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
