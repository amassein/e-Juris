<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041782155</ID>
<ANCIEN_ID>J4_L_2020_04_000001904099</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/78/21/CETATEXT000041782155.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de NANTES, 2ème chambre, 02/04/2020, 19NT04099, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-04-02</DATE_DEC>
<JURIDICTION>CAA de NANTES</JURIDICTION>
<NUMERO>19NT04099</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. PEREZ</PRESIDENT>
<AVOCATS>LE VERGER</AVOCATS>
<RAPPORTEUR>M. Thomas  GIRAUD</RAPPORTEUR>
<COMMISSAIRE_GVT>M. DERLANGE</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
       Procédure contentieuse antérieure :<br/>
<br/>
       Mme F... a demandé au tribunal administratif de Nantes d'annuler la décision du 28 juillet 2016 par laquelle la commission de recours contre les décisions de refus de visa d'entrée en France a rejeté son recours formé contre la décision des autorités consulaires françaises à Kinshasa du 28 avril 2016 refusant de délivrer à sa fille B... G... un visa de long séjour en France.<br/>
<br/>
       Par un jugement n° 1610658 du 28 mars 2019, le tribunal administratif de Nantes a rejeté sa demande.<br/>
<br/>
       Procédure devant la cour :<br/>
<br/>
       Par une requête enregistrée le 18 octobre 2019, Mme F..., représentée par Me E..., demande à la cour :<br/>
<br/>
       1°) d'annuler ce jugement du tribunal administratif de Nantes du 28 mars 2019 ;<br/>
<br/>
       2°) d'annuler la décision du  28 juillet 2016 de la commission de recours contre les décisions de refus de visa d'entrée en France ;<br/>
<br/>
       3°) d'enjoindre au ministre de l'intérieur de délivrer à l'enfant B... un visa de long séjour dans un délai de quarante-huit heures à compter de la notification de la décision à intervenir ou, à défaut, de procéder au réexamen de la demande de visa, sous astreinte de 200 euros par jour de retard ;<br/>
<br/>
       4°) de mettre à la charge de l'Etat le versement à son conseil, qui renonce à percevoir la part contributive de l'Etat au titre de l'aide juridictionnelle, d'une somme de 1 500 euros sur le fondement des dispositions combinées des articles L. 761-1 du code de justice administrative et 37 de la loi n° 91-647 du 10 juillet 1991.<br/>
       Elle soutient que :<br/>
       - la commission ne pouvait se fonder sur l'absence de déclaration sur ses enfants par la requérante, devant l'OFPRA, au moment de sa demande d'asile ;<br/>
- la décision attaquée est entachée d'erreur d'appréciation ;<br/>
- la filiation est établie par la possession d'état ;<br/>
       - la décision attaquée a été prise en violation de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
       - la décision attaquée a été prise en violation du 1° de l'article 3 de la convention internationale relative aux droits de l'enfant ; <br/>
       - la décision attaquée est entachée d'erreur manifeste d'appréciation quant à ses conséquences sur sa situation personnelle et celle de ses enfants. <br/>
<br/>
       Par un mémoire en défense, enregistré le 31 janvier 2020, le ministre de l'intérieur conclut au rejet de la requête.<br/>
<br/>
       Il soutient que les moyens soulevés par Mme F... ne sont pas fondés.<br/>
<br/>
<br/>
       Mme F... a été admise au bénéfice de l'aide juridictionnelle totale par une décision du 13 août 2019.<br/>
<br/>
<br/>
<br/>
       Vu les autres pièces du dossier.<br/>
<br/>
       Vu :<br/>
       - le code civil ;<br/>
       - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
       - la loi n° 91-647 du 10 juillet 1991 ;<br/>
       - le décret n° 91-1266 du 19 décembre 1991 ;<br/>
       - le code de justice administrative.<br/>
<br/>
<br/>
       Le président de la formation de jugement a dispensé le rapporteur public, sur sa proposition, de prononcer des conclusions à l'audience.<br/>
<br/>
<br/>
       Les parties ont été régulièrement averties du jour de l'audience.<br/>
<br/>
       Le rapport de M. C... a été entendu au cours de l'audience publique. <br/>
       Considérant ce qui suit :<br/>
       1. Mme F..., réfugiée congolaise, relève appel du jugement du 28 mars 2019 par lequel le tribunal administratif de Nantes a rejeté sa demande tendant à l'annulation de la décision du 28 juillet 2016  par laquelle la commission de recours contre les décisions de refus de visa d'entrée en France (CRV) a rejeté son recours formé contre la décision des autorités consulaires françaises à Kinshasa du 28 avril 2016 refusant de délivrer un visa de long séjour en France à sa fille B... G....<br/>
<br/>
       2. En premier lieu, si la requérante n'a pas déclaré l'enfant B... au moment de sa demande d'asile auprès de l'office français de protection des réfugiés et apatrides (OFPRA), elle indique dans ses écritures que de fausses informations lui avaient été données sur le décès de sa fille, en 2007, alors qu'elle était déjà sur le territoire français. En tout état de cause, l'absence de déclaration auprès de l'OFPRA de l'existence de sa fille ne la privait pas de la possibilité de solliciter, dans le cadre de la procédure de réunification familiale, une fois établi le lien de filiation avec sa fille, un visa de long séjour. Ainsi, en se fondant sur les dispositions de l'article R. 722-4 du code de l'entrée et du séjour des étrangers et du droit d'asile qui ne concernent que les pouvoirs du directeur de l'OFPRA concernant la certification de la situation de famille et de l'état civil des demandeurs d'asile, la CRV a entaché sa décision d'une erreur de droit.<br/>
       3. En deuxième lieu, aux termes de l'article L. 752-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " I.-Sauf si sa présence constitue une menace pour l'ordre public, le ressortissant étranger qui s'est vu reconnaître la qualité de réfugié ou qui a obtenu le bénéfice de la protection subsidiaire peut demander à bénéficier de son droit à être rejoint, au titre de la réunification familiale : / 1° Par son conjoint ou le partenaire avec lequel il est lié par une union civile, âgé d'au moins dix-huit ans, si le mariage ou l'union civile est antérieur à la date d'introduction de sa demande d'asile ; / 2° Par son concubin, âgé d'au moins dix-huit ans, avec lequel il avait, avant la date d'introduction de sa demande d'asile, une vie commune suffisamment stable et continue ; / 3° Par les enfants non mariés du couple, âgés au plus de dix-neuf ans. / Si le réfugié ou le bénéficiaire de la protection subsidiaire est un mineur non marié, il peut demander à bénéficier de son droit à être rejoint par ses ascendants directs au premier degré. / L'âge des enfants est apprécié à la date à laquelle la demande de réunification familiale a été introduite. / II.-Les articles L. 411-2 à L. 411-4 et le premier alinéa de l'article L. 411-7 sont applicables. / La réunification familiale n'est pas soumise à des conditions de durée préalable de séjour régulier, de ressources ou de logement. / Les membres de la famille d'un réfugié ou d'un bénéficiaire de la protection subsidiaire sollicitent, pour entrer en France, un visa d'entrée pour un séjour d'une durée supérieure à trois mois auprès des autorités diplomatiques et consulaires, qui statuent sur cette demande dans les meilleurs délais. / Pour l'application du troisième alinéa du présent II, ils produisent les actes de l'état civil justifiant de leur identité et des liens familiaux avec le réfugié ou le bénéficiaire de la protection subsidiaire. En l'absence d'acte de l'état civil ou en cas de doute sur leur authenticité, les éléments de possession d'état définis à l'article 311-1 du code civil et les documents établis ou authentifiés par l'Office français de protection des réfugiés et apatrides, sur le fondement de l'article L. 721-3 du présent code, peuvent permettre de justifier de la situation de famille et de l'identité des demandeurs. Les éléments de possession d'état font foi jusqu'à preuve du contraire. Les documents établis par l'office font foi jusqu'à inscription de faux. / La réunification familiale ne peut être refusée que si le demandeur ne se conforme pas aux principes essentiels qui, conformément aux lois de la République, régissent la vie familiale en France, pays d'accueil. / Est exclu de la réunification familiale un membre de la famille dont la présence en France constituerait une menace pour l'ordre public ou lorsqu'il est établi qu'il est instigateur, auteur ou complice des persécutions et atteintes graves qui ont justifié l'octroi d'une protection au titre de l'asile. ". <br/>
       4. L'article L. 111-6 du code de l'entrée et du séjour des étrangers et du droit d'asile prévoit, en son premier alinéa, que la vérification des actes d'état civil étrangers doit être effectuée dans les conditions définies par l'article 47 du code civil. L'article 47 du code civil dispose quant à lui que : " Tout acte de l'état civil des Français et des étrangers fait en pays étranger et rédigé dans les formes usitées dans ce pays fait foi, sauf si d'autres actes ou pièces détenus, des données extérieures ou des éléments tirés de l'acte lui-même établissent, le cas échéant après toutes vérifications utiles, que cet acte est irrégulier, falsifié ou que les faits qui y sont déclarés ne correspondent pas à la réalité ". <br/>
       5. Il résulte de ces dispositions que la force probante d'un acte d'état civil établi à l'étranger peut être combattue par tout moyen susceptible d'établir que l'acte en cause est irrégulier, falsifié ou inexact. En cas de contestation par l'administration de la valeur probante d'un acte d'état civil établi à l'étranger, il appartient au juge administratif de former sa conviction au vu de l'ensemble des éléments produits par les parties. <br/>
       6. Or, ni le fait que le jugement supplétif rendu par le tribunal pour enfants de Kinshasa le 6 novembre 2015 soit intervenu 12 ans après la naissance de l'enfant B... et que sur la base de celui-ci ait été établi l'acte de naissance n° 1809 dressé au centre d'état civil de la commune de Barumbu (ville de Kinshasa) le 17 décembre 2015, ni la circonstance, en la supposer établie, que la demande de jugement  supplétif ait été formée en vue des demandes de visa en litige ne sont, en eux-mêmes, de nature à caractériser une fraude. <br/>
       7. Il résulte de l'instruction que la commission de recours contre les refus de visa d'entrée en France n'aurait pas pris la même décision si elle ne s'était fondée que sur le motif selon lequel " les déclarations au recours de la réfugiée pour justifier de la déclaration tardive de B... à l'OFPRA supposée être la jumelle de John, mentionnent que le père biologique a produit un certificat de décès de l'enfant ", dès lors qu'il ressort des pièces du dossier que c'est à la suite d'informations erronées que Mme F... a cru que sa fille B... était décédée.<br/>
       8. Il résulte de ce qui précède que Mme F... est fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Nantes a rejeté sa demande.<br/>
Sur les conclusions à fin d'injonction, sous astreinte :<br/>
       9. Le présent arrêt, eu égard à ses motifs, et sous réserve d'un changement dans les circonstances de droit ou de fait, implique de délivrer à la jeune B... G... le visa de long séjour sollicité. Il y a lieu, par suite, d'enjoindre au ministre de l'intérieur de délivrer à l'intéressée le visa sollicité dans un délai d'un mois à compter de la notification du présent arrêt sans qu'il soit nécessaire d'assortir cette injonction d'une astreinte.<br/>
Sur les frais liés au litige :<br/>
       10. Mme F... a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions de l'article 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement de la somme de 1 500 euros à Me E... dans les conditions fixées à l'article 37 de la loi du 10 juillet 1991 et à l'article 108 du décret du 19 décembre 1991.<br/>
<br/>
DECIDE :<br/>
Article 1er : Le jugement du tribunal administratif de Nantes du 28 mars 2019 et la décision de la commission de recours contre les refus de visa d'entrée en France du 28 juillet 2016 sont annulés.<br/>
<br/>
Article 2 : Il est enjoint au ministre de l'intérieur de délivrer à Mme B... G..., sous réserve d'un changement dans les circonstances de droit ou de fait, un visa de long séjour dans le délai d'un mois à compter de la notification du présent arrêt.<br/>
<br/>
Article 3 : Le versement de la somme de 1 500 euros à Me E... est mis à la charge de l'Etat dans les conditions fixées à l'article 37 de la loi du 10 juillet 1991 et à l'article 108 du décret du 19 décembre 1991.<br/>
Article 4 : Le présent arrêt sera notifié à Mme D... F... et au ministre de l'intérieur. <br/>
<br/>
<br/>
<br/>
<br/>
       Délibéré après l'audience du 10 mars 2020, à laquelle siégeaient :<br/>
       -  M. Pérez, président de chambre,<br/>
       - M. A...'hirondel, premier conseiller,<br/>
       - M. C..., premier conseiller.<br/>
<br/>
<br/>
      Rendu public par mise à disposition au greffe de la juridiction le 2 avril 2020.<br/>
Le rapporteur,<br/>
T. C...<br/>
Le président,<br/>
A. PEREZ<br/>
Le greffier,<br/>
K. BOURON<br/>
<br/>
       La République mande et ordonne au ministre de l'intérieur en ce qui le concerne, et à tous huissiers de justice à ce requis en ce qui concerne les voies de droit commun contre les parties privées, de pourvoir à l'exécution de la présente décision.<br/>
<br/>
<br/>
<br/>
<br/>
6<br/>
N° 19NT04099<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
