<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034651750</ID>
<ANCIEN_ID>JG_L_2017_05_000000408921</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/65/17/CETATEXT000034651750.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 10/05/2017, 408921, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-05-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408921</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Etienne de Lageneste</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:408921.20170510</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 15 mars 2017 au secrétariat du contentieux du Conseil d'Etat, M. A... B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'instruction publiée le 18 juin 2015 sous la référence BOI-CF-INF-40-10-10-10 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - la décision du Conseil constitutionnel n° 2016-546 QPC du 24 juin 2016 ;<br/>
              - la loi n° 2012-354 du 14 mars 2012 ;<br/>
              - le code général des impôts ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Etienne de Lageneste, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 24 avril 2017, présentée par M. B....<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le premier alinéa de l'article 1741 du code général des impôts punit de d'emprisonnement et d'amende le fait de se soustraire frauduleusement à l'établissement ou au paiement total ou partiel des impôts, notamment par la dissimulation volontaire des sommes sujettes à l'impôt.<br/>
<br/>
              2. M. B...demande l'annulation pour excès de pouvoir de l'instruction BOI-CF-INF-40-10-10-10 publiée le 18 juin 2015, par laquelle l'administration a fait connaître son interprétation des dispositions de l'article 1741 du code général des impôts en ce qui concerne les éléments constitutifs du délit général de fraude fiscale.<br/>
<br/>
              3. Au soutien de sa requête, M. B...soulève un moyen unique, présenté dans un mémoire distinct, tiré de ce que les dispositions de l'article 1741 du code général des impôts, telles qu'interprétées par le Conseil constitutionnel dans sa décision n° 2016-546 QPC du 24 juin 2016, méconnaissent les principes de légalité des délits et des peines, de clarté et de précision de la loi, de prévisibilité juridique et de sécurité juridique, qui résultent de l'article 8 de la Déclaration des droits de l'homme et du citoyen de 1789.<br/>
<br/>
              4. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              5. En soutenant que l'article 1741 du code général des impôts méconnaît ce principe au motif qu'il ne s'applique qu'aux cas les plus graves de dissimulation frauduleuse de sommes soumises à l'impôt, ainsi que l'a jugé le Conseil constitutionnel au paragraphe 21 de la décision n° 2016-546 QPC, M. B...doit nécessairement être regardé comme critiquant seulement les mots " soit qu'il ait volontairement dissimulé une part des sommes sujettes à l'impôt " figurant dans la première phrase du premier alinéa de l'article 1741 du code général des impôts.<br/>
<br/>
              6. Ces mots de la première phrase du premier alinéa de l'article 1741 du code général des impôts ont été déclarés par le Conseil constitutionnel conformes à la Constitution, sous notamment la réserve énoncée au paragraphe 21, dans les motifs et le dispositif de sa décision n° 2016-546 QPC du 24 juin 2016. Aucun changement de circonstances n'est intervenu depuis cette décision. Par suite, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée. La requête de M. B... doit donc être rejetée.<br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>
<br/>                          D E C I D E :<br/>
                                        --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M.B....<br/>
<br/>
Article 2 : La requête de M. B... est rejetée.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. A...B...et au ministre de l'économie et des finances.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
