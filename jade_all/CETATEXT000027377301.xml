<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027377301</ID>
<ANCIEN_ID>JG_L_2013_04_000000366670</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/37/73/CETATEXT000027377301.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 29/04/2013, 366670, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-04-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366670</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SPINOSI</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:366670.20130429</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu l'ordonnance n° 1205429 du 21 février 2013, enregistrée le 7 mars 2013 au secrétariat du contentieux du Conseil d'Etat, par laquelle le tribunal administratif de Strasbourg, avant de statuer sur la demande de la société Wallach Energies tendant à la décharge de la contribution exceptionnelle assise sur la provision pour hausse des prix constituée par les entreprises du secteur pétrolier à laquelle elle a été assujettie au titre de l'exercice clos au 31 décembre 2010, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution de l'article 16 de la loi n° 2011-900 de finances rectificatives pour 2011 ;<br/>
<br/>
              Vu le mémoire, enregistré le 19 février 2013 au greffe du tribunal administratif de Strasbourg, présenté par la société Wallach Energies, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu la loi n° 2004-1484 du 30 décembre 2004, notamment son article 36 ;<br/>
<br/>
              Vu la loi n° 2011-900 du 29 juillet 2011 de finances rectificatives pour 2011, notamment son article 16 ; <br/>
<br/>
              Vu le code général des impôts ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, Maître des Requêtes, <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Spinosi, avocat de la société Wallach Energies ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 16 de la loi du 29 juillet 2011 de finances rectificatives pour 2011 : " I. - Les entreprises dont l'objet est d'effectuer la première transformation du pétrole brut ou de distribuer les carburants issus de cette transformation acquittent une contribution exceptionnelle assise sur la fraction excédant 100 000 euros du montant de la provision pour hausse des prix prévue au onzième alinéa du 5° du 1 de l'article 39 du code général des impôts et inscrite au bilan à la clôture de l'exercice ou à la clôture de l'exercice précédent si le montant correspondant est supérieur. / Le taux de la contribution est fixé à 15 %. La contribution est acquittée dans les sept mois de la clôture de l'exercice. Elle est liquidée, déclarée, recouvrée et contrôlée comme en matière de taxe sur le chiffre d'affaires et sous les mêmes garanties et sanctions. II. - Le I s'applique au titre du premier exercice clos à compter du 31 décembre 2010. " ;<br/>
<br/>
              Sur l'assiette de la contribution exceptionnelle :<br/>
<br/>
              3. Considérant que la société Wallach Energies soutient que l'assiette de la contribution exceptionnelle instituée par cette disposition, constituée par les provisions pour hausse des prix selon les modalités qu'elle fixe, ne reflète pas les capacités contributives des entreprises de transformation du pétrole brut et des entreprises de distribution de carburants issus de cette transformation et n'est pas en relation avec les finalités de ce prélèvement ; que la disposition litigieuse méconnaîtrait ainsi le principe d'égalité devant la loi, garanti par l'article 6 de la Déclaration des droits de l'homme et du citoyen de 1789, dès lors que cette contribution n'aurait pas pour objet de dissuader ces entreprises de constituer de telles provisions, qu'elle ne distinguerait pas les entreprises selon qu'elles disposent d'une grande ou d'une faible latitude de répercuter la hausse des prix des produits pétroliers sur leurs marges, que les provisions ne permettraient pas de saisir la réalité de la situation économique et financière d'une entreprise, et que le choix d'une telle assiette par le législateur créerait des différences de traitement non justifiées entre entreprises placées dans des situations identiques, selon qu'elles ont ou non pris, dans le passé, une décision de gestion de constituer des provisions pour hausse des prix ;<br/>
<br/>
              4. Considérant qu'aux termes de l'article 6 de la Déclaration des droits de l'homme et du citoyen de 1789, la loi " doit être la même pour tous, soit qu'elle protège, soit qu'elle punisse " ; que le principe d'égalité ne s'oppose ni à ce que législateur règle de façon différente des situations différentes ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général, pourvu que, dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la loi qui l'établit ; qu'il n'en résulte pas pour autant que le principe constitutionnel d'égalité oblige à traiter différemment des personnes se trouvant dans des situations différentes ;<br/>
<br/>
              5. Considérant, en premier lieu, que le législateur, en instituant la contribution exceptionnelle litigieuse a, ainsi qu'il ressort des travaux préparatoires, non seulement entendu mettre à contribution les entreprises du secteur pétrolier ayant, en raison de la forte augmentation des prix des produits pétroliers, réalisé des profits exceptionnels, mais aussi financer le coût pour le budget de l'Etat de la revalorisation des barèmes de frais kilométriques utilisés par de nombreux contribuables ; qu'il a ainsi poursuivi, en partie, un objectif de rendement ; que, par suite, la circonstance que la contribution exceptionnelle n'aurait pas pour objet de dissuader les entreprises de constituer des provisions pour hausse des prix est, par elle-même, sans incidence sur l'appréciation de la constitutionnalité de ce prélèvement ;<br/>
<br/>
              6. Considérant, en deuxième lieu, qu'une provision pour hausse des prix est une provision réglementée ayant un objet fiscal, consistant à atténuer l'effet de la hausse des prix d'une matière première sur les résultats imposables de l'année où elle est constatée dans les écritures comptables ; que son montant est en relation avec la valeur des stocks de l'entreprise concernée et, en règle générale, avec la taille de cette dernière ; qu'elle ne peut être constatée qu'en cas de hausse significative des prix d'une matière première, excédant 10 % sur deux années ; qu'elle représente un avantage de trésorerie pour l'entreprise, avant sa réintégration obligatoire dans les résultats, au plus tard lors du sixième exercice suivant son inscription ; qu'ainsi, alors même qu'elle résulte d'une décision de gestion passée, facultative pour l'entreprise, une provision pour hausse des prix n'est pas manifestement insusceptible de constituer l'assiette d'une imposition destinée à tirer les conséquences d'une hausse importante des prix des produits pétroliers sur les résultats des entreprises de ce secteur ; que, par suite, le choix du législateur d'asseoir la contribution exceptionnelle sur cette assiette n'a pas pour effet de créer, entre les entreprises soumises à cet impôt, des différences de traitement qui ne seraient pas en rapport avec les objectifs assignés à cette taxe ; que le principe constitutionnel d'égalité devant la loi n'imposant pas au législateur de traiter différemment des situations différentes, la contribution exceptionnelle n'est pas contraire à ce principe au seul motif qu'elle ne serait pas modulée en fonction du degré de latitude dont disposent les entreprises concernées pour répercuter la hausse des prix des produits pétroliers sur leurs marges ; <br/>
<br/>
              7. Considérant, enfin, qu'une entreprise qui décide de constater une telle provision n'est pas placée dans la même situation qu'une entreprise qui n'en constate pas ; que, par suite, l'assiette de la contribution exceptionnelle en litige n'a pas pour effet de traiter différemment des entreprises placées dans des situations économiques et financières identiques ;<br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que la question soulevée, en tant qu'elle fait valoir que la contribution exceptionnelle litigieuse méconnaîtrait, en raison de l'assiette sur laquelle elle est assise, le principe d'égalité devant la loi, ne présente pas un caractère sérieux ; <br/>
<br/>
              Sur les redevables de la contribution exceptionnelle :<br/>
<br/>
              9. Considérant qu'aux termes de l'article 13 de la Déclaration des droits de l'homme et du citoyen de 1789 : " Pour l'entretien de la force publique, et pour les dépenses d'administration, une contribution commune est indispensable : elle doit être également répartie entre tous les citoyens, en raison de leurs facultés " ; que cette exigence ne serait pas respectée si l'impôt faisait peser sur une catégorie de contribuables une charge excessive au regard de leurs facultés contributives ; qu'en vertu de l'article 34 de la Constitution, il appartient au législateur de déterminer, dans le respect des principes constitutionnels et compte tenu des caractéristiques de chaque impôt, les règles selon lesquelles doivent être appréciées les facultés contributives ; qu'en particulier, pour assurer le respect du principe d'égalité, il doit fonder son appréciation sur des critères objectifs et rationnels en fonction des buts qu'il se propose ; que cette appréciation ne doit cependant pas entraîner de rupture caractérisée de l'égalité devant les charges publiques ;<br/>
<br/>
              10. Considérant que la société Wallach Energies fait valoir que la contribution exceptionnelle, en raison de l'abattement à hauteur de 100 000 euros que prévoit la disposition litigieuse et du mécanisme de plafonnement dont les provisions pour hausses des prix font l'objet, en application de l'article 36 de la loi du 30 décembre 2004 de finances pour 2005, aurait des effets dégressifs, en faisant peser une charge fiscale proportionnellement plus importante sur les petites et moyennes entreprises indépendantes que sur les grandes entreprises du secteur pétrolier, lesquelles étaient pourtant prioritairement visées par ce prélèvement ; qu'ainsi, cette contribution méconnaîtrait manifestement le principe d'égalité devant les charges publiques garanti par l'article 13 de la Déclaration des droits de l'homme et du citoyen de 1789 ; <br/>
<br/>
              11. Considérant qu'aux termes de l'article 36 de la loi du 30 décembre 2004 de finances pour 2005, complétant le onzième alinéa du 5° du 1 de l'article 39 du code général des impôts et rendu applicable pour la détermination du résultat des exercices clos à compter du 22 septembre 2004 : " Le montant de la dotation à cette provision ne peut excéder 15 millions d'euros par période de douze mois, au titre de chaque exercice, majoré le cas échéant d'une fraction égale à 10 % de la dotation à cette provision déterminée dans les conditions prévues à la phrase précédente (...) " ; <br/>
<br/>
              12. Considérant qu'il résulte de ce qui a été dit ci-dessus que les grandes entreprises du secteur pétrolier se trouvent, du fait notamment du plafonnement de la provision pour hausse des prix résultant du dispositif de l'article 36 de la loi de finances pour 2005, placées dans une situation différente des petites et moyennes entreprises ; qu'elles ne peuvent, en particulier, bénéficier, dans la même proportion que ces dernières, de l'avantage résultant de la constitution de cette provision et des facilités de trésorerie en résultant ; que la disposition contestée exonère les petites entreprises de la contribution en ne taxant que la fraction des provisions excédant 100 000 euros ; qu'enfin, les différences de traitement résultant de la contribution sont temporaires, compte tenu du caractère exceptionnel de celle-ci ; que, par suite, si la combinaison de la disposition litigieuse et de l'article 36 de la loi de finances pour 2005 se traduit par une imposition proportionnellement plus lourde pour les entreprises de taille moyenne, l'ensemble du dispositif ne méconnaît pas de façon caractérisée le principe d'égalité devant les charges publiques ;<br/>
<br/>
              13. Considérant qu'il résulte de tout ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; que, par suite, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ; <br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le tribunal administratif de Strasbourg.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société Wallach Energies et au ministre de l'économie et des finances.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre et au tribunal administratif de Strasbourg.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
