<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043038325</ID>
<ANCIEN_ID>JG_L_2021_01_000000429996</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/03/83/CETATEXT000043038325.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 21/01/2021, 429996, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-01-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429996</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP BUK LAMENT - ROBILLOT</AVOCATS>
<RAPPORTEUR>M. David Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:429996.20210121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Société mutuelle d'assurance du bâtiment et des travaux publics (SMABTP) a demandé au tribunal administratif de Nouvelle-Calédonie la décharge des cotisations d'impôt sur les sociétés et d'impôt sur le revenu des valeurs mobilières dont elle s'est acquittée en Nouvelle-Calédonie au titre de l'année 2013. Par un jugement n° 1700257 du 14 décembre 2017, le tribunal administratif de Nouvelle-Calédonie a partiellement fait droit à sa demande en prononçant la décharge de la cotisation d'impôt sur le revenu des valeurs mobilières de l'année 2013 et a rejeté le surplus de ses conclusions. <br/>
<br/>
              Par un arrêt nos 18PA00484, 18PA00549 du 19 février 2019, la cour administrative d'appel de Paris a, sur appel du gouvernement de la Nouvelle-Calédonie, annulé ce jugement et remis à la charge de la société SMABTP la cotisation d'impôt sur le revenu des valeurs mobilières de l'année 2013.  <br/>
<br/>
              Par un pourvoi sommaire et deux mémoires complémentaires, enregistrés les 19 avril 2019, 19 juillet 2019 et, 8 janvier 2020, au secrétariat du contentieux du Conseil d'Etat, la SMABTP demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la Nouvelle-Calédonie la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Elle soutient que la cour administrative d'appel a entaché son arrêt : <br/>
              - d'irrégularité en procédant, sur la minute de l'arrêt, à la pseudonymisation du nom de son agent spécial en Nouvelle Calédonie ;<br/>
              - d'erreur de droit en jugeant que les dispositions de l'article Lp 15 du code des impôts de la Nouvelle-Calédonie devaient prévaloir sur les stipulations de la convention fiscale bilatérale conclue entre le gouvernement de la République française et le conseil de gouvernement de la Nouvelle-Calédonie ;<br/>
              - d'insuffisance de motivation, d'erreur de droit et d'inexacte qualification juridique des faits en jugeant, tant en ce qui concerne l'impôt sur les sociétés que l'impôt sur le revenu des valeurs mobilières, qu'elle devait être considérée comme ayant disposé, durant l'année 2013, en la personne de son agent spécial, d'un établissement stable en Nouvelle-Calédonie ;<br/>
              - d'erreur de droit en jugeant qu'elle devait être regardée comme une entreprise exploitée en Nouvelle-Calédonie au sens de l'article Lp 15 du code des impôts de la<br/>
Nouvelle-Calédonie alors qu'elle a son siège social en France.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention entre le gouvernement de la République française et le conseil de gouvernement de la Nouvelle-Calédonie, approuvée et publiée par la loi n° 83-676 du 26 juillet 1983 ; <br/>
              - le code des assurances ; <br/>
              - le code des impôts de la Nouvelle-Calédonie ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. David Moreau, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de la SMABTP et à la SCP Buk Lament - Robillot, avocat du gouvernement de la Nouvelle-Calédonie ;<br/>
<br/>
<br/>
              Vu la note en délibéré, enregistrée le 6 janvier 2021, présentée par le gouvernement de la Nouvelle-Calédonie ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la Société mutuelle d'assurance du bâtiment et des travaux publics (SMABTP) a demandé le 20 décembre 2016 à la direction des services fiscaux de la Nouvelle-Calédonie la décharge des cotisations d'impôt sur le revenu des valeurs mobilières et d'impôt sur les sociétés au titre respectivement de l'année 2013 et de son exercice clos en 2013. Le président du gouvernement de la Nouvelle-Calédonie a rejeté sa réclamation le 1er juin 2017. La SMABTP se pourvoit en cassation contre l'arrêt du 19 février 2019 par lequel la cour administrative d'appel de Paris a, d'une part, rejeté son appel formé contre le jugement du 14 décembre 2017 par lequel le tribunal administratif de Nouvelle-Calédonie a rejeté sa demande de décharge de la cotisation d'impôt sur les sociétés et, d'autre part, sur l'appel formé par le gouvernement de la Nouvelle-Calédonie, remis à sa charge la cotisation d'impôt sur le revenu des valeurs mobilières dont elle avait été déchargée par le même jugement.<br/>
<br/>
              Sur la régularité de la minute :<br/>
<br/>
              2. Le deuxième alinéa de l'article R. 741-2 du code de justice administrative dispose que : " La décision contient le nom des parties, l'analyse des conclusions et mémoires ainsi que les visas des dispositions législatives ou réglementaires dont elle fait application ".<br/>
<br/>
              3. Si la SMABTP soutient que l'arrêt attaqué est entaché d'irrégularité au motif que sa minute désigne son agent spécial en Nouvelle-Calédonie par l'initiale de son nom de famille et non par son nom complet, il résulte de l'article R. 741 2 du code de justice administrative, cité au point 2, que le nom des tiers au litige ne fait pas partie, à la différence du nom des parties, des mentions obligatoires qu'une décision de justice doit comporter à peine d'irrégularité. Par ailleurs, contrairement à ce que soutient la SMABTP, la désignation de son agent spécial par l'initiale de son nom de famille ne nuit pas à la compréhension de la décision et, par suite, n'empêche pas le juge de cassation d'exercer son contrôle. Le moyen doit donc être écarté. <br/>
<br/>
              Sur le bien-fondé des impositions :<br/>
<br/>
              4. L'article 5 de la convention fiscale entre le Gouvernement de la République française et le Conseil du gouvernement du territoire de la Nouvelle-Calédonie et dépendances approuvée par la loi du 26 juillet 1983, dispose que : " 1. Au sens de la présente convention, l'expression "établissement stable" désigne une installation fixe d'affaires par l'intermédiaire de laquelle une entreprise exerce tout ou partie de son activité (...) / 5. Nonobstant les dispositions des paragraphes 1 et 2, lorsqu'une personne - autre qu'un agent jouissant d'un statut indépendant auquel s'applique le paragraphe 6  - agit pour le compte d'une entreprise et dispose dans un territoire de pouvoirs qu'elle y exerce habituellement lui permettant de conclure des contrats au nom de l'entreprise, cette entreprise est considérée comme ayant un établissement stable dans ce territoire pour toutes les activités que cette personne exerce pour l'entreprise (...) / 6. Une entreprise n'est pas considérée comme ayant un établissement stable dans un territoire du seul fait qu'elle exerce son activité par l'entremise d'un courtier, d'un commissionnaire général ou de tout autre agent jouissant d'un statut indépendant, à condition que ces personnes agissent dans le cadre ordinaire de leur activité (...) ". Il résulte de ces dispositions que, pour avoir un établissement stable dans un territoire, une entreprise doit soit y disposer d'une installation fixe d'affaires par laquelle elle exerce tout ou partie de son activité, soit avoir recours à une personne non indépendante ayant le pouvoir d'y conclure des contrats au nom de l'entreprise. Toutefois, dans ce dernier cas, l'établissement stable n'est constitué que si cette personne utilise effectivement, de façon non occasionnelle, le pouvoir qui lui est ainsi dévolu.<br/>
<br/>
              5. Aux termes de l'article 7 de la convention fiscale entre le Gouvernement de la République française et le Conseil du gouvernement du territoire de la Nouvelle-Calédonie et dépendances : " 1. Les bénéfices d'une entreprise d'un territoire ne sont imposables que dans ce territoire, à moins que l'entreprise n'exerce son activité dans l'autre territoire par l'intermédiaire d'un " établissement stable " qui y est situé. Si l'entreprise exerce son activité d'une telle façon, les bénéfices de l'entreprise sont imposables dans l'autre territoire mais uniquement dans la mesure où ils sont imputables à cet établissement stable. (...) ".<br/>
<br/>
              6. Aux termes de l'article Lp 15 du code des impôts de la Nouvelle-Calédonie : " Les bénéfices passibles de l'impôt sur les sociétés sont déterminés en tenant compte uniquement des bénéfices réalisés par les entreprises exploitées, ou ayant leur siège social en Nouvelle-Calédonie, ainsi que de ceux dont l'imposition est attribuée à la Nouvelle-Calédonie par une convention fiscale. / La notion d'entreprise exploitée en Nouvelle-Calédonie s'entend de l'exercice habituel d'une activité qui peut soit s'effectuer dans le cadre d'un établissement autonome, soit être réalisée par l'intermédiaire de représentants dépendants économiquement ou juridiquement, soit résulter de la réalisation d'opérations formant un cycle commercial complet. Toutefois, en matière d'assurance, l'entreprise est considérée comme exploitée en Nouvelle-Calédonie pour les produits d'assurance qui sont commercialisés localement (...) ".<br/>
<br/>
              7.  En premier lieu, il ressort des énonciations de l'arrêt attaqué que la cour, après avoir analysé les stipulations du contrat conclu entre la SMABTP et son agent spécial définissant les missions de celui-ci, a jugé que ce dernier avait le pouvoir de conclure des contrats en Nouvelle-Calédonie au nom de la société. Par suite, les moyens tirés de ce que la cour aurait commis une erreur de droit et insuffisamment motivé son arrêt en ne caractérisant pas le pouvoir de son agent de conclure des contrats en son nom doivent être écartés.<br/>
<br/>
              8. En deuxième lieu, il ressort des énonciations de l'arrêt attaqué que la cour a, pour établir l'absence d'indépendance de l'agent spécial de la SMABTP, relevé qu'il proposait à la souscription des contrats de la société adaptés, sur instruction de cette dernière, aux spécificités locales, que le contrat le liant à la société définissait les critères qu'il devait appliquer pour la gestion des sinistres, qu'il était soumis à son pouvoir de contrôle et d'inspection, qu'il devait satisfaire à toute demande de communication de document de sa part et que sa rémunération était constituée de commissions sur les prestations réalisées complétées par une participation aux résultats de la société. En déduisant de l'ensemble de ces éléments que cet agent spécial ne jouissait pas d'un statut indépendant au sens de la convention fiscale et que la SMABTP devait donc être regardée comme disposant d'un établissement stable en Nouvelle-Calédonie, la cour n'a pas inexactement qualifié les faits qui lui étaient soumis. Si la société requérante se prévaut de ce que la rémunération de son agent était assurée par des commissions " brutes " qui laissaient à sa charge l'ensemble de ses frais d'exploitation, ce moyen, nouveau en cassation, est inopérant. <br/>
<br/>
              9. En dernier lieu, il ressort des énonciations de l'arrêt attaqué que si la cour a estimé pouvoir relever que l'imposition à l'impôt sur les sociétés des bénéfices de la SMABTP pouvait être fondée sur les dispositions de l'article Lp 15 du code des impôts de la Nouvelle-Calédonie relatives à l'imposition des produits d'assurance, elle a jugé que la SMABTP disposait, en la personne de son agent spécial, d'un établissement stable en<br/>
Nouvelle- Calédonie au sens de l'article 5 de la convention fiscale de 1983, pour en déduire qu'elle devait être imposée à l'impôt sur les sociétés sur ce territoire en application de l'article 7, paragraphe 1 de la même convention. Ce motif, dont il résulte de ce qui a été dit aux points 7 et 8 qu'il n'était entaché ni d'insuffisance de motivation, ni d'erreur de droit, ni d'erreur de qualification juridique des faits justifiait le rejet par la cour des conclusions présentées devant elle par la SMABTP au titre de l'impôt sur les sociétés. Par suite, le moyen tiré de l'inapplicabilité de l'article Lp 15 du code des impôts de la Nouvelle-Calédonie ne peut, en tout état de cause, qu'être écarté.<br/>
<br/>
              10. Il résulte de tout ce qui précède que la SMABTP n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du gouvernement de la Nouvelle-Calédonie qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu, dans les circonstances de l'espèce de mettre à la charge de la SMABTP la somme de 3 000 euros à verser à ce titre au gouvernement de la Nouvelle-Calédonie.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la SMABTP est rejeté.<br/>
Article 2 : La SMABTP versera la somme de 3 000 euros au gouvernement de la Nouvelle-Calédonie au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à la société mutuelle d'assurance du bâtiment et des travaux publics et au gouvernement de la Nouvelle-Calédonie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
