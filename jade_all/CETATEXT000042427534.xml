<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042427534</ID>
<ANCIEN_ID>JG_L_2020_10_000000432857</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/42/75/CETATEXT000042427534.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 14/10/2020, 432857, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432857</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE ; SCP BOUTET-HOURDEAUX ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Edouard Solier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:432857.20201014</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le médecin-conseil, chef de service de l'échelon local du service médical des Bouches-du-Rhône et la caisse primaire d'assurance maladie des Bouches-du-Rhône ont porté plainte contre M. B... A... devant la section des assurances sociales de la chambre disciplinaire de première instance de Provence-Alpes-Côte d'Azur et Corse de l'ordre des médecins. Par une première décision du 20 février 2017, la section des assurances sociales a, d'une part, infligé à M. A... la sanction de l'interdiction de donner des soins aux assurés sociaux pour une durée de six mois, dont trois mois assortis du sursis et, d'autre part, ordonné un supplément d'instruction pour déterminer le montant du trop-remboursé. Par une seconde décision du 3 mai 2017, la section des assurances sociales a condamné M. A... à reverser à la caisse primaire d'assurance maladie des Bouches-du-Rhône la somme de 11 860,33 euros.<br/>
<br/>
               Par une décision du 25 juin 2019, la section des assurances sociales du Conseil national de l'ordre des médecins a, d'une part, annulé la décision du 20 février 2017 seulement en tant qu'elle est relative à des actes prescrits dénoncés dans la plainte de la caisse primaire d'assurance maladie et, d'autre part, infligé à M. A... la même sanction qu'en première instance et l'a condamné à reverser la même somme à la caisse primaire d'assurance maladie des Bouches-du-Rhône.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 juillet et 5 août 2019 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge du médecin-conseil, chef de service de l'échelon local du service médical des Bouches-du-Rhône et de la caisse primaire d'assurance maladie des Bouches-du-Rhône la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la sécurité sociale ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Edouard Solier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange, de la Burgade, avocat de M. A... et à la SCP Boutet-Hourdeaux, avocat du médecin-conseil, chef de service de l'échelon local du service Médical des Bouches-du-Rhône et de la caisse primaire centrale d'assurance maladie des Bouches-du-Rhône ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que par deux décisions des 20 février et 3 mai 2017, la section des assurances sociales de la chambre disciplinaire de première instance de Provence-Alpes-Côte d'Azur et Corse de l'ordre des médecins, saisie de plaintes du médecin-conseil, chef de service de l'échelon local du service médical des Bouches-du-Rhône et de la caisse primaire d'assurance maladie des Bouches-du-Rhône, a infligé à M. A..., médecin spécialiste qualifié en radiologie, la sanction d'interdiction de donner des soins aux assurés sociaux pour une durée de six mois, dont trois mois assortis du sursis, et l'a condamné à reverser à la caisse primaire d'assurance maladie des Bouches-du-Rhône la somme de 11 860,33 euros. Par une décision du 25 juin 2019, la section des assurances sociales du Conseil national de l'ordre des médecins a, d'une part, annulé la décision du 20 février 2017 seulement en tant qu'elle est relative à des actes prescrits mentionnés par la plainte de la caisse primaire d'assurance maladie et, d'autre part, infligé à M. A... la même sanction. Elle l'a par ailleurs, condamné à reverser la même somme qu'en première instance. M. A... se pourvoit en cassation contre cette décision.<br/>
<br/>
              2. En premier lieu, pour écarter le moyen tiré de l'irrégularité des deux décisions de la section des assurances sociales de la chambre disciplinaire de première instance, la section des assurances sociales du Conseil national de l'ordre des médecins a pu, sans dénaturer les pièces du dossier ni entacher sa décision d'erreur de droit, se fonder sur le fait qu'aucune disposition n'impose que la notification des décisions de première instance soit revêtue des signatures du président de la formation de jugement et du secrétaire d'audience. Si elle a ensuite précisé que les minutes des décisions en cause portent la signature du président de la formation de jugement et que les exemplaires destinés au docteur A... comportent la signature de la secrétaire de la juridiction, cette motivation, qui est surabondante, ne peut être utilement critiquée en cassation.<br/>
<br/>
              3. En deuxième lieu, il résulte de la combinaison, d'une part, des dispositions du IV de l'article L. 315-1, du III de l'article R. 315-1 et de l'article R. 315-1-1 du code de la sécurité sociale et, d'autre part, de l'article L. 114-10 du même code, que l'exigence d'agrément et d'assermentation prévue par ce dernier article ne s'applique pas aux médecins-conseils pour l'exercice, sur le fondement du IV de l'article L. 315-1 du code de la sécurité sociale, de leur mission d'analyse de l'activité des professionnels de santé dispensant des soins aux bénéficiaires de l'assurance maladie. Dès lors, la section des assurances sociales du Conseil national de l'ordre des médecins n'a pas commis d'erreur de droit en écartant le moyen par lequel M. A... contestait la validité des éléments recueillis par les médecins-conseils ayant participé aux opérations de contrôle de son activité, faute d'agrément et d'assermentation de ces médecins-conseils.<br/>
<br/>
              4. En troisième lieu, en relevant que M. A... " a facturé 152 actes d'infiltration vertébrale avec guidage pour 125 patients sans apporter la preuve qu'il les aurait effectivement réalisés lui-même ", la section des assurances sociales s'est prononcée sur le grief tiré de que ces actes n'avaient pas été effectivement réalisés, grief qui était énoncé dans les plaintes soumises à la section des assurances sociales de la chambre disciplinaire de première instance. Il s'ensuit que le moyen tiré de la méconnaissance du principe du contradictoire et des droits de la défense en ce que la décision attaquée est fondée, pour infliger une sanction à M. A..., sur un grief sur lequel il n'a pas été mis à même de s'expliquer, doit être écarté.<br/>
<br/>
              5. En quatrième lieu, il ressort des termes mêmes de la décision attaquée, qui est suffisamment motivée, que si, compte tenu de la date de son dépôt, certains faits dénoncés dans la plainte de la caisse primaire d'assurance maladie des Bouches-du-Rhône étaient prescrits, les mêmes actes reprochés à M. A... étaient également dénoncés dans la plainte du médecin-conseil sans que ceux-ci ne fussent atteints par la prescription. Ainsi, la section des assurances sociales de la chambre disciplinaire nationale a pu, sans dénaturer les pièces du dossier ni entacher sa décision d'erreur de droit, condamner le praticien à reverser à la caisse primaire d'assurance maladie la même somme qu'en première instance, alors même qu'elle a également retenu que certains des faits dénoncés par cette caisse dans sa plainte étaient prescrits.<br/>
<br/>
              6. En cinquième lieu, si le législateur a prévu que la procédure d'analyse de l'activité se déroule dans le respect des droits de la défense, ce respect n'est pas une condition de recevabilité de la plainte devant les juridictions chargées du contrôle technique, dès lors que le respect des droits de la défense est, alors, assuré par l'application des règles de la procédure juridictionnelle. Par suite, les moyens relatifs à la régularité de l'enquête menée par le service médical présentés en appel par M. A... étaient inopérants, ainsi que l'a jugé la section des assurances sociales de la chambre disciplinaire nationale, qui n'a pas entaché sa décision d'insuffisance de motivation faute d'y avoir répondu.<br/>
<br/>
              7. En sixième lieu, la décision attaquée est suffisamment motivée en ce qui concerne les griefs reprochés à M. A... et les motifs pour lesquels ils ont été regardés comme établis.<br/>
<br/>
              8. En dernier lieu, la sanction infligée n'est pas hors de proportion avec les faits reprochés, eu égard, notamment, à leur nature et à leur nombre.<br/>
<br/>
              9. Il résulte de ce qui précède que le pourvoi de M. A... doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. Dans les circonstances de l'espèce, il n'y a pas lieu de mettre à la charge de M. A... une somme au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A... est rejeté.<br/>
Article 2 : Les conclusions présentées par le médecin-conseil, chef de service de l'échelon local du service médical des Bouches-du-Rhône et la caisse primaire d'assurance maladie des Bouches-du-Rhône au titre de l'article L.761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. A..., au médecin-conseil, chef de service de l'échelon local du service médical des Bouches-du-Rhône et à la caisse primaire d'assurance maladie des Bouches-du-Rhône. <br/>
Copie en sera adressée au Conseil national de l'ordre des médecins.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
