<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038135440</ID>
<ANCIEN_ID>JG_L_2019_02_000000418017</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/13/54/CETATEXT000038135440.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 08/02/2019, 418017, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418017</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Thomas Janicot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:418017.20190208</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif de Nîmes d'ordonner, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de la décision du 14 novembre 2017 par laquelle le président de la communauté de communes Beaucaire Terre d'Argence a prononcé sa révocation à compter du 1er janvier 2018.<br/>
<br/>
              Par une ordonnance n° 1800070 du 24 janvier 2018, le juge des référés du tribunal administratif de Nîmes a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 9 et 26 février 2018, au secrétariat du contentieux du Conseil d'État, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de la communauté de communes Beaucaire Terre d'Argence la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Janicot, auditeur, <br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M. A...et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la communauté de communes Beaucaire Terre d'Argence ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que, par un arrêté du 19 novembre 2015, le président de la communauté de communes Beaucaire Terre d'Argence a prononcé la révocation de M.A..., adjoint technique territorial, au motif qu'il avait volé des lots de sacs à déchets appartenant à cet établissement public. Par une ordonnance du 9 décembre 2015, le juge des référés du tribunal administratif de Nîmes a rejeté la demande de suspension de l'exécution de cette décision. Par un avis du 13 avril 2016, le conseil de discipline de recours de la région Midi-Pyrénées, saisi par M.A..., a préconisé qu'une sanction d'exclusion de ses fonctions pour une durée de six mois lui soit infligée. Le président de la communauté de communes, en application de l'article 91 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, selon lequel l'autorité territoriale ne peut prononcer, à l'encontre d'un fonctionnaire territorial, une sanction plus sévère que celle proposée par le conseil de discipline de recours, a pris, par un arrêté du 13 juin 2016, une sanction d'exclusion temporaire de six mois à compter du 1er juillet 2016. Par un jugement du 5 octobre 2017, le tribunal administratif de Nîmes a annulé cet avis au motif que la sanction n'était pas proportionnée à la gravité des faits reprochés à M.A.... A la suite de ce jugement, le président de la communauté de communes a prononcé, par un arrêté du 14 novembre 2017, la révocation de ce fonctionnaire. Par une ordonnance du 24 janvier 2018, le juge des référés du même tribunal a rejeté la demande de suspension de l'exécution de cette décision. M. A...se pourvoit en cassation contre cette ordonnance. <br/>
<br/>
              2. En premier lieu, aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              3. Saisi sur le fondement de l'article L. 521-1 du code de justice administrative d'une demande tendant à ce qu'il prononce, à titre provisoire et conservatoire, la suspension d'une décision administrative, le juge des référés procède, dans les plus brefs délais, à une instruction succincte, distincte de celle au vu de laquelle le juge saisi du principal statuera, pour apprécier si les préjudices que l'exécution de cette décision pourrait entraîner sont suffisamment graves et immédiats pour caractériser une situation d'urgence et si les moyens invoqués apparaissent, en cet état de l'instruction, de nature à faire naître un doute sérieux sur la légalité de la décision. Il se prononce par une ordonnance qui n'est pas revêtue de l'autorité de la chose jugée et dont il peut lui-même modifier la portée au vu d'un élément nouveau invoqué devant lui par toute personne intéressée. <br/>
<br/>
              4. Eu égard à la nature de l'office ainsi attribué au juge des référés, la circonstance qu'un juge a statué sur une première demande de suspension de l'exécution d'une décision en référé ne fait pas à elle seule obstacle à ce que ce même juge statue en cette même qualité sur une deuxième demande en référé du même requérant tendant à la suspension d'une décision prise par la même autorité administrative et produisant des effets similaires à la première. Par suite, M. A...n'est pas fondé à soutenir que l'ordonnance attaquée aurait été rendue en méconnaissance du principe d'impartialité dès lors qu'elle a été prise par le magistrat qui avait déjà rejeté, par une ordonnance du 19 novembre 2015, sa demande de suspension de l'exécution de la décision de révocation prise à son encontre par le président de la communauté de communes Beaucaire Terre d'Argence le 19 novembre 2015. <br/>
<br/>
              5. En deuxième lieu, aucune disposition législative ou réglementaire ni aucun principe, et notamment pas le principe d'impartialité eu égard au fait que l'article 89 de la loi du 26 janvier 1984 permet au maire d'infliger une sanction plus sévère que celle préconisée par le conseil de discipline régional de recours mais distincte de la révocation, ne fait obstacle à ce que le magistrat, qui a présidé la formation de jugement ayant prononcé l'annulation de l'avis de cette instance de recours au motif que la sanction proposée par l'instance de recours n'était pas proportionnée à la gravité des fautes commises, siège en qualité de juge des référés pour statuer sur le litige qui lui a été soumis par un agent public, qui, à la suite de ce jugement, a fait l'objet d'une mesure identique à la première sanction. <br/>
<br/>
              6. M. A...soutient que la circonstance que le juge des référés a participé à la formation de jugement ayant statué le 5 octobre 2017 sur la demande d'annulation présentée par la communauté de communes à l'encontre de l'avis du 13 avril 2016 méconnaîtrait les exigences du principe d'impartialité. Toutefois, il ressort des pièces du dossier soumis au juge des référés que le jugement du 5 octobre 2017 ne s'est prononcé que sur la proportionnalité de la mesure d'exclusion temporaire de six mois proposée par le conseil de discipline de recours de la région Midi-Pyrénées et non sur la proportionnalité de la mesure de révocation de M. A...prise par le président de la communauté de communes dont la suspension était demandée au juge des référés. Par suite, alors même que le tribunal administratif s'était prononcé dans son jugement sur la matérialité des faits reprochés à M. A...et sur l'intention frauduleuse de ce dernier, le juge des référés, qui n'avait pas préjugé du litige, dès lors que l'annulation par le jugement du 5 octobre 2017 de l'avis du conseil de discipline et de recours du 13 avril 2016 laissait à l'administration une large marge d'appréciation quant à la nouvelle sanction disciplinaire, a régulièrement statué sur la demande de suspension.<br/>
<br/>
              7. En dernier lieu, compte-tenu de la gravité des faits reprochés à M.A..., c'est par une appréciation souveraine exempte de dénaturation que le juge des référés a estimé que le moyen tiré de ce que la sanction infligée par la communauté de communes Beaucaire Terre d'Argence à M. A...était disproportionnée avec les faits qui lui étaient reprochés n'était pas de nature à faire naître un doute sérieux quant sa légalité. <br/>
<br/>
              8. Il résulte de ce qui précède que le pourvoi de M. A...doit être rejeté, y compris, par voie de conséquence, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              9. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions de la communauté de communes Beaucaire Terre d'Argence présentées au titre des mêmes dispositions. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté. <br/>
Article 2 : Les conclusions de la communauté de communes Beaucaire Terre d'Argence présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. B...A...et à la communauté de communes Beaucaire Terre d'Argence.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
