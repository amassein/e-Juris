<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034293452</ID>
<ANCIEN_ID>JG_L_2017_03_000000394693</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/29/34/CETATEXT000034293452.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 27/03/2017, 394693, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394693</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Manon Perrière</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:394693.20170327</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Finorpa SCR a demandé au tribunal administratif de Lille la décharge en droits et pénalités des cotisations minimales de taxe professionnelle auxquelles elle a été assujettie au titre des années 2006, 2007 et 2008 et le versement d'intérêts moratoires. Par un jugement n° 1103460 du 27 mars 2014, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14DA00920 du 22 septembre 2015, la cour administrative d'appel de Douai a annulé ce jugement et fait droit à la demande de la société.<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés le 20 novembre 2015 et le 22 août 2016 au secrétariat du contentieux du Conseil d'Etat, le ministre des finances et des comptes publics demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - la loi n° 85-695 du 11 juillet ;<br/>
              - le règlement du comité de la réglementation bancaire n° 90-01 du 23 février 1990 ;<br/>
              - le règlement du comité de la réglementation bancaire n° 91-01 du 16 janvier 1991 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Manon Perrière, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société Finorpa SCR ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à l'issue d'une vérification de comptabilité, l'administration a estimé que la société Finorpa SCR, qui relève du régime des sociétés de capital-risque, était redevable de la cotisation minimale de taxe professionnelle assise sur la valeur ajoutée au titre des années 2006 à 2008. La société a demandé la décharge des impositions et pénalités mises à sa charge. Par un jugement du 27 mars 2014, le tribunal administratif de Lille a rejeté sa demande. Le ministre des finances et des comptes publics se pourvoit en cassation contre l'arrêt du 22 septembre 2015 par lequel la cour administrative d'appel de Douai a annulé ce jugement et fait droit à la demande de la société.<br/>
<br/>
              2. Aux termes de l'article 1-1 de la loi du 11 juillet 1985 portant diverses dispositions d'ordre économique et financier, dans sa rédaction alors applicable, les sociétés de capital-risque sont des sociétés par actions ayant pour objet social " la gestion d'un portefeuille de valeurs mobilières ". En vertu de l'article 1er de cette loi, elles doivent procéder à des investissements dans des sociétés non cotées pour pouvoir bénéficier d'un régime de faveur au regard de l'imposition des sociétés. Il en résulte que ces sociétés doivent être regardées comme exerçant à titre habituel une activité professionnelle au sens des dispositions, alors en vigueur, du I de l'article 1447 du code général des impôts relatives à l'assujettissement à la taxe professionnelle. <br/>
<br/>
              3. Aux termes de l'article 1647 E du même code alors applicable : " I. La cotisation de taxe professionnelle des entreprises dont le chiffre d'affaires est supérieur à 7 600 000 euros est au moins égale à 1,5 % de la valeur ajoutée produite par l'entreprise, telle que définie au II de l'article 1647 B sexies (...) ". Aux termes du II de l'article 1647 B sexies du même code alors applicable: " (...) 1. La valeur ajoutée (...) est égale à l'excédent hors taxe de la production sur les consommations de biens et services en provenance de tiers (...). / Pour la généralité des entreprises, la production de l'exercice est égale à la différence entre : / D'une part, les ventes, les travaux, les prestations de services ou les recettes ; les produits accessoires ; les subventions d'exploitation ; les ristournes, rabais et remises obtenus ; les travaux faits par l'entreprise pour elle-même ; les stocks à la fin de l'exercice ; / Et, d'autre part, les achats de matières et marchandises, droits de douane compris ; les réductions sur ventes ; les stocks au début de l'exercice. / Les consommations de biens et services en provenance de tiers comprennent : les travaux, fournitures et services extérieurs, à l'exception des loyers afférents aux biens pris en crédit-bail, ou des loyers afférents à des biens, visés au a du 1° de l'article 1467, pris en location par un assujetti à la taxe professionnelle pour une durée de plus de six mois ou des redevances afférentes à ces biens résultant d'une convention de location-gérance, les frais de transports et déplacements, les frais divers de gestion (...) / 3. La production des établissements de crédit, des entreprises ayant pour activité exclusive la gestion des valeurs mobilières est égale à la différence entre : / D'une part, les produits d'exploitation bancaires et produits accessoires ; / Et, d'autre part, les charges d'exploitation bancaires (...) ". Eu égard à l'objet de ces dispositions, qui est de tenir compte de la capacité contributive des entreprises en fonction de leur activité, les entreprises ayant pour activité exclusive la gestion de valeurs mobilières ne s'entendent, pour leur application, que des entreprises qui exercent cette activité pour leur propre compte. Les sociétés de capital-risque, qui gèrent pour leur propre compte des participations financières, sont, dès lors, soumises aux modalités de calcul de la valeur ajoutée prévues au 3 du II de l'article 1647 B sexies du code général des impôts pour les entreprises ayant pour activité exclusive la gestion de valeurs mobilières.<br/>
<br/>
              4. Les dispositions de l'article 1647 B sexies du code général des impôts fixent la liste limitative des catégories d'éléments comptables qui doivent être pris en compte dans le calcul de la valeur ajoutée servant de base à la cotisation minimale de taxe professionnelle. Pour l'application de ces dispositions, la production d'une entreprise ayant pour activité exclusive la gestion de valeurs mobilières doit être calculée, comme celle des établissements de crédit, en fonction des règles comptables fixées par le règlement du comité de la réglementation bancaire n° 91-01 du 16 janvier 1991 relatif à l'établissement et à la publication des comptes individuels annuels des établissements de crédit. En vertu de ces dispositions, les produits d'exploitation bancaires et produits accessoires incluent les " gains et pertes sur opérations des portefeuilles de négociation " et les " gains et pertes sur opérations des portefeuilles de placement et assimilés ", mais non les " gains et pertes sur actifs immobilisés ", ce dernier poste étant défini comme " le solde en bénéfice ou perte des opérations sur titres de participation, sur autres titres détenus à long terme et sur parts dans les entreprises liées ".<br/>
<br/>
              5. Pour l'application de l'article 1647 E du code général des impôts à une société de capital-risque, le seuil d'assujettissement à la cotisation minimale de taxe professionnelle doit aussi être calculé en ne tenant compte que des produits d'exploitation bancaires et des produits accessoires, lesquels ne comprennent pas, selon le règlement du comité de la réglementation bancaire n° 91-01 du 16 janvier 1991 applicable en l'espèce, les plus-values sur titres de participation et autres titres détenus à long terme.<br/>
<br/>
              6. L'article 9 bis du règlement du comité de la réglementation bancaire n° 90-01 du 23 février 1990 relatif à la comptabilisation des opérations sur titres des établissements de crédit définit, dans sa version applicable en l'espèce, les titres de l'activité de portefeuille comme ceux qui ont été acquis dans le cadre d'investissements " réalisés de façon régulière avec pour seul objectif d'en retirer un gain en capital à moyen terme sans intention d'investir durablement dans le développement du fonds de commerce de l'entreprise émettrice, ni de participer activement à sa gestion opérationnelle ", en ajoutant que " des titres ne peuvent être affectés à ce portefeuille que si cette activité, exercée de manière significative et permanente dans un cadre structuré, procure à l'établissement une rentabilité récurrente, provenant principalement des plus-values de cession réalisées " et en citant comme exemple " les titres détenus dans le cadre d'une activité de capital-risque ". Le même article dispose que relèvent de la catégorie des autres titres détenus à long terme " les investissements réalisés sous forme de titres dans l'intention de favoriser le développement de relations professionnelles durables en créant un lien privilégié avec l'entreprise émettrice, mais sans influence dans la gestion des entreprises dont les titres sont détenus en raison du faible pourcentage des droits de vote qu'ils représentent ". Enfin, selon les dispositions du même article, les titres de participation sont ceux " dont la possession durable est estimée utile à l'activité de l'entreprise, notamment parce qu'elle permet d'exercer une influence sur la société émettrice des titres, ou d'en assurer le contrôle ". Une telle utilité peut notamment être caractérisée si les conditions d'achat des titres en cause révèlent l'intention de l'acquéreur d'exercer une influence sur la société émettrice et lui donnent les moyens d'exercer une telle influence.<br/>
<br/>
              7. D'une part, les dispositions citées au point 6, élaborées pour la comptabilisation des titres détenus en propre par les établissements de crédit, sociétés de financement et organismes assimilés, déterminent, pour l'application des articles 1647 E et 1647 B sexies du code général des impôts, la répartition, entre les catégories qu'elles définissent, des titres détenus par les sociétés de capital-risque. Compte tenu de l'objet spécifique des sociétés de capital-risque, défini à l'article 1-1 de la loi du 11 juillet 1985 portant diverses dispositions d'ordre économique et financier, l'exemple des " titres détenus dans le cadre d'une activité de capital-risque " donné à la fin de l'alinéa définissant les titres de portefeuille ne doit pas être compris, contrairement à ce que soutient le ministre, comme excluant que les titres détenus par de telles sociétés puissent être, s'ils en remplissent les conditions, placés dans les catégories des titres de participation ou des autres titres détenus à long terme. Dès lors que les titres immobilisés de l'activité de portefeuille sont destinés à rester durablement au bilan d'une entreprise et n'ont ainsi habituellement pas vocation à être cédés à court terme, ils doivent être regardés comme relevant, pour l'application de ces dispositions, de cette dernière catégorie. Ainsi, en jugeant que les gains résultant de la cession ou de l'apport de titres immobilisés de l'activité de portefeuille ne devaient pas être pris en compte pour apprécier si le chiffre d'affaire d'une société de capital-risque excédait le seuil d'assujettissement à la cotisation minimale de taxe professionnelle tel que fixé par l'article 1647 E du code général des impôts, la cour n'a pas commis d'erreur de droit.<br/>
<br/>
              8. D'autre part, il ressort des pièces du dossier soumis aux juges du fond que les plus-values réalisées par la société Finorpa SCR lors de la cession des titres qu'elle avait reçus en apport à sa création en 2005 présentent le caractère de gains exceptionnels. Ainsi, en jugeant, pour les exclure du chiffre d'affaires et de la valeur ajoutée pour chacun des trois exercices concernés, que ces résultats présentaient un caractère exceptionnel, la cour, dont l'arrêt est suffisamment motivé, a exactement qualifié les faits de l'espèce.<br/>
<br/>
              9. Il résulte de ce qui précède que le ministre n'est pas fondé à demander l'annulation de l'arrêt attaqué.<br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à la société Finorpa SCR de la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre est rejeté.<br/>
<br/>
Article 2 : L'Etat versera la somme de 3 000 euros à la société Finorpa SCR.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'économie et des finances et à la société Finorpa SCR.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
