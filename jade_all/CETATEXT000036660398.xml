<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036660398</ID>
<ANCIEN_ID>JG_L_2018_02_000000418168</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/66/03/CETATEXT000036660398.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 23/02/2018, 418168, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-02-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418168</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:418168.20180223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au juge des référés du tribunal administratif de Nantes, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au préfet de la Loire-Atlantique de lui indiquer un lieu susceptible de l'accueillir dans un délai de 24 heures à compter de l'ordonnance à intervenir. Par une ordonnance n° 1801150 du 7 février 2018, le juge des référés du tribunal administratif de Nantes a rejeté cette demande.<br/>
<br/>
              Par une requête, enregistrée le 14 février 2018 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à ses conclusions présentées devant le juge des référés du tribunal administratif de Nantes ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 700 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - la condition d'urgence est remplie eu égard à la situation de détresse dans laquelle elle se trouve compte tenu de son absence de solution d'hébergement et de ressources alors qu'elle est isolée et enceinte de six mois ;<br/>
              - la carence caractérisée du préfet de la Loire-Atlantique porte, compte tenu de sa vulnérabilité et de son état de grossesse avancé et d'épuisement, une atteinte grave et manifestement illégale à une liberté fondamentale.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. A cet égard, il appartient au juge d'appel de prendre en considération les éléments recueillis par le juge du premier degré dans le cadre de la procédure écrite et orale qu'il a diligentée.<br/>
<br/>
              2. L'article L. 345-2 du code de l'action sociale et des familles prévoit que, dans chaque département, est mis en place, sous l'autorité du préfet, " un dispositif de veille sociale chargé d'accueillir les personnes sans abri ou en détresse ". L'article L. 345-2-2 de ce code dispose que : " Toute personne sans abri en situation de détresse médicale, psychique ou sociale a accès, à tout moment, à un dispositif d'hébergement d'urgence (...) ". Enfin, aux termes de l'article L. 121-7 du même code : " Sont à la charge de l'Etat au titre de l'aide sociale : (...) 8° Les mesures d'aide sociale en matière de logement, d'hébergement et de réinsertion, mentionnées aux articles L. 345-1 à L. 345-3 (...) ".<br/>
<br/>
              3. Il appartient aux autorités de l'Etat de mettre en oeuvre le droit à l'hébergement d'urgence reconnu par la loi à toute personne sans abri qui se trouve en situation de détresse médicale, psychique et sociale. Une carence caractérisée des autorités de l'Etat dans l'accomplissement de cette mission peut faire apparaître, pour l'application de l'article L. 521-2 du code de justice administrative, une atteinte manifestement illégale à une liberté fondamentale lorsqu'elle entraîne des conséquences graves pour la personne intéressée. Il incombe au juge des référés d'apprécier, dans chaque cas, les diligences accomplies par l'administration, en tenant compte des moyens dont elle dispose, ainsi que de l'âge, de l'état de santé et de la situation de famille de la personne intéressée. Les ressortissants étrangers qui font l'objet d'une obligation de quitter le territoire français ou dont la demande d'asile a été définitivement rejetée et doivent ainsi quitter le territoire en vertu des dispositions de l'article L. 743-3 du code de l'entrée et du séjour des étrangers et du droit d'asile, n'ayant pas vocation à bénéficier du dispositif d'hébergement d'urgence, une carence constitutive d'une atteinte grave et manifestement illégale à une liberté fondamentale ne saurait être caractérisée, à l'issue de la période strictement nécessaire à la mise en oeuvre de leur départ volontaire, qu'en cas de circonstances exceptionnelles.<br/>
<br/>
              4. Il résulte de l'instruction menée par le juge des référés du tribunal administratif de Nantes, d'une part, que la Cour nationale du droit d'asile a rejeté la demande de Mme B...A..., ressortissante nigériane, dirigée contre la décision de l'Office français de protection des réfugiés et des apatrides lui refusant le bénéfice du statut de réfugiée, d'autre part, que, par un arrêté du 29 janvier 2018, le préfet de la Loire-Atlantique a rejeté la demande de titre de séjour formée par Mme A...et a assorti ce refus d'une obligation de quitter le territoire français, en accordant à l'intéressée un délai de trente jours à compter de la notification de sa décision afin de quitter volontairement le territoire français. Si le délai ainsi fixé par cet arrêté, dont la date de notification à l'intéressée n'est d'ailleurs pas établie, n'est pas expiré à la date de la présente décision et si Mme A...fait valoir qu'elle est enceinte de six mois et fatiguée, l'instruction n'a pas établi que sa présence dans la rue ou les espaces publics aurait été signalée par les services publics ou par les associations humanitaires venant habituellement en aide aux personnes sans-abri, les éléments recueillis lors du signalement épisodique de sa situation au centre d'appel " 115 " apparaissant témoigner de ce que l'intéressée, qui a indiqué connaître à Nantes une personne qui l'a hébergée et entretenir des relations avec des proches en France, dont sa soeur à Melun, parvient à conserver un hébergement, bien que de façon précaire. Il résulte également de l'instruction menée par le juge des référés du tribunal administratif de Nantes que, pour faire face à l'insuffisance des places disponibles compte tenu de l'augmentation du nombre des demandes, l'Etat a recouru en Loire-Atlantique de façon importante à l'hébergement hôtelier, sans pour autant parvenir à répondre à l'ensemble des besoins les plus urgents.<br/>
<br/>
              5. Dans ces conditions, sur lesquelles Mme A...n'apporte en appel aucun élément nouveau, la situation de l'intéressée ne caractérise pas une carence de l'Etat constitutive d'une atteinte grave et manifestement illégale à une liberté fondamentale telle que mentionnée au point 3. Elle n'est, par suite, pas fondée à se plaindre de ce que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Nantes a rejeté sa demande. Il est, en conséquence, manifeste que son appel ne peut qu'être rejeté selon la procédure prévue par l'article L. 522-3 du code de justice administrative, y compris les conclusions présentées au titre des articles L. 761-1 du même code et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de Mlle A...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme B...A....<br/>
Copie en sera adressée au ministre d'Etat, ministre de l'intérieur et au préfet de la Loire-Atlantique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
