<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037605897</ID>
<ANCIEN_ID>JG_L_2018_11_000000423566</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/60/58/CETATEXT000037605897.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 12/11/2018, 423566</TITRE>
<DATE_DEC>2018-11-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423566</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Pauline Berne</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:423566.20181112</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un nouveau mémoire, enregistrés les 24 août et 3 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, M. B...K...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2018-677 du 30 juillet 2018 portant convocation des électeurs pour l'élection d'un délégué consulaire dans la circonscription électorale des Pays-Bas, modifié par le décret n° 2018-713 du 3 août 2018 ; <br/>
<br/>
              2°) d'enjoindre au ministre de l'Europe et des affaires étrangères d'attribuer le siège de délégué consulaire à Mme H...C..., candidate figurant en sixième position sur la liste " Ensemble, mieux vivre aux Pays-Bas ".<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - la loi n° 2013-659 du 22 juillet 2013 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Berne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Selon l'article 3 de la loi du 22 juillet 2013 relative à la représentation des Français établis hors de France, les conseillers consulaires exercent, notamment, une compétence consultative sur les questions consulaires ou d'intérêt général concernant les Français établis dans la circonscription consulaire. Selon l'article 40 de la même loi, les délégués consulaires, élus en même temps que les conseillers consulaires, complètent le corps électoral des sénateurs représentant les Français établis hors de France. Aux termes du second alinéa de l'article 28 de la même loi, relatif aux conseillers consulaires : " Dans les circonscriptions où l'élection a eu lieu à la représentation proportionnelle, le candidat venant sur une liste immédiatement après le dernier élu est appelé à remplacer, jusqu'au prochain renouvellement général, le conseiller consulaire élu sur cette liste dont le siège devient vacant pour quelque cause que ce soit, autre que l'annulation des opérations électorales ". Aux termes du premier alinéa de l'article 29 de la même loi : " En cas d'annulation des opérations électorales d'une circonscription ou lorsque les dispositions de l'article 28 ou, le cas échéant, celles de l'article 43 ne peuvent plus être appliquées, il est procédé à des élections partielles dans un délai de quatre mois ". Aux termes de l'article 30 de la même loi : " Les démissions des conseillers consulaires sont adressées à l'ambassadeur ou au chef de poste consulaire du chef-lieu de la circonscription électorale. / La démission est définitive dès sa réception par cette autorité, qui en informe immédiatement le ministre des affaires étrangères ". Aux termes de l'article 42 de la même loi : " Une fois les sièges de conseiller consulaire attribués, les sièges de délégué consulaire sont répartis entre les listes, dans les conditions prévues à l'article 27. Pour chacune d'elles, ils sont attribués dans l'ordre de présentation, en commençant par le premier des candidats non proclamé élu conseiller consulaire ". Enfin, l'article 43 de la même loi dispose que : " Par dérogation au second alinéa de l'article 28, le délégué consulaire venant sur une liste immédiatement après le dernier conseiller consulaire élu est appelé à remplacer, jusqu'au prochain renouvellement général, le conseiller consulaire élu sur cette liste dont le siège devient vacant pour quelque cause que ce soit, autre que l'annulation des opérations électorales. / Le candidat venant sur une liste immédiatement après le dernier délégué consulaire élu est appelé à remplacer, jusqu'au prochain renouvellement général, le délégué consulaire élu sur cette liste dont le siège devient vacant pour quelque cause que ce soit, autre que l'annulation des opérations électorales. / Lorsque les dispositions du deuxième alinéa du présent article ne peuvent plus être appliquées, il est fait application de l'article 29 ".<br/>
<br/>
              2. Il ressort des pièces du dossier qu'à l'issue du vote de 2014 des Français établis dans la circonscription consulaire d'Amsterdam, aux Pays-Bas, la liste " Ensemble, mieux vivre au Pays-Bas ", comportant onze membres ayant notamment pour objectif le partage des mandats entre les colistiers, a obtenu deux des cinq sièges de conseiller consulaire et le seul siège de délégué consulaire. En 2016, la démission des deux conseillers consulaires issus de cette liste a entraîné l'attribution de leurs sièges aux deux candidats figurant aux troisième et quatrième rangs sur la même liste. M.M..., candidat figurant en cinquième rang de cette liste est devenu délégué consulaire en remplacement du délégué consulaire élu en 2014 au troisième rang de la liste et ayant accédé au mandat de conseiller consulaire. En 2018, ces deux conseillers consulaires nommés en 2016 ont démissionné à leur tour. L'un des deux sièges de conseiller consulaire devenus, ainsi, vacants a été attribué à M.M..., candidat figurant au cinquième rang de la même liste, alors titulaire du mandat de délégué consulaire. En revanche, les candidats placés du sixième au dixième rang de cette liste ont, successivement, démissionné du mandat de conseiller consulaire qui leur a été attribué dans le cadre de la procédure de remplacement ou l'ont refusé, de telle sorte que l'autre siège de conseiller consulaire devenu vacant a été attribué au candidat figurant en onzième et dernière position sur la liste. Le mandat du délégué consulaire tenu par M.M..., candidat placé en cinquième position de cette liste, étant devenu vacant par sa nomination, en 2018, en tant que conseiller consulaire, le Premier ministre, par le décret attaqué, a convoqué les électeurs de la circonscription électorale des Pays-Bas pour l'élection d'un délégué consulaire. M.K..., candidat en tête de la liste " Ensemble, mieux vivre au Pays-Bas " lors des élections de 2014, demande l'annulation de ce décret et qu'il soit enjoint au ministre de l'Europe et des affaires étrangères d'appeler Mme C..., placée en sixième position sur cette liste, à remplacer M.M..., cinquième de liste, en qualité de délégué consulaire.<br/>
<br/>
              Sur la recevabilité de la requête :<br/>
<br/>
              3. M.K..., dont il n'est pas contesté qu'il est inscrit sur la liste électorale de la circonscription consulaire d'Amsterdam, aux Pays-Bas, justifie d'un intérêt lui donnant qualité pour demander l'annulation du décret attaqué dont l'objet est d'organiser une consultation électorale dans cette circonscription. Par suite, la fin de non-recevoir opposée par le ministre de l'Europe et des affaires étrangères et tirée de ce que le requérant, qui a démissionné en 2016 de son mandat de conseiller consulaire, n'aurait pas intérêt à demander l'annulation du décret qu'il attaque, doit être écartée.<br/>
<br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              4. Il résulte des dispositions rappelées au point 1 que le remplacement des conseillers consulaires ou des délégués consulaires, en cas de vacance pour toute cause autre que l'annulation des opérations électorales, est organisé à partir des listes de candidats dans les circonscriptions où l'élection a eu lieu à la représentation proportionnelle. Il résulte de ces mêmes dispositions que tant pour l'élection que pour le remplacement, les mandats de conseiller consulaire et de délégué consulaire, dont l'objet est différent, sont soumis à des régimes distincts. En particulier, le remplacement dans le mandat de délégué consulaire est soumis au deuxième alinéa de l'article 43 de la loi du 22 juillet 2013 qui prévoit qu'il est fait appel, sous les conditions qui y sont posées, au candidat figurant, sur la même liste, immédiatement après le dernier délégué consulaire élu, indépendamment de l'utilisation qui a pu être faite de cette liste pour pourvoir, par ailleurs, au remplacement des conseillers consulaires conformément à l'article 42 de la même loi.<br/>
<br/>
              5. Or, les candidats de la liste " Ensemble, mieux vivre au Pays-Bas " présentée aux suffrages en 2014, qui n'ont pas perdu cette qualité par leurs démissions ou refus touchant l'exercice du mandat de conseiller consulaire, sont encore en nombre suffisant pour pourvoir conformément à l'article 43 de la loi du 22 juillet 2013, dans l'ordre de cette liste, au remplacement rendu nécessaire en 2018 dans le mandat de délégué consulaire. Dès lors, le décret attaqué ne pouvait pas légalement tirer la conséquence qu'il était nécessaire d'organiser l'élection d'un délégué consulaire de la circonscription consulaire d'Amsterdam, aux Pays-Bas, du motif que le dernier conseiller consulaire élu de la liste " Ensemble, mieux vivre au Pays-Bas " était le onzième et dernier candidat de cette liste. Par suite, le décret attaqué doit être annulé.<br/>
<br/>
              Sur les conclusions accessoires à fin d'injonction :<br/>
<br/>
              6. L'annulation du décret attaqué implique nécessairement que Mme C..., candidate en sixième position sur la liste " Ensemble, mieux vivre au Pays-Bas ", soit appelée à remplacer, jusqu'au prochain renouvellement général, M. M..., délégué consulaire élu, candidat en cinquième position sur cette liste, dont le siège est devenu vacant en juillet 2018 du fait de son acceptation du mandat de conseiller consulaire. Il y a lieu pour le Conseil d'Etat d'ordonner que cette mesure soit prise dans un délai d'un mois.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le décret n° 2018-677 du 30 juillet 2018 est annulé.<br/>
Article 2 : Il est enjoint au ministre de l'Europe et des affaires étrangères de prendre les mesures nécessaires pour que MmeC..., candidate en sixième position sur la liste " Ensemble, mieux vivre au Pays-Bas ", soit appelée, dans le délai d'un mois à compter de la notification de la présente décision, à devenir déléguée consulaire. <br/>
Article 3 : La présente décision sera notifiée à M. B...K...et au ministre de l'Europe et des affaires étrangères. <br/>
Copie en sera adressée au Premier ministre, à Mme H...C..., à Mme A...D..., à Mme L...E..., épouseN..., à Mme A...I...et à Mme J...G..., épouseF....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-07 ÉLECTIONS ET RÉFÉRENDUM. ÉLECTIONS DIVERSES. - REPRÉSENTATION DES FRANÇAIS ÉTABLIS HORS DE FRANCE - ELECTION ET REMPLACEMENT DES CONSEILLERS CONSULAIRES ET DES DÉLÉGUÉS CONSULAIRES DANS LES CIRCONSCRIPTIONS POURVUES À LA REPRÉSENTATION PROPORTIONNELLE (ART. 3, 28, 29, 30, 42 ET 43 DE LA LOI DU 22 JUILLET 2013) - 1) PRINCIPE - RÉGIMES DISTINCTS ENTRE CES DEUX MANDATS - EXISTENCE - CONSÉQUENCE - REMPLACEMENT DU DÉLÉGUÉ CONSULAIRE PAR LE CANDIDAT FIGURANT, SUR LA MÊME LISTE, IMMÉDIATEMENT APRÈS LE DERNIER DÉLÉGUÉ CONSULAIRE ÉLU - EXISTENCE, INDÉPENDAMMENT DE L'UTILISATION DE CETTE LISTE POUR LE REMPLACEMENT DES CONSEILLERS CONSULAIRES - 2) APPLICATION - DÉCRET CONVOQUANT LES ÉLECTEURS POUR L'ÉLECTION D'UN NOUVEAU DÉLÉGUÉ CONSULAIRE, FAUTE DE POUVOIR REMPLACER LE DÉLÉGUÉ DÉMISSIONNAIRE PAR UN SUIVANT DE LISTE - ILLÉGALITÉ À AVOIR DÉDUIT L'IMPOSSIBILITÉ DU REMPLACEMENT DE LA CIRCONSTANCE QUE LE DERNIER CANDIDAT DE CETTE LISTE AVAIT ÉTÉ DÉSIGNÉ CONSEILLER CONSULAIRE - EXISTENCE.
</SCT>
<ANA ID="9A"> 28-07 1) Il résulte des articles 3, 28, 29, 30, 42 et 43 de la loi n° 2013-659 du 22 juillet 2013 que le remplacement des conseillers consulaires ou des délégués consulaires, en cas de vacance pour toute cause autre que l'annulation des opérations électorales, est organisé à partir des listes de candidats dans les circonscriptions où l'élection a eu lieu à la représentation proportionnelle. Il résulte de ces mêmes articles que tant pour l'élection que pour le remplacement, les mandats de conseiller consulaire et de délégué consulaire, dont l'objet est différent, sont soumis à des régimes distincts. En particulier, le remplacement dans le mandat de délégué consulaire est soumis au deuxième alinéa de l'article 43 de la loi du 22 juillet 2013 qui prévoit qu'il est fait appel, sous les conditions qui y sont posées, au candidat figurant, sur la même liste, immédiatement après le dernier délégué consulaire élu, indépendamment de l'utilisation qui a pu être faite de cette liste pour pourvoir, par ailleurs, au remplacement des conseillers consulaires conformément à l'article 42 de la même loi.,,,2) Démissions successives de conseillers consulaires, ayant conduit pour leur remplacement à désigner le dernier candidat d'une liste et entraînant par suite la vacance d'un mandat de délégué consulaire, élu sur cette même liste. Décret portant convocation des électeurs pour l'élection d'un nouveau délégué consulaire.... ...Les candidats de la liste concernée, qui n'ont pas perdu cette qualité par leurs démissions ou refus touchant l'exercice du mandat de conseiller consulaire, sont encore en nombre suffisant pour pourvoir conformément à l'article 43 de la loi du 22 juillet 2013, dans l'ordre de cette liste, au remplacement rendu nécessaire dans le mandat de délégué consulaire. Dès lors, le décret ne peut légalement tirer la conséquence qu'il est nécessaire d'organiser l'élection d'un délégué consulaire, du motif que le dernier conseiller consulaire élu de la liste est le dernier candidat de cette liste. Par suite, annulation.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
