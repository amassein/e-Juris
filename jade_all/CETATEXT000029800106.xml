<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029800106</ID>
<ANCIEN_ID>JG_L_2014_10_000000374462</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/80/01/CETATEXT000029800106.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème SSJS, 15/10/2014, 374462, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-10-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374462</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; BOUTHORS</AVOCATS>
<RAPPORTEUR>Mme Charline Nicolas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:374462.20141015</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 7 janvier et 8 avril 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour le syndicat intercommunal de collecte et de traitement des eaux usées (SICTEU) de la région de Soultz-sous-Forêts, dont le siège est 14 rue des Eglises à Hoffen (67250) ; le SICTEU de la région de Soultz-sous-Forêts demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 12NC01498 du 7 novembre 2013 par lequel la cour administrative d'appel de Nancy a, sur la requête de la société TST-Robotics, en premier lieu, annulé le jugement n° 0702508 du 6 juillet 2012 par lequel le tribunal administratif de Strasbourg a rejeté sa demande tendant à la condamnation du SICTEU de la région de Soultz-sous-Forêts à lui payer, suite à son éviction irrégulière lors de la passation d'un marché public, à titre principal, une somme de 62 941 euros correspondant au manque à gagner qu'elle a subi ou, à titre subsidiaire, une somme de 2 313,16 euros correspondant aux frais engagés pour présenter son offre, ces sommes portant intérêts au taux légal à compter du 16 mai 2007 et ces intérêts étant capitalisés à compter du 16 mai 2008, et, en second lieu, condamné le SICTEU de la région de Soultz-sous-Forêts à verser à la société TST-Robotics la somme de 10 000 euros avec intérêts au taux légal à compter du 16 mai 2007, les intérêts échus le 16 mai 2008 étant capitalisés à cette date ainsi qu'à chaque échéance annuelle à compter de celle-ci pour produire eux-mêmes intérêts ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la requête d'appel de la société TST-Robotics ;<br/>
<br/>
              3°) de mettre à la charge de la société TST-Robotics le versement de la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des marchés publics ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Charline Nicolas, auditeur,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat du syndicat intercommunal de collecte et de traitement des eaux usées de la région de Soultz-sous-Forêts, et à Me Bouthors, avocat de la société TST-Robotics ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que par un avis d'appel public à la concurrence publié le 19 janvier 2007, le syndicat intercommunal de collecte et de traitement des eaux usées (SICTEU) de la région de Soultz-sous-Forêts a ouvert une procédure négociée de passation d'un marché de travaux " Assainissement général - plan pluriannuel - programme 2007 " ; que le SICTEU de la région de Soultz-sous-Forêts se pourvoit en cassation contre l'arrêt du 7 novembre 2013 par lequel la cour administrative d'appel de Nancy l'a condamné à verser à la société TST-Robotics la somme de 10 000 euros avec intérêts au taux légal à compter du 16 mai 2007 en réparation du préjudice subi du fait de son éviction irrégulière du marché litigieux, attribué à la société Phoenix ; <br/>
<br/>
              2. Considérant que lorsqu'un candidat à l'attribution d'un contrat public demande la réparation du préjudice qu'il estime avoir subi du fait de l'irrégularité ayant, selon lui, affecté la procédure ayant conduit à son éviction, il appartient au juge, si cette irrégularité est établie, de vérifier quelle est la cause directe de l'éviction du candidat et, par suite, qu'il existe un lien direct de causalité entre la faute en résultant et le préjudice dont le candidat demande l'indemnisation ; <br/>
<br/>
              3. Considérant que la cour administrative d'appel de Nancy a relevé qu'il résultait de l'instruction que la procédure de passation du lot n° 1 du marché avait méconnu le principe d'égalité de traitement des candidats à l'attribution d'un marché public faute, pour le maître d'ouvrage et le maître d'oeuvre d'avoir sollicité, en application de l'article 55 du code des marchés publics, des justifications auprès de la société Phoenix sur le montant du prix figurant, après négociation, dans sa dernière offre, alors même que ce nouveau prix était inférieur à celui qui figurait dans la première offre de la société TST-Robotics, et qui avait elle-même fait l'objet d'une demande de justification en application de ces dispositions ; qu'en jugeant toutefois que cette illégalité était directement à l'origine d'une perte de chance sérieuse par la société TST-Robotics d'obtenir le marché quand bien même son offre était, selon ses énonciations, la plus avantageuse avant la négociation ayant permis à l'attributaire de baisser ses prix, la cour a commis une erreur de qualification juridique ;  <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, que le SICTEU de la région de Soultz-sous-Forêts est fondé à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du SICTEU de la région de Soultz-sous-Forêts qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, de mettre à la charge de la société TST-Robotics la somme de 3 500 euros au titre des frais exposés par le SICTEU de la région de Soultz-sous-Forêts et non compris dans les dépens, en application des dispositions de l'article L. 761-1 du code de justice administrative ;  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 7 novembre 2013 de la cour administrative d'appel de Nancy est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy.<br/>
Article 3 : La société TST-Robotics versera une somme de 3 500 euros au SICTEU  de la région de Soultz-sous-Forêts en application de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Les conclusions de la société TST-Robotics présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée au syndicat intercommunal de collecte et de traitement des eaux usées de la région de Soultz-sous-Forêts et à la société TST-Robotics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
