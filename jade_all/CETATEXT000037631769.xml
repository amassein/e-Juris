<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037631769</ID>
<ANCIEN_ID>JG_L_2018_11_000000418218</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/63/17/CETATEXT000037631769.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 21/11/2018, 418218, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-11-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418218</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>M. Jean-Baptiste de Froment</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:418218.20181121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Toulouse d'annuler pour excès de pouvoir l'arrêté du 28 octobre 2016 par lequel le préfet de la Haute-Garonne a rejeté sa demande de renouvellement de titre de séjour, l'a obligé à quitter le territoire français dans un délai de trente jours et a fixé le pays de renvoi. Par un jugement n° 1605642 du 1er juin 2017, le tribunal administratif de Toulouse a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 17BX02099 du 2 novembre 2017, la présidente de la cour administrative d'appel de Bordeaux a rejeté l'appel formé par M. A...contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 15 février et 9 mars 2018 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de renvoyer l'affaire devant la cour administrative d'appel de Bordeaux ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros à verser à la SCP Coutard et Munier-Apaire, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Baptiste de Froment, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat de M.A....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B...A..., de nationalité djiboutienne, a séjourné en France régulièrement entre le 11 septembre 2012 et le 30 septembre 2016 sous couvert de titres de séjour portant la mention " étudiant ". Par un arrêté du 28 octobre 2016, le préfet de la Haute-Garonne a refusé de faire droit à sa demande de renouvellement de titre de séjour en raison du défaut de caractère réel et sérieux de ses études, l'a obligé à quitter le territoire français dans un délai de trente jours et a fixé le pays de renvoi. Par un jugement du 1er juin 2017, le tribunal administratif de Toulouse a rejeté le recours pour excès de pouvoir formé par M. A...contre cet arrêté. La présidente de la cour administrative d'appel de Bordeaux a rejeté son appel contre ce jugement par une ordonnance du 2 novembre 2017 contre laquelle M. A...se pourvoit en cassation. <br/>
<br/>
              2. En premier lieu, il ressort des pièces du dossier soumis aux juges du fond que, par un mémoire complémentaire enregistré le 18 septembre 2017, M. A...soutenait que l'arrêté litigieux était insuffisamment motivé en ce que le préfet n'aurait pas indiqué " le texte ou la base duquel ont pu être retenus ces trois critères cumulatifs d'appréciation du sérieux des études ". Si cet argument était expressément présenté comme un moyen nouveau, il ne faisait que développer le moyen d'insuffisance de motivation de l'arrêté déjà soulevé dans les écritures de première instance et dans le premier mémoire d'appel, auquel l'ordonnance litigieuse répond en reprenant les motifs du jugement de première instance, qui y avait expressément répondu. Par suite, le moyen tiré de ce que ce que l'ordonnance attaquée n'aurait pas répondu au moyen nouveau soulevé par le second mémoire d'appel du requérant, enregistré le 17 septembre 2017, ne peut qu'être écarté.<br/>
<br/>
              3. En second lieu, contrairement à ce qui est soutenu, en relevant que, " le requérant ne se prévaut d'aucun élément de fait ou de droit nouveau par rapport à l'argumentation développée devant le tribunal administratif " et en estimant qu'" il ne ressort pas des pièces du dossier que les premiers juges auraient, par les motifs qu'ils ont retenus et qu'il y a lieu d'adopter, commis une erreur en écartant ces moyens ", pour juger la requête d'appel manifestement dépourvue de fondement, la présidente de la cour administrative d'appel de Bordeaux a suffisamment motivé son ordonnance.  <br/>
<br/>
              4. Il résulte de tout ce qui précède que M. A...n'est pas fondé à demander l'annulation de l'ordonnance qu'il attaque. Par suite, son pourvoi, y compris ses conclusions tendant à l'application des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, doit être rejeté.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
