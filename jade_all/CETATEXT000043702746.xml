<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043702746</ID>
<ANCIEN_ID>JG_L_2021_06_000000448417</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/70/27/CETATEXT000043702746.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 24/06/2021, 448417</TITRE>
<DATE_DEC>2021-06-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448417</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Marc Pichon de Vendeuil</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:448417.20210624</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au juge des référés du tribunal administratif de Montreuil de suspendre, sur le fondement de l'article L. 521-1 du code de justice administrative, l'exécution de la décision du 23 septembre 2020 par laquelle le garde des sceaux, ministre de la justice a procédé à son licenciement pour cause disciplinaire et de lui enjoindre de le réintégrer dans un délai de quinze jours. <br/>
<br/>
              Par une ordonnance n° 2012904 du 21 décembre 2020, le juge des référés a fait droit à sa demande.<br/>
<br/>
              Par un pourvoi et un nouveau mémoire, enregistrés les 5 janvier et 11 mars 2021 au secrétariat du contentieux du Conseil d'Etat, le ministre de la justice demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la requête de M. B....<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le décret n° 86-83 du 17 janvier 1986 ;<br/>
              - le décret n° 2019-797 du 26 juillet 2019 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Marc Pichon de Vendeuil, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Foussard, Froger, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur la compétence territoriale du tribunal administratif de Montreuil :<br/>
<br/>
              1. Aux termes du second alinéa de l'article R. 312-2 du code de justice administrative : " Lorsqu'il n'a pas été fait application de la procédure de renvoi prévue à l'article R. 351-3 et que le moyen tiré de l'incompétence territoriale du tribunal administratif n'a pas été invoqué par les parties avant la clôture de l'instruction de première instance, ce moyen ne peut plus être ultérieurement soulevé par les parties ou relevé d'office par le juge d'appel ou de cassation ". Il résulte de ces dispositions que le moyen tiré de de ce que le tribunal administratif de Montreuil n'était pas le tribunal territorialement compétent pour connaître de la demande de suspension de M. B..., soulevé pour la première fois devant le juge de cassation par le garde des sceaux, ministre de la justice, est inopérant.<br/>
<br/>
              Sur le bien-fondé de l'ordonnance de référé :<br/>
<br/>
              2. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              3. En premier lieu, l'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés, saisi de conclusions tendant à la suspension d'un acte administratif, d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. L'urgence doit être appréciée objectivement compte tenu de l'ensemble des circonstances de l'affaire.<br/>
<br/>
              4. Le juge des référés a retenu, par une appréciation souveraine des pièces du dossier exempte de dénaturation, que la condition d'urgence était remplie, dès lors que M. B... était privé de ressources du fait de son licenciement sans préavis ni indemnité et qu'il ne disposait pas d'économies pour pouvoir assurer ses dépenses courantes, en l'absence de revenu de remplacement.<br/>
<br/>
              5. En deuxième lieu, en jugeant que, eu égard à la nature et au degré de gravité des faits de l'espèce, le moyen tiré du caractère disproportionné de la sanction de licenciement était, en l'état de l'instruction, propre à créer un doute sérieux quant à la légalité de la décision attaquée, le juge des référés du tribunal administratif de Montreuil n'a pas non plus dénaturé les pièces du dossier qui lui était soumis.<br/>
<br/>
              6. Enfin, il ne ressort pas des énonciations de l'ordonnance attaquée que le juge des référés se soit fondé, pour prononcer la suspension de la décision en litige, sur des motifs tirés de l'incompétence ou de la partialité de son auteur, de la composition irrégulière du conseil de discipline, ou de ce qu'elle serait entachée d'erreur de fait ou d'erreur manifeste d'appréciation. Par suite, les moyens du pourvoi dirigés contre ces motifs sont inopérants.<br/>
<br/>
              7. Il résulte de tout ce qui précède que le garde des sceaux, ministre de la justice n'est pas fondé à demander l'annulation de l'ordonnance qu'il attaque.<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 500 euros à verser à M. B... au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi du garde des sceaux, ministre de la justice est rejeté.<br/>
Article 2 : L'Etat versera à M. B... une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au garde des sceaux, ministre de la justice et à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-01-02 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. COMPÉTENCE TERRITORIALE. - INCOMPÉTENCE TERRITORIALE DU TRIBUNAL ADMINISTRATIF POUR CONNAÎTRE DE LA DEMANDE DE 1ÈRE INSTANCE - MOYEN SOULEVÉ POUR LA PREMIÈRE FOIS EN CASSATION [RJ1] - INOPÉRANCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-04-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS INOPÉRANTS. - INCLUSION - MOYEN, SOULEVÉ POUR LA PREMIÈRE FOIS EN CASSATION, TIRÉ DE L'INCOMPÉTENCE TERRITORIALE DU TRIBUNAL ADMINISTRATIF POUR CONNAÎTRE DE LA DEMANDE DE 1ÈRE INSTANCE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-08-02-004-03-02 PROCÉDURE. VOIES DE RECOURS. CASSATION. RECEVABILITÉ. RECEVABILITÉ DES MOYENS. MOYEN SOULEVÉ POUR LA PREMIÈRE FOIS DEVANT LE JUGE DE CASSATION. - MOYEN D'INCOMPÉTENCE TERRITORIALE DU TRIBUNAL ADMINISTRATIF POUR CONNAÎTRE DE LA DEMANDE DE 1ÈRE INSTANCE - INOPÉRANCE [RJ1].
</SCT>
<ANA ID="9A"> 17-05-01-02 Il résulte du second alinéa de l'article R. 312-2 du code de justice administrative (CJA) que le moyen tiré de de ce que le tribunal administratif n'était pas le tribunal territorialement compétent pour connaître de la demande de première instance, soulevé pour la première fois devant le juge de cassation, est inopérant.</ANA>
<ANA ID="9B"> 54-07-01-04-03 Il résulte du second alinéa de l'article R. 312-2 du code de justice administrative (CJA) que le moyen tiré de de ce que le tribunal administratif n'était pas le tribunal territorialement compétent pour connaître de la demande de première instance, soulevé pour la première fois devant le juge de cassation, est inopérant.</ANA>
<ANA ID="9C"> 54-08-02-004-03-02 Il résulte du second alinéa de l'article R. 312-2 du code de justice administrative (CJA) que le moyen tiré de de ce que le tribunal administratif n'était pas le tribunal territorialement compétent pour connaître de la demande de première instance, soulevé pour la première fois devant le juge de cassation, est inopérant.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., en précisant, CE, 20 mai 2011, Commune du Lavandou et Biver, n°s 328338 328642, T. pp. 850-922. Rappr., sur l'inopérance, en principe, d'un moyen soulevé pour la première fois en cassation, qui n'est pas né de l'arrêt ou du jugement attaqué et qui n'est pas d'ordre public, CE, 24 novembre 2010, Commune de Lyon, n° 325195, T. pp. 833-913-922-932.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
