<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031315605</ID>
<ANCIEN_ID>JG_L_2015_10_000000375027</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/31/56/CETATEXT000031315605.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème SSR, 14/10/2015, 375027</TITRE>
<DATE_DEC>2015-10-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375027</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Charles Touboul</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2015:375027.20151014</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 375027, par une requête, enregistrée le 29 janvier 2014 au secrétariat du contentieux du Conseil d'Etat, l'association " Automobile club des avocats " et l'association " Ligue de défense des conducteurs " demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2014-3 du 3 janvier 2014 relatif à la vitesse maximale autorisée sur le boulevard périphérique de Paris ;<br/>
<br/>
              2°) d'ordonner avant dire droit la production de l'avis rendu sur le projet de décret par la section des travaux publics du Conseil d'Etat. <br/>
<br/>
<br/>
              2° Sous le n° 382372, par une requête, un mémoire rectificatif, un mémoire complémentaire et un nouveau mémoire, enregistrés les 7 et 16 juillet et 8 octobre 2014 et 24 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, M. A...B...demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret n° 2014-3 du 3 janvier 2014 relatif à la vitesse maximale autorisée sur le boulevard périphérique de Paris ainsi que la décision implicite par laquelle le Premier ministre a rejeté son recours gracieux contre ce décret.<br/>
<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              3° Sous le n° 382380, par une requête, un mémoire rectificatif et un mémoire complémentaire, enregistrés les 7 et 16 juillet et 8 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, M. D...C...demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret n° 2014-3 du 3 janvier 2014 relatif à la vitesse maximale autorisée sur le boulevard périphérique de Paris.<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu :<br/>
<br/>
              - la Constitution, notamment son Préambule ;<br/>
<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              - le code général des collectivités territoriales ;<br/>
<br/>
              - le code de la route ;<br/>
<br/>
              - le décret n° 2002-810 du 2 mai 2002 ;<br/>
<br/>
              - le décret n° 2009-615 du 3 juin 2009 ;<br/>
<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles Touboul, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article R. 413-3 du code de la route, dans sa rédaction antérieure au décret attaqué : " En agglomération, la vitesse des véhicules est limitée à 50 km/ h. /Toutefois, cette limite peut être relevée à 70 km/ h sur les sections de route où les accès des riverains et les traversées des piétons sont en nombre limité et sont protégés par des dispositifs appropriés. La décision est prise par arrêté de l'autorité détentrice du pouvoir de police de la circulation, après consultation des autorités gestionnaires de la voie et, s'il s'agit d'une route à grande circulation, après avis conforme du préfet./ Sur le boulevard périphérique de Paris, cette limite est fixée à 80 km/h " ; que, par le décret attaqué du 3 janvier 2014 relatif à la vitesse maximale autorisée sur le boulevard périphérique de Paris, le Premier ministre a modifié le dernier alinéa de cet article pour abaisser de 80 à 70 km/h la vitesse maximale autorisée sur cette voie de circulation ; que ce décret a modifié, en outre, les articles R. 413-8 et R. 413-9 du même code pour abaisser, dans les mêmes conditions, la vitesse maximale autorisée pour certaines catégories de véhicules ; que les requêtes visées ci-dessus tendent à l'annulation de ce décret pour excès de pouvoir ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              En ce qui concerne la compétence du Premier ministre :<br/>
<br/>
              2. Considérant qu'il appartient au Premier ministre, en vertu de ses pouvoirs propres, d'édicter les mesures de police applicables à l'ensemble du territoire ; qu'il est loisible au Premier ministre, dans l'exercice de cette compétence, de fixer sur le territoire national des limites de vitesse de circulation différentes applicables à des types de voies distincts ; que les règles ainsi fixées par le Premier ministre n'ont ni pour objet ni pour effet de priver les autorités de police dont relèvent les voies concernées du pouvoir de fixer des limites plus strictes en fonction de circonstances locales particulières ;<br/>
<br/>
              3. Considérant que les deux premiers alinéas de l'article R. 413-3 du code de la route limitent la vitesse en agglomération à 50 km/h sur l'ensemble du territoire national, tout en permettant à l'autorité de police compétente de porter cette limite à 70 km/h sur les sections de route où les accès des riverains et les traversées des piétons sont en nombre limité et sont protégés par des dispositifs appropriés ; que le troisième alinéa du même article, tel que modifié par le décret attaqué, fixe la limite à 70 km/h sur le boulevard périphérique de Paris ; qu'en fixant à cet alinéa une règle de vitesse applicable au boulevard périphérique de Paris, différente de celles prévues aux deux premiers alinéas pour les autres voies situées en agglomération, le Premier ministre, contrairement à ce que soutiennent MM. B...etC..., n'a pas excédé ses pouvoirs ni méconnu la compétence des autorités locales de police ;<br/>
<br/>
<br/>
              En ce qui concerne la procédure :<br/>
<br/>
              4. Considérant, en premier lieu, qu'il ressort des copies du projet de décret soumis par le Gouvernement au Conseil d'Etat et du projet adopté par la section des travaux publics du Conseil d'Etat lors de sa séance du 24 décembre 2013, versées au dossier des requêtes n° 382372 et 382380 par le ministre de l'intérieur, que le texte publié au Journal officiel ne contient pas de dispositions qui différeraient à la fois du projet initial du Gouvernement et du texte adopté par le Conseil d'Etat ; que, par suite, le moyen tiré par M. B...et M. C...de ce que le décret attaqué aurait, pour ce motif, été pris à l'issue d'une procédure irrégulière ne peut qu'être écarté ;<br/>
<br/>
              5. Considérant, en second lieu, qu'après l'expiration du délai de recours sont irrecevables, sauf s'ils sont d'ordre public, les moyens présentés par le requérant qui ne se rattachent pas à une cause juridique invoquée par lui avant l'expiration de ce délai ; que le délai de recours contentieux contre le décret du 3 janvier 2014, déclenché par la publication de ce décret au Journal officiel du 5 janvier 2014, est expiré le 6 mars suivant ; que, dans leur requête enregistrée le 29 janvier 2014, l'association  " Automobile club des avocats " et l'association " Ligue de défense des conducteurs " se sont bornées à invoquer un moyen relatif à la légalité interne du décret ; que le moyen soulevé dans leur mémoire en réplique enregistré le 3 juillet 2014 et tiré de ce que le Premier ministre n'a pas mis en oeuvre la procédure prévue aux articles L. 120-1 et suivants du code de l'environnement, qui n'est pas d'ordre public, concerne la légalité externe du décret et se rattache ainsi à une cause juridique distincte de celle invoquée dans le délai de recours ; que ce moyen est, par suite, irrecevable et ne peut qu'être écarté ; <br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier que la réduction de 80 à 70 km/h de la vitesse maximale de circulation sur le boulevard périphérique de Paris est motivée, en premier lieu, par des considérations de sécurité routière, une baisse de 13 % des accidents corporels étant escomptée ; qu'il n'est pas sérieusement contesté, d'une part, que la vitesse constitue sur le périphérique parisien le facteur principal d'accidents, y compris corporels et, d'autre part, que les accidents les plus graves interviennent aux heures où la vitesse de circulation est la plus élevée du fait de la fluidité du trafic et où la vitesse maximale de circulation a ainsi la portée la plus effective ;<br/>
<br/>
              7. Considérant que cet abaissement de la vitesse maximale autorisée est motivé, en deuxième lieu, par le souci de réduire les nuisances sonores subies par les riverains ; que si la réduction de ces nuisances devrait être limitée à un décibel en moyenne, il n'est pas contesté qu'elle permettra de réduire d'environ 8 % l'exposition de la population au-delà du seuil nocturne de 62 db et concernera environ 3 000 personnes ;<br/>
<br/>
              8. Considérant que cet abaissement de la vitesse maximale de circulation est motivé, en troisième lieu, par le souci de limiter la pollution atmosphérique, une réduction de 0,3 % des émissions d'oxydes d'azote (NOx) et une diminution de 1 % des émissions de particules entre 21 heures et 7 heures étant escomptées ; que si son impact sur la pollution de l'air sera difficile à mesurer, l'abaissement de la vitesse maximale sur le boulevard périphérique de Paris est associé à d'autres mesures tendant à réduire les émissions polluantes liées à la circulation automobile en Ile-de-France dont les effets sont susceptibles de se conjuguer ;<br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que les moyens tirés de ce que cette mesure serait entachée d'erreur d'appréciation doivent être écartés ;<br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin d'examiner la fin de non recevoir opposée par le ministre de l'intérieur à la requête de M.C..., que les requêtes doivent être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Les requêtes des associations " Automobile club des avocats " et " Ligue de défense des conducteurs ", de M. B...et de M. C...sont rejetées.<br/>
Article 2 : La présente décision sera notifiée à l'association " Automobile club des avocats ", à l'association " Ligue de défense des conducteurs ", à M. A...B..., à M. D... C..., au Premier ministre et au ministre de l'intérieur.<br/>
Copie en sera adressée au ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-02-02-01-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. PREMIER MINISTRE. - POLICE GÉNÉRALE NATIONALE - 1) LIMITES DE VITESSE SUR LES DIFFÉRENTES VOIES DE CIRCULATION - INCLUSION - 2) POSSIBILITÉ DE FIXER UNE RÈGLE SPÉCIFIQUE POUR LE BOULEVARD PÉRIPHÉRIQUE PARISIEN - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 01-02-02-01-02 1) Il appartient au Premier ministre, en vertu de ses pouvoirs propres, d'édicter les mesures de police applicables à l'ensemble du territoire. Il est loisible au Premier ministre, dans l'exercice de cette compétence, de fixer sur le territoire national des limites de vitesse de circulation différentes applicables à des types de voies distincts. Les règles ainsi fixées par le Premier ministre n'ont ni pour objet ni pour effet de priver les autorités de police dont relèvent les voies concernées du pouvoir de fixer des limites plus strictes en fonction de circonstances locales particulières.... ,,2) Fixation par le Premier ministre de la limite de vitesse applicable au boulevard périphérique de Paris à 70 km/h alors que la limite prévue par les deux premiers alinéas de l'article R. 413-3 du code de la route pour les autres voies situées en agglomération est de 50 km/h, sauf décision de l'autorité de police locale pour relever cette limite jusqu'à 70 km/h. En fixant pour le boulevard périphérique parisien cette règle de vitesse différente de celles prévues aux deux premiers alinéas de l'article R. 413-3 du code de la route, le Premier ministre n'a pas excédé ses pouvoirs ni méconnu la compétence des autorités locales de police.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE,  8 août 1919, Labonne, n° 56377, p. 737.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
