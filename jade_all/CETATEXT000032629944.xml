<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032629944</ID>
<ANCIEN_ID>JG_L_2014_12_000000386107</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/62/99/CETATEXT000032629944.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 17/12/2014, 386107, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-12-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386107</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2014:386107.20141217</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 1er décembre 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par l'association Approche-Ecohabitat, représentée par son représentant légal, dont le siège est 53, impasse de l'Odet, espace associatif Quimper Cornouaille, à Quimper (29000), et M. A... B..., demeurant... ; les requérants demandent au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution, d'une part, du décret n° 2014-812 du 16 juillet 2014 pris pour l'application du second alinéa du 2 de l'article 200 quater du code général des impôts et du dernier alinéa du 2 du I de l'article 244 quater U du code général des impôts et, d'autre part, de l'arrêté du 16 juillet 2014 de la ministre du logement et de l'égalité des territoires et de la ministre de l'écologie, du développement durable et de l'énergie relatif aux critères de qualifications requis pour le bénéfice du crédit d'impôt " développement durable " et des avances remboursables sans intérêts destinées au financement de travaux de rénovation afin d'améliorer la performance énergétique des logements anciens ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros au titre de l'article L.761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              ils soutiennent que :<br/>
              - ils ont intérêt à agir, dès lors, d'une part, que l'association Approche-Ecohabitat a pour objet la promotion d'un habitat écologique et économe en énergie et, d'autre part, que le décret et l'arrêté litigieux portent atteinte aux intérêts de M. B..., qui exerce la profession d'artisan charpentier ;<br/>
              - la condition d'urgence est remplie, dès lors que le décret et l'arrêté litigieux produiront leurs effets le 1er janvier 2015, sans que des mesures transitoires aient été prévues ;<br/>
              - il existe un doute sérieux quant à la légalité des dispositions litigieuses ;<br/>
              - elles sont intervenues à l'issue d'une procédure irrégulière, dès lors, d'une part, que le Conseil national de l'habitat n'a pas été consulté, en méconnaissance des dispositions issues de l'article R. 361-2 du code de la construction et de l'habitation et, d'autre part, que le décret litigieux n'a pas été soumis au Conseil d'Etat ;<br/>
              - elles méconnaissent les dispositions issues de l'article L. 420-2 du code de commerce ;<br/>
              - elles portent atteinte au principe de sécurité juridique, en l'absence de mesures transitoires ;<br/>
              - elles portent atteinte au principe d'égalité entre les entreprises ;<br/>
<br/>
<br/>
              Vu le décret et l'arrêté dont la suspension de l'exécution est demandée ;<br/>
<br/>
              Vu la copie de la requête à fin d'annulation de ce décret et de cet arrêté ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution ;<br/>
<br/>
              Vu le code de la construction et de l'habitation ;<br/>
<br/>
              Vu le code général des impôts ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d' une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; que la condition d'urgence prévue par ces dispositions est remplie lorsque l'exécution d'une décision administrative porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; que l'article L. 522-3 du code de justice administrative précise que le juge des référés peut rejeter une requête sans instruction ni audience lorsque la demande ne présente pas un caractère d'urgence ;<br/>
<br/>
              2. Considérant que le décret et l'arrêté contestés ont été pris pour l'application du second alinéa du 2 de l'article 200 quater et du dernier alinéa du 2 du I de l'article 244 quater U du code général des impôts, qui instituent respectivement un crédit d'impôt sur le revenu dit " développement durable " et des avances remboursables sans intérêts destinées au financement de travaux de rénovation afin d'améliorer la performance énergétique des logements anciens ; que le décret précise les catégories de travaux pour lesquelles est exigé, pour le bénéfice du crédit d'impôt et des avances remboursables sans intérêts, le respect de critères de qualification de l'entreprise qui les réalise ; qu'il prévoit que, pour justifier du respect de ces critères, l'entreprise doit être titulaire d'un signe de qualité répondant à un référentiel d'exigences de moyens et de compétences délivré par un organisme ayant passé une convention avec l'Etat ; que, pour sa part, l'arrêté contesté fixe en annexes les exigences auxquelles doit satisfaire une entreprise pour être titulaire d'un signe de qualité ; que les signes de qualité objets de l'annexe I sont dénommés " qualification ", tandis que sont concernés par l'annexe II les signes de qualité, dénommés " certification ", portant sur la capacité d'une entreprise à concevoir et réaliser des travaux de rénovation énergétique pour un bâtiment dans le cadre d'une offre globale d'amélioration de la performance énergétique ainsi que sa capacité à assurer l'accompagnement du maître d'ouvrage tout au long du projet ; <br/>
<br/>
              3. Considérant que si le décret et l'arrêté contestés, en soumettant le bénéfice du crédit d'impôt et des avances remboursables sans intérêts à la détention, par l'entreprise qui réalise les travaux, d'un signe de qualité, ont pour effet d'inciter les entreprises du secteur du bâtiment à satisfaire aux exigences nécessaires pour être titulaire d'un tel signe, leur entrée en vigueur ne crée pas, en elle-même, de nouvelles exigences auxquelles seraient soumises ces entreprises pour obtenir une qualification ou une certification ; qu'en effet, les exigences fixées par les annexes de l'arrêté contesté reprennent celles des annexes à la Charte d'engagement définissant les conditions d'obtention de la mention " RGE " (" Reconnu Garant de l'environnement ") relative aux signes de qualité délivrés aux entreprises réalisant des travaux concourant à améliorer la performance énergétique des bâtiments, signée le 9 novembre 2011 entre l'Etat et l'Agence de l'Environnement de la Maîtrise de l'Energie, d'une part, et certains organismes de qualification et certification et organisations professionnelles du bâtiment, dont la Confédération de l'Artisanat et des Petites Entreprises du Bâtiment, d'autre part, et modifiée par un avenant signé le 4 novembre 2013 ; qu'en outre, les professionnels du secteur ont disposé du temps nécessaire pour pouvoir être à même de satisfaire, en temps utile, aux exigences qui découlent du décret et de l'arrêté contestés ; qu'en effet, la Charte d'engagement annonçait dès 2011 la mise en place à venir du principe " d'éco-conditionnalité " pour les travaux de performance énergétique aidés dans le bâtiment ; qu'en outre, le bénéfice de la qualification " RGE " a été accordé, à titre transitoire, à l'ensemble des entreprises déjà titulaires des signes de qualité délivrés par les organismes signataires de la Charte, ces entreprises n'étant soumises aux nouvelles exigences qu'au fur et à mesure du renouvellement de leur qualification ; que, dans ces conditions, les requérants, qui n'apportent aucun élément précis et chiffré à l'appui de leurs allégations, ne justifient pas, à raison de l'application des dispositions des actes litigieux aux dépenses engagées à compter du 1er janvier 2015, d'un préjudice suffisamment grave et immédiat aux intérêts qu'ils entendent défendre ; que n'est pas davantage rapportée l'existence d'un risque, à cette date, de pénurie des professionnels du secteur titulaires d'une qualification ou d'une certification " RGE " ;  <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que la condition d'urgence n'est manifestement pas remplie ; que la requête, y compris les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative, ne peut, par suite, qu'être rejetée selon la procédure prévue par l'article L. 522-3 de ce code ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de l'association Approche-Ecohabitat et de M. B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à l'association Approche-Ecohabitat et à M. A... B....<br/>
Copie de la présente ordonnance sera transmise au Premier ministre, à la ministre du logement, de l'égalité des territoires et de la ruralité et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
