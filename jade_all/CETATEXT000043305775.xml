<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043305775</ID>
<ANCIEN_ID>JG_L_2021_03_000000450290</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/30/57/CETATEXT000043305775.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 09/03/2021, 450290, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450290</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:450290.20210309</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              M. B... A... a demandé au juge des référés du tribunal administratif de Nice, statuant sur le fondement de l'article L. 521-2 du code de justice administratif, d'une part, de l'admettre au bénéfice de l'aide juridictionnelle provisoire et, d'autre part, d'enjoindre au directeur de l'Office français de l'immigration et de l'intégration (OFII) de rétablir, à son profit, le bénéfice des conditions matérielles d'accueil, dans un délai de huit jours à compter de la notification de l'ordonnance à intervenir, sous astreinte de 200 euros par jour de retard. Par une ordonnance n° 2100600 du 8 février 2021 le juge des référés du tribunal administratif de Nice a rejeté sa requête. <br/>
<br/>
              Par une requête, enregistrée le 2 mars 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative : <br/>
<br/>
              1°) de réformer l'ordonnance n° 2100600 du 8 février 2021 ; <br/>
<br/>
              2°) d'enjoindre au directeur de l'OFII de rétablir les conditions matérielles d'accueil à sa fille Mme C... B..., par son intermédiaire en tant que représentant légal, et de verser son allocation de demandeur d'asile, dans un délai de huit jours à compter de la notification de l'ordonnance à intervenir sous astreinte de 100 euros par jour de retard ; <br/>
<br/>
              3°) de mettre à la charge de l'OFII la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Il soutient que : <br/>
              - l'ordonnance du juge des référés du tribunal administratif de Nice, rejetant sa requête au motif que son droit au bénéfice des l'allocation de demandeur d'asile a pris fin après l'écoulement d'un délai d'un mois suivant la notification de la décision de l'Office français de protection des réfugiés et apatrides du 12 mars 2020, notifiée le 6 août 2020, est entachée d'irrégularité dès lors que, d'une part, la première demande d'asile de sa fille C... B..., née le 26 mai 2020, a été enregistrée le 1er février 2021 et, d'autre part, cette demande est encore, à ce jour, en cours d'instruction par les services de l'OFPRA, de sorte que c'est à tort que le juge des référés de première instance a considéré que sa fille n'avait plus la qualité de demandeur d'asile ; <br/>
              - la condition d'urgence est satisfaite dès lors que, d'une part, ses deux filles en bas âges et lui ne perçoivent plus l'allocation de demandeur d'asile depuis plusieurs mois, ce qui les place dans une situation de vulnérabilité extrême, et, d'autre part, cette privation des conditions matérielles d'accueil garanties par la loi est constitutive d'une atteinte grave et manifestement illégale au droit d'asile ; <br/>
              - il est porté une atteinte grave et manifestement illégale au droit d'asile ainsi qu'à l'intérêt supérieur de l'enfant ; <br/>
              - la décision du directeur de l'OFII leur refusant l'octroi du bénéfice des conditions matérielles d'accueil est entachée d'illégalité manifeste dès lors que, d'une part, sa fille, C... B..., bénéficie du droit de se maintenir sur le territoire français le temps d'examen de sa demande d'asile, conformément à l'article L. 743-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, et, d'autre part, il n'existe aucune impossibilité matérielle pour l'OFII de lui verser, par l'intermédiaire de ses parents, l'allocation de demandeur d'asile. <br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la directive n° 2003/9/CE du Conseil du 27 janvier 2003 relative à des normes minimales pour l'accueil des demandeurs d'asile dans les Etats membres ;<br/>
              - la directive n° 2003/33/UE du Parlement européen et du Conseil 26 juin 2013 établissant des normes pour l'accueil des personnes demandant la protection internationale ;<br/>
              - la convention internationale du 26 janvier 1990 relative aux droits de l'enfant ; <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;  <br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. M. B... A..., de nationalité nigériane, a demandé à se voir reconnaître la qualité de réfugié. Après le rejet de sa demande par l'OFPRA, confirmé par la Cour nationale du droit d'asile, il a demandé le réexamen de sa demande, qui a conduit à une nouvelle décision de rejet de l'OFPRA, pour irrecevabilité, le 12 mars 2020, qui lui a été notifiée le 6 août 2020. <br/>
<br/>
              3. Le 4 février 2021, M. A..., agissant exclusivement en son nom propre, et non, comme il est soutenu en appel, au nom de l'une de ses filles mineures, a demandé au juge des référés du tribunal administratif de Nice, sur le fondement de l'article L. 521-2 du code de justice administrative, le rétablissement dans son chef et à raison des droits qu'il estimait y avoir du bénéfice des conditions matérielles d'accueil allouées aux demandeurs d'asile. Au visa de l'article L. 744-9 du code de l'entrée et du séjour des étrangers et du droit d'asile, qui prévoit que le bénéfice des conditions matérielles d'accueil prend fin à l'expiration du délai de recours contre la décision de l'OFPRA refusant l'asile, le juge des référés du tribunal administratif de Nice, par une ordonnance du 8 février 2021, a constaté que l'intéressé avait perdu tout droit à ce bénéfice et en conséquence rejeté sa demande, par une ordonnance don M. A... relève appel. <br/>
<br/>
              4. Pour contester l'ordonnance qu'il attaque, M. A... se fonde en appel exclusivement sur les droits qu'il estime que sa fille C... tiendrait de sa qualité de demanderesse du statut de réfugié, aux conditions matérielles d'accueil. Or, si l'intéressé avait mentionné devant le juge des référés du tribunal administratif de Nice que sa fille mineure C... était demanderesse, c'était au titre du réexamen de sa propre situation personnelle. Il n'en tirait aucune conséquence quant aux droits propres de sa fille, qui n'était d'ailleurs pas partie à l'instance. Par ailleurs, le courrier que, le 22 février 2021, soit après l'ordonnance attaquée, l'OFPRA lui a adressé, pour indiquer que sa fille née sur le territoire français ne pouvait être associée au réexamen de la situation de son père et devait faire l'objet d'une nouvelle demande, ne la plaçait pas de ce fait dans la situation de demanderesse d'asile, et l'invitait à procéder à une demande en ce sens. Les conditions dans lesquelles C... B..., si elle a effectivement demandé à être reconnue comme réfugiée, pourrait bénéficier, par l'entremise de son père B... A..., des conditions matérielles d'accueil, constituent un litige distinct, qui n'a jamais été soumis au premier juge des référés, et dont l'invocation est insusceptible de fournir les moyens de critiquer utilement son ordonnance.<br/>
<br/>
              5. Il résulte de ce qui précède que l'appel de M. A... ne peut qu'être rejeté, ainsi que, les dispositions de l'article L. 761-1 du code de justice administrative y faisant obstacle dès lors que l'Etat n'est pas la partie perdante, sa demande tendant à ce que l'Etat lui verse une somme d'argent sur leur fondement. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. A... est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à M. B... A....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
