<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042175739</ID>
<ANCIEN_ID>JG_L_2020_07_000000437151</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/17/57/CETATEXT000042175739.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 29/07/2020, 437151, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437151</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:437151.20200729</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Elidis Boissons Services a demandé au tribunal administratif de Versailles de prononcer la restitution, d'une part, de cotisations de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie dans les rôles de la commune de Poissy (Yvelines) au titre des années 2012, 2013, 2014, 2015 et 2016 à raison de propriétés situées, dans cette commune, au 17, rue Saint-Sébastien, d'autre part, de cotisations de taxe annuelle sur les locaux à usage de bureaux, les locaux commerciaux, les locaux de stockage et les surfaces de stationnement perçue dans la région Ile-de-France auxquelles elle a été assujettie au titre des années 2014 et 2015 à raison de locaux situés à la même adresse. Par un jugement n° 1701844 du 25 octobre 2019, ce tribunal a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 26 décembre 2019 et 4 mai 2020 au secrétariat du contentieux du Conseil d'Etat, la société Elidis Boissons Services demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la société Elidis Boissons Services ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur les conclusions relatives à la taxe annuelle sur les locaux à usage de bureaux, les locaux commerciaux et les locaux de stockage en Ile-de-France :<br/>
<br/>
              1. Dans la mesure où elle tend à la décharge de cotisations de taxe annuelle sur les locaux à usage de bureaux, les locaux commerciaux et les locaux de stockage en Ile-de-France, régie par l'article 231 ter du code général des impôts, qui n'a pas le caractère d'un impôt local, la demande que la société Elidis Boissons Services a présentée au tribunal administratif de Versailles n'entre dans aucun des cas, énumérés à l'article R. 811-1 du code de justice administrative, où par exception, le tribunal administratif statue en premier et dernier ressort. Par suite, les conclusions que la société Elidis Boissons Services a présentées devant le Conseil d'Etat, en tant qu'elles sont relatives à cette taxe, ont le caractère d'un appel. Il y a lieu par suite, en application de l'article R. 351-1 du même code, d'en attribuer le jugement à la cour administrative d'appel de Versailles, compétente pour en connaître en vertu des dispositions combinées des articles R. 322-1 et R. 221-7 de ce code.<br/>
<br/>
              Sur les conclusions relatives à la taxe foncière sur les propriétés bâties :<br/>
<br/>
              2. En second lieu, aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
              3. Pour demander l'annulation du jugement qu'elle attaque en tant qu'il rejette ses conclusions en décharge de cotisations de taxe foncière sur les propriétés bâties, la société Elidis Boissons Services soutient que le tribunal administratif de Versailles a :<br/>
              - commis une erreur de droit en jugeant que les clauses de la convention en vertu de laquelle elle occupait une dépendance du domaine public et qui l'autorisait à y maintenir des immeubles pouvait légalement faire obstacle à ce que la personne publique propriétaire accède à la propriété de ces immeubles à la date de sa résiliation ; <br/>
              - méconnu en conséquence le I de l'article 1400 du code général des impôts en jugeant qu'elle était demeurée redevable de la taxe foncière sur les propriétés bâties due à raison de ces immeubles après cette résiliation. <br/>
<br/>
              4. Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement de la requête de la société Elidis Boissons Services, dirigée contre le jugement du 25 octobre 2019 du tribunal administratif de Versailles en tant qu'il a statué en matière de taxe annuelle sur les locaux à usage de bureaux, les locaux commerciaux et les locaux de stockage en Ile-de-France, est attribué à la cour administrative d'appel de Versailles.<br/>
Article 2 : Le pourvoi de la société Elidis Boissons Services, dirigé contre le jugement du 25 octobre 2019 du tribunal administratif de Versailles en tant qu'il a statué en matière de taxe foncière sur les propriétés bâties, n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à la société Elidis Boissons Services et au président de la cour administrative d'appel de Versailles.<br/>
Copie en sera adressée au ministre de l'économie, des finances et de la relance.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
