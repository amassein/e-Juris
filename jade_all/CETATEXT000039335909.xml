<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039335909</ID>
<ANCIEN_ID>JG_L_2019_11_000000432143</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/33/59/CETATEXT000039335909.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 06/11/2019, 432143, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-11-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432143</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Damien Pons</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:432143.20191106</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A... D... a saisi le tribunal administratif de la Guadeloupe d'un litige relatif au montant de l'allocation aux adultes handicapés versée à sa mère, Mme C... B..., dont elle est curatrice. Par une ordonnance n° 1900388 du 17 avril 2019, le président du tribunal administratif de la Guadeloupe a transmis sa demande au tribunal de grande instance de Pointe-à-Pitre.<br/>
<br/>
              Par une requête, enregistrée le 2 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, Mme D... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à sa demande.<br/>
<br/>
              Elle soutient que c'est à tort que la caisse d'allocations familiales déduit le montant de sa pension de retraite et de l'allocation de solidarité aux personnes âgées de l'allocation aux adultes handicapés versée à sa mère.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'organisation judiciaire ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le décret n° 2015-233 du 27 février 2015 ;<br/>
              - le code de justice administrative, notamment son article R. 611-8 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Damien Pons, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article R. 351-5-1 du code de justice administrative : " Lorsque le Conseil d'Etat est saisi de conclusions se rapportant à un litige qui ne relève pas de la compétence de la juridiction administrative, il est compétent, nonobstant les règles relatives aux voies de recours et à la répartition des compétences entre les juridictions administratives, pour se prononcer sur ces conclusions et décliner la compétence de la juridiction administrative ".<br/>
<br/>
              2. En premier lieu, aux termes de l'article L. 142-1 du code de la sécurité sociale : " Le contentieux général de la sécurité sociale comprend les litiges relatifs : / 1° A l'application des législations et réglementations de sécurité sociale et de mutualité sociale agricole, à l'exception des litiges relevant du contentieux technique de la sécurité sociale (...) ". En vertu de l'article L. 821-5 du même code, les différends auxquels peut donner lieu l'application du titre 2 du livre 8 de ce code, consacré à l'allocation aux adultes handicapés, et qui ne relèvent pas d'un autre contentieux, " sont réglés suivant les dispositions régissant le contentieux général de la sécurité sociale ".<br/>
<br/>
              3. Mme D... a contesté devant le tribunal administratif de la Guadeloupe le montant de l'allocation aux adultes handicapés versée à sa mère, Mme C... B..., et, notamment, la prise en considération de sa pension de retraite et de l'allocation de solidarité aux personnes âgées dans le calcul de ce montant. Il résulte des dispositions citées ci-dessus qu'il appartient à la juridiction judiciaire - et en son sein, en premier ressort, au tribunal de grande instance spécialement désigné en application de l'article L. 211-16 du code de l'organisation judiciaire - de connaître d'un tel recours. Par suite, la requête présentée par Mme D... se rapporte à un litige qui, ainsi que l'a jugé le président du tribunal administratif de la Guadeloupe, ne relève pas de la compétence de la juridiction administrative mais du tribunal de grande instance de Pointe-à-Pitre.<br/>
<br/>
              4. En second lieu, aux termes du premier alinéa de l'article 32 du décret du 27 février 2015 relatif au Tribunal des conflits et aux questions préjudicielles : " Lorsqu'une juridiction de l'ordre judiciaire ou de l'ordre administratif décline la compétence de l'ordre de juridiction auquel elle appartient au motif que le litige ne ressortit pas à cet ordre, elle renvoie les parties à saisir la juridiction compétente de l'autre ordre de juridiction. Toutefois, lorsque la juridiction est saisie d'un contentieux relatif à l'admission à l'aide sociale tel que défini par le code de l'action sociale et des familles ou par le code de la sécurité sociale, elle transmet le dossier de la procédure, sans préjuger de la recevabilité de la demande, à la juridiction de l'autre ordre de juridiction qu'elle estime compétente par une ordonnance qui n'est susceptible d'aucun recours ".<br/>
<br/>
              5. Le litige relatif au montant de l'allocation aux adultes handicapés de Mme B... relève du contentieux général de la sécurité sociale et non du contentieux de l'admission à l'aide sociale relevant du code de la sécurité sociale, défini à l'article L. 142-3 de ce code, non plus que du contentieux de l'admission à l'aide sociale relevant du code de l'action sociale et des familles. Par suite, le président du tribunal administratif de la Guadeloupe n'était pas dans la situation, prévue par l'article 32 du décret du 27 février 2015, où il devait transmettre le dossier de la procédure à la juridiction de l'autre ordre de juridiction qu'il estimait compétente. Toutefois, il résulte de ce qui a été dit au point 3 que la demande de Mme D... relève de la compétence du tribunal de grande instance de Pointe-à-Pitre. Par suite, l'ordonnance qu'elle attaque ne lui fait pas grief en tant qu'elle transmet le dossier de la procédure à cette juridiction.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de Mme D... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à Mme A... D....<br/>
Copie en sera adressée au président du tribunal de grande instance de Pointe-à-Pitre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
