<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027201053</ID>
<ANCIEN_ID>JG_L_2013_03_000000345647</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/20/10/CETATEXT000027201053.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 21/03/2013, 345647, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-03-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>345647</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Emmanuelle Mignon</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2013:345647.20130321</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 10 janvier et 11 avril 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SARL France Afrique Exploitation, dont le siège est Rouquet à Saint-Gély-du-Fesc (34980), représentée par son gérant ; la SARL France Afrique Exploitation demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08MA03170 du 10 novembre 2010 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0506830 du 9 avril 2008 du tribunal administratif de Montpellier rejetant sa demande tendant à la condamnation de la commune de Saint-Gély-du-Fesc à lui verser une somme de 594 218,10 euros en réparation du préjudice subi du fait du refus illégal du maire de la commune de délivrer à la SNC Cazorla un certificat d'achèvement des travaux, d'autre part, à la condamnation de cette commune à lui verser cette somme, assortie d'intérêts au taux légal ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Saint-Gély-du-Fesc la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Emmanuelle Mignon, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Boré et Salve de Bruneton, avocat de la Sarl France Afrique Exploitation et de la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune de Saint-Gély-du-Fesc,<br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Boré et Salve de Bruneton, avocat de la Sarl France Afrique Exploitation et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune de Saint-Gély-du-Fesc ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SARL France Afrique Exploitation s'est vu confier, par deux contrats passés les 20 janvier et 14 septembre 1982 avec la société en nom collectif Cazorla, d'une part, diverses tâches en vue de l'obtention de l'autorisation du lotissement " Les Hauts de Saint-Gély ", situé sur le territoire de la commune de Saint-Gély-du-Fesc, d'autre part, un mandat exclusif de vente de l'ensemble des lots du lotissement ; que la rémunération de la SARL France Afrique Exploitation au titre du premier contrat, intitulé " contrat de coopération commerciale ", s'élevait à la somme de 1 452 225 francs, portée par avenant à 2 102 225 francs et était due au démarrage de la commercialisation de la troisième tranche du lotissement ; que sa rémunération était égale à 5 % du prix hors taxe de vente de chaque lot au titre du mandat de vente ; qu'à la suite du refus, par la commune, de lui délivrer le certificat d'achèvement des travaux de la deuxième tranche du lotissement, la SNC Cazorla n'a pu vendre certains des lots de cette tranche ; que, connaissant, de ce fait, des difficultés financières, elle n'a pas réalisé la troisième tranche du lotissement ; que, par une décision du Conseil d'Etat statuant au contentieux du 16 février 2005, la commune de Saint-Gély-du-Fesc a été condamnée à verser à la SNC Cazorla une somme de 550 000 euros en réparation du préjudice que cette société a subi du fait de l'illégalité du refus du maire de délivrer le certificat d'achèvement des travaux de la deuxième tranche ; que la SARL France Afrique Exploitation a elle aussi demandé réparation du préjudice qu'elle estime avoir subi du fait de la faute de la commune ; qu'elle se pourvoit en cassation contre l'arrêt du 10 novembre 2010 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant à l'annulation du jugement du tribunal administratif de Montpellier rejetant sa demande d'indemnisation ;<br/>
<br/>
              2. Considérant, en premier lieu, que la société requérante invoquait devant la cour le préjudice résultant de l'absence de versement des sommes qui lui étaient dues au titre du contrat de coopération commerciale et du manque à gagner au titre du mandat de vente des lots, en se prévalant de la reconnaissance, par la décision du Conseil d'Etat du 16 février 2005, d'un lien direct de causalité entre le refus illégal de la commune de délivrer le certificat d'achèvement des travaux de la deuxième tranche du lotissement et les difficultés financières de la SNC Cazorla ; que, par suite, la cour n'a pas dénaturé ses écritures en se référant au préjudice qu'elle soutenait avoir subi du fait des problèmes financiers de la SNC Cazorla ; qu'elle n'a pas davantage entaché son arrêt de contradiction de motifs en faisant mention tant de ce préjudice que de celui que la société estimait avoir subi du fait du défaut de réalisation de la troisième tranche du lotissement ;<br/>
<br/>
              3. Considérant, en second lieu, que la responsabilité d'une personne publique n'est susceptible d'être engagée que s'il existe un lien de causalité suffisamment direct entre les fautes qu'elle a commises et le préjudice subi par la victime ; que, ainsi que l'a relevé la cour, il ressort des pièces du dossier qui lui était soumis que, si la commune de Saint-Gély-du-Fesc, en refusant de délivrer à la SNC Cazorla le certificat d'achèvement des travaux de la deuxième tranche du lotissement, a commis une faute de nature à engager sa responsabilité, le préjudice né, pour la société requérante, du défaut de règlement par la SNC Cazorla de la rémunération due au titre du contrat de coopération commerciale conclu entre les deux sociétés résulte des stipulations de ce contrat, qui avait prévu un règlement au démarrage de la troisième tranche du lotissement seulement ; que la cour a par ailleurs jugé, sans que son arrêt ne soit contesté sur ce point, que le préjudice résultant du manque à gagner au titre du mandat de vente des lots, trouve son origine dans la décision de la SNC Cazorla de renoncer à la troisième tranche du lotissement à la suite des difficultés financières qu'elle a rencontrées ; que, dès lors, en jugeant que le préjudice subi par la société requérante ne présentait pas un lien suffisamment direct avec la faute de la commune, la cour administrative d'appel de Marseille n'a pas commis d'erreur de droit et a exactement qualifié les faits qui lui étaient soumis ;<br/>
<br/>
              4. Considérant qu'il résulte de tout ce qui précède que la SARL France Afrique Exploitation n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la commune de Saint-Gély-du-Fesc, qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la SARL France Afrique Exploitation le versement à la commune d'une somme de 3 000 euros au même titre ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de la SARL France Afrique Exploitation est rejeté.<br/>
Article 2 : La SARL France Afrique Exploitation versera à la commune de Saint-Gély-du-Fesc une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la SARL France Afrique Exploitation et à la commune de Saint-Gély-du-Fesc.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
