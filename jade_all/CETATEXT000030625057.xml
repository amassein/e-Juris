<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030625057</ID>
<ANCIEN_ID>JG_L_2015_05_000000371061</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/62/50/CETATEXT000030625057.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 22/05/2015, 371061</TITRE>
<DATE_DEC>2015-05-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>371061</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>Mme Pauline Pannier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:371061.20150522</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 8 août 2013 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social ; le ministre demande au Conseil d'Etat d'annuler l'arrêt n° 12LY02034 du 20 juin 2013 par lequel la cour administrative d'appel de Lyon, sur appel de Mme B...A..., a, d'une part, annulé le jugement n° 1102606 du 7 juin 2012 par lequel le tribunal administratif de Dijon a rejeté la demande de l'intéressée tendant à l'annulation pour excès de pouvoir de la décision de l'inspecteur du travail du 26 septembre 2011 autorisant M. D...C..., agissant en qualité de liquidateur judiciaire de la société SMDI Fermetures à la licencier et, d'autre part, annulé cette décision ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu le code du travail ; <br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Pannier, auditeur,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'en vertu des dispositions du code du travail, le licenciement des salariés légalement investis de fonctions représentatives, qui bénéficient d'une protection exceptionnelle dans l'intérêt de l'ensemble des travailleurs qu'ils représentent, ne peut intervenir que sur autorisation de l'inspecteur du travail ; que, lorsque le licenciement d'un de ces salariés est envisagé, ce licenciement ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale de l'intéressé ; que, dans le cas où la demande d'autorisation de licenciement présentée par l'employeur est fondée sur un motif de caractère économique, il appartient à l'inspecteur du travail et, le cas échéant, au ministre, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si la situation de l'entreprise justifie le licenciement du salarié ; qu'à ce titre, lorsque la demande est fondée sur la cessation d'activité de l'entreprise, celle-ci n'a pas à être justifiée par l'existence de mutations technologiques, de difficultés économiques ou de menaces pesant sur la compétitivité de l'entreprise ; qu'il appartient alors à l'autorité administrative de contrôler, outre le respect des exigences procédurales légales et des garanties conventionnelles, que la cessation d'activité de l'entreprise est totale et définitive, que l'employeur a satisfait, le cas échéant, à l'obligation de reclassement prévue par le code du travail et que la demande ne présente pas de caractère discriminatoire ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 640-1 du code de commerce : " La procédure de liquidation judiciaire est destinée à mettre fin à l'activité de l'entreprise ou à réaliser le patrimoine du débiteur par une cession globale ou séparée de ses droits et de ses biens. (...) " ; que, dans le cas où le tribunal de commerce n'a pas autorisé de maintien de l'activité dans les conditions prévues à l'article L. 641-10 du même code, le jugement ouvrant la liquidation judiciaire a pour effet la cessation totale et définitive de l'activité de l'entreprise ; qu'il incombe toutefois à l'inspecteur du travail, saisi d'une demande d'autorisation de licenciement motivée par l'intervention d'un jugement de liquidation judiciaire, de tenir compte, à la date à laquelle il se prononce, de tous les éléments de droit ou de fait recueillis lors de son enquête qui seraient de nature à faire obstacle au licenciement envisagé ; que si, notamment, la cession des droits et biens de l'entreprise s'est accompagnée d'une reprise, même partielle, de l'activité, dans des conditions impliquant un transfert du contrat de travail du salarié à un nouvel employeur en application de l'article L. 1224-1 du code du travail, une telle circonstance fait obstacle au licenciement demandé ;<br/>
<br/>
              3. Considérant dès lors, qu'en jugeant que l'inspecteur du travail ne pouvait se borner à constater que le jugement du tribunal de commerce de Dijon en date du 31 mai 2011 prononçait la liquidation judiciaire de la société employeur de Mme A..." avec cessation de l'activité fixée à la date de ce jugement " pour en déduire que le motif économique de licenciement était établi, sans procéder, conformément à ce qui a été rappelé ci-dessus, aux vérifications qui lui incombaient, la cour administrative d'appel de Lyon n'a entaché son arrêt d'aucune erreur de droit relative à l'autorité de chose jugée par le tribunal de commerce ou à l'étendue du contrôle de l'administration sur le motif économique tiré de la cessation d'activité de l'entreprise ; <br/>
<br/>
              4. Considérant qu'il résulte de tout ce qui précède que le pourvoi du ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social doit être rejeté ;<br/>
<br/>
              5. Considérant que Mme A...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Masse-Dessen, Thouvenin, Coudray, avocat de MmeA..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à cette société ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi du ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social est rejeté. <br/>
<br/>
Article 2 : L'Etat versera à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de MmeA..., la somme de 3 000 euros sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social et à Mme B...A....<br/>
Copie en sera adressée pour information à M. D...C..., agissant en qualité de liquidateur judiciaire de la société SMDI Fermetures.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-07-01-04-03 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. CONDITIONS DE FOND DE L'AUTORISATION OU DU REFUS D'AUTORISATION. LICENCIEMENT POUR MOTIF ÉCONOMIQUE. - LICENCIEMENT FONDÉ SUR LA CESSATION D'ACTIVITÉ DE L'ENTREPRISE - JUGEMENT PLAÇANT UNE ENTREPRISE EN LIQUIDATION JUDICIAIRE SANS POURSUITE D'ACTIVITÉ - APPRÉCIATION DU CARACTÈRE TOTAL ET DÉFINITIF DE LA CESSATION D'ACTIVITÉ [RJ1].
</SCT>
<ANA ID="9A"> 66-07-01-04-03 Demande d'autorisation de licenciement fondée sur la cessation d'activité de l'entreprise mise en liquidation judiciaire. Dans le cas où le tribunal de commerce n'a pas autorisé de maintien de l'activité dans les conditions prévues à l'article L. 641-10 du code de commerce, le jugement ouvrant la liquidation judiciaire a pour effet la cessation totale et définitive de l'activité de l'entreprise. Il incombe toutefois à l'inspecteur du travail de tenir compte, à la date à laquelle il se prononce, de tous les éléments de droit ou de fait recueillis lors de son enquête qui seraient de nature à faire obstacle au licenciement envisagé. Si, notamment, la cession des droits et biens de l'entreprise s'est accompagnée d'une reprise, même partielle, de l'activité, dans des conditions impliquant un transfert du contrat de travail du salarié à un nouvel employeur en application de l'article L. 1224-1 du code du travail, une telle circonstance fait obstacle au licenciement demandé.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, 8 avril 2013, M. Schintu, n° 348559, p. 59.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
