<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032112611</ID>
<ANCIEN_ID>JG_L_2016_02_000000391569</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/11/26/CETATEXT000032112611.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème SSR, 26/02/2016, 391569, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-02-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391569</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Etienne de Lageneste</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:391569.20160226</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance n° 1505403 du 1er juillet 2015, enregistrée le 7 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de Montreuil a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée à ce tribunal par le Syndicat national des agents des douanes CGT.<br/>
<br/>
              Par cette requête, enregistrée au greffe du tribunal administratif de Montreuil le 17 juin 2015, et deux nouveaux mémoires, enregistrés les 8 décembre 2015 et 8 février 2016 au secrétariat du contentieux du Conseil d'Etat, le Syndicat national des agents des douanes CGT demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le 1. de l'article 3 de l'arrêté du secrétaire d'Etat chargé du budget du 14 avril 2015 précisant les modalités de remboursement d'une fraction de la taxe intérieure de consommation sur le gazole utilisé par certains véhicules routiers en tant qu'il prévoit une mesure de réorganisation des services des douanes ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des douanes ;<br/>
              - le décret n° 82-453 du 28 mai 1982, notamment son article 48 ;<br/>
              - le décret n° 2007-1665 du 26 novembre 2007 ;<br/>
              - le décret n° 2011-184 du 15 février 2011, notamment son article 34 ;<br/>
              - le décret n° 2014-1395 du 24 novembre 2014 ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Etienne de Lageneste, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que l'arrêté contesté du 14 avril 2015, pris sur le fondement de l'article 2 du décret du 24 novembre 2014, a pour objet de préciser les pièces justificatives à fournir ainsi que les modalités particulières de présentation et d'instruction des demandes de remboursement d'une fraction de la taxe intérieure de consommation sur le gazole utilisé par certains véhicules routiers ; que le 1. de son article 3 dispose que : " Les personnes, dont le siège social est situé en France continentale, dans le ressort de la direction interrégionale des douanes et droits indirects de Metz, adressent leur demande : - jusqu'au 30 juin 2015 : au bureau de douane, chargé du recouvrement de la taxe spéciale sur certains véhicules routiers prévue par l'article 284 bis du code des douanes, dans le département où se situe le siège social de l'entreprise ; - à compter du 1er juillet 2015, au service de la fiscalité routière, basé à Metz " ; que le Syndicat national des agents des douanes CGT demande l'annulation de ces dispositions en tant que la mention du service de la fiscalité routière révèle la création de ce service et la modification de l'organisation des services déconcentrés de la direction générale des douanes et droits indirects ;<br/>
<br/>
              2. Considérant que le 27 octobre 2014, le secrétaire d'Etat chargé du budget a annoncé la mise en place, à compter du 1er juillet 2015, d'un service national douanier de la fiscalité routière, basé à Metz, dont la mission consisterait notamment à centraliser le traitement des demandes de remboursement de la taxe intérieure de consommation sur le gazole utilisé par certains véhicules routiers ; que l'arrêté attaqué prévoit que les demandes de remboursement de la taxe intérieure de consommation déposées dans le ressort de la direction interrégionale de Metz seront traitées par les bureaux de douanes départementaux jusqu'au 30 juin 2015 et par le service de la fiscalité routière, installé à Metz, à compter du 1er juillet 2015 ; qu'il ne ressort pas des pièces du dossier qu'un acte, régulièrement signé et publié, autre que l'arrêté attaqué ait créé ce service avant le 1er juillet 2015 ; qu'ainsi, l'arrêté attaqué révèle la création de ce service et la modification de l'organisation des services déconcentrés de la direction générale des douanes et droits indirects ;<br/>
<br/>
              3. Considérant que l'arrêté attaqué a été signé au nom du ministre par la sous-directrice des droits indirects, qui n'est pas compétente en matière d'organisation des services, laquelle relève de la compétence de la sous-direction A " Ressources humaines, relations sociales et organisation " ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens de la requête, le syndicat est fondé à demander l'annulation des dispositions de l'article 3 de l'arrêté du 14 avril 2015 en tant qu'elles révèlent la création du service de la fiscalité routière et la modification de l'organisation des services déconcentrés de la direction générale des douanes et droits indirects, c'est-à-dire des  mots  " jusqu'au 30 juin 2015 : " et des mots " - à compter du 1er juillet 2015, au service de la fiscalité routière, basé à Metz " ;<br/>
<br/>
              4. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le Syndicat national des agents des douanes CGT au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
                         D E C I D E :<br/>
                           --------------<br/>
<br/>
Article 1er : Au 1. de l'article 3 de l'arrêté du 14 avril 2015, les mots  " jusqu'au 30 juin 2015 : " et les mots " - à compter du 1er juillet 2015, au service de la fiscalité routière, basé à Metz " sont annulés.<br/>
<br/>
Article 2 : Les conclusions du Syndicat national des agents des douanes CGT présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 3 : La présente décision sera notifiée au Syndicat national des agents des douanes CGT et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
