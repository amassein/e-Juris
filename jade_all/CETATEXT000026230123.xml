<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026230123</ID>
<ANCIEN_ID>JG_L_2012_07_000000350319</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/23/01/CETATEXT000026230123.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 27/07/2012, 350319, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>350319</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jean-Pierre Jouguelet</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>Mme Cécile Isidoro</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:350319.20120727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 23 juin 2011 au secrétariat du contentieux du Conseil d'Etat, présentée par Mme Rahmatou C, demeurant ... ; Mme C demande au Conseil d'Etat d'annuler le jugement n° 1100177 du 25 mai 2011 par lequel le tribunal administratif de Mayotte a rejeté sa protestation tendant à l'annulation des opérations électorales qui se sont déroulées les 20 et 27 mars 2011 pour l'élection d'un conseiller général dans le canton de Chirongui au terme desquelles M. Ali B a été proclamé élu ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code électoral ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Isidoro, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
<br/>
<br/>1. Considérant que lors du second tour de scrutin des opérations électorales pour l'élection du conseiller général du canton de Chirongui, qui a eu lieu le 27 mars 2011, M. B a obtenu 1 170 voix, Mme C 1 080 voix et M. D 342 voix ; que M. B a, ainsi, été proclamé élu ; que Mme C fait appel du jugement du 25 mai 2011 par lequel le tribunal administratif de Mayotte a rejeté sa protestation à l'encontre de cette élection ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'il ressort de la protestation présentée en première instance par Mme C que celle-ci a critiqué, devant le tribunal administratif de Mayotte, le fait que des irrégularités ayant affecté les opérations de vote par procuration auraient empêché 111 électeurs de voter ; qu'elle critique, désormais, le fait que les différentes irrégularités commises dans la tenue des listes d'émargements, qui dans sa requête d'appel concernent 140 personnes ayant donné procuration, auraient privé les électeurs de la faculté d'exercer leur contrôle sur les opérations électorales, sans qu'il soit fait état de ce qu'elles auraient été empêchées de voter ; que les personnes dont la liste est donnée dans la requête d'appel ne sont, pour l'essentiel, pas les mêmes que celles qui sont visées dans la protestation de première instance ; qu'ainsi le grief de Mme C, présenté après l'expiration du délai pour contester les opérations électorales est nouveau, et, par suite, irrecevable ;<br/>
<br/>
              3. Considérant, en deuxième lieu, que Mme C soutient que 79 électeurs, dont elle fournit la liste, auraient été radiés de la liste électorale sans en avoir été avertis, en méconnaissance des dispositions des articles L. 23 et R. 8 du code électoral ; que toutefois, il ne résulte pas de l'instruction que les radiations en cause auraient eu lieu sans que les personnes concernées en aient été informées et que ces radiations aient été constitutives de manoeuvres ;<br/>
<br/>
              4. Considérant, par ailleurs, que le tribunal n'a pas, contrairement à ce que soutient la requérante, méconnu ses pouvoirs en n'enjoignant pas à la commune de produire les pièces ayant conduit la commission administrative à prononcer l'inscription sur la liste électorale de 13 personnes ;<br/>
<br/>
              5. Considérant, en troisième lieu, qu'il résulte de l'instruction que les bulletins de vote au nom de M. B ont bien été, conformément aux prescriptions du premier alinéa de l'article R. 30 du code électoral, imprimés sur papier blanc ; qu'ainsi, le grief tiré de la méconnaissance de ces prescriptions ne peut qu'être écarté ;<br/>
<br/>
              5. Considérant, en quatrième lieu, qu'aux termes du troisième alinéa de l'article L. 62-1 du code électoral : " Le vote de chaque électeur est constaté par sa signature apposée à l'encre en face de son nom sur la liste d'émargement " ; qu'aux termes du deuxième alinéa de l'article L. 64 du même code : " Lorsqu'un électeur se trouve dans l'impossibilité de signer, l'émargement prévu par le troisième alinéa de l'article L. 62-1 est apposé par un électeur de son  choix qui fait suivre sa signature de la mention suivante : l'électeur ne peut signer lui-même " ; qu'il résulte de ces dispositions destinées à assurer la sincérité des opérations électorales que seule la signature personnelle, à l'encre, d'un électeur est de nature à apporter la preuve de sa participation au scrutin, sauf cas d'impossibilité dûment reportée sur la liste d'émargement ; que, dès lors, la constatation d'un vote par l'apposition d'une croix ou d'un trait sur la liste d'émargement ne peut être regardée comme garantissant l'authenticité de ce vote, non plus que l'apposition d'une signature sensiblement différente lors de chacun des tours d'une élection qui en comporte plus d'un ou l'apposition d'une seule signature recouvrant les emplacements destinés à celle de deux électeurs différents ; qu'il résulte de l'instruction que, comme le soutient à bon droit la requérante, 18 votes doivent être tenus pour irrégulièrement exprimés au regard de ces prescriptions ; que, toutefois, compte tenu de l'écart de voix séparant les deux candidats arrivés en tête au second tour, ces irrégularités ne peuvent être regardées comme de nature à remettre en cause la sincérité du scrutin ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que la requête de Mme C ne peut qu'être rejetée ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à sa charge le versement d'une somme à M. B au titre de l'application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de Mme C est rejetée.<br/>
Article 2 : Les conclusions de M. B tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme Rahmatou C, à M. Ali B, au ministre de l'intérieur et à la ministre de la réforme de l'Etat, de la décentralisation et de la fonction publique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
