<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038212336</ID>
<ANCIEN_ID>JG_L_2019_03_000000425779</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/21/23/CETATEXT000038212336.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 07/03/2019, 425779, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-03-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425779</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP KRIVINE, VIAUD ; SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Olivier  Fuchs</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:425779.20190307</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par deux mémoires distincts et un nouveau mémoire, enregistrés le 12 décembre 2018 et les 7 et 23 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, présentés en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, M. A... B...demande au Conseil d'Etat, à l'appui de son pourvoi tendant à l'annulation de l'ordonnance n° 1806305 du 9 novembre 2018 par laquelle le juge des référés du tribunal administratif de Strasbourg, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a dit n'y avoir pas lieu de statuer sur sa demande tendant à la suspension de l'exécution de la décision du 8 octobre 2018 par laquelle la ministre du travail a, d'une part, annulé la décision du 1er février 2018 de l'inspecteur du travail de la 2ème section de l'unité territoriale du Bas-Rhin refusant à la société Fiducial Private Security l'autorisation de le licencier et, d'autre part, accordé cette autorisation, de renvoyer au Conseil constitutionnel la question de la conformité aux droits  et libertés garantis par la Constitution de l'article L. 521-1 du code de justice administrative et de l'article L. 1232-6 du code du travail.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code du travail ;<br/>
              - la loi n° 2018-217 du 29 mars 2018 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Fuchs, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Krivine, Viaud, avocat de M. B...et à la SCP Gatineau, Fattaccini, avocat de la société Fiducial Private Security ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par une ordonnance du 9 novembre 2018, le juge des référés du tribunal administratif de Strasbourg a dit n'y avoir plus lieu de statuer sur la demande de M.B..., salarié protégé, tendant à la suspension de l'exécution de la décision du 8 octobre 2018 par laquelle la ministre du travail a autorisé son licenciement, en raison de ce que, postérieurement à l'introduction de sa demande en référé, son employeur l'avait licencié. A l'appui de son pourvoi en cassation contre cette ordonnance, M. B...fait valoir, par deux mémoires distincts, que les dispositions combinées des articles L. 521-1 du code de justice administrative et L. 1232-6 du code du travail portent atteinte aux droits et libertés garantis par la Constitution.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              3. L'article L. 1232-6 du code du travail, dans sa rédaction en vigueur, issue de la loi du 29 mars 2018 ratifiant diverses ordonnances prises sur le fondement de la loi n° 2017-1340 du 15 septembre 2017 d'habilitation à prendre par ordonnances les mesures pour le renforcement du dialogue social, dispose que : " Lorsque l'employeur décide de licencier un salarié, il lui notifie sa décision par lettre recommandée avec avis de réception. / Cette lettre comporte l'énoncé du ou des motifs invoqués par l'employeur. / Elle ne peut être expédiée moins de deux jours ouvrables après la date prévue de l'entretien préalable au licenciement auquel le salarié a été convoqué. / Un décret en Conseil d'Etat détermine les modalités d'application du présent article. Un arrêté du ministre chargé du travail fixe les modèles que l'employeur peut utiliser pour procéder à la notification du licenciement ".<br/>
<br/>
              4. Par ailleurs, l'article L. 521-1 du code de justice administrative dispose que : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision. / Lorsque la suspension est prononcée, il est statué sur la requête en annulation ou en réformation de la décision dans les meilleurs délais. La suspension prend fin au plus tard lorsqu'il est statué sur la requête en annulation ou en réformation de la décision ".<br/>
<br/>
              5. A l'appui de sa question prioritaire de constitutionnalité, M. B...soutient qu'à défaut d'avoir prévu, aux dispositions citées aux points 3 et 4, un dispositif de nature à faire obstacle à ce que l'employeur puisse prononcer le licenciement du salarié protégé avant que le juge des référés ait statué sur une éventuelle demande de suspension de l'exécution de l'autorisation administrative de licenciement, le législateur a méconnu, au détriment des salariés protégés, le droit à un recours effectif et le principe d'égalité devant la loi, qui sont respectivement garantis par l'article 16 et par l'article 6 de la Déclaration des droits de l'homme et du citoyen. Il fait notamment valoir, d'une part, que le juge des référés saisi sur le fondement de l'article L. 521-1 du code de justice administrative d'une demande de suspension de l'exécution d'une autorisation administrative de licenciement ne peut plus, une fois le licenciement intervenu, se prononcer utilement sur cette demande, dès lors qu'à compter du licenciement, la décision administrative qui l'a autorisé a épuisé tous ses effets juridiques et que, d'autre part, l'existence de la décision administrative d'autorisation de licenciement fait obstacle, devant le juge judiciaire du contrat de travail, à toute contestation du motif de licenciement.<br/>
<br/>
              6. Les dispositions de l'article L. 1232-6 du code du travail sont applicables au litige et n'ont pas déjà été déclarées conformes à la Constitution par le Conseil constitutionnel. En outre, le moyen tiré de ce que, en tant qu'elles ne prévoient pas de garanties de nature à ce que, lorsque le licenciement est subordonné à une autorisation administrative, le juge des référés puisse statuer utilement, s'il en est saisi, sur une demande de suspension de l'exécution de cette autorisation, elles portent atteinte aux droits et libertés garantis par la Constitution, notamment au droit à un recours effectif, soulève une question présentant un caractère sérieux. <br/>
<br/>
              7. En revanche, les dispositions de l'article L. 521-1 du code de justice administrative ont seulement pour objet de déterminer les règles de droit commun applicables à la procédure de référé suspension devant le juge administratif. Par suite, le moyen tiré de ce que, faute de comporter la garantie exposée au point 5, elles porteraient atteinte au droit des salariés protégés à un recours effectif ou violeraient, à leur détriment, le principe d'égalité devant la loi, qui ne soulève pas de question nouvelle, ne présente pas de caractère sérieux. Ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée à son encontre, le moyen tiré de ce que cet article porterait atteinte aux droits et libertés que garantit la Constitution doit être écarté. <br/>
<br/>
              8. Il résulte de tout ce qui précède qu'il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. B...en tant seulement qu'elle porte sur l'article L. 1232-6 du code du travail. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité à la Constitution des dispositions de l'article L. 1232-6 du code du travail, en tant qu'elles ne prévoient pas de garanties de nature à ce que, lorsque le licenciement est subordonné à une autorisation administrative, le juge des référés puisse statuer utilement, s'il en est saisi, sur une demande de suspension de l'exécution de cette autorisation, est renvoyée au Conseil constitutionnel.<br/>
Article 2 : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. B...en tant qu'elle porte sur l'article L. 521-1 du code de justice administrative.<br/>
Article 3 : Il est sursis à statuer sur le pourvoi de M. B...jusqu'à ce que le Conseil constitutionnel ait tranché la question de constitutionnalité mentionnée à l'article 1er de la présente décision. <br/>
Article 4 : La présente décision sera notifiée à M. A...B..., à la société Fiducial Private Security et à la ministre du travail.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
