<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026687507</ID>
<ANCIEN_ID>JG_L_2012_11_000000353116</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/68/75/CETATEXT000026687507.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 26/11/2012, 353116, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-11-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353116</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Marc Dandelot</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>M. Louis Dutheillet de Lamothe</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:353116.20121126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu l'ordonnance n° 1102264 du 1er octobre 2011, enregistrée le 4 octobre 2011 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président du tribunal administratif de Rennes a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée à ce tribunal par la société Soditelmer ;<br/>
<br/>
              Vu la requête enregistrée au greffe du tribunal administratif de Rennes le 7 juin 2011, présentée par la société Soditelmer, dont le siège est rue du Loch, à Camaret-sur-Mer (29570) ; la société Soditelmer demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 737 T - 757 T du 13 avril 2011 par laquelle la Commission nationale d'aménagement commercial a refusé de lui accorder l'autorisation préalable requise en vue de la création d'un supermarché à l'enseigne " U Express " d'une surface de 1 400 m² sur le territoire de la commune de Telgruc-sur-Mer  (Finistère) ; <br/>
<br/>
              2°) de mettre solidairement à la charge de l'Etat, de la société Crozondis et de la société Sojea une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu la loi n° 73-1193 du 27 décembre 1973 ;<br/>
<br/>
              Vu la loi n° 2008-776 du 4 août 2008 ;<br/>
<br/>
              Vu le décret n° 2008-1212 du 24 novembre 2008 ;<br/>
<br/>
              Vu le décret n° 2011-921 du 1er août 2011 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jérôme Marchand-Arvier, Maître des Requêtes,  <br/>
<br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par la société Sojea ;<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 752-17 du code de commerce : " A l'initiative du préfet, du maire de la commune d'implantation, du président de l'établissement public de coopération intercommunale visé au b du 1° du II de l'article L. 751-2, de celui visé au e du même 1° du même article ou du président du syndicat mixte visé au même e et de toute personne ayant intérêt à agir, la décision de la commission départementale d'aménagement commercial peut, dans un délai d'un mois, faire l'objet d'un recours devant la Commission nationale d'aménagement commercial. La Commission nationale se prononce dans un délai de quatre mois à compter de sa saisine. / La saisine de la commission nationale est un préalable obligatoire à un recours contentieux à peine d'irrecevabilité de ce dernier. " ; que l'institution par ces dispositions d'un recours administratif, préalable obligatoire à la saisine du juge, a pour effet de laisser à l'autorité compétente pour en connaître le soin d'arrêter définitivement la position de l'administration ; qu'il s'ensuit que la décision prise à la suite du recours se substitue nécessairement à la décision initiale ; que, dès lors, lorsque la Commission nationale d'aménagement commercial rejette implicitement ou explicitement le recours d'un tiers dirigé contre une décision d'autorisation, la décision qu'elle prend ainsi a la nature d'une nouvelle autorisation créatrice de droits délivrée au pétitionnaire, qui se substitue à l'autorisation initiale ;<br/>
<br/>
              2. Considérant que lorsque le rejet d'un tel recours prend la forme d'une décision implicite, cette décision s'analyse comme une décision implicite d'acceptation au sens de l'article 23 de la loi du 12 avril 2000 qui peut dès lors être retirée, pour illégalité, selon les dispositions de cet article : " / 1° Pendant le délai de recours contentieux, lorsque des mesures d'information des tiers ont été mises en oeuvre ; / 2° Pendant le délai de deux mois à compter de la date à laquelle est intervenue la décision, lorsqu'aucune mesure d'information des tiers n'a été mise en oeuvre ; / 3° Pendant la durée de l'instance au cas où un recours contentieux a été formé " ; qu'il résulte de ce qui précède que le délai de quatre mois dans lequel la Commission nationale d'aménagement commercial doit statuer en application des dispositions précitées n'est pas imparti à peine de dessaisissement ; que la commission nationale pouvait légalement retirer les décisions implicites nées de son silence les 17 mars et 3 avril 2011 dans le délai de deux mois à compter de cette date, dès lors que celles-ci étaient illégales ;<br/>
<br/>
              3. Considérant que le moyen de ce que la commission nationale aurait statué sans recueillir les avis des ministres intéressés manque en fait ;<br/>
<br/>
              4. Considérant qu'il appartient aux commissions d'aménagement commercial, lorsqu'elles se prononcent sur un projet d'exploitation commerciale soumis à autorisation en application de l'article L. 752-1 du code de commerce, d'apprécier la conformité de ce projet aux objectifs prévus à l'article 1er de la loi du 27 décembre 1973 et à l'article L. 750-1 du code de commerce, au vu des critères d'évaluation mentionnés à l'article L. 752-6 du même code ; que l'autorisation ne peut être refusée que si, eu égard à ses effets, le projet compromet la réalisation de ces objectifs ;<br/>
<br/>
              5. Considérant que, pour apprécier la conformité à ces dispositions du projet litigieux, la commission nationale a relevé, d'une part, que celui-ci compromettait la réalisation de l'objectif d'aménagement du territoire du fait de ses effets sur l'animation de la vie urbaine et rurale, les flux de transport, qu'au demeurant, les infrastructures actuelles de desserte du projet ne permettraient pas un accès sécurisé à la clientèle, et d'autre part, qu'il méconnaissait l'objectif de développement durable faute de s'insérer dans un réseau de transports en commun ou de modes de déplacement doux et du fait de son insertion paysagère insuffisante ; qu'il ne ressort par des pièces du dossier que l'appréciation portée par la commission nationale quant à l'atteinte portée par le projet sur l'animation de la vie urbaine ait été erronée, au regard de la situation du site en secteur excentré de Telgruc-sur-Mer, dans une zone rurale d'habitat dispersé que traverse un axe routier fréquenté de la surface de vente du projet ; qu'il ressort des pièces du dossier que le projet générera un surcroît de circulation, en l'absence de desserte du site suffisante par les transports en commun ou des modes de transports doux, et que sa conception architecturale contrastera avec son environnement rural ; que dès lors, la Commission nationale d'aménagement commercial a fait une exacte application des dispositions mentionnées ci-dessus en estimant que le projet projeté n'était pas conforme aux objectifs fixés par le législateur ; que, par suite, la décision implicite née de l'expiration du délai de quatre mois prévu à l'article L. 751-2 du code du commerce était illégale, et la Commission nationale n'a pas commis d'illégalité en la retirant pour lui substituer une décision de refus ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la société Soditelmer n'est pas fondée à demander l'annulation de la décision attaquée ;<br/>
<br/>
              7. Considérant que ces dispositions font obstacle à ce qu'il soit fait droit aux conclusions présentées par la société Soditelmer au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Soditelmer la somme de 3 000 euros à verser à la société Sojea, au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
            Article 1er : La requête de la société Soditelmer est rejetée. <br/>
<br/>
Article 2 : La société Soditelmer versera à la société Sojea la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 3 : La présente décision sera notifiée à la société Soditelmer, à la société Crozondis, à la société Sojea et à la Commission nationale d'aménagement commercial. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
