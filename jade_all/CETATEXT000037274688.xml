<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037274688</ID>
<ANCIEN_ID>JG_L_2018_07_000000421791</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/27/46/CETATEXT000037274688.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 16/07/2018, 421791, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421791</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:421791.20180716</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au juge des référés du tribunal administratif de Versailles, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner la suspension de l'exécution de l'arrêté du 11 mai 2018 par lequel le ministre d'Etat, ministre de l'intérieur a prononcé à son encontre une mesure individuelle de contrôle administratif et de surveillance. Par une ordonnance n° 1803727 du 1er juin 2018, le juge des référés du tribunal administratif de Versailles a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 27 juin 2018 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
              1°) d'annuler l'ordonnance contestée ;<br/>
<br/>
              2°) de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence est présumée remplie eu égard à la nature de la décision litigieuse ;<br/>
              - la décision contestée porte une atteinte grave et manifestement illégale à sa liberté d'aller et venir ainsi qu'à son droit de mener une vie privée et familiale normale ; <br/>
              - le ministre se borne, pour fonder la mesure, à faire référence à des notes blanches rapportant des informations imprécises et non circonstanciées sur des faits inexacts ;<br/>
              - le ministre s'est fondé sur un faisceau d'indices erronés ou inexactement interprétés pour estimer que son comportement constituait une menace d'une particulière gravité au sens de l'article L. 228-1 du code de la sécurité intérieure ;<br/>
              - c'est à tort que le ministre estime qu'il est en relation de manière habituelle avec des personnes incitant, facilitant ou participant à des actes de terrorisme, en se référant ainsi à des personnes détenues placées à l'isolement alors qu'il était lui-même dans cette situation ; <br/>
              - c'est à tort que le ministre estime qu'il soutient, diffuse ou adhère à des thèses incitant à la commission d'actes de terrorisme ou faisant l'apologie de tels actes, en se référant au partage d'une publication sur un réseau social intervenue en juin 2016 qui n'avait pas une telle portée et à des faits de radicalisme ou de prosélytisme religieux pendant sa détention qui ne sont pas établis et qu'il conteste.<br/>
              Par un mémoire en défense, enregistré le 3 juillet 2018, le ministre d'Etat, ministre de l'intérieur conclut au rejet de la requête. Il fait valoir que la condition d'urgence n'est pas remplie et que les moyens soulevés par M. B...ne sont pas fondés.<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. A...B...et, d'autre part, le ministre d'Etat, ministre de l'intérieur ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 4 juillet 2018 à 11 heures 30 au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Pinatel, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. B... ;<br/>
              - le représentant de M.B... ;<br/>
<br/>
              - M.B... ;<br/>
<br/>
              - les représentants du ministre d'Etat, ministre de l'intérieur ;<br/>
              et à l'issue de laquelle le juge des référés a différé la clôture de l'instruction jusqu'au 6 juillet 2018 à 17 heures puis jusqu'au 11 juillet 2018 à 17 heures ;<br/>
<br/>
<br/>
              Vu la mesure d'instruction supplémentaire par laquelle le juge des référés a demandé au ministre de l'intérieur, le 6 juillet 2018, de produire, d'une part, les rapports d'incidents, établis par l'administration pénitentiaire et mentionnés dans la " note blanche " produite en défense, dont M. C...a fait l'objet au cours de sa détention, d'autre part, tout élément relatif aux sanctions disciplinaires auxquelles a procédé l'administration pénitentiaire à son encontre au cours de sa détention en 2016, ainsi qu'au placement à l'isolement dont il a fait l'objet le 21 décembre 2016 au cours de sa détention et, enfin, les justificatifs de la notification, faite à M. B...le 12 avril 2017, de l'arrêté d'assignation à résidence pris à son encontre le 11 avril 2017 ;<br/>
<br/>
              Vu les mémoires, enregistrés les 6 et 11 juillet 2018, par lesquels le ministre d'Etat, ministre de l'intérieur produit la décision du chef d'établissement du centre pénitentiaire de Bois d'Arcy du 21 décembre 2016 plaçant M. B...à l'isolement provisoire pour une durée de cinq jours, le procès-verbal relatif à l'expertise psychiatrique de l'intéressé sollicitée dans le cadre de sa garde à vue du 20 avril 2017 ainsi que les justificatifs de la notification, faite à M. B... le 12 avril 2017, de l'arrêté d'assignation à résidence pris à son encontre le 11 avril 2017 ; <br/>
<br/>
              Vu le mémoire, enregistré le 10 juillet 2018, par lequel M. B...persiste dans ses précédentes écritures ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la loi n° 2017-1510 du 30 octobre 2017 ;<br/>
              - le code de la sécurité intérieure ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 relative à l'aide juridique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ".<br/>
<br/>
              2. Aux termes de l'article L. 228-1 du code de la sécurité intérieure: " Aux seules fins de prévenir la commission d'actes de terrorisme, toute personne à l'égard de laquelle il existe des raisons sérieuses de penser que son comportement constitue une menace d'une particulière gravité pour la sécurité et l'ordre publics et qui soit entre en relation de manière habituelle avec des personnes ou des organisations incitant, facilitant ou participant à des actes de terrorisme, soit soutient, diffuse, lorsque cette diffusion s'accompagne d'une manifestation d'adhésion à l'idéologie exprimée, ou adhère à des thèses incitant à la commission d'actes de terrorisme ou faisant l'apologie de tels actes peut se voir prescrire par le ministre de l'intérieur les obligations prévues au présent chapitre ". Aux termes de l'article L. 228-2 du même code : " Le ministre de l'intérieur peut, après en avoir informé le procureur de la République de Paris et le procureur de la République territorialement compétent, faire obligation à la personne mentionnée à l'article L. 228-1 de : 1° Ne pas se déplacer à l'extérieur d'un périmètre géographique déterminé, qui ne peut être inférieur au territoire de la commune. La délimitation de ce périmètre permet à l'intéressé de poursuivre une vie familiale et professionnelle et s'étend, le cas échéant, aux territoires d'autres communes ou d'autres départements que ceux de son lieu habituel de résidence ; / 2° Se présenter périodiquement aux services de police ou aux unités de gendarmerie, dans la limite d'une fois par jour, en précisant si cette obligation s'applique les dimanches et jours fériés ou chômés ; / 3° Déclarer son lieu d'habitation et tout changement de lieu d'habitation. / Les obligations prévues aux 1° à 3° du présent article sont prononcées pour une durée maximale de trois mois à compter de la notification de la décision du ministre. Elles peuvent être renouvelées par décision motivée, pour une durée maximale de trois mois, lorsque les conditions prévues à l'article L. 228-1 continuent d'être réunies. Au-delà d'une durée cumulée de six mois, chaque renouvellement est subordonné à l'existence d'éléments nouveaux ou complémentaires. La durée totale cumulée des obligations prévues aux 1° à 3° du présent article ne peut excéder douze mois. Les mesures sont levées dès que les conditions prévues à l'article L. 228-1 ne sont plus satisfaites. / Toute décision de renouvellement des obligations prévues aux 1° à 3° du présent article est notifiée à la personne concernée au plus tard cinq jours avant son entrée en vigueur. Si la personne concernée saisit le juge administratif d'une demande présentée sur le fondement de l'article L. 521-2 du code de justice administrative dans un délai de quarante-huit heures à compter de la notification de la décision, la mesure ne peut entrer en vigueur avant que le juge ait statué sur la demande. / La personne soumise aux obligations prévues aux 1° à 3° du présent article peut, dans un délai d'un mois à compter de la notification de la décision ou à compter de la notification de chaque renouvellement, demander au tribunal administratif l'annulation de cette décision. Le tribunal administratif statue dans un délai de deux mois à compter de sa saisine. Ces recours s'exercent sans préjudice des procédures prévues aux articles L. 521-1 et L. 521-2 du code de justice administrative ".<br/>
<br/>
              3. Il résulte de l'instruction que M.B..., connu pour des faits de délinquance de droit commun avec récidive, a été interpellé le 8 mars 2015 et condamné, par jugement du 10 mars 2015, à six mois d'emprisonnement pour des faits de conduite de véhicule à moteur malgré injonction de restituer le permis de conduire et de menace de mort ou d'atteinte aux biens dangereuse pour les personnes à l'encontre d'un dépositaire de l'autorité publique. Il a fait l'objet, le 3 juillet 2016, d'une mesure d'assignation à résidence prise en application de l'article 6 de la loi du 3 avril 1955 relative à l'état d'urgence, avec obligation de se présenter trois fois par jour au commissariat de Poissy. Interpellé pour s'être présenté au commissariat de son domicile aux Mureaux, il a été condamné à six mois d'emprisonnement pour non respect de cette obligation et incarcéré à.possibles, " en particulier en soirée en l'absence de surveillants " comme le fait valoir le ministre, ne suffit pas à caractériser une entrée en relation de manière habituelle au sens de l'article L. 228-1 du code de la sécurité intérieure Libéré le 12 avril 2017, il a de nouveau enfreint son obligation de présentation quotidienne aux services de police résultant d'un arrêté d'assignation à résidence pris le 11 avril 2017 à son encontre, sur le fondement de l'article 6 de la loi du 3 avril 1955. Interpellé le 19 avril 2017, il a été condamné à une peine d'emprisonnement d'un an pour ce motif, exécutée à la maison d'arrêt de Bois-d'Arcy. Par un arrêté du 16 février 2018, pris sur le fondement des dispositions citées ci-dessus des articles L. 228-1 et L. 228-2 du code de la sécurité intérieure, le ministre d'Etat, ministre de l'intérieur a, pour une durée de trois mois, pris à l'encontre de M.B..., libéré le 19 février 2018, une mesure individuelle de contrôle administratif et de surveillance. Par l'arrêté litigieux du 11 mai 2018, le ministre d'Etat, ministre de l'intérieur, a décidé de renouveler, pour une durée de trois mois à compter du 18 mai 2018, cette mesure qui fait obligation à M. B...de ne pas se déplacer en dehors du territoire de la commune des Mureaux, de se présenter tous les jours de la semaine, y compris les dimanches, les jours fériés ou chômés, à 14 heures, à l'hôtel de police des Mureaux, d'y déclarer son lieu d'habitation dans un délai de vingt-quatre heures à compter de la notification de l'arrêté ainsi que tout changement de domicile. M. B...relève appel de l'ordonnance du 1er juin 2018 par laquelle le juge des référés du tribunal administratif de Versailles a rejeté sa demande, présentée sur le fondement de l'article L. 521-2 du code de justice administrative, tendant à la suspension des effets de cette mesure.<br/>
<br/>
              Sur la condition d'urgence :<br/>
<br/>
              4. Eu égard à son objet et à ses effets, notamment aux restrictions apportées à la liberté d'aller et venir, une décision prise par l'autorité administrative en application des articles L. 228-1 et L. 228-2 du code de la sécurité intérieure, porte, en principe et par elle-même, sauf à ce que l'administration fasse valoir des circonstances particulières, une atteinte grave et immédiate à la situation de cette personne, de nature à créer une situation d'urgence justifiant que le juge administratif des référés, saisi sur le fondement de l'article L. 521-2 du code de justice administrative, puisse prononcer dans de très brefs délais, si les autres conditions posées par cet article sont remplies, une mesure provisoire et conservatoire de sauvegarde. Les éléments que le ministre de l'intérieur fait valoir, dans ses écritures et au cours de l'audience publique, et notamment la circonstance que le requérant n'a pas contesté la première mesure de contrôle administratif et de surveillance prise à son encontre, ne conduisent pas à remettre en cause, au cas d'espèce, l'existence d'une situation d'urgence caractérisée de nature à justifier l'intervention du juge des référés dans les conditions d'urgence particulière prévues par l'article L. 521-2 du code de justice administrative.<br/>
<br/>
              Sur la condition tenant à l'atteinte grave et manifestement illégale à une liberté fondamentale :<br/>
<br/>
              5. Il appartient au juge des référés de s'assurer, en l'état de l'instruction devant lui, que l'autorité administrative, opérant la conciliation nécessaire entre le respect des libertés et la sauvegarde de l'ordre public, n'a pas porté d'atteinte grave et manifestement illégale à une liberté fondamentale dans l'application de l'article L. 228-1 du code de la sécurité intérieure qui permet de prendre à l'égard d'une personne les mesures individuels de contrôle administratif et de surveillance prévues aux articles suivants, dont celles de l'article L. 228-2. Il résulte de l'article L. 228-1 du code de la sécurité intérieure que ces mesures doivent être prises aux seules fins de prévenir la commission d'actes de terrorisme et sont subordonnées à deux conditions cumulatives, la première tenant à la menace d'une particulière gravité pour la sécurité et l'ordre publics résultant du comportement de l'intéressé, la seconde aux relations qu'il entretient avec des personnes ou des organisations incitant, facilitant ou participant à des actes de terrorisme ou, de façon alternative, au soutien, à la diffusion ou à l'adhésion à des thèses incitant à la commission d'actes de terrorisme ou faisant l'apologie de tels actes.<br/>
<br/>
              6. Il résulte de l'instruction que le ministre d'Etat, ministre de l'intérieur s'est fondé, s'agissant de cette seconde condition, d'une part, sur la circonstance que M. B...a partagé sur son compte Facebook, le 14 juin 2016, une publication relative à l'assassinat d'un fonctionnaire de police et de sa conjointe commis le 13 juin 2016 au nom de " Daesh ", sur sa pratique radicale de l'islam et sa volonté d'endoctrinement d'autres détenus au cours de son incarcération au centre pénitentiaire de Bois-d'Arcy du 12 juillet 2016 au 12 avril 2017, et, enfin, sur sa " haine à l'égard de la France et de ses institutions " et sa pratique du takfirisme, composante du salafisme, d'autre part, sur les contacts qu'il a noués durant ses incarcérations, d'abord avec deux détenus condamnés pour faits de terrorisme détenus à la maison d'arrêt de Bois-d'Arcy puis avec plusieurs individus incarcérés à la maison d'arrêt de Nanterre pour des faits de participation à une association de malfaiteurs en vue de la préparation d'un acte de terrorisme ou suivis au titre de la radicalisation. <br/>
<br/>
              7. Considérant que M. B...conteste une grande partie de ces faits ou l'interprétation qui en est retenue par le ministre.<br/>
<br/>
              8. S'agissant, en premier lieu, de l'appréciation du ministre selon laquelle il doit être regardé comme soutenant et diffusant des thèses incitant à la commission d'actes de terrorisme, M. B...ne conteste pas avoir partagé sur son compte Facebook le 14 juin 2016 une publication émanant d'une autre personne relative à l'acte terroriste commis la veille, mais il fait valoir que cette publication, qui qualifie l'acte en cause de " crime ", et comporte la mention " Que Dieu te pardonne " destinée à son auteur, le bref commentaire que M. B...a lui-même rédigé appelant à sa miséricorde, ne fait l'éloge ni de l'acte ni de son auteur. M. B...admet par ailleurs sa pratique de l'islam tout en réfutant son radicalisme et son prosélytisme religieux. Il dénie les appréciations portées sur sa relation aux institutions. <br/>
<br/>
              9. Il résulte de l'instruction que la publication sur le compte Facebook de l'intéressé en date du 14 juin 2016 est au nombre des éléments qui, eu égard à sa teneur, ont justifié la première mesure d'assignation à résidence prise à l'encontre de l'intéressé. Cette publication est cependant demeurée isolée, aucun autre acte ne lui étant reproché, se rapportant à la diffusion de thèses incitant à la commission d'actes de terrorisme ou faisant l'apologie de tels actes. La perquisition effectuée à son domicile en avril 2017, dont il est fait état dans la " note blanche " des services de renseignement versée au débat contradictoire, fait état de la présence de nombreux ouvrages en langue arabe et en français portant sur l'islam, mais pas d'éléments en relation avec une organisation terroriste. Les faits de radicalisme et de prosélytisme religieux invoqués par le ministre et repris de cette même " note blanche " sont issus des périodes d'incarcération de l'intéressé. Invité à produire les éléments circonstanciés relatifs à ces faits survenus en détention, le ministre a produit la décision du chef d'établissement du centre pénitentiaire de Bois d'Arcy du 21 décembre 2016 plaçant l'intéressé à l'isolement provisoire pour une durée de cinq jours, motivée par son " discours radical " et son " prosélytisme récurrent et agressif ". Cette décision repose sur trois relevés de l'administration pénitentiaire, le premier en date du 13 septembre 2016 relatif à l'inscription au culte, le deuxième du 9 décembre 2016 tiré d'une conversation au retour du culte, le troisième du 21 décembre 2016, seul qualifié de compte rendu d'incident, relatant une altercation avec un autre détenu au sujet de la pratique de la religion musulmane et de l'attitude à l'égard des autres religions. Cependant, si ces éléments révèlent de la part du requérant une relation équivoque à la religion, susceptible de justifier, dans un contexte de menaces d'atteintes graves à l'ordre public prenant appui sur ce type de considérations, des mesures de surveillance autres que celles ici en cause, elles ne suffisent pas à caractériser le soutien ou la diffusion de thèses incitant à la commission d'actes de terrorisme pour l'application de l'article L. 228-1 du code de la sécurité intérieure. Par ailleurs, si la " note blanche " fait état de la pratique du takfirisme et de " la haine à l'égard de la France et de ses institutions ", ces éléments non circonstanciés sont rapportés d'un " entretien administratif " effectué avec un autre détenu en avril 2017, sans être confortés par d'autres informations relevées par l'administration pénitentiaire quant au comportement de l'intéressé pendant sa détention. Le ministre fait au surplus valoir des incidents tirés de la seconde incarcération de M. B..., du 19 avril 2017 au 19 février 2018, relatifs à des actes de prosélytisme et à des menaces proférés à l'égard d'un agent de l'administration pénitentiaire. Si ces incidents sont cités par la " note blanche ", ils ne sont pas précisés par le ministre qui ne fournit aucun élément issu de l'administration pénitentiaire à leur sujet, tel des observations, des rapports d'incident ou des mesures prises à l'égard du détenu. Enfin, aucun fait n'est relevé à l'encontre de l'intéressé depuis sa libération intervenue le 19 février 2018.<br/>
<br/>
              10. En second lieu, M. B...fait valoir que l'appréciation du ministre selon laquelle il doit être regardé comme entrant en relation de manière habituelle avec des personnes incitant, facilitant ou participant à des actes de terrorisme, repose sur son placement à l'isolement en même temps et dans les mêmes lieux que ces personnes. Il résulte de l'instruction que les personnes citées par la " note blanche ", au nombre de deux pour sa première période d'incarcération, du 21 décembre 2016 au 12 avril 2017, et au nombre de huit pour sa seconde période d'incarcération, du 19 avril 2017 au 19 février 2018, étaient également placées à l'isolement. Il n'est pas contesté que M. B...et ces personnes ne bénéficiaient pas d'activité commune. Dès lors, à défaut d'éléments circonstanciés sur les relations que M. B...aurait entretenues avec elles, la seule circonstance que des contacts demeurent.possibles, " en particulier en soirée en l'absence de surveillants " comme le fait valoir le ministre, ne suffit pas à caractériser une entrée en relation de manière habituelle au sens de l'article L. 228-1 du code de la sécurité intérieure Par ailleurs, le ministre ne soutient pas que M. B...ait établi ou tenté d'établir des contacts avec des personnes ou des organisations incitant, facilitant ou participant à des actes de terrorisme depuis la fin de son incarcération.<br/>
<br/>
              11. Il résulte de ce qui précède, que faute de caractériser le soutien, la diffusion ou l'adhésion à des thèses incitant à la commission d'actes de terrorisme ou faisant l'apologie de tels actes ou, alternativement, une relation habituelle avec des personnes ou des organisations incitant, facilitant ou participant à des actes de terrorisme, l'arrêté litigieux qui renouvelle une première mesure individuelle de contrôle administratif et de surveillance prise à l'encontre de M. B..., est entaché d'une illégalité manifeste. Il y a lieu, par suite, d'en suspendre l'exécution. Il suit de là que M. B...est fondé à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Versailles a refusé de faire droit à sa demande.<br/>
<br/>
              12. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement d'une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Versailles du 1er juin 2018 est annulée.<br/>
Article 2 : L'exécution de l'arrêté du ministre d'Etat, ministre de l'intérieur en date du 11 mai 2018 est suspendue.<br/>
Article 3 : L'Etat versera une somme de 1 500 euros à M. B...en application de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente ordonnance sera notifiée à M. A...B...et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
