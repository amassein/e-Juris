<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000020381693</ID>
<ANCIEN_ID>JG_L_2008_10_000000292894</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/20/38/16/CETATEXT000020381693.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 29/10/2008, 292894, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2008-10-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>292894</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin</PRESIDENT>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>M. Benoit  Bohnert</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Collin Pierre</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 27 avril et 7 août 2006 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SA NORD DISTRIBUTION AUTOMOBILE - S.E., dont le siège est situé 537 rue de Cambrai B.P. 100 à Douai (59502) ; la SA NORD DISTRIBUTION AUTOMOBILE - S.E. demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 22 février 2006 par lequel la cour administrative d'appel de Douai a rejeté sa requête tendant, d'une part, à l'annulation du jugement du 22 janvier 2004 du tribunal administratif de Lille rejetant sa demande tendant à la décharge des droits supplémentaires de taxe sur la valeur ajoutée et des pénalités correspondantes qui lui ont été assignés pour la période du 1er décembre 1997 au 28 février 1998, d'autre part, à ce que soit prononcée cette décharge ;<br/>
<br/>
              2°) réglant l'affaire au fond, de prononcer la décharges des impositions en litige et des pénalités correspondantes ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la directive 77/388/CEE du Conseil du 17 mai 1977, modifiée par la directive 94/5/CE du Conseil du 14 février 1994 ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Benoit Bohnert, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Célice, Blancpain, Soltner, avocat de la SA NORD DISTRIBUTION AUTOMOBILE - S.E., <br/>
<br/>
              - les conclusions de M. Pierre Collin, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SA NORD DISTRIBUTION AUTOMOBILE - S.E., qui a pour activité le négoce de voitures d'occasion, a fait l'objet d'une vérification de comptabilité au titre de la taxe sur la valeur ajoutée due au cours de la période comprise entre le 1er décembre 1997 et le 28 février 1998 ; qu'à l'occasion de ce contrôle, l'administration fiscale a constaté que cette société a notamment acquis auprès de fournisseurs français des véhicules d'occasion immatriculés dans d'autres Etats membres de la Communauté européenne, et qu'elle a maintenu, lors de la revente de ces véhicules, le régime de taxation sur la marge, prévu par les dispositions de l'article 297 A du code général des impôts, dont ses fournisseurs avaient eux-mêmes précédemment fait application à l'occasion de la livraison de ces véhicules ; qu'en se fondant notamment sur la circonstance que ces derniers avaient appartenu initialement à des sociétés de location de voitures ayant bénéficié du droit à déduction de la taxe sur la valeur ajoutée qui a grevé leur prix d'acquisition, l'administration fiscale a assujetti l'ensemble des opérations de revente de ces véhicules par la SA NORD DISTRIBUTION AUTOMOBILE - S.E. à la taxe sur la valeur ajoutée sur le prix de vente total, et assorti les rappels de taxe correspondants de la pénalité de 40 % prévue par les dispositions de l'article 1729 du code général des impôts ; que cette société se pourvoit en cassation contre l'arrêt du 22 février 2006 par lequel la cour administrative d'appel de Douai a rejeté sa requête tendant à l'annulation du jugement du 22 janvier 2004 par lequel le tribunal administratif de Lille a rejeté sa demande tendant à la décharge des droits supplémentaires de taxe sur la valeur ajoutée et des pénalités correspondantes qui lui ont été assignés pour la période du 1er décembre 1997 au 28 février 1998 ;<br/>
<br/>
              Sur le bien-fondé des rappels de taxe sur la valeur ajoutée :<br/>
<br/>
              Considérant qu'aux termes du I de l'article 256 bis du code général des impôts : 1° Sont également soumises à la taxe sur la valeur ajoutée les acquisitions intracommunautaires de biens meubles corporels effectuées à titre onéreux par un assujetti agissant en tant que tel ou par une personne morale non assujettie lorsque le vendeur est un assujetti agissant en tant que tel et qui ne bénéficie pas dans son Etat du régime particulier de franchise des petites entreprises (...) / 2° bis Les acquisitions intracommunautaires de biens d'occasion (...) effectuées à titre onéreux par un assujetti (...) ne sont pas soumises à la taxe sur la valeur ajoutée lorsque le vendeur ou l'assujetti est un assujetti revendeur qui a appliqué dans l'Etat membre de départ de l'expédition ou du transport du bien les dispositions de la législation de cet Etat prises pour la mise en oeuvre des B ou C de l'article 26 bis de la directive n° 77/388/CEE du Conseil des Communautés européennes du 17 mai 1977 (...) ; qu'aux termes du I de l'article 297 A du même code : 1° La base d'imposition des livraisons par un assujetti revendeur de biens d'occasion (...) qui lui ont été livrés par un non redevable de la taxe sur la valeur ajoutée ou par une personne qui n'est pas autorisée à facturer la taxe sur la valeur ajoutée au titre de cette livraison est constituée de la différence entre le prix de vente et le prix d'achat (...) ; qu'aux termes de l'article 297 E du même code, dans sa rédaction applicable en l'espèce : Les assujettis qui appliquent les dispositions de l'article 297 A ne peuvent pas faire apparaître la taxe sur la valeur ajoutée sur leurs factures ou tous autres documents en tenant lieu ; que ces dispositions, issues de la loi de finances rectificative pour 1994 du 29 décembre 1994, ont pour objet de transposer l'article 26 bis de la sixième directive du 17 mai 1977, issu de l'article 1er de la septième directive du 14 février 1994 ; qu'il résulte desdites dispositions qu'une entreprise française assujettie à la taxe sur la valeur ajoutée a la qualité d'assujetti revendeur et peut appliquer le régime de taxation sur marge prévu par l'article 297 A du code général des impôts, lorsqu'elle revend un bien d'occasion acquis auprès d'un fournisseur implanté en France qui, en sa qualité d'assujetti revendeur, lui a délivré une facture conforme aux dispositions précitées de l'article 297 E du code général des impôts, et dont le fournisseur, situé quant à lui dans un autre Etat membre, a aussi cette qualité ou n'est pas assujetti à la taxe sur la valeur ajoutée ; que l'administration peut toutefois remettre en cause l'application de ce régime lorsque l'entreprise française ne pouvait ignorer la circonstance que son fournisseur n'avait pas la qualité d'assujetti revendeur et n'était pas autorisé à appliquer lui-même le régime de taxation sur marge prévu par l'article 26 bis de la directive du 17 mai 1977 ;<br/>
<br/>
              Considérant, en premier lieu, qu'en jugeant que, lorsqu'une entreprise produit des factures émanant de ses fournisseurs qui mentionnent que les ventes de véhicules s'effectuaient sous le régime de la taxe sur la marge mentionné ci-dessus, il incombe à l'administration, si elle s'y croit fondée, de démontrer, d'une part que les mentions portées sur ces factures sont erronées, d'autre part que le bénéficiaire de ces achats de véhicules savait ou aurait dû savoir que les opérations présentaient le caractère d'acquisitions intracommunautaires taxables sur l'intégralité du prix de revente à ses propres clients, et sans que pèse sur le contribuable l'obligation de vérifier la qualité d'assujetti revendeur de ses fournisseurs, la cour n'a pas commis d'erreur de droit ni méconnu les règles gouvernant la charge de la preuve ;<br/>
<br/>
              Considérant, en deuxième lieu, qu'après avoir relevé dans les motifs de son arrêt, d'une part, que l'administration a fait valoir, sans être contredite sur ce point, que les véhicules litigieux avaient été achetés globalement par les fournisseurs de la SA NORD DISTRIBUTION AUTOMOBILE - S.E. auprès d'utilisateurs professionnels allemands ou espagnols, qui, en leur qualité de loueurs de véhicules, étaient autorisés à facturer la taxe sur la valeur ajoutée sur les véhicules qu'ils cédaient en vertu de règles objectives applicables en matière de taxe sur la valeur ajoutée, que la société requérante, en sa qualité de professionnel de l'achat et de la vente de véhicules d'occasion, devait connaître et, d'autre part, que l'administration faisait encore valoir que la société requérante ne pouvait ignorer l'origine exacte des véhicules achetés auprès de la société Tag ou de ses autres fournisseurs, dès lors que l'un des directeurs salariés du groupe de concessionnaires et vendeurs de véhicules neufs et d'occasion qu'elle constitue avec la société Garage de l'Autoroute SE était le gérant de la société Tag et qu'elle était en possession de l'ensemble des cartes grises desdits véhicules dont les mentions révélaient, sans qu'aucun doute ne puisse s'élever sur ces informations, l'origine des véhicules achetés par ses fournisseurs, l'identité de leurs premiers propriétaires et la circonstance qu'ils avaient été initialement affectés à une activité de location de voitures ouvrant droit à déduction de la taxe sur la valeur ajoutée, la cour a pu estimer, par une appréciation souveraine qui n'est pas arguée de dénaturation, que nonobstant la circonstance que les factures délivrées à la société requérante par ses fournisseurs aient expressément fait référence aux dispositions fiscales permettant une taxation de la vente selon le régime de la marge, l'administration devait être regardée comme ayant établi d'une part, le caractère inexact des mentions portées sur les factures émises par les fournisseurs de la SA NORD DISTRIBUTION AUTOMOBILE - S.E. et, d'autre part, la circonstance que cette société ne pouvait ignorer cette situation, et déduire de ce constat, sans commettre d'erreur de droit et en motivant suffisamment son arrêt, que le service était fondé à remettre en cause l'application par la SA NORD DISTRIBUTION AUTOMOBILE - S.E. du régime de taxation sur marge ;<br/>
<br/>
              Considérant, en troisième lieu, qu'aux termes du I de l'article 242 terdecies de l'annexe II au code général des impôts, dans sa rédaction applicable aux documents invoqués : Un certificat délivré par l'administration fiscale doit être obligatoirement présenté pour obtenir l'immatriculation ou la francisation d'un moyen de transport visé au 1 du III de l'article 298 sexies du code général des impôts et provenant d'un autre Etat membre de la Communauté européenne ; qu'il résulte de ces dispositions que le visa apposé par l'administration fiscale sur le certificat 1993 VT est délivré par l'administration, sur le fondement d'un contrôle en la forme des documents présentés, pour les seuls besoins de l'immatriculation ou de la francisation d'un moyen de transport introduit en France, sans avoir pour objet de prendre position sur le régime fiscal applicable au regard de la taxe sur la valeur ajoutée, ainsi d'ailleurs qu'en témoignent les mentions figurant sur ce certificat ; que la délivrance de ce document ne peut être regardée, en l'absence de toute mention expresse en ce sens, comme ayant le caractère d'une prise de position formelle de l'administration sur le régime de taxe sur la valeur ajoutée applicable à la transaction ; que, par suite, la cour n'a pas entaché son arrêt d'une erreur de droit en jugeant que la SA NORD DISTRIBUTION AUTOMOBILE - S.E. ne saurait se prévaloir, sur le fondement de l'article L. 80 B du livre des procédures fiscales, d'une prise de position formelle de l'administration sur l'appréciation de sa situation de fait au regard de la loi fiscale résultant de la seule apposition de ce visa ;<br/>
<br/>
              Sur l'application des pénalités prévues à l'article 1729 du code général des impôts :<br/>
<br/>
              Considérant que la cour administrative d'appel de Douai a relevé, ainsi qu'il a été dit ci-dessus, qu'en sa qualité de professionnel de l'achat et de la vente de véhicules d'occasion, la société requérante ne pouvait ignorer que, compte tenu de l'origine exacte des véhicules achetés notamment auprès de la société Tag, dont elle avait nécessairement connaissance au vu des mentions portées sur les cartes grises de ces véhicules, elle n'était pas en droit d'appliquer le régime de taxation sur la marge prévu à l'article 297 A du code général des impôts aux ventes qu'elle consentait à ses clients ; qu'en déduisant de cette appréciation souveraine que l'administration devait être regardée comme apportant la preuve de l'absence de bonne foi du contribuable, la cour, qui a suffisamment motivé son arrêt, a exactement qualifié les faits de la cause et n'a pas entaché son arrêt d'une erreur de droit ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que la SA NORD DISTRIBUTION AUTOMOBILE - S.E. n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que ces dispositions font obstacle à ce que soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante, la somme demandée par la SA NORD DISTRIBUTION AUTOMOBILE - S.E. au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de SOCIETE NORD DISTRIBUTION AUTOMOBILE - S.E. est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la SOCIETE NORD DISTRIBUTION AUTOMOBILE - S.E. et au ministre du budget, des comptes publics et de la fonction publique.<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
