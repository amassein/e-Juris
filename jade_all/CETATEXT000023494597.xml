<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023494597</ID>
<ANCIEN_ID>JG_L_2011_01_000000331986</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/49/45/CETATEXT000023494597.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 28/01/2011, 331986</TITRE>
<DATE_DEC>2011-01-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>331986</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Nicolas  Polge</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Boulouis Nicolas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:331986.20110128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 14 septembre et 15 décembre 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour le DEPARTEMENT DES ALPES-MARITIMES, représenté par le président du conseil général ; le département demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 06MA02162 du 10 juillet 2009 par lequel la cour administrative d'appel de Marseille, réformant le jugement du 12 mai 2006 par lequel le tribunal administratif de Nice a, d'une part, annulé le titre exécutoire n° 432-1 émis le 1er août 2001 à l'encontre de la société Cicom Organisation, ainsi que, pour une partie de son montant, le titre n° 434-1 émis le 3 août 2001, et, d'autre part, rejeté les conclusions de la société tendant à l'annulation du titre exécutoire n° 3105-1 émis le 2 août 2001, a annulé les titres exécutoires n° 3105-1 et 434-1 ; <br/>
<br/>
              2°) de mettre la somme de 3 000 euros à la charge de la société Cicom Organisation au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Polge, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Gaschignard, avocat du DEPARTEMENT DES ALPES-MARITIMES et de la SCP Piwnica, Molinié, avocat de la société Cicom Organisation, <br/>
<br/>
              -  les conclusions de M. Nicolas Boulouis, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Gaschignard, avocat du DEPARTEMENT DES ALPES-MARITIMES et à la SCP Piwnica, Molinié, avocat de la société Cicom Organisation ;<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le DEPARTEMENT DES ALPES-MARITIMES a conclu le 26 juin 1995 un contrat avec la société Cicom Organisation pour la gestion du centre international de la communication avancée, lequel est venu à expiration le 30 novembre 2000 ; que, dans le cadre du règlement du solde du contrat, le département a émis quatre titres exécutoires, dont trois ont été contestés par cette société devant le juge administratif ; que, par un jugement du 12 mai 2006, le tribunal administratif de Nice a partiellement fait droit aux demandes de cette société ; que, saisie par cette dernière, la cour administrative d'appel de Marseille a, par l'arrêt attaqué, annulé deux titres exécutoires, accueillant ainsi l'ensemble des conclusions de la société Cicom Organisation ; <br/>
<br/>
              Considérant, en premier lieu, qu'il ressort des pièces du dossier que le DEPARTEMENT DES ALPES-MARITIMES, qui n'était pas représenté à l'audience publique, y a été régulièrement convoqué, en application de l'article R. 711-2 du code de justice administrative, contrairement à ce qu'il soutient ; qu'il n'est par suite pas fondé à soutenir que l'arrêt attaqué serait pour ce motif irrégulier ;<br/>
<br/>
              Considérant, en deuxième lieu, qu'aux termes de l'article 40 du contrat de concession liant le département à la société Cicom Organisation, inséré dans le chapitre 7 intitulé " Sanctions - contentieux " et régissant l'ensemble des litiges entre les parties : " Les parties au présent contrat conviennent que les contestations sur l'interprétation ou l'exécution de celui-ci seront soumises à un expert désigné conjointement par la collectivité et le gérant dans un délai de quinze jours après la déclaration d'un litige par l'une d'entre elles. (..) A défaut de conciliation ou d'accord sur la désignation d'un expert, les contestations qui s'élèveront entre le gérant et la collectivité au sujet du présent contrat seront soumises au tribunal administratif de Nice. " ; que, d'une part, la cour administrative d'appel n'a pas dénaturé la portée de ces stipulations contractuelles en jugeant qu'elles faisaient obstacle à ce que le département émette directement des titres exécutoires pour le règlement des sommes correspondant à une contestation relative à l'exécution du contrat, sans mettre préalablement en oeuvre la procédure de concertation consistant en une déclaration de litige et à la désignation conjointe d'un expert ; que, d'autre part, en retenant que la remise en cause des comptes de la délégation par le département, pourtant antérieurement approuvés par lui, constituait une telle contestation, la cour s'est livrée à une interprétation souveraine des faits de l'espèce, exempte de dénaturation ; qu'elle en a légalement déduit que les titres de perception émis, pour le recouvrement des sommes correspondant à cette contestation, en méconnaissance de l'obligation contractuelle de mise en oeuvre de la procédure de conciliation préalable, étaient entachés d'illégalité ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que le DEPARTEMENT DES ALPES-MARITIMES n'est pas fondé à demander l'annulation de l'arrêt de la cour administrative d'appel de Marseille du 10 juillet 2010 ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative doivent, par suite, être rejetées ; qu'il y a lieu, en revanche, de mettre à sa charge le versement à la société Cicom Organisation de la somme de 3 500 euros sur le fondement des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
     D E C I D E :<br/>
     --------------<br/>
Article 1er : Le pourvoi du DEPARTEMENT DES ALPES-MARITIMES est rejeté.<br/>
<br/>
Article 2 : Le DEPARTEMENT DES ALPES-MARITIMES versera à la société Cicom Organisation une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée au DEPARTEMENT DES ALPES-MARITIMES et à la société Cicom Organisation.<br/>
''<br/>
''<br/>
''<br/>
''<br/>
N° 331986- 2 -<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-05 MARCHÉS ET CONTRATS ADMINISTRATIFS. EXÉCUTION FINANCIÈRE DU CONTRAT. - CLAUSE DU CONTRAT SUBORDONNANT LA SAISINE DU JUGE POUR LE RÈGLEMENT DES CONTESTATIONS SUR L'INTERPRÉTATION OU L'EXÉCUTION DU CONTRAT À LA MISE EN OEUVRE PRÉALABLE D'UNE PROCÉDURE DE CONCILIATION - CONSÉQUENCE - IMPOSSIBILITÉ POUR LA COLLECTIVITÉ PUBLIQUE D'ÉMETTRE DES TITRES EXÉCUTOIRES POUR LE RÈGLEMENT DU SOLDE DU CONTRAT SANS METTRE EN OEUVRE LA PROCÉDURE DE CONCILIATION PRÉALABLE [RJ1].
</SCT>
<ANA ID="9A"> 39-05 Une stipulation contractuelle subordonnant la saisine du juge, pour le règlement des contestations sur l'interprétation ou l'exécution du contrat, à la mise en oeuvre préalable d'une procédure de conciliation, fait également obstacle à ce que la collectivité publique contractante émette directement des titres exécutoires pour le règlement des sommes correspondant à une contestation relative à l'exécution du contrat, sans mettre en oeuvre la procédure de conciliation préalable.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., pour l'irrecevabilité de la saisine du juge, en l'absence de respect par le demandeur des stipulations du contrat prévoyant une procédure administrative préalable, CE, Section, 19 janvier 1973, Société d'exploitation électrique de la rivière du Sant, n° 82338, p. 48.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
