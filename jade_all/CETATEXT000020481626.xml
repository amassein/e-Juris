<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000020481626</ID>
<ANCIEN_ID>JG_L_2009_03_000000325640</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/20/48/16/CETATEXT000020481626.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 26/03/2009, 325640, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2009-03-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>325640</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Bélaval</PRESIDENT>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Philippe  Bélaval</RAPPORTEUR>
<COMMISSAIRE_GVT/>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 27 février 2009 au secrétariat du contentieux du Conseil d'Etat, présentée par M. Sami A, élisant domicile chez Mme A, ..., et Mme Solange B épouse A, demeurant à la même adresse ; M. A et Mme B, épouse A demandent au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de la décision implicite par laquelle la commission de recours contre les décisions de refus de visa d'entrée en France a rejeté son recours dirigé contre la décision du 18 septembre 2008 du consul général de France à Ankara (Turquie) refusant à M. A un visa de long séjour en qualité de conjoint de ressortissant français ; <br/>
<br/>
              2) d'enjoindre au consul général de France à Ankara de délivrer à M. A un visa de long séjour sous astreinte de 100 euros par jour de retard à compter de la notification de l'ordonnance à intervenir, ou, à titre subsidiaire, de procéder au réexamen de la demande de visa dans un délai de quinze jours à compter de la notification de l'ordonnance à intervenir ; <br/>
<br/>
              4°) de mettre à la charge de l'Etat le versement de la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              il soutient que l'urgence résulte de la séparation imposée aux époux, de la durée du mariage et la date à laquelle le dépôt de la demande de visa a été effectuée ; que l'urgence est également caractérisée par l'état de santé de son épouse ; qu'il existe un doute sérieux quant à la légalité de la décision attaquée dans la mesure où elle est entachée d'une erreur manifeste d'appréciation ; que les éléments avancés par l'administration ne sont pas suffisants pour prouver l'absence d'intention matrimoniale ; que les époux justifient d'une communauté de vie ; que son épouse s'est rendue plusieurs fois en Turquie pour lui rendre visite ; qu'ils entretiennent leur relation par téléphone ; qu'elle lui apporte un soutien financier ; <br/>
<br/>
<br/>
              Vu la demande adressée par M. A à la commission de recours contre les refus de visa d'entée en France ; <br/>
              Vu la requête à fin d'annulation de la décision de cette commission ; <br/>
<br/>
              Vu, enregistré le 18 mars 2009, le mémoire en défense présenté par le ministre de l'immigration, de l'intégration, de l'identité nationale et du développement solidaire qui conclut au rejet de la requête ; le ministre soutient que l'administration n'a pas commis d'erreur manifeste d'appréciation sur la réalité d'une communauté de vie entre les époux ; que M. A poursuit des relations avec son ancienne compagne turque ; que le mariage a été conclu a des fins étrangères à l'institution matrimoniale ;<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
	Vu la convention européenne des droits de l'homme et de sauvegarde des libertés fondamentales ;<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. Sami A et, d'autre part, le ministre de l'immigration, de l'intégration, de l'identité nationale et du développement solidaire ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du mardi 24 mars 2009 à 12 h au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Molinié, avocat au Conseil d'Etat et à la Cour de cassation, avocat des requérants ;<br/>
<br/>
              - Mme B, épouse A;<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes des dispositions de l'article L. 521-1 du code de justice administrative : «  Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision. » ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier que M. A, ressortissant turc né en 1960, est entré en France en 1999, puis, à l'expiration de son visa, s'est maintenu sur le territoire en qualité de demandeur d'asile ; qu'après l'échec de deux demandes d'asile, il a épousé en juin 2006 une ressortissante française, puis est rentré en Turquie en mars 2007 afin de solliciter un visa de long séjour en qualité de conjoint de ressortissant français ; que l'existence alléguée d'une vie commune habituelle entre M. et Mme A depuis 2002 n'est pas établie par des documents qui ne font ressortir que la domiciliation du requérant à l'une des adresses de sa future épouse, laquelle était mariée à une autre personne jusqu'en 2006 ; qu'il est constant que les époux ne parlent aucune langue commune ; qu'il n'est pas sérieusement contesté que, depuis son retour en Turquie, M. A a repris une relation avec la compagne qu'il fréquentait avant sa venue en France et dont il a eu quatre enfants ; que si Mme A s'est rendue en Turquie à quatre reprises depuis mars 2007, il n'est établi que ces voyages étaient destinés à rendre visite à son époux, ce que conteste formellement le ministre, que pour l'un d'entre eux ; que les envois épisodiques d'argent à M. A ne sauraient à eux seuls justifier de la poursuite d'une relation affective entre les époux, non plus que les appels téléphoniques, au demeurant en nombre limité, passés en direction de la Turquie ; que dans ces conditions, un doute suffisant existe, en l'état de l'instruction, sur la sincérité de l'intention des époux d'entretenir une relation conforme aux buts en vue desquels l'institution du mariage est établie, pour que l'atteinte portée par le refus de visa opposé à M. A à leur droit à mener une vie privée et familiale normale soit regardée comme constitutive en elle-même d'une situation d'urgence au sens des dispositions précitées de l'article L. 521-1 du code de justice administrative ; que par suite la requête de M. et Mme A ne peut qu'être rejetée, y compris les conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. et Mme A est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. Sami A, à Mme Solange B, épouse A et au ministre de l'immigration, de l'intégration, de l'identité nationale et du développement solidaire.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
