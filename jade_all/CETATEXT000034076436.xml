<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034076436</ID>
<ANCIEN_ID>JG_L_2017_02_000000395101</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/07/64/CETATEXT000034076436.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 22/02/2017, 395101, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-02-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395101</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:395101.20170222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° M. A...a demandé au tribunal administratif de Toulon, d'une part, d'annuler la décision du 6 juin 2014 du ministre de l'intérieur constatant la perte de validité de son permis de conduire pour solde de points nul ainsi que les décisions de retrait de points ayant concouru à cette perte de validité et, d'autre part, d'enjoindre au ministre de l'intérieur de lui restituer son permis de conduire. Par un jugement n° 1402408 du 30 septembre 2015, le tribunal administratif a annulé la décision du 6 juin 2014, enjoint au ministre de l'intérieur de rétablir six points sur le capital du permis de conduire de l'intéressé et rejeté le surplus de ses conclusions.<br/>
<br/>
              Par un pourvoi enregistré au secrétariat du contentieux du Conseil d'Etat le 8 décembre 2015 sous le n° 395101, le ministre de l'intérieur demande au Conseil d'Etat d'annuler ce jugement en tant qu'il annule la décision du 6 juin 2014 et prononce une injonction. Il soutient que le tribunal administratif a commis une erreur de droit en jugeant que les points dont était doté le permis probatoire obtenu par M. A...le 2 février 2011 et restitué par lui à la suite de l'annulation de la décision du 30 septembre 2005 constatant la perte de validité de son permis de conduire obtenu le 3 décembre 1980 devaient être reportés sur ce permis. <br/>
<br/>
              Le pourvoi du ministre de l'intérieur a été communiqué à M. A...qui n'a pas produit de mémoire.<br/>
<br/>
<br/>
<br/>
              2° M. A...a demandé au tribunal administratif de Toulon d'annuler la décision implicite par laquelle le ministre de l'intérieur a rejeté son recours gracieux dirigé contre la décision du 6 juin 2014 constatant la perte de validité de son permis de conduire pour solde de points nul et d'enjoindre au ministre de l'intérieur de rétablir six points sur son permis de conduire. Par un jugement n° 1403194 du 30 juin 2016, le tribunal administratif a fait droit à ces conclusions.<br/>
<br/>
              Par un pourvoi enregistré au secrétariat du contentieux du Conseil d'Etat le 6 septembre 2016 sous le n° 403238, le ministre de l'intérieur demande au Conseil d'Etat d'annuler ce jugement. Il soutient que le tribunal administratif a commis une erreur de droit en jugeant que les points dont était doté le permis probatoire obtenu par M. A...le 2 février 2011 et restitué par lui à la suite de l'annulation de la décision du 30 septembre 2005 constatant la perte de validité de son permis de conduire obtenu le 3 décembre 1980 devaient être reportés sur ce permis. <br/>
<br/>
              Le pourvoi du ministre de l'intérieur a été communiqué à M. A...qui n'a pas produit de mémoire.<br/>
<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la route ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier que, par une décision du 30 septembre 2005, le ministre de l'intérieur a constaté la perte de validité du permis de conduire de M. A...pour solde de points nul ; que l'intéressé a demandé au tribunal administratif de Marseille d'annuler cette décision ; qu'alors que sa demande était pendante devant le tribunal, il a obtenu, le 2 février 2011, un nouveau permis de conduire, présentant un caractère probatoire et doté d'un capital de six points en application des dispositions du deuxième alinéa de l'article L. 223-1 du code de la route ; qu'à la suite de l'annulation, par un jugement du 24 mars 2011, de la décision du 30 septembre 2005 et de plusieurs des décisions de retrait de points sur lesquelles elle reposait, M. A...a restitué son permis probatoire à l'administration et s'est vu remettre son permis initial ; que, par une décision du 6 juin 2014, le ministre de l'intérieur a constaté qu'en raison de nouvelles infractions ayant entraîné des retraits de points ce permis avait perdu sa validité ; que, par un recours gracieux présenté le 25 juin 2014, l'intéressé a demandé au ministre de reporter sur ce permis les six points dont le permis probatoire était affecté au moment de sa restitution et, en conséquence, de constater que le solde de points n'était pas nul et de rapporter sa décision ; que, par des requêtes distinctes, il a demandé au tribunal administratif de Toulon, d'une part, d'annuler la décision du 6 juin 2014 et les décisions de retrait de points sur lesquelles elle reposait et, d'autre part, d'annuler la décision implicite rejetant son recours gracieux ; que le ministre se pourvoit en cassation, sous le n° 395101, contre le jugement rendu le 30 septembre 2015 sur la première requête, en tant qu'après avoir rejeté les conclusions dirigées contre les décisions de retrait de points il annule la décision constatant la perte de validité du permis et enjoint à l'administration de rétablir six points sur le capital du permis de conduire de M. A...et, sous le n° 403238, contre le jugement rendu le 30 juin 2016 sur la seconde requête, en tant qu'il annule la décision rejetant le recours gracieux de l'intéressé et prononce la même injonction ; qu'il y a lieu de joindre ces pourvois pour statuer par une seule décision ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 223-1 du code de la route : " Le permis de conduire est affecté d'un nombre de points. Celui-ci est réduit de plein droit si le titulaire du permis a commis une infraction pour laquelle cette réduction est prévue./ (...)/ Lorsque le nombre de points est nul, le permis perd sa validité./ (...) " ; qu'aux termes de l'article L. 223-5 du même code : " I.- En cas de retrait de la totalité des points, l'intéressé reçoit de l'autorité administrative l'injonction de remettre son permis de conduire au préfet de son département de résidence et perd le droit de conduire un véhicule./ II.- Il ne peut obtenir un nouveau permis de conduire avant l'expiration d'un délai de six mois à compter de la date de remise de son permis au préfet et sous réserve d'être reconnu apte après un examen ou une analyse médical, clinique, biologique et psychotechnique effectué à ses frais. Ce délai est porté à un an lorsqu'un nouveau retrait de la totalité des points intervient dans un délai de cinq ans suivant le précédent./ (...) " ;<br/>
<br/>
              3. Considérant que, lorsque la décision du ministre de l'intérieur constatant la perte de validité d'un permis de conduire pour solde de points nul est annulée par le juge administratif, cette décision est réputée n'être jamais intervenue ; que, pour déterminer si l'intéressé peut, en exécution du jugement, prétendre à la restitution du permis par l'administration, il y a lieu de vérifier que son solde de points n'est pas nul ; que le solde doit être calculé en tenant compte, en premier lieu, des retraits de points sur lesquels reposait la décision annulée qui n'ont pas été regardés comme illégaux par le juge, en deuxième lieu, des retraits justifiés par des infractions qui n'avaient pas été prises en compte par cette décision, y compris celles que l'intéressé a pu commettre en conduisant avec un nouveau permis obtenu dans les conditions prévues au II précité de l'article L. 223-5 du code de la route, et, enfin, des reconstitutions de points prévues par les dispositions applicables au permis illégalement retiré ;<br/>
<br/>
              4. Considérant qu'une même personne ne saurait disposer de plus d'un permis de conduire ; que, par suite, le requérant qui obtient l'annulation d'une décision constatant la perte de validité de son permis alors qu'il s'est vu délivrer un nouveau permis ne peut prétendre à la restitution par l'administration de son permis initial, sous réserve que son solde de points, calculé comme indiqué au point 3, ne soit pas nul, qu'à la condition que lui-même restitue le nouveau permis ; que, s'il lui est loisible de renoncer au bénéfice de son permis initial et de conserver son nouveau permis, il ne saurait prétendre, en cas de récupération de son permis initial, au transfert sur ce permis des points dont le nouveau permis était doté ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que le tribunal administratif de Toulon a commis une erreur de droit en jugeant que les six points dont le nouveau permis de conduire de M. A...était doté auraient dû être transférés sur son permis initial lorsqu'il lui a été remis en échange du nouveau permis ; que cette erreur doit entraîner l'annulation des articles 1er et 2 du jugement du 30 septembre 2015 et des articles 1er et 2 du jugement du 30 juin 2016 ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-1 du code de justice administrative ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui a été dit ci-dessus que le moyen, invoqué par M. A...à l'encontre de la décision du 6 juin 2014 du ministre de l'intérieur et de la décision rejetant son recours gracieux, tiré de ce que les six points dont son permis de conduire probatoire était doté devaient être reportés sur le capital de son permis initial, n'est pas fondé ;<br/>
<br/>
              8. Considérant que le moyen tiré de ce que la décision du 6 juin 2014 doit être annulée par voie de conséquence de l'annulation des décisions de retrait de points sur lesquelles elle repose ne saurait être accueilli dès lors que les conclusions de M. A...tendant à l'annulation de ces décisions ont été rejetées par l'article 3 du jugement du 30 septembre 2015, qui est devenu définitif sur ce point ; <br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que M. A...n'est pas fondé à demander l'annulation de la décision du 6 juin 2014 du ministre de l'intérieur et de la décision rejetant son recours gracieux ; que ses conclusions à fin d'injonction doivent, par suite, être rejetées ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er et 2 du jugement du 30 septembre 2015 du tribunal administratif de Toulon sont annulés.<br/>
<br/>
Article 2 : Les articles 1er et 2 du jugement du 30 juin 2016 du tribunal administratif de Toulon sont annulés.<br/>
<br/>
Article 3 : Les demandes présentées par M. A...devant le tribunal administratif sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. B...A...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
