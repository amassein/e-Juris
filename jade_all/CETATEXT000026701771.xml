<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026701771</ID>
<ANCIEN_ID>JG_L_2012_11_000000340971</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/70/17/CETATEXT000026701771.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème sous-section jugeant seule, 28/11/2012, 340971, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-11-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>340971</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Thierry Tuot</PRESIDENT>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>M. François Loloum</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:340971.20121128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi et le mémoire complémentaire, enregistrés les 28 juin et 27 septembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la Caisse Méditerranéenne de Financement (CAMEFI), dont le siège social est Hôtel de direction des Docks, 10, Place de la Joliette à Marseille (13002) ; la CAMEFI demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 29 avril 2010 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant à l'annulation du jugement du tribunal administratif de Marseille en date du 16 mai 2007 ayant rejeté sa demande d'être déchargée des suppléments d'impôt sur les sociétés et de la contribution exceptionnelle à cet impôt ainsi que des pénalités correspondantes auxquels elle a été assujettie au titre des années 1997 à 1999 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de lui accorder la réduction d'imposition litigieuse ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Loloum, Conseiller d'Etat,<br/>
<br/>
              - les observations de la SCP Célice, Blancpain, Soltner, avocat de la CAMEFI,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Célice, Blancpain, Soltner, avocat de la CAMEFI ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'une vérification de comptabilité portant sur les exercices clos en 1997, 1998 et 1999, l'administration a estimé que la Caisse Méditerranéenne de Financement (CAMEFI) avait, d'une part, commis un acte anormal de gestion en consentant à la SARL FEDIMO des avances en compte courant rémunérées au taux de 0,5 % inférieur au taux auquel elle-même se refinançait auprès de la Caisse fédérale et, d'autre part, procédé à un transfert de bénéfices à l'étranger au sens de l'article 57 du code général des impôts en affectant à son agence installée à Monaco les intérêts versés en 1998 et 1999 par le Crédit Mutuel méditerranéen à raison du solde positif de son compte courant ; que la CAMEFI demande l'annulation de l'arrêt de la cour administrative d'appel de Marseille qui a rejeté sa requête tendant à la décharge des suppléments d'impôt sur les sociétés et de la contribution exceptionnelle à cet impôt résultant de ces redressements ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la CAMEFI, établissement bancaire appartenant au réseau du Crédit mutuel, a confié en 1995 à la SARL FEDIMO, société du même groupe qui exerçait une activité de marchand de biens, la mission d'intervenir dans les enchères portant sur les immeubles grevés de sûreté de ses clients défaillants et, le cas échéant, de se porter adjudicataire de la vente de ces biens ; que pour couvrir les frais financiers de ces opérations, y compris le coût des acquisitions, ainsi que les pertes éventuelles lors de la revente des biens, la CAMEFI a consenti à la SARL FEDIMO des avances assorties d'intérêts dont le taux a été fixé en 1997 à 0,5 %, taux inférieur au taux de refinancement que supportait alors la CAMEFI auprès de la Caisse fédérale du groupe ; qu'il était également convenu qu'en cas de revente dégageant des plus-values, la SARL FEDIMO verserait à la CAMEFI un intérêt participatif dont le taux ne pourrait excéder ce même taux de refinancement ;<br/>
<br/>
              3. Considérant qu'en jugeant que l'administration établissait que le taux pratiqué de 0,5 % constituait un avantage consenti à un tiers en se fondant comme élément de comparaison pertinent sur le taux de refinancement qui représentait pour la CAMEFI le coût de revient de ses avances, la cour n'a commis ni erreur de droit ni erreur de qualification juridique ; qu'après avoir relevé, par une appréciation souveraine des faits, que la CAMEFI, par ses avances, supportait la réalité des risques financiers des opérations menées par la SARL FEDIMO et que l'intérêt qu'elle pouvait percevoir en cas de revente bénéficiaire était plafonné au taux de refinancement, la cour a pu, sans méconnaître le régime de dévolution de la preuve, regarder l'administration comme établissant le caractère anormal du taux de 0,5 % à défaut pour la CAMEFI de justifier de l'existence d'une contrepartie à son profit ; qu'elle n'était pas tenue de répondre au moyen inopérant de la requérante tiré de l'absence de volonté d'accorder une libéralité ; qu'elle n'avait pas davantage à examiner les moyens tirés de la méconnaissance du champ d'application de la loi au regard des dispositions de l'article 216 A du code général des impôts dès lors que l'opération en cause n'avait pas le caractère d'un abandon de créance  auquel ces dispositions s'appliquent exclusivement ;<br/>
<br/>
              Sur les avances versées à l'agence monégasque :<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la Caisse Fédérale du Crédit Mutuel a accordé à la CAMEFI pour son agence à Monaco en phase de démarrage une avance de trésorerie sous la forme de fonds mis à disposition en compte courant d'un montant de 50 000 000 francs assorti d'un taux d'intérêt nul pour 1998 et d'un taux de 2,848 % pour 1999 ; que la Caisse Fédérale, qui gère la trésorerie des caisses locales du réseau au nombre desquelles figure la CAMEFI, a versé, à raison du solde positif du compte courant ainsi crédité, une rémunération nette de 4,50 % en 1998 et de 0,952 % en 1999 ; que l'administration a estimé que la CAMEFI avait procédé à un transfert de bénéfices à l'étranger au sens de l'article 57 du code général des impôts en faisant bénéficier l'agence de Monaco de ces intérêts du compte courant ;<br/>
<br/>
              5. Considérant que, si la requérante soutenait que les avances et les produits financiers qui s'y rapportaient avaient été consentis par la Caisse fédérale directement à l'agence monégasque, la cour s'est livrée à une appréciation souveraine des pièces du dossier, dépourvue de dénaturation, et n'a pas méconnu le régime de dévolution de la preuve en jugeant, par son arrêt, qui n'est pas entaché d'omission de réponse à moyen, que l'administration établissait que la CAMEFI avait appréhendé les avances de la Caisse Fédérale et les produits financiers correspondants et avait procédé à son seul bénéfice au transfert de ces dernières sommes à son agence implantée à Monaco ; que dès lors que la requérante s'est bornée devant la cour à nier avoir joué un quelconque rôle et être à l'origine de ce transfert de fonds à l'étranger, qui auraient été virés directement de la caisse fédérale vers l'agence monégasque, la cour n'avait pas à rechercher si l'administration démontrait le caractère prétendument anormal du prêt consenti à l'agence monégasque et la nature de l'avantage dont cette agence aurait bénéficié ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la CAMEFI n'est pas fondée à demander l'annulation de l'arrêt attaqué ; qu'en conséquence les conclusions du pourvoi tendant à l'application de l'article L. 761-1 du code de justice administrative doivent être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de la CAMEFI est rejeté.<br/>
Article 2 : la présente décision sera notifiée à la Caisse Méditerranéenne de Financement (CAMEFI) et au ministre de l'économie et des finances.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
