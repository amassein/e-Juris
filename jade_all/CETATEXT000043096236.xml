<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043096236</ID>
<ANCIEN_ID>JG_L_2021_02_000000434335</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/09/62/CETATEXT000043096236.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 03/02/2021, 434335, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434335</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Bénédicte Fauvarque-Cosson</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:434335.20210203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société à responsabilité limitée AC Promotions a demandé au tribunal administratif de Bastia d'annuler pour excès de pouvoir l'arrêté du 11 décembre 2015 par lequel le maire de Cuttoli-Corticchiato a refusé de lui délivrer le permis de construire 18 logements sur les parcelles cadastrées section A n° 1378 et n° 1384 situées lieu-dit Giaccharello, ainsi que la décision du 12 février 2016 rejetant son recours gracieux. Par un jugement n° 1600478 du 18 janvier 2018, le tribunal administratif de Bastia a fait droit à cette demande.<br/>
<br/>
              Par un arrêt n° 18MA01249 du 8 juillet 2019, la cour administrative d'appel de Marseille a rejeté l'appel formé par la commune de Cuttoli-Corticchiato contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 6 septembre et 6 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, la commune de Cuttoli-Corticchiato demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la société AC Promotions la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... D..., conseillère d'Etat,  <br/>
<br/>
              - les conclusions de Mme A... C..., rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de la commune de Cuttoli-Corticchiato et à la SCP Rocheteau, Uzan-Sarano, avocat de la SARL AC Promotions ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société AC Promotions a déposé le 11 juillet 2013 une demande de permis de construire dix-huit logements pour une surface de plancher créée de 1 380 mètres carrés sur les parcelles, cadastrées section A n° 1378 et n° 1384, situées sur la commune de Cuttoli-Corticchiato au lieu-dit Giaccharello. Par un arrêté du 4 octobre 2013, le maire de cette commune a refusé de délivrer le permis sollicité. Par un jugement du 1er octobre 2015, le tribunal administratif de Bastia a annulé ce refus et enjoint à la commune de Cuttoli-Corticchiato de se prononcer à nouveau sur la demande de la société dans un délai de deux mois. La parcelle d'assiette du projet n'étant plus régie par aucun plan local d'urbanisme ou document en tenant lieu à la suite de l'annulation, par le jugement du 5 juillet 2007 du tribunal administratif de Bastia, confirmée par l'arrêt du 29 janvier 2010 de la cour administrative d'appel de Marseille, de la délibération du 11 juillet 2006 du conseil municipal de la commune de Cuttoli-Corticchiato, en tant qu'elle classait la parcelle cadastrée section A n° 1378 en zone N du plan local d'urbanisme, le préfet de la Corse-du-Sud, saisi de la demande de la société AC Promotions, a émis un avis favorable au projet le 3 décembre 2015. Par un arrêté du 11 décembre 2015, le maire de Cuttoli-Corticchiato a de nouveau refusé de délivrer le permis de construire sollicité et, par une décision du 12 février 2016, a rejeté le recours gracieux formé par la société AC Promotions contre cet arrêté. Par un jugement du 18 janvier 2018, le tribunal administratif de Bastia a annulé l'arrêté du 11 décembre 2015 et la décision du 12 février 2016. Par un arrêt du 8 juillet 2019 contre lequel la commune de Cuttoli-Corticchiato se pourvoit en cassation, la cour administrative d'appel de Marseille a rejeté l'appel formé par la commune contre ce jugement. <br/>
<br/>
              2. Aux termes de l'article L. 422-6 du code de l'urbanisme : " En cas d'annulation par voie juridictionnelle ou d'abrogation d'une carte communale, d'un plan local d'urbanisme ou d'un document d'urbanisme en tenant lieu, ou de constatation de leur illégalité par la juridiction administrative ou l'autorité compétente et lorsque cette décision n'a pas pour effet de remettre en vigueur un document d'urbanisme antérieur, le maire ou le président de l'établissement public de coopération intercommunale recueille l'avis conforme du préfet sur les demandes de permis ou les déclarations préalables postérieures à cette annulation, à cette abrogation ou à cette constatation ". Si, en application de ces dispositions, le maire a compétence liée pour refuser un permis de construire en cas d'avis défavorable du préfet, il n'est en revanche pas tenu de suivre un avis favorable de ce même préfet et peut, lorsqu'il estime disposer d'un motif légal de le faire au titre d'autres dispositions que celles ayant donné lieu à cet avis, refuser d'accorder le permis de construire sollicité.<br/>
<br/>
              3. Par suite, la cour a commis une erreur de droit en jugeant que le maire de Cuttoli-Corticchiato était, du fait de l'avis favorable émis par le préfet le 3 décembre 2015, en situation de compétence liée pour délivrer le permis de construire sollicité.<br/>
<br/>
              4. Par suite, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, la commune est fondée à demander pour ce motif l'annulation de l'arrêt qu'elle attaque. <br/>
<br/>
              5. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la commune de Cuttoli-Corticchiato au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 8 juillet 2019 de la cour administrative d'appel de Marseille est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : Les conclusions présentées par la commune de Cuttoli-Corticchiato au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la commune de Cuttoli-Corticchiato.<br/>
Copie en sera adressé à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales et à la société à responsabilité limitée AC Promotions.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
