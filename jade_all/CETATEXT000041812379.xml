<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041812379</ID>
<ANCIEN_ID>JG_L_2020_04_000000439940</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/81/23/CETATEXT000041812379.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 08/04/2020, 439940, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-04-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439940</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:439940.20200408</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
              Par une requête, enregistrée le 3 avril 2020 au secrétariat du contentieux du Conseil d'Etat, l'association l'Amicale des jardiniers des Marchais demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
              1°) d'ordonner la suspension de l'exécution de tout acte administratif pris par le préfet de la Vendée, contrevenant aux dispositions du décret n° 2020-293 du 23 mars 2020 ;<br/>
<br/>
              2°) d'enjoindre au préfet de la Vendée, d'une part, de procéder à la diffusion d'un nouveau communiqué de presse rectificatif, autorisant les jardiniers (non professionnels) à se prévaloir des dispositions visées au 5° de l'article 3 du décret du 23 mars 2020 sous astreinte et, d'autre part, de communiquer un mail rectificatif à l'association ;<br/>
<br/>
              3°) d'enjoindre au préfet de la Vendée de clarifier sa position sur son interprétation des contraintes législatives et règlementaires, s'agissant notamment de la liberté d'aller et de venir des jardiniers (non professionnels) en définissant, une fois pour toutes et sur toute l'étendue du territoire national, les conditions de leurs déplacements, eu égard à leur rôle traditionnel, en tant que producteurs de denrées alimentaires, nécessaires à l'autosuffisance alimentaire de notre pays.<br/>
<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - elle justifie d'un intérêt à agir ;<br/>
              - la condition d'urgence est remplie eu égard, en premier lieu, à la nécessité pour les jardiniers concernés de pouvoir accéder rapidement à leur jardin pour prélever leurs récoltes et préparer leur terrain pour y semer ou y planter les graines et les plants nécessaires aux récoltes futures, en deuxième lieu, au risque de subir les pertes des légumes plantés l'an dernier et, en dernier lieu, à l'obligation pour eux de s'approvisionner en magasin dans des conditions sanitaires plus dégradées que celles auxquelles ils sont exposés en plein air ;<br/>
              - il est porté une atteinte grave et manifestement illégale au principe d'égalité, à l'interdiction des discriminations et à la liberté d'aller et venir ;<br/>
              - la distinction faite par le préfet entre les jardiniers non professionnels exploitant un jardin attenant à leur domicile et les déplacements liés à l'exercice physique individuel de jardiniers respectant les règles de distanciation sociale, dans le cadre de l'exploitation d'un jardin situé à moins d'un kilomètre de leur domicile, constitue une discrimination et une violation du principe d'égalité ;<br/>
              - la décision du préfet de la Vendée méconnaît les dispositions du décret du 23 mars 2020, en interdisant arbitrairement les déplacements des personnes disposant d'un jardin à moins d'un kilomètre de leur domicile ;<br/>
              - l'activité de jardinage est une activité physique en pleine air qui peut être exercée dans le respect des règles de distanciation sociale édictées par le décret du 23 mars 2020.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Le juge des référés du Conseil d'Etat ne peut être régulièrement saisi, en premier et dernier ressort, d'une requête tendant à la mise en oeuvre de l'une des procédures régies par le livre V du code de justice administrative que pour autant que le litige principal auquel se rattache ou est susceptible de se rattacher la mesure d'urgence qu'il lui est demandé de prendre, ressortit lui-même à la compétence directe du Conseil d'Etat. L'article R. 522-8-1 du même code prévoit que, par dérogation aux dispositions du titre V du livre III relatif au règlement des questions de compétence au sein de la juridiction administrative, le juge des référés qui entend décliner la compétence de la juridiction rejette les conclusions dont il est saisi par voie d'ordonnance.<br/>
<br/>
              3. L'article R. 311-1 du code de justice administrative énumère de façon exhaustive les litiges dont le Conseil d'Etat est compétent pour connaître en premier et dernier ressort. Or, la décision que l'association requérante entend contester n'est manifestement pas au nombre de celles dont il appartient au Conseil d'Etat de connaître en premier et dernier ressort. <br/>
<br/>
              4. Il résulte de ce qui précède que la requête de l'association l'Amicale des jardiniers des Marchais doit être rejetée selon la procédure prévue à l'article L. 522-3 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de l'association l'Amicale des jardiniers des Marchais est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à l'association l'Amicale des jardiniers des Marchais.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
