<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044222837</ID>
<ANCIEN_ID>JG_L_2021_10_000000444885</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/22/28/CETATEXT000044222837.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 18/10/2021, 444885, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>444885</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Sébastien Jeannard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:444885.20211018</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement n° 1927772 du 25 septembre 2020, enregistré le même jour au secrétariat du contentieux du Conseil d'Etat, le tribunal administratif de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête et le mémoire en réplique, enregistrés le 27 décembre 2019 et le 25 mai 2020 au greffe de ce tribunal, présentée par la Commission de recherche et d'informations indépendantes pour la création de registres de santé (CRIIREG), l'association Alerte des médecins limousins sur les pesticides et le collectif Environnement santé 74. <br/>
<br/>
              Par cette requête, ce mémoire en réplique et un nouveau mémoire, enregistré le 23 juin 2021, la CRIIREG et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 29 décembre 2019 par laquelle le ministre des solidarités et de la santé a rejeté leur demande tendant à ce qu'il soit créé un registre national, décliné à l'échelle régionale et départementale, des malformations congénitales et des cancers ;<br/>
<br/>
              2°) d'enjoindre au ministre des solidarités et de la santé de prendre toutes mesures utiles pour que soit lancée la procédure permettant l'instauration de registres des malformations congénitales et des cancers ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ; <br/>
              - la Charte des droits fondamentaux de l'Union européenne ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sébastien Jeannard, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier que, par une lettre du 4 octobre 2019, la Commission de recherche et d'informations indépendantes pour la création de registres de santé (CRIIREG), l'association Alerte des médecins limousins sur les pesticides et le collectif Environnement santé 74 ont demandé au ministre des solidarités et de la santé de mettre en place des registres nationaux, déclinés à l'échelle régionale et départementale, pour les malformations congénitales et les cancers. Par un courrier du 29 octobre 2019, le ministre a rejeté leur demande. Les associations requérantes demandent l'annulation pour excès de pouvoir de ce refus et à ce qu'il soit enjoint au ministre des solidarités et de la santé de prendre toutes mesures utiles pour que soit lancée la procédure permettant d'instaurer des registres nationaux des malformations congénitales et des cancers.<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              2. Le refus de prendre un acte réglementaire présente un caractère réglementaire et, par suite, ne relève pas des dispositions de l'article L. 211-2 du code des relations entre le public et l'administration, qui sont relatives à la motivation des décisions individuelles défavorables. Dès lors, le moyen tiré du défaut de motivation qui entacherait la décision du ministre du 29 décembre 2019 ne peut qu'être écarté.<br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              3. L'effet utile de l'annulation pour excès de pouvoir du refus opposé à la demande des requérants de prendre toute mesure utile permettant la création de registres nationaux des malformations congénitales et des cancers réside dans l'obligation, que le juge peut prescrire d'office en vertu des dispositions de l'article L. 911-1 du code de justice administrative, pour l'autorité compétente, de prendre les mesures jugées nécessaires. Il s'ensuit que lorsqu'il est saisi de conclusions aux fins d'annulation d'un tel refus, le juge de l'excès de pouvoir est conduit à apprécier sa légalité au regard des règles applicables et des circonstances prévalant à la date de sa décision.<br/>
<br/>
              4. En premier lieu, la décision attaquée, refusant de mettre en place les registres nationaux des malformations congénitales et des cancers réclamés par les requérants, ne porte et n'est susceptible de porter par elle-même aucune atteinte à l'environnement. Par suite, et en tout état de cause, ni les dispositions de l'article 1er de la Charte de l'environnement, qui proclament que : " Chacun a le droit de vivre dans un environnement équilibré et respectueux de la santé ", ni celles de l'article 2 de cette charte, selon lesquelles " Toute personne a le devoir de prendre part à la préservation et à l'amélioration de l'environnement ", ni celles de son article 3, aux termes desquelles : " Toute personne doit, dans les conditions définies par la loi, prévenir les atteintes qu'elle est susceptible de porter à l'environnement ou, à défaut, en limiter les conséquences ", pas davantage que celles de son article 5, prévoyant que : " lorsque la réalisation d'un dommage, bien qu'incertaine en l'état des connaissances scientifiques, pourrait affecter de manière grave et irréversible l'environnement, les autorités publiques veillent, par application du principe de précaution et dans leurs domaines d'attributions, à la mise en œuvre de procédures d'évaluation des risques et à l'adoption de mesures provisoires et proportionnées afin de parer à la réalisation du dommage " ne peuvent être utilement invoquées à son encontre. <br/>
<br/>
              5. En deuxième lieu, l'article 35 de la Charte des droits fondamentaux de l'Union européenne fixant l'objectif selon lequel un " (...) niveau élevé de protection de la santé humaine est assuré dans la définition et la mise en œuvre de toutes les politiques et actions de l'Union " n'impose pas la création de tels registres par les autorités nationales.<br/>
<br/>
              6. En troisième lieu, il ne ressort pas des pièces du dossier que le ministre des solidarités et de la santé aurait commis une erreur manifeste d'appréciation en s'abstenant d'instaurer les registres nationaux sollicités ni en instaurant, à supposer que tel soit le cas, un septième registre territorial au lieu d'un registre national des malformations congénitales. <br/>
<br/>
              7. Il résulte de tout ce qui précède que les requérants ne sont pas fondés à demander l'annulation de la décision du ministre des solidarités et de la santé du 29 octobre 2019. Leurs conclusions à fin d'injonction et présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être également rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la Commission de recherche et d'informations indépendantes pour la création de registres de santé, l'association Alerte des médecins limousins sur les pesticides et le collectif Environnement santé 74 est rejetée. <br/>
Article 2 : La présente décision sera notifiée à la Commission de recherche et d'informations indépendantes pour la création de registres de santé, première dénommée, pour l'ensemble des requérants. <br/>
Copie en sera adressée au ministre des solidarités et de la santé.<br/>
              Délibéré à l'issue de la séance du 23 septembre 2021 où siégeaient : Mme Gaëlle Dumortier, présidente de chambre, présidant ; M. Damien Botteghi, conseiller d'Etat et M. Sébastien Jeannard, maître des requêtes en service extraordinaire-rapporteur. <br/>
<br/>
              Rendu le 18 octobre 2021.<br/>
                 La présidente : <br/>
                 Signé : Mme Gaëlle Dumortier<br/>
 		Le rapporteur : <br/>
      Signé : M. Sébastien Jeannard<br/>
                 La secrétaire :<br/>
                 Signé : Mme A... B...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
