<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044500332</ID>
<ANCIEN_ID>JG_L_2021_12_000000447040</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/50/03/CETATEXT000044500332.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 14/12/2021, 447040, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>447040</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:447040.20211214</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C... A... a demandé à la commission du contentieux du stationnement payant d'être déchargé du montant du forfait de post-stationnement mis à sa charge le 18 janvier 2018 par la Ville de Paris, ainsi que de la majoration dont il est assorti. Par une ordonnance n° 19034858 du 29 mai 2020, le magistrat désigné de la commission du contentieux du stationnement payant a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 30 novembre 2020 et 1er mars 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la Ville de Paris la somme de 6 000 euros à verser à la SCP Piwnica, Molinié, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Piwnica, Molinié, avocat de M. A....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'en raison de l'absence de paiement d'une redevance de stationnement constatée le 18 janvier 2018, M. A... a reçu notification d'un avis de paiement d'un forfait de post-stationnement d'un montant de 35 euros pour la Ville de Paris, assorti d'une majoration revenant à l'Etat. M. A... se pourvoit en cassation contre l'ordonnance du 29 mai 2020 par laquelle le magistrat désigné par le président de la commission du contentieux du stationnement payant a rejeté sa requête tendant à la décharge des sommes mises à sa charge.<br/>
<br/>
              2. Il résulte des dispositions de l'article L. 2333-87 du code général des collectivités territoriales qu'il appartient en principe au redevable d'un forfait de post-stationnement qui entend contester le bien-fondé de la somme mise à sa charge de saisir l'autorité administrative d'un recours administratif préalable dirigé contre l'avis de paiement et, en cas de rejet de ce recours, d'introduire une requête contre cette décision de rejet devant la commission du contentieux du stationnement payant. Toutefois, en cas d'absence de paiement de sa part dans les trois mois et d'émission, en conséquence, d'un titre exécutoire portant sur le montant du forfait de post-stationnement augmenté de la majoration due à l'Etat, il est loisible au même redevable de contester ce titre exécutoire devant la commission du contentieux du stationnement payant, qu'il ait ou non engagé un recours administratif contre l'avis de paiement et contesté au contentieux le rejet de son recours. A ce titre, s'il résulte des dispositions de l'article R. 2333-120-35 du même code que le redevable qui saisit la commission du contentieux du stationnement payant d'une requête contre un titre exécutoire n'est pas recevable à exciper de l'illégalité de l'avis de paiement du forfait de post-stationnement auquel ce titre exécutoire s'est substitué, ces mêmes dispositions ne font pas obstacle à ce que l'intéressé conteste, dans le cadre d'un litige dirigé contre le titre exécutoire, l'obligation de payer la somme réclamée par l'administration.<br/>
<br/>
              3. Il ressort des termes de l'ordonnance attaquée que, pour rejeter la requête de M. A..., le magistrat désigné par le président de la commission du contentieux du stationnement payant a jugé que le moyen tiré de l'absence d'obligation de payer la somme réclamée par l'administration était inopérant, au motif qu'il mettait en cause la légalité de l'avis de paiement auquel un titre exécutoire s'était substitué. Il résulte de ce qui a été dit ci-dessus que l'ordonnance attaquée est, sur ce point, entachée d'une erreur de droit. M. A... est, par suite, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, fondé à en demander l'annulation.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce et sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de la Ville de Paris la somme de 2 000 euros à verser à la SCP Piwnica, Molinié, avocat de M. A..., au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 relative à l'aide juridique.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance de la commission du contentieux du stationnement payant du 27 juillet 2020 est annulée.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la commission du contentieux du stationnement payant.<br/>
<br/>
Article 3 : La Ville de Paris versera la somme de 2 000 euros à la SCP Piwnica, Molinié, avocat de Mme D..., au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. C... A..., à la Ville de Paris et au ministre de l'économie, des finances et de la relance.<br/>
Copie en sera adressée à l'Agence nationale de traitement automatisé des infractions.<br/>
<br/>
              Délibéré à l'issue de la séance du 18 novembre 2021 où siégeaient : M. Denis Piveteau, président de chambre, présidant ; M. Olivier Yeznikian, conseiller d'Etat et M. Florian Roussel, maître des requêtes-rapporteur. <br/>
<br/>
              Rendu le 14 décembre 2021.<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Denis Piveteau<br/>
 		Le rapporteur : <br/>
      Signé : M. Florian Roussel<br/>
                 La secrétaire :<br/>
                 Signé : Mme E... B...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
