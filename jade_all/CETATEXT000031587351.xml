<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031587351</ID>
<ANCIEN_ID>JG_L_2015_12_000000370175</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/58/73/CETATEXT000031587351.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 07/12/2015, 370175, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370175</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Vincent Montrieux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:370175.20151207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Nantes, par deux requêtes distinctes, d'une part, d'annuler pour excès de pouvoir la décision du 21 juin 2010 par laquelle le maire de Sainte-Gemmes-sur-Loire (Maine-et-Loire) l'a placée en position de disponibilité d'office, d'enjoindre à la commune de la rétablir dans ses droits statutaires dans un délai d'un mois suivant la notification du jugement à intervenir, d'autre part, d'annuler pour excès de pouvoir la décision du maire de Sainte-Gemmes-sur-Loire refusant de reconnaître l'imputabilité au service de la maladie dont elle est atteinte, d'enjoindre à la commune de Sainte-Gemmes-sur-Loire de reconnaître l'imputabilité au service de sa maladie à compter du 19 janvier 2009 ou, à défaut, de statuer sur cette imputabilité, enfin, de condamner la commune à lui verser une indemnité de 10 087,46 euros, assortie des intérêts aux taux légal et de la capitalisation des intérêts, en réparation du préjudice matériel et moral que lui ont causé ces décisions. <br/>
<br/>
              Par un jugement n°s 1005037, 1008659 du 13 mai 2013, le tribunal administratif de Nantes a :<br/>
              - annulé l'arrêté du 21 juin 2010 du maire de Sainte-Gemmes-sur-Loire en ce qu'il place Mme B...en disponibilité d'office et en ce qu'il refuse de reconnaître l'imputabilité au service de la pathologie de l'intéressée ;<br/>
              - condamné la commune de Sainte-Gemmes-sur Loire à verser à Mme B... une somme de 1 000 euros en réparation de son préjudice moral ;<br/>
              - condamné la commune à réparer le préjudice financier subi par Mme B... du fait de l'illégalité de son placement en position de disponibilité d'office pour la période du 21 juin 2010 au 7 janvier 2011, et renvoyé Mme B...devant la commune pour le calcul et la liquidation de la somme due en réparation de ce préjudice ;<br/>
              - enjoint au maire de Sainte-Gemmes-sur-Loire de réexaminer la situation de MmeB..., dans un délai de trois mois à compter de la notification du jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 15 juillet et 15 octobre 2013 au secrétariat du contentieux du Conseil d'Etat, la commune de Sainte-Gemmes-sur-Loire demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les demandes de Mme B...;<br/>
<br/>
              3°) de mettre à la charge de Mme B...la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 86-68 du 13 janvier 1986 ;<br/>
              - le décret n° 87-602 du 30 juillet 1987 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Montrieux, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de la commune de Sainte-Gemmes-sur-Loire, et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de Mme B...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que MmeB..., attaché territorial de la commune de Sainte-Gemmes-sur-Loire (Maine-et-Loire), a été placée en congé de longue maladie du 19 janvier 2009 au 18 janvier 2010 puis en congé de longue durée du 19 janvier au 18 avril 2010 ; que le 4 mars 2010, la commission de réforme territoriale a émis un avis favorable à l'imputabilité au service de la maladie dont elle était atteinte, puis, le 29 avril 2010, un avis favorable à sa reprise des fonctions à temps partiel thérapeutique pendant trois mois ; que, par arrêtés en date du 17 mai 2010, Mme B...a été placée en congé de maladie ordinaire du 19 avril au 15 mai 2010 ; qu'elle a repris ses fonctions à temps plein à compter du 8 juin 2010 ; que, par arrêté du 21 juin 2010, le maire de la commune de Sainte-Gemmes-sur-Loire a placé Mme B...en disponibilité d'office ; <br/>
<br/>
              Sur le placement en position de disponibilité d'office :<br/>
<br/>
              2. Considérant qu'aux termes de l'article 72 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " La disponibilité est la position du fonctionnaire qui, placé hors de son administration ou service d'origine, cesse de bénéficier, dans cette position, de ses droits à l'avancement et à la retraite. / La disponibilité est prononcée, soit à la demande de l'intéressé, soit d'office à l'expiration des congés prévus aux 2°, 3° et 4° de l'article 57 (...) " ; qu'aux termes de l'article 57 de la même loi : " Le fonctionnaire en activité a droit : / (...) 2° A des congés de maladie dont la durée totale peut atteindre un an pendant une période de douze mois consécutifs en cas de maladie dûment constatée mettant l'intéressé dans l'impossibilité d'exercer ses fonctions. (...) ; / 3° A des congés de longue maladie d'une durée maximale de trois ans dans les cas où il est constaté que la maladie met l'intéressé dans l'impossibilité d'exercer ses fonctions, rend nécessaires un traitement et des soins prolongés et présente un caractère invalidant et de gravité confirmée (...) ; / 4° A un congé de longue durée, en cas de tuberculose, maladie mentale, affection cancéreuse, poliomyélite ou déficit immunitaire grave et acquis (...). / Si la maladie ouvrant droit à congé de longue durée a été contractée dans l'exercice des fonctions, les périodes fixées ci-dessus sont respectivement portées à cinq ans et trois ans (...) " ; que, selon le premier alinéa de l'article 19 du décret du 13 janvier 1986 relatif aux positions de détachement, hors cadres, de disponibilité, du congé parental des fonctionnaires territoriaux et à l'intégration : " La mise en disponibilité peut être prononcée d'office à l'expiration des droits statutaires à congés de maladie prévus au premier alinéa du 2°, au premier alinéa du 3° et au 4° de l'article 57 de la loi du 26 janvier 1984 et s'il ne peut, dans l'immédiat, être procédé au reclassement du fonctionnaire (...) " ; que le deuxième alinéa de l'article 17 du décret du 30 juillet 1987 pris pour l'application de la loi n° 84-53 portant dispositions statutaires relatives à la fonction publique territoriale et relatif à l'organisation des comités médicaux, aux conditions d'aptitude physique et au régime des congés de maladie des fonctionnaires territoriaux prévoit que : " Lorsque le fonctionnaire a obtenu pendant une période totale de douze mois consécutifs des congés de maladie d'une durée totale de douze mois, il ne peut, à l'expiration de sa dernière période de congé, reprendre son service sans l'avis favorable du comité médical. En cas d'avis défavorable, il est mis soit en disponibilité, soit reclassé dans  un autre emploi, soit s'il est reconnu définitivement inapte à l'exercice de tout emploi, admis à la retraite après avis de la commission  de réforme (...) " ; que l'article 37 du même décret dispose que : " Le fonctionnaire ne pouvant, à l'expiration de la dernière période de congé de longue maladie ou de longue durée, reprendre son service est soit reclassé dans un autre emploi, (...), soit mis en disponibilité, soit admis à la retraite après avis de la commission de réforme (...) " ; qu'enfin, l'article 38 de ce décret précise que : " La mise en disponibilité visée aux articles 17 et 37 du présent décret est prononcée après avis du comité médical ou de la commission de réforme prévue par le décret n° 65-773 du 9 septembre 1965 susvisé, sur l'inaptitude du fonctionnaire à reprendre ses fonctions. / L'avis est donné par la commission de réforme lorsque le congé antérieur a été accordé en vertu de l'article 57 (4°, 2e alinéa) de la loi n° 84-53 du 26 janvier 1984 susvisée. (...) " ;  <br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions qu'en relevant que Mme B... n'avait pas épuisé ses droits à congés de maladie, de longue maladie ou de longue durée mentionnés aux 2°, 3° et 4° de l'article 57 de la loi du 26 janvier 1984, déterminant les seuls cas dans lesquels l'administration aurait pu légalement la mettre en disponibilité d'office pour des raisons liées à son état de santé, le tribunal a déduit sans erreur de droit ni insuffisance de motivation que le maire de la commune de Sainte-Gemmes-sur-Loire n'avait pu légalement prendre une telle mesure à son endroit ; <br/>
<br/>
              4. Considérant qu'il résulte des termes mêmes du jugement attaqué que le motif par lequel le tribunal a relevé que le maire de la commune de Sainte-Gemmes-sur-Loire n'avait pas recueilli l'avis du comité médical départemental avant de prendre la décision contestée de mise en disponibilité d'office présente un caractère surabondant ; que, dès lors, les moyens du pourvoi dirigés contre ce motif sont inopérants ;<br/>
<br/>
              Sur l'imputabilité au service de la pathologie de Mme B...:<br/>
<br/>
              5. Considérant, en premier lieu, qu'en analysant l'arrêté du 21 juin 2010 comme refusant implicitement de reconnaître l'imputabilité au service de la maladie dont était atteinte MmeB..., le tribunal administratif ne s'est pas mépris sur la portée de cet arrêté, eu égard notamment aux motifs sur lesquels il reposait ; qu'il n'a commis aucune erreur de droit en retenant que la demande introduite par Mme B...contre ce refus n'était pas tardive dès lors que la date de notification de l'arrêté litigieux n'était pas établie par les pièces du dossier ;<br/>
<br/>
              6. Considérant, en second lieu, que pour juger que le maire de la commune de Sainte-Gemmes-sur-Loire avait entaché son arrêté du 21 juin 2010, en tant qu'il refusait implicitement de reconnaître l'imputabilité au service de la maladie de MmeB..., d'une erreur d'appréciation, le tribunal administratif a relevé que le maire s'était uniquement fondé sur l'insuffisante motivation des avis de la commission de réforme des 4 mars et 29 avril 2010 alors, d'une part, que ces avis se référaient à une expertise médicale effectuée le 24 novembre 2009 ainsi qu'à l'entier dossier médical de Mme B...et, d'autre part, la commune n'avait engagé aucune procédure permettant de disposer d'une contre-expertise ; qu'en se fondant sur ce motif, le tribunal, qui  a suffisamment motivé son jugement, n'a pas commis d'erreur de droit ;  <br/>
<br/>
              Sur les conclusions indemnitaires :<br/>
<br/>
              7. Considérant qu'aux termes du premier alinéa de l'article R. 711-3 du code de justice administrative : " Si le jugement de l'affaire doit intervenir après le prononcé de conclusions du rapporteur public, les parties ou leurs mandataires sont mis en mesure de connaître, avant la tenue de l'audience, le sens de ces conclusions sur l'affaire qui les concerne " ;<br/>
<br/>
              8. Considérant que la communication aux parties du sens des conclusions, prévue par les dispositions de cet article, a pour objet de mettre les parties en mesure d'apprécier l'opportunité d'assister à l'audience publique, de préparer, le cas échéant, les observations orales qu'elles peuvent y présenter, après les conclusions du rapporteur public, à l'appui de leur argumentation écrite et d'envisager, si elles l'estiment utile, la production, après la séance publique, d'une note en délibéré ; qu'en conséquence, les parties ou leurs mandataires doivent être mis en mesure de connaître, dans un délai raisonnable avant l'audience, l'ensemble des éléments du dispositif de la décision que le rapporteur public compte proposer à la formation de jugement d'adopter, à l'exception de la réponse aux conclusions qui revêtent un caractère accessoire, notamment celles qui sont relatives à l'application de l'article L. 761-1 du code de justice administrative ; que cette exigence s'impose à peine d'irrégularité de la décision rendue sur les conclusions du rapporteur public ;<br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que la commune de Sainte-Gemmes-sur-Loire est fondée à soutenir que le jugement attaqué a été rendu au terme d'une procédure partiellement irrégulière, faute pour le rapporteur public d'avoir mis les parties en mesure de connaître le sens de ses conclusions s'agissant des demandes à caractère indemnitaire ; que le jugement attaqué doit donc être annulé en tant qu'il a statué sur les demandes de Mme B...tendant à l'indemnisation de son préjudice matériel et moral ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              10. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la commune de Sainte-Gemmes-sur-Loire au titre des dispositions de l'article L. 761-1 du code de justice administrative ainsi qu'aux conclusions présentées par Mme B...au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Nantes du 13 mai 2013 est annulé en tant qu'il a statué sur les conclusions de Mme B...tendant à l'indemnisation de son préjudice matériel et moral.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, au tribunal administratif de Nantes.<br/>
Article 3 : Le surplus du pourvoi de la commune de Saint-Gemmes-sur-Loire ainsi que les conclusions de Mme B...tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
Article 4 : La présente décision sera notifiée à la commune de Sainte-Gemmes-sur-Loire et à Mme A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
