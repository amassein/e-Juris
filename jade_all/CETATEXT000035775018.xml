<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035775018</ID>
<ANCIEN_ID>JG_L_2017_10_000000412470</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/77/50/CETATEXT000035775018.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 11/10/2017, 412470, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-10-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412470</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Karin Ciavaldini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:412470.20171011</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 17 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, M. A... B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir les commentaires administratifs publiés le 18 juin 2015 au Bulletin officiel des finances publiques sous la référence BOI-CF-INF-40-10-10-10 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Karin Ciavaldini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. M. B... demande l'annulation pour excès de pouvoir des commentaires administratifs, publiés le 18 juin 2015 au Bulletin officiel des finances publiques sous la référence BOI-CF-INF-40-10-10-10, par lesquels l'administration a fait connaître son interprétation de l'article 1741 du code général des impôts en ce qui concerne les éléments constitutifs du délit général de fraude fiscale.<br/>
<br/>
              2. Le premier alinéa de l'article 1741 du code général des impôts prévoit que le fait de se soustraire frauduleusement à l'établissement ou au paiement total ou partiel des impôts, notamment par l'omission volontaire de faire sa déclaration dans les délais prescrits, est passible, indépendamment des sanctions fiscales applicables, d'une amende et d'une peine d'emprisonnement. Les sanctions fiscales applicables au même manquement sont prévues au 1 de l'article 1728 du même code.<br/>
<br/>
              3. Dans la question prioritaire de constitutionnalité qu'il présente à l'appui de sa requête, M. B...soulève le moyen tiré de ce que les dispositions de l'article 1741 du code général des impôts, en tant qu'elles mentionnent l'omission volontaire de faire une déclaration dans les délais prescrits, et celles du 1 de l'article 1728 du même code méconnaissent les principes de nécessité et de proportionnalité des délits et des peines garantis par l'article 8 de la Déclaration des droits de l'homme et du citoyen de 1789, dès lors qu'elles permettent, à l'encontre de la même personne et en raison des mêmes faits, le cumul de procédures et de sanctions, sauf à ce que le Conseil constitutionnel juge que les dispositions contestées de l'article 1741 du code général des impôts doivent être lues comme ne pouvant être appliquées qu'aux cas les plus graves d'omission volontaire de déclaration dans les délais prescrits.<br/>
<br/>
              4. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              5. Les commentaires administratifs qui font l'objet de la requête de M. B... portent sur le seul article 1741 du code général des impôts et ne traitent pas de la question de l'application combinée des dispositions, mentionnées au point 2, des articles 1741 et 1728 de ce code. Dès lors, les dispositions contestées par la question prioritaire de constitutionnalité soulevée par M. B...ne sont pas applicables au litige. Il n'y a donc pas lieu de renvoyer la question au Conseil constitutionnel. <br/>
<br/>
              6. La requête de M.B..., qui ne comporte pas d'autre moyen, doit, dès lors, être rejetée, y compris ses conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>              D E C I D E :<br/>
                             --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M.B....<br/>
<br/>
Article 2 : La requête de M. B... est rejetée.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. A...B...et au ministre de l'action et des comptes publics.<br/>
            Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
