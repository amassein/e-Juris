<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026504628</ID>
<ANCIEN_ID>JG_L_2012_10_000000348253</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/50/46/CETATEXT000026504628.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème sous-section jugeant seule, 17/10/2012, 348253, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-10-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>348253</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Rémy Schwartz</PRESIDENT>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Fabrice Aubert</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:348253.20121017</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 8 avril et 1er juillet 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Amar A, demeurant ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 10PA00093 du 16 octobre 2010 par laquelle le président de la 6ème chambre de la cour administrative d'appel de Paris a rejeté sa requête tendant à l'annulation de l'ordonnance n° 0908871 du 29 octobre 2009 par laquelle le vice-président du tribunal administratif de Paris a rejeté sa demande tendant à l'annulation de la décision du 29 décembre 2008 par laquelle le préfet de la région Ile-de-France, préfet de Paris, a refusé de lui délivrer la carte de combattant ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros au bénéfice de la SCP Rocheteau, Uzan-Sarano au titre de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabrice Aubert, Auditeur,  <br/>
<br/>
              - les observations de la SCP Rocheteau, Uzan-Sarano, avocat de M. A,<br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Rocheteau, Uzan-Sarano, avocat de M. A ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article R. 222-1 du code de justice administrative : " Les présidents (...) de cour administrative d'appel (...) peuvent, par ordonnance : (...) 4° Rejeter les requêtes manifestement irrecevables lorsque la juridiction n'est pas tenue d'inviter leur auteur à les régulariser (...) " ; qu'aux termes de l'article R. 612-1 du même code : " Lorsque des conclusions sont entachées d'une irrecevabilité susceptible d'être couverte après l'expiration du délai de recours, la juridiction ne peut les rejeter en relevant d'office cette irrecevabilité qu'après avoir invité leur auteur à les régulariser./ Toutefois, la juridiction d'appel (...) peut rejeter de telles conclusions sans demande de régularisation préalable pour les cas d'irrecevabilité tirés de la méconnaissance d'une obligation mentionnée dans la notification de la décision attaquée conformément à l'article R. 751-5 (...) " ; qu'aux termes de l'article R. 751-5 : " (...) Lorsque la décision rendue relève de la compétence de la cour administrative d'appel et, sauf lorsqu'une disposition particulière a prévu une dispense de ministère d'avocat en appel, la notification mentionne que l'appel ne peut être présenté que par l'un des mandataires mentionnés à l'article R. 431-2 (...) " ;<br/>
<br/>
              2. Considérant que, par une ordonnance du 16 octobre 2010, le président de la 6ème chambre de la cour administrative d'appel de Paris a rejeté comme entaché d'une irrecevabilité manifeste l'appel formé par M. A contre une ordonnance du 29 octobre 2009 du vice-président du tribunal administratif de Paris ; que, pour fonder cette ordonnance, il a relevé, d'une part, que la requête de M. A n'était pas présentée par l'un des mandataires mentionnés à l'article R. 431-2, alors qu'il avait été informé de cette obligation par la notification de l'ordonnance attaquée, et, d'autre part, que si l'intéressé avait sollicité, le 14 février 2010, le bénéfice de l'aide juridictionnelle, le bureau d'aide juridictionnelle près le tribunal de grande instance de Paris avait, par décision en date du 15 avril 2010, constaté la caducité de sa demande, faute de production des pièces justificatives qui lui avaient été réclamées ;<br/>
<br/>
              3. Considérant toutefois qu'en fixant, pour pallier l'absence d'indication au dossier de la date de réception par l'intéressé de la décision du bureau d'aide juridictionnelle, cette date au 16 juin 2010 au vu des " délais d'acheminement normaux " pour les services postaux à l'étranger, le président de la 6ème chambre de la cour administrative d'appel de Paris a commis une erreur de droit ; qu'au surplus, en fixant au 16 octobre 2010 la date d'expiration du délai de deux mois augmenté du délai de distance permettant au requérant de régulariser par l'intermédiaire d'un avocat sa requête d'appel, le président de la cour administrative d'appel a, eu égard à la date du 16 juin 2010 qu'il avait retenue comme date de réception de la décision de caducité de l'aide juridictionnelle, commis une autre erreur de droit, dès lors que s'agissant d'un délai franc, celui-ci expirait le 17 octobre 2010 à minuit ; que, dès lors, M. A est fondé à demander l'annulation de l'ordonnance attaquée en date du 16 octobre 2010 ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              5. Considérant que, pour rejeter la demande de M. A tendant à l'annulation de la décision du 29 décembre 2008 du préfet de la région Ile-de-France, préfet de Paris, refusant de lui attribuer la carte de combattant, le vice-président du tribunal administratif de Paris s'est notamment fondé sur la circonstance que le requérant n'établissait pas l'erreur manifeste d'appréciation du préfet ; que, toutefois, le ministre reconnait que M. A remplit les conditions pour obtenir la délivrance de cette carte ; qu'ainsi M. A est fondé à demander l'annulation de l'ordonnance du 29 octobre 2009 du vice-président du tribunal administratif de Paris ainsi que la décision préfectorale litigieuse ; <br/>
<br/>
              6. Considérant que M. A a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Rocheteau, Uzan-Sarano, avocat de M. A, renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Rocheteau, Uzan-Sarano ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 16 octobre 2010 du président de la 6ème chambre de la cour administrative d'appel de Paris est annulée.<br/>
Article 2 : L'ordonnance du 29 octobre 2009 du vice-président du tribunal administratif de Paris est annulée.<br/>
Article 3 : La décision du 29 décembre 2008 du préfet de la région Ile-de-France, préfet de Paris, est annulée.<br/>
Article 4 : L'Etat versera à la SCP Rocheteau, Uzan-Sarano, avocat de M. A, une somme de 3 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 5 : La présente décision sera notifiée à M. Amar A, au ministre de la défense et au préfet de la région Ile-de-France, préfet de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
