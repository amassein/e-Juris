<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044500343</ID>
<ANCIEN_ID>JG_L_2021_12_000000456741</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/50/03/CETATEXT000044500343.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 14/12/2021, 456741, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>456741</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BUK LAMENT - ROBILLOT</AVOCATS>
<RAPPORTEUR>M. Thomas Janicot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:456741.20211214</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La commune de La Trinité, à l'appui de sa demande tendant à l'annulation de la décision du 31 mars 2021 par laquelle la direction départementale des finances publiques a fixé le coefficient correcteur communal en application du IV de l'article 16 de la loi n° 2019-1479 du 28 décembre 2019 de finances pour 2020, a produit un mémoire, enregistré le 26 mai 2021 au greffe du tribunal administratif de Nice, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lequel elle soulève une question prioritaire de constitutionnalité.<br/>
<br/>
              Par une ordonnance n° 2102918 du 13 septembre 2021, enregistrée le 15 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, le président de la 2ème chambre du tribunal administratif de Nice a décidé, avant qu'il soit statué sur la demande de la commune de La Trinité et par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution du IV de l'article 16 de la loi du 28 décembre 2019 de finances pour 2020. <br/>
<br/>
              Par la question prioritaire de constitutionnalité transmise et par un mémoire en réplique, enregistré le 24 novembre 2021 au secrétariat du contentieux du Conseil d'Etat, la commune de La Trinité soutient que le IV de l'article 16 de la loi du 28 décembre 2019 de finances pour 2020, applicable au litige, méconnaît l'article 6 de la Déclaration des droits de l'homme et du citoyen de 1789 dont résulte le principe d'égalité devant la loi fiscale et l'article 72 de la Constitution, qui garantit la libre administration des collectivités territoriales. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - la loi n° 2019-1479 du 28 décembre 2019, notamment son article 16 ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code général des impôts ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Janicot, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Buk Lament - Robillot, avocat de la commune de La Trinité ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes de l'article L. 5212-19 du code général des collectivités territoriales, relatif aux syndicats de communes : " Les recettes du budget du syndicat comprennent : 1° La contribution des communes associées (...) ". Aux termes de l'article L. 5212-20 du même code : " La contribution des communes associées mentionnée au 1° de l'article L. 5212-19 est obligatoire pour ces communes pendant la durée du syndicat et dans la limite des nécessités du service telle que les décisions du syndicat l'ont déterminée. / Le comité du syndicat peut décider de remplacer en tout ou partie cette contribution par le produit de la taxe d'habitation, des taxes foncières et de la cotisation foncière des entreprises. / La mise en recouvrement de ces impôts ne peut toutefois être poursuivie que si le conseil municipal, obligatoirement consulté dans un délai de quarante jours, ne s'y est pas opposé en affectant d'autres ressources au paiement de sa quote-part ".<br/>
<br/>
              3. Aux termes de l'article 1379 du code général des impôts : " I. Les communes perçoivent, dans les conditions déterminées par le présent chapitre : 1° La taxe foncière sur les propriétés bâties, prévue aux articles 1380 et 1381 ; 2° La taxe foncière sur les propriétés non bâties, prévue à l'article 1393 ; 3° La taxe d'habitation, prévue à l'article 1407 ". Aux termes de l'article 1407 bis du même code : " Les communes autres que celles visées à l'article 232 peuvent, par une délibération prise dans les conditions prévues à l'article 1639 A bis, assujettir à la taxe d'habitation, pour la part communale et celle revenant aux établissements publics de coopération intercommunale sans fiscalité propre, les logements vacants depuis plus de deux années au 1er janvier de l'année d'imposition. La vacance s'apprécie au sens des V et VI de l'article 232 ". Aux termes de l'article 1609 quater du même code : " Le comité d'un syndicat de communes peut décider, dans les conditions prévues à l'article L 5212-20 du code général des collectivités territoriales, de lever les impositions mentionnées aux 1° à 4° du I de l'article 1379 du présent code en remplacement de tout ou partie de la contribution des communes associées. La répartition de ces impositions s'effectue suivant les modalités définies au III de l'article 1636 B octies ". Aux termes de l'article 1636 B octies du même code : " Le produit fiscal à recouvrer dans chacune des communes membres au profit d'un syndicat de communes est réparti entre les taxes foncières, la taxe d'habitation et la cotisation foncière des entreprises proportionnellement aux recettes que chacune de ces taxes procurerait à la commune si l'on appliquait les taux de l'année précédente aux bases de l'année d'imposition ". <br/>
<br/>
              4. L'article 16 de la loi du 28 décembre 2019 de finances pour 2020 prévoit que tous les contribuables soient progressivement exonérés de la taxe d'habitation sur la résidence principale d'ici 2023. Dans ce cadre, il prévoit de transférer à l'Etat, à compter de 2021, le produit résiduel de cette taxe et de compenser la perte de recettes qui en découle pour les communes par le transfert à ces dernières de la part départementale de la taxe foncière sur les propriétés bâties. Afin que le montant de taxe foncière sur les propriétés bâties transféré soit égal au montant du produit de taxe d'habitation perçu par chaque commune sur la base imposable constatée en 2020 et au regard des taux fixés en 2017, il instaure un coefficient correcteur dont les modalités de calcul sont fixées à son IV aux termes duquel :  " IV.-A.- Pour chaque commune, est calculée la différence entre les deux termes suivants : 1° La somme : a) Du produit de la base d'imposition à la taxe d'habitation sur les locaux meublés affectés à l'habitation principale de la commune déterminée au titre de 2020 par le taux communal de taxe d'habitation appliqué en 2017 sur le territoire de la commune ; b) Des compensations d'exonération de taxe d'habitation versées en 2020 à la commune ; c) De la moyenne annuelle des rôles supplémentaires de taxe d'habitation sur les locaux meublés affectés à l'habitation principale émis en 2018, 2019 et 2020 au profit de la commune ; 2° La somme : a) Du produit net issu des rôles généraux de la taxe foncière sur les propriétés bâties émis en 2020 au profit du département sur le territoire de la commune ; b) Des compensations d'exonération de taxe foncière sur les propriétés bâties versées en 2020 au département sur le territoire de la commune ; c) De la moyenne annuelle des rôles supplémentaires de taxe foncière sur les propriétés bâties émis en 2018, 2019 et 2020 au profit du département sur le territoire de la commune. B.-Pour chaque commune, est calculé un coefficient correcteur égal au rapport entre les termes suivants : 1° La somme : a) Du produit net issu des rôles généraux de la taxe foncière sur les propriétés bâties émis en 2020 au profit de la commune ; b) Du produit net issu des rôles généraux de la taxe foncière sur les propriétés bâties émis en 2020 au profit du département sur le territoire de la commune ; c) De la différence définie au A du présent IV ; 2° La somme : a) Du produit net issu des rôles généraux de la taxe foncière sur les propriétés bâties émis en 2020 au profit de la commune ; b) Du produit net issu des rôles généraux de la taxe foncière sur les propriétés bâties émis en 2020 au profit du département sur le territoire de la commune ".<br/>
<br/>
              5. Il résulte de ces dispositions que le taux communal de taxe d'habitation mentionné au a) du 1° du A du IV de l'article 16 de la loi du 28 décembre 2019 de finances pour 2020, lequel est fixé par la commune, se distingue du taux additionnel de taxe d'habitation levée par un syndicat de communes en application de l'article L. 5212-20 du code général des collectivités territoriales, qui est fixé en fonction du produit fiscal à recouvrer dans chaque commune membre par répartition entre les taxes foncières, la taxe d'habitation et la cotisation foncière des entreprises, proportionnellement aux recettes que chacune de ces taxes procurerait à la commune si l'on appliquait les taux de l'année précédente aux bases de l'année d'imposition. Il en résulte que le coefficient correcteur mentionné au point 4 et destiné à compenser pour les communes la suppression progressive de la taxe d'habitation sur la résidence principale n'inclut pas le produit de la taxe d'habitation perçu par un syndicat de communes en application de l'article L. 5212-20 du code général des collectivités territoriales.<br/>
<br/>
              6. Les dispositions du IV de l'article 16 de la loi du 28 décembre 2019 de finances pour 2020 sont applicables au litige et n'ont pas été déclarées conformes à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel. Le moyen tiré de ce qu'elles portent atteinte aux droits et libertés garantis par la Constitution, notamment au principe d'égalité devant la loi fiscale garanti par l'article 6 de la Déclaration des droits de l'homme et du citoyen de 1789 et à la libre administration des collectivités territoriales garantie par l'article 72 de la Constitution, soulève une question présentant un caractère sérieux. Ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée. <br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La question prioritaire de constitutionnalité portant sur les dispositions du IV de l'article 16 de la loi du 28 décembre 2019 de finances pour 2020 est transmise au Conseil constitutionnel. <br/>
Article 2 : La présente décision sera notifiée à la commune de La Trinité, au ministre de l'économie, des finances et de la relance et à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales. <br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre et au tribunal administratif de Nice. <br/>
<br/>
              Délibéré à l'issue de la séance du 26 novembre 2021 où siégeaient : Mme Christine Maugüé, présidente adjointe de la section du contentieux, présidant ; M. I... C..., M. Pierre Collin, présidents de chambre ; M. G... K..., M. H... E..., M. D... J..., M. B... L..., M. Pierre Boussaroque, conseillers d'Etat et M. Thomas Janicot, maître des requêtes-rapporteur. <br/>
<br/>
              Rendu le 14 décembre 2021.<br/>
<br/>
<br/>
              La Présidente : <br/>
              Signé : Mme Christine Maugüé<br/>
<br/>
               Le rapporteur : <br/>
              	Signé : M. Thomas Janicot<br/>
<br/>
<br/>
              La secrétaire :<br/>
              Signé : Mme A... F...<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
