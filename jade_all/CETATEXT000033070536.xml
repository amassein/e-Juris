<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033070536</ID>
<ANCIEN_ID>JG_L_2016_08_000000402742</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/07/05/CETATEXT000033070536.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, formation collégiale, 26/08/2016, 402742, Publié au recueil Lebon</TITRE>
<DATE_DEC>2016-08-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402742</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés, formation collégiale</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU ; SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>M. Bernard Stirn</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2016:402742.20160826</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              I - La Ligue des droits de l'homme, M. B...D...et M. A...C..., ont demandé au juge des référés du tribunal administratif de Nice, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner la suspension de l'exécution des dispositions du 4.3 de l'article 4 de l'arrêté du 5 août 2016 du maire de la commune de Villeneuve-Loubet portant règlement de police, de sécurité et d'exploitation des plages concédées par l'Etat à la commune de Villeneuve-Loubet. Par une ordonnance n° 1603508 et 1603523 du 22 août 2016, le juge des référés du tribunal administratif de Nice a rejeté leurs demandes. <br/>
<br/>
              Par une requête et un mémoire en réplique enregistrés les 23 et 25 août 2016 au secrétariat du contentieux du Conseil d'Etat, la Ligue des droits de l'homme, M. B...D...et M. A... C..., demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à leur demande de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Ils soutiennent que : <br/>
              - ils sont recevables à solliciter la suspension de l'exécution de l'arrêté contesté ;<br/>
              - la condition d'urgence est remplie dès lors que, d'une part, l'arrêté préjudicie de manière suffisamment grave et immédiate à un intérêt public, à la situation des requérants ainsi qu'aux intérêts qu'ils entendent défendre, d'autre part, l'appel a été formé dans les plus brefs délais et, enfin, l'arrêté contesté a vocation à produire ses effets jusqu'au 15 septembre 2016 ;<br/>
              - l'arrêté contesté porte une atteinte grave et manifestement illégale à la liberté de manifester ses convictions religieuses, à la liberté de se vêtir dans l'espace public et à la liberté d'aller et de venir ;<br/>
              - il ne repose sur aucun fondement juridique pertinent;<br/>
              - la restriction apportée aux libertés n'est pas justifiée par des circonstances particulières locales.<br/>
<br/>
              Par deux mémoires en défense, enregistrés les 24 et 25 août 2016, le maire de la commune de Villeneuve-Loubet conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par les requérants ne sont pas fondés. <br/>
<br/>
<br/>
<br/>
              II - L'Association de défense des droits de l'homme Collectif contre l'islamophobie en France a demandé au juge des référés du tribunal administratif de Nice, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner la suspension de l'exécution du 4.3 de l'article 4.3 du même arrêté du 5 août 2016 du maire de la commune de Villeneuve-Loubet. Par une ordonnance n° 1603508 et 1603523 du 22 août 2016, le juge des référés du tribunal administratif de Nice a rejeté sa demande. <br/>
<br/>
              Par une requête enregistrée le 24 août 2016 au secrétariat du contentieux du Conseil d'Etat, l'Association de défense des droits de l'homme Collectif contre l'islamophobie en France demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - elle est recevable à solliciter la suspension de l'exécution de l'arrêté contesté ;<br/>
              - l'arrêté contesté méconnaît la loi du 9 décembre 1905 ;<br/>
              - la condition d'urgence est remplie dès lors que, d'une part, l'arrêté contesté préjudicie de manière suffisamment grave et immédiate à un intérêt public, à la situation des requérants ainsi qu'aux intérêts qu'ils entendent défendre, d'autre part, l'appel a été formé dans les plus brefs délais et, enfin, l'arrêté contesté a vocation à produire ses effets jusqu'au 15 septembre 2016 ;<br/>
              - l'arrêté contesté porte une atteinte grave et manifestement illégale au principe d'égalité des citoyens devant la loi, à la liberté d'expression, à la liberté de conscience et à la liberté d'aller et venir ;<br/>
              - il ne repose sur aucun fondement juridique pertinent.<br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré 25 août 2016, le maire de la commune de Villeneuve-Loubet conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par l'association requérante ne sont pas fondés. <br/>
<br/>
              Des observations, enregistrées le 25 août 2016, ont été présentées par le ministre de l'intérieur.<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son Préambule et l'article 1er ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code général des collectivités territoriales ; <br/>
              - la loi du 9 décembre 1905 concernant la séparation des Eglises et de l'Etat ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la Ligue des droits de l'homme et autres et l'Association de défense des droits de l'homme Collectif contre l'islamophobie en France et, d'autre part, la commune de Villeneuve-Loubet ainsi que le ministre de l'intérieur ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 25 août 2016 à 15 heures au cours de laquelle ont été entendus : <br/>
              - Me Spinosi, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la Ligue des droits de l'homme et autres ; <br/>
              - les représentants de l'Association de défense des droits de l'homme Collectif contre l'islamophobie en France ;<br/>
               - Me Pinatel, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la commune de Villeneuve-Loubet ; <br/>
<br/>
              - le représentant de la commune de Villeneuve-Loubet ; <br/>
<br/>
              - la représentante du ministre de l'intérieur ;<br/>
<br/>
              et à l'issue de laquelle l'instruction a été close ; <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. En vertu de l'article L. 521-2 du code de justice administrative, lorsqu'est constituée une situation d'urgence particulière, justifiant qu'il se prononce dans de brefs délais, le juge des référés peut ordonner toute mesure nécessaire à la sauvegarde d'une liberté fondamentale à laquelle une autorité administrative aurait porté une atteinte grave et manifestement illégale. <br/>
<br/>
              2. Des arrêtés du maire de Villeneuve-Loubet (Alpes-Maritimes) du 20 juin 2014 puis du 18 juillet 2016 ont réglementé l'usage des plages concédées à la commune par l'Etat. Ces arrêtés ont été abrogés et remplacés par un nouvel arrêté du 5 août 2016 qui comporte un nouvel article 4.3 aux termes duquel : " Sur l'ensemble des secteurs de plage de la commune, l'accès à la baignade est interdit, du 15 juin au 15 septembre inclus, à toute personne ne disposant pas d'une tenue correcte, respectueuse des bonnes moeurs et du principe de laïcité, et respectant les règles d'hygiène et de sécurité des baignades adaptées au domaine public maritime. Le port de vêtements, pendant la baignade, ayant une connotation contraire aux principes mentionnés ci-avant est strictement interdit sur les plages de la commune ". Ainsi que l'ont confirmé les débats qui ont eu lieu au cours de l'audience publique, ces dispositions ont entendu interdire le port de tenues qui manifestent de manière ostensible une appartenance religieuse lors de la baignade et, en conséquence, sur les plages qui donnent accès à celle-ci. <br/>
<br/>
              3. Deux requêtes ont été présentées devant le juge des référés du tribunal administratif de Nice pour demander, sur le fondement de l'article L. 521-2 du code de justice administrative, la suspension de l'exécution de ces dispositions de l'article 4.3 de l'arrêté du maire de Villeneuve-Loubet. La première de ces requêtes a été introduite par la Ligue des droits de l'homme, M. B...D...et M. A...C..., la seconde par l'Association de défense des droits de l'homme Collectif contre l'islamophobie en France. Par une ordonnance du 22 août 2016, le juge des référés du tribunal administratif de Nice, statuant en formation collégiale de trois juges des référés, a rejeté ces deux requêtes. La Ligue des droits de l'homme, M. B...D...et M. A...C..., d'une part, l'Association de défense des droits de l'homme Collectif contre l'islamophobie en France, d'autre part, font appel de cette ordonnance par deux requêtes qui présentent à juger les mêmes questions et qu'il y a lieu de joindre. <br/>
<br/>
              4. En vertu de l'article L. 2212-1 du code général des collectivités territoriales, le maire est chargé, sous le contrôle administratif du préfet, de la police municipale qui, selon l'article L. 2212-2 de ce code, " a pour objet d'assurer le bon ordre, la sûreté, la sécurité et la salubrité publiques ". L'article L. 2213-23 dispose en outre que : " Le maire exerce la police des baignades et des activités nautiques pratiquées à partir du rivage avec des engins de plage et des engins non immatriculés...Le maire réglemente l'utilisation des aménagements réalisés pour la pratique de ces activités. Il pourvoit d'urgence à toutes les mesures d'assistance et de secours. Le maire délimite une ou plusieurs zones surveillées dans les parties du littoral présentant une garantie suffisante pour la sécurité des baignades et des activités mentionnées ci-dessus. Il détermine des périodes de surveillance... ". <br/>
<br/>
              5. Si le maire est chargé par les dispositions citées au point 4 du maintien de l'ordre dans la commune, il doit concilier l'accomplissement de sa mission avec le respect des libertés garanties par les lois. Il en résulte que les mesures de police que le maire d'une commune du littoral édicte en vue de réglementer l'accès à la plage et la pratique de la baignade doivent être adaptées, nécessaires et proportionnées au regard des seules nécessités de l'ordre public, telles qu'elles découlent des circonstances de temps et de lieu, et compte tenu des exigences qu'impliquent le bon accès au rivage, la sécurité de la baignade ainsi que l'hygiène et la décence sur la plage. Il n'appartient pas au maire de se fonder sur d'autres considérations et les restrictions qu'il apporte aux libertés doivent être justifiées par des risques avérés d'atteinte à l'ordre public. <br/>
<br/>
              6. Il ne résulte pas de l'instruction que des risques de trouble à l'ordre public aient résulté, sur les plages de la commune de Villeneuve-Loubet, de la tenue adoptée en vue de la baignade par certaines personnes. S'il a été fait état au cours de l'audience publique du port sur les plages de la commune de tenues de la nature de celles que l'article 4.3 de l'arrêté litigieux entend prohiber, aucun élément produit devant le juge des référés ne permet de retenir que de tels risques en auraient résulté. En l'absence de tels risques, l'émotion et les inquiétudes résultant des attentats terroristes, et notamment de celui commis à Nice le 14 juillet dernier, ne sauraient suffire à justifier légalement la mesure d'interdiction contestée. Dans ces conditions, le maire ne pouvait, sans excéder ses pouvoirs de police, édicter des dispositions qui interdisent l'accès à la plage et la baignade alors qu'elles ne reposent ni sur des risques avérés de troubles à l'ordre public ni, par ailleurs, sur des motifs d'hygiène ou de décence. L'arrêté litigieux a ainsi porté une atteinte grave et manifestement illégale aux libertés fondamentales que sont la liberté d'aller et venir, la liberté de conscience et la liberté personnelle. Les conséquences de l'application de telles dispositions sont en l'espèce constitutives d'une situation d'urgence qui justifie que le juge des référés fasse usage des pouvoirs qu'il tient de l'article L. 521-2 du code de justice administrative. Il y a donc lieu d'annuler l'ordonnance du juge des référés du tribunal administratif de Nice du 22 août 2016 et d'ordonner la suspension de l'exécution de l'article 4.3 de l'arrêté du maire de Villeneuve-Loubet en date du 5 août 2016.<br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la Ligue des droits de l'homme, de M.D..., de M. C...et de l'Association de défense des droits de l'homme Collectif contre l'islamophobie en France. Il n'y pas lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Villeneuve-Loubet, en application de ces dispositions, les sommes que demandent, d'une part, la Ligue des droits de l'homme, M. D...et M.C..., d'autre part l'Association de défense des droits de l'homme Collectif contre l'islamophobie en France. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Nice en date du 22 août 2016 est annulée.<br/>
Article 2 : L'exécution de l'article 4.3 de l'arrêté du maire de Villeneuve-Loubet en date du 5 août 2016 est suspendue.<br/>
Article 3 : Les conclusions de la commune de Villeneuve-Loubet et celles de la Ligue des droits de l'homme, de M.D..., de M.C..., et de l'Association de défense des droits de l'homme Collectif contre l'islamophobie en France tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4. La présente ordonnance sera notifiée à la Ligue des droits de l'homme, à M.D..., à M.C..., à l'Association de défense des droits de l'homme Collectif contre l'islamophobie en France, à la commune de Villeneuve-Loubet et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-03-02 COLLECTIVITÉS TERRITORIALES. COMMUNE. ATTRIBUTIONS. POLICE. - 1) CONDITIONS DE LÉGALITÉ DE MESURES RÉGLEMENTANT L'ACCÈS À LA PLAGE ET LA BAIGNADE - 2) INTERDICTION DE L'ACCÈS AUX PLAGES ET DE LA BAIGNADE AUX PERSONNES PORTANT UNE TENUE MANIFESTANT DE MANIÈRE OSTENSIBLE UNE APPARTENANCE RELIGIEUSE - ILLÉGALITÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-03-05 DROITS CIVILS ET INDIVIDUELS. LIBERTÉS PUBLIQUES ET LIBERTÉS DE LA PERSONNE. LIBERTÉ D'ALLER ET VENIR. - MÉCONNAISSANCE - EXISTENCE - INTERDICTION DE L'ACCÈS AUX PLAGES ET DE LA BAIGNADE AUX PERSONNES PORTANT UNE TENUE MANIFESTANT DE MANIÈRE OSTENSIBLE UNE APPARTENANCE RELIGIEUSE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">26-03-07 DROITS CIVILS ET INDIVIDUELS. LIBERTÉS PUBLIQUES ET LIBERTÉS DE LA PERSONNE. LIBERTÉ DES CULTES. - MÉCONNAISSANCE - EXISTENCE - INTERDICTION DE L'ACCÈS AUX PLAGES ET DE LA BAIGNADE AUX PERSONNES PORTANT UNE TENUE MANIFESTANT DE MANIÈRE OSTENSIBLE UNE APPARTENANCE RELIGIEUSE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">49-04 POLICE. POLICE GÉNÉRALE. - POLICE MUNICIPALE - 1) CONDITIONS DE LÉGALITÉ DES MESURES RÉGLEMENTANT L'ACCÈS À LA PLAGE ET LA BAIGNADE - 2) INTERDICTION DE L'ACCÈS AUX PLAGES ET DE LA BAIGNADE AUX PERSONNES PORTANT UNE TENUE MANIFESTANT DE MANIÈRE OSTENSIBLE UNE APPARTENANCE RELIGIEUSE - ILLÉGALITÉ.
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">54-035-03-03-01-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ TENDANT AU PRONONCÉ DE MESURES NÉCESSAIRES À LA SAUVEGARDE D'UNE LIBERTÉ FONDAMENTALE (ART. L. 521-2 DU CODE DE JUSTICE ADMINISTRATIVE). CONDITIONS D'OCTROI DE LA MESURE DEMANDÉE. ATTEINTE GRAVE ET MANIFESTEMENT ILLÉGALE À UNE LIBERTÉ FONDAMENTALE. ATTEINTE GRAVE ET MANIFESTEMENT ILLÉGALE. - EXISTENCE - INTERDICTION DE L'ACCÈS AUX PLAGES ET DE LA BAIGNADE AUX PERSONNES PORTANT UNE TENUE MANIFESTANT DE MANIÈRE OSTENSIBLE UNE APPARTENANCE RELIGIEUSE.
</SCT>
<ANA ID="9A"> 135-02-03-02 1) Les mesures de police que le maire d'une commune du littoral édicte en vue de réglementer l'accès à la plage et la pratique de la baignade doivent être adaptées, nécessaires et proportionnées au regard des seules nécessités de l'ordre public, telles qu'elles découlent des circonstances de temps et de lieu, et compte tenu des exigences qu'impliquent le bon accès au rivage, la sécurité de la baignade ainsi que l'hygiène et la décence sur la plage. Il n'appartient pas au maire de se fonder sur d'autres considérations et les restrictions qu'il apporte aux libertés doivent être justifiées par des risques avérés d'atteinte à l'ordre public.,,,2) Le maire ne peut, sans excéder ses pouvoirs de police, édicter des dispositions qui interdisent l'accès à la plage et la baignade aux personnes qui portent une tenue manifestant de manière ostensible une appartenance religieuse alors qu'elles ne reposent ni sur des risques avérés de troubles à l'ordre public ni sur des motifs d'hygiène ou de décence.</ANA>
<ANA ID="9B"> 26-03-05 Le maire ne peut, sans excéder ses pouvoirs de police, édicter des dispositions qui interdisent l'accès à la plage et la baignade aux personnes qui portent une tenue manifestant de manière ostensible une appartenance religieuse alors qu'elles ne reposent ni sur des risques avérés de troubles à l'ordre public ni sur des motifs d'hygiène ou de décence. De telles dispositions portent une atteinte grave et manifestement illégale aux libertés fondamentales que sont la liberté d'aller et venir, la liberté de conscience et la liberté personnelle.</ANA>
<ANA ID="9C"> 26-03-07 Le maire ne peut, sans excéder ses pouvoirs de police, édicter des dispositions qui interdisent l'accès à la plage et la baignade aux personnes qui portent une tenue manifestant de manière ostensible une appartenance religieuse alors qu'elles ne reposent ni sur des risques avérés de troubles à l'ordre public ni sur des motifs d'hygiène ou de décence. De telles dispositions portent une atteinte grave et manifestement illégale aux libertés fondamentales que sont la liberté d'aller et venir, la liberté de conscience et la liberté personnelle.</ANA>
<ANA ID="9D"> 49-04 1) Les mesures de police que le maire d'une commune du littoral édicte en vue de réglementer l'accès à la plage et la pratique de la baignade doivent être adaptées, nécessaires et proportionnées au regard des seules nécessités de l'ordre public, telles qu'elles découlent des circonstances de temps et de lieu, et compte tenu des exigences qu'impliquent le bon accès au rivage, la sécurité de la baignade ainsi que l'hygiène et la décence sur la plage. Il n'appartient pas au maire de se fonder sur d'autres considérations et les restrictions qu'il apporte aux libertés doivent être justifiées par des risques avérés d'atteinte à l'ordre public.,,,2) Le maire ne peut, sans excéder ses pouvoirs de police, édicter des dispositions qui interdisent l'accès à la plage et la baignade aux personnes qui portent une tenue manifestant de manière ostensible une appartenance religieuse alors qu'elles ne reposent ni sur des risques avérés de troubles à l'ordre public ni sur des motifs d'hygiène ou de décence.</ANA>
<ANA ID="9E"> 54-035-03-03-01-02 Le maire ne peut, sans excéder ses pouvoirs de police, édicter des dispositions qui interdisent l'accès à la plage et la baignade aux personnes qui portent une tenue manifestant de manière ostensible une appartenance religieuse alors qu'elles ne reposent ni sur des risques avérés de troubles à l'ordre public ni sur des motifs d'hygiène ou de décence. De telles dispositions portent une atteinte grave et manifestement illégale aux libertés fondamentales que sont la liberté d'aller et venir, la liberté de conscience et la liberté personnelle.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
