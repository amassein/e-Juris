<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031978235</ID>
<ANCIEN_ID>JG_L_2016_02_000000387840</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/97/82/CETATEXT000031978235.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème SSR, 03/02/2016, 387840, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-02-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387840</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO</AVOCATS>
<RAPPORTEUR>M. Yannick Faure</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2016:387840.20160203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une décision du 9 juillet 2015, le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi de l'exploitation agricole à responsabilité limitée (EARL) Le Dreff Coat Laez dirigées contre l'ordonnance n° 1500293 du juge des référés du tribunal administratif de Rennes du 27 janvier 2015 en tant que cette ordonnance s'est prononcée sur sa demande de suspension de l'exécution de la décision du 21 novembre 2014 du responsable de l'unité territoriale des Côtes-d'Armor de la direction régionale des entreprises, de la concurrence, de la consommation, du travail et de l'emploi (DIRECCTE) refusant la reprise du contrat d'apprentissage conclu avec Mme A...B...et interdisant à cette dernière de recruter de nouveaux apprentis ainsi que des jeunes sous contrat en alternance pour une durée de dix-huit mois.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yannick Faure, auditeur,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de l'EARL Le Dreff Coat Laez ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que le responsable de l'unité territoriale des Côtes-d'Armor de la direction régionale des entreprises, de la concurrence, de la consommation, du travail et de l'emploi de Bretagne a, le 17 novembre 2014, prononcé la suspension à titre provisoire, en application de l'article L. 6225-4 du code du travail, du contrat d'apprentissage conclu entre l'EARL Le Dreff Coat Laez et MmeB..., puis a refusé, le 21 novembre suivant, en application des articles L. 6225-5 et L. 6225-6 du même code, d'autoriser la reprise de l'exécution de ce contrat d'apprentissage et a interdit à l'entreprise, pour une durée de dix-huit mois, de recruter de nouveaux apprentis ainsi que des jeunes sous contrat en alternance ; que, par une ordonnance du 27 janvier 2015, le juge des référés du tribunal administratif de Rennes a rejeté la demande de suspension de l'exécution de ces décisions présentée par l'EARL ; que, par une décision du 9 juillet 2015, le Conseil d'Etat, statuant au contentieux a admis les conclusions du pourvoi en cassation formé par l'EARL Le Dreff Coat Laez dirigées contre cette ordonnance en tant seulement qu'elle se prononce sur la décision du 21 novembre 2014 ;<br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              3. Considérant que l'urgence justifie que soit prononcée la suspension d'un  acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sur la situation de ce dernier ou, le cas échéant, des personnes concernées, sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ; qu'il lui appartient également, l'urgence s'appréciant objectivement et compte tenu de l'ensemble des circonstances de chaque espèce, de faire apparaître dans sa décision tous les éléments qui, eu égard notamment à l'argumentation des parties, l'ont conduit à considérer que la suspension demandée revêtait un caractère d'urgence ;<br/>
<br/>
              4. Considérant que les pouvoirs de suspendre un contrat d'apprentissage puis de refuser d'autoriser la reprise de l'exécution du contrat ont été conférés au directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi par les articles L. 6225-4 et L. 6225-5 du code du travail  pour la protection de l'apprenti, lorsqu'existe un risque sérieux d'atteinte à sa santé ou à son intégrité physique ou morale ; qu'en jugeant que, pour justifier de l'urgence à suspendre l'exécution de la décision litigieuse, l'EARL Le Dreff Coat Laez ne pouvait utilement se prévaloir de ses effets sur la situation de MmeB..., le juge des référés n'a pas commis d'erreur de droit ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que l'EARL Le Dreff Coat Laez n'est pas fondée à demander l'annulation de l'ordonnance qu'elle attaque, laquelle est suffisamment motivée ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de l'EARL Le Dreff Coat Laez est rejeté.<br/>
Article 2 : La présente décision sera notifiée à l'exploitation agricole à responsabilité limitée Le Dreff Coat Laez et à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social. <br/>
Copie en sera adressée à Mme A...B....<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
