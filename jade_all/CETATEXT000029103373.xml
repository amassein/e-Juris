<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029103373</ID>
<ANCIEN_ID>JG_L_2014_06_000000372803</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/10/33/CETATEXT000029103373.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 18/06/2014, 372803</TITRE>
<DATE_DEC>2014-06-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372803</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Nicolas Labrune</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:372803.20140618</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire, le mémoire complémentaire et le nouveau mémoire, enregistrés les 15 octobre 2013, 30 octobre 2013 et 29 janvier 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Electricité de France, dont le siège social est situé 22-30 avenue de Wagram à Paris (75008) ; Electricité de France (" EDF ") demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 13BX01195 du 18 septembre 2013 par laquelle le juge des référés de la cour administrative d'appel de Bordeaux a rejeté sa requête tendant, d'une part, à l'annulation de l'ordonnance n° 1201053 du 8 avril 2013 du juge des référés du tribunal administratif de Fort-de-France rejetant sa demande de condamnation de l'Etat à lui verser une provision de 10 000 000 d'euros au titre du renchérissement de l'approvisionnement en fioul lourd des centrales de production de la Martinique du fait des réquisitions préfectorales dont elle a été l'objet, d'autre part, à la condamnation de l'Etat à lui verser cette provision ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande de provision sur le fondement de l'article R. 541-1 du code de justice administrative ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales, notamment son article L. 2215-1 ;<br/>
<br/>
              Vu le décret n° 2003-1241 du 23 décembre 2003 ;<br/>
<br/>
              Vu le décret n° 2010-1332 du 8 novembre 2010 ;<br/>
<br/>
              Vu le code de justice administrative, notamment son article R. 541-1 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Labrune, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat d'Electricité de France ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que, durant la période antérieure au mois de septembre 2009, la société Electricité de France (EDF) s'approvisionnait en fioul lourd, pour les centrales thermiques de Bellefontaine et de la Pointe des carrières, qu'elle exploite en Martinique, soit, s'agissant d'une partie de l'approvisionnement de la centrale de Bellefontaine, directement sur le marché international, par l'intermédiaire de la société VITOL, soit, s'agissant du surplus des approvisionnements, auprès de la société anonyme de la raffinerie des Antilles (SARA), par l'intermédiaire de la société Esso Antilles Guyane, à un prix librement convenu par EDF avec la SARA ; qu'à compter du mois de septembre 2009, le préfet de la Martinique a pris, sur le fondement des dispositions de l'article L. 2215-1 du code général des collectivités territoriales, plusieurs arrêtés requérant EDF de s'approvisionner en fioul lourd auprès de la SARA, au prix maximum autorisé pour ce produit en vertu, d'abord, du décret du 23 décembre 2003 réglementant les prix des produits pétroliers dans les départements de la Guadeloupe et de la Martinique, puis du décret du 8 novembre 2010 réglementant les prix des produits pétroliers et du gaz de pétrole liquéfié dans les départements de la Guadeloupe, de la Guyane et de la Martinique ; qu'EDF a saisi le tribunal administratif de plusieurs demandes, pour un montant total de plus de 60 millions d'euros au 31 décembre 2013, tendant à ce que l'Etat l'indemnise du surcoût ayant résulté pour elle de la différence entre le prix administré qui lui était imposé par les réquisitions et le cours mondial auquel elle aurait pu s'approvisionner en l'absence de réquisitions ; que, saisi par l'Etat sur le fondement de l'article R. 532-1 du code de justice administrative, le juge des référés du tribunal administratif de Fort de France a désigné un expert afin, notamment, d'évaluer ce surcoût ; que, postérieurement à la remise de son rapport par l'expert, EDF a demandé au juge des référés du tribunal administratif de Fort-de-France, sur le fondement de l'article R. 541-1 du code de justice administrative, de condamner l'Etat à lui verser une provision de 10 millions d'euros, au titre de la rétribution à laquelle elle estime avoir droit du fait des réquisitions dont elle est l'objet depuis quatre ans ; qu'EDF se pourvoit en cassation contre l'ordonnance par laquelle le juge des référés de la cour administrative d'appel de Bordeaux a rejeté son appel contre l'ordonnance du juge des référés du tribunal administratif de Fort-de-France rejetant sa demande de provision ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 541-1 du code de justice administrative : " Le juge des référés peut, même en l'absence d'une demande au fond, accorder une provision au créancier qui l'a saisi lorsque l'existence de l'obligation n'est pas sérieusement contestable. Il peut, même d'office, subordonner le versement de la provision à la constitution d'une garantie " ; qu'il résulte de ces dispositions qu'il appartient au juge des référés d'apprécier le caractère non sérieusement contestable de la seule obligation invoquée devant lui par la partie qui demande une provision, sans tenir compte d'une éventuelle créance distincte que le défendeur détiendrait sur le demandeur ;<br/>
<br/>
              3. Considérant qu'en jugeant qu'il convient, pour apprécier le caractère non sérieusement contestable de la rétribution à laquelle EDF estime avoir droit du fait des réquisitions préfectorales dont elle est l'objet depuis le mois de septembre 2009, de tenir compte du montant des avantages dont la société avait bénéficié en matière de prix d'achat lors de son approvisionnement en fioul lourd antérieurement à la mise en oeuvre des réquisitions, alors que cet enrichissement d'EDF, à le supposer établi, relèverait d'une cause juridique sans lien avec le litige, le juge des référés de la cour administrative d'appel a commis une erreur de droit ; qu'EDF est, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, fondée à demander l'annulation de l'ordonnance attaquée ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée par EDF, en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant qu'il ressort des énonciations mêmes de l'ordonnance du juge des référés du tribunal administratif que celui-ci s'est fondé sur un rapport administratif relatif à la fixation des prix des carburants dans les départements d'outre-mer ; qu'il est constant que ce rapport n'avait été produit par aucune des parties et ne leur avait pas été communiqué ; que, de ce fait, les parties ont été privées de la possibilité de débattre contradictoirement du contenu de ce rapport, en particulier des questions, qu'il posait, d'un éventuel enrichissement d'EDF au cours de la période antérieure à la mise en oeuvre des réquisitions et de la possibilité que cet éventuel enrichissement compense en tout ou partie la rétribution due au titre des réquisitions préfectorales ; que, dès lors, le juge des référés du tribunal administratif a méconnu les exigences inhérentes au caractère contradictoire de l'instruction ; qu'il suit de là qu'EDF est fondée à soutenir que son ordonnance est intervenue à la suite d'une procédure irrégulière et à en demander, pour ce motif, l'annulation ;<br/>
<br/>
              6. Considérant qu'il y a lieu d'évoquer et de statuer immédiatement sur la demande présentée par EDF devant le juge des référés du tribunal administratif de Fort-de-France ;<br/>
<br/>
              7. Considérant, ainsi qu'il a été dit au point 3, que les avantages financiers dont EDF aurait pu bénéficier pour son approvisionnement en fioul lourd antérieurement à la mise en oeuvre des réquisitions, à les supposer établis, relèvent d'une cause juridique distincte de celle invoquée par EDF pour demander l'octroi d'une provision et sans lien avec le litige ; qu'ils n'ont, par conséquent, pas à être pris en compte pour apprécier le caractère non sérieusement contestable de la rétribution que revendique EDF au titre des réquisitions préfectorales dont elle a été l'objet depuis le mois de septembre 2009 ;<br/>
<br/>
              8. Considérant qu'aux termes de l'article L. 2215-1 du code général des collectivités territoriales : " (...) 4° En cas d'urgence, lorsque l'atteinte constatée ou prévisible au bon ordre, à la salubrité, à la tranquillité et à la sécurité publiques l'exige et que les moyens dont dispose le préfet ne permettent plus de poursuivre les objectifs pour lesquels il détient des pouvoirs de police, celui-ci peut, par arrêté motivé, pour toutes les communes du département ou plusieurs ou une seule d'entre elles, réquisitionner tout bien ou service, requérir toute personne nécessaire au fonctionnement de ce service ou à l'usage de ce bien et prescrire toute mesure utile jusqu'à ce que l'atteinte à l'ordre public ait pris fin ou que les conditions de son maintien soient assurées. / (...) La rétribution par l'Etat de la personne requise ne peut se cumuler avec une rétribution par une autre personne physique ou morale. / La rétribution doit uniquement compenser les frais matériels, directs et certains résultants de l'application de l'arrêté de réquisition. /Dans le cas d'une réquisition adressée à une entreprise, lorsque la prestation requise est de même nature que celles habituellement fournies à la clientèle, le montant de la rétribution est calculé d'après le prix commercial normal et licite de la prestation. / Dans les conditions prévues par le code de justice administrative, le président du tribunal administratif ou le magistrat qu'il délègue peut, dans les quarante-huit heures de la publication ou de la notification de l'arrêté, à la demande de la personne requise, accorder une provision représentant tout ou partie de l'indemnité précitée, lorsque l'existence et la réalité de cette indemnité ne sont pas sérieusement contestables. (...) " ; qu'il résulte de ces dispositions que lorsqu'une réquisition consiste en une obligation faite à une entreprise d'acheter un bien, le montant de la rétribution à laquelle a droit l'entreprise requise est égal à la différence entre les coûts effectivement supportés par elle dans le cadre de la réquisition et les coûts qu'elle aurait supportés si elle avait été libre de ses achats, calculés d'après le prix commercial normal et licite de l'achat du bien ;<br/>
<br/>
              9. Considérant qu'il est constant qu'en l'absence de réquisition, EDF aurait pu s'approvisionner en fioul lourd sur le marché international, au prix du marché ; que, par suite, le montant de la rétribution à laquelle elle a droit au titre des réquisitions dont elle a fait l'objet est égal au surcoût résultant de la différence entre le prix administré, qui lui était imposé, et le prix du marché international sur la même période, duquel il convient de soustraire les coûts de transport et de stockage de fioul lourd qu'EDF a évités en s'approvisionnant, dans les conditions imposées par les réquisitions, auprès de la SARA ;<br/>
<br/>
              10. Considérant qu'il résulte de l'instruction, notamment du rapport de l'expert désigné, sur le fondement de l'article R. 532-1 du code de justice administrative, par le juge des référés du tribunal administratif de Fort-de-France, que le surcoût résultant pour EDF de la différence entre le prix qui lui était imposé par les réquisitions et le prix du marché international sur la même période peut être évalué, pour la seule période de septembre 2009 à février 2011, à plus de 17 millions d'euros ; que les coûts de transport et de stockage de fioul lourd qu'EDF a évités en s'approvisionnant, dans les conditions imposées par les réquisitions, auprès de la SARA, sont très inférieurs à 7 millions d'euros ; qu'il suit de là que l'obligation dont se prévaut EDF à l'encontre de l'Etat n'est pas sérieusement contestable ; qu'il y a lieu, par suite, de condamner l'Etat à verser à EDF une provision d'un montant de 10 millions d'euros ;<br/>
<br/>
              11. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge d'EDF, qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 5 000 euros à verser à EDF, au titre de l'ensemble des instances, sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'ordonnance du 18 septembre 2013 du juge des référés de la cour administrative d'appel de Bordeaux est annulée.<br/>
Article 2 : L'ordonnance du 8 avril 2013 du juge des référés du tribunal administratif de Fort-de-France est annulée.<br/>
Article 3 : L'Etat est condamné à verser à EDF une provision de 10 millions d'euros.<br/>
Article 4 : L'Etat versera à EDF une somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Les conclusions du ministre des outre-mer présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à la société Electricité de France et au ministre des outre-mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-03-015-03 PROCÉDURE. PROCÉDURES DE RÉFÉRÉ AUTRES QUE CELLES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ-PROVISION. POUVOIRS ET DEVOIRS DU JUGE. - APPRÉCIATION DU CARACTÈRE NON SÉRIEUSEMENT CONTESTABLE DE LA SEULE OBLIGATION INVOQUÉE PAR LE DÉFENDEUR - EXISTENCE - PRISE EN COMPTE D'UNE ÉVENTUELLE CRÉANCE DISTINCTE DÉTENUE PAR LE DÉFENDEUR - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-03-015-04 PROCÉDURE. PROCÉDURES DE RÉFÉRÉ AUTRES QUE CELLES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ-PROVISION. CONDITIONS. - CARACTÈRE NON SÉRIEUSEMENT CONTESTABLE DE L'OBLIGATION - APPRÉCIATION - PRISE EN COMPTE D'UNE ÉVENTUELLE CRÉANCE DISTINCTE DÉTENUE PAR LE DÉFENDEUR - ABSENCE.
</SCT>
<ANA ID="9A"> 54-03-015-03 Il résulte des dispositions de l'article R. 541-1 du code de justice administrative (CJA) qu'il appartient au juge des référés d'apprécier le caractère non sérieusement contestable de la seule obligation invoquée devant lui par la partie qui demande une provision, sans tenir compte d'une éventuelle créance distincte que le défendeur détiendrait sur le demandeur.</ANA>
<ANA ID="9B"> 54-03-015-04 Il résulte des dispositions de l'article R. 541-1 du code de justice administrative (CJA) qu'il appartient au juge des référés d'apprécier le caractère non sérieusement contestable de la seule obligation invoquée devant lui par la partie qui demande une provision, sans tenir compte d'une éventuelle créance distincte que le défendeur détiendrait sur le demandeur.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
