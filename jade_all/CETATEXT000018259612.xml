<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000018259612</ID>
<ANCIEN_ID>JG_L_2008_01_000000273438</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/18/25/96/CETATEXT000018259612.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 30/01/2008, 273438</TITRE>
<DATE_DEC>2008-01-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>273438</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Delarue</PRESIDENT>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN</AVOCATS>
<RAPPORTEUR>M. Eric  Berti</RAPPORTEUR>
<COMMISSAIRE_GVT>Mlle Courrèges Anne</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 22 octobre 2004 et 16 février 2005 au secrétariat du contentieux du Conseil d'Etat, présentés pour la FEDERATION GENERALE AGROALIMENTAIRE CFDT dont le siège est 47-49 avenue Simon Bolivar à Paris (75019) représentée par ses dirigeants en exercice ; la FEDERATION GENERALE AGROALIMENTAIRE CFDT demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret n° 2004-862 du 24 août 2004 portant application de l'article L. 732-35-1 du code rural, en tant qu'il ajoute un article 71 au décret n° 55-753 du 31 mai 1955, tendant à modifier et à compléter le décret du 18 octobre 1952 et fixant les conditions d'application de la loi du 5 janvier 1955 relative à l'allocation de vieillesse agricole ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code rural ;<br/>
<br/>
              Vu le décret n° 55-753 du 31 mai 1955 tendant à modifier et à compléter le décret n° 52-1166 du 18 octobre 1952 fixant les conditions d'application de la loi du 10 juillet 1952 relative à l'allocation de vieillesse agricole ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Eric Berti, chargé des fonctions de Maître des requêtes,<br/>
<br/>
              - les observations de la SCP Masse-Dessen, Thouvenin, avocat de la FEDERATION GENERALE AGROALIMENTAIRE CFDT, <br/>
<br/>
              - les conclusions de Mlle Anne Courrèges, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant que l'article L. 732-35-1 du code rural dispose que : « Les personnes dont la pension de retraite de base prend effet postérieurement au 31 décembre 2003 peuvent demander la prise en compte, par le régime d'assurance vieillesse des personnes non salariées des professions agricoles, de périodes d'activité accomplies en qualité d'aide familial défini au 2° de l'article L. 722-10. / Par dérogation aux dispositions du 2° de l'article L. 722-10, les périodes d'activité accomplies en tant qu'aide familial à compter de l'âge de quatorze ans peuvent être prises en compte par le régime d'assurance vieillesse des personnes non salariées des professions agricoles. / Un décret détermine les conditions d'application du présent article et, notamment, le mode de calcul des cotisations et les modalités selon lesquelles les demandes de versement de cotisations correspondant à ces périodes doivent être présentées » ;<br/>
<br/>
              Considérant qu'en application de ces dispositions, le décret du 24 août 2004 a inséré dans le décret du 31 mai 1955 un article 71, précisant le barème applicable pour le calcul de la cotisation due pour une année civile au titre du versement pour la retraite afférent à certaines périodes d'activités accomplies en qualité d'aide familial, prévu à l'article L. 732-35-1 du code rural ; que ce barème est progressif selon le critère de la durée de cotisation au régime des assurances sociales agricoles de l'assuré et varie ainsi de 7,5 % du coût d'une année de rachat d'études supérieures pour un assuré justifiant d'une durée de cotisation à ce régime d'au moins égale à 38 années, jusqu'à 100 % de ce coût lorsque l'assuré compte moins de 17,5 années de cotisation à ce même régime ;<br/>
<br/>
              Considérant que si le principe d'égalité ne s'oppose pas à ce que des personnes affiliées à des régimes de sécurité sociale différents, lesquels forment chacun un ensemble dont les dispositions ne peuvent être envisagées isolément, soient soumises à des règles d'assiette, de liquidation et de taux différentes, il fait obstacle, eu égard à l'objet même de la mesure, à ce que les cotisations dues pour valider une même période de travail, au titre d'un même régime et qui n'avaient pu légalement donner lieu à cotisation, soient calculées en faisant application de taux différents selon que les intéressés ont poursuivi leur carrière professionnelle dans ce régime ou, au contraire, ont été affiliés ultérieurement à un autre régime ;<br/>
<br/>
              Considérant qu'en l'espèce, la différenciation des taux prévue par l'article 71 litigieux introduit une discrimination illégale entre les assurés ayant contribué durant toute leur carrière au régime des assurances sociales agricoles et les assurés ayant cotisé dans le cadre d'un autre régime de sécurité sociale après leur période accomplie en qualité d'aide familial ; que par suite, et sans qu'il soit besoin d'examiner l'autre moyen de la requête, la FEDERATION GENERALE AGROALIMENTAIRE CFDT est fondée à soutenir que les dispositions de l'article 71 insérées dans le décret du 31 mai 1955 par le décret du 24 août 2004, méconnaissent le principe d'égalité et doivent, à ce titre, être annulées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le décret n° 2004-862 du 24 août 2004 est annulé, en tant qu'il ajoute un article 71 au décret n° 55-753 du 31 mai 1955.<br/>
Article 2 : La présente décision sera notifiée à la FEDERATION GENERALE AGROALIMENTAIRE CFDT, au Premier ministre et au ministre de l'agriculture et de la pêche.<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. ÉGALITÉ DEVANT LES CHARGES PUBLIQUES. - MÉCONNAISSANCE - COTISATIONS DUES POUR VALIDER UNE MÊME PÉRIODE DE TRAVAIL, AU TITRE D'UN MÊME RÉGIME ET QUI N'AVAIENT PU LÉGALEMENT DONNER LIEU À COTISATION, CALCULÉES EN FAISANT APPLICATION DE TAUX DIFFÉRENTS SELON QUE LES INTÉRESSÉS ONT POURSUIVI LEUR CARRIÈRE PROFESSIONNELLE DANS CE RÉGIME OU ONT ÉTÉ AFFILIÉS ULTÉRIEUREMENT À UN AUTRE RÉGIME - CONSÉQUENCE - ILLÉGALITÉ DE L'ARTICLE 71 DU DÉCRET DU 31 MAI 1955 ISSU DU DÉCRET DU 24 AOÛT 2004 [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">03-02-06 AGRICULTURE, CHASSE ET PÊCHE. PROBLÈMES SOCIAUX DE L'AGRICULTURE. ASSURANCES SOCIALES. - ARTICLE 71 INSÉRÉ PAR LE DÉCRET DU 24 AOÛT 2004 DANS LE DÉCRET DU 31 MAI 1955 TENDANT À MODIFIER ET À COMPLÉTER LE DÉCRET N° 52-1166 DU 18 OCTOBRE 1952 FIXANT LES CONDITIONS D'APPLICATION DE LA LOI N° 55-21 DU 5 JANVIER 1955 RELATIVE À L'ALLOCATION DE VIEILLESSE AGRICOLE - PRINCIPE D'ÉGALITÉ DEVANT LES CHARGES PUBLIQUES - MÉCONNAISSANCE - COTISATIONS DUES POUR VALIDER UNE MÊME PÉRIODE DE TRAVAIL, AU TITRE D'UN MÊME RÉGIME ET QUI N'AVAIENT PU LÉGALEMENT DONNER LIEU À COTISATION, CALCULÉES EN FAISANT APPLICATION DE TAUX DIFFÉRENTS SELON QUE LES INTÉRESSÉS ONT POURSUIVI LEUR CARRIÈRE PROFESSIONNELLE DANS CE RÉGIME OU ONT ÉTÉ AFFILIÉS ULTÉRIEUREMENT À UN AUTRE RÉGIME - CONSÉQUENCE - ILLÉGALITÉ [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">62-03-02-02 SÉCURITÉ SOCIALE. COTISATIONS. ASSIETTE, TAUX ET CALCUL DES COTISATIONS. ASSURANCE VIEILLESSE. - ARTICLE 71 INSÉRÉ PAR LE DÉCRET DU 24 AOÛT 2004 DANS LE DÉCRET DU 31 MAI 1955 TENDANT À MODIFIER ET À COMPLÉTER LE DÉCRET N° 52-1166 DU 18 OCTOBRE 1952 FIXANT LES CONDITIONS D'APPLICATION DE LA LOI N° 55-21 DU 5 JANVIER 1955 RELATIVE À L'ALLOCATION DE VIEILLESSE AGRICOLE - PRINCIPE D'ÉGALITÉ DEVANT LES CHARGES PUBLIQUES - MÉCONNAISSANCE - COTISATIONS DUES POUR VALIDER UNE MÊME PÉRIODE DE TRAVAIL, AU TITRE D'UN MÊME RÉGIME ET QUI N'AVAIENT PU LÉGALEMENT DONNER LIEU À COTISATION, CALCULÉES EN FAISANT APPLICATION DE TAUX DIFFÉRENTS SELON QUE LES INTÉRESSÉS ONT POURSUIVI LEUR CARRIÈRE PROFESSIONNELLE DANS CE RÉGIME OU ONT ÉTÉ AFFILIÉS ULTÉRIEUREMENT À UN AUTRE RÉGIME - CONSÉQUENCE - ILLÉGALITÉ [RJ1].
</SCT>
<ANA ID="9A"> 01-04-03-02 Si le principe d'égalité ne s'oppose pas à ce que des personnes affiliées à des régimes de sécurité sociale différents, lesquels forment chacun un ensemble dont les dispositions ne peuvent être envisagées isolément, soient soumises à des règles d'assiette, de liquidation et de taux différentes, il fait obstacle, eu égard à l'objet même de la mesure, à ce que les cotisations dues pour valider une même période de travail, au titre d'un même régime et qui n'avaient pu légalement donner lieu à cotisation, soient calculées en faisant application de taux différents selon que les intéressés ont poursuivi leur carrière professionnelle dans ce régime ou, au contraire, ont été affiliés ultérieurement à un autre régime. En l'espèce, la différenciation des taux prévue par l'article 71 du décret n° 55-753 du 31 mai 1955, inséré par le décret n° 2004-862 du 24 août 2004, introduit une discrimination illégale entre les assurés ayant contribué durant toute leur carrière au régime des assurances sociales agricoles et les assurés ayant cotisé dans le cadre d'un autre régime de sécurité sociale après leur période accomplie en qualité d'aide familial.</ANA>
<ANA ID="9B"> 03-02-06 Si le principe d'égalité ne s'oppose pas à ce que des personnes affiliées à des régimes de sécurité sociale différents, lesquels forment chacun un ensemble dont les dispositions ne peuvent être envisagées isolément, soient soumises à des règles d'assiette, de liquidation et de taux différentes, il fait obstacle, eu égard à l'objet même de la mesure, à ce que les cotisations dues pour valider une même période de travail, au titre d'un même régime et qui n'avaient pu légalement donner lieu à cotisation, soient calculées en faisant application de taux différents selon que les intéressés ont poursuivi leur carrière professionnelle dans ce régime ou, au contraire, ont été affiliés ultérieurement à un autre régime. En l'espèce, la différenciation des taux prévue par l'article 71 du décret n° 55-753 du 31 mai 1955, inséré par le décret n° 2004-862 du 24 août 2004, introduit une discrimination illégale entre les assurés ayant contribué durant toute leur carrière au régime des assurances sociales agricoles et les assurés ayant cotisé dans le cadre d'un autre régime de sécurité sociale après leur période accomplie en qualité d'aide familial.</ANA>
<ANA ID="9C"> 62-03-02-02 Si le principe d'égalité ne s'oppose pas à ce que des personnes affiliées à des régimes de sécurité sociale différents, lesquels forment chacun un ensemble dont les dispositions ne peuvent être envisagées isolément, soient soumises à des règles d'assiette, de liquidation et de taux différentes, il fait obstacle, eu égard à l'objet même de la mesure, à ce que les cotisations dues pour valider une même période de travail, au titre d'un même régime et qui n'avaient pu légalement donner lieu à cotisation, soient calculées en faisant application de taux différents selon que les intéressés ont poursuivi leur carrière professionnelle dans ce régime ou, au contraire, ont été affiliés ultérieurement à un autre régime. En l'espèce, la différenciation des taux prévue par l'article 71 du décret n° 55-753 du 31 mai 1955, inséré par le décret n° 2004-862 du 24 août 2004, introduit une discrimination illégale entre les assurés ayant contribué durant toute leur carrière au régime des assurances sociales agricoles et les assurés ayant cotisé dans le cadre d'un autre régime de sécurité sociale après leur période accomplie en qualité d'aide familial.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. 17 novembre 2000, Union de recouvrement des cotisations de sécurité sociale et d'allocations familiales (URSSAF) de la Haute-Garonne, n° 185772, p. 517 ; 6 septembre 2006, Union des familles en Europe, n° 277752, p. 393.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
