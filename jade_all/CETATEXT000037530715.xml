<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037530715</ID>
<ANCIEN_ID>JG_L_2018_10_000000404660</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/53/07/CETATEXT000037530715.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 24/10/2018, 404660</TITRE>
<DATE_DEC>2018-10-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404660</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:404660.20181024</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Les cliniques d'Ajaccio a porté plainte contre M. A...B...devant la chambre disciplinaire de première instance de Provence-Alpes-Côte d'Azur et Corse de l'ordre des médecins. Le conseil départemental de Corse du Sud de l'ordre des médecins s'est associé à la plainte. Par une décision n° 5162 du 30 janvier 2015, la chambre disciplinaire de première instance a infligé à M. B... la sanction d'interdiction d'exercer la médecine pendant six mois, dont trois mois assortis du sursis.<br/>
<br/>
              Par une décision n° 12674 du 4 octobre 2016, la chambre disciplinaire nationale de l'ordre des médecins a rejeté l'appel formé par M. B...contre cette décision.  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 25 octobre et 10 novembre 2016 et le 3 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre solidairement à la charge du conseil départemental de Corse du Sud de l'ordre des médecins et de la société Les cliniques d'Ajaccio la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes ; <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange, de la Burgade, avocat de M.B..., à la SCP Célice, Soltner, Texidor, Perier, avocat de la société Les cliniques d'Ajaccio et à la SCP Matuchansky, Poupot, Valdelièvre, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 12 octobre 2018, présentée par M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B..., chirurgien-orthopédiste, a, le 28 juin 2013, alors qu'il exerçait ses fonctions au sein de l'établissement " Les cliniques d'Ajaccio ", refusé au dernier moment de pratiquer une intervention programmée sur une patiente âgée de quatre-vingt deux ans, alors qu'il avait non seulement confirmé la tenue de cette intervention auprès de sa patiente et fait venir un confrère anesthésiste-réanimateur en le laissant procéder à une anesthésie générale mais aussi, sachant par avance qu'il ne pourrait disposer d'une aide opératoire pour cette opération, convoqué un huissier afin que celui-ci constate que cette absence d'aide opératoire l'empêchait de travailler dans des conditions satisfaisantes ; que, par une décision du 30 janvier 2015, la chambre disciplinaire de première instance de Provence-Alpes-Côte d'Azur et Corse de l'ordre des médecins a infligé à M. B...la sanction d'interdiction d'exercer la médecine pendant six mois, dont trois mois avec sursis ; que, par une décision du 4 octobre 2016 contre laquelle le praticien se pourvoit en cassation, la chambre disciplinaire nationale de l'ordre des médecins a rejeté l'appel formé par M. B...contre cette décision ; <br/>
<br/>
              2. Considérant que les juridictions disciplinaires de l'ordre des médecins, saisies d'une plainte contre un praticien, peuvent légalement connaître de l'ensemble du comportement professionnel de l'intéressé, sans se limiter aux faits dénoncés dans la plainte ni aux griefs articulés par le plaignant ; qu'à ce titre, la chambre disciplinaire nationale de l'ordre des médecins peut légalement se fonder, pour infliger une sanction à un médecin, sur des griefs nouveaux qui n'ont pas été dénoncés dans la plainte soumise à la chambre disciplinaire de première instance, à condition toutefois d'avoir mis au préalable l'intéressé à même de s'expliquer sur ces griefs ; qu'elle n'est, en revanche, pas tenue de communiquer préalablement aux parties le choix, qui lui incombe, de la qualification juridique des griefs au regard des dispositions du code de déontologie médicale ; <br/>
<br/>
              3. Considérant, en premier lieu, qu'aux termes de l'article R. 4127-2 du code de la santé publique : " Le médecin, au service de l'individu et de la santé publique, exerce sa mission dans le respect de la vie humaine, de la personne et de sa dignité (...) " ; qu'aux termes de l'article R. 4127-35 du même code : " Le médecin doit à la personne qu'il examine, qu'il soigne ou qu'il conseille une information loyale, claire et appropriée sur son état, les investigations et les soins qu'il lui propose. Tout au long de la maladie, il tient compte de la personnalité du patient dans ses explications et veille à leur compréhension (...) " ; qu'enfin, aux termes de l'article R. 4127-40 du même code : " Le médecin doit s'interdire, dans les investigations et interventions qu'il pratique comme dans les thérapeutiques qu'il prescrit, de faire courir au patient un risque injustifié " ;<br/>
<br/>
              4. Considérant qu'il ressort des termes mêmes de la décision attaquée que, pour infliger à M. B...la sanction litigieuse, la chambre disciplinaire nationale a, notamment, retenu que son attitude à l'égard de sa patiente âgée avait porté atteinte à la dignité de cette dernière, en violation des dispositions de l'article R. 4127-2 du code de la santé publique, lui avait fait courir un risque injustifié, en violation des dispositions de l'article R. 4127-40 du même code, et constituait un manquement à son obligation d'information loyale, prévue par les dispositions de l'article R. 4127-35 du même code ;  que M. B...soutient que la chambre disciplinaire nationale devait l'inviter à présenter préalablement sa défense sur cette troisième qualification juridique ;<br/>
<br/>
              5. Considérant, toutefois, qu'il ressort des pièces du dossier soumis aux juges du fond que la plainte présentée par la société Les cliniques d'Ajaccio contre M. B...en première instance comportait le grief, d'ailleurs retenu par la chambre disciplinaire de première instance, tiré de ce qu'il n'avait, en mettant ainsi en danger sa patiente, prévenu personne de son intention d'annuler l'opération programmée en cas d'absence, pourtant anticipée par lui, d'une aide opératoire ; que, M. B...ayant pu utilement présenter sa défense sur ce grief, la chambre disciplinaire nationale a pu, sans irrégularité, le retenir comme fondement de la sanction qu'elle a prononcée en le qualifiant non seulement, ainsi que l'avaient déjà fait les premiers juges, d'atteinte à la dignité de la patiente et de mise en danger injustifiée, méconnaissant les articles R. 4127-2 et R. 4127-40 du code de la santé publique, mais aussi de manquement à l'obligation d'information loyale de cette même patiente, méconnaissant les dispositions de l'article R. 4127-35 du même code ;<br/>
<br/>
              6. Considérant, en deuxième lieu, qu'aux termes de l'article R. 4127-56 du code de la santé publique : " Les médecins doivent entretenir entre eux des rapports de bonne confraternité (...) " ; qu'il ressort des termes mêmes de la décision attaquée que, pour infliger à M. B...la sanction litigieuse, la chambre disciplinaire nationale a notamment retenu que son attitude à l'égard du médecin anesthésiste-réanimateur, qu'il n'avait pas davantage mis au courant de son intention de ne pas procéder à l'opération, a méconnu l'obligation de confraternité ;<br/>
<br/>
              7. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le grief tiré de la faute disciplinaire commise, le jour de l'opération chirurgicale, par M. B... à l'égard de son confrère anesthésiste-réanimateur, s'il n'a pas été invoqué dans la plainte de la société Les cliniques d'Ajaccio soumise à la juridiction de première instance, a, en revanche, été ensuite relevé par la société, tant dans son mémoire en réplique devant la chambre disciplinaire régionale que dans son mémoire en défense devant le juge d'appel, mémoires qui ont été communiqués à M. B...dans le cadre de l'instruction contradictoire ; que M. B... a, par suite, été mis à même de s'expliquer utilement sur ce grief ; qu'il n'est, dès lors, pas fondé à soutenir qu'en ne procédant pas elle-même à la communication préalable de ce grief, la chambre disciplinaire nationale aurait entaché sa décision d'irrégularité ; <br/>
<br/>
              8. Considérant, en troisième lieu, qu'en estimant, d'une part, que M. B...avait, par son comportement, mis en danger sa patiente et l'avait utilisée pour servir ses intérêts personnels et, d'autre part, s'agissant de son comportement à l'égard des personnels d'assistance opératoire, qu'il avait été invité à plusieurs reprises à changer d'attitude à leur égard, la chambre disciplinaire nationale a porté, sur les pièces du dossier qui lui était soumis, une appréciation souveraine, exempte de dénaturation ;<br/>
<br/>
              9. Considérant, en quatrième lieu, qu'en jugeant que les comportements de M. B..., tant à l'égard de sa patiente et de son confrère anesthésiste-réanimateur qu'à l'égard des personnels d'assistance opératoire, revêtaient le caractère de fautes disciplinaires, la chambre disciplinaire nationale, qui a suffisamment motivé sa décision, a exactement qualifié les faits qui lui étaient soumis ;<br/>
<br/>
              10. Considérant, enfin, qu'eu égard à la gravité des comportements reprochés, la chambre disciplinaire nationale n'a, en rejetant la requête de M. B... et en confirmant ainsi la sanction d'interdiction d'exercer la médecine pendant six mois, dont trois mois avec sursis, prononcée par la chambre disciplinaire de première instance de Provence-Alpes-Côte d'Azur et Corse, pas retenu une sanction hors de proportion avec les fautes commises ; <br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui précède que M. B... n'est pas fondé à demander l'annulation de la décision qu'il attaque ; <br/>
<br/>
              12. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la société Les cliniques d'Ajaccio et du conseil départemental de l'ordre des médecins de Corse du Sud, qui ne sont pas, dans la présente instance, les parties perdantes ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B...une somme de 3 000 euros à verser à la société Les cliniques d'Ajaccio au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B...est rejeté. <br/>
Article 2 : M. B...versera à la société Les cliniques d'Ajaccio une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à M. A...B..., à la société Les cliniques d'Ajaccio et au conseil départemental de Corse du Sud de l'ordre des médecins. <br/>
Copie en sera adressée au Conseil national de l'ordre des médecins.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-04-01-03 PROFESSIONS, CHARGES ET OFFICES. DISCIPLINE PROFESSIONNELLE. PROCÉDURE DEVANT LES JURIDICTIONS ORDINALES. POUVOIRS DU JUGE DISCIPLINAIRE. - 1) PRINCIPES [RJ1] - OBLIGATION POUR LA JURIDICTION DISCIPLINAIRE DE SE LIMITER AUX FAITS DÉNONCÉS DANS LA PLAINTE OU AUX GRIEFS ARTICULÉS PAR LE PLAIGNANT - ABSENCE - FACULTÉ POUR LE JUGE D'APPEL DE SE FONDER SUR DES GRIEFS NOUVEAUX NON DÉNONCÉS DANS LA PLAINTE - EXISTENCE - CONDITIONS - 2) ESPÈCE - GRIEF NON COMMUNIQUÉ PAR LA JURIDICTION D'APPEL, MAIS RELEVÉ PAR LE PLAIGNANT DANS SES ÉCRITURES, COMMUNIQUÉES DANS LE CADRE DE L'INSTRUCTION - ABSENCE D'IRRÉGULARITÉ [RJ2].
</SCT>
<ANA ID="9A"> 55-04-01-03 1) Les juridictions disciplinaires de l'ordre des médecins, saisies d'une plainte contre un praticien, peuvent légalement connaître de l'ensemble du comportement professionnel de l'intéressé, sans se limiter aux faits dénoncés dans la plainte ni aux griefs articulés par le plaignant. A ce titre, la chambre disciplinaire nationale de l'ordre des médecins peut légalement se fonder, pour infliger une sanction à un médecin, sur des griefs nouveaux qui n'ont pas été dénoncés dans la plainte soumise à la chambre disciplinaire de première instance, à condition toutefois d'avoir mis au préalable l'intéressé à même de s'expliquer sur ces griefs. Elle n'est, en revanche, pas tenue de communiquer préalablement aux parties le choix, qui lui incombe, de la qualification juridique des griefs au regard des dispositions du code de déontologie médicale.,,2) Grief non invoqué dans la plainte soumise à la juridiction de première instance, mais relevé ensuite par le plaignant, tant dans son mémoire en réplique devant cette juridiction que dans son mémoire en défense devant la juridiction d'appel, mémoires qui ont été communiqués dans le cadre de l'instruction contradictoire. Le praticien ayant été mis à même de s'expliquer utilement sur ce grief, il n'est pas fondé à soutenir qu'en ne procédant pas elle-même à la communication préalable de ce grief, la juridiction d'appel aurait entaché sa décision d'irrégularité.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., en précisant, CE, 15 décembre 2010, M.,n° 329246, T. pp. 833-961 ; CE, 10 juillet 2017, M.,et autre, n° 385419, T. p. 786.,,[RJ2] Cf., sol. contr., CE, décision du même jour, Mme,, n° 405018, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
