<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025115881</ID>
<ANCIEN_ID>JG_L_2011_12_000000347491</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/11/58/CETATEXT000025115881.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 23/12/2011, 347491, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2011-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>347491</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD</AVOCATS>
<RAPPORTEUR>Mme Suzanne von Coester</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:347491.20111223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 14 mars et 14 avril 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Etienne I, domicilié ... ; M. I demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 1001676 du 11 février 2011 par lequel le tribunal administratif de Besançon a annulé l'élection des représentants de la commune de Belfort au conseil communautaire de l'agglomération belfortaine lors des opérations qui se sont déroulées le 9 décembre 2010 ;<br/>
<br/>
              2°) de rejeter la protestation de M. J contre ces opérations électorales ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 25 novembre 2011, présentée pour M. J ;<br/>
<br/>
              Vu le code électoral ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Suzanne von Coester, Maître des Requêtes,<br/>
<br/>
              - les observations de la SCP Potier de la Varde, Buk Lament, avocat de M. Etienne I et de la SCP Barthélemy, Matuchansky, Vexliard, avocat de M. Leouahdi Sélim J,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Potier de la Varde, Buk Lament, avocat de M. Etienne I et à la SCP Barthélemy, Matuchansky, Vexliard, avocat de M. Leouahdi Sélim J ;<br/>
<br/>
<br/>
<br/>Considérant que, par une délibération du 9 décembre 2010, le conseil municipal de Belfort a décidé de procéder à la désignation des 32 représentants de la commune au sein de la communauté d'agglomération belfortaine ; qu'à l'issue du scrutin qui s'est déroulé dans la nuit du 9 au 10 décembre 2010, seul M. J, qui occupait les fonctions de vice-président de la communauté d'agglomération en charge de la gestion des déchets, n'a pas été réélu ; que, sur requête de M. J, le tribunal administratif de Besançon a, par un jugement du 11 février 2011, annulé ces opérations électorales ; que M. I, qui a été élu à l'issue de ces opérations, a intérêt à faire appel de ce jugement ;<br/>
<br/>
              Considérant qu'aux termes de l'article L. 2121-33 du code général des collectivités territoriales : " Le conseil municipal procède à la désignation de ses membres ou de délégués pour siéger au sein d'organismes extérieurs dans les cas et conditions prévus par les dispositions du présent code et des textes régissant ces organismes. La fixation par les dispositions précitées de la durée des fonctions assignées à ces membres ou délégués ne fait pas obstacle à ce qu'il puisse être procédé à tout moment, et pour le reste de cette durée, à leur remplacement par une nouvelle désignation opérée dans les mêmes formes " ; qu'en vertu de ces dispositions, le conseil municipal dispose d'un large pouvoir d'appréciation pour décider de procéder à de nouvelles désignations de ses délégués dans un organisme extérieur, sous le contrôle du juge de l'élection ; que ce contrôle ne saurait porter que sur la légalité des motifs allégués, et non sur leur bien-fondé ;<br/>
<br/>
              Considérant qu'il résulte de l'instruction que le conseil municipal de Belfort a procédé, sur le fondement de ces dispositions, à une nouvelle élection de ses représentants au sein de la communauté d'agglomération belfortaine en raison de l'évolution de l'équilibre des différentes composantes politiques au sein du conseil municipal, résultant notamment de l'adhésion de M. J, vice-président de la communauté d'agglomération, à la composante minoritaire de la majorité municipale ; que ce motif politique, dès lors qu'il était établi, justifiait légalement qu'il soit procédé à une nouvelle désignation des représentants de la commune au sein d'un organisme extérieur ; qu'en cherchant, après l'avoir regardé comme établi, à en apprécier la portée et le bien-fondé, le tribunal administratif de Besançon a commis une erreur de droit ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que c'est à tort que, pour annuler les opérations électorales du 9 décembre 2010, le tribunal administratif de Besançon s'est fondé sur ce qu'aucun des motifs avancés n'était de nature à justifier qu'il soit procédé à une nouvelle élection des représentants de la commune à la communauté d'agglomération ;<br/>
<br/>
              Considérant toutefois qu'il appartient au Conseil d'Etat, saisi par l'effet dévolutif de l'appel, d'examiner les autres griefs soulevés par M. J dans sa protestation devant le tribunal ;<br/>
<br/>
              Considérant qu'en vertu des dispositions de l'article L. 2121-33 précité, le conseil municipal est compétent pour procéder au remplacement de ses membres désignés pour siéger au sein d'organismes extérieurs, même en l'absence de déclaration de vacance de poste préalable ;<br/>
<br/>
              Considérant que M. J n'est pas fondé à soutenir que la procédure de désignation devait être précédée de déclarations de candidature ;<br/>
<br/>
              Considérant qu'aucune disposition non plus qu'aucun principe n'interdit que le vote conduise à une nouvelle désignation de représentants antérieurement désignés ;<br/>
<br/>
              Considérant qu'il résulte de l'instruction que le scrutin qui s'est déroulé dans la nuit du 9 au 10 décembre 2010 a porté sur la désignation de chacun des 32 représentants de la commune de Belfort à la communauté d'agglomération belfortaine, au terme de scrutins uninominaux majoritaires à un tour ; que, dès lors, le grief tiré de l'illégalité du recours à un scrutin de liste manque en fait ;<br/>
<br/>
              Considérant qu'aucun texte ni aucun principe général n'interdit l'usage de bulletins manuscrits lors d'opérations électorales telles que celles organisées en l'espèce ; que M. J n'allègue pas que des bulletins manuscrits utilisés pour ces élections aient comporté des signes de reconnaissance ;<br/>
<br/>
              Considérant, enfin, que le détournement de procédure allégué n'est pas établi ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que M. I est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Besançon a annulé l'élection des représentants de la commune de Belfort à la communauté d'agglomération belfortaine organisée le 9 décembre 2010 ; que, par suite, la protestation de M. J, y compris ses conclusions à fin d'injonction et autres conclusions accessoires, ne peut qu'être rejetée ;<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. I, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
              				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Besançon du 11 février 2011 est annulé.<br/>
<br/>
Article 2 : Les opérations électorales qui se sont déroulées le 9 décembre 2010 au sein du conseil municipal de Belfort sont validées.<br/>
<br/>
		Article 3 : La protestation de M. J est rejetée.<br/>
Article 4 : Les conclusions de M. J présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. Etienne I, à M. Leouahdi Sélim J, à Mme Marie-Claude N, à M. Bruno AD, à M. Christian W, à M. Maurice F, à Mme Marie-Antoinette K, à M. Emile V, à M. Pascal O, à Mme Isabelle X, à M. Jacques M, à M. Azzedine U, à Mme Anny A, à M. Olivier P, à Mme Francine L, à Mme Samia AC, à M. Hubert AF, à Mme Marie-Laure D, à M. Alain AE, à Mme Jacqueline Y, à M. Robert AB, à Mme Latifa G, à M. Bertrand E, à Mme Marie-Christine AA, à Mme Michèle Alice T, à M. Denis C, à Mme Sylvie B, à M. Pascal S, à Mme Myriam AG, à M. Gérard Z, à M. Jean-Marie R, à Mme Armelle Q, à Mme Céline H et à la commune de Belfort.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
