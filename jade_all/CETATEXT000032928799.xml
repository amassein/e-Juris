<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032928799</ID>
<ANCIEN_ID>JG_L_2016_07_000000380854</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/92/87/CETATEXT000032928799.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 22/07/2016, 380854, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380854</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR</AVOCATS>
<RAPPORTEUR>M. Julien Anfruns</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:380854.20160722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 27 octobre 2015, le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi de M. et Mme C...A...dirigées contre l'arrêt n° 13PA00155 du 27 mars 2014 de la cour administrative d'appel de Paris en tant qu'il se rapporte aux conclusions tendant à la décharge des pénalités pour activité occulte mises à leur charge.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Julien Anfruns, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, Texidor, avocat de M. et Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme B... A..., néeD..., a cédé le 30 décembre 2005 à la société G.D. International, qui fabrique et commercialise des articles de parfumerie et des cosmétiques, le fonds de commerce de la société Gisèle D...et conclu le même jour avec la société acquéreuse une convention d'assistance en vertu de laquelle elle devait la faire profiter de son expertise et percevoir en contrepartie une rémunération mensuelle d'un montant hors taxes de 5 000 euros. L'administration fiscale a estimé qu'elle avait exercé durant cette période une activité indépendante de consultant de manière occulte, dès lors qu'elle n'avait pas souscrit de déclarations de bénéfices non commerciaux ou de taxe sur la valeur ajoutée et qu'elle ne s'était pas fait connaître d'un centre de formalité des entreprises. Elle a par conséquent évalué d'office ses bénéfices non commerciaux au titre des années 2006 à 2008 puis majoré les impositions supplémentaires en résultant de la pénalité prévue par les dispositions du c) du 1 de l'article 1728 du code général des impôts en cas de découverte d'une activité occulte. M. et Mme A...se pourvoient en cassation contre l'arrêt du 27 mars 2014 par lequel la cour administrative d'appel de Paris, après avoir annulé le jugement du tribunal administratif de Paris du 18 décembre 2012 rejetant leur demande de décharge de ces impositions supplémentaires et pénalités, a rejeté cette demande. Seules les conclusions de leur pourvoi dirigées contre cet arrêt en tant qu'il s'est prononcé sur les pénalités pour activité occulte mises à leur charge ont été admises par la décision du Conseil d'Etat statuant au contentieux du 27 octobre 2015.<br/>
<br/>
              2. Aux termes de l'article 1728 du code général des impôts, dans sa rédaction alors applicable : " 1. Le défaut de production dans les délais prescrits d'une déclaration ou d'un acte comportant l'indication d'éléments à retenir pour l'assiette ou la liquidation de l'impôt entraîne l'application, sur le montant des droits mis à la charge du contribuable ou résultant de la déclaration ou de l'acte déposé tardivement, d'une majoration de : / (...) c. 80 % en cas de découverte d'une activité occulte. (...) ". Il résulte de ces dispositions, éclairées par les travaux parlementaires qui ont précédé l'adoption de la loi dont elles sont issues, que dans le cas où un contribuable n'a ni déposé dans le délai légal les déclarations qu'il était tenu de souscrire, ni fait connaître son activité à un centre de formalités des entreprises ou au greffe du tribunal de commerce, l'administration doit être réputée apporter la preuve, qui lui incombe, de l'exercice occulte de l'activité professionnelle si le contribuable n'est pas lui même en mesure d'établir qu'il a commis une erreur justifiant qu'il ne se soit acquitté d'aucune de ces obligations déclaratives.<br/>
<br/>
              3. Devant la cour, M. et Mme A...demandaient la décharge notamment des pénalités pour activité occulte dont ont été assorties les impositions supplémentaires mises à leur charge, en soutenant que l'activité de Mme A...ne pouvait être qualifiée d'occulte dès lors que le contrat qu'elle avait conclu avec la société G. D. International avait été transféré, par deux avenants des 30 janvier 2006 et 25 janvier 2007, aux sociétés D...puis Holos. La cour, qui s'est bornée, dans le dispositif de son arrêt, après avoir annulé le jugement du tribunal administratif de Paris du 18 décembre 2012, à rejeter les conclusions des requérants tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu auxquelles ils ont été assujettis au titre des années 2006 à 2008, a omis de statuer sur leurs conclusions tendant à la décharge des pénalités pour activité occulte.<br/>
<br/>
              4. Il résulte de ce qui précède que M. et Mme A...sont fondés à demander l'annulation de l'article 3 de l'arrêt qu'ils attaquent en tant qu'il a omis de se prononcer sur les conclusions tendant à la décharge des pénalités pour activité occulte mises à leur charge. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement d'une somme de 3 500 euros à M. et Mme A...au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 3 de l'arrêt de la cour administrative d'appel de Paris du 27 mars 2014 est annulé en tant qu'il a omis de se prononcer sur les conclusions tendant à la décharge des pénalités pour activité occulte mises à la charge de M. et MmeA....<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Paris.<br/>
Article 3 : L'Etat versera à M. et Mme A...une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. et Mme A...et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
