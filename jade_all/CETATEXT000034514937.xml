<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034514937</ID>
<ANCIEN_ID>JG_L_2017_04_000000392521</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/51/49/CETATEXT000034514937.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 28/04/2017, 392521, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-04-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392521</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:392521.20170428</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La SA SC2N a demandé au tribunal administratif de Montreuil de prononcer la réduction de la cotisation de taxe professionnelle à laquelle elle a été assujettie au titre de l'année 2009 dans les rôles de la commune de Mondeville (Calvados). Par un jugement no 1207105 du 18 novembre 2013, le tribunal administratif de Montreuil a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14VE00306 du 23 juin 2015, la cour administrative d'appel de Versailles a, sur appel de la SA SC2N, annulé ce jugement et fait droit à sa demande.<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 10 août 2015 et 26 avril 2016 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'économie et des finances demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 2004-1484 du 30 décembre 2004 ;<br/>
              - le décret n° 2007-1888 du 26 décembre 2007 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bastien Lignereux, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la SA SC2N ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 1647 C sexies du code général des impôts, dans sa rédaction applicable à l'imposition en litige : " I. - Les redevables de la taxe professionnelle et les établissements temporairement exonérés de cet impôt (...) peuvent bénéficier d'un crédit d'impôt, pris en charge par l'Etat et égal à 1 000 euros par salarié employé depuis au moins un an au 1er janvier de l'année d'imposition dans un établissement affecté à une activité mentionnée au premier alinéa de l'article 1465 et situé dans une zone d'emploi reconnue en grande difficulté au regard des délocalisations au titre de la même année. / (...) / IV. Le crédit d'impôt s'applique (...) dans les limites prévues par le règlement (CE) n° 69/2001 de la Commission du 12 janvier 2001 concernant l'application des articles 87 et 88 du traité CE aux aides de minimis. / N'ouvrent pas droit au crédit d'impôt les emplois situés dans les établissements où est exercée à titre principal une activité relevant de l'un des secteurs suivants, définis selon la nomenclature d'activités française de l'Institut national de la statistique et des études économiques : construction automobile, construction navale, fabrication de fibres artificielles ou synthétiques et sidérurgie. (...) ". Il ressort de ces dispositions, éclairées par les travaux préparatoires de la loi du 30 décembre 2004 de finances pour 2005, dont elles sont issues, que les secteurs d'activité exclus du bénéfice du crédit d'impôt qu'elles prévoient l'ont été pour respecter les règles édictées par la Commission européenne en matière d'aides d'Etat.<br/>
<br/>
              2. La communication de la Commission européenne intitulée " Encadrement communautaire des aides d'Etat dans le secteur automobile (97/C 279/01) " publiée au Journal officiel des Communautés européennes C 279 du 15 septembre 1997, applicable lors de l'année d'imposition en litige, définit le " secteur automobile " comme incluant " le développement, la fabrication et le montage de " véhicules automobiles ", de " moteurs " pour véhicules automobiles et de " modules ou sous-systèmes " pour ces véhicules ou moteurs, directement par un constructeur ou par un " équipementier de premier rang ", et, dans ce dernier cas uniquement, dans le cadre d'un " projet global " ". <br/>
<br/>
              3. Pour juger que l'établissement de la société SC2N situé à Mondeville (Calvados), pour lequel elle avait demandé en vain le bénéfice du crédit d'impôt prévu par l'article 1647 C sexies du code général des impôts, n'était pas au nombre de ceux où est exercée à titre principal une activité de construction automobile telle que définie par la nomenclature des activités françaises de l'INSEE, la cour administrative d'appel de Versailles a jugé que le secteur de " la construction automobile " correspondait au groupe 29-1 " construction de véhicules automobiles " de cette nomenclature, alors que la division 29 intitulée " industrie automobile " comprend également le groupe 29-2 " carrosseries automobiles remorques et semi-remorques " et le groupe 29-3 intitulé " équipements automobiles ". Il résulte de ce qui est dit aux points 1 et 2 ci-dessus que la cour, en limitant ainsi la notion de construction automobile, au sens des dispositions du IV de l'article 1647 C sexies du code général des impôts, à la " construction de véhicules automobiles ", alors que l'encadrement édicté par la Commission européenne en matière d'aides d'Etat ne se limite pas à ce secteur d'activité, a commis une erreur de droit. <br/>
<br/>
              4. Il résulte de ce qui précède que le ministre est fondé à demander l'annulation de l'arrêt qu'il attaque. Dès lors, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 23 juin 2015 de la cour administrative d'appel de Versailles est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : Les conclusions présentées par la société SC2N sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée au ministre de l'économie et des finances et à la société SC2N.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
