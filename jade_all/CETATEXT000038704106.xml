<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038704106</ID>
<ANCIEN_ID>JG_L_2019_06_000000423623</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/70/41/CETATEXT000038704106.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 28/06/2019, 423623, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-06-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423623</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Louise Bréhier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:423623.20190628</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 27 août 2018 et 20 février 2019 au secrétariat du contentieux du Conseil d'Etat, le syndicat CGT du ministère des affaires étrangères demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la note du 7 avril 2017 de la direction générale de l'administration et de la fonction publique relative à l'accès des agents recrutés sur des contrats soumis au droit local aux concours internes du ministère des affaires étrangères et du développement international ;  <br/>
<br/>
              2°) d'annuler la décision implicite de rejet née du silence gardé par le ministre de l'action et des comptes publics sur la demande d'abrogation de cette note ; <br/>
<br/>
              3°) d'annuler la décision implicite de rejet née du silence gardé par le ministre des affaires étrangères et du développement international sur sa demande d'abrogation des dispositions de l'avis de concours d'adjoint administratif principal de 2ème classe de chancellerie, relatives aux services effectués en qualité d'agent de droit local, publié le 11 septembre 2017 sur le site de ce ministère ; <br/>
<br/>
              4°) d'enjoindre au ministre de l'action et des comptes publics d'abroger la note du 7 avril 2017 et au ministre de l'Europe et des affaires étrangères d'abroger les dispositions relatives aux services effectués en qualité d'agent de droit local parues dans l'avis de concours d'adjoint administratif principal de 2ème classe de chancellerie, publié le 11 septembre 2017 sur le site du ministère, dans un délai d'un mois à compter de la décision à intervenir et sous astreinte de 500 euros par jour de retard ; <br/>
<br/>
              5°) de mettre à la charge de l'Etat une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 84-16 du 11 janvier 1984 ; <br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - le décret n° 2016-580 du 11 mai 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Louise Bréhier, auditrice, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat du syndicat CGT du ministère des affaires étrangères ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Par deux courriers du 27 avril 2018, le syndicat CGT du ministère des affaires étrangères a demandé au ministre de l'action et des comptes publics d'abroger une note du 7 avril 2017 relative à l'accès des agents recrutés sur des contrats soumis au droit local aux concours internes du ministère des affaires étrangères et du développement international et au ministre des affaires étrangères et du développement international d'abroger certaines énonciations de l'avis de concours d'adjoint administratif principal de 2ème classe de chancellerie publié le 11 septembre 2017. Le syndicat CGT du ministère des affaires étrangères demande l'annulation pour excès de pouvoir de la note du 7 avril 2017 et des décisions nées du silence gardé par ces autorités sur ces demandes. <br/>
<br/>
              2.	D'une part, l'article 4 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat dispose que : " Par dérogation au principe énoncé à l'article 3 du titre Ier du statut général, des agents contractuels peuvent être recrutés dans les cas suivants : (...) 2° Pour les emplois du niveau de la catégorie A et, dans les représentations de l'Etat à l'étranger, des autres catégories, lorsque la nature des fonctions ou les besoins des services le justifient. ". Le V de l'article 34 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations dispose que : " Lorsque les nécessités du service le justifient, les services de l'Etat à l'étranger peuvent, dans le respect des conventions internationales du travail, faire appel à des personnels contractuels recrutés sur place, sur des contrats de travail soumis au droit local, pour exercer des fonctions concourant au fonctionnement desdits services. ". D'autre part, selon l'article 19 de la loi du 11 janvier 1984 citée ci-dessus : " Les fonctionnaires sont recrutés par voie de concours organisés suivant l'une au moins des modalités ci-après : (...) 2° Des concours réservés aux fonctionnaires de l'Etat, aux militaires et, dans les conditions prévues par les statuts particuliers, aux agents de l'Etat, aux agents permanents de droit public relevant de l'Etat ou des circonscriptions territoriales exerçant leurs fonctions sur le territoire des îles Wallis et Futuna (...). Les candidats à ces concours devront avoir accompli une certaine durée de services publics et, le cas échéant, reçu une certaine formation. (...) ". Sur le fondement de ces dispositions, le III de l'article 3-6 du décret du 11 mai 2016 relatif à l'organisation des carrières des fonctionnaires de catégorie C de la fonction publique de l'Etat a prévu  que les concours internes d'accès à ces corps : " sont ouverts aux fonctionnaires et agents contractuels de la fonction publique de l'Etat, de la fonction publique territoriale et de la fonction publique hospitalière, aux militaires, ainsi qu'aux agents en fonction dans une organisation internationale intergouvernementale comptant au moins un an de services publics au 1er janvier de l'année au titre de laquelle ces concours sont organisés (...) ". <br/>
<br/>
              3.	Les actes contestés indiquent d'une part que les services requis pour pouvoir se présenter au concours d'adjoint administratif principal de 2ème classe de chancellerie, soumis aux dispositions statutaires du décret du 11 mai 2016 citées ci-dessus, sont des services accomplis en qualité d'agent public et, d'autre part, que ne peuvent être regardés comme tels les services accomplis, par les personnes recrutées, sur le fondement des dispositions des articles 4 de la loi du 11 janvier 1984 et 34 de la loi du 12 avril 2000, en qualité d'agent de droit local. <br/>
<br/>
              4.	Il résulte des dispositions du V de l'article 34 de la loi du 12 avril 2000 que les agents de droit local sont recrutés par les services de l'Etat à l'étranger sur des contrats de travail soumis au droit local. Ces contrats, n'étant en aucune façon régis par le droit français, ne sauraient être regardés comme des contrats de droit public. Dès lors, les années accomplies comme agent de droit local par les titulaires de ces contrats ne peuvent être prises en compte au titre de la durée des services publics exigée par les dispositions de l'article 19 de la loi du 11 janvier 1984 et, par suite, de l'article 3-6 du décret du 11 mai 2016, les candidats devant, pour remplir la condition de services publics exigée par ces dispositions, avoir servi pendant la durée requise en qualité d'agent de droit public. <br/>
<br/>
              5.	Par suite, les dispositions litigieuses se bornant à rappeler les règles mentionnées par les dispositions législatives et règlementaires citées au point 2, les moyens tirés de ce que ces dispositions seraient entachées d'incompétence, méconnaîtraient les dispositions de l'article 19 de la loi du 11 janvier 1984, les dispositions combinées de l'article 4-2 de la loi du 11 janvier 1984, du décret du 18 juin 1969 et de l'arrêté du 18 juin 1969 du ministre des affaires étrangères et européennes, du ministre du budget, des comptes publics et de la fonction publique et du secrétaire d'Etat chargé de la fonction publique, le principe d'égalité devant la loi et le principe d'égal accès aux emplois publics posé par l'article 6 de la Déclaration des droits de l'homme et du citoyen à laquelle renvoie le Préambule de la Constitution ne peuvent qu'être écartés. <br/>
<br/>
              6.	Il résulte de tout ce qui précède que, sans qu'il soit besoin de statuer sur les fins de non-recevoir opposées par le ministre de l'action et des comptes publics et le ministre de l'Europe et des affaires étrangères, le syndicat CGT du ministère des affaires étrangères n'est pas fondé à demander l'annulation pour excès de pouvoir de la note du 7 avril 2017, de la décision née du silence gardé par le ministre de l'action et des comptes publics sur sa demande d'abrogation de la note du 7 avril 2017 ni de la décision née du silence gardé par le ministre de l'Europe et des affaires étrangères sur sa demande d'abrogation des énonciations de l'avis de concours, qu'il attaque. Ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative ne peuvent par suite qu'être rejetées. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête du syndicat CGT des affaires étrangères est rejetée. <br/>
<br/>
Article 2 : La présente décision sera notifiée au syndicat CGT du ministère des affaires étrangères, au ministre de l'action et des comptes publics et au ministre des affaires étrangères. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
