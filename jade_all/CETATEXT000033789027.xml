<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033789027</ID>
<ANCIEN_ID>JG_L_2016_12_000000397638</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/78/90/CETATEXT000033789027.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 30/12/2016, 397638</TITRE>
<DATE_DEC>2016-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397638</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:397638.20161230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Nice d'annuler la décision du 26 septembre 2014 par laquelle le ministre de l'intérieur a constaté l'invalidité de son permis de conduire pour défaut de points et d'enjoindre au ministre de l'intérieur de lui restituer son permis sous astreinte de 150 euros par jour de retard. Par un jugement n°1501611 du 22 décembre 2015, le tribunal administratif a annulé la décision du 26 septembre 2014 et rejeté le surplus des conclusions.<br/>
<br/>
              Par un pourvoi, enregistré le 4 mars 2016 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M. B....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la directive 2006/126/CE du Parlement européen et du Conseil du 20 décembre 2006 relative au permis de conduire ;<br/>
<br/>
              - le code de la route ;<br/>
<br/>
              - l'arrêté du 8 février 1999 du ministre de l'équipement, des transports et du logement ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de MadameC..., maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, le 27 décembre 2012, M. B...a obtenu l'échange de son permis de conduire français, qui avait perdu une partie de ses points, contre un permis bulgare ; que, le 2 mars 2013, il s'est rendu coupable de conduite en état d'ivresse ; que le préfet des Alpes-Maritimes a pris le 5 mars suivant un arrêté portant interdiction administrative de conduire en France pour une durée de quatre mois ; que, par un jugement du 23 octobre 2013, le tribunal de grande instance de Grasse a condamné l'intéressé à trois mois d'emprisonnement avec sursis et 400 euros d'amende et prononcé à titre de peine complémentaire une interdiction de conduire de même durée que l'interdiction administrative ; qu'à la suite de ce jugement établissant la réalité de l'infraction, le ministre de l'intérieur a retiré sur le permis français de M. B... le nombre de points prévu par le code de la route et constaté, par une décision du 26 septembre 2014, la perte de validité de ce permis du fait d'un solde de points nul ; que le ministre se pourvoit en cassation contre le jugement du 22 décembre 2015 par lequel le tribunal administratif de Nice a annulé cette décision au motif que, si l'infraction commise obligeait l'intéressé, sous peine d'amende, à demander l'échange de son permis bulgare contre un permis français, l'administration n'avait pu légalement procéder d'office à cet échange pour constater ensuite la perte de validité du permis français ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 222-1 du code de la route dans sa rédaction applicable à la date de la décision litigieuse :  " Tout permis de conduire national délivré à une personne ayant sa résidence normale en France par un Etat membre de la Communauté européenne ou d'un autre Etat partie à l'accord sur l'Espace économique européen, en cours de validité dans cet Etat, est reconnu en France sous réserve que son titulaire satisfasse aux conditions définies par arrêté du ministre chargé des transports, après avis du ministre de l'intérieur et du ministre chargé des affaires étrangères. Ces conditions sont relatives à la durée de validité, au contrôle médical, aux mentions indispensables à la gestion du permis de conduire ainsi qu'aux mesures restrictives qui affectent ce permis (...) " ; qu'aux termes de l'article R. 222-2 du même code : " Toute personne ayant sa résidence normale en France, titulaire d'un permis de conduire national délivré par un Etat membre de la Communauté européenne ou d'un autre Etat partie à l'accord sur l'Espace économique européen, en cours de validité dans cet Etat, peut, sans qu'elle soit tenue de subir les examens prévus au premier alinéa de l'article D. 221-3, l'échanger contre le permis de conduire français selon les modalités définies par arrêté du ministre chargé des transports, pris après avis du ministre de la justice, du ministre de l'intérieur et du ministre chargé des affaires étrangères./ L'échange d'un tel permis de conduire contre le permis français est obligatoire lorsque son titulaire a commis, sur le territoire français, une infraction au présent code ayant entraîné une mesure de restriction, de suspension, de retrait du droit de conduire ou de retrait de points. Cet échange doit être effectué selon les modalités définies par l'arrêté prévu à l'alinéa précédent, aux fins d'appliquer les mesures précitées./ Le fait de ne pas effectuer l'échange de son permis de conduire dans le cas prévu à l'alinéa précédent est puni de l'amende prévue pour les contraventions de la quatrième classe " ; qu'aux termes de l'article 4 de l'arrêté du 8 février 1999 fixant les conditions de reconnaissance et d'échange des permis de conduire délivrés par les Etats appartenant à l'Union européenne et à l'Espace économique européen  : " 4.1. Les titulaires d'un permis de conduire obtenu dans un Etat appartenant à l'Union européenne ou à l'Espace économique européen, ayant fixé leur résidence normale sur le territoire français, peuvent demander l'échange de leur permis de conduire contre un permis français équivalent. (...) / 4.2. L'échange d'un tel permis contre un permis de conduire français est obligatoirement effectué si le conducteur a commis, sur le territoire français, une infraction ayant entraîné une mesure de restriction, de suspension, de retrait, d'annulation du droit de conduire, de retrait de points (...) " ;<br/>
<br/>
              3. Considérant qu'il résulte de la combinaison de ces dispositions que si le titulaire d'un permis de conduire délivré par l'un des Etats membres de l'Union européenne ou de l'Espace économique européen n'est, en principe, pas tenu de procéder à l'échange de ce permis pour conduire en France, cet échange devient en revanche obligatoire si, ayant sa résidence normale en France, il a commis sur le territoire national une infraction ayant entraîné une mesure de restriction, de suspension, de retrait ou d'annulation du droit de conduire ou de retrait de points ; que lorsque le titulaire d'un tel permis n'a pas procédé à l'échange auquel il était tenu, l'administration est fondée à le regarder comme étant exclusivement titulaire d'un permis français et à appliquer sur ce permis les mesures qu'appelle l'infraction commise et, le cas échéant, les mesures ultérieurement applicables ; que sont dépourvues d'incidence à cet égard les dispositions du dernier alinéa de l'article R. 222-2 du code de la route selon lesquelles le conducteur qui, en pareille hypothèse, n'effectue pas l'échange de son permis s'expose à une amende ; qu'il suit de là qu'en annulant la décision du ministre de l'intérieur constatant la perte de validité du permis français de M. B...au motif que ce dernier n'avait pas effectué l'échange de son permis bulgare, le tribunal administratif de Nice a commis une erreur de droit ; que son jugement doit, dès lors, être annulé ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Nice du 22 décembre 2015 est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée au tribunal administratif de Nice.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à M. A... B....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">49-04-01-04 POLICE. POLICE GÉNÉRALE. CIRCULATION ET STATIONNEMENT. PERMIS DE CONDUIRE. - ECHANGE D'UN PERMIS DE CONDUIRE ÉTRANGER CONTRE UN PERMIS DE CONDUIRE FRANÇAIS - CAS OÙ LE TITULAIRE RÉSIDE EN FRANCE ET COMMET UNE INFRACTION AU CODE DE LA ROUTE ALORS QU'IL N'A PAS PROCÉDÉ À L'ÉCHANGE.
</SCT>
<ANA ID="9A"> 49-04-01-04 Il résulte de la combinaison des articles R. 222-1 et R. 222-2 du code de la route et de l'arrêté du 8 février 1999 fixant les conditions de reconnaissance et d'échange des permis de conduire délivrés par les Etats appartenant à l'Union européenne et à l'Espace économique européen que si le titulaire d'un permis de conduire délivré par l'un des ces pays n'est, en principe, pas tenu de procéder à l'échange de ce permis pour conduire en France, cet échange devient en revanche obligatoire si, ayant sa résidence normale en France, il a commis sur le territoire national une infraction ayant entraîné une mesure de restriction, de suspension, de retrait ou d'annulation du droit de conduire ou de retrait de points. Lorsque que le titulaire d'un tel permis n'a pas procédé à l'échange auquel il était tenu, l'administration est fondée à le regarder comme étant exclusivement titulaire d'un permis français et à appliquer sur ce permis les mesures qu'appelle l'infraction commise et, le cas échéant, des mesures ultérieurement applicables. Sont dépourvues d'incidence à cet égard les dispositions du dernier alinéa de l'article R. 222-2 du code de la route selon lesquelles le conducteur qui, en pareille hypothèse, n'effectue pas l'échange de son permis s'expose à une amende.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
