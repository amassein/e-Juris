<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029288213</ID>
<ANCIEN_ID>JG_L_2014_07_000000354365</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/28/82/CETATEXT000029288213.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 23/07/2014, 354365, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-07-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354365</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>Mme Marie Grosset</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:354365.20140723</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 28 novembre 2011 et 28 février 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société d'éditions et de protection route, dont le siège est 3, rue de Liège à Paris Cedex 09 (75441) ; la société demande au Conseil d'Etat :<br/>
<br/>
                1°) d'annuler l'arrêt n° 10PA03557 du 22 septembre 2011 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant à l'annulation du jugement n° 0720707 du 5 mai 2010 par lequel le tribunal administratif de Paris a rejeté sa demande tendant à la condamnation de l'Etat à lui verser la somme de 920 606 euros, en réparation du préjudice subi à raison des dispositions de l'article L. 321-1-3 du code du travail dans leur rédaction issue de la loi n° 93-1312 du 20 décembre 1993 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
               3°) de mettre à la charge de l'Etat le versement de la somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              Vu le traité sur l'Union européenne ;<br/>
<br/>
              Vu la directive 75/129/CEE du Conseil du 17 février 1975, modifiée par la directive 92/56/CEE du Conseil du 24 juin 1992 ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu la loi n° 92-722 du 29 juillet 1992 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Grosset, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat de la société d'éditions et de protection route ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que  la société d'éditions et de protection route (SEPR), condamnée par les juridictions judiciaires au versement de sommes s'élevant à un total de 920 606 euros au profit de certains de ses salariés à la suite de l'annulation de plusieurs procédures de licenciement, a demandé la condamnation de l'Etat, d'une part, sur le fondement de sa responsabilité sans faute du fait des lois et, d'autre part, sur le fondement de la méconnaissance par la France de ses engagements internationaux, au motif que les condamnations prononcées à son encontre trouvaient leur origine dans le manque de clarté de l'article L. 321-1-3 du code du travail alors en vigueur ; que la cour administrative d'appel de Paris a, comme le tribunal administratif de Paris, rejeté les conclusions de la société requérante sur ces deux fondements ; <br/>
<br/>
              Sur l'arrêt attaqué, en tant qu'il statue sur la régularité du jugement :<br/>
<br/>
              2. Considérant qu'il ressort des termes de l'arrêt attaqué que la cour a répondu, par une motivation suffisante, aux moyens tirés d'une insuffisance de motivation du jugement du tribunal administratif ;<br/>
<br/>
              Sur l'arrêt attaqué, en tant qu'il statue sur la responsabilité sans faute du fait des lois :<br/>
<br/>
              3. Considérant que la responsabilité de l'Etat du fait des lois est susceptible d'être engagée, sur le fondement de l'égalité des citoyens devant les charges publiques, pour assurer la réparation de préjudices nés de l'adoption d'une loi, à la condition que cette loi n'ait pas exclu toute indemnisation et que le préjudice dont il est demandé réparation, revêtant un caractère grave et spécial, ne puisse, dès lors, être regardé comme une charge incombant normalement aux intéressés ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, quelle que soit la portée conférée par la Cour de cassation, par ses arrêts du 3 décembre 1996, aux dispositions de l'article L. 321-1-3 du code du travail issues de l'article 26 de la loi du 29 juillet 1992, en ce qui concerne notamment l'obligation d'établir un plan social, ces dispositions se sont appliquées à tous les employeurs envisageant, dans le cadre d'une restructuration, le licenciement de plus de dix salariés à la suite de leur refus d'une modification substantielle de leur contrat de travail ; que la cour administrative d'appel n'a pas dénaturé les faits de l'espèce en jugeant qu'au regard de ce texte, tous les employeurs étaient placés dans la même situation ; qu'elle en a exactement déduit, sans commettre d'erreur de droit et par un arrêt suffisamment motivé, que la société requérante n'était pas fondée, faute de pouvoir se prévaloir d'un préjudice spécial, à mettre en cause la responsabilité de l'Etat sur le fondement de l'égalité devant les charges publiques ;<br/>
<br/>
              Sur l'arrêt attaqué, en tant qu'il statue sur la responsabilité du fait de la méconnaissance par la France de ses engagements internationaux :<br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société requérante sollicitait, quel que soit le fondement retenu, la réparation des préjudices qu'elle estimait avoir subis du fait de l'intervention d'une loi adoptée en méconnaissance des engagements internationaux de la France ; que la cour, en se bornant à constater que la société requérante revendiquait à tort l'application d'un régime de responsabilité pour faute pour écarter les moyens tirés de la méconnaissance des engagements internationaux de la France et notamment des principes de sécurité juridique et de confiance légitime et du droit à un procès équitable reconnus respectivement par le droit communautaire et le droit de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, s'est méprise sur la portée de ses écritures ; que, dès lors, la société SEPR est fondée, sans qu'il soit besoin d'examiner les autres moyens du pourvoi dirigés contre cette partie de l'arrêt attaqué, à en demander l'annulation en tant qu'il statue sur la responsabilité de l'Etat du fait de la méconnaissance par la France de ses engagements internationaux ;<br/>
<br/>
              6. Considérant qu'il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au fond dans la mesure de la cassation prononcée ;<br/>
<br/>
              7. Considérant que la responsabilité de l'Etat du fait des lois est, outre l'hypothèse évoquée au point 3, également susceptible d'être engagée en raison des obligations qui sont les siennes pour assurer le respect des conventions internationales par les autorités publiques, pour réparer l'ensemble des préjudices qui résultent de l'intervention d'une loi adoptée en méconnaissance des engagements internationaux de la France, au nombre desquels figure le respect des principes de sécurité juridique et de confiance légitime reconnus par le droit communautaire et, désormais, par le droit de l'Union européenne ;<br/>
<br/>
              8. Considérant que l'article 26 de la loi du 29 juillet 1992 a introduit dans le code du travail un article L. 321-1-2, transféré à l'article L. 321-1-3 par la loi du 20 décembre 1993, ainsi rédigé : " Lorsque, pour l'un des motifs énoncés à l'article L. 321-1, l'employeur envisage le licenciement de plusieurs salariés ayant refusé une modification substantielle de leur contrat de travail, ces licenciements sont soumis aux dispositions applicables en cas de licenciement collectif pour motif économique " ; qu'aux termes de l'article L. 321-4-1 du même code, dans sa rédaction applicable au litige : " Dans les entreprises employant au moins cinquante salariés,  lorsque le nombre de licenciements est au moins égal à dix dans une même période de trente jours, l'employeur doit établir et mettre en oeuvre un plan social pour éviter les licenciements ou en limiter le nombre et pour faciliter le reclassement du personnel dont le licenciement ne pourrait être évité, notamment des salariés âgés ou qui présentent des caractéristiques sociales ou de qualification rendant leur réinsertion professionnelle particulièrement difficile. / La procédure de licenciement est nulle et de nul effet tant qu'un plan visant au reclassement de salariés s'intégrant au plan social n'est pas présenté par l'employeur aux représentants du personnel, qui doivent être réunis, informés et consultés (...) " ; que le droit des licenciements collectifs, notamment les obligations mises à la charge des employeurs qui envisagent de procéder à de tels licenciements, était, à la date d'adoption de la loi du 29 juillet 1992, régi par la directive du Conseil du 17 février 1975 concernant le rapprochement des législations des États membres relatives aux licenciements collectifs ;<br/>
<br/>
              9. Considérant que la Cour de cassation a jugé, dans ses arrêts du 3 décembre 1996, que l'employeur qui, dans le cadre d'une restructuration qu'il a décidée, est conduit à proposer à plus de dix salariés la modification d'un élément essentiel de leur contrat de travail et, par conséquent, à envisager le licenciement de ces salariés ou, à tout le moins, la rupture de leur contrat de travail pour motif économique, est tenu d'établir un plan social ; que l'un de ses arrêts prononce, pour ce motif, la cassation d'un arrêt par lequel une cour d'appel avait jugé que l'employeur n'était tenu d'engager la procédure de licenciement qu'après le refus des salariés exprimé à l'expiration du délai prévu par l'article L. 321-1-2 du code du travail ; <br/>
<br/>
              10. Considérant que, si la société requérante fait valoir qu'elle n'avait pas été en mesure d'anticiper l'interprétation donnée de ces dispositions par la Cour de cassation, elle critique ainsi non pas la loi elle-même mais la portée qui lui a été ultérieurement conférée par la jurisprudence ; qu'elle n'est, par suite, pas fondée à mettre en cause la responsabilité de l'Etat au motif que la loi aurait été adoptée en méconnaissance des principes dont elle se prévaut ;  <br/>
<br/>
              11. Considérant que, par suite, la société requérante n'est pas fondée à se plaindre de ce que, par le jugement attaqué, le tribunal administratif de Paris a rejeté sa demande ;  <br/>
<br/>
              Sur l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              12. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 22 septembre 2011 est annulé en tant qu'il statue sur la responsabilité de l'Etat pour méconnaissance des engagements internationaux de la France.<br/>
Article 2 : Les conclusions présentées par la société d'éditions et de protection route devant la cour administrative d'appel de Paris tendant à la mise en jeu de la responsabilité de l'Etat pour méconnaissance des engagements internationaux de la France et le surplus des conclusions de son pourvoi sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la société d'éditions et de protection route et au ministre du travail, de l'emploi et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">15-07 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RESPONSABILITÉ POUR MANQUEMENT AU DROIT DE L'UNION EUROPÉENNE. - RESPONSABILITÉ DE L'ETAT DU FAIT D'UNE LOI ADOPTÉE EN MÉCONNAISSANCE DES ENGAGEMENTS INTERNATIONAUX DE LA FRANCE [RJ1] - 1) NOTION D'ENGAGEMENT INTERNATIONAL - INCLUSION - PRINCIPES GÉNÉRAUX DU DROIT DE L'UE - 2) INVOCATION DE LA MÉCONNAISSANCE PAR UNE LOI DES PRINCIPES DE SÉCURITÉ JURIDIQUE ET DE CONFIANCE LÉGITIME EN RAISON DU CARACTÈRE IMPRÉVISIBLE DE L'INTERPRÉTATION QU'EN A DONNÉE LA JURISPRUDENCE DE LA COUR DE CASSATION - RESPONSABILITÉ DE L'ETAT SUSCEPTIBLE D'ÊTRE ENGAGÉE SUR CE FONDEMENT - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-01-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. FONDEMENT DE LA RESPONSABILITÉ. - RESPONSABILITÉ DU FAIT DES LOIS - FONDEMENTS - 1) EGALITÉ DEVANT LES CHARGES PUBLIQUES - CONDITIONS D'ENGAGEMENT - INCLUSION - LOI N'AYANT PAS EXCLU TOUTE INDEMNISATION [RJ2] -  PRÉJUDICE GRAVE ET SPÉCIAL - 2) OBLIGATION D'ASSURER LE RESPECT DES ENGAGEMENTS INTERNATIONAUX DE LA FRANCE - A) NOTION D'ENGAGEMENT INTERNATIONAL - INCLUSION - PRINCIPES GÉNÉRAUX DU DROIT DE L'UE - B) INVOCATION DE LA MÉCONNAISSANCE PAR UNE LOI DES PRINCIPES DE SÉCURITÉ JURIDIQUE ET DE CONFIANCE LÉGITIME EN RAISON DU CARACTÈRE IMPRÉVISIBLE DE L'INTERPRÉTATION QU'EN A DONNÉE LA JURISPRUDENCE DE LA COUR DE CASSATION - RESPONSABILITÉ DE L'ETAT SUSCEPTIBLE D'ÊTRE ENGAGÉE SUR CE FONDEMENT - ABSENCE.
</SCT>
<ANA ID="9A"> 15-07 1) La responsabilité de l'Etat du fait des lois est susceptible d'être engagée en raison des obligations qui sont les siennes pour assurer le respect des conventions internationales par les autorités publiques, pour réparer l'ensemble des préjudices qui résultent de l'intervention d'une loi adoptée en méconnaissance des engagements internationaux de la France, au nombre desquels figure le respect des principes de sécurité juridique et de confiance légitime reconnus par le droit communautaire et, désormais, par le droit de l'Union européenne (UE).,,,2) Requérant cherchant à engager la responsabilité de l'Etat du fait d'une loi intervenue en méconnaissance des principes de sécurité juridique et de confiance légitime reconnus par le droit de l'UE. S'il fait valoir, à ce titre, qu'il n'a pas été en mesure d'anticiper l'interprétation donnée des dispositions législatives en cause par la Cour de cassation, il critique ainsi non pas la loi elle-même mais la portée qui lui a été ultérieurement conférée par la jurisprudence et n'est, par suite, pas fondé à mettre en cause la responsabilité de l'Etat au motif que la loi aurait été adoptée en méconnaissance des principes dont il se prévaut.</ANA>
<ANA ID="9B"> 60-01-02 1) La responsabilité de l'Etat du fait des lois est susceptible d'être engagée, sur le fondement de l'égalité des citoyens devant les charges publiques, pour assurer la réparation de préjudices nés de l'adoption d'une loi, à la condition que cette loi n'ait pas exclu toute indemnisation et que le préjudice dont il est demandé réparation, revêtant un caractère grave et spécial, ne puisse, dès lors, être regardé comme une charge incombant normalement aux intéressés.,,,2) a) La responsabilité de l'Etat du fait des lois est susceptible d'être engagée en raison des obligations qui sont les siennes pour assurer le respect des conventions internationales par les autorités publiques, pour réparer l'ensemble des préjudices qui résultent de l'intervention d'une loi adoptée en méconnaissance des engagements internationaux de la France, au nombre desquels figure le respect des principes de sécurité juridique et de confiance légitime reconnus par le droit communautaire et, désormais, par le droit de l'Union européenne (UE).,,,b) Requérant cherchant à engager la responsabilité de l'Etat du fait d'une loi intervenue en méconnaissance des principes de sécurité juridique et de confiance légitime reconnus par le droit de l'UE. S'il fait valoir, à ce titre, qu'il n'a pas été en mesure d'anticiper l'interprétation donnée des dispositions législatives en cause par la Cour de cassation, il critique ainsi non pas la loi elle-même mais la portée qui lui a été ultérieurement conférée par la jurisprudence et n'est, par suite, pas fondé à mettre en cause la responsabilité de l'Etat au motif que la loi aurait été adoptée en méconnaissance des principes dont il se prévaut.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 8 février 2007, Gardedieu, n° 279522, p. 78.,,[RJ2] Cf., en tant qu'est admis que le silence de la loi ne saurait être interprété comme excluant, par principe, tout droit à réparation des préjudices que son application peut provoquer, CE, 2 novembre 2005, Société coopérative agricole Ax'ion, n° 266564, p. 468. Comp., pour la rédaction antérieure du considérant de principe  La Fleurette  sur ce point, CE, Assemblée, 8 février 2007, Gardedieu, n° 279522, p. 78.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
