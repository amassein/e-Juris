<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027592828</ID>
<ANCIEN_ID>JG_L_2013_06_000000347450</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/59/28/CETATEXT000027592828.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 21/06/2013, 347450, Publié au recueil Lebon</TITRE>
<DATE_DEC>2013-06-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>347450</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; FOUSSARD</AVOCATS>
<RAPPORTEUR>Mme Domitille Duval-Arnould</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2013:347450.20130621</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 14 mars 2011 et 14 juin 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour le centre hospitalier Emile Roux du Puy-en-Velay, dont le siège est BP 352 au Puy-en-Velay (43012 Cedex) ; le centre hospitalier du Puy-en-Velay demande au Conseil d'Etat d'annuler l'arrêt n° 09LY02761-09LY02780 du 6 janvier 2011 par lequel la cour administrative d'appel de Lyon a, sur appel de M. A...B..., annulé le jugement n° 0801701 du 6 octobre 2009 du tribunal administratif de Clermont-Ferrand et l'a condamné à verser, d'une part, à M. B...la somme de 28 000 euros et, d'autre part, à la caisse primaire d'assurance maladie de Haute-Loire la somme de 154 858,26 euros ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Domitille Duval-Arnould, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat du centre hospitalier du Puy-en-Velay et à Me Foussard, avocat de la caisse primaire d'assurance maladie de la Haute-Loire ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.B..., présentant une sigmoïdite récidivante, a subi le 8 décembre 2005 une coelioscopie et une résection du colon au centre hospitalier du Puy-en-Velay ; qu'à la suite de différentes complications, ayant nécessité des reprises chirurgicales elles-mêmes à l'origine de nouvelles complications infectieuses, il a conservé des séquelles qui l'ont conduit à rechercher la responsabilité du centre hospitalier du Puy-en-Velay au titre, d'une part, de fautes qui auraient été commises dans sa prise en charge et, d'autre part, de la survenue d'une infection nosocomiale ; que, par un jugement du 6 octobre 2009, le tribunal administratif de Clermont-Ferrand a rejeté la demande de M. B...ainsi que les conclusions présentées par la caisse primaire d'assurance maladie de la Haute-Loire ; que, par un arrêt du 6 janvier 2011, la cour administrative d'appel de Lyon, jugeant que le centre hospitalier du Puy-en-Velay était tenu de répondre des dommages résultant de l'infection nosocomiale alléguée, a censuré la décision des premiers juges et fait droit tant aux demandes indemnitaires de M. B...qu'aux conclusions présentées par la caisse d'assurance maladie ; que le centre hospitalier du Puy-en-Velay se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              2. Considérant qu'aux termes du I de l'article L. 1142-1 du code de la santé publique : " Hors le cas où leur responsabilité est encourue en raison d'un défaut d'un produit de santé, les professionnels de santé mentionnés à la quatrième partie du présent code, ainsi que tout établissement, service ou organisme dans lesquels sont réalisés des actes individuels de prévention, de diagnostic ou de soins ne sont responsables des conséquences dommageables d'actes de prévention, de diagnostic ou de soins qu'en cas de faute.  / Les établissements, services et organismes susmentionnés sont responsables des dommages résultant d'infections nosocomiales, sauf s'ils rapportent la preuve d'une cause étrangère " ; que si ces dispositions font peser sur l'établissement de santé la responsabilité des infections nosocomiales, qu'elles soient exogènes ou endogènes, à moins que la preuve d'une cause étrangère soit rapportée, seule une infection survenant au cours ou au décours d'une prise en charge et qui n'était ni présente, ni en incubation au début de la prise en charge peut être qualifiée de nosocomiale ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B...présentait, à son admission au centre hospitalier du Puy-en-Velay, une maladie infectieuse qui a motivé l'intervention chirurgicale pratiquée ; que, pour retenir qu'il avait contracté une infection nosocomiale,  la cour administrative d'appel de Lyon s'est bornée à constater que les suites opératoires avaient été compliquées par une multi-infection résultant, selon l'expert, de la dissémination de nombreuses colonies microbiennes ; qu'en retenant cette qualification sans rechercher si les complications survenues étaient soit consécutives au développement de l'infection préexistante, soit distinctes et liées à une nouvelle infection survenue au cours des soins prodigués au centre hospitalier du Puy-en-Velay, les juges d'appel ont commis une erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, le centre hospitalier du Puy-en-Velay est fondé à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du centre hospitalier du Puy-en-Velay qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon n° 09LY02761-09LY02780 du 6 janvier 2011 est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
<br/>
Article 3 : Les conclusions de la caisse primaire d'assurance maladie de Haute-Loire au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée au centre hospitalier du Puy-en-Velay, à M. A... B...et à la caisse primaire d'assurance maladie de Haute-Loire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-02-01-01-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. ÉTABLISSEMENTS PUBLICS D'HOSPITALISATION. RESPONSABILITÉ POUR FAUTE MÉDICALE : ACTES MÉDICAUX. - INFECTIONS NOSOCOMIALES - PRÉSOMPTION DE RESPONSABILITÉ DE L'ÉTABLISSEMENT SAUF S'IL RAPPORTE LA PREUVE D'UNE CAUSE ÉTRANGÈRE (ART. L. 1142-1 DU CSP) - DÉFINITION DE L'INFECTION NOSOCOMIALE [RJ1].
</SCT>
<ANA ID="9A"> 60-02-01-01-02 Si les dispositions du I de l'article L. 1142-1 du code de la santé publique (CSP) font peser sur l'établissement de santé la responsabilité des infections nosocomiales, qu'elles soient exogènes ou endogènes, à moins que la preuve d'une cause étrangère soit rapportée, seule une infection survenant au cours ou au décours d'une prise en charge et qui n'était ni présente, ni en incubation au début de la prise en charge peut être qualifiée de nosocomiale.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur le régime de présomption de responsabilité institué par l'article L. 1142-1 du CSP, CE, 10 octobre 2011, Centre hospitalier universitaire d'Angers, n° 328500, p. 458.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
