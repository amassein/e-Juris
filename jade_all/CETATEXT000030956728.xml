<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030956728</ID>
<ANCIEN_ID>JG_L_2015_07_000000388359</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/95/67/CETATEXT000030956728.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème SSJS, 27/07/2015, 388359, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388359</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR>Mme Anne Egerszegi</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:388359.20150727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par décision du 20 novembre 2014, la Commission nationale des comptes de campagne et des financements politiques (CNCCFP) a rejeté le compte de campagne de M. C... A..., candidat à l'élection municipale générale des 23 et 30 mars 2014 dans la circonscription du secteur 6 de Marseille. En application de l'article L. 52-15 du code électoral, elle a saisi le tribunal administratif de Marseille. Par un jugement du 3 février 2015, le tribunal administratif de Marseille a rejeté la saisine de la CNCCFP et fixé le montant du remboursement forfaitaire de l'Etat à 10 679 euros. <br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 27 février et le 5 juin 2015 au secrétariat du contentieux du Conseil d'Etat, la CNCCFP demande au Conseil d'Etat, à titre principal, d'annuler le jugement du tribunal administratif de Marseille et, à titre subsidiaire, de ramener le remboursement forfaitaire de l'Etat de 10 679 à zéro euro. <br/>
<br/>
<br/>
              Vu les pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ; <br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Egerszegi, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat de M. C...A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte de l'instruction que le compte de campagne de M. A..., candidat à l'élection municipale générale des 23 et 30 mars 2014 dans la circonscription du secteur 6 de Marseille, comptabilise une dépense engagée en vue de la campagne électorale correspondant à la sous-location à sa colistière, Mme D...B..., d'un local de 76 m² pour la période du 1er octobre 2013 au 31 mars 2014 pour un montant total de 6 900 euros ; que, par une décision du 20 novembre 2014, la Commission nationale des comptes de campagne et des financements politiques (CNCCFP) a rejeté le compte de campagne de M. A... au motif de l'irrégularité du financement de ce local par l'indemnité représentative de frais de mandat de député de MmeB..., en méconnaissance de l'article L. 52-8-1 du code électoral ; que par un jugement du 3 février 2015, le tribunal administratif de Marseille, saisi par la CNCCFP en application de l'article L. 52-15 du même code, a jugé que c'était à tort que le compte de campagne avait été rejeté à tort ; qu'il a fixé le montant du remboursement dû par l'Etat à M. A...en application de l'article L. 52-11-1 du code électoral à la somme de 10 679 euros ; que la CNCCFP relève appel de ce jugement ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 52-12 du code électoral : " Chaque candidat ou candidat tête de liste soumis au plafonnement prévu à l'article L. 52-11 et qui a obtenu au moins 1 % des suffrages exprimés est tenu d'établir un compte de campagne retraçant, selon leur origine, l'ensemble des recettes perçues et, selon leur nature, l'ensemble des dépenses engagées ou effectuées en vue de l'élection, hors celles de la campagne officielle par lui-même ou pour son compte, au cours de la période mentionnée à l'article L. 52-4 (...) Sont réputées faites pour son compte les dépenses exposées directement au profit du candidat et avec l'accord de celui-ci, par les personnes physiques qui lui apportent leur soutien, ainsi que par les partis et groupements politiques qui ont été créés en vue de lui apporter leur soutien ou qui lui apportent leur soutien. Le candidat estime et inclut, en recettes et en dépenses, les avantages directs ou indirects, les prestations de services et dons en nature dont il a bénéficié. Le compte de campagne doit être en équilibre ou excédentaire et ne peut présenter un déficit (...) " ; qu'aux termes de l'article L. 52-8-1 de ce code : " Aucun candidat ne peut utiliser, directement ou indirectement, les indemnités et les avantages en nature mis à disposition de leurs membres par les assemblées parlementaires pour couvrir les frais liés à l'exercice de leur mandat " ; que ni ces dispositions ni aucune autre disposition législative n'obligent la Commission nationale des comptes de campagne et des financements politiques à rejeter le compte d'un candidat faisant apparaître une méconnaissance des dispositions précitées de l'article L. 52-8-1 du code électoral ; qu'il lui appartient, sous le contrôle du juge de l'élection, d'apprécier, compte tenu de l'ensemble des circonstances de l'espèce, notamment de la nature de l'avantage dont a bénéficié l'intéressé et de son montant, si l'utilisation de cet avantage doit entraîner le rejet du compte ;<br/>
<br/>
              3. Considérant que " l'indemnité représentative de frais de mandat ", versée, selon l'article L. 136-2 du code de la sécurité sociale, " à titre d'allocation spéciale pour frais par les assemblées à tous leurs membres ", est destinée à couvrir des dépenses liées à l'exercice du mandat parlementaire ; qu'en conséquence, cette indemnité est au nombre de celles qui sont mentionnées à l'article L. 52-8-1 du code électoral et ne saurait, sans que soient méconnues les dispositions de cet article, être affectée, directement ou indirectement, au financement d'une campagne électorale de son bénéficiaire ;<br/>
<br/>
              4. Considérant qu'il résulte de l'instruction que le mandataire financier de M. A... a acquitté l'intégralité des loyers dus en contrepartie de l'occupation du local en cause au moyen de ressources du compte de campagne de M.A..., pour un montant de 6 900 euros dont il n'est pas contesté qu'il correspondait à la valeur locative du bien ; que M. A... ne peut ainsi être regardé comme ayant bénéficié, directement ou indirectement, d'un financement de l'occupation de ce local par des indemnités ou des avantages en nature mis à disposition de leurs membres par les assemblées parlementaires pour couvrir l'exercice de leur mandat ; que, par suite, M. A...ne saurait être regardé comme ayant méconnu l'article L. 52-8-1 du code électoral, sans qu'aient d'incidence la circonstance que le bail initial de Mme B...n'autorisait pas de sous-location et que la signature de la convention de sous-location ainsi que le paiement du loyer correspondant soient intervenus postérieurement au début de l'occupation du local ni la circonstance alléguée que Mme B...aurait financé directement ou indirectement la location de ce local par son indemnité représentative de frais de mandat perçue en sa qualité de député ;<br/>
<br/>
              5. Considérant qu'au demeurant, Mme B...s'acquittait, en sa qualité de preneur du local en cause, d'un loyer à partir d'un premier compte bancaire lui-même abondé par un virement en provenance d'un second compte bancaire alimenté par l'indemnité représentative de frais de mandat perçue en sa qualité de député ; que le premier compte bancaire a reçu, pendant la période d'occupation du local du 1er octobre 2013 au 31 mars 2014, des recettes étrangères à cette indemnité ; que, compte tenu de leurs montants, ces recettes étaient suffisantes pour permettre la location du local en litige exclusivement à partir de recettes étrangères à l'indemnité représentative de frais de mandat ; que la location du local en litige par Mme B...ne peut par suite pas être regardée, en tout état de cause, comme ayant été financée sur les recettes provenant de cette indemnité ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que la CNCCFP n'est pas fondée à se plaindre de ce que, par le jugement attaqué, le tribunal administratif de Marseille a rejeté sa saisine et fixé le montant du remboursement forfaitaire de l'Etat à 10 679 euros ;<br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 500 euros à verser à M. A...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la Commission nationale des comptes de campagne et des financements politiques est rejetée.<br/>
<br/>
Article 2 : L'Etat versera à M. A...la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 3 : La présente décision sera notifiée à la Commission nationale des comptes de campagne et des financements politiques, à M. C...A...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
