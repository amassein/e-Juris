<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036253328</ID>
<ANCIEN_ID>JG_L_2017_12_000000391695</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/25/33/CETATEXT000036253328.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème chambre jugeant seule, 22/12/2017, 391695, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391695</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème chambre jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>Mme Sara-Lou Gerber</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:391695.20171222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M.  A...B...a demandé au tribunal administratif de Melun d'annuler pour excès de pouvoir la décision du 16 juillet 2012 par laquelle le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social, après avoir a annulé la décision de l'inspectrice du travail de la 5ème section de la Seine-et-Marne du 24 janvier 2012, a autorisé son licenciement. Par un jugement n° 1207800 du 2 avril 2014, le tribunal administratif a annulé cette décision.<br/>
<br/>
              Par un arrêt n° 14PA02516, 14PA02446 du 4 juin 2015, la cour administrative d'appel de Paris a rejeté les appels formés contre ce jugement par la société Imprimerie Didier Mary et MeC..., agissant en qualité de mandataire judiciaire de celle-ci et par la société H2D Didier Mary.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 13 juillet et 13 octobre 2015 et le 28 avril 2017 au secrétariat du contentieux du Conseil d'Etat, la société Imprimerie Didier Mary, la société H2D Didier Mary, la société Garnier, C..., agissant en qualité de liquidateur judiciaire de la société Imprimerie Didier Mary, et la SELARL Cabooter, agissant en qualité d'administrateur judiciaire de la société H2D Didier Mary, demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de commerce ;<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sara-Lou Gerber, auditeur,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de la société imprimerie Didier Mary et autres ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 18 décembre 2017, présentée par la société Imprimerie Didier Mary et autres.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un jugement du 6 octobre 2011, le tribunal de commerce de Meaux a arrêté le plan de cession totale des actifs de la société Imprimerie Didier Mary, entreprise du groupe Circle Printers, à la société H2D Didier Mary, entreprise du groupe H2D, qui prévoyait le transfert de 251 des 456 salariés de la société Imprimerie Didier Mary et le licenciement de l'ensemble des salariés non-transférés ; que la société Imprimerie Didier Mary a, ultérieurement, été placée en liquidation par un jugement du même tribunal du 26 octobre 2011 ; que l'inspectrice du travail de la 5ème section de la Seine-et-Marne ayant, par une décision du 24 janvier 2012, refusé d'autoriser le licenciement de M. A...B..., salarié protégé, le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a, par une décision du 16 juillet 2012, annulé cette décision et autorisé le licenciement de l'intéressé ; que M. B...a toutefois obtenu l'annulation de cette décision par un jugement du 2 avril 2014 du tribunal administratif de Melun ; que la société Imprimerie Didier Mary, son mandataire judiciaire et son liquidateur judiciaire, la société Garnier, C..., ainsi que la société H2D Didier Mary et son administrateur judiciaire, la société Cabooter, se pourvoient en cassation contre l'arrêt du 4 juin 2015 par lequel la cour administrative d'appel de Paris a rejeté leurs appels dirigés contre ce jugement du 2 avril 2014 ;<br/>
<br/>
              2. Considérant, en premier lieu, que, contrairement à ce que soutiennent les requérants, il résulte des termes mêmes de l'arrêt attaqué que la cour administrative d'appel a répondu, en l'écartant, au moyen tiré de ce que l'employeur avait satisfait à son obligation de reclassement à l'égard de M. B... ;<br/>
<br/>
              3. Considérant que, dès lors que, ainsi qu'il vient d'être dit, elle jugeait que l'obligation de reclassement n'avait pas été correctement satisfaite et que l'autorisation administrative de licenciement était, par suite, illégale, la cour, qui a suffisamment motivé son arrêt sur ce point, n'était pas tenue de répondre aux autres moyens des appelants relatifs à la réalité du motif économique du licenciement ; que les requérants ne sont, par suite, pas fondés à soutenir que son arrêt serait, pour ce motif, entaché d'irrégularité ;<br/>
<br/>
              4. Considérant que si, dans leur dernier mémoire, les requérants soutiennent que la cour a commis une erreur de droit en jugeant que l'administration devait se prononcer sur la situation économique de l'ensemble des sociétés du groupe oeuvrant dans le secteur d'activité auquel appartenait l'entreprise, il résulte des termes mêmes de l'arrêt que la cour ne s'est, s'agissant de la situation de M.B..., pas fondée sur cette circonstance pour rejeter l'appel ; que ce moyen est, par suite, inopérant ; <br/>
<br/>
              5. Considérant, enfin, qu'en estimant que les administrateurs judiciaires de la société Imprimerie Didier Mary n'avaient pas procédé à une recherche suffisamment sérieuse et personnalisée des offres de reclassement disponibles pour M. B... dans le groupe Circle Printers auquel appartenait la société, la cour administrative d'appel de Paris a porté sur les pièces du dossier une appréciation souveraine, qui n'est pas entachée de dénaturation ; qu'elle a pu, par suite, sans erreur de droit en déduire que l'autorisation de licenciement ne pouvait légalement être accordée par l'administration ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que le pourvoi de la société Imprimerie Didier Mary et autres doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de la société Imprimerie Didier Mary et autres est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la société Imprimerie Didier Mary, premier dénommé pour l'ensemble des sociétés requérantes et à M. A...B.... <br/>
Copie en sera adressée à la ministre du travail.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
