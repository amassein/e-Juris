<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034166804</ID>
<ANCIEN_ID>JG_L_2017_03_000000408372</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/16/68/CETATEXT000034166804.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 01/03/2017, 408372, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408372</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:408372.20170301</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 27 février 2017 au secrétariat du contentieux du Conseil d'Etat, le département de la Sarthe demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution de l'article 1er, 10°, du décret n° 2017-122 du 1er février 2017 relatif à la réforme des minimas sociaux du ministre des affaires sociales et de la santé, en tant qu'il a créé l'article R. 262-25-5 du code de l'action sociale et des familles ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence est remplie dès lors que l'article R. 262-25-5 du code de l'action sociale et des familles permettant la dématérialisation des demandes d'allocation de revenu de solidarité active contraint le département, d'une part, de passer d'un budget primitif de 66,2 millions d'euros à 71,2 millions d'euros et, d'autre part, de réorganiser ses services ;<br/>
              - le président du conseil départemental de la Sarthe, qui gère les ressources du revenu de solidarité active, n'aura plus de droit de regard sur les ouvertures de droits au revenu de solidarité active ;<br/>
              - il existe un doute sérieux quant à la légalité de la décision contestée ;<br/>
              - la décision litigieuse est entachée d'un vice d'incompétence dès lors que le pouvoir réglementaire ne pouvait dispenser l'usager de la fourniture de pièces justificatives, une disposition législative étant nécessaire ;<br/>
              - elle est entachée d'une erreur manifeste d'appréciation dès lors qu'il était opportun pour le Premier ministre de saisir le Conseil économique, social et environnemental ;   <br/>
              - elle méconnaît le principe de libre administration des collectivités territoriales, inscrit à l'article 72 de la Constitution ;<br/>
              - elle méconnaît l'autonomie financière des collectivités territoriales, reconnue tant par l'article 72-2 de la Constitution que par l'article 9-1 de la Charte européenne de l'autonomie locale, en ce que les départements n'ont plus le contrôle de leurs dépenses ;<br/>
              - elle méconnaît l'article L. 262-13 du code de l'action sociale et des familles en privant le président du conseil départemental de sa compétence de principe en matière de revenu de solidarité active.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
	- le code de justice administrative ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; qu'en vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée ; <br/>
<br/>
              2. Considérant que le code de l'action sociale et des familles fixe les conditions d'attribution du revenu de solidarité active (RSA), institué par son article L. 262-1 ; que les demandes peuvent être déposées auprès de plusieurs catégories d'organismes dont la liste est fixée par l'article D. 262-26 de ce code ; que leur instruction peut être effectuée par les services du département ou ceux des caisses d'allocations familiales ou des caisses de mutualité sociale agricole ; qu'en vertu du second alinéa de l'article L. 262-13 de ce code, ces mêmes organismes peuvent également se voir déléguer par le conseil départemental le pouvoir d'attribution de cette prestation, normalement dévolu au président du conseil départemental ;<br/>
<br/>
              3. Considérant que le décret litigieux du 1er février 2017 a notamment complété les règles relatives à l'instruction des demandes d'attribution du RSA ; qu'un nouvel article R. 262-25-5 du code de l'action sociale et des familles prévoit ainsi que lorsqu'elle est déposée auprès d'une caisse d'allocations familiales ou de mutualité sociale agricole, la demande de RSA peut être formulée non seulement par le dépôt d'un formulaire mais également par téléservice et que, dans ce dernier cas, le demandeur est dispensé de la fourniture de pièces justificatives dès lors que ces organismes disposent des informations nécessaires ou qu'elles peuvent être obtenues auprès d'autres organismes, mentionnés à l'article L. 262-40 du même code ; que le département de la Sarthe demande la suspension de l'exécution de ces dispositions sur le fondement de l'article L. 521-1 du code de justice administrative cité ci-dessus ;<br/>
<br/>
              4. Considérant que pour justifier de l'urgence à suspendre l'exécution de l'article R. 262-25-5 du code de l'action sociale et des familles, le département de la Sarthe soutient que l'utilisation vraisemblablement importante du téléservice pour formuler les demandes de RSA combinée avec la dispense de justificatifs qui s'attache à ce mode de saisine privera ses services de la possibilité d'écarter les demandes infondées ou frauduleuses et renchérira le coût de cette prestation pour le département ; que toutefois la faculté d'utiliser le téléservice est sans incidence tant sur les conditions d'attribution de la prestation, inchangées par les dispositions litigieuses, que sur les décisions d'attribution elles-mêmes, dont la compétence revient au président du conseil départemental à moins qu'une délégation ait été donnée à l'organisme instructeur sur le fondement de l'article L. 262-13 ; que, par suite, les dispositions litigieuses qui ne sauraient, contrairement à ce qui est soutenu, conduire à une  " automaticité d'ouverture des droits " au RSA, ne causent, en l'état de l'instruction, au département de la Sarthe, aucun préjudice grave et immédiat de nature à justifier la suspension de leur exécution ;    <br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que le département n'est pas fondé à demander la suspension de l'exécution du décret contesté ; que la requête, y compris les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative, ne peut, par suite, qu'être rejetée selon la procédure prévue par l'article L. 522-3 de ce code ;<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête du département de la Sarthe est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée au département de la Sarthe et à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
