<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039168442</ID>
<ANCIEN_ID>JG_L_2019_10_000000430633</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/16/84/CETATEXT000039168442.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 02/10/2019, 430633, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430633</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Cécile Renault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:430633.20191002</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              M. D... A... a demandé au juge des référés du tribunal administratif de Paris, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de la décision implicite par laquelle la ministre des armées a refusé de lui accorder la protection fonctionnelle et de lui enjoindre de mettre en oeuvre, dans un délai de 8 jours à compter de la notification de l'ordonnance, toute mesure de nature à assurer sa mise en sécurité immédiate ainsi que celle de sa famille par tout moyen approprié et la délivrance d'un visa, ou de réexaminer sa demande de protection fonctionnelle dans le même délai. <br/>
<br/>
              Par une ordonnance n° 1906710 du 26 avril 2019, le juge des référés du tribunal administratif de Paris a suspendu l'exécution de la décision attaquée et a enjoint, d'une part, à l'Etat de délivrer à M. A..., à son épouse et ses enfants des visas dans un délai d'un mois à compter de la notification de son ordonnance et, d'autre part, à la ministre des armées de prendre toute mesure de nature à assurer la sécurité de l'intéressé et de sa famille jusqu'à ce que des visas leur soient effectivement délivrés.<br/>
<br/>
              1° Sous le n° 430633, par un pourvoi, enregistré le 10 mai 2019 au secrétariat du contentieux du Conseil d'Etat, la ministre des armées demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de M. A....<br/>
<br/>
              2° Sous le n° 430636, par une requête, enregistrée le 11 mai 2019 au secrétariat du contentieux du Conseil d'Etat, la ministre des armées demande au Conseil d'Etat d'ordonner le sursis à exécution de l'ordonnance du 26 avril 2019 du juge des référés du tribunal administratif de Paris jusqu'à ce qu'il ait été statué sur son pourvoi en cassation.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... C..., auditrice,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M. A....<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le pourvoi et la requête de la ministre des armées sont dirigés contre la même ordonnance. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2.  Il ressort des pièces du dossier soumis au juge des référés que M. A..., ressortissant afghan, a exercé, entre le 5 août 2008 et le 31 octobre 2009, les fonctions d'interprète auprès du " Regional Command-Capital " de la Force internationale d'assistance et de sécurité à Kaboul. Les autorités françaises ont annoncé au mois de mai 2012 le retrait des forces françaises de ce pays à partir du mois de juillet. M. A..., qui vit à Kaboul avec sa famille, a sollicité le 3 mars 2018, de la ministre des armées qu'elle lui accorde, dans le cadre d'un dispositif de réexamen des demandes de relocalisation des personnels civils à recrutement local, le bénéfice de la protection fonctionnelle pour lui-même et sa famille compte tenu des menaces dont ils feraient l'objet en Afghanistan en raison de ses anciennes fonctions. Cette demande a fait l'objet d'une décision implicite de rejet dont M. A... a demandé, sur le fondement de l'article L. 521-1 du code de justice administrative, au juge des référés du tribunal administratif de Paris la suspension assortie d'une injonction tendant à la mise en oeuvre de mesures de sécurité et à la délivrance d'un visa pour lui-même et sa famille. La ministre des armées se pourvoit en cassation contre l'ordonnance du 26 avril 2019 par laquelle le juge des référés de ce tribunal a suspendu sa décision et a enjoint, d'une part, à l'Etat de délivrer à l'intéressé, ainsi qu'à son épouse et à ses enfants, des visas dans un délai d'un mois à compter de la notification de l'ordonnance et, d'autre part, à la ministre des armées de prendre toute mesure de nature à assurer sa sécurité et celle de sa famille jusqu'à la délivrance de ces visas. Elle demande en outre qu'il soit sursis à l'exécution de cette ordonnance sur le fondement de l'article R. 821-5 du code de justice administrative.<br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              3. L'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre.<br/>
<br/>
              4. Il ressort des énonciations de l'ordonnance attaquée que, pour considérer que la condition d'urgence était satisfaite, le juge des référés du tribunal administratif de Paris a retenu, en particulier, qu'en raison de son concours comme interprète, M. A... avait reçu des menaces de mort téléphoniques par des groupes insurgés en Afghanistan et qu'il avait dû déménager à plusieurs reprises à Kaboul afin d'assurer sa sécurité et celle de sa famille. Toutefois, il ressort des pièces du dossier soumis au juge des référés qu'aucune de ces affirmations, dont la validité était sérieusement contestée par la ministre des armées, n'est étayée par des éléments probants. Par suite, en estimant qu'était établie l'existence de menaces présentant un caractère personnel, actuel et réel en raison des anciennes fonctions exercées par M. A..., le juge des référés a entaché son ordonnance de dénaturation des pièces du dossier. La ministre des armées est, dès lors, fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à en demander l'annulation.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée, en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              Sur la demande de suspension :<br/>
<br/>
              6. Aux termes du IV de l'article 11 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires : " La collectivité publique est tenue de protéger le fonctionnaire contre les atteintes volontaires à l'intégrité de la personne, les violences, (...) les menaces, les injures, les diffamations ou les outrages dont il pourrait être victime sans qu'une faute personnelle puisse lui être imputée. Elle est tenue de réparer, le cas échéant, le préjudice qui en est résulté ".<br/>
<br/>
              7. Il résulte d'un principe général du droit que, lorsqu'un agent public est mis en cause par un tiers en raison de ses fonctions, il incombe à la collectivité dont il dépend de le couvrir des condamnations civiles prononcées contre lui, dans la mesure où une faute personnelle détachable du service ne lui est pas imputable, de lui accorder sa protection dans le cas où il fait l'objet de poursuites pénales, sauf s'il a commis une faute personnelle, et, à moins qu'un motif d'intérêt général ne s'y oppose, de le protéger contre les menaces, violences, voies de fait, injures, diffamations ou outrages dont il est l'objet. Ce principe général du droit s'étend aux agents non-titulaires de l'Etat recrutés à l'étranger, alors même que leur contrat est soumis au droit local. La juridiction administrative est compétente pour connaître des recours contre les décisions des autorités de l'Etat refusant aux intéressés le bénéfice de cette protection.<br/>
<br/>
              8. Ainsi qu'il a été dit au point 4, il ne ressort pas des pièces du dossier que M. A... faisait, à la date de la décision dont la suspension est demandée, l'objet de menaces personnelles, actuelles et réelles en raison de ses anciennes fonctions auprès des forces armées françaises. Dans ces conditions, le moyen tiré de la violation de l'article 11 de la loi du 13 juillet 1983 ou du principe général du droit à la protection fonctionnelle qui l'inspire, n'est pas, en l'état de l'instruction et en tout état de cause, de nature à faire naître un doute sérieux sur la légalité du refus opposé par la ministre des armées à la demande de protection fonctionnelle présentée en 2018 par M. A....<br/>
<br/>
              9. Il en va de même des moyens tirés, d'une part, de l'absence de motivation de la décision implicite de rejet et, d'autre part, d'un défaut d'examen individuel de sa demande.<br/>
<br/>
              10. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur la condition d'urgence, que M. A... n'est pas fondé à demander la suspension de la décision de refus de protection fonctionnelle qui lui a été opposée par la ministre des armées. <br/>
<br/>
              Sur les conclusions à fin de sursis à exécution :<br/>
<br/>
              11. Compte tenu de ce qui précède, il n'y a plus lieu de statuer sur les conclusions à fin de sursis à l'exécution de l'ordonnance du 26 avril 2019 en litige.<br/>
<br/>
              Sur les frais du litige :<br/>
<br/>
              12. Les conclusions présentées par M. A... sur le fondement des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 et ne peuvent qu'être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
     --------------<br/>
Article 1er : L'ordonnance du 26 avril 2019 du juge des référés du tribunal administratif de Paris est annulée.<br/>
Article 2 : La demande présentée par M. A... devant le juge des référés du tribunal administratif de Paris est rejetée.<br/>
Article 3 : Les conclusions présentées devant le Conseil d'Etat sur le fondement des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 4 : Il n'y a pas lieu de statuer sur les conclusions de la requête n° 430636.<br/>
Article 5 : La présente décision sera notifiée à la ministre des armées et à M. D... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
