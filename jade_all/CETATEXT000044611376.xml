<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044611376</ID>
<ANCIEN_ID>JG_L_2021_12_000000444501</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/61/13/CETATEXT000044611376.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 28/12/2021, 444501, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>444501</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Myriam Benlolo Carabot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Domingo</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:444501.20211228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et dix mémoires, enregistrés les 16 septembre et 26 novembre 2020, les 25 mars, 31 mars, 4 avril, le 22 avril, le 25 avril, 27 avril et 6 mai 2021 au secrétariat du contentieux du Conseil d'Etat, M. C... D... demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret n° 2020-944 du 30 juillet 2020 modifiant le décret n° 2020-860 du 10 juillet 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans les territoires sortis de l'état d'urgence sanitaire et dans ceux où il a été prorogé.<br/>
<br/>
              Il soutient que le décret attaqué est contraire à l'article 34 de la Constitution en ce qu'il accorde une délégation du pouvoir législatif au représentant de l'Etat dans le département, ainsi que des pouvoirs excessifs en l'absence de toute précision sur leur modalités et conditions de mise en œuvre.<br/>
<br/>
              La requête a été communiquée au ministre des solidarités et de la santé, au ministre de l'intérieur et au ministre des outre-mer, qui n'ont pas produit d'observations.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Myriam Benlolo Carabot, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Domingo, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. L'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre des solidarités et de la santé puis le Premier ministre à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Pour faire face à l'aggravation de l'épidémie, la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 a créé un régime d'état d'urgence sanitaire aux articles L. 3131-12 à L. 3131-20 du code de la santé publique et déclaré l'état d'urgence sanitaire pour une durée de deux mois à compter du 24 mars 2020. La loi du 11 mai 2020 a prorogé cet état d'urgence sanitaire jusqu'au 10 juillet 2020 inclus. <br/>
<br/>
              2. L'évolution de la situation sanitaire a conduit à un assouplissement des mesures prises. La loi du 9 juillet 2020, organisant un régime de sortie de cet état d'urgence, a autorisé le Premier ministre à prendre, hormis sur les territoires dans lesquels l'article 2 de la même loi proroge l'état d'urgence sanitaire, à compter du 11 juillet 2020 et jusqu'au 30 octobre 2020 inclus, diverses mesures dans l'intérêt de la santé publique et aux seules fins de lutter contre la propagation de l'épidémie de covid-19. Dans ce cadre, en application du I de l'article 1er de la loi, le Premier ministre peut notamment, par décret pris sur le rapport du ministre chargé de la santé, réglementer ou interdire la circulation des personnes et des véhicules, ainsi que l'accès aux moyens de transport collectif et les conditions de leur usage et réglementer l'ouverture, y compris les conditions d'accès et de présence, des établissements recevant du public. Le II de cet article dispose que " Lorsque le Premier ministre prend des mesures mentionnées au I, il peut habiliter le représentant de l'Etat territorialement compétent à prendre toutes les mesures générales ou individuelles d'application de ces dispositions. Lorsque les mesures prévues au même I doivent s'appliquer dans un champ géographique qui n'excède pas le territoire d'un département, le Premier ministre peut habiliter le représentant du département à les décider lui-même ". Le III de cet article prévoit que ces mesures sont strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu. Il y est mis fin sans délai lorsqu'elles ne sont plus nécessaires.  Pris pour l'application de la loi précitée, le décret du 10 juillet 2020 prescrit les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans les territoires sortis de l'état d'urgence sanitaire et dans ceux où il a été prorogé. <br/>
<br/>
              3. Aux termes de l'article 1er du décret du 10 juillet 2020, dans sa rédaction initiale : " I. - Afin de ralentir la propagation du virus, les mesures d'hygiène définies en annexe 1 au présent décret et de distanciation sociale, incluant la distanciation physique d'au moins un mètre entre deux personnes, dites barrières, définies au niveau national, doivent être observées en tout lieu et en toute circonstance. / II. - Les rassemblements, réunions, activités, accueils et déplacements ainsi que l'usage des moyens de transports qui ne sont pas interdits en vertu du présent décret sont organisés en veillant au strict respect de ces mesures. " Aux termes de l'article 2 du même décret : " (...) Les obligations de port du masque prévues au présent décret ne s'appliquent pas aux personnes en situation de handicap munies d'un certificat médical justifiant de cette dérogation et qui mettent en œuvre les mesures sanitaires de nature à prévenir la propagation du virus (...) ". Les articles 8, 11, 15, 21, 27, 36, 38, 40, 44, 45 et 47 du même décret rendent obligatoire l'usage du masque aux personnes de onze ans ou plus dans les transports et les établissements recevant du public. Le décret du 30 juillet 2020 modifiant le décret du 10 juillet 2020, dont M. D... demande l'annulation, a complété le II de son article 1er par la phrase suivante : " Dans les cas où le port du masque n'est pas prescrit par le présent décret, le préfet de département est habilité à le rendre obligatoire, sauf dans les locaux d'habitation, lorsque les circonstances locales l'exigent ".<br/>
<br/>
              4. Il résulte des dispositions de la loi du 9 juillet 2020 citées au point 2 que le Premier ministre peut habiliter le préfet à prendre lui-même, lorsqu'elles doivent s'appliquer dans un champ géographique qui n'excède pas le territoire du département, des mesures relatives à la circulation des personnes et à l'accès aux moyens de transport ainsi qu'aux établissements et espaces ouverts au public, aux seules fins de lutter contre la propagation de l'épidémie de covid 19. Ce pouvoir de police administrative, qui inclut la possibilité d'imposer le port d'un masque dans des cas où le décret lui-même ne l'exige pas, s'exerce sous le contrôle du juge, qui en apprécie la nécessité, la proportionnalité et le caractère approprié. Le moyen tiré de ce que le décret contesté méconnaîtrait la répartition des compétences entre le législateur et le pouvoir réglementaire et n'encadrerait pas suffisamment les pouvoirs du préfet ne peut, par suite, qu'être écarté.<br/>
<br/>
              5. Il résulte de ce qui précède que M. D... n'est pas fondé à demander l'annulation du décret du 30 juillet 2020.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. D... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. C... D....<br/>
Copie en sera adressée au ministre des solidarités et de la santé, au ministre de l'intérieur et au ministre des outre-mer.<br/>
              Délibéré à l'issue de la séance du 15 décembre 2021 où siégeaient : M. Bertrand Dacosta, président de chambre, présidant ; Mme Nathalie Escaut, conseillère d'Etat et Mme Myriam Benlolo Carabot, maître des requêtes en service extraordinaire-rapporteure.<br/>
<br/>
              Rendu le 28 décembre 2021.<br/>
Le président : <br/>
Signé : M. Bertrand Dacosta<br/>
La rapporteure : <br/>
Signé : Mme Myriam Benlolo Carabot<br/>
La secrétaire :<br/>
Signé : Mme A... B...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
