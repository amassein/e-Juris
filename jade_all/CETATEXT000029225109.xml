<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029225109</ID>
<ANCIEN_ID>JG_L_2014_07_000000368559</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/22/51/CETATEXT000029225109.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 11/07/2014, 368559, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368559</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:368559.20140711</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 15 mai et 13 août 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Orange, dont le siège est 1, avenue Nelson Mandela à Arcueil (94745) ; la société Orange demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement n° 1001901 du 15 mars 2013 en tant que, par ce jugement, le tribunal administratif de Rennes a annulé, à la demande de Mme F...C..., l'arrêté du 27 novembre 2009 par lequel le maire de Châteaulin ne s'est pas opposé à la déclaration préalable déposée par la société Orange France pour l'installation de trois antennes de radiotéléphonie et l'implantation d'un local technique sur une parcelle cadastrée section AB n° 356, ainsi que le rejet du recours gracieux formé à l'encontre de cet arrêté ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de Mme C...; <br/>
<br/>
              3°) de mettre à la charge de MmeC..., M. A...B..., M. D...et M. E...la somme globale de 4 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la société Orange ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que par un arrêté du 27 novembre 2009, le maire de Châteaulin (29150) ne s'est pas opposé à la déclaration préalable de travaux présentée par la société Orange, consistant en l'installation de trois antennes de téléphonie sur la toiture d'un bâtiment sis 39, rue de la gare à Châteaulin, à l'intérieur d'une fausse cheminée en composite, et en l'aménagement d'un local technique sous la terrasse de ce bâtiment ; que MmeC..., et MM. A...B..., D...et E...ont saisi le tribunal administratif de Rennes d'une demande tendant à l'annulation de cet arrêté ; que, par un jugement du 15 mars 2013, le tribunal administratif de Rennes a, d'une part, rejeté la requête comme irrecevable en tant qu'elle était présentée par MM. A...B..., D...etE..., d'autre part, faisant droit à la demande de MmeC..., annulé cet arrêté, au motif que les travaux projetés ne relevaient pas du régime de la déclaration préalable de travaux mais du permis de construire ; que la société Orange se pourvoit en cassation contre ce jugement, en tant qu'il annule l'arrêté du 27 novembre 2009 ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 421-1 du code de l'urbanisme, dans sa rédaction applicable à l'arrêté litigieux : " Les constructions, même ne comportant pas de fondations, doivent être précédées de la délivrance d'un permis de construire. / Un décret en Conseil d'Etat arrête la liste des travaux exécutés sur des constructions existantes ainsi que des changements de destination qui, en raison de leur nature ou de leur localisation, doivent également être précédés de la délivrance d'un tel permis. " ; qu'aux termes de l'article L. 421-4 de ce code, dans sa rédaction applicable à l'arrêté litigieux : " Un décret en Conseil d'Etat arrête la liste des constructions, aménagements, installations et travaux qui, en raison de leurs dimensions, de leur nature ou de leur localisation, ne justifient pas l'exigence d'un permis et font l'objet d'une déclaration préalable. / (...) " ; qu'aux termes de l'article R. 421-1 du code, dans sa rédaction applicable à l'arrêté litigieux : " Les constructions nouvelles doivent être précédées de la délivrance d'un permis de construire, à l'exception : / a) Des constructions mentionnées aux articles R. 421-2 à R. 421-8 qui sont dispensées de toute formalité au titre du code de l'urbanisme ; / b) Des constructions mentionnées aux articles R. 421-9 à R. 421-12 qui doivent faire l'objet d'une déclaration préalable. " ; qu'aux termes de l'article R. 421-13 du code, dans sa rédaction applicable à l'arrêté litigieux : " Les travaux exécutés sur des constructions existantes sont dispensés de toute formalité au titre du code de l'urbanisme à l'exception : / a) Des travaux mentionnés aux articles R. 421-14 à R. 421-16, qui sont soumis à permis de construire ; / b) Des travaux mentionnés à l'article R. 421-17, qui doivent faire l'objet d'une déclaration préalable. / (...) " ; <br/>
<br/>
              3. Considérant que l'implantation d'une antenne de radiotéléphonie mobile sur la toiture d'un immeuble constitue une opération de travaux exécutés sur une construction existante ; que si ce type d'ouvrage, pour être soumis à simple déclaration préalable, doit respecter les critères fixés par les articles R. 421-14 et R. 421-17 du code de l'urbanisme et, notamment, avoir pour effet, pour l'ensemble constitué par la ou les antennes-relais et par l'armoire technique, la création d'une surface hors oeuvre brute comprise entre deux et vingt mètres carrés, en revanche, sa hauteur est sans incidence sur la détermination du régime applicable ; que, par suite, en jugeant que l'implantation d'une antenne de radiotéléphonie sur le toit d'un immeuble devait respecter les règles relatives aux constructions nouvelles et faire l'objet d'un permis de construire en vertu des dispositions combinées des articles R. 421-1 et R. 421-9 du code de l'urbanisme, lorsque la hauteur de l'ouvrage au-dessus du sol est supérieure à 12 mètres et la surface hors oeuvre brute créée supérieure à 2 mètres carrés, le tribunal administratif de Rennes a commis une erreur de droit ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, que la société Orange est fondée à demander l'annulation du jugement qu'elle attaque en tant que par ce jugement, le tribunal administratif a annulé l'arrêté du 27 novembre 2009 du maire de Châteaulin ne s'opposant pas à la déclaration préalable de la société Orange France ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui a été dit au point 1 que MM. A...B..., D...et E...ne sont pas parties à l'instance devant le Conseil d'Etat ; que, par suite, les conclusions présentées par la société Orange sur le fondement de l'article L. 761-1 du code de justice administrative, tendant à ce qu'une somme soit mise à leur charge ne peuvent qu'être rejetées ; qu'en revanche, il y a lieu, dans les circonstance de l'espèce, de mettre à la charge de Mme C...la somme de 500 euros à verser à la société Orange, au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Rennes du 15 mars 2013 est annulé en tant qu'il a fait droit à la demande présentée par MmeC.... <br/>
<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, au tribunal administratif de Rennes. <br/>
<br/>
Article 3 : Mme C...versera à la société Orange une somme de 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.  <br/>
Article 4 : La présente décision sera notifiée à la société Orange, à Mme F...C...et à la commune de Châteaulin. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
