<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000022413149</ID>
<ANCIEN_ID>JG_L_2010_06_000000336106</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/22/41/31/CETATEXT000022413149.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 24/06/2010, 336106</TITRE>
<DATE_DEC>2010-06-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>336106</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Vigouroux</PRESIDENT>
<AVOCATS>SCP PEIGNOT, GARREAU</AVOCATS>
<RAPPORTEUR>Mme Paquita  Morellet-Steiner</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Escaut Nathalie</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2010:336106.20100624</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le mémoire, enregistré le 30 avril 2010 au secrétariat du contentieux du Conseil d'Etat, présenté pour M. Jean-Noël A, demeurant ..., en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 ; M. A demande au Conseil d'Etat, à l'appui de son pourvoi tendant à l'annulation de l'arrêt du 9 décembre 2008 par lequel la cour régionale des pensions de Montpellier a annulé le jugement du 12 février 2008 du tribunal départemental des pensions de l'Hérault lui accordant la revalorisation de sa pension d'invalidité, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article L. 78 du code des pensions militaires d'invalidité et des victimes de la guerre ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son article 61-1 ; <br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
<br/>
              Vu l'article L. 78 du code des pensions militaires d'invalidité et des victimes de la guerre ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Paquita Morellet-Steiner, Maître des Requêtes,<br/>
<br/>
              - les observations de la SCP Peignot, Garreau, avocat de M. A, <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Peignot, Garreau, avocat de M. A ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : "Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...)" ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soulevée soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              Considérant, d'une part, que le moyen tiré de la méconnaissance du droit à un recours effectif devant une juridiction ne pose pas de question nouvelle ;<br/>
<br/>
              Considérant, d'autre part, que, si l'article L. 78 du code des pensions militaires d'invalidité permet de demander, sans condition de délai, la révision d'une pension, le motif invoqué par M. A devant la cour régionale des pensions de Montpellier tiré de l'illégalité résultant de la différence entre l'indice de la pension d'invalidité fixé par le décret du 5 septembre 1956 pour les sergents de l'armée de terre par rapport à l'indice attaché au grade équivalent dans la marine nationale, n'est pas au nombre de ceux susceptibles de lui ouvrir cette procédure qui ne s'applique qu'en cas d'erreur, d'inexactitude ou d'omission matérielle portant sur la liquidation ou sur les informations personnelles du pensionné, au vu desquelles l'arrêté de concession a été pris ; que, dès lors qu'en vertu des dispositions de l'article L. 79 du même code, M. A pouvait, dans le délai fixé par l'article 5 du décret du 20 février 1959, contester les arrêtés par lesquels lui avait été concédée une pension d'invalidité pour tout motif de droit et notamment pour celui qu'il a invoqué devant la cour régionale des pensions de Montpellier, la question tirée de ce que les dispositions de l'article L. 78 de ce code méconnaissent le droit à un recours effectif devant une juridiction garanti par l'article 16 de la Déclaration des droits de l'homme et du citoyen ne présente pas, par suite, un caractère sérieux ; qu'ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que les dispositions de l'article L. 78 du code des pensions civiles et militaires d'invalidité portent atteinte aux droits et libertés garantis par la Constitution doit être écarté ; <br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée.<br/>
Article 2 : La présente décision sera notifiée à M. Jean-Noël A, au Premier ministre et au ministre de la défense.<br/>
		Copie en sera adressée au Conseil constitutionnel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">48-01-03-04-03 PENSIONS. PENSIONS MILITAIRES D'INVALIDITÉ ET DES VICTIMES DE GUERRE. CARACTÈRE DES PENSIONS CONCÉDÉES. RÉVISION DES PENSIONS CONCÉDÉES. RÉVISION DE L'ARTICLE L. 78. - QUESTION PRIORITAIRE DE CONSTITUTIONNALITÉ - PRINCIPE DU DROIT À UN RECOURS EFFECTIF (ART. 16 DDHC) - QUESTION NE PRÉSENTANT PAS UN CARACTÈRE SÉRIEUX - DROIT AU RECOURS ASSURÉ PAR L'ARTICLE L. 79 DU CPMI.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-10-05-04-02 PROCÉDURE. - DEMANDE EN RÉVISION (ART. L. 78 DU CPMI) - PRINCIPE DU DROIT À UN RECOURS EFFECTIF (ART. 16 DDHC) - DROIT AU RECOURS ASSURÉ PAR L'ARTICLE L. 79 DU CPMI.
</SCT>
<ANA ID="9A"> 48-01-03-04-03 Si l'article L. 78 du code des pensions militaires d'invalidité et des victimes de guerre (CPMI) ne permet qu'un recours en révision de la pension pour erreur, inexactitude ou omission matérielle portant sur la liquidation ou sur les informations personnelles du pensionné, l'article L. 79 permet à ce dernier de contester, dans les délais fixés par l'article 5 du décret n° 58-327 du 20 février 1959 relatif aux juridictions des pensions, les arrêtés lui concédant une pension d'invalidité pour tout motif de droit. Dès lors, la question de la méconnaissance, par l'article L. 78, du droit au recours effectif garanti par l'article 16 de la Déclaration des droits de l'homme et du citoyen (DDHC) ne présente par un caractère sérieux.</ANA>
<ANA ID="9B"> 54-10-05-04-02 Si l'article L. 78 du code des pensions militaires d'invalidité et des victimes de guerre (CPMI) ne permet qu'un recours en révision de la pension pour erreur, inexactitude ou omission matérielle portant sur la liquidation ou sur les informations personnelles du pensionné, l'article L. 79 permet à ce dernier de contester, dans les délais fixés par l'article 5 du décret n° 58-327 du 20 février 1959 relatif aux juridictions des pensions, les arrêtés lui concédant une pension d'invalidité pour tout motif de droit. Dès lors, la question de la méconnaissance, par l'article L. 78, du droit au recours effectif garanti par l'article 16 de la Déclaration des droits de l'homme et du citoyen (DDHC) ne présente par un caractère sérieux.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
