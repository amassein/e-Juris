<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031184138</ID>
<ANCIEN_ID>JG_L_2015_09_000000364465</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/18/41/CETATEXT000031184138.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 18/09/2015, 364465, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-09-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364465</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Emmanuelle Petitdemange</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:364465.20150918</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Kampexport International a demandé au tribunal administratif de Rennes de prononcer la réduction des rappels de la taxe affectée au Centre technique interprofessionnel des fruits et légumes mis à sa charge au titre de la période du 1er janvier 2004 au 30 septembre 2007. Par un jugement n°s 0803269, 0903121 et 1000224 du 26 mai 2011, le tribunal administratif de Rennes a rejeté ses demandes.<br/>
<br/>
              Par un arrêt n° 11NT01671 du 11 octobre 2012, la cour administrative d'appel de Nantes a rejeté l'appel formé par la société Kampexport International contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 12 décembre 2012, 27 février 2013 et 2 juin 2015 au secrétariat du contentieux du Conseil d'Etat, la société Kampexport International demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 2003-1312 du 30 décembre 2003 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Emmanuelle Petitdemange, auditeur,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de la société Kampexport International et à la SCP Foussard, Froger, avocat du centre technique interprofessionnel des fruits et légumes ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 73 de la loi du 30 décembre 2003 de finances rectificatives pour 2003, alors applicable : "  A. - I. - Il est créé une taxe dont le produit est affecté (...) au Centre technique interprofessionnel des fruits et légumes. (...). II. - La taxe est due par les personnes assurant la production ou le commerce de gros de plantes aromatiques à usage culinaire, de fruits et légumes frais, secs ou séchés, à l'exception des pommes de terre de conservation ou des bananes (...).  III. - La taxe est due sur les opérations suivantes : / 1° La dernière transaction en gros entre deux personnes portant sur les produits mentionnés au II, qu'ils soient d'origine française ou importés de pays n'appartenant pas à la Communauté européenne. Les transactions portant sur les produits en provenance des Etats membres de la Communauté européenne sont exonérées de la taxe ; (...) / La taxe est due par le vendeur lorsque celui-ci est établi en France. Elle figure de façon distincte sur la facture fournie à l'acheteur. /  (...) " ; que sont mentionnés aux IV à X de ce même A l'assiette et le taux de la taxe, le fait générateur et l'exigibilité, constitués par la livraison, les modalités de déclaration et de paiement de la taxe ainsi que les pouvoirs du Centre technique en matière de contrôle des déclarations et de redressement des omissions ou insuffisance de déclaration ;  qu'en vertu du B du même article : " Un décret en Conseil d'Etat précise, en tant que de besoin, les modalités d'application du présent article " ; que le C prévoit que : "  Les dispositions du A entrent en vigueur au 1er janvier 2004 " ; <br/>
<br/>
              2. Considérant, en premier lieu, qu'en jugeant que l'insuffisance de la motivation de la décision rejetant la réclamation contentieuse de la société Kampexport International tendant à la réduction de la taxe affectée au Centre technique interprofessionnel des fruits et légumes au titre de la période du 1er janvier 2004 au 30 septembre 2007 était sans incidence sur la régularité de la procédure d'imposition comme sur le bien-fondé de la taxe en litige, la cour a suffisamment motivé son arrêt ;<br/>
<br/>
              3. Considérant, en deuxième lieu, que l'article 73 de la loi du 30 décembre 2003 définissait les opérations soumises à la taxe ainsi que ses redevables ; qu'il en précisait l'assiette, le fait générateur, le taux, les modalités de déclaration, de paiement, de recouvrement et de contrôle ainsi que le régime contentieux ; que ces dispositions ne comportaient aucune ambiguïté quant aux redevables concernés, à leur implantation géographique ou à l'application du principe de territorialité de l'impôt dès lors notamment que le II du A de cet article assujettissait à la taxe toutes les personnes assurant la production ou le commerce de gros et que le III du A indiquait que la taxe est due sur la dernière livraison en gros par le vendeur lorsque celui-ci était établi en France ; qu'il en résulte que l'application de l'article 73 de la loi du 30 décembre 2003 n'était pas rendue manifestement impossible par l'absence de décret auquel le B de cet article renvoyait, en tant que de besoin, pour préciser ses modalités d'application ; que, par suite, la cour n'a pas commis d'erreur de droit en jugeant que ces dispositions étaient immédiatement entrées en vigueur, alors même que n'était pas intervenu de décret d'application ;<br/>
<br/>
              4. Considérant, en troisième lieu, qu'en estimant que la société requérante n'apportait pas la preuve de ce que ses clients étaient eux-mêmes des grossistes, alors qu'elle avait produit de nombreux bordereaux de transit, des lettres de voiture internationale et des factures à destination de clients de l'Union européenne mentionnant des livraisons de fruits et légumes en quantités excédant manifestement le cadre de ventes au détail, la cour a dénaturé les pièces du dossier qui lui était soumis ; que, toutefois, en soumettant à la taxe la dernière transaction en gros entre deux personnes portant sur les produits mentionnés à l'article 73 de la loi du 30 décembre 2003 et en en rendant redevable le vendeur lorsque celui-ci est établi en France, cet article impliquait nécessairement, eu égard au principe de territorialité de l'impôt, que la transaction en gros entre un vendeur établi en France et un acheteur établi à l'étranger fût regardée comme la dernière transaction en gros au sens du 1° du III du A de cet article, alors même que cet acheteur était un grossiste ; qu'ainsi, les ventes de la société effectuées à destination de grossistes d'autres Etats membres de l'Union européenne étaient placées dans le champ d'application de la taxe ; que ce motif de pur droit, qui répond à un moyen invoqué devant les juges du fond et dont l'examen n'implique l'appréciation d'aucune circonstance de fait, doit être substitué au motif de l'arrêt attaqué dont il justifie légalement le dispositif ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que le pourvoi de la société Kampexport International doit être rejeté ; <br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du Centre technique interprofessionnel des fruits et légumes, qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la société Kampexport International la somme de 1 500 euros à verser au Centre technique interprofessionnel des fruits et légumes sur le même fondement ;<br/>
<br/>
<br/>
<br/>
<br/>
              	         D E C I D E :<br/>
                               --------------<br/>
<br/>
Article 1er : Le pourvoi de la société Kampexport International est rejeté.<br/>
<br/>
Article 2 : La société Kampexport International versera au Centre technique interprofessionnel des fruits et légumes la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société Kampexport International, au Centre technique interprofessionnel des fruits et légumes et au ministre de l'agriculture, de l'agroalimentaire et de la forêt.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
