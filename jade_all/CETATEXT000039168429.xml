<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039168429</ID>
<ANCIEN_ID>JG_L_2019_10_000000418666</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/16/84/CETATEXT000039168429.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 02/10/2019, 418666</TITRE>
<DATE_DEC>2019-10-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418666</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU ; SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>M. Thibaut Félix</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:418666.20191002</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C... E... et l'association La Clave et le Bas Estéron ont demandé au tribunal administratif de Nice d'annuler pour excès de pouvoir, d'une part, l'arrêté du 16 juillet 2013 par lequel le maire de la commune du Broc a délivré à la société civile immobilière La Clave le permis de construire une maison d'habitation avec piscine dans le lieu-dit La Clave, sur quatre parcelles cadastrées nos E 138, 139, 140 et 1076 et, d'autre part, d'annuler l'arrêté du 11 juin 2013 par lequel le maire de la commune a délivré à M. D... F..., aux droits duquel est venue Mme B... A..., le permis de construire une maison d'habitation dans le même lieu-dit, sur une parcelle cadastrée n° E 143. Par un jugement nos 1303310, 1303314 du 26 février 2016, le tribunal administratif de Nice a rejeté ces demandes.<br/>
<br/>
              Par un arrêt n° 16MA01736 du 28 décembre 2017, la cour administrative d'appel de Marseille a, sur l'appel de l'association La Clave et le Bas Estéron, annulé ce jugement et les arrêtés attaqués du 16 juillet 2013 et du 11 juin 2013.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 28 février, 28 mai et 18 décembre 2018 au secrétariat du contentieux du Conseil d'Etat, la commune du Broc et la SCI La Clave demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de l'association La Clave et le Bas Estéron ;<br/>
<br/>
              3°) de mettre à la charge de cette association la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - la loi n° 2010-788 du 12 juillet 2010 ;<br/>
              - le décret n° 2003-1169 du 2 décembre 2003 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thibaut Félix, auditeur,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la commune du Broc et de la SCI la Clave ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le maire du Broc a délivré à M. D... F..., par un arrêté du 11 juin 2013, le permis de construire une maison d'habitation sur une parcelle cadastrée n° E 143 et à la SCI La Clave, par un arrêté du 16 juillet 2013, le permis de construire une maison d'habitation avec piscine sur quatre parcelles cadastrées nos E 138, 139, 140 et 1076, dans le lieu-dit la Clave, en secteur NBa de la zone NB du plan d'occupation des sols de la commune, zone naturelle desservie partiellement par des équipements qu'il n'est pas prévu de renforcer et dans lesquelles des constructions ont déjà été édifiées. Par un jugement du 26 février 2016, le tribunal administratif de Nice a rejeté le recours de M. E... et de l'association La Clave et le Bas Estéron tendant à l'annulation de ces permis de construire. Toutefois, par un arrêt du 28 décembre 2017, contre lequel la commune du Broc et la SCI La Clave se pourvoient en cassation, la cour administrative d'appel de Marseille a, sur l'appel de l'association, annulé ces permis. <br/>
<br/>
              2. En premier lieu, l'article NB 1 du règlement du plan d'occupation des sols de la commune du Broc, alors en vigueur, autorise les constructions à usage d'habitation, dans la limite d'un volume par tranche de 1 500 mètres carrés de terrain dans le secteur NBa et par tranche de 2 500 mètres carrés dans le secteur NBb. Aux termes de l'article NB 4 du même règlement : " Toute construction ou installation requérant une alimentation en eau potable doit être raccordée au réseau public d'eau potable ". <br/>
<br/>
              3. Il résulte de ces dispositions que les constructions à usage d'habitation ne sont admises en zone NB qu'à la condition de respecter les différentes dispositions du règlement du plan d'occupation des sols dans la zone, dont, en particulier, l'exigence de raccordement au réseau public d'eau potable prévue à l'article NB 4 du règlement. Par suite, en jugeant que, alors même que le secteur NBa où se situent les terrains d'assiette des projets litigieux n'était pas desservi par un réseau public d'eau potable, les permis de construire attaqués méconnaissaient les dispositions précitées de l'article NB 4 du règlement du plan d'occupation des sols, au motif que les deux projets en litige n'étaient pas raccordés au réseau public d'eau potable, la cour, qui n'a pas donné de ces dispositions une interprétation les entachant de contradiction, n'a pas commis d'erreur de droit.<br/>
<br/>
              4. En second lieu, aux termes du cinquième alinéa de l'article L. 111-1-1 du code de l'urbanisme, dans sa rédaction applicable au litige : " Les dispositions des directives territoriales d'aménagement qui précisent les modalités d'application des articles L. 145-1 et suivants sur les zones de montagne (...) s'appliquent aux personnes et opérations qui y sont mentionnées ". Aux termes du second alinéa de l'article L. 145-2 du même code, applicable au litige : " Les directives territoriales d'aménagement précisant les modalités d'application des dispositions du présent chapitre ou, en leur absence, lesdites dispositions sont applicables à toute personne publique ou privée pour l'exécution de tous travaux, constructions (...) ". Enfin, aux termes du III de l'article L. 145-3 du même code, dans sa rédaction applicable au litige, dont les dispositions sont désormais reprises aux articles L. 122-5, L. 122-5-1 et L. 122-6 du même code, l'urbanisation en zone de montagne " doit se réaliser en continuité avec les bourgs, villages, hameaux, groupes de constructions traditionnelles ou d'habitations existants. / Lorsque la commune est dotée d'un plan local d'urbanisme ou d'une carte communale, ce document peut délimiter les hameaux et groupes de constructions traditionnelles ou d'habitations existants en continuité desquels il prévoit une extension de l'urbanisation, en prenant en compte les caractéristiques traditionnelles de l'habitat, les constructions implantées et l'existence de voies et réseaux. / Lorsque la commune n'est pas dotée d'un plan local d'urbanisme ou d'une carte communale, les notions de hameaux et de groupes de constructions traditionnelles ou d'habitations existants doivent être interprétées en prenant en compte les critères mentionnés à l'alinéa précédent ".<br/>
<br/>
              5. D'une part, il résulte de ces dispositions qu'il appartient à l'autorité administrative chargée de se prononcer sur une demande d'autorisation d'occupation ou d'utilisation du sol mentionnée au second alinéa de l'article L. 145-2 du code de l'urbanisme de s'assurer, sous le contrôle du juge de l'excès de pouvoir, de la conformité du projet aux dispositions du code de l'urbanisme particulières à la montagne, le cas échéant au regard des prescriptions d'une directive territoriale d'aménagement demeurée en vigueur qui sont suffisamment précises et compatibles avec les dispositions des articles L. 145-1 et suivants du même code. <br/>
<br/>
              6. D'autre part, il résulte des dispositions du III de l'article L. 145-3 du code de l'urbanisme, éclairées par les travaux préparatoires de la loi du 2 juillet 2003 urbanisme et habitat qui les a modifiées, que l'urbanisation en zone de montagne, sans être autorisée en zone d'urbanisation diffuse, peut être réalisée non seulement en continuité avec les bourgs, villages et hameaux existants, mais également en continuité avec les " groupes de constructions traditionnelles ou d'habitations existants " et qu'est ainsi possible l'édification de constructions nouvelles en continuité d'un groupe de constructions traditionnelles ou d'un groupe d'habitations qui, ne s'inscrivant pas dans les traditions locales, ne pourrait être regardé comme un hameau. L'existence d'un tel groupe suppose plusieurs constructions qui, eu égard notamment à leurs caractéristiques, à leur implantation les unes par rapport aux autres et à l'existence de voies et de réseaux, peuvent être perçues comme appartenant à un même ensemble. <br/>
<br/>
              7. S'il ressort des pièces du dossier soumis aux juges du fond que les projets litigieux s'inscrivent dans les " espaces naturels " délimités par la carte 19 de la directive territoriale d'aménagement des Alpes-Maritimes, approuvée par décret du 2 décembre 2003, le point III-132-3 de cette directive admet, dans les espaces naturels, " le confortement (...) des groupes de constructions traditionnelles ou d'habitations existants ". Le point III-132-4-4 prévoit en outre que " dans la frange sud de la zone montagne ", " les bourgs et villages sont constitués de "vieux villages" et de quartiers nouveaux, intégrant les hameaux, groupes de constructions traditionnelles et groupes d'habitations, qui comprennent un nombre significatif de maisons très proches les unes des autres ", tandis que les secteurs d'urbanisation diffuse sont définis comme ceux où s'est développé un habitat de faible densité, soit 2 à 4 maisons à l'hectare. <br/>
<br/>
              8. Pour juger que les projets litigieux n'étaient pas situés en continuité avec un groupe d'habitations existant, la cour a relevé que les habitations existantes dans ce secteur, au nombre d'une dizaine, étaient espacées de 25 à 40 mètres et que le secteur n'était pas desservi par les réseaux d'eau et d'assainissement. En se fondant sur ces critères pour juger, au terme d'une appréciation souveraine non arguée de dénaturation, portée au regard des prescriptions de la directive territoriale d'aménagement des Alpes-Maritimes, que les permis attaqués méconnaissaient les dispositions de l'article L. 145-3 du code de l'urbanisme, la cour n'a pas commis d'erreur de droit. <br/>
<br/>
              9. Il résulte de tout ce qui précède, sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée au pourvoi par l'association La Clave et le Bas Estéron, que la commune du Broc et la SCI La Clave ne sont pas fondées à demander l'annulation de l'arrêt de la cour administrative d'appel de Marseille qu'elles attaquent.<br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'association La Clave et le Bas Estéron, qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune du Broc et de la SCI La Clave le versement à l'association La Clave et le Bas Estéron d'une somme de 1 500 euros chacune au titre de ces dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de la commune du Broc et de la SCI La Clave est rejeté.<br/>
Article 2 : La commune du Broc et la SCI La Clave verseront chacune à l'association La Clave et le Bas Estéron une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à la commune du Broc, à la société civile immobilière La Clave et à l'association La Clave et le Bas Estéron.<br/>
Copie en sera adressée à Mme B... A....<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-001-01-02-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES GÉNÉRALES D'UTILISATION DU SOL. RÈGLES GÉNÉRALES DE L'URBANISME. PRESCRIPTIONS D'AMÉNAGEMENT ET D'URBANISME. RÉGIME ISSU DE LA LOI DU 9 JANVIER 1985 SUR LA MONTAGNE. - RÈGLE D'URBANISATION EN CONTINUITÉ - NOTION DE GROUPES DE CONSTRUCTIONS TRADITIONNELLES OU D'HABITATIONS EXISTANTS (III DE L'ART. L. 145-3 DU CODE DE L'URBANISME) - CONSTRUCTIONS POUVANT ÊTRE PERÇUES COMME APPARTENANT À UN MÊME ENSEMBLE.
</SCT>
<ANA ID="9A"> 68-001-01-02-01 Il résulte du III de l'article L. 145-3 du code de l'urbanisme, éclairées par les travaux préparatoires de la loi n° 2003-590 du 2 juillet 2003 qui les a modifiées, que l'urbanisation en zone de montagne, sans être autorisée en zone d'urbanisation diffuse, peut être réalisée non seulement en continuité avec les bourgs, villages et hameaux existants, mais également en continuité avec les groupes de constructions traditionnelles ou d'habitations existants et qu'est ainsi possible l'édification de constructions nouvelles en continuité d'un groupe de constructions traditionnelles ou d'un groupe d'habitations qui, ne s'inscrivant pas dans les traditions locales, ne pourrait être regardé comme un hameau. L'existence d'un tel groupe suppose plusieurs constructions qui, eu égard notamment à leurs caractéristiques, à leur implantation les unes par rapport aux autres et à l'existence de voies et de réseaux, peuvent être perçues comme appartenant à un même ensemble.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
