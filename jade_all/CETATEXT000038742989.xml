<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038742989</ID>
<ANCIEN_ID>JG_L_2019_07_000000419367</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/74/29/CETATEXT000038742989.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 08/07/2019, 419367, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419367</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Louise Cadin</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2019:419367.20190708</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 419367, par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés les 23 mars 2018, 25 septembre 2018 et 8 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, l'association 40 millions d'automobilistes demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir, d'une part, la décision rendue publique à l'issue du comité interministériel de la sécurité routière du 2 octobre 2015 de déléguer la conduite de voitures équipées de radars à des opérateurs privés, et, d'autre part, la décision fixant les conditions d'application de cette première décision, rendue publique par un communiqué de presse du 20 février 2017 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              2° Sous le n° 424410, par une requête et un mémoire en réplique, enregistrés les 21 septembre et 13 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, l'association 40 millions d'automobilistes demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir, d'une part, la décision implicite du 23 juillet 2018 par laquelle le Premier ministre a rejeté sa demande tendant à l'abrogation de la décision rendue publique à l'issue du comité interministériel de la sécurité routière du 2 octobre 2015, de déléguer la conduite de voitures équipées de radars à des opérateurs privés, et, d'autre part, la décision implicite du 22 juillet 2018 par laquelle le ministre de l'intérieur a rejeté sa demande tendant à l'abrogation de sa décision en fixant les conditions d'application, rendue publique par un communiqué de presse du 20 février 2017 ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre et au ministre de l'intérieur d'abroger ces décisions dans un délai de deux mois à compter de la présente décision ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la Constitution ; <br/>
              - le code du travail ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le décret n° 75-360 du 15 mai 1975 ;<br/>
              - le décret n° 2011-184 du 15 février 2011 ;<br/>
              - le décret n° 2014-1094 du 26 septembre 2014 ;<br/>
              - le décret n° 2014-1217 du 21 octobre 2014 ;<br/>
              - l'arrêté du 26 septembre 2014 portant création des comités techniques des services déconcentrés de police nationale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Louise Cadin, auditrice,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur l'intervention de la commune de Naujac-sur-Mer :<br/>
<br/>
              1. La commune de Naujac-sur-Mer ne justifie pas, en sa seule qualité de collectivité territoriale, d'un intérêt suffisant à l'annulation des décisions attaquées. La circonstance que son maire ait pris un arrêté interdisant la circulation de voitures équipées de radars sur le territoire de la commune n'est pas davantage de nature à lui conférer un tel intérêt. Ainsi, son intervention n'est pas recevable.<br/>
<br/>
              Sur la portée des décisions attaquées :<br/>
<br/>
              2. D'une part, aux termes de l'article 1er du décret du 15 mai 1975 relatif au comité interministériel de la sécurité routière susvisé : " Il est institué un comité interministériel de la sécurité routière, chargé de définir la politique du Gouvernement dans le domaine de la sécurité routière, et de s'assurer de son application. / (...) Le comité interministériel est placé sous la présidence du Premier ministre ou par délégation de celui-ci sous la présidence du ministre des transports ". Aux termes de l'article 2 de ce décret : " Le comité interministériel de la sécurité routière arrête les mesures générales destinées à améliorer la sécurité routière ". <br/>
<br/>
              3. Le 2 octobre 2015, le Comité interministériel de la sécurité routière a adopté la mesure suivante : " Augmenter, dans les meilleurs délais, l'utilisation des radars embarqués dans des véhicules banalisés, en confiant leur mise en oeuvre à des prestataires agréés, sous étroit contrôle de l'Etat ". Il résulte des dispositions citées ci-dessus que cette mesure doit être regardée comme une décision réglementaire du Premier ministre concernant l'organisation du service public de la sécurité routière et portant sur la délégation à des prestataires privés la conduite de voitures équipées de radars. <br/>
<br/>
              4. D'autre part, en vertu de l'article 3 du décret du 15 mai 1975 : " Un délégué interministériel à la sécurité routière, nommé par décret en conseil des ministres et placé sous l'autorité du Premier ministre, assure le secrétariat du comité interministériel. A ce titre, il veille à l'élaboration des mesures susceptibles d'améliorer la sécurité routière, prépare les délibérations du comité et suit l'application des décisions prises (...) ".<br/>
<br/>
              5. Un communiqué de presse comportant les mentions suivantes a été publié le 20 février 2017 sur le site Internet de la sécurité routière : " Vendredi 24 février 2017 débutera en Normandie l'expérimentation qui permettra, au 1er septembre prochain, que la conduite des voitures-radar lancées en 2013 soit confiée à des prestataires privés./ Ce test de plusieurs mois est destiné à permettre l'homologation du nouveau système et ne donnera lieu à aucune contravention./ La première voiture-radar conduite par un opérateur privé est programmée pour septembre 2017 en Normandie. Le dispositif d'externalisation sera ensuite progressivement étendu aux autres régions. Ce seront, dès lors, des entreprises qui fourniront des chauffeurs pour conduire, sous étroit contrôle de l'État, ces véhicules banalisés, comme décidé par le Comité interministériel de la sécurité routière du 2 octobre 2015 (mesure n°2) et dans les conditions précisées par le ministre de l'Intérieur, Bruno LE ROUX, le 8 janvier dernier lors du Conseil national de la sécurité routière (CNSR) (...) ". Sont en outre précisés le rôle des futurs prestataires, les modalités de leur rémunération ainsi que la marge de tolérance en cas de constat de dépassement des vitesses autorisées. Dans les termes où il est rédigé, ce communiqué doit être regardé comme révélant l'existence d'une décision, présentant un caractère réglementaire, prise par le délégué interministériel à la sécurité routière par délégation du Premier ministre, d'expérimenter l'externalisation de la conduite des voitures équipées de radars dans la région Normandie, avant sa généralisation à l'ensemble du territoire national, selon les modalités définies par ce même communiqué. <br/>
<br/>
              Sur la légalité externe des décisions attaquées :<br/>
<br/>
              En ce qui concerne la compétence du pouvoir réglementaire :<br/>
<br/>
              6. En vertu du deuxième alinéa de l'article 34 de la Constitution, la loi fixe les règles concernant la procédure pénale, au nombre desquelles figurent, notamment, la détermination des catégories de personnes compétentes pour constater les infractions aux dispositions pénalement sanctionnées, en rassembler les preuves et en rechercher les auteurs, ainsi que les modalités suivant lesquelles ces personnes exécutent leurs missions. Il ressort de la décision rendue publique par le communiqué de presse du 20 février 2017 que les salariés des entreprises prestataires doivent effectuer leur mission en respectant les trajets et les plages horaires de contrôle définis par l'administration, que ni ces conducteurs ni les entreprises qui les emploient n'ont accès aux données relatives aux infractions relevées par les radars et que seuls les officiers de police judiciaire chargés de constater les infractions auront accès à des données. Les décisions litigieuses n'ont ainsi ni pour objet ni pour effet de permettre à une nouvelle catégorie de personnes de rassembler les preuves d'infractions pénales et d'en rechercher les auteurs, et se bornent à déléguer à des tiers privés l'exécution de tâches matérielles qui concourent aux missions de police judiciaire, lesquelles restent dévolues aux forces de l'ordre. Par suite, l'association requérante n'est pas fondée à soutenir que les décisions litigieuses sont intervenues dans le domaine réservé à la loi par la Constitution.<br/>
<br/>
              En ce qui concerne la compétence au sein du pouvoir réglementaire :<br/>
<br/>
              7. Ainsi qu'il a été dit aux points 3 et 5, les décisions litigieuses doivent être regardées comme des décisions réglementaires du Premier ministre, qui était compétent pour les adopter en vertu de l'article 21 de la Constitution. <br/>
<br/>
              En ce qui concerne la consultation des comités techniques :<br/>
<br/>
              8. Aux termes de l'article 34 du décret du 15 février 2011 relatif aux comités techniques dans les administrations et les établissements publics de l'Etat : " les comités techniques sont consultés (...) sur les questions et projets de textes relatifs : 1° à l'organisation et au fonctionnement des administrations, établissements ou services ". Aux termes de l'article 1er du décret du 26 septembre 2014 instituant un comité technique de réseau de la direction générale de la police nationale : " Par dérogation à l'article 5 du décret du 15 février 2011 susvisé, il est institué auprès du directeur général de la police nationale un comité technique de réseau compétent pour connaître des questions communes concernant les services placés sous l'autorité du directeur général et les services de police de la préfecture de police ". Aux termes de l'arrêté du 26 septembre 2014 portant création des comités techniques des services déconcentrés de la police nationale : " Il est institué dans chaque département auprès du préfet et dans le département des Bouches-du-Rhône auprès du préfet de police un comité technique des services déconcentrés de la police nationale ayant compétence dans le cadre du titre III du décret du 15 février 2011 susvisé pour connaître de toutes les questions concernant les services déconcentrés de la police nationale dans le ressort territorial du département ". Enfin l'article 1er du décret du 21 octobre 2014 institue un comité technique de la gendarmerie nationale " compétent pour l'ensemble des composantes de la gendarmerie nationale, mentionnées à l'article R. 3225-4 du code de la défense ".<br/>
<br/>
              9. La décision prise par le Premier ministre à l'issue du comité interministériel de la sécurité routière du 2 octobre 2015, qui se borne à adopter le principe de la délégation de la conduite de voitures équipées de radars à des entreprises privées au niveau national, n'a pas affecté par elle-même, en l'absence de définition suffisante des modalités de mise en oeuvre de cette mesure, l'organisation et le fonctionnement des services de police et de gendarmerie. Par suite, l'administration n'était pas tenue de consulter, avant de prendre cette décision, le comité technique de réseau de la police nationale et le comité technique de la gendarmerie nationale, contrairement à ce que soutient la requérante. <br/>
<br/>
              10. La décision rendue publique par le communiqué de presse du 20 février 2017, en tant qu'elle prévoit les conditions dans lesquelles la délégation à des entreprises privées de la conduite de voitures équipées de radars sera seulement testée en Normandie, n'emporte pas à ce stade de conséquences sur l'organisation et le fonctionnement ni des services de police et de gendarmerie dans leur ensemble, ni des services déconcentrés de police et de gendarmerie de la région Normandie. Dans ces conditions, l'administration n'était tenue de consulter, avant de prendre cette décision, ni le comité technique de réseau de la direction générale de la police nationale, ni le comité technique de la direction générale de la gendarmerie nationale, ni les comités techniques des services déconcentrés concernés en région Normandie. En revanche, de telles consultations sont exigées avant de procéder à l'extension du dispositif pérenne sur tout ou partie du territoire, laquelle affecte nécessairement l'organisation et le fonctionnement des services de police et de gendarmerie. <br/>
<br/>
              Sur la légalité interne : <br/>
<br/>
              En ce qui concerne l'interdiction de déléguer une mission de police à une personne privée :<br/>
<br/>
              11. Les décisions litigieuses ne permettent de déléguer à des personnes privées que la seule tâche matérielle de conduite de véhicules équipés de radars, accessoire aux missions de police qui restent dévolues aux forces de l'ordre. Il ressort en outre des pièces du dossier que l'administration a prévu que les trajets effectués par les véhicules des prestataires seraient déterminés sous l'étroit contrôle des services de l'Etat et que les conducteurs n'auraient accès ni aux matériels de contrôle, ni aux données relatives à la constatation des infractions. Par suite, la requérante n'est pas fondée à soutenir que les décisions attaquées méconnaîtraient l'interdiction de déléguer une mission de police à une personne privée.<br/>
<br/>
              En ce qui concerne l'atteinte à liberté d'aller et venir :<br/>
<br/>
              12. Le choix de déléguer la conduite des voitures équipées de radars à une personne privée ne porte pas, par lui-même, atteinte à la liberté d'aller et venir. La requérante ne peut, dès lors, utilement soutenir que les décisions litigieuses porteraient à cette liberté une atteinte disproportionnée.<br/>
<br/>
              En ce qui concerne l'interdiction de prêt de main d'oeuvre <br/>
<br/>
              13. Aux termes de l'article L. 8241-1 du code du travail : " toute opération à but lucratif ayant pour objet exclusif le prêt de main d'oeuvre est interdite ". Il ressort des pièces du dossier que les décisions litigieuses ont pour objet de confier à des entreprises privées une prestation consistant à organiser la circulation de véhicules appartenant à l'administration, dans le cadre d'un cahier des charges défini par celle-ci et sous la seule responsabilité de l'encadrement des sociétés prestataires, et non de mettre à la disposition de l'Etat des personnels qui seraient placés sous l'autorité hiérarchique directe des services de police et de gendarmerie. Les contrats passés à cette fin ne sauraient donc être regardés comme ayant pour objet exclusif un prêt de main d'oeuvre. Le moyen tiré de ce que l'administration aurait, en prenant les décisions litigieuses, méconnu les dispositions précitées du code du travail doit, par suite, être écarté. <br/>
<br/>
              En ce qui concerne la mise à la disposition de l'Etat de personnels de droit privé :<br/>
<br/>
              14. Aux termes de l'article 43 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat : " Les administrations et les établissements publics administratifs de l'Etat peuvent, lorsque des fonctions exercées en leur sein nécessitent une qualification technique spécialisée, bénéficier, dans les cas et conditions définis par décret en Conseil d'Etat, de la mise à disposition de personnels de droit privé. Cette mise à disposition est assortie du remboursement par l'Etat ou l'établissement public des rémunérations, charges sociales, frais professionnels et avantages en nature des intéressés et de la passation d'une convention avec leurs employeurs. / Les personnels mentionnés à l'alinéa précédent sont soumis aux règles d'organisation et de fonctionnement du service où ils servent et aux obligations s'imposant aux fonctionnaires ". Ainsi qu'il a été dit au point précédent, les  décisions litigieuses n'ont pas pour effet de placer l'administration en situation de signer avec des personnes privées des contrats ayant pour objet exclusif la mise à disposition de personnels. Par suite, les requérantes ne peuvent utilement soutenir que les décisions litigieuses méconnaîtraient les dispositions précitées. <br/>
<br/>
              15. Il résulte de ce qui précède que, sans qu'il soit besoin de statuer sur les fins de non-recevoir opposées par le ministre, l'association requérante n'est pas fondée à demander l'annulation des décisions litigieuses. <br/>
<br/>
              Sur les conclusions présentées par l'association requérante au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              16. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de la commune de Naujac-sur-Mer n'est pas admise.<br/>
Article 2 : Les requêtes de l'association 40 millions d'automobilistes sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à l'association 40 millions d'automobilistes, au Premier ministre et au ministre de l'Intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
