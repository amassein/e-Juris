<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030956709</ID>
<ANCIEN_ID>JG_L_2015_07_000000385882</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/95/67/CETATEXT000030956709.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème SSJS, 27/07/2015, 385882, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385882</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>Mme Anne Egerszegi</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:385882.20150727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. D...C...a saisi le tribunal administratif de Montreuil d'une protestation tendant, d'une part, à annuler les opérations électorales qui se sont déroulées les 23 et 30 mars 2014 pour l'élection des conseillers municipaux et communautaires dans la commune de Noisy-le-Sec (Seine-Saint-Denis), et, par voie de conséquence, l'élection de M. A... B..., élu maire, et de ses adjoints et, d'autre part, à prononcer l'inéligibilité de M. B.... Par un jugement n°1402881 du 21 octobre 2014, le tribunal administratif de Montreuil a rejeté sa protestation.<br/>
<br/>
              Par une requête, enregistrée le 21 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, M. C...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de faire droit à sa protestation.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Egerszegi, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de M. A...B...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il résulte de l'instruction qu'à l'issue des opérations électorales du second tour qui se sont déroulées le 30 mars 2014 pour l'élection des conseillers municipaux de la commune de Noisy-le-Sec, la liste conduite par M. B...a obtenu 5 835 voix, et la liste conduite par M. E..., sur laquelle M. C...figurait en troisième position, a obtenu 4 146 voix. La liste conduite par M. B...a obtenu 34 des 43 sièges de conseillers municipaux qui étaient à pourvoir. M. C...a demandé au tribunal administratif de Montreuil, d'une part, d'annuler les opérations électorales, et, par voie de conséquence, l'élection du maire et de ses adjoints, et, d'autre part, de déclarer M. B...inéligible. Par un jugement du 21 octobre 2014, le tribunal administratif de Montreuil a rejeté sa protestation et M. C...relève appel de ce jugement.<br/>
<br/>
              Sur les conclusions tendant à l'annulation des opérations électorales :<br/>
<br/>
              En ce qui concerne le moyen tiré de ce que le déroulement de la campagne électorale aurait méconnu les dispositions de l'article L. 52-1 et du deuxième alinéa de l'article L. 52-8 du code électoral :<br/>
<br/>
              2. En premier lieu, aux termes de l'article L. 52-1 du code électoral : " Pendant les six mois précédant le premier jour du mois d'une élection et jusqu'à la date du tour de scrutin où celle-ci est acquise, l'utilisation à des fins de propagande électorale de tout procédé de publicité commerciale par la voie de la presse ou par tout moyen de communication audiovisuelle est interdite. / A compter du premier jour du sixième mois précédant le mois au cours duquel il doit être procédé à des élections générales, aucune campagne de promotion publicitaire des réalisations ou de la gestion d'une collectivité ne peut être organisée sur le territoire des collectivités intéressées par le scrutin. Sans préjudice des dispositions du présent chapitre, cette interdiction ne s'applique pas à la présentation, par un candidat ou pour son compte, dans le cadre de l'organisation de sa campagne, du bilan de la gestion des mandats qu'il détient ou qu'il a détenus. Les dépenses afférentes sont soumises aux dispositions relatives au financement et au plafonnement des dépenses électorales contenues au chapitre V bis du présent titre." / Aux termes du deuxième alinéa de l'article L. 52-8 du code électoral : " Les personnes morales, à l'exception des partis ou groupements politiques, ne peuvent participer au financement de la campagne électorale d'un candidat, ni en lui consentant des dons sous quelque forme que ce soit, ni en lui fournissant des biens, services ou autres avantages directs ou indirects à des prix inférieurs à ceux qui sont habituellement pratiqués."<br/>
<br/>
              3. M. C...soutient que M. B...a mené une campagne de promotion prohibée par les dispositions précitées de l'article L. 52-1 du code électoral en adressant au personnel de la commune une lettre annonçant une revalorisation de leur régime indemnitaire, aux électeurs de la commune une lettre revendiquant son action contre le tracé de la ligne de tramway T1 et aux locataires de la société d'économie mixte Noisy-le-Sec Habitat des lettres vantant les réalisations de cette société qu'il préside en sa qualité de maire.  M. C...soutient également que la prise en charge par la société d'économie mixte Noisy-le-Sec Habitat des frais d'envoi de la lettre adressée le 27 février 2014 par M. B...aux locataires de cette société, a constitué un don prohibé par les dispositions précitées du deuxième alinéa de l'article L. 52-8 du code électoral. Il résulte toutefois de l'instruction que, en premier lieu, la lettre relative à la revalorisation du régime indemnitaire des agents, comme cela ressort notamment d'une note de la direction générale des services municipaux du 13 janvier 2014, s'inscrit dans un processus de négociation avec les organisations représentatives du personnel initié en 2012. En deuxième lieu, la lettre relative au tracé du tramway T1 a été diffusée en février 2013, en dehors de la période de campagne électorale. En troisième lieu, la première des lettres adressées aux locataires de la société Noisy-le-Sec Habitat est contenue dans un bulletin d'information publié tous les six mois et la seconde, en date du 27 février 2014, a été adressée par le directeur général délégué de la société afin de porter à la connaissance des locataires le contenu de la réponse adressée par M. B... à des propos tenus par deux des membres de la liste conduite par M. E...mettant en cause le fonctionnement interne de la société. <br/>
<br/>
              4. M. C...fait valoir également que la municipalité a multiplié les inaugurations dans les trois mois précédant le scrutin, notamment les 6 et 8 février 2014 et le 8 mars 2014. Il résulte toutefois de l'instruction que les cérémonies organisées par la municipalité l'ont été à l'occasion de manifestations qui se déroulent chaque année, telle le " Téléthon ", ou sont liées à des projets initiés antérieurement à la période électorale, comme l'inauguration du square Winkopp ou la pose de la première pierre de l'extension du gymnase Pierre de Coubertin.<br/>
<br/>
              5. M. C...soutient en outre qu'un tract de la Fédération musulmane de Noisy-le-Sec, appelant au soutien de la liste menée par M.B..., a fait l'objet d'une diffusion massive, de nature à caractériser une campagne de promotion publicitaire prohibée par les dispositions précitées de l'article L. 52-1 du code électoral et à constituer également un don prohibé par les dispositions précitées du deuxième alinéa de l'article L. 52-8 du code électoral. Il résulte toutefois de l'instruction, en tout état de cause, que le caractère massif de la diffusion de ce document, au demeurant destiné seulement à soutenir le projet de la construction d'une mosquée à Noisy-le-Sec, n'est pas établi.<br/>
<br/>
              6. M. C...soutient également que M. B... a adressé des lettres aux nouveaux électeurs inscrits à compter du 1er janvier 2014 ainsi qu'aux abstentionnistes du premier tour, en utilisant pour ce faire du matériel communal et en faisant peser le coût de cette opération sur les finances communales, en violation des dispositions précitées du deuxième alinéa de l'article L. 52-8 du code électoral. Il n'est toutefois pas établi que des fichiers appartenant à la commune auraient été utilisés pour effectuer l'envoi de ces lettres aux électeurs concernés. Il résulte par ailleurs de l'instruction que les frais d'impression de ces lettres ont été intégrés dans le compte de campagne de M.B..., ainsi que l'indique la décision de la Commission nationale des comptes de campagne et des financements politiques du 7 juillet 2014.<br/>
<br/>
              7. M. C...soutient par ailleurs que, d'une part, M. B... a utilisé sur la page " Facebook " de la liste " En avant Noisy avec Laurent B...", sur un document établissant le bilan de son mandat et sur un document de programme, des photographies émanant de promoteurs et d'aménageurs privés ainsi que de la société Noisy-le-Sec Habitat et que, d'autre part, l'agenda 2014 diffusé par la ville a été financé par des entreprises pour faire la promotion du maire, en violation des dispositions précitées du deuxième alinéa de l'article L. 52-8 du code électoral. Il résulte toutefois de l'instruction que les photographies en cause ont été prises par un photographe amateur ou publiées avec l'autorisation de la société en charge du projet de construction de la ZAC de l'Ourcq, la Commission nationale des comptes de campagne et des financements politiques ayant, au demeurant, dans sa décision du 7 juillet 2014, relevé que ces photographies avaient été mises à disposition des élus de la commune et étaient libres de droit. Il ressort par ailleurs de l'instruction que l'agenda 2014 ne présente aucun caractère promotionnel en faveur de l'action du maire sortant.<br/>
<br/>
              8. Il résulte de ce qui précède que les moyens tirés de la violation des dispositions de l'article L. 52-1 et du deuxième alinéa de l'article L. 52-8 du code électoral doivent être écartés.<br/>
<br/>
              En ce qui concerne les moyens tirés de ce que le déroulement des opérations électorales aurait méconnu les dispositions des articles L. 98 et L. 106 du code électoral : <br/>
<br/>
              9. En premier lieu, aux termes de l'article L. 98 du code électoral : " Lorsque, par attroupements, clameurs ou démonstrations menaçantes, on aura troublé les opérations d'un collège électoral, porté atteinte à l'exercice du droit électoral ou à la liberté du vote, les coupables seront punis d'un emprisonnement de deux ans et d'une amende 15 000 euros ". S'il n'appartient pas au juge de l'élection de faire application de ces dispositions en ce qu'elles édictent des sanctions pénales, il lui revient, en revanche, de rechercher si des attroupements, des clameurs ou des démonstrations menaçantes, tels que définis par celles-ci, ont constitué des pressions de nature à altérer la sincérité du scrutin.<br/>
<br/>
              10. M. C...soutient que de graves irrégularités ont été constatées par les délégués et assesseurs de plusieurs listes qui les ont mentionnées aux procès-verbaux des bureaux de vote n° 10, 13, 14, 17 et 18, que ces irrégularités ont été observées par les services de police et que des pressions ont été effectuées, notamment par des agents de la commune, pour inciter les électeurs à voter pour la liste conduite par M.B.... M. C...n'assortit cependant d'aucun commencement de preuve ses allégations relatives aux irrégularités qui auraient entaché les opérations électorales se déroulant dans les bureaux de vote n°10, 13 et 14 . Il ne résulte par ailleurs pas de l'instruction que le regroupement devant les bureaux de vote n° 17 et n° 18 de personnes portant des écharpes bleues soutenant la liste conduite par le défendeur serait constitutive d'une pression exercée sur les électeurs, alors qu'au demeurant un attroupement de personnes portant une écharpe rouge en soutien de la liste à laquelle appartenait le protestataire s'était également formé devant le bureau de vote n° 17 et que les altercations entre les personnes devant le bureau de vote n° 17 ont concerné les soutiens des deux listes. <br/>
<br/>
              11. En second lieu, aux termes de l'article L. 106 du code électoral : " Quiconque, par des dons ou libéralités en argent ou en nature, par des promesses de libéralités, de faveurs, d'emplois publics ou privés ou d' autres avantages particuliers, faits en vue d'influencer le vote d'un ou de plusieurs électeurs aura obtenu ou tenté d'obtenir leur suffrage, soit directement, soit par l'entremise d'un tiers, quiconque, par les mêmes moyens, aura déterminé ou tenté de déterminer un ou plusieurs d'entre eux à s'abstenir, sera puni de deux ans d'emprisonnement et d'une amende de 15 000 euros " . S'il n'appartient pas au juge de l'élection de faire application de ces dispositions en ce qu'elles édictent des sanctions pénales, il lui revient, en revanche, de rechercher si des pressions telles que définies par celles-ci ont été exercées sur les électeurs et ont été de nature à altérer la sincérité du scrutin. Dans les circonstances de l'espèce, la circonstance, au demeurant non établie, que des promesses auraient été faites par l'adjointe au maire déléguée au logement à une électrice afin de l'inciter à voter pour la liste conduite par M. B... n'est pas, eu égard à l'écart important des voix, de nature à remettre en cause les résultats du scrutin.<br/>
<br/>
              12. Il résulte de ce qui précède que les moyens tirés de la violation des dispositions des articles L. 98 et L. 106 du code électoral doivent être écartés.<br/>
<br/>
              13. Il résulte de tout ce qui précède, et sans qu'il soit besoin de se prononcer sur la fin de non-recevoir soulevée par M.B..., que M. C... n'est pas fondé à soutenir que c'est à tort que le tribunal administratif de Montreuil a rejeté sa protestation.<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              14. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. C...est rejetée.<br/>
<br/>
Article 2 : Les conclusions présentées par M. B...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. D...C...et à M. A... B....<br/>
Copie en sera adressée au ministre de l'intérieur et à la Commission nationale des comptes de campagne et des financements politiques.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
