<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030465451</ID>
<ANCIEN_ID>JG_L_2015_04_000000372011</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/46/54/CETATEXT000030465451.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 09/04/2015, 372011, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-04-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372011</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR>M. Denis Rapone</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:372011.20150409</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. C...F...a demandé au tribunal administratif de Lyon d'annuler pour excès de pouvoir l'arrêté du 7 juillet 2010 par lequel le maire de Tassin-la-Demi-Lune a accordé un permis de construire valant division à MM. C...D..., A...G...et H...E.... Par un jugement n° 1007463 du 11 octobre 2012, le tribunal administratif de Lyon a annulé cet arrêté.<br/>
<br/>
              Par un arrêt n° 12LY02996 du 9 juillet 2013, la cour administrative d'appel de Lyon a, à la demande de la commune de Tassin-la-Demi-Lune, annulé ce jugement en tant qu'il annulait les dispositions de l'arrêté du 7 juillet 2010 autorisant la construction du bâtiment n° 1, rejeté dans cette mesure les conclusions de la demande présentée par M. F...devant le tribunal administratif et rejeté le surplus des conclusions de l'appel de la commune. <br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 9 septembre et 9 décembre 2013 au secrétariat du contentieux du Conseil d'Etat, la commune de Tassin-la-Demi-Lune demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt de la cour administrative d'appel de Lyon du 9 juillet 2013 en tant qu'il rejette partiellement son appel ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à celles de ses conclusions d'appel qui ont été rejetées par la cour ;<br/>
<br/>
              3°) de mettre à la charge de M. F...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier.<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Denis Rapone, conseiller d'Etat, <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat de la commune de Tassin-la-Demi-Lune.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article R. 123-10-1 du code de l'urbanisme, dans sa rédaction applicable à la date du permis de construire en litige : " Dans le cas d'un lotissement ou dans celui de la construction, sur un même terrain, de plusieurs bâtiments dont le terrain d'assiette doit faire l'objet d'une division en propriété ou en jouissance, les règles édictées par le plan local d'urbanisme sont appréciées au regard de l'ensemble du projet, sauf si le règlement de ce plan s'y oppose ". Il résulte de ces dispositions, applicables notamment aux permis de construire, que si les règles d'un plan local d'urbanisme relatives à l'implantation des constructions par rapport aux limites séparatives s'appliquent à l'ensemble des constructions d'un lotissement dans leurs relations avec les parcelles situées à l'extérieur du périmètre de ce lotissement, elles ne sont pas, sauf prescription contraire du plan, applicables à l'implantation des constructions à l'intérieur de ce périmètre. Par suite, en énonçant que les dispositions alors applicables de l'article R. 123-10-1 du code de l'urbanisme " n'impliquent pas que les limites entre les lots ne puissent être prises en compte pour l'application des règles de recul par rapport aux limites séparatives au moment de la délivrance d'un permis de construire dans le lotissement ", la cour a commis une erreur de droit. <br/>
<br/>
              2. Il résulte de ce qui précède que la commune de Tassin-la-Demi-Lune est fondée à demander l'annulation de l'arrêt qu'elle attaque en tant qu'il se prononce sur la légalité de l'arrêté du 7 juillet 2010 en ce qu'il autorise la construction des bâtiments n° 2 et 3. Le motif d'erreur de droit retenu suffisant à entraîner cette annulation, il n'est pas nécessaire de statuer sur l'autre moyen du pourvoi.<br/>
<br/>
              3. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. F... une somme de 1 500 euros à verser à la commune de Tassin-la-Demi-Lune au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 9 juillet 2013 est annulé en tant qu'il rejette les conclusions d'appel de la commune de Tassin-la-Demi-Lune dirigées contre le jugement du tribunal administratif de Lyon du 11 octobre 2012 annulant l'arrêté du 7 juillet 2010 en ce qu'il autorise la construction des bâtiments n° 2 et 3.<br/>
Article 2 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Lyon.<br/>
Article 3 : M. F...versera une somme de 1 500 euros à la commune de Tassin-la-Demi-Lune au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la commune de Tassin-la-Demi-Lune et à M. B... F....<br/>
Copie en sera adressée à MM. C...D..., A...G...et H...E....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
