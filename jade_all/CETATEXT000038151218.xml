<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038151218</ID>
<ANCIEN_ID>JG_L_2019_02_000000424881</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/15/12/CETATEXT000038151218.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 20/02/2019, 424881, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424881</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER ; CORLAY</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Nevache</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:424881.20190220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif de Toulouse, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre sous astreinte au président du conseil départemental de l'Aveyron de l'accueillir provisoirement pour une durée de cinq jours en prenant notamment en charge son hébergement et son alimentation et, en cas de carence de ce dernier, d'enjoindre au préfet de l'Aveyron de lui proposer un lieu d'hébergement adapté. <br/>
<br/>
              Par une ordonnance n°s 1804404, 1804405, 1804406 du 20 septembre 2018, le juge des référés du tribunal administratif de Toulouse a rejeté sa demande.<br/>
<br/>
              Par un pourvoi, enregistré le 15 octobre 2018 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code civil ; <br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Nevache, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de M.A..., et à Me Corlay, avocat du département de l'Aveyron.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. En vertu de l'article L. 221-1 du code de l'action sociale et des familles, le service de l'aide sociale à l'enfance est un service du département chargé notamment de " mener en urgence des actions de protection " en faveur des mineurs confrontés à des difficultés risquant de mettre en danger leur santé, leur sécurité ou leur moralité ou de compromettre gravement leur éducation ou leur développement physique, affectif, intellectuel et social. Aux termes des deuxième et quatrième alinéas de l'article L. 223-2 du même code : " En cas d'urgence et lorsque le représentant légal du mineur est dans l'impossibilité de donner son accord, l'enfant est recueilli provisoirement par le service qui en avise immédiatement le procureur de la République. / (...) / Si, dans le cas prévu au deuxième alinéa du présent article, l'enfant n'a pas pu être remis à sa famille ou le représentant légal n'a pas pu ou a refusé de donner son accord dans un délai de cinq jours, le service saisit également l'autorité judiciaire en vue de l'application de l'article 375-5 du code civil ". L'article R. 221-11 du même code prévoit que : " I.- Le président du conseil départemental du lieu où se trouve une personne se déclarant mineure et privée temporairement ou définitivement de la protection de sa famille met en place un accueil provisoire d'urgence d'une durée de cinq jours, à compter du premier jour de sa prise en charge, selon les conditions prévues aux deuxième et quatrième alinéas de l'article L. 223-2. /  II.- Au cours de la période d'accueil provisoire d'urgence, le président du conseil départemental procède aux investigations nécessaires en vue d'évaluer la situation de cette personne au regard notamment de ses déclarations sur son identité, son âge, sa famille d'origine, sa nationalité et son état d'isolement. (...) / III.- L'évaluation est réalisée par les services du département, ou par toute structure du secteur public ou du secteur associatif à laquelle la mission d'évaluation a été déléguée par le président du conseil départemental. (...) / IV.- Au terme du délai mentionné au I, ou avant l'expiration de ce délai si l'évaluation a été conduite avant son terme, le président du conseil départemental saisit le procureur de la République en vertu du quatrième alinéa de l'article L. 223-2 et du second alinéa de l'article 375-5 du code civil. En ce cas, l'accueil provisoire d'urgence mentionné au I se prolonge tant que n'intervient pas une décision de l'autorité judiciaire. / S'il estime que la situation de la personne mentionnée au présent article ne justifie pas la saisine de l'autorité judiciaire, il notifie à cette personne une décision de refus de prise en charge délivrée dans les conditions des articles L. 222-5 et R. 223-2. En ce cas, l'accueil provisoire d'urgence mentionné au I prend fin ".<br/>
<br/>
              2. Il résulte de ces dispositions que, sous réserve des cas où la condition de minorité ne serait à l'évidence pas remplie, il incombe aux autorités du département de mettre en place un accueil provisoire d'urgence pour toute personne se déclarant mineure et privée temporairement ou définitivement de la protection de sa famille, confrontée à des difficultés risquant de mettre en danger sa santé, sa sécurité ou sa moralité, en particulier parce qu'elle est sans abri. Cet accueil, qui doit notamment permettre de procéder aux investigations nécessaires en vue d'évaluer la situation du jeune au regard, en particulier, de ses déclarations sur son identité, son âge, sa famille d'origine, sa nationalité et son état d'isolement, se prolonge jusqu'à l'intervention de la décision de l'autorité judiciaire, si le président du conseil départemental estime que sa situation justifie la saisine de cette autorité, ou prend fin lorsqu'il notifie à l'intéressé une décision de refus de prise en charge, dans l'hypothèse inverse. Lorsqu'elle entraîne des conséquences graves pour l'intéressé, une carence caractérisée dans l'accomplissement de cette mission porte une atteinte grave et manifestement illégale à une liberté fondamentale.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis au juge des référés que M.A..., indiquant être né le 25 janvier 2003 en Guinée, est entré en France, selon ses dires, en septembre 2018 et est arrivé à Rodez le 13 septembre 2018. Pris en charge par l'antenne locale de la Ligue des droits de l'homme, il a sollicité téléphoniquement sa prise en charge par le service de l'aide sociale à l'enfance du département de l'Aveyron et a saisi le département et le procureur de la République par télécopie le 14 septembre 2018, puis a réitéré sa demande auprès du département par le même moyen le 17 septembre 2018. En l'absence de réponse, il a saisi le juge des référés du tribunal administratif de Toulouse, sur le fondement de l'article L. 521-2 du code de justice administrative, en lui demandant d'enjoindre sous astreinte au président du conseil départemental de l'accueillir provisoirement pour une durée de cinq jours et, en cas de carence de ce dernier, d'enjoindre au préfet de l'Aveyron de lui proposer un lieu d'hébergement adapté. <br/>
<br/>
              4. Toutefois, il ressort des pièces du dossier que, postérieurement à l'introduction de son pourvoi contre l'ordonnance du 20 septembre 2018 par laquelle le juge des référés a rejeté sa demande, M.A..., mis à l'abri par les services du département de l'Aveyron, a fait l'objet d'une évaluation sociale puis a été confié au service de l'aide sociale à l'enfance du département de la Haute-Garonne par une ordonnance de placement provisoire du 23 octobre 2018, dont il n'est pas contesté qu'elle a été mise en oeuvre. <br/>
<br/>
              5. Par suite, les conclusions de M. A...tendant à l'annulation de l'ordonnance du juge des référés du tribunal administratif  de Toulouse du 20 septembre 2018 sont devenues sans objet. Il n'y a, dès lors, pas lieu d'y statuer. <br/>
<br/>
              6. Dans les circonstances de l'espèce, il n'y a pas lieu de faire droit aux conclusions de la SCP Célice, Soltner, Texidor, Perier présentées sur le fondement des articles L.  761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, qui sont dirigées contre l'Etat.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions du pourvoi de M. A...dirigées contre l'ordonnance du juge des référés du tribunal administratif  de Toulouse du 20 septembre 2018.<br/>
Article 2 : Les conclusions de la SCP Célice, Soltner, Texidor, Perier présentées sur le fondement des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. B... A...et au département de l'Aveyron. <br/>
Copie en sera adressée à la ministre des solidarités et de la santé. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
