<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028842831</ID>
<ANCIEN_ID>JG_L_2014_04_000000353219</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/84/28/CETATEXT000028842831.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème / 10ème SSR, 11/04/2014, 353219, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-04-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353219</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème / 10ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Jean-Marie Deligne</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:353219.20140411</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 7 octobre 2011 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre du budget, des comptes publics et de la réforme de l'Etat ; le ministre demande au Conseil d'Etat d'annuler l'arrêt n° 10VE01600 du 21 juillet 2011 par lequel la cour administrative d'appel de Versailles a fait droit à la requête de M. B...A...tendant à l'annulation du jugement n° 0700723 du 25 mars 2010 du tribunal administratif de Versailles rejetant sa demande de décharge du rappel de taxe sur la valeur ajoutée qui lui a été réclamé au titre de la période correspondant à l'année 2003 et des pénalités correspondantes ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la directive 77/388/CEE du Conseil du 17 mai 1977 ;<br/>
<br/>
              Vu le code général des impôts ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marie Deligne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, Trichet, avocat de M. A...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A..., titulaire d'un brevet d'Etat d'éducateur sportif, exerce depuis le 1er octobre 2000, en tant que travailleur indépendant, la profession libérale de moniteur dans la discipline de l'escalade, sous la dénomination " Les arts des cimes " ; qu'à ce titre, il a dispensé, dans le cadre d'un contrat le liant à l'office municipal de l'éducation physique et des sports de la ville de Châtillon, des cours d'initiation à l'escalade dans des écoles primaires à raison de deux jours par semaine ; qu'à l'issue d'un contrôle, l'administration a assujetti M. A...à la taxe sur la valeur ajoutée à raison des recettes tirées de son activité ; que le ministre chargé du budget se pourvoit en cassation contre l'arrêt du 21 juillet 2011 par lequel la cour administrative d'appel de Versailles a fait droit à la requête de M. A...tendant à l'annulation du jugement du 25 mars 2010 du tribunal administratif de Versailles rejetant sa demande de décharge du rappel de taxe sur la valeur ajoutée qui lui avait été réclamé au titre de la période correspondant à l'année 2003 et des pénalités correspondantes ;<br/>
<br/>
              2. Considérant qu'aux termes du I de l'article 256 du code général des impôts : " Sont soumises à la taxe sur la valeur ajoutée les livraisons de biens et les prestations de services effectuées à titre onéreux par un assujetti agissant en tant que tel " ; qu'aux termes du premier alinéa de l'article 256 A du même code, dans sa rédaction alors applicable : " Sont assujettis à la taxe sur la valeur ajoutée les personnes qui effectuent de manière indépendante une des activités économiques mentionnées au cinquième alinéa, quels que soient le statut juridique de ces personnes, leur situation au regard des autres impôts et la forme ou la nature de leur intervention " ; qu'il résulte toutefois, des dispositions de l'article 261 de ce code que : " Sont exonérés de la taxe sur la valeur ajoutée : / 4 (...) 4° a. Les prestations de services et les livraisons de biens qui leur sont étroitement liées, effectuées dans le cadre : / de l'enseignement primaire, secondaire et supérieur dispensé dans les établissements publics et les établissements privés (...) / b. Les cours ou leçons relevant de l'enseignement scolaire, universitaire, professionnel, artistique ou sportif, dispensés par des personnes physiques qui sont rémunérées directement par leurs élèves " ; <br/>
<br/>
              3. Considérant que les dispositions précitées de l'article 261 du code général des impôts, doivent être interprétées à la lumière des points i et j de l'article 13 de la sixième directive du 17 mai 1977 en matière d'harmonisation des législations des États membres relatives aux taxes sur le chiffre d'affaires, dont elles assurent la transposition et aux termes desquels les exonérations ainsi prévues portent, respectivement, sur " l'éducation de l'enfance ou de la jeunesse, l'enseignement scolaire ou universitaire, la formation ou le recyclage professionnel, ainsi que les prestations de services et les livraisons de biens qui leur sont étroitement liées, effectués par des organismes de droit public de même objet ou par d'autres organismes reconnus comme ayant des fins comparables par l'État membre concerné " et sur " les leçons données, à titre personnel, par des enseignants et portant sur l'enseignement scolaire ou universitaire " ; qu'ainsi, l'exonération prévue par les dispositions du a doit être regardée comme limitée à l'éventuelle rémunération des cours et leçons donnés dans les établissements scolaires ou universitaires qu'elles mentionnent et perçue par ces derniers ; qu'elle ne s'étend pas à celle qui peut être perçue par la personne physique qui les a dispensés, dont les droits à exonération, lorsqu'elle est assujettie à la taxe sur la valeur ajoutée, sont exclusivement régis par les dispositions du b ; <br/>
<br/>
              4. Considérant qu'en jugeant, après avoir constaté que M. A...exerçait à titre libéral son activité d'enseignement d'initiation à l'escalade dans les écoles primaires de la ville de Châtillon en vertu d'un contrat le liant à l'office municipal d'éducation physique et des sports de Châtillon, que la rémunération qu'il percevait à raison de cette prestation était exonérée de la taxe sur la valeur ajoutée par application des dispositions du a du 4° du 4 de l'article 261 du code général des impôts, la cour a commis une erreur de droit ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, le ministre est fondé à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 21 juillet 2011 de la cour administrative d'appel de Versailles est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : Les conclusions présentées par M. A...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée au ministre des finances et des comptes publics et à M. B... A....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
