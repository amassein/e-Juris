<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034993692</ID>
<ANCIEN_ID>JG_L_2017_06_000000398104</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/99/36/CETATEXT000034993692.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 21/06/2017, 398104, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398104</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Vincent Ploquin-Duchefdelaville</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:398104.20170621</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Versailles de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre de l'année 2003, à raison de la taxation, dans la catégorie des revenus de capitaux mobiliers, de revenus distribués. Par un jugement n° 0909147 du 19 décembre 2013, ce tribunal, après avoir prononcé un non-lieu à statuer à concurrence d'une somme de 6 703 euros, a réduit les impositions litigieuses à concurrence de 30 500 euros en base et a rejeté le surplus des conclusions de cette demande.<br/>
<br/>
              Par un arrêt n° 14VE00541 du 21 janvier 2016, la cour administrative d'appel de Versailles a rejeté l'appel formé par M. A...contre l'article 3 de ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 21 mars et 21 juin 2016 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Ploquin-Duchefdelaville, auditeur,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de M.A....<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'une vérification de comptabilité d'une société, dont M. B...A...était associé et président-directeur général, l'administration a mis à la charge de ce dernier des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales au titre de l'année 2003, à raison de la taxation de revenus distribués par cette société. Par un jugement du 19 décembre 2013, le tribunal administratif de Versailles, après avoir constaté un non-lieu à statuer à hauteur d'un dégrèvement prononcé en cours d'instance, a réduit les impositions litigieuses et a rejeté le surplus de la demande. M. A...se pourvoit en cassation contre l'arrêt du 21 janvier 2016 par lequel la cour administrative d'appel de Versailles a rejeté son appel contre l'article 3 de ce jugement.<br/>
<br/>
              2. Aux termes de l'article L. 57 du livre des procédures fiscales : " L'administration adresse au contribuable une proposition de rectification qui doit être motivée de manière à lui permettre de formuler ses observations ou de faire connaître son acceptation ". Aux termes de l'article R. 57-1 du même livre : " La proposition de rectification prévue par l'article L. 57 fait connaître au contribuable la nature et les motifs de la rectification envisagée. L'administration invite, en même temps, le contribuable à faire parvenir son acceptation ou ses observations dans un délai de trente jours à compter de la réception de la proposition ". Il résulte de ces dispositions qu'un contribuable dispose d'un délai de trente jours, qui est un délai franc, pour faire connaître ses observations sur la proposition de rectification qui lui a été adressée par l'administration.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que M. A...a reçu une proposition de rectification le 13 avril 2006. Par suite, en jugeant que M. A...n'avait pas adressé ses observations dans le délai prévu par le livre des procédures fiscales, alors qu'il avait répondu le lundi 15 mai 2006, la cour a commis une erreur de droit. Dès lors, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, M. A...est fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 2 000 euros à verser à M.A..., au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>            D E C I D E :<br/>
                          --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 21 janvier 2016 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
<br/>
Article 3 : L'Etat versera à M. A...la somme de 2 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. B...A...et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
