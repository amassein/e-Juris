<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038351105</ID>
<ANCIEN_ID>JG_L_2019_04_000000417343</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/35/11/CETATEXT000038351105.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 05/04/2019, 417343, Publié au recueil Lebon</TITRE>
<DATE_DEC>2019-04-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417343</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT>Mme Anne Iljic</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEORD:2019:417343.20190405</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société à responsabilité limitée Margo Cinéma a demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir la décision du 27 janvier 2016 par laquelle la ministre de la culture et de la communication a délivré un visa d'exploitation au film documentaire intitulé " Salafistes ", assorti d'une interdiction de représentation publique aux mineurs de dix-huit ans et de l'avertissement suivant : " Ce film contient des propos et des images extrêmement violents et intolérants susceptibles de heurter le public ". Par un jugement n° 1601819/5-1 du 12 juillet 2016, le tribunal administratif de Paris a annulé cette décision en tant que la ministre a assorti le visa d'une interdiction aux mineurs de dix-huit ans.<br/>
<br/>
              Par un arrêt n° 16PA02615 du 14 novembre 2017, la cour administrative d'appel de Paris, sur appel de la ministre de la culture et de la communication, a annulé ce jugement et rejeté la demande présentée par la société Margo Cinéma devant le tribunal administratif de Paris.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 15 janvier et 13 avril 2018 et 27 février 2019 au secrétariat du contentieux du Conseil d'Etat, la société Margo Cinéma demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la ministre de la culture et de la communication ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code du cinéma et de l'image animée ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Raphaël Chambon, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Anne Iljic, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la société Margo Cinéma et à la SCP Piwnica, Molinié, avocat du ministre de la culture ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par une décision du 27 janvier 2016, la ministre de la culture et de la communication a accordé au film intitulé " Salafistes " un visa d'exploitation cinématographique assorti d'une interdiction de représentation aux mineurs de dix-huit ans et d'un avertissement. Saisi d'un recours pour excès de pouvoir formé par la société Margo Cinéma, le tribunal administratif de Paris, par un jugement du 12 juillet 2016, a annulé cette décision en tant que le ministre avait assorti le visa d'une interdiction aux mineurs de dix-huit ans. La société Margo Cinéma se pourvoit en cassation contre l'arrêt du 14 novembre 2017 par lequel la cour administrative d'appel de Paris, faisant droit à l'appel du ministre de la culture et de la communication, a annulé ce jugement et rejeté la demande qu'elle avait présentée devant le tribunal administratif de Paris.  <br/>
<br/>
              2. L'article L. 211-1 du code du cinéma et de l'image animée dispose que : " La représentation cinématographique est subordonnée à l'obtention d'un visa d'exploitation délivré par le ministre chargé de la culture. / Ce visa peut être refusé ou sa délivrance subordonnée à des conditions pour des motifs tirés de la protection de l'enfance et de la jeunesse ou du respect de la dignité humaine (...) ". Aux termes de l'article R. 211-10 du même code : " Le ministre chargé de la culture délivre le visa d'exploitation cinématographique aux oeuvres ou documents cinématographiques (...) destinés à une représentation cinématographique, après avis de la commission de classification des oeuvres cinématographiques (...) ". Aux termes de l'article R. 211-12 du même code, dans sa rédaction applicable à la date de la décision attaquée : " Le visa d'exploitation cinématographique s'accompagne de l'une des mesures de classification suivantes : / 1° Autorisation de la représentation pour tous publics ; /  2° Interdiction de la représentation aux mineurs de douze ans ; / 3° Interdiction de la représentation aux mineurs de seize ans ; / 4° Interdiction de la représentation aux mineurs de dix-huit ans sans inscription sur la liste prévue à l'article L. 311-2, lorsque l'oeuvre ou le document comporte des scènes de sexe non simulées ou de très grande violence mais qui, par la manière dont elles sont filmées et la nature du thème traité, ne justifient pas une telle inscription ; /  5° Interdiction de la représentation aux mineurs de dix-huit ans avec inscription de l'oeuvre ou du document sur la liste prévue à l'article L. 311-2 ". <br/>
<br/>
              3. Les dispositions de l'article L. 211-1 du code du cinéma et de l'image animée confèrent au ministre chargé de la culture l'exercice d'une police spéciale fondée sur les nécessités de la protection de l'enfance et de la jeunesse et du respect de la dignité humaine. A cette fin, il lui revient d'apprécier s'il y a lieu d'assortir la délivrance du visa d'exploitation d'une oeuvre ou d'un document cinématographique de l'une des restrictions prévues par les dispositions précédemment citées. Saisi d'un recours contre une telle mesure de police, il appartient au juge de l'excès de pouvoir de contrôler le caractère proportionné de la mesure retenue au regard des objectifs poursuivis par la loi. <br/>
<br/>
              4. S'agissant des mesures de classification prévues aux 4° et 5° de l'article R. 211-12 du code du cinéma et de l'image animée, il lui appartient d'apprécier si le film, pris dans son ensemble, revêt un caractère pornographique ou d'incitation à la violence justifiant que la délivrance du visa d'exploitation soit accompagnée d'une interdiction de la représentation aux mineurs de dix-huit ans avec inscription sur la liste prévue à l'article L. 311-2 du code du cinéma et de l'image animée ou si, alors même qu'il comporte des scènes de sexe non simulées ou de très grande violence, la manière dont cette oeuvre ou ce document est filmée et la nature du thème traité conduisent à limiter la restriction dont est assorti le visa d'exploitation à la seule interdiction de la représentation aux mineurs de dix-huit ans. <br/>
<br/>
              5. Lorsqu'une oeuvre ou un document cinématographique comporte des scènes violentes, il y a lieu de prendre en considération, pour déterminer si la protection de l'enfance et de la jeunesse et le respect de la dignité humaine justifient une des mesures de classification prévues aux 4° et 5° de l'article R. 211-12, la manière dont elles sont filmées, l'effet qu'elles sont destinées à produire sur les spectateurs, notamment de nature à présenter la violence sous un jour favorable ou à la banaliser, enfin, toute caractéristique permettant d'apprécier la mise à distance de la violence et d'en relativiser l'impact sur la jeunesse. <br/>
<br/>
              6. En ce qui concerne les films à caractère documentaire, qui visent à décrire la réalité des situations dont ils portent témoignage et qui ont ainsi pour objet de contribuer à l'établissement et à la diffusion de connaissances, l'appréciation doit être portée par le ministre, sous le contrôle du juge de l'excès de pouvoir, compte tenu de la nécessité de garantir le respect de la liberté d'information, protégée notamment par l'article 11 de la Déclaration des droits de l'homme et du citoyen de 1789 et par l'article 10 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.<br/>
<br/>
              7. En l'espèce, il ressort des pièces du dossier soumis aux juges du fond que le film documentaire intitulé " Salafistes " comporte des scènes violentes montrant de nombreuses exactions, assassinats, tortures, amputations, réellement commises par des groupes se revendiquant notamment des organisations Daech et Al-Qaïda au Maghreb islamique et présente, en parallèle, les propos de plusieurs protagonistes légitimant les actions en cause, menées contre des populations civiles, sans qu'aucun commentaire critique n'accompagne les scènes de violence. Toutefois, ces scènes s'insèrent de manière cohérente dans le propos du film documentaire, dont l'objet est d'informer le public sur la réalité de la violence salafiste en confrontant les discours tenus par des personnes promouvant cette idéologie aux actes de violence commis par des personnes et groupes s'en réclamant. En outre, tant l'avertissement figurant en début de film que la dédicace finale du documentaire aux victimes des attentats du 13 novembre 2015 sont de nature à faire comprendre, y compris par des spectateurs âgés de moins de dix-huit ans, l'objectif d'information et de dénonciation poursuivi par l'oeuvre documentaire, qui concourt ainsi à l'établissement et à la diffusion de connaissances sans présenter la violence sous un jour favorable ni la banaliser. Il en résulte, compte tenu de l'objet du film documentaire et du traitement de la violence qu'il retient, et eu égard à la nécessité de garantir le respect de la liberté d'information, y compris à l'égard de mineurs de dix-huit ans, que les scènes violentes du film documentaire intitulé " Salafistes " ne sont pas de nature à être qualifiées de scènes de " très grande violence " au sens des dispositions du 4° de l'article R. 211-12 du code du cinéma et de l'image animée. Dans ces conditions, la cour administrative d'appel de Paris a inexactement qualifié les faits de l'espèce en jugeant que la ministre de la culture et de la communication avait pu légalement assortir le visa d'exploitation du film " Salafistes " d'une interdiction de représentation aux mineurs de dix-huit ans.<br/>
<br/>
              8. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur l'autre moyen du pourvoi, que la société Margo Cinéma est fondée à demander l'annulation de l'arrêt qu'elle attaque.   <br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              10. Il ressort des pièces du dossier et de ce qui a été dit au point 7 que la protection de l'enfance et de la jeunesse et le respect de la dignité humaine n'impliquent pas que le visa d'exploitation du film documentaire " Salafistes " comporte une interdiction de diffusion aux mineurs de dix-huit ans.<br/>
<br/>
              11. Il résulte de ce qui précède que le ministre chargé de la culture n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Paris a annulé la décision du 27 janvier 2016 en ce qu'elle a assorti le visa d'exploitation du film documentaire intitulé " Salafistes " d'une interdiction de représentation publique aux mineurs de dix-huit ans.<br/>
<br/>
              12. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la société Margo Cinéma, au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Margo Cinéma qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 14 novembre 2017 est annulé.<br/>
Article 2 : L'appel du ministre de la culture et de la communication dirigé contre le jugement du tribunal administratif de Paris du 12 juillet 2016 est rejeté.<br/>
Article 3 : L'Etat versera une somme de 3 000 euros à la société Margo Cinéma au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par l'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société à responsabilité limitée Margo Cinéma et au ministre de la culture.  <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">09-05-01 ARTS ET LETTRES. CINÉMA. - 1) PRINCIPES GÉNÉRAUX DE LA POLICE SPÉCIALE DU CINÉMA CONFIÉE AU MINISTRE CHARGÉ DE LA CULTURE (ART. L. 211-1 DU CCIA) - 2) OFFICE DU JUGE SAISI D'UN RECOURS CONTRE LE VISA DÉLIVRÉ À UNE &#140;UVRE L'INTERDISANT AUX MINEURS, AU TITRE DU 4° OU DU 5° DE L'ARTICLE R. 211-12 DU CCIA - CAS D'UN FILM COMPORTANT DE NOMBREUSES SCÈNES VIOLENTES [RJ1] - 3) APPLICATION À UN FILM À CARACTÈRE DOCUMENTAIRE - A) SPÉCIFICITÉ DE CE GENRE ET CONSÉQUENCE SUR LE CONTRÔLE DU JUGE - B) ESPÈCE.
</SCT>
<ANA ID="9A"> 09-05-01 1) Les dispositions de l'article L. 211-1 du code du cinéma et de l'image animée (CCIA) confèrent au ministre chargé de la culture l'exercice d'une police spéciale fondée sur les nécessités de la protection de l'enfance et de la jeunesse et du respect de la dignité humaine. A cette fin, il lui revient d'apprécier s'il y a lieu d'assortir la délivrance du visa d'exploitation d'une oeuvre ou d'un document cinématographique de l'une des restrictions prévues par les articles R. 211-10 et R. 211-12 de ce code.... ,,2) Saisi d'un recours contre une telle mesure de police, il appartient au juge de l'excès de pouvoir de contrôler le caractère proportionné de la mesure retenue au regard des objectifs poursuivis par la loi.... ,,S'agissant des mesures de classification prévues aux 4° et 5° de l'article R. 211-12 du CCIA, il lui appartient d'apprécier si le film, pris dans son ensemble, revêt un caractère pornographique ou d'incitation à la violence justifiant que la délivrance du visa d'exploitation soit accompagnée d'une interdiction de la représentation aux mineurs de dix-huit ans avec inscription sur la liste prévue à l'article L. 311-2 du CCIA ou si, alors même qu'il comporte des scènes de sexe non simulées ou de très grande violence, la manière dont cette oeuvre ou ce document est filmée et la nature du thème traité conduisent à limiter la restriction dont est assorti le visa d'exploitation à la seule interdiction de la représentation aux mineurs de dix-huit ans.... ,,Lorsqu'une oeuvre ou un document cinématographique comporte des scènes violentes, il y a lieu de prendre en considération, pour déterminer si la protection de l'enfance et de la jeunesse et le respect de la dignité humaine justifient une des mesures de classification prévues aux 4° et 5° de l'article R. 211-12, la manière dont elles sont filmées, l'effet qu'elles sont destinées à produire sur les spectateurs, notamment de nature à présenter la violence sous un jour favorable ou à la banaliser, enfin, toute caractéristique permettant d'apprécier la mise à distance de la violence et d'en relativiser l'impact sur la jeunesse.... ,,3) a) En ce qui concerne les films à caractère documentaire, qui visent à décrire la réalité des situations dont ils portent témoignage et qui ont ainsi pour objet de contribuer à l'établissement et à la diffusion de connaissances, l'appréciation doit être portée par le ministre, sous le contrôle du juge de l'excès de pouvoir, compte tenu de la nécessité de garantir le respect de la liberté d'information, protégée notamment par l'article 11 de la Déclaration des droits de l'homme et du citoyen de 1789 et par l'article 10 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.,,,b) En l'espèce, il ressort des pièces du dossier soumis aux juges du fond que le film documentaire intitulé Salafistes comporte des scènes violentes montrant de nombreuses exactions, assassinats, tortures, amputations, réellement commises par des groupes se revendiquant notamment des organisations Daech et Al-Qaïda au Maghreb islamique et présente, en parallèle, les propos de plusieurs protagonistes légitimant les actions en cause, menées contre des populations civiles, sans qu'aucun commentaire critique n'accompagne les scènes de violence. Toutefois, ces scènes s'insèrent de manière cohérente dans le propos du film documentaire, dont l'objet est d'informer le public sur la réalité de la violence salafiste en confrontant les discours tenus par des personnes promouvant cette idéologie aux actes de violence commis par des personnes et groupes s'en réclamant. En outre, tant l'avertissement figurant en début de film que la dédicace finale du documentaire aux victimes des attentats du 13 novembre 2015 sont de nature à faire comprendre, y compris par des spectateurs âgés de moins de dix-huit ans, l'objectif d'information et de dénonciation poursuivi par l'oeuvre documentaire, qui concourt ainsi à l'établissement et à la diffusion de connaissances sans présenter la violence sous un jour favorable ni la banaliser. Il en résulte, compte tenu de l'objet du film documentaire et du traitement de la violence qu'il retient, et eu égard à la nécessité de garantir le respect de la liberté d'information, y compris à l'égard de mineurs de dix-huit ans, que les scènes violentes du film documentaire intitulé Salafistes ne sont pas de nature à être qualifiées de scènes de très grande violence au sens des dispositions du 4° de l'article R.211-12 du CCIA.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 1er juin 2015, Association Promouvoir, n° 372057, p. 178.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
