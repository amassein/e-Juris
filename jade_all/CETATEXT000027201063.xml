<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027201063</ID>
<ANCIEN_ID>JG_L_2013_03_000000347635</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/20/10/CETATEXT000027201063.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 20/03/2013, 347635</TITRE>
<DATE_DEC>2013-03-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>347635</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL ; SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>Mme Esther de Moustier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:347635.20130320</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°), sous le n° 347635, le pourvoi sommaire et le mémoire complémentaire, enregistrés les 21 mars et 20 juin 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Biguglia (Haute-Corse), représentée par son maire ; elle demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n°s 0900642-0900784-0900790-0901012-0901084-0901221-1000334-1000456-1000666-1000774-1000859-1000991-1001032 du 20 janvier 2011 par lequel le tribunal administratif de Bastia, statuant sur les demandes de Mme C... A..., en premier lieu, a annulé plusieurs arrêtés de 2008, 2009 et 2010 par lesquels son maire a placé Mme A...en congé ordinaire de maladie à demi traitement et plusieurs pris en 2010 par lesquels son maire a placé Mme A...en disponibilité d'office pour raison de santé, en deuxième lieu, lui a enjoint, dans le délai d'un mois à compter de la notification de ce jugement, de placer Mme A...en congé de longue durée imputable au service à compter du 18 août 2008, avec toutes les conséquences de droit sur sa situation statutaire et, enfin, a mis les frais de l'expertise à sa charge ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les demandes de Mme A...; <br/>
<br/>
              3°) de mettre à la charge de Mme A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu 2°), sous le n° 351537, le pourvoi, enregistré le 3 août 2011 au secrétariat du contentieux du Conseil d'Etat, présenté pour la commune de Biguglia, représentée par son maire ; elle demande au Conseil d'Etat : <br/>
<br/>
               1°) d'annuler le jugement n° 1001136 et 1001222 du 23 juin 2011 par lequel le tribunal administratif de Bastia, faisant droit à la demande de MmeB..., en premier lieu, a annulé les arrêtés des 1er octobre et 1er novembre 2010 par lesquels son maire a placé Mme A... en disponibilité d'office pour raison de santé, respectivement du 1er au 31 octobre 2010, puis du 1er au 30 novembre 2010, et, en second lieu, lui a enjoint, dans le délai d'un mois à compter de la notification de son jugement, de placer Mme A...en congé de longue durée imputable au service à compter du 18 août 2008, avec toutes les conséquences de droit sur sa situation statutaire ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les demandes de Mme A...; <br/>
<br/>
              3°) de mettre à la charge de Mme A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984 ;<br/>
<br/>
              Vu le décret n° 87-602 du 30 juillet 1987 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Esther de Moustier, Auditeur, <br/>
<br/>
              - les observations de la SCP Baraduc, Duhamel, avocat de la commune de Biguglia  et de la SCP Thouin-Palat, Boucard, avocat de MmeB...,<br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Baraduc, Duhamel, avocat de la commune de Biguglia  et à la SCP Thouin-Palat, Boucard, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que les pourvois visés ci-dessus présentent à juger les mêmes questions ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces des dossiers soumis aux juges du fond que MmeB..., adjoint technique territorial de 2ème classe de la commune de Biguglia, a été placée, par des arrêtés successifs du maire de Biguglia, en congé de maladie ordinaire à compter du 5 septembre 2008, puis, par des arrêtés des 1er décembre 2008, 1er janvier, 22 janvier, 1er mars, 9 avril, 1er juillet, 3 août, 1er septembre, 1er octobre, 2 novembre, 2 décembre 2009 et 4 janvier 2010 en congé ordinaire de maladie à demi traitement à compter du 18 novembre 2008 et, enfin,  à la suite de deux avis du comité médical départemental défavorables à son placement en congé de longue maladie, en disponibilité d'office pour raisons de santé à compter du 5 septembre 2009, par des arrêtés des 1er mars, 1er mai, 1er juin,  1er juillet, 1er août, 1er septembre, 1er octobre et 1er novembre 2010 ; qu'elle a été maintenue en disponibilité d'office à la suite d'un avis de la commission de réforme défavorable à son placement en congé de longue durée ; que, par deux jugements en date des 20 janvier et 23 juin 2011, le tribunal administratif de Bastia, d'une part, a annulé les arrêtés plaçant Mme A...en congé ordinaire de maladie à demi traitement, puis en disponibilité d'office pour raisons de santé, d'autre part, a enjoint au maire de la commune de placer l'intéressée en congé de longue durée imputable au service à compter du 18 août 2008 ; que la commune de Biguglia se pourvoit en cassation contre ces jugements ; <br/>
<br/>
              3. Considérant qu'aux termes de l'article 57 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " Le fonctionnaire en activité a droit : (...) / 2° A des congés de maladie dont la durée totale peut atteindre un an pendant une période de douze mois consécutifs en cas de maladie dûment constatée mettant l'intéressé dans l'impossibilité d'exercer ses fonctions. Celui-ci conserve alors l'intégralité de son traitement pendant une durée de trois mois ; ce traitement est réduit de moitié pendant les neuf mois suivants. (...) / 3° A des congés de longue maladie d'une durée maximale de trois ans dans les cas où il est constaté que la maladie met l'intéressé dans l'impossibilité d'exercer ses fonctions, rend nécessaires un traitement et des soins prolongés et présente un caractère invalidant et de gravité confirmée. Le fonctionnaire conserve l'intégralité de son traitement pendant un an ; le traitement est réduit de moitié pendant les deux années qui suivent. (...) / 4° A un congé de longue durée, en cas de tuberculose, maladie mentale, affection cancéreuse, poliomyélite ou déficit immunitaire grave et acquis, de trois ans à plein traitement et de deux ans à demi-traitement. (...) / Sauf dans le cas où le fonctionnaire ne peut être placé en congé de longue maladie à plein traitement, le congé de longue durée ne peut être attribué qu'à l'issue de la période rémunérée à plein traitement d'un congé de longue maladie. Cette période est réputée être une période du congé de longue durée accordé pour la même affection. Tout congé attribué par la suite pour cette affection est un congé de longue durée. " ; qu'aux termes de l'article 20 du décret du 30 juillet 1987 pris pour l'application de cette loi et relatif à l'organisation des comités médicaux, aux conditions d'aptitude physique et au régime des congés de maladie des fonctionnaires territoriaux : " Le fonctionnaire atteint d'une des affections énumérées au 4° de l'article 57 de la loi du 26 janvier 1984 susvisée, qui est dans l'impossibilité d'exercer ses fonctions et qui a épuisé, à quelque titre que ce soit, la période rémunérée à plein traitement d'un congé de longue maladie, est placé en congé de longue durée selon la procédure définie à l'article 25 ci-dessous.(...) / Lorsqu'elle a été attribuée au titre de l'affection ouvrant droit au congé de longue durée considéré, la période de congé de longue maladie à plein traitement, déjà accordée, est décomptée comme congé de longue durée. " ;<br/>
<br/>
              4. Considérant qu'il résulte des dispositions du troisième alinéa du 4° de l'article 57 de la loi du 26 janvier 1984 et de l'article 20 du décret du 30 juillet 1987 qu'un fonctionnaire ne peut être placé en congé de longue durée qu'après avoir épuisé ses droits à congé de longue maladie rémunéré à plein traitement ; que si la période de congé de longue maladie à plein traitement doit être décomptée, lorsque ce congé a été attribué au fonctionnaire au titre de l'affection ouvrant droit au congé de longue durée, comme une période de congé de longue durée, cette circonstance est sans incidence sur la portée de ces dispositions ;<br/>
<br/>
              5. Considérant que, pour annuler les arrêtés municipaux plaçant Mme A...en congé de maladie ordinaire puis en disponibilité d'office pour raisons de santé et enjoindre au maire de la commune de la placer en congé de longue durée, le tribunal administratif de Bastia a relevé que l'affection de l'intéressée, qui la mettait dans l'impossibilité d'exercer ses fonctions, rendait nécessaires un traitement et des soins prolongés et présentait un caractère invalidant et de gravité confirmée, nécessitait son placement en congé de longue durée ; qu'en statuant ainsi, sans rechercher si Mme A...avait épuisé ses droits à congé de longue maladie rémunéré à plein traitement, le tribunal administratif a commis une erreur de droit ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, ses jugements doivent être annulés ;<br/>
<br/>
              6. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la commune de Biguglia au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Biguglia qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>		D E C I D E :<br/>
              		--------------<br/>
<br/>
Article 1er : Les jugements des 20 janvier et 23 juin 2011 du tribunal administratif de Bastia sont annulés.<br/>
<br/>
Article 2 : Les affaires sont renvoyées au tribunal administratif de Bastia. <br/>
<br/>
Article 3 : Les conclusions de Mme A...et de la commune de Biguglia présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la commune de Biguglia et à Mme C... A....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-05-04-02 FONCTIONNAIRES ET AGENTS PUBLICS. POSITIONS. CONGÉS. CONGÉS DE LONGUE DURÉE. - PLACEMENT D'UN FONCTIONNAIRE TERRITORIAL EN CONGÉ DE LONGUE DURÉE - CONDITION - EPUISEMENT PRÉALABLE DE SES DROITS À CONGÉ DE LONGUE MALADIE RÉMUNÉRÉ À PLEIN TRAITEMENT - CIRCONSTANCE QUE LA PÉRIODE DE CONGÉ DE LONGUE MALADIE À PLEIN TRAITEMENT DOIT ÊTRE DÉCOMPTÉE, LORSQUE CE CONGÉ A ÉTÉ ATTRIBUÉ AU TITRE DE L'AFFECTION OUVRANT DROIT AU CONGÉ DE LONGUE DURÉE, COMME UNE PÉRIODE DE CONGÉ DE LONGUE DURÉE - INCIDENCE - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-07-01-03 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. STATUT GÉNÉRAL DES FONCTIONNAIRES DE L'ÉTAT ET DES COLLECTIVITÉS LOCALES. DISPOSITIONS STATUTAIRES RELATIVES À LA FONCTION PUBLIQUE TERRITORIALE (LOI DU 26 JANVIER 1984). - PLACEMENT EN CONGÉ DE LONGUE DURÉE - CONDITION - EPUISEMENT PRÉALABLE DES DROITS À CONGÉ DE LONGUE MALADIE RÉMUNÉRÉ À PLEIN TRAITEMENT - CIRCONSTANCE QUE LA PÉRIODE DE CONGÉ DE LONGUE MALADIE À PLEIN TRAITEMENT DOIT ÊTRE DÉCOMPTÉE, LORSQUE CE CONGÉ A ÉTÉ ATTRIBUÉ AU TITRE DE L'AFFECTION OUVRANT DROIT AU CONGÉ DE LONGUE DURÉE, COMME UNE PÉRIODE DE CONGÉ DE LONGUE DURÉE - INCIDENCE - ABSENCE.
</SCT>
<ANA ID="9A"> 36-05-04-02 Il résulte des dispositions du troisième alinéa du 4° de l'article 57 de la loi n° 84-53 du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale et de l'article 20 du décret n° 87-602 du 30 juillet 1987 pris pour l'application de cette loi et relatif à l'organisation des comités médicaux, aux conditions d'aptitude physique et au régime des congés de maladie des fonctionnaires territoriaux, qu'un fonctionnaire ne peut être placé en congé de longue durée qu'après avoir épuisé ses droits à congé de longue maladie rémunéré à plein traitement. Si la période de congé de longue maladie à plein traitement doit être décomptée, lorsque ce congé a été attribué au fonctionnaire au titre de l'affection ouvrant droit au congé de longue durée, comme une période de congé de longue durée, cette circonstance est sans incidence sur la portée de ces dispositions.</ANA>
<ANA ID="9B"> 36-07-01-03 Il résulte des dispositions du troisième alinéa du 4° de l'article 57 de la loi n° 84-53 du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale et de l'article 20 du décret n° 87-602 du 30 juillet 1987 pris pour l'application de cette loi et relatif à l'organisation des comités médicaux, aux conditions d'aptitude physique et au régime des congés de maladie des fonctionnaires territoriaux, qu'un fonctionnaire ne peut être placé en congé de longue durée qu'après avoir épuisé ses droits à congé de longue maladie rémunéré à plein traitement. Si la période de congé de longue maladie à plein traitement doit être décomptée, lorsque ce congé a été attribué au fonctionnaire au titre de l'affection ouvrant droit au congé de longue durée, comme une période de congé de longue durée, cette circonstance est sans incidence sur la portée de ces dispositions.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
