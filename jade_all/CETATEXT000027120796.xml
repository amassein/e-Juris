<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027120796</ID>
<ANCIEN_ID>JG_L_2013_02_000000357537</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/12/07/CETATEXT000027120796.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 27/02/2013, 357537, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-02-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357537</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:357537.20130227</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée le 12 mars 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par M. A... B..., demeurant .........., Principauté de Monaco ; il demande au Conseil d'Etat d'annuler, pour excès de pouvoir, l'instruction 14 B-1-10 du 6 avril 2010 de la direction générale des finances publiques, publiée au bulletin officiel des impôts du 15 avril 2010, ainsi que la décision du 13 janvier 2012 par laquelle sa demande tendant à l'abrogation de cette instruction a été rejetée ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 4 février 2013, présentée par M. A... ... ;<br/>
<br/>
              Vu la convention fiscale conclue entre la France et la Principauté de Monaco signée à Paris le 18 mai 1963 ; <br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le décret n° 2008-1281 du 8 décembre 2008 ; <br/>
<br/>
              Vu l'arrêté du 7 septembre 2012 du ministre délégué auprès du ministre de l'économie et des finances, chargé du budget, et du ministre de l'économie et des finances modifiant l'arrêté du 16 octobre 1980 portant modalités de publication et consultation des documents administratifs du ministère de l'économie ;<br/>
<br/>
              Vu l'arrêté du 10 septembre 2012 du Premier ministre relatif à la mise à disposition des instructions et circulaires publiées au Bulletin officiel des finances publiques-impôts ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, Maître des Requêtes, <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Sur la régularité de la procédure juridictionnelle devant le Conseil d'Etat :<br/>
<br/>
              1. Considérant que, contrairement à ce qui est soutenu, la procédure contradictoire a été régulièrement suivie ;<br/>
<br/>
              Sur les conclusions dirigées contre l'instruction 14 B-1-10 du 6 avril 2010 de la direction générale des finances publiques :<br/>
<br/>
              2. Considérant que la publication de cette instruction au bulletin officiel des impôts du 15 avril 2010 n'a pu faire courir le délai de recours contentieux à l'égard des tiers ; que, toutefois, par requête enregistrée le 10 juin 2010 au secrétariat du contentieux du Conseil d'Etat, M. A... ... a demandé l'annulation pour excès de pouvoir de la même instruction et a joint à sa demande le texte de l'instruction ; que sa requête a été rejetée par une décision n° 340438 du Conseil d'Etat, statuant au contentieux du 2 novembre 2011 ; qu'il doit ainsi être regardé comme ayant eu connaissance de l'instruction au plus tard à la date à laquelle il a introduit cette requête ; que, dès lors, le délai de recours contentieux dont il disposait a couru à compter de cette date ; que, par suite, les conclusions tendant à l'annulation de cette instruction présentées par M. A... ... dans la présente requête, enregistrée le 13 mars 2012 au secrétariat du contentieux du Conseil d'Etat, soit postérieurement à l'expiration du délai de recours contentieux, sont tardives et doivent être, ainsi que le soutient le ministre en défense par un mémoire signé par un agent régulièrement habilité à cette fin, rejetées comme irrecevables ;<br/>
<br/>
              Sur les conclusions dirigées contre la décision du 13 janvier 2012 refusant d'abroger l'instruction 14 B-1-10 du 6 avril 2010 :<br/>
<br/>
              3. Considérant que, lorsque, postérieurement à l'introduction d'une requête dirigée contre le refus d'abroger un acte comportant des dispositions générales, l'autorité qui a pris cet acte procède à son abrogation, les conclusions tendant à l'annulation pour excès de pouvoir de ce refus ont perdu leur objet ; qu'il en va toutefois différemment lorsque cette même autorité reprend, dans un nouvel acte, les dispositions qui ont été abrogées, sans les modifier ou en ne leur apportant que des modifications de pure forme ;<br/>
<br/>
              4. Considérant qu'en vertu du décret du 8 décembre 2008 relatif aux conditions de publication des instructions et circulaires, dans sa rédaction issue du décret du 6 septembre 2012, un arrêté du Premier ministre peut prévoir que, pour les circulaires et instructions intervenant dans certains domaines marqués par un besoin régulier de mise à jour portant sur un nombre important de données, leur mise à disposition sur un site internet autre que le site du Premier ministre, mentionné à l'article 1er du décret du 8 décembre 2008 (www.circulaires.legifrance.gouv.fr.), produira les mêmes effets que la mise à disposition sur ce site ; que, par un arrêté du 7 septembre 2012 du ministre délégué auprès du ministre de l'économie et des finances, chargé du budget, et du ministre de l'économie et des finances, a été créé le " Bulletin officiel des finances publiques " (BOFIP), qui comporte notamment une section relative aux impôts et qui peut être consulté sur le portail " www. impots. gouv. fr " ; qu'en vertu de l'arrêté du Premier ministre du 10 septembre 2012, la mise à disposition des circulaires et instructions sur le site internet " BOFIP-Impôts " (http://bofip.impots.gouv.fr) produit, à compter du 12 septembre 2012, les mêmes effets qu'une mise à disposition sur le site du Premier ministre ; que, par instruction 13 A-2-12 du 7 septembre 2012, publiée au bulletin officiel des impôts du même jour, les mêmes ministres ont indiqué que la section " impôts " du bulletin officiel des finances publiques se substituait notamment au " Bulletin officiel des impôts ", à la documentation administrative de base et à la rubrique " rescrits " du portail "www.impots.gouv.fr " et qu'à compter du 12 septembre 2012, seuls les commentaires publiés au Bulletin officiel des finances publiques-impôts seraient opposables à l'administration en application du deuxième alinéa de l'article L. 80 A du livre des procédures fiscales ; que la même instruction indique que, par voie de conséquence, à compter de cette même date, sont rapportés " tous autres commentaires publiés antérieurement sous forme de documentation administrative de base, d'instructions, de réponses ministérielles, de rescrits de portée générale et de réponses apportées dans le cadre du comité fiscal de la mission d'organisation administrative (...) " ;<br/>
<br/>
              5. Considérant que les stipulations du 1 de l'article 7 de la convention fiscale conclue entre la France et la Principauté de Monaco prévoient que les personnes physiques de nationalité française qui transporteront à Monaco leur domicile ou leur résidence - ou qui ne peuvent pas justifier de cinq ans de résidence habituelle à Monaco à la date du 13 octobre 1962 - seront assujetties en France à l'impôt sur le revenu des personnes physiques et à la taxe complémentaire dans les mêmes conditions que si elles avaient leur domicile ou leur résidence en France ; que l'instruction 14 B-1-10 du 6 avril 2010 précisait que ni la volonté des rédacteurs de cette convention ni la lettre de ses stipulations ne permettaient d'estimer qu'en visant les personnes qui ne pouvaient justifier de cinq ans de résidence habituelle à Monaco à la date du 13 octobre 1962, la France et la principauté de Monaco n'avaient pas entendu viser la situation des personnes de nationalité française nées à Monaco ; que M. A... ... a demandé l'abrogation de cette instruction aux motifs qu'elle prévoit " l'imposition à l'impôt sur le revenu en France de revenus de source non française au prétexte qu'il est un des français nés à Monaco après le 12 octobre 1957 " ; que ces dispositions de l'instruction, qui étaient seules contestées, n'ont pas été reprises au bulletin officiel des finances publiques-impôts (BOFIP-impôts) et ont ainsi été abrogées à compter du 12 septembre 2012 ; que l'administration ne les a pas reprises sous une autre forme ; que, par suite, les conclusions de M. A... ... dirigées contre le refus d'abroger l'instruction litigieuse ont perdu leur objet ; qu'il n'y a plus lieu d'y statuer ;<br/>
<br/>
                 6. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. A... ... au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions de la requête de M. A... ... tendant à l'annulation de la décision du 13 janvier 2012 rejetant sa demande tendant à l'abrogation de l'instruction 14 B-1-10 du 6 avril 2010.<br/>
<br/>
Article 2 : Le surplus des conclusions de la requête de M. A... ... est rejeté.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. A... B... et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
