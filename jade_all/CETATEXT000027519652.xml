<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027519652</ID>
<ANCIEN_ID>JG_L_2013_06_000000361697</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/51/96/CETATEXT000027519652.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 03/06/2013, 361697, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-06-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361697</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Appréciation de la légalité</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP TIFFREAU-CORLAY-MARLANGE ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD</AVOCATS>
<RAPPORTEUR>Mme Esther de Moustier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:361697.20130603</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu l'ordonnance n° 12PA02914 du 26 juillet 2012, enregistrée le 6 août 2012 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président de la cour administrative d'appel de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée à cette cour par M. B...A...;<br/>
<br/>
              Vu la requête,  enregistrée au greffe de la cour administrative d'appel de Paris le 5 juillet 2012, présentée par  M. B... A..., demeurant ...Cedex) et les mémoires, enregistrés les 30 août et 28 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A...; il demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement n° 12042 du 26 avril 2012 par lequel le tribunal administratif de Nouvelle-Calédonie a, à la suite d'un  arrêt de la cour d'appel de Nouméa en date du 3 novembre 2011 décidant de surseoir à statuer sur le litige dont elle était saisie, déclaré que les locaux du snack-restaurant de la piscine municipale de Ouen-Toro appartenaient au domaine public de la commune de Nouméa ; <br/>
<br/>
              2°) de déclarer que ces locaux n'appartiennent pas au domaine public communal ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Nouméa la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu le code des communes de la Nouvelle-Calédonie ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Esther de Moustier, Auditeur,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Tiffreau-Corlay-Marlange, avocat de M. A...et à la SCP Barthélemy, Matuchansky, Vexliard, avocat de la commune de Nouméa ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant, en premier lieu, que le tribunal administratif de Nouvelle-Calédonie, qui n'était pas tenu de répondre à tous les arguments qui lui étaient soumis, s'est prononcé par des motifs suffisants sur les moyens invoqués par M A...; que, par suite, son jugement n'est pas irrégulier ; <br/>
<br/>
              2. Considérant, en second lieu, qu'il résulte de l'instruction que l'ensemble immobilier de la piscine municipale de Ouen-Toro est composé d'un grand bassin, de pataugeoires, de tribunes, de vestiaires ainsi que d'un hall d'entrée abritant un guichet, des locaux abritant un snack-restaurant et d'une réserve pour cet établissement commercial ; que cet ensemble immobilier est propriété de la commune de Nouméa ; que les locaux du snack-restaurant, qui ont été construits en même temps que la piscine, ont été conçus pour permettre à ses usagers d'y accéder ; qu'ils sont situés dans l'enceinte de cet ensemble immobilier et ne disposent pas d'un accès direct et autonome à l'extérieur ; que les conventions d'occupation de ces locaux conclues entre la commune et M. A...prévoient que les horaires d'ouverture du snack-restaurant sont ceux de l'ouverture de la piscine au public et que toute ouverture au-delà de vingt et une heures doit faire l'objet d'une autorisation expresse de la commune ; que, même s'ils sont accessibles aux personnes extérieures à la piscine et se situent avant le guichet d'accès aux bassins, les locaux du snack-restaurant, exploité par M.A..., ont été construits afin de permettre aux usagers de la piscine municipale de se désaltérer et de se rassasier ; que, par suite, ces locaux sont indivisibles des locaux affectés au service public ; qu'ainsi, en l'absence de tout acte exprès prononçant leur déclassement, ils n'ont cessé de constituer une dépendance du domaine public communal ; qu'est sans incidence sur leur appartenance au domaine public la circonstance que M. A...a été autorisé par la commune à y exercer une activité complémentaire de production et vente de foie gras ; <br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède que M. A...n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Nouvelle-Calédonie a déclaré que les locaux qu'il exploitait dans l'enceinte de la piscine municipale de Ouen-Toro constituaient une dépendance du domaine public de la commune de Nouméa ; que, par voie de conséquence, ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative doivent également être rejetées ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A...la somme de 3 000 euros à verser à la commune de Nouméa au titre de ces dispositions ; <br/>
<br/>
<br/>
<br/>
<br/>
              		D E C I D E :<br/>
              		--------------<br/>
<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : M. A...versera à la commune de Nouméa la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à M. A...et à la commune de Nouméa.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
