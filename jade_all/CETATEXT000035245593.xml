<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035245593</ID>
<ANCIEN_ID>JG_L_2017_07_000000408808</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/24/55/CETATEXT000035245593.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 19/07/2017, 408808, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-07-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408808</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS</AVOCATS>
<RAPPORTEUR>M. Cyrille Beaufils</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:408808.20170719</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Veniel investissements a saisi le conseil régional de l'ordre des géomètres-experts de Rennes d'une plainte à l'encontre de M. B...A.... Par une décision du 25 septembre 2015, le conseil régional a rejeté sa plainte. <br/>
<br/>
              Par une décision n° 957 D du 11 janvier 2017, le conseil supérieur de l'ordre des géomètres-experts a rejeté l'appel formé par la société Veniel investissements contre cette décision. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 10 mars et 12 juin 2017 au secrétariat du contentieux du Conseil d'Etat, la société Veniel investissements demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette décision ; <br/>
<br/>
              2°) de mettre à la charge de M. A...la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par un mémoire distinct, enregistré le 12 juin 2017 au secrétariat du contentieux du Conseil d'Etat, la société Veniel investissements demande au Conseil d'Etat, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de son pourvoi tendant à l'annulation de la décision du 11 janvier 2017 du conseil supérieur de l'ordre des géomètres-experts, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de l'article 11 de la loi n° 46-942 du 7 mai 1946 instituant l'ordre des géomètres-experts. <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - la loi n° 46-942 du 7 mai 1946 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Cyrille Beaufils, auditeur,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de la société Veniel Investissements ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 10 juillet 2017, présentée par le ministre d'Etat, ministre de la transition écologique et solidaire ;<br/>
<br/>
<br/>
<br/>
<br/>Sur la question prioritaire de constitutionnalité : <br/>
<br/>
              1.	Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2.	Considérant qu'aux termes de l'article 11 de la loi du 7 mai 1946 instituant l'ordre des géomètres-experts : " Les pouvoirs publics sont représentés auprès du conseil supérieur et des conseils régionaux par un commissaire du gouvernement désigné parmi les membres du Conseil d'Etat. / Le commissaire du Gouvernement auprès du conseil supérieur et des conseils régionaux de l'ordre des géomètres-experts est nommé par décret pris sur proposition du ministre chargé de l'urbanisme, après avis du ministre chargé de l'éducation, du ministre chargé de l'agriculture et du ministre chargé de l'économie et des finances. Sauf en matière disciplinaire, il reçoit ses instructions de chacun des ministres intéressés, chacun d'eux agissant dans le cadre de sa compétence. / Le commissaire du Gouvernement participe avec voix délibérative aux séances du conseil supérieur siégeant en formation disciplinaire. Son délégué participe avec voix délibérative aux séances du conseil régional siégeant en formation disciplinaire. / Le commissaire du Gouvernement peut déléguer, sous sa responsabilité, tout ou partie des attributions que lui confère la présente loi à des présidents ou conseillers des tribunaux administratifs et cours administratives d'appel en activité ou honoraires. / Le commissaire du gouvernement assiste aux séances du conseil supérieur de l'ordre et, s'il le désire, aux séances des conseils régionaux. Il a pouvoir, notamment, d'introduire devant les conseils régionaux toutes actions contre les personnes ou sociétés soumises à la surveillance et au contrôle de l'ordre. Il peut aussi saisir le conseil supérieur de toutes décisions des conseils régionaux. Il approuve les règlements intérieurs rédigés par les conseils de l'ordre. / Le commissaire du gouvernement procède à la mise en place des conseils prévus par la présente loi. "<br/>
<br/>
              3. Considérant que la société Veniel investissements soutient que ces dispositions, qui sont applicables au litige et n'ont pas été déjà déclarées conformes à la Constitution, méconnaissent l'article 16 de la Déclaration des droits de l'homme et du citoyen dans la mesure où elles prévoient que le commissaire du Gouvernement participe aux décisions disciplinaires du Conseil supérieur et des conseils régionaux de l'ordre des géomètres-experts avec voix délibérative, alors qu'il est nommé sur proposition du ministre chargé de l'urbanisme après avis de plusieurs autres ministres dont il est susceptible de recevoir les instructions et qu'il cumule les fonctions de poursuite et de jugement dans le cas où il est à l'origine de l'action disciplinaire ;<br/>
<br/>
              4. Considérant qu'il résulte cependant de ces dispositions, telles qu'interprétées par la jurisprudence du Conseil d'Etat, que la circonstance que le commissaire du Gouvernement et ses délégués disposent de fonctions de poursuite et de jugement ne porte pas, par elle-même, atteinte à l'impartialité de la juridiction ordinale ; qu'en effet, si l'un d'eux est à l'origine des poursuites disciplinaires ou fait appel d'une décision d'un conseil régional siégeant en formation disciplinaire, le commissaire du Gouvernement et son délégué doivent être regardés comme ayant pris parti sur les faits reprochés au géomètre-expert et ne peuvent, par suite, siéger au sein des formations disciplinaires du conseil de l'ordre des géomètres-experts sans méconnaître le principe d'impartialité ; que ces dispositions n'ont donc pas pour objet et ne sauraient avoir pour effet de permettre que le commissaire du Gouvernement, s'il a engagé les poursuites disciplinaires ou été à l'origine d'un appel, siège au sein de l'instance de discipline appelée à se prononcer sur l'affaire en cause ; qu'il résulte par ailleurs des termes mêmes de l'article 11 de la loi du 7 mai 1946 que le commissaire du Gouvernement ne peut recevoir des instructions ministérielles en matière disciplinaire ; qu'il s'ensuit que la question de la conformité de l'article 11 de la loi du 7 mai 1946 au droit à un recours juridictionnel effectif, aux droits de la défense, au droit à un procès équitable et aux principes d'impartialité et d'indépendance des juridictions, protégés par l'article 16 de la Déclaration des droits de l'homme et du citoyen de 1789, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; qu'il n'y a pas lieu de la renvoyer au Conseil constitutionnel ;<br/>
<br/>
              Sur l'admission du pourvoi : <br/>
<br/>
              5.	Considérant qu'aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux. " ;<br/>
<br/>
              6. Considérant que pour demander l'annulation de la décision du conseil supérieur de l'ordre des géomètres-experts qu'elle attaque, la société Veniel investissements soutient qu'elle doit être annulée par voie de conséquence de l'inconstitutionnalité des dispositions de l'article 11 de la loi du 7 mai 1946 ; qu'elle est entachée d'une erreur de droit en ce qu'elle se fonde, pour écarter la faute du géomètre-expert, sur la circonstance que son client était informé de la situation de conflit d'intérêts qui lui était reprochée ; qu'elle est entachée d'une inexacte qualification des faits de l'espèce en ce qu'elle retient qu'il n'existait pas d'incompatibilité entre les différentes missions que le géomètre-expert avait acceptées ; <br/>
<br/>
              7. Considérant qu'aucun de ces moyens n'est de nature à permettre l'admission du pourvoi ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
---------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société Veniel investissements.<br/>
Article 2 : Le pourvoi de société Veniel investissements n'est pas admis. <br/>
Article 3 : La présente décision sera notifiée à la société Veniel investissements.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre, à M. B...A..., au président du conseil supérieur de l'ordre des géomètres-experts, au président du conseil régional de l'ordre des géomètres-experts de Rennes, au ministre d'Etat, ministre de la transition écologique et solidaire et au ministre de la cohésion des territoires.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
