<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042471946</ID>
<ANCIEN_ID>JG_L_2020_10_000000431574</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/47/19/CETATEXT000042471946.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 23/10/2020, 431574, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431574</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLEZ</AVOCATS>
<RAPPORTEUR>Mme Mélanie Villiers</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:431574.20201023</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A... B..., ressortissante marocaine, a demandé au tribunal administratif de Nice, d'une part, d'annuler la décision implicite de rejet née du silence gardé par le préfet des Alpes-Maritimes sur sa demande de titre de séjour présentée le 10 août 2016, d'autre part, d'annuler l'arrêté du 31 mai 2017 par lequel le préfet a refusé de l'admettre au séjour et lui a ordonné de quitter le territoire français dans un délai de trente jours à destination du Maroc, et, enfin, à ce qu'il lui soit enjoint de lui délivrer une carte de séjour ou à défaut de réexaminer sa situation dans un délai de deux mois à compter de la notification du jugement. Par un jugement n°s 1702164, 1702394 du 12 octobre 2017, le tribunal administratif de Nice a rejeté ses demandes.<br/>
<br/>
              Par un arrêt n°18MA00262 du 31 octobre 2018, la cour administrative d'appel de Marseille a rejeté l'appel formé par Mme B... contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 11 juin et 12 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros, à verser à la SCP Boullez, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - l'accord franco-marocain en matière de séjour et d'emploi du 9 octobre 1987 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme C... D..., maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boullez, avocat de Mme B... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales stipule que : " 1. Toute personne a droit au respect de sa vie privée et familiale (...) ". Aux termes des dispositions du 7° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Sauf si sa présence constitue une menace pour l'ordre public, la carte de séjour temporaire portant la mention " vie privée et familiale " est délivrée de plein droit : (...) / 7° A l'étranger ne vivant pas en état de polygamie, qui n'entre pas dans les catégories précédentes ou dans celles qui ouvrent droit au regroupement familial, dont les liens personnels et familiaux en France, appréciés notamment au regard de leur intensité, de leur ancienneté et de leur stabilité, des conditions d'existence de l'intéressé, de son insertion dans la société française ainsi que de la nature de ses liens avec la famille restée dans le pays d'origine, sont tels que le refus d'autoriser son séjour porterait à son droit au respect de sa vie privée et familiale une atteinte disproportionnée au regard des motifs du refus, sans que la condition prévue à l'article L. 313-2 soit exigée. L'insertion de l'étranger dans la société française est évaluée en tenant compte notamment de sa connaissance des valeurs de la République ". Aux termes de l'article L. 313-14 du même code : " La carte de séjour temporaire mentionnée à l'article L. 313-11 ou la carte de séjour temporaire mentionnée aux 1° et 2° de l'article L. 313-10 peut être délivrée, sauf si sa présence constitue une menace pour l'ordre public, à l'étranger ne vivant pas en état de polygamie dont l'admission au séjour répond à des considérations humanitaires ou se justifie au regard des motifs exceptionnels qu'il fait valoir, sans que soit opposable la condition prévue à l'article L. 313 2 (...) ".<br/>
<br/>
              2. Mme B..., ressortissante marocaine, a sollicité, par un courrier du 29 juillet 2016, reçu le 10 août 2016 à la préfecture des Alpes-Maritimes, son admission au séjour au titre de sa vie privée et familiale. Une décision implicite de rejet est née du silence gardé par le préfet des Alpes-Maritimes. Par un arrêté du 31 mai 2017, le préfet a explicitement rejeté sa demande de titre de séjour et lui a ordonné de quitter le territoire français dans un délai de trente jours à destination du Maroc. Par un jugement du 12 octobre 2017, le tribunal administratif de Nice a estimé qu'il n'y avait plus lieu de statuer sur la demande de Mme B... tendant à l'annulation de la décision implicite du préfet et a rejeté sa demande tendant à l'annulation de l'arrêté du 31 mai 2017. Mme B... se pourvoit en cassation contre l'arrêt du 31 octobre 2018 par lequel la cour administrative d'appel de Marseille a rejeté sa requête d'appel.<br/>
<br/>
              3. Si Mme B... fait valoir qu'elle n'a plus d'attache familiale dans son pays d'origine depuis le décès de son second époux, que ses deux enfants résident régulièrement en France et qu'elle est, depuis son arrivée en France, hébergée et totalement prise en charge par sa fille, qui a acquis la nationalité française en 2011, il ressort des pièces du dossier soumis aux juges du fond qu'elle n'est entrée en France qu'en juin 2014, après avoir passé l'essentiel de sa vie au Maroc, où elle a vécu jusqu'à l'âge de 55 ans en étant longtemps éloignée de ses enfants venus s'installer en France. Par suite, la cour administrative d'appel de Marseille n'a pas inexactement qualifié les faits de l'espèce en déduisant de l'ensemble de ces circonstances que le refus de délivrance du titre de séjour litigieux n'avait pas porté une atteinte disproportionnée au droit de celle-ci au respect de sa vie privée et familiale et n'avait, par suite, pas méconnu les dispositions des articles L. 313-11, 7° et L. 313-14 du code de l'entrée et du séjour des étrangers et du droit d'asile et les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.<br/>
<br/>
              4. Il résulte de ce qui précède que Mme B... n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. Les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font par suite obstacle à ce qu'il soit fait droit aux conclusions présentées sur le fondement de ces dispositions par la SCP Boullez, son avocat.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme B... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme A... B... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
