<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030704420</ID>
<ANCIEN_ID>JG_L_2015_06_000000369534</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/70/44/CETATEXT000030704420.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème SSJS, 03/06/2015, 369534, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-06-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369534</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Karin Ciavaldini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:369534.20150603</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme B...A...ont demandé au tribunal administratif de Bordeaux d'annuler l'arrêté du 3 juin 2008 par lequel le préfet de la Gironde a transféré dans le domaine public de la commune de Saint-Selve une voie privée située au lieu-dit "Bigard" sur le territoire de cette commune. Par un jugement n° 0803104 du 10 mars 2011, le tribunal administratif de Bordeaux a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 11BX01130 du 18 avril 2013, la cour administrative d'appel de Bordeaux a rejeté l'appel formé contre ce jugement par M. et MmeA....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 juin et 17 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, M. et Mme A...demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Saint-Selve et de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ainsi que la contribution pour l'aide juridique mentionnée à l'article R. 761-1 du même code. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Karin Ciavaldini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Balat, avocat de M. et Mme A...et à la SCP Rocheteau, Uzan-Sarano, avocat de la commune de Saint-Selve ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 318-3 du code de l'urbanisme dans sa rédaction applicable aux faits de l'espèce : " La propriété des voies privées ouvertes à la circulation publique dans des ensembles d'habitations peut, après enquête publique, être transférée d'office sans indemnité dans le domaine public de la commune sur le territoire de laquelle ces voies sont situées. / La décision de l'autorité administrative portant transfert vaut classement dans le domaine public et éteint, par elle-même et à sa date, tous droits réels et personnels existant sur les biens transférés. / Cette décision est prise par délibération du conseil municipal. Si un propriétaire intéressé a fait connaître son opposition, cette décision est prise par arrêté du représentant de l'Etat dans le département, à la demande de la commune (...) " ; que le transfert des voies privées dans le domaine public communal prévu par ces dispositions est subordonné à l'ouverture de ces voies à la circulation publique, laquelle traduit la volonté de leurs propriétaires d'accepter l'usage public de leur bien et de renoncer à son usage purement privé ; que le propriétaire d'une voie privée ouverte à la circulation est en droit d'en interdire à tout moment l'usage au public ; <br/>
<br/>
              2. Considérant qu'il ressort des énonciations de l'arrêt attaqué que, pour qualifier la voie litigieuse de voie privée ouverte à la circulation publique, la cour administrative d'appel de Bordeaux a relevé que cette voie servait depuis de nombreuses années de passage d'usage commun ; qu'il résulte du point 1 ci-dessus qu'en ne recherchant pas si les propriétaires de cette voie, en particulier M. et MmeA..., avaient manifesté leur consentement, au moins tacite, à l'ouverture de la voie à la circulation générale et, ce faisant, leur renoncement à un usage purement privé de celle-ci, alors que M. et Mme A...soutenaient devant elle qu'ils s'étaient toujours opposés à l'usage public de la voie, la cour a commis une erreur de droit ; que dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. et Mme A...sont fondés à demander l'annulation de l'arrêt qu'ils attaquent ; <br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier qu'avant d'exprimer, lors de l'enquête publique menée en janvier 2008, leur opposition au transfert dans le domaine public communal de la voie privée en litige, M. et Mme A...se sont, dès 1992, constamment opposés à la circulation de tiers sur leur parcelle ; qu'en l'absence de leur volonté d'accepter l'usage public de leur bien et de renoncer par là à son usage purement privé, la voie litigieuse ne pouvait être regardée comme ouverte à la circulation publique ; que, dès lors, M. et Mme A...sont fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Bordeaux a rejeté leur demande d'annulation de l'arrêté du 3 juin 2008 portant transfert d'office dans le domaine public de la commune de Saint-Selve de la voie privée desservant les parcelles cadastrées section A n° 398, 404, 405, 406, 407, 408, 411, 412, 413, 414, 722, 983 et 984 au lieu-dit " Bigard " ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Saint-Selve la somme de 3 000 euros à verser à M. et Mme A...au titre des dispositions des articles L. 761-1 et R. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. et Mme A...qui ne sont pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
                                  D E C I D E :<br/>
                                   --------------<br/>
<br/>
Article 1er : L'arrêt du 18 avril 2013 de la cour administrative d'appel de Bordeaux est annulé.<br/>
<br/>
Article 2 : Le jugement du 10 mars 2011 du tribunal administratif de Bordeaux et l'arrêté du 3 juin 2008 du préfet de la Gironde sont annulés.<br/>
<br/>
Article 3 : La commune de Saint-Selve versera à M. et Mme A...une somme de 3 000 euros au titre des articles L. 761-1 et R. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées par la commune de Saint-Selve au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 5 : La présente décision sera notifiée à M. et Mme B...A..., à la commune de Saint-Selve, au ministre de l'intérieur et à la ministre du logement, de l'égalité des territoires et de la ruralité.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
