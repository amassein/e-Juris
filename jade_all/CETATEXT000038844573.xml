<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038844573</ID>
<ANCIEN_ID>JG_L_2019_06_000000427397</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/84/45/CETATEXT000038844573.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 12/06/2019, 427397, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-06-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427397</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>Mme Cécile Renault</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:427397.20190612</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société SONOCAR Industrie a demandé au juge des référés du tribunal administratif de Toulon, sur le fondement de l'article L. 551-6 du code de justice administrative, d'enjoindre à l'Etat, sous astreinte, d'une part, de lui communiquer, dans un délai d'une semaine, l'analyse des offres des candidats, s'agissant des prix globaux correspondant aux deux scénarios " prix barème recomposés ", effectuée dans le cadre de la procédure de passation, par la direction du service de soutien de la flotte de Toulon, d'un accord-cadre relatif à la fourniture et à l'application de peintures sur des navires et matériels de la Marine nationale basés en façade méditerranéenne, d'autre part, de suspendre l'exécution de toute décision s'y rapportant, y compris la décision du 3 décembre 2018 lui notifiant le rejet de son offre, la décision d'attribuer le marché à la société Prezioso Linjebygg, et la signature de l'accord-cadre et, enfin, d'ordonner à l'Etat de reprendre la procédure de passation du marché en litige. Par une ordonnance n° 1803912 du 11 janvier 2019, le juge des référés du tribunal administratif de Toulon a fait droit à la demande de suspension de l'exécution du marché et a rejeté le surplus des conclusions de la demande.<br/>
<br/>
              1° Sous le n° 427397, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 28 janvier et 11 février 2019 au secrétariat du contentieux du Conseil d'Etat, la Société Prezioso Linjebygg demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société SONOCAR Industrie ;<br/>
<br/>
              3°) de mettre à la charge de la société SONOCAR Industrie la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              2° Sous le n° 427407, par un pourvoi et un mémoire en réplique, enregistrés les 28 janvier et 11 avril 2019 au secrétariat du contentieux du Conseil d'Etat, la ministre des armées demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société SONOCAR Industrie.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la directive 2009/81/CE du Parlement européen et du Conseil du 13 juillet 2009 ;<br/>
              - le règlement d'exécution (UE) n° 2015/1986 de la Commission du 11 novembre 2015 ; <br/>
              - l'ordonnance n° 2015-899 du 23 juillet 2015 ; <br/>
              - le décret n° 2016-361 du 25 mars 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Renault, auditrice,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de la société Prezioso Linjebygg et à la SCP Foussard, Froger, avocat de la société SONOCAR Industrie ; <br/>
<br/>
              Vu la note en délibérée, enregistrée le 28 mai 2019, présentée par la société SONOCAR Industrie sous les n°s 427397 et 427407 ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les pourvois de la ministre des armées et de la société Prezioso Linjebygg sont dirigés contre la même ordonnance. Il y a lieu de les joindre pour statuer par une seule décision. <br/>
<br/>
              2. Aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation, la délégation d'un service public ou la sélection d'un actionnaire opérateur économique d'une société d'économie mixte à opération unique. (...) Le juge est saisi avant la conclusion du contrat ". Aux termes de l'article L. 551-2 du même code : " I. Le juge peut ordonner à l'auteur du manquement de se conformer à ses obligations et suspendre l'exécution de toute décision qui se rapporte à la passation du contrat, sauf s'il estime, en considération de l'ensemble des intérêts susceptibles d'être lésés et notamment de l'intérêt public, que les conséquences négatives de ces mesures pourraient l'emporter sur leurs avantages. Il peut, en outre, annuler les décisions qui se rapportent à la passation du contrat et supprimer les clauses ou prescriptions destinées à figurer dans le contrat et qui méconnaissent lesdites obligations. / II. -Toutefois, le I n'est pas applicable aux contrats passés dans les domaines de la défense ou de la sécurité au sens de l'article 6 de l'ordonnance n° 2015-899 du 23 juillet 2015 relative aux marchés publics. Pour ces contrats, il est fait application des articles L. 551.6 et L. 551-7 ". Aux termes de l'article L. 551-6 du code de justice administrative : " Le juge peut ordonner à l'auteur du manquement de se conformer à ses obligations en lui fixant un délai à cette fin. Il peut lui enjoindre de suspendre l'exécution de toute décision se rapportant à la passation du contrat (...) Il peut, en outre, prononcer une astreinte provisoire courant à l'expiration des délais impartis (...) ".<br/>
<br/>
              3. Il ressort des pièces du dossier soumis au juge du référé précontractuel du tribunal administratif de Toulon que la direction du service de soutien de la flotte de Toulon a lancé une procédure négociée pour l'attribution d'un accord-cadre à bons de commande portant sur la fourniture et l'application de peintures sur des navires et matériels de la Marine nationale basés en façade méditerranéenne. Par courrier du 3 décembre 2018, la direction du service de soutien de la flotte de Toulon a informé la société SONOCAR Industrie du rejet de son offre. La ministre des armées et la société Prezioso Linjebygg, attributaire du marché, se pourvoient en cassation contre l'ordonnance du 11 janvier 2019 par laquelle le juge des référés du tribunal administratif de Toulon, saisi sur le fondement de l'article L. 551-6 du code de justice administrative par la société SONOCAR Industrie, a enjoint à la ministre des armées de suspendre l'exécution de l'ensemble des décisions se rapportant à la procédure de passation et, si elle entendait passer un tel marché, de reprendre la procédure au stade de l'avis d'appel public à la concurrence. <br/>
<br/>
              Sur les conclusions du pourvoi de la ministre des armées dirigées contre l'ordonnance attaquée en tant qu'elle rejette les conclusions de la société SONOCAR tendant à ce qu'il lui soit enjoint de communiquer le montant global de l'estimation financière de l'offre de la société Prezioso Linjebygg : <br/>
<br/>
              4. Aux termes du I de l'article 44 de l'ordonnance du 23 juillet 2015 relative aux marchés publics : " (...) l'acheteur ne peut communiquer les informations confidentielles qu'il détient dans le cadre du marché public, telles que celles dont la divulgation violerait le secret des affaires ou pourrait nuire à une concurrence loyale entre les opérateurs économiques, notamment par la communication, en cours de consultation, du montant global ou du prix détaillé des offres. Toutefois, l'acheteur peut demander aux opérateurs économiques de consentir à ce que certaines informations confidentielles qu'ils ont fournies, précisément désignées, puissent être divulguées ". Aux termes de l'article 88 du décret du 25 mars 2016 relatif aux marchés publics de défense ou de sécurité : " (...) II. - Pour les marchés publics passés selon une procédure formalisée, l'acheteur, dès qu'il décide de rejeter une candidature ou une offre, notifie à chaque candidat ou soumissionnaire concerné le rejet de sa candidature ou de son offre en lui indiquant les motifs de ce rejet. / Lorsque cette notification intervient après l'attribution du marché public, elle précise, en outre, le nom de l'attributaire et les motifs qui ont conduit au choix de son offre. Elle mentionne également la date à compter de laquelle l'acheteur est susceptible de signer le marché public dans le respect des dispositions de l'article 89. A la demande de tout soumissionnaire ayant fait une offre qui n'a pas été rejetée au motif qu'elle était irrégulière, inacceptable ou inappropriée, l'acheteur communique dans les meilleurs délais et au plus tard quinze jours à compter de la réception de cette demande : / 1° Lorsque les négociations ou le dialogue ne sont pas encore achevés, les informations relatives au déroulement et à l'avancement des négociations ou du dialogue ; / 2° Lorsque le marché public a été attribué, les caractéristiques et les avantages de l'offre retenue ". <br/>
<br/>
              5. Il ressort des pièces du dossier de la procédure de référé que la ministre des armées a spontanément produit, dans le cadre de l'instance de référé devant le tribunal administratif de Toulon, le montant global de l'estimation financière de l'offre de la société Prezioso Linjebygg. Le juge du référé précontractuel en a pris acte et a rejeté les conclusions aux fins d'injonction présentées à cette fin par la société, après avoir relevé que ces éléments étaient bien au nombre des caractéristiques et avantages de l'offre retenue, communicables en application de l'article 88 du décret du 25 mars 2016, le pouvoir adjudicateur n'établissant pas qu'une telle communication aurait porté atteinte au secret des affaires. Si, dans son pourvoi, la ministre fait valoir que le juge des référés aurait à cet égard commis une erreur de droit, cette contestation, qui n'est pas dirigée contre le dispositif de l'ordonnance attaquée mais seulement contre ses motifs, n'est, en tout état de cause, pas recevable. <br/>
<br/>
              Sur les conclusions des pourvois dirigées contre l'ordonnance attaquée en tant qu'elle a enjoint à la ministre des armées de suspendre l'exécution de l'ensemble des décisions se rapportant à la passation de l'accord-cadre :<br/>
<br/>
              6. Pour suspendre l'exécution de l'ensemble des décisions se rapportant à la passation du marché, le juge du référé précontractuel du tribunal administratif de Toulon a relevé, d'une part, que le pouvoir adjudicateur avait prévu que l'accord-cadre aurait un montant maximum, mais n'avait fixé celui-ci qu'à la fin de la procédure de négociation, et, d'autre part, que l'avis de marché ne comportait aucune mention relative à la quantité ou à l'étendue globale de l'accord-cadre. <br/>
<br/>
              7. En premier lieu, aux termes de l'article 70 du décret du 25 mars 2016 : " I. - Les acheteurs peuvent conclure des accords-cadres définis à l'article 4 de l'ordonnance du 23 juillet 2015 susvisée avec un ou plusieurs opérateurs économiques. (...) Lorsque l'accord-cadre fixe toutes les stipulations contractuelles, il est exécuté au fur et à mesure de l'émission de bons de commande dans les conditions fixées à l'article 72. (...) / II. - Les accords-cadres peuvent être conclus : 1° Soit avec un minimum et un maximum en valeur ou en quantité ; 2° Soit avec seulement un minimum ou un maximum ; 3° Soit sans minimum ni maximum (...) ".<br/>
<br/>
              8. Il résulte de ces dispositions que l'acheteur public n'est pas tenu de fixer un montant maximum pour l'accord-cadre qu'il entend conclure. En outre, aucune règle ni aucun principe ne lui interdit, dans le cadre d'une procédure négociée, qu'il ait informé ou non les candidats dans les documents de la consultation que la négociation pourrait le conduire à fixer un montant maximum, de fixer effectivement un tel montant en fin de procédure. Par conséquent, en jugeant que le pouvoir adjudicateur, dans le cadre d'une procédure négociée, était tenu, dès lors qu'il avait envisagé d'assigner un montant maximal à un marché, de mentionner ce montant dans les documents de la consultation, le juge du référé précontractuel du tribunal administratif de Toulon a commis une erreur de droit.<br/>
<br/>
              9. En deuxième lieu, aux termes de l'annexe IV de la directive 2009/81/CE du Parlement européen et du Conseil du 13 juillet 2009 relative à la coordination des procédures de passation de certains marchés de travaux, de fournitures et de services par des pouvoirs adjudicateurs ou entités adjudicatrices dans les domaines de la défense et de la sécurité, l'avis de marché doit indiquer, dans le cas d'un accord-cadre, outre la " quantité des services à fournir ", " la valeur totale des prestations estimée pour toute la durée de l'accord-cadre ". Le modèle d'avis de marché établi, pour les marchés de défense ou de sécurité, par l'annexe XIV au règlement n° 2015/1986 établissant les formulaires standard pour la publication d'avis dans le cadre de la passation de marchés publics, prévoit que doit figurer dans l'avis de marché, outre la " quantité ou étendue globale ", une " estimation de la valeur totale des acquisitions pour l'ensemble de la durée de l'accord-cadre ". <br/>
<br/>
              10. Il ressort des pièces du dossier soumis au juge du référé que le cadre " quantité ou étendue globale " de l'avis d'appel public à la concurrence de l'accord-cadre contesté ne comportait aucune des indications requises. Par suite, en relevant que le pouvoir adjudicateur avait manqué à ses obligations de publicité et de mise en concurrence en ne faisant figurer aucune information, même à titre indicatif ou prévisionnel, dans l'avis de marché, et en jugeant qu'une telle irrégularité ne pouvait être palliée ni par le fait qu'avait été fourni aux candidats admis à présenter une offre un scénario de commandes, ni par la transmission aux candidats à l'issue de la première réunion de négociation d'un tableau récapitulant les bons de commande et métrés associés notifiés au titre du marché précédent, le juge du référé précontractuel n'a entaché son ordonnance, qui est suffisamment motivée, ni d'erreur de droit, ni de dénaturation.<br/>
<br/>
              11. Il appartient toutefois au juge du référé précontractuel de rechercher si l'entreprise qui le saisit se prévaut de manquements qui, eu égard à leur portée et au stade de la procédure auquel ils se rapportent, sont susceptibles de l'avoir lésée ou risquent de la léser, fût-ce de façon indirecte en avantageant une entreprise concurrente.<br/>
<br/>
              12. Il ressort des pièces du dossier soumis au juge du référé que l'article 6 du règlement de consultation de l'accord-cadre stipule : " Le prix global (...) est calculé TTC sur la base des montants suivants : / - Le prix de prestations qui pourraient être commandées sur la base du barème (PBa), évalué à l'aide d'un scénario d'emploi du barème établi sur la base du retour d'expérience du SSD et joint en annexe A au présent règlement (...) ". Ce scénario d'emploi comporte une colonne " quantité totale commandée au titre du scénario d'emploi du barème " qui donne des indications notamment en nombre de jours ou de m2 pour les 250 lignes de prestations, fondées sur les prestations effectivement réalisées dans le cadre du marché en cours. De plus, les candidats ont été en mesure de demander des précisions au cours des réunions de négociation. Dès lors, la direction du service de soutien de la flotte de Toulon doit être regardée comme ayant apporté des précisions suffisantes quant à l'étendue des besoins à satisfaire aux entreprises dont la candidature a été admise. Par conséquent, en jugeant que le manquement aux obligations de publicité résultant de l'absence d'indication portant sur l'étendue globale du marché avait nécessairement eu un impact sur les prix unitaires proposés par la société SONOCAR Industrie et, par suite, sur l'élaboration de son offre de prix global, et avait été ainsi susceptible de la léser, alors même que cette société avait participé aux négociations, le juge du référé précontractuel du tribunal administratif de Toulon a entaché son ordonnance d'une erreur de qualification juridique. <br/>
<br/>
              13. Il résulte de ce qui précède que l'ordonnance du juge du référé précontractuel du tribunal administratif de Toulon doit être annulée en tant qu'elle statue sur les conclusions tendant à la suspension de l'exécution de l'ensemble des décisions se rapportant à la passation de l'accord-cadre.<br/>
<br/>
              14. Dans les circonstances de l'espèce, il y a lieu, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire, dans cette mesure, au titre de la procédure de référé engagée.<br/>
<br/>
              15. En premier lieu, doivent être écartés, pour les motifs précédemment exposés, les moyens tirés de l'absence de mention d'un montant maximum de l'accord-cadre dans les documents de la consultation et de ce que l'avis de marché ne comportait aucune mention relative à la quantité ou à l'étendue globale de l'accord-cadre.<br/>
<br/>
              16. En deuxième lieu, il résulte de l'instruction que la société Prezioso Linjebygg a produit l'ensemble des documents attestant de la recevabilité de sa candidature. Par suite, le moyen tiré de ce que la ministre des armées aurait dû la rejeter au motif que la société n'aurait pas été à jour de ses obligations fiscales et sociales ne peut qu'être écarté.<br/>
<br/>
              17. En troisième lieu, aux termes de l'article 6 du règlement de consultation de l'accord-cadre : " Toute ligne de prestation sur barèmes non renseignée par le soumissionnaire (prestation non chiffrée ou comportant une exclusion) est complétée par le service par le prix le plus élevé de toutes les offres multiplié par deux ". Contrairement à ce que soutient la société SONACAR Industrie, la circonstance qu'elle a rempli toutes les lignes de ce document tout en n'obtenant pas une note plus élevée que celle de son concurrent ne saurait révéler, par elle-même, que cette méthode de notation du critère du prix n'aurait pas été appliquée. Par ailleurs, les quelques incohérences ou approximations dans la présentation des prestations faisant l'objet de l'accord-cadre, relevées par la société dans l'annexe financière ayant permis de comparer les offres des candidats, n'établissent pas que cette comparaison aurait été faussée.<br/>
<br/>
              18. Enfin, si la société SONACAR Industrie soutient que le pouvoir adjudicateur aurait dénaturé les termes de son offre sur deux points, affectant ainsi la notation du sous-critère " management " du critère " valeur technique et managériale ", il résulte de l'instruction que, même si elle avait obtenu sur ce sous-critère la note de 20 sur 20, et non la note de 19,77 sur 20 qui lui a été attribuée, sa note globale serait restée inférieure à celle de la société Prezioso Linjebygg. Le manquement, à le supposer établi, n'est donc, en tout état de cause, pas susceptible de l'avoir lésée.<br/>
<br/>
              19. Il résulte de tout ce qui précède que les conclusions de la société SONOCAR Industrie tendant à la suspension de l'exécution de l'ensemble des décisions se rapportant à la passation de l'accord-cadre doivent être rejetées.<br/>
<br/>
              20. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société SONOCAR Industrie le versement d'une somme de 4 500 euros à la société Prezioso Linjebygg sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative, au titre de l'ensemble de la procédure. Les dispositions de cet article font obstacle à ce qu'une somme soit mise à la charge de l'Etat et de la société Prezioso Linjebygg qui ne sont pas, dans la présente instance, les parties perdantes.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er et 2 de l'ordonnance du 11 janvier 2019 du juge du référé précontractuel du tribunal administratif de Toulon sont annulés. <br/>
Article 2 : Les conclusions à fin de suspension de l'exécution des décisions se rapportant à la passation de l'accord-cadre présentées par la société SONOCAR Industrie devant le juge du référé précontractuel du tribunal administratif de Toulon et ses conclusions présentées devant le Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La société SONOCAR Industrie versera à la société Prezioso Linjebygg une somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions du pourvoi de la ministre des armées est rejeté.<br/>
Article 5 : La présente décision sera notifiée à la ministre des armées, à la société Prezioso Linjebygg et à la société SONOCAR Industrie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
