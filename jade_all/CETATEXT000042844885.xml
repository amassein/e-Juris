<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042844885</ID>
<ANCIEN_ID>JG_L_2020_12_000000434252</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/84/48/CETATEXT000042844885.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 30/12/2020, 434252, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434252</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SARL DIDIER, PINET</AVOCATS>
<RAPPORTEUR>Mme Rose-Marie Abel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:434252.20201230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme D... A... a demandé au tribunal administratif de Paris d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de l'arrêté du 28 juin 2019 par lequel le Centre d'action sociale de la Ville de Paris (CASVP) a prononcé à son encontre la sanction d'exclusion temporaire de fonctions d'un an, assortie d'un sursis de six mois et d'enjoindre au CASVP de la réintégrer dans ses fonctions jusqu'à ce qu'il soit statué sur sa requête au fond, dans le délai de huit jours à compter de la notification de l'ordonnance à intervenir, sous astreinte de 100 euros par jour de retard. Par une ordonnance n°1916395/2 du 20 août 2019, le juge des référés du tribunal administratif de Paris a fait droit à sa demande.<br/>
<br/>
              Par une requête et des observations complémentaires, enregistrées les 4 septembre et 14 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, le CASVP demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au titre de la procédure de référé engagée, de rejeter les demandes de Mme A... ;<br/>
<br/>
              3°) de mettre à la charge de Mme A... la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 94-415 du 24 mai 1994 ;<br/>
              - le décret n° 89-677 du 18 septembre 1989; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Rose-Marie Abel, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Foussard, Froger, avocat du Centre d'action sociale de la Ville de Paris (CASVP) et à la SARL Didier, Pinet, avocat de Mme A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Paris que, par un arrêté du 28 juin 2019, la directrice générale du centre d'action sociale de la Ville de Paris (CASVP) a prononcé à l'encontre de Mme A..., agent social affectée à la cuisine de l'établissement d'hébergement pour personnes âgées Oasis, une sanction d'exclusion temporaire de fonctions d'un an, assortie d'un sursis de six mois, sanction relevant du 3ème groupe. Mme A... a saisi le tribunal administratif de Paris d'un recours pour excès de pouvoir contre cette décision et d'une demande en référé tendant à la suspension de son exécution sur le fondement de l'article L. 521-1 du code de justice administrative. Le CASVP se pourvoit en cassation contre l'ordonnance du 20 août 2019 par laquelle le juge des référés du tribunal administratif de Paris a suspendu l'exécution de cette décision.<br/>
<br/>
              3. Aux termes de l'article 1er du décret du 24 mai 1994 portant dispositions statutaires relatives aux administrations parisiennes : " Le présent décret s'applique aux personnels de la Ville de Paris et de ses établissements publics administratifs, ci-après dénommés les administrations parisiennes, ainsi qu'aux personnels relevant du droit public de ses établissements publics industriels et commerciaux. / Ces personnels sont dénommés ci-après personnels des administrations parisiennes. " Aux termes de l'article 1er de l'arrêté du 3 avril 2019 portant délégation de signature de la maire de Paris : "  La signature de la Maire de Paris, Présidente du Conseil d'administration du Centre d'action sociale de la Ville de Paris, est déléguée à Madame C... B..., directrice générale du Centre d'action sociale de la Ville de Paris, à l'effet de signer tous arrêtés, actes et décisions préparés par les services placés sous son autorité, relatifs à la situation des personnels titulaires et contractuels du Centre d'action sociale de la Ville de Paris à l'exception de ceux relatifs à la situation des directeurs et directeurs adjoints d'établissements soumis aux règles définies par la fonction publique hospitalière ". Aux termes de l'article 2 de cet arrêté, la délégation de signature permet à la directrice générale du CASVP  " de signer tous arrêtés, actes et décisions préparés par les services placés sous son autorité, relatifs à la situation des agents affectés au Centre d'action sociale de la Ville de Paris appartenant à un corps d'administrations parisiennes ou y étant détaché, à l'exception : / - des actes de nomination dans leur corps et dans les grades ; / - des arrêtés de radiation des cadres suite à une démission, à un licenciement, à une révocation, à un abandon de poste ou pour perte des droits civiques ; / - des décisions infligeant les sanctions disciplinaires des deuxième, troisième et quatrième groupes ".<br/>
<br/>
              4. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Paris que Mme A... est un agent titulaire relevant du corps des agents sociaux du CASVP et appartient donc aux " personnels " de ce centre, conformément à l'article 1er du décret du 24 mai 1994. Par suite, en jugeant que le moyen tiré de l'incompétence de la directrice générale du CASVP pour prononcer une sanction du 3ème groupe à l'encontre de Mme A... était de nature à créer un doute sérieux quant à la légalité de cette décision, alors que cette directrice générale tient des dispositions de l'article 1er de l'arrêté cité au point 3 compétence pour prononcer des sanctions, quel que soit leur groupe, à l'encontre des personnels propres du centre, le juge des référés a commis une erreur de droit. <br/>
<br/>
              5. Il résulte de ce qui précède que le CASVP est fondé à demander l'annulation de l'ordonnance qu'il attaque.<br/>
<br/>
              6. Il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée.<br/>
<br/>
              7. Mme A... soutient que la décision attaquée a été prise par une autorité incompétente, que la conseillère de Paris ayant siégé au conseil de discipline n'avait pas qualité pour y siéger, que les articles 4, 9 et 14 du décret du 18 septembre 1989 relatif à la procédure disciplinaire applicable aux fonctionnaires territoriaux ont été méconnus, que la sanction est disproportionnée et fondée sur des faits relevant de l'insuffisance professionnelle ou des faits non assortis de précisions suffisantes et qu'elle a été placée dans des conditions de travail irrégulières et dégradées. Aucun de ces moyens ne paraît, en l'état de l'instruction, propre à créer un doute sérieux quant à la légalité de la décision contestée.<br/>
<br/>
              8. L'une des conditions posées par l'article L. 521-1 du code de justice administrative n'étant pas remplie, la demande de Mme A... tendant à ce que soit ordonnée la suspension de l'exécution de l'arrêté du 28 juin 2019 par lequel le CASVP a prononcé à son encontre la sanction d'exclusion temporaire de fonctions d'un an, assortie d'un sursis de six mois, doit être rejetée. <br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du CASVP qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme A... la somme que demande le CASVP au titre des mêmes dispositions. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 20 août 2019 du juge des référés du tribunal administratif de Paris est annulée. <br/>
Article 2 : La demande présentée par Mme A... devant le juge des référés du tribunal administratif de Paris est rejetée.<br/>
Article 3 : Les conclusions présentées par Mme A... et le CASVP au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au centre d'action sociale de la Ville de Paris et à Mme D... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
